	.text
	.file	"unate.bc"
	.globl	map_cover_to_unate
	.p2align	4, 0x90
	.type	map_cover_to_unate,@function
map_cover_to_unate:                     # @map_cover_to_unate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	subq	%r14, %rdi
	sarq	$3, %rdi
	addq	$-3, %rdi
	movl	cdata+36(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movq	8(%r14), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	addl	$-3, %eax
	movl	%eax, 12(%r15)
	testl	%eax, %eax
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph64
	movq	24(%r15), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movl	4(%r15), %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_clear
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	incl	%ebp
	cmpl	12(%r15), %ebp
	jl	.LBB0_2
.LBB0_3:                                # %.preheader
	movl	cube(%rip), %ebx
	testl	%ebx, %ebx
	jle	.LBB0_14
# BB#4:                                 # %.lr.ph59
	movq	cdata(%rip), %r9
	leaq	24(%r14), %r8
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	cmpl	$0, (%r9,%r11,4)
	jle	.LBB0_13
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movl	$1, %edi
	movl	%r11d, %ecx
	shll	%cl, %edi
	movl	$1, %esi
	movl	%r10d, %ecx
	shll	%cl, %esi
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB0_12
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	24(%r15), %rcx
	movl	%r10d, %ebx
	sarl	$5, %ebx
	incl	%ebx
	movl	%r11d, %eax
	sarl	$5, %eax
	incl	%eax
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%rdx,%rax,4), %edi
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=2
	orl	%esi, (%rcx,%rbx,4)
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=2
	movslq	(%r15), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	movq	(%rbp), %rdx
	addq	$8, %rbp
	testq	%rdx, %rdx
	jne	.LBB0_8
# BB#11:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	cube(%rip), %ebx
.LBB0_12:                               # %._crit_edge
                                        #   in Loop: Header=BB0_5 Depth=1
	incl	%r10d
.LBB0_13:                               #   in Loop: Header=BB0_5 Depth=1
	incq	%r11
	movslq	%ebx, %rax
	cmpq	%rax, %r11
	jl	.LBB0_5
.LBB0_14:                               # %._crit_edge60
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	map_cover_to_unate, .Lfunc_end0-map_cover_to_unate
	.cfi_endproc

	.globl	map_unate_to_cover
	.p2align	4, 0x90
	.type	map_unate_to_cover,@function
map_unate_to_cover:                     # @map_unate_to_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	12(%r15), %edi
	movl	cube(%rip), %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	sf_new
	movl	12(%r15), %ecx
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%ecx, 12(%rax)
	movslq	cube+4(%rip), %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	testq	%rbx, %rbx
	jle	.LBB1_13
# BB#1:                                 # %.lr.ph89
	movq	cdata+24(%rip), %rcx
	testb	$1, %bl
	jne	.LBB1_3
# BB#2:
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	cmpl	$1, %ebx
	jne	.LBB1_8
	jmp	.LBB1_13
.LBB1_3:
	cmpl	$0, (%rcx)
	je	.LBB1_4
# BB#5:
	movl	$0, (%rax)
	movl	$1, %ebp
	jmp	.LBB1_6
.LBB1_4:
	xorl	%ebp, %ebp
.LBB1_6:                                # %.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %ebx
	je	.LBB1_13
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rcx,%rdx,4)
	je	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	movslq	%ebp, %rsi
	incl	%ebp
	movl	%edx, (%rax,%rsi,4)
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=1
	cmpl	$0, 4(%rcx,%rdx,4)
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_8 Depth=1
	movslq	%ebp, %rsi
	incl	%ebp
	leal	1(%rdx), %edi
	movl	%edi, (%rax,%rsi,4)
.LBB1_12:                               #   in Loop: Header=BB1_8 Depth=1
	addq	$2, %rdx
	cmpq	%rbx, %rdx
	jl	.LBB1_8
.LBB1_13:                               # %._crit_edge90
	movslq	(%r15), %rdx
	movslq	12(%r15), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB1_34
# BB#14:                                # %.lr.ph84
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	24(%rdx), %r13
	movq	24(%r15), %r11
	leaq	(%r11,%rcx,4), %r8
	testl	%ebp, %ebp
	jle	.LBB1_29
# BB#15:                                # %.lr.ph84.split.us.preheader
	movq	cube+24(%rip), %r9
	movq	cube+16(%rip), %r10
	movq	cdata(%rip), %rbx
	movl	%ebp, %r12d
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph84.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_21 Depth 2
                                        #       Child Loop BB1_24 Depth 3
	movl	cube(%rip), %esi
	movl	$1, %ecx
	cmpl	$33, %esi
	jl	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	decl	%esi
	sarl	$5, %esi
	incl	%esi
	movl	%esi, %ecx
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=1
	movl	%ecx, (%r13)
	movslq	%ecx, %rsi
	shll	$5, %ecx
	subl	cube(%rip), %ecx
	movl	$-1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	incq	%rsi
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, -4(%r13,%rsi,4)
	decq	%rsi
	movl	$-1, %edi
	cmpq	$1, %rsi
	jg	.LBB1_19
# BB#20:                                # %.lr.ph80.us.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph80.us
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_24 Depth 3
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%r11,%rcx,4), %ecx
	btl	%ebp, %ecx
	jae	.LBB1_27
# BB#22:                                #   in Loop: Header=BB1_21 Depth=2
	movslq	(%rax,%rbp,4), %rcx
	movslq	(%r9,%rcx,4), %rsi
	movl	(%r10,%rcx,4), %r14d
	cmpl	%esi, %r14d
	jg	.LBB1_27
# BB#23:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB1_21 Depth=2
	movslq	%r14d, %rdi
	decq	%rdi
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph.us
                                        #   Parent Loop BB1_16 Depth=1
                                        #     Parent Loop BB1_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, 4(%rbx,%rdi,4)
	jne	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_24 Depth=3
	movl	$-2, %edx
	movl	%r14d, %ecx
	roll	%cl, %edx
	movl	%r14d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edx, 4(%r13,%rcx,4)
.LBB1_26:                               #   in Loop: Header=BB1_24 Depth=3
	incq	%rdi
	incl	%r14d
	cmpq	%rsi, %rdi
	jl	.LBB1_24
.LBB1_27:                               # %.loopexit.us
                                        #   in Loop: Header=BB1_21 Depth=2
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB1_21
# BB#28:                                # %._crit_edge.us
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	(%rcx), %rcx
	leaq	(%r13,%rcx,4), %r13
	movslq	(%r15), %rcx
	leaq	(%r11,%rcx,4), %r11
	cmpq	%r8, %r11
	jb	.LBB1_16
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph84.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
	movl	cube(%rip), %esi
	movl	$1, %ecx
	cmpl	$33, %esi
	jl	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	decl	%esi
	sarl	$5, %esi
	incl	%esi
	movl	%esi, %ecx
.LBB1_31:                               #   in Loop: Header=BB1_29 Depth=1
	movl	%ecx, (%r13)
	movslq	%ecx, %rsi
	shll	$5, %ecx
	subl	cube(%rip), %ecx
	movl	$-1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	incq	%rsi
	.p2align	4, 0x90
.LBB1_32:                               #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, -4(%r13,%rsi,4)
	decq	%rsi
	movl	$-1, %edi
	cmpq	$1, %rsi
	jg	.LBB1_32
# BB#33:                                # %.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	(%rcx), %rcx
	leaq	(%r13,%rcx,4), %r13
	movslq	(%r15), %rcx
	leaq	(%r11,%rcx,4), %r11
	cmpq	%r8, %r11
	jb	.LBB1_29
.LBB1_34:                               # %._crit_edge85
	testq	%rax, %rax
	je	.LBB1_36
# BB#35:
	movq	%rax, %rdi
	callq	free
.LBB1_36:
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	map_unate_to_cover, .Lfunc_end1-map_unate_to_cover
	.cfi_endproc

	.globl	unate_compl
	.p2align	4, 0x90
	.type	unate_compl,@function
unate_compl:                            # @unate_compl
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r15
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	shll	$16, %eax
	orl	%eax, (%rbx)
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movq	%r14, %rdi
	callq	unate_complement
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	sf_rev_contain          # TAILCALL
.Lfunc_end2:
	.size	unate_compl, .Lfunc_end2-unate_compl
	.cfi_endproc

	.globl	unate_complement
	.p2align	4, 0x90
	.type	unate_complement,@function
unate_complement:                       # @unate_complement
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	12(%r12), %eax
	cmpl	$1, %eax
	je	.LBB3_3
# BB#1:
	testl	%eax, %eax
	jne	.LBB3_9
# BB#2:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	movl	4(%r12), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movq	24(%r15), %rax
	movl	12(%r15), %ecx
	leal	1(%rcx), %edx
	imull	(%r15), %ecx
	movl	%edx, 12(%r15)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	movl	4(%r12), %esi
	xorl	%eax, %eax
	callq	set_clear
	jmp	.LBB3_91
.LBB3_3:
	movq	24(%r12), %r14
	movl	4(%r12), %edi
	xorl	%eax, %eax
	movl	%edi, %esi
	callq	sf_new
	movq	%rax, %r15
	movl	4(%r12), %esi
	testl	%esi, %esi
	jle	.LBB3_8
# BB#4:                                 # %.lr.ph140
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	sarl	$5, %eax
	movslq	%eax, %r13
	movl	4(%r14,%r13,4), %eax
	movl	$1, %ebx
	movl	%ebp, %ecx
	shll	%cl, %ebx
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %eax
	jae	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	incq	%r13
	movq	24(%r15), %rax
	movl	12(%r15), %ecx
	leal	1(%rcx), %edx
	imull	(%r15), %ecx
	movl	%edx, 12(%r15)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	callq	set_clear
	orl	%ebx, (%rax,%r13,4)
	movl	4(%r12), %esi
.LBB3_7:                                #   in Loop: Header=BB3_5 Depth=1
	incl	%ebp
	cmpl	%esi, %ebp
	jl	.LBB3_5
.LBB3_8:                                # %._crit_edge141
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	jmp	.LBB3_91
.LBB3_9:
	movl	4(%r12), %ebx
	cmpl	$33, %ebx
	jge	.LBB3_11
# BB#10:
	movl	$8, %edi
	jmp	.LBB3_12
.LBB3_11:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_12:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r14
	movl	4(%r12), %r15d
	movl	12(%r12), %edi
	incl	%r15d
	testl	%edi, %edi
	jle	.LBB3_19
# BB#13:                                # %.lr.ph135
	movq	24(%r12), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_14:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	shrl	$16, %eax
	cmpl	%r15d, %eax
	jae	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_14 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	movzwl	2(%rbx), %r15d
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_14 Depth=1
	jne	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_14 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	set_or
.LBB3_18:                               #   in Loop: Header=BB3_14 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	incl	%ebp
	movl	12(%r12), %edi
	cmpl	%edi, %ebp
	jl	.LBB3_14
.LBB3_19:                               # %._crit_edge136
	cmpl	$1, %r15d
	je	.LBB3_22
# BB#20:                                # %._crit_edge136
	testl	%r15d, %r15d
	jne	.LBB3_48
# BB#21:
	movl	$0, 12(%r12)
	testq	%r14, %r14
	jne	.LBB3_89
	jmp	.LBB3_90
.LBB3_22:
	movl	4(%r12), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB3_43
# BB#23:                                # %.lr.ph.i
	movq	24(%r12), %rbp
	leaq	(%rbp,%rax,4), %r13
	movq	24(%r15), %rbx
	.p2align	4, 0x90
.LBB3_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_36 Depth 2
                                        #     Child Loop BB3_40 Depth 2
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	je	.LBB3_42
# BB#25:                                #   in Loop: Header=BB3_24 Depth=1
	movl	(%rbp), %eax
	andl	$1023, %eax             # imm = 0x3FF
	leaq	1(%rax), %r10
	cmpq	$8, %r10
	jb	.LBB3_39
# BB#26:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_24 Depth=1
	movq	%r10, %r9
	andq	$2040, %r9              # imm = 0x7F8
	je	.LBB3_39
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_24 Depth=1
	leaq	4(%rbp,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB3_29
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_24 Depth=1
	leaq	4(%rbx,%rax,4), %rcx
	cmpq	%rcx, %rbp
	jb	.LBB3_39
.LBB3_29:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_24 Depth=1
	leaq	-8(%r9), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB3_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_24 Depth=1
	leaq	-12(%rbx,%rax,4), %rcx
	leaq	-12(%rbp,%rax,4), %rdx
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_32:                               # %vector.body.prol
                                        #   Parent Loop BB3_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	%xmm0, (%rcx,%rsi,4)
	movups	%xmm1, -16(%rcx,%rsi,4)
	addq	$-8, %rsi
	incq	%rdi
	jne	.LBB3_32
# BB#33:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_24 Depth=1
	negq	%rsi
	cmpq	$24, %r8
	jae	.LBB3_35
	jmp	.LBB3_37
.LBB3_30:                               #   in Loop: Header=BB3_24 Depth=1
	xorl	%esi, %esi
	cmpq	$24, %r8
	jb	.LBB3_37
.LBB3_35:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_24 Depth=1
	movq	%r10, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbx,%rax,4), %rdx
	leaq	-12(%rbp,%rax,4), %rcx
	.p2align	4, 0x90
.LBB3_36:                               # %vector.body
                                        #   Parent Loop BB3_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rsi,4), %xmm0
	movups	-16(%rcx,%rsi,4), %xmm1
	movups	%xmm0, (%rdx,%rsi,4)
	movups	%xmm1, -16(%rdx,%rsi,4)
	movups	-32(%rcx,%rsi,4), %xmm0
	movups	-48(%rcx,%rsi,4), %xmm1
	movups	%xmm0, -32(%rdx,%rsi,4)
	movups	%xmm1, -48(%rdx,%rsi,4)
	movups	-64(%rcx,%rsi,4), %xmm0
	movups	-80(%rcx,%rsi,4), %xmm1
	movups	%xmm0, -64(%rdx,%rsi,4)
	movups	%xmm1, -80(%rdx,%rsi,4)
	movups	-96(%rcx,%rsi,4), %xmm0
	movups	-112(%rcx,%rsi,4), %xmm1
	movups	%xmm0, -96(%rdx,%rsi,4)
	movups	%xmm1, -112(%rdx,%rsi,4)
	addq	$-32, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB3_36
.LBB3_37:                               # %middle.block
                                        #   in Loop: Header=BB3_24 Depth=1
	cmpq	%r9, %r10
	je	.LBB3_41
# BB#38:                                #   in Loop: Header=BB3_24 Depth=1
	subq	%r9, %rax
	.p2align	4, 0x90
.LBB3_39:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_24 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB3_40:                               # %scalar.ph
                                        #   Parent Loop BB3_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rax,4), %ecx
	movl	%ecx, -4(%rbx,%rax,4)
	decq	%rax
	jg	.LBB3_40
.LBB3_41:                               # %.loopexit200
                                        #   in Loop: Header=BB3_24 Depth=1
	incl	12(%r15)
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
.LBB3_42:                               #   in Loop: Header=BB3_24 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r13, %rbp
	jb	.LBB3_24
.LBB3_43:                               # %abs_covered_many.exit
	movq	%r14, %r13
	movq	%r15, %rdi
	callq	unate_complement
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	cmpl	$0, 12(%r14)
	jle	.LBB3_44
# BB#45:                                # %.lr.ph130
	movq	24(%r14), %rbp
	xorl	%ebx, %ebx
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB3_46:                               # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	set_or
	movslq	(%r14), %rax
	leaq	(%rbp,%rax,4), %rbp
	incl	%ebx
	cmpl	12(%r14), %ebx
	jl	.LBB3_46
# BB#47:
	movq	%r14, %r12
	movq	%r15, %r14
	testq	%r14, %r14
	jne	.LBB3_89
	jmp	.LBB3_90
.LBB3_48:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	sf_count_restricted
	movslq	4(%r12), %rcx
	testq	%rcx, %rcx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jle	.LBB3_55
# BB#49:                                # %.lr.ph.i114
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	movl	$-1, %r14d
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB3_52
# BB#50:                                # %.prol.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_51:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rdx,4), %ebp
	cmpl	%ebx, %ebp
	cmovgl	%edx, %r14d
	cmovgel	%ebp, %ebx
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB3_51
.LBB3_52:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB3_54
	.p2align	4, 0x90
.LBB3_53:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rdx,4), %esi
	cmpl	%ebx, %esi
	cmovgl	%edx, %r14d
	cmovgel	%esi, %ebx
	movl	4(%rax,%rdx,4), %esi
	leal	1(%rdx), %edi
	cmpl	%ebx, %esi
	cmovlel	%r14d, %edi
	cmovgel	%esi, %ebx
	movl	8(%rax,%rdx,4), %esi
	leal	2(%rdx), %ebp
	cmpl	%ebx, %esi
	cmovlel	%edi, %ebp
	cmovgel	%esi, %ebx
	movl	12(%rax,%rdx,4), %esi
	leal	3(%rdx), %r14d
	cmpl	%ebx, %esi
	cmovlel	%ebp, %r14d
	cmovgel	%esi, %ebx
	addq	$4, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB3_53
.LBB3_54:
	movq	%rax, %rdi
	callq	free
	cmpl	$-1, %r14d
	jne	.LBB3_58
	jmp	.LBB3_57
.LBB3_44:
	movq	%r14, %r12
	movq	%r13, %r14
	testq	%r14, %r14
	jne	.LBB3_89
	jmp	.LBB3_90
.LBB3_55:                               # %._crit_edge.i
	testq	%rax, %rax
	je	.LBB3_57
# BB#56:                                # %.thread30.i
	movq	%rax, %rdi
	callq	free
.LBB3_57:                               # %.thread.i
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	$-1, %r14d
.LBB3_58:                               # %abs_select_restricted.exit
	movl	4(%r12), %esi
	movl	12(%r12), %edi
	xorl	%eax, %eax
	callq	sf_new
	movl	(%r12), %esi
	movl	12(%r12), %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB3_79
# BB#59:                                # %.lr.ph.i117
	movq	24(%r12), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,4), %rbp
	movq	24(%rax), %rdi
	movl	%r14d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %r9
	movl	$1, %r10d
	movl	%r14d, %ecx
	shll	%cl, %r10d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_60:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_68 Depth 2
                                        #     Child Loop BB3_72 Depth 2
                                        #     Child Loop BB3_76 Depth 2
	testl	(%rdx,%r9,4), %r10d
	jne	.LBB3_78
# BB#61:                                #   in Loop: Header=BB3_60 Depth=1
	movl	(%rdx), %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	leaq	1(%rcx), %r13
	cmpq	$8, %r13
	jb	.LBB3_75
# BB#62:                                # %min.iters.checked164
                                        #   in Loop: Header=BB3_60 Depth=1
	movq	%r13, %r11
	andq	$2040, %r11             # imm = 0x7F8
	je	.LBB3_75
# BB#63:                                # %vector.memcheck181
                                        #   in Loop: Header=BB3_60 Depth=1
	leaq	4(%rdx,%rcx,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB3_65
# BB#64:                                # %vector.memcheck181
                                        #   in Loop: Header=BB3_60 Depth=1
	leaq	4(%rdi,%rcx,4), %rsi
	cmpq	%rsi, %rdx
	jb	.LBB3_75
.LBB3_65:                               # %vector.body160.preheader
                                        #   in Loop: Header=BB3_60 Depth=1
	leaq	-8(%r11), %r15
	movl	%r15d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB3_66
# BB#67:                                # %vector.body160.prol.preheader
                                        #   in Loop: Header=BB3_60 Depth=1
	movq	%rax, %r8
	leaq	-12(%rdi,%rcx,4), %rax
	leaq	-12(%rdx,%rcx,4), %rbp
	negq	%rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_68:                               # %vector.body160.prol
                                        #   Parent Loop BB3_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, -16(%rax,%rsi,4)
	addq	$-8, %rsi
	incq	%rbx
	jne	.LBB3_68
# BB#69:                                # %vector.body160.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_60 Depth=1
	negq	%rsi
	movq	%r8, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpq	$24, %r15
	jae	.LBB3_71
	jmp	.LBB3_73
.LBB3_66:                               #   in Loop: Header=BB3_60 Depth=1
	xorl	%esi, %esi
	cmpq	$24, %r15
	jb	.LBB3_73
.LBB3_71:                               # %vector.body160.preheader.new
                                        #   in Loop: Header=BB3_60 Depth=1
	movq	%r13, %r15
	andq	$-8, %r15
	negq	%r15
	negq	%rsi
	leaq	-12(%rdi,%rcx,4), %rbx
	leaq	-12(%rdx,%rcx,4), %r8
	.p2align	4, 0x90
.LBB3_72:                               # %vector.body160
                                        #   Parent Loop BB3_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r8,%rsi,4), %xmm0
	movups	-16(%r8,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, -16(%rbx,%rsi,4)
	movups	-32(%r8,%rsi,4), %xmm0
	movups	-48(%r8,%rsi,4), %xmm1
	movups	%xmm0, -32(%rbx,%rsi,4)
	movups	%xmm1, -48(%rbx,%rsi,4)
	movups	-64(%r8,%rsi,4), %xmm0
	movups	-80(%r8,%rsi,4), %xmm1
	movups	%xmm0, -64(%rbx,%rsi,4)
	movups	%xmm1, -80(%rbx,%rsi,4)
	movups	-96(%r8,%rsi,4), %xmm0
	movups	-112(%r8,%rsi,4), %xmm1
	movups	%xmm0, -96(%rbx,%rsi,4)
	movups	%xmm1, -112(%rbx,%rsi,4)
	addq	$-32, %rsi
	cmpq	%rsi, %r15
	jne	.LBB3_72
.LBB3_73:                               # %middle.block161
                                        #   in Loop: Header=BB3_60 Depth=1
	cmpq	%r11, %r13
	je	.LBB3_77
# BB#74:                                #   in Loop: Header=BB3_60 Depth=1
	subq	%r11, %rcx
	.p2align	4, 0x90
.LBB3_75:                               # %scalar.ph162.preheader
                                        #   in Loop: Header=BB3_60 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB3_76:                               # %scalar.ph162
                                        #   Parent Loop BB3_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rcx,4), %esi
	movl	%esi, -4(%rdi,%rcx,4)
	decq	%rcx
	jg	.LBB3_76
.LBB3_77:                               # %.loopexit199
                                        #   in Loop: Header=BB3_60 Depth=1
	incl	12(%rax)
	movslq	(%rax), %rcx
	leaq	(%rdi,%rcx,4), %rdi
	movl	(%r12), %esi
.LBB3_78:                               #   in Loop: Header=BB3_60 Depth=1
	movslq	%esi, %rcx
	leaq	(%rdx,%rcx,4), %rdx
	cmpq	%rbp, %rdx
	jb	.LBB3_60
.LBB3_79:                               # %abs_covered.exit
	movq	%rax, %rdi
	callq	unate_complement
	movq	%rax, %rbx
	cmpl	$0, 12(%rbx)
	jle	.LBB3_82
# BB#80:                                # %.lr.ph126
	movq	24(%rbx), %rax
	movl	$1, %edx
	movl	%r14d, %ecx
	shll	%cl, %edx
	movl	%r14d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_81:                               # =>This Inner Loop Header: Depth=1
	orl	%edx, (%rax,%rcx,4)
	movslq	(%rbx), %rdi
	leaq	(%rax,%rdi,4), %rax
	incl	%esi
	cmpl	12(%rbx), %esi
	jl	.LBB3_81
.LBB3_82:                               # %._crit_edge127
	movl	12(%r12), %edx
	testl	%edx, %edx
	jle	.LBB3_87
# BB#83:                                # %.lr.ph
	movq	24(%r12), %rax
	movl	%r14d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rsi
	andl	$31, %r14d
	movl	$1, %edi
	movl	%r14d, %ecx
	shll	%cl, %edi
	movl	$-2, %r8d
	roll	%cl, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_84:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %ebp
	testl	%edi, %ebp
	je	.LBB3_86
# BB#85:                                #   in Loop: Header=BB3_84 Depth=1
	andl	%r8d, %ebp
	movl	%ebp, (%rax,%rsi,4)
	addl	$-65536, (%rax)         # imm = 0xFFFF0000
	movl	12(%r12), %edx
.LBB3_86:                               #   in Loop: Header=BB3_84 Depth=1
	movslq	(%r12), %rbp
	leaq	(%rax,%rbp,4), %rax
	incl	%ecx
	cmpl	%edx, %ecx
	jl	.LBB3_84
.LBB3_87:                               # %._crit_edge
	movq	%r12, %rdi
	callq	unate_complement
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	callq	sf_append
	movq	%rax, %r12
	movq	16(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB3_90
.LBB3_89:
	movq	%r14, %rdi
	callq	free
.LBB3_90:
	movq	%r12, %r15
.LBB3_91:
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	unate_complement, .Lfunc_end3-unate_complement
	.cfi_endproc

	.globl	exact_minimum_cover
	.p2align	4, 0x90
	.type	exact_minimum_cover,@function
exact_minimum_cover:                    # @exact_minimum_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi47:
	.cfi_def_cfa_offset 656
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB4_1
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph140
                                        # =>This Inner Loop Header: Depth=1
	sarl	%eax
	incl	%ebp
	testl	%eax, %eax
	jne	.LBB4_2
# BB#3:                                 # %._crit_edge141
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_save
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	lex_sort
	movq	%rax, %rbx
	movl	4(%rbx), %esi
	movl	$1, %r13d
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movq	%r12, 80(%rsp)
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movl	%ebp, 88(%rsp)
	movq	24(%r12), %rax
	movl	12(%r12), %ecx
	leal	1(%rcx), %edx
	imull	(%r12), %ecx
	movl	%edx, 12(%r12)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	movl	4(%rbx), %esi
	xorl	%eax, %eax
	callq	set_fill
	movl	(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	%edx, %eax
	imull	%ecx, %eax
	testl	%eax, %eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jle	.LBB4_27
# BB#4:                                 # %.lr.ph136.preheader
	movq	24(%rbx), %rdi
	decl	%edx
	imull	%ecx, %edx
	movslq	%edx, %rcx
	leaq	(%rdi,%rcx,4), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	cltq
	leaq	(%rdi,%rax,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph136
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
                                        #     Child Loop BB4_13 Depth 2
                                        #     Child Loop BB4_20 Depth 2
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	set_ord
	movl	%eax, %ecx
	movl	4(%r12), %esi
	xorl	%eax, %eax
	movl	%ecx, %edi
	callq	sf_new
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %r14
	movl	4(%r12), %esi
	testl	%esi, %esi
	jle	.LBB4_10
# BB#6:                                 # %.lr.ph126
                                        #   in Loop: Header=BB4_5 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_7:                                #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	$5, %eax
	movslq	%eax, %r15
	movl	4(%rdi,%r15,4), %eax
	movl	$1, %ebx
	movl	%ebp, %ecx
	shll	%cl, %ebx
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %eax
	jae	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=2
	incq	%r15
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
	leal	1(%rcx), %edx
	imull	(%r14), %ecx
	movl	%edx, 12(%r14)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	callq	set_fill
	movq	8(%rsp), %rdi           # 8-byte Reload
	notl	%ebx
	andl	%ebx, (%rax,%r15,4)
	movl	4(%r12), %esi
.LBB4_9:                                #   in Loop: Header=BB4_7 Depth=2
	incl	%ebp
	cmpl	%esi, %ebp
	jl	.LBB4_7
.LBB4_10:                               # %._crit_edge127
                                        #   in Loop: Header=BB4_5 Depth=1
	movslq	%r13d, %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	movq	%r14, 80(%rsp,%rcx)
	leal	1(%rax), %r13d
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	%edx, 88(%rsp,%rcx)
	testl	%eax, %eax
	jle	.LBB4_25
# BB#11:                                # %.lr.ph130
                                        #   in Loop: Header=BB4_5 Depth=1
	cmpq	64(%rsp), %rdi          # 8-byte Folded Reload
	movslq	%r13d, %r13
	movq	%r13, %r12
	je	.LBB4_19
# BB#12:                                # %.lr.ph130.split.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	shlq	$32, %r12
	movabsq	$-8589934592, %rax      # imm = 0xFFFFFFFE00000000
	addq	%rax, %r12
	movq	%r13, %r15
	shlq	$4, %r15
	leaq	48(%rsp), %rax
	addq	%rax, %r15
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph130.split
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	24(%r15), %eax
	cmpl	8(%r15), %eax
	jne	.LBB4_18
# BB#14:                                # %.critedge1
                                        #   in Loop: Header=BB4_13 Depth=2
	decq	%r13
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	(%r15), %rbp
	movq	16(%r15), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	unate_intersect
	movq	%rax, %r13
	movq	%r12, %rax
	sarq	$28, %rax
	leaq	80(%rsp,%rax), %rax
	orq	$8, %rax
	movl	(%rax), %eax
	leal	-1(%rax), %r14d
	cmpl	$10, %eax
	jg	.LBB4_17
# BB#15:                                # %.critedge1
                                        #   in Loop: Header=BB4_13 Depth=2
	movl	debug(%rip), %eax
	movl	$2048, %ecx             # imm = 0x800
	andl	%ecx, %eax
	je	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_13 Depth=2
	movl	12(%r13), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	12(%rbx), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	12(%rbp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	56(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %r9
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r8d          # 4-byte Reload
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB4_17:                               #   in Loop: Header=BB4_13 Depth=2
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%r13, (%r15)
	movl	%r14d, 8(%r15)
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %r12
	addq	$-16, %r15
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpq	$1, %r13
	movq	8(%rsp), %rdi           # 8-byte Reload
	jg	.LBB4_13
.LBB4_18:                               # %.lr.ph130.split..critedge.loopexit174_crit_edge
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph130.split.us.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	shlq	$32, %r12
	movq	%r13, %r15
	shlq	$4, %r15
	leaq	48(%rsp), %rax
	addq	%rax, %r15
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB4_20:                               # %.lr.ph130.split.us
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decq	%r13
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	(%r15), %rbx
	movq	16(%r15), %r13
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	unate_intersect
	movq	%rax, %r14
	movl	24(%r15), %eax
	xorl	%ecx, %ecx
	cmpl	8(%r15), %eax
	setge	%cl
	incq	%rcx
	imulq	%rbp, %rcx
	addq	%r12, %rcx
	sarq	$28, %rcx
	leaq	80(%rsp,%rcx), %rax
	orq	$8, %rax
	movl	(%rax), %eax
	leal	-1(%rax), %ebp
	cmpl	$10, %eax
	jg	.LBB4_23
# BB#21:                                # %.lr.ph130.split.us
                                        #   in Loop: Header=BB4_20 Depth=2
	movl	debug(%rip), %eax
	movl	$2048, %ecx             # imm = 0x800
	andl	%ecx, %eax
	je	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_20 Depth=2
	movl	12(%r14), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	12(%r13), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	12(%rbx), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	56(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %r9
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r8d          # 4-byte Reload
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB4_23:                               #   in Loop: Header=BB4_20 Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_free
	movq	%r14, (%r15)
	movl	%ebp, 8(%r15)
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	addq	%rbp, %r12
	addq	$-16, %r15
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpq	$1, %r13
	jg	.LBB4_20
# BB#24:                                #   in Loop: Header=BB4_5 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB4_25:                               # %.critedge
                                        #   in Loop: Header=BB4_5 Depth=1
	movslq	(%r12), %rax
	leaq	(%rdi,%rax,4), %rdi
	cmpq	72(%rsp), %rdi          # 8-byte Folded Reload
	jb	.LBB4_5
# BB#26:                                # %._crit_edge137.loopexit
	movq	80(%rsp), %r12
.LBB4_27:                               # %._crit_edge137
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %eax
	cmpl	$33, %eax
	jge	.LBB4_29
# BB#28:
	movl	$8, %edi
	jmp	.LBB4_30
.LBB4_1:
	movl	4(%rbx), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	jmp	.LBB4_52
.LBB4_29:
	decl	%eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB4_30:
	callq	malloc
	movq	%rax, %rcx
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	4(%rbx), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_clear
	movq	%rax, %rcx
	movl	4(%rbx), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_fill
	movslq	(%r12), %rcx
	movslq	12(%r12), %rdx
	imulq	%rcx, %rdx
	testl	%edx, %edx
	jle	.LBB4_47
# BB#31:                                # %.lr.ph.preheader
	movq	24(%r12), %rcx
	leaq	(%rcx,%rdx,4), %r13
	leaq	-4(%rax), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	4(%rax), %r11
	movq	%rax, %rdx
	addq	$-12, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$-1024, %r14d           # imm = 0xFC00
	movl	$1, %r15d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB4_32:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_41 Depth 2
                                        #     Child Loop BB4_45 Depth 2
	movl	(%rax), %edi
	movl	(%rcx), %esi
	andl	%r14d, %esi
	andl	$1023, %edi             # imm = 0x3FF
	movq	%rdi, %rdx
	cmoveq	%r15, %rdx
	movl	%edi, %ebp
	orl	%esi, %ebp
	movl	%ebp, (%rcx)
	cmpq	$8, %rdx
	jb	.LBB4_44
# BB#33:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_32 Depth=1
	movq	%rdx, %r9
	andq	$1016, %r9              # imm = 0x3F8
	je	.LBB4_44
# BB#34:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_32 Depth=1
	leaq	1(%rdi), %rbp
	testl	%edi, %edi
	cmovneq	%r8, %rbp
	leaq	-4(%rcx,%rbp,4), %rbx
	leaq	(%r11,%rdi,4), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB4_36
# BB#35:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_32 Depth=1
	leaq	4(%rcx,%rdi,4), %rsi
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,4), %rbp
	cmpq	%rsi, %rbp
	jb	.LBB4_44
.LBB4_36:                               # %vector.body.preheader
                                        #   in Loop: Header=BB4_32 Depth=1
	leaq	-8(%r9), %rsi
	movq	%rsi, %rbp
	shrq	$3, %rbp
	btl	$3, %esi
	jb	.LBB4_37
# BB#38:                                # %vector.body.prol
                                        #   in Loop: Header=BB4_32 Depth=1
	movups	-12(%rax,%rdi,4), %xmm0
	movups	-28(%rax,%rdi,4), %xmm1
	movups	-12(%rcx,%rdi,4), %xmm2
	movups	-28(%rcx,%rdi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rdi,4)
	movups	%xmm3, -28(%rcx,%rdi,4)
	movl	$8, %ebx
	testq	%rbp, %rbp
	jne	.LBB4_40
	jmp	.LBB4_42
.LBB4_37:                               #   in Loop: Header=BB4_32 Depth=1
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	je	.LBB4_42
.LBB4_40:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_32 Depth=1
	movq	%rdx, %rbp
	andq	$-8, %rbp
	negq	%rbp
	negq	%rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rdi,4), %r10
	leaq	-12(%rcx,%rdi,4), %r8
	.p2align	4, 0x90
.LBB4_41:                               # %vector.body
                                        #   Parent Loop BB4_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r10,%rbx,4), %xmm0
	movups	-16(%r10,%rbx,4), %xmm1
	movups	(%r8,%rbx,4), %xmm2
	movups	-16(%r8,%rbx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%r8,%rbx,4)
	movups	%xmm3, -16(%r8,%rbx,4)
	movups	-32(%r10,%rbx,4), %xmm0
	movups	-48(%r10,%rbx,4), %xmm1
	movups	-32(%r8,%rbx,4), %xmm2
	movups	-48(%r8,%rbx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%r8,%rbx,4)
	movups	%xmm3, -48(%r8,%rbx,4)
	addq	$-16, %rbx
	cmpq	%rbx, %rbp
	jne	.LBB4_41
.LBB4_42:                               # %middle.block
                                        #   in Loop: Header=BB4_32 Depth=1
	cmpq	%r9, %rdx
	movl	$2, %r8d
	je	.LBB4_46
# BB#43:                                #   in Loop: Header=BB4_32 Depth=1
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB4_44:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_32 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB4_45:                               # %scalar.ph
                                        #   Parent Loop BB4_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rcx,%rdi,4), %edx
	notl	%edx
	andl	-4(%rax,%rdi,4), %edx
	movl	%edx, -4(%rcx,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB4_45
.LBB4_46:                               # %.loopexit
                                        #   in Loop: Header=BB4_32 Depth=1
	movslq	(%r12), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%r13, %rcx
	jb	.LBB4_32
	jmp	.LBB4_48
.LBB4_47:                               # %._crit_edge
	testq	%rax, %rax
	je	.LBB4_49
.LBB4_48:                               # %._crit_edge.thread
	movq	%rax, %rdi
	callq	free
.LBB4_49:
	testb	$16, debug+1(%rip)
	je	.LBB4_51
# BB#50:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_print
.LBB4_51:
	xorl	%eax, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sf_free
.LBB4_52:
	movq	%r12, %rax
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	exact_minimum_cover, .Lfunc_end4-exact_minimum_cover
	.cfi_endproc

	.globl	unate_intersect
	.p2align	4, 0x90
	.type	unate_intersect,@function
unate_intersect:                        # @unate_intersect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 112
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	4(%rbp), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movslq	(%rbp), %rcx
	movslq	12(%rbp), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB5_28
# BB#1:                                 # %.lr.ph106
	movq	24(%rbp), %r13
	leaq	(%r13,%rax,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	24(%rbx), %r15
	movl	(%r12), %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
                                        #     Child Loop BB5_17 Depth 2
	movslq	12(%r12), %rdx
	movslq	%eax, %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB5_25
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	24(%r12), %r14
	leaq	(%r14,%rcx,4), %rbp
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB5_17
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.split.us
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	set_andp
	testl	%eax, %eax
	je	.LBB5_16
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	set_ord
	movl	%eax, %ebp
	movl	20(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %ebp
	jle	.LBB5_6
# BB#7:                                 #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_4 Depth=2
	xorl	%eax, %eax
	callq	sf_free
.LBB5_9:                                #   in Loop: Header=BB5_4 Depth=2
	movq	24(%rbx), %r15
	movl	$0, 12(%rbx)
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	set_and
	movl	%ebp, %ecx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=2
	jl	.LBB5_15
.LBB5_10:                               # %.thread85.us
                                        #   in Loop: Header=BB5_4 Depth=2
	movl	12(%rbx), %eax
	incl	%eax
	movl	%eax, 12(%rbx)
	cmpl	8(%rbx), %eax
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	jge	.LBB5_11
# BB#14:                                #   in Loop: Header=BB5_4 Depth=2
	movslq	(%rbx), %rax
	leaq	(%r15,%rax,4), %r15
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_4 Depth=2
	movq	%r12, %rbp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_contain
	movq	%rax, %r12
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB5_13
# BB#12:                                #   in Loop: Header=BB5_4 Depth=2
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	sf_union
	movq	%rax, %r12
.LBB5_13:                               #   in Loop: Header=BB5_4 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movq	24(%rbx), %r15
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r12
.LBB5_15:                               # %.thread.us
                                        #   in Loop: Header=BB5_4 Depth=2
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB5_16:                               # %.thread.us
                                        #   in Loop: Header=BB5_4 Depth=2
	movslq	(%r12), %rax
	leaq	(%r14,%rax,4), %r14
	cmpq	%rbp, %r14
	jb	.LBB5_4
	jmp	.LBB5_24
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph.split
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	set_andp
	testl	%eax, %eax
	je	.LBB5_23
# BB#18:                                # %.thread85
                                        #   in Loop: Header=BB5_17 Depth=2
	movl	12(%rbx), %eax
	incl	%eax
	movl	%eax, 12(%rbx)
	cmpl	8(%rbx), %eax
	jge	.LBB5_19
# BB#22:                                #   in Loop: Header=BB5_17 Depth=2
	movslq	(%rbx), %rax
	leaq	(%r15,%rax,4), %r15
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_17 Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_contain
	movq	%rax, %rbp
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_17 Depth=2
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	sf_union
	movq	%rax, %rbp
.LBB5_21:                               #   in Loop: Header=BB5_17 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movq	24(%rbx), %r15
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB5_23:                               # %.thread
                                        #   in Loop: Header=BB5_17 Depth=2
	movslq	(%r12), %rax
	leaq	(%r14,%rax,4), %r14
	cmpq	%rbp, %r14
	jb	.LBB5_17
.LBB5_24:                               #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB5_25:                               # %._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	movslq	(%rbp), %rcx
	leaq	(%r13,%rcx,4), %r13
	cmpq	48(%rsp), %r13          # 8-byte Folded Reload
	jb	.LBB5_2
# BB#26:                                # %._crit_edge107
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_contain
	movq	%rax, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB5_27
# BB#29:
	xorl	%eax, %eax
	movq	%rcx, %rsi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_union                # TAILCALL
.LBB5_28:                               # %._crit_edge107.thread
	xorl	%eax, %eax
	movq	%rbx, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_contain              # TAILCALL
.LBB5_27:
	movq	%rcx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	unate_intersect, .Lfunc_end5-unate_intersect
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"# EXACT_MINCOV[%d]: %4d = %4d x %4d, time = %s\n"
	.size	.L.str, 48

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"abs_select_restricted: should not have best_var == -1"
	.size	.L.str.2, 54

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"MINCOV: family of all minimal coverings is"
	.size	.Lstr, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
