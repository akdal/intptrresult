	.text
	.file	"BcjCoder.bc"
	.globl	_ZN16CBCJ_x86_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_Encoder9SubFilterEPhj,@function
_ZN16CBCJ_x86_Encoder9SubFilterEPhj:    # @_ZN16CBCJ_x86_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	leaq	16(%rdi), %rcx
	movl	$1, %r8d
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	x86_Convert             # TAILCALL
.Lfunc_end0:
	.size	_ZN16CBCJ_x86_Encoder9SubFilterEPhj, .Lfunc_end0-_ZN16CBCJ_x86_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN16CBCJ_x86_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_Decoder9SubFilterEPhj,@function
_ZN16CBCJ_x86_Decoder9SubFilterEPhj:    # @_ZN16CBCJ_x86_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	leaq	16(%rdi), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	x86_Convert             # TAILCALL
.Lfunc_end1:
	.size	_ZN16CBCJ_x86_Decoder9SubFilterEPhj, .Lfunc_end1-_ZN16CBCJ_x86_Decoder9SubFilterEPhj
	.cfi_endproc

	.section	.text._ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,@function
_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv: # @_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB2_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB2_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB2_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB2_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB2_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB2_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB2_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB2_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB2_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB2_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB2_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB2_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB2_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB2_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB2_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB2_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB2_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv, .Lfunc_end2-_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter6AddRefEv,"axG",@progbits,_ZN16CBranchConverter6AddRefEv,comdat
	.weak	_ZN16CBranchConverter6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter6AddRefEv,@function
_ZN16CBranchConverter6AddRefEv:         # @_ZN16CBranchConverter6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN16CBranchConverter6AddRefEv, .Lfunc_end3-_ZN16CBranchConverter6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter7ReleaseEv,"axG",@progbits,_ZN16CBranchConverter7ReleaseEv,comdat
	.weak	_ZN16CBranchConverter7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter7ReleaseEv,@function
_ZN16CBranchConverter7ReleaseEv:        # @_ZN16CBranchConverter7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN16CBranchConverter7ReleaseEv, .Lfunc_end4-_ZN16CBranchConverter7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CBCJ_x86_EncoderD0Ev,"axG",@progbits,_ZN16CBCJ_x86_EncoderD0Ev,comdat
	.weak	_ZN16CBCJ_x86_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_EncoderD0Ev,@function
_ZN16CBCJ_x86_EncoderD0Ev:              # @_ZN16CBCJ_x86_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end5:
	.size	_ZN16CBCJ_x86_EncoderD0Ev, .Lfunc_end5-_ZN16CBCJ_x86_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBCJ_x86_Encoder7SubInitEv,"axG",@progbits,_ZN16CBCJ_x86_Encoder7SubInitEv,comdat
	.weak	_ZN16CBCJ_x86_Encoder7SubInitEv
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_Encoder7SubInitEv,@function
_ZN16CBCJ_x86_Encoder7SubInitEv:        # @_ZN16CBCJ_x86_Encoder7SubInitEv
	.cfi_startproc
# BB#0:
	movl	$0, 16(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN16CBCJ_x86_Encoder7SubInitEv, .Lfunc_end6-_ZN16CBCJ_x86_Encoder7SubInitEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end7-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN16CBCJ_x86_DecoderD0Ev,"axG",@progbits,_ZN16CBCJ_x86_DecoderD0Ev,comdat
	.weak	_ZN16CBCJ_x86_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_DecoderD0Ev,@function
_ZN16CBCJ_x86_DecoderD0Ev:              # @_ZN16CBCJ_x86_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end8:
	.size	_ZN16CBCJ_x86_DecoderD0Ev, .Lfunc_end8-_ZN16CBCJ_x86_DecoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBCJ_x86_Decoder7SubInitEv,"axG",@progbits,_ZN16CBCJ_x86_Decoder7SubInitEv,comdat
	.weak	_ZN16CBCJ_x86_Decoder7SubInitEv
	.p2align	4, 0x90
	.type	_ZN16CBCJ_x86_Decoder7SubInitEv,@function
_ZN16CBCJ_x86_Decoder7SubInitEv:        # @_ZN16CBCJ_x86_Decoder7SubInitEv
	.cfi_startproc
# BB#0:
	movl	$0, 16(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN16CBCJ_x86_Decoder7SubInitEv, .Lfunc_end9-_ZN16CBCJ_x86_Decoder7SubInitEv
	.cfi_endproc

	.type	_ZTV16CBCJ_x86_Encoder,@object # @_ZTV16CBCJ_x86_Encoder
	.section	.rodata,"a",@progbits
	.globl	_ZTV16CBCJ_x86_Encoder
	.p2align	3
_ZTV16CBCJ_x86_Encoder:
	.quad	0
	.quad	_ZTI16CBCJ_x86_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBCJ_x86_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBCJ_x86_Encoder7SubInitEv
	.quad	_ZN16CBCJ_x86_Encoder9SubFilterEPhj
	.size	_ZTV16CBCJ_x86_Encoder, 88

	.type	_ZTS16CBCJ_x86_Encoder,@object # @_ZTS16CBCJ_x86_Encoder
	.globl	_ZTS16CBCJ_x86_Encoder
	.p2align	4
_ZTS16CBCJ_x86_Encoder:
	.asciz	"16CBCJ_x86_Encoder"
	.size	_ZTS16CBCJ_x86_Encoder, 19

	.type	_ZTS9CBranch86,@object  # @_ZTS9CBranch86
	.section	.rodata._ZTS9CBranch86,"aG",@progbits,_ZTS9CBranch86,comdat
	.weak	_ZTS9CBranch86
_ZTS9CBranch86:
	.asciz	"9CBranch86"
	.size	_ZTS9CBranch86, 11

	.type	_ZTI9CBranch86,@object  # @_ZTI9CBranch86
	.section	.rodata._ZTI9CBranch86,"aG",@progbits,_ZTI9CBranch86,comdat
	.weak	_ZTI9CBranch86
	.p2align	3
_ZTI9CBranch86:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS9CBranch86
	.size	_ZTI9CBranch86, 16

	.type	_ZTI16CBCJ_x86_Encoder,@object # @_ZTI16CBCJ_x86_Encoder
	.section	.rodata,"a",@progbits
	.globl	_ZTI16CBCJ_x86_Encoder
	.p2align	4
_ZTI16CBCJ_x86_Encoder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CBCJ_x86_Encoder
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI16CBranchConverter
	.quad	2                       # 0x2
	.quad	_ZTI9CBranch86
	.quad	4098                    # 0x1002
	.size	_ZTI16CBCJ_x86_Encoder, 56

	.type	_ZTV16CBCJ_x86_Decoder,@object # @_ZTV16CBCJ_x86_Decoder
	.globl	_ZTV16CBCJ_x86_Decoder
	.p2align	3
_ZTV16CBCJ_x86_Decoder:
	.quad	0
	.quad	_ZTI16CBCJ_x86_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBCJ_x86_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBCJ_x86_Decoder7SubInitEv
	.quad	_ZN16CBCJ_x86_Decoder9SubFilterEPhj
	.size	_ZTV16CBCJ_x86_Decoder, 88

	.type	_ZTS16CBCJ_x86_Decoder,@object # @_ZTS16CBCJ_x86_Decoder
	.globl	_ZTS16CBCJ_x86_Decoder
	.p2align	4
_ZTS16CBCJ_x86_Decoder:
	.asciz	"16CBCJ_x86_Decoder"
	.size	_ZTS16CBCJ_x86_Decoder, 19

	.type	_ZTI16CBCJ_x86_Decoder,@object # @_ZTI16CBCJ_x86_Decoder
	.globl	_ZTI16CBCJ_x86_Decoder
	.p2align	4
_ZTI16CBCJ_x86_Decoder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CBCJ_x86_Decoder
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI16CBranchConverter
	.quad	2                       # 0x2
	.quad	_ZTI9CBranch86
	.quad	4098                    # 0x1002
	.size	_ZTI16CBCJ_x86_Decoder, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
