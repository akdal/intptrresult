	.text
	.file	"g72x.bc"
	.globl	g72x_init_state
	.p2align	4, 0x90
	.type	g72x_init_state,@function
g72x_init_state:                        # @g72x_init_state
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader24
	movq	$34816, (%rdi)          # imm = 0x8800
	movw	$544, 8(%rdi)           # imm = 0x220
	movw	$0, 32(%rdi)
	movq	$0, 10(%rdi)
	movw	$32, 48(%rdi)
	movw	$0, 18(%rdi)
	movw	$0, 34(%rdi)
	movw	$32, 50(%rdi)
	movw	$0, 20(%rdi)
	movw	$32, 36(%rdi)
	movw	$0, 22(%rdi)
	movw	$32, 38(%rdi)
	movw	$0, 24(%rdi)
	movw	$32, 40(%rdi)
	movw	$0, 26(%rdi)
	movw	$32, 42(%rdi)
	movw	$0, 28(%rdi)
	movw	$32, 44(%rdi)
	movw	$0, 30(%rdi)
	movw	$32, 46(%rdi)
	movb	$0, 52(%rdi)
	retq
.Lfunc_end0:
	.size	g72x_init_state, .Lfunc_end0-g72x_init_state
	.cfi_endproc

	.globl	predictor_zero
	.p2align	4, 0x90
	.type	predictor_zero,@function
predictor_zero:                         # @predictor_zero
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movswl	20(%rbx), %edi
	sarl	$2, %edi
	movswl	36(%rbx), %esi
	callq	fmult
	movl	%eax, %r14d
	movswl	22(%rbx), %edi
	sarl	$2, %edi
	movswl	38(%rbx), %esi
	callq	fmult
	movl	%eax, %ebp
	addl	%r14d, %ebp
	movswl	24(%rbx), %edi
	sarl	$2, %edi
	movswl	40(%rbx), %esi
	callq	fmult
	movl	%eax, %r14d
	addl	%ebp, %r14d
	movswl	26(%rbx), %edi
	sarl	$2, %edi
	movswl	42(%rbx), %esi
	callq	fmult
	movl	%eax, %ebp
	addl	%r14d, %ebp
	movswl	28(%rbx), %edi
	sarl	$2, %edi
	movswl	44(%rbx), %esi
	callq	fmult
	movl	%eax, %r14d
	addl	%ebp, %r14d
	movswl	30(%rbx), %edi
	sarl	$2, %edi
	movswl	46(%rbx), %esi
	callq	fmult
	addl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	predictor_zero, .Lfunc_end1-predictor_zero
	.cfi_endproc

	.p2align	4, 0x90
	.type	fmult,@function
fmult:                                  # @fmult
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	%edi, %eax
	negl	%eax
	andl	$8191, %eax             # imm = 0x1FFF
	testl	%edi, %edi
	cmovgl	%edi, %eax
	movswl	%ax, %r8d
	movswl	power2(%rip), %ecx
	movl	$65530, %r10d           # imm = 0xFFFA
	cmpl	%r8d, %ecx
	jg	.LBB2_2
# BB#1:                                 # %.lr.ph.i.136
	movswl	power2+2(%rip), %ecx
	movl	$65531, %r10d           # imm = 0xFFFB
	cmpl	%r8d, %ecx
	jg	.LBB2_2
# BB#6:                                 # %.lr.ph.i.237
	movswl	power2+4(%rip), %eax
	movl	$65532, %r10d           # imm = 0xFFFC
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#7:                                 # %.lr.ph.i.338
	movswl	power2+6(%rip), %eax
	movl	$65533, %r10d           # imm = 0xFFFD
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#8:                                 # %.lr.ph.i.439
	movswl	power2+8(%rip), %eax
	movl	$65534, %r10d           # imm = 0xFFFE
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#9:                                 # %.lr.ph.i.540
	movswl	power2+10(%rip), %eax
	movl	$65535, %r10d           # imm = 0xFFFF
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#10:                                # %.lr.ph.i.641
	movswl	power2+12(%rip), %eax
	movl	$65536, %r10d           # imm = 0x10000
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#11:                                # %.lr.ph.i.742
	movswl	power2+14(%rip), %eax
	movl	$65537, %r10d           # imm = 0x10001
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#12:                                # %.lr.ph.i.843
	movswl	power2+16(%rip), %eax
	movl	$65538, %r10d           # imm = 0x10002
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#13:                                # %.lr.ph.i.944
	movswl	power2+18(%rip), %eax
	movl	$65539, %r10d           # imm = 0x10003
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#14:                                # %.lr.ph.i.1045
	movswl	power2+20(%rip), %eax
	movl	$65540, %r10d           # imm = 0x10004
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#15:                                # %.lr.ph.i.1146
	movswl	power2+22(%rip), %eax
	movl	$65541, %r10d           # imm = 0x10005
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#16:                                # %.lr.ph.i.1247
	movswl	power2+24(%rip), %eax
	movl	$65542, %r10d           # imm = 0x10006
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#17:                                # %.lr.ph.i.1348
	movswl	power2+26(%rip), %eax
	movl	$65543, %r10d           # imm = 0x10007
	cmpl	%r8d, %eax
	jg	.LBB2_2
# BB#18:                                # %.lr.ph.i.1449
	movswl	power2+28(%rip), %eax
	xorl	%r10d, %r10d
	cmpl	%r8d, %eax
	setle	%r10b
	orl	$65544, %r10d           # imm = 0x10008
.LBB2_2:                                # %quan.exit
	testl	%r8d, %r8d
	je	.LBB2_3
# BB#4:
	movl	%r10d, %r9d
	shll	$16, %r9d
	movswl	%r10w, %ecx
	movl	%r8d, %edx
	sarl	%cl, %edx
	negl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r8d
	cmpl	$-65536, %r9d           # imm = 0xFFFF0000
	cmovgl	%edx, %r8d
	movswl	%r8w, %r8d
	jmp	.LBB2_5
.LBB2_3:
	movl	$32, %r8d
.LBB2_5:
	movl	%esi, %ecx
	shrl	$6, %ecx
	andl	$15, %ecx
	addl	%ecx, %r10d
	shll	$16, %r10d
	addl	$-851968, %r10d         # imm = 0xFFF30000
	movl	%r10d, %ecx
	sarl	$16, %ecx
	movl	%esi, %edx
	andl	$63, %edx
	shll	$12, %edx
	imull	%r8d, %edx
	addl	$196608, %edx           # imm = 0x30000
	sarl	$16, %edx
	movl	%edx, %eax
	shll	%cl, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	negl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	cmpl	$-65536, %r10d          # imm = 0xFFFF0000
	cmovgl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	xorl	%esi, %edi
	cmovnsl	%edx, %eax
	retq
.Lfunc_end2:
	.size	fmult, .Lfunc_end2-fmult
	.cfi_endproc

	.globl	predictor_pole
	.p2align	4, 0x90
	.type	predictor_pole,@function
predictor_pole:                         # @predictor_pole
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movswl	18(%rbx), %edi
	sarl	$2, %edi
	movswl	50(%rbx), %esi
	callq	fmult
	movl	%eax, %ebp
	movswl	16(%rbx), %edi
	sarl	$2, %edi
	movswl	48(%rbx), %esi
	callq	fmult
	addl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	predictor_pole, .Lfunc_end3-predictor_pole
	.cfi_endproc

	.globl	step_size
	.p2align	4, 0x90
	.type	step_size,@function
step_size:                              # @step_size
	.cfi_startproc
# BB#0:
	movswl	14(%rdi), %edx
	cmpl	$256, %edx              # imm = 0x100
	jl	.LBB4_2
# BB#1:
	movswl	8(%rdi), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB4_2:
	movq	(%rdi), %rax
	shrq	$6, %rax
	movswl	8(%rdi), %ecx
	sarl	$2, %edx
	subl	%eax, %ecx
	jle	.LBB4_4
# BB#3:
	imull	%edx, %ecx
.LBB4_6:
	sarl	$6, %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB4_4:
	testl	%ecx, %ecx
	js	.LBB4_5
# BB#7:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB4_5:
	imull	%edx, %ecx
	addl	$63, %ecx
	jmp	.LBB4_6
.Lfunc_end4:
	.size	step_size, .Lfunc_end4-step_size
	.cfi_endproc

	.globl	quantize
	.p2align	4, 0x90
	.type	quantize,@function
quantize:                               # @quantize
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	%ecx, %r8d
	movl	%edi, %r9d
	negl	%r9d
	cmovll	%edi, %r9d
	shll	$16, %r9d
	movl	%r9d, %r10d
	sarl	$17, %r10d
	movswl	power2(%rip), %ecx
	xorl	%eax, %eax
	cmpl	%r10d, %ecx
	movl	$0, %ecx
	jg	.LBB5_2
# BB#1:                                 # %.lr.ph.i.136
	movswl	power2+2(%rip), %r11d
	movl	$1, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#11:                                # %.lr.ph.i.237
	movswl	power2+4(%rip), %r11d
	movl	$2, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#12:                                # %.lr.ph.i.338
	movswl	power2+6(%rip), %r11d
	movl	$3, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#13:                                # %.lr.ph.i.439
	movswl	power2+8(%rip), %r11d
	movl	$4, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#14:                                # %.lr.ph.i.540
	movswl	power2+10(%rip), %r11d
	movl	$5, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#15:                                # %.lr.ph.i.641
	movswl	power2+12(%rip), %r11d
	movl	$6, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#16:                                # %.lr.ph.i.742
	movswl	power2+14(%rip), %r11d
	movl	$7, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#17:                                # %.lr.ph.i.843
	movswl	power2+16(%rip), %r11d
	movl	$8, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#18:                                # %.lr.ph.i.944
	movswl	power2+18(%rip), %r11d
	movl	$9, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#19:                                # %.lr.ph.i.1045
	movswl	power2+20(%rip), %r11d
	movl	$10, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#20:                                # %.lr.ph.i.1146
	movswl	power2+22(%rip), %r11d
	movl	$11, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#21:                                # %.lr.ph.i.1247
	movswl	power2+24(%rip), %r11d
	movl	$12, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#22:                                # %.lr.ph.i.1348
	movswl	power2+26(%rip), %r11d
	movl	$13, %ecx
	cmpl	%r10d, %r11d
	jg	.LBB5_2
# BB#23:                                # %.lr.ph.i.1449
	movswl	power2+28(%rip), %r11d
	xorl	%ecx, %ecx
	cmpl	%r10d, %r11d
	setle	%cl
	orl	$14, %ecx
.LBB5_2:                                # %quan.exit
	sarl	$9, %r9d
	sarl	%cl, %r9d
	testl	%r8d, %r8d
	jle	.LBB5_6
# BB#3:                                 # %.lr.ph.i33.preheader
	andl	$127, %r9d
	shll	$7, %ecx
	orl	%r9d, %ecx
	shrl	$2, %esi
	subl	%esi, %ecx
	movswl	%cx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rdx,%rax,2), %esi
	cmpl	%ecx, %esi
	jg	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	incq	%rax
	cmpl	%r8d, %eax
	jl	.LBB5_4
.LBB5_6:                                # %quan.exit35
	testl	%edi, %edi
	js	.LBB5_7
# BB#8:
	testl	%eax, %eax
	jne	.LBB5_10
# BB#9:
	leal	1(%r8,%r8), %eax
.LBB5_10:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB5_7:
	leal	1(%r8,%r8), %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end5:
	.size	quantize, .Lfunc_end5-quantize
	.cfi_endproc

	.globl	reconstruct
	.p2align	4, 0x90
	.type	reconstruct,@function
reconstruct:                            # @reconstruct
	.cfi_startproc
# BB#0:
	shrl	$2, %edx
	addl	%esi, %edx
	testw	%dx, %dx
	js	.LBB6_1
# BB#2:
	movl	%edx, %eax
	shrl	$7, %eax
	andl	$15, %eax
	andl	$127, %edx
	shll	$7, %edx
	orl	$16384, %edx            # imm = 0x4000
	movl	$14, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movswl	%dx, %ecx
	leal	-32768(%rcx), %eax
	testl	%edi, %edi
	cmovel	%ecx, %eax
	retq
.LBB6_1:
	testl	%edi, %edi
	movl	$-32768, %eax           # imm = 0x8000
	cmovel	%edi, %eax
	retq
.Lfunc_end6:
	.size	reconstruct, .Lfunc_end6-reconstruct
	.cfi_endproc

	.globl	update
	.p2align	4, 0x90
	.type	update,@function
update:                                 # @update
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%ecx, %r10d
	movq	64(%rsp), %r14
	movl	%r8d, %r13d
	andl	$32767, %r13d           # imm = 0x7FFF
	movq	(%r14), %rbp
	movq	%rbp, %rcx
	shrq	$15, %rcx
	movl	%ebp, %ebx
	shrl	$10, %ebx
	andl	$31, %ebx
	orl	$32, %ebx
	movl	%ecx, %eax
	shll	$16, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	cmpl	$589824, %eax           # imm = 0x90000
	movswl	%bx, %eax
	movl	$31744, %ecx            # imm = 0x7C00
	cmovlel	%eax, %ecx
	cmpb	$0, 52(%r14)
	je	.LBB7_1
# BB#2:
	movl	%ecx, %eax
	shrl	%eax
	addl	%ecx, %eax
	shrl	%eax
	cwtl
	xorl	%r11d, %r11d
	cmpl	%eax, %r13d
	setg	%r11b
	jmp	.LBB7_3
.LBB7_1:
	xorl	%r11d, %r11d
.LBB7_3:
	movl	56(%rsp), %r12d
	subl	%esi, %edx
	shrl	$5, %edx
	addl	%esi, %edx
	movw	%dx, 8(%r14)
	movl	%edx, %ebx
	shll	$16, %ebx
	movw	$544, %cx               # imm = 0x220
	cmpl	$35651584, %ebx         # imm = 0x2200000
	jl	.LBB7_5
# BB#4:
	movw	$5120, %cx              # imm = 0x1400
	cmpl	$335544321, %ebx        # imm = 0x14000001
	jl	.LBB7_6
.LBB7_5:                                # %.sink.split240
	movw	%cx, 8(%r14)
	movw	%cx, %dx
.LBB7_6:
	movl	%r12d, %eax
	shrl	$31, %eax
	movswq	%dx, %rcx
	movq	%rbp, %rdx
	negq	%rdx
	sarq	$6, %rdx
	addq	%rbp, %rcx
	addq	%rdx, %rcx
	movq	%rcx, (%r14)
	testl	%r11d, %r11d
	je	.LBB7_8
# BB#7:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
                                        # implicit-def: %R15W
	jmp	.LBB7_30
.LBB7_8:
	movswl	18(%r14), %r15d
	movl	%r15d, %ecx
	shrl	$7, %ecx
	subl	%ecx, %r15d
	testl	%r12d, %r12d
	je	.LBB7_93
# BB#9:
	movzwl	32(%r14), %ebp
	xorl	%eax, %ebp
	movswl	16(%r14), %r12d
	movl	%r12d, %ecx
	negl	%ecx
	testl	%ebp, %ebp
	cmovnew	%r12w, %cx
	movswl	%cx, %edx
	cmpl	$-8192, %edx            # imm = 0xE000
	jg	.LBB7_11
# BB#10:
	movswl	%r15w, %edx
	addl	$-256, %edx
	jmp	.LBB7_14
.LBB7_93:
	movw	%r15w, 18(%r14)
	movswl	16(%r14), %edx
	movl	%edx, %ecx
	shrl	$8, %ecx
	jmp	.LBB7_23
.LBB7_11:
	cmpl	$8192, %edx             # imm = 0x2000
	jl	.LBB7_13
# BB#12:
	movswl	%r15w, %edx
	addl	$255, %edx
	jmp	.LBB7_14
.LBB7_13:
	shrl	$5, %edx
	addl	%r15d, %edx
.LBB7_14:
	movswl	34(%r14), %ebx
	movl	%edx, %ecx
	shll	$16, %ecx
	cmpl	%ebx, %eax
	movswl	%dx, %edx
	movw	$-12288, %r15w          # imm = 0xD000
	jne	.LBB7_15
# BB#18:
	cmpl	$-813629440, %ecx       # imm = 0xCF810000
	jl	.LBB7_22
# BB#19:
	movw	$12288, %r15w           # imm = 0x3000
	cmpl	$796852224, %ecx        # imm = 0x2F7F0000
	jg	.LBB7_22
# BB#20:
	subl	$-128, %edx
	jmp	.LBB7_21
.LBB7_15:
	cmpl	$-796852224, %ecx       # imm = 0xD0810000
	jl	.LBB7_22
# BB#16:
	movw	$12288, %r15w           # imm = 0x3000
	cmpl	$813629440, %ecx        # imm = 0x307F0000
	jg	.LBB7_22
# BB#17:
	addl	$65408, %edx            # imm = 0xFF80
.LBB7_21:
	movw	%dx, %r15w
.LBB7_22:
	movw	%r15w, 18(%r14)
	movl	%r12d, %ecx
	shrl	$8, %ecx
	testl	%ebp, %ebp
	movl	$192, %ebx
	movl	$65344, %edx            # imm = 0xFF40
	cmovel	%ebx, %edx
	addl	%r12d, %edx
.LBB7_23:
	subl	%ecx, %edx
	movw	%dx, 16(%r14)
	movl	$15360, %ecx            # imm = 0x3C00
	subl	%r15d, %ecx
	movswl	%dx, %ebp
	movswl	%cx, %ebx
	negl	%ebx
	cmpl	%ebx, %ebp
	jge	.LBB7_25
# BB#24:
	movw	%bx, %cx
	jmp	.LBB7_26
.LBB7_25:
	cmpw	%cx, %dx
	jle	.LBB7_27
.LBB7_26:                               # %.sink.split
	movw	%cx, 16(%r14)
.LBB7_27:                               # %.preheader
	xorl	%ecx, %ecx
	cmpl	$5, %edi
	sete	%cl
	orl	$8, %ecx
	movzwl	20(%r14), %edx
	movswl	%dx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	testl	%r13d, %r13d
	je	.LBB7_29
# BB#28:                                # %.preheader.split.preheader
	movswl	36(%r14), %edi
	xorl	%r8d, %edi
	sarl	$31, %edi
	andl	$65280, %edi            # imm = 0xFF00
	leal	128(%rdi,%rdx), %edx
	movw	%dx, 20(%r14)
	movswl	22(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movswl	38(%r14), %edi
	xorl	%r8d, %edi
	sarl	$31, %edi
	andl	$65280, %edi            # imm = 0xFF00
	leal	128(%rdi,%rdx), %edx
	movw	%dx, 22(%r14)
	movswl	24(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movswl	40(%r14), %edi
	xorl	%r8d, %edi
	sarl	$31, %edi
	andl	$65280, %edi            # imm = 0xFF00
	leal	128(%rdi,%rdx), %edx
	movw	%dx, 24(%r14)
	movswl	26(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movswl	42(%r14), %edi
	xorl	%r8d, %edi
	sarl	$31, %edi
	andl	$65280, %edi            # imm = 0xFF00
	leal	128(%rdi,%rdx), %edx
	movw	%dx, 26(%r14)
	movswl	28(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movswl	44(%r14), %edi
	xorl	%r8d, %edi
	sarl	$31, %edi
	andl	$65280, %edi            # imm = 0xFF00
	leal	128(%rdi,%rdx), %edx
	movw	%dx, 28(%r14)
	movswl	30(%r14), %edx
	movl	%edx, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	subl	%edi, %edx
	movswl	46(%r14), %ecx
	xorl	%r8d, %ecx
	sarl	$31, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	leal	128(%rcx,%rdx), %ecx
	movw	%cx, 30(%r14)
	jmp	.LBB7_30
.LBB7_29:                               # %.preheader.split.us.preheader
	movw	%dx, 20(%r14)
	movswl	22(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movw	%dx, 22(%r14)
	movswl	24(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movw	%dx, 24(%r14)
	movswl	26(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movw	%dx, 26(%r14)
	movswl	28(%r14), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	subl	%edi, %edx
	movw	%dx, 28(%r14)
	movswl	30(%r14), %edx
	movl	%edx, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	subl	%edi, %edx
	movw	%dx, 30(%r14)
.LBB7_30:                               # %.loopexit
	movzwl	44(%r14), %ecx
	movw	%cx, 46(%r14)
	movzwl	42(%r14), %ecx
	movw	%cx, 44(%r14)
	movzwl	40(%r14), %ecx
	movw	%cx, 42(%r14)
	movzwl	38(%r14), %ecx
	movw	%cx, 40(%r14)
	movzwl	36(%r14), %ecx
	movw	%cx, 38(%r14)
	testl	%r13d, %r13d
	je	.LBB7_94
# BB#31:                                # %.lr.ph.i.preheader
	movswl	power2(%rip), %ecx
	xorl	%edi, %edi
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#32:                                # %.lr.ph.i.1285
	movswl	power2+2(%rip), %ecx
	movl	$65536, %edi            # imm = 0x10000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#80:                                # %.lr.ph.i.2286
	movswl	power2+4(%rip), %ecx
	movl	$131072, %edi           # imm = 0x20000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#81:                                # %.lr.ph.i.3287
	movswl	power2+6(%rip), %ecx
	movl	$196608, %edi           # imm = 0x30000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#82:                                # %.lr.ph.i.4288
	movswl	power2+8(%rip), %ecx
	movl	$262144, %edi           # imm = 0x40000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#83:                                # %.lr.ph.i.5289
	movswl	power2+10(%rip), %ecx
	movl	$327680, %edi           # imm = 0x50000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#84:                                # %.lr.ph.i.6290
	movswl	power2+12(%rip), %ecx
	movl	$393216, %edi           # imm = 0x60000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#85:                                # %.lr.ph.i.7291
	movswl	power2+14(%rip), %ecx
	movl	$458752, %edi           # imm = 0x70000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#86:                                # %.lr.ph.i.8292
	movswl	power2+16(%rip), %ecx
	movl	$524288, %edi           # imm = 0x80000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#87:                                # %.lr.ph.i.9293
	movswl	power2+18(%rip), %ecx
	movl	$589824, %edi           # imm = 0x90000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#88:                                # %.lr.ph.i.10294
	movswl	power2+20(%rip), %ecx
	movl	$655360, %edi           # imm = 0xA0000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#89:                                # %.lr.ph.i.11295
	movswl	power2+22(%rip), %ecx
	movl	$720896, %edi           # imm = 0xB0000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#90:                                # %.lr.ph.i.12296
	movswl	power2+24(%rip), %ecx
	movl	$786432, %edi           # imm = 0xC0000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#91:                                # %.lr.ph.i.13297
	movswl	power2+26(%rip), %ecx
	movl	$851968, %edi           # imm = 0xD0000
	cmpl	%r13d, %ecx
	jg	.LBB7_33
# BB#92:                                # %.lr.ph.i.14298
	movswl	power2+28(%rip), %ecx
	cmpl	%r13d, %ecx
	movl	$917504, %ecx           # imm = 0xE0000
	movl	$983040, %edi           # imm = 0xF0000
	cmovgl	%ecx, %edi
.LBB7_33:                               # %quan.exit
	movl	%edi, %ecx
	shrl	$16, %ecx
	shrl	$10, %edi
	shll	$6, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	leal	(%r13,%rdi), %ecx
	testl	%r8d, %r8d
	leal	64512(%r13,%rdi), %r8d
	cmovnsl	%ecx, %r8d
	jmp	.LBB7_34
.LBB7_94:
	sarl	$31, %r8d
	andl	$64512, %r8d            # imm = 0xFC00
	orl	$32, %r8d
.LBB7_34:
	movw	%r8w, 36(%r14)
	movzwl	48(%r14), %ecx
	movw	%cx, 50(%r14)
	testl	%r9d, %r9d
	je	.LBB7_35
# BB#36:
	jle	.LBB7_40
# BB#37:                                # %.lr.ph.i249.preheader
	movswl	power2(%rip), %edx
	xorl	%ecx, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#38:                                # %.lr.ph.i249.1257
	movswl	power2+2(%rip), %edx
	movl	$1, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#54:                                # %.lr.ph.i249.2258
	movswl	power2+4(%rip), %edx
	movl	$2, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#55:                                # %.lr.ph.i249.3259
	movswl	power2+6(%rip), %edx
	movl	$3, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#56:                                # %.lr.ph.i249.4260
	movswl	power2+8(%rip), %edx
	movl	$4, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#57:                                # %.lr.ph.i249.5261
	movswl	power2+10(%rip), %edx
	movl	$5, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#58:                                # %.lr.ph.i249.6262
	movswl	power2+12(%rip), %edx
	movl	$6, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#59:                                # %.lr.ph.i249.7263
	movswl	power2+14(%rip), %edx
	movl	$7, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#60:                                # %.lr.ph.i249.8264
	movswl	power2+16(%rip), %edx
	movl	$8, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#61:                                # %.lr.ph.i249.9265
	movswl	power2+18(%rip), %edx
	movl	$9, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#62:                                # %.lr.ph.i249.10266
	movswl	power2+20(%rip), %edx
	movl	$10, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#63:                                # %.lr.ph.i249.11267
	movswl	power2+22(%rip), %edx
	movl	$11, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#64:                                # %.lr.ph.i249.12268
	movswl	power2+24(%rip), %edx
	movl	$12, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#65:                                # %.lr.ph.i249.13269
	movswl	power2+26(%rip), %edx
	movl	$13, %ecx
	cmpl	%r9d, %edx
	jg	.LBB7_39
# BB#66:                                # %.lr.ph.i249.14270
	movswl	power2+28(%rip), %edx
	xorl	%ecx, %ecx
	cmpl	%r9d, %edx
	setle	%cl
	orl	$14, %ecx
.LBB7_39:                               # %quan.exit251
	movl	%ecx, %edx
	shll	$6, %edx
	shll	$6, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r9d
	addl	%edx, %r9d
	movw	%r9w, %dx
	jmp	.LBB7_44
.LBB7_35:
	movw	$32, %dx
	jmp	.LBB7_44
.LBB7_40:
	movw	$-992, %dx              # imm = 0xFC20
	cmpl	$-32767, %r9d           # imm = 0x8001
	jl	.LBB7_44
# BB#41:                                # %.lr.ph.i244
	negl	%r9d
	movl	%r9d, %edi
	shll	$16, %edi
	movswl	%r9w, %ebx
	movswl	power2(%rip), %edx
	xorl	%ecx, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#42:                                # %.lr.ph.i244.1271
	movswl	power2+2(%rip), %edx
	movl	$1, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#67:                                # %.lr.ph.i244.2272
	movswl	power2+4(%rip), %edx
	movl	$2, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#68:                                # %.lr.ph.i244.3273
	movswl	power2+6(%rip), %edx
	movl	$3, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#69:                                # %.lr.ph.i244.4274
	movswl	power2+8(%rip), %edx
	movl	$4, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#70:                                # %.lr.ph.i244.5275
	movswl	power2+10(%rip), %edx
	movl	$5, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#71:                                # %.lr.ph.i244.6276
	movswl	power2+12(%rip), %edx
	movl	$6, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#72:                                # %.lr.ph.i244.7277
	movswl	power2+14(%rip), %edx
	movl	$7, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#73:                                # %.lr.ph.i244.8278
	movswl	power2+16(%rip), %edx
	movl	$8, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#74:                                # %.lr.ph.i244.9279
	movswl	power2+18(%rip), %edx
	movl	$9, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#75:                                # %.lr.ph.i244.10280
	movswl	power2+20(%rip), %edx
	movl	$10, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#76:                                # %.lr.ph.i244.11281
	movswl	power2+22(%rip), %edx
	movl	$11, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#77:                                # %.lr.ph.i244.12282
	movswl	power2+24(%rip), %edx
	movl	$12, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#78:                                # %.lr.ph.i244.13283
	movswl	power2+26(%rip), %edx
	movl	$13, %ecx
	cmpl	%ebx, %edx
	jg	.LBB7_43
# BB#79:                                # %.lr.ph.i244.14284
	movswl	power2+28(%rip), %edx
	xorl	%ecx, %ecx
	cmpl	%ebx, %edx
	setle	%cl
	orl	$14, %ecx
.LBB7_43:                               # %quan.exit246
	movl	%ecx, %edx
	shll	$6, %edx
	sarl	$10, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	orl	$64512, %edx            # imm = 0xFC00
	addl	%edi, %edx
.LBB7_44:
	movw	%dx, 48(%r14)
	movzwl	32(%r14), %ecx
	movw	%cx, 34(%r14)
	movw	%ax, 32(%r14)
	movswl	%r15w, %eax
	cmpl	$-11776, %eax           # imm = 0xD200
	setl	%al
	testl	%r11d, %r11d
	sete	%cl
	andb	%al, %cl
	movb	%cl, 52(%r14)
	movswl	10(%r14), %eax
	movl	%r10d, %edx
	subl	%eax, %edx
	shrl	$5, %edx
	addl	%eax, %edx
	movw	%dx, 10(%r14)
	shll	$2, %r10d
	movswl	12(%r14), %eax
	subl	%eax, %r10d
	shrl	$7, %r10d
	addl	%eax, %r10d
	testl	%r11d, %r11d
	movw	%r10w, 12(%r14)
	je	.LBB7_46
# BB#45:
	movw	$256, 14(%r14)          # imm = 0x100
	jmp	.LBB7_53
.LBB7_46:
	cmpl	$1535, %esi             # imm = 0x5FF
	jle	.LBB7_47
# BB#48:
	testb	%cl, %cl
	je	.LBB7_49
.LBB7_47:
	movswl	14(%r14), %ecx
	addq	$14, %r14
	movl	$512, %edx              # imm = 0x200
	subl	%ecx, %edx
.LBB7_52:
	shrl	$4, %edx
	addl	%edx, %ecx
	movw	%cx, (%r14)
.LBB7_53:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_49:
	movswl	%dx, %eax
	shll	$2, %eax
	movswl	%r10w, %ecx
	subl	%ecx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$3, %ecx
	cmpl	%ecx, %edx
	movw	14(%r14), %cx
	leaq	14(%r14), %r14
	movswl	%cx, %esi
	jge	.LBB7_50
# BB#51:
	negl	%esi
	movl	%esi, %edx
	jmp	.LBB7_52
.LBB7_50:
	movl	$512, %edx              # imm = 0x200
	subl	%esi, %edx
	jmp	.LBB7_52
.Lfunc_end7:
	.size	update, .Lfunc_end7-update
	.cfi_endproc

	.globl	tandem_adjust_alaw
	.p2align	4, 0x90
	.type	tandem_adjust_alaw,@function
tandem_adjust_alaw:                     # @tandem_adjust_alaw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, %r13d
	movl	%esi, %r12d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	(,%rdi,4), %eax
	andl	$-8, %eax
	cmpl	$-32767, %edi           # imm = 0x8001
	movl	$-8, %edi
	cmovgel	%eax, %edi
	xorl	%eax, %eax
	callq	linear2alaw
	movl	%eax, %r15d
	movzbl	%r15b, %r14d
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	alaw2linear
	shrl	$2, %eax
	subl	%r12d, %eax
	movswl	%ax, %edi
	leal	-1(%rbx), %ecx
	movl	%r13d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	quantize
	movsbl	%al, %eax
	cmpl	%ebp, %eax
	je	.LBB8_11
# BB#1:
	xorl	%ebx, %ebp
	xorl	%ebx, %eax
	cmpl	%ebp, %eax
	jle	.LBB8_6
# BB#2:
	testb	%r15b, %r15b
	js	.LBB8_3
# BB#4:
	movl	$42, %eax
	cmpl	$42, %r14d
	jne	.LBB8_5
	jmp	.LBB8_12
.LBB8_6:
	testb	%r15b, %r15b
	js	.LBB8_7
# BB#8:
	movl	$213, %eax
	cmpl	$85, %r14d
	jne	.LBB8_9
	jmp	.LBB8_12
.LBB8_3:
	movl	$85, %eax
	cmpl	$213, %r14d
	je	.LBB8_12
.LBB8_9:
	xorl	$85, %r14d
	decl	%r14d
	jmp	.LBB8_10
.LBB8_7:
	movl	$170, %eax
	cmpl	$170, %r14d
	je	.LBB8_12
.LBB8_5:
	xorl	$85, %r14d
	incl	%r14d
.LBB8_10:
	xorl	$85, %r14d
.LBB8_11:
	movl	%r14d, %eax
.LBB8_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	tandem_adjust_alaw, .Lfunc_end8-tandem_adjust_alaw
	.cfi_endproc

	.globl	tandem_adjust_ulaw
	.p2align	4, 0x90
	.type	tandem_adjust_ulaw,@function
tandem_adjust_ulaw:                     # @tandem_adjust_ulaw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	%r8d, %r14d
	movl	%ecx, %ebp
	movl	%edx, %r13d
	movl	%esi, %r12d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	(,%rdi,4), %ecx
	xorl	%eax, %eax
	cmpl	$-32767, %edi           # imm = 0x8001
	cmovll	%eax, %ecx
	xorl	%eax, %eax
	movl	%ecx, %edi
	callq	linear2ulaw
	movl	%eax, %r15d
	movzbl	%r15b, %ebx
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	ulaw2linear
	shrl	$2, %eax
	subl	%r12d, %eax
	movswl	%ax, %edi
	leal	-1(%r14), %ecx
	movl	%r13d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	quantize
	movsbl	%al, %eax
	cmpl	%ebp, %eax
	je	.LBB9_10
# BB#1:
	xorl	%r14d, %ebp
	xorl	%r14d, %eax
	cmpl	%ebp, %eax
	jle	.LBB9_6
# BB#2:
	testb	%r15b, %r15b
	js	.LBB9_3
# BB#4:
	leal	-1(%rbx), %eax
	testl	%ebx, %ebx
	jmp	.LBB9_5
.LBB9_6:
	testb	%r15b, %r15b
	js	.LBB9_7
# BB#8:
	leal	1(%rbx), %eax
	cmpl	$127, %ebx
	movl	$254, %ebx
	jmp	.LBB9_9
.LBB9_3:
	leal	1(%rbx), %eax
	cmpl	$255, %ebx
	movl	$126, %ebx
.LBB9_9:
	cmovnel	%eax, %ebx
	jmp	.LBB9_10
.LBB9_7:
	leal	-1(%rbx), %eax
	cmpl	$128, %ebx
.LBB9_5:
	cmovel	%ebx, %eax
	movl	%eax, %ebx
.LBB9_10:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	tandem_adjust_ulaw, .Lfunc_end9-tandem_adjust_ulaw
	.cfi_endproc

	.type	power2,@object          # @power2
	.data
	.p2align	4
power2:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	8                       # 0x8
	.short	16                      # 0x10
	.short	32                      # 0x20
	.short	64                      # 0x40
	.short	128                     # 0x80
	.short	256                     # 0x100
	.short	512                     # 0x200
	.short	1024                    # 0x400
	.short	2048                    # 0x800
	.short	4096                    # 0x1000
	.short	8192                    # 0x2000
	.short	16384                   # 0x4000
	.size	power2, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
