	.text
	.file	"lu.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4575657221408423936     # double 0.0078125
.LCPI6_2:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 176
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 48(%rsp)
	leaq	48(%rsp), %rdi
	movl	$32, %esi
	movl	$131072, %edx           # imm = 0x20000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_70
# BB#1:
	movq	48(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_70
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 48(%rsp)
	leaq	48(%rsp), %rdi
	movl	$32, %esi
	movl	$131072, %edx           # imm = 0x20000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_70
# BB#3:                                 # %polybench_alloc_data.exit
	movq	48(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_70
# BB#4:                                 # %polybench_alloc_data.exit21
	leaq	24(%rbx), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
	incq	%rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-128, %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	129(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsi)
	leal	130(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rsi)
	leal	131(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	132(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$4, %rdx
	jne	.LBB6_6
# BB#7:                                 # %.loopexit.i
                                        #   in Loop: Header=BB6_5 Depth=1
	addq	$1024, %rax             # imm = 0x400
	cmpq	$128, %rcx
	jne	.LBB6_5
# BB#8:                                 # %init_array.exit.preheader
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	leaq	24(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1048(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1032(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %esi
	xorl	%r12d, %r12d
	movl	$127, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rcx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_10:                               # %init_array.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_15 Depth 2
                                        #     Child Loop BB6_17 Depth 2
                                        #       Child Loop BB6_22 Depth 3
                                        #       Child Loop BB6_28 Depth 3
	leaq	1(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpq	$127, %rax
	jg	.LBB6_9
# BB#11:                                # %.lr.ph.i
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	%r12, %rdx
	shlq	$10, %rdx
	movl	$127, %edi
	movl	$127, %eax
	subl	%r12d, %eax
	movq	%rdx, %r8
	leaq	(%rbx,%rdx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	(%rdx,%r12,8), %rbp
	testb	$1, %al
	movq	%rsi, %rdx
	je	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_10 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	divsd	(%rbp), %xmm1
	movsd	%xmm1, (%rax,%rsi,8)
	leaq	1(%rsi), %rdx
.LBB6_13:                               # %.prol.loopexit146
                                        #   in Loop: Header=BB6_10 Depth=1
	imulq	$1032, %r12, %r9        # imm = 0x408
	subq	%r12, %rdi
	cmpq	$126, %r12
	je	.LBB6_16
# BB#14:                                # %.lr.ph.i.new
                                        #   in Loop: Header=BB6_10 Depth=1
	movl	$128, %eax
	subq	%rdx, %rax
	leaq	(%rcx,%rdx,8), %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_15:                               #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	divsd	(%rbp), %xmm1
	movsd	%xmm1, (%rdx,%rbx,8)
	movsd	8(%rdx,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	divsd	(%rbp), %xmm1
	movsd	%xmm1, 8(%rdx,%rbx,8)
	addq	$2, %rbx
	cmpq	%rbx, %rax
	jne	.LBB6_15
.LBB6_16:                               # %.preheader.us.i.preheader
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	andq	$-4, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	8(%rbx,%r9), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	addq	$1032, %r9              # imm = 0x408
	movq	%r9, 96(%rsp)           # 8-byte Spill
	leaq	1024(%rbx,%r8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	addq	$2048, %r8              # imm = 0x800
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rdi, %r9
	andq	$-4, %r9
	leaq	(%rsi,%r9), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	xorl	%r8d, %r8d
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader.us.i
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_22 Depth 3
                                        #       Child Loop BB6_28 Depth 3
	movq	%r14, %r15
	shlq	$10, %r15
	addq	%rbx, %r15
	cmpq	$4, %rdi
	leaq	(%r15,%r12,8), %rbp
	movq	%rsi, %rax
	jb	.LBB6_24
# BB#18:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_17 Depth=2
	testq	%r9, %r9
	movq	%rsi, %rax
	je	.LBB6_24
# BB#19:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_17 Depth=2
	movq	%r8, %rax
	shlq	$10, %rax
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	addq	%rbx, %rdx
	addq	104(%rsp), %rax         # 8-byte Folded Reload
	addq	%rbx, %rax
	cmpq	%rbp, %rdx
	sbbb	%r13b, %r13b
	cmpq	%rax, %rbp
	sbbb	%bl, %bl
	andb	%r13b, %bl
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%rax, 88(%rsp)          # 8-byte Folded Reload
	sbbb	%r13b, %r13b
	testb	$1, %bl
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rsi, %rax
	jne	.LBB6_24
# BB#20:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_17 Depth=2
	andb	%r13b, %dl
	andb	$1, %dl
	movq	%rsi, %rax
	jne	.LBB6_24
# BB#21:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_17 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%r11, %rax
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_22:                               # %vector.body
                                        #   Parent Loop BB6_10 Depth=1
                                        #     Parent Loop BB6_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rax), %xmm1
	movupd	(%rax), %xmm2
	movsd	(%rbp), %xmm3           # xmm3 = mem[0],zero
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movupd	-16(%rbx), %xmm4
	movupd	(%rbx), %xmm5
	mulpd	%xmm3, %xmm4
	mulpd	%xmm3, %xmm5
	addpd	%xmm1, %xmm4
	addpd	%xmm2, %xmm5
	movupd	%xmm4, -16(%rax)
	movupd	%xmm5, (%rax)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB6_22
# BB#23:                                # %middle.block
                                        #   in Loop: Header=BB6_17 Depth=2
	cmpq	%r9, %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB6_29
	.p2align	4, 0x90
.LBB6_24:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_17 Depth=2
	testb	$1, %al
	movq	%rax, %r13
	je	.LBB6_26
# BB#25:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB6_17 Depth=2
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movq	40(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	(%r15,%rax,8), %xmm1
	movsd	%xmm1, (%r15,%rax,8)
	leaq	1(%rax), %r13
.LBB6_26:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB6_17 Depth=2
	cmpq	$127, %rax
	je	.LBB6_29
# BB#27:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB6_17 Depth=2
	leaq	(%r10,%r13,8), %rax
	.p2align	4, 0x90
.LBB6_28:                               # %scalar.ph
                                        #   Parent Loop BB6_10 Depth=1
                                        #     Parent Loop BB6_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rcx,%r13,8), %xmm1
	addsd	-8(%rax), %xmm1
	movsd	%xmm1, -8(%rax)
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	mulsd	8(%rcx,%r13,8), %xmm1
	addsd	(%rax), %xmm1
	movsd	%xmm1, (%rax)
	addq	$2, %r13
	addq	$16, %rax
	cmpq	$128, %r13
	jne	.LBB6_28
.LBB6_29:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB6_17 Depth=2
	incq	%r14
	incq	%r8
	addq	$1024, %r11             # imm = 0x400
	addq	$1024, %r10             # imm = 0x400
	cmpq	$128, %r14
	jne	.LBB6_17
.LBB6_9:                                # %.loopexit.i22
                                        #   in Loop: Header=BB6_10 Depth=1
	incq	%rsi
	addq	$1024, %rcx             # imm = 0x400
	addq	$1032, 24(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x408
	addq	$1032, 16(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x408
	decq	(%rsp)                  # 8-byte Folded Spill
	addq	$1024, 8(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x400
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpq	$128, %r12
	jne	.LBB6_10
# BB#58:                                # %kernel_lu.exit
	movq	112(%rsp), %rbp         # 8-byte Reload
	leaq	24(%rbp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_59:                               # %.preheader.i28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_60 Depth 2
	incq	%rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-128, %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_60:                               #   Parent Loop BB6_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	129(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsi)
	leal	130(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rsi)
	leal	131(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	132(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$4, %rdx
	jne	.LBB6_60
# BB#30:                                # %.loopexit.i25
                                        #   in Loop: Header=BB6_59 Depth=1
	addq	$1024, %rax             # imm = 0x400
	cmpq	$128, %rcx
	jne	.LBB6_59
# BB#31:                                # %init_array.exit32.preheader
	leaq	24(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1048(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1032(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %esi
	xorl	%r14d, %r14d
	movl	$127, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB6_32:                               # %init_array.exit32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_37 Depth 2
                                        #     Child Loop BB6_39 Depth 2
                                        #       Child Loop BB6_44 Depth 3
                                        #       Child Loop BB6_50 Depth 3
	leaq	1(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpq	$127, %rax
	jg	.LBB6_52
# BB#33:                                # %.lr.ph.i39
                                        #   in Loop: Header=BB6_32 Depth=1
	movq	%r14, %rcx
	shlq	$10, %rcx
	movl	$127, %r9d
	movl	$127, %eax
	subl	%r14d, %eax
	movq	%rcx, %r8
	leaq	(%rbp,%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r14,8), %rdx
	testb	$1, %al
	movq	%rsi, %rcx
	je	.LBB6_35
# BB#34:                                #   in Loop: Header=BB6_32 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	divsd	(%rdx), %xmm0
	movsd	%xmm0, (%rax,%rsi,8)
	leaq	1(%rsi), %rcx
.LBB6_35:                               # %.prol.loopexit
                                        #   in Loop: Header=BB6_32 Depth=1
	imulq	$1032, %r14, %r10       # imm = 0x408
	subq	%r14, %r9
	cmpq	$126, %r14
	je	.LBB6_38
# BB#36:                                # %.lr.ph.i39.new
                                        #   in Loop: Header=BB6_32 Depth=1
	movl	$128, %eax
	subq	%rcx, %rax
	leaq	(%rbx,%rcx,8), %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_37:                               #   Parent Loop BB6_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	divsd	(%rdx), %xmm0
	movsd	%xmm0, (%rcx,%rdi,8)
	movsd	8(%rcx,%rdi,8), %xmm0   # xmm0 = mem[0],zero
	divsd	(%rdx), %xmm0
	movsd	%xmm0, 8(%rcx,%rdi,8)
	addq	$2, %rdi
	cmpq	%rdi, %rax
	jne	.LBB6_37
.LBB6_38:                               # %.preheader.us.i42.preheader
                                        #   in Loop: Header=BB6_32 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	andq	$-4, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	8(%rbp,%r10), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	addq	$1032, %r10             # imm = 0x408
	movq	%r10, 96(%rsp)          # 8-byte Spill
	leaq	1024(%rbp,%r8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	addq	$2048, %r8              # imm = 0x800
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%r9, %r12
	andq	$-4, %r12
	leaq	(%rsi,%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	xorl	%r8d, %r8d
	movq	%rsi, %r15
	.p2align	4, 0x90
.LBB6_39:                               # %.preheader.us.i42
                                        #   Parent Loop BB6_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_44 Depth 3
                                        #       Child Loop BB6_50 Depth 3
	movq	%r15, %rdx
	shlq	$10, %rdx
	addq	%rbp, %rdx
	cmpq	$4, %r9
	leaq	(%rdx,%r14,8), %rbp
	movq	%rsi, %rax
	jb	.LBB6_46
# BB#40:                                # %min.iters.checked101
                                        #   in Loop: Header=BB6_39 Depth=2
	testq	%r12, %r12
	movq	%rsi, %rax
	je	.LBB6_46
# BB#41:                                # %vector.memcheck122
                                        #   in Loop: Header=BB6_39 Depth=2
	movq	%r8, %rax
	shlq	$10, %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	movq	112(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %rcx
	addq	104(%rsp), %rax         # 8-byte Folded Reload
	addq	%rdi, %rax
	cmpq	%rbp, %rcx
	sbbb	%dil, %dil
	cmpq	%rax, %rbp
	sbbb	%r13b, %r13b
	andb	%dil, %r13b
	cmpq	80(%rsp), %rcx          # 8-byte Folded Reload
	sbbb	%cl, %cl
	cmpq	%rax, 88(%rsp)          # 8-byte Folded Reload
	sbbb	%dil, %dil
	testb	$1, %r13b
	movq	%rsi, %rax
	jne	.LBB6_46
# BB#42:                                # %vector.memcheck122
                                        #   in Loop: Header=BB6_39 Depth=2
	andb	%dil, %cl
	andb	$1, %cl
	movq	%rsi, %rax
	jne	.LBB6_46
# BB#43:                                # %vector.body97.preheader
                                        #   in Loop: Header=BB6_39 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%r11, %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_44:                               # %vector.body97
                                        #   Parent Loop BB6_32 Depth=1
                                        #     Parent Loop BB6_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rcx), %xmm0
	movupd	(%rcx), %xmm1
	movsd	(%rbp), %xmm2           # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movupd	-16(%rdi), %xmm3
	movupd	(%rdi), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	addpd	%xmm0, %xmm3
	addpd	%xmm1, %xmm4
	movupd	%xmm3, -16(%rcx)
	movupd	%xmm4, (%rcx)
	addq	$32, %rdi
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB6_44
# BB#45:                                # %middle.block98
                                        #   in Loop: Header=BB6_39 Depth=2
	cmpq	%r12, %r9
	movq	64(%rsp), %rax          # 8-byte Reload
	je	.LBB6_51
	.p2align	4, 0x90
.LBB6_46:                               # %scalar.ph99.preheader
                                        #   in Loop: Header=BB6_39 Depth=2
	testb	$1, %al
	movq	%rax, %r13
	je	.LBB6_48
# BB#47:                                # %scalar.ph99.prol
                                        #   in Loop: Header=BB6_39 Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movq	40(%rsp), %rcx          # 8-byte Reload
	mulsd	(%rcx,%rax,8), %xmm0
	addsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
	leaq	1(%rax), %r13
.LBB6_48:                               # %scalar.ph99.prol.loopexit
                                        #   in Loop: Header=BB6_39 Depth=2
	cmpq	$127, %rax
	je	.LBB6_51
# BB#49:                                # %scalar.ph99.preheader.new
                                        #   in Loop: Header=BB6_39 Depth=2
	leaq	(%r10,%r13,8), %rax
	.p2align	4, 0x90
.LBB6_50:                               # %scalar.ph99
                                        #   Parent Loop BB6_32 Depth=1
                                        #     Parent Loop BB6_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%r13,8), %xmm0
	addsd	-8(%rax), %xmm0
	movsd	%xmm0, -8(%rax)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	8(%rbx,%r13,8), %xmm0
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	addq	$2, %r13
	addq	$16, %rax
	cmpq	$128, %r13
	jne	.LBB6_50
.LBB6_51:                               # %._crit_edge.us.i48
                                        #   in Loop: Header=BB6_39 Depth=2
	incq	%r15
	incq	%r8
	addq	$1024, %r11             # imm = 0x400
	addq	$1024, %r10             # imm = 0x400
	cmpq	$128, %r15
	movq	112(%rsp), %rbp         # 8-byte Reload
	jne	.LBB6_39
.LBB6_52:                               # %.loopexit.i35
                                        #   in Loop: Header=BB6_32 Depth=1
	incq	%rsi
	addq	$1024, %rbx             # imm = 0x400
	addq	$1032, 24(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x408
	addq	$1032, 16(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x408
	decq	(%rsp)                  # 8-byte Folded Spill
	addq	$1024, 8(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x400
	movq	56(%rsp), %r14          # 8-byte Reload
	cmpq	$128, %r14
	jne	.LBB6_32
# BB#53:                                # %.preheader.i54.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_1(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_2(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB6_54:                               # %.preheader.i54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_55 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_55:                               #   Parent Loop BB6_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbp,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_56
# BB#61:                                #   in Loop: Header=BB6_55 Depth=2
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbp,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_57
# BB#62:                                #   in Loop: Header=BB6_55 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$128, %rcx
	movq	%rdi, %rcx
	jl	.LBB6_55
# BB#63:                                #   in Loop: Header=BB6_54 Depth=1
	incq	%rdx
	subq	$-128, %rax
	cmpq	$128, %rdx
	jl	.LBB6_54
# BB#64:                                # %check_FP.exit
	movl	$2049, %edi             # imm = 0x801
	callq	malloc
	movq	%rax, %r15
	movb	$0, 2048(%r15)
	xorl	%r14d, %r14d
	movq	%rbp, %r12
	.p2align	4, 0x90
.LBB6_65:                               # %.preheader.i60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_66 Depth 2
	movq	%r12, %rax
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_66:                               #   Parent Loop BB6_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r15,%rcx)
	movb	%bl, -14(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r15,%rcx)
	movb	%sil, -12(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r15,%rcx)
	movb	%sil, -10(%r15,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r15,%rcx)
	movb	%sil, -8(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r15,%rcx)
	movb	%sil, -6(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r15,%rcx)
	movb	%sil, -4(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r15,%rcx)
	movb	%sil, -2(%r15,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r15,%rcx)
	movb	%dl, (%r15,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$2063, %rcx             # imm = 0x80F
	jne	.LBB6_66
# BB#67:                                #   in Loop: Header=BB6_65 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r15, %rdi
	callq	fputs
	incq	%r14
	addq	$1024, %r12             # imm = 0x400
	cmpq	$128, %r14
	jne	.LBB6_65
# BB#68:                                # %print_array.exit
	movq	%r15, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbp, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_69
.LBB6_56:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_57:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_69:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_70:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
