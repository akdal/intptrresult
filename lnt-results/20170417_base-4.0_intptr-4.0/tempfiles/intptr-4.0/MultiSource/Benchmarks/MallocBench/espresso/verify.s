	.text
	.file	"verify.bc"
	.globl	verify
	.p2align	4, 0x90
	.type	verify,@function
verify:                                 # @verify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdx, %rsi
	callq	cube2list
	movq	%rax, %rbx
	movslq	(%r13), %rcx
	movslq	12(%r13), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_7
# BB#1:                                 # %.lr.ph55.preheader
	movq	24(%r13), %rbp
	leaq	(%rbp,%rax,4), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cube_is_covered
	testl	%eax, %eax
	jne	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.1, %edi
	callq	puts
	cmpl	$0, verbose_debug(%rip)
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pc1
	movq	%rax, %rdi
	callq	puts
	movl	$1, %r15d
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	(%r13), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r14, %rbp
	jb	.LBB0_2
	jmp	.LBB0_7
.LBB0_4:
	movl	$1, %r15d
.LBB0_7:                                # %._crit_edge56
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	callq	free
.LBB0_9:
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	cube2list
	movq	%rax, %r14
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_16
# BB#10:                                # %.lr.ph.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	cube_is_covered
	testl	%eax, %eax
	jne	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	cmpl	$0, verbose_debug(%rip)
	je	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_11 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rdi
	callq	puts
	movl	$1, %r15d
.LBB0_15:                               #   in Loop: Header=BB0_11 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB0_11
	jmp	.LBB0_16
.LBB0_13:
	movl	$1, %r15d
.LBB0_16:                               # %._crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#17:
	callq	free
.LBB0_18:
	movq	%r14, %rdi
	callq	free
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	verify, .Lfunc_end0-verify
	.cfi_endproc

	.globl	PLA_verify
	.p2align	4, 0x90
	.type	PLA_verify,@function
PLA_verify:                             # @PLA_verify
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#1:
	cmpq	$0, (%rax)
	je	.LBB1_6
# BB#2:
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#3:
	cmpq	$0, (%rax)
	je	.LBB1_6
# BB#4:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	PLA_permute
	movq	(%rbx), %rsi
	movl	4(%rsi), %eax
	movq	(%r14), %rdi
	cmpl	4(%rdi), %eax
	jne	.LBB1_5
# BB#8:
	movq	8(%rbx), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	verify                  # TAILCALL
.LBB1_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$46, %esi
.LBB1_7:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$40, %esi
	jmp	.LBB1_7
.Lfunc_end1:
	.size	PLA_verify, .Lfunc_end1-PLA_verify
	.cfi_endproc

	.globl	PLA_permute
	.p2align	4, 0x90
	.type	PLA_permute,@function
PLA_permute:                            # @PLA_permute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 112
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rbp
	movslq	4(%rbp), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rsi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	4(%rbp), %ecx
	testl	%ecx, %ecx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph79
	movq	56(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	(%r14), %rdi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movslq	4(%rdi), %r12
	testq	%r12, %r12
	jle	.LBB2_9
# BB#4:                                 # %.lr.ph75
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rbx
	movq	%r14, %r15
	movq	56(%r14), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_5:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%r14,8), %rsi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_6
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=2
	incq	%r14
	cmpq	%r12, %r14
	jl	.LBB2_5
# BB#8:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	%r15, %r14
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movslq	%ecx, %rax
	incl	%ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r14d, (%rsi,%rax,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %ecx
	movq	%r15, %r14
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB2_9:                                # %.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB2_3
# BB#10:
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB2_12
	jmp	.LBB2_13
.LBB2_1:                                # %.._crit_edge80_crit_edge
	movq	(%r14), %rdi
	xorl	%ebp, %ebp
	testq	%rdi, %rdi
	je	.LBB2_13
.LBB2_12:
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sf_permute
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, (%r14)
.LBB2_13:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_15
# BB#14:
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sf_permute
	movq	%rax, 16(%r14)
.LBB2_15:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
# BB#16:
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%ebp, %edx
	callq	sf_permute
	movq	%rax, 8(%r14)
.LBB2_17:
	movslq	cube(%rip), %r13
	movq	%r13, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %r15
	testl	%ebp, %ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	jle	.LBB2_23
# BB#18:                                # %.lr.ph72
	movq	56(%r14), %rax
	movq	%rbp, %r8
	movl	%ebp, %ecx
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB2_20
	.p2align	4, 0x90
.LBB2_19:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rbx,%rsi,4), %rbp
	movq	(%rax,%rbp,8), %rbp
	movq	%rbp, (%r15,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB2_19
.LBB2_20:                               # %.prol.loopexit
	cmpq	$3, %rdx
	movq	%r8, %rbp
	jb	.LBB2_23
# BB#21:                                # %.lr.ph72.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	leaq	12(%rbx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB2_22:                               # =>This Inner Loop Header: Depth=1
	movslq	-12(%rsi), %rdi
	movq	(%rax,%rdi,8), %rdi
	movq	%rdi, -24(%rdx)
	movslq	-8(%rsi), %rdi
	movq	(%rax,%rdi,8), %rdi
	movq	%rdi, -16(%rdx)
	movslq	-4(%rsi), %rdi
	movq	(%rax,%rdi,8), %rdi
	movq	%rdi, -8(%rdx)
	movslq	(%rsi), %rdi
	movq	(%rax,%rdi,8), %rdi
	movq	%rdi, (%rdx)
	addq	$32, %rdx
	addq	$16, %rsi
	addq	$-4, %rcx
	jne	.LBB2_22
.LBB2_23:                               # %.preheader
	cmpl	%r13d, %ebp
	jge	.LBB2_25
# BB#24:                                # %.lr.ph
	movslq	%ebp, %rax
	leaq	(%r15,%rax,8), %rdi
	leal	1(%rbp), %eax
	cmpl	%eax, %r13d
	cmovgel	%r13d, %eax
	decl	%eax
	subl	%ebp, %eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB2_25:                               # %._crit_edge
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_27
# BB#26:
	callq	free
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	$0, 56(%r14)
.LBB2_27:
	movq	%r15, 56(%r14)
	testq	%rbx, %rbx
	je	.LBB2_29
# BB#28:
	movq	%rbx, %rdi
	callq	free
.LBB2_29:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	PLA_permute, .Lfunc_end2-PLA_permute
	.cfi_endproc

	.globl	check_consistency
	.p2align	4, 0x90
	.type	check_consistency,@function
check_consistency:                      # @check_consistency
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbp
	cmpl	$0, 12(%rbp)
	je	.LBB3_1
# BB#2:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$1, %r14d
	cmpl	$0, verbose_debug(%rip)
	je	.LBB3_4
# BB#3:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cprint
	jmp	.LBB3_4
.LBB3_1:
	movl	$.Lstr.9, %edi
	callq	puts
.LBB3_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbp
	cmpl	$0, 12(%rbp)
	je	.LBB3_5
# BB#6:
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$1, %r14d
	cmpl	$0, verbose_debug(%rip)
	je	.LBB3_8
# BB#7:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cprint
	jmp	.LBB3_8
.LBB3_5:
	movl	$.Lstr.8, %edi
	callq	puts
.LBB3_8:
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbp
	cmpl	$0, 12(%rbp)
	je	.LBB3_9
# BB#10:
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$1, %r14d
	cmpl	$0, verbose_debug(%rip)
	je	.LBB3_12
# BB#11:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cprint
	jmp	.LBB3_12
.LBB3_9:
	movl	$.Lstr.7, %edi
	callq	puts
.LBB3_12:
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	callq	cube3list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	tautology
	testl	%eax, %eax
	je	.LBB3_14
# BB#13:
	movl	$.Lstr.6, %edi
	callq	puts
	jmp	.LBB3_17
.LBB3_14:
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	callq	cube3list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %rbx
	movl	$.Lstr.5, %edi
	callq	puts
	cmpl	$0, verbose_debug(%rip)
	je	.LBB3_16
# BB#15:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cprint
.LBB3_16:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movl	$1, %r14d
.LBB3_17:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	check_consistency, .Lfunc_end3-check_consistency
	.cfi_endproc

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Warning: cannot permute columns without names\n"
	.size	.L.str.3, 47

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"PLA_verify: PLA's are not the same size\n"
	.size	.L.str.4, 41

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"some minterm in Fold is not covered by F u Dold"
	.size	.Lstr, 48

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"some minterm in F is not covered by Fold u Dold"
	.size	.Lstr.1, 48

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Some minterm(s) belong to both the ON-SET and DC-SET !"
	.size	.Lstr.2, 55

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Some minterm(s) belong to both the ON-SET and OFF-SET !"
	.size	.Lstr.3, 56

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"Some minterm(s) belong to both the OFF-SET and DC-SET !"
	.size	.Lstr.4, 56

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"There are minterms left unspecified !"
	.size	.Lstr.5, 38

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"Union of ON-SET, OFF-SET and DC-SET is the universe"
	.size	.Lstr.6, 52

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"DC-SET and OFF-SET are disjoint"
	.size	.Lstr.7, 32

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"ON-SET and OFF-SET are disjoint"
	.size	.Lstr.8, 32

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"ON-SET and DC-SET are disjoint"
	.size	.Lstr.9, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
