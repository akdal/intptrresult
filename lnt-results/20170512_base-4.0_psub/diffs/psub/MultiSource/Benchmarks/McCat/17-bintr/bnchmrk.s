	.text
	.file	"bnchmrk.bc"
	.globl	fillTree
	.p2align	4, 0x90
	.type	fillTree,@function
fillTree:                               # @fillTree
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$.Lstr, %edi
	callq	puts
	leaq	4(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	scanf
	movl	4(%rsp), %edi
	testl	%edi, %edi
	je	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	4(%rsp), %r14
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rsi
	callq	insertSortedBinaryTree
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	scanf
	movl	4(%rsp), %edi
	testl	%edi, %edi
	jne	.LBB0_2
.LBB0_3:                                # %._crit_edge
	movl	$.Lstr.1, %edi
	callq	puts
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	fillTree, .Lfunc_end0-fillTree
	.cfi_endproc

	.globl	fillSearchedValues
	.p2align	4, 0x90
	.type	fillSearchedValues,@function
fillSearchedValues:                     # @fillSearchedValues
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$.Lstr.2, %edi
	callq	puts
	leaq	12(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	scanf
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB1_4
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	leaq	12(%rsp), %r14
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, -4(%r15,%rbx,4)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	scanf
	cmpq	$99, %rbx
	jg	.LBB1_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	12(%rsp), %eax
	incq	%rbx
	testl	%eax, %eax
	jne	.LBB1_2
.LBB1_4:                                # %._crit_edge
	movl	$.Lstr.3, %edi
	callq	puts
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	fillSearchedValues, .Lfunc_end1-fillSearchedValues
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
	subq	$416, %rsp              # imm = 0x1A0
.Lcfi15:
	.cfi_def_cfa_offset 448
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	movl	$.Lstr.4, %edi
	callq	puts
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	$0, 48(%rsp)
	movl	$.Lstr, %edi
	callq	puts
	leaq	4(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	scanf
	movl	4(%rsp), %edi
	testl	%edi, %edi
	je	.LBB2_3
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	8(%rsp), %r14
	leaq	4(%rsp), %rbx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rsi
	callq	insertSortedBinaryTree
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	movl	4(%rsp), %edi
	testl	%edi, %edi
	jne	.LBB2_2
.LBB2_3:                                # %fillTree.exit
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	leaq	4(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	scanf
	movl	4(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_7
# BB#4:                                 # %.lr.ph.i12.preheader
	movl	$1, %ebp
	leaq	4(%rsp), %rbx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, 12(%rsp,%rbp,4)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	cmpq	$99, %rbp
	jg	.LBB2_7
# BB#6:                                 # %.lr.ph.i12
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	4(%rsp), %eax
	incq	%rbp
	testl	%eax, %eax
	jne	.LBB2_5
.LBB2_7:                                # %fillSearchedValues.exit
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movq	8(%rsp), %rdi
	callq	getSizeBinaryTree
	movl	%eax, %ecx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movq	8(%rsp), %rdi
	callq	getSumBinaryTree
	movl	$.L.str.8, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rsp), %rdi
	callq	getArithmeticMeanBinaryTree
	movl	$.L.str.9, %edi
	movb	$1, %al
	callq	printf
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi
	movl	16(%rsp,%rbx,4), %ebp
	movl	%ebp, %esi
	callq	memberOfBinaryTree
	testl	%eax, %eax
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	movl	$.L.str.10, %edi
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	movl	$.L.str.11, %edi
.LBB2_11:                               #   in Loop: Header=BB2_8 Depth=1
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	incq	%rbx
	cmpq	$100, %rbx
	jne	.LBB2_8
# BB#12:
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	xorl	%eax, %eax
	addq	$416, %rsp              # imm = 0x1A0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Size                   : %d\n\n"
	.size	.L.str.7, 30

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Sum                    : %f\n\n"
	.size	.L.str.8, 30

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Arithmetic Mean        : %f\n\n"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%d is in the tree.\n"
	.size	.L.str.10, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d is NOT in the tree.\n"
	.size	.L.str.11, 24

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Constructing tree\n\n"
	.size	.Lstr, 20

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"\n\nTree constructed\n\n"
	.size	.Lstr.1, 21

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Constructing array of values\n\n"
	.size	.Lstr.2, 31

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"\n\nArray of values constructed\n\n"
	.size	.Lstr.3, 32

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"Beginning of program\n\n"
	.size	.Lstr.4, 23

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"Summary of sorted binary tree\n=============================\n"
	.size	.Lstr.5, 61

	.type	.Lstr.6,@object         # @str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.6:
	.asciz	"\n"
	.size	.Lstr.6, 2

	.type	.Lstr.7,@object         # @str.7
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.7:
	.asciz	"\n\nEnd of program"
	.size	.Lstr.7, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
