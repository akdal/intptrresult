	.text
	.file	"pfloat.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4679240012837945344     # double 65536
.LCPI0_2:
	.quad	4535124824762089472     # double 1.52587890625E-5
	.text
	.globl	dtop
	.p2align	4, 0x90
	.type	dtop,@function
dtop:                                   # @dtop
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movl	$129, %edi
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_6
# BB#1:
	xorps	%xmm0, %xmm0
	movapd	(%rsp), %xmm1           # 16-byte Reload
	ucomisd	%xmm1, %xmm0
	movapd	%xmm1, %xmm0
	jbe	.LBB0_3
# BB#2:
	xorpd	.LCPI0_0(%rip), %xmm0
.LBB0_3:
	seta	6(%r15)
	movq	%r15, %r14
	addq	$8, %r14
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	fmod
	cvttsd2si	%xmm0, %eax
	movw	%ax, (%rbx)
	addq	$2, %rbx
	movapd	(%rsp), %xmm0           # 16-byte Reload
	mulsd	.LCPI0_2(%rip), %xmm0
	callq	floor
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB0_4
	jp	.LBB0_4
# BB#5:
	subl	%r14d, %ebx
	shrl	%ebx
	movw	%bx, 4(%r15)
	movq	%r15, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	presult                 # TAILCALL
.LBB0_6:
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	dtop, .Lfunc_end0-dtop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4679240012837945344     # double 65536
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	ptod
	.p2align	4, 0x90
	.type	ptod,@function
ptod:                                   # @ptod
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	incw	(%rdi)
.LBB1_2:
	leaq	8(%rdi), %rax
	movzwl	4(%rdi), %ecx
	leaq	8(%rdi,%rcx,2), %rcx
	xorpd	%xmm0, %xmm0
	movsd	.LCPI1_0(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movzwl	-2(%rcx), %edx
	addq	$-2, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	addsd	%xmm1, %xmm0
	cmpq	%rax, %rcx
	ja	.LBB1_3
# BB#4:
	cmpb	$0, 6(%rdi)
	je	.LBB1_6
# BB#5:
	xorpd	.LCPI1_1(%rip), %xmm0
.LBB1_6:
	testq	%rdi, %rdi
	je	.LBB1_9
# BB#7:
	decw	(%rdi)
	jne	.LBB1_9
# BB#8:
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
	xorl	%eax, %eax
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	callq	pfree
	movapd	(%rsp), %xmm0           # 16-byte Reload
	addq	$24, %rsp
.LBB1_9:
	retq
.Lfunc_end1:
	.size	ptod, .Lfunc_end1-ptod
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
