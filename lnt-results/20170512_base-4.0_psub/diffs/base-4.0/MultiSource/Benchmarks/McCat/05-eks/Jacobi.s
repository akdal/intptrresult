	.text
	.file	"Jacobi.bc"
	.globl	ApplyGivens
	.p2align	4, 0x90
	.type	ApplyGivens,@function
ApplyGivens:                            # @ApplyGivens
	.cfi_startproc
# BB#0:
	cmpl	%r8d, %ecx
	jg	.LBB0_7
# BB#1:                                 # %.lr.ph55
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movslq	%esi, %r9
	movq	(%rdi,%r9,8), %r13
	movslq	%edx, %r10
	movq	(%rdi,%r10,8), %rdx
	movslq	%ecx, %rsi
	movslq	%r8d, %r11
	cmpq	%rsi, %r11
	movq	%rsi, %r15
	cmovgeq	%r11, %r15
	incq	%r15
	subq	%rsi, %r15
	cmpq	$1, %r15
	jbe	.LBB0_2
# BB#8:                                 # %min.iters.checked
	movq	%r15, %r14
	andq	$-2, %r14
	je	.LBB0_2
# BB#9:                                 # %vector.memcheck
	leaq	(%r13,%rsi,8), %rbx
	cmpq	%rsi, %r11
	movq	%rsi, %rax
	cmovgeq	%r11, %rax
	leaq	8(%rdx,%rax,8), %rbp
	cmpq	%rbp, %rbx
	jae	.LBB0_11
# BB#10:                                # %vector.memcheck
	leaq	8(%r13,%rax,8), %rax
	leaq	(%rdx,%rsi,8), %rbx
	cmpq	%rax, %rbx
	jb	.LBB0_2
.LBB0_11:                               # %vector.ph
	movaps	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm0, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%r14), %rbx
	movq	%rbx, %rax
	shrq	%rax
	btl	$1, %ebx
	jb	.LBB0_12
# BB#13:                                # %vector.body.prol
	movupd	(%r13,%rsi,8), %xmm4
	movupd	(%rdx,%rsi,8), %xmm5
	movapd	%xmm4, %xmm6
	mulpd	%xmm2, %xmm6
	movapd	%xmm5, %xmm7
	mulpd	%xmm3, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%r13,%rsi,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rdx,%rsi,8)
	movl	$2, %ebp
	testq	%rax, %rax
	jne	.LBB0_15
	jmp	.LBB0_17
.LBB0_12:
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.LBB0_17
.LBB0_15:                               # %vector.ph.new
	movq	%r14, %r12
	subq	%rbp, %r12
	addq	%rsi, %rbp
	leaq	16(%rdx,%rbp,8), %rbx
	leaq	16(%r13,%rbp,8), %rax
	.p2align	4, 0x90
.LBB0_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm4
	movupd	-16(%rbx), %xmm5
	movapd	%xmm4, %xmm6
	mulpd	%xmm2, %xmm6
	movapd	%xmm5, %xmm7
	mulpd	%xmm3, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, -16(%rax)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, -16(%rbx)
	movupd	(%rax), %xmm4
	movupd	(%rbx), %xmm5
	movapd	%xmm4, %xmm6
	mulpd	%xmm2, %xmm6
	movapd	%xmm5, %xmm7
	mulpd	%xmm3, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%rax)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rbx)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$-4, %r12
	jne	.LBB0_16
.LBB0_17:                               # %middle.block
	cmpq	%r14, %r15
	je	.LBB0_4
# BB#18:
	addq	%r14, %rsi
.LBB0_2:                                # %scalar.ph.preheader
	decq	%rsi
	.p2align	4, 0x90
.LBB0_3:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%r13,%rsi,8), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rdx,%rsi,8), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, 8(%r13,%rsi,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, 8(%rdx,%rsi,8)
	incq	%rsi
	cmpq	%r11, %rsi
	jl	.LBB0_3
.LBB0_4:                                # %.preheader
	cmpl	%r8d, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jg	.LBB0_7
# BB#5:                                 # %.lr.ph
	movslq	%ecx, %rax
	decq	%rax
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rdi,%rax,8), %rcx
	movsd	(%rcx,%r9,8), %xmm2     # xmm2 = mem[0],zero
	movsd	(%rcx,%r10,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm2, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%rcx,%r9,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rcx,%r10,8)
	incq	%rax
	cmpq	%r11, %rax
	jl	.LBB0_6
.LBB0_7:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	ApplyGivens, .Lfunc_end0-ApplyGivens
	.cfi_endproc

	.globl	Jacobi
	.p2align	4, 0x90
	.type	Jacobi,@function
Jacobi:                                 # @Jacobi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi18:
	.cfi_def_cfa_offset 464
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r13
	callq	newIdMatrix
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	cmpl	$2, %ebx
	jl	.LBB1_47
# BB#1:                                 # %.preheader.preheader
	movslq	%eax, %rcx
	movl	$51, %edx
	subl	%eax, %edx
	movl	%edx, 68(%rsp)          # 4-byte Spill
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rsi
	notq	%rsi
	addl	%eax, %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	$2, %edi
	subq	%rcx, %rdi
	movq	%rsi, %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	1(%rsi), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	leaq	-2(%rcx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	-8(%r13,%rcx,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	(,%rcx,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	%rcx, %rsi
	movq	%r13, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_14 Depth 3
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_20 Depth 3
                                        #       Child Loop BB1_24 Depth 3
                                        #         Child Loop BB1_36 Depth 4
                                        #         Child Loop BB1_40 Depth 4
                                        #         Child Loop BB1_42 Depth 4
	movl	$51, %eax
	subq	%rsi, %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	jle	.LBB1_46
# BB#3:                                 # %.lr.ph86
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	112(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rdi), %eax
	movq	280(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rcx
	notq	%rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movl	%edx, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rcx
	notq	%rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	264(%rsp), %rcx         # 8-byte Reload
	subq	%rdi, %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rcx
	subq	%rdi, %rcx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rdi,%rcx), %rbx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %r8
	movq	%rbx, %rcx
	subq	%r8, %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	%rdi, %rcx
	subq	%rbp, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movl	%edx, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	subq	%rax, %rbx
	movq	%rbx, 304(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	leal	(%rsi,%rsi), %eax
	cltq
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	68(%rsp), %eax          # 4-byte Reload
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	$-1, 192(%rsp)          # 8-byte Folded Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$2, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_14 Depth 3
                                        #       Child Loop BB1_18 Depth 3
                                        #       Child Loop BB1_20 Depth 3
                                        #       Child Loop BB1_24 Depth 3
                                        #         Child Loop BB1_36 Depth 4
                                        #         Child Loop BB1_40 Depth 4
                                        #         Child Loop BB1_42 Depth 4
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	336(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	cmpq	$-52, %rbx
	movq	$-51, %rcx
	cmovleq	%rcx, %rbx
	shlq	$32, %rbx
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	xorq	%rax, %rbx
	sarq	$32, %rbx
	cmpq	%rbx, %rdx
	cmovgeq	%rdx, %rbx
	movq	328(%rsp), %r12         # 8-byte Reload
	subq	%rdx, %r12
	cmpq	$-52, %r12
	cmovleq	%rcx, %r12
	shlq	$32, %r12
	xorq	%rax, %r12
	sarq	$32, %r12
	cmpq	%r12, %rdx
	movq	%r12, %r15
	cmovgeq	%rdx, %r15
	movq	(%r13,%rdx,8), %rax
	leaq	(%rdx,%rsi), %rbp
	leal	-1(%rdx,%rsi), %r14d
	movsd	-8(%rax,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movsd	(%rax,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	leaq	56(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	Givens
	movq	72(%rsp), %r9           # 8-byte Reload
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # xmm1 = mem[0],zero
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%r9,%rax), %rax
	cmpq	$51, %rax
	movl	$50, %ecx
	cmovgeq	%rcx, %rax
	cltq
	cmpq	%rax, %r9
	jg	.LBB1_22
# BB#5:                                 # %.lr.ph55.i
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	$1, %ecx
	subq	%r9, %rcx
	addq	%rcx, %r15
	movq	-8(%r13,%rbp,8), %rcx
	movq	(%r13,%rbp,8), %rdx
	cmpq	$2, %r15
	movq	%r9, %rdi
	jb	.LBB1_17
# BB#6:                                 # %min.iters.checked130
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	%r15, %r8
	andq	$-2, %r8
	movq	%r9, %rdi
	je	.LBB1_17
# BB#7:                                 # %vector.memcheck149
                                        #   in Loop: Header=BB1_4 Depth=2
	incq	%rbx
	leaq	(%rcx,%r9,8), %rdi
	leaq	(%rdx,%rbx,8), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB1_9
# BB#8:                                 # %vector.memcheck149
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	(%rcx,%rbx,8), %rsi
	leaq	(%rdx,%r9,8), %rdi
	cmpq	%rsi, %rdi
	movq	%r9, %rdi
	jb	.LBB1_17
.LBB1_9:                                # %vector.ph150
                                        #   in Loop: Header=BB1_4 Depth=2
	movaps	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm0, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%r8), %rsi
	movq	%rsi, %rdi
	shrq	%rdi
	btl	$1, %esi
	jb	.LBB1_10
# BB#11:                                # %vector.body124.prol
                                        #   in Loop: Header=BB1_4 Depth=2
	movupd	(%rcx,%r9,8), %xmm4
	movupd	(%rdx,%r9,8), %xmm5
	movaps	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movaps	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%rcx,%r9,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rdx,%r9,8)
	movl	$2, %ebx
	testq	%rdi, %rdi
	jne	.LBB1_13
	jmp	.LBB1_15
.LBB1_10:                               #   in Loop: Header=BB1_4 Depth=2
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB1_15
.LBB1_13:                               # %vector.ph150.new
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	%r9, %r12
	cmovlq	%r9, %r12
	addq	136(%rsp), %r12         # 8-byte Folded Reload
	andq	$-2, %r12
	subq	%rbx, %r12
	movq	144(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbx,%rsi), %rsi
	leaq	(%rdx,%rsi,8), %rdi
	addq	%r9, %rbx
	leaq	(%rcx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_14:                               # %vector.body124
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rbx), %xmm4
	movupd	-16(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, (%rbx)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, -16(%rdi)
	movupd	16(%rbx), %xmm4
	movupd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, 16(%rbx)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rdi)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %r12
	jne	.LBB1_14
.LBB1_15:                               # %middle.block125
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	%r8, %r15
	je	.LBB1_19
# BB#16:                                #   in Loop: Header=BB1_4 Depth=2
	addq	%r9, %r8
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_17:                               # %scalar.ph126.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	decq	%rdi
	.p2align	4, 0x90
.LBB1_18:                               # %scalar.ph126
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rcx,%rdi,8), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rdx,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	movaps	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, 8(%rcx,%rdi,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, 8(%rdx,%rdi,8)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB1_18
.LBB1_19:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	192(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r13,%rcx,8), %rdx
	movsd	-8(%rdx,%rbp,8), %xmm2  # xmm2 = mem[0],zero
	movsd	(%rdx,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	movaps	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, -8(%rdx,%rbp,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdx,%rbp,8)
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB1_20
# BB#21:                                # %ApplyGivens.exit.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # xmm1 = mem[0],zero
.LBB1_22:                               # %ApplyGivens.exit
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	ApplyRGivens
	cmpq	232(%rsp), %rbp         # 8-byte Folded Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	jge	.LBB1_45
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rax         # 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	304(%rsp), %rax         # 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	288(%rsp), %rax         # 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	184(%rsp), %r15         # 8-byte Reload
	movq	176(%rsp), %r13         # 8-byte Reload
	movq	168(%rsp), %r12         # 8-byte Reload
	movq	160(%rsp), %r14         # 8-byte Reload
	xorl	%ebx, %ebx
	movq	152(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB1_24
.LBB1_31:                               # %vector.ph
                                        #   in Loop: Header=BB1_24 Depth=3
	movaps	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm0, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%r8), %rdi
	movq	%rdi, %r15
	shrq	%r15
	btl	$1, %edi
	movq	24(%rsp), %r10          # 8-byte Reload
	jb	.LBB1_32
# BB#33:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	%r9, %rsi
	movupd	-8(%rcx,%rsi,8), %xmm4
	movupd	-8(%rdx,%rsi,8), %xmm5
	movaps	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movaps	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, -8(%rcx,%rsi,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, -8(%rdx,%rsi,8)
	movl	$2, %edi
	jmp	.LBB1_34
.LBB1_32:                               #   in Loop: Header=BB1_24 Depth=3
	xorl	%edi, %edi
.LBB1_34:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%rbx, %r11
	testq	%r15, %r15
	je	.LBB1_37
# BB#35:                                # %vector.ph.new
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	%r10, %rbx
	cmpq	%rbx, %r11
	cmovlq	%rbx, %r11
	addq	%r12, %r11
	andq	$-2, %r11
	movq	%r9, %rbx
	leaq	(%rdx,%rbx,8), %r9
	leaq	(%rcx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_36:                               # %vector.body
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-8(%rbx,%rdi,8), %xmm4
	movupd	-8(%r9,%rdi,8), %xmm5
	movapd	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, -8(%rbx,%rdi,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, -8(%r9,%rdi,8)
	movupd	8(%rbx,%rdi,8), %xmm4
	movupd	8(%r9,%rdi,8), %xmm5
	movapd	%xmm2, %xmm6
	mulpd	%xmm4, %xmm6
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	subpd	%xmm7, %xmm6
	movupd	%xmm6, 8(%rbx,%rdi,8)
	mulpd	%xmm3, %xmm4
	mulpd	%xmm2, %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, 8(%r9,%rdi,8)
	addq	$4, %rdi
	cmpq	%rdi, %r11
	jne	.LBB1_36
.LBB1_37:                               # %middle.block
                                        #   in Loop: Header=BB1_24 Depth=3
	cmpq	%r8, %r13
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	je	.LBB1_41
# BB#38:                                #   in Loop: Header=BB1_24 Depth=3
	addq	%r8, %r14
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_36 Depth 4
                                        #         Child Loop BB1_40 Depth 4
                                        #         Child Loop BB1_42 Depth 4
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	360(%rsp), %r15         # 8-byte Reload
	imulq	%rbx, %r15
	addq	384(%rsp), %r15         # 8-byte Folded Reload
	movq	352(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, 240(%rsp)         # 8-byte Spill
	imulq	%rbx, %rdi
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi), %r12
	cmpq	$-52, %r12
	movq	$-51, %rcx
	cmovleq	%rcx, %r12
	shlq	$32, %r12
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	xorq	%rax, %r12
	sarq	$32, %r12
	cmpq	%r12, %r15
	cmovgeq	%r15, %r12
	movq	368(%rsp), %rdx         # 8-byte Reload
	movq	%rdi, 400(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdi), %rbx
	cmpq	$-52, %rbx
	cmovleq	%rcx, %rbx
	shlq	$32, %rbx
	xorq	%rax, %rbx
	sarq	$32, %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rax
	cmovgeq	%r15, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	-1(%rbp), %r14
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	-8(%rax,%rbp,8), %rax
	movq	%rbp, %r13
	leaq	(%r13,%rsi), %rbp
	movsd	-8(%rax,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movsd	(%rax,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	leal	-1(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leaq	56(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	Givens
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # xmm1 = mem[0],zero
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%r13,%rax), %rax
	cmpq	$51, %rax
	movl	$50, %ecx
	cmovgeq	%rcx, %rax
	cltq
	cmpq	%rax, %r14
	jle	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_24 Depth=3
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	240(%rsp), %rbx         # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_26:                               # %.lr.ph55.i76
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	%r13, %r9
	movq	400(%rsp), %rcx         # 8-byte Reload
	addq	344(%rsp), %rcx         # 8-byte Folded Reload
	movq	392(%rsp), %rsi         # 8-byte Reload
	addq	%rcx, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	-8(%rdx,%rbp,8), %rcx
	movq	(%rdx,%rbp,8), %rdx
	cmpq	$2, %rsi
	movq	%rsi, %r13
	jb	.LBB1_27
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	%r13, %r8
	andq	$-2, %r8
	je	.LBB1_27
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_24 Depth=3
	incq	%r12
	leaq	(%rcx,%r15,8), %rsi
	leaq	(%rdx,%r12,8), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB1_31
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_24 Depth=3
	leaq	(%rcx,%r12,8), %rsi
	leaq	(%rdx,%r15,8), %rdi
	cmpq	%rsi, %rdi
	jae	.LBB1_31
	.p2align	4, 0x90
.LBB1_27:                               #   in Loop: Header=BB1_24 Depth=3
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB1_39:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_24 Depth=3
	decq	%r14
	.p2align	4, 0x90
.LBB1_40:                               # %scalar.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	8(%rcx,%r14,8), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rdx,%r14,8), %xmm3   # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	movaps	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, 8(%rcx,%r14,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, 8(%rdx,%r14,8)
	incq	%r14
	cmpq	%rax, %r14
	jl	.LBB1_40
.LBB1_41:                               # %.lr.ph.i78.preheader
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	%r15, %rcx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB1_42:                               # %.lr.ph.i78
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx), %rdi
	movsd	-8(%rdi,%rbp,8), %xmm2  # xmm2 = mem[0],zero
	movsd	(%rdi,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm4
	movsd	%xmm4, -8(%rdi,%rbp,8)
	mulsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdi,%rbp,8)
	incq	%rdx
	addq	$8, %rcx
	cmpq	%rax, %rdx
	jl	.LBB1_42
# BB#43:                                # %ApplyGivens.exit83.loopexit
                                        #   in Loop: Header=BB1_24 Depth=3
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # xmm1 = mem[0],zero
	movq	240(%rsp), %rbx         # 8-byte Reload
	movq	%r10, %r14
.LBB1_44:                               # %ApplyGivens.exit83
                                        #   in Loop: Header=BB1_24 Depth=3
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %edx
	callq	ApplyRGivens
	incq	%rbx
	movq	224(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %r14
	addq	208(%rsp), %r12         # 8-byte Folded Reload
	addq	%rsi, %r13
	addq	200(%rsp), %r15         # 8-byte Folded Reload
	cmpq	232(%rsp), %rbp         # 8-byte Folded Reload
	jl	.LBB1_24
.LBB1_45:                               # %._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	incq	152(%rsp)               # 8-byte Folded Spill
	decq	136(%rsp)               # 8-byte Folded Spill
	incq	144(%rsp)               # 8-byte Folded Spill
	incq	192(%rsp)               # 8-byte Folded Spill
	incq	160(%rsp)               # 8-byte Folded Spill
	decq	168(%rsp)               # 8-byte Folded Spill
	incq	176(%rsp)               # 8-byte Folded Spill
	addq	$8, 184(%rsp)           # 8-byte Folded Spill
	cmpq	320(%rsp), %rdx         # 8-byte Folded Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	jne	.LBB1_4
.LBB1_46:                               # %._crit_edge87
                                        #   in Loop: Header=BB1_2 Depth=1
	decq	%rsi
	incl	68(%rsp)                # 4-byte Folded Spill
	incq	112(%rsp)               # 8-byte Folded Spill
	decq	96(%rsp)                # 8-byte Folded Spill
	incq	104(%rsp)               # 8-byte Folded Spill
	incq	208(%rsp)               # 8-byte Folded Spill
	decq	128(%rsp)               # 8-byte Folded Spill
	addq	$-8, 120(%rsp)          # 8-byte Folded Spill
	addq	$-8, 200(%rsp)          # 8-byte Folded Spill
	cmpq	$1, %rsi
	jg	.LBB1_2
.LBB1_47:                               # %._crit_edge90
	movq	80(%rsp), %rax          # 8-byte Reload
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Jacobi, .Lfunc_end1-Jacobi
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
