	.text
	.file	"smg2_setup_rap.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_1:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
.LCPI0_2:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_3:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI0_4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	hypre_SMG2CreateRAPOp
	.p2align	4, 0x90
	.type	hypre_SMG2CreateRAPOp,@function
hypre_SMG2CreateRAPOp:                  # @hypre_SMG2CreateRAPOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	.Lhypre_SMG2CreateRAPOp.RAP_num_ghost+16(%rip), %rax
	movq	%rax, 16(%rsp)
	movaps	.Lhypre_SMG2CreateRAPOp.RAP_num_ghost(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	cmpl	$0, 72(%r15)
	je	.LBB0_1
# BB#5:                                 # %.loopexit.loopexit7386
	movl	$5, %ebp
	movl	$5, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [4294967295,4294967295,0,0]
	movups	%xmm0, (%rax)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [4294967295,0,1,4294967295]
	movups	%xmm0, 16(%rax)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [0,4294967295,0,0]
	movups	%xmm0, 32(%rax)
	movq	$0, 48(%rax)
	movq	%rax, %rcx
	addq	$56, %rcx
	jmp	.LBB0_2
.LBB0_1:                                # %.preheader
	movl	$9, %ebp
	movl	$9, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [4294967295,4294967295,0,0]
	movups	%xmm0, (%rax)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [4294967295,0,1,4294967295]
	movups	%xmm0, 16(%rax)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movq	%rcx, 32(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rax)
	movl	$0, 56(%rax)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [1,0,0,4294967295]
	movups	%xmm0, 60(%rax)
	movl	$1, 76(%rax)
	movl	$0, 80(%rax)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [0,1,0,1]
	movups	%xmm0, 84(%rax)
	movl	$1, 100(%rax)
	movq	%rax, %rcx
	addq	$104, %rcx
.LBB0_2:                                # %.loopexit
	movl	$0, (%rcx)
	movl	$2, %edi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, %rbx
	movl	(%r15), %edi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	hypre_StructMatrixCreate
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	hypre_StructStencilDestroy
	movl	72(%r15), %eax
	movl	%eax, 72(%rbp)
	cmpl	$0, 72(%r15)
	je	.LBB0_4
# BB#3:
	movl	$0, 4(%rsp)
	movl	$0, 12(%rsp)
.LBB0_4:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	hypre_StructMatrixSetNumGhost
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMG2CreateRAPOp, .Lfunc_end0-hypre_SMG2CreateRAPOp
	.cfi_endproc

	.globl	hypre_SMG2BuildRAPSym
	.p2align	4, 0x90
	.type	hypre_SMG2BuildRAPSym,@function
hypre_SMG2BuildRAPSym:                  # @hypre_SMG2BuildRAPSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$648, %rsp              # imm = 0x288
.Lcfi15:
	.cfi_def_cfa_offset 704
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%r8, 640(%rsp)          # 8-byte Spill
	movq	%rdx, 632(%rsp)         # 8-byte Spill
	movq	%rsi, 624(%rsp)         # 8-byte Spill
	movq	%rdi, 264(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB1_33
# BB#1:                                 # %.preheader1117.lr.ph
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	8(%rdx), %rdi
	movq	24(%rdx), %rdx
	movl	8(%rdx), %r13d
	movq	16(%rdi), %rdx
	movq	16(%rax), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 408(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 400(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movq	%r9, 496(%rsp)          # 8-byte Spill
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	movq	%rsi, 616(%rsp)         # 8-byte Spill
	movl	%r13d, 484(%rsp)        # 4-byte Spill
	movq	%rdx, 608(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader1117
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #     Child Loop BB1_23 Depth 2
                                        #       Child Loop BB1_26 Depth 3
                                        #         Child Loop BB1_28 Depth 4
                                        #     Child Loop BB1_10 Depth 2
                                        #       Child Loop BB1_15 Depth 3
                                        #         Child Loop BB1_16 Depth 4
	movq	600(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	movslq	272(%rsp), %rcx         # 4-byte Folded Reload
	leaq	-1(%rcx), %rbx
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rbp
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$24, %rbp
	cmpl	%eax, 4(%rdx,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB1_3
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rsi), %rcx
	leaq	(%r15,%r15,2), %rax
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax,8), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	640(%rsp), %rsi         # 8-byte Reload
	movq	%r9, %rdx
	leaq	588(%rsp), %rcx
	callq	hypre_StructMapCoarseToFine
	movq	264(%rsp), %r14         # 8-byte Reload
	movq	40(%r14), %rax
	movq	(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	624(%rsp), %r12         # 8-byte Reload
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r15, 504(%rsp)         # 8-byte Spill
	movq	632(%rsp), %r13         # 8-byte Reload
	movq	40(%r13), %rax
	movq	(%rax), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	488(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	leaq	4(%rsp), %r15
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %r12       # imm = 0x100000000
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 560(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 552(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	484(%rsp), %r13d        # 4-byte Reload
	cmpl	$6, %r13d
	jl	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	264(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	movq	272(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 392(%rsp)         # 8-byte Spill
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	488(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	504(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 536(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 512(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	-12(%rcx,%rbp), %eax
	movl	-24(%rcx,%rbp), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	-12(%rbx,%rbp), %eax
	movl	-24(%rbx,%rbp), %ecx
	movl	$0, %esi
	cmovsl	%esi, %edx
	movl	%edx, 128(%rsp)         # 4-byte Spill
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	cmovsl	%esi, %edx
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movq	$1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	576(%rsp), %rsi
	callq	hypre_BoxGetSize
	leaq	(,%r14,8), %rax
	leaq	(%rax,%rax,2), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi,%rdi), %edx
	movl	%edx, 192(%rsp)         # 4-byte Spill
	movl	4(%rsi,%rdi), %r8d
	movl	12(%rsi,%rdi), %ecx
	movl	%ecx, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %ecx
	movl	16(%rsi,%rdi), %ecx
	movq	%rdi, %r9
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movl	$0, %edi
	cmovsl	%edi, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	movl	%r8d, 184(%rsp)         # 4-byte Spill
	subl	%r8d, %eax
	incl	%eax
	cmpl	%r8d, %ecx
	movl	-24(%rbx,%rbp), %edx
	movl	%edx, 168(%rsp)         # 4-byte Spill
	movl	-12(%rbx,%rbp), %ecx
	cmovsl	%edi, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %ecx
	movl	-20(%rbx,%rbp), %edx
	cmovsl	%edi, %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	movl	-8(%rbx,%rbp), %ecx
	movq	%rbx, %rsi
	movl	%ecx, %r12d
	movl	%edx, 160(%rsp)         # 4-byte Spill
	subl	%edx, %r12d
	incl	%r12d
	cmpl	%edx, %ecx
	cmovsl	%edi, %r12d
	movl	$0, %r8d
	movq	200(%rsp), %r14         # 8-byte Reload
	movl	-24(%r14,%rbp), %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movl	-12(%r14,%rbp), %edx
	movl	%edx, %r15d
	subl	%eax, %r15d
	incl	%r15d
	cmpl	%eax, %edx
	cmovsl	%r8d, %r15d
	movl	-20(%r14,%rbp), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	-8(%r14,%rbp), %edi
	movl	%edi, %r11d
	subl	%eax, %r11d
	incl	%r11d
	cmpl	%eax, %edi
	cmovsl	%r8d, %r11d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	-24(%rdi,%rbp), %edx
	movl	-12(%rdi,%rbp), %ecx
	movl	%ecx, %r10d
	subl	%edx, %r10d
	incl	%r10d
	cmpl	%edx, %ecx
	cmovsl	%r8d, %r10d
	movl	-20(%rdi,%rbp), %ecx
	movl	-8(%rdi,%rbp), %eax
	movl	%eax, %ebx
	subl	%ecx, %ebx
	incl	%ebx
	cmpl	%ecx, %eax
	cmovsl	%r8d, %ebx
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r9), %eax
	movl	%eax, %r9d
	subl	-16(%rdi,%rbp), %r9d
	movl	%eax, %r8d
	subl	-16(%r14,%rbp), %r8d
	movl	596(%rsp), %edi
	subl	-16(%rsi,%rbp), %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %esi
	movl	%esi, %ebp
	subl	%edx, %ebp
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	subl	8(%rbp,%rdx), %eax
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	4(%rbp,%rdx), %ebp
	movl	%eax, %r14d
	movl	%ebp, %eax
	subl	%ecx, %eax
	imull	%ebx, %r9d
	addl	%eax, %r9d
	movl	%esi, %eax
	subl	152(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%ebp, %ecx
	subl	144(%rsp), %ecx         # 4-byte Folded Reload
	imull	%r11d, %r8d
	addl	%ecx, %r8d
	movl	588(%rsp), %eax
	subl	168(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	592(%rsp), %edx
	subl	160(%rsp), %edx         # 4-byte Folded Reload
	imull	%r12d, %edi
	addl	%edx, %edi
	subl	192(%rsp), %esi         # 4-byte Folded Reload
	movl	%esi, 40(%rsp)          # 4-byte Spill
	subl	184(%rsp), %ebp         # 4-byte Folded Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	imull	%edx, %r14d
	addl	%ebp, %r14d
	movl	176(%rsp), %eax         # 4-byte Reload
	imull	%eax, %edi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	496(%rsp), %rdi         # 8-byte Reload
	movl	4(%rdi), %ecx
	imull	%eax, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	imull	%eax, %r12d
	movl	%r8d, %ecx
	movq	%r9, %rsi
	imull	%r10d, %esi
	imull	%r15d, %ecx
	movl	88(%rsp), %eax          # 4-byte Reload
	imull	%eax, %r14d
	movl	%r10d, 28(%rsp)         # 4-byte Spill
	imull	%r10d, %ebx
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	imull	%r15d, %r11d
	movl	%r11d, 60(%rsp)         # 4-byte Spill
	imull	8(%rdi), %r12d
	movl	%r12d, 64(%rsp)         # 4-byte Spill
	imull	%eax, %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	576(%rsp), %r10d
	movl	580(%rsp), %ebp
	cmpl	%r10d, %ebp
	movl	%r10d, %edx
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	cmovgel	%ebp, %edx
	movl	584(%rsp), %eax
	cmpl	%edx, %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	cmovgel	%eax, %edx
	cmpl	$5, %r13d
	movslq	(%rdi), %rax
	jne	.LBB1_20
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	testl	%edx, %edx
	movq	112(%rsp), %r11         # 8-byte Reload
	jle	.LBB1_32
# BB#8:                                 # %.lr.ph1175
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_32
# BB#9:                                 # %.preheader1114.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	28(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %edx
	subl	%r10d, %edx
	movl	24(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %ebx
	subl	%r10d, %ebx
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	%eax, %r12d
	imull	%r10d, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	subl	%r12d, %eax
	movl	88(%rsp), %r15d         # 4-byte Reload
	subl	%r10d, %r15d
	movq	104(%rsp), %r8          # 8-byte Reload
	movl	%r8d, %edi
	imull	%r9d, %edi
	movq	%rdi, 336(%rsp)         # 8-byte Spill
	subl	%edi, 56(%rsp)          # 4-byte Folded Spill
	movl	%r8d, %edi
	imull	%ebp, %edi
	movl	%edi, 328(%rsp)         # 4-byte Spill
	subl	%edi, 60(%rsp)          # 4-byte Folded Spill
	movl	%r8d, %edi
	movl	20(%rsp), %ebp          # 4-byte Reload
	imull	%ebp, %edi
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	subl	%edi, 64(%rsp)          # 4-byte Folded Spill
	leal	-1(%r8), %edi
	imull	%edi, %eax
	addl	%ebp, %eax
	subl	%r12d, %eax
	movl	%eax, 288(%rsp)         # 4-byte Spill
	movl	%r8d, %eax
	movl	88(%rsp), %ebp          # 4-byte Reload
	imull	%ebp, %eax
	movl	%eax, 312(%rsp)         # 4-byte Spill
	subl	%eax, 16(%rsp)          # 4-byte Folded Spill
	imull	%edi, %edx
	addl	%r9d, %edx
	subl	%r10d, %edx
	movl	%edx, 304(%rsp)         # 4-byte Spill
	imull	%edi, %ebx
	addl	24(%rsp), %ebx          # 4-byte Folded Reload
	subl	%r10d, %ebx
	movl	%ebx, 296(%rsp)         # 4-byte Spill
	imull	%edi, %r15d
	addl	%ebp, %r15d
	subl	%r10d, %r15d
	movl	%r15d, 280(%rsp)        # 4-byte Spill
	movslq	120(%rsp), %rdx         # 4-byte Folded Reload
	movslq	128(%rsp), %rdi         # 4-byte Folded Reload
	addl	40(%rsp), %r14d         # 4-byte Folded Reload
	addl	72(%rsp), %r11d         # 4-byte Folded Reload
	addl	32(%rsp), %ecx          # 4-byte Folded Reload
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	shlq	$3, %rdi
	movl	$8, %eax
	subq	%rdi, %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	shlq	$3, 216(%rsp)           # 8-byte Folded Spill
	leaq	(,%rdx,8), %rax
	movq	248(%rsp), %r8          # 8-byte Reload
	movq	%r8, %rdi
	subq	%rax, %rdi
	movq	%rdi, 440(%rsp)         # 8-byte Spill
	movq	256(%rsp), %r9          # 8-byte Reload
	movq	%r9, %rdi
	subq	%rax, %rdi
	movq	%rdi, 432(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, %rbx
	subq	%rax, %rbx
	movq	%rbx, 424(%rsp)         # 8-byte Spill
	subq	%rax, 240(%rsp)         # 8-byte Folded Spill
	movq	224(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	%rbx, 416(%rsp)         # 8-byte Spill
	leaq	(%r8,%rdx,8), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	(%r9,%rdx,8), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdx,8), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r10, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader1114
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_15 Depth 3
                                        #         Child Loop BB1_16 Depth 4
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_11
# BB#12:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB1_10 Depth=2
	testl	%r10d, %r10d
	jle	.LBB1_13
# BB#14:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB1_10 Depth=2
	movl	%eax, 360(%rsp)         # 4-byte Spill
	movq	336(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	movl	%eax, 352(%rsp)         # 4-byte Spill
	movq	320(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r11), %eax
	movl	%eax, 344(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%r14d, 100(%rsp)        # 4-byte Spill
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader.us
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_16 Depth 4
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movl	%ecx, 472(%rsp)         # 4-byte Spill
	movslq	%ecx, %rax
	movq	552(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbx
	movq	560(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdi
	movq	%rsi, 456(%rsp)         # 8-byte Spill
	movslq	%esi, %rax
	movq	568(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	448(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movl	%r14d, 120(%rsp)        # 4-byte Spill
	movslq	%r14d, %rax
	movq	%r11, 112(%rsp)         # 8-byte Spill
	movslq	%r11d, %rsi
	movq	520(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	528(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	512(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	536(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	544(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r12
	movq	248(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	384(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r13
	movq	432(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r8
	movq	424(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	416(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r9
	movq	464(%rsp), %rax         # 8-byte Reload
	leaq	(%rdx,%rax), %r14
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	80(%rsp), %r11          # 8-byte Reload
	movq	216(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_10 Depth=2
                                        #       Parent Loop BB1_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rax), %xmm0
	mulsd	-16(%r14,%rsi,8), %xmm0
	movq	48(%rsp), %r10          # 8-byte Reload
	movsd	%xmm0, (%r10,%rsi,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r8,%rax), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r14,%rsi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	144(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rax), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%r12,%rax), %xmm2
	addsd	%xmm0, %xmm2
	movq	40(%rsp), %rbp          # 8-byte Reload
	movsd	%xmm2, (%rbp,%rsi,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movq	136(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rax), %xmm0
	mulsd	(%r14,%rsi,8), %xmm0
	movq	32(%rsp), %rbp          # 8-byte Reload
	movsd	%xmm0, (%rbp,%rsi,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rax), %xmm0
	mulsd	-8(%rcx,%rsi,8), %xmm0
	movq	192(%rsp), %rbp         # 8-byte Reload
	addsd	(%rbp,%rax), %xmm0
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movq	168(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rax), %xmm1
	mulsd	-8(%rdx,%rsi,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	72(%rsp), %rbp          # 8-byte Reload
	movsd	%xmm1, (%rbp,%rsi,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r8,%rax), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rcx,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	184(%rsp), %rbp         # 8-byte Reload
	addsd	(%rbp,%rax), %xmm1
	movsd	(%rdi,%rsi,8), %xmm3    # xmm3 = mem[0],zero
	movq	160(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rax), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%rdx,%rsi,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r9,%rax), %xmm0
	addsd	%xmm4, %xmm0
	movq	152(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rax), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%r12,%rax), %xmm2
	addsd	%xmm3, %xmm2
	movq	176(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rax), %xmm5
	addsd	%xmm2, %xmm5
	movq	200(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm5, (%rbp,%rsi,8)
	incq	%rsi
	addq	%r15, %rax
	cmpl	%esi, %r11d
	jne	.LBB1_16
# BB#17:                                # %._crit_edge1152.us
                                        #   in Loop: Header=BB1_15 Depth=3
	movl	128(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	472(%rsp), %ecx         # 4-byte Reload
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	addl	28(%rsp), %esi          # 4-byte Folded Reload
	movl	120(%rsp), %r14d        # 4-byte Reload
	addl	88(%rsp), %r14d         # 4-byte Folded Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	addl	20(%rsp), %r11d         # 4-byte Folded Reload
	cmpl	104(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB1_15
# BB#18:                                # %._crit_edge1162.loopexit
                                        #   in Loop: Header=BB1_10 Depth=2
	movl	96(%rsp), %ecx          # 4-byte Reload
	addl	328(%rsp), %ecx         # 4-byte Folded Reload
	movl	100(%rsp), %r14d        # 4-byte Reload
	addl	312(%rsp), %r14d        # 4-byte Folded Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	movl	360(%rsp), %eax         # 4-byte Reload
	movl	352(%rsp), %edx         # 4-byte Reload
	movl	344(%rsp), %esi         # 4-byte Reload
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader.preheader
                                        #   in Loop: Header=BB1_10 Depth=2
	addl	304(%rsp), %esi         # 4-byte Folded Reload
	addl	288(%rsp), %r11d        # 4-byte Folded Reload
	addl	296(%rsp), %ecx         # 4-byte Folded Reload
	addl	280(%rsp), %r14d        # 4-byte Folded Reload
.LBB1_11:                               #   in Loop: Header=BB1_10 Depth=2
	movl	%esi, %edx
	movl	%r11d, %esi
.LBB1_19:                               # %._crit_edge1162
                                        #   in Loop: Header=BB1_10 Depth=2
	addl	56(%rsp), %edx          # 4-byte Folded Reload
	addl	60(%rsp), %ecx          # 4-byte Folded Reload
	addl	64(%rsp), %esi          # 4-byte Folded Reload
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
	incl	%eax
	cmpl	68(%rsp), %eax          # 4-byte Folded Reload
	movl	%esi, %r11d
	movl	%edx, %esi
	jne	.LBB1_10
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %r8
	testl	%edx, %edx
	movq	112(%rsp), %rbx         # 8-byte Reload
	jle	.LBB1_32
# BB#21:                                # %.lr.ph1145
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_32
# BB#22:                                # %.preheader1115.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rax
	movl	28(%rsp), %r9d          # 4-byte Reload
	imull	%r9d, %edx
	subl	%edx, 56(%rsp)          # 4-byte Folded Spill
	movl	%eax, %edx
	movl	24(%rsp), %edi          # 4-byte Reload
	imull	%edi, %edx
	subl	%edx, 60(%rsp)          # 4-byte Folded Spill
	movl	%eax, %edx
	movl	20(%rsp), %ebp          # 4-byte Reload
	imull	%ebp, %edx
	subl	%edx, 64(%rsp)          # 4-byte Folded Spill
	movl	%eax, %edx
	movl	88(%rsp), %eax          # 4-byte Reload
	imull	%eax, %edx
	subl	%edx, 16(%rsp)          # 4-byte Folded Spill
	movl	%edi, %edx
	subl	%r10d, %edx
	movl	%edx, 424(%rsp)         # 4-byte Spill
	movq	%r8, %r11
	movl	%r11d, %edx
	imull	%r10d, %edx
	subl	%edx, %ebp
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movslq	120(%rsp), %r8          # 4-byte Folded Reload
	movslq	128(%rsp), %rdx         # 4-byte Folded Reload
	leal	-1(%r10), %ebp
	incq	%rbp
	movq	%rbp, 384(%rsp)         # 8-byte Spill
	imulq	%r11, %rbp
	movq	%rbp, 376(%rsp)         # 8-byte Spill
	addl	40(%rsp), %r14d         # 4-byte Folded Reload
	addl	72(%rsp), %ebx          # 4-byte Folded Reload
	addl	32(%rsp), %ecx          # 4-byte Folded Reload
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	movl	$0, 212(%rsp)           # 4-byte Folded Spill
	testl	%r10d, %r10d
	movl	$0, %ebp
	cmovgl	%r10d, %ebp
	addl	%ebp, %edi
	subl	%r10d, %edi
	movl	%edi, 24(%rsp)          # 4-byte Spill
	shlq	$3, %rdx
	movq	448(%rsp), %rdi         # 8-byte Reload
	subq	%rdx, %rdi
	addq	$8, %rdi
	movq	%rdi, 368(%rsp)         # 8-byte Spill
	addl	%ebp, %r9d
	subl	%r10d, %r9d
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	subl	%r10d, %ebp
	movl	%ebp, 416(%rsp)         # 4-byte Spill
	leaq	(,%r8,8), %rax
	movq	248(%rsp), %r9          # 8-byte Reload
	movq	%r9, %rdx
	subq	%rax, %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	shlq	$3, %r11
	movq	%r11, 216(%rsp)         # 8-byte Spill
	movq	408(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rdx
	subq	%rax, %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rbp
	subq	%rax, %rbp
	movq	%rbp, 344(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	subq	%rax, %rdi
	movq	%rdi, 336(%rsp)         # 8-byte Spill
	subq	%rax, 240(%rsp)         # 8-byte Folded Spill
	movq	400(%rsp), %rdi         # 8-byte Reload
	subq	%rax, %rdi
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	movq	392(%rsp), %rdi         # 8-byte Reload
	subq	%rax, %rdi
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rdi         # 8-byte Reload
	subq	%rax, %rdi
	movq	%rdi, 312(%rsp)         # 8-byte Spill
	leaq	(%r9,%r8,8), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leaq	(%r11,%r8,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leaq	(%rdx,%r8,8), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r8,8), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	%r10, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_23:                               # %.preheader1115
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_26 Depth 3
                                        #         Child Loop BB1_28 Depth 4
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_24
# BB#25:                                # %.preheader1113.preheader
                                        #   in Loop: Header=BB1_23 Depth=2
	xorl	%eax, %eax
	movl	%ecx, %edx
	.p2align	4, 0x90
.LBB1_26:                               # %.preheader1113
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_23 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_28 Depth 4
	testl	%r10d, %r10d
	jle	.LBB1_30
# BB#27:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_26 Depth=3
	movl	%eax, 432(%rsp)         # 4-byte Spill
	movl	%edx, 440(%rsp)         # 4-byte Spill
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movslq	%ecx, %rax
	movq	552(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdx
	movq	560(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r9
	movq	%rsi, 456(%rsp)         # 8-byte Spill
	movslq	%esi, %rax
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	movq	568(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r13
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r11
	movl	%r14d, 100(%rsp)        # 4-byte Spill
	movslq	%r14d, %r12
	shlq	$3, %r12
	movslq	%ebx, %rsi
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	520(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	528(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	512(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	536(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addq	544(%rsp), %r12         # 8-byte Folded Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r8
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdi
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	320(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	408(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	232(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rcx
	movq	400(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	392(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	304(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %rbx
	movq	%rbx, 472(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	288(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbp
	movq	%rbp, 464(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rsi
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	movq	216(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_23 Depth=2
                                        #       Parent Loop BB1_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r8,%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-16(%r15,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	200(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%rax,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	48(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm2, (%rbx,%rbp,8)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r15,%rbp,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	192(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%rcx,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	40(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm2, (%rbx,%rbp,8)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movq	184(%rsp), %rbx         # 8-byte Reload
	movsd	(%rbx,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r15,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	176(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm0
	addsd	%xmm1, %xmm0
	movq	152(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	32(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm2, (%rbx,%rbp,8)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r8,%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r13,%rbp,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	144(%rsp), %rbx         # 8-byte Reload
	addsd	(%rbx,%r14), %xmm1
	movsd	(%r9,%rbp,8), %xmm3     # xmm3 = mem[0],zero
	movq	472(%rsp), %rbx         # 8-byte Reload
	movsd	(%rbx,%r14), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	-8(%r11,%rbp,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	168(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm0
	addsd	%xmm4, %xmm0
	movq	88(%rsp), %rbx          # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%rax,%r14), %xmm2
	addsd	%xmm3, %xmm2
	movq	136(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm5
	addsd	%xmm2, %xmm5
	movq	72(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm5, (%rbx,%rbp,8)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r13,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	128(%rsp), %rbx         # 8-byte Reload
	addsd	(%rbx,%r14), %xmm1
	movsd	(%r9,%rbp,8), %xmm3     # xmm3 = mem[0],zero
	movq	464(%rsp), %rbx         # 8-byte Reload
	movsd	(%rbx,%r14), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%r11,%rbp,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	160(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm0
	addsd	%xmm4, %xmm0
	mulsd	(%rsi,%r14), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%rcx,%r14), %xmm2
	addsd	%xmm3, %xmm2
	movq	120(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r14), %xmm5
	addsd	%xmm2, %xmm5
	movsd	%xmm5, (%r12,%rbp,8)
	incq	%rbp
	addq	%r10, %r14
	cmpl	%ebp, 80(%rsp)          # 4-byte Folded Reload
	jne	.LBB1_28
# BB#29:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB1_26 Depth=3
	movl	440(%rsp), %edx         # 4-byte Reload
	addl	384(%rsp), %edx         # 4-byte Folded Reload
	movl	100(%rsp), %r14d        # 4-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movl	96(%rsp), %ecx          # 4-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	movl	432(%rsp), %eax         # 4-byte Reload
.LBB1_30:                               # %._crit_edge
                                        #   in Loop: Header=BB1_26 Depth=3
	addl	28(%rsp), %esi          # 4-byte Folded Reload
	addl	424(%rsp), %edx         # 4-byte Folded Reload
	addl	20(%rsp), %ebx          # 4-byte Folded Reload
	addl	416(%rsp), %r14d        # 4-byte Folded Reload
	incl	%eax
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	104(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB1_26
	jmp	.LBB1_31
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_23 Depth=2
	movl	%ecx, %edx
.LBB1_31:                               # %._crit_edge1132
                                        #   in Loop: Header=BB1_23 Depth=2
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	addl	60(%rsp), %edx          # 4-byte Folded Reload
	addl	64(%rsp), %ebx          # 4-byte Folded Reload
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
	movl	212(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 212(%rsp)         # 4-byte Spill
	cmpl	68(%rsp), %eax          # 4-byte Folded Reload
	movl	%edx, %ecx
	jne	.LBB1_23
	.p2align	4, 0x90
.LBB1_32:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	504(%rsp), %r15         # 8-byte Reload
	incq	%r15
	movq	616(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %r15
	movq	496(%rsp), %r9          # 8-byte Reload
	movq	608(%rsp), %rdx         # 8-byte Reload
	jl	.LBB1_2
.LBB1_33:                               # %._crit_edge1185
	xorl	%eax, %eax
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SMG2BuildRAPSym, .Lfunc_end1-hypre_SMG2BuildRAPSym
	.cfi_endproc

	.globl	hypre_SMG2BuildRAPNoSym
	.p2align	4, 0x90
	.type	hypre_SMG2BuildRAPNoSym,@function
hypre_SMG2BuildRAPNoSym:                # @hypre_SMG2BuildRAPNoSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi28:
	.cfi_def_cfa_offset 656
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, 576(%rsp)          # 8-byte Spill
	movq	%rdx, 568(%rsp)         # 8-byte Spill
	movq	%rsi, 560(%rsp)         # 8-byte Spill
	movq	%rdi, 296(%rsp)         # 8-byte Spill
	movq	%rcx, 448(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB2_34
# BB#1:                                 # %.preheader1045.lr.ph
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	8(%rdx), %rcx
	movq	24(%rdx), %rdx
	movl	8(%rdx), %edx
	movl	%edx, 268(%rsp)         # 4-byte Spill
	movq	16(%rcx), %rdx
	movq	16(%rax), %rax
	movq	%rax, 536(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 304(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 400(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 392(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	%rbp, 440(%rsp)         # 8-byte Spill
	movq	%rsi, 552(%rsp)         # 8-byte Spill
	movq	%rdx, 544(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader1045
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
                                        #     Child Loop BB2_23 Depth 2
                                        #       Child Loop BB2_28 Depth 3
                                        #         Child Loop BB2_29 Depth 4
                                        #     Child Loop BB2_10 Depth 2
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_16 Depth 4
	movq	536(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbx,4), %eax
	movslq	304(%rsp), %rcx         # 4-byte Folded Reload
	leaq	-1(%rcx), %r12
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %r14
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$24, %r14
	cmpl	%eax, 4(%rdx,%r12,4)
	leaq	1(%r12), %r12
	jne	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsi), %rcx
	leaq	(%rbx,%rbx,2), %rax
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax,8), %rdi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	576(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, %rdx
	leaq	524(%rsp), %rcx
	movq	%rbx, 456(%rsp)         # 8-byte Spill
	callq	hypre_StructMapCoarseToFine
	movq	296(%rsp), %r13         # 8-byte Reload
	movq	40(%r13), %rax
	movq	(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	560(%rsp), %rbx         # 8-byte Reload
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	568(%rsp), %rbp         # 8-byte Reload
	movq	40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movl	%r12d, %esi
	leaq	12(%rsp), %r15
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	movq	%rbx, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 496(%rsp)         # 8-byte Spill
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	%rbx, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r13, %rdi
	movq	%r15, %r13
	movq	%r12, %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movl	%r12d, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 272(%rsp)         # 8-byte Spill
	cmpl	$6, 268(%rsp)           # 4-byte Folded Reload
	jl	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	296(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	304(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 384(%rsp)         # 8-byte Spill
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	448(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	movq	456(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, %r15
	movq	%r15, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	%r15, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	248(%rsp), %r12         # 8-byte Reload
	movl	-12(%r12,%r14), %eax
	movl	-24(%r12,%r14), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	-12(%r15,%r14), %eax
	movl	-24(%r15,%r14), %ecx
	movl	$0, %esi
	cmovsl	%esi, %edx
	movl	%edx, 144(%rsp)         # 4-byte Spill
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	cmovsl	%esi, %edx
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	512(%rsp), %rsi
	callq	hypre_BoxGetSize
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rax), %esi
	movl	%esi, 240(%rsp)         # 4-byte Spill
	movl	4(%rbp,%rax), %edi
	movl	12(%rbp,%rax), %ecx
	movl	%ecx, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %ecx
	movl	16(%rbp,%rax), %ecx
	movl	$0, %ebp
	cmovsl	%ebp, %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
	movl	%edi, 232(%rsp)         # 4-byte Spill
	subl	%edi, %edx
	incl	%edx
	cmpl	%edi, %ecx
	movl	-24(%r15,%r14), %esi
	movl	%esi, 216(%rsp)         # 4-byte Spill
	movl	-12(%r15,%r14), %ecx
	cmovsl	%ebp, %edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %ecx
	movl	-20(%r15,%r14), %esi
	cmovsl	%ebp, %edx
	movl	$0, %ebx
	movl	%edx, 224(%rsp)         # 4-byte Spill
	movl	-8(%r15,%r14), %ecx
	movl	%ecx, %ebp
	movl	%esi, 208(%rsp)         # 4-byte Spill
	subl	%esi, %ebp
	incl	%ebp
	cmpl	%esi, %ecx
	cmovsl	%ebx, %ebp
	movq	256(%rsp), %r13         # 8-byte Reload
	movl	-24(%r13,%r14), %edx
	movl	%edx, 200(%rsp)         # 4-byte Spill
	movl	-12(%r13,%r14), %ecx
	movl	%ecx, %r9d
	subl	%edx, %r9d
	incl	%r9d
	cmpl	%edx, %ecx
	cmovsl	%ebx, %r9d
	movl	-20(%r13,%r14), %edx
	movl	%edx, 192(%rsp)         # 4-byte Spill
	movl	-8(%r13,%r14), %ecx
	movl	%ecx, %r10d
	subl	%edx, %r10d
	incl	%r10d
	cmpl	%edx, %ecx
	cmovsl	%ebx, %r10d
	movq	%r12, %rdi
	movl	-24(%rdi,%r14), %edx
	movl	-12(%rdi,%r14), %ecx
	movl	%ecx, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %ecx
	cmovsl	%ebx, %r8d
	movl	-20(%rdi,%r14), %r12d
	movl	-8(%rdi,%r14), %esi
	movq	%rdi, %rcx
	movl	%esi, %edi
	subl	%r12d, %edi
	incl	%edi
	cmpl	%r12d, %esi
	cmovsl	%ebx, %edi
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	8(%rsi,%rax), %ebx
	movl	%ebx, %r11d
	subl	-16(%rcx,%r14), %r11d
	movl	%ebx, %esi
	subl	-16(%r13,%r14), %esi
	movl	532(%rsp), %r13d
	subl	-16(%r15,%r14), %r13d
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %r14d
	movl	%r14d, %ecx
	subl	%edx, %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	subl	8(%rcx,%rax), %ebx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%rax), %edx
	movl	%edx, %eax
	subl	%r12d, %eax
	imull	%edi, %r11d
	addl	%eax, %r11d
	movl	%r14d, %eax
	subl	200(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%r10d, %r15d
	movl	%edx, %eax
	subl	192(%rsp), %eax         # 4-byte Folded Reload
	imull	%r15d, %esi
	addl	%eax, %esi
	movl	524(%rsp), %eax
	subl	216(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	528(%rsp), %ecx
	subl	208(%rsp), %ecx         # 4-byte Folded Reload
	imull	%ebp, %r13d
	addl	%ecx, %r13d
	subl	240(%rsp), %r14d        # 4-byte Folded Reload
	movl	%r14d, 64(%rsp)         # 4-byte Spill
	movl	%ebp, %r10d
	movl	%edi, %r12d
	subl	232(%rsp), %edx         # 4-byte Folded Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	imull	%r14d, %ebx
	addl	%edx, %ebx
	movl	224(%rsp), %eax         # 4-byte Reload
	imull	%eax, %r13d
	movq	%r13, 184(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rbp         # 8-byte Reload
	movl	4(%rbp), %ecx
	imull	%eax, %ecx
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	imull	%eax, %r10d
	imull	%r8d, %r11d
	imull	%r9d, %esi
	movl	28(%rsp), %eax          # 4-byte Reload
	imull	%eax, %ebx
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	imull	%r8d, %r12d
	movl	%r9d, 52(%rsp)          # 4-byte Spill
	imull	%r9d, %r15d
	imull	8(%rbp), %r10d
	imull	%eax, %r14d
	movl	512(%rsp), %edx
	movl	516(%rsp), %eax
	cmpl	%edx, %eax
	movl	%edx, %ecx
	movq	%rax, 176(%rsp)         # 8-byte Spill
	cmovgel	%eax, %ecx
	movl	520(%rsp), %eax
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	cmpl	$5, 268(%rsp)           # 4-byte Folded Reload
	movslq	(%rbp), %r8
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%eax, 88(%rsp)          # 4-byte Spill
	jne	.LBB2_20
# BB#7:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%ecx, %ecx
	jle	.LBB2_33
# BB#8:                                 # %.lr.ph1103
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	jle	.LBB2_33
# BB#9:                                 # %.preheader1042.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	movl	%ecx, %edi
	subl	%edx, %eax
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	52(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	subl	%edx, %esi
	movl	%r8d, %r13d
	imull	%edx, %r13d
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	92(%rsp), %r8d          # 4-byte Reload
	movl	%r8d, %r9d
	subl	%r13d, %r9d
	movq	%r11, 56(%rsp)          # 8-byte Spill
	movl	28(%rsp), %r11d         # 4-byte Reload
	subl	32(%rsp), %r11d         # 4-byte Folded Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %edx
	imull	%edi, %edx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	subl	%edx, %r12d
	leal	-1(%rcx), %edx
	imull	%edx, %r9d
	addl	%r8d, %r9d
	subl	%r13d, %r9d
	movl	%r9d, 312(%rsp)         # 4-byte Spill
	movl	%ecx, %edi
	imull	%ebp, %edi
	movl	%edi, 352(%rsp)         # 4-byte Spill
	subl	%edi, %r15d
	imull	%edx, %eax
	imull	%edx, %esi
	imull	%edx, %r11d
	movl	%ecx, %edx
	imull	%r8d, %edx
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	subl	%edx, %r10d
	addl	64(%rsp), %ebx          # 4-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	28(%rsp), %edi          # 4-byte Reload
	imull	%edi, %ecx
	movl	%ecx, 336(%rsp)         # 4-byte Spill
	subl	%ecx, %r14d
	addl	48(%rsp), %eax          # 4-byte Folded Reload
	subl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 328(%rsp)         # 4-byte Spill
	addl	%ebp, %esi
	movq	184(%rsp), %rbp         # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	subl	32(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, 320(%rsp)         # 4-byte Spill
	addl	%edi, %r11d
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %r11d
	movl	%r11d, 116(%rsp)        # 4-byte Spill
	movslq	136(%rsp), %r11         # 4-byte Folded Reload
	movslq	144(%rsp), %r8          # 4-byte Folded Reload
	addl	96(%rsp), %ebp          # 4-byte Folded Reload
	addl	80(%rsp), %edx          # 4-byte Folded Reload
	addl	72(%rsp), %esi          # 4-byte Folded Reload
	movq	168(%rsp), %r9          # 8-byte Reload
	leaq	8(%r9,%r8,8), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addq	$8, %r9
	movq	%r9, 168(%rsp)          # 8-byte Spill
	addq	$8, 288(%rsp)           # 8-byte Folded Spill
	shlq	$3, %r13
	movq	280(%rsp), %r8          # 8-byte Reload
	leaq	(%r8,%r11,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	(,%r11,8), %rax
	subq	%rax, %r8
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movl	88(%rsp), %eax          # 4-byte Reload
	xorl	%edi, %edi
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	movl	%r10d, 128(%rsp)        # 4-byte Spill
	movl	%r15d, 124(%rsp)        # 4-byte Spill
	movl	%r12d, 120(%rsp)        # 4-byte Spill
	movq	%r13, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader1042
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_16 Depth 4
	cmpl	$0, 176(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_11
# BB#12:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB2_10 Depth=2
	testl	%ecx, %ecx
	jle	.LBB2_13
# BB#14:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB2_10 Depth=2
	movl	%edi, 136(%rsp)         # 4-byte Spill
	movq	360(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	movl	%eax, 376(%rsp)         # 4-byte Spill
	movq	344(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbp), %eax
	movl	%eax, 368(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 132(%rsp)         # 4-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader.us
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_16 Depth 4
	movl	%eax, 232(%rsp)         # 4-byte Spill
	movl	%ecx, 216(%rsp)         # 4-byte Spill
	movslq	%ecx, %rcx
	movq	504(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	496(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r10
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movslq	%esi, %rcx
	movq	208(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r12
	movq	168(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	288(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ebx, 224(%rsp)         # 4-byte Spill
	movslq	%ebx, %rcx
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movslq	%ebp, %r8
	movq	472(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	464(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movq	488(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8,8), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,8), %r9
	movq	200(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,8), %rbp
	movq	192(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,8), %rdx
	movq	160(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r8,8), %rdi
	movq	152(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%r8,8), %rbx
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8,8), %r15
	movq	%r10, %rcx
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_10 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rax,%r8,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%rbp,%r10), %xmm0
	mulsd	(%r12,%r8,8), %xmm0
	movq	64(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm0, (%rsi,%r8,8)
	movsd	(%rax,%r8,8), %xmm0     # xmm0 = mem[0],zero
	movsd	(%rdx,%r10), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r12,%r8,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	(%rdi,%r10), %xmm0
	addsd	%xmm1, %xmm0
	movq	240(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r10), %xmm2
	addsd	%xmm0, %xmm2
	movq	96(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%r8,8)
	movsd	(%rax,%r8,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%rbx,%r10), %xmm0
	mulsd	-16(%r12,%r8,8), %xmm0
	movq	256(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm0, (%rsi,%r8,8)
	movsd	(%rcx,%r8,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%r15,%r10), %xmm0
	movq	80(%rsp), %r11          # 8-byte Reload
	mulsd	(%r11,%r8,8), %xmm0
	addsd	(%r9,%r10), %xmm0
	movsd	(%rax,%r8,8), %xmm1     # xmm1 = mem[0],zero
	mulsd	(%rbp,%r10), %xmm1
	movq	72(%rsp), %rsi          # 8-byte Reload
	mulsd	(%rsi,%r8,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	248(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm1, (%rsi,%r8,8)
	incq	%r8
	addq	%r13, %r10
	cmpl	%r8d, %r14d
	jne	.LBB2_16
# BB#17:                                # %._crit_edge1080.us
                                        #   in Loop: Header=BB2_15 Depth=3
	movl	232(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	216(%rsp), %ecx         # 4-byte Reload
	addl	52(%rsp), %ecx          # 4-byte Folded Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	movl	224(%rsp), %ebx         # 4-byte Reload
	addl	28(%rsp), %ebx          # 4-byte Folded Reload
	movq	184(%rsp), %rbp         # 8-byte Reload
	addl	92(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	176(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB2_15
# BB#18:                                # %._crit_edge1090.loopexit
                                        #   in Loop: Header=BB2_10 Depth=2
	movl	24(%rsp), %edx          # 4-byte Reload
	addl	352(%rsp), %edx         # 4-byte Folded Reload
	movl	132(%rsp), %ebx         # 4-byte Reload
	addl	336(%rsp), %ebx         # 4-byte Folded Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	128(%rsp), %r10d        # 4-byte Reload
	movl	124(%rsp), %r15d        # 4-byte Reload
	movl	120(%rsp), %r12d        # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	136(%rsp), %edi         # 4-byte Reload
	movl	376(%rsp), %esi         # 4-byte Reload
	movl	368(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_13:                               # %.preheader.preheader
                                        #   in Loop: Header=BB2_10 Depth=2
	addl	328(%rsp), %esi         # 4-byte Folded Reload
	addl	312(%rsp), %ebp         # 4-byte Folded Reload
	addl	320(%rsp), %edx         # 4-byte Folded Reload
	addl	116(%rsp), %ebx         # 4-byte Folded Reload
.LBB2_11:                               #   in Loop: Header=BB2_10 Depth=2
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
.LBB2_19:                               # %._crit_edge1090
                                        #   in Loop: Header=BB2_10 Depth=2
	addl	%r12d, %esi
	addl	%r15d, %edx
	addl	%r10d, %ebp
	addl	%r14d, %ebx
	incl	%edi
	cmpl	%eax, %edi
                                        # kill: %EBP<def> %EBP<kill> %RBP<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jne	.LBB2_10
	jmp	.LBB2_33
	.p2align	4, 0x90
.LBB2_20:                               #   in Loop: Header=BB2_2 Depth=1
	testl	%ecx, %ecx
	jle	.LBB2_33
# BB#21:                                # %.lr.ph1073
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	jle	.LBB2_33
# BB#22:                                # %.preheader1043.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	movl	%ecx, %edi
	subl	%edx, %eax
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	52(%rsp), %esi          # 4-byte Reload
	subl	%edx, %esi
	movl	%r8d, %ebp
	imull	%edx, %ebp
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	92(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %r8d
	subl	%ebp, %r8d
	movq	%r11, 56(%rsp)          # 8-byte Spill
	movl	28(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r11d
	subl	32(%rsp), %r11d         # 4-byte Folded Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %edx
	imull	%edi, %edx
	movq	%rdx, 592(%rsp)         # 8-byte Spill
	subl	%edx, %r12d
	leal	-1(%rcx), %edx
	imull	%edx, %r8d
	addl	%r9d, %r8d
	subl	%ebp, %r8d
	movl	%r8d, 412(%rsp)         # 4-byte Spill
	movl	%ecx, %ebp
	movl	52(%rsp), %r8d          # 4-byte Reload
	imull	%r8d, %ebp
	movl	%ebp, 428(%rsp)         # 4-byte Spill
	subl	%ebp, %r15d
	imull	%edx, %eax
	imull	%edx, %esi
	imull	%edx, %r11d
	movl	%ecx, %edx
	imull	%r9d, %edx
	movq	104(%rsp), %r9          # 8-byte Reload
	movq	%rdx, 584(%rsp)         # 8-byte Spill
	subl	%edx, %r10d
	addl	64(%rsp), %ebx          # 4-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	imull	%r13d, %ecx
	movl	%ecx, 424(%rsp)         # 4-byte Spill
	subl	%ecx, %r14d
	addl	%edi, %eax
	subl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 420(%rsp)         # 4-byte Spill
	addl	%r8d, %esi
	movq	184(%rsp), %rbp         # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	subl	32(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, 416(%rsp)         # 4-byte Spill
	addl	%r13d, %r11d
	movq	32(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %r11d
	movl	%r11d, 408(%rsp)        # 4-byte Spill
	movslq	136(%rsp), %r13         # 4-byte Folded Reload
	movslq	144(%rsp), %rsi         # 4-byte Folded Reload
	addl	96(%rsp), %ebp          # 4-byte Folded Reload
	addl	80(%rsp), %edx          # 4-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax,%rsi,8), %rsi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	addq	$8, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	addq	$8, 288(%rsp)           # 8-byte Folded Spill
	shlq	$3, %r9
	leaq	(,%r13,8), %rax
	movq	280(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rsi
	subq	%rax, %rsi
	movq	%rsi, 376(%rsp)         # 8-byte Spill
	movq	384(%rsp), %r8          # 8-byte Reload
	movq	%r8, %rsi
	subq	%rax, %rsi
	movq	%rsi, 368(%rsp)         # 8-byte Spill
	leaq	(%r11,%r13,8), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leaq	(%r8,%r13,8), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	392(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	400(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movl	88(%rsp), %eax          # 4-byte Reload
	xorl	%edi, %edi
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	movl	%r10d, 128(%rsp)        # 4-byte Spill
	movl	%r15d, 124(%rsp)        # 4-byte Spill
	movl	%r12d, 120(%rsp)        # 4-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_23:                               # %.preheader1043
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_28 Depth 3
                                        #         Child Loop BB2_29 Depth 4
	cmpl	$0, 176(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_24
# BB#25:                                # %.preheader1041.lr.ph
                                        #   in Loop: Header=BB2_23 Depth=2
	testl	%ecx, %ecx
	jle	.LBB2_26
# BB#27:                                # %.preheader1041.us.preheader
                                        #   in Loop: Header=BB2_23 Depth=2
	movl	%edi, 116(%rsp)         # 4-byte Spill
	movq	592(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	movl	%eax, 436(%rsp)         # 4-byte Spill
	movq	584(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbp), %eax
	movl	%eax, 432(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 132(%rsp)         # 4-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB2_28:                               # %.preheader1041.us
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_23 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_29 Depth 4
	movl	%eax, 160(%rsp)         # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movslq	%ecx, %rax
	movq	504(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r12
	movq	496(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movslq	%esi, %rcx
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	168(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	288(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	%ebx, 152(%rsp)         # 4-byte Spill
	movslq	%ebx, %rcx
	movq	472(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	movq	488(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	movslq	%ebp, %rbx
	movq	384(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rdx
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	392(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	400(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rdi
	movq	336(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	352(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	344(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r15
	movq	320(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r13
	movq	312(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r8
	movq	376(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r11
	movq	368(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r10
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_29:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_23 Depth=2
                                        #       Parent Loop BB2_28 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r12,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rax,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	200(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r14), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%rdx,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	96(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbx,8)
	movsd	(%r12,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movq	192(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%rax,%rbx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	(%rcx,%r14), %xmm0
	addsd	%xmm1, %xmm0
	movq	232(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	256(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbx,8)
	movsd	(%r12,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-16(%rax,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	(%r13,%r14), %xmm0
	addsd	%xmm1, %xmm0
	movq	224(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r14), %xmm2
	addsd	%xmm0, %xmm2
	movq	248(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbx,8)
	movq	80(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r11,%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	72(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	216(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%r14), %xmm1
	movsd	(%r12,%rbx,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%rdi,%r14), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	64(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbx,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r10,%r14), %xmm0
	addsd	%xmm4, %xmm0
	mulsd	(%r8,%r14), %xmm3
	addsd	%xmm0, %xmm3
	movq	208(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r14), %xmm2
	addsd	%xmm3, %xmm2
	mulsd	(%rdx,%r14), %xmm5
	addsd	%xmm2, %xmm5
	movq	240(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm5, (%rsi,%rbx,8)
	incq	%rbx
	addq	%rbp, %r14
	cmpl	%ebx, %r9d
	jne	.LBB2_29
# BB#30:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_28 Depth=3
	movl	160(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	144(%rsp), %ecx         # 4-byte Reload
	addl	52(%rsp), %ecx          # 4-byte Folded Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	movl	152(%rsp), %ebx         # 4-byte Reload
	addl	28(%rsp), %ebx          # 4-byte Folded Reload
	movq	184(%rsp), %rbp         # 8-byte Reload
	addl	92(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	176(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB2_28
# BB#31:                                # %._crit_edge1060.loopexit
                                        #   in Loop: Header=BB2_23 Depth=2
	movl	24(%rsp), %edx          # 4-byte Reload
	addl	428(%rsp), %edx         # 4-byte Folded Reload
	movl	132(%rsp), %ebx         # 4-byte Reload
	addl	424(%rsp), %ebx         # 4-byte Folded Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	128(%rsp), %r10d        # 4-byte Reload
	movl	124(%rsp), %r15d        # 4-byte Reload
	movl	120(%rsp), %r12d        # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	116(%rsp), %edi         # 4-byte Reload
	movl	436(%rsp), %esi         # 4-byte Reload
	movl	432(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB2_32
	.p2align	4, 0x90
.LBB2_26:                               # %.preheader1041.preheader
                                        #   in Loop: Header=BB2_23 Depth=2
	addl	420(%rsp), %esi         # 4-byte Folded Reload
	addl	412(%rsp), %ebp         # 4-byte Folded Reload
	addl	416(%rsp), %edx         # 4-byte Folded Reload
	addl	408(%rsp), %ebx         # 4-byte Folded Reload
.LBB2_24:                               #   in Loop: Header=BB2_23 Depth=2
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
.LBB2_32:                               # %._crit_edge1060
                                        #   in Loop: Header=BB2_23 Depth=2
	addl	%r12d, %esi
	addl	%r15d, %edx
	addl	%r10d, %ebp
	addl	%r14d, %ebx
	incl	%edi
	cmpl	%eax, %edi
                                        # kill: %EBP<def> %EBP<kill> %RBP<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jne	.LBB2_23
	.p2align	4, 0x90
.LBB2_33:                               # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	456(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	552(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rbx
	movq	440(%rsp), %rbp         # 8-byte Reload
	movq	544(%rsp), %rdx         # 8-byte Reload
	jl	.LBB2_2
.LBB2_34:                               # %._crit_edge1113
	xorl	%eax, %eax
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_SMG2BuildRAPNoSym, .Lfunc_end2-hypre_SMG2BuildRAPNoSym
	.cfi_endproc

	.globl	hypre_SMG2RAPPeriodicSym
	.p2align	4, 0x90
	.type	hypre_SMG2RAPPeriodicSym,@function
hypre_SMG2RAPPeriodicSym:               # @hypre_SMG2RAPPeriodicSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 256
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	cmpl	$1, 60(%rax)
	jne	.LBB3_37
# BB#1:
	movq	8(%rax), %rbp
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructMatrixAssemble
	cmpl	$0, 8(%rbp)
	jle	.LBB3_37
# BB#2:                                 # %.lr.ph515
	xorl	%ebx, %ebx
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
                                        #       Child Loop BB3_9 Depth 3
                                        #         Child Loop BB3_16 Depth 4
                                        #         Child Loop BB3_12 Depth 4
                                        #     Child Loop BB3_26 Depth 2
                                        #       Child Loop BB3_27 Depth 3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_30 Depth 4
                                        #         Child Loop BB3_33 Depth 4
	movq	(%rbp), %rcx
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %r13
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r13), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r12         # 8-byte Reload
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, 48(%rsp)
	movq	$0, 40(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	leaq	40(%rsp), %r14
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 40(%rsp)
	movl	$0, 48(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 40(%rsp)
	movl	$0, 48(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %r15
	movq	$-1, 40(%rsp)
	movl	$0, 48(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %rbp
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 40(%rsp)
	movl	$0, 48(%rsp)
	movq	%r12, %rdi
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %r12
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	156(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	16(%rsp), %rdi          # 8-byte Reload
	xorps	%xmm2, %xmm2
	movl	(%rdi,%r13), %esi
	movl	4(%rdi,%r13), %ebx
	movl	12(%rdi,%r13), %eax
	movl	%eax, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %eax
	movl	16(%rdi,%r13), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, %edx
	subl	%ebx, %edx
	incl	%edx
	cmpl	%ebx, %eax
	cmovsl	%ecx, %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	156(%rsp), %eax
	movl	160(%rsp), %ecx
	movl	164(%rsp), %edx
	cmpl	%eax, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	cmpl	%eax, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB3_36
# BB#4:                                 # %.lr.ph449
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx,%r13), %r11
	leaq	8(%rdi,%r13), %r9
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r12, 96(%rsp)          # 8-byte Spill
	jle	.LBB3_22
# BB#5:                                 # %.lr.ph449
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_22
# BB#6:                                 # %.preheader425.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, %eax
	movq	24(%rsp), %r10          # 8-byte Reload
	subl	%r10d, %eax
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	%r8d, %edi
	movq	88(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %edi
	imull	%edx, %edi
	movl	%edi, 120(%rsp)         # 4-byte Spill
	leal	-1(%rcx), %edi
	imull	%eax, %edi
	addl	%edx, %edi
	subl	%r10d, %edi
	movl	%edi, 112(%rsp)         # 4-byte Spill
	movl	%edx, %eax
	imull	%ecx, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%esi, 32(%rsp)          # 4-byte Spill
	subl	%esi, %eax
	movl	(%r11), %ecx
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	subl	%ebx, %ecx
	movq	%r11, 168(%rsp)         # 8-byte Spill
	movl	4(%r11), %esi
	movq	%r9, 176(%rsp)          # 8-byte Spill
	subl	(%r9), %esi
	imull	%r8d, %esi
	addl	%ecx, %esi
	imull	%edx, %esi
	addl	%eax, %esi
	leal	-1(%r10), %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	8(%rdx,%rcx,8), %rdi
	movq	80(%rsp), %r9           # 8-byte Reload
	leaq	8(%r9,%rcx,8), %r13
	leaq	-8(%r12), %r14
	leaq	(%r12,%rcx,8), %r8
	leaq	8(%rbp,%rcx,8), %r11
	leaq	8(%r15,%rcx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %r12
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	leaq	2(%rax), %r10
	andq	%r12, %r10
	cmpq	%r13, %rdx
	sbbb	%cl, %cl
	cmpq	%rdi, %r9
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r8, %rdx
	sbbb	%cl, %cl
	cmpq	%rdi, %r14
	sbbb	%al, %al
	andb	%cl, %al
	orb	%bl, %al
	cmpq	%r11, %rdx
	sbbb	%cl, %cl
	cmpq	%rdi, %rbp
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	sbbb	%cl, %cl
	cmpq	%rdi, %r15
	sbbb	%dl, %dl
	andb	%cl, %dl
	orb	%bl, %dl
	orb	%al, %dl
	cmpq	%r8, %r9
	sbbb	%al, %al
	cmpq	%r13, %r14
	sbbb	%cl, %cl
	andb	%al, %cl
	cmpq	%r11, %r9
	sbbb	%al, %al
	cmpq	%r13, %rbp
	sbbb	%bl, %bl
	andb	%al, %bl
	orb	%cl, %bl
	cmpq	16(%rsp), %r9           # 8-byte Folded Reload
	sbbb	%dil, %dil
	cmpq	%r13, %r15
	movl	%esi, %r9d
	sbbb	%al, %al
	andb	%dil, %al
	orb	%bl, %al
	orb	%dl, %al
	andb	$1, %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader425.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_9 Depth 3
                                        #         Child Loop BB3_16 Depth 4
                                        #         Child Loop BB3_12 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	112(%rsp), %eax         # 4-byte Reload
	jle	.LBB3_20
# BB#8:                                 # %.preheader423.us.us.preheader
                                        #   in Loop: Header=BB3_7 Depth=2
	movl	%edx, 128(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	jmp	.LBB3_9
.LBB3_15:                               # %vector.body575.preheader
                                        #   in Loop: Header=BB3_9 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r8,8), %rax
	leaq	(%r15,%r8,8), %rsi
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r8,8), %rdx
	leaq	(%rbp,%r8,8), %rdi
	leaq	(%r14,%r8,8), %rcx
	addq	%r10, %r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_16:                               # %vector.body575
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        #       Parent Loop BB3_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	(%rcx,%rbx,8), %xmm0
	movupd	(%rdi,%rbx,8), %xmm1
	addpd	%xmm0, %xmm1
	movupd	(%rdx,%rbx,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx,%rbx,8)
	movupd	(%rsi,%rbx,8), %xmm0
	addpd	%xmm0, %xmm0
	movupd	(%rax,%rbx,8), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rax,%rbx,8)
	addq	$2, %rbx
	cmpq	%rbx, %r10
	jne	.LBB3_16
# BB#17:                                # %middle.block576
                                        #   in Loop: Header=BB3_9 Depth=3
	cmpq	%r10, %r12
	movl	%r10d, %ebx
	jne	.LBB3_11
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader423.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_16 Depth 4
                                        #         Child Loop BB3_12 Depth 4
	movslq	%r9d, %r8
	cmpq	$1, %r12
	jbe	.LBB3_10
# BB#13:                                # %min.iters.checked579
                                        #   in Loop: Header=BB3_9 Depth=3
	testq	%r10, %r10
	je	.LBB3_10
# BB#14:                                # %vector.memcheck642
                                        #   in Loop: Header=BB3_9 Depth=3
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB3_15
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_9 Depth=3
	xorl	%ebx, %ebx
.LBB3_11:                               # %scalar.ph577.preheader
                                        #   in Loop: Header=BB3_9 Depth=3
	leaq	(%rbp,%r8,8), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r8,8), %rsi
	leaq	(%r15,%r8,8), %r11
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r8,8), %rdx
	leaq	(%r14,%r8,8), %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%ebx, %ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_12:                               # %scalar.ph577
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        #       Parent Loop BB3_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rdi,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rax,%rbx,8), %xmm0
	addsd	(%rsi,%rbx,8), %xmm0
	movsd	%xmm0, (%rsi,%rbx,8)
	movsd	(%r11,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	addsd	(%rdx,%rbx,8), %xmm0
	movsd	%xmm0, (%rdx,%rbx,8)
	incq	%rbx
	cmpl	%ebx, %ecx
	jne	.LBB3_12
.LBB3_18:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_9 Depth=3
	addl	12(%rsp), %r9d          # 4-byte Folded Reload
	incl	%r13d
	cmpl	88(%rsp), %r13d         # 4-byte Folded Reload
	jne	.LBB3_9
# BB#19:                                #   in Loop: Header=BB3_7 Depth=2
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	64(%rsp), %r9d          # 4-byte Reload
	movl	128(%rsp), %edx         # 4-byte Reload
.LBB3_20:                               # %._crit_edge430.us
                                        #   in Loop: Header=BB3_7 Depth=2
	addl	%r9d, %eax
	addl	120(%rsp), %eax         # 4-byte Folded Reload
	incl	%edx
	cmpl	8(%rsp), %edx           # 4-byte Folded Reload
	movl	%eax, %r9d
	jne	.LBB3_7
# BB#21:                                # %._crit_edge450
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 140(%rsp)           # 4-byte Folded Reload
	movq	96(%rsp), %r12          # 8-byte Reload
	movl	36(%rsp), %ebx          # 4-byte Reload
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	168(%rsp), %r11         # 8-byte Reload
	jle	.LBB3_36
.LBB3_22:                               # %.lr.ph477
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB3_36
# BB#23:                                # %.lr.ph477.split.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_36
# BB#24:                                # %.lr.ph477.split.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_36
# BB#25:                                # %.preheader424.us.us.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	subl	%esi, %eax
	movl	(%r11), %ecx
	subl	%ebx, %ecx
	movl	4(%r11), %esi
	subl	(%r9), %esi
	movl	4(%rsp), %r14d          # 4-byte Reload
	imull	%r14d, %esi
	addl	%ecx, %esi
	movl	12(%rsp), %r9d          # 4-byte Reload
	imull	%r9d, %esi
	addl	%eax, %esi
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %r10d
	leaq	8(%rbp,%r10,8), %rax
	leaq	8(%r15,%r10,8), %r8
	leaq	8(%r12,%r10,8), %r11
	leaq	1(%r10), %rdi
	movq	%rdi, %rcx
	movabsq	$8589934588, %rdx       # imm = 0x1FFFFFFFC
	andq	%rdx, %rcx
	leaq	-4(%rcx), %r12
	shrq	$2, %r12
	cmpq	%r8, %rbp
	sbbb	%bl, %bl
	cmpq	%rax, %r15
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%r11, %rbp
	sbbb	%bl, %bl
	cmpq	%rax, 96(%rsp)          # 8-byte Folded Reload
	sbbb	%al, %al
	andb	%bl, %al
	movq	96(%rsp), %rbx          # 8-byte Reload
	orb	%dl, %al
	cmpq	%r11, %r15
	sbbb	%r11b, %r11b
	cmpq	%r8, %rbx
	sbbb	%dl, %dl
	andb	%r11b, %dl
	orb	%al, %dl
	andb	$1, %dl
	movb	%dl, 64(%rsp)           # 1-byte Spill
	movq	%r12, 128(%rsp)         # 8-byte Spill
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	andl	$1, %r12d
	movq	%r12, 120(%rsp)         # 8-byte Spill
	imull	%r9d, %r14d
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	leaq	48(%rbx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rbx, %rax
	addq	$24, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	48(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%r15, %rax
	addq	$24, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	addq	$24, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_26:                               # %.preheader424.us.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_27 Depth 3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_30 Depth 4
                                        #         Child Loop BB3_33 Depth 4
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	%esi, %r9d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_27:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_45 Depth 4
                                        #         Child Loop BB3_30 Depth 4
                                        #         Child Loop BB3_33 Depth 4
	movslq	%r9d, %r14
	xorl	%r8d, %r8d
	cmpq	$3, %rdi
	jbe	.LBB3_28
# BB#38:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_27 Depth=3
	testq	%rcx, %rcx
	je	.LBB3_28
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_27 Depth=3
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_28
# BB#40:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_27 Depth=3
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_41
# BB#42:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_27 Depth=3
	movups	%xmm2, (%rbp,%r14,8)
	movups	%xmm2, 16(%rbp,%r14,8)
	movups	%xmm2, (%r15,%r14,8)
	movups	%xmm2, 16(%r15,%r14,8)
	movups	%xmm2, (%rbx,%r14,8)
	movups	%xmm2, 16(%rbx,%r14,8)
	movl	$4, %eax
	cmpq	$0, 128(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_44
	jmp	.LBB3_46
.LBB3_41:                               #   in Loop: Header=BB3_27 Depth=3
	xorl	%eax, %eax
	cmpq	$0, 128(%rsp)           # 8-byte Folded Reload
	je	.LBB3_46
.LBB3_44:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_27 Depth=3
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r14,8), %rdx
	movq	104(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r14,8), %rsi
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r14,8), %rbx
	.p2align	4, 0x90
.LBB3_45:                               # %vector.body
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_26 Depth=2
                                        #       Parent Loop BB3_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm2, -48(%rbx,%rax,8)
	movups	%xmm2, -32(%rbx,%rax,8)
	movups	%xmm2, -48(%rsi,%rax,8)
	movups	%xmm2, -32(%rsi,%rax,8)
	movups	%xmm2, -48(%rdx,%rax,8)
	movups	%xmm2, -32(%rdx,%rax,8)
	movups	%xmm2, -16(%rbx,%rax,8)
	movups	%xmm2, (%rbx,%rax,8)
	movups	%xmm2, -16(%rsi,%rax,8)
	movups	%xmm2, (%rsi,%rax,8)
	movups	%xmm2, -16(%rdx,%rax,8)
	movups	%xmm2, (%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB3_45
.LBB3_46:                               # %middle.block
                                        #   in Loop: Header=BB3_27 Depth=3
	cmpq	%rcx, %rdi
	movq	96(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_34
# BB#47:                                #   in Loop: Header=BB3_27 Depth=3
	addq	%rcx, %r14
	movl	%ecx, %r8d
	.p2align	4, 0x90
.LBB3_28:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_27 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	subl	%r8d, %esi
	movl	%r10d, %eax
	subl	%r8d, %eax
	andl	$3, %esi
	je	.LBB3_31
# BB#29:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_27 Depth=3
	negl	%esi
	.p2align	4, 0x90
.LBB3_30:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_26 Depth=2
                                        #       Parent Loop BB3_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, (%rbp,%r14,8)
	movq	$0, (%r15,%r14,8)
	movq	$0, (%rbx,%r14,8)
	incq	%r14
	incl	%r8d
	incl	%esi
	jne	.LBB3_30
.LBB3_31:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_27 Depth=3
	cmpl	$3, %eax
	jb	.LBB3_34
# BB#32:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_27 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %r13
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	subl	%r8d, %esi
	.p2align	4, 0x90
.LBB3_33:                               # %scalar.ph
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_26 Depth=2
                                        #       Parent Loop BB3_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, -24(%rax)
	movq	$0, -24(%r11)
	movq	$0, -24(%r13)
	movq	$0, -16(%rax)
	movq	$0, -16(%r11)
	movq	$0, -16(%r13)
	movq	$0, -8(%rax)
	movq	$0, -8(%r11)
	movq	$0, -8(%r13)
	movq	$0, (%rax)
	movq	$0, (%r11)
	movq	$0, (%r13)
	addq	$32, %r13
	addq	$32, %r11
	addq	$32, %rax
	addl	$-4, %esi
	jne	.LBB3_33
.LBB3_34:                               # %._crit_edge454.us.us.us.us
                                        #   in Loop: Header=BB3_27 Depth=3
	addl	12(%rsp), %r9d          # 4-byte Folded Reload
	incl	%r12d
	cmpl	88(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB3_27
# BB#35:                                # %._crit_edge458.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB3_26 Depth=2
	movl	32(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	36(%rsp), %esi          # 4-byte Reload
	addl	4(%rsp), %esi           # 4-byte Folded Reload
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB3_26
	.p2align	4, 0x90
.LBB3_36:                               # %._crit_edge478
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	192(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	184(%rsp), %rbp         # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_3
.LBB3_37:                               # %.loopexit
	xorl	%eax, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_SMG2RAPPeriodicSym, .Lfunc_end3-hypre_SMG2RAPPeriodicSym
	.cfi_endproc

	.globl	hypre_SMG2RAPPeriodicNoSym
	.p2align	4, 0x90
	.type	hypre_SMG2RAPPeriodicNoSym,@function
hypre_SMG2RAPPeriodicNoSym:             # @hypre_SMG2RAPPeriodicNoSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 224
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	cmpl	$1, 60(%rax)
	jne	.LBB4_15
# BB#1:                                 # %.preheader274
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB4_15
# BB#2:                                 # %.lr.ph301
	xorl	%ebx, %ebx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_10 Depth 4
	movq	(%rcx), %rcx
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %r15
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r15), %r12
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	40(%r14), %rax
	movq	(%rax), %r13
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	leaq	12(%rsp), %rbp
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	$-1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	leaq	60(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r13,%r15), %esi
	movl	4(%r13,%r15), %r9d
	movl	12(%r13,%r15), %ecx
	movl	%ecx, %eax
	subl	%esi, %eax
	incl	%eax
	cmpl	%esi, %ecx
	movl	16(%r13,%r15), %ecx
	movl	$0, %edx
	cmovsl	%edx, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%ecx, %ebx
	subl	%r9d, %ebx
	incl	%ebx
	cmpl	%r9d, %ecx
	cmovsl	%edx, %ebx
	movl	60(%rsp), %ecx
	movl	64(%rsp), %edx
	movl	68(%rsp), %eax
	cmpl	%ecx, %edx
	movl	%ecx, %ebp
	cmovgel	%edx, %ebp
	cmpl	%ebp, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmovgel	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB4_14
# BB#4:                                 # %.lr.ph298
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_14
# BB#5:                                 # %.lr.ph298
                                        #   in Loop: Header=BB4_3 Depth=1
	testl	%edx, %edx
	jle	.LBB4_14
# BB#6:                                 # %.preheader273.us.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	subl	%ecx, %ebp
	movl	%ebx, %edi
	subl	%edx, %edi
	imull	%eax, %edi
	movl	%edi, 52(%rsp)          # 4-byte Spill
	leal	-1(%rdx), %edi
	imull	%ebp, %edi
	addl	%eax, %edi
	subl	%ecx, %edi
	movl	%edi, 48(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	imull	%edx, %edi
	movl	%edi, 44(%rsp)          # 4-byte Spill
	movl	(%r12), %ebp
	subl	%esi, %ebp
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	4(%rdi,%r15), %esi
	movl	8(%rdi,%r15), %edi
	subl	%r9d, %esi
	subl	8(%r13,%r15), %edi
	imull	%ebx, %edi
	addl	%esi, %edi
	imull	%eax, %edi
	addl	%ebp, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader273.us
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_10 Depth 4
	testl	%ecx, %ecx
	movl	48(%rsp), %esi          # 4-byte Reload
	jle	.LBB4_13
# BB#8:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB4_7 Depth=2
	movl	%eax, 56(%rsp)          # 4-byte Spill
	xorl	%r9d, %r9d
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	%edi, %esi
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader.us.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_10 Depth 4
	movslq	%esi, %rbp
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %r10
	movq	136(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbp,8), %r11
	movq	152(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbp,8), %r8
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,8), %rdi
	movq	144(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rbp,8), %r14
	movq	160(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rbp,8), %r12
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,8), %r13
	movq	128(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rbx
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,8), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_10:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_7 Depth=2
                                        #       Parent Loop BB4_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%r15,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r11,%r15,8), %xmm0
	addsd	(%r8,%r15,8), %xmm0
	movsd	%xmm0, (%r8,%r15,8)
	movq	$0, (%r10,%r15,8)
	movq	$0, (%r11,%r15,8)
	movsd	(%rdi,%r15,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r14,%r15,8), %xmm0
	addsd	(%r12,%r15,8), %xmm0
	movsd	%xmm0, (%r12,%r15,8)
	movq	$0, (%rdi,%r15,8)
	movq	$0, (%r14,%r15,8)
	movsd	(%r13,%r15,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbx,%r15,8), %xmm0
	addsd	(%rbp,%r15,8), %xmm0
	movsd	%xmm0, (%rbp,%r15,8)
	movq	$0, (%r13,%r15,8)
	movq	$0, (%rbx,%r15,8)
	incq	%r15
	cmpl	%r15d, %ecx
	jne	.LBB4_10
# BB#11:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB4_9 Depth=3
	incl	%r9d
	addl	28(%rsp), %esi          # 4-byte Folded Reload
	cmpl	%edx, %r9d
	jne	.LBB4_9
# BB#12:                                #   in Loop: Header=BB4_7 Depth=2
	movl	44(%rsp), %esi          # 4-byte Reload
	movl	32(%rsp), %edi          # 4-byte Reload
	movl	56(%rsp), %eax          # 4-byte Reload
.LBB4_13:                               # %._crit_edge279.us
                                        #   in Loop: Header=BB4_7 Depth=2
	addl	%edi, %esi
	addl	52(%rsp), %esi          # 4-byte Folded Reload
	incl	%eax
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%esi, %edi
	jne	.LBB4_7
.LBB4_14:                               # %._crit_edge299
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	88(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_3
.LBB4_15:                               # %.loopexit
	xorl	%eax, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_SMG2RAPPeriodicNoSym, .Lfunc_end4-hypre_SMG2RAPPeriodicNoSym
	.cfi_endproc

	.type	.Lhypre_SMG2CreateRAPOp.RAP_num_ghost,@object # @hypre_SMG2CreateRAPOp.RAP_num_ghost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lhypre_SMG2CreateRAPOp.RAP_num_ghost:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lhypre_SMG2CreateRAPOp.RAP_num_ghost, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
