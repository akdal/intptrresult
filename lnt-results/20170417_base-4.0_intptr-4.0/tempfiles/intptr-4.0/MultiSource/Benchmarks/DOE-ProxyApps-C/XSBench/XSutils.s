	.text
	.file	"XSutils.bc"
	.globl	gpmatrix
	.p2align	4, 0x90
	.type	gpmatrix,@function
gpmatrix:                               # @gpmatrix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r14, %r12
	imulq	%r15, %r12
	movq	%r12, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, %rbx
	shlq	$3, %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rcx
	testq	%r12, %r12
	je	.LBB0_11
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %r12b
	jne	.LBB0_3
# BB#2:
	xorl	%esi, %esi
	xorl	%edi, %edi
	cmpq	$1, %r12
	jne	.LBB0_5
	jmp	.LBB0_11
.LBB0_3:
	movq	%rbx, (%rcx)
	movl	$1, %edi
	movl	$1, %esi
	cmpq	$1, %r12
	je	.LBB0_11
.LBB0_5:                                # %.lr.ph.preheader.new
	leaq	(%rsi,%rsi,2), %rax
	shlq	$4, %rax
	addq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r14
	testq	%rdx, %rdx
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movslq	%edi, %rax
	incl	%edi
	movq	%rbx, (%rcx,%rax,8)
.LBB0_8:                                # %.lr.ph.123
                                        #   in Loop: Header=BB0_6 Depth=1
	incq	%rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r14
	testq	%rdx, %rdx
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=1
	leaq	48(%rbx), %rax
	movslq	%edi, %rdx
	incl	%edi
	movq	%rax, (%rcx,%rdx,8)
.LBB0_10:                               #   in Loop: Header=BB0_6 Depth=1
	addq	$96, %rbx
	incq	%rsi
	cmpq	%r12, %rsi
	jne	.LBB0_6
.LBB0_11:                               # %._crit_edge
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	gpmatrix, .Lfunc_end0-gpmatrix
	.cfi_endproc

	.globl	gpmatrix_free
	.p2align	4, 0x90
	.type	gpmatrix_free,@function
gpmatrix_free:                          # @gpmatrix_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	gpmatrix_free, .Lfunc_end1-gpmatrix_free
	.cfi_endproc

	.globl	NGP_compare
	.p2align	4, 0x90
	.type	NGP_compare,@function
NGP_compare:                            # @NGP_compare
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	movl	$-1, %ecx
	cmovbel	%eax, %ecx
	ucomisd	%xmm1, %xmm0
	movl	$1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end2:
	.size	NGP_compare, .Lfunc_end2-NGP_compare
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	binary_search
	.p2align	4, 0x90
	.type	binary_search,@function
binary_search:                          # @binary_search
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movapd	%xmm0, %xmm1
	movq	%rdi, %r14
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	xorl	%ebp, %ebp
	ucomisd	%xmm1, %xmm0
	ja	.LBB3_11
# BB#1:
	leal	-1(%rsi), %ebp
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	ucomisd	(%r14,%rax), %xmm1
	jbe	.LBB3_2
# BB#4:
	addl	$-2, %esi
	movl	%esi, %ebp
	jmp	.LBB3_11
.LBB3_2:                                # %.preheader
	testl	%esi, %esi
	jle	.LBB3_11
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movsd	%xmm1, (%rsp)           # 8-byte Spill
.LBB3_7:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	subl	%ebx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI3_0(%rip), %xmm0
	callq	floor
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	cvttsd2si	%xmm0, %eax
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movsd	(%r14,%rcx), %xmm0      # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB3_6
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=2
	ucomisd	%xmm1, %xmm0
	jbe	.LBB3_9
# BB#10:                                #   in Loop: Header=BB3_5 Depth=2
	leal	-1(%rax), %ebp
	cmpl	%ebx, %eax
	jg	.LBB3_5
	jmp	.LBB3_11
.LBB3_6:                                # %.outer
                                        #   in Loop: Header=BB3_7 Depth=1
	leal	1(%rax), %ebx
	cmpl	%eax, %ebp
	jg	.LBB3_7
	jmp	.LBB3_11
.LBB3_9:
	movl	%eax, %ebp
.LBB3_11:                               # %.loopexit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	binary_search, .Lfunc_end3-binary_search
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	rn
	.p2align	4, 0x90
	.type	rn,@function
rn:                                     # @rn
	.cfi_startproc
# BB#0:
	imulq	$16807, (%rdi), %rcx    # imm = 0x41A7
	movabsq	$8589934597, %rdx       # imm = 0x200000005
	movq	%rcx, %rax
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$30, %rax
	movq	%rax, %rdx
	shlq	$31, %rdx
	subq	%rax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, (%rdi)
	cvtsi2sdl	%ecx, %xmm0
	divsd	.LCPI4_0(%rip), %xmm0
	retq
.Lfunc_end4:
	.size	rn, .Lfunc_end4-rn
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	rn_v
	.p2align	4, 0x90
	.type	rn_v,@function
rn_v:                                   # @rn_v
	.cfi_startproc
# BB#0:
	imulq	$16807, rn_v.seed(%rip), %rcx # imm = 0x41A7
	movabsq	$8589934597, %rdx       # imm = 0x200000005
	movq	%rcx, %rax
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$30, %rax
	movq	%rax, %rdx
	shlq	$31, %rdx
	subq	%rax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, rn_v.seed(%rip)
	cvtsi2sdl	%ecx, %xmm0
	divsd	.LCPI5_0(%rip), %xmm0
	retq
.Lfunc_end5:
	.size	rn_v, .Lfunc_end5-rn_v
	.cfi_endproc

	.globl	hash
	.p2align	4, 0x90
	.type	hash,@function
hash:                                   # @hash
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB6_1
# BB#2:                                 # %.lr.ph.preheader
	incq	%rdi
	movl	$5381, %edx             # imm = 0x1505
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %ecx
	movl	%edx, %eax
	shll	$5, %eax
	addl	%edx, %eax
	addl	%ecx, %eax
	movzbl	(%rdi), %ecx
	incq	%rdi
	testb	%cl, %cl
	movl	%eax, %edx
	jne	.LBB6_3
	jmp	.LBB6_4
.LBB6_1:
	movl	$5381, %eax             # imm = 0x1505
.LBB6_4:                                # %._crit_edge
	xorl	%edx, %edx
	divl	%esi
	movl	%edx, %eax
	retq
.Lfunc_end6:
	.size	hash, .Lfunc_end6-hash
	.cfi_endproc

	.globl	estimate_mem_usage
	.p2align	4, 0x90
	.type	estimate_mem_usage,@function
estimate_mem_usage:                     # @estimate_mem_usage
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %rax
	leaq	64(,%rax,4), %rcx
	imulq	24(%rsp), %rax
	imulq	%rcx, %rax
	shrq	$20, %rax
	retq
.Lfunc_end7:
	.size	estimate_mem_usage, .Lfunc_end7-estimate_mem_usage
	.cfi_endproc

	.globl	binary_dump
	.p2align	4, 0x90
	.type	binary_dump,@function
binary_dump:                            # @binary_dump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r12
	testq	%r15, %r15
	jle	.LBB8_3
# BB#1:
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph27
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	$48, %esi
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	fwrite
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB8_2
.LBB8_3:                                # %.preheader
	imulq	%r15, %r14
	testq	%r14, %r14
	jle	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	callq	fwrite
	movq	8(%r13), %rdi
	movl	$4, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	fwrite
	addq	$16, %r13
	decq	%r14
	jne	.LBB8_4
.LBB8_5:                                # %._crit_edge
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.Lfunc_end8:
	.size	binary_dump, .Lfunc_end8-binary_dump
	.cfi_endproc

	.globl	binary_read
	.p2align	4, 0x90
	.type	binary_read,@function
binary_read:                            # @binary_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r12
	testq	%r15, %r15
	jle	.LBB9_3
# BB#1:
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph29
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	$48, %esi
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	fread
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB9_2
.LBB9_3:                                # %.preheader
	imulq	%r15, %r14
	testq	%r14, %r14
	jle	.LBB9_5
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	callq	fread
	movq	8(%r13), %rdi
	movl	$4, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	fread
	addq	$16, %r13
	decq	%r14
	jne	.LBB9_4
.LBB9_5:                                # %._crit_edge
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.Lfunc_end9:
	.size	binary_read, .Lfunc_end9-binary_read
	.cfi_endproc

	.type	rn_v.seed,@object       # @rn_v.seed
	.data
	.p2align	3
rn_v.seed:
	.quad	1337                    # 0x539
	.size	rn_v.seed, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"XS_data.dat"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"wb"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
