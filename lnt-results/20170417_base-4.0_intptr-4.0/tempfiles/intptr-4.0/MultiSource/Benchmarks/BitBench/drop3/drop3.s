	.text
	.file	"drop3.bc"
	.globl	drop_0xx
	.p2align	4, 0x90
	.type	drop_0xx,@function
drop_0xx:                               # @drop_0xx
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$3, %edx
	jl	.LBB0_36
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ecx
	shll	$8, %ecx
	andl	$65528, %r9d            # imm = 0xFFF8
	orl	%ecx, %r9d
	cmpl	$32768, %r9d            # imm = 0x8000
	jb	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	%r9d, %r10d
	shrl	$13, %r10d
	movzwl	%r8w, %ecx
	leal	(%r10,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r10d
	movb	%r10b, (%rsi)
	incq	%rsi
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$6, %edx
	jl	.LBB0_34
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	leal	(,%r9,8), %r10d
	andl	$65472, %r10d           # imm = 0xFFC0
	cmpl	$32768, %r10d           # imm = 0x8000
	jb	.LBB0_9
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	shrl	$13, %r10d
	movzwl	%r8w, %ecx
	leal	(%r10,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r10d
	movb	%r10b, (%rsi)
	incq	%rsi
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$9, %edx
	jl	.LBB0_34
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	shll	$6, %r9d
	movzbl	1(%rdi), %r10d
	movl	%r10d, %ecx
	shll	$6, %ecx
	andl	$65024, %r9d            # imm = 0xFE00
	orl	%ecx, %r9d
	cmpl	$32768, %r9d            # imm = 0x8000
	jb	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%r9d, %r11d
	shrl	$13, %r11d
	movzwl	%r8w, %ecx
	leal	(%r11,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r11d
	movb	%r11b, (%rsi)
	incq	%rsi
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$12, %edx
	jl	.LBB0_34
# BB#14:                                #   in Loop: Header=BB0_2 Depth=1
	leal	(,%r9,8), %r11d
	andl	$65024, %r11d           # imm = 0xFE00
	cmpl	$32768, %r11d           # imm = 0x8000
	jb	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	shrl	$13, %r11d
	movzwl	%r8w, %ecx
	leal	(%r11,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r11d
	movb	%r11b, (%rsi)
	incq	%rsi
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$15, %edx
	jl	.LBB0_34
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	shll	$6, %r9d
	andl	$61440, %r9d            # imm = 0xF000
	cmpl	$32768, %r9d            # imm = 0x8000
	jb	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_2 Depth=1
	shrl	$13, %r9d
	movzwl	%r8w, %ecx
	leal	(%r9,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r9d
	movb	%r9b, (%rsi)
	incq	%rsi
.LBB0_21:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$18, %edx
	jl	.LBB0_34
# BB#22:                                #   in Loop: Header=BB0_2 Depth=1
	movzbl	2(%rdi), %r9d
	movl	%r9d, %ecx
	shll	$7, %ecx
	andl	$1, %r10d
	shll	$15, %r10d
	orl	%ecx, %r10d
	cmpl	$32768, %r10d           # imm = 0x8000
	jb	.LBB0_25
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	shrl	$13, %r10d
	movzwl	%r8w, %ecx
	leal	(%r10,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r10d
	movb	%r10b, (%rsi)
	incq	%rsi
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$21, %edx
	jl	.LBB0_34
# BB#26:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%r9d, %r10d
	andl	$63, %r10d
	shll	$10, %r10d
	cmpl	$32768, %r10d           # imm = 0x8000
	jb	.LBB0_29
# BB#27:                                #   in Loop: Header=BB0_2 Depth=1
	shrl	$13, %r10d
	movzwl	%r8w, %ecx
	leal	(%r10,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r10d
	movb	%r10b, (%rsi)
	incq	%rsi
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$24, %edx
	jl	.LBB0_34
# BB#30:                                #   in Loop: Header=BB0_2 Depth=1
	addl	$-24, %edx
	andl	$7, %r9d
	shll	$13, %r9d
	cmpl	$24577, %r9d            # imm = 0x6001
	jb	.LBB0_33
# BB#31:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%r9d, %r10d
	shrl	$13, %r10d
	movzwl	%r8w, %ecx
	leal	(%r10,%rcx,8), %r8d
	addl	$3, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	%r8w, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r10d
	movb	%r10b, (%rsi)
	incq	%rsi
.LBB0_33:                               #   in Loop: Header=BB0_2 Depth=1
	shll	$3, %r9d
	addq	$3, %rdi
	cmpl	$2, %edx
	jg	.LBB0_2
.LBB0_34:                               # %._crit_edge
	movl	%eax, %edx
	andl	$7, %edx
	je	.LBB0_36
# BB#35:
	movzwl	%r8w, %edi
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movb	%dil, (%rsi)
.LBB0_36:                               # %._crit_edge.thread
	sarl	$3, %eax
	retq
.Lfunc_end0:
	.size	drop_0xx, .Lfunc_end0-drop_0xx
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 80
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movl	%edi, %ebx
	movl	$4000000, %edi          # imm = 0x3D0900
	callq	malloc
	movq	%rax, %r15
	movl	$4000000, %edi          # imm = 0x3D0900
	callq	malloc
	movq	%rax, %r14
	cmpl	$2, %ebx
	jl	.LBB1_3
# BB#1:
	movq	8(%r12), %rdi
	movl	$.L.str, %esi
	callq	fopen
	testq	%rax, %rax
	jne	.LBB1_2
# BB#6:
	movq	8(%r12), %rdi
	callq	perror
	movl	$1, %edi
	callq	exit
.LBB1_3:
	movq	stdin(%rip), %rax
	cmpl	$1, %ebx
	je	.LBB1_5
	jmp	.LBB1_7
.LBB1_2:
	decl	%ebx
	cmpl	$1, %ebx
	jne	.LBB1_7
.LBB1_5:
	movl	$1, %esi
	movl	$4000000, %edx          # imm = 0x3D0900
	movq	%r15, %rdi
	movq	%rax, %rcx
	callq	fread
	movq	%rax, %rbx
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	shlq	$3, %rbx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	drop_0xx
	movl	%eax, %ebx
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB1_7:
	movl	$.Lstr, %edi
	callq	puts
	movl	$2, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d\n"
	.size	.L.str.2, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Usage: drop_0XX [infile]"
	.size	.Lstr, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
