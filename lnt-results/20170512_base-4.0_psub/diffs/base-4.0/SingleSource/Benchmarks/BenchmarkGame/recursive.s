	.text
	.file	"recursive.bc"
	.globl	ack
	.p2align	4, 0x90
	.type	ack,@function
ack:                                    # @ack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rdi), %ebx
	testl	%esi, %esi
	je	.LBB0_2
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	decl	%esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	ack
	movl	%eax, %esi
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %esi
.LBB0_3:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	movl	%ebx, %edi
	jne	.LBB0_1
.LBB0_4:                                # %tailrecurse._crit_edge
	incl	%esi
	movl	%esi, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	ack, .Lfunc_end0-ack
	.cfi_endproc

	.globl	fib
	.p2align	4, 0x90
	.type	fib,@function
fib:                                    # @fib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movl	$1, %ebp
	cmpl	$2, %ebx
	jl	.LBB1_3
# BB#1:                                 # %tailrecurse.preheader
	incl	%ebx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	leal	-3(%rbx), %edi
	callq	fib
	addl	%eax, %ebp
	decl	%ebx
	cmpl	$2, %ebx
	jg	.LBB1_2
.LBB1_3:                                # %tailrecurse._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fib, .Lfunc_end1-fib
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4611686018427387904     # double 2
.LCPI2_1:
	.quad	-4611686018427387904    # double -2
.LCPI2_2:
	.quad	-4616189618054758400    # double -1
.LCPI2_3:
	.quad	4607182418800017408     # double 1
	.text
	.globl	fibFP
	.p2align	4, 0x90
	.type	fibFP,@function
fibFP:                                  # @fibFP
	.cfi_startproc
# BB#0:
	movapd	%xmm0, %xmm1
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB2_1
# BB#2:
	movsd	.LCPI2_3(%rip), %xmm0   # xmm0 = mem[0],zero
	retq
.LBB2_1:
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
	movsd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	fibFP
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI2_2(%rip), %xmm0
	callq	fibFP
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addq	$24, %rsp
	retq
.Lfunc_end2:
	.size	fibFP, .Lfunc_end2-fibFP
	.cfi_endproc

	.globl	tak
	.p2align	4, 0x90
	.type	tak,@function
tak:                                    # @tak
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -48
.Lcfi14:
	.cfi_offset %r12, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %r14d
	movl	%edi, %r15d
	cmpl	%r15d, %r14d
	jge	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%r15), %edi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	tak
	movl	%eax, %r12d
	leal	-1(%r14), %edi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	tak
	movl	%eax, %ebp
	decl	%ebx
	movl	%ebx, %edi
	movl	%r15d, %esi
	movl	%r14d, %edx
	callq	tak
	movl	%eax, %ebx
	cmpl	%r12d, %ebp
	movl	%ebp, %r14d
	movl	%r12d, %r15d
	jl	.LBB3_1
.LBB3_2:                                # %tailrecurse._crit_edge
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	tak, .Lfunc_end3-tak
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	takFP
	.p2align	4, 0x90
	.type	takFP,@function
takFP:                                  # @takFP
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 48
	movapd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm0
	jbe	.LBB4_3
	.p2align	4, 0x90
.LBB4_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movapd	%xmm3, %xmm1
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	takFP
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI4_0(%rip), %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	takFP
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI4_0(%rip), %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	takFP
	movapd	%xmm0, %xmm2
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	ja	.LBB4_1
.LBB4_3:                                # %tailrecurse._crit_edge
	movapd	%xmm2, %xmm0
	addq	$40, %rsp
	retq
.Lfunc_end4:
	.size	takFP, .Lfunc_end4-takFP
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4630544841867001856     # double 38
.LCPI5_1:
	.quad	4613937818241073152     # double 3
.LCPI5_2:
	.quad	4611686018427387904     # double 2
.LCPI5_3:
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movl	$3, %edi
	movl	$11, %esi
	callq	ack
	movl	%eax, %ecx
	movl	$.L.str, %edi
	movl	$11, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	printf
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	fibFP
	movaps	%xmm0, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	printf
	movl	$30, %edi
	movl	$20, %esi
	movl	$10, %edx
	callq	tak
	movl	%eax, %r8d
	movl	$.L.str.2, %edi
	movl	$30, %esi
	movl	$20, %edx
	movl	$10, %ecx
	xorl	%eax, %eax
	callq	printf
	movl	$3, %edi
	callq	fib
	movl	%eax, %ecx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movsd	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI5_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI5_3(%rip), %xmm2   # xmm2 = mem[0],zero
	callq	takFP
	movl	$.L.str.4, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Ack(3,%d): %d\n"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Fib(%.1f): %.1f\n"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Tak(%d,%d,%d): %d\n"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Fib(3): %d\n"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Tak(3.0,2.0,1.0): %.1f\n"
	.size	.L.str.4, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
