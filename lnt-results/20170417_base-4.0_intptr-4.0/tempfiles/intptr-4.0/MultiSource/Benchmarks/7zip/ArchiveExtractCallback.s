	.text
	.file	"ArchiveExtractCallback.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey,@function
_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey: # @_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rdi, %r12
	movq	112(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	104(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	120(%rsp), %rax
	movb	96(%rsp), %cl
	movq	%rsi, 40(%r12)
	movb	%r8b, 264(%r12)
	movb	%r9b, 265(%r12)
	movb	%cl, 266(%r12)
	movq	$1, 288(%r12)
	movq	%rax, 280(%r12)
	testq	%r15, %r15
	je	.LBB0_2
# BB#1:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB0_2:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_4:                                # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackEaSEPS0_.exit
	movq	%r15, 48(%r12)
	leaq	56(%r12), %rbx
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB0_5
# BB#6:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 56(%r12)
	movq	48(%r12), %rdi
	jmp	.LBB0_7
.LBB0_5:                                # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackEaSEPS0_.exit._ZN9CMyComPtrI21ICompressProgressInfoE7ReleaseEv.exit_crit_edge
	movq	%r15, %rdi
.LBB0_7:                                # %_ZN9CMyComPtrI21ICompressProgressInfoE7ReleaseEv.exit
	movq	(%rdi), %rax
	movl	$IID_ICompressProgressInfo, %esi
	movq	%rbx, %rdx
	callq	*(%rax)
	movq	296(%r12), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
	movq	296(%r12), %rax
	movb	$0, 65(%rax)
	leaq	232(%r12), %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	12(%rax), %ebp
	movl	244(%r12), %esi
	addl	%ebp, %esi
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB0_16
# BB#8:                                 # %.lr.ph.i.i
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %rbp
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movslq	8(%rbp), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB0_10
# BB#11:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp0:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp1:
# BB#12:                                # %.noexc.i
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	%rcx, (%r13)
	movl	$0, (%rcx)
	movl	%ebx, 12(%r13)
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	xorl	%ecx, %ecx
.LBB0_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	(%rbp), %rsi
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB0_14
# BB#15:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB0_9 Depth=1
	movl	%r15d, 8(%r13)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	248(%r12), %rax
	movslq	244(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 244(%r12)
	incq	%r14
	cmpq	24(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB0_9
.LBB0_16:                               # %_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r12)
	leaq	72(%r12), %r14
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpq	%rdx, %r14
	je	.LBB0_25
# BB#17:
	movl	$0, 80(%r12)
	movq	72(%r12), %rbx
	movl	$0, (%rbx)
	movslq	8(%rdx), %r15
	incq	%r15
	movl	84(%r12), %ebp
	cmpl	%ebp, %r15d
	je	.LBB0_22
# BB#18:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_21
# BB#19:
	testl	%ebp, %ebp
	jle	.LBB0_21
# BB#20:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	80(%r12), %rax
.LBB0_21:                               # %._crit_edge16.i.i
	movq	%r13, 72(%r12)
	movl	$0, (%r13,%rax,4)
	movl	%r15d, 84(%r12)
	movq	%r13, %rbx
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB0_22:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rdx), %rax
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_23
# BB#24:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rdx), %eax
	movl	%eax, 80(%r12)
.LBB0_25:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movq	%r14, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE # TAILCALL
.LBB0_26:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey, .Lfunc_end0-_ZN23CArchiveExtractCallback4InitEPKN9NWildcard11CCensorNodeEPK4CArcP29IFolderArchiveExtractCallbackbbbRK11CStringBaseIwERK13CObjectVectorISA_Ey
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback8SetTotalEy
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback8SetTotalEy,@function
_ZN23CArchiveExtractCallback8SetTotalEy: # @_ZN23CArchiveExtractCallback8SetTotalEy
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, 288(%rdi)
	xorl	%eax, %eax
	cmpb	$0, 267(%rdi)
	jne	.LBB1_7
# BB#1:
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#2:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*40(%rax)
.Ltmp4:
.LBB1_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_3:
.Ltmp5:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB1_4
# BB#6:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB1_7
.LBB1_4:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp6:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp7:
# BB#8:
.LBB1_5:
.Ltmp8:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN23CArchiveExtractCallback8SetTotalEy, .Lfunc_end1-_ZN23CArchiveExtractCallback8SetTotalEy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	3                       #   On action: 2
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback12SetCompletedEPKy
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback12SetCompletedEPKy,@function
_ZN23CArchiveExtractCallback12SetCompletedEPKy: # @_ZN23CArchiveExtractCallback12SetCompletedEPKy
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#3:
	testq	%rsi, %rsi
	je	.LBB2_15
# BB#4:
	movb	267(%rax), %cl
	testb	%cl, %cl
	je	.LBB2_15
# BB#5:
	movabsq	$4294967297, %r10       # imm = 0x100000001
	movl	$2147483648, %r9d       # imm = 0x80000000
	movq	296(%rax), %rcx
	movq	48(%rcx), %r8
	movq	(%rsi), %rcx
	movq	280(%rax), %rbx
	movq	288(%rax), %rdx
	cmpq	%r9, %rbx
	jbe	.LBB2_6
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rsi
	shrq	%rsi
	shrq	%rdx
	cmpq	%r10, %rbx
	movq	%rsi, %rbx
	ja	.LBB2_7
	jmp	.LBB2_8
.LBB2_1:
	xorl	%eax, %eax
	jmp	.LBB2_18
.LBB2_15:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*48(%rax)
.Ltmp10:
	jmp	.LBB2_18
.LBB2_6:
	movq	%rbx, %rsi
.LBB2_8:                                # %_ZL13NormalizeValsRyS_.exit.i
	cmpq	%r9, %rcx
	jbe	.LBB2_9
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.i1.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	shrq	%rax
	shrq	%rdx
	cmpq	%r10, %rcx
	movq	%rax, %rcx
	ja	.LBB2_10
	jmp	.LBB2_11
.LBB2_9:
	movq	%rcx, %rax
.LBB2_11:                               # %.loopexit
	testq	%rdx, %rdx
	movl	$1, %ecx
	cmovneq	%rdx, %rcx
	imulq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	%r8, %rax
	movq	%rax, (%rsp)
	movq	(%rdi), %rax
.Ltmp12:
	movq	%rsp, %rsi
	callq	*48(%rax)
.Ltmp13:
.LBB2_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_12:
.Ltmp14:
	jmp	.LBB2_13
.LBB2_2:
.Ltmp11:
.LBB2_13:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB2_14
# BB#17:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB2_18
.LBB2_14:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp15:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp16:
# BB#19:
.LBB2_16:
.Ltmp17:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN23CArchiveExtractCallback12SetCompletedEPKy, .Lfunc_end2-_ZN23CArchiveExtractCallback12SetCompletedEPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\317\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	3                       #   On action: 2
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	3                       #   On action: 2
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_,@function
_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_: # @_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	272(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp18:
	callq	*40(%rax)
.Ltmp19:
.LBB3_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_1:
.Ltmp20:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB3_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB3_5
.LBB3_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp21:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp22:
# BB#6:
.LBB3_3:
.Ltmp23:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_, .Lfunc_end3-_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin3   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp19-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_,@function
_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_: # @_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	256(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp24:
	callq	*40(%rax)
.Ltmp25:
.LBB4_5:                                # %_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_1:
.Ltmp26:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB4_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB4_5
.LBB4_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp27:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp28:
# BB#3:
.LBB4_6:
.Ltmp29:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_, .Lfunc_end4-_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp24-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin4   #     jumps to .Ltmp26
	.byte	3                       #   On action: 2
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp27-.Ltmp25         #   Call between .Ltmp25 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin4   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_,@function
_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_: # @_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	72(%r15), %rbp
	cmpq	%r12, %rbp
	je	.LBB5_9
# BB#1:
	movl	$0, 8(%r12)
	movq	(%r12), %rbx
	movl	$0, (%rbx)
	movslq	80(%r15), %r13
	incq	%r13
	movl	12(%r12), %eax
	cmpl	%eax, %r13d
	je	.LBB5_6
# BB#2:
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB5_5
# BB#3:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB5_5
# BB#4:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	8(%r12), %rcx
.LBB5_5:                                # %._crit_edge16.i.i
	movq	%rax, (%r12)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 12(%r12)
	movq	%rax, %rbx
.LBB5_6:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rbp), %rax
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB5_7
# BB#8:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	80(%r15), %eax
	movl	%eax, 8(%r12)
.LBB5_9:                                # %_ZN11CStringBaseIwEaSERKS0_.exit.preheader
	cmpl	$0, 12(%r14)
	jle	.LBB5_16
# BB#10:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_14 Depth 2
	testq	%rbx, %rbx
	jle	.LBB5_13
# BB#12:                                #   in Loop: Header=BB5_11 Depth=1
	movl	$1, %esi
	movq	%r12, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r12), %rax
	movslq	8(%r12), %rcx
	movl	$47, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r12)
	movl	$0, 4(%rax,%rcx,4)
.LBB5_13:                               #   in Loop: Header=BB5_11 Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	8(%rbp), %esi
	movq	%r12, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r12), %rdi
	movslq	8(%r12), %rax
	leaq	(%rdi,%rax,4), %rcx
	movq	(%rbp), %rdx
	.p2align	4, 0x90
.LBB5_14:                               #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB5_14
# BB#15:                                # %_ZN11CStringBaseIwEpLERKS0_.exit
                                        #   in Loop: Header=BB5_11 Depth=1
	movl	8(%rbp), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r12)
	callq	_ZN8NWindows5NFile10NDirectory17MyCreateDirectoryEPKw
	incq	%rbx
	movslq	12(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_11
.LBB5_16:                               # %_ZN11CStringBaseIwEaSERKS0_.exit._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_, .Lfunc_end5-_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_
	.cfi_endproc

	.globl	_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb,@function
_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb: # @_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, %r14
	movb	$0, (%rbx)
	movl	$0, (%rsp)
	movq	32(%rdi), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp30:
	movq	%rsp, %rcx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp31:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB6_6
# BB#2:
	movzwl	(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_5
# BB#3:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB6_6
# BB#4:
	movq	8(%rsp), %rax
	movq	%rax, (%r14)
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%eax, %ecx
	setne	(%rbx)
.LBB6_5:
	xorl	%ebp, %ebp
.LBB6_6:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB6_7:
.Ltmp32:
	movq	%rax, %rbx
.Ltmp33:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp34:
# BB#8:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_9:
.Ltmp35:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb, .Lfunc_end6-_ZN23CArchiveExtractCallback7GetTimeEijR9_FILETIMERb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp30-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin5   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp33-.Ltmp31         #   Call between .Ltmp31 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin5   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp34     #   Call between .Ltmp34 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.text
	.globl	_ZN23CArchiveExtractCallback13GetUnpackSizeEv
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback13GetUnpackSizeEv,@function
_ZN23CArchiveExtractCallback13GetUnpackSizeEv: # @_ZN23CArchiveExtractCallback13GetUnpackSizeEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$0, 8(%rsp)
	movq	32(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movl	180(%rbx), %esi
.Ltmp36:
	leaq	8(%rsp), %rcx
	movl	$7, %edx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp37:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB8_5
# BB#2:
	xorl	%ebp, %ebp
	cmpw	$0, 8(%rsp)
	setne	192(%rbx)
	je	.LBB8_5
# BB#3:
.Ltmp38:
	leaq	8(%rsp), %rdi
	callq	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
.Ltmp39:
# BB#4:
	movq	%rax, 184(%rbx)
.LBB8_5:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB8_6:
.Ltmp40:
	movq	%rax, %rbx
.Ltmp41:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp42:
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_8:
.Ltmp43:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN23CArchiveExtractCallback13GetUnpackSizeEv, .Lfunc_end8-_ZN23CArchiveExtractCallback13GetUnpackSizeEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp36-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp39-.Ltmp36         #   Call between .Ltmp36 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin6   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp41-.Ltmp39         #   Call between .Ltmp39 and .Ltmp41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin6   #     jumps to .Ltmp43
	.byte	1                       #   On action: 1
	.long	.Ltmp42-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp42     #   Call between .Ltmp42 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	69                      # 0x45
	.long	82                      # 0x52
	.long	82                      # 0x52
	.long	79                      # 0x4f
.LCPI9_1:
	.long	82                      # 0x52
	.long	58                      # 0x3a
	.long	32                      # 0x20
	.long	67                      # 0x43
.LCPI9_2:
	.long	97                      # 0x61
	.long	110                     # 0x6e
	.long	32                      # 0x20
	.long	110                     # 0x6e
.LCPI9_3:
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
	.long	99                      # 0x63
.LCPI9_4:
	.long	114                     # 0x72
	.long	101                     # 0x65
	.long	97                      # 0x61
	.long	116                     # 0x74
.LCPI9_5:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
.LCPI9_6:
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	119                     # 0x77
.LCPI9_7:
	.long	105                     # 0x69
	.long	116                     # 0x74
	.long	104                     # 0x68
	.long	32                      # 0x20
.LCPI9_8:
	.long	97                      # 0x61
	.long	117                     # 0x75
	.long	116                     # 0x74
	.long	111                     # 0x6f
.LCPI9_9:
	.long	32                      # 0x20
	.long	110                     # 0x6e
	.long	97                      # 0x61
	.long	109                     # 0x6d
.LCPI9_10:
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
	.long	114                     # 0x72
.LCPI9_11:
	.long	101                     # 0x65
	.long	110                     # 0x6e
	.long	97                      # 0x61
	.long	109                     # 0x6d
.LCPI9_12:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	101                     # 0x65
	.long	120                     # 0x78
.LCPI9_13:
	.long	105                     # 0x69
	.long	115                     # 0x73
	.long	116                     # 0x74
	.long	105                     # 0x69
.LCPI9_14:
	.long	110                     # 0x6e
	.long	103                     # 0x67
	.long	32                      # 0x20
	.long	102                     # 0x66
.LCPI9_15:
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	32                      # 0x20
.LCPI9_16:
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
	.long	100                     # 0x64
.LCPI9_17:
	.long	101                     # 0x65
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	116                     # 0x74
.LCPI9_18:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	111                     # 0x6f
	.long	117                     # 0x75
.LCPI9_19:
	.long	116                     # 0x74
	.long	112                     # 0x70
	.long	117                     # 0x75
	.long	116                     # 0x74
.LCPI9_20:
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
.LCPI9_21:
	.long	99                      # 0x63
	.long	97                      # 0x61
	.long	110                     # 0x6e
	.long	32                      # 0x20
.LCPI9_22:
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
.LCPI9_23:
	.long	111                     # 0x6f
	.long	112                     # 0x70
	.long	101                     # 0x65
	.long	110                     # 0x6e
.LCPI9_24:
	.long	32                      # 0x20
	.long	111                     # 0x6f
	.long	117                     # 0x75
	.long	116                     # 0x74
.LCPI9_25:
	.long	112                     # 0x70
	.long	117                     # 0x75
	.long	116                     # 0x74
	.long	32                      # 0x20
.LCPI9_26:
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.text
	.globl	_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami,@function
_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami: # @_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi64:
	.cfi_def_cfa_offset 352
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	224(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_3
# BB#1:
	movq	(%rdi), %rax
.Ltmp44:
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp45:
# BB#2:                                 # %.noexc
	movq	$0, 224(%r14)
.LBB9_3:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	$0, (%rbx)
	movq	208(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_6
# BB#4:
	movq	(%rdi), %rax
.Ltmp46:
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp47:
# BB#5:                                 # %.noexc265
	movq	$0, 208(%r14)
.LBB9_6:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit266
	movb	$0, 141(%r14)
	movb	$0, 136(%r14)
	movq	$0, 184(%r14)
	movb	$0, 192(%r14)
	movl	%r15d, 180(%r14)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
.Ltmp48:
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp49:
# BB#7:
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%rax, 96(%rsp)
	movl	$0, (%rax)
	movl	$4, 108(%rsp)
	movq	32(%r14), %rdi
	movq	(%rdi), %r12
.Ltmp51:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	leaq	96(%rsp), %rbx
	movl	%r15d, %esi
	movq	%rbx, %rdx
	callq	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
	movl	%eax, %ebp
.Ltmp52:
# BB#8:
	testl	%ebp, %ebp
	jne	.LBB9_39
# BB#9:
	leaq	176(%r14), %rdx
.Ltmp53:
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	movl	%r15d, %esi
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	callq	_Z19IsArchiveItemFolderP10IInArchivejRb
	movl	%eax, %ebp
.Ltmp54:
# BB#10:
	testl	%ebp, %ebp
	jne	.LBB9_39
# BB#11:
	leaq	112(%r14), %rax
	cmpq	%rax, %rbx
	je	.LBB9_21
# BB#12:
	movl	$0, 120(%r14)
	movq	112(%r14), %rbx
	movl	$0, (%rbx)
	movslq	104(%rsp), %rbp
	incq	%rbp
	movl	124(%r14), %r13d
	cmpl	%r13d, %ebp
	je	.LBB9_18
# BB#13:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp55:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp56:
# BB#14:                                # %.noexc268
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB9_17
# BB#15:                                # %.noexc268
	testl	%r13d, %r13d
	jle	.LBB9_17
# BB#16:                                # %._crit_edge.thread.i.i
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	120(%r14), %rcx
.LBB9_17:                               # %._crit_edge16.i.i
	movq	%rax, 112(%r14)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 124(%r14)
	movq	%rax, %rbx
.LBB9_18:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	96(%rsp), %rax
	.p2align	4, 0x90
.LBB9_19:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_19
# BB#20:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	104(%rsp), %eax
	movl	%eax, 120(%r14)
.LBB9_21:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movl	$0, 48(%rsp)
	movq	(%r12), %rax
.Ltmp57:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rcx
	movl	$29, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp58:
# BB#22:
	movl	$1, %ebp
	testl	%ebx, %ebx
	jne	.LBB9_27
# BB#23:
	movzwl	48(%rsp), %eax
	testw	%ax, %ax
	je	.LBB9_26
# BB#24:
	movl	$-2147467259, %ebx      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$21, %eax
	jne	.LBB9_27
# BB#25:
	movq	56(%rsp), %rax
	movq	%rax, 128(%r14)
	movb	$1, 136(%r14)
.LBB9_26:
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
.LBB9_27:
.Ltmp62:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp63:
# BB#28:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit271
	testl	%ebp, %ebp
	je	.LBB9_30
# BB#29:
	movl	%ebx, %ebp
	jmp	.LBB9_39
.LBB9_30:
	leaq	141(%r14), %rcx
.Ltmp65:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movl	$15, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	callq	_Z22GetArchiveItemBoolPropP10IInArchivejjRb
	movl	%eax, %ebp
.Ltmp66:
# BB#31:
	testl	%ebp, %ebp
	cmovnel	%ebp, %ebx
	jne	.LBB9_39
# BB#32:
	movl	$0, 144(%rsp)
	movq	32(%r14), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movl	180(%r14), %esi
.Ltmp67:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rcx
	movl	$7, %edx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp68:
# BB#33:
	leaq	184(%r14), %r13
	testl	%ebp, %ebp
	jne	.LBB9_37
# BB#34:
	xorl	%ebp, %ebp
	cmpw	$0, 144(%rsp)
	setne	192(%r14)
	je	.LBB9_37
# BB#35:
.Ltmp69:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
.Ltmp70:
# BB#36:
	movq	%rax, (%r13)
.LBB9_37:
.Ltmp75:
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp76:
# BB#38:
	testl	%ebp, %ebp
	cmovnel	%ebp, %ebx
	je	.LBB9_42
.LBB9_39:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_41
# BB#40:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_41:
	movl	%ebp, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_42:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_45
# BB#43:
	xorl	%edx, %edx
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	sete	%dl
.Ltmp77:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	leaq	96(%rsp), %rsi
	callq	_ZNK9NWildcard11CCensorNode9CheckPathERK11CStringBaseIwEb
.Ltmp78:
# BB#44:
	xorl	%ebp, %ebp
	testb	%al, %al
	je	.LBB9_39
.LBB9_45:
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB9_47
# BB#46:
	cmpb	$0, 265(%r14)
	je	.LBB9_63
.LBB9_47:
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
.LBB9_48:
	xorl	%ebp, %ebp
	cmpb	$0, 266(%r14)
	je	.LBB9_39
# BB#49:
.Ltmp262:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp263:
# BB#50:
	movl	$0, 8(%rbx)
	movq	$_ZTV17COutStreamWithCRC+16, (%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 216(%r14)
.Ltmp264:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTV17COutStreamWithCRC+24(%rip)
.Ltmp265:
# BB#51:                                # %.noexc548
	movq	224(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_53
# BB#52:
	movq	(%rdi), %rax
.Ltmp266:
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp267:
.LBB9_53:
	movq	%rbx, 224(%r14)
	movq	216(%r14), %r15
	testq	%r15, %r15
	movq	%r15, %r12
	je	.LBB9_56
# BB#54:
	movq	(%r15), %rax
.Ltmp268:
.Lcfi89:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp269:
# BB#55:                                # %._ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit551_crit_edge
	movq	216(%r14), %r12
.LBB9_56:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit551
	movq	168(%rsp), %r13         # 8-byte Reload
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB9_58
# BB#57:
	movq	(%rbx), %rax
.Ltmp271:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp272:
.LBB9_58:                               # %.noexc552
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB9_60
# BB#59:
	movq	(%rdi), %rax
.Ltmp273:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp274:
.LBB9_60:
	movq	%rbx, 16(%r12)
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB9_62
# BB#61:
	movq	(%rdi), %rax
.Ltmp275:
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp276:
.LBB9_62:
	movq	%r15, (%r13)
	movq	216(%r14), %rax
	movq	$0, 24(%rax)
	movb	$1, 36(%rax)
	movl	$-1, 32(%rax)
	jmp	.LBB9_39
.LBB9_63:
	cmpb	$0, 264(%r14)
	je	.LBB9_67
# BB#64:
.Ltmp79:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp80:
# BB#65:
	movl	$0, 8(%rbx)
	movq	$_ZTV17CStdOutFileStream+16, (%rbx)
.Ltmp81:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTV17CStdOutFileStream+24(%rip)
.Ltmp82:
# BB#66:
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rbx, (%rax)
	xorl	%ebp, %ebp
	jmp	.LBB9_39
.LBB9_67:
	movl	$0, 120(%rsp)
	movq	(%r12), %rax
.Ltmp83:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rcx
	movl	$9, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp84:
# BB#68:
	testl	%r12d, %r12d
	cmovnel	%r12d, %ebx
	movl	$1, %ebp
	jne	.LBB9_73
# BB#69:
	xorl	%eax, %eax
	movzwl	120(%rsp), %ecx
	testw	%cx, %cx
	je	.LBB9_72
# BB#70:
	movl	$-2147467259, %r12d     # imm = 0x80004005
	movzwl	%cx, %eax
	cmpl	$19, %eax
	jne	.LBB9_73
# BB#71:
	movl	128(%rsp), %eax
	movl	%eax, 168(%r14)
	movb	$1, %al
.LBB9_72:
	movb	%al, 175(%r14)
	xorl	%ebp, %ebp
	movl	%ebx, %r12d
.LBB9_73:
.Ltmp88:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp89:
# BB#74:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit278
	testl	%ebp, %ebp
	je	.LBB9_76
# BB#75:
	movl	%r12d, %ebp
	jmp	.LBB9_39
.LBB9_76:
	movb	$0, 172(%r14)
	movl	$0, 120(%rsp)
	movq	32(%r14), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp91:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rcx
	movl	$10, %edx
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp92:
# BB#77:
	testl	%ebp, %ebp
	jne	.LBB9_82
# BB#78:
	movzwl	120(%rsp), %eax
	testw	%ax, %ax
	je	.LBB9_81
# BB#79:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB9_82
# BB#80:
	movq	128(%rsp), %rax
	movq	%rax, 144(%r14)
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%eax, %ecx
	setne	172(%r14)
.LBB9_81:
	xorl	%ebp, %ebp
.LBB9_82:
.Ltmp97:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp98:
# BB#83:
	testl	%ebp, %ebp
	cmovnel	%ebp, %r12d
	jne	.LBB9_39
# BB#84:
	movb	$0, 173(%r14)
	movl	$0, 48(%rsp)
	movq	32(%r14), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp99:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rcx
	movl	$11, %edx
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp100:
# BB#85:
	testl	%ebp, %ebp
	jne	.LBB9_90
# BB#86:
	movzwl	48(%rsp), %eax
	testw	%ax, %ax
	je	.LBB9_89
# BB#87:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB9_90
# BB#88:
	movq	56(%rsp), %rax
	movq	%rax, 152(%r14)
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%eax, %ecx
	setne	173(%r14)
.LBB9_89:
	xorl	%ebp, %ebp
.LBB9_90:
.Ltmp105:
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp106:
# BB#91:
	testl	%ebp, %ebp
	cmovnel	%ebp, %r12d
	jne	.LBB9_39
# BB#92:
	movb	$0, 174(%r14)
	movl	$0, 64(%rsp)
	movq	32(%r14), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp107:
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rcx
	movl	$12, %edx
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp108:
# BB#93:
	testl	%ebp, %ebp
	jne	.LBB9_98
# BB#94:
	movzwl	64(%rsp), %eax
	testw	%ax, %ax
	je	.LBB9_97
# BB#95:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB9_98
# BB#96:
	movq	72(%rsp), %rax
	movq	%rax, 160(%r14)
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%eax, %ecx
	setne	174(%r14)
.LBB9_97:
	xorl	%ebp, %ebp
.LBB9_98:
.Ltmp113:
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp114:
# BB#99:
	testl	%ebp, %ebp
	movl	%ebp, %ebx
	cmovel	%r12d, %ebx
	jne	.LBB9_39
# BB#100:
	movb	$0, 144(%rsp)
	movq	32(%r14), %rax
	movq	(%rax), %rdi
.Ltmp115:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rcx
	movl	$21, %edx
	movl	%r15d, %esi
	callq	_Z22GetArchiveItemBoolPropP10IInArchivejjRb
.Ltmp116:
# BB#101:                               # %_ZNK4CArc10IsItemAntiEjRb.exit
	testl	%eax, %eax
	movl	%eax, %ebp
	cmovel	%ebx, %ebp
	je	.LBB9_103
# BB#102:                               # %.thread723
	movl	%eax, %ebp
	jmp	.LBB9_39
.LBB9_103:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp118:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	leaq	96(%rsp), %rdi
	leaq	64(%rsp), %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	callq	_Z16SplitPathToPartsRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp119:
# BB#104:
	movl	76(%rsp), %eax
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB9_114
# BB#105:
	movl	88(%r14), %ecx
	cmpl	$2, %ecx
	je	.LBB9_115
# BB#106:
	xorl	%r15d, %r15d
	cmpl	$1, %ecx
	jne	.LBB9_116
# BB#107:
	movl	244(%r14), %ecx
	movl	%ecx, %r15d
	cmpl	%ecx, %eax
	jle	.LBB9_114
# BB#108:                               # %.preheader711
	testl	%r15d, %r15d
	jle	.LBB9_155
# BB#109:                               # %.lr.ph
	movslq	%r15d, %r12
	xorl	%ebx, %ebx
.LBB9_110:                              # =>This Inner Loop Header: Depth=1
	movq	248(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	80(%rsp), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rax), %rdi
	movq	(%rcx), %rsi
.Ltmp121:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp122:
# BB#111:                               # %_ZNK11CStringBaseIwE13CompareNoCaseERKS0_.exit
                                        #   in Loop: Header=BB9_110 Depth=1
	testl	%eax, %eax
	jne	.LBB9_114
# BB#112:                               #   in Loop: Header=BB9_110 Depth=1
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB9_110
# BB#113:                               # %.thread.loopexit
	movl	76(%rsp), %eax
	jmp	.LBB9_117
.LBB9_114:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	jmp	.LBB9_307
.LBB9_115:
	leal	-1(%rax), %r15d
.LBB9_116:
	movl	%r12d, %ebp
.LBB9_117:                              # %.thread
	cmpl	%eax, %r15d
	cmovgl	%eax, %r15d
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	jle	.LBB9_124
# BB#118:                               # %.lr.ph.i
	movslq	8(%rsp), %r12           # 4-byte Folded Reload
	xorl	%r15d, %r15d
.LBB9_119:                              # =>This Inner Loop Header: Depth=1
	movq	80(%rsp), %rax
	movq	(%rax,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB9_123
# BB#120:                               #   in Loop: Header=BB9_119 Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_122
# BB#121:                               #   in Loop: Header=BB9_119 Depth=1
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_122:                              # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB9_119 Depth=1
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB9_123:                              #   in Loop: Header=BB9_119 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jl	.LBB9_119
.LBB9_124:                              # %._crit_edge.i
.Ltmp124:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp125:
# BB#125:                               # %_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii.exit
.Ltmp126:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_Z15MakeCorrectPathR13CObjectVectorI11CStringBaseIwEE
.Ltmp127:
# BB#126:
.Ltmp128:
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	leaq	224(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_Z21MakePathNameFromPartsRK13CObjectVectorI11CStringBaseIwEE
.Ltmp129:
# BB#127:
	leaq	160(%r14), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	cmpb	$0, 144(%rsp)
	je	.LBB9_131
.LBB9_128:                              # %.thread679
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	80(%r14), %ebx
	movslq	%ebx, %r15
	leaq	1(%r15), %r12
	testl	%r12d, %r12d
	je	.LBB9_140
# BB#129:                               # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp140:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp141:
# BB#130:                               # %.noexc304
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r12d, 28(%rsp)
	jmp	.LBB9_141
.LBB9_131:
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	jne	.LBB9_134
# BB#132:
	cmpl	$0, 76(%rsp)
	je	.LBB9_128
# BB#133:
.Ltmp131:
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp132:
.LBB9_134:
	cmpl	$0, 76(%rsp)
	je	.LBB9_128
# BB#135:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 192(%rsp)
.Ltmp133:
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp134:
# BB#136:
	movq	%rax, 192(%rsp)
	movl	$0, (%rax)
	movl	$4, 204(%rsp)
.Ltmp135:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rsi
	leaq	192(%rsp), %rdx
	movq	%r14, %rdi
	callq	_ZN23CArchiveExtractCallback22CreateComplexDirectoryERK13CObjectVectorI11CStringBaseIwEERS2_
.Ltmp136:
# BB#137:
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB9_166
# BB#138:
	movq	192(%rsp), %rdi
	cmpb	$0, 138(%r14)
	je	.LBB9_160
# BB#139:
	leaq	144(%r14), %rax
	xorl	%esi, %esi
	cmpb	$0, 172(%r14)
	cmovneq	%rax, %rsi
	cmpb	$0, 139(%r14)
	jne	.LBB9_161
	jmp	.LBB9_162
.LBB9_140:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
.LBB9_141:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	72(%r14), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB9_142:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_142
# BB#143:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%ebx, 24(%rsp)
	movl	232(%rsp), %eax
	movl	%r12d, %ecx
	subl	%ebx, %ecx
	cmpl	%eax, %ecx
	jg	.LBB9_175
# BB#144:
	decl	%ecx
	cmpl	$8, %r12d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB9_146
# BB#145:                               # %select.true.sink
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB9_146:                              # %select.end
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%r12,%rdi), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	cmpl	%r12d, %eax
	je	.LBB9_175
# BB#147:
	movslq	140(%rsp), %rax         # 4-byte Folded Reload
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp143:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, 40(%rsp)          # 8-byte Spill
.Ltmp144:
# BB#148:                               # %.noexc324
	testl	%r12d, %r12d
	jle	.LBB9_174
# BB#149:                               # %.preheader.i.i
	testl	%ebx, %ebx
	jle	.LBB9_159
# BB#150:                               # %.lr.ph.i.i
	cmpl	$7, %ebx
	jbe	.LBB9_156
# BB#151:                               # %min.iters.checked
	movq	%r15, %rax
	andq	$-8, %rax
	je	.LBB9_156
# BB#152:                               # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB9_168
# BB#153:                               # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB9_154:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movups	%xmm0, (%rdi,%rsi,4)
	movups	%xmm1, 16(%rdi,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB9_154
	jmp	.LBB9_169
.LBB9_155:
	movl	%ebx, %ebp
	jmp	.LBB9_117
.LBB9_156:
	xorl	%eax, %eax
.LBB9_157:                              # %scalar.ph.preheader
	subq	%rax, %r15
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rax,4), %rax
.LBB9_158:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	addq	$4, %rax
	decq	%r15
	jne	.LBB9_158
	jmp	.LBB9_173
.LBB9_159:                              # %._crit_edge.i.i
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jne	.LBB9_173
	jmp	.LBB9_174
.LBB9_160:
	xorl	%esi, %esi
	cmpb	$0, 139(%r14)
	je	.LBB9_162
.LBB9_161:
	leaq	152(%r14), %rax
	xorl	%edx, %edx
	cmpb	$0, 173(%r14)
	cmovneq	%rax, %rdx
	cmpb	$0, 140(%r14)
	jne	.LBB9_163
	jmp	.LBB9_164
.LBB9_162:
	xorl	%edx, %edx
	cmpb	$0, 140(%r14)
	je	.LBB9_164
.LBB9_163:
	cmpb	$0, 174(%r14)
	movq	216(%rsp), %rcx         # 8-byte Reload
	jne	.LBB9_165
.LBB9_164:
	movq	32(%r14), %rax
	xorl	%ecx, %ecx
	cmpb	$0, 56(%rax)
	leaq	48(%rax), %rax
	cmovneq	%rax, %rcx
.LBB9_165:
.Ltmp137:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile10NDirectory10SetDirTimeEPKwPK9_FILETIMES6_S6_
.Ltmp138:
.LBB9_166:                              # %._crit_edge
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_128
# BB#167:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB9_128
.LBB9_168:
	xorl	%esi, %esi
.LBB9_169:                              # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB9_172
# BB#170:                               # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	112(%rdx,%rsi,4), %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	112(%rdi,%rsi,4), %rsi
.LBB9_171:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB9_171
.LBB9_172:                              # %middle.block
	cmpq	%rax, %r15
	jne	.LBB9_157
.LBB9_173:                              # %._crit_edge.thread.i.i321
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movl	24(%rsp), %ebx
.LBB9_174:                              # %._crit_edge16.i.i322
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 16(%rsp)
	movslq	%ebx, %rax
	movl	$0, (%rcx,%rax,4)
	movl	140(%rsp), %eax         # 4-byte Reload
	movl	%eax, 28(%rsp)
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_175:                              # %.noexc.i
	movslq	%ebx, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	movq	224(%rsp), %rsi
.LBB9_176:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_176
# BB#177:
	movslq	232(%rsp), %r15
	addq	%rax, %r15
	movl	%r15d, 24(%rsp)
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB9_190
# BB#178:
	leaq	96(%r14), %r12
	leaq	16(%rsp), %rax
	cmpq	%r12, %rax
	je	.LBB9_188
# BB#179:
	movl	$0, 104(%r14)
	movq	96(%r14), %rbx
	movl	$0, (%rbx)
	incq	%r15
	movl	108(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB9_185
# BB#180:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp146:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r13
.Ltmp147:
# BB#181:                               # %.noexc316
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_184
# BB#182:                               # %.noexc316
	testl	%ebp, %ebp
	jle	.LBB9_184
# BB#183:                               # %._crit_edge.thread.i.i310
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	104(%r14), %rax
.LBB9_184:                              # %._crit_edge16.i.i311
	movq	%r13, 96(%r14)
	movl	$0, (%r13,%rax,4)
	movl	%r15d, 108(%r14)
	movq	%r13, %rbx
.LBB9_185:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i312
	movq	16(%rsp), %rax
.LBB9_186:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_186
# BB#187:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i315
	movl	24(%rsp), %eax
	movl	%eax, 104(%r14)
.LBB9_188:                              # %_ZN11CStringBaseIwEaSERKS0_.exit317
	xorl	%ebp, %ebp
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	cmpb	$0, 144(%rsp)
	je	.LBB9_303
# BB#189:
	movq	(%r12), %rdi
.Ltmp148:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile10NDirectory17MyRemoveDirectoryEPKw
.Ltmp149:
	jmp	.LBB9_303
.LBB9_190:
	cmpb	$0, 136(%r14)
	je	.LBB9_202
.LBB9_191:
	cmpb	$0, 144(%rsp)
	je	.LBB9_208
.LBB9_192:
	leaq	96(%r14), %rax
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	leaq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB9_303
# BB#193:
	movl	$0, 104(%r14)
	movq	96(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	108(%r14), %r13d
	cmpl	%r13d, %r15d
	je	.LBB9_199
# BB#194:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp242:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
.Ltmp243:
# BB#195:                               # %.noexc535
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_198
# BB#196:                               # %.noexc535
	testl	%r13d, %r13d
	jle	.LBB9_198
# BB#197:                               # %._crit_edge.thread.i.i529
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	104(%r14), %rax
.LBB9_198:                              # %._crit_edge16.i.i530
	movq	%r12, 96(%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 108(%r14)
	movq	%r12, %rbx
.LBB9_199:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i531
	movq	16(%rsp), %rax
.LBB9_200:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_200
# BB#201:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i534
	movl	24(%rsp), %eax
	movl	%eax, 104(%r14)
	jmp	.LBB9_303
.LBB9_202:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 280(%rsp)
.Ltmp150:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp151:
# BB#203:
	movq	%rax, 280(%rsp)
	movl	$0, (%rax)
	movl	$4, 292(%rsp)
	movq	16(%rsp), %rsi
.Ltmp152:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp153:
# BB#204:
	testb	%al, %al
	je	.LBB9_299
# BB#205:
	movl	92(%r14), %eax
	movl	$1, %r12d
	testl	%eax, %eax
	je	.LBB9_229
# BB#206:
	cmpl	$2, %eax
	jne	.LBB9_237
# BB#207:
	xorl	%ebp, %ebp
	jmp	.LBB9_300
.LBB9_208:
.Ltmp213:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %r12
.Ltmp214:
# BB#209:
	movl	$0, 8(%r12)
	movq	$_ZTV14COutFileStream+16, (%r12)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 16(%r12)
	movl	$-1, 24(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
.Ltmp215:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	_Znam
.Ltmp216:
# BB#210:
	movq	%rax, 32(%r12)
	movb	$0, (%rax)
	movl	$4, 44(%r12)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 16(%r12)
	movq	%r12, 200(%r14)
.Ltmp218:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*_ZTV14COutFileStream+24(%rip)
.Ltmp219:
# BB#211:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit486
	movq	200(%r14), %rdi
	movq	16(%rsp), %rsi
	movzbl	136(%r14), %eax
	leal	2(%rax,%rax), %edx
	movq	$0, 1104(%rdi)
	addq	$16, %rdi
.Ltmp220:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj
.Ltmp221:
# BB#212:                               # %_ZN14COutFileStream4OpenEPKwj.exit
	testb	%al, %al
	je	.LBB9_220
# BB#213:
	cmpb	$0, 136(%r14)
	je	.LBB9_216
# BB#214:
	movq	128(%r14), %rsi
	movq	200(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp230:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp231:
# BB#215:
	testl	%ebx, %ebx
	jne	.LBB9_228
.LBB9_216:
	movq	(%r12), %rax
.Ltmp235:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp236:
# BB#217:                               # %.noexc520
	movq	208(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_219
# BB#218:
	movq	(%rdi), %rax
.Ltmp237:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp238:
.LBB9_219:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit523.thread
	movq	%r12, 208(%r14)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%r12, (%rax)
	jmp	.LBB9_192
.LBB9_220:                              # %.preheader.preheader
.Ltmp222:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movl	$104, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp223:
# BB#221:                               # %.noexc499
	movaps	.LCPI9_21(%rip), %xmm0  # xmm0 = [99,97,110,32]
	movups	%xmm0, (%rbp)
	movaps	.LCPI9_22(%rip), %xmm0  # xmm0 = [110,111,116,32]
	movups	%xmm0, 16(%rbp)
	movaps	.LCPI9_23(%rip), %xmm0  # xmm0 = [111,112,101,110]
	movups	%xmm0, 32(%rbp)
	movaps	.LCPI9_24(%rip), %xmm0  # xmm0 = [32,111,117,116]
	movups	%xmm0, 48(%rbp)
	movaps	.LCPI9_25(%rip), %xmm0  # xmm0 = [112,117,116,32]
	movups	%xmm0, 64(%rbp)
	movaps	.LCPI9_26(%rip), %xmm0  # xmm0 = [102,105,108,101]
	movups	%xmm0, 80(%rbp)
	movq	$32, 96(%rbp)
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB9_224
# BB#222:
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	addl	$27, %ecx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp224:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbx
.Ltmp225:
# BB#223:                               # %._crit_edge16.i.i514
	movl	96(%rbp), %eax
	movl	%eax, 96(%rbx)
	movups	80(%rbp), %xmm0
	movups	%xmm0, 80(%rbx)
	movups	64(%rbp), %xmm0
	movups	%xmm0, 64(%rbx)
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	$0, 100(%rbx)
	movq	%rbx, %rbp
.LBB9_224:                              # %.noexc.i495
	leaq	100(%rbp), %rcx
	movq	16(%rsp), %rsi
.LBB9_225:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB9_225
# BB#226:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp227:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rsi
	callq	*72(%rax)
	movl	%eax, %ebx
.Ltmp228:
# BB#227:                               # %_ZN11CStringBaseIwED2Ev.exit518
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB9_228:
	movq	(%r12), %rax
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
.Ltmp232:
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp233:
	movl	%ebx, %ebp
	jmp	.LBB9_303
.LBB9_229:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	movq	16(%rsp), %rsi
	leaq	264(%rsp), %rdx
	movq	96(%rsp), %r8
	xorl	%ecx, %ecx
	cmpb	$0, 174(%r14)
	movq	216(%rsp), %r9          # 8-byte Reload
	cmoveq	%rcx, %r9
	cmpb	$0, 192(%r14)
	cmoveq	%rcx, %r13
.Ltmp154:
.Lcfi139:
	.cfi_escape 0x2e, 0x10
	leaq	240(%rsp), %rcx
	leaq	48(%rsp), %rbx
	pushq	%rbx
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$16, %rsp
.Lcfi142:
	.cfi_adjust_cfa_offset -16
.Ltmp155:
# BB#230:
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	je	.LBB9_232
# BB#231:
	movl	%eax, %ebp
	jmp	.LBB9_300
.LBB9_232:
	movl	48(%rsp), %edx
	cmpq	$5, %rdx
	ja	.LBB9_310
# BB#233:
	movl	$3, %eax
	movl	$-2147467260, %ecx      # imm = 0x80004004
	jmpq	*.LJTI9_0(,%rdx,8)
.LBB9_234:                              # %..thread682_crit_edge
	movl	92(%r14), %eax
	cmpl	$4, %eax
	jne	.LBB9_238
	jmp	.LBB9_252
.LBB9_235:
	movl	$1, %eax
.LBB9_236:                              # %.sink.split
	movl	%eax, 92(%r14)
.LBB9_237:
	cmpl	$4, %eax
	je	.LBB9_252
.LBB9_238:
	cmpl	$3, %eax
	jne	.LBB9_255
# BB#239:
.Ltmp184:
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	leaq	16(%rsp), %rdi
	callq	_Z14AutoRenamePathR11CStringBaseIwE
.Ltmp185:
# BB#240:
	testb	%al, %al
	jne	.LBB9_299
# BB#241:                               # %.preheader708.preheader
.Ltmp186:
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	movl	$168, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp187:
# BB#242:                               # %._crit_edge16.i.i.i332
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [69,82,82,79]
	movups	%xmm0, (%r15)
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [82,58,32,67]
	movups	%xmm0, 16(%r15)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [97,110,32,110]
	movups	%xmm0, 32(%r15)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [111,116,32,99]
	movups	%xmm0, 48(%r15)
	movaps	.LCPI9_4(%rip), %xmm0   # xmm0 = [114,101,97,116]
	movups	%xmm0, 64(%r15)
	movaps	.LCPI9_5(%rip), %xmm0   # xmm0 = [101,32,102,105]
	movups	%xmm0, 80(%r15)
	movaps	.LCPI9_6(%rip), %xmm0   # xmm0 = [108,101,32,119]
	movups	%xmm0, 96(%r15)
	movaps	.LCPI9_7(%rip), %xmm0   # xmm0 = [105,116,104,32]
	movups	%xmm0, 112(%r15)
	movaps	.LCPI9_8(%rip), %xmm0   # xmm0 = [97,117,116,111]
	movups	%xmm0, 128(%r15)
	movaps	.LCPI9_9(%rip), %xmm0   # xmm0 = [32,110,97,109]
	movups	%xmm0, 144(%r15)
	movq	$101, 160(%r15)
.Ltmp189:
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	movl	$168, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp190:
# BB#243:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i333
	movl	$0, (%rbx)
	xorl	%eax, %eax
.LBB9_244:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_244
# BB#245:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i336
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB9_248
# BB#246:
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	addl	$43, %ecx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp192:
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbp
.Ltmp193:
# BB#247:                               # %._crit_edge16.i.i357
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	movl	$164, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$0, 164(%rbp)
	movq	%rbp, %rbx
.LBB9_248:                              # %.noexc.i337
	movq	16(%rsp), %rax
	xorl	%ecx, %ecx
.LBB9_249:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, 164(%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_249
# BB#250:                               # %_ZN11CStringBaseIwED2Ev.exit345
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp195:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rsi
	callq	*72(%rax)
.Ltmp196:
# BB#251:
	testl	%eax, %eax
	movl	$-2147467259, %ebp      # imm = 0x80004005
	cmovnel	%eax, %ebp
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_268
.LBB9_252:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
	movslq	24(%rsp), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB9_269
# BB#253:                               # %._crit_edge16.i.i364
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp157:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp158:
# BB#254:                               # %.noexc369
	movq	%rax, 176(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 188(%rsp)
	jmp	.LBB9_270
.LBB9_255:
	movq	16(%rsp), %rdi
.Ltmp198:
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
.Ltmp199:
# BB#256:
	testb	%al, %al
	jne	.LBB9_299
# BB#257:                               # %.preheader707.preheader
.Ltmp201:
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	movl	$140, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp202:
# BB#258:                               # %._crit_edge16.i.i.i449
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [69,82,82,79]
	movups	%xmm0, (%r15)
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [82,58,32,67]
	movups	%xmm0, 16(%r15)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [97,110,32,110]
	movups	%xmm0, 32(%r15)
	movaps	.LCPI9_16(%rip), %xmm0  # xmm0 = [111,116,32,100]
	movups	%xmm0, 48(%r15)
	movaps	.LCPI9_17(%rip), %xmm0  # xmm0 = [101,108,101,116]
	movups	%xmm0, 64(%r15)
	movaps	.LCPI9_18(%rip), %xmm0  # xmm0 = [101,32,111,117]
	movups	%xmm0, 80(%r15)
	movaps	.LCPI9_19(%rip), %xmm0  # xmm0 = [116,112,117,116]
	movups	%xmm0, 96(%r15)
	movaps	.LCPI9_20(%rip), %xmm0  # xmm0 = [32,102,105,108]
	movups	%xmm0, 112(%r15)
	movabsq	$137438953573, %rax     # imm = 0x2000000065
	movq	%rax, 128(%r15)
	movl	$0, 136(%r15)
.Ltmp204:
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	movl	$140, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp205:
# BB#259:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i450
	movl	$0, (%rbx)
	xorl	%eax, %eax
.LBB9_260:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_260
# BB#261:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i453
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB9_264
# BB#262:
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	addl	$36, %ecx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp207:
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbp
.Ltmp208:
# BB#263:                               # %._crit_edge16.i.i474
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	movl	$136, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$0, 136(%rbp)
	movq	%rbp, %rbx
.LBB9_264:                              # %.noexc.i454
	movq	16(%rsp), %rax
	xorl	%ecx, %ecx
.LBB9_265:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, 136(%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_265
# BB#266:                               # %_ZN11CStringBaseIwED2Ev.exit462
.Lcfi159:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp210:
.Lcfi160:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rsi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp211:
# BB#267:
.Lcfi161:
	.cfi_escape 0x2e, 0x00
.LBB9_268:                              # %_ZN11CStringBaseIwED2Ev.exit361
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB9_300
.LBB9_269:
	xorl	%eax, %eax
.LBB9_270:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i365
	movq	16(%rsp), %rcx
.LBB9_271:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB9_271
# BB#272:
	movl	%r15d, 184(%rsp)
.Ltmp159:
.Lcfi162:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_Z14AutoRenamePathR11CStringBaseIwE
.Ltmp160:
# BB#273:
	testb	%al, %al
	je	.LBB9_287
# BB#274:
	movq	16(%rsp), %rdi
	movq	176(%rsp), %rsi
.Ltmp169:
.Lcfi163:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_
.Ltmp170:
# BB#275:
	xorl	%ebx, %ebx
	testb	%al, %al
	jne	.LBB9_296
# BB#276:                               # %.preheader709.preheader
.Ltmp172:
.Lcfi164:
	.cfi_escape 0x2e, 0x00
	movl	$148, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp173:
# BB#277:                               # %._crit_edge16.i.i.i406
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [69,82,82,79]
	movups	%xmm0, (%r15)
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [82,58,32,67]
	movups	%xmm0, 16(%r15)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [97,110,32,110]
	movups	%xmm0, 32(%r15)
	movaps	.LCPI9_10(%rip), %xmm0  # xmm0 = [111,116,32,114]
	movups	%xmm0, 48(%r15)
	movaps	.LCPI9_11(%rip), %xmm0  # xmm0 = [101,110,97,109]
	movups	%xmm0, 64(%r15)
	movaps	.LCPI9_12(%rip), %xmm0  # xmm0 = [101,32,101,120]
	movups	%xmm0, 80(%r15)
	movaps	.LCPI9_13(%rip), %xmm0  # xmm0 = [105,115,116,105]
	movups	%xmm0, 96(%r15)
	movaps	.LCPI9_14(%rip), %xmm0  # xmm0 = [110,103,32,102]
	movups	%xmm0, 112(%r15)
	movaps	.LCPI9_15(%rip), %xmm0  # xmm0 = [105,108,101,32]
	movups	%xmm0, 128(%r15)
	movl	$0, 144(%r15)
.Ltmp175:
.Lcfi165:
	.cfi_escape 0x2e, 0x00
	movl	$148, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp176:
# BB#278:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i407
	movl	$0, (%rbx)
	xorl	%eax, %eax
.LBB9_279:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_279
# BB#280:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i410
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB9_283
# BB#281:
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	addl	$38, %ecx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp178:
.Lcfi166:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbp
.Ltmp179:
# BB#282:                               # %._crit_edge16.i.i431
.Lcfi167:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
.Lcfi168:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$0, 144(%rbp)
	movq	%rbp, %rbx
.LBB9_283:                              # %.noexc.i411
	movq	16(%rsp), %rax
	xorl	%ecx, %ecx
.LBB9_284:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, 144(%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_284
# BB#285:                               # %_ZN11CStringBaseIwED2Ev.exit419
.Lcfi169:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp181:
.Lcfi170:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rsi
	callq	*72(%rax)
.Ltmp182:
# BB#286:
	testl	%eax, %eax
	movl	$-2147467259, %ebp      # imm = 0x80004005
	cmovnel	%eax, %ebp
.Lcfi171:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_295
.LBB9_287:                              # %.preheader710.preheader
.Ltmp161:
.Lcfi172:
	.cfi_escape 0x2e, 0x00
	movl	$168, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp162:
# BB#288:                               # %.noexc377
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [69,82,82,79]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [82,58,32,67]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [97,110,32,110]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [111,116,32,99]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_4(%rip), %xmm0   # xmm0 = [114,101,97,116]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI9_5(%rip), %xmm0   # xmm0 = [101,32,102,105]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI9_6(%rip), %xmm0   # xmm0 = [108,101,32,119]
	movups	%xmm0, 96(%rbx)
	movaps	.LCPI9_7(%rip), %xmm0   # xmm0 = [105,116,104,32]
	movups	%xmm0, 112(%rbx)
	movaps	.LCPI9_8(%rip), %xmm0   # xmm0 = [97,117,116,111]
	movups	%xmm0, 128(%rbx)
	movaps	.LCPI9_9(%rip), %xmm0   # xmm0 = [32,110,97,109]
	movups	%xmm0, 144(%rbx)
	movq	$101, 160(%rbx)
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB9_291
# BB#289:
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	addl	$43, %ecx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp163:
.Lcfi173:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbp
.Ltmp164:
# BB#290:                               # %._crit_edge16.i.i391
.Lcfi174:
	.cfi_escape 0x2e, 0x00
	movl	$164, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
.Lcfi175:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$0, 164(%rbp)
	movq	%rbp, %rbx
.LBB9_291:                              # %.noexc.i373
	leaq	164(%rbx), %rcx
	movq	16(%rsp), %rsi
.LBB9_292:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB9_292
# BB#293:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp166:
.Lcfi176:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rsi
	callq	*72(%rax)
.Ltmp167:
# BB#294:                               # %_ZN11CStringBaseIwED2Ev.exit395
	testl	%eax, %eax
	movl	$-2147467259, %ebp      # imm = 0x80004005
	cmovnel	%eax, %ebp
.Lcfi177:
	.cfi_escape 0x2e, 0x00
.LBB9_295:                              # %_ZN11CStringBaseIwED2Ev.exit435
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$1, %ebx
.LBB9_296:                              # %_ZN11CStringBaseIwED2Ev.exit435
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_298
# BB#297:
.Lcfi178:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_298:                              # %_ZN11CStringBaseIwED2Ev.exit438
	testl	%ebx, %ebx
	jne	.LBB9_300
.LBB9_299:
	xorl	%r12d, %r12d
.LBB9_300:                              # %_ZN11CStringBaseIwED2Ev.exit361
	movq	280(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_302
# BB#301:
.Lcfi179:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_302:                              # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	testl	%r12d, %r12d
	je	.LBB9_191
.LBB9_303:                              # %_ZN11CStringBaseIwEaSERKS0_.exit536
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_305
# BB#304:
.Lcfi180:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_305:                              # %_ZN11CStringBaseIwED2Ev.exit537
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_307
# BB#306:
.Lcfi181:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_307:                              # %.loopexit
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp253:
.Lcfi182:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp254:
# BB#308:
.Ltmp259:
.Lcfi183:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp260:
# BB#309:
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB9_39
	jmp	.LBB9_48
.LBB9_310:
	movl	$-2147467259, %ecx      # imm = 0x80004005
	movl	%ecx, %ebp
	jmp	.LBB9_300
.LBB9_311:
	movl	$2, 92(%r14)
.LBB9_312:
	xorl	%ecx, %ecx
.LBB9_313:                              # %.thread680
	movl	%ecx, %ebp
	jmp	.LBB9_300
.LBB9_314:
.Ltmp180:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi184:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB9_317
.LBB9_315:
.Ltmp183:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi185:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_323
.LBB9_316:
.Ltmp177:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_317:                              # %_ZN11CStringBaseIwED2Ev.exit436
.Lcfi186:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB9_335
.LBB9_318:
.Ltmp174:
	jmp	.LBB9_334
.LBB9_319:                              # %_ZN11CStringBaseIwED2Ev.exit.i376
.Ltmp165:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi187:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_323
.LBB9_320:
.Ltmp209:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi188:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB9_326
.LBB9_321:
.Ltmp194:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi189:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB9_331
.LBB9_322:                              # %_ZN11CStringBaseIwED2Ev.exit396
.Ltmp168:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi190:
	.cfi_escape 0x2e, 0x00
.LBB9_323:                              # %_ZN11CStringBaseIwED2Ev.exit437
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB9_335
.LBB9_324:
.Ltmp212:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi191:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_329
.LBB9_325:
.Ltmp206:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_326:                              # %_ZN11CStringBaseIwED2Ev.exit479
.Lcfi192:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	jmp	.LBB9_337
.LBB9_327:
.Ltmp203:
	jmp	.LBB9_345
.LBB9_328:
.Ltmp197:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi193:
	.cfi_escape 0x2e, 0x00
.LBB9_329:                              # %_ZN11CStringBaseIwED2Ev.exit363
	movq	%rbx, %rdi
	jmp	.LBB9_337
.LBB9_330:
.Ltmp191:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_331:                              # %_ZN11CStringBaseIwED2Ev.exit362
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	jmp	.LBB9_337
.LBB9_332:
.Ltmp188:
	jmp	.LBB9_345
.LBB9_333:
.Ltmp171:
.LBB9_334:                              # %_ZN11CStringBaseIwED2Ev.exit437
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_335:                              # %_ZN11CStringBaseIwED2Ev.exit437
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_346
# BB#336:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
.LBB9_337:                              # %_ZN11CStringBaseIwED2Ev.exit363
	callq	_ZdaPv
	jmp	.LBB9_346
.LBB9_338:
.Ltmp156:
	jmp	.LBB9_345
.LBB9_339:                              # %_ZN11CStringBaseIwED2Ev.exit.i498
.Ltmp226:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_341
.LBB9_340:                              # %_ZN11CStringBaseIwED2Ev.exit519
.Ltmp229:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi197:
	.cfi_escape 0x2e, 0x00
.LBB9_341:
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB9_349
.LBB9_342:
.Ltmp234:
	jmp	.LBB9_356
.LBB9_343:
.Ltmp217:
	movq	%rdx, %r13
	movq	%rax, %r14
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB9_357
.LBB9_344:
.Ltmp200:
.LBB9_345:                              # %_ZN11CStringBaseIwED2Ev.exit363
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_346:                              # %_ZN11CStringBaseIwED2Ev.exit363
	movq	280(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_357
# BB#347:
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB9_357
.LBB9_348:
.Ltmp239:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_349:
	movq	(%r12), %rax
.Ltmp240:
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp241:
	jmp	.LBB9_357
.LBB9_350:
.Ltmp145:
	movq	%rdx, %r13
	movq	%rax, %r14
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB9_360
# BB#351:
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB9_359
.LBB9_352:
.Ltmp139:
	movq	%rdx, %r13
	movq	%rax, %r14
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_360
# BB#353:
.Lcfi202:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB9_359
.LBB9_354:
.Ltmp142:
	movq	%rdx, %r13
	movq	%rax, %r14
	jmp	.LBB9_360
.LBB9_355:
.Ltmp244:
.LBB9_356:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit525
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_357:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit525
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_360
# BB#358:
.Lcfi203:
	.cfi_escape 0x2e, 0x00
.LBB9_359:
	callq	_ZdaPv
.LBB9_360:
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_362
# BB#361:
.Lcfi204:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB9_362:                              # %_ZN11CStringBaseIwED2Ev.exit543
	leaq	64(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB9_383
.LBB9_363:
.Ltmp261:
	jmp	.LBB9_392
.LBB9_364:
.Ltmp255:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp256:
.Lcfi205:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp257:
	jmp	.LBB9_393
.LBB9_365:
.Ltmp258:
.Lcfi206:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_366:
.Ltmp120:
	jmp	.LBB9_382
.LBB9_367:
.Ltmp130:
	jmp	.LBB9_382
.LBB9_368:
.Ltmp117:
	jmp	.LBB9_392
.LBB9_369:
.Ltmp109:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp110:
.Lcfi207:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp111:
	jmp	.LBB9_393
.LBB9_370:
.Ltmp112:
.Lcfi208:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_371:
.Ltmp101:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp102:
.Lcfi209:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp103:
	jmp	.LBB9_393
.LBB9_372:
.Ltmp104:
.Lcfi210:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_373:
.Ltmp93:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp94:
.Lcfi211:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp95:
	jmp	.LBB9_393
.LBB9_374:
.Ltmp96:
.Lcfi212:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_375:
.Ltmp90:
	jmp	.LBB9_392
.LBB9_376:
.Ltmp85:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp86:
.Lcfi213:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp87:
	jmp	.LBB9_393
.LBB9_377:
.Ltmp277:
	movq	%rdx, %r13
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB9_393
# BB#378:
	movq	(%r15), %rax
.Ltmp278:
.Lcfi214:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp279:
	jmp	.LBB9_393
.LBB9_379:
.Ltmp71:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp72:
.Lcfi215:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp73:
	jmp	.LBB9_393
.LBB9_380:
.Ltmp74:
.Lcfi216:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_381:
.Ltmp123:
.LBB9_382:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_383:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp245:
.Lcfi217:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp246:
# BB#384:
.Ltmp251:
.Lcfi218:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp252:
	jmp	.LBB9_393
.LBB9_385:
.Ltmp247:
	movq	%rax, %rbp
.Ltmp248:
.Lcfi219:
	.cfi_escape 0x2e, 0x00
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp249:
	jmp	.LBB9_390
.LBB9_386:
.Ltmp250:
.Lcfi220:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_387:
.Ltmp64:
	jmp	.LBB9_392
.LBB9_388:
.Ltmp59:
	movq	%rdx, %r13
	movq	%rax, %r14
.Ltmp60:
.Lcfi221:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp61:
	jmp	.LBB9_393
.LBB9_389:
.Ltmp280:
	movq	%rax, %rbp
.LBB9_390:                              # %.body545
.Lcfi222:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.LBB9_391:
.Ltmp270:
.LBB9_392:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit555
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_393:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit555
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_396
# BB#394:
.Lcfi223:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB9_396
.LBB9_395:
.Ltmp50:
	movq	%rdx, %r13
	movq	%rax, %r14
.LBB9_396:
.Lcfi224:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r13d
	je	.LBB9_398
# BB#397:
.Lcfi225:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB9_41
.LBB9_398:
.Lcfi226:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp281:
.Lcfi227:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp282:
# BB#399:
.LBB9_400:
.Ltmp283:
	movq	%rax, %rbx
.Lcfi228:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Lcfi229:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami, .Lfunc_end9-_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_234
	.quad	.LBB9_235
	.quad	.LBB9_312
	.quad	.LBB9_311
	.quad	.LBB9_236
	.quad	.LBB9_313
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\304\007"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\265\007"              # Call site table length
	.long	.Ltmp44-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp49-.Ltmp44         #   Call between .Ltmp44 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin7   #     jumps to .Ltmp50
	.byte	3                       #   On action: 2
	.long	.Ltmp51-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp56-.Ltmp51         #   Call between .Ltmp51 and .Ltmp56
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp57-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin7   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp62-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin7   #     jumps to .Ltmp64
	.byte	3                       #   On action: 2
	.long	.Ltmp65-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp67-.Lfunc_begin7   # >> Call Site 6 <<
	.long	.Ltmp70-.Ltmp67         #   Call between .Ltmp67 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin7   #     jumps to .Ltmp71
	.byte	3                       #   On action: 2
	.long	.Ltmp75-.Lfunc_begin7   # >> Call Site 7 <<
	.long	.Ltmp269-.Ltmp75        #   Call between .Ltmp75 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp271-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp276-.Ltmp271       #   Call between .Ltmp271 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin7  #     jumps to .Ltmp277
	.byte	3                       #   On action: 2
	.long	.Ltmp79-.Lfunc_begin7   # >> Call Site 9 <<
	.long	.Ltmp82-.Ltmp79         #   Call between .Ltmp79 and .Ltmp82
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp83-.Lfunc_begin7   # >> Call Site 10 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin7   #     jumps to .Ltmp85
	.byte	3                       #   On action: 2
	.long	.Ltmp88-.Lfunc_begin7   # >> Call Site 11 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin7   #     jumps to .Ltmp90
	.byte	3                       #   On action: 2
	.long	.Ltmp91-.Lfunc_begin7   # >> Call Site 12 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin7   #     jumps to .Ltmp93
	.byte	3                       #   On action: 2
	.long	.Ltmp97-.Lfunc_begin7   # >> Call Site 13 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp99-.Lfunc_begin7   # >> Call Site 14 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin7  #     jumps to .Ltmp101
	.byte	3                       #   On action: 2
	.long	.Ltmp105-.Lfunc_begin7  # >> Call Site 15 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin7  # >> Call Site 16 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin7  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp113-.Lfunc_begin7  # >> Call Site 17 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	3                       #   On action: 2
	.long	.Ltmp115-.Lfunc_begin7  # >> Call Site 18 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin7  #     jumps to .Ltmp117
	.byte	3                       #   On action: 2
	.long	.Ltmp118-.Lfunc_begin7  # >> Call Site 19 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin7  #     jumps to .Ltmp120
	.byte	3                       #   On action: 2
	.long	.Ltmp121-.Lfunc_begin7  # >> Call Site 20 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin7  #     jumps to .Ltmp123
	.byte	3                       #   On action: 2
	.long	.Ltmp124-.Lfunc_begin7  # >> Call Site 21 <<
	.long	.Ltmp129-.Ltmp124       #   Call between .Ltmp124 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin7  #     jumps to .Ltmp130
	.byte	3                       #   On action: 2
	.long	.Ltmp140-.Lfunc_begin7  # >> Call Site 22 <<
	.long	.Ltmp134-.Ltmp140       #   Call between .Ltmp140 and .Ltmp134
	.long	.Ltmp142-.Lfunc_begin7  #     jumps to .Ltmp142
	.byte	3                       #   On action: 2
	.long	.Ltmp135-.Lfunc_begin7  # >> Call Site 23 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp139-.Lfunc_begin7  #     jumps to .Ltmp139
	.byte	3                       #   On action: 2
	.long	.Ltmp143-.Lfunc_begin7  # >> Call Site 24 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin7  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp137-.Lfunc_begin7  # >> Call Site 25 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin7  #     jumps to .Ltmp139
	.byte	3                       #   On action: 2
	.long	.Ltmp146-.Lfunc_begin7  # >> Call Site 26 <<
	.long	.Ltmp151-.Ltmp146       #   Call between .Ltmp146 and .Ltmp151
	.long	.Ltmp244-.Lfunc_begin7  #     jumps to .Ltmp244
	.byte	3                       #   On action: 2
	.long	.Ltmp152-.Lfunc_begin7  # >> Call Site 27 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp200-.Lfunc_begin7  #     jumps to .Ltmp200
	.byte	3                       #   On action: 2
	.long	.Ltmp213-.Lfunc_begin7  # >> Call Site 28 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp244-.Lfunc_begin7  #     jumps to .Ltmp244
	.byte	3                       #   On action: 2
	.long	.Ltmp215-.Lfunc_begin7  # >> Call Site 29 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin7  #     jumps to .Ltmp217
	.byte	3                       #   On action: 2
	.long	.Ltmp218-.Lfunc_begin7  # >> Call Site 30 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp244-.Lfunc_begin7  #     jumps to .Ltmp244
	.byte	3                       #   On action: 2
	.long	.Ltmp220-.Lfunc_begin7  # >> Call Site 31 <<
	.long	.Ltmp223-.Ltmp220       #   Call between .Ltmp220 and .Ltmp223
	.long	.Ltmp239-.Lfunc_begin7  #     jumps to .Ltmp239
	.byte	3                       #   On action: 2
	.long	.Ltmp224-.Lfunc_begin7  # >> Call Site 32 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin7  #     jumps to .Ltmp226
	.byte	3                       #   On action: 2
	.long	.Ltmp227-.Lfunc_begin7  # >> Call Site 33 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin7  #     jumps to .Ltmp229
	.byte	3                       #   On action: 2
	.long	.Ltmp232-.Lfunc_begin7  # >> Call Site 34 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin7  #     jumps to .Ltmp234
	.byte	3                       #   On action: 2
	.long	.Ltmp154-.Lfunc_begin7  # >> Call Site 35 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin7  #     jumps to .Ltmp156
	.byte	3                       #   On action: 2
	.long	.Ltmp184-.Lfunc_begin7  # >> Call Site 36 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp200-.Lfunc_begin7  #     jumps to .Ltmp200
	.byte	3                       #   On action: 2
	.long	.Ltmp186-.Lfunc_begin7  # >> Call Site 37 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin7  #     jumps to .Ltmp188
	.byte	3                       #   On action: 2
	.long	.Ltmp189-.Lfunc_begin7  # >> Call Site 38 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin7  #     jumps to .Ltmp191
	.byte	3                       #   On action: 2
	.long	.Ltmp192-.Lfunc_begin7  # >> Call Site 39 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin7  #     jumps to .Ltmp194
	.byte	3                       #   On action: 2
	.long	.Ltmp193-.Lfunc_begin7  # >> Call Site 40 <<
	.long	.Ltmp195-.Ltmp193       #   Call between .Ltmp193 and .Ltmp195
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin7  # >> Call Site 41 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin7  #     jumps to .Ltmp197
	.byte	3                       #   On action: 2
	.long	.Ltmp157-.Lfunc_begin7  # >> Call Site 42 <<
	.long	.Ltmp199-.Ltmp157       #   Call between .Ltmp157 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin7  #     jumps to .Ltmp200
	.byte	3                       #   On action: 2
	.long	.Ltmp201-.Lfunc_begin7  # >> Call Site 43 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin7  #     jumps to .Ltmp203
	.byte	3                       #   On action: 2
	.long	.Ltmp204-.Lfunc_begin7  # >> Call Site 44 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin7  #     jumps to .Ltmp206
	.byte	3                       #   On action: 2
	.long	.Ltmp207-.Lfunc_begin7  # >> Call Site 45 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin7  #     jumps to .Ltmp209
	.byte	3                       #   On action: 2
	.long	.Ltmp208-.Lfunc_begin7  # >> Call Site 46 <<
	.long	.Ltmp210-.Ltmp208       #   Call between .Ltmp208 and .Ltmp210
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin7  # >> Call Site 47 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin7  #     jumps to .Ltmp212
	.byte	3                       #   On action: 2
	.long	.Ltmp159-.Lfunc_begin7  # >> Call Site 48 <<
	.long	.Ltmp170-.Ltmp159       #   Call between .Ltmp159 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin7  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp172-.Lfunc_begin7  # >> Call Site 49 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin7  #     jumps to .Ltmp174
	.byte	3                       #   On action: 2
	.long	.Ltmp175-.Lfunc_begin7  # >> Call Site 50 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin7  #     jumps to .Ltmp177
	.byte	3                       #   On action: 2
	.long	.Ltmp178-.Lfunc_begin7  # >> Call Site 51 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin7  #     jumps to .Ltmp180
	.byte	3                       #   On action: 2
	.long	.Ltmp179-.Lfunc_begin7  # >> Call Site 52 <<
	.long	.Ltmp181-.Ltmp179       #   Call between .Ltmp179 and .Ltmp181
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin7  # >> Call Site 53 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin7  #     jumps to .Ltmp183
	.byte	3                       #   On action: 2
	.long	.Ltmp161-.Lfunc_begin7  # >> Call Site 54 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp171-.Lfunc_begin7  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp163-.Lfunc_begin7  # >> Call Site 55 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin7  #     jumps to .Ltmp165
	.byte	3                       #   On action: 2
	.long	.Ltmp164-.Lfunc_begin7  # >> Call Site 56 <<
	.long	.Ltmp166-.Ltmp164       #   Call between .Ltmp164 and .Ltmp166
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin7  # >> Call Site 57 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin7  #     jumps to .Ltmp168
	.byte	3                       #   On action: 2
	.long	.Ltmp253-.Lfunc_begin7  # >> Call Site 58 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin7  #     jumps to .Ltmp255
	.byte	3                       #   On action: 2
	.long	.Ltmp259-.Lfunc_begin7  # >> Call Site 59 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin7  #     jumps to .Ltmp261
	.byte	3                       #   On action: 2
	.long	.Ltmp240-.Lfunc_begin7  # >> Call Site 60 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp280-.Lfunc_begin7  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp256-.Lfunc_begin7  # >> Call Site 61 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin7  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp110-.Lfunc_begin7  # >> Call Site 62 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin7  #     jumps to .Ltmp112
	.byte	1                       #   On action: 1
	.long	.Ltmp102-.Lfunc_begin7  # >> Call Site 63 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin7  #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin7   # >> Call Site 64 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin7   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 65 <<
	.long	.Ltmp279-.Ltmp86        #   Call between .Ltmp86 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin7  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp72-.Lfunc_begin7   # >> Call Site 66 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin7   #     jumps to .Ltmp74
	.byte	1                       #   On action: 1
	.long	.Ltmp245-.Lfunc_begin7  # >> Call Site 67 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin7  #     jumps to .Ltmp247
	.byte	1                       #   On action: 1
	.long	.Ltmp251-.Lfunc_begin7  # >> Call Site 68 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp280-.Lfunc_begin7  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp248-.Lfunc_begin7  # >> Call Site 69 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin7  #     jumps to .Ltmp250
	.byte	1                       #   On action: 1
	.long	.Ltmp60-.Lfunc_begin7   # >> Call Site 70 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp280-.Lfunc_begin7  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin7   # >> Call Site 71 <<
	.long	.Ltmp281-.Ltmp61        #   Call between .Ltmp61 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin7  # >> Call Site 72 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin7  #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin7  # >> Call Site 73 <<
	.long	.Lfunc_end9-.Ltmp282    #   Call between .Ltmp282 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi230:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi233:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi234:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 64
.Lcfi237:
	.cfi_offset %rbx, -56
.Lcfi238:
	.cfi_offset %r12, -48
.Lcfi239:
	.cfi_offset %r13, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB10_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZdaPv
.LBB10_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB10_2
.LBB10_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end10:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end10-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi245:
	.cfi_def_cfa_offset 32
.Lcfi246:
	.cfi_offset %rbx, -24
.Lcfi247:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp284:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp285:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB11_2:
.Ltmp286:
	movq	%rax, %r14
.Ltmp287:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_4:
.Ltmp289:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end11-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp284-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin8  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp287-.Ltmp285       #   Call between .Ltmp285 and .Ltmp287
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin8  #     jumps to .Ltmp289
	.byte	1                       #   On action: 1
	.long	.Ltmp288-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp288   #   Call between .Ltmp288 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback16PrepareOperationEi
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback16PrepareOperationEi,@function
_ZN23CArchiveExtractCallback16PrepareOperationEi: # @_ZN23CArchiveExtractCallback16PrepareOperationEi
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 32
.Lcfi251:
	.cfi_offset %rbx, -24
.Lcfi252:
	.cfi_offset %r14, -16
	movl	%esi, %eax
	movb	$0, 137(%rdi)
	testl	%eax, %eax
	jne	.LBB12_3
# BB#1:
	movl	$1, %eax
	cmpb	$0, 265(%rdi)
	jne	.LBB12_3
# BB#2:
	movb	$1, 137(%rdi)
	xorl	%eax, %eax
.LBB12_3:
	movq	48(%rdi), %rcx
	movq	112(%rdi), %rsi
	movq	(%rcx), %rbx
	movq	%rdi, %rdx
	subq	$-128, %rdx
	xorl	%r8d, %r8d
	cmpb	$0, 136(%rdi)
	cmovneq	%rdx, %r8
	movzbl	176(%rdi), %edx
.Ltmp290:
	movq	%rcx, %rdi
	movl	%eax, %ecx
	callq	*64(%rbx)
.Ltmp291:
.LBB12_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_4:
.Ltmp292:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB12_5
# BB#7:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB12_8
.LBB12_5:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp293:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp294:
# BB#9:
.LBB12_6:
.Ltmp295:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN23CArchiveExtractCallback16PrepareOperationEi, .Lfunc_end12-_ZN23CArchiveExtractCallback16PrepareOperationEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp290-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin9  #     jumps to .Ltmp292
	.byte	3                       #   On action: 2
	.long	.Ltmp291-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp293-.Ltmp291       #   Call between .Ltmp291 and .Ltmp293
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin9  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp294   #   Call between .Ltmp294 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback18SetOperationResultEi
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback18SetOperationResultEi,@function
_ZN23CArchiveExtractCallback18SetOperationResultEi: # @_ZN23CArchiveExtractCallback18SetOperationResultEi
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi256:
	.cfi_def_cfa_offset 48
.Lcfi257:
	.cfi_offset %rbx, -32
.Lcfi258:
	.cfi_offset %r14, -24
.Lcfi259:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	$4, %r14d
	jae	.LBB13_1
# BB#9:
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_12
# BB#10:
	movq	216(%rbx), %rax
	movl	32(%rax), %ecx
	notl	%ecx
	addl	%ecx, 328(%rbx)
	movq	24(%rax), %rax
	movq	%rax, 184(%rbx)
	movb	$1, 192(%rbx)
	movq	(%rdi), %rax
.Ltmp298:
	callq	*16(%rax)
.Ltmp299:
# BB#11:                                # %.noexc31
	movq	$0, 224(%rbx)
.LBB13_12:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit32
	cmpq	$0, 208(%rbx)
	je	.LBB13_13
# BB#14:
	movq	200(%rbx), %rdi
	cmpb	$0, 138(%rbx)
	je	.LBB13_15
# BB#16:
	leaq	144(%rbx), %rax
	xorl	%esi, %esi
	cmpb	$0, 172(%rbx)
	cmovneq	%rax, %rsi
	cmpb	$0, 139(%rbx)
	jne	.LBB13_19
	jmp	.LBB13_18
.LBB13_1:
	movq	208(%rbx), %rdi
	movl	$-2147467259, %ebp      # imm = 0x80004005
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#2:
	movq	(%rdi), %rax
.Ltmp296:
	callq	*16(%rax)
.Ltmp297:
# BB#3:                                 # %.noexc
	movq	$0, 208(%rbx)
	jmp	.LBB13_4
.LBB13_13:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit32._ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit35_crit_edge
	leaq	192(%rbx), %rbp
	cmpb	$0, (%rbp)
	jne	.LBB13_38
	jmp	.LBB13_31
.LBB13_15:
	xorl	%esi, %esi
	cmpb	$0, 139(%rbx)
	je	.LBB13_18
.LBB13_19:
	leaq	152(%rbx), %rax
	xorl	%edx, %edx
	cmpb	$0, 173(%rbx)
	cmovneq	%rax, %rdx
	cmpb	$0, 140(%rbx)
	jne	.LBB13_21
	jmp	.LBB13_23
.LBB13_18:
	xorl	%edx, %edx
	cmpb	$0, 140(%rbx)
	je	.LBB13_23
.LBB13_21:
	cmpb	$0, 174(%rbx)
	je	.LBB13_23
# BB#22:
	leaq	160(%rbx), %rcx
	jmp	.LBB13_24
.LBB13_23:
	movq	32(%rbx), %rax
	xorl	%ecx, %ecx
	cmpb	$0, 56(%rax)
	leaq	48(%rax), %rax
	cmovneq	%rax, %rcx
.LBB13_24:
	addq	$16, %rdi
.Ltmp300:
	callq	_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_
.Ltmp301:
# BB#25:                                # %_ZN14COutFileStream7SetTimeEPK9_FILETIMES2_S2_.exit
	movq	200(%rbx), %rdi
	movq	1104(%rdi), %rax
	movq	%rax, 184(%rbx)
	movb	$1, 192(%rbx)
.Ltmp302:
	callq	_ZN14COutFileStream5CloseEv
	movl	%eax, %ebp
.Ltmp303:
# BB#26:
	testl	%ebp, %ebp
	jne	.LBB13_4
# BB#27:
	leaq	192(%rbx), %rbp
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_30
# BB#28:
	movq	(%rdi), %rax
.Ltmp304:
	callq	*16(%rax)
.Ltmp305:
# BB#29:                                # %.noexc34
	movq	$0, 208(%rbx)
.LBB13_30:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit35
	cmpb	$0, (%rbp)
	jne	.LBB13_38
.LBB13_31:
	movl	$0, (%rsp)
	movq	32(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movl	180(%rbx), %esi
.Ltmp306:
	movq	%rsp, %rcx
	movl	$7, %edx
	callq	*64(%rax)
.Ltmp307:
# BB#32:
	testl	%eax, %eax
	jne	.LBB13_36
# BB#33:
	cmpw	$0, (%rsp)
	setne	(%rbp)
	je	.LBB13_36
# BB#34:
.Ltmp308:
	movq	%rsp, %rdi
	callq	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
.Ltmp309:
# BB#35:
	movq	%rax, 184(%rbx)
.LBB13_36:
.Ltmp314:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp315:
# BB#37:
	cmpb	$0, (%rbp)
	je	.LBB13_39
.LBB13_38:                              # %.thread
	movq	184(%rbx), %rax
	addq	%rax, 320(%rbx)
.LBB13_39:
	leaq	312(%rbx), %rax
	leaq	304(%rbx), %rcx
	cmpb	$0, 176(%rbx)
	cmovneq	%rcx, %rax
	incq	(%rax)
	cmpb	$0, 137(%rbx)
	je	.LBB13_42
# BB#40:
	cmpb	$0, 175(%rbx)
	je	.LBB13_42
# BB#41:
	movq	96(%rbx), %rdi
	movl	168(%rbx), %esi
.Ltmp316:
	callq	_ZN8NWindows5NFile10NDirectory19MySetFileAttributesEPKwj
.Ltmp317:
.LBB13_42:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movzbl	141(%rbx), %edx
.Ltmp318:
	movl	%r14d, %esi
	callq	*80(%rax)
	movl	%eax, %ebp
.Ltmp319:
.LBB13_4:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB13_43:
.Ltmp310:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.Ltmp311:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp312:
	jmp	.LBB13_6
.LBB13_44:
.Ltmp313:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_5:
.Ltmp320:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.LBB13_6:                               # %.body
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB13_7
# BB#8:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB13_4
.LBB13_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp321:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp322:
# BB#46:
.LBB13_45:
.Ltmp323:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN23CArchiveExtractCallback18SetOperationResultEi, .Lfunc_end13-_ZN23CArchiveExtractCallback18SetOperationResultEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	105                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp298-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp305-.Ltmp298       #   Call between .Ltmp298 and .Ltmp305
	.long	.Ltmp320-.Lfunc_begin10 #     jumps to .Ltmp320
	.byte	3                       #   On action: 2
	.long	.Ltmp306-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp309-.Ltmp306       #   Call between .Ltmp306 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin10 #     jumps to .Ltmp310
	.byte	3                       #   On action: 2
	.long	.Ltmp314-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp319-.Ltmp314       #   Call between .Ltmp314 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin10 #     jumps to .Ltmp320
	.byte	3                       #   On action: 2
	.long	.Ltmp311-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin10 #     jumps to .Ltmp313
	.byte	1                       #   On action: 1
	.long	.Ltmp312-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp321-.Ltmp312       #   Call between .Ltmp312 and .Ltmp321
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin10 #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Lfunc_end13-.Ltmp322   #   Call between .Ltmp322 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw,@function
_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw: # @_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi261:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi262:
	.cfi_def_cfa_offset 32
.Lcfi263:
	.cfi_offset %rbx, -24
.Lcfi264:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_1
.LBB14_4:
	movq	(%rdi), %rax
.Ltmp326:
	movq	%r14, %rsi
	callq	*40(%rax)
.Ltmp327:
	jmp	.LBB14_9
.LBB14_1:
	movq	48(%rbx), %rdi
	addq	$64, %rbx
	movq	(%rdi), %rax
.Ltmp324:
	movl	$IID_ICryptoGetTextPassword, %esi
	movq	%rbx, %rdx
	callq	*(%rax)
.Ltmp325:
# BB#2:                                 # %_ZNK9CMyComPtrI29IFolderArchiveExtractCallbackE14QueryInterfaceI22ICryptoGetTextPasswordEEiRK4GUIDPPT_.exit
	testl	%eax, %eax
	je	.LBB14_3
.LBB14_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB14_3:                               # %_ZNK9CMyComPtrI29IFolderArchiveExtractCallbackE14QueryInterfaceI22ICryptoGetTextPasswordEEiRK4GUIDPPT_.exit._crit_edge
	movq	(%rbx), %rdi
	jmp	.LBB14_4
.LBB14_5:
.Ltmp328:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB14_6
# BB#8:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB14_9
.LBB14_6:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp329:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp330:
# BB#10:
.LBB14_7:
.Ltmp331:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw, .Lfunc_end14-_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp326-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp325-.Ltmp326       #   Call between .Ltmp326 and .Ltmp325
	.long	.Ltmp328-.Lfunc_begin11 #     jumps to .Ltmp328
	.byte	3                       #   On action: 2
	.long	.Ltmp325-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp329-.Ltmp325       #   Call between .Ltmp325 and .Ltmp329
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin11 #     jumps to .Ltmp331
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp330   #   Call between .Ltmp330 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw,@function
_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw: # @_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw # TAILCALL
.Lfunc_end15:
	.size	_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw, .Lfunc_end15-_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.cfi_endproc

	.section	.text._ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,@function
_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv: # @_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi265:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB16_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB16_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB16_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB16_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB16_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB16_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB16_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB16_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB16_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB16_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB16_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB16_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB16_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB16_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB16_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB16_16
.LBB16_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICryptoGetTextPassword(%rip), %cl
	jne	.LBB16_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+1(%rip), %al
	jne	.LBB16_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+2(%rip), %al
	jne	.LBB16_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+3(%rip), %al
	jne	.LBB16_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+4(%rip), %al
	jne	.LBB16_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+5(%rip), %al
	jne	.LBB16_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+6(%rip), %al
	jne	.LBB16_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+7(%rip), %al
	jne	.LBB16_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+8(%rip), %al
	jne	.LBB16_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+9(%rip), %al
	jne	.LBB16_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+10(%rip), %al
	jne	.LBB16_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+11(%rip), %al
	jne	.LBB16_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+12(%rip), %al
	jne	.LBB16_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+13(%rip), %al
	jne	.LBB16_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+14(%rip), %al
	jne	.LBB16_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+15(%rip), %al
	jne	.LBB16_33
.LBB16_16:
	leaq	8(%rdi), %rax
	jmp	.LBB16_50
.LBB16_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressProgressInfo(%rip), %cl
	jne	.LBB16_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+1(%rip), %cl
	jne	.LBB16_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+2(%rip), %cl
	jne	.LBB16_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+3(%rip), %cl
	jne	.LBB16_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+4(%rip), %cl
	jne	.LBB16_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+5(%rip), %cl
	jne	.LBB16_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+6(%rip), %cl
	jne	.LBB16_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+7(%rip), %cl
	jne	.LBB16_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+8(%rip), %cl
	jne	.LBB16_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+9(%rip), %cl
	jne	.LBB16_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+10(%rip), %cl
	jne	.LBB16_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+11(%rip), %cl
	jne	.LBB16_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+12(%rip), %cl
	jne	.LBB16_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+13(%rip), %cl
	jne	.LBB16_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+14(%rip), %cl
	jne	.LBB16_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+15(%rip), %cl
	jne	.LBB16_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB16_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB16_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN23CArchiveExtractCallback6AddRefEv,"axG",@progbits,_ZN23CArchiveExtractCallback6AddRefEv,comdat
	.weak	_ZN23CArchiveExtractCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback6AddRefEv,@function
_ZN23CArchiveExtractCallback6AddRefEv:  # @_ZN23CArchiveExtractCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN23CArchiveExtractCallback6AddRefEv, .Lfunc_end17-_ZN23CArchiveExtractCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZN23CArchiveExtractCallback7ReleaseEv,"axG",@progbits,_ZN23CArchiveExtractCallback7ReleaseEv,comdat
	.weak	_ZN23CArchiveExtractCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallback7ReleaseEv,@function
_ZN23CArchiveExtractCallback7ReleaseEv: # @_ZN23CArchiveExtractCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi266:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZN23CArchiveExtractCallback7ReleaseEv, .Lfunc_end18-_ZN23CArchiveExtractCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZN23CArchiveExtractCallbackD2Ev,"axG",@progbits,_ZN23CArchiveExtractCallbackD2Ev,comdat
	.weak	_ZN23CArchiveExtractCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallbackD2Ev,@function
_ZN23CArchiveExtractCallbackD2Ev:       # @_ZN23CArchiveExtractCallbackD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi267:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi268:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 32
.Lcfi270:
	.cfi_offset %rbx, -32
.Lcfi271:
	.cfi_offset %r14, -24
.Lcfi272:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$_ZTV23CArchiveExtractCallback+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CArchiveExtractCallback+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTV23CArchiveExtractCallback+192, 16(%r15)
	movq	272(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp332:
	callq	*16(%rax)
.Ltmp333:
.LBB19_2:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	leaq	232(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 232(%r15)
.Ltmp343:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp344:
# BB#3:
.Ltmp349:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp350:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	224(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp354:
	callq	*16(%rax)
.Ltmp355:
.LBB19_6:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	208(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp359:
	callq	*16(%rax)
.Ltmp360:
.LBB19_8:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit13
	movq	112(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_10
# BB#9:
	callq	_ZdaPv
.LBB19_10:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_12
# BB#11:
	callq	_ZdaPv
.LBB19_12:                              # %_ZN11CStringBaseIwED2Ev.exit14
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_14
# BB#13:
	callq	_ZdaPv
.LBB19_14:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_16
# BB#15:
	movq	(%rdi), %rax
.Ltmp364:
	callq	*16(%rax)
.Ltmp365:
.LBB19_16:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_18
# BB#17:
	movq	(%rdi), %rax
.Ltmp369:
	callq	*16(%rax)
.Ltmp370:
.LBB19_18:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit18
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp375:
	callq	*16(%rax)
.Ltmp376:
.LBB19_20:                              # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB19_45:
.Ltmp377:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_46:
.Ltmp371:
	movq	%rax, %r14
	jmp	.LBB19_47
.LBB19_42:
.Ltmp366:
	movq	%rax, %r14
	jmp	.LBB19_43
.LBB19_30:
.Ltmp361:
	movq	%rax, %r14
	jmp	.LBB19_34
.LBB19_31:
.Ltmp356:
	movq	%rax, %r14
	jmp	.LBB19_32
.LBB19_23:
.Ltmp334:
	movq	%rax, %r14
	leaq	232(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 232(%r15)
.Ltmp335:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp336:
# BB#24:
.Ltmp341:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp342:
	jmp	.LBB19_28
.LBB19_25:
.Ltmp337:
	movq	%rax, %r14
.Ltmp338:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp339:
	jmp	.LBB19_51
.LBB19_26:
.Ltmp340:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB19_27:
.Ltmp351:
	movq	%rax, %r14
	jmp	.LBB19_28
.LBB19_21:
.Ltmp345:
	movq	%rax, %r14
.Ltmp346:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp347:
.LBB19_28:                              # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit23
	movq	224(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_32
# BB#29:
	movq	(%rdi), %rax
.Ltmp352:
	callq	*16(%rax)
.Ltmp353:
.LBB19_32:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit25
	movq	208(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_34
# BB#33:
	movq	(%rdi), %rax
.Ltmp357:
	callq	*16(%rax)
.Ltmp358:
.LBB19_34:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit27
	movq	112(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_36
# BB#35:
	callq	_ZdaPv
.LBB19_36:                              # %_ZN11CStringBaseIwED2Ev.exit28
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_38
# BB#37:
	callq	_ZdaPv
.LBB19_38:                              # %_ZN11CStringBaseIwED2Ev.exit29
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_40
# BB#39:
	callq	_ZdaPv
.LBB19_40:                              # %_ZN11CStringBaseIwED2Ev.exit30
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_43
# BB#41:
	movq	(%rdi), %rax
.Ltmp362:
	callq	*16(%rax)
.Ltmp363:
.LBB19_43:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit32
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_47
# BB#44:
	movq	(%rdi), %rax
.Ltmp367:
	callq	*16(%rax)
.Ltmp368:
.LBB19_47:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit34
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB19_49
# BB#48:
	movq	(%rdi), %rax
.Ltmp372:
	callq	*16(%rax)
.Ltmp373:
.LBB19_49:                              # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackED2Ev.exit36
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_22:
.Ltmp348:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB19_50:
.Ltmp374:
	movq	%rax, %r14
.LBB19_51:                              # %.body21
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN23CArchiveExtractCallbackD2Ev, .Lfunc_end19-_ZN23CArchiveExtractCallbackD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Ltmp332-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin12 #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp343-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp345-.Lfunc_begin12 #     jumps to .Ltmp345
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp350-.Ltmp349       #   Call between .Ltmp349 and .Ltmp350
	.long	.Ltmp351-.Lfunc_begin12 #     jumps to .Ltmp351
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin12 #     jumps to .Ltmp356
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin12 #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp364-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp365-.Ltmp364       #   Call between .Ltmp364 and .Ltmp365
	.long	.Ltmp366-.Lfunc_begin12 #     jumps to .Ltmp366
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin12 #     jumps to .Ltmp371
	.byte	0                       #   On action: cleanup
	.long	.Ltmp375-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin12 #     jumps to .Ltmp377
	.byte	0                       #   On action: cleanup
	.long	.Ltmp376-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp335-.Ltmp376       #   Call between .Ltmp376 and .Ltmp335
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin12 #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp341-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp374-.Lfunc_begin12 #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp338-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin12 #     jumps to .Ltmp340
	.byte	1                       #   On action: 1
	.long	.Ltmp346-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp347-.Ltmp346       #   Call between .Ltmp346 and .Ltmp347
	.long	.Ltmp348-.Lfunc_begin12 #     jumps to .Ltmp348
	.byte	1                       #   On action: 1
	.long	.Ltmp352-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp373-.Ltmp352       #   Call between .Ltmp352 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin12 #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp373-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Lfunc_end19-.Ltmp373   #   Call between .Ltmp373 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN23CArchiveExtractCallbackD0Ev,"axG",@progbits,_ZN23CArchiveExtractCallbackD0Ev,comdat
	.weak	_ZN23CArchiveExtractCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN23CArchiveExtractCallbackD0Ev,@function
_ZN23CArchiveExtractCallbackD0Ev:       # @_ZN23CArchiveExtractCallbackD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi275:
	.cfi_def_cfa_offset 32
.Lcfi276:
	.cfi_offset %rbx, -24
.Lcfi277:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp378:
	callq	_ZN23CArchiveExtractCallbackD2Ev
.Ltmp379:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_2:
.Ltmp380:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN23CArchiveExtractCallbackD0Ev, .Lfunc_end20-_ZN23CArchiveExtractCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp378-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp379-.Ltmp378       #   Call between .Ltmp378 and .Ltmp379
	.long	.Ltmp380-.Lfunc_begin13 #     jumps to .Ltmp380
	.byte	0                       #   On action: cleanup
	.long	.Ltmp379-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp379   #   Call between .Ltmp379 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end21:
	.size	_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end21-_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N23CArchiveExtractCallback6AddRefEv,"axG",@progbits,_ZThn8_N23CArchiveExtractCallback6AddRefEv,comdat
	.weak	_ZThn8_N23CArchiveExtractCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallback6AddRefEv,@function
_ZThn8_N23CArchiveExtractCallback6AddRefEv: # @_ZThn8_N23CArchiveExtractCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end22:
	.size	_ZThn8_N23CArchiveExtractCallback6AddRefEv, .Lfunc_end22-_ZThn8_N23CArchiveExtractCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N23CArchiveExtractCallback7ReleaseEv,"axG",@progbits,_ZThn8_N23CArchiveExtractCallback7ReleaseEv,comdat
	.weak	_ZThn8_N23CArchiveExtractCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallback7ReleaseEv,@function
_ZThn8_N23CArchiveExtractCallback7ReleaseEv: # @_ZThn8_N23CArchiveExtractCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi278:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB23_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB23_2:                               # %_ZN23CArchiveExtractCallback7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZThn8_N23CArchiveExtractCallback7ReleaseEv, .Lfunc_end23-_ZThn8_N23CArchiveExtractCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N23CArchiveExtractCallbackD1Ev,"axG",@progbits,_ZThn8_N23CArchiveExtractCallbackD1Ev,comdat
	.weak	_ZThn8_N23CArchiveExtractCallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallbackD1Ev,@function
_ZThn8_N23CArchiveExtractCallbackD1Ev:  # @_ZThn8_N23CArchiveExtractCallbackD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN23CArchiveExtractCallbackD2Ev # TAILCALL
.Lfunc_end24:
	.size	_ZThn8_N23CArchiveExtractCallbackD1Ev, .Lfunc_end24-_ZThn8_N23CArchiveExtractCallbackD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N23CArchiveExtractCallbackD0Ev,"axG",@progbits,_ZThn8_N23CArchiveExtractCallbackD0Ev,comdat
	.weak	_ZThn8_N23CArchiveExtractCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N23CArchiveExtractCallbackD0Ev,@function
_ZThn8_N23CArchiveExtractCallbackD0Ev:  # @_ZThn8_N23CArchiveExtractCallbackD0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi280:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi281:
	.cfi_def_cfa_offset 32
.Lcfi282:
	.cfi_offset %rbx, -24
.Lcfi283:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp381:
	movq	%rbx, %rdi
	callq	_ZN23CArchiveExtractCallbackD2Ev
.Ltmp382:
# BB#1:                                 # %_ZN23CArchiveExtractCallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp383:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZThn8_N23CArchiveExtractCallbackD0Ev, .Lfunc_end25-_ZThn8_N23CArchiveExtractCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp381-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin14 #     jumps to .Ltmp383
	.byte	0                       #   On action: cleanup
	.long	.Ltmp382-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp382   #   Call between .Ltmp382 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end26:
	.size	_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N23CArchiveExtractCallback6AddRefEv,"axG",@progbits,_ZThn16_N23CArchiveExtractCallback6AddRefEv,comdat
	.weak	_ZThn16_N23CArchiveExtractCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallback6AddRefEv,@function
_ZThn16_N23CArchiveExtractCallback6AddRefEv: # @_ZThn16_N23CArchiveExtractCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end27:
	.size	_ZThn16_N23CArchiveExtractCallback6AddRefEv, .Lfunc_end27-_ZThn16_N23CArchiveExtractCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N23CArchiveExtractCallback7ReleaseEv,"axG",@progbits,_ZThn16_N23CArchiveExtractCallback7ReleaseEv,comdat
	.weak	_ZThn16_N23CArchiveExtractCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallback7ReleaseEv,@function
_ZThn16_N23CArchiveExtractCallback7ReleaseEv: # @_ZThn16_N23CArchiveExtractCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi284:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB28_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:                               # %_ZN23CArchiveExtractCallback7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZThn16_N23CArchiveExtractCallback7ReleaseEv, .Lfunc_end28-_ZThn16_N23CArchiveExtractCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N23CArchiveExtractCallbackD1Ev,"axG",@progbits,_ZThn16_N23CArchiveExtractCallbackD1Ev,comdat
	.weak	_ZThn16_N23CArchiveExtractCallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallbackD1Ev,@function
_ZThn16_N23CArchiveExtractCallbackD1Ev: # @_ZThn16_N23CArchiveExtractCallbackD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN23CArchiveExtractCallbackD2Ev # TAILCALL
.Lfunc_end29:
	.size	_ZThn16_N23CArchiveExtractCallbackD1Ev, .Lfunc_end29-_ZThn16_N23CArchiveExtractCallbackD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N23CArchiveExtractCallbackD0Ev,"axG",@progbits,_ZThn16_N23CArchiveExtractCallbackD0Ev,comdat
	.weak	_ZThn16_N23CArchiveExtractCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N23CArchiveExtractCallbackD0Ev,@function
_ZThn16_N23CArchiveExtractCallbackD0Ev: # @_ZThn16_N23CArchiveExtractCallbackD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 32
.Lcfi288:
	.cfi_offset %rbx, -24
.Lcfi289:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp384:
	movq	%rbx, %rdi
	callq	_ZN23CArchiveExtractCallbackD2Ev
.Ltmp385:
# BB#1:                                 # %_ZN23CArchiveExtractCallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_2:
.Ltmp386:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZThn16_N23CArchiveExtractCallbackD0Ev, .Lfunc_end30-_ZThn16_N23CArchiveExtractCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp384-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp385-.Ltmp384       #   Call between .Ltmp384 and .Ltmp385
	.long	.Ltmp386-.Lfunc_begin15 #     jumps to .Ltmp386
	.byte	0                       #   On action: cleanup
	.long	.Ltmp385-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp385   #   Call between .Ltmp385 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO8COutFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO8COutFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFileD0Ev,@function
_ZN8NWindows5NFile3NIO8COutFileD0Ev:    # @_ZN8NWindows5NFile3NIO8COutFileD0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi292:
	.cfi_def_cfa_offset 32
.Lcfi293:
	.cfi_offset %rbx, -24
.Lcfi294:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp387:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp388:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB31_2:
.Ltmp389:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_ZN8NWindows5NFile3NIO8COutFileD0Ev, .Lfunc_end31-_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp387-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp388-.Ltmp387       #   Call between .Ltmp387 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin16 #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end31-.Ltmp388   #   Call between .Ltmp388 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi295:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi296:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi297:
	.cfi_def_cfa_offset 32
.Lcfi298:
	.cfi_offset %rbx, -24
.Lcfi299:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp390:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp391:
# BB#1:
.Ltmp396:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp397:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_5:
.Ltmp398:
	movq	%rax, %r14
	jmp	.LBB32_6
.LBB32_3:
.Ltmp392:
	movq	%rax, %r14
.Ltmp393:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp394:
.LBB32_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_4:
.Ltmp395:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end32-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp390-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp391-.Ltmp390       #   Call between .Ltmp390 and .Ltmp391
	.long	.Ltmp392-.Lfunc_begin17 #     jumps to .Ltmp392
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin17 #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin17 #     jumps to .Ltmp395
	.byte	1                       #   On action: 1
	.long	.Ltmp394-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end32-.Ltmp394   #   Call between .Ltmp394 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi300:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi301:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi303:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi304:
	.cfi_def_cfa_offset 48
.Lcfi305:
	.cfi_offset %rbx, -48
.Lcfi306:
	.cfi_offset %r12, -40
.Lcfi307:
	.cfi_offset %r14, -32
.Lcfi308:
	.cfi_offset %r15, -24
.Lcfi309:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB33_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB33_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB33_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB33_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB33_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB33_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB33_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB33_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB33_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB33_17
.LBB33_7:
	xorl	%ecx, %ecx
.LBB33_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB33_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB33_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB33_10
.LBB33_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB33_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB33_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB33_13
	jmp	.LBB33_26
.LBB33_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB33_27
.LBB33_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB33_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB33_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB33_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB33_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB33_20
	jmp	.LBB33_21
.LBB33_18:
	xorl	%ebx, %ebx
.LBB33_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB33_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB33_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB33_23
.LBB33_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB33_8
	jmp	.LBB33_26
.Lfunc_end33:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end33-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.type	_ZTV23CArchiveExtractCallback,@object # @_ZTV23CArchiveExtractCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTV23CArchiveExtractCallback
	.p2align	3
_ZTV23CArchiveExtractCallback:
	.quad	0
	.quad	_ZTI23CArchiveExtractCallback
	.quad	_ZN23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZN23CArchiveExtractCallback6AddRefEv
	.quad	_ZN23CArchiveExtractCallback7ReleaseEv
	.quad	_ZN23CArchiveExtractCallbackD2Ev
	.quad	_ZN23CArchiveExtractCallbackD0Ev
	.quad	_ZN23CArchiveExtractCallback8SetTotalEy
	.quad	_ZN23CArchiveExtractCallback12SetCompletedEPKy
	.quad	_ZN23CArchiveExtractCallback9GetStreamEjPP20ISequentialOutStreami
	.quad	_ZN23CArchiveExtractCallback16PrepareOperationEi
	.quad	_ZN23CArchiveExtractCallback18SetOperationResultEi
	.quad	_ZN23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.quad	_ZN23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.quad	-8
	.quad	_ZTI23CArchiveExtractCallback
	.quad	_ZThn8_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N23CArchiveExtractCallback6AddRefEv
	.quad	_ZThn8_N23CArchiveExtractCallback7ReleaseEv
	.quad	_ZThn8_N23CArchiveExtractCallbackD1Ev
	.quad	_ZThn8_N23CArchiveExtractCallbackD0Ev
	.quad	_ZThn8_N23CArchiveExtractCallback21CryptoGetTextPasswordEPPw
	.quad	-16
	.quad	_ZTI23CArchiveExtractCallback
	.quad	_ZThn16_N23CArchiveExtractCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N23CArchiveExtractCallback6AddRefEv
	.quad	_ZThn16_N23CArchiveExtractCallback7ReleaseEv
	.quad	_ZThn16_N23CArchiveExtractCallbackD1Ev
	.quad	_ZThn16_N23CArchiveExtractCallbackD0Ev
	.quad	_ZThn16_N23CArchiveExtractCallback12SetRatioInfoEPKyS1_
	.size	_ZTV23CArchiveExtractCallback, 240

	.type	_ZTS23CArchiveExtractCallback,@object # @_ZTS23CArchiveExtractCallback
	.globl	_ZTS23CArchiveExtractCallback
	.p2align	4
_ZTS23CArchiveExtractCallback:
	.asciz	"23CArchiveExtractCallback"
	.size	_ZTS23CArchiveExtractCallback, 26

	.type	_ZTS23IArchiveExtractCallback,@object # @_ZTS23IArchiveExtractCallback
	.section	.rodata._ZTS23IArchiveExtractCallback,"aG",@progbits,_ZTS23IArchiveExtractCallback,comdat
	.weak	_ZTS23IArchiveExtractCallback
	.p2align	4
_ZTS23IArchiveExtractCallback:
	.asciz	"23IArchiveExtractCallback"
	.size	_ZTS23IArchiveExtractCallback, 26

	.type	_ZTS9IProgress,@object  # @_ZTS9IProgress
	.section	.rodata._ZTS9IProgress,"aG",@progbits,_ZTS9IProgress,comdat
	.weak	_ZTS9IProgress
_ZTS9IProgress:
	.asciz	"9IProgress"
	.size	_ZTS9IProgress, 11

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI9IProgress,@object  # @_ZTI9IProgress
	.section	.rodata._ZTI9IProgress,"aG",@progbits,_ZTI9IProgress,comdat
	.weak	_ZTI9IProgress
	.p2align	4
_ZTI9IProgress:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IProgress
	.quad	_ZTI8IUnknown
	.size	_ZTI9IProgress, 24

	.type	_ZTI23IArchiveExtractCallback,@object # @_ZTI23IArchiveExtractCallback
	.section	.rodata._ZTI23IArchiveExtractCallback,"aG",@progbits,_ZTI23IArchiveExtractCallback,comdat
	.weak	_ZTI23IArchiveExtractCallback
	.p2align	4
_ZTI23IArchiveExtractCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23IArchiveExtractCallback
	.quad	_ZTI9IProgress
	.size	_ZTI23IArchiveExtractCallback, 24

	.type	_ZTS22ICryptoGetTextPassword,@object # @_ZTS22ICryptoGetTextPassword
	.section	.rodata._ZTS22ICryptoGetTextPassword,"aG",@progbits,_ZTS22ICryptoGetTextPassword,comdat
	.weak	_ZTS22ICryptoGetTextPassword
	.p2align	4
_ZTS22ICryptoGetTextPassword:
	.asciz	"22ICryptoGetTextPassword"
	.size	_ZTS22ICryptoGetTextPassword, 25

	.type	_ZTI22ICryptoGetTextPassword,@object # @_ZTI22ICryptoGetTextPassword
	.section	.rodata._ZTI22ICryptoGetTextPassword,"aG",@progbits,_ZTI22ICryptoGetTextPassword,comdat
	.weak	_ZTI22ICryptoGetTextPassword
	.p2align	4
_ZTI22ICryptoGetTextPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoGetTextPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoGetTextPassword, 24

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI23CArchiveExtractCallback,@object # @_ZTI23CArchiveExtractCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTI23CArchiveExtractCallback
	.p2align	4
_ZTI23CArchiveExtractCallback:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS23CArchiveExtractCallback
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI23IArchiveExtractCallback
	.quad	2                       # 0x2
	.quad	_ZTI22ICryptoGetTextPassword
	.quad	2050                    # 0x802
	.quad	_ZTI21ICompressProgressInfo
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTI23CArchiveExtractCallback, 88

	.type	_ZTVN8NWindows5NFile3NIO8COutFileE,@object # @_ZTVN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO8COutFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO8COutFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO8COutFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO8COutFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO8COutFileE,@object # @_ZTSN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO8COutFileE:
	.asciz	"N8NWindows5NFile3NIO8COutFileE"
	.size	_ZTSN8NWindows5NFile3NIO8COutFileE, 31

	.type	_ZTIN8NWindows5NFile3NIO8COutFileE,@object # @_ZTIN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO8COutFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO8COutFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO8COutFileE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
