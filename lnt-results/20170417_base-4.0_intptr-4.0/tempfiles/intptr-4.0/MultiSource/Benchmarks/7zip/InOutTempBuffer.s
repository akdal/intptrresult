	.text
	.file	"InOutTempBuffer.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN16CInOutTempBufferC2Ev
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBufferC2Ev,@function
_ZN16CInOutTempBufferC2Ev:              # @_ZN16CInOutTempBufferC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movb	$0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movl	$0, (%rax)
	movl	$4, 20(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%rbx)
	movl	$-1, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
.Ltmp0:
	movl	$4, %edi
	callq	_Znam
.Ltmp1:
# BB#1:
	leaq	24(%rbx), %r15
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	$4, 52(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 24(%rbx)
	movq	$0, 1112(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1128(%rbx)
.Ltmp3:
	movl	$16, %edi
	callq	_Znam
.Ltmp4:
# BB#2:
	movq	%rax, 1128(%rbx)
	movl	$0, (%rax)
	movl	$4, 1140(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_4:
.Ltmp5:
	movq	%rax, %r14
.Ltmp6:
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp7:
	jmp	.LBB0_5
.LBB0_11:
.Ltmp8:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB0_3:
.Ltmp2:
	movq	%rax, %r14
.LBB0_5:
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
.Ltmp10:
# BB#6:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#7:
	callq	_ZdaPv
.LBB0_8:                                # %_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_9:
.Ltmp11:
	movq	%rax, %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_10
# BB#12:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB0_10:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN16CInOutTempBufferC2Ev, .Lfunc_end0-_ZN16CInOutTempBufferC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN16CInOutTempBuffer6CreateEv
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBuffer6CreateEv,@function
_ZN16CInOutTempBuffer6CreateEv:         # @_ZN16CInOutTempBuffer6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$0, 1112(%rbx)
	jne	.LBB2_2
# BB#1:
	movl	$1048576, %edi          # imm = 0x100000
	callq	_Znam
	movq	%rax, 1112(%rbx)
.LBB2_2:
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN16CInOutTempBuffer6CreateEv, .Lfunc_end2-_ZN16CInOutTempBuffer6CreateEv
	.cfi_endproc

	.globl	_ZN16CInOutTempBufferD2Ev
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBufferD2Ev,@function
_ZN16CInOutTempBufferD2Ev:              # @_ZN16CInOutTempBufferD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	1112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	callq	_ZdaPv
.LBB3_2:
	movq	1128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	callq	_ZdaPv
.LBB3_4:                                # %_ZN11CStringBaseIwED2Ev.exit
	leaq	24(%rbx), %rdi
.Ltmp12:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp13:
# BB#5:
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
.Ltmp19:
# BB#6:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB3_12
# BB#7:
	popq	%rbx
	popq	%r14
	jmp	_ZdaPv                  # TAILCALL
.LBB3_12:                               # %_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev.exit
	popq	%rbx
	popq	%r14
	retq
.LBB3_8:
.Ltmp20:
	movq	%rax, %r14
	jmp	.LBB3_9
.LBB3_13:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
.Ltmp16:
.LBB3_9:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_11
# BB#10:
	callq	_ZdaPv
.LBB3_11:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_14:
.Ltmp17:
	movq	%rax, %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_16
# BB#15:
	callq	_ZdaPv
.LBB3_16:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN16CInOutTempBufferD2Ev, .Lfunc_end3-_ZN16CInOutTempBufferD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16CInOutTempBuffer11InitWritingEv
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBuffer11InitWritingEv,@function
_ZN16CInOutTempBuffer11InitWritingEv:   # @_ZN16CInOutTempBuffer11InitWritingEv
	.cfi_startproc
# BB#0:
	movl	$0, 1120(%rdi)
	movb	$0, 1144(%rdi)
	movq	$0, 1152(%rdi)
	movl	$-1, 1160(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN16CInOutTempBuffer11InitWritingEv, .Lfunc_end4-_ZN16CInOutTempBuffer11InitWritingEv
	.cfi_endproc

	.globl	_ZN16CInOutTempBuffer11WriteToFileEPKvj
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBuffer11WriteToFileEPKvj,@function
_ZN16CInOutTempBuffer11WriteToFileEPKvj: # @_ZN16CInOutTempBuffer11WriteToFileEPKvj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB5_5
# BB#1:
	cmpb	$0, 1144(%rbx)
	je	.LBB5_6
# BB#2:                                 # %._crit_edge
	leaq	24(%rbx), %r15
.LBB5_3:
	leaq	8(%rsp), %rcx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj
	testb	%al, %al
	je	.LBB5_17
# BB#4:
	movl	1160(%rbx), %edi
	movl	8(%rsp), %edx
	movq	%r14, %rsi
	callq	CrcUpdate
	movl	%eax, 1160(%rbx)
	movl	8(%rsp), %eax
	addq	%rax, 1152(%rbx)
	cmpl	%ebp, %eax
	sete	%al
	jmp	.LBB5_18
.LBB5_5:
	movb	$1, %al
	jmp	.LBB5_18
.LBB5_6:
	movq	$0, 16(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movl	$0, (%rax)
	movl	$4, 20(%rsp)
.Ltmp21:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE
.Ltmp22:
# BB#7:
	testb	%al, %al
	je	.LBB5_15
# BB#8:
	movq	8(%rsp), %rsi
	leaq	1128(%rbx), %rcx
.Ltmp23:
	movl	$.L.str, %edx
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6CreateEPKwS4_R11CStringBaseIwE
.Ltmp24:
# BB#9:
	testl	%eax, %eax
	je	.LBB5_15
# BB#10:
	leaq	24(%rbx), %r15
	movq	1128(%rbx), %rsi
.Ltmp25:
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
.Ltmp26:
# BB#11:
	testb	%al, %al
	je	.LBB5_15
# BB#12:                                # %.critedge
	movb	$1, 1144(%rbx)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#13:
	callq	_ZdaPv
	jmp	.LBB5_3
.LBB5_15:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_17
# BB#16:
	callq	_ZdaPv
.LBB5_17:                               # %_ZN11CStringBaseIwED2Ev.exit10
	xorl	%eax, %eax
.LBB5_18:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_19:
.Ltmp27:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_21
# BB#20:
	callq	_ZdaPv
.LBB5_21:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN16CInOutTempBuffer11WriteToFileEPKvj, .Lfunc_end5-_ZN16CInOutTempBuffer11WriteToFileEPKvj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp21-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp21         #   Call between .Ltmp21 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end5-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN16CInOutTempBuffer5WriteEPKvj
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBuffer5WriteEPKvj,@function
_ZN16CInOutTempBuffer5WriteEPKvj:       # @_ZN16CInOutTempBuffer5WriteEPKvj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	1120(%rbp), %edi
	cmpq	$1048575, %rdi          # imm = 0xFFFFF
	ja	.LBB6_2
# BB#1:
	movl	$1048576, %ebx          # imm = 0x100000
	subl	%edi, %ebx
	cmpl	%r14d, %ebx
	cmovael	%r14d, %ebx
	addq	1112(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movl	1160(%rbp), %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	CrcUpdate
	movl	%eax, 1160(%rbp)
	addl	%ebx, 1120(%rbp)
	subl	%ebx, %r14d
	addq	%rbx, %r15
	addq	%rbx, 1152(%rbp)
.LBB6_2:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN16CInOutTempBuffer11WriteToFileEPKvj # TAILCALL
.Lfunc_end6:
	.size	_ZN16CInOutTempBuffer5WriteEPKvj, .Lfunc_end6-_ZN16CInOutTempBuffer5WriteEPKvj
	.cfi_endproc

	.globl	_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream,@function
_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream: # @_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$1096, %rsp             # imm = 0x448
.Lcfi37:
	.cfi_def_cfa_offset 1152
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	movl	$-2147467259, %ebp      # imm = 0x80004005
	testb	%al, %al
	je	.LBB7_22
# BB#1:
	movl	1120(%rbx), %edx
	testq	%rdx, %rdx
	je	.LBB7_2
# BB#3:
	movq	1112(%rbx), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_22
# BB#4:
	movq	1112(%rbx), %rsi
	movl	1120(%rbx), %edx
	movl	$-1, %edi
	callq	CrcUpdate
	movl	%eax, %r15d
	movl	1120(%rbx), %r12d
	cmpb	$0, 1144(%rbx)
	jne	.LBB7_6
	jmp	.LBB7_18
.LBB7_2:
	xorl	%r12d, %r12d
	movl	$-1, %r15d
	cmpb	$0, 1144(%rbx)
	je	.LBB7_18
.LBB7_6:
	movl	$-1, 16(%rsp)
	movq	$0, 32(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 24(%rsp)
	movb	$0, (%rax)
	movl	$4, 36(%rsp)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 8(%rsp)
	movq	1128(%rbx), %rsi
.Ltmp28:
	leaq	8(%rsp), %rdi
	xorl	%edx, %edx
	callq	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
.Ltmp29:
# BB#7:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	testb	%al, %al
	je	.LBB7_21
# BB#8:                                 # %.preheader
	cmpq	1152(%rbx), %r12
	jae	.LBB7_17
# BB#9:
	leaq	4(%rsp), %r13
	.p2align	4, 0x90
.LBB7_10:                               # =>This Inner Loop Header: Depth=1
	movq	1112(%rbx), %rsi
.Ltmp31:
	movl	$1048576, %edx          # imm = 0x100000
	leaq	8(%rsp), %rdi
	movq	%r13, %rcx
	callq	_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj
.Ltmp32:
# BB#11:                                #   in Loop: Header=BB7_10 Depth=1
	testb	%al, %al
	je	.LBB7_21
# BB#12:                                #   in Loop: Header=BB7_10 Depth=1
	movl	4(%rsp), %edx
	testq	%rdx, %rdx
	je	.LBB7_17
# BB#13:                                #   in Loop: Header=BB7_10 Depth=1
	movq	1112(%rbx), %rsi
.Ltmp33:
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.Ltmp34:
# BB#14:                                #   in Loop: Header=BB7_10 Depth=1
	testl	%eax, %eax
	jne	.LBB7_20
# BB#15:                                #   in Loop: Header=BB7_10 Depth=1
	movq	1112(%rbx), %rsi
	movl	4(%rsp), %edx
.Ltmp35:
	movl	%r15d, %edi
	callq	CrcUpdate
	movl	%eax, %r15d
.Ltmp36:
# BB#16:                                # %.thread50
                                        #   in Loop: Header=BB7_10 Depth=1
	movl	4(%rsp), %eax
	addq	%rax, %r12
	cmpq	1152(%rbx), %r12
	jb	.LBB7_10
.LBB7_17:                               # %.loopexit.thread
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.LBB7_18:
	cmpl	%r15d, 1160(%rbx)
	movl	$-2147467259, %ebp      # imm = 0x80004005
	jne	.LBB7_22
# BB#19:
	xorl	%eax, %eax
	cmpq	1152(%rbx), %r12
	movl	$-2147467259, %ebp      # imm = 0x80004005
	cmovel	%eax, %ebp
	jmp	.LBB7_22
.LBB7_20:
	movl	%eax, %ebp
.LBB7_21:                               # %.loopexit
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.LBB7_22:
	movl	%ebp, %eax
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_23:
.Ltmp30:
	jmp	.LBB7_24
.LBB7_27:
.Ltmp37:
.LBB7_24:
	movq	%rax, %rbx
.Ltmp38:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp39:
# BB#25:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_26:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream, .Lfunc_end7-_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp28-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp31         #   Call between .Ltmp31 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Lfunc_end7-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj,@function
_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj: # @_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	16(%rdi), %rbp
	movl	1120(%rbp), %edi
	cmpq	$1048575, %rdi          # imm = 0xFFFFF
	ja	.LBB8_1
# BB#2:
	movl	$1048576, %ebx          # imm = 0x100000
	subl	%edi, %ebx
	cmpl	%r15d, %ebx
	cmovael	%r15d, %ebx
	addq	1112(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movl	1160(%rbp), %edi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	CrcUpdate
	movl	%eax, 1160(%rbp)
	addl	%ebx, 1120(%rbp)
	movl	%r15d, %edx
	subl	%ebx, %edx
	addq	%rbx, %r12
	addq	%rbx, 1152(%rbp)
	jmp	.LBB8_3
.LBB8_1:
	movl	%r15d, %edx
.LBB8_3:                                # %_ZN16CInOutTempBuffer5WriteEPKvj.exit
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_ZN16CInOutTempBuffer11WriteToFileEPKvj
	testb	%al, %al
	je	.LBB8_4
# BB#6:
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB8_8
# BB#7:
	movl	%r15d, (%r14)
	jmp	.LBB8_8
.LBB8_4:
	movl	$-2147467259, %eax      # imm = 0x80004005
	testq	%r14, %r14
	je	.LBB8_8
# BB#5:
	movl	$0, (%r14)
.LBB8_8:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj, .Lfunc_end8-_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj
	.cfi_endproc

	.section	.text._ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv,@function
_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv: # @_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB9_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB9_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB9_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB9_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB9_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB9_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB9_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB9_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB9_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB9_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB9_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB9_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB9_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB9_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB9_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB9_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB9_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end9-_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN27CSequentialOutTempBufferImp6AddRefEv,"axG",@progbits,_ZN27CSequentialOutTempBufferImp6AddRefEv,comdat
	.weak	_ZN27CSequentialOutTempBufferImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZN27CSequentialOutTempBufferImp6AddRefEv,@function
_ZN27CSequentialOutTempBufferImp6AddRefEv: # @_ZN27CSequentialOutTempBufferImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN27CSequentialOutTempBufferImp6AddRefEv, .Lfunc_end10-_ZN27CSequentialOutTempBufferImp6AddRefEv
	.cfi_endproc

	.section	.text._ZN27CSequentialOutTempBufferImp7ReleaseEv,"axG",@progbits,_ZN27CSequentialOutTempBufferImp7ReleaseEv,comdat
	.weak	_ZN27CSequentialOutTempBufferImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN27CSequentialOutTempBufferImp7ReleaseEv,@function
_ZN27CSequentialOutTempBufferImp7ReleaseEv: # @_ZN27CSequentialOutTempBufferImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB11_2:
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN27CSequentialOutTempBufferImp7ReleaseEv, .Lfunc_end11-_ZN27CSequentialOutTempBufferImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end12-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN27CSequentialOutTempBufferImpD0Ev,"axG",@progbits,_ZN27CSequentialOutTempBufferImpD0Ev,comdat
	.weak	_ZN27CSequentialOutTempBufferImpD0Ev
	.p2align	4, 0x90
	.type	_ZN27CSequentialOutTempBufferImpD0Ev,@function
_ZN27CSequentialOutTempBufferImpD0Ev:   # @_ZN27CSequentialOutTempBufferImpD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end13:
	.size	_ZN27CSequentialOutTempBufferImpD0Ev, .Lfunc_end13-_ZN27CSequentialOutTempBufferImpD0Ev
	.cfi_endproc

	.section	.text._ZN8NWindows5NFile3NIO8COutFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO8COutFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFileD0Ev,@function
_ZN8NWindows5NFile3NIO8COutFileD0Ev:    # @_ZN8NWindows5NFile3NIO8COutFileD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 32
.Lcfi59:
	.cfi_offset %rbx, -24
.Lcfi60:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp41:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp42:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB14_2:
.Ltmp43:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN8NWindows5NFile3NIO8COutFileD0Ev, .Lfunc_end14-_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp42    #   Call between .Ltmp42 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -24
.Lcfi65:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp44:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp45:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp46:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end15-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp44-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin5   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp45    #   Call between .Ltmp45 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV27CSequentialOutTempBufferImp,@object # @_ZTV27CSequentialOutTempBufferImp
	.section	.rodata,"a",@progbits
	.globl	_ZTV27CSequentialOutTempBufferImp
	.p2align	3
_ZTV27CSequentialOutTempBufferImp:
	.quad	0
	.quad	_ZTI27CSequentialOutTempBufferImp
	.quad	_ZN27CSequentialOutTempBufferImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZN27CSequentialOutTempBufferImp6AddRefEv
	.quad	_ZN27CSequentialOutTempBufferImp7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN27CSequentialOutTempBufferImpD0Ev
	.quad	_ZN27CSequentialOutTempBufferImp5WriteEPKvjPj
	.size	_ZTV27CSequentialOutTempBufferImp, 64

	.type	_ZTS27CSequentialOutTempBufferImp,@object # @_ZTS27CSequentialOutTempBufferImp
	.globl	_ZTS27CSequentialOutTempBufferImp
	.p2align	4
_ZTS27CSequentialOutTempBufferImp:
	.asciz	"27CSequentialOutTempBufferImp"
	.size	_ZTS27CSequentialOutTempBufferImp, 30

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI27CSequentialOutTempBufferImp,@object # @_ZTI27CSequentialOutTempBufferImp
	.section	.rodata,"a",@progbits
	.globl	_ZTI27CSequentialOutTempBufferImp
	.p2align	4
_ZTI27CSequentialOutTempBufferImp:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS27CSequentialOutTempBufferImp
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI27CSequentialOutTempBufferImp, 56

	.type	_ZTVN8NWindows5NFile3NIO8COutFileE,@object # @_ZTVN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO8COutFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO8COutFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO8COutFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO8COutFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO8COutFileE,@object # @_ZTSN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO8COutFileE:
	.asciz	"N8NWindows5NFile3NIO8COutFileE"
	.size	_ZTSN8NWindows5NFile3NIO8COutFileE, 31

	.type	_ZTIN8NWindows5NFile3NIO8COutFileE,@object # @_ZTIN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO8COutFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO8COutFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO8COutFileE, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	55                      # 0x37
	.long	122                     # 0x7a
	.long	116                     # 0x74
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24


	.globl	_ZN16CInOutTempBufferC1Ev
	.type	_ZN16CInOutTempBufferC1Ev,@function
_ZN16CInOutTempBufferC1Ev = _ZN16CInOutTempBufferC2Ev
	.globl	_ZN16CInOutTempBufferD1Ev
	.type	_ZN16CInOutTempBufferD1Ev,@function
_ZN16CInOutTempBufferD1Ev = _ZN16CInOutTempBufferD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
