	.text
	.file	"driver.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8056, %rsp             # imm = 0x1F78
.Lcfi6:
	.cfi_def_cfa_offset 8112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$40, (%rsp)
	leaq	48(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	40(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	24(%rsp), %r8
	movl	$1, %r14d
	movl	$1, %r9d
	callq	matgen
	testl	%eax, %eax
	jne	.LBB0_27
# BB#1:                                 # %.lr.ph46
	movaps	.LCPI0_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm7, %xmm7
	leaq	48(%rsp), %r13
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_11 Depth 2
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_17 Depth 3
	movl	48(%rsp), %ecx
	movl	52(%rsp), %edx
	cmpl	$1000, %edx             # imm = 0x3E8
	jg	.LBB0_28
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1000, %ecx             # imm = 0x3E8
	jg	.LBB0_28
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorps	%xmm0, %xmm0
	testl	%ecx, %ecx
	jle	.LBB0_19
# BB#5:                                 # %.lr.ph42
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%edx, %edx
	jle	.LBB0_6
# BB#10:                                # %.lr.ph42.split.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movslq	%ecx, %r8
	leal	-1(%rdx), %ecx
	movl	%edx, %esi
	andl	$3, %esi
	xorps	%xmm0, %xmm0
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph42.split.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_17 Depth 3
	testl	%esi, %esi
	movq	56(%rsp,%rdi,8), %rbp
	je	.LBB0_12
# BB#13:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_11 Depth=2
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rbp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	cmpltss	%xmm7, %xmm3
	movaps	%xmm3, %xmm4
	andnps	%xmm2, %xmm4
	xorps	%xmm6, %xmm2
	andps	%xmm2, %xmm3
	orps	%xmm4, %xmm3
	addss	%xmm3, %xmm1
	incl	%eax
	addq	$4, %rbp
	cmpl	%eax, %esi
	jne	.LBB0_14
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_11 Depth=2
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
.LBB0_15:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_11 Depth=2
	cmpl	$3, %ecx
	jb	.LBB0_18
# BB#16:                                # %.lr.ph42.split.us.new
                                        #   in Loop: Header=BB0_11 Depth=2
	movl	%edx, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rbp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	xorps	%xmm6, %xmm4
	movaps	%xmm2, %xmm5
	cmpltss	%xmm7, %xmm5
	andps	%xmm5, %xmm4
	andnps	%xmm2, %xmm5
	orps	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	movaps	%xmm3, %xmm1
	xorps	%xmm6, %xmm1
	movaps	%xmm3, %xmm2
	cmpltss	%xmm7, %xmm2
	andps	%xmm2, %xmm1
	andnps	%xmm3, %xmm2
	orps	%xmm1, %xmm2
	addss	%xmm5, %xmm2
	movss	8(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	xorps	%xmm6, %xmm3
	movaps	%xmm1, %xmm4
	cmpltss	%xmm7, %xmm4
	andps	%xmm4, %xmm3
	andnps	%xmm1, %xmm4
	orps	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	12(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	xorps	%xmm6, %xmm3
	movaps	%xmm2, %xmm1
	cmpltss	%xmm7, %xmm1
	andps	%xmm1, %xmm3
	andnps	%xmm2, %xmm1
	orps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	addq	$16, %rbp
	addl	$-4, %ebx
	jne	.LBB0_17
.LBB0_18:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_11 Depth=2
	maxss	%xmm1, %xmm0
	incq	%rdi
	cmpq	%r8, %rdi
	jl	.LBB0_11
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph42.split.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leal	-1(%rcx), %edx
	movl	%ecx, %esi
	xorl	%eax, %eax
	andl	$7, %esi
	je	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph42.split.prol
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	cmpl	%eax, %esi
	jne	.LBB0_7
.LBB0_8:                                # %.lr.ph42.split.prol.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$7, %edx
	jb	.LBB0_19
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph42.split
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	$8, %eax
	cmpl	%ecx, %eax
	jl	.LBB0_9
	.p2align	4, 0x90
.LBB0_19:                               # %._crit_edge43
                                        #   in Loop: Header=BB0_2 Depth=1
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movq	24(%rsp), %r15
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	sgefa
	testl	%eax, %eax
	jne	.LBB0_26
# BB#20:                                #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %r12
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	sgesl
	movl	52(%rsp), %ecx
	cmpl	$7, %ecx
	jg	.LBB0_23
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %esi
	movq	%r13, %rdi
	callq	matdump
	movl	52(%rsp), %ecx
	cmpl	$7, %ecx
	jg	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %rdi
	movl	$.L.str.3, %edx
	movl	%ecx, %esi
	callq	fvecdump
	movl	52(%rsp), %esi
	movl	$.L.str.4, %edx
	movq	%r12, %rdi
	callq	fvecdump
	movl	52(%rsp), %ecx
.LBB0_23:                               # %.thread
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rbx
	movq	16(%rsp), %r13
	movl	$2, %r8d
	xorl	%eax, %eax
	movl	%ecx, %edi
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	callq	vexopy
	movl	52(%rsp), %edi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	snrm2
	movq	32(%rsp), %rbp
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r12
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	sgesl
	movl	52(%rsp), %ecx
	cmpl	$7, %ecx
	jg	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.5, %edx
	movq	%rbp, %rdi
	movl	%ecx, %esi
	callq	fvecdump
	movl	52(%rsp), %ecx
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$2, %r8d
	xorl	%eax, %eax
	movl	%ecx, %edi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	callq	vexopy
	movl	52(%rsp), %edi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	snrm2
	movq	%r12, %r13
	leaq	16(%rsp), %r12
.LBB0_26:                               # %.backedge
                                        #   in Loop: Header=BB0_2 Depth=1
	incl	%r14d
	movl	$40, (%rsp)
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	40(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	24(%rsp), %r8
	movl	%r14d, %r9d
	callq	matgen
	testl	%eax, %eax
	movaps	.LCPI0_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm7, %xmm7
	je	.LBB0_2
.LBB0_27:                               # %._crit_edge47
	xorl	%eax, %eax
	addq	$8056, %rsp             # imm = 0x1F78
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_28:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4038384144200923364     # double 9.9999999999999996E-39
.LCPI1_5:
	.quad	5175426328141668785     # double 9.9999999999999997E+37
.LCPI1_8:
	.quad	4621819117588971520     # double 10
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI1_2:
	.long	2147483648              # 0x80000000
	.long	0                       # 0x0
	.long	2147483648              # 0x80000000
	.long	0                       # 0x0
.LCPI1_3:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI1_4:
	.quad	5175426328141668785     # double 9.9999999999999997E+37
	.quad	5175426328141668785     # double 9.9999999999999997E+37
.LCPI1_6:
	.long	1065353216              # float 1
	.long	1073741824              # float 2
	.long	1077936128              # float 3
	.long	1082130432              # float 4
.LCPI1_7:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI1_13:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_9:
	.long	1120403456              # float 100
.LCPI1_10:
	.long	1065353216              # float 1
.LCPI1_11:
	.long	1082130432              # float 4
.LCPI1_12:
	.long	1077936128              # float 3
	.text
	.globl	matgen
	.p2align	4, 0x90
	.type	matgen,@function
matgen:                                 # @matgen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r12
	cmpl	$2, %r15d
	jl	.LBB1_5
# BB#1:
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rbx, %r14
	movl	$.Lstr.3, %edi
	callq	puts
	cmpl	$0, 4(%r12)
	jle	.LBB1_4
# BB#2:                                 # %.lr.ph395.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph395
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12,%rbx,8), %rdi
	xorl	%eax, %eax
	callq	free
	incq	%rbx
	movslq	4(%r12), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_3
.LBB1_4:                                # %._crit_edge396
	movq	(%rbp), %rdi
	xorl	%eax, %eax
	callq	free
	movq	(%r13), %rdi
	xorl	%eax, %eax
	callq	free
	movq	%r14, %rbx
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	free
	movq	(%rsp), %r8             # 8-byte Reload
.LBB1_5:
	leal	-1(%r15), %eax
	cmpl	$13, %eax
	ja	.LBB1_34
# BB#6:
	movl	144(%rsp), %ecx
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_7:
	imull	%r15d, %ecx
	movq	%r12, %r14
	leal	(%rcx,%rcx,2), %r12d
	movl	%r12d, (%r14)
	movl	%r12d, 4(%r14)
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	get_space
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#8:
	movq	%rdx, %r13
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%r12d, %edx
	callq	printf
	testl	%r12d, %r12d
	jle	.LBB1_107
# BB#9:                                 # %.lr.ph336.split.us.preheader
	movl	%r12d, %eax
	xorl	%ecx, %ecx
	movss	.LCPI1_10(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph336.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx,%rcx,8), %rdx
	leaq	-3(%rcx), %rdi
	leaq	2(%rcx), %rbp
	leaq	1(%rcx), %r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %rbx
	xorps	%xmm1, %xmm1
	jl	.LBB1_14
# BB#12:                                #   in Loop: Header=BB1_11 Depth=2
	cmpq	%rbp, %rbx
	jg	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_11 Depth=2
	leal	1(%rcx,%rbx), %esi
	cvtsi2ssl	%esi, %xmm2
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
.LBB1_14:                               #   in Loop: Header=BB1_11 Depth=2
	movss	%xmm1, (%rdx,%rbx,4)
	incq	%rbx
	cmpl	%ebx, %eax
	jne	.LBB1_11
# BB#15:                                # %._crit_edge.us
                                        #   in Loop: Header=BB1_10 Depth=1
	cmpq	%rax, %r8
	movq	%r8, %rcx
	jne	.LBB1_10
	jmp	.LBB1_57
.LBB1_16:
	leal	(%rcx,%rcx,4), %eax
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rax,%rax,2), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	get_space
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#17:
	movl	144(%rsp), %r13d
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%r12d, %edx
	callq	printf
	cmpl	$7, %r15d
	movss	.LCPI1_9(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI1_10(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	je	.LBB1_19
# BB#18:
	movaps	%xmm2, %xmm1
.LBB1_19:
	cmpl	$8, %r15d
	movq	%r14, %rbp
	je	.LBB1_21
# BB#20:
	movaps	%xmm2, %xmm0
.LBB1_21:
	testl	%r13d, %r13d
	jle	.LBB1_99
# BB#22:                                # %.lr.ph345.split.us.preheader
	movslq	%r12d, %rax
	xorl	%ecx, %ecx
	movss	.LCPI1_11(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph345.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_24 Depth 2
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx,%rcx,8), %rsi
	leaq	1(%rcx), %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_24:                               #   Parent Loop BB1_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdi), %rbp
	movl	$0, (%rsi,%rdi,4)
	cmpq	%rdi, %rcx
	movaps	%xmm2, %xmm3
	je	.LBB1_27
# BB#25:                                #   in Loop: Header=BB1_24 Depth=2
	cmpl	%ecx, %ebp
	movaps	%xmm1, %xmm3
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_24 Depth=2
	cmpq	%rdi, %rdx
	movaps	%xmm0, %xmm3
	jne	.LBB1_28
	.p2align	4, 0x90
.LBB1_27:                               # %.sink.split.us
                                        #   in Loop: Header=BB1_24 Depth=2
	movss	%xmm3, (%rsi,%rdi,4)
.LBB1_28:                               #   in Loop: Header=BB1_24 Depth=2
	cmpl	%r12d, %ebp
	movq	%rbp, %rdi
	jl	.LBB1_24
# BB#29:                                # %._crit_edge342.us
                                        #   in Loop: Header=BB1_23 Depth=1
	cmpq	%rax, %rdx
	movq	%rdx, %rcx
	jl	.LBB1_23
# BB#30:
	movq	%r14, %rbp
	jmp	.LBB1_99
.LBB1_31:
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%r12)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#32:                                # %.thread
	movq	%rdx, %r14
	movq	%r12, (%rsp)            # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	$1, %r12d
	movl	$.L.str.8, %edi
	movl	$1, %edx
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	cmpl	$4, %r15d
	je	.LBB1_108
# BB#33:                                # %.thread
	xorps	%xmm0, %xmm0
	jmp	.LBB1_109
.LBB1_34:
	movl	$.Lstr.2, %edi
	jmp	.LBB1_125
.LBB1_35:
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rcx,%rcx,4), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#36:
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r15, %rbp
	xorl	%r15d, %r15d
	movl	$.L.str.10, %edi
	movl	$9, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	printf
	testl	%ebp, %ebp
	jle	.LBB1_110
# BB#37:                                # %.lr.ph353.split.us.preheader
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movslq	%r12d, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph353.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_39 Depth 2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%rbx,8), %rbp
	movl	%r15d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_39:                               #   Parent Loop BB1_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r14d, %xmm1
	movsd	.LCPI1_8(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbp,%r13,4)
	incq	%r13
	incl	%r14d
	cmpl	%r12d, %r13d
	jl	.LBB1_39
# BB#40:                                # %._crit_edge350.us
                                        #   in Loop: Header=BB1_38 Depth=1
	incq	%rbx
	decl	%r15d
	cmpq	48(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB1_38
# BB#41:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_99
.LBB1_42:
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(,%rcx,4), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#43:
	movq	%rdx, %r13
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
	movl	$.L.str.11, %edi
	movl	$10, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB1_57
# BB#44:                                # %.lr.ph361.split.us.preheader
	movslq	%r12d, %rax
	testl	%r12d, %r12d
	movl	$1, %ecx
	cmovgl	%r12d, %ecx
	decl	%ecx
	incq	%rcx
	movq	%rcx, %rdx
	movabsq	$8589934588, %rsi       # imm = 0x1FFFFFFFC
	andq	%rsi, %rdx
	leaq	-4(%rdx), %r9
	shrq	$2, %r9
	movl	%r9d, %r8d
	andl	$1, %r8d
	xorl	%ebp, %ebp
	movaps	.LCPI1_6(%rip), %xmm0   # xmm0 = [1.000000e+00,2.000000e+00,3.000000e+00,4.000000e+00]
	movdqa	.LCPI1_7(%rip), %xmm1   # xmm1 = [1,2,3,4]
	.p2align	4, 0x90
.LBB1_45:                               # %.lr.ph361.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_52 Depth 2
                                        #     Child Loop BB1_55 Depth 2
	leal	-2(%rbp), %ebx
	cmpq	$4, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsi,%rbp,8), %rdi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebx, %xmm2
	jae	.LBB1_47
# BB#46:                                #   in Loop: Header=BB1_45 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_47:                               # %min.iters.checked487
                                        #   in Loop: Header=BB1_45 Depth=1
	testq	%rdx, %rdx
	je	.LBB1_50
# BB#48:                                # %vector.ph491
                                        #   in Loop: Header=BB1_45 Depth=1
	movaps	%xmm2, %xmm3
	shufps	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	testq	%r8, %r8
	jne	.LBB1_51
# BB#49:                                # %vector.body482.prol
                                        #   in Loop: Header=BB1_45 Depth=1
	movaps	%xmm3, %xmm4
	divps	%xmm0, %xmm4
	movups	%xmm4, (%rdi)
	movl	$4, %ebx
	testq	%r9, %r9
	jne	.LBB1_52
	jmp	.LBB1_53
.LBB1_50:                               #   in Loop: Header=BB1_45 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB1_55
.LBB1_51:                               #   in Loop: Header=BB1_45 Depth=1
	xorl	%ebx, %ebx
	testq	%r9, %r9
	je	.LBB1_53
	.p2align	4, 0x90
.LBB1_52:                               # %vector.body482
                                        #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%ebx, %xmm4
	pshufd	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	paddd	%xmm1, %xmm4
	cvtdq2ps	%xmm4, %xmm4
	movaps	%xmm3, %xmm5
	divps	%xmm4, %xmm5
	movups	%xmm5, (%rdi,%rbx,4)
	leal	4(%rbx), %esi
	movd	%esi, %xmm4
	pshufd	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	paddd	%xmm1, %xmm4
	cvtdq2ps	%xmm4, %xmm4
	movaps	%xmm3, %xmm5
	divps	%xmm4, %xmm5
	movups	%xmm5, 16(%rdi,%rbx,4)
	addq	$8, %rbx
	cmpq	%rdx, %rbx
	jne	.LBB1_52
.LBB1_53:                               # %middle.block483
                                        #   in Loop: Header=BB1_45 Depth=1
	cmpq	%rdx, %rcx
	je	.LBB1_56
# BB#54:                                #   in Loop: Header=BB1_45 Depth=1
	leaq	(%rdi,%rdx,4), %rdi
	movl	%edx, %ebx
	.p2align	4, 0x90
.LBB1_55:                               # %scalar.ph484
                                        #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	movaps	%xmm2, %xmm4
	divss	%xmm3, %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rdi
	cmpl	%r12d, %ebx
	jl	.LBB1_55
.LBB1_56:                               # %._crit_edge358.us
                                        #   in Loop: Header=BB1_45 Depth=1
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB1_45
.LBB1_57:
	movq	%r14, %rbp
	movq	%r13, %rbx
	jmp	.LBB1_99
.LBB1_58:
	leal	(%rcx,%rcx), %eax
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rax,%rax,2), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#59:
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.12, %edi
	movl	$11, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB1_98
# BB#60:                                # %.lr.ph369.split.us.preheader
	movslq	%r12d, %rax
	movl	$1, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_61:                               # %.lr.ph369.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_62 Depth 2
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsi,%rdx,8), %rsi
	movl	%ecx, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_62:                               #   Parent Loop BB1_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cmpq	%rdx, %rbp
	jg	.LBB1_64
# BB#63:                                #   in Loop: Header=BB1_62 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	cvtss2sd	%xmm0, %xmm0
.LBB1_64:                               #   in Loop: Header=BB1_62 Depth=2
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rsi,%rbp,4)
	incq	%rbp
	incl	%edi
	cmpl	%r12d, %ebp
	jl	.LBB1_62
# BB#65:                                # %._crit_edge366.us
                                        #   in Loop: Header=BB1_61 Depth=1
	incq	%rdx
	decl	%ecx
	cmpq	%rax, %rdx
	jl	.LBB1_61
	jmp	.LBB1_98
.LBB1_66:
	leal	(%rcx,%rcx), %eax
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rax,%rax,2), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#67:
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.13, %edi
	movl	$12, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB1_98
# BB#68:                                # %.lr.ph377.split.us.preheader
	movslq	%r12d, %rax
	movl	$1, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_69:                               # %.lr.ph377.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_70 Depth 2
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsi,%rdx,8), %rsi
	movl	%ecx, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_70:                               #   Parent Loop BB1_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cmpq	%rdx, %rbp
	jl	.LBB1_72
# BB#71:                                #   in Loop: Header=BB1_70 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	cvtss2sd	%xmm0, %xmm0
.LBB1_72:                               #   in Loop: Header=BB1_70 Depth=2
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rsi,%rbp,4)
	incq	%rbp
	incl	%edi
	cmpl	%r12d, %ebp
	jl	.LBB1_70
# BB#73:                                # %._crit_edge374.us
                                        #   in Loop: Header=BB1_69 Depth=1
	incq	%rdx
	decl	%ecx
	cmpq	%rax, %rdx
	jl	.LBB1_69
	jmp	.LBB1_98
.LBB1_74:
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rcx,%rcx,4), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#75:
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movsd	.LCPI1_5(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$.L.str.14, %edi
	movl	$13, %esi
	movb	$1, %al
	movl	%r12d, %edx
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB1_98
# BB#76:                                # %.lr.ph384
	movl	%r12d, %eax
	imull	%eax, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movslq	%r12d, %r8
	testl	%r12d, %r12d
	movl	$1, %r9d
	cmovgl	%r12d, %r9d
	decl	%r9d
	incq	%r9
	movq	%r9, %rdx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rdx
	movaps	%xmm0, %xmm15
	movlhps	%xmm15, %xmm15          # xmm15 = xmm15[0,0]
	xorl	%esi, %esi
	movdqa	.LCPI1_1(%rip), %xmm8   # xmm8 = [0,1,2,3]
	movdqa	.LCPI1_2(%rip), %xmm11  # xmm11 = [2147483648,0,2147483648,0]
	movdqa	.LCPI1_3(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movapd	.LCPI1_4(%rip), %xmm10  # xmm10 = [1.000000e+38,1.000000e+38]
	movsd	.LCPI1_5(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB1_77:                               # %.lr.ph381.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_80 Depth 2
                                        #     Child Loop BB1_84 Depth 2
	leaq	1(%rsi), %rdi
	cmpq	$4, %r9
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%rsi,8), %rax
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%edi, %xmm6
	jb	.LBB1_82
# BB#78:                                # %min.iters.checked456
                                        #   in Loop: Header=BB1_77 Depth=1
	testq	%rdx, %rdx
	je	.LBB1_82
# BB#79:                                # %vector.ph460
                                        #   in Loop: Header=BB1_77 Depth=1
	leaq	(%rax,%rdx,4), %rbp
	movd	%rsi, %xmm2
	pshufd	$68, %xmm2, %xmm12      # xmm12 = xmm2[0,1,0,1]
	movd	%esi, %xmm2
	pshufd	$0, %xmm2, %xmm13       # xmm13 = xmm2[0,0,0,0]
	movaps	%xmm6, %xmm14
	shufps	$0, %xmm14, %xmm14      # xmm14 = xmm14[0,0,0,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_80:                               # %vector.body451
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%ecx, %xmm5
	pshufd	$0, %xmm5, %xmm2        # xmm2 = xmm5[0,0,0,0]
	paddd	%xmm8, %xmm2
	pshufd	$78, %xmm2, %xmm7       # xmm7 = xmm2[2,3,0,1]
	movdqa	%xmm7, %xmm3
	psrad	$31, %xmm3
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	movdqa	%xmm2, %xmm5
	punpckldq	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movdqa	%xmm12, %xmm3
	pxor	%xmm11, %xmm3
	pxor	%xmm11, %xmm5
	movdqa	%xmm5, %xmm4
	pcmpgtd	%xmm3, %xmm4
	pshufd	$160, %xmm4, %xmm1      # xmm1 = xmm4[0,0,2,2]
	pcmpeqd	%xmm3, %xmm5
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pand	%xmm1, %xmm5
	pshufd	$245, %xmm4, %xmm1      # xmm1 = xmm4[1,1,3,3]
	por	%xmm5, %xmm1
	pxor	%xmm11, %xmm7
	movdqa	%xmm7, %xmm4
	pcmpgtd	%xmm3, %xmm4
	pshufd	$160, %xmm4, %xmm5      # xmm5 = xmm4[0,0,2,2]
	pcmpeqd	%xmm3, %xmm7
	pshufd	$245, %xmm7, %xmm3      # xmm3 = xmm7[1,1,3,3]
	pand	%xmm5, %xmm3
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	por	%xmm3, %xmm4
	shufps	$136, %xmm4, %xmm1      # xmm1 = xmm1[0,2],xmm4[0,2]
	pslld	$31, %xmm1
	psrad	$31, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm13, %xmm1
	por	%xmm2, %xmm1
	paddd	%xmm9, %xmm1
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm14, %xmm2
	divps	%xmm1, %xmm2
	cvtps2pd	%xmm2, %xmm1
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	cvtps2pd	%xmm2, %xmm2
	mulpd	%xmm10, %xmm1
	mulpd	%xmm10, %xmm2
	divpd	%xmm15, %xmm2
	divpd	%xmm15, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	cvtpd2ps	%xmm2, %xmm2
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movupd	%xmm1, (%rax,%rcx,4)
	addq	$4, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB1_80
# BB#81:                                # %middle.block452
                                        #   in Loop: Header=BB1_77 Depth=1
	cmpq	%rdx, %r9
	movl	%edx, %eax
	movsd	.LCPI1_5(%rip), %xmm3   # xmm3 = mem[0],zero
	jne	.LBB1_83
	jmp	.LBB1_85
	.p2align	4, 0x90
.LBB1_82:                               #   in Loop: Header=BB1_77 Depth=1
	movq	%rax, %rbp
	xorl	%eax, %eax
.LBB1_83:                               # %scalar.ph453.preheader
                                        #   in Loop: Header=BB1_77 Depth=1
	cltq
	.p2align	4, 0x90
.LBB1_84:                               # %scalar.ph453
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rax
	movl	%esi, %ecx
	cmovgl	%eax, %ecx
	incl	%ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	movaps	%xmm6, %xmm2
	divss	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rbp)
	incq	%rax
	addq	$4, %rbp
	cmpl	%r12d, %eax
	jl	.LBB1_84
.LBB1_85:                               # %..loopexit326_crit_edge.us
                                        #   in Loop: Header=BB1_77 Depth=1
	cmpq	%r8, %rdi
	movq	%rdi, %rsi
	jl	.LBB1_77
	jmp	.LBB1_98
.LBB1_86:
	movq	%rbp, %r14
	movq	%r12, %rbp
	leal	(%rcx,%rcx,4), %r12d
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rcx, %r15
	movq	%rbx, %rcx
	callq	get_space
	movq	%rbx, %rdx
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_126
# BB#87:
	movq	%rdx, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$.L.str.15, %edi
	movl	$14, %esi
	movb	$1, %al
	movl	%r12d, %edx
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB1_98
# BB#88:                                # %.lr.ph392.split.us.preheader
	movl	%r12d, %eax
	imull	%eax, %eax
	cvtsi2ssl	%eax, %xmm14
	movslq	%r12d, %r8
	testl	%r12d, %r12d
	movl	$1, %r9d
	cmovgl	%r12d, %r9d
	decl	%r9d
	incq	%r9
	movq	%r9, %rdx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rdx
	movaps	%xmm14, %xmm11
	shufps	$0, %xmm11, %xmm11      # xmm11 = xmm11[0,0,0,0]
	xorl	%esi, %esi
	movsd	.LCPI1_5(%rip), %xmm5   # xmm5 = mem[0],zero
	movdqa	.LCPI1_1(%rip), %xmm10  # xmm10 = [0,1,2,3]
	movdqa	.LCPI1_2(%rip), %xmm0   # xmm0 = [2147483648,0,2147483648,0]
	movdqa	.LCPI1_3(%rip), %xmm12  # xmm12 = [1,1,1,1]
	movapd	.LCPI1_4(%rip), %xmm13  # xmm13 = [1.000000e+38,1.000000e+38]
	.p2align	4, 0x90
.LBB1_89:                               # %.lr.ph392.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_92 Depth 2
                                        #     Child Loop BB1_96 Depth 2
	leaq	1(%rsi), %rdi
	cmpq	$4, %r9
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%rsi,8), %rax
	xorps	%xmm7, %xmm7
	cvtsi2ssl	%edi, %xmm7
	jb	.LBB1_94
# BB#90:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_89 Depth=1
	testq	%rdx, %rdx
	je	.LBB1_94
# BB#91:                                # %vector.ph
                                        #   in Loop: Header=BB1_89 Depth=1
	leaq	(%rax,%rdx,4), %rbp
	movd	%rsi, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	movdqa	%xmm1, 48(%rsp)         # 16-byte Spill
	movd	%esi, %xmm1
	pshufd	$0, %xmm1, %xmm15       # xmm15 = xmm1[0,0,0,0]
	movaps	%xmm7, %xmm9
	shufps	$0, %xmm9, %xmm9        # xmm9 = xmm9[0,0,0,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_92:                               # %vector.body
                                        #   Parent Loop BB1_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%ecx, %xmm6
	pshufd	$0, %xmm6, %xmm3        # xmm3 = xmm6[0,0,0,0]
	paddd	%xmm10, %xmm3
	pshufd	$78, %xmm3, %xmm4       # xmm4 = xmm3[2,3,0,1]
	movdqa	%xmm4, %xmm2
	psrad	$31, %xmm2
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm3, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm3, %xmm6
	punpckldq	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	movdqa	48(%rsp), %xmm2         # 16-byte Reload
	pxor	%xmm0, %xmm2
	pxor	%xmm0, %xmm6
	movdqa	%xmm6, %xmm5
	pcmpgtd	%xmm2, %xmm5
	pshufd	$160, %xmm5, %xmm8      # xmm8 = xmm5[0,0,2,2]
	pcmpeqd	%xmm2, %xmm6
	pshufd	$245, %xmm6, %xmm6      # xmm6 = xmm6[1,1,3,3]
	pand	%xmm8, %xmm6
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	por	%xmm6, %xmm5
	pxor	%xmm0, %xmm4
	movdqa	%xmm4, %xmm6
	pcmpgtd	%xmm2, %xmm6
	pshufd	$160, %xmm6, %xmm1      # xmm1 = xmm6[0,0,2,2]
	pcmpeqd	%xmm2, %xmm4
	pshufd	$245, %xmm4, %xmm2      # xmm2 = xmm4[1,1,3,3]
	pand	%xmm1, %xmm2
	pshufd	$245, %xmm6, %xmm1      # xmm1 = xmm6[1,1,3,3]
	por	%xmm2, %xmm1
	shufps	$136, %xmm1, %xmm5      # xmm5 = xmm5[0,2],xmm1[0,2]
	pslld	$31, %xmm5
	psrad	$31, %xmm5
	pand	%xmm5, %xmm3
	pandn	%xmm15, %xmm5
	por	%xmm3, %xmm5
	paddd	%xmm12, %xmm5
	cvtdq2ps	%xmm5, %xmm1
	divps	%xmm9, %xmm1
	mulps	%xmm11, %xmm1
	cvtps2pd	%xmm1, %xmm2
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	cvtps2pd	%xmm1, %xmm1
	divpd	%xmm13, %xmm1
	divpd	%xmm13, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm1, %xmm1
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movupd	%xmm2, (%rax,%rcx,4)
	addq	$4, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB1_92
# BB#93:                                # %middle.block
                                        #   in Loop: Header=BB1_89 Depth=1
	cmpq	%rdx, %r9
	movl	%edx, %eax
	movsd	.LCPI1_5(%rip), %xmm5   # xmm5 = mem[0],zero
	jne	.LBB1_95
	jmp	.LBB1_97
	.p2align	4, 0x90
.LBB1_94:                               #   in Loop: Header=BB1_89 Depth=1
	movq	%rax, %rbp
	xorl	%eax, %eax
.LBB1_95:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_89 Depth=1
	cltq
	.p2align	4, 0x90
.LBB1_96:                               # %scalar.ph
                                        #   Parent Loop BB1_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rax
	movl	%esi, %ecx
	cmovgl	%eax, %ecx
	incl	%ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm7, %xmm1
	mulss	%xmm14, %xmm1
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm5, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rbp)
	incq	%rax
	addq	$4, %rbp
	cmpl	%r12d, %eax
	jl	.LBB1_96
.LBB1_97:                               # %._crit_edge389.us
                                        #   in Loop: Header=BB1_89 Depth=1
	cmpq	%r8, %rdi
	movq	%rdi, %rsi
	jl	.LBB1_89
.LBB1_98:
	movq	%r13, %rbp
.LBB1_99:                               # %.loopexit320
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r11
	movl	$1065353216, (%r11)     # imm = 0x3F800000
	cmpl	$2, %r12d
	jl	.LBB1_120
# BB#100:
	movq	(%rbx), %rax
	movl	$0, (%rax)
	cmpl	$2, %r12d
	jne	.LBB1_102
# BB#101:
	movl	$2, %r12d
	jmp	.LBB1_120
.LBB1_102:                              # %.lr.ph.preheader
	leaq	8(%r11), %rcx
	leal	-3(%r12), %eax
	incq	%rax
	movl	$2, %edi
	cmpq	$2, %rax
	jb	.LBB1_118
# BB#103:                               # %min.iters.checked511
	movabsq	$8589934588, %rdx       # imm = 0x1FFFFFFFC
	leaq	2(%rdx), %rsi
	movq	%rax, %rdx
	andq	%rsi, %rdx
	andq	%rax, %rsi
	je	.LBB1_118
# BB#104:                               # %vector.body507.preheader
	movq	%rbx, %r9
	movq	%rbp, %r8
	leaq	-2(%rsi), %rbx
	movl	%ebx, %edi
	shrl	%edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_111
# BB#105:                               # %vector.body507.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	movaps	.LCPI1_13(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB1_106:                              # %vector.body507.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx,%rbp,4), %xmm1  # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm1
	movlps	%xmm1, (%rcx,%rbp,4)
	addq	$2, %rbp
	incq	%rdi
	jne	.LBB1_106
	jmp	.LBB1_112
.LBB1_107:                              # %.loopexit320.thread
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r11
	movl	$1065353216, (%r11)     # imm = 0x3F800000
	movq	%r14, %rbp
	movq	%r13, %rbx
	jmp	.LBB1_120
.LBB1_108:
	movss	.LCPI1_12(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
.LBB1_109:                              # %.thread
	movq	%r13, %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rax
	movss	%xmm0, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r11
	movl	$1065353216, (%r11)     # imm = 0x3F800000
	movq	%r14, %rbx
	jmp	.LBB1_120
.LBB1_110:
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_99
.LBB1_111:
	xorl	%ebp, %ebp
.LBB1_112:                              # %vector.body507.prol.loopexit
	cmpq	$6, %rbx
	jb	.LBB1_115
# BB#113:                               # %vector.body507.preheader.new
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	leaq	32(%r11,%rbp,4), %rbp
	movdqa	.LCPI1_13(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB1_114:                              # %vector.body507
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%rbp), %rbx
	movd	%rbx, %xmm1
	pxor	%xmm0, %xmm1
	movq	%xmm1, -24(%rbp)
	movq	%rbx, -16(%rbp)
	movq	%xmm1, -8(%rbp)
	movq	%rbx, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_114
.LBB1_115:                              # %middle.block508
	cmpq	%rsi, %rax
	jne	.LBB1_117
# BB#116:
	movq	%r8, %rbp
	movq	%r9, %rbx
	jmp	.LBB1_120
.LBB1_117:
	addl	$2, %edx
	leaq	(%rcx,%rsi,4), %rcx
	movl	%edx, %edi
	movq	%r8, %rbp
	movq	%r9, %rbx
.LBB1_118:                              # %.lr.ph.preheader615
	movaps	.LCPI1_13(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB1_119:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm1
	movss	%xmm1, (%rcx)
	incl	%edi
	addq	$4, %rcx
	cmpl	%edi, %r12d
	jne	.LBB1_119
.LBB1_120:                              # %.thread319
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %r15
	testq	%r15, %r15
	jle	.LBB1_124
# BB#121:
	movslq	4(%rax), %r10
	testq	%r10, %r10
	jle	.LBB1_124
# BB#122:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%rbp), %rdx
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rax), %rsi
	leal	-1(%r10), %r9d
	incq	%r9
	xorl	%ebp, %ebp
	cmpq	$7, %r9
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	ja	.LBB1_127
# BB#123:
	movq	%rsi, %rcx
	movq	%rdx, %rdi
	jmp	.LBB1_139
.LBB1_124:                              # %matvec.exit
	movl	$.Lstr.1, %edi
.LBB1_125:                              # %.critedge
	callq	puts
	movl	$1, %ebx
.LBB1_126:                              # %.critedge
	movl	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_127:                              # %min.iters.checked533
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	leaq	-4(%rax), %rdx
	andq	%r9, %rdx
	je	.LBB1_132
# BB#128:                               # %vector.memcheck
	leal	-1(%r10), %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rax,4), %rcx
	leaq	4(%rsi,%rax,4), %rax
	cmpq	%rax, %rdi
	sbbb	%al, %al
	cmpq	%rcx, %rsi
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r11, %rdi
	sbbb	%al, %al
	cmpq	%rcx, %r11
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB1_133
# BB#129:                               # %vector.memcheck
	andb	%cl, %al
	andb	$1, %al
	movq	%rsi, %rcx
	jne	.LBB1_139
# BB#130:                               # %vector.body529.preheader
	leaq	-8(%rdx), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	jb	.LBB1_134
# BB#131:                               # %vector.body529.prol
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movss	(%r11), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	mulps	%xmm2, %xmm0
	mulps	%xmm1, %xmm2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movups	%xmm0, (%rcx)
	movups	%xmm2, 16(%rcx)
	movl	$8, %edi
	testq	%rax, %rax
	jne	.LBB1_135
	jmp	.LBB1_137
.LBB1_132:
	movq	%rsi, %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_139
.LBB1_133:
	movq	%rsi, %rcx
	jmp	.LBB1_139
.LBB1_134:
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.LBB1_137
.LBB1_135:                              # %vector.body529.preheader.new
	movss	(%r11), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	48(%rcx,%rdi,4), %rcx
	leaq	48(%rsi,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_136:                              # %vector.body529
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm1
	movups	-32(%rdi), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	%xmm1, -48(%rcx)
	movups	%xmm2, -32(%rcx)
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	%xmm1, -16(%rcx)
	movups	%xmm2, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdi
	addq	$-16, %rax
	jne	.LBB1_136
.LBB1_137:                              # %middle.block530
	cmpq	%rdx, %r9
	je	.LBB1_145
# BB#138:
	leaq	(%rsi,%rdx,4), %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rdi
	movl	%edx, %ebp
.LBB1_139:                              # %scalar.ph531.preheader
	movl	%r10d, %edx
	subl	%ebp, %edx
	leal	-1(%r10), %eax
	subl	%ebp, %eax
	andl	$3, %edx
	je	.LBB1_142
# BB#140:                               # %scalar.ph531.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB1_141:                              # %scalar.ph531.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r11), %xmm0
	movss	%xmm0, (%rdi)
	incl	%ebp
	addq	$4, %rdi
	addq	$4, %rcx
	incl	%edx
	jne	.LBB1_141
.LBB1_142:                              # %scalar.ph531.prol.loopexit
	cmpl	$3, %eax
	jb	.LBB1_145
# BB#143:                               # %scalar.ph531.preheader.new
	movl	%r10d, %eax
	subl	%ebp, %eax
	.p2align	4, 0x90
.LBB1_144:                              # %scalar.ph531
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r11), %xmm0
	movss	%xmm0, (%rdi)
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	(%r11), %xmm0
	movss	%xmm0, 4(%rdi)
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	(%r11), %xmm0
	movss	%xmm0, 8(%rdi)
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	(%r11), %xmm0
	movss	%xmm0, 12(%rdi)
	addq	$16, %rcx
	addq	$16, %rdi
	addl	$-4, %eax
	jne	.LBB1_144
.LBB1_145:                              # %.preheader.i
	movq	%r15, 48(%rsp)          # 8-byte Spill
	cmpl	$2, %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jl	.LBB1_163
# BB#146:                               # %.lr.ph74.split.us.preheader.i
	leaq	(%rdx,%r9,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leal	-1(%r10), %r8d
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	addq	$-4, %rax
	andq	%r9, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	16(%rdx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	movq	%r9, %r15
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_148
.LBB1_147:                              #   in Loop: Header=BB1_148 Depth=1
	movq	%rax, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB1_156
	.p2align	4, 0x90
.LBB1_148:                              # %.lr.ph74.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_153 Depth 2
                                        #     Child Loop BB1_161 Depth 2
	xorl	%r13d, %r13d
	cmpq	$8, %r9
	leaq	(%r11,%rbp,4), %rcx
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%rbp,8), %rax
	jb	.LBB1_155
# BB#149:                               # %min.iters.checked571
                                        #   in Loop: Header=BB1_148 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB1_155
# BB#150:                               # %vector.memcheck590
                                        #   in Loop: Header=BB1_148 Depth=1
	leaq	(%rax,%r9,4), %rdi
	cmpq	%rdi, %rdx
	sbbb	%bl, %bl
	cmpq	%r14, %rax
	movq	%rdx, %rdi
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rcx, %rdi
	sbbb	%bl, %bl
	cmpq	%r14, %rcx
	sbbb	%dil, %dil
	testb	$1, %dl
	jne	.LBB1_147
# BB#151:                               # %vector.memcheck590
                                        #   in Loop: Header=BB1_148 Depth=1
	andb	%dil, %bl
	andb	$1, %bl
	jne	.LBB1_147
# BB#152:                               # %vector.body567.preheader
                                        #   in Loop: Header=BB1_148 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rdi
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	addq	$16, %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_153:                              # %vector.body567
                                        #   Parent Loop BB1_148 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	-16(%rbx), %xmm3
	movups	(%rbx), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbx)
	movups	%xmm4, (%rbx)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$-8, %rdx
	jne	.LBB1_153
# BB#154:                               # %middle.block568
                                        #   in Loop: Header=BB1_148 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %r9
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	%eax, %r13d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jne	.LBB1_157
	jmp	.LBB1_162
	.p2align	4, 0x90
.LBB1_155:                              #   in Loop: Header=BB1_148 Depth=1
	movq	%rax, %rdi
.LBB1_156:                              # %scalar.ph569.preheader
                                        #   in Loop: Header=BB1_148 Depth=1
	movq	%rdx, %r9
.LBB1_157:                              # %scalar.ph569.preheader
                                        #   in Loop: Header=BB1_148 Depth=1
	movl	%r10d, %eax
	subl	%r13d, %eax
	testb	$1, %al
	jne	.LBB1_159
# BB#158:                               #   in Loop: Header=BB1_148 Depth=1
	movl	%r13d, %ebx
	cmpl	%r13d, %r8d
	jne	.LBB1_160
	jmp	.LBB1_162
	.p2align	4, 0x90
.LBB1_159:                              # %scalar.ph569.prol
                                        #   in Loop: Header=BB1_148 Depth=1
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%r9), %xmm0
	movss	%xmm0, (%r9)
	leal	1(%r13), %ebx
	addq	$4, %r9
	addq	$4, %rdi
	cmpl	%r13d, %r8d
	je	.LBB1_162
.LBB1_160:                              # %scalar.ph569.preheader.new
                                        #   in Loop: Header=BB1_148 Depth=1
	movl	%r10d, %eax
	subl	%ebx, %eax
	.p2align	4, 0x90
.LBB1_161:                              # %scalar.ph569
                                        #   Parent Loop BB1_148 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%r9), %xmm0
	movss	%xmm0, (%r9)
	movss	4(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	4(%r9), %xmm0
	movss	%xmm0, 4(%r9)
	addq	$8, %rdi
	addq	$8, %r9
	addl	$-2, %eax
	jne	.LBB1_161
.LBB1_162:                              # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_148 Depth=1
	incq	%rbp
	cmpq	48(%rsp), %rbp          # 8-byte Folded Reload
	movq	%r15, %r9
	jne	.LBB1_148
.LBB1_163:                              # %.loopexit
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	(%r13), %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	leal	-1(%r9), %r8d
	movl	%r9d, %ebp
	andl	$3, %ebp
	xorl	%edx, %edx
	jmp	.LBB1_165
	.p2align	4, 0x90
.LBB1_164:                              # %._crit_edge85.us.i..lr.ph88.split.us.i_crit_edge
                                        #   in Loop: Header=BB1_165 Depth=1
	addq	$4, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rax, %rdx
.LBB1_165:                              # %.lr.ph88.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_167 Depth 2
                                        #     Child Loop BB1_171 Depth 2
	testl	%ebp, %ebp
	movl	$0, (%rcx)
	je	.LBB1_168
# BB#166:                               # %.prol.preheader
                                        #   in Loop: Header=BB1_165 Depth=1
	xorl	%edi, %edi
	xorps	%xmm0, %xmm0
	movq	%r11, %rax
	.p2align	4, 0x90
.LBB1_167:                              #   Parent Loop BB1_165 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	incl	%edi
	addq	$4, %rax
	addq	$4, %rsi
	cmpl	%edi, %ebp
	jne	.LBB1_167
	jmp	.LBB1_169
	.p2align	4, 0x90
.LBB1_168:                              #   in Loop: Header=BB1_165 Depth=1
	xorps	%xmm0, %xmm0
	movq	%r11, %rax
	xorl	%edi, %edi
.LBB1_169:                              # %.prol.loopexit
                                        #   in Loop: Header=BB1_165 Depth=1
	cmpl	$3, %r8d
	jb	.LBB1_172
# BB#170:                               # %.lr.ph88.split.us.i.new
                                        #   in Loop: Header=BB1_165 Depth=1
	movl	%r9d, %ebx
	subl	%edi, %ebx
	.p2align	4, 0x90
.LBB1_171:                              #   Parent Loop BB1_165 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rcx)
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	4(%rax), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rcx)
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rax), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	addq	$16, %rsi
	addq	$16, %rax
	addl	$-4, %ebx
	jne	.LBB1_171
.LBB1_172:                              # %._crit_edge85.us.i
                                        #   in Loop: Header=BB1_165 Depth=1
	leaq	1(%rdx), %rax
	cmpq	%r10, %rax
	jne	.LBB1_164
# BB#173:                               # %matvec.exit316
	xorl	%ebx, %ebx
	cmpl	$7, %r12d
	jg	.LBB1_126
# BB#174:
	movl	$.L.str.18, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	matdump
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	movl	$.L.str.19, %edx
	movl	%r12d, %esi
	callq	fvecdump
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movl	$.L.str.20, %edx
	movl	%r12d, %esi
	callq	fvecdump
	movq	(%r13), %rdi
	movl	$.L.str.21, %edx
	movl	%r12d, %esi
	callq	fvecdump
	jmp	.LBB1_126
.Lfunc_end1:
	.size	matgen, .Lfunc_end1-matgen
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_31
	.quad	.LBB1_31
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_35
	.quad	.LBB1_42
	.quad	.LBB1_58
	.quad	.LBB1_66
	.quad	.LBB1_74
	.quad	.LBB1_86

	.text
	.globl	get_space
	.p2align	4, 0x90
	.type	get_space,@function
get_space:                              # @get_space
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movslq	(%rbx), %rdi
	shlq	$2, %rdi
	movl	4(%rbx), %ebp
	xorl	%eax, %eax
	callq	malloc
	cmpl	$0, %ebp
	jle	.LBB2_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, 8(%rbx,%rbp,8)
	testq	%rax, %rax
	je	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_3 Depth=1
	incq	%rbp
	movslq	4(%rbx), %r15
	movslq	(%rbx), %rdi
	shlq	$2, %rdi
	xorl	%eax, %eax
	callq	malloc
	cmpq	%r15, %rbp
	jl	.LBB2_3
.LBB2_5:                                # %._crit_edge
	movq	%rax, (%r13)
	movslq	(%rbx), %rdi
	shlq	$2, %rdi
	xorl	%eax, %eax
	callq	malloc
	movq	%rax, (%r12)
	movslq	(%rbx), %rdi
	shlq	$2, %rdi
	xorl	%eax, %eax
	callq	malloc
	movq	%rax, (%r14)
	movslq	(%rbx), %rdi
	shlq	$2, %rdi
	xorl	%eax, %eax
	callq	malloc
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	cmpq	$0, (%r13)
	je	.LBB2_9
# BB#6:
	cmpq	$0, (%r12)
	je	.LBB2_9
# BB#7:
	testq	%rax, %rax
	je	.LBB2_9
# BB#8:
	movq	(%r14), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB2_11
.LBB2_9:
	movl	$.Lstr.4, %edi
	jmp	.LBB2_10
.LBB2_4:
	movl	$.Lstr.5, %edi
.LBB2_10:
	callq	puts
	movl	$1, %eax
.LBB2_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	get_space, .Lfunc_end2-get_space
	.cfi_endproc

	.globl	matvec
	.p2align	4, 0x90
	.type	matvec,@function
matvec:                                 # @matvec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movslq	(%rdi), %r8
	testq	%r8, %r8
	movl	$1, %eax
	jle	.LBB3_38
# BB#1:
	movslq	4(%rdi), %r9
	testq	%r9, %r9
	jle	.LBB3_38
# BB#2:
	testl	%ecx, %ecx
	je	.LBB3_11
# BB#3:                                 # %.lr.ph88.split.us.preheader
	leal	-1(%r8), %r10d
	movl	%r8d, %ebp
	andl	$3, %ebp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph88.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
                                        #     Child Loop BB3_9 Depth 2
	testl	%ebp, %ebp
	movq	8(%rdi,%r11,8), %rax
	movl	$0, (%rdx)
	je	.LBB3_5
# BB#6:                                 # %.prol.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdx)
	incl	%ebx
	addq	$4, %rcx
	addq	$4, %rax
	cmpl	%ebx, %ebp
	jne	.LBB3_7
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_4 Depth=1
	xorps	%xmm0, %xmm0
	movq	%rsi, %rcx
	xorl	%ebx, %ebx
.LBB3_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpl	$3, %r10d
	jb	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	4(%rcx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdx)
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rcx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdx)
	addl	$4, %ebx
	addq	$16, %rax
	addq	$16, %rcx
	cmpl	%r8d, %ebx
	jl	.LBB3_9
.LBB3_10:                               # %._crit_edge85.us
                                        #   in Loop: Header=BB3_4 Depth=1
	incq	%r11
	addq	$4, %rdx
	cmpq	%r9, %r11
	jl	.LBB3_4
	jmp	.LBB3_37
.LBB3_11:                               # %.lr.ph79
	movabsq	$8589934584, %r10       # imm = 0x1FFFFFFF8
	movq	8(%rdi), %rax
	leal	-1(%r9), %r14d
	incq	%r14
	xorl	%ebp, %ebp
	cmpq	$7, %r14
	jbe	.LBB3_12
# BB#13:                                # %min.iters.checked
	movq	%r14, %r11
	andq	%r10, %r11
	je	.LBB3_12
# BB#14:                                # %vector.memcheck
	leal	-1(%r9), %ecx
	leaq	4(%rdx,%rcx,4), %r15
	leaq	4(%rax,%rcx,4), %rcx
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	cmpq	%r15, %rax
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%rsi, %rdx
	sbbb	%cl, %cl
	cmpq	%r15, %rsi
	sbbb	%r15b, %r15b
	testb	$1, %bl
	jne	.LBB3_12
# BB#15:                                # %vector.memcheck
	andb	%r15b, %cl
	andb	$1, %cl
	jne	.LBB3_12
# BB#16:                                # %vector.body.preheader
	leaq	-8(%r11), %rbx
	movq	%rbx, %rcx
	shrq	$3, %rcx
	btl	$3, %ebx
	jb	.LBB3_17
# BB#18:                                # %vector.body.prol
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	mulps	%xmm2, %xmm0
	mulps	%xmm1, %xmm2
	movups	%xmm0, (%rdx)
	movups	%xmm2, 16(%rdx)
	movl	$8, %r15d
	testq	%rcx, %rcx
	jne	.LBB3_20
	jmp	.LBB3_22
.LBB3_12:
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB3_24:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm0
	movss	%xmm0, (%rcx)
	incl	%ebp
	addq	$4, %rcx
	addq	$4, %rax
	cmpl	%r9d, %ebp
	jl	.LBB3_24
.LBB3_25:                               # %.preheader
	xorl	%eax, %eax
	cmpl	$2, %r8d
	jl	.LBB3_38
# BB#26:                                # %.lr.ph74.split.us.preheader
	testl	%r9d, %r9d
	movl	$1, %r12d
	cmovgl	%r9d, %r12d
	decl	%r12d
	leaq	4(%rdx,%r12,4), %r15
	incq	%r12
	andq	%r12, %r10
	leaq	(%rdx,%r10,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	16(%rdx), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph74.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_33 Depth 2
                                        #     Child Loop BB3_35 Depth 2
	xorl	%ebp, %ebp
	cmpq	$8, %r12
	leaq	(%rsi,%r13,4), %rcx
	movq	8(%rdi,%r13,8), %rbx
	jb	.LBB3_28
# BB#29:                                # %min.iters.checked116
                                        #   in Loop: Header=BB3_27 Depth=1
	testq	%r10, %r10
	je	.LBB3_28
# BB#30:                                # %vector.memcheck136
                                        #   in Loop: Header=BB3_27 Depth=1
	leaq	(%rbx,%r12,4), %rax
	cmpq	%rax, %rdx
	sbbb	%al, %al
	cmpq	%r15, %rbx
	sbbb	%r11b, %r11b
	andb	%al, %r11b
	cmpq	%rcx, %rdx
	sbbb	%al, %al
	cmpq	%r15, %rcx
	sbbb	%r14b, %r14b
	testb	$1, %r11b
	jne	.LBB3_28
# BB#31:                                # %vector.memcheck136
                                        #   in Loop: Header=BB3_27 Depth=1
	andb	%r14b, %al
	andb	$1, %al
	jne	.LBB3_28
# BB#32:                                # %vector.body112.preheader
                                        #   in Loop: Header=BB3_27 Depth=1
	leaq	(%rbx,%r10,4), %rax
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	addq	$16, %rbx
	movq	%r10, %rbp
	movq	-16(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_33:                               # %vector.body112
                                        #   Parent Loop BB3_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbx), %xmm1
	movups	(%rbx), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	-16(%r14), %xmm3
	movups	(%r14), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%r14)
	movups	%xmm4, (%r14)
	addq	$32, %r14
	addq	$32, %rbx
	addq	$-8, %rbp
	jne	.LBB3_33
# BB#34:                                # %middle.block113
                                        #   in Loop: Header=BB3_27 Depth=1
	cmpq	%r10, %r12
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movl	%r10d, %ebp
	jne	.LBB3_35
	jmp	.LBB3_36
	.p2align	4, 0x90
.LBB3_28:                               #   in Loop: Header=BB3_27 Depth=1
	movq	%rbx, %rax
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB3_35:                               # %scalar.ph114
                                        #   Parent Loop BB3_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
	incl	%ebp
	addq	$4, %rbx
	addq	$4, %rax
	cmpl	%r9d, %ebp
	jl	.LBB3_35
.LBB3_36:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_27 Depth=1
	incq	%r13
	cmpq	%r8, %r13
	jl	.LBB3_27
.LBB3_37:
	xorl	%eax, %eax
.LBB3_38:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:
	xorl	%r15d, %r15d
	testq	%rcx, %rcx
	je	.LBB3_22
.LBB3_20:                               # %vector.body.preheader.new
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%r11, %rbx
	subq	%r15, %rbx
	leaq	48(%rdx,%r15,4), %rbp
	leaq	48(%rax,%r15,4), %rcx
	.p2align	4, 0x90
.LBB3_21:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rcx), %xmm1
	movups	-32(%rcx), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	%xmm1, -48(%rbp)
	movups	%xmm2, -32(%rbp)
	movups	-16(%rcx), %xmm1
	movups	(%rcx), %xmm2
	mulps	%xmm0, %xmm1
	mulps	%xmm0, %xmm2
	movups	%xmm1, -16(%rbp)
	movups	%xmm2, (%rbp)
	addq	$64, %rbp
	addq	$64, %rcx
	addq	$-16, %rbx
	jne	.LBB3_21
.LBB3_22:                               # %middle.block
	cmpq	%r11, %r14
	je	.LBB3_25
# BB#23:
	leaq	(%rax,%r11,4), %rax
	leaq	(%rdx,%r11,4), %rcx
	movl	%r11d, %ebp
	jmp	.LBB3_24
.Lfunc_end3:
	.size	matvec, .Lfunc_end3-matvec
	.cfi_endproc

	.globl	matdump
	.p2align	4, 0x90
	.type	matdump,@function
matdump:                                # @matdump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 64
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	(%r14), %rbx
	movq	%rsi, %rdi
	callq	puts
	cmpl	$0, 4(%r14)
	jle	.LBB4_18
# BB#1:                                 # %.lr.ph51
	imulq	$715827883, %rbx, %r15  # imm = 0x2AAAAAAB
	movq	%r15, %rax
	shrq	$63, %rax
	shrq	$32, %r15
	addl	%eax, %r15d
	imull	$-6, %r15d, %r12d
	addl	%ebx, %r12d
	cmpl	$6, %ebx
	jl	.LBB4_11
# BB#2:                                 # %.lr.ph51.split.us.preheader
	leaq	8(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph51.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
                                        #     Child Loop BB4_8 Depth 2
	movl	%r12d, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%r12d, %r12d
	je	.LBB4_5
# BB#10:                                #   in Loop: Header=BB4_4 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
.LBB4_5:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB4_4 Depth=2
	movslq	%ebp, %rbp
	movq	8(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movq	16(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movq	24(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movq	32(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movq	40(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movq	48(%r14,%rbp,8), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	addq	$6, %rbp
	movl	$10, %edi
	callq	putchar
	incl	%r12d
	cmpl	%r15d, %r12d
	jl	.LBB4_4
# BB#6:                                 # %..preheader41_crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %r12d
	testl	%r12d, %r12d
	jle	.LBB4_9
# BB#7:                                 # %.lr.ph48.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebp, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	addq	$8, %rbp
	decl	%ebx
	jne	.LBB4_8
.LBB4_9:                                # %._crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$10, %edi
	callq	putchar
	incq	%r13
	movslq	4(%r14), %rax
	cmpq	%rax, %r13
	jl	.LBB4_3
	jmp	.LBB4_18
.LBB4_11:                               # %.lr.ph51.split
	testl	%r12d, %r12d
	jle	.LBB4_12
# BB#14:                                # %.lr.ph51.split.split.us.preheader
	movl	%r12d, %r15d
	leaq	8(%r14), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph51.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	movq	%r15, %rbp
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB4_16:                               #   Parent Loop BB4_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movss	(%rax,%r13,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB4_16
# BB#17:                                # %._crit_edge.us70
                                        #   in Loop: Header=BB4_15 Depth=1
	movl	$10, %edi
	callq	putchar
	incq	%r13
	movslq	4(%r14), %rax
	cmpq	%rax, %r13
	jl	.LBB4_15
	jmp	.LBB4_18
.LBB4_12:                               # %.lr.ph51.split.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph51.split.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	$10, %edi
	callq	putchar
	incl	%ebx
	cmpl	4(%r14), %ebx
	jl	.LBB4_13
.LBB4_18:                               # %._crit_edge52
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end4:
	.size	matdump, .Lfunc_end4-matdump
	.cfi_endproc

	.globl	fvecdump
	.p2align	4, 0x90
	.type	fvecdump,@function
fvecdump:                               # @fvecdump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 64
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movslq	%esi, %rbx
	imulq	$715827883, %rbx, %r15  # imm = 0x2AAAAAAB
	movq	%r15, %rax
	shrq	$63, %rax
	shrq	$32, %r15
	addl	%eax, %r15d
	imull	$-6, %r15d, %r14d
	addl	%ebx, %r14d
	movq	%rdx, %rdi
	callq	puts
	xorl	%r13d, %r13d
	cmpl	$6, %ebx
	jl	.LBB5_3
# BB#1:                                 # %.lr.ph44.preheader
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rbp
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	leaq	24(%rbp), %r12
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movss	12(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movss	16(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	addl	$6, %r13d
	movl	$10, %edi
	callq	putchar
	incl	%ebx
	cmpl	%r15d, %ebx
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge45
	testl	%r14d, %r14d
	je	.LBB5_7
# BB#4:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	testl	%r14d, %r14d
	jle	.LBB5_6
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.27, %edi
	movb	$1, %al
	callq	printf
	addq	$4, %r12
	decl	%r14d
	jne	.LBB5_5
.LBB5_6:                                # %._crit_edge
	movl	$10, %edi
	callq	putchar
.LBB5_7:
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end5:
	.size	fvecdump, .Lfunc_end5-fvecdump
	.cfi_endproc

	.globl	ivecdump
	.p2align	4, 0x90
	.type	ivecdump,@function
ivecdump:                               # @ivecdump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 64
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movslq	%esi, %rbx
	imulq	$954437177, %rbx, %r15  # imm = 0x38E38E39
	movq	%r15, %rax
	shrq	$63, %rax
	sarq	$33, %r15
	addl	%eax, %r15d
	imull	$-6, %r15d, %r14d
	addl	%ebx, %r14d
	movq	%rdx, %rdi
	callq	puts
	xorl	%r13d, %r13d
	cmpl	$9, %ebx
	jl	.LBB6_3
# BB#1:                                 # %.lr.ph44.preheader
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rbp
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	leaq	36(%rbp), %r12
	movl	(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	16(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	20(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	24(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	28(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	32(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	addl	$9, %r13d
	movl	$10, %edi
	callq	putchar
	incl	%ebx
	cmpl	%r15d, %ebx
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge45
	testl	%r14d, %r14d
	je	.LBB6_7
# BB#4:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	testl	%r14d, %r14d
	jle	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	addq	$4, %r12
	decl	%r14d
	jne	.LBB6_5
.LBB6_6:                                # %._crit_edge
	movl	$10, %edi
	callq	putchar
.LBB6_7:
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end6:
	.size	ivecdump, .Lfunc_end6-ivecdump
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Matrix row dim (%d) or column dim (%d) too big.\n"
	.size	.L.str, 49

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"One-Norm(A) ---------- %e.\n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FACTORED MATRIX FOLLOWS"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"True Solution"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Solution"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Solution to transposed system"
	.size	.L.str.5, 30

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Hilbert Slice.  Test case %d of size %d.\n"
	.size	.L.str.7, 42

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Monoelemental.  Test case %d of size %d.\n"
	.size	.L.str.8, 42

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Tridiagional.  Test case %d of size %d.\n"
	.size	.L.str.9, 41

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Rank One.  Test case %d of size %d.\n"
	.size	.L.str.10, 37

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Zero Column.  Test case %d of size %d.\n"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Upper Triangular.  Test case %d of size %d.\n"
	.size	.L.str.12, 45

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Lower Triangular.  Test case %d of size %d.\n"
	.size	.L.str.13, 45

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Near Overflow.  Test case %d of size %d. BIG = %e\n"
	.size	.L.str.14, 51

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Near Underflow.  Test case %d of size %d. SMALL = %e\n"
	.size	.L.str.15, 54

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"MATRIX FOLLOWS"
	.size	.L.str.18, 15

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SOLUTION"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"RIGHT HAND SIDE"
	.size	.L.str.20, 16

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"TRANSPOSE RIGHT HAND SIDE"
	.size	.L.str.21, 26

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%3d|"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"    "
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%12.4e"
	.size	.L.str.27, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%8d"
	.size	.L.str.29, 4

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"MATGEN: Error in matvec."
	.size	.Lstr.1, 25

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"MATGEN: All tests complete."
	.size	.Lstr.2, 28

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"\n\n**********************************************************************"
	.size	.Lstr.3, 73

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"GET_SPACE: Can't get enouph space for vectors..."
	.size	.Lstr.4, 49

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"GET_SPACE: Can't get enouph space for matricies..."
	.size	.Lstr.5, 51


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
