	.text
	.file	"btGImpactBvh.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1056964608              # float 0.5
.LCPI0_2:
	.long	1065353216              # float 1
.LCPI0_3:
	.long	3212836864              # float -1
	.text
	.globl	_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.p2align	4, 0x90
	.type	_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii,@function
_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii: # @_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.cfi_startproc
# BB#0:
	movl	%ecx, %r10d
	subl	%edx, %r10d
	xorps	%xmm3, %xmm3
	movaps	%xmm3, -24(%rsp)
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph130
	movq	16(%rsi), %rax
	movslq	%edx, %r9
	movslq	%ecx, %r11
	leaq	(%r9,%r9,8), %r8
	leaq	24(%rax,%r8,4), %rdi
	movq	%r11, %rax
	subq	%r9, %rax
	xorps	%xmm4, %xmm4
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = <0.5,0.5,u,u>
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	-24(%rdi), %xmm5        # xmm5 = mem[0],zero
	addps	%xmm2, %xmm5
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	-16(%rdi), %xmm2
	mulps	%xmm0, %xmm5
	mulss	%xmm1, %xmm2
	addps	%xmm5, %xmm3
	addss	%xmm2, %xmm4
	addq	$36, %rdi
	decq	%rax
	jne	.LBB0_3
# BB#4:                                 # %._crit_edge131
	cmpl	%edx, %ecx
	cvtsi2ssl	%r10d, %xmm8
	jle	.LBB0_5
# BB#6:                                 # %.lr.ph
	movss	.LCPI0_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm5
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	mulss	%xmm4, %xmm5
	movq	16(%rsi), %rax
	leaq	24(%rax,%r8,4), %rcx
	subq	%r9, %r11
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm7         # xmm7 = mem[0],zero
	movsd	-24(%rcx), %xmm2        # xmm2 = mem[0],zero
	addps	%xmm7, %xmm2
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	addss	-16(%rcx), %xmm7
	mulps	%xmm0, %xmm2
	mulss	%xmm1, %xmm7
	subps	%xmm6, %xmm2
	subss	%xmm5, %xmm7
	mulps	%xmm2, %xmm2
	mulss	%xmm7, %xmm7
	addps	%xmm2, %xmm4
	addss	%xmm7, %xmm3
	addq	$36, %rcx
	decq	%r11
	jne	.LBB0_7
# BB#8:                                 # %._crit_edge
	movss	%xmm4, -24(%rsp)
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, -20(%rsp)
	movss	%xmm3, -16(%rsp)
	jmp	.LBB0_9
.LBB0_1:                                # %._crit_edge131.thread
	cvtsi2ssl	%r10d, %xmm8
.LBB0_5:                                # %._crit_edge131._crit_edge
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
.LBB0_9:
	addss	.LCPI0_3(%rip), %xmm8
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	movss	%xmm1, -24(%rsp)
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	%xmm2, -20(%rsp)
	mulss	%xmm3, %xmm0
	movss	%xmm0, -16(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm1, %xmm2
	seta	%cl
	ucomiss	-24(%rsp,%rcx,4), %xmm0
	movl	$2, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end0:
	.size	_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii, .Lfunc_end0-_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1056964608              # float 0.5
.LCPI1_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.text
	.globl	_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.p2align	4, 0x90
	.type	_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii,@function
_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii: # @_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r14, -32
.Lcfi6:
	.cfi_offset %r15, -24
.Lcfi7:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%ecx, %r9d
	subl	%edx, %r9d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -72(%rsp)
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph94
	movq	16(%rsi), %r10
	movslq	%edx, %rbx
	movslq	%ecx, %rax
	leaq	(%rbx,%rbx,8), %rdi
	leaq	24(%r10,%rdi,4), %rdi
	subq	%rbx, %rax
	xorps	%xmm0, %xmm0
	movss	.LCPI1_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	addss	-24(%rdi), %xmm4
	addss	-20(%rdi), %xmm5
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	addss	-16(%rdi), %xmm6
	mulss	%xmm3, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	addss	%xmm4, %xmm2
	addss	%xmm5, %xmm1
	addss	%xmm6, %xmm0
	addq	$36, %rdi
	decq	%rax
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge95
	movss	%xmm2, -72(%rsp)
	movss	%xmm1, -68(%rsp)
	movss	%xmm0, -64(%rsp)
	jmp	.LBB1_5
.LBB1_1:                                # %._crit_edge106
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB1_5:
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r9d, %xmm3
	movss	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	mulss	%xmm4, %xmm2
	movss	%xmm2, -72(%rsp)
	mulss	%xmm4, %xmm1
	movss	%xmm1, -68(%rsp)
	mulss	%xmm0, %xmm4
	movss	%xmm4, -64(%rsp)
	cmpl	%edx, %ecx
	movl	%edx, %eax
	jle	.LBB1_10
# BB#6:                                 # %.lr.ph
	movslq	%r8d, %r10
	movss	-72(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	leaq	-24(%rsp), %r8
	movslq	%edx, %rax
	movslq	%ecx, %r11
	leaq	(%rax,%rax,8), %rdi
	leaq	16(,%rdi,4), %rdi
	subq	%rax, %r11
	movaps	.LCPI1_2(%rip), %xmm1   # xmm1 = <0.5,0.5,u,u>
	movss	.LCPI1_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %rbx
	movsd	(%rbx,%rdi), %xmm3      # xmm3 = mem[0],zero
	movsd	-16(%rbx,%rdi), %xmm4   # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	movss	8(%rbx,%rdi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	addss	-8(%rbx,%rdi), %xmm3
	mulps	%xmm1, %xmm4
	mulss	%xmm2, %xmm3
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm4, -56(%rsp)
	movlps	%xmm5, -48(%rsp)
	movss	-56(%rsp,%r10,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	jbe	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	movups	-16(%rbx,%rdi), %xmm3
	movaps	%xmm3, -40(%rsp)
	movups	(%rbx,%rdi), %xmm3
	movups	%xmm3, (%r8)
	movl	16(%rbx,%rdi), %r14d
	cltq
	leaq	(,%rax,4), %rbp
	leaq	(%rbp,%rbp,8), %r15
	movl	32(%rbx,%r15), %ebp
	movl	%ebp, 16(%rbx,%rdi)
	movups	(%rbx,%r15), %xmm3
	movups	16(%rbx,%r15), %xmm4
	movups	%xmm4, (%rbx,%rdi)
	movups	%xmm3, -16(%rbx,%rdi)
	movq	16(%rsi), %rbx
	movaps	-40(%rsp), %xmm3
	movaps	-24(%rsp), %xmm4
	movups	%xmm4, 16(%rbx,%r15)
	movups	%xmm3, (%rbx,%r15)
	movl	%r14d, 32(%rbx,%r15)
	incl	%eax
.LBB1_9:                                #   in Loop: Header=BB1_7 Depth=1
	addq	$36, %rdi
	decq	%r11
	jne	.LBB1_7
.LBB1_10:                               # %._crit_edge
	movslq	%r9d, %rsi
	imulq	$1431655766, %rsi, %rsi # imm = 0x55555556
	movq	%rsi, %rdi
	shrq	$63, %rdi
	shrq	$32, %rsi
	addl	%edi, %esi
	leal	(%rsi,%rdx), %edi
	cmpl	%edi, %eax
	jle	.LBB1_12
# BB#11:
	decl	%ecx
	subl	%esi, %ecx
	cmpl	%ecx, %eax
	jl	.LBB1_13
.LBB1_12:                               # %.critedge
	sarl	%r9d
	addl	%edx, %r9d
	movl	%r9d, %eax
.LBB1_13:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii, .Lfunc_end1-_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI2_1:
	.long	4286578687              # float -3.40282347E+38
	.text
	.globl	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.p2align	4, 0x90
	.type	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii,@function
_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii: # @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 96
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %r15
	movslq	(%r15), %r14
	leal	1(%r14), %eax
	movl	%eax, (%r15)
	movl	%ebx, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jne	.LBB2_2
# BB#1:
	movq	16(%r12), %rax
	movslq	%r13d, %rcx
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	movq	24(%r15), %rdx
	shlq	$2, %r14
	leaq	(%r14,%r14,8), %rsi
	movups	(%rax,%rcx), %xmm0
	movups	16(%rax,%rcx), %xmm1
	movups	%xmm1, 16(%rdx,%rsi)
	movups	%xmm0, (%rdx,%rsi)
	movq	24(%r15), %rax
	movq	16(%r12), %rdx
	movl	32(%rdx,%rcx), %ecx
	movl	%ecx, 32(%rax,%rsi)
	jmp	.LBB2_6
.LBB2_2:
	movq	%r12, %rsi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	callq	_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	movq	%r12, %rsi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	movl	%eax, %r8d
	callq	_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	movabsq	$9187343237679939583, %rcx # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rcx, 8(%rsp)
	movl	$2139095039, 16(%rsp)   # imm = 0x7F7FFFFF
	movl	$-8388609, 24(%rsp)     # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rcx # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rcx, 28(%rsp)
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	cmpl	%r13d, %ebx
	jle	.LBB2_5
# BB#3:                                 # %.lr.ph
	leaq	24(%rsp), %r8
	movq	16(%r12), %rdx
	movslq	%r13d, %rsi
	movslq	4(%rsp), %r11           # 4-byte Folded Reload
	leaq	(%rsi,%rsi,8), %rdi
	leaq	16(%rdx,%rdi,4), %rdx
	subq	%rsi, %r11
	movd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	leaq	8(%rsp), %r9
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm3
	movdqa	%xmm2, %xmm4
	movdqa	%xmm2, %xmm5
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	leaq	-16(%rdx), %rdi
	ucomiss	-16(%rdx), %xmm5
	movq	%r9, %rsi
	cmovaq	%rdi, %rsi
	movl	(%rsi), %r10d
	movl	%r10d, 8(%rsp)
	ucomiss	-12(%rdx), %xmm4
	movq	%r9, %rbp
	cmovaq	%rdi, %rbp
	movl	4(%rbp), %ebp
	movl	%ebp, 12(%rsp)
	ucomiss	-8(%rdx), %xmm2
	cmovbeq	%r9, %rdi
	movl	8(%rdi), %edi
	movl	%edi, 16(%rsp)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movq	%r8, %rsi
	cmovaq	%rdx, %rsi
	movl	(%rsi), %ecx
	movl	%ecx, 24(%rsp)
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r8, %rbx
	cmovaq	%rdx, %rbx
	movl	4(%rbx), %ebx
	movl	%ebx, 28(%rsp)
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	%r8, %rsi
	cmovaq	%rdx, %rsi
	movl	8(%rsi), %esi
	movl	%esi, 32(%rsp)
	movd	%r10d, %xmm5
	movd	%ebp, %xmm4
	movd	%edi, %xmm2
	movd	%ecx, %xmm3
	movd	%ebx, %xmm1
	movd	%esi, %xmm0
	addq	$36, %rdx
	decq	%r11
	jne	.LBB2_4
.LBB2_5:                                # %._crit_edge
	movq	24(%r15), %rdx
	movq	%r14, %rcx
	shlq	$2, %rcx
	movq	%r12, %rbx
	leaq	(%rcx,%rcx,8), %r12
	movups	8(%rsp), %xmm0
	movups	24(%rsp), %xmm1
	movups	%xmm1, 16(%rdx,%r12)
	movups	%xmm0, (%rdx,%r12)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	movl	%eax, %ecx
	movl	%eax, %ebp
	callq	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	callq	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	movq	24(%r15), %rax
	movl	(%r15), %ecx
	subl	%ecx, %r14d
	movl	%r14d, 32(%rax,%r12)
.LBB2_6:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii, .Lfunc_end2-_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.cfi_endproc

	.globl	_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.p2align	4, 0x90
	.type	_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY,@function
_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY: # @_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 96
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$0, (%r15)
	movslq	4(%r14), %rcx
	movq	%rcx, %r12
	addq	%r12, %r12
	movl	12(%r15), %edx
	cmpl	%r12d, %edx
	jge	.LBB3_24
# BB#1:
	movslq	%edx, %rbp
	cmpl	%r12d, 16(%r15)
	jge	.LBB3_2
# BB#3:
	testl	%ecx, %ecx
	je	.LBB3_4
# BB#5:
	movq	%r12, %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	12(%r15), %edx
	jmp	.LBB3_6
.LBB3_2:                                # %..lr.ph.i_crit_edge
	leaq	24(%r15), %r13
	jmp	.LBB3_18
.LBB3_4:
	xorl	%ebx, %ebx
.LBB3_6:                                # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi.exit.i.i
	leaq	24(%r15), %r13
	testl	%edx, %edx
	jle	.LBB3_13
# BB#7:                                 # %.lr.ph.i.i.i
	movslq	%edx, %rax
	testb	$1, %al
	jne	.LBB3_9
# BB#8:
	xorl	%ecx, %ecx
	cmpl	$1, %edx
	jne	.LBB3_11
	jmp	.LBB3_13
.LBB3_9:
	movq	(%r13), %rcx
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rbx)
	movups	16(%rcx), %xmm0
	movups	%xmm0, 16(%rbx)
	movl	32(%rcx), %ecx
	movl	%ecx, 32(%rbx)
	movl	$1, %ecx
	cmpl	$1, %edx
	je	.LBB3_13
.LBB3_11:                               # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	movups	16(%rdx,%rcx), %xmm0
	movups	%xmm0, 16(%rbx,%rcx)
	movl	32(%rdx,%rcx), %edx
	movl	%edx, 32(%rbx,%rcx)
	movq	(%r13), %rdx
	movups	36(%rdx,%rcx), %xmm0
	movups	%xmm0, 36(%rbx,%rcx)
	movups	52(%rdx,%rcx), %xmm0
	movups	%xmm0, 52(%rbx,%rcx)
	movl	68(%rdx,%rcx), %edx
	movl	%edx, 68(%rbx,%rcx)
	addq	$72, %rcx
	addq	$-2, %rax
	jne	.LBB3_12
.LBB3_13:                               # %_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_.exit.i.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#14:
	cmpb	$0, 32(%r15)
	je	.LBB3_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB3_16:
	movq	$0, (%r13)
.LBB3_17:                               # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi.exit.preheader.i
	movb	$1, 32(%r15)
	movq	%rbx, 24(%r15)
	movl	%r12d, 16(%r15)
.LBB3_18:                               # %.lr.ph.i
	movslq	%r12d, %rax
	leaq	24(%rsp), %rcx
	leaq	-1(%rax), %rdx
	testb	$1, %bpl
	movq	%rbp, %rsi
	je	.LBB3_20
# BB#19:                                # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi.exit.i.prol
	movq	(%r13), %rsi
	leaq	(%rbp,%rbp,8), %rdi
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rsi,%rdi,4)
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%rsi,%rdi,4)
	movl	$0, 32(%rsi,%rdi,4)
	leaq	1(%rbp), %rsi
.LBB3_20:                               # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi.exit.i.prol.loopexit
	cmpq	%rbp, %rdx
	je	.LBB3_23
# BB#21:                                # %.lr.ph.i.new
	subq	%rsi, %rax
	shlq	$2, %rsi
	leaq	(%rsi,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB3_22:                               # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rsi
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rsi,%rdx)
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%rsi,%rdx)
	movl	$0, 32(%rsi,%rdx)
	movq	(%r13), %rsi
	movups	8(%rsp), %xmm0
	movups	%xmm0, 36(%rsi,%rdx)
	movups	(%rcx), %xmm0
	movups	%xmm0, 52(%rsi,%rdx)
	movl	$0, 68(%rsi,%rdx)
	addq	$72, %rdx
	addq	$-2, %rax
	jne	.LBB3_22
.LBB3_23:                               # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_.exit.loopexit
	movl	4(%r14), %ecx
.LBB3_24:                               # %_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_.exit
	movl	%r12d, 12(%r15)
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii # TAILCALL
.Lfunc_end3:
	.size	_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY, .Lfunc_end3-_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI4_1:
	.long	4286578687              # float -3.40282347E+38
	.text
	.globl	_ZN12btGImpactBvh5refitEv
	.p2align	4, 0x90
	.type	_ZN12btGImpactBvh5refitEv,@function
_ZN12btGImpactBvh5refitEv:              # @_ZN12btGImpactBvh5refitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 128
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movslq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB4_7
# BB#1:                                 # %.lr.ph
	leaq	16(%rsp), %r15
	leaq	48(%rsp), %rdi
	movq	%rax, %rcx
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rbx
	leal	1(%rax), %ebp
	movq	%rsp, %r14
	leaq	32(%rsp), %r12
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r13), %rax
	movl	-4(%rax,%rbx), %esi
	testl	%esi, %esi
	js	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rdx
	callq	*32(%rax)
	leaq	48(%rsp), %rdi
	movq	24(%r13), %rax
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movabsq	$9187343237679939583, %rcx # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rcx, (%rsp)
	movl	$2139095039, 8(%rsp)    # imm = 0x7F7FFFFF
	movl	$-8388609, 16(%rsp)     # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rcx # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rcx, 20(%rsp)
	movups	(%rax,%rbx), %xmm0
	movups	16(%rax,%rbx), %xmm1
	movaps	%xmm1, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsp), %xmm0
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	movl	(%rcx), %r8d
	movl	%r8d, (%rsp)
	ucomiss	36(%rsp), %xmm0
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	orq	$4, %rcx
	movl	(%rcx), %r9d
	movl	%r9d, 4(%rsp)
	ucomiss	40(%rsp), %xmm0
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	movl	8(%rcx), %r10d
	movl	%r10d, 8(%rsp)
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movq	%r15, %rcx
	cmovaq	%rdi, %rcx
	movl	(%rcx), %r11d
	movl	%r11d, 16(%rsp)
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movq	%r15, %rcx
	cmovaq	%rdi, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 20(%rsp)
	movd	56(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movq	%r15, %rdx
	cmovaq	%rdi, %rdx
	movl	8(%rdx), %esi
	movl	%esi, 24(%rsp)
	movl	32(%rax,%rbx), %edi
	cmpl	$-2, %edi
	movl	$-1, %edx
	cmovgl	%edi, %edx
	subl	%edi, %edx
	leaq	48(%rsp), %rdi
	addl	%ebp, %edx
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	movd	%esi, %xmm0
	movd	%ecx, %xmm1
	movd	%r11d, %xmm2
	movd	%r10d, %xmm3
	movd	%r9d, %xmm4
	movd	%r8d, %xmm5
	movslq	%edx, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	movups	16(%rax,%rcx,4), %xmm6
	movaps	%xmm6, 48(%rsp)
	movups	(%rax,%rcx,4), %xmm6
	movaps	%xmm6, 32(%rsp)
	ucomiss	32(%rsp), %xmm5
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rsp)
	ucomiss	36(%rsp), %xmm4
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	orq	$4, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 4(%rsp)
	ucomiss	40(%rsp), %xmm3
	movq	%r14, %rcx
	cmovaq	%r12, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rsp)
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movq	%r15, %rcx
	cmovaq	%rdi, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 16(%rsp)
	movss	52(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r15, %rcx
	cmovaq	%rdi, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 20(%rsp)
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	%r15, %rcx
	cmovaq	%rdi, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 24(%rsp)
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	movups	(%rsp), %xmm0
	movups	16(%rsp), %xmm1
	movups	%xmm1, -20(%rax,%rbx)
	movups	%xmm0, -36(%rax,%rbx)
	addq	$-36, %rbx
	decl	%ebp
	cmpl	$1, %ebp
	jne	.LBB4_2
.LBB4_7:                                # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN12btGImpactBvh5refitEv, .Lfunc_end4-_ZN12btGImpactBvh5refitEv
	.cfi_endproc

	.globl	_ZN12btGImpactBvh8buildSetEv
	.p2align	4, 0x90
	.type	_ZN12btGImpactBvh8buildSetEv,@function
_ZN12btGImpactBvh8buildSetEv:           # @_ZN12btGImpactBvh8buildSetEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 112
.Lcfi53:
	.cfi_offset %rbx, -48
.Lcfi54:
	.cfi_offset %r12, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*24(%rax)
	movl	%eax, %r14d
.Ltmp1:
# BB#1:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	testl	%r14d, %r14d
	jle	.LBB5_2
# BB#9:
	movslq	%r14d, %rbp
	leaq	(,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rdi
.Ltmp2:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp3:
# BB#10:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi.exit.i.i
	movslq	4(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB5_17
# BB#11:                                # %.lr.ph.i.i.i
	movq	16(%rsp), %rdi
	testb	$1, %al
	jne	.LBB5_13
# BB#12:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB5_15
	jmp	.LBB5_18
.LBB5_2:                                # %.loopexit.thread
	movl	%r14d, 4(%rsp)
	jmp	.LBB5_3
.LBB5_17:                               # %_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_.exit.i.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_18
	jmp	.LBB5_21
.LBB5_13:
	movups	(%rdi), %xmm0
	movups	%xmm0, (%r15)
	movups	16(%rdi), %xmm0
	movups	%xmm0, 16(%r15)
	movl	32(%rdi), %ecx
	movl	%ecx, 32(%r15)
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB5_18
.LBB5_15:                               # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rsi
	leaq	(%rdi,%rsi), %rcx
	movq	%r15, %rdx
	addq	%rsi, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_16:                               # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rsi), %xmm0
	movups	%xmm0, (%rdx,%rsi)
	movups	16(%rcx,%rsi), %xmm0
	movups	%xmm0, 16(%rdx,%rsi)
	movl	32(%rcx,%rsi), %ebx
	movl	%ebx, 32(%rdx,%rsi)
	movups	36(%rcx,%rsi), %xmm0
	movups	%xmm0, 36(%rdx,%rsi)
	movups	52(%rcx,%rsi), %xmm0
	movups	%xmm0, 52(%rdx,%rsi)
	movl	68(%rcx,%rsi), %ebx
	movl	%ebx, 68(%rdx,%rsi)
	addq	$72, %rsi
	addq	$-2, %rax
	jne	.LBB5_16
.LBB5_18:                               # %_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_.exit.i.i.thread
	cmpb	$0, 24(%rsp)
	je	.LBB5_20
# BB#19:
.Ltmp4:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp5:
.LBB5_20:                               # %.noexc13
	movq	$0, 16(%rsp)
.LBB5_21:                               # %.lr.ph.i
	movb	$1, 24(%rsp)
	movq	%r15, 16(%rsp)
	movl	%r14d, 8(%rsp)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 16(%r15)
	movl	$0, 32(%r15)
	cmpl	$1, %r14d
	je	.LBB5_28
# BB#22:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.preheader
	leaq	48(%rsp), %rax
	testb	$1, %r14b
	jne	.LBB5_23
# BB#24:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.prol
	movq	16(%rsp), %rcx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 36(%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 52(%rcx)
	movl	$0, 68(%rcx)
	movl	$2, %ecx
	cmpl	$2, %r14d
	jne	.LBB5_26
	jmp	.LBB5_28
.LBB5_23:
	movl	$1, %ecx
	cmpl	$2, %r14d
	je	.LBB5_28
.LBB5_26:                               # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.preheader.new
	subq	%rcx, %rbp
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB5_27:                               # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 16(%rdx,%rcx)
	movl	$0, 32(%rdx,%rcx)
	movq	16(%rsp), %rdx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 36(%rdx,%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 52(%rdx,%rcx)
	movl	$0, 68(%rdx,%rcx)
	addq	$72, %rcx
	addq	$-2, %rbp
	jne	.LBB5_27
.LBB5_28:                               # %.loopexit
	movl	%r14d, 4(%rsp)
	testl	%r14d, %r14d
	jle	.LBB5_3
# BB#29:                                # %.lr.ph
	movq	16(%rsp), %rdx
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_30:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
	addq	%rbx, %rdx
.Ltmp7:
	movl	%ebp, %esi
	callq	*32(%rax)
.Ltmp8:
# BB#31:                                #   in Loop: Header=BB5_30 Depth=1
	movq	16(%rsp), %rdx
	movl	%ebp, 32(%rdx,%rbx)
	incq	%rbp
	movslq	4(%rsp), %rax
	addq	$36, %rbx
	cmpq	%rax, %rbp
	jl	.LBB5_30
.LBB5_3:                                # %._crit_edge
.Ltmp10:
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
.Ltmp11:
# BB#4:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#5:
	cmpb	$0, 24(%rsp)
	je	.LBB5_7
# BB#6:
	callq	_Z21btAlignedFreeInternalPv
.LBB5_7:
	movq	$0, 16(%rsp)
.LBB5_8:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev.exit15
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_32:
.Ltmp6:
	jmp	.LBB5_35
.LBB5_34:
.Ltmp12:
	jmp	.LBB5_35
.LBB5_33:
.Ltmp9:
.LBB5_35:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_39
# BB#36:
	cmpb	$0, 24(%rsp)
	je	.LBB5_38
# BB#37:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB5_38:                               # %.noexc
	movq	$0, 16(%rsp)
.LBB5_39:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_40:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN12btGImpactBvh8buildSetEv, .Lfunc_end5-_ZN12btGImpactBvh8buildSetEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp2           #   Call between .Ltmp2 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end5-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end6:
	.size	__clang_call_terminate, .Lfunc_end6-__clang_call_terminate

	.text
	.globl	_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE,@function
_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE: # @_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 112
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	(%rdi), %r9d
	testl	%r9d, %r9d
	jle	.LBB7_43
# BB#1:                                 # %.lr.ph
	leaq	4(%rdx), %r11
	xorl	%r13d, %r13d
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movq	%r11, 40(%rsp)          # 8-byte Spill
	jmp	.LBB7_2
.LBB7_43:                               # %.._crit_edge_crit_edge
	addq	$4, %rdx
	movq	%rdx, %r11
	jmp	.LBB7_44
.LBB7_20:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	%r8b, 7(%rsp)           # 1-byte Spill
	leaq	-8(%rcx), %r8
	movl	%r8d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB7_23
# BB#21:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	negq	%rbx
	xorl	%esi, %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_22:                               # %vector.body.prol
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rbx
	jne	.LBB7_22
	jmp	.LBB7_24
.LBB7_23:                               #   in Loop: Header=BB7_2 Depth=1
	xorl	%esi, %esi
.LBB7_24:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	$24, %r8
	jb	.LBB7_27
# BB#25:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rcx, %r8
	subq	%rsi, %r8
	leaq	112(%rdi,%rsi,4), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	112(%rax,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB7_26:                               # %vector.body
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rbx
	subq	$-128, %rsi
	addq	$-32, %r8
	jne	.LBB7_26
.LBB7_27:                               # %middle.block
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	%rcx, %rbp
	movb	7(%rsp), %r8b           # 1-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB7_34
	jmp	.LBB7_28
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_22 Depth 2
                                        #     Child Loop BB7_26 Depth 2
                                        #     Child Loop BB7_30 Depth 2
                                        #     Child Loop BB7_33 Depth 2
	movq	24(%rdi), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,8), %r15
	movss	(%rax,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%r12), %xmm0
	jbe	.LBB7_4
# BB#3:                                 # %_ZNK6btAABB13has_collisionERKS_.exit.thread
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, 32(%rax,%r15,4)
	setns	%r8b
	xorl	%r14d, %r14d
	jmp	.LBB7_39
	.p2align	4, 0x90
.LBB7_4:                                # %_ZNK6btAABB13has_collisionERKS_.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movss	8(%rax,%r15,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax,%r15,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	ucomiss	16(%rax,%r15,4), %xmm2
	setbe	%bl
	ucomiss	20(%r12), %xmm1
	setbe	%cl
	andb	%bl, %cl
	ucomiss	20(%rax,%r15,4), %xmm3
	setbe	%sil
	ucomiss	24(%r12), %xmm0
	setbe	%bl
	andb	%sil, %bl
	andb	%cl, %bl
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rax,%r15,4), %xmm0
	setbe	%r14b
	andb	%bl, %r14b
	movl	32(%rax,%r15,4), %esi
	testl	%esi, %esi
	setns	%r8b
	js	.LBB7_39
# BB#5:                                 # %_ZNK6btAABB13has_collisionERKS_.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	testb	%r14b, %r14b
	je	.LBB7_39
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	4(%rdx), %r10d
	cmpl	8(%rdx), %r10d
	jne	.LBB7_38
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	leal	(%r10,%r10), %ebp
	testl	%r10d, %r10d
	movl	$1, %ecx
	cmovel	%ecx, %ebp
	cmpl	%ebp, %r10d
	jge	.LBB7_38
# BB#8:                                 #   in Loop: Header=BB7_2 Depth=1
	testl	%ebp, %ebp
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	je	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_2 Depth=1
	movslq	%ebp, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	movl	%r8d, %ebx
	movq	%r11, %rbp
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbp, %r11
	movl	%ebx, %r8d
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	(%r11), %r10d
	jmp	.LBB7_11
.LBB7_10:                               #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
.LBB7_11:                               # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rdx), %rdi
	testl	%r10d, %r10d
	jle	.LBB7_14
# BB#12:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movslq	%r10d, %rbp
	cmpl	$8, %r10d
	jae	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%ecx, %ecx
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB7_28
.LBB7_14:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	testq	%rdi, %rdi
	movl	8(%rsp), %r11d          # 4-byte Reload
	jne	.LBB7_34
	jmp	.LBB7_37
.LBB7_15:                               # %min.iters.checked
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rbp, %rcx
	andq	$-8, %rcx
	movl	8(%rsp), %r11d          # 4-byte Reload
	je	.LBB7_19
# BB#16:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_2 Depth=1
	leaq	(%rdi,%rbp,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB7_20
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_2 Depth=1
	leaq	(%rax,%rbp,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB7_20
.LBB7_19:                               #   in Loop: Header=BB7_2 Depth=1
	xorl	%ecx, %ecx
.LBB7_28:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	subl	%ecx, %r10d
	leaq	-1(%rbp), %rsi
	subq	%rcx, %rsi
	andq	$7, %r10
	je	.LBB7_31
# BB#29:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	negq	%r10
	.p2align	4, 0x90
.LBB7_30:                               # %scalar.ph.prol
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx,4), %ebx
	movl	%ebx, (%rax,%rcx,4)
	incq	%rcx
	incq	%r10
	jne	.LBB7_30
.LBB7_31:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	$7, %rsi
	jb	.LBB7_34
# BB#32:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB7_2 Depth=1
	subq	%rcx, %rbp
	leaq	28(%rdi,%rcx,4), %rbx
	leaq	28(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB7_33:                               # %scalar.ph
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rbx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rbx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rbx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rbx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rbx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rbx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rbx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rbx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rbx
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB7_33
.LBB7_34:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpb	$0, 24(%rdx)
	je	.LBB7_36
# BB#35:                                #   in Loop: Header=BB7_2 Depth=1
	movl	%r11d, %ebp
	movq	%rax, %rbx
	movb	%r8b, 7(%rsp)           # 1-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movb	7(%rsp), %r8b           # 1-byte Reload
	movq	%rbx, %rax
	movl	%ebp, %r11d
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB7_36:                               #   in Loop: Header=BB7_2 Depth=1
	movq	$0, 16(%rdx)
	movl	4(%rdx), %r10d
.LBB7_37:                               # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movb	$1, 24(%rdx)
	movq	%rax, 16(%rdx)
	movl	%r11d, 8(%rdx)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB7_38:                               # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rdx), %rcx
	movslq	%r10d, %rax
	movl	%esi, (%rcx,%rax,4)
	incl	4(%rdx)
.LBB7_39:                               #   in Loop: Header=BB7_2 Depth=1
	movl	$1, %eax
	testb	%r14b, %r14b
	jne	.LBB7_42
# BB#40:                                #   in Loop: Header=BB7_2 Depth=1
	testb	%r8b, %r8b
	jne	.LBB7_42
# BB#41:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rdi), %rcx
	xorl	%eax, %eax
	subl	32(%rcx,%r15,4), %eax
.LBB7_42:                               #   in Loop: Header=BB7_2 Depth=1
	addl	%eax, %r13d
	cmpl	%r9d, %r13d
	jl	.LBB7_2
.LBB7_44:                               # %._crit_edge
	cmpl	$0, (%r11)
	setg	%al
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE, .Lfunc_end7-_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI8_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE,@function
_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE: # @_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 112
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.LBB8_50
# BB#1:                                 # %.lr.ph
	leaq	4(%rcx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movaps	.LCPI8_0(%rip), %xmm12  # xmm12 = <0.5,0.5,u,u>
	movss	.LCPI8_1(%rip), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movaps	.LCPI8_2(%rip), %xmm14  # xmm14 = [nan,nan,nan,nan]
	xorps	%xmm15, %xmm15
	movl	$1, %r8d
	jmp	.LBB8_37
.LBB8_2:                                # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	mulss	%xmm6, %xmm10
	mulss	%xmm8, %xmm1
	subss	%xmm1, %xmm10
	andps	%xmm14, %xmm10
	unpcklps	%xmm7, %xmm9    # xmm9 = xmm9[0],xmm7[0],xmm9[1],xmm7[1]
	mulps	%xmm9, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm0, %xmm1
	movl	32(%rax,%r14,4), %edx
	testl	%edx, %edx
	setns	%bpl
	ucomiss	%xmm1, %xmm10
	setbe	%r12b
	ja	.LBB8_46
# BB#3:                                 # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	testl	%edx, %edx
	js	.LBB8_46
# BB#4:                                 #   in Loop: Header=BB8_37 Depth=1
	movl	4(%rcx), %r11d
	cmpl	8(%rcx), %r11d
	jne	.LBB8_36
# BB#5:                                 #   in Loop: Header=BB8_37 Depth=1
	leal	(%r11,%r11), %r9d
	testl	%r11d, %r11d
	cmovel	%r8d, %r9d
	cmpl	%r9d, %r11d
	jge	.LBB8_36
# BB#6:                                 #   in Loop: Header=BB8_37 Depth=1
	testl	%r9d, %r9d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	je	.LBB8_8
# BB#7:                                 #   in Loop: Header=BB8_37 Depth=1
	movslq	%r9d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm15, %xmm15
	movaps	.LCPI8_2(%rip), %xmm14  # xmm14 = [nan,nan,nan,nan]
	movss	.LCPI8_1(%rip), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movaps	.LCPI8_0(%rip), %xmm12  # xmm12 = <0.5,0.5,u,u>
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %r11d
	jmp	.LBB8_9
.LBB8_8:                                #   in Loop: Header=BB8_37 Depth=1
	xorl	%eax, %eax
.LBB8_9:                                # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	16(%rcx), %rdi
	testl	%r11d, %r11d
	jle	.LBB8_12
# BB#10:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movslq	%r11d, %r9
	cmpl	$8, %r11d
	jae	.LBB8_13
# BB#11:                                #   in Loop: Header=BB8_37 Depth=1
	xorl	%edx, %edx
	jmp	.LBB8_26
.LBB8_12:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	testq	%rdi, %rdi
	jne	.LBB8_32
	jmp	.LBB8_35
.LBB8_13:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	%r9, %rdx
	andq	$-8, %rdx
	je	.LBB8_17
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_37 Depth=1
	leaq	(%rdi,%r9,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB8_18
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_37 Depth=1
	leaq	(%rax,%r9,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB8_18
.LBB8_17:                               #   in Loop: Header=BB8_37 Depth=1
	xorl	%edx, %edx
.LBB8_26:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	subl	%edx, %r11d
	leaq	-1(%r9), %r8
	subq	%rdx, %r8
	andq	$7, %r11
	je	.LBB8_29
# BB#27:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	negq	%r11
	.p2align	4, 0x90
.LBB8_28:                               # %scalar.ph.prol
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	incq	%r11
	jne	.LBB8_28
.LBB8_29:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	$7, %r8
	jb	.LBB8_32
# BB#30:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB8_37 Depth=1
	subq	%rdx, %r9
	leaq	28(%rdi,%rdx,4), %rbx
	leaq	28(%rax,%rdx,4), %rsi
	.p2align	4, 0x90
.LBB8_31:                               # %scalar.ph
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rbx), %edx
	movl	%edx, -28(%rsi)
	movl	-24(%rbx), %edx
	movl	%edx, -24(%rsi)
	movl	-20(%rbx), %edx
	movl	%edx, -20(%rsi)
	movl	-16(%rbx), %edx
	movl	%edx, -16(%rsi)
	movl	-12(%rbx), %edx
	movl	%edx, -12(%rsi)
	movl	-8(%rbx), %edx
	movl	%edx, -8(%rsi)
	movl	-4(%rbx), %edx
	movl	%edx, -4(%rsi)
	movl	(%rbx), %edx
	movl	%edx, (%rsi)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %r9
	jne	.LBB8_31
.LBB8_32:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpb	$0, 24(%rcx)
	je	.LBB8_34
# BB#33:                                #   in Loop: Header=BB8_37 Depth=1
	movq	%rax, %rbx
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbx, %rax
	xorps	%xmm15, %xmm15
	movaps	.LCPI8_2(%rip), %xmm14  # xmm14 = [nan,nan,nan,nan]
	movss	.LCPI8_1(%rip), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movaps	.LCPI8_0(%rip), %xmm12  # xmm12 = <0.5,0.5,u,u>
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB8_34:                               #   in Loop: Header=BB8_37 Depth=1
	movq	$0, 16(%rcx)
	movl	4(%rcx), %r11d
.LBB8_35:                               # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movb	$1, 24(%rcx)
	movq	%rax, 16(%rcx)
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%edx, 8(%rcx)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %ebx          # 4-byte Reload
	movl	$1, %r8d
	movl	24(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
.LBB8_36:                               # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	16(%rcx), %r9
	movslq	%r11d, %rax
	movl	%edx, (%r9,%rax,4)
	incl	4(%rcx)
	jmp	.LBB8_46
.LBB8_18:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	leaq	-8(%rdx), %r8
	movl	%r8d, %r10d
	shrl	$3, %r10d
	incl	%r10d
	andq	$3, %r10
	je	.LBB8_21
# BB#19:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	negq	%r10
	xorl	%esi, %esi
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB8_20:                               # %vector.body.prol
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, 16(%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%r10
	jne	.LBB8_20
	jmp	.LBB8_22
.LBB8_21:                               #   in Loop: Header=BB8_37 Depth=1
	xorl	%esi, %esi
.LBB8_22:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	$24, %r8
	jb	.LBB8_25
# BB#23:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	%rdx, %r8
	subq	%rsi, %r8
	leaq	112(%rdi,%rsi,4), %r10
	leaq	112(%rax,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB8_24:                               # %vector.body
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%r10), %xmm0
	movups	-96(%r10), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%r10), %xmm0
	movups	-64(%r10), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%r10), %xmm0
	movups	-32(%r10), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%r10), %xmm0
	movups	(%r10), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %r10
	subq	$-128, %rsi
	addq	$-32, %r8
	jne	.LBB8_24
.LBB8_25:                               # %middle.block
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	%rdx, %r9
	je	.LBB8_32
	jmp	.LBB8_26
	.p2align	4, 0x90
.LBB8_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_20 Depth 2
                                        #     Child Loop BB8_24 Depth 2
                                        #     Child Loop BB8_28 Depth 2
                                        #     Child Loop BB8_31 Depth 2
	movq	24(%rdi), %rax
	movslq	%r13d, %rdx
	leaq	(%rdx,%rdx,8), %r14
	movsd	(%rax,%r14,4), %xmm5    # xmm5 = mem[0],zero
	movsd	16(%rax,%r14,4), %xmm0  # xmm0 = mem[0],zero
	movss	24(%rax,%r14,4), %xmm11 # xmm11 = mem[0],zero,zero,zero
	addps	%xmm0, %xmm5
	movss	8(%rax,%r14,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm4
	mulps	%xmm12, %xmm5
	subps	%xmm5, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movaps	%xmm1, %xmm2
	andps	%xmm14, %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.LBB8_39
# BB#38:                                #   in Loop: Header=BB8_37 Depth=1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	ucomiss	%xmm15, %xmm2
	jae	.LBB8_45
.LBB8_39:                               #   in Loop: Header=BB8_37 Depth=1
	movss	4(%r15), %xmm10         # xmm10 = mem[0],zero,zero,zero
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	subss	%xmm5, %xmm10
	movaps	%xmm10, %xmm5
	andps	%xmm14, %xmm5
	movaps	%xmm0, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	ucomiss	%xmm6, %xmm5
	jbe	.LBB8_41
# BB#40:                                #   in Loop: Header=BB8_37 Depth=1
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	ucomiss	%xmm15, %xmm5
	jae	.LBB8_45
.LBB8_41:                               #   in Loop: Header=BB8_37 Depth=1
	mulss	%xmm13, %xmm4
	subss	%xmm4, %xmm11
	movss	8(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movaps	%xmm2, %xmm4
	andps	%xmm14, %xmm4
	ucomiss	%xmm11, %xmm4
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	jbe	.LBB8_43
# BB#42:                                #   in Loop: Header=BB8_37 Depth=1
	movaps	%xmm2, %xmm4
	mulss	%xmm5, %xmm4
	ucomiss	%xmm15, %xmm4
	jae	.LBB8_45
.LBB8_43:                               # %._crit_edge.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movss	4(%rsi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm8, %xmm7
	movaps	%xmm10, %xmm4
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm7
	andps	%xmm14, %xmm7
	movaps	%xmm5, %xmm4
	andps	%xmm14, %xmm4
	mulss	%xmm4, %xmm6
	movaps	%xmm8, %xmm9
	andps	%xmm14, %xmm9
	movaps	%xmm11, %xmm3
	mulss	%xmm9, %xmm3
	addss	%xmm6, %xmm3
	ucomiss	%xmm3, %xmm7
	ja	.LBB8_45
# BB#44:                                #   in Loop: Header=BB8_37 Depth=1
	mulss	%xmm1, %xmm5
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	subss	%xmm2, %xmm5
	andps	%xmm14, %xmm5
	mulss	%xmm0, %xmm4
	movaps	%xmm6, %xmm7
	andps	%xmm14, %xmm7
	mulss	%xmm7, %xmm11
	addss	%xmm4, %xmm11
	ucomiss	%xmm11, %xmm5
	jbe	.LBB8_2
	.p2align	4, 0x90
.LBB8_45:                               # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit.thread
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpl	$0, 32(%rax,%r14,4)
	setns	%bpl
	xorl	%r12d, %r12d
.LBB8_46:                               #   in Loop: Header=BB8_37 Depth=1
	movl	$1, %eax
	testb	%r12b, %r12b
	jne	.LBB8_49
# BB#47:                                #   in Loop: Header=BB8_37 Depth=1
	testb	%bpl, %bpl
	jne	.LBB8_49
# BB#48:                                #   in Loop: Header=BB8_37 Depth=1
	movq	24(%rdi), %rdx
	xorl	%eax, %eax
	subl	32(%rdx,%r14,4), %eax
.LBB8_49:                               #   in Loop: Header=BB8_37 Depth=1
	addl	%eax, %r13d
	cmpl	%ebx, %r13d
	jl	.LBB8_37
	jmp	.LBB8_51
.LBB8_50:                               # %.._crit_edge_crit_edge
	addq	$4, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.LBB8_51:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, (%rax)
	setg	%al
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE, .Lfunc_end8-_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.cfi_endproc

	.globl	_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.p2align	4, 0x90
	.type	_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet,@function
_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet: # @_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 160
.Lcfi89:
	.cfi_offset %rbx, -40
.Lcfi90:
	.cfi_offset %r12, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	cmpl	$0, (%rbx)
	je	.LBB9_3
# BB#1:
	cmpl	$0, (%r15)
	je	.LBB9_3
# BB#2:
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	movl	$1, (%rsp)
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
.LBB9_3:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet, .Lfunc_end9-_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI10_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_2:
	.long	897988541               # float 9.99999997E-7
	.section	.text._ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,"axG",@progbits,_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,comdat
	.weak	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,@function
_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_: # @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 16
	movsd	(%rsi), %xmm11          # xmm11 = mem[0],zero
	movsd	16(%rsi), %xmm9         # xmm9 = mem[0],zero
	movsd	32(%rsi), %xmm12        # xmm12 = mem[0],zero
	movss	8(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, -104(%rsp)       # 4-byte Spill
	movss	24(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	40(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, -100(%rsp)       # 4-byte Spill
	movd	48(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movdqa	.LCPI10_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm5
	movd	52(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	56(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm0
	pshufd	$224, %xmm5, %xmm4      # xmm4 = xmm5[0,0,2,3]
	mulps	%xmm11, %xmm4
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm9, %xmm2
	addps	%xmm4, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, -16(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm5
	mulss	%xmm10, %xmm1
	subss	%xmm1, %xmm5
	mulss	%xmm6, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm5, -32(%rsp)        # 16-byte Spill
	movss	(%rdx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, -112(%rsp)       # 4-byte Spill
	movaps	%xmm11, %xmm3
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	%xmm8, -108(%rsp)       # 4-byte Spill
	movss	16(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, -96(%rsp)        # 16-byte Spill
	movaps	%xmm12, %xmm2
	movaps	%xmm2, %xmm15
	mulss	%xmm0, %xmm15
	addss	%xmm1, %xmm15
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm5
	mulss	%xmm6, %xmm0
	movss	20(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm0, %xmm3
	movss	36(%rdx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm13
	movaps	%xmm2, %xmm1
	movaps	%xmm1, -48(%rsp)        # 16-byte Spill
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -116(%rsp)       # 4-byte Spill
	movaps	%xmm5, %xmm2
	movaps	%xmm2, -80(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	movss	24(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	movaps	%xmm0, -64(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm4
	addss	%xmm3, %xmm4
	movss	40(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	mulss	%xmm12, %xmm11
	addss	%xmm4, %xmm11
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	movdqa	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movdqa	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	pshufd	$229, %xmm1, %xmm8      # xmm8 = xmm1[1,1,2,3]
	movdqa	%xmm8, %xmm9
	mulss	-96(%rsp), %xmm9        # 16-byte Folded Reload
	addss	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	mulss	-112(%rsp), %xmm0       # 4-byte Folded Reload
	movdqa	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm8, %xmm3
	mulss	%xmm14, %xmm3
	addss	%xmm1, %xmm3
	mulss	-116(%rsp), %xmm4       # 4-byte Folded Reload
	mulss	%xmm5, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm12, %xmm8
	addss	%xmm2, %xmm8
	movss	-104(%rsp), %xmm1       # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	-108(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm10, %xmm7
	addss	%xmm0, %xmm7
	movaps	-96(%rsp), %xmm0        # 16-byte Reload
	movss	-100(%rsp), %xmm4       # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	addss	%xmm7, %xmm0
	movaps	%xmm0, %xmm2
	movss	-112(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm10, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm4, %xmm14
	movaps	%xmm4, %xmm7
	addss	%xmm6, %xmm14
	movss	-116(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm4
	mulss	%xmm10, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm7, %xmm12
	addss	%xmm5, %xmm12
	movss	48(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	-80(%rsp), %xmm0        # 16-byte Folded Reload
	movss	52(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm10
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	-64(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	56(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	-48(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	-16(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm4, %xmm10
	addss	%xmm7, %xmm10
	addss	-32(%rsp), %xmm10       # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm10, %xmm1           # xmm1 = xmm10[0],xmm1[1,2,3]
	movlps	%xmm0, (%rdi)
	movlps	%xmm1, 8(%rdi)
	movss	%xmm15, 16(%rdi)
	movss	%xmm13, 20(%rdi)
	movss	%xmm11, 24(%rdi)
	movl	$0, 28(%rdi)
	movss	%xmm9, 32(%rdi)
	movss	%xmm3, 36(%rdi)
	movss	%xmm8, 40(%rdi)
	movl	$0, 44(%rdi)
	movss	%xmm2, 48(%rdi)
	movss	%xmm14, 52(%rdi)
	movss	%xmm12, 56(%rdi)
	movl	$0, 60(%rdi)
	movaps	.LCPI10_1(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm0, %xmm15
	movss	.LCPI10_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm15
	movss	%xmm15, 64(%rdi)
	andps	%xmm0, %xmm13
	addss	%xmm1, %xmm13
	movss	%xmm13, 68(%rdi)
	andps	%xmm0, %xmm11
	addss	%xmm1, %xmm11
	movss	%xmm11, 72(%rdi)
	andps	%xmm0, %xmm9
	addss	%xmm1, %xmm9
	movss	%xmm9, 80(%rdi)
	andps	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, 84(%rdi)
	andps	%xmm0, %xmm8
	addss	%xmm1, %xmm8
	movss	%xmm8, 88(%rdi)
	andps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 96(%rdi)
	andps	%xmm0, %xmm14
	addss	%xmm1, %xmm14
	movss	%xmm14, 100(%rdi)
	andps	%xmm0, %xmm12
	addss	%xmm1, %xmm12
	movss	%xmm12, 104(%rdi)
	popq	%rax
	retq
.Lfunc_end10:
	.size	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_, .Lfunc_end10-_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib,@function
_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib: # @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 176
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %rbx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movb	176(%rsp), %al
	movq	24(%r14), %rcx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movslq	%r8d, %rdx
	leaq	(%rdx,%rdx,8), %r15
	movups	16(%rcx,%r15,4), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	(%rcx,%r15,4), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	24(%rbp), %rcx
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movslq	%r9d, %r12
	leaq	(%r12,%r12,8), %r13
	movups	16(%rcx,%r13,4), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rcx,%r13,4), %xmm0
	movaps	%xmm0, 48(%rsp)
	movzbl	%al, %ecx
	leaq	80(%rsp), %rdi
	leaq	48(%rsp), %rsi
	movq	%rbx, %rdx
	callq	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	testb	%al, %al
	je	.LBB11_25
# BB#1:
	movq	%r12, %r8
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	24(%r14), %rax
	movl	32(%rax,%r15,4), %esi
	movq	24(%rbp), %rax
	movl	32(%rax,%r13,4), %edx
	testl	%esi, %esi
	movq	%rbp, %r15
	movq	%r14, %rax
	js	.LBB11_21
# BB#2:
	testl	%edx, %edx
	movq	%rbx, %r13
	movq	%rdi, %rbp
	js	.LBB11_20
# BB#3:
	movq	%rcx, %r15
	movl	4(%r15), %eax
	cmpl	8(%r15), %eax
	jne	.LBB11_19
# BB#4:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r14d
	cmovnel	%ecx, %r14d
	cmpl	%r14d, %eax
	jge	.LBB11_19
# BB#5:
	movl	%esi, %r13d
	movl	%edx, %r12d
	testl	%r14d, %r14d
	je	.LBB11_6
# BB#7:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%r15), %eax
	jmp	.LBB11_8
.LBB11_21:
	movq	%r12, %rbp
	leal	1(%rbp), %r13d
	testl	%edx, %edx
	movq	%rdi, %r12
	js	.LBB11_23
# BB#22:
	movl	$0, (%rsp)
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	movl	%r12d, %r9d
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r14), %rax
	movslq	%r13d, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	movl	32(%rax,%rcx,4), %eax
	addl	$2, %ebp
	subl	%eax, %r13d
	testl	%eax, %eax
	cmovnsl	%ebp, %r13d
	movl	$0, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	movl	%r12d, %r9d
	jmp	.LBB11_24
.LBB11_20:
	leal	1(%rbp), %r14d
	movl	$0, (%rsp)
	movq	%rax, %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%r15, %rsi
	movq	%r15, %rbx
	movq	%rcx, %r15
	movq	%r15, %rdx
	movq	%r13, %rcx
	movl	%r12d, %r8d
	movl	%r14d, %r9d
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%rbx), %rax
	movslq	%r14d, %r9
	leaq	(%r9,%r9,8), %rcx
	movl	32(%rax,%rcx,4), %eax
	addl	$2, %ebp
	subl	%eax, %r9d
	testl	%eax, %eax
	cmovnsl	%ebp, %r9d
	movl	$0, (%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	movl	%r12d, %r8d
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	jmp	.LBB11_24
.LBB11_23:
	movq	%r8, %r14
	leaq	(,%r14,4), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leal	1(%r14), %r9d
	movl	$0, (%rsp)
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %ebp
	movq	%rbx, %r13
	movl	%ebp, %r8d
	movl	%r9d, %ebx
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r15), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	36(%rcx,%rcx,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	32(%rax,%rcx), %eax
	leal	2(%r14), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movl	%ebx, %r9d
	subl	%eax, %r9d
	testl	%eax, %eax
	cmovnsl	%ecx, %r9d
	movl	$0, (%rsp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r12), %rax
	movslq	%ebp, %rbp
	leaq	(,%rbp,4), %rcx
	leaq	(%rcx,%rcx,8), %r14
	movl	32(%rax,%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$2, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %r8d
	subl	%eax, %r8d
	testl	%eax, %eax
	cmovnsl	%ecx, %r8d
	movl	$0, (%rsp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r9d
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r12), %rax
	movl	32(%rax,%r14), %eax
	subl	%eax, %ebp
	testl	%eax, %eax
	cmovnsl	16(%rsp), %ebp          # 4-byte Folded Reload
	movq	24(%r15), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	32(%rax,%rcx), %eax
	subl	%eax, %ebx
	testl	%eax, %eax
	cmovnsl	36(%rsp), %ebx          # 4-byte Folded Reload
	movl	$0, (%rsp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movl	%ebp, %r8d
	movl	%ebx, %r9d
.LBB11_24:
	callq	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	jmp	.LBB11_25
.LBB11_6:
	xorl	%ebx, %ebx
.LBB11_8:                               # %_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi.exit.i.i.i
	movq	16(%r15), %rdi
	testl	%eax, %eax
	jle	.LBB11_14
# BB#9:                                 # %.lr.ph.i.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdx
	xorl	%esi, %esi
	andq	$3, %rdx
	je	.LBB11_11
	.p2align	4, 0x90
.LBB11_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rsi,8), %ebp
	movl	%ebp, (%rbx,%rsi,8)
	movl	4(%rdi,%rsi,8), %ebp
	movl	%ebp, 4(%rbx,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB11_10
.LBB11_11:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_15
# BB#12:                                # %.lr.ph.i.i.i.i.new
	subq	%rsi, %rcx
	leaq	28(%rbx,%rsi,8), %rdx
	leaq	28(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB11_13:                              # =>This Inner Loop Header: Depth=1
	movl	-28(%rsi), %ebp
	movl	%ebp, -28(%rdx)
	movl	-24(%rsi), %ebp
	movl	%ebp, -24(%rdx)
	movl	-20(%rsi), %ebp
	movl	%ebp, -20(%rdx)
	movl	-16(%rsi), %ebp
	movl	%ebp, -16(%rdx)
	movl	-12(%rsi), %ebp
	movl	%ebp, -12(%rdx)
	movl	-8(%rsi), %ebp
	movl	%ebp, -8(%rdx)
	movl	-4(%rsi), %ebp
	movl	%ebp, -4(%rdx)
	movl	(%rsi), %ebp
	movl	%ebp, (%rdx)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB11_13
	jmp	.LBB11_15
.LBB11_14:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.i.i.i
	testq	%rdi, %rdi
	je	.LBB11_18
.LBB11_15:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.thread.i.i.i
	cmpb	$0, 24(%r15)
	je	.LBB11_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%r15), %eax
.LBB11_17:
	movq	$0, 16(%r15)
.LBB11_18:                              # %_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv.exit.i.i.i
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%r14d, 8(%r15)
	movl	%r12d, %edx
	movl	%r13d, %esi
.LBB11_19:                              # %_ZN9btPairSet9push_pairEii.exit
	movq	16(%r15), %rcx
	cltq
	movl	%esi, (%rcx,%rax,8)
	movl	%edx, 4(%rcx,%rax,8)
	incl	%eax
	movl	%eax, 4(%r15)
.LBB11_25:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib, .Lfunc_end11-_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI12_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,"axG",@progbits,_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,comdat
	.weak	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.p2align	4, 0x90
	.type	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,@function
_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb: # @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 64
	movsd	16(%rdi), %xmm6         # xmm6 = mem[0],zero
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	addps	%xmm6, %xmm3
	movss	24(%rdi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm0
	movaps	.LCPI12_0(%rip), %xmm4  # xmm4 = <0.5,0.5,u,u>
	mulps	%xmm4, %xmm3
	movss	.LCPI12_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	xorps	%xmm7, %xmm7
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, -48(%rsp)
	movlps	%xmm2, -40(%rsp)
	subps	%xmm3, %xmm6
	subss	%xmm0, %xmm10
	xorps	%xmm0, %xmm0
	movss	%xmm10, %xmm0           # xmm0 = xmm10[0],xmm0[1,2,3]
	movlps	%xmm6, -32(%rsp)
	movlps	%xmm0, -24(%rsp)
	movsd	16(%rsi), %xmm9         # xmm9 = mem[0],zero
	movsd	(%rsi), %xmm14          # xmm14 = mem[0],zero
	addps	%xmm9, %xmm14
	movss	24(%rsi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm2
	mulps	%xmm4, %xmm14
	mulss	%xmm1, %xmm2
	subps	%xmm14, %xmm9
	subss	%xmm2, %xmm13
	movaps	%xmm9, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	movss	%xmm13, %xmm7           # xmm7 = xmm13[0],xmm7[1,2,3]
	movlps	%xmm9, (%rsp)
	movlps	%xmm7, 8(%rsp)
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm1
	movss	%xmm0, -68(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm1
	movaps	%xmm14, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm8, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm1, %xmm4
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm4, %xmm7
	addss	(%rdx), %xmm7
	subss	%xmm3, %xmm7
	movss	%xmm7, -64(%rsp)
	movss	64(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	68(%rdx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm15, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm1, %xmm3
	movss	72(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm6, %xmm1
	movaps	.LCPI12_2(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm3
	ucomiss	%xmm1, %xmm3
	ja	.LBB12_11
# BB#1:
	movss	%xmm4, -72(%rsp)        # 4-byte Spill
	movss	%xmm10, -116(%rsp)      # 4-byte Spill
	movss	%xmm5, -76(%rsp)        # 4-byte Spill
	movss	%xmm0, -88(%rsp)        # 4-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	32(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	36(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm3
	mulss	%xmm10, %xmm3
	movaps	%xmm8, %xmm1
	movss	%xmm0, -80(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	movss	40(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	movss	%xmm0, -92(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	addss	4(%rdx), %xmm3
	subss	-44(%rsp), %xmm3
	movss	%xmm3, -60(%rsp)
	movss	80(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	84(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm9, %xmm1
	movss	%xmm4, -84(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm4
	addss	%xmm1, %xmm4
	movss	88(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -96(%rsp)        # 4-byte Spill
	mulss	%xmm13, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	addss	%xmm6, %xmm1
	movaps	.LCPI12_2(%rip), %xmm4  # xmm4 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm4
	ucomiss	%xmm1, %xmm4
	movaps	%xmm11, %xmm6
	ja	.LBB12_11
# BB#2:
	movss	%xmm10, -100(%rsp)      # 4-byte Spill
	movss	%xmm12, -108(%rsp)      # 4-byte Spill
	movss	%xmm15, -104(%rsp)      # 4-byte Spill
	movss	48(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	52(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm14
	mulss	%xmm11, %xmm8
	addss	%xmm14, %xmm8
	movss	56(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm8, %xmm2
	addss	8(%rdx), %xmm2
	subss	-40(%rsp), %xmm2
	movss	%xmm2, -56(%rsp)
	movss	96(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	100(%rdx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm14, %xmm4
	mulss	%xmm6, %xmm4
	addss	%xmm1, %xmm4
	movss	104(%rdx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm4, %xmm1
	addss	-116(%rsp), %xmm1       # 4-byte Folded Reload
	movaps	.LCPI12_2(%rip), %xmm4  # xmm4 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm4
	ucomiss	%xmm1, %xmm4
	ja	.LBB12_11
# BB#3:                                 # %.preheader102117
	movaps	%xmm6, -16(%rsp)        # 16-byte Spill
	movss	%xmm13, -112(%rsp)      # 4-byte Spill
	movss	-68(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	-100(%rsp), %xmm13      # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm13
	addss	%xmm1, %xmm13
	mulss	%xmm2, %xmm5
	addss	%xmm13, %xmm5
	movss	-72(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	mulss	%xmm15, %xmm1
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movss	-116(%rsp), %xmm6       # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm8
	addss	%xmm0, %xmm8
	addss	%xmm9, %xmm8
	andps	.LCPI12_2(%rip), %xmm5
	ucomiss	%xmm8, %xmm5
	ja	.LBB12_11
# BB#4:
	movss	-76(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	-80(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm11
	addss	%xmm1, %xmm11
	movaps	%xmm15, %xmm1
	movss	-104(%rsp), %xmm13      # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	movss	-84(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	addss	-16(%rsp), %xmm14       # 16-byte Folded Reload
	andps	.LCPI12_2(%rip), %xmm11
	ucomiss	%xmm14, %xmm11
	jbe	.LBB12_5
.LBB12_11:
	xorl	%eax, %eax
.LBB12_12:                              # %.critedge
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addq	$56, %rsp
	retq
.LBB12_5:
	mulss	-88(%rsp), %xmm7        # 4-byte Folded Reload
	mulss	-92(%rsp), %xmm3        # 4-byte Folded Reload
	addss	%xmm7, %xmm3
	mulss	%xmm10, %xmm2
	addss	%xmm3, %xmm2
	movss	-108(%rsp), %xmm14      # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm15
	mulss	-96(%rsp), %xmm4        # 4-byte Folded Reload
	addss	%xmm15, %xmm4
	mulss	%xmm12, %xmm6
	addss	%xmm4, %xmm6
	movss	-112(%rsp), %xmm11      # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm6
	andps	.LCPI12_2(%rip), %xmm2
	ucomiss	%xmm6, %xmm2
	setbe	%al
	ja	.LBB12_12
# BB#6:
	xorb	$1, %cl
	jne	.LBB12_12
# BB#7:                                 # %.preheader.preheader
	movss	4(%rsp), %xmm10         # xmm10 = mem[0],zero,zero,zero
	leaq	88(%rdx), %r8
	movl	$1, %r10d
	movaps	.LCPI12_2(%rip), %xmm8  # xmm8 = [nan,nan,nan,nan]
	movaps	-16(%rsp), %xmm0        # 16-byte Reload
	jmp	.LBB12_9
.LBB12_8:                               # %.loopexit112..preheader_crit_edge
                                        #   in Loop: Header=BB12_9 Depth=1
	movss	-4(%r8), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	(%r8), %xmm14           # xmm14 = mem[0],zero,zero,zero
	addq	$16, %r8
	movq	%r9, %r10
	movaps	%xmm10, %xmm0
.LBB12_9:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r10), %r9
	movslq	%r9d, %rsi
	imulq	$1431655766, %rsi, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %esi
	xorl	%ecx, %ecx
	cmpq	$1, %r10
	sete	%cl
	xorl	%eax, %eax
	xorl	%edi, %edi
	cmpq	$3, %r10
	movslq	%esi, %r11
	movss	-64(%rsp,%r11,4), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movl	$0, %esi
	cmovneq	%r10, %rsi
	movss	-64(%rsp,%rsi,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	-32(%rsp,%rcx,4), %xmm6 # xmm6 = mem[0],zero,zero,zero
	setne	%dil
	movss	-28(%rsp,%rdi,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	incq	%rdi
	shlq	$4, %rsi
	movss	16(%rdx,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	shlq	$4, %r11
	movss	16(%rdx,%r11), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm1
	shlq	$4, %rdi
	movss	64(%rdx,%rdi), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	shlq	$4, %rcx
	movss	64(%rdx,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm11, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm0, %xmm2
	andps	%xmm8, %xmm1
	ucomiss	%xmm2, %xmm1
	ja	.LBB12_12
# BB#10:                                #   in Loop: Header=BB12_9 Depth=1
	movss	20(%rdx,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	20(%rdx,%r11), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	movss	68(%rdx,%rdi), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	68(%rdx,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm9, %xmm14
	addss	%xmm4, %xmm14
	movss	-24(%r8), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm14, %xmm2
	andps	%xmm8, %xmm1
	ucomiss	%xmm2, %xmm1
	ja	.LBB12_11
# BB#13:                                #   in Loop: Header=BB12_9 Depth=1
	mulss	24(%rdx,%rsi), %xmm5
	mulss	24(%rdx,%r11), %xmm3
	subss	%xmm3, %xmm5
	mulss	72(%rdx,%rdi), %xmm6
	mulss	72(%rdx,%rcx), %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm9, %xmm13
	addss	%xmm7, %xmm13
	mulss	%xmm10, %xmm4
	addss	%xmm13, %xmm4
	andps	%xmm8, %xmm5
	ucomiss	%xmm4, %xmm5
	ja	.LBB12_11
# BB#14:                                # %.loopexit112
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	$2, %r10
	jle	.LBB12_8
# BB#15:
	movb	$1, %al
	jmp	.LBB12_12
.Lfunc_end12:
	.size	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb, .Lfunc_end12-_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
