	.text
	.file	"unarithmetic.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI0_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_2:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
.LCPI0_3:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI0_4:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI0_5:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
.LCPI0_6:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
.LCPI0_7:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_8:
	.quad	257                     # 0x101
	.quad	257                     # 0x101
.LCPI0_9:
	.quad	253                     # 0xfd
	.quad	253                     # 0xfd
.LCPI0_10:
	.quad	249                     # 0xf9
	.quad	249                     # 0xf9
.LCPI0_11:
	.quad	245                     # 0xf5
	.quad	245                     # 0xf5
.LCPI0_12:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
	.text
	.globl	do_deari
	.p2align	4, 0x90
	.type	do_deari,@function
do_deari:                               # @do_deari
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edi, in_size(%rip)
	movl	$0, in_pos(%rip)
	movl	$0, deari_pos(%rip)
	movl	$1, %eax
	movd	%rax, %xmm5
	pslldq	$8, %xmm5               # xmm5 = zero,zero,zero,zero,zero,zero,zero,zero,xmm5[0,1,2,3,4,5,6,7]
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [2,3]
	xorl	%eax, %eax
	movdqa	.LCPI0_1(%rip), %xmm8   # xmm8 = [0,1,2,3]
	movdqa	.LCPI0_2(%rip), %xmm9   # xmm9 = [4,5,6,7]
	movdqa	.LCPI0_3(%rip), %xmm11  # xmm11 = [1,1]
	movdqa	.LCPI0_4(%rip), %xmm10  # xmm10 = [5,5,5,5]
	movdqa	.LCPI0_5(%rip), %xmm6   # xmm6 = [255,255,255,255]
	movdqa	.LCPI0_6(%rip), %xmm7   # xmm7 = [8,8]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm3        # xmm3 = xmm2[0,0,0,0]
	movdqa	%xmm3, %xmm2
	paddd	%xmm8, %xmm2
	paddd	%xmm9, %xmm3
	movdqa	%xmm5, %xmm0
	paddq	%xmm11, %xmm0
	movdqa	%xmm4, %xmm1
	paddq	%xmm11, %xmm1
	movd	%xmm0, %rcx
	shufps	$136, %xmm1, %xmm0      # xmm0 = xmm0[0,2],xmm1[0,2]
	movd	%ecx, %xmm1
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movdqa	%xmm5, %xmm1
	shufps	$136, %xmm4, %xmm1      # xmm1 = xmm1[0,2],xmm4[0,2]
	paddd	%xmm10, %xmm1
	movaps	%xmm0, char_to_index(,%rax,4)
	movdqa	%xmm1, char_to_index+16(,%rax,4)
	pand	%xmm6, %xmm2
	packuswb	%xmm2, %xmm2
	packuswb	%xmm2, %xmm2
	movd	%xmm2, index_to_char(%rcx)
	pand	%xmm6, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	movd	%xmm3, index_to_char+4(%rcx)
	addq	$8, %rax
	paddq	%xmm7, %xmm5
	paddq	%xmm7, %xmm4
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_1
# BB#2:                                 # %vector.body37.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movq	$-1024, %rax            # imm = 0xFC00
	movaps	.LCPI0_7(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movdqa	.LCPI0_8(%rip), %xmm8   # xmm8 = [257,257]
	movdqa	.LCPI0_9(%rip), %xmm9   # xmm9 = [253,253]
	movdqa	.LCPI0_10(%rip), %xmm10 # xmm10 = [249,249]
	movdqa	.LCPI0_11(%rip), %xmm11 # xmm11 = [245,245]
	movdqa	.LCPI0_12(%rip), %xmm7  # xmm7 = [16,16]
	.p2align	4, 0x90
.LBB0_3:                                # %vector.body37
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm2, freq+1024(%rax)
	movaps	%xmm2, freq+1040(%rax)
	movdqa	%xmm8, %xmm3
	psubq	%xmm0, %xmm3
	movdqa	%xmm8, %xmm4
	psubq	%xmm1, %xmm4
	movdqa	%xmm9, %xmm5
	psubq	%xmm0, %xmm5
	movdqa	%xmm9, %xmm6
	psubq	%xmm1, %xmm6
	shufps	$136, %xmm4, %xmm3      # xmm3 = xmm3[0,2],xmm4[0,2]
	shufps	$136, %xmm6, %xmm5      # xmm5 = xmm5[0,2],xmm6[0,2]
	movaps	%xmm3, cum_freq+1024(%rax)
	movaps	%xmm5, cum_freq+1040(%rax)
	movaps	%xmm2, freq+1056(%rax)
	movaps	%xmm2, freq+1072(%rax)
	movdqa	%xmm10, %xmm3
	psubq	%xmm0, %xmm3
	movdqa	%xmm10, %xmm4
	psubq	%xmm1, %xmm4
	movdqa	%xmm11, %xmm5
	psubq	%xmm0, %xmm5
	movdqa	%xmm11, %xmm6
	psubq	%xmm1, %xmm6
	shufps	$136, %xmm4, %xmm3      # xmm3 = xmm3[0,2],xmm4[0,2]
	shufps	$136, %xmm6, %xmm5      # xmm5 = xmm5[0,2],xmm6[0,2]
	movaps	%xmm3, cum_freq+1056(%rax)
	movaps	%xmm5, cum_freq+1072(%rax)
	paddq	%xmm7, %xmm0
	paddq	%xmm7, %xmm1
	addq	$64, %rax
	jne	.LBB0_3
# BB#4:                                 # %.preheader.i
	movl	$1, freq+1024(%rip)
	movl	$1, cum_freq+1024(%rip)
	movl	$1, freq+1028(%rip)
	movl	$0, cum_freq+1028(%rip)
	movl	$0, freq(%rip)
	movl	$0, bits_to_go(%rip)
	movl	$0, garbage_bits(%rip)
	movq	$0, value(%rip)
	movl	$1, %eax
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_10
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpl	%edi, %esi
	jae	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	movq	in(%rip), %rbp
	movl	%esi, %ebx
	leal	1(%rsi), %esi
	movl	%esi, in_pos(%rip)
	movzbl	(%rbp,%rbx), %ebp
	movl	%ebp, buffer(%rip)
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jmp	.LBB0_9
.LBB0_8:                                #   in Loop: Header=BB0_5 Depth=1
	cmpl	$14, %r8d
	leal	1(%r8), %edx
	movl	%edx, garbage_bits(%rip)
	movl	%edx, %r8d
	jge	.LBB0_31
.LBB0_9:                                #   in Loop: Header=BB0_5 Depth=1
	movl	$8, bits_to_go(%rip)
	movl	$8, %ebx
.LBB0_10:                               # %input_bit.exit.i17
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	%rcx, %rbp
	addq	%rbp, %rbp
	movl	buffer(%rip), %ecx
	movl	%ecx, %edx
	sarl	%edx
	movl	%edx, buffer(%rip)
	decl	%ebx
	movl	%ebx, bits_to_go(%rip)
	andl	$1, %ecx
	orq	%rbp, %rcx
	movq	%rcx, value(%rip)
	incl	%eax
	cmpl	$17, %eax
	jl	.LBB0_5
# BB#11:                                # %start_decoding.exit
	movq	$0, low(%rip)
	movq	$65535, high(%rip)      # imm = 0xFFFF
	movl	$65535, %esi            # imm = 0xFFFF
	xorl	%r14d, %r14d
	movabsq	$-4294967296, %r9       # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %r8        # imm = 0x100000000
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_12:                               # %update_model.exit.loopexit
                                        #   in Loop: Header=BB0_13 Depth=1
	movq	low(%rip), %r14
	movq	value(%rip), %rcx
.LBB0_13:                               # %update_model.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_35 Depth 2
                                        #     Child Loop BB0_37 Depth 2
                                        #     Child Loop BB0_41 Depth 2
	subq	%r14, %rsi
	incq	%rsi
	movl	$1, %eax
	subq	%r14, %rax
	addq	%rcx, %rax
	movslq	cum_freq(%rip), %r15
	imulq	%r15, %rax
	decq	%rax
	cqto
	idivq	%rsi
	movl	$1, %edx
	movl	$2, %ebp
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ebx
	movl	%ebp, %edi
	movslq	cum_freq+4(,%r11,4), %rcx
	addq	%r9, %r10
	incq	%r11
	leal	1(%rbx), %edx
	leal	1(%rdi), %ebp
	cmpl	%eax, %ecx
	jg	.LBB0_14
# BB#15:                                #   in Loop: Header=BB0_13 Depth=1
	movq	%r9, %rax
	subq	%r10, %rax
	negq	%r10
	sarq	$30, %rax
	movslq	cum_freq(%rax), %rax
	imulq	%rsi, %rax
	cqto
	idivq	%r15
	imulq	%rsi, %rcx
	leaq	-1(%r14,%rax), %rsi
	movq	%rcx, %rax
	cqto
	idivq	%r15
	addq	%r14, %rax
	movl	$low, %ecx
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_28:                               # %input_bit.exit.i
                                        #   in Loop: Header=BB0_16 Depth=2
	addq	%rcx, %rcx
	movl	buffer(%rip), %eax
	movl	%eax, %ebp
	sarl	%ebp
	movl	%ebp, buffer(%rip)
	decl	%edx
	movl	%edx, bits_to_go(%rip)
	andl	$1, %eax
	orq	%rcx, %rax
	movl	$value, %ecx
.LBB0_16:                               #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, (%rcx)
	movq	low(%rip), %rax
	cmpq	$32767, %rsi            # imm = 0x7FFF
	jg	.LBB0_18
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	value(%rip), %rcx
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	$32767, %rax            # imm = 0x7FFF
	jle	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_16 Depth=2
	movl	$32768, %edx            # imm = 0x8000
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	$49151, %rsi            # imm = 0xBFFF
	jg	.LBB0_32
# BB#21:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	$16384, %rax            # imm = 0x4000
	jl	.LBB0_32
# BB#22:                                #   in Loop: Header=BB0_16 Depth=2
	movl	$16384, %edx            # imm = 0x4000
.LBB0_23:                               # %.sink.split.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	value(%rip), %rcx
	subq	%rdx, %rcx
	movq	%rcx, value(%rip)
	subq	%rdx, %rax
	movq	%rax, low(%rip)
	subq	%rdx, %rsi
.LBB0_24:                               #   in Loop: Header=BB0_16 Depth=2
	addq	%rax, %rax
	movq	%rax, low(%rip)
	leaq	1(%rsi,%rsi), %rsi
	movl	bits_to_go(%rip), %edx
	testl	%edx, %edx
	jne	.LBB0_28
# BB#25:                                #   in Loop: Header=BB0_16 Depth=2
	movl	in_pos(%rip), %eax
	cmpl	in_size(%rip), %eax
	jae	.LBB0_29
# BB#26:                                #   in Loop: Header=BB0_16 Depth=2
	movq	in(%rip), %rdx
	leal	1(%rax), %ebp
	movl	%ebp, in_pos(%rip)
	movzbl	(%rdx,%rax), %eax
	movl	%eax, buffer(%rip)
	jmp	.LBB0_27
.LBB0_29:                               #   in Loop: Header=BB0_16 Depth=2
	movl	garbage_bits(%rip), %eax
	leal	1(%rax), %edx
	movl	%edx, garbage_bits(%rip)
	cmpl	$14, %eax
	jge	.LBB0_30
.LBB0_27:                               #   in Loop: Header=BB0_16 Depth=2
	movl	$8, bits_to_go(%rip)
	movl	$8, %edx
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_32:                               # %decode_symbol.exit
                                        #   in Loop: Header=BB0_13 Depth=1
	cmpl	$257, %r11d             # imm = 0x101
	je	.LBB0_42
# BB#33:                                # %.critedge
                                        #   in Loop: Header=BB0_13 Depth=1
	sarq	$32, %r10
	movb	index_to_char(%r10), %al
	movq	deari(%rip), %rcx
	movl	deari_pos(%rip), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, deari_pos(%rip)
	movb	%al, (%rcx,%rdx)
	cmpl	$16383, cum_freq(%rip)  # imm = 0x3FFF
	jne	.LBB0_36
# BB#34:                                # %.preheader39.i.preheader
                                        #   in Loop: Header=BB0_13 Depth=1
	xorl	%ecx, %ecx
	movl	$259, %eax              # imm = 0x103
	.p2align	4, 0x90
.LBB0_35:                               # %.preheader39.i
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	freq-8(,%rax,4), %edx
	leal	1(%rdx), %ebp
	shrl	$31, %ebp
	leal	1(%rdx,%rbp), %edx
	sarl	%edx
	movl	%edx, freq-8(,%rax,4)
	movl	%ecx, cum_freq-8(,%rax,4)
	addl	%ecx, %edx
	movl	freq-12(,%rax,4), %ecx
	leal	1(%rcx), %ebp
	shrl	$31, %ebp
	leal	1(%rcx,%rbp), %ecx
	sarl	%ecx
	movl	%ecx, freq-12(,%rax,4)
	movl	%edx, cum_freq-12(,%rax,4)
	addl	%edx, %ecx
	addq	$-2, %rax
	cmpq	$1, %rax
	jg	.LBB0_35
.LBB0_36:                               # %.preheader38.preheader.i
                                        #   in Loop: Header=BB0_13 Depth=1
	movl	freq(,%r10,4), %edx
	shlq	$32, %rbx
	addq	%r8, %rbx
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB0_37:                               # %.preheader38.i
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	leaq	-1(%rax), %rcx
	addq	%r9, %rbx
	decl	%edi
	cmpl	freq-4(,%rax,4), %edx
	je	.LBB0_37
# BB#38:                                #   in Loop: Header=BB0_13 Depth=1
	cmpl	%r11d, %edi
	jge	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_13 Depth=1
	movzbl	index_to_char+1(%rcx), %edx
	movzbl	index_to_char(%r10), %ebx
	movb	%bl, index_to_char+1(%rcx)
	movb	%dl, index_to_char(%r10)
	movl	%r11d, char_to_index(,%rdx,4)
	movl	%edi, char_to_index(,%rbx,4)
.LBB0_40:                               # %.preheader.preheader.i
                                        #   in Loop: Header=BB0_13 Depth=1
	cltq
	incq	%rax
	movl	$freq, %ecx
	.p2align	4, 0x90
.LBB0_41:                               # %.preheader.i11
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	-4(%rcx,%rax,4)
	decq	%rax
	movl	$cum_freq, %ecx
	jg	.LBB0_41
	jmp	.LBB0_12
.LBB0_42:
	movq	%rsi, high(%rip)
	movl	deari_pos(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_30:
	movq	%rsi, high(%rip)
.LBB0_31:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	do_deari, .Lfunc_end0-do_deari
	.cfi_endproc

	.type	in_size,@object         # @in_size
	.comm	in_size,4,4
	.type	in_pos,@object          # @in_pos
	.comm	in_pos,4,4
	.type	deari_pos,@object       # @deari_pos
	.comm	deari_pos,4,4
	.type	cum_freq,@object        # @cum_freq
	.comm	cum_freq,1032,16
	.type	index_to_char,@object   # @index_to_char
	.comm	index_to_char,258,16
	.type	char_to_index,@object   # @char_to_index
	.comm	char_to_index,1024,16
	.type	freq,@object            # @freq
	.comm	freq,1032,16
	.type	buffer,@object          # @buffer
	.comm	buffer,4,4
	.type	bits_to_go,@object      # @bits_to_go
	.comm	bits_to_go,4,4
	.type	garbage_bits,@object    # @garbage_bits
	.comm	garbage_bits,4,4
	.type	value,@object           # @value
	.local	value
	.comm	value,8,8
	.type	low,@object             # @low
	.local	low
	.comm	low,8,8
	.type	high,@object            # @high
	.local	high
	.comm	high,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Bad input file\n"
	.size	.L.str, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
