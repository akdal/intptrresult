	.text
	.file	"gxcache.bc"
	.globl	gx_alloc_char_bits
	.p2align	4, 0x90
	.type	gx_alloc_char_bits,@function
gx_alloc_char_bits:                     # @gx_alloc_char_bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	%r15d, 24(%r14)
	movl	%ebp, 28(%r14)
	movq	%r14, %rdi
	callq	gx_device_memory_bitmap_size
	movq	%rax, %rsi
	movl	152(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB0_3
# BB#1:
	movl	68(%rbx), %eax
	xorl	%edx, %edx
	divl	%ecx
	cmpl	%eax, %ebp
	ja	.LBB0_10
.LBB0_3:
	movl	60(%rbx), %eax
	cmpl	%eax, 56(%rbx)
	jae	.LBB0_6
# BB#4:
	movl	44(%rbx), %eax
	subl	40(%rbx), %eax
	cmpq	%rsi, %rax
	jb	.LBB0_7
# BB#5:                                 # %._crit_edge
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	1104(%rbx), %eax
	movl	1136(%rbx), %r13d
	movq	%rax, %rdi
	jmp	.LBB0_9
.LBB0_6:                                # %thread-pre-split
	testl	%eax, %eax
	je	.LBB0_10
.LBB0_7:                                # %thread-pre-split.thread
	movl	44(%rbx), %eax
	xorl	%r13d, %r13d
	cmpq	%rsi, %rax
	jb	.LBB0_11
# BB#8:
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	$0, 40(%rbx)
	movl	$0, 48(%rbx)
	movl	$0, 56(%rbx)
	movl	$0, 1104(%rbx)
	movl	$0, 1120(%rbx)
	movl	$0, 1136(%rbx)
	leaq	72(%rbx), %rdi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	xorl	%eax, %eax
	xorl	%edi, %edi
.LBB0_9:
	movq	1128(%rbx), %r12
	leal	1(%r13), %ecx
	movl	%ecx, 1136(%rbx)
	movl	%r13d, %ecx
	leaq	(%rcx,%rcx,8), %r15
	leaq	(%r12,%r15,8), %rbp
	addq	1096(%rbx), %rdi
	movq	%rdi, 64(%r12,%r15,8)
	movq	16(%rsp), %r13          # 8-byte Reload
	addl	%r13d, %eax
	movl	%eax, 1104(%rbx)
	movl	%r13d, %edx
	xorl	%esi, %esi
	callq	memset
	movl	12(%rsp), %eax          # 4-byte Reload
	movw	%ax, 28(%r12,%r15,8)
	movl	8(%rsp), %eax           # 4-byte Reload
	movw	%ax, 26(%r12,%r15,8)
	movzwl	152(%r14), %eax
	movw	%ax, 24(%r12,%r15,8)
	movq	64(%r12,%r15,8), %rax
	movq	%rax, 160(%r14)
	movq	8(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	incl	56(%rbx)
	addl	%r13d, 40(%rbx)
	movq	%rbp, %r13
	jmp	.LBB0_11
.LBB0_10:
	xorl	%r13d, %r13d
.LBB0_11:
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gx_alloc_char_bits, .Lfunc_end0-gx_alloc_char_bits
	.cfi_endproc

	.globl	zap_cache
	.p2align	4, 0x90
	.type	zap_cache,@function
zap_cache:                              # @zap_cache
	.cfi_startproc
# BB#0:
	movl	$0, 40(%rdi)
	movl	$0, 48(%rdi)
	movl	$0, 56(%rdi)
	movl	$0, 1104(%rdi)
	movl	$0, 1120(%rdi)
	movl	$0, 1136(%rdi)
	addq	$72, %rdi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	jmp	memset                  # TAILCALL
.Lfunc_end1:
	.size	zap_cache, .Lfunc_end1-zap_cache
	.cfi_endproc

	.globl	gx_unalloc_cached_char
	.p2align	4, 0x90
	.type	gx_unalloc_cached_char,@function
gx_unalloc_cached_char:                 # @gx_unalloc_cached_char
	.cfi_startproc
# BB#0:
	movzwl	24(%rsi), %eax
	movzwl	26(%rsi), %ecx
	imull	%eax, %ecx
	decl	1136(%rdi)
	subl	%ecx, 1104(%rdi)
	decl	56(%rdi)
	subl	%ecx, 40(%rdi)
	retq
.Lfunc_end2:
	.size	gx_unalloc_cached_char, .Lfunc_end2-gx_unalloc_cached_char
	.cfi_endproc

	.globl	gx_lookup_fm_pair
	.p2align	4, 0x90
	.type	gx_lookup_fm_pair,@function
gx_lookup_fm_pair:                      # @gx_lookup_fm_pair
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movss	336(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	368(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	384(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movq	328(%rdi), %rbx
	movq	24(%rbx), %r14
	movl	48(%r14), %r8d
	testl	%r8d, %r8d
	je	.LBB3_1
# BB#2:                                 # %.lr.ph
	movq	1112(%r14), %rdx
	movl	1120(%r14), %eax
	shlq	$5, %rax
	addq	%rdx, %rax
	leaq	52(%r14), %rsi
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	movq	$-1, %rcx
	cmpq	%rdx, %rbp
	jne	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	(%rsi), %ecx
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	shlq	$5, %rcx
	leaq	(%rbp,%rcx), %rax
	cmpq	%rbx, (%rbp,%rcx)
	je	.LBB3_6
.LBB3_10:                               # %.backedge
                                        #   in Loop: Header=BB3_3 Depth=1
	decl	%edi
	jne	.LBB3_3
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movss	8(%rbp,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB3_10
	jp	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	movss	12(%rbp,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jne	.LBB3_10
	jp	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	movss	16(%rbp,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jne	.LBB3_10
	jp	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movss	20(%rbp,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm0
	jne	.LBB3_10
	jp	.LBB3_10
	jmp	.LBB3_15
.LBB3_1:                                # %.._crit_edge_crit_edge
	leaq	52(%r14), %rsi
.LBB3_11:                               # %._crit_edge
	movl	(%rsi), %r15d
	cmpl	%r15d, %r8d
	jne	.LBB3_12
# BB#13:
	movl	$0, 40(%r14)
	movl	$0, 48(%r14)
	movl	$0, 56(%r14)
	movl	$0, 1104(%r14)
	movl	$0, 1120(%r14)
	movl	$0, 1136(%r14)
	leaq	72(%r14), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movss	%xmm1, 20(%rsp)         # 4-byte Spill
	movss	%xmm2, 16(%rsp)         # 4-byte Spill
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	callq	memset
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movl	$1, %r8d
	jmp	.LBB3_14
.LBB3_12:                               # %._crit_edge._crit_edge
	movl	1120(%r14), %ebp
	incl	%r8d
.LBB3_14:
	movl	%r8d, 48(%r14)
	movq	1112(%r14), %rcx
	movl	%ebp, %edx
	shlq	$5, %rdx
	leaq	(%rcx,%rdx), %rax
	incl	%ebp
	xorl	%esi, %esi
	cmpl	%r15d, %ebp
	cmovnel	%ebp, %esi
	movl	%esi, 1120(%r14)
	movq	%rbx, (%rcx,%rdx)
	movss	%xmm1, 8(%rcx,%rdx)
	movss	%xmm2, 12(%rcx,%rdx)
	movss	%xmm3, 16(%rcx,%rdx)
	movss	%xmm4, 20(%rcx,%rdx)
	movl	$0, 24(%rcx,%rdx)
.LBB3_15:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	gx_lookup_fm_pair, .Lfunc_end3-gx_lookup_fm_pair
	.cfi_endproc

	.globl	gx_add_cached_char
	.p2align	4, 0x90
	.type	gx_add_cached_char,@function
gx_add_cached_char:                     # @gx_add_cached_char
	.cfi_startproc
# BB#0:
	movl	8(%rsi), %eax
	andl	$127, %eax
	leaq	72(%rdi,%rax,8), %rax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB4_1
# BB#2:
	movq	%rsi, (%rcx)
	movq	$0, (%rsi)
	movq	%rdx, 16(%rsi)
	incl	24(%rdx)
	retq
.Lfunc_end4:
	.size	gx_add_cached_char, .Lfunc_end4-gx_add_cached_char
	.cfi_endproc

	.globl	gx_lookup_cached_char
	.p2align	4, 0x90
	.type	gx_lookup_cached_char,@function
gx_lookup_cached_char:                  # @gx_lookup_cached_char
	.cfi_startproc
# BB#0:
	movq	328(%rdi), %rax
	movq	24(%rax), %rax
	movl	%edx, %ecx
	andl	$127, %ecx
	movq	72(%rax,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB5_2
	jmp	.LBB5_5
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB5_5
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%edx, 8(%rax)
	jne	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpq	%rsi, 16(%rax)
	jne	.LBB5_4
	jmp	.LBB5_6
.LBB5_5:
	xorl	%eax, %eax
.LBB5_6:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	gx_lookup_cached_char, .Lfunc_end5-gx_lookup_cached_char
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	gx_copy_cached_char
	.p2align	4, 0x90
	.type	gx_copy_cached_char,@function
gx_copy_cached_char:                    # @gx_copy_cached_char
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 160
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rbx
	movq	256(%rbx), %rax
	cmpb	$0, 136(%rax)
	je	.LBB6_1
# BB#2:
	movq	120(%rax), %rbp
	movq	128(%rax), %r13
	cmpl	$0, 356(%r15)
	je	.LBB6_3
.LBB6_5:
	subq	48(%r14), %rbp
	leaq	2048(%rbp), %r8
	shrq	$12, %r8
	subq	56(%r14), %r13
	movzwl	26(%r14), %eax
	cmpl	60(%r15), %r8d
	jl	.LBB6_9
# BB#6:
	movzwl	28(%r14), %r10d
	leal	(%r8,%r10), %ecx
	cmpl	68(%r15), %ecx
	jg	.LBB6_9
# BB#7:
	leaq	2048(%r13), %r9
	shrq	$12, %r9
	cmpl	64(%r15), %r9d
	jl	.LBB6_9
# BB#8:
	leal	(%r9,%rax), %ecx
	cmpl	72(%r15), %ecx
	jle	.LBB6_10
.LBB6_9:
	movups	104(%rbx), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	88(%rbx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	24(%rbx), %xmm0
	movups	40(%rbx), %xmm1
	movups	56(%rbx), %xmm2
	movups	72(%rbx), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rbp, %xmm0
	movsd	.LCPI6_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	64(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	movss	%xmm2, 64(%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	80(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 80(%rsp)
	movzwl	24(%r14), %esi
	shll	$3, %esi
	movq	64(%r14), %r9
	movq	%rsp, %r8
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	%eax, %edx
	callq	gs_imagemask
.LBB6_11:
	movl	%eax, %ecx
	sarl	$31, %eax
	andl	%ecx, %eax
	jmp	.LBB6_12
.LBB6_1:
	movl	$-14, %eax
	jmp	.LBB6_12
.LBB6_3:
	movq	304(%rbx), %rdi
	movq	312(%rbx), %r12
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	gx_color_render
	movl	$1, %eax
	cmpl	$0, 16(%r12)
	je	.LBB6_4
.LBB6_12:                               # %.thread
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_4:                                # %.critedge
	movl	$1, 356(%r15)
	jmp	.LBB6_5
.LBB6_10:
	movq	312(%rbx), %rbp
	movq	448(%rbx), %rcx
	movq	(%rcx), %rdi
	movq	8(%rdi), %rbx
	movq	64(%r14), %rsi
	movzwl	24(%r14), %ecx
	xorl	%edx, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	(%rbp)
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rbx)
	addq	$32, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB6_11
.Lfunc_end6:
	.size	gx_copy_cached_char, .Lfunc_end6-gx_copy_cached_char
	.cfi_endproc

	.type	cached_char_sizeof,@object # @cached_char_sizeof
	.data
	.globl	cached_char_sizeof
	.p2align	2
cached_char_sizeof:
	.long	72                      # 0x48
	.size	cached_char_sizeof, 4

	.type	cached_fm_pair_sizeof,@object # @cached_fm_pair_sizeof
	.globl	cached_fm_pair_sizeof
	.p2align	2
cached_fm_pair_sizeof:
	.long	32                      # 0x20
	.size	cached_fm_pair_sizeof, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
