	.text
	.file	"dfgparser.bc"
	.globl	dfg_parse
	.p2align	4, 0x90
	.type	dfg_parse,@function
dfg_parse:                              # @dfg_parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2088, %rsp             # imm = 0x828
.Lcfi3:
	.cfi_offset %rbx, -56
.Lcfi4:
	.cfi_offset %r12, -48
.Lcfi5:
	.cfi_offset %r13, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
	leaq	-528(%rbp), %r15
	leaq	-2128(%rbp), %r8
	movl	$0, dfg_nerrs(%rip)
	movl	$-2, dfg_char(%rip)
	movl	$200, %eax
	movq	%rax, -104(%rbp)        # 8-byte Spill
	movq	%r8, -120(%rbp)         # 8-byte Spill
	movq	%r15, %r9
	xorl	%r13d, %r13d
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r15
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_51 Depth 2
                                        #     Child Loop BB0_272 Depth 2
                                        #     Child Loop BB0_257 Depth 2
                                        #     Child Loop BB0_242 Depth 2
                                        #     Child Loop BB0_220 Depth 2
                                        #     Child Loop BB0_204 Depth 2
                                        #     Child Loop BB0_182 Depth 2
                                        #     Child Loop BB0_193 Depth 2
                                        #     Child Loop BB0_189 Depth 2
                                        #     Child Loop BB0_173 Depth 2
                                        #     Child Loop BB0_166 Depth 2
                                        #     Child Loop BB0_161 Depth 2
                                        #     Child Loop BB0_128 Depth 2
                                        #     Child Loop BB0_106 Depth 2
                                        #     Child Loop BB0_99 Depth 2
                                        #     Child Loop BB0_326 Depth 2
                                        #     Child Loop BB0_64 Depth 2
	movw	%r13w, (%r15)
	movq	-104(%rbp), %rax        # 8-byte Reload
	leaq	-2(%r9,%rax,2), %rax
	cmpq	%rax, %r15
	jae	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, -88(%rbp)         # 8-byte Spill
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	cmpq	$9999, -104(%rbp)       # 8-byte Folded Reload
                                        # imm = 0x270F
	ja	.LBB0_421
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	subq	%r9, %r15
	sarq	%r15
	incq	%r15
	movq	-104(%rbp), %r12        # 8-byte Reload
	addq	%r12, %r12
	cmpq	$10000, %r12            # imm = 0x2710
	movl	$10000, %eax            # imm = 0x2710
	cmovaeq	%rax, %r12
	leaq	(%r12,%r12,4), %rax
	leaq	22(%rax,%rax), %rax
	andq	$-16, %rax
	movq	%rsp, %r14
	subq	%rax, %r14
	movq	%r14, %rsp
	leaq	(%r15,%r15), %rdx
	movq	%r14, %rdi
	movq	%r9, %rsi
	callq	memcpy
	leaq	7(%r12,%r12), %rbx
	andq	$-8, %rbx
	addq	%r14, %rbx
	leaq	(,%r15,8), %rdx
	movq	%rbx, %rdi
	movq	-120(%rbp), %rsi        # 8-byte Reload
	callq	memcpy
	movq	%r12, -104(%rbp)        # 8-byte Spill
	cmpq	%r12, %r15
	jge	.LBB0_402
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	leaq	-8(%rbx,%r15,8), %r8
	leaq	-2(%r14,%r15,2), %r15
	movq	%rbx, -120(%rbp)        # 8-byte Spill
	movq	%r15, -88(%rbp)         # 8-byte Spill
	movq	%r14, %r9
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%r13d, %rbx
	movswl	yypact(%rbx,%rbx), %r15d
	cmpl	$-356, %r15d            # imm = 0xFE9C
	je	.LBB0_21
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	dfg_char(%rip), %eax
	cmpl	$-2, %eax
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r13
	movq	%r9, %r12
	callq	dfg_lex
	movq	%r12, %r9
	movq	%r13, %r8
	movl	%eax, dfg_char(%rip)
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	testl	%eax, %eax
	jle	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$2, %ecx
	cmpl	$318, %eax              # imm = 0x13E
	ja	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%eax, %rcx
	movzbl	yytranslate(%rcx), %ecx
	jmp	.LBB0_14
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$0, dfg_char(%rip)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	leal	(%rcx,%r15), %edx
	cmpl	$506, %edx              # imm = 0x1FA
	ja	.LBB0_21
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%edx, %rsi
	movswl	yycheck(%rsi,%rsi), %edi
	cmpl	%ecx, %edi
	jne	.LBB0_21
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	movzwl	yytable(%rsi,%rsi), %r13d
	testl	%r13d, %r13d
	je	.LBB0_398
# BB#17:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$35, %edx
	je	.LBB0_420
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%eax, %eax
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$-2, dfg_char(%rip)
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movq	dfg_lval(%rip), %rax
	movq	%rax, 8(%r8)
	addq	$8, %r8
	movq	-88(%rbp), %r15         # 8-byte Reload
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_21:                               #   in Loop: Header=BB0_2 Depth=1
	movzbl	yydefact(%rbx), %r13d
	testq	%r13, %r13
	je	.LBB0_398
# BB#22:                                #   in Loop: Header=BB0_2 Depth=1
	movzbl	yyr2(%r13), %r12d
	movl	$1, %eax
	subq	%r12, %rax
	movq	(%r8,%rax,8), %rax
	movq	%rax, -48(%rbp)
	xorl	%eax, %eax
	testb	%al, %al
	movq	-88(%rbp), %r15         # 8-byte Reload
	jne	.LBB0_342
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%r13d, %eax
	addb	$-128, %al
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rax
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_26:                               #   in Loop: Header=BB0_2 Depth=1
	movq	dfg_VARLIST(%rip), %r14
	movl	$16, %edi
	movq	%r12, %r15
	movq	%r8, %r12
	movq	%r9, %rbx
	callq	memory_Malloc
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r15, %r12
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	$0, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, dfg_VARLIST(%rip)
	movb	$1, dfg_VARDECL(%rip)
	jmp	.LBB0_342
.LBB0_28:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rdi
	jmp	.LBB0_86
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, -48(%rbp)
	jmp	.LBB0_342
.LBB0_30:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rsi
	movl	$298, %edi              # imm = 0x12A
	movl	$1, %edx
	jmp	.LBB0_147
.LBB0_31:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	-24(%r8), %r13
	movq	-16(%r8), %r12
	testq	%r13, %r13
	je	.LBB0_282
# BB#32:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$16, %edi
	movq	%r8, %r14
	movq	%r9, -56(%rbp)          # 8-byte Spill
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	movq	%r14, %r13
	movq	-48(%r14), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	jmp	.LBB0_285
.LBB0_209:                              #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_33:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.1(%rip)
	jmp	.LBB0_342
.LBB0_34:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-32(%r8), %rdi
	jmp	.LBB0_86
.LBB0_35:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#287:                               #   in Loop: Header=BB0_2 Depth=1
	movl	-24(%r8), %edi
	jmp	.LBB0_288
.LBB0_37:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$0, dfg_VARDECL(%rip)
	jmp	.LBB0_342
.LBB0_38:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	dfg_VARLIST(%rip), %rax
	movq	8(%rax), %rdi
	movl	$dfg_VarFree, %esi
	callq	list_DeleteWithElement
	movq	dfg_VARLIST(%rip), %rax
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, dfg_VARLIST(%rip)
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_289
# BB#39:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_361
.LBB0_40:                               #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_41:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EQUIV(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_42:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_43:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#290:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %edi
	jmp	.LBB0_291
.LBB0_45:                               #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_46:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_350
# BB#47:                                #   in Loop: Header=BB0_2 Depth=1
	movq	-8(%r8), %rax
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_48:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-16(%r8), %rbx
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_338
# BB#49:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_339
# BB#50:                                # %.preheader.i561.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_51:                               # %.preheader.i561
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_51
	jmp	.LBB0_129
.LBB0_52:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#295:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, %r14
	movq	-24(%r8), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	movq	%r8, %r13
	movq	-8(%r8), %rbx
	movq	%rbx, %rdi
	callq	list_Length
	movq	-56(%rbp), %rdi         # 8-byte Reload
	movl	%eax, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jns	.LBB0_432
# BB#296:                               # %symbol_IsPredicate.exit.i566
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB0_432
# BB#297:                               # %dfg_AtomCreate.exit568
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	term_Create
	movq	%r13, %r8
	movq	%r14, %r9
	jmp	.LBB0_298
.LBB0_54:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#299:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, %r13
	movq	%r8, %r14
	movq	(%r8), %rbx
	xorl	%edi, %edi
	callq	list_Length
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jg	.LBB0_302
# BB#300:                               #   in Loop: Header=BB0_2 Depth=1
	jns	.LBB0_441
# BB#301:                               # %symbol_IsFunction.exit.i569
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB0_441
.LBB0_302:                              # %dfg_TermCreate.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%eax, %edi
	callq	term_Create
	movq	%r14, %r8
	movq	%r13, %r9
	jmp	.LBB0_298
.LBB0_56:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#303:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-24(%r8), %rbx
	movq	%r8, %r13
	movq	-8(%r8), %r14
	movq	%r14, %rdi
	callq	list_Length
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jg	.LBB0_306
# BB#304:                               #   in Loop: Header=BB0_2 Depth=1
	jns	.LBB0_441
# BB#305:                               # %symbol_IsFunction.exit.i571
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB0_441
.LBB0_306:                              # %dfg_TermCreate.exit573
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	term_Create
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	jmp	.LBB0_298
.LBB0_58:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
	jmp	.LBB0_353
.LBB0_60:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rbx
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_381
# BB#61:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_378
# BB#62:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_379
# BB#63:                                # %.preheader.i577.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_64:                               # %.preheader.i577
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_64
# BB#65:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_380
.LBB0_66:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rdi
	movl	$.L.str.5, %esi
	movq	%r8, %rbx
	movq	%r9, %r14
	callq	strcmp
	movq	%r14, %r9
	movq	%rbx, %r8
	testl	%eax, %eax
	je	.LBB0_342
.LBB0_67:                               #   in Loop: Header=BB0_2 Depth=1
	movl	dfg_IGNORE(%rip), %eax
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	$1, dfg_IGNORE(%rip)
	jmp	.LBB0_342
.LBB0_68:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, %r13
	movq	-48(%r8), %rbx
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	movq	%r8, %r14
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_103
# BB#69:                                #   in Loop: Header=BB0_2 Depth=1
	movl	stack_POINTER(%rip), %eax
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movl	stack_STACK(,%rax,8), %eax
	movl	%eax, dfg_IGNORE(%rip)
	movq	-48(%r14), %rbx
	jmp	.LBB0_103
.LBB0_70:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-88(%r8), %rdi
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_308
# BB#71:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_312
.LBB0_72:                               # %.thread965
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %rbx
	callq	string_StringFree
	movq	%rbx, %r8
	jmp	.LBB0_312
.LBB0_73:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#319:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_211
# BB#320:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$16, %edi
	movq	%r8, %r14
	movq	%r12, %r15
	movq	%r9, %r12
	callq	memory_Malloc
	movq	%r12, %r9
	movq	%r15, %r12
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%r14, %r8
	movq	%rbx, 8(%rax)
	jmp	.LBB0_321
.LBB0_75:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_322
.LBB0_76:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rbx
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_77:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_78:                               #   in Loop: Header=BB0_2 Depth=1
	movl	-8(%r8), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_79:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r14
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_328
.LBB0_80:                               # %._crit_edge962
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r13
	movl	$0, -48(%rbp)
	jmp	.LBB0_83
.LBB0_81:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r14
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_331
.LBB0_82:                               # %._crit_edge961
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r13
	movl	-32(%r8), %eax
	movl	%eax, -48(%rbp)
.LBB0_83:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %rbx
	callq	string_StringFree
	movq	%rbx, %r8
.LBB0_85:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	movq	%r13, %r9
	movq	%r14, %r13
	je	.LBB0_342
.LBB0_86:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_StringFree
	jmp	.LBB0_151
.LBB0_87:                               #   in Loop: Header=BB0_2 Depth=1
	movl	stack_POINTER(%rip), %eax
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movl	stack_STACK(,%rax,8), %eax
	movl	%eax, dfg_IGNORE(%rip)
	cmpl	$0, (%r8)
	movq	-16(%r8), %rdi
	je	.LBB0_91
# BB#88:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_StringFree
	movq	%rbx, %r9
	movq	%r14, %r8
.LBB0_90:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%edi, %edi
.LBB0_91:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)
	jmp	.LBB0_342
.LBB0_92:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_93:                               #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_94:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	%r8, %rbx
	movb	$0, dfg_VARDECL(%rip)
	movq	dfg_VARLIST(%rip), %rax
	movq	8(%rax), %rdi
	movl	$dfg_VarFree, %esi
	callq	list_DeleteWithElement
	movq	dfg_VARLIST(%rip), %rax
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, dfg_VARLIST(%rip)
	testq	%rcx, %rcx
	jne	.LBB0_431
# BB#95:                                # %dfg_VarCheck.exit590
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	jmp	.LBB0_247
.LBB0_96:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	dfg_TERMLIST(%rip), %rbx
	movq	%r8, %r13
	movq	-8(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_335
# BB#97:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_336
# BB#98:                                # %.preheader.i594.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_99:                               # %.preheader.i594
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_99
# BB#100:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_337
.LBB0_101:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, %r13
	movq	%r8, %r14
	movq	(%r8), %rbx
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_103
# BB#102:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$0, dfg_IGNORETEXT(%rip)
	movq	(%r14), %rbx
.LBB0_103:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdi
	callq	string_StringFree
	movq	%r14, %r8
	movq	%r13, %r9
	movq	%r12, %r13
	jmp	.LBB0_341
.LBB0_104:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$1, dfg_IGNORETEXT(%rip)
	jmp	.LBB0_342
.LBB0_105:                              # %.preheader613
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-8(%r8), %rax
	testq	%rax, %rax
	movq	%r12, -64(%rbp)         # 8-byte Spill
	je	.LBB0_342
	.p2align	4, 0x90
.LBB0_106:                              #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %r12
	movq	%r9, %r13
	movq	%r8, %r14
	movq	8(%rax), %rdi
	callq	symbol_Lookup
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_425
# BB#107:                               #   in Loop: Header=BB0_106 Depth=2
	movq	%r14, %rcx
	jns	.LBB0_422
# BB#108:                               # %symbol_IsPredicate.exit597
                                        #   in Loop: Header=BB0_106 Depth=2
	negl	%ebx
	movl	symbol_TYPEMASK(%rip), %eax
	andl	%ebx, %eax
	cmpl	$2, %eax
	jne	.LBB0_422
# BB#109:                               #   in Loop: Header=BB0_106 Depth=2
	movq	-8(%rcx), %rax
	movq	8(%rax), %rdi
	callq	string_StringFree
	movq	%r14, %r8
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	orl	$64, 20(%rax)
	movq	-8(%r8), %rcx
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -8(%r8)
	testq	%rax, %rax
	movq	%r13, %r9
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
	jne	.LBB0_106
	jmp	.LBB0_342
.LBB0_110:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-24(%r8), %rdi
	movq	%r8, %rbx
	callq	flag_Id
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	je	.LBB0_434
# BB#111:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%rbx), %rdi
	callq	string_StringFree
	movq	dfg_FLAGS(%rip), %rax
	movq	%rax, -72(%rbp)         # 8-byte Spill
	movq	%rbx, %r13
	movl	-8(%rbx), %ebx
	movl	%r14d, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB0_435
# BB#112:                               #   in Loop: Header=BB0_2 Depth=1
	movl	%r14d, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB0_436
# BB#113:                               # %flag_SetFlagValue.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%r14d, %eax
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	%ebx, (%rcx,%rax,4)
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	%r12, %r13
	jmp	.LBB0_341
.LBB0_114:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, %r14
	movq	(%r8), %rdi
	movq	%r8, %r13
	callq	symbol_Lookup
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_438
# BB#115:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r13), %rdi
	callq	string_StringFree
	movq	dfg_PRECEDENCE(%rip), %r15
	callq	symbol_GetIncreasedOrderingCounter
	movslq	%ebx, %rcx
	movq	%rcx, -56(%rbp)         # 8-byte Spill
	negl	%ebx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movslq	%ebx, %rcx
	movl	%eax, (%r15,%rcx,4)
	movq	dfg_USERPRECEDENCE(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	-56(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, dfg_USERPRECEDENCE(%rip)
	movq	%r13, %r8
	movq	%r14, %r9
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%r12, %r13
	jmp	.LBB0_341
.LBB0_116:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-32(%r8), %rdi
	movq	%r8, %r13
	callq	symbol_Lookup
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_439
# BB#117:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-32(%r13), %rdi
	callq	string_StringFree
	movq	dfg_PRECEDENCE(%rip), %r14
	callq	symbol_GetIncreasedOrderingCounter
	movslq	%ebx, %rcx
	movq	%rcx, -80(%rbp)         # 8-byte Spill
	negl	%ebx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movslq	%ebx, %rbx
	movl	%eax, (%r14,%rbx,4)
	movq	dfg_USERPRECEDENCE(%rip), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	-80(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, dfg_USERPRECEDENCE(%rip)
	movl	-16(%r13), %ecx
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rdx
	movl	%ecx, 12(%rdx)
	movl	-8(%r13), %ecx
	testl	%ecx, %ecx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	-72(%rbp), %r13         # 8-byte Reload
	je	.LBB0_342
# BB#118:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%rax,%rbx,8), %rax
	orl	%ecx, 20(%rax)
	jmp	.LBB0_342
.LBB0_119:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_120:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	(%r8), %rax
	cmpb	$0, 1(%rax)
	jne	.LBB0_433
# BB#121:                               #   in Loop: Header=BB0_2 Depth=1
	movb	(%rax), %al
	movl	%eax, %ecx
	addb	$-108, %cl
	cmpb	$6, %cl
	ja	.LBB0_433
# BB#122:                               #   in Loop: Header=BB0_2 Depth=1
	movzbl	%cl, %ecx
	movl	$67, %edx
	btl	%ecx, %edx
	jae	.LBB0_433
# BB#123:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%ecx, %ecx
	cmpb	$114, %al
	sete	%cl
	shll	$3, %ecx
	cmpb	$109, %al
	movl	$16, %eax
	cmovnel	%ecx, %eax
	movl	%eax, -48(%rbp)
	movq	(%r8), %rdi
	movq	%r8, %rbx
	callq	string_StringFree
	jmp	.LBB0_247
.LBB0_124:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rdi
	movl	$string_StringFree, %esi
	movq	%r8, %rbx
	movq	%r9, %r14
	callq	list_DeleteWithElement
	jmp	.LBB0_148
.LBB0_125:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-16(%r8), %rbx
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_338
# BB#126:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_339
# BB#127:                               # %.preheader.i602.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_128:                              # %.preheader.i602
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_128
.LBB0_129:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_340
.LBB0_130:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.0(%rip)
	jmp	.LBB0_342
.LBB0_131:                              #   in Loop: Header=BB0_2 Depth=1
	movl	-16(%r8), %eax
	movl	%eax, dfg_DESC.4(%rip)
	jmp	.LBB0_342
.LBB0_132:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.5(%rip)
	jmp	.LBB0_342
.LBB0_133:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.2(%rip)
	jmp	.LBB0_342
.LBB0_134:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.3(%rip)
	jmp	.LBB0_342
.LBB0_135:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rax
	movq	%rax, dfg_DESC.6(%rip)
	jmp	.LBB0_342
.LBB0_136:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_137:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$1, -48(%rbp)
	jmp	.LBB0_342
.LBB0_138:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$2, -48(%rbp)
	jmp	.LBB0_342
.LBB0_139:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rsi
	movl	$284, %edi              # imm = 0x11C
	jmp	.LBB0_146
.LBB0_140:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%r8), %rsi
	movl	-8(%r8), %edx
	movl	$284, %edi              # imm = 0x11C
	jmp	.LBB0_150
.LBB0_141:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rsi
	movl	$298, %edi              # imm = 0x12A
	jmp	.LBB0_146
.LBB0_142:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%r8), %rsi
	movl	-8(%r8), %edx
	movl	$298, %edi              # imm = 0x12A
	jmp	.LBB0_150
.LBB0_143:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rsi
	movl	$294, %edi              # imm = 0x126
	jmp	.LBB0_146
.LBB0_144:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%r8), %rsi
	movl	-8(%r8), %edx
	movl	$294, %edi              # imm = 0x126
	jmp	.LBB0_150
.LBB0_145:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rsi
	movl	$300, %edi              # imm = 0x12C
.LBB0_146:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$-2, %edx
.LBB0_147:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %rbx
	movq	%r9, %r14
	callq	dfg_SymbolDecl
.LBB0_148:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, %r9
	movq	%rbx, %r8
	jmp	.LBB0_342
.LBB0_149:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%r8), %rsi
	movl	-8(%r8), %edx
	movl	$300, %edi              # imm = 0x12C
.LBB0_150:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	dfg_SymbolDecl
.LBB0_151:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %r9
	movq	%r14, %r8
	jmp	.LBB0_342
.LBB0_152:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$-1, -48(%rbp)
	jmp	.LBB0_342
.LBB0_153:                              #   in Loop: Header=BB0_2 Depth=1
	movl	(%r8), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_154:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	-32(%r8), %rdi
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	-16(%r8), %rbx
	movl	$1, %esi
	callq	dfg_Symbol
	movl	%eax, %r14d
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	dfg_Symbol
	movl	%eax, %ebx
	testl	%r14d, %r14d
	jns	.LBB0_429
# BB#155:                               # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%r14d, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB0_429
# BB#156:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, -72(%rbp)         # 8-byte Spill
	testl	%ebx, %ebx
	jns	.LBB0_429
# BB#157:                               # %symbol_IsPredicate.exit23.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%ebx, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB0_429
# BB#158:                               #   in Loop: Header=BB0_2 Depth=1
	movl	symbol_STANDARDVARCOUNTER(%rip), %edi
	incl	%edi
	movl	%edi, symbol_STANDARDVARCOUNTER(%rip)
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %r13
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, -96(%rbp)         # 8-byte Spill
	movq	%r13, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, -112(%rbp)        # 4-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	-96(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movl	-112(%rbp), %edi        # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, -112(%rbp)        # 8-byte Spill
	movl	fol_ALL(%rip), %eax
	movl	%eax, -96(%rbp)         # 4-byte Spill
	movq	%r13, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	-112(%rbp), %rcx        # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movl	-96(%rbp), %edi         # 4-byte Reload
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %r14
	movq	dfg_SORTDECLLIST(%rip), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%r13, %r13
	je	.LBB0_372
# BB#159:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	movq	-80(%rbp), %r8          # 8-byte Reload
	movq	-56(%rbp), %r9          # 8-byte Reload
	je	.LBB0_373
# BB#160:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB0_161:                              # %.preheader.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_161
# BB#162:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_373
.LBB0_163:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	dfg_SORTDECLLIST(%rip), %r14
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	-8(%r8), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%r13, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%r14, %r14
	je	.LBB0_346
# BB#164:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_347
# BB#165:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, %rdx
	movq	-80(%rbp), %r8          # 8-byte Reload
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB0_166:                              # %.preheader.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_166
# BB#167:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_348
.LBB0_168:                              #   in Loop: Header=BB0_2 Depth=1
	movb	$0, dfg_VARDECL(%rip)
	jmp	.LBB0_342
.LBB0_169:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	dfg_VARLIST(%rip), %rax
	movq	8(%rax), %rdi
	movl	$dfg_VarFree, %esi
	callq	list_DeleteWithElement
	movq	dfg_VARLIST(%rip), %rax
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, dfg_VARLIST(%rip)
	testq	%rcx, %rcx
	jne	.LBB0_431
# BB#170:                               # %dfg_VarCheck.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movl	fol_ALL(%rip), %edi
	movq	%r13, %rax
	movq	-48(%rax), %rsi
	movq	-16(%rax), %rdx
	callq	dfg_CreateQuantifier
	movq	%rax, %r14
	movq	dfg_SORTDECLLIST(%rip), %rax
	movq	%rax, -80(%rbp)         # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	-80(%rbp), %rsi         # 8-byte Reload
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rsi, %rsi
	je	.LBB0_371
# BB#171:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	movq	-56(%rbp), %r9          # 8-byte Reload
	je	.LBB0_376
# BB#172:                               # %.preheader.i514.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rsi, %rdx
	movq	%r13, %r8
	.p2align	4, 0x90
.LBB0_173:                              # %.preheader.i514
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_173
# BB#174:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_377
.LBB0_175:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	-56(%r8), %rdi
	movl	$1, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jns	.LBB0_429
# BB#176:                               # %symbol_IsPredicate.exit.i517
                                        #   in Loop: Header=BB0_2 Depth=1
	negl	%eax
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%eax, %ecx
	cmpl	$2, %ecx
	jne	.LBB0_429
# BB#177:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-80(%rbp), %rcx         # 8-byte Reload
	movl	-48(%rcx), %r8d
	movq	-16(%rcx), %rcx
	movq	%rcx, -96(%rbp)         # 8-byte Spill
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%eax, %rdx
	movq	(%rcx,%rdx,8), %rax
	movl	20(%rax), %ebx
	testb	$2, %bh
	je	.LBB0_179
# BB#178:                               #   in Loop: Header=BB0_2 Depth=1
	addl	$-512, %ebx             # imm = 0xFE00
	movl	%ebx, 20(%rax)
	movq	(%rcx,%rdx,8), %rax
	movl	20(%rax), %ebx
.LBB0_179:                              # %symbol_RemoveProperty.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	testb	$1, %bh
	je	.LBB0_181
# BB#180:                               #   in Loop: Header=BB0_2 Depth=1
	addl	$-256, %ebx
	movl	%ebx, 20(%rax)
	movq	(%rcx,%rdx,8), %rax
.LBB0_181:                              # %symbol_RemoveProperty.exit27.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB0_184
	.p2align	4, 0x90
.LBB0_182:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB0_182
# BB#183:                               # %list_Delete.exit.loopexit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	symbol_SIGNATURE(%rip), %rcx
	movq	(%rcx,%rdx,8), %rax
.LBB0_184:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	orb	$2, 21(%rax)
	testl	%r8d, %r8d
	je	.LBB0_186
# BB#185:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%rcx,%rdx,8), %rax
	orl	$256, 20(%rax)          # imm = 0x100
.LBB0_186:                              # %.preheader.i519
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rdx, -112(%rbp)        # 8-byte Spill
	cmpq	$0, -96(%rbp)           # 8-byte Folded Reload
	je	.LBB0_197
# BB#187:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r8d, %r8d
	movq	-96(%rbp), %r13         # 8-byte Reload
	je	.LBB0_193
# BB#188:                               # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-96(%rbp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_189:                              # %.lr.ph.split.us.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rdi
	callq	symbol_Lookup
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB0_428
# BB#190:                               #   in Loop: Header=BB0_189 Depth=2
	jns	.LBB0_423
# BB#191:                               # %symbol_IsFunction.exit.us.i
                                        #   in Loop: Header=BB0_189 Depth=2
	movl	%r14d, %ebx
	negl	%ebx
	movl	symbol_TYPEMASK(%rip), %eax
	andl	%ebx, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB0_423
# BB#192:                               #   in Loop: Header=BB0_189 Depth=2
	movq	8(%r13), %rdi
	callq	string_StringFree
	movslq	%r14d, %rax
	movq	%rax, 8(%r13)
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%ebx, %rax
	movq	(%rcx,%rax,8), %rdx
	orl	$512, 20(%rdx)          # imm = 0x200
	movq	(%rcx,%rax,8), %rax
	orl	$256, 20(%rax)          # imm = 0x100
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_189
	jmp	.LBB0_197
	.p2align	4, 0x90
.LBB0_193:                              # %.lr.ph.split.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rdi
	callq	symbol_Lookup
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB0_428
# BB#194:                               #   in Loop: Header=BB0_193 Depth=2
	jns	.LBB0_423
# BB#195:                               # %symbol_IsFunction.exit.i
                                        #   in Loop: Header=BB0_193 Depth=2
	movl	%r14d, %ebx
	negl	%ebx
	movl	symbol_TYPEMASK(%rip), %eax
	andl	%ebx, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB0_423
# BB#196:                               #   in Loop: Header=BB0_193 Depth=2
	movq	8(%r13), %rdi
	callq	string_StringFree
	movslq	%r14d, %rax
	movq	%rax, 8(%r13)
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%ebx, %rax
	movq	(%rcx,%rax,8), %rax
	orl	$512, 20(%rax)          # imm = 0x200
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_193
.LBB0_197:                              # %dfg_SymbolGenerated.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-112(%rbp), %rax        # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	-96(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 32(%rax)
	movq	-80(%rbp), %r8          # 8-byte Reload
	jmp	.LBB0_253
.LBB0_198:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_199:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$1, -48(%rbp)
	jmp	.LBB0_342
.LBB0_200:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %r14
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	(%r8), %r13
	movl	$16, %edi
	movq	%r12, %r15
	movq	%r8, %r12
	movq	%r9, %rbx
	callq	memory_Malloc
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r15, %r12
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%r13, 8(%rax)
	movq	-72(%rbp), %r13         # 8-byte Reload
	movq	%r14, (%rax)
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_201:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	-16(%r8), %rdi
	movq	%r8, %rbx
	callq	list_NReverse
	movq	%rbx, %r8
	cmpl	$0, -40(%r8)
	movl	$dfg_CONJECLIST, %ecx
	movl	$dfg_AXIOMLIST, %eax
	cmoveq	%rcx, %rax
	movq	(%rax), %rcx
	movq	-16(%r8), %rdx
	testq	%rcx, %rcx
	je	.LBB0_349
# BB#202:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rdx, %rdx
	je	.LBB0_374
# BB#203:                               # %.preheader.i523.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rcx, %rdi
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB0_204:                              # %.preheader.i523
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_204
	jmp	.LBB0_205
.LBB0_206:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$1, -48(%rbp)
	jmp	.LBB0_342
.LBB0_207:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_210:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
.LBB0_350:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_NOT(%rip), %eax
	movl	%eax, -56(%rbp)         # 4-byte Spill
	movq	-8(%r8), %rbx
	movl	$16, %edi
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r8, %r13
	movq	%r9, %r14
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	-56(%rbp), %edi         # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	%r14, %r9
	movq	%r13, %r8
.LBB0_298:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_212:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#351:                               #   in Loop: Header=BB0_2 Depth=1
	movl	-40(%r8), %eax
	jmp	.LBB0_352
.LBB0_214:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
	jmp	.LBB0_353
.LBB0_216:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rbx
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_385
# BB#217:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_382
# BB#218:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_383
# BB#219:                               # %.preheader.i533.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_220:                              # %.preheader.i533
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_220
# BB#221:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_384
.LBB0_222:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_IMPLIED(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_223:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_AND(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_224:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_OR(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_225:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EXIST(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_226:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_ALL(%rip), %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_227:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	movq	(%r8), %rdi
	je	.LBB0_229
# BB#228:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_StringFree
	movq	%rbx, %r9
	movq	%r14, %r8
	xorl	%edi, %edi
.LBB0_229:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)
	jmp	.LBB0_342
.LBB0_230:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#354:                               #   in Loop: Header=BB0_2 Depth=1
	movl	(%r8), %edi
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_IntToString
	jmp	.LBB0_292
.LBB0_232:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#355:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.1, %edi
	jmp	.LBB0_291
.LBB0_234:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#356:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %edi
.LBB0_291:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_StringCopy
	jmp	.LBB0_292
.LBB0_236:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
	jmp	.LBB0_353
.LBB0_238:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rbx
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_389
# BB#239:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_386
# BB#240:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_387
# BB#241:                               # %.preheader.i539.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_242:                              # %.preheader.i539
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_242
# BB#243:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_388
.LBB0_244:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_342
# BB#245:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	(%r8), %rdi
	xorl	%esi, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jle	.LBB0_440
# BB#246:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%eax, %edi
	callq	term_Create
	movq	%rax, -48(%rbp)
.LBB0_247:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %r8
	movq	%r14, %r9
	jmp	.LBB0_342
.LBB0_248:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_342
# BB#249:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, -72(%rbp)         # 8-byte Spill
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	-24(%r8), %rdi
	movl	$1, %esi
	callq	dfg_Symbol
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jns	.LBB0_432
# BB#250:                               # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%r14d, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB0_432
# BB#251:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-8(%r13), %rdi
	xorl	%esi, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jle	.LBB0_440
# BB#252:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%eax, %edi
	callq	term_Create
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, -48(%rbp)
	movq	%r13, %r8
.LBB0_253:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	-72(%rbp), %r13         # 8-byte Reload
	jmp	.LBB0_342
.LBB0_254:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	-16(%r8), %rdi
	movq	%r8, %rbx
	callq	list_NReverse
	movq	%rbx, %r8
	cmpl	$0, -56(%r8)
	movl	$dfg_CONCLAUSES, %ecx
	movl	$dfg_AXCLAUSES, %eax
	cmoveq	%rcx, %rax
	movq	(%rax), %rcx
	movq	-16(%r8), %rdx
	testq	%rcx, %rcx
	je	.LBB0_349
# BB#255:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rdx, %rdx
	je	.LBB0_374
# BB#256:                               # %.preheader.i545.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rcx, %rdi
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB0_257:                              # %.preheader.i545
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_257
.LBB0_205:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rdx, (%rsi)
	movq	%rcx, (%rax)
	jmp	.LBB0_342
.LBB0_259:                              #   in Loop: Header=BB0_2 Depth=1
	movl	stack_POINTER(%rip), %eax
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movl	stack_STACK(,%rax,8), %eax
	movl	%eax, dfg_IGNORE(%rip)
	jmp	.LBB0_342
.LBB0_260:                              #   in Loop: Header=BB0_2 Depth=1
	movq	$0, -48(%rbp)
	jmp	.LBB0_342
.LBB0_261:                              #   in Loop: Header=BB0_2 Depth=1
	movb	$0, dfg_VARDECL(%rip)
	jmp	.LBB0_342
.LBB0_262:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	dfg_VARLIST(%rip), %rax
	movq	8(%rax), %rdi
	movl	$dfg_VarFree, %esi
	callq	list_DeleteWithElement
	movq	dfg_VARLIST(%rip), %rax
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, dfg_VARLIST(%rip)
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_359
# BB#263:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_361
.LBB0_264:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#362:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_OR(%rip), %edi
.LBB0_288:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-8(%r8), %rsi
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	term_Create
.LBB0_292:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %r9
	movq	%r14, %r8
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_266:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
.LBB0_353:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %r14
	movl	$16, %edi
	movq	%r12, %r15
	movq	%r8, %r12
	movq	%r9, %rbx
	callq	memory_Malloc
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r15, %r12
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%r14, 8(%rax)
.LBB0_321:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	$0, (%rax)
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_268:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rbx
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_393
# BB#269:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	(%r8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_390
# BB#270:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_391
# BB#271:                               # %.preheader.i555.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_272:                              # %.preheader.i555
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_272
# BB#273:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_392
.LBB0_274:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#364:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r14
	movq	%r13, %r12
	movq	%r9, %r13
	movq	%r8, %rbx
	movq	(%r8), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	xorl	%edi, %edi
	callq	list_Length
	movq	-56(%rbp), %rdi         # 8-byte Reload
	movl	%eax, %esi
	callq	dfg_Symbol
	testl	%eax, %eax
	jns	.LBB0_432
# BB#365:                               # %symbol_IsPredicate.exit.i564
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB0_432
# BB#366:                               # %dfg_AtomCreate.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%eax, %edi
	callq	term_Create
	movq	%rbx, %r8
	movq	%r13, %r9
	movq	%r12, %r13
	movq	%r14, %r12
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_276:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#367:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_TRUE(%rip), %edi
	jmp	.LBB0_368
.LBB0_278:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	jne	.LBB0_211
# BB#369:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_FALSE(%rip), %edi
.LBB0_368:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movq	%r8, %rbx
	movq	%r9, %r14
	callq	term_Create
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_280:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, dfg_IGNORE(%rip)
	je	.LBB0_370
.LBB0_211:                              #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_282:                              #   in Loop: Header=BB0_2 Depth=1
	testq	%r12, %r12
	je	.LBB0_284
# BB#283:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %rdi
	movq	%r8, %r14
	movq	%r9, %rbx
	callq	string_StringFree
	movq	%rbx, %r9
	movq	%r14, %r8
.LBB0_284:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r13
	movq	-48(%r8), %rax
.LBB0_285:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, -48(%rbp)
	cmpq	$0, dfg_VARLIST(%rip)
	jne	.LBB0_431
# BB#286:                               # %dfg_VarCheck.exit526
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r13, %r8
	movq	-72(%rbp), %r13         # 8-byte Reload
	jmp	.LBB0_341
.LBB0_338:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_339:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_340:                              # %list_Nconc.exit604
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_341
.LBB0_349:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rdx, %rcx
.LBB0_374:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, %r9
	movq	%rcx, (%rax)
	jmp	.LBB0_342
.LBB0_289:                              #   in Loop: Header=BB0_2 Depth=1
	movl	-72(%rbx), %edi
	jmp	.LBB0_360
.LBB0_308:                              #   in Loop: Header=BB0_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_312
# BB#309:                               #   in Loop: Header=BB0_2 Depth=1
	cmpq	$0, -72(%r8)
	je	.LBB0_72
# BB#310:                               #   in Loop: Header=BB0_2 Depth=1
	cmpq	$0, -32(%r8)
	je	.LBB0_72
# BB#311:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r14
	movq	-56(%r8), %rdi
	movq	%r8, %rbx
	callq	clause_GetOriginFromString
	movl	%eax, %r13d
	movq	-56(%rbx), %rdi
	callq	string_StringFree
	movslq	-16(%rbx), %rax
	movq	%rax, -72(%rbp)         # 8-byte Spill
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movl	%r13d, %r12d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%r12, 8(%r13)
	movq	$0, (%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%rcx, -96(%rbp)         # 8-byte Spill
	movq	-72(%rbp), %rax         # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	%r13, (%rcx)
	movq	-88(%rbx), %rax
	movq	%rax, -72(%rbp)         # 8-byte Spill
	movq	-72(%rbx), %rax
	movq	%rax, -80(%rbp)         # 8-byte Spill
	movq	%rbx, %r13
	movq	-32(%rbx), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	-96(%rbp), %rax         # 8-byte Reload
	movq	%rax, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	-80(%rbp), %rax         # 8-byte Reload
	movq	%rax, 8(%r12)
	movq	%rbx, (%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	-72(%rbp), %rax         # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%r12, (%rbx)
	movq	dfg_PROOFLIST(%rip), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	-64(%rbp), %r12         # 8-byte Reload
	movq	%rax, dfg_PROOFLIST(%rip)
	jmp	.LBB0_317
.LBB0_312:                              # %.thread966
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-72(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_314
# BB#313:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %rbx
	callq	term_Delete
	movq	%rbx, %r8
.LBB0_314:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r14
	movq	-56(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_316
# BB#315:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %rbx
	callq	string_StringFree
	movq	%rbx, %r8
.LBB0_316:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r8, %r13
	movq	-32(%r8), %rdi
	movl	$string_StringFree, %esi
	callq	list_DeleteWithElement
.LBB0_317:                              #   in Loop: Header=BB0_2 Depth=1
	cmpq	$0, dfg_VARLIST(%rip)
	movq	-56(%rbp), %r9          # 8-byte Reload
	jne	.LBB0_431
# BB#318:                               # %dfg_VarCheck.exit580
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r13, %r8
	movq	%r14, %r13
	jmp	.LBB0_342
.LBB0_322:                              #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %r14
	testq	%r14, %r14
	je	.LBB0_76
# BB#323:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	%r9, -56(%rbp)          # 8-byte Spill
	movq	%r8, %r13
	movq	-16(%r8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_395
# BB#324:                               #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_396
# BB#325:                               # %.preheader.i584.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_326:                              # %.preheader.i584
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_326
# BB#327:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_397
.LBB0_328:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_80
# BB#329:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_80
# BB#330:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.6, %esi
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	%r9, %r13
	callq	strcmp
	movq	%r13, %r9
	movq	-80(%rbp), %r8          # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_80
	jmp	.LBB0_334
.LBB0_331:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-16(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_82
# BB#332:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_82
# BB#333:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.6, %esi
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	%r9, %r13
	callq	strcmp
	movq	%r13, %r9
	movq	-80(%rbp), %r8          # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_82
.LBB0_334:                              #   in Loop: Header=BB0_2 Depth=1
	leaq	-48(%rbp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	string_StringToInt
	movq	-80(%rbp), %r8          # 8-byte Reload
	jmp	.LBB0_83
.LBB0_335:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_336:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_337:                              # %list_Nconc.exit596
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	%rbx, dfg_TERMLIST(%rip)
.LBB0_341:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-64(%rbp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_342:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(,%r12,8), %rax
	subq	%rax, %r8
	addq	%r12, %r12
	subq	%r12, %r15
	movq	-48(%rbp), %rax
	movq	%rax, 8(%r8)
	addq	$8, %r8
	movzbl	yyr1(%r13), %eax
	movswl	yypgoto-142(%rax,%rax), %ecx
	movswl	(%r15), %edx
	addl	%edx, %ecx
	cmpl	$506, %ecx              # imm = 0x1FA
	ja	.LBB0_345
# BB#343:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	%ecx, %rcx
	cmpw	%dx, yycheck(%rcx,%rcx)
	jne	.LBB0_345
# BB#344:                               #   in Loop: Header=BB0_2 Depth=1
	movzwl	yytable(%rcx,%rcx), %r13d
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_345:                              #   in Loop: Header=BB0_2 Depth=1
	movswl	yydefgoto-142(%rax,%rax), %r13d
	jmp	.LBB0_1
.LBB0_346:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r14
.LBB0_347:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-80(%rbp), %r8          # 8-byte Reload
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	%r12, %r13
.LBB0_348:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-64(%rbp), %r12         # 8-byte Reload
	movq	%r14, dfg_SORTDECLLIST(%rip)
	jmp	.LBB0_342
.LBB0_359:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_ALL(%rip), %edi
.LBB0_360:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-40(%rbx), %rsi
	movq	-8(%rbx), %rdx
	callq	dfg_CreateQuantifier
.LBB0_361:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %r8
	movq	%rax, -48(%rbp)
	movq	%r14, %r9
	jmp	.LBB0_342
.LBB0_370:                              #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EQUALITY(%rip), %eax
.LBB0_352:                              # %.loopexit614
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, -72(%rbp)         # 4-byte Spill
	movq	%r12, -64(%rbp)         # 8-byte Spill
	movq	%r13, %r12
	movq	-24(%r8), %r13
	movq	-8(%r8), %r14
	movl	$16, %edi
	movq	%r8, -80(%rbp)          # 8-byte Spill
	movq	%r9, -56(%rbp)          # 8-byte Spill
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
	movq	%rbx, (%rax)
	movl	-72(%rbp), %edi         # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	-56(%rbp), %r9          # 8-byte Reload
	movq	-80(%rbp), %r8          # 8-byte Reload
	movq	%rax, -48(%rbp)
	jmp	.LBB0_342
.LBB0_371:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rsi
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
	jmp	.LBB0_377
.LBB0_372:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r13
	movq	-80(%rbp), %r8          # 8-byte Reload
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_373:                              # %dfg_SubSort.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, dfg_SORTDECLLIST(%rip)
	movq	-72(%rbp), %r13         # 8-byte Reload
	jmp	.LBB0_342
.LBB0_376:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
.LBB0_377:                              # %list_Nconc.exit516
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-72(%rbp), %r13         # 8-byte Reload
	movq	%rsi, dfg_SORTDECLLIST(%rip)
	jmp	.LBB0_342
.LBB0_378:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_379:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_380:                              # %list_Nconc.exit579
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
.LBB0_381:                              # %list_Nconc.exit579
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_382:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_383:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_384:                              # %list_Nconc.exit535
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
.LBB0_385:                              # %list_Nconc.exit535
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_386:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_387:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_388:                              # %list_Nconc.exit541
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
.LBB0_389:                              # %list_Nconc.exit541
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_390:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_391:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_392:                              # %list_Nconc.exit557
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
.LBB0_393:                              # %list_Nconc.exit557
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_395:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_396:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r8
	movq	-56(%rbp), %r9          # 8-byte Reload
.LBB0_397:                              # %list_Nconc.exit586
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, %r13
	movq	-64(%rbp), %r12         # 8-byte Reload
	movq	%rbx, -48(%rbp)
	jmp	.LBB0_342
.LBB0_398:
	incl	dfg_nerrs(%rip)
	cmpl	$-355, %r15d            # imm = 0xFE9D
	jl	.LBB0_430
# BB#399:
	movslq	%r15d, %rcx
	movslq	dfg_char(%rip), %rax
	cmpq	$318, %rax              # imm = 0x13E
	ja	.LBB0_404
# BB#400:
	movzbl	yytranslate(%rax), %eax
	jmp	.LBB0_405
.LBB0_401:
	movq	-56(%r8), %rdi
	callq	string_StringFree
	xorl	%eax, %eax
	jmp	.LBB0_403
.LBB0_402:
	movl	$1, %eax
.LBB0_403:                              # %.loopexit
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_404:
	movl	$2, %eax
.LBB0_405:
	movq	%rax, -104(%rbp)        # 8-byte Spill
	negl	%r15d
	xorl	%r14d, %r14d
	testw	%cx, %cx
	cmovnsl	%r14d, %r15d
	cmpl	$171, %r15d
	movq	%rcx, -88(%rbp)         # 8-byte Spill
	jg	.LBB0_411
# BB#406:                               # %.lr.ph783.preheader
	movslq	%r15d, %r13
	leaq	(%rcx,%r13), %rax
	leaq	yycheck(%rax,%rax), %r12
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_407:                              # %.lr.ph783
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %r13d
	je	.LBB0_410
# BB#408:                               # %.lr.ph783
                                        #   in Loop: Header=BB0_407 Depth=1
	movswl	(%r12), %eax
	cmpl	%eax, %r13d
	jne	.LBB0_410
# BB#409:                               #   in Loop: Header=BB0_407 Depth=1
	movq	yytname(,%r13,8), %rdi
	callq	strlen
	leaq	15(%rbx,%rax), %rbx
	incl	%r14d
.LBB0_410:                              #   in Loop: Header=BB0_407 Depth=1
	incq	%r13
	addq	$2, %r12
	cmpq	$172, %r13
	jl	.LBB0_407
	jmp	.LBB0_412
.LBB0_411:
	xorl	%ebx, %ebx
.LBB0_412:                              # %._crit_edge
	movq	-104(%rbp), %rax        # 8-byte Reload
	movq	yytname(,%rax,8), %r13
	movq	%r13, %rdi
	callq	strlen
	leaq	40(%rbx,%rax), %rax
	andq	$-16, %rax
	movq	%rsp, %rcx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	negq	%rax
	movq	%rdx, %rsp
	leaq	24(%rdx), %rdi
	movups	.L.str.16(%rip), %xmm0
	movups	%xmm0, (%rcx,%rax)
	movups	.L.str.16+9(%rip), %xmm0
	movq	%rdx, -104(%rbp)        # 8-byte Spill
	movups	%xmm0, 9(%rdx)
	movq	%r13, %rsi
	callq	stpcpy
	cmpl	$4, %r14d
	jg	.LBB0_419
# BB#413:                               # %._crit_edge
	cmpl	$171, %r15d
	jg	.LBB0_419
# BB#414:                               # %.lr.ph.preheader
	movslq	%r15d, %rbx
	movq	-88(%rbp), %rcx         # 8-byte Reload
	addq	%rbx, %rcx
	leaq	yycheck(%rcx,%rcx), %r14
	xorl	%r12d, %r12d
	movl	$.L.str.17, %r13d
	.p2align	4, 0x90
.LBB0_415:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %ebx
	je	.LBB0_418
# BB#416:                               # %.lr.ph
                                        #   in Loop: Header=BB0_415 Depth=1
	movswl	(%r14), %ecx
	cmpl	%ecx, %ebx
	jne	.LBB0_418
# BB#417:                               #   in Loop: Header=BB0_415 Depth=1
	testl	%r12d, %r12d
	movl	$.L.str.18, %esi
	cmoveq	%r13, %rsi
	movq	%rax, %rdi
	callq	stpcpy
	movq	yytname(,%rbx,8), %rsi
	movq	%rax, %rdi
	callq	stpcpy
	incl	%r12d
.LBB0_418:                              #   in Loop: Header=BB0_415 Depth=1
	incq	%rbx
	addq	$2, %r14
	cmpq	$172, %rbx
	jl	.LBB0_415
.LBB0_419:                              # %.loopexit607
	movq	-104(%rbp), %rdi        # 8-byte Reload
	callq	dfg_error
.LBB0_420:
	xorl	%eax, %eax
	jmp	.LBB0_403
.LBB0_421:                              # %.thread
	movl	$.L.str.21, %edi
	callq	dfg_error
.LBB0_422:                              # %symbol_IsPredicate.exit597.thread
	movq	stdout(%rip), %rdi
	movq	%rcx, %rbx
	callq	fflush
	movq	-8(%rbx), %rax
	movq	8(%rax), %rsi
	movl	$.L.str.9, %edi
	jmp	.LBB0_426
.LBB0_423:                              # %symbol_IsFunction.exit.thread.i
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.232, %edi
.LBB0_424:                              # %symbol_IsPredicate.exit.thread.i
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_425:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	-8(%r14), %rax
	movq	8(%rax), %rsi
	movl	$.L.str.7, %edi
.LBB0_426:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.8, %edi
.LBB0_427:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_428:
	addq	$8, %r13
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movq	(%r13), %rdx
	movl	$.L.str.231, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_429:                              # %symbol_IsPredicate.exit.thread.i
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.230, %edi
	jmp	.LBB0_424
.LBB0_430:
	movl	$.L.str.20, %edi
	callq	dfg_error
.LBB0_431:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	movl	$.L.str.28, %edx
	movl	$1881, %ecx             # imm = 0x759
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.233, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.LBB0_432:                              # %symbol_IsPredicate.exit.thread
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.4, %edi
	jmp	.LBB0_424
.LBB0_433:
	movq	stdout(%rip), %rdi
	movq	%r8, %rbx
	callq	fflush
	movq	(%rbx), %rsi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.15, %edi
	jmp	.LBB0_427
.LBB0_434:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	-24(%rbx), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_435:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%r14d, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.42, %edi
	jmp	.LBB0_437
.LBB0_436:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%r14d, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.43, %edi
.LBB0_437:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_438:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r13), %rsi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.12, %edi
	jmp	.LBB0_427
.LBB0_439:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	-32(%r13), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.13, %edi
	jmp	.LBB0_427
.LBB0_440:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.3, %edi
	jmp	.LBB0_424
.LBB0_441:                              # %symbol_IsFunction.exit.thread.i570
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.41, %edi
	jmp	.LBB0_424
.Lfunc_end0:
	.size	dfg_parse, .Lfunc_end0-dfg_parse
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_52
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_54
	.quad	.LBB0_56
	.quad	.LBB0_58
	.quad	.LBB0_60
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_66
	.quad	.LBB0_68
	.quad	.LBB0_342
	.quad	.LBB0_70
	.quad	.LBB0_73
	.quad	.LBB0_75
	.quad	.LBB0_77
	.quad	.LBB0_78
	.quad	.LBB0_79
	.quad	.LBB0_81
	.quad	.LBB0_67
	.quad	.LBB0_87
	.quad	.LBB0_25
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_24
	.quad	.LBB0_92
	.quad	.LBB0_29
	.quad	.LBB0_29
	.quad	.LBB0_25
	.quad	.LBB0_93
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_26
	.quad	.LBB0_94
	.quad	.LBB0_342
	.quad	.LBB0_96
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_101
	.quad	.LBB0_104
	.quad	.LBB0_28
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_105
	.quad	.LBB0_110
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_114
	.quad	.LBB0_116
	.quad	.LBB0_119
	.quad	.LBB0_120
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_124
	.quad	.LBB0_353
	.quad	.LBB0_125
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_401
	.quad	.LBB0_342
	.quad	.LBB0_130
	.quad	.LBB0_33
	.quad	.LBB0_131
	.quad	.LBB0_132
	.quad	.LBB0_342
	.quad	.LBB0_133
	.quad	.LBB0_342
	.quad	.LBB0_134
	.quad	.LBB0_342
	.quad	.LBB0_135
	.quad	.LBB0_136
	.quad	.LBB0_137
	.quad	.LBB0_138
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_139
	.quad	.LBB0_140
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_141
	.quad	.LBB0_142
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_30
	.quad	.LBB0_30
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_143
	.quad	.LBB0_144
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_145
	.quad	.LBB0_149
	.quad	.LBB0_152
	.quad	.LBB0_153
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_154
	.quad	.LBB0_163
	.quad	.LBB0_34
	.quad	.LBB0_26
	.quad	.LBB0_168
	.quad	.LBB0_169
	.quad	.LBB0_175
	.quad	.LBB0_198
	.quad	.LBB0_199
	.quad	.LBB0_353
	.quad	.LBB0_200
	.quad	.LBB0_28
	.quad	.LBB0_28
	.quad	.LBB0_201
	.quad	.LBB0_206
	.quad	.LBB0_207
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_209
	.quad	.LBB0_31
	.quad	.LBB0_209
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_210
	.quad	.LBB0_212
	.quad	.LBB0_35
	.quad	.LBB0_26
	.quad	.LBB0_37
	.quad	.LBB0_38
	.quad	.LBB0_40
	.quad	.LBB0_25
	.quad	.LBB0_214
	.quad	.LBB0_216
	.quad	.LBB0_41
	.quad	.LBB0_222
	.quad	.LBB0_42
	.quad	.LBB0_223
	.quad	.LBB0_224
	.quad	.LBB0_225
	.quad	.LBB0_226
	.quad	.LBB0_227
	.quad	.LBB0_230
	.quad	.LBB0_43
	.quad	.LBB0_232
	.quad	.LBB0_234
	.quad	.LBB0_236
	.quad	.LBB0_238
	.quad	.LBB0_244
	.quad	.LBB0_248
	.quad	.LBB0_342
	.quad	.LBB0_342
	.quad	.LBB0_254
	.quad	.LBB0_67
	.quad	.LBB0_259
	.quad	.LBB0_45
	.quad	.LBB0_31
	.quad	.LBB0_260
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_26
	.quad	.LBB0_261
	.quad	.LBB0_262
	.quad	.LBB0_264
	.quad	.LBB0_266
	.quad	.LBB0_268
	.quad	.LBB0_25
	.quad	.LBB0_46
	.quad	.LBB0_353
	.quad	.LBB0_48
	.quad	.LBB0_274
	.quad	.LBB0_276
	.quad	.LBB0_278
	.quad	.LBB0_280

	.text
	.p2align	4, 0x90
	.type	dfg_SymbolDecl,@function
dfg_SymbolDecl:                         # @dfg_SymbolDecl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 64
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r15
	movl	%edi, %ebx
	xorl	%ebp, %ebp
	cmpl	$-2, %r13d
	je	.LBB1_3
# BB#1:
	cmpl	$-1, %r13d
	je	.LBB1_41
# BB#2:
	movl	%r13d, %ebp
.LBB1_3:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	cmpl	$64, %r14d
	jb	.LBB1_5
# BB#4:
	movb	$0, 63(%r15)
.LBB1_5:
	movq	%r15, %rdi
	callq	symbol_Lookup
	movl	%eax, %r12d
	testl	%r12d, %r12d
	je	.LBB1_31
# BB#6:
	leal	-284(%rbx), %eax
	rorl	%eax
	cmpl	$8, %eax
	ja	.LBB1_13
# BB#7:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_20:
	testl	%r12d, %r12d
	jns	.LBB1_23
# BB#21:                                # %.symbol_IsJunctor.exit_crit_edge
	movl	symbol_TYPEMASK(%rip), %ecx
	movl	%r12d, %eax
	negl	%eax
	jmp	.LBB1_22
.LBB1_31:
	cmpl	$298, %ebx              # imm = 0x12A
	je	.LBB1_34
# BB#32:
	cmpl	$284, %ebx              # imm = 0x11C
	jne	.LBB1_35
# BB#33:
	movq	dfg_PRECEDENCE(%rip), %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	symbol_CreateFunction
	jmp	.LBB1_36
.LBB1_8:
	testl	%r12d, %r12d
	jns	.LBB1_23
# BB#9:                                 # %symbol_IsFunction.exit
	movl	%r12d, %eax
	negl	%eax
	movl	symbol_TYPEMASK(%rip), %ecx
	movl	%ecx, %edx
	andl	%eax, %edx
	orl	$1, %edx
	cmpl	$1, %edx
	jne	.LBB1_23
# BB#10:
	cmpl	$294, %ebx              # imm = 0x126
	je	.LBB1_22
# BB#11:
	cmpl	$298, %ebx              # imm = 0x12A
	jne	.LBB1_12
	jmp	.LBB1_18
.LBB1_16:
	testl	%r12d, %r12d
	jns	.LBB1_23
# BB#17:                                # %.symbol_IsPredicate.exit_crit_edge
	movl	symbol_TYPEMASK(%rip), %ecx
	movl	%r12d, %eax
	negl	%eax
.LBB1_18:                               # %symbol_IsPredicate.exit
	movl	%ecx, %edx
	andl	%eax, %edx
	cmpl	$2, %edx
	jne	.LBB1_23
# BB#19:
	cmpl	$294, %ebx              # imm = 0x126
	je	.LBB1_22
.LBB1_12:
	cmpl	$300, %ebx              # imm = 0x12C
	jne	.LBB1_13
.LBB1_22:                               # %symbol_IsJunctor.exit
	andl	%ecx, %eax
	cmpl	$3, %eax
	jne	.LBB1_23
.LBB1_13:
	cmpl	$-2, %r13d
	je	.LBB1_38
# BB#14:
	movl	%r12d, %eax
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	%r13d, 16(%rax)
	je	.LBB1_38
# BB#15:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %ebx
	movl	%r12d, %edi
	callq	symbol_Arity
	movl	%eax, %ecx
	movl	$.L.str.222, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB1_34:
	movq	dfg_PRECEDENCE(%rip), %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	symbol_CreatePredicate
	jmp	.LBB1_36
.LBB1_35:
	movq	dfg_PRECEDENCE(%rip), %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	symbol_CreateJunctor
.LBB1_36:
	movl	%eax, %ebx
	cmpl	$-2, %r13d
	jne	.LBB1_38
# BB#37:
	movl	$12, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	%ebx, (%rbp)
	movl	$0, 4(%rbp)
	movl	$0, 8(%rbp)
	movq	dfg_SYMBOLLIST(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, dfg_SYMBOLLIST(%rip)
.LBB1_38:
	cmpl	$64, %r14d
	jb	.LBB1_40
# BB#39:
	movb	$32, 63(%r15)
.LBB1_40:
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	string_StringFree       # TAILCALL
.LBB1_23:                               # %symbol_IsFunction.exit.thread
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.217, %edi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	misc_UserErrorReport
	negl	%r12d
	andl	symbol_TYPEMASK(%rip), %r12d
	cmpl	$2, %r12d
	jb	.LBB1_27
# BB#24:                                # %symbol_IsFunction.exit.thread
	je	.LBB1_28
# BB#25:                                # %symbol_IsFunction.exit.thread
	cmpl	$3, %r12d
	jne	.LBB1_29
# BB#26:
	movl	$.L.str.220, %edi
	jmp	.LBB1_30
.LBB1_41:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.216, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB1_27:
	movl	$.L.str.218, %edi
	jmp	.LBB1_30
.LBB1_28:
	movl	$.L.str.219, %edi
	jmp	.LBB1_30
.LBB1_29:
	movl	$.L.str.221, %edi
.LBB1_30:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end1:
	.size	dfg_SymbolDecl, .Lfunc_end1-dfg_SymbolDecl
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_8
	.quad	.LBB1_13
	.quad	.LBB1_13
	.quad	.LBB1_13
	.quad	.LBB1_13
	.quad	.LBB1_20
	.quad	.LBB1_13
	.quad	.LBB1_16
	.quad	.LBB1_20

	.text
	.globl	dfg_CreateQuantifier
	.p2align	4, 0x90
	.type	dfg_CreateQuantifier,@function
dfg_CreateQuantifier:                   # @dfg_CreateQuantifier
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movl	%edi, 4(%rsp)           # 4-byte Spill
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#2:                                 # %.lr.ph111.preheader
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.LBB2_3
.LBB2_12:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, %r13
.LBB2_17:                               # %list_Nconc.exit99
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	testq	%r12, %r12
	je	.LBB2_18
# BB#19:                                #   in Loop: Header=BB2_3 Depth=1
	testq	%rax, %rax
	je	.LBB2_23
# BB#20:                                # %.preheader.i91.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB2_21:                               # %.preheader.i91
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_21
# BB#22:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB2_23
.LBB2_18:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, %r12
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_15 Depth 2
                                        #     Child Loop BB2_21 Depth 2
                                        #     Child Loop BB2_8 Depth 2
	movq	8(%rbx), %rbp
	movslq	(%rbp), %r14
	testq	%r14, %r14
	jle	.LBB2_11
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%r13, %r13
	je	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_3 Depth=1
	testq	%rax, %rax
	je	.LBB2_10
# BB#7:                                 # %.preheader.i79.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader.i79
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_8
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movslq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%r13, %r13
	je	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_3 Depth=1
	testq	%rax, %rax
	je	.LBB2_17
# BB#14:                                # %.preheader.i97.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader.i97
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_15
# BB#16:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, %r13
.LBB2_10:                               # %list_Nconc.exit81
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %rdi
	callq	term_Delete
.LBB2_23:                               # %list_Nconc.exit93
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB2_3
	jmp	.LBB2_24
.LBB2_1:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.LBB2_24:                               # %._crit_edge112
	movq	%r13, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB2_27
# BB#25:                                # %.lr.ph105.preheader
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_26
.LBB2_27:                               # %._crit_edge106
	testq	%r12, %r12
	je	.LBB2_28
# BB#29:
	movl	4(%rsp), %ebx           # 4-byte Reload
	cmpl	%ebx, fol_ALL(%rip)
	jne	.LBB2_41
# BB#30:
	movl	fol_OR(%rip), %eax
	cmpl	(%r15), %eax
	jne	.LBB2_38
# BB#31:                                # %.lr.ph.preheader
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB2_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_NOT(%rip), %r14d
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_32
# BB#33:
	movq	16(%r15), %rax
	testq	%rax, %rax
	movl	4(%rsp), %ebx           # 4-byte Reload
	je	.LBB2_37
# BB#34:                                # %.preheader.i85.preheader
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB2_35:                               # %.preheader.i85
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_35
# BB#36:
	movq	%rax, (%rcx)
.LBB2_37:                               # %list_Nconc.exit87
	movq	%r12, 16(%r15)
	jmp	.LBB2_55
.LBB2_28:
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB2_55
.LBB2_41:
	cmpl	%ebx, fol_EXIST(%rip)
	jne	.LBB2_55
# BB#42:
	movl	fol_AND(%rip), %eax
	cmpl	(%r15), %eax
	jne	.LBB2_48
# BB#43:
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_47
# BB#44:                                # %.preheader.i73.preheader
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB2_45:                               # %.preheader.i73
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_45
# BB#46:
	movq	%rax, (%rcx)
.LBB2_47:                               # %list_Nconc.exit75
	movq	%r12, 16(%r15)
	jmp	.LBB2_55
.LBB2_38:
	cmpq	$0, (%r12)
	je	.LBB2_39
# BB#40:
	movl	fol_AND(%rip), %edi
	movq	%r12, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	fol_IMPLIES(%rip), %r12d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	%rbp, (%rax)
	movl	%r12d, %edi
	movq	%rax, %rsi
	jmp	.LBB2_54
.LBB2_48:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB2_52
# BB#49:                                # %.preheader.i.preheader
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB2_50:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_50
# BB#51:
	movq	%rax, (%rcx)
.LBB2_52:                               # %list_Nconc.exit
	movl	fol_AND(%rip), %edi
	jmp	.LBB2_53
.LBB2_39:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%r12)
	movl	fol_IMPLIES(%rip), %edi
.LBB2_53:
	movq	%r12, %rsi
.LBB2_54:
	callq	term_Create
	movq	%rax, %r15
.LBB2_55:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%r13, %rsi
	movq	%rax, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fol_CreateQuantifier    # TAILCALL
.Lfunc_end2:
	.size	dfg_CreateQuantifier, .Lfunc_end2-dfg_CreateQuantifier
	.cfi_endproc

	.p2align	4, 0x90
	.type	dfg_Symbol,@function
dfg_Symbol:                             # @dfg_Symbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 48
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	strlen
	cmpl	$63, %eax
	jbe	.LBB3_1
# BB#2:
	movb	63(%rbx), %r15b
	movb	$0, 63(%rbx)
	movq	%rbx, %rdi
	callq	symbol_Lookup
	movl	%eax, %ebp
	movb	%r15b, 63(%rbx)
	testl	%ebp, %ebp
	jne	.LBB3_4
	jmp	.LBB3_14
.LBB3_1:
	movq	%rbx, %rdi
	callq	symbol_Lookup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB3_14
.LBB3_4:
	movq	%rbx, %rdi
	callq	string_StringFree
	movq	dfg_SYMBOLLIST(%rip), %rax
	testq	%rax, %rax
	jne	.LBB3_9
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_9 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_6
.LBB3_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rbx
	cmpl	%ebp, (%rbx)
	jne	.LBB3_8
# BB#10:
	cmpl	$0, 4(%rbx)
	je	.LBB3_13
# BB#11:
	cmpl	%r14d, 8(%rbx)
	je	.LBB3_29
# BB#12:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.224, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.225, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	misc_UserErrorReport
	movl	%ebp, %edi
	callq	symbol_Name
	movq	%rax, %rcx
	movl	$.L.str.226, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	misc_UserErrorReport
	movl	8(%rbx), %esi
	movl	$.L.str.227, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB3_6:                                # %._crit_edge.i
	movl	%ebp, %eax
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	%r14d, 16(%rax)
	je	.LBB3_29
# BB#7:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %r14d
	movl	%ebp, %edi
	callq	symbol_Name
	movq	%rax, %rbx
	movl	%ebp, %edi
	callq	symbol_Arity
	movl	%eax, %ecx
	movl	$.L.str.228, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	%rbx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB3_14:
	testl	%r14d, %r14d
	jne	.LBB3_15
# BB#17:
	movq	dfg_VARLIST(%rip), %r14
	testq	%r14, %r14
	jne	.LBB3_19
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_23:                               # %.critedge1.i
                                        #   in Loop: Header=BB3_19 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB3_24
.LBB3_19:                               # %.lr.ph43.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_21 Depth 2
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_21
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_21 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_23
.LBB3_21:                               # %.lr.ph.i19
                                        #   Parent Loop BB3_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_22
# BB#27:                                # %.critedge.i
	movq	%rbx, %rdi
	callq	string_StringFree
	movq	8(%rbp), %rcx
	addq	$8, %rcx
	jmp	.LBB3_28
.LBB3_24:                               # %.loopexit.i
	cmpb	$1, dfg_VARDECL(%rip)
	jne	.LBB3_26
# BB#25:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, (%rbp)
	movl	symbol_STANDARDVARCOUNTER(%rip), %eax
	incl	%eax
	movl	%eax, symbol_STANDARDVARCOUNTER(%rip)
	movl	%eax, 8(%rbp)
	movq	dfg_VARLIST(%rip), %rbx
	movq	8(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	leaq	8(%rbp), %rcx
	movq	%r14, (%rax)
	movq	%rax, 8(%rbx)
.LBB3_28:                               # %dfg_VarLookup.exit
	movl	(%rcx), %ebp
.LBB3_29:                               # %dfg_SymCheck.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_13:
	movl	%r14d, 8(%rbx)
	movl	$1, 4(%rbx)
	jmp	.LBB3_29
.LBB3_15:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.223, %edi
	jmp	.LBB3_16
.LBB3_26:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.229, %edi
.LBB3_16:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end3:
	.size	dfg_Symbol, .Lfunc_end3-dfg_Symbol
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	misc_Error, .Lfunc_end4-misc_Error
	.cfi_endproc

	.globl	dfg_error
	.p2align	4, 0x90
	.type	dfg_error,@function
dfg_error:                              # @dfg_error
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end5:
	.size	dfg_error, .Lfunc_end5-dfg_error
	.cfi_endproc

	.globl	dfg_Free
	.p2align	4, 0x90
	.type	dfg_Free,@function
dfg_Free:                               # @dfg_Free
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 16
	movq	dfg_DESC.0(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	callq	string_StringFree
.LBB6_2:
	movq	dfg_DESC.1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:
	callq	string_StringFree
.LBB6_4:
	movq	dfg_DESC.2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_6
# BB#5:
	callq	string_StringFree
.LBB6_6:
	movq	dfg_DESC.3(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#7:
	callq	string_StringFree
.LBB6_8:
	movq	dfg_DESC.5(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#9:
	callq	string_StringFree
.LBB6_10:
	movq	dfg_DESC.6(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_11
# BB#12:
	popq	%rax
	jmp	string_StringFree       # TAILCALL
.LBB6_11:
	popq	%rax
	retq
.Lfunc_end6:
	.size	dfg_Free, .Lfunc_end6-dfg_Free
	.cfi_endproc

	.globl	dfg_ProblemName
	.p2align	4, 0x90
	.type	dfg_ProblemName,@function
dfg_ProblemName:                        # @dfg_ProblemName
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.0(%rip), %rax
	retq
.Lfunc_end7:
	.size	dfg_ProblemName, .Lfunc_end7-dfg_ProblemName
	.cfi_endproc

	.globl	dfg_ProblemAuthor
	.p2align	4, 0x90
	.type	dfg_ProblemAuthor,@function
dfg_ProblemAuthor:                      # @dfg_ProblemAuthor
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.1(%rip), %rax
	retq
.Lfunc_end8:
	.size	dfg_ProblemAuthor, .Lfunc_end8-dfg_ProblemAuthor
	.cfi_endproc

	.globl	dfg_ProblemVersion
	.p2align	4, 0x90
	.type	dfg_ProblemVersion,@function
dfg_ProblemVersion:                     # @dfg_ProblemVersion
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.2(%rip), %rax
	retq
.Lfunc_end9:
	.size	dfg_ProblemVersion, .Lfunc_end9-dfg_ProblemVersion
	.cfi_endproc

	.globl	dfg_ProblemLogic
	.p2align	4, 0x90
	.type	dfg_ProblemLogic,@function
dfg_ProblemLogic:                       # @dfg_ProblemLogic
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.3(%rip), %rax
	retq
.Lfunc_end10:
	.size	dfg_ProblemLogic, .Lfunc_end10-dfg_ProblemLogic
	.cfi_endproc

	.globl	dfg_ProblemStatus
	.p2align	4, 0x90
	.type	dfg_ProblemStatus,@function
dfg_ProblemStatus:                      # @dfg_ProblemStatus
	.cfi_startproc
# BB#0:
	movl	dfg_DESC.4(%rip), %eax
	retq
.Lfunc_end11:
	.size	dfg_ProblemStatus, .Lfunc_end11-dfg_ProblemStatus
	.cfi_endproc

	.globl	dfg_ProblemStatusString
	.p2align	4, 0x90
	.type	dfg_ProblemStatusString,@function
dfg_ProblemStatusString:                # @dfg_ProblemStatusString
	.cfi_startproc
# BB#0:
	movslq	dfg_DESC.4(%rip), %rax
	cmpq	$3, %rax
	jae	.LBB12_2
# BB#1:                                 # %switch.lookup
	movq	.Lswitch.table(,%rax,8), %rax
	retq
.LBB12_2:
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	movl	$.L.str.28, %edx
	movl	$1025, %ecx             # imm = 0x401
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end12:
	.size	dfg_ProblemStatusString, .Lfunc_end12-dfg_ProblemStatusString
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.215, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end13:
	.size	misc_DumpCore, .Lfunc_end13-misc_DumpCore
	.cfi_endproc

	.globl	dfg_ProblemDescription
	.p2align	4, 0x90
	.type	dfg_ProblemDescription,@function
dfg_ProblemDescription:                 # @dfg_ProblemDescription
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.5(%rip), %rax
	retq
.Lfunc_end14:
	.size	dfg_ProblemDescription, .Lfunc_end14-dfg_ProblemDescription
	.cfi_endproc

	.globl	dfg_ProblemDate
	.p2align	4, 0x90
	.type	dfg_ProblemDate,@function
dfg_ProblemDate:                        # @dfg_ProblemDate
	.cfi_startproc
# BB#0:
	movq	dfg_DESC.6(%rip), %rax
	retq
.Lfunc_end15:
	.size	dfg_ProblemDate, .Lfunc_end15-dfg_ProblemDate
	.cfi_endproc

	.globl	dfg_FPrintDescription
	.p2align	4, 0x90
	.type	dfg_FPrintDescription,@function
dfg_FPrintDescription:                  # @dfg_FPrintDescription
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.31, %edi
	movl	$29, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.0(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB16_2
# BB#1:
	movq	%rbx, %rsi
	callq	fputs
	jmp	.LBB16_3
.LBB16_2:
	movl	$.L.str.32, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_3:
	movl	$.L.str.33, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB16_5
# BB#4:
	movq	%rbx, %rsi
	callq	fputs
	jmp	.LBB16_6
.LBB16_5:
	movl	$.L.str.32, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_6:
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpq	$0, dfg_DESC.2(%rip)
	je	.LBB16_8
# BB#7:
	movl	$.L.str.35, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.2(%rip), %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_8:
	cmpq	$0, dfg_DESC.3(%rip)
	je	.LBB16_10
# BB#9:
	movl	$.L.str.36, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.3(%rip), %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_10:
	movl	$.L.str.37, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movslq	dfg_DESC.4(%rip), %rax
	cmpq	$3, %rax
	jae	.LBB16_17
# BB#11:                                # %dfg_ProblemStatusString.exit
	movq	.Lswitch.table(,%rax,8), %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	$.L.str.38, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.5(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB16_13
# BB#12:
	movq	%rbx, %rsi
	callq	fputs
	jmp	.LBB16_14
.LBB16_13:
	movl	$.L.str.32, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_14:
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpq	$0, dfg_DESC.6(%rip)
	je	.LBB16_16
# BB#15:
	movl	$.L.str.39, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	dfg_DESC.6(%rip), %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB16_16:
	movl	$.L.str.40, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.LBB16_17:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	movl	$.L.str.28, %edx
	movl	$1025, %ecx             # imm = 0x401
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end16:
	.size	dfg_FPrintDescription, .Lfunc_end16-dfg_FPrintDescription
	.cfi_endproc

	.globl	dfg_DFGParser
	.p2align	4, 0x90
	.type	dfg_DFGParser,@function
dfg_DFGParser:                          # @dfg_DFGParser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 80
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, dfg_in(%rip)
	movl	$1, dfg_LINENUMBER(%rip)
	movl	$1, dfg_IGNORETEXT(%rip)
	movq	$0, dfg_AXIOMLIST(%rip)
	movq	$0, dfg_CONJECLIST(%rip)
	movq	$0, dfg_SORTDECLLIST(%rip)
	movq	$0, dfg_USERPRECEDENCE(%rip)
	movq	$0, dfg_AXCLAUSES(%rip)
	movq	$0, dfg_CONCLAUSES(%rip)
	movq	$0, dfg_PROOFLIST(%rip)
	movq	$0, dfg_TERMLIST(%rip)
	movq	$0, dfg_SYMBOLLIST(%rip)
	movq	$0, dfg_VARLIST(%rip)
	movb	$0, dfg_VARDECL(%rip)
	movl	$0, dfg_IGNORE(%rip)
	movq	%r15, dfg_FLAGS(%rip)
	movq	%r13, dfg_PRECEDENCE(%rip)
	movq	$0, dfg_DESC.0(%rip)
	movq	$0, dfg_DESC.1(%rip)
	movq	$0, dfg_DESC.2(%rip)
	movq	$0, dfg_DESC.3(%rip)
	movl	$2, dfg_DESC.4(%rip)
	movq	$0, dfg_DESC.5(%rip)
	movq	$0, dfg_DESC.6(%rip)
	callq	dfg_parse
	movq	dfg_SYMBOLLIST(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_5
# BB#1:                                 # %.lr.ph.i
	movl	symbol_TYPESTATBITS(%rip), %ecx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rax
	movl	8(%rax), %edx
	xorl	%esi, %esi
	subl	(%rax), %esi
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rdi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	cmpl	16(%rsi), %edx
	je	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	movl	%edx, 16(%rsi)
.LBB17_4:                               #   in Loop: Header=BB17_2 Depth=1
	movq	memory_ARRAY+96(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+96(%rip), %rdx
	movq	%rax, (%rdx)
	movq	dfg_SYMBOLLIST(%rip), %rdx
	movq	(%rdx), %rax
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	%rax, dfg_SYMBOLLIST(%rip)
	testq	%rax, %rax
	jne	.LBB17_2
.LBB17_5:                               # %dfg_SymCleanUp.exit.preheader
	movq	dfg_AXCLAUSES(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB17_6
	.p2align	4, 0x90
.LBB17_7:                               # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	(%rbx), %rdi
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	dfg_CreateClauseFromTerm
	movq	%rax, 8(%rbp)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_7 Depth=1
	callq	string_StringFree
.LBB17_9:                               # %dfg_SymCleanUp.exit
                                        #   in Loop: Header=BB17_7 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB17_7
# BB#10:                                # %dfg_SymCleanUp.exit._crit_edge.loopexit
	movq	dfg_AXCLAUSES(%rip), %rdi
	jmp	.LBB17_11
.LBB17_6:
	xorl	%edi, %edi
.LBB17_11:                              # %dfg_SymCleanUp.exit._crit_edge
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, dfg_AXCLAUSES(%rip)
	movq	dfg_CONCLAUSES(%rip), %r14
	testq	%r14, %r14
	je	.LBB17_16
	.p2align	4, 0x90
.LBB17_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rbx
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	dfg_CreateClauseFromTerm
	movq	%rax, 8(%r14)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_14
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	callq	string_StringFree
.LBB17_14:                              #   in Loop: Header=BB17_12 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB17_12
# BB#15:                                # %._crit_edge.loopexit
	movq	dfg_CONCLAUSES(%rip), %rbp
.LBB17_16:                              # %._crit_edge
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, dfg_CONCLAUSES(%rip)
	movq	dfg_PROOFLIST(%rip), %rdi
	callq	dfg_DeleteProofList
	movq	dfg_TERMLIST(%rip), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	dfg_AXCLAUSES(%rip), %rbx
	movq	dfg_CONCLAUSES(%rip), %rax
	testq	%rbx, %rbx
	je	.LBB17_17
# BB#18:
	testq	%rax, %rax
	je	.LBB17_19
# BB#20:                                # %.preheader.i69.preheader
	movq	%rbx, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB17_21:                              # %.preheader.i69
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB17_21
# BB#22:
	movq	%rax, (%rcx)
	jmp	.LBB17_23
.LBB17_17:
	movq	%rax, %rbx
.LBB17_19:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB17_23:                              # %list_Nconc.exit71
	movq	(%rbp), %rax
	movq	dfg_AXIOMLIST(%rip), %rcx
	testq	%rax, %rax
	je	.LBB17_24
# BB#25:
	testq	%rcx, %rcx
	je	.LBB17_29
# BB#26:                                # %.preheader.i63.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB17_27:                              # %.preheader.i63
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB17_27
# BB#28:
	movq	%rcx, (%rdx)
	jmp	.LBB17_29
.LBB17_24:
	movq	%rcx, %rax
.LBB17_29:                              # %list_Nconc.exit65
	movq	%rax, (%rbp)
	movq	(%rdi), %rax
	movq	dfg_CONJECLIST(%rip), %rcx
	testq	%rax, %rax
	je	.LBB17_30
# BB#31:
	testq	%rcx, %rcx
	je	.LBB17_35
# BB#32:                                # %.preheader.i57.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB17_33:                              # %.preheader.i57
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB17_33
# BB#34:
	movq	%rcx, (%rdx)
	jmp	.LBB17_35
.LBB17_30:
	movq	%rcx, %rax
.LBB17_35:                              # %list_Nconc.exit59
	movq	80(%rsp), %rbp
	movq	%rax, (%rdi)
	movq	(%r12), %rax
	movq	dfg_SORTDECLLIST(%rip), %rcx
	testq	%rax, %rax
	je	.LBB17_36
# BB#37:
	testq	%rcx, %rcx
	je	.LBB17_41
# BB#38:                                # %.preheader.i51.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB17_39:                              # %.preheader.i51
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB17_39
# BB#40:
	movq	%rcx, (%rdx)
	jmp	.LBB17_41
.LBB17_36:
	movq	%rcx, %rax
.LBB17_41:                              # %list_Nconc.exit53
	movq	%rax, (%r12)
	movq	dfg_USERPRECEDENCE(%rip), %rdi
	callq	list_NReverse
	movq	(%rbp), %rax
	movq	dfg_USERPRECEDENCE(%rip), %rcx
	testq	%rax, %rax
	je	.LBB17_42
# BB#43:
	testq	%rcx, %rcx
	je	.LBB17_47
# BB#44:                                # %.preheader.i.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB17_45:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB17_45
# BB#46:
	movq	%rcx, (%rdx)
	jmp	.LBB17_47
.LBB17_42:
	movq	%rcx, %rax
.LBB17_47:                              # %list_Nconc.exit
	movq	%rax, (%rbp)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	dfg_DFGParser, .Lfunc_end17-dfg_DFGParser
	.cfi_endproc

	.globl	dfg_CreateClauseFromTerm
	.p2align	4, 0x90
	.type	dfg_CreateClauseFromTerm,@function
dfg_CreateClauseFromTerm:               # @dfg_CreateClauseFromTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 64
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %r12d
	movl	(%rdi), %eax
	movq	16(%rdi), %rbp
	cmpl	fol_ALL(%rip), %eax
	jne	.LBB18_1
# BB#2:
	movq	(%rbp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	addq	$16, %rax
	jmp	.LBB18_3
.LBB18_1:
	leaq	16(%rdi), %rax
.LBB18_3:
	movq	$0, (%rax)
	callq	term_Delete
	testq	%rbp, %rbp
	je	.LBB18_15
# BB#4:                                 # %.lr.ph
	movl	symbol_TYPEMASK(%rip), %r13d
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB18_5:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB18_11
# BB#6:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB18_5 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r13d, %ecx
	cmpl	$2, %ecx
	jne	.LBB18_11
# BB#7:                                 #   in Loop: Header=BB18_5 Depth=1
	cmpl	%eax, fol_TRUE(%rip)
	je	.LBB18_8
# BB#9:                                 #   in Loop: Header=BB18_5 Depth=1
	cmpl	%eax, fol_FALSE(%rip)
	jne	.LBB18_14
# BB#10:                                #   in Loop: Header=BB18_5 Depth=1
	callq	term_Delete
	movq	$0, 8(%rbx)
	jmp	.LBB18_14
	.p2align	4, 0x90
.LBB18_11:                              # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB18_5 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	cmpl	%eax, fol_FALSE(%rip)
	je	.LBB18_8
# BB#12:                                #   in Loop: Header=BB18_5 Depth=1
	cmpl	%eax, fol_TRUE(%rip)
	jne	.LBB18_14
# BB#13:                                #   in Loop: Header=BB18_5 Depth=1
	callq	term_Delete
	movq	$0, 8(%rbp)
.LBB18_14:                              #   in Loop: Header=BB18_5 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB18_5
.LBB18_15:                              # %._crit_edge
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %rbx
	xorl	%edx, %edx
	testl	%r12d, %r12d
	sete	%dl
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %r8
	movq	%r14, %r9
	callq	clause_CreateFromLiterals
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB18_17
	.p2align	4, 0x90
.LBB18_16:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB18_16
	jmp	.LBB18_17
.LBB18_8:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	list_PointerDeleteElement
	movl	$term_Delete, %esi
	movq	%rbp, %rdi
	callq	list_DeleteWithElement
.LBB18_17:                              # %list_Delete.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	dfg_CreateClauseFromTerm, .Lfunc_end18-dfg_CreateClauseFromTerm
	.cfi_endproc

	.globl	dfg_DeleteProofList
	.p2align	4, 0x90
	.type	dfg_DeleteProofList,@function
dfg_DeleteProofList:                    # @dfg_DeleteProofList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB19_4
	.p2align	4, 0x90
.LBB19_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_2 Depth 2
	movq	8(%r14), %rbx
	movq	8(%rbx), %rdi
	callq	string_StringFree
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movl	$string_StringFree, %esi
	callq	list_DeleteWithElement
	testq	%rbx, %rbx
	je	.LBB19_3
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph.i
                                        #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB19_2
.LBB19_3:                               # %list_Delete.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB19_1
.LBB19_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	dfg_DeleteProofList, .Lfunc_end19-dfg_DeleteProofList
	.cfi_endproc

	.globl	dfg_ProofParser
	.p2align	4, 0x90
	.type	dfg_ProofParser,@function
dfg_ProofParser:                        # @dfg_ProofParser
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 48
.Lcfi87:
	.cfi_offset %rbx, -48
.Lcfi88:
	.cfi_offset %r12, -40
.Lcfi89:
	.cfi_offset %r13, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rdi, dfg_in(%rip)
	movl	$1, dfg_LINENUMBER(%rip)
	movl	$1, dfg_IGNORETEXT(%rip)
	movq	$0, dfg_AXIOMLIST(%rip)
	movq	$0, dfg_CONJECLIST(%rip)
	movq	$0, dfg_SORTDECLLIST(%rip)
	movq	$0, dfg_USERPRECEDENCE(%rip)
	movq	$0, dfg_AXCLAUSES(%rip)
	movq	$0, dfg_CONCLAUSES(%rip)
	movq	$0, dfg_PROOFLIST(%rip)
	movq	$0, dfg_TERMLIST(%rip)
	movq	$0, dfg_SYMBOLLIST(%rip)
	movq	$0, dfg_VARLIST(%rip)
	movb	$0, dfg_VARDECL(%rip)
	movl	$0, dfg_IGNORE(%rip)
	movq	%rsi, dfg_FLAGS(%rip)
	movq	%rdx, dfg_PRECEDENCE(%rip)
	movq	$0, dfg_DESC.0(%rip)
	movq	$0, dfg_DESC.1(%rip)
	movq	$0, dfg_DESC.2(%rip)
	movq	$0, dfg_DESC.3(%rip)
	movl	$2, dfg_DESC.4(%rip)
	movq	$0, dfg_DESC.5(%rip)
	movq	$0, dfg_DESC.6(%rip)
	callq	dfg_parse
	movq	dfg_SYMBOLLIST(%rip), %rax
	testq	%rax, %rax
	je	.LBB20_5
# BB#1:                                 # %.lr.ph.i22
	movl	symbol_TYPESTATBITS(%rip), %ecx
	.p2align	4, 0x90
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rax
	movl	8(%rax), %edx
	xorl	%esi, %esi
	subl	(%rax), %esi
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rdi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	cmpl	16(%rsi), %edx
	je	.LBB20_4
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	movl	%edx, 16(%rsi)
.LBB20_4:                               #   in Loop: Header=BB20_2 Depth=1
	movq	memory_ARRAY+96(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+96(%rip), %rdx
	movq	%rax, (%rdx)
	movq	dfg_SYMBOLLIST(%rip), %rdx
	movq	(%rdx), %rax
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	%rax, dfg_SYMBOLLIST(%rip)
	testq	%rax, %rax
	jne	.LBB20_2
.LBB20_5:                               # %dfg_SymCleanUp.exit
	movq	dfg_AXCLAUSES(%rip), %r12
	movq	dfg_CONCLAUSES(%rip), %rax
	testq	%r12, %r12
	je	.LBB20_6
# BB#7:
	testq	%rax, %rax
	je	.LBB20_13
# BB#8:
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB20_9:                               # %.preheader.i26
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB20_9
# BB#10:
	movq	%rax, (%rdx)
	jmp	.LBB20_11
.LBB20_6:
	movq	%rax, %r12
.LBB20_11:                              # %list_Nconc.exit28
	movq	%r12, dfg_AXCLAUSES(%rip)
	movq	$0, dfg_CONCLAUSES(%rip)
	testq	%r12, %r12
	jne	.LBB20_14
# BB#12:
	xorl	%edi, %edi
	jmp	.LBB20_19
.LBB20_13:                              # %list_Nconc.exit28.thread
	movq	$0, dfg_CONCLAUSES(%rip)
	.p2align	4, 0x90
.LBB20_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %r13
	movq	(%r13), %r14
	cmpq	$0, 8(%r13)
	je	.LBB20_15
# BB#16:                                #   in Loop: Header=BB20_14 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$16, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	$0, 8(%r15)
	movq	%rbx, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%r15, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r13)
	jmp	.LBB20_17
	.p2align	4, 0x90
.LBB20_15:                              #   in Loop: Header=BB20_14 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r13, (%rax)
	movq	$0, 8(%r12)
.LBB20_17:                              #   in Loop: Header=BB20_14 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB20_14
# BB#18:                                # %._crit_edge.loopexit
	movq	dfg_AXCLAUSES(%rip), %rdi
.LBB20_19:                              # %._crit_edge
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, dfg_AXCLAUSES(%rip)
	movq	dfg_AXIOMLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB20_23
	.p2align	4, 0x90
.LBB20_20:                              # %.lr.ph.i45
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB20_22
# BB#21:                                #   in Loop: Header=BB20_20 Depth=1
	callq	string_StringFree
.LBB20_22:                              #   in Loop: Header=BB20_20 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB20_20
.LBB20_23:                              # %dfg_DeleteFormulaPairList.exit48
	movq	dfg_CONJECLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB20_27
	.p2align	4, 0x90
.LBB20_24:                              # %.lr.ph.i35
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB20_26
# BB#25:                                #   in Loop: Header=BB20_24 Depth=1
	callq	string_StringFree
.LBB20_26:                              #   in Loop: Header=BB20_24 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB20_24
.LBB20_27:                              # %dfg_DeleteFormulaPairList.exit38
	movq	dfg_SORTDECLLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB20_31
	.p2align	4, 0x90
.LBB20_28:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB20_30
# BB#29:                                #   in Loop: Header=BB20_28 Depth=1
	callq	string_StringFree
.LBB20_30:                              #   in Loop: Header=BB20_28 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB20_28
.LBB20_31:                              # %dfg_DeleteFormulaPairList.exit
	movq	dfg_TERMLIST(%rip), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	dfg_PROOFLIST(%rip), %rdi
	callq	list_NReverse
	movq	%rax, %rcx
	movq	%rcx, dfg_PROOFLIST(%rip)
	movq	dfg_AXCLAUSES(%rip), %rax
	testq	%rax, %rax
	je	.LBB20_32
# BB#33:
	testq	%rcx, %rcx
	je	.LBB20_37
# BB#34:                                # %.preheader.i.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB20_35:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB20_35
# BB#36:
	movq	%rcx, (%rdx)
	jmp	.LBB20_37
.LBB20_32:
	movq	%rcx, %rax
.LBB20_37:                              # %list_Nconc.exit
	movq	%rax, dfg_AXCLAUSES(%rip)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	dfg_ProofParser, .Lfunc_end20-dfg_ProofParser
	.cfi_endproc

	.globl	dfg_DeleteFormulaPairList
	.p2align	4, 0x90
	.type	dfg_DeleteFormulaPairList,@function
dfg_DeleteFormulaPairList:              # @dfg_DeleteFormulaPairList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB21_4
	.p2align	4, 0x90
.LBB21_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB21_3
# BB#2:                                 #   in Loop: Header=BB21_1 Depth=1
	callq	string_StringFree
.LBB21_3:                               #   in Loop: Header=BB21_1 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB21_1
.LBB21_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	dfg_DeleteFormulaPairList, .Lfunc_end21-dfg_DeleteFormulaPairList
	.cfi_endproc

	.globl	dfg_TermParser
	.p2align	4, 0x90
	.type	dfg_TermParser,@function
dfg_TermParser:                         # @dfg_TermParser
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, dfg_in(%rip)
	movl	$1, dfg_LINENUMBER(%rip)
	movl	$1, dfg_IGNORETEXT(%rip)
	movq	$0, dfg_AXIOMLIST(%rip)
	movq	$0, dfg_CONJECLIST(%rip)
	movq	$0, dfg_SORTDECLLIST(%rip)
	movq	$0, dfg_USERPRECEDENCE(%rip)
	movq	$0, dfg_AXCLAUSES(%rip)
	movq	$0, dfg_CONCLAUSES(%rip)
	movq	$0, dfg_PROOFLIST(%rip)
	movq	$0, dfg_TERMLIST(%rip)
	movq	$0, dfg_SYMBOLLIST(%rip)
	movq	$0, dfg_VARLIST(%rip)
	movb	$0, dfg_VARDECL(%rip)
	movl	$0, dfg_IGNORE(%rip)
	movq	%rsi, dfg_FLAGS(%rip)
	movq	%rdx, dfg_PRECEDENCE(%rip)
	movq	$0, dfg_DESC.0(%rip)
	movq	$0, dfg_DESC.1(%rip)
	movq	$0, dfg_DESC.2(%rip)
	movq	$0, dfg_DESC.3(%rip)
	movl	$2, dfg_DESC.4(%rip)
	movq	$0, dfg_DESC.5(%rip)
	movq	$0, dfg_DESC.6(%rip)
	callq	dfg_parse
	movq	dfg_SYMBOLLIST(%rip), %rax
	testq	%rax, %rax
	je	.LBB22_5
# BB#1:                                 # %.lr.ph.i4
	movl	symbol_TYPESTATBITS(%rip), %ecx
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rax
	movl	8(%rax), %edx
	xorl	%esi, %esi
	subl	(%rax), %esi
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rdi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	cmpl	16(%rsi), %edx
	je	.LBB22_4
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	movl	%edx, 16(%rsi)
.LBB22_4:                               #   in Loop: Header=BB22_2 Depth=1
	movq	memory_ARRAY+96(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+96(%rip), %rdx
	movq	%rax, (%rdx)
	movq	dfg_SYMBOLLIST(%rip), %rdx
	movq	(%rdx), %rax
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	%rax, dfg_SYMBOLLIST(%rip)
	testq	%rax, %rax
	jne	.LBB22_2
.LBB22_5:                               # %dfg_SymCleanUp.exit
	movq	dfg_AXCLAUSES(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB22_9
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph.i11
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB22_8
# BB#7:                                 #   in Loop: Header=BB22_6 Depth=1
	callq	string_StringFree
.LBB22_8:                               #   in Loop: Header=BB22_6 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB22_6
.LBB22_9:                               # %dfg_DeleteFormulaPairList.exit14
	movq	dfg_CONCLAUSES(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB22_13
	.p2align	4, 0x90
.LBB22_10:                              # %.lr.ph.i21
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB22_12
# BB#11:                                #   in Loop: Header=BB22_10 Depth=1
	callq	string_StringFree
.LBB22_12:                              #   in Loop: Header=BB22_10 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB22_10
.LBB22_13:                              # %dfg_DeleteFormulaPairList.exit24
	movq	dfg_AXIOMLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB22_17
	.p2align	4, 0x90
.LBB22_14:                              # %.lr.ph.i31
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB22_16
# BB#15:                                #   in Loop: Header=BB22_14 Depth=1
	callq	string_StringFree
.LBB22_16:                              #   in Loop: Header=BB22_14 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB22_14
.LBB22_17:                              # %dfg_DeleteFormulaPairList.exit34
	movq	dfg_CONJECLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB22_21
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB22_20
# BB#19:                                #   in Loop: Header=BB22_18 Depth=1
	callq	string_StringFree
.LBB22_20:                              #   in Loop: Header=BB22_18 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB22_18
.LBB22_21:                              # %dfg_DeleteFormulaPairList.exit44
	movq	dfg_PROOFLIST(%rip), %rdi
	callq	dfg_DeleteProofList
	movq	dfg_SORTDECLLIST(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB22_25
	.p2align	4, 0x90
.LBB22_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	callq	term_Delete
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB22_24
# BB#23:                                #   in Loop: Header=BB22_22 Depth=1
	callq	string_StringFree
.LBB22_24:                              #   in Loop: Header=BB22_22 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB22_22
.LBB22_25:                              # %dfg_DeleteFormulaPairList.exit
	movq	dfg_TERMLIST(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	dfg_TermParser, .Lfunc_end22-dfg_TermParser
	.cfi_endproc

	.globl	dfg_StripLabelsFromList
	.p2align	4, 0x90
	.type	dfg_StripLabelsFromList,@function
dfg_StripLabelsFromList:                # @dfg_StripLabelsFromList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -24
.Lcfi106:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB23_2
	jmp	.LBB23_5
	.p2align	4, 0x90
.LBB23_4:                               #   in Loop: Header=BB23_2 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB23_5
.LBB23_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	(%r14), %rax
	movq	%rax, 8(%rbx)
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	callq	string_StringFree
	jmp	.LBB23_4
.LBB23_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	dfg_StripLabelsFromList, .Lfunc_end23-dfg_StripLabelsFromList
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_Arity,@function
symbol_Arity:                           # @symbol_Arity
	.cfi_startproc
# BB#0:
	negl	%edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	16(%rax), %eax
	retq
.Lfunc_end24:
	.size	symbol_Arity, .Lfunc_end24-symbol_Arity
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_Name,@function
symbol_Name:                            # @symbol_Name
	.cfi_startproc
# BB#0:
	negl	%edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	retq
.Lfunc_end25:
	.size	symbol_Name, .Lfunc_end25-symbol_Name
	.cfi_endproc

	.p2align	4, 0x90
	.type	dfg_VarFree,@function
dfg_VarFree:                            # @dfg_VarFree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 16
.Lcfi108:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	string_StringFree
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end26:
	.size	dfg_VarFree, .Lfunc_end26-dfg_VarFree
	.cfi_endproc

	.type	dfg_nerrs,@object       # @dfg_nerrs
	.comm	dfg_nerrs,4,4
	.type	dfg_char,@object        # @dfg_char
	.comm	dfg_char,4,4
	.type	yypact,@object          # @yypact
	.section	.rodata,"a",@progbits
	.p2align	4
yypact:
	.short	9                       # 0x9
	.short	65504                   # 0xffe0
	.short	35                      # 0x23
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65530                   # 0xfffa
	.short	13                      # 0xd
	.short	67                      # 0x43
	.short	20                      # 0x14
	.short	45                      # 0x2d
	.short	53                      # 0x35
	.short	30                      # 0x1e
	.short	65180                   # 0xfe9c
	.short	110                     # 0x6e
	.short	46                      # 0x2e
	.short	118                     # 0x76
	.short	121                     # 0x79
	.short	65524                   # 0xfff4
	.short	73                      # 0x49
	.short	65180                   # 0xfe9c
	.short	91                      # 0x5b
	.short	84                      # 0x54
	.short	113                     # 0x71
	.short	112                     # 0x70
	.short	141                     # 0x8d
	.short	123                     # 0x7b
	.short	128                     # 0x80
	.short	132                     # 0x84
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	175                     # 0xaf
	.short	152                     # 0x98
	.short	161                     # 0xa1
	.short	155                     # 0x9b
	.short	191                     # 0xbf
	.short	2                       # 0x2
	.short	162                     # 0xa2
	.short	180                     # 0xb4
	.short	65180                   # 0xfe9c
	.short	204                     # 0xcc
	.short	232                     # 0xe8
	.short	214                     # 0xd6
	.short	173                     # 0xad
	.short	65180                   # 0xfe9c
	.short	252                     # 0xfc
	.short	176                     # 0xb0
	.short	206                     # 0xce
	.short	209                     # 0xd1
	.short	213                     # 0xd5
	.short	226                     # 0xe2
	.short	232                     # 0xe8
	.short	47                      # 0x2f
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	80                      # 0x50
	.short	218                     # 0xda
	.short	254                     # 0xfe
	.short	224                     # 0xe0
	.short	65522                   # 0xfff2
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	230                     # 0xe6
	.short	233                     # 0xe9
	.short	65180                   # 0xfe9c
	.short	234                     # 0xea
	.short	241                     # 0xf1
	.short	232                     # 0xe8
	.short	242                     # 0xf2
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	243                     # 0xf3
	.short	237                     # 0xed
	.short	21                      # 0x15
	.short	244                     # 0xf4
	.short	65180                   # 0xfe9c
	.short	260                     # 0x104
	.short	65180                   # 0xfe9c
	.short	246                     # 0xf6
	.short	245                     # 0xf5
	.short	250                     # 0xfa
	.short	251                     # 0xfb
	.short	294                     # 0x126
	.short	247                     # 0xf7
	.short	248                     # 0xf8
	.short	2                       # 0x2
	.short	232                     # 0xe8
	.short	93                      # 0x5d
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	232                     # 0xe8
	.short	255                     # 0xff
	.short	272                     # 0x110
	.short	232                     # 0xe8
	.short	253                     # 0xfd
	.short	65180                   # 0xfe9c
	.short	256                     # 0x100
	.short	65180                   # 0xfe9c
	.short	232                     # 0xe8
	.short	257                     # 0x101
	.short	232                     # 0xe8
	.short	290                     # 0x122
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	258                     # 0x102
	.short	21                      # 0x15
	.short	261                     # 0x105
	.short	65180                   # 0xfe9c
	.short	271                     # 0x10f
	.short	65180                   # 0xfe9c
	.short	262                     # 0x106
	.short	264                     # 0x108
	.short	14                      # 0xe
	.short	263                     # 0x107
	.short	317                     # 0x13d
	.short	108                     # 0x6c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	265                     # 0x109
	.short	266                     # 0x10a
	.short	80                      # 0x50
	.short	119                     # 0x77
	.short	65180                   # 0xfe9c
	.short	85                      # 0x55
	.short	268                     # 0x10c
	.short	312                     # 0x138
	.short	65180                   # 0xfe9c
	.short	124                     # 0x7c
	.short	65180                   # 0xfe9c
	.short	270                     # 0x10e
	.short	273                     # 0x111
	.short	269                     # 0x10d
	.short	65180                   # 0xfe9c
	.short	274                     # 0x112
	.short	65180                   # 0xfe9c
	.short	309                     # 0x135
	.short	275                     # 0x113
	.short	65180                   # 0xfe9c
	.short	65484                   # 0xffcc
	.short	276                     # 0x114
	.short	277                     # 0x115
	.short	232                     # 0xe8
	.short	279                     # 0x117
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	281                     # 0x119
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	284                     # 0x11c
	.short	287                     # 0x11f
	.short	288                     # 0x120
	.short	321                     # 0x141
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	286                     # 0x11e
	.short	108                     # 0x6c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	289                     # 0x121
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	138                     # 0x8a
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	156                     # 0x9c
	.short	291                     # 0x123
	.short	293                     # 0x125
	.short	232                     # 0xe8
	.short	65519                   # 0xffef
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	346                     # 0x15a
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	40                      # 0x28
	.short	296                     # 0x128
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	297                     # 0x129
	.short	299                     # 0x12b
	.short	302                     # 0x12e
	.short	300                     # 0x12c
	.short	65180                   # 0xfe9c
	.short	303                     # 0x12f
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	285                     # 0x11d
	.short	301                     # 0x12d
	.short	85                      # 0x55
	.short	232                     # 0xe8
	.short	143                     # 0x8f
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	337                     # 0x151
	.short	16                      # 0x10
	.short	304                     # 0x130
	.short	298                     # 0x12a
	.short	306                     # 0x132
	.short	65180                   # 0xfe9c
	.short	32                      # 0x20
	.short	65180                   # 0xfe9c
	.short	311                     # 0x137
	.short	305                     # 0x131
	.short	65180                   # 0xfe9c
	.short	56                      # 0x38
	.short	308                     # 0x134
	.short	314                     # 0x13a
	.short	310                     # 0x136
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	315                     # 0x13b
	.short	318                     # 0x13e
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	108                     # 0x6c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	313                     # 0x139
	.short	319                     # 0x13f
	.short	156                     # 0x9c
	.short	65534                   # 0xfffe
	.short	320                     # 0x140
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	316                     # 0x13c
	.short	322                     # 0x142
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	323                     # 0x143
	.short	324                     # 0x144
	.short	307                     # 0x133
	.short	325                     # 0x145
	.short	326                     # 0x146
	.short	65180                   # 0xfe9c
	.short	240                     # 0xf0
	.short	65180                   # 0xfe9c
	.short	327                     # 0x147
	.short	329                     # 0x149
	.short	108                     # 0x6c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	331                     # 0x14b
	.short	332                     # 0x14c
	.short	334                     # 0x14e
	.short	333                     # 0x14d
	.short	65180                   # 0xfe9c
	.short	335                     # 0x14f
	.short	65180                   # 0xfe9c
	.short	336                     # 0x150
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	145                     # 0x91
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	96                      # 0x60
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	338                     # 0x152
	.short	340                     # 0x154
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	342                     # 0x156
	.short	232                     # 0xe8
	.short	163                     # 0xa3
	.short	339                     # 0x153
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	239                     # 0xef
	.short	343                     # 0x157
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	344                     # 0x158
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	341                     # 0x155
	.short	347                     # 0x15b
	.short	348                     # 0x15c
	.short	350                     # 0x15e
	.short	65180                   # 0xfe9c
	.short	3                       # 0x3
	.short	65180                   # 0xfe9c
	.short	65521                   # 0xfff1
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	42                      # 0x2a
	.short	232                     # 0xe8
	.short	65180                   # 0xfe9c
	.short	43                      # 0x2b
	.short	65180                   # 0xfe9c
	.short	349                     # 0x15d
	.short	351                     # 0x15f
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	96                      # 0x60
	.short	232                     # 0xe8
	.short	352                     # 0x160
	.short	96                      # 0x60
	.short	96                      # 0x60
	.short	353                     # 0x161
	.short	355                     # 0x163
	.short	357                     # 0x165
	.short	57                      # 0x39
	.short	358                     # 0x166
	.short	361                     # 0x169
	.short	65180                   # 0xfe9c
	.short	359                     # 0x167
	.short	65180                   # 0xfe9c
	.short	163                     # 0xa3
	.short	108                     # 0x6c
	.short	360                     # 0x168
	.short	362                     # 0x16a
	.short	65180                   # 0xfe9c
	.short	363                     # 0x16b
	.short	364                     # 0x16c
	.short	65180                   # 0xfe9c
	.short	44                      # 0x2c
	.short	65180                   # 0xfe9c
	.short	65523                   # 0xfff3
	.short	65180                   # 0xfe9c
	.short	366                     # 0x16e
	.short	365                     # 0x16d
	.short	65180                   # 0xfe9c
	.short	168                     # 0xa8
	.short	372                     # 0x174
	.short	65180                   # 0xfe9c
	.short	369                     # 0x171
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	96                      # 0x60
	.short	65180                   # 0xfe9c
	.short	96                      # 0x60
	.short	232                     # 0xe8
	.short	371                     # 0x173
	.short	373                     # 0x175
	.short	341                     # 0x155
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	0                       # 0x0
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	367                     # 0x16f
	.short	65180                   # 0xfe9c
	.short	370                     # 0x172
	.short	65180                   # 0xfe9c
	.short	375                     # 0x177
	.short	65180                   # 0xfe9c
	.short	306                     # 0x132
	.short	374                     # 0x176
	.short	228                     # 0xe4
	.short	377                     # 0x179
	.short	379                     # 0x17b
	.short	380                     # 0x17c
	.short	341                     # 0x155
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	50                      # 0x32
	.short	381                     # 0x17d
	.short	376                     # 0x178
	.short	382                     # 0x17e
	.short	65180                   # 0xfe9c
	.short	383                     # 0x17f
	.short	65180                   # 0xfe9c
	.short	384                     # 0x180
	.short	66                      # 0x42
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	386                     # 0x182
	.short	228                     # 0xe4
	.short	387                     # 0x183
	.short	385                     # 0x181
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	388                     # 0x184
	.short	7                       # 0x7
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	389                     # 0x185
	.short	232                     # 0xe8
	.short	239                     # 0xef
	.short	65180                   # 0xfe9c
	.short	228                     # 0xe4
	.short	65180                   # 0xfe9c
	.short	69                      # 0x45
	.short	239                     # 0xef
	.short	393                     # 0x189
	.short	232                     # 0xe8
	.short	232                     # 0xe8
	.short	90                      # 0x5a
	.short	96                      # 0x60
	.short	306                     # 0x132
	.short	390                     # 0x186
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	153                     # 0x99
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	391                     # 0x187
	.short	179                     # 0xb3
	.short	65180                   # 0xfe9c
	.short	396                     # 0x18c
	.short	395                     # 0x18b
	.short	65180                   # 0xfe9c
	.short	397                     # 0x18d
	.short	239                     # 0xef
	.short	398                     # 0x18e
	.short	401                     # 0x191
	.short	65180                   # 0xfe9c
	.short	402                     # 0x192
	.short	399                     # 0x18f
	.short	65180                   # 0xfe9c
	.short	168                     # 0xa8
	.short	96                      # 0x60
	.short	409                     # 0x199
	.short	408                     # 0x198
	.short	185                     # 0xb9
	.short	65180                   # 0xfe9c
	.short	410                     # 0x19a
	.short	411                     # 0x19b
	.short	65180                   # 0xfe9c
	.short	405                     # 0x195
	.short	168                     # 0xa8
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	400                     # 0x190
	.short	412                     # 0x19c
	.short	65180                   # 0xfe9c
	.short	168                     # 0xa8
	.short	413                     # 0x19d
	.short	198                     # 0xc6
	.short	345                     # 0x159
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	168                     # 0xa8
	.short	168                     # 0xa8
	.short	394                     # 0x18a
	.short	65180                   # 0xfe9c
	.short	168                     # 0xa8
	.short	65180                   # 0xfe9c
	.size	yypact, 954

	.type	yytranslate,@object     # @yytranslate
	.p2align	4
yytranslate:
	.ascii	"\000\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002@A\002\002E\002B\002\002\002\002\002\002\002\002\002\002\002F\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002C\002D\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037 !\"#$%&'()*+,-./0123456789:;<=>?"
	.size	yytranslate, 319

	.type	yycheck,@object         # @yycheck
	.p2align	4
yycheck:
	.short	3                       # 0x3
	.short	46                      # 0x2e
	.short	103                     # 0x67
	.short	3                       # 0x3
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	361                     # 0x169
	.short	9                       # 0x9
	.short	20                      # 0x14
	.short	276                     # 0x114
	.short	3                       # 0x3
	.short	8                       # 0x8
	.short	164                     # 0xa4
	.short	65                      # 0x41
	.short	27                      # 0x1b
	.short	6                       # 0x6
	.short	18                      # 0x12
	.short	69                      # 0x45
	.short	32                      # 0x20
	.short	31                      # 0x1f
	.short	18                      # 0x12
	.short	38                      # 0x26
	.short	19                      # 0x13
	.short	23                      # 0x17
	.short	364                     # 0x16c
	.short	18                      # 0x12
	.short	5                       # 0x5
	.short	40                      # 0x28
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	64                      # 0x40
	.short	12                      # 0xc
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	48                      # 0x30
	.short	40                      # 0x28
	.short	53                      # 0x35
	.short	41                      # 0x29
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	45                      # 0x2d
	.short	41                      # 0x29
	.short	37                      # 0x25
	.short	47                      # 0x2f
	.short	63                      # 0x3f
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	47                      # 0x2f
	.short	390                     # 0x186
	.short	11                      # 0xb
	.short	55                      # 0x37
	.short	3                       # 0x3
	.short	47                      # 0x2f
	.short	322                     # 0x142
	.short	59                      # 0x3b
	.short	17                      # 0x11
	.short	325                     # 0x145
	.short	65                      # 0x41
	.short	46                      # 0x2e
	.short	416                     # 0x1a0
	.short	60                      # 0x3c
	.short	56                      # 0x38
	.short	62                      # 0x3e
	.short	8                       # 0x8
	.short	64                      # 0x40
	.short	60                      # 0x3c
	.short	71                      # 0x47
	.short	62                      # 0x3e
	.short	425                     # 0x1a9
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	23                      # 0x17
	.short	67                      # 0x43
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	27                      # 0x1b
	.short	179                     # 0xb3
	.short	66                      # 0x42
	.short	232                     # 0xe8
	.short	65                      # 0x41
	.short	26                      # 0x1a
	.short	16                      # 0x10
	.short	185                     # 0xb9
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	90                      # 0x5a
	.short	91                      # 0x5b
	.short	36                      # 0x24
	.short	40                      # 0x28
	.short	358                     # 0x166
	.short	95                      # 0x5f
	.short	360                     # 0x168
	.short	49                      # 0x31
	.short	98                      # 0x62
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	18                      # 0x12
	.short	3                       # 0x3
	.short	103                     # 0x67
	.short	69                      # 0x45
	.short	105                     # 0x69
	.short	18                      # 0x12
	.short	107                     # 0x6b
	.short	108                     # 0x6c
	.short	258                     # 0x102
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	18                      # 0x12
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	41                      # 0x29
	.short	4                       # 0x4
	.short	27                      # 0x1b
	.short	14                      # 0xe
	.short	128                     # 0x80
	.short	41                      # 0x29
	.short	47                      # 0x2f
	.short	131                     # 0x83
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	65                      # 0x41
	.short	47                      # 0x2f
	.short	37                      # 0x25
	.short	65                      # 0x41
	.short	69                      # 0x45
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	60                      # 0x3c
	.short	449                     # 0x1c1
	.short	62                      # 0x3e
	.short	47                      # 0x2f
	.short	64                      # 0x40
	.short	60                      # 0x3c
	.short	149                     # 0x95
	.short	62                      # 0x3e
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	29                      # 0x1d
	.short	459                     # 0x1cb
	.short	56                      # 0x38
	.short	254                     # 0xfe
	.short	63                      # 0x3f
	.short	65                      # 0x41
	.short	60                      # 0x3c
	.short	465                     # 0x1d1
	.short	62                      # 0x3e
	.short	69                      # 0x45
	.short	427                     # 0x1ab
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	471                     # 0x1d7
	.short	472                     # 0x1d8
	.short	168                     # 0xa8
	.short	169                     # 0xa9
	.short	475                     # 0x1db
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	173                     # 0xad
	.short	3                       # 0x3
	.short	59                      # 0x3b
	.short	176                     # 0xb0
	.short	18                      # 0x12
	.short	178                     # 0xb2
	.short	179                     # 0xb3
	.short	180                     # 0xb4
	.short	181                     # 0xb5
	.short	67                      # 0x43
	.short	183                     # 0xb7
	.short	18                      # 0x12
	.short	185                     # 0xb9
	.short	450                     # 0x1c2
	.short	43                      # 0x2b
	.short	337                     # 0x151
	.short	18                      # 0x12
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	27                      # 0x1b
	.short	64                      # 0x40
	.short	41                      # 0x29
	.short	201                     # 0xc9
	.short	202                     # 0xca
	.short	25                      # 0x19
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	47                      # 0x2f
	.short	41                      # 0x29
	.short	37                      # 0x25
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	47                      # 0x2f
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	47                      # 0x2f
	.short	60                      # 0x3c
	.short	65                      # 0x41
	.short	62                      # 0x3e
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	60                      # 0x3c
	.short	63                      # 0x3f
	.short	62                      # 0x3e
	.short	35                      # 0x23
	.short	64                      # 0x40
	.short	60                      # 0x3c
	.short	67                      # 0x43
	.short	62                      # 0x3e
	.short	51                      # 0x33
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	237                     # 0xed
	.short	21                      # 0x15
	.short	32                      # 0x20
	.short	64                      # 0x40
	.short	24                      # 0x18
	.short	242                     # 0xf2
	.short	243                     # 0xf3
	.short	27                      # 0x1b
	.short	66                      # 0x42
	.short	246                     # 0xf6
	.short	247                     # 0xf7
	.short	290                     # 0x122
	.short	18                      # 0x12
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	21                      # 0x15
	.short	18                      # 0x12
	.short	254                     # 0xfe
	.short	24                      # 0x18
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	18                      # 0x12
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	47                      # 0x2f
	.short	10                      # 0xa
	.short	24                      # 0x18
	.short	50                      # 0x32
	.short	37                      # 0x25
	.short	68                      # 0x44
	.short	69                      # 0x45
	.short	54                      # 0x36
	.short	41                      # 0x29
	.short	56                      # 0x38
	.short	65                      # 0x41
	.short	63                      # 0x3f
	.short	41                      # 0x29
	.short	60                      # 0x3c
	.short	47                      # 0x2f
	.short	62                      # 0x3e
	.short	64                      # 0x40
	.short	52                      # 0x34
	.short	47                      # 0x2f
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	285                     # 0x11d
	.short	286                     # 0x11e
	.short	56                      # 0x38
	.short	67                      # 0x43
	.short	47                      # 0x2f
	.short	47                      # 0x2f
	.short	60                      # 0x3c
	.short	292                     # 0x124
	.short	62                      # 0x3e
	.short	67                      # 0x43
	.short	60                      # 0x3c
	.short	39                      # 0x27
	.short	62                      # 0x3e
	.short	56                      # 0x38
	.short	66                      # 0x42
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	60                      # 0x3c
	.short	60                      # 0x3c
	.short	62                      # 0x3e
	.short	62                      # 0x3e
	.short	66                      # 0x42
	.short	44                      # 0x2c
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	15                      # 0xf
	.short	65                      # 0x41
	.short	314                     # 0x13a
	.short	66                      # 0x42
	.short	63                      # 0x3f
	.short	66                      # 0x42
	.short	64                      # 0x40
	.short	69                      # 0x45
	.short	45                      # 0x2d
	.short	28                      # 0x1c
	.short	66                      # 0x42
	.short	323                     # 0x143
	.short	65                      # 0x41
	.short	67                      # 0x43
	.short	65                      # 0x41
	.short	67                      # 0x43
	.short	64                      # 0x40
	.short	55                      # 0x37
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	13                      # 0xd
	.short	19                      # 0x13
	.short	66                      # 0x42
	.short	336                     # 0x150
	.short	69                      # 0x45
	.short	67                      # 0x43
	.short	66                      # 0x42
	.short	64                      # 0x40
	.short	69                      # 0x45
	.short	30                      # 0x1e
	.short	19                      # 0x13
	.short	386                     # 0x182
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	351                     # 0x15f
	.short	65                      # 0x41
	.short	63                      # 0x3f
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	7                       # 0x7
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	19                      # 0x13
	.short	66                      # 0x42
	.short	361                     # 0x169
	.short	66                      # 0x42
	.short	405                     # 0x195
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	63                      # 0x3f
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	417                     # 0x1a1
	.short	65                      # 0x41
	.short	419                     # 0x1a3
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	422                     # 0x1a6
	.short	67                      # 0x43
	.short	65                      # 0x41
	.short	113                     # 0x71
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	65                      # 0x41
	.short	68                      # 0x44
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	128                     # 0x80
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	442                     # 0x1ba
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	62                      # 0x3e
	.short	3                       # 0x3
	.short	201                     # 0xc9
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	416                     # 0x1a0
	.short	64                      # 0x40
	.short	70                      # 0x46
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	69                      # 0x45
	.short	64                      # 0x40
	.short	67                      # 0x43
	.short	424                     # 0x1a8
	.short	425                     # 0x1a9
	.short	66                      # 0x42
	.short	66                      # 0x42
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	65                      # 0x41
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	64                      # 0x40
	.short	66                      # 0x42
	.short	60                      # 0x3c
	.short	69                      # 0x45
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	69                      # 0x45
	.short	64                      # 0x40
	.short	62                      # 0x3e
	.short	69                      # 0x45
	.short	65                      # 0x41
	.short	67                      # 0x43
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	64                      # 0x40
	.short	449                     # 0x1c1
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	40                      # 0x28
	.short	65                      # 0x41
	.short	68                      # 0x44
	.short	66                      # 0x42
	.short	237                     # 0xed
	.short	67                      # 0x43
	.short	65                      # 0x41
	.short	459                     # 0x1cb
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	66                      # 0x42
	.short	69                      # 0x45
	.short	65                      # 0x41
	.short	465                     # 0x1d1
	.short	68                      # 0x44
	.short	70                      # 0x46
	.short	67                      # 0x43
	.short	69                      # 0x45
	.short	67                      # 0x43
	.short	471                     # 0x1d7
	.short	472                     # 0x1d8
	.short	69                      # 0x45
	.short	69                      # 0x45
	.short	475                     # 0x1db
	.short	65                      # 0x41
	.short	69                      # 0x45
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	65                      # 0x41
	.short	243                     # 0xf3
	.short	66                      # 0x42
	.short	393                     # 0x189
	.short	411                     # 0x19b
	.short	90                      # 0x5a
	.short	405                     # 0x195
	.short	451                     # 0x1c3
	.short	393                     # 0x189
	.short	447                     # 0x1bf
	.short	419                     # 0x1a3
	.short	63                      # 0x3f
	.short	65535                   # 0xffff
	.short	336                     # 0x150
	.short	65535                   # 0xffff
	.short	285                     # 0x11d
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	178                     # 0xb2
	.size	yycheck, 1014

	.type	yytable,@object         # @yytable
	.p2align	4
yytable:
	.short	10                      # 0xa
	.short	77                      # 0x4d
	.short	139                     # 0x8b
	.short	388                     # 0x184
	.short	331                     # 0x14b
	.short	99                      # 0x63
	.short	384                     # 0x180
	.short	261                     # 0x105
	.short	30                      # 0x1e
	.short	301                     # 0x12d
	.short	293                     # 0x125
	.short	328                     # 0x148
	.short	196                     # 0xc4
	.short	184                     # 0xb8
	.short	362                     # 0x16a
	.short	1                       # 0x1
	.short	262                     # 0x106
	.short	185                     # 0xb9
	.short	62                      # 0x3e
	.short	31                      # 0x1f
	.short	5                       # 0x5
	.short	209                     # 0xd1
	.short	329                     # 0x149
	.short	389                     # 0x185
	.short	387                     # 0x183
	.short	5                       # 0x5
	.short	110                     # 0x6e
	.short	363                     # 0x16b
	.short	67                      # 0x43
	.short	294                     # 0x126
	.short	295                     # 0x127
	.short	68                      # 0x44
	.short	3                       # 0x3
	.short	111                     # 0x6f
	.short	296                     # 0x128
	.short	4                       # 0x4
	.short	32                      # 0x20
	.short	58                      # 0x3a
	.short	332                     # 0x14c
	.short	263                     # 0x107
	.short	297                     # 0x129
	.short	298                     # 0x12a
	.short	65                      # 0x41
	.short	6                       # 0x6
	.short	299                     # 0x12b
	.short	264                     # 0x108
	.short	210                     # 0xd2
	.short	300                     # 0x12c
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	407                     # 0x197
	.short	223                     # 0xdf
	.short	88                      # 0x58
	.short	388                     # 0x184
	.short	7                       # 0x7
	.short	339                     # 0x153
	.short	94                      # 0x5e
	.short	224                     # 0xe0
	.short	342                     # 0x156
	.short	11                      # 0xb
	.short	154                     # 0x9a
	.short	428                     # 0x1ac
	.short	8                       # 0x8
	.short	73                      # 0x49
	.short	9                       # 0x9
	.short	348                     # 0x15c
	.short	55                      # 0x37
	.short	8                       # 0x8
	.short	106                     # 0x6a
	.short	9                       # 0x9
	.short	436                     # 0x1b4
	.short	155                     # 0x9b
	.short	156                     # 0x9c
	.short	389                     # 0x185
	.short	425                     # 0x1a9
	.short	249                     # 0xf9
	.short	349                     # 0x15d
	.short	362                     # 0x16a
	.short	213                     # 0xd5
	.short	12                      # 0xc
	.short	257                     # 0x101
	.short	240                     # 0xf0
	.short	250                     # 0xfa
	.short	13                      # 0xd
	.short	221                     # 0xdd
	.short	185                     # 0xb9
	.short	15                      # 0xf
	.short	58                      # 0x3a
	.short	126                     # 0x7e
	.short	19                      # 0x13
	.short	363                     # 0x16b
	.short	382                     # 0x17e
	.short	130                     # 0x82
	.short	383                     # 0x17f
	.short	16                      # 0x10
	.short	134                     # 0x86
	.short	21                      # 0x15
	.short	245                     # 0xf5
	.short	5                       # 0x5
	.short	293                     # 0x125
	.short	138                     # 0x8a
	.short	246                     # 0xf6
	.short	141                     # 0x8d
	.short	5                       # 0x5
	.short	144                     # 0x90
	.short	138                     # 0x8a
	.short	284                     # 0x11c
	.short	333                     # 0x14d
	.short	335                     # 0x14f
	.short	359                     # 0x167
	.short	25                      # 0x19
	.short	176                     # 0xb0
	.short	336                     # 0x150
	.short	360                     # 0x168
	.short	5                       # 0x5
	.short	89                      # 0x59
	.short	90                      # 0x5a
	.short	67                      # 0x43
	.short	294                     # 0x126
	.short	295                     # 0x127
	.short	68                      # 0x44
	.short	6                       # 0x6
	.short	26                      # 0x1a
	.short	296                     # 0x128
	.short	23                      # 0x17
	.short	94                      # 0x5e
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	172                     # 0xac
	.short	297                     # 0x129
	.short	298                     # 0x12a
	.short	418                     # 0x1a2
	.short	7                       # 0x7
	.short	299                     # 0x12b
	.short	431                     # 0x1af
	.short	419                     # 0x1a3
	.short	300                     # 0x12c
	.short	6                       # 0x6
	.short	419                     # 0x1a3
	.short	34                      # 0x22
	.short	8                       # 0x8
	.short	454                     # 0x1c6
	.short	9                       # 0x9
	.short	7                       # 0x7
	.short	91                      # 0x5b
	.short	8                       # 0x8
	.short	188                     # 0xbc
	.short	9                       # 0x9
	.short	37                      # 0x25
	.short	169                     # 0xa9
	.short	28                      # 0x1c
	.short	464                     # 0x1d0
	.short	73                      # 0x49
	.short	281                     # 0x119
	.short	36                      # 0x24
	.short	437                     # 0x1b5
	.short	8                       # 0x8
	.short	468                     # 0x1d4
	.short	9                       # 0x9
	.short	360                     # 0x168
	.short	438                     # 0x1b6
	.short	127                     # 0x7f
	.short	128                     # 0x80
	.short	473                     # 0x1d9
	.short	474                     # 0x1da
	.short	198                     # 0xc6
	.short	199                     # 0xc7
	.short	476                     # 0x1dc
	.short	161                     # 0xa1
	.short	162                     # 0xa2
	.short	205                     # 0xcd
	.short	369                     # 0x171
	.short	38                      # 0x26
	.short	208                     # 0xd0
	.short	5                       # 0x5
	.short	138                     # 0x8a
	.short	138                     # 0x8a
	.short	214                     # 0xd6
	.short	218                     # 0xda
	.short	40                      # 0x28
	.short	220                     # 0xdc
	.short	5                       # 0x5
	.short	138                     # 0x8a
	.short	455                     # 0x1c7
	.short	41                      # 0x29
	.short	354                     # 0x162
	.short	5                       # 0x5
	.short	167                     # 0xa7
	.short	168                     # 0xa8
	.short	43                      # 0x2b
	.short	370                     # 0x172
	.short	371                     # 0x173
	.short	175                     # 0xaf
	.short	176                     # 0xb0
	.short	44                      # 0x2c
	.short	372                     # 0x174
	.short	45                      # 0x2d
	.short	6                       # 0x6
	.short	172                     # 0xac
	.short	235                     # 0xeb
	.short	47                      # 0x2f
	.short	373                     # 0x175
	.short	374                     # 0x176
	.short	7                       # 0x7
	.short	6                       # 0x6
	.short	375                     # 0x177
	.short	200                     # 0xc8
	.short	201                     # 0xc9
	.short	376                     # 0x178
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	236                     # 0xec
	.short	237                     # 0xed
	.short	291                     # 0x123
	.short	292                     # 0x124
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	50                      # 0x32
	.short	9                       # 0x9
	.short	52                      # 0x34
	.short	202                     # 0xca
	.short	441                     # 0x1b9
	.short	442                     # 0x1ba
	.short	8                       # 0x8
	.short	51                      # 0x33
	.short	9                       # 0x9
	.short	53                      # 0x35
	.short	314                     # 0x13a
	.short	8                       # 0x8
	.short	59                      # 0x3b
	.short	9                       # 0x9
	.short	60                      # 0x3c
	.short	5                       # 0x5
	.short	66                      # 0x42
	.short	205                     # 0xcd
	.short	67                      # 0x43
	.short	62                      # 0x3e
	.short	78                      # 0x4e
	.short	68                      # 0x44
	.short	267                     # 0x10b
	.short	214                     # 0xd6
	.short	69                      # 0x45
	.short	82                      # 0x52
	.short	271                     # 0x10f
	.short	273                     # 0x111
	.short	319                     # 0x13f
	.short	5                       # 0x5
	.short	444                     # 0x1bc
	.short	243                     # 0xf3
	.short	67                      # 0x43
	.short	5                       # 0x5
	.short	138                     # 0x8a
	.short	68                      # 0x44
	.short	458                     # 0x1ca
	.short	459                     # 0x1cb
	.short	6                       # 0x6
	.short	70                      # 0x46
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	280                     # 0x118
	.short	67                      # 0x43
	.short	7                       # 0x7
	.short	79                      # 0x4f
	.short	68                      # 0x44
	.short	71                      # 0x47
	.short	400                     # 0x190
	.short	470                     # 0x1d6
	.short	471                     # 0x1d7
	.short	72                      # 0x48
	.short	6                       # 0x6
	.short	73                      # 0x49
	.short	83                      # 0x53
	.short	84                      # 0x54
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	7                       # 0x7
	.short	9                       # 0x9
	.short	85                      # 0x55
	.short	86                      # 0x56
	.short	7                       # 0x7
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	134                     # 0x86
	.short	315                     # 0x13b
	.short	73                      # 0x49
	.short	95                      # 0x5f
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	321                     # 0x141
	.short	9                       # 0x9
	.short	98                      # 0x62
	.short	8                       # 0x8
	.short	96                      # 0x60
	.short	9                       # 0x9
	.short	73                      # 0x49
	.short	102                     # 0x66
	.short	103                     # 0x67
	.short	104                     # 0x68
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	109                     # 0x6d
	.short	114                     # 0x72
	.short	105                     # 0x69
	.short	107                     # 0x6b
	.short	108                     # 0x6c
	.short	113                     # 0x71
	.short	121                     # 0x79
	.short	118                     # 0x76
	.short	334                     # 0x14e
	.short	117                     # 0x75
	.short	119                     # 0x77
	.short	124                     # 0x7c
	.short	120                     # 0x78
	.short	123                     # 0x7b
	.short	132                     # 0x84
	.short	142                     # 0x8e
	.short	136                     # 0x88
	.short	340                     # 0x154
	.short	137                     # 0x89
	.short	131                     # 0x83
	.short	147                     # 0x93
	.short	140                     # 0x8c
	.short	149                     # 0x95
	.short	150                     # 0x96
	.short	158                     # 0x9e
	.short	152                     # 0x98
	.short	153                     # 0x99
	.short	159                     # 0x9f
	.short	174                     # 0xae
	.short	165                     # 0xa5
	.short	315                     # 0x13b
	.short	164                     # 0xa4
	.short	173                     # 0xad
	.short	177                     # 0xb1
	.short	178                     # 0xb2
	.short	179                     # 0xb3
	.short	182                     # 0xb6
	.short	194                     # 0xc2
	.short	403                     # 0x193
	.short	186                     # 0xba
	.short	181                     # 0xb5
	.short	183                     # 0xb7
	.short	189                     # 0xbd
	.short	187                     # 0xbb
	.short	190                     # 0xbe
	.short	377                     # 0x179
	.short	191                     # 0xbf
	.short	192                     # 0xc0
	.short	195                     # 0xc3
	.short	193                     # 0xc1
	.short	219                     # 0xdb
	.short	232                     # 0xe8
	.short	197                     # 0xc5
	.short	239                     # 0xef
	.short	206                     # 0xce
	.short	214                     # 0xd6
	.short	207                     # 0xcf
	.short	403                     # 0x193
	.short	225                     # 0xe1
	.short	242                     # 0xf2
	.short	227                     # 0xe3
	.short	228                     # 0xe4
	.short	229                     # 0xe5
	.short	230                     # 0xe6
	.short	233                     # 0xe9
	.short	231                     # 0xe7
	.short	241                     # 0xf1
	.short	248                     # 0xf8
	.short	276                     # 0x114
	.short	429                     # 0x1ad
	.short	251                     # 0xfb
	.short	403                     # 0x193
	.short	243                     # 0xf3
	.short	253                     # 0xfd
	.short	433                     # 0x1b1
	.short	247                     # 0xf7
	.short	252                     # 0xfc
	.short	148                     # 0x94
	.short	255                     # 0xff
	.short	258                     # 0x102
	.short	256                     # 0x100
	.short	269                     # 0x10d
	.short	259                     # 0x103
	.short	266                     # 0x10a
	.short	166                     # 0xa6
	.short	270                     # 0x10e
	.short	274                     # 0x112
	.short	275                     # 0x113
	.short	277                     # 0x115
	.short	278                     # 0x116
	.short	282                     # 0x11a
	.short	283                     # 0x11b
	.short	285                     # 0x11d
	.short	286                     # 0x11e
	.short	448                     # 0x1c0
	.short	287                     # 0x11f
	.short	288                     # 0x120
	.short	289                     # 0x121
	.short	318                     # 0x13e
	.short	388                     # 0x184
	.short	234                     # 0xea
	.short	310                     # 0x136
	.short	290                     # 0x122
	.short	311                     # 0x137
	.short	312                     # 0x138
	.short	322                     # 0x142
	.short	320                     # 0x140
	.short	323                     # 0x143
	.short	325                     # 0x145
	.short	326                     # 0x146
	.short	214                     # 0xd6
	.short	327                     # 0x147
	.short	472                     # 0x1d8
	.short	338                     # 0x152
	.short	341                     # 0x155
	.short	337                     # 0x151
	.short	346                     # 0x15a
	.short	345                     # 0x159
	.short	435                     # 0x1b3
	.short	214                     # 0xd6
	.short	347                     # 0x15b
	.short	350                     # 0x15e
	.short	351                     # 0x15f
	.short	355                     # 0x163
	.short	356                     # 0x164
	.short	352                     # 0x160
	.short	357                     # 0x165
	.short	367                     # 0x16f
	.short	368                     # 0x170
	.short	380                     # 0x17c
	.short	358                     # 0x166
	.short	381                     # 0x17d
	.short	385                     # 0x181
	.short	393                     # 0x189
	.short	386                     # 0x182
	.short	413                     # 0x19d
	.short	395                     # 0x18b
	.short	397                     # 0x18d
	.short	399                     # 0x18f
	.short	404                     # 0x194
	.short	405                     # 0x195
	.short	406                     # 0x196
	.short	411                     # 0x19b
	.short	377                     # 0x179
	.short	414                     # 0x19e
	.short	417                     # 0x1a1
	.short	363                     # 0x16b
	.short	423                     # 0x1a7
	.short	415                     # 0x19f
	.short	420                     # 0x1a4
	.short	260                     # 0x104
	.short	422                     # 0x1a6
	.short	440                     # 0x1b8
	.short	377                     # 0x179
	.short	424                     # 0x1a8
	.short	427                     # 0x1ab
	.short	434                     # 0x1b2
	.short	443                     # 0x1bb
	.short	445                     # 0x1bd
	.short	377                     # 0x179
	.short	446                     # 0x1be
	.short	475                     # 0x1db
	.short	449                     # 0x1c1
	.short	447                     # 0x1bf
	.short	465                     # 0x1d1
	.short	377                     # 0x179
	.short	377                     # 0x179
	.short	450                     # 0x1c2
	.short	451                     # 0x1c3
	.short	377                     # 0x179
	.short	457                     # 0x1c9
	.short	462                     # 0x1ce
	.short	460                     # 0x1cc
	.short	461                     # 0x1cd
	.short	466                     # 0x1d2
	.short	268                     # 0x10c
	.short	469                     # 0x1d5
	.short	408                     # 0x198
	.short	426                     # 0x1aa
	.short	125                     # 0x7d
	.short	421                     # 0x1a5
	.short	456                     # 0x1c8
	.short	409                     # 0x199
	.short	452                     # 0x1c4
	.short	430                     # 0x1ae
	.short	100                     # 0x64
	.short	0                       # 0x0
	.short	353                     # 0x161
	.short	0                       # 0x0
	.short	313                     # 0x139
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	212                     # 0xd4
	.size	yytable, 1014

	.type	dfg_lval,@object        # @dfg_lval
	.comm	dfg_lval,8,8
	.type	yydefact,@object        # @yydefact
	.p2align	4
yydefact:
	.ascii	"\000\000\000\000\001bca`_\000\000\000\000\022\000\000\2562\000\000\024\000\000G\000\000\b\000\032\000\000\000\2574h\000\000\000\n\000\000 \002\000\000\000\000H\214\000\000\000\000\000\000\000\026\030\000\000$\000\000\300\261\000\000~\000\000\000\000}56|\000\000\000i\250\004\000\000\000\000\000\000\000\000\000\000\034\036\000\000*\000\000\301\0003\000\000\000>\000\0008EF\000\000\000\215\021\005\000\000\000\000\f\000\025\027\000\000\000\000\"\000\000\000\303\000\260\000\210\000:\000?\000\000\212\000\000\000\000\000\251\t\000\016\020\017\000\000\000\00010\000\000\033\035\000\000\000\000&(\000\000\000\000\000\000\000\000\000\000\000\200\000I\000\000\252\013\000\000\000\000\031\000!#\000\000\000\000\000,.\023\302\304\265\263\000\000\000f;d\000B\000\000\213\000\000\000\000\254\006\000\000\003\037\000%'\000\000\000\000\000\211\177\000\000\000\000\000\000\000\000\000\000\000\216\000\007\000\000\000+-\264\000\000\000\000\262\000e\0009C\000@7DTmk\220\000\000\r)\000\000\000\000\266g\000\000\000[X]^YZ\000\\UK\000\000\000M\000\201\000\253\255/\000\000\274\000\272\000\000=A\000\000\000\000\000\000\000\000\000\000\000\270\000\267\000\000\000\000L\000\000V\000Qoj\000\000\217\000\000\273\000<NJ\000P\000\000\000\000Kpq\203l\237\234\242\241\235\236\233\240\232\000\230\276\271\000WR\000\000\000\000\000K\204\205\000\243\000\000O\000r\000\000vx\000\000\000\000\246\247\000\000\231\277\275\000\000\000u\000n\000\000\000\000\000\000\000s\000w\207\000z\202\000\000\244\000\000y\000\000\000\000S\000\000{\000\000\000\000\000\222\000\000\206\224\000\245t\000\000\223\000\000\000\000\221\225\000\000\000\226\000\227"
	.size	yydefact, 477

	.type	yyr2,@object            # @yyr2
	.p2align	4
yyr2:
	.ascii	"\000\002\n\013\005\005\005\005\000\005\000\005\000\005\001\001\001\006\000\t\000\005\001\003\001\005\000\005\001\003\001\005\000\005\001\003\000\005\001\003\001\005\000\005\001\003\001\005\001\001\000\005\000\002\001\007\002\007\000\000\013\t\000\001\001\003\001\003\b\001\001\000\002\000\007\000\002\001\004\006\004\000\000\n\000\001\001\003\001\001\001\001\001\001\001\001\001\001\001\001\001\003\001\004\000\002\n\000\013\000\007\000\001\001\000\000\n\004\001\003\001\004\001\003\001\001\001\006\004\000\007\000\001\001\b\004\001\004\001\003\000\002\000\t\000\017\001\003\000\004\003\005\000\003\001\001\001\001\001\001\001\001\001\000\003\007\001\001\000\002\000\006\000\003\000\002\005\000\t\001\003\000\003\004\004\006\001\003\001\006\000\002\001\002\005\001\003"
	.size	yyr2, 197

	.type	dfg_SORTDECLLIST,@object # @dfg_SORTDECLLIST
	.local	dfg_SORTDECLLIST
	.comm	dfg_SORTDECLLIST,8,8
	.type	dfg_AXIOMLIST,@object   # @dfg_AXIOMLIST
	.local	dfg_AXIOMLIST
	.comm	dfg_AXIOMLIST,8,8
	.type	dfg_CONJECLIST,@object  # @dfg_CONJECLIST
	.local	dfg_CONJECLIST
	.comm	dfg_CONJECLIST,8,8
	.type	dfg_IGNORE,@object      # @dfg_IGNORE
	.local	dfg_IGNORE
	.comm	dfg_IGNORE,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"set_flag"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"set_DomPred"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"set_precedence"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Line %d: Symbol is not a variable.\n"
	.size	.L.str.3, 38

	.type	dfg_LINENUMBER,@object  # @dfg_LINENUMBER
	.comm	dfg_LINENUMBER,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n Line %d: Symbol is not a predicate.\n"
	.size	.L.str.4, 39

	.type	dfg_AXCLAUSES,@object   # @dfg_AXCLAUSES
	.local	dfg_AXCLAUSES
	.comm	dfg_AXCLAUSES,8,8
	.type	dfg_CONCLAUSES,@object  # @dfg_CONCLAUSES
	.local	dfg_CONCLAUSES
	.comm	dfg_CONCLAUSES,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SPASS"
	.size	.L.str.5, 6

	.type	dfg_PROOFLIST,@object   # @dfg_PROOFLIST
	.local	dfg_PROOFLIST
	.comm	dfg_PROOFLIST,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"splitlevel"
	.size	.L.str.6, 11

	.type	dfg_TERMLIST,@object    # @dfg_TERMLIST
	.local	dfg_TERMLIST
	.comm	dfg_TERMLIST,8,8
	.type	dfg_IGNORETEXT,@object  # @dfg_IGNORETEXT
	.comm	dfg_IGNORETEXT,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n Undefined symbol %s"
	.size	.L.str.7, 22

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" in DomPred list.\n"
	.size	.L.str.8, 19

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n Symbol %s isn't a predicate"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\n Found unknown flag %s"
	.size	.L.str.10, 24

	.type	dfg_FLAGS,@object       # @dfg_FLAGS
	.local	dfg_FLAGS
	.comm	dfg_FLAGS,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n Undefined symbol %s "
	.size	.L.str.11, 23

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" in precedence list.\n"
	.size	.L.str.12, 22

	.type	dfg_PRECEDENCE,@object  # @dfg_PRECEDENCE
	.local	dfg_PRECEDENCE
	.comm	dfg_PRECEDENCE,8,8
	.type	dfg_USERPRECEDENCE,@object # @dfg_USERPRECEDENCE
	.local	dfg_USERPRECEDENCE
	.comm	dfg_USERPRECEDENCE,8,8
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"in precedence list.\n"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n Invalid symbol status %s"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" in precedence list."
	.size	.L.str.15, 21

	.type	yyr1,@object            # @yyr1
	.section	.rodata,"a",@progbits
	.p2align	4
yyr1:
	.ascii	"\000GHIJKLMNNOOPPQQQRSSTTUUVVWWXXYYZZ[[\\\\]]^^__``aabbccddeeeefgehiijjkklmmnnooppqqqqrsqttuuvvvwwxxyyyyyzz{{||}~}\177\177\200\200\201\202\203\201\204\205\205\206\206\207\207\210\210\210\210\210\211\211\212\212\213\213\214\215\215\216\216\217\217\221\220\222\222\223\223\224\224\225\225\227\226\230\230\230\230\230\230\230\230\230\231\231\231\232\232\233\233\235\234\236\236\237\237\240\241\240\242\242\243\243\244\244\244\245\245\246\246\247\247\250\250\251\252\252"
	.size	yyr1, 197

	.type	yypgoto,@object         # @yypgoto
	.p2align	4
yypgoto:
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	392                     # 0x188
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	259                     # 0x103
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	202                     # 0xca
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	216                     # 0xd8
	.short	65384                   # 0xff68
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	267                     # 0x10b
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65196                   # 0xfeac
	.short	65269                   # 0xfef5
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	70                      # 0x46
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65533                   # 0xfffd
	.short	65181                   # 0xfe9d
	.short	235                     # 0xeb
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	87                      # 0x57
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	33                      # 0x21
	.short	78                      # 0x4e
	.short	68                      # 0x44
	.short	65180                   # 0xfe9c
	.short	65491                   # 0xffd3
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	92                      # 0x5c
	.short	39                      # 0x27
	.short	65435                   # 0xff9b
	.short	328                     # 0x148
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65228                   # 0xfecc
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	154                     # 0x9a
	.short	65180                   # 0xfe9c
	.short	65180                   # 0xfe9c
	.short	425                     # 0x1a9
	.short	207                     # 0xcf
	.size	yypgoto, 200

	.type	yydefgoto,@object       # @yydefgoto
	.p2align	4
yydefgoto:
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	14                      # 0xe
	.short	20                      # 0x14
	.short	27                      # 0x1b
	.short	87                      # 0x57
	.short	122                     # 0x7a
	.short	39                      # 0x27
	.short	54                      # 0x36
	.short	160                     # 0xa0
	.short	157                     # 0x9d
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	29                      # 0x1d
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	42                      # 0x2a
	.short	92                      # 0x5c
	.short	93                      # 0x5d
	.short	61                      # 0x3d
	.short	129                     # 0x81
	.short	97                      # 0x61
	.short	170                     # 0xaa
	.short	171                     # 0xab
	.short	133                     # 0x85
	.short	203                     # 0xcb
	.short	204                     # 0xcc
	.short	163                     # 0xa3
	.short	24                      # 0x18
	.short	46                      # 0x2e
	.short	74                      # 0x4a
	.short	180                     # 0xb4
	.short	244                     # 0xf4
	.short	75                      # 0x4b
	.short	143                     # 0x8f
	.short	272                     # 0x110
	.short	217                     # 0xd9
	.short	48                      # 0x30
	.short	112                     # 0x70
	.short	35                      # 0x23
	.short	222                     # 0xde
	.short	324                     # 0x144
	.short	343                     # 0x157
	.short	361                     # 0x169
	.short	398                     # 0x18e
	.short	302                     # 0x12e
	.short	344                     # 0x158
	.short	303                     # 0x12f
	.short	304                     # 0x130
	.short	305                     # 0x131
	.short	76                      # 0x4c
	.short	215                     # 0xd7
	.short	216                     # 0xd8
	.short	49                      # 0x31
	.short	80                      # 0x50
	.short	308                     # 0x134
	.short	307                     # 0x133
	.short	364                     # 0x16c
	.short	365                     # 0x16d
	.short	416                     # 0x1a0
	.short	439                     # 0x1b7
	.short	366                     # 0x16e
	.short	401                     # 0x191
	.short	402                     # 0x192
	.short	432                     # 0x1b0
	.short	306                     # 0x132
	.short	330                     # 0x14a
	.short	390                     # 0x186
	.short	391                     # 0x187
	.short	392                     # 0x188
	.short	145                     # 0x91
	.short	146                     # 0x92
	.short	81                      # 0x51
	.short	115                     # 0x73
	.short	279                     # 0x117
	.short	309                     # 0x135
	.short	453                     # 0x1c5
	.short	463                     # 0x1cf
	.short	467                     # 0x1d3
	.short	378                     # 0x17a
	.short	394                     # 0x18a
	.short	379                     # 0x17b
	.short	412                     # 0x19c
	.short	410                     # 0x19a
	.short	116                     # 0x74
	.short	151                     # 0x97
	.short	226                     # 0xe2
	.short	254                     # 0xfe
	.short	22                      # 0x16
	.short	33                      # 0x21
	.short	101                     # 0x65
	.short	211                     # 0xd3
	.short	238                     # 0xee
	.short	265                     # 0x109
	.short	316                     # 0x13c
	.short	317                     # 0x13d
	.short	396                     # 0x18c
	.short	63                      # 0x3f
	.short	64                      # 0x40
	.short	135                     # 0x87
	.size	yydefgoto, 200

	.type	yytname,@object         # @yytname
	.p2align	4
yytname:
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.51
	.quad	.L.str.52
	.quad	.L.str.53
	.quad	.L.str.54
	.quad	.L.str.55
	.quad	.L.str.56
	.quad	.L.str.57
	.quad	.L.str.58
	.quad	.L.str.59
	.quad	.L.str.60
	.quad	.L.str.61
	.quad	.L.str.62
	.quad	.L.str.63
	.quad	.L.str.64
	.quad	.L.str.65
	.quad	.L.str.66
	.quad	.L.str.67
	.quad	.L.str.68
	.quad	.L.str.69
	.quad	.L.str.70
	.quad	.L.str.71
	.quad	.L.str.72
	.quad	.L.str.73
	.quad	.L.str.74
	.quad	.L.str.75
	.quad	.L.str.76
	.quad	.L.str.77
	.quad	.L.str.78
	.quad	.L.str.79
	.quad	.L.str.80
	.quad	.L.str.81
	.quad	.L.str.82
	.quad	.L.str.83
	.quad	.L.str.84
	.quad	.L.str.85
	.quad	.L.str.86
	.quad	.L.str.87
	.quad	.L.str.88
	.quad	.L.str.89
	.quad	.L.str.90
	.quad	.L.str.91
	.quad	.L.str.92
	.quad	.L.str.93
	.quad	.L.str.94
	.quad	.L.str.95
	.quad	.L.str.96
	.quad	.L.str.97
	.quad	.L.str.98
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.101
	.quad	.L.str.102
	.quad	.L.str.103
	.quad	.L.str.104
	.quad	.L.str.105
	.quad	.L.str.106
	.quad	.L.str.107
	.quad	.L.str.108
	.quad	.L.str.109
	.quad	.L.str.110
	.quad	.L.str.111
	.quad	.L.str.112
	.quad	.L.str.113
	.quad	.L.str.114
	.quad	.L.str.115
	.quad	.L.str.116
	.quad	.L.str.117
	.quad	.L.str.118
	.quad	.L.str.119
	.quad	.L.str.120
	.quad	.L.str.121
	.quad	.L.str.122
	.quad	.L.str.123
	.quad	.L.str.124
	.quad	.L.str.125
	.quad	.L.str.126
	.quad	.L.str.127
	.quad	.L.str.128
	.quad	.L.str.129
	.quad	.L.str.130
	.quad	.L.str.131
	.quad	.L.str.132
	.quad	.L.str.133
	.quad	.L.str.134
	.quad	.L.str.135
	.quad	.L.str.136
	.quad	.L.str.137
	.quad	.L.str.138
	.quad	.L.str.139
	.quad	.L.str.140
	.quad	.L.str.141
	.quad	.L.str.142
	.quad	.L.str.143
	.quad	.L.str.144
	.quad	.L.str.145
	.quad	.L.str.146
	.quad	.L.str.147
	.quad	.L.str.148
	.quad	.L.str.149
	.quad	.L.str.150
	.quad	.L.str.151
	.quad	.L.str.152
	.quad	.L.str.153
	.quad	.L.str.154
	.quad	.L.str.155
	.quad	.L.str.156
	.quad	.L.str.157
	.quad	.L.str.158
	.quad	.L.str.159
	.quad	.L.str.160
	.quad	.L.str.161
	.quad	.L.str.162
	.quad	.L.str.163
	.quad	.L.str.164
	.quad	.L.str.165
	.quad	.L.str.166
	.quad	.L.str.167
	.quad	.L.str.168
	.quad	.L.str.169
	.quad	.L.str.170
	.quad	.L.str.171
	.quad	.L.str.172
	.quad	.L.str.173
	.quad	.L.str.174
	.quad	.L.str.175
	.quad	.L.str.176
	.quad	.L.str.177
	.quad	.L.str.178
	.quad	.L.str.179
	.quad	.L.str.180
	.quad	.L.str.181
	.quad	.L.str.182
	.quad	.L.str.183
	.quad	.L.str.184
	.quad	.L.str.185
	.quad	.L.str.186
	.quad	.L.str.187
	.quad	.L.str.188
	.quad	.L.str.189
	.quad	.L.str.190
	.quad	.L.str.191
	.quad	.L.str.192
	.quad	.L.str.193
	.quad	.L.str.194
	.quad	.L.str.195
	.quad	.L.str.196
	.quad	.L.str.197
	.quad	.L.str.198
	.quad	.L.str.199
	.quad	.L.str.200
	.quad	.L.str.201
	.quad	.L.str.202
	.quad	.L.str.203
	.quad	.L.str.204
	.quad	.L.str.205
	.quad	.L.str.206
	.quad	.L.str.207
	.quad	.L.str.208
	.quad	.L.str.209
	.quad	.L.str.210
	.quad	.L.str.211
	.quad	.L.str.212
	.quad	.L.str.213
	.quad	.L.str.214
	.quad	0
	.size	yytname, 1376

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"parse error, unexpected "
	.size	.L.str.16, 25

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	", expecting "
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	" or "
	.size	.L.str.18, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"parse error"
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"parser stack overflow"
	.size	.L.str.21, 22

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\n Line %i: %s\n"
	.size	.L.str.22, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"satisfiable"
	.size	.L.str.24, 12

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"unsatisfiable"
	.size	.L.str.25, 14

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"unknown"
	.size	.L.str.26, 8

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.27, 31

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"dfgparser.y"
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\n In dfg_ProblemStatusString: Invalid status.\n"
	.size	.L.str.29, 47

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.30, 133

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"list_of_descriptions.\n  name("
	.size	.L.str.31, 30

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"{* *}"
	.size	.L.str.32, 6

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	").\n  author("
	.size	.L.str.33, 13

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	").\n"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"  version("
	.size	.L.str.35, 11

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"  logic("
	.size	.L.str.36, 9

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"  status("
	.size	.L.str.37, 10

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	").\n  description("
	.size	.L.str.38, 18

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"  date("
	.size	.L.str.39, 8

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"end_of_list."
	.size	.L.str.40, 13

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\n Line %d: is not a function.\n"
	.size	.L.str.41, 31

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.42, 50

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.43, 50

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"$end"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"error"
	.size	.L.str.45, 6

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"$undefined"
	.size	.L.str.46, 11

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"DFG_AND"
	.size	.L.str.47, 8

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"DFG_AUTHOR"
	.size	.L.str.48, 11

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"DFG_AXIOMS"
	.size	.L.str.49, 11

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"DFG_BEGPROB"
	.size	.L.str.50, 12

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"DFG_BY"
	.size	.L.str.51, 7

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"DFG_CLAUSE"
	.size	.L.str.52, 11

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"DFG_CLOSEBRACE"
	.size	.L.str.53, 15

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"DFG_CLSLIST"
	.size	.L.str.54, 12

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"DFG_CNF"
	.size	.L.str.55, 8

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"DFG_CONJECS"
	.size	.L.str.56, 12

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"DFG_DATE"
	.size	.L.str.57, 9

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"DFG_DECLLIST"
	.size	.L.str.58, 13

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"DFG_DESC"
	.size	.L.str.59, 9

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"DFG_DESCLIST"
	.size	.L.str.60, 13

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"DFG_DNF"
	.size	.L.str.61, 8

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"DFG_DOMPRED"
	.size	.L.str.62, 12

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"DFG_ENDLIST"
	.size	.L.str.63, 12

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"DFG_ENDPROB"
	.size	.L.str.64, 12

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"DFG_EQUAL"
	.size	.L.str.65, 10

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"DFG_EQUIV"
	.size	.L.str.66, 10

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"DFG_EXISTS"
	.size	.L.str.67, 11

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"DFG_FALSE"
	.size	.L.str.68, 10

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"DFG_FORMLIST"
	.size	.L.str.69, 13

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"DFG_FORMULA"
	.size	.L.str.70, 12

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"DFG_FORALL"
	.size	.L.str.71, 11

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"DFG_FREELY"
	.size	.L.str.72, 11

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"DFG_FUNC"
	.size	.L.str.73, 9

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"DFG_GENERATED"
	.size	.L.str.74, 14

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"DFG_GENSET"
	.size	.L.str.75, 11

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"DFG_HYPOTH"
	.size	.L.str.76, 11

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"DFG_IMPLIED"
	.size	.L.str.77, 12

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"DFG_IMPLIES"
	.size	.L.str.78, 12

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"DFG_LOGIC"
	.size	.L.str.79, 10

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"DFG_NAME"
	.size	.L.str.80, 9

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"DFG_NOT"
	.size	.L.str.81, 8

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"DFG_OPENBRACE"
	.size	.L.str.82, 14

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"DFG_OPERAT"
	.size	.L.str.83, 11

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"DFG_OR"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"DFG_PREC"
	.size	.L.str.85, 9

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"DFG_PRED"
	.size	.L.str.86, 9

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"DFG_PRDICAT"
	.size	.L.str.87, 12

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"DFG_PRFLIST"
	.size	.L.str.88, 12

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"DFG_QUANTIF"
	.size	.L.str.89, 12

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"DFG_SATIS"
	.size	.L.str.90, 10

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"DFG_SETFLAG"
	.size	.L.str.91, 12

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"DFG_SETTINGS"
	.size	.L.str.92, 13

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"DFG_SYMLIST"
	.size	.L.str.93, 12

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"DFG_SORT"
	.size	.L.str.94, 9

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"DFG_SORTS"
	.size	.L.str.95, 10

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"DFG_STATUS"
	.size	.L.str.96, 11

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"DFG_STEP"
	.size	.L.str.97, 9

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"DFG_SUBSORT"
	.size	.L.str.98, 12

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"DFG_TERMLIST"
	.size	.L.str.99, 13

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"DFG_TRUE"
	.size	.L.str.100, 9

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"DFG_UNKNOWN"
	.size	.L.str.101, 12

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"DFG_UNSATIS"
	.size	.L.str.102, 12

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"DFG_VERSION"
	.size	.L.str.103, 12

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"DFG_NUM"
	.size	.L.str.104, 8

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"DFG_MINUS1"
	.size	.L.str.105, 11

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"DFG_ID"
	.size	.L.str.106, 7

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"DFG_TEXT"
	.size	.L.str.107, 9

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"'('"
	.size	.L.str.108, 4

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"')'"
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"'.'"
	.size	.L.str.110, 4

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"'['"
	.size	.L.str.111, 4

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"']'"
	.size	.L.str.112, 4

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"','"
	.size	.L.str.113, 4

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"':'"
	.size	.L.str.114, 4

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"$accept"
	.size	.L.str.115, 8

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"problem"
	.size	.L.str.116, 8

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"description"
	.size	.L.str.117, 12

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"name"
	.size	.L.str.118, 5

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"author"
	.size	.L.str.119, 7

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"status"
	.size	.L.str.120, 7

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"desctext"
	.size	.L.str.121, 9

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"versionopt"
	.size	.L.str.122, 11

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"logicopt"
	.size	.L.str.123, 9

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"dateopt"
	.size	.L.str.124, 8

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"log_state"
	.size	.L.str.125, 10

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"logicalpart"
	.size	.L.str.126, 12

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"symbollistopt"
	.size	.L.str.127, 14

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"functionsopt"
	.size	.L.str.128, 13

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"functionlist"
	.size	.L.str.129, 13

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"func"
	.size	.L.str.130, 5

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"predicatesopt"
	.size	.L.str.131, 14

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"predicatelist"
	.size	.L.str.132, 14

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"pred"
	.size	.L.str.133, 5

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"sortsopt"
	.size	.L.str.134, 9

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"sortlist"
	.size	.L.str.135, 9

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"operatorsopt"
	.size	.L.str.136, 13

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"operatorlist"
	.size	.L.str.137, 13

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"op"
	.size	.L.str.138, 3

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"quantifiersopt"
	.size	.L.str.139, 15

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"quantifierlist"
	.size	.L.str.140, 15

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"quant"
	.size	.L.str.141, 6

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"number"
	.size	.L.str.142, 7

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"declarationlistopt"
	.size	.L.str.143, 19

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"decllistopt"
	.size	.L.str.144, 12

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"decl"
	.size	.L.str.145, 5

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"@1"
	.size	.L.str.146, 3

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"@2"
	.size	.L.str.147, 3

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"gendecl"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"freelyopt"
	.size	.L.str.149, 10

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"funclist"
	.size	.L.str.150, 9

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"sortdecl"
	.size	.L.str.151, 9

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"formulalist"
	.size	.L.str.152, 12

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"origin"
	.size	.L.str.153, 7

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"formulalistsopt"
	.size	.L.str.154, 16

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"formulalistopt"
	.size	.L.str.155, 15

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"labelopt"
	.size	.L.str.156, 9

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"formula"
	.size	.L.str.157, 8

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"@3"
	.size	.L.str.158, 3

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"@4"
	.size	.L.str.159, 3

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"formulaopt"
	.size	.L.str.160, 11

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"arglist"
	.size	.L.str.161, 8

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"binsymbol"
	.size	.L.str.162, 10

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"nsymbol"
	.size	.L.str.163, 8

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"quantsymbol"
	.size	.L.str.164, 12

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"id"
	.size	.L.str.165, 3

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"qtermlist"
	.size	.L.str.166, 10

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"qterm"
	.size	.L.str.167, 6

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"clauselistsopt"
	.size	.L.str.168, 15

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"clauselist"
	.size	.L.str.169, 11

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"@5"
	.size	.L.str.170, 3

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"cnfclausesopt"
	.size	.L.str.171, 14

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"cnfclauseopt"
	.size	.L.str.172, 13

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"cnfclause"
	.size	.L.str.173, 10

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"@6"
	.size	.L.str.174, 3

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"@7"
	.size	.L.str.175, 3

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"cnfclausebody"
	.size	.L.str.176, 14

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"litlist"
	.size	.L.str.177, 8

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"lit"
	.size	.L.str.178, 4

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"atomlist"
	.size	.L.str.179, 9

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"atom"
	.size	.L.str.180, 5

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"dnfclausesopt"
	.size	.L.str.181, 14

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"dnfclauseopt"
	.size	.L.str.182, 13

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"dnfclause"
	.size	.L.str.183, 10

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"dnfclausebody"
	.size	.L.str.184, 14

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"term"
	.size	.L.str.185, 5

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"termlist"
	.size	.L.str.186, 9

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"prooflistsopt"
	.size	.L.str.187, 14

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"prooflist"
	.size	.L.str.188, 10

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"@8"
	.size	.L.str.189, 3

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"prooflistopt"
	.size	.L.str.190, 13

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"parentlist"
	.size	.L.str.191, 11

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"assoclistopt"
	.size	.L.str.192, 13

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"assoclist"
	.size	.L.str.193, 10

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"id_or_formula"
	.size	.L.str.194, 14

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"@9"
	.size	.L.str.195, 3

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"anysymbol"
	.size	.L.str.196, 10

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"optargs"
	.size	.L.str.197, 8

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"clause"
	.size	.L.str.198, 7

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"listOfTermsopt"
	.size	.L.str.199, 15

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"listOfTerms"
	.size	.L.str.200, 12

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"@10"
	.size	.L.str.201, 4

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"terms"
	.size	.L.str.202, 6

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"settinglistsopt"
	.size	.L.str.203, 16

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"settinglist"
	.size	.L.str.204, 12

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"@11"
	.size	.L.str.205, 4

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"flags"
	.size	.L.str.206, 6

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"spassflags"
	.size	.L.str.207, 11

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"spassflag"
	.size	.L.str.208, 10

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"preclist"
	.size	.L.str.209, 9

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"precitem"
	.size	.L.str.210, 9

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"statopt"
	.size	.L.str.211, 8

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"gsettings"
	.size	.L.str.212, 10

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"gsetting"
	.size	.L.str.213, 9

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"labellist"
	.size	.L.str.214, 10

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"\n\n"
	.size	.L.str.215, 3

	.type	dfg_SYMBOLLIST,@object  # @dfg_SYMBOLLIST
	.local	dfg_SYMBOLLIST
	.comm	dfg_SYMBOLLIST,8,8
	.type	dfg_VARLIST,@object     # @dfg_VARLIST
	.local	dfg_VARLIST
	.comm	dfg_VARLIST,8,8
	.type	dfg_VARDECL,@object     # @dfg_VARDECL
	.local	dfg_VARDECL
	.comm	dfg_VARDECL,1,4
	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"\n Line %u: symbols with arbitrary arity are not allowed.\n"
	.size	.L.str.216, 58

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"\n Line %u: symbol %s was already declared as "
	.size	.L.str.217, 46

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"function.\n"
	.size	.L.str.218, 11

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"predicate.\n"
	.size	.L.str.219, 12

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"junctor.\n"
	.size	.L.str.220, 10

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"unknown type.\n"
	.size	.L.str.221, 15

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"\n Line %u: symbol %s was already declared with arity %d\n"
	.size	.L.str.222, 57

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"\n Line %d: Undefined symbol %s.\n"
	.size	.L.str.223, 33

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"\n Line %u:"
	.size	.L.str.224, 11

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	" The actual arity %u"
	.size	.L.str.225, 21

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	" of symbol %s differs"
	.size	.L.str.226, 22

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	" from the previous arity %u.\n"
	.size	.L.str.227, 30

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"\n Line %u: Symbol %s was declared with arity %u.\n"
	.size	.L.str.228, 50

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"\n Line %u: Free Variable %s.\n"
	.size	.L.str.229, 30

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"\n Line %d: Symbol is not a sort predicate.\n"
	.size	.L.str.230, 44

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"\n Line %d: undefined symbol %s.\n"
	.size	.L.str.231, 33

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"\n Line %d: Symbol is not a function.\n"
	.size	.L.str.232, 38

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"\n In dfg_VarCheck: List of variables should be empty!\n"
	.size	.L.str.233, 55

	.type	dfg_DESC.0,@object      # @dfg_DESC.0
	.local	dfg_DESC.0
	.comm	dfg_DESC.0,8,8
	.type	dfg_DESC.1,@object      # @dfg_DESC.1
	.local	dfg_DESC.1
	.comm	dfg_DESC.1,8,8
	.type	dfg_DESC.2,@object      # @dfg_DESC.2
	.local	dfg_DESC.2
	.comm	dfg_DESC.2,8,8
	.type	dfg_DESC.3,@object      # @dfg_DESC.3
	.local	dfg_DESC.3
	.comm	dfg_DESC.3,8,8
	.type	dfg_DESC.4,@object      # @dfg_DESC.4
	.local	dfg_DESC.4
	.comm	dfg_DESC.4,4,8
	.type	dfg_DESC.5,@object      # @dfg_DESC.5
	.local	dfg_DESC.5
	.comm	dfg_DESC.5,8,8
	.type	dfg_DESC.6,@object      # @dfg_DESC.6
	.local	dfg_DESC.6
	.comm	dfg_DESC.6,8,8
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
