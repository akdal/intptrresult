	.text
	.file	"z29.bc"
	.globl	InitSym
	.p2align	4, 0x90
	.type	InitSym,@function
InitSym:                                # @InitSym
	.cfi_startproc
# BB#0:
	movl	$0, scope_top(%rip)
	movb	$0, suppress_scope(%rip)
	movl	$0, suppress_visible(%rip)
	xorl	%eax, %eax
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	leaq	symtab+48(%rax), %rcx
	movq	%rcx, symtab+56(%rax)
	movq	%rcx, symtab+48(%rax)
	addq	$64, %rax
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	leaq	symtab(%rax), %rcx
	movq	%rcx, symtab+8(%rax)
	movq	%rcx, symtab(%rax)
	leaq	symtab+16(%rax), %rcx
	movq	%rcx, symtab+24(%rax)
	movq	%rcx, symtab+16(%rax)
	leaq	symtab+32(%rax), %rcx
	movq	%rcx, symtab+40(%rax)
	movq	%rcx, symtab+32(%rax)
	cmpq	$28480, %rax            # imm = 0x6F40
	jne	.LBB0_3
# BB#2:
	retq
.Lfunc_end0:
	.size	InitSym, .Lfunc_end0-InitSym
	.cfi_endproc

	.globl	PushScope
	.p2align	4, 0x90
	.type	PushScope,@function
PushScope:                              # @PushScope
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	movl	scope_top(%rip), %eax
	cmpl	$300, %eax              # imm = 0x12C
	jl	.LBB1_4
# BB#3:
	leaq	32(%rbx), %r8
	movl	$29, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	scope_top(%rip), %eax
.LBB1_4:
	cltq
	movq	%rbx, scope(,%rax,8)
	movl	%ebp, npars_only(,%rax,4)
	movl	%r14d, vis_only(,%rax,4)
	movl	$0, body_ok(,%rax,4)
	incl	%eax
	movl	%eax, scope_top(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	PushScope, .Lfunc_end1-PushScope
	.cfi_endproc

	.globl	PopScope
	.p2align	4, 0x90
	.type	PopScope,@function
PopScope:                               # @PopScope
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	cmpl	$0, scope_top(%rip)
	jg	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB2_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_4:
	decl	scope_top(%rip)
	popq	%rax
	retq
.Lfunc_end2:
	.size	PopScope, .Lfunc_end2-PopScope
	.cfi_endproc

	.globl	SuppressVisible
	.p2align	4, 0x90
	.type	SuppressVisible,@function
SuppressVisible:                        # @SuppressVisible
	.cfi_startproc
# BB#0:
	movl	$1, suppress_visible(%rip)
	retq
.Lfunc_end3:
	.size	SuppressVisible, .Lfunc_end3-SuppressVisible
	.cfi_endproc

	.globl	UnSuppressVisible
	.p2align	4, 0x90
	.type	UnSuppressVisible,@function
UnSuppressVisible:                      # @UnSuppressVisible
	.cfi_startproc
# BB#0:
	movl	$0, suppress_visible(%rip)
	retq
.Lfunc_end4:
	.size	UnSuppressVisible, .Lfunc_end4-UnSuppressVisible
	.cfi_endproc

	.globl	SuppressScope
	.p2align	4, 0x90
	.type	SuppressScope,@function
SuppressScope:                          # @SuppressScope
	.cfi_startproc
# BB#0:
	movb	$1, suppress_scope(%rip)
	retq
.Lfunc_end5:
	.size	SuppressScope, .Lfunc_end5-SuppressScope
	.cfi_endproc

	.globl	UnSuppressScope
	.p2align	4, 0x90
	.type	UnSuppressScope,@function
UnSuppressScope:                        # @UnSuppressScope
	.cfi_startproc
# BB#0:
	movb	$0, suppress_scope(%rip)
	retq
.Lfunc_end6:
	.size	UnSuppressScope, .Lfunc_end6-UnSuppressScope
	.cfi_endproc

	.globl	SwitchScope
	.p2align	4, 0x90
	.type	SwitchScope,@function
SwitchScope:                            # @SwitchScope
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	subq	$2408, %rsp             # imm = 0x968
.Lcfi9:
	.cfi_def_cfa_offset 2432
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	testq	%rdi, %rdi
	movq	StartSym(%rip), %rbx
	je	.LBB7_11
# BB#1:                                 # %.preheader11
	cmpq	%rdi, %rbx
	je	.LBB7_16
# BB#2:                                 # %.lr.ph15.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph15
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rdi), %rdi
	movq	%rdi, (%rsp,%rax,8)
	incq	%rax
	cmpq	%rbx, %rdi
	jne	.LBB7_3
# BB#4:                                 # %.preheader
	testl	%eax, %eax
	jle	.LBB7_16
# BB#5:                                 # %.lr.ph.preheader
	movslq	%eax, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rsp,%rbx,8), %r14
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_8:                                #   in Loop: Header=BB7_6 Depth=1
	movl	scope_top(%rip), %eax
	cmpl	$300, %eax              # imm = 0x12C
	jl	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_6 Depth=1
	leaq	32(%r14), %r8
	movl	$29, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	scope_top(%rip), %eax
.LBB7_10:                               # %PushScope.exit10
                                        #   in Loop: Header=BB7_6 Depth=1
	cltq
	movq	%r14, scope(,%rax,8)
	movl	$0, npars_only(,%rax,4)
	movl	$0, vis_only(,%rax,4)
	movl	$0, body_ok(,%rax,4)
	incl	%eax
	movl	%eax, scope_top(%rip)
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB7_6
	jmp	.LBB7_16
.LBB7_11:
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB7_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_13:
	movl	scope_top(%rip), %eax
	cmpl	$300, %eax              # imm = 0x12C
	jl	.LBB7_15
# BB#14:
	leaq	32(%rbx), %r8
	movl	$29, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	scope_top(%rip), %eax
.LBB7_15:                               # %PushScope.exit
	cltq
	movq	%rbx, scope(,%rax,8)
	movl	$0, npars_only(,%rax,4)
	movl	$0, vis_only(,%rax,4)
	movl	$0, body_ok(,%rax,4)
	incl	%eax
	movl	%eax, scope_top(%rip)
.LBB7_16:                               # %.loopexit
	addq	$2408, %rsp             # imm = 0x968
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	SwitchScope, .Lfunc_end7-SwitchScope
	.cfi_endproc

	.globl	UnSwitchScope
	.p2align	4, 0x90
	.type	UnSwitchScope,@function
UnSwitchScope:                          # @UnSwitchScope
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_8
# BB#1:                                 # %.preheader
	cmpq	%rbx, StartSym(%rip)
	je	.LBB8_13
# BB#2:                                 # %.lr.ph.preheader
	movl	scope_top(%rip), %eax
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jg	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_5:                                #   in Loop: Header=BB8_3 Depth=1
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_7:                                # %PopScope.exit4
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	scope_top(%rip), %eax
	decl	%eax
	movl	%eax, scope_top(%rip)
	movq	48(%rbx), %rbx
	cmpq	StartSym(%rip), %rbx
	jne	.LBB8_3
	jmp	.LBB8_13
.LBB8_8:
	cmpl	$0, scope_top(%rip)
	jg	.LBB8_10
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_10:
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB8_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_12:                               # %PopScope.exit
	decl	scope_top(%rip)
.LBB8_13:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end8:
	.size	UnSwitchScope, .Lfunc_end8-UnSwitchScope
	.cfi_endproc

	.globl	BodyParAllowed
	.p2align	4, 0x90
	.type	BodyParAllowed,@function
BodyParAllowed:                         # @BodyParAllowed
	.cfi_startproc
# BB#0:
	movslq	scope_top(%rip), %rax
	movl	$1, body_ok-4(,%rax,4)
	retq
.Lfunc_end9:
	.size	BodyParAllowed, .Lfunc_end9-BodyParAllowed
	.cfi_endproc

	.globl	BodyParNotAllowed
	.p2align	4, 0x90
	.type	BodyParNotAllowed,@function
BodyParNotAllowed:                      # @BodyParNotAllowed
	.cfi_startproc
# BB#0:
	movslq	scope_top(%rip), %rax
	movl	$0, body_ok-4(,%rax,4)
	retq
.Lfunc_end10:
	.size	BodyParNotAllowed, .Lfunc_end10-BodyParNotAllowed
	.cfi_endproc

	.globl	DebugScope
	.p2align	4, 0x90
	.type	DebugScope,@function
DebugScope:                             # @DebugScope
	.cfi_startproc
# BB#0:                                 # %.loopexit
	retq
.Lfunc_end11:
	.size	DebugScope, .Lfunc_end11-DebugScope
	.cfi_endproc

	.globl	GetScopeSnapshot
	.p2align	4, 0x90
	.type	GetScopeSnapshot,@function
GetScopeSnapshot:                       # @GetScopeSnapshot
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB12_1
# BB#2:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB12_3
.LBB12_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB12_3:
	movb	$17, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movslq	scope_top(%rip), %r15
	movq	scope-8(,%r15,8), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB12_27
# BB#4:                                 # %.lr.ph
	decq	%r15
	shlq	$2, %r15
	movl	$4095, %r12d            # imm = 0xFFF
	movl	$-8388608, %ebp         # imm = 0xFF800000
	.p2align	4, 0x90
.LBB12_5:                               # =>This Inner Loop Header: Depth=1
	movzbl	zz_lengths+149(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB12_6
# BB#7:                                 #   in Loop: Header=BB12_5 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB12_8
	.p2align	4, 0x90
.LBB12_6:                               #   in Loop: Header=BB12_5 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB12_8:                               #   in Loop: Header=BB12_5 Depth=1
	movb	$-107, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB12_9
# BB#10:                                #   in Loop: Header=BB12_5 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB12_11
	.p2align	4, 0x90
.LBB12_9:                               #   in Loop: Header=BB12_5 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB12_11:                              #   in Loop: Header=BB12_5 Depth=1
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB12_14
# BB#12:                                #   in Loop: Header=BB12_5 Depth=1
	testq	%rax, %rax
	je	.LBB12_14
# BB#13:                                #   in Loop: Header=BB12_5 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB12_14:                              #   in Loop: Header=BB12_5 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB12_17
# BB#15:                                #   in Loop: Header=BB12_5 Depth=1
	testq	%rax, %rax
	je	.LBB12_17
# BB#16:                                #   in Loop: Header=BB12_5 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB12_17:                              #   in Loop: Header=BB12_5 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB12_18
# BB#19:                                #   in Loop: Header=BB12_5 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB12_20
	.p2align	4, 0x90
.LBB12_18:                              #   in Loop: Header=BB12_5 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB12_20:                              #   in Loop: Header=BB12_5 Depth=1
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB12_23
# BB#21:                                #   in Loop: Header=BB12_5 Depth=1
	testq	%rax, %rax
	je	.LBB12_23
# BB#22:                                #   in Loop: Header=BB12_5 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB12_23:                              #   in Loop: Header=BB12_5 Depth=1
	movq	%rax, zz_res(%rip)
	movq	scope(%r15,%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB12_26
# BB#24:                                #   in Loop: Header=BB12_5 Depth=1
	testq	%rcx, %rcx
	je	.LBB12_26
# BB#25:                                #   in Loop: Header=BB12_5 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB12_26:                              #   in Loop: Header=BB12_5 Depth=1
	movl	npars_only(%r15), %eax
	andl	%r12d, %eax
	movl	40(%rbx), %ecx
	andl	%ebp, %ecx
	orl	%eax, %ecx
	movl	vis_only(%r15), %eax
	shll	$12, %eax
	andl	$4190208, %eax          # imm = 0x3FF000
	orl	%ecx, %eax
	movl	body_ok(%r15), %ecx
	andl	$1, %ecx
	shll	$22, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
	movq	scope-8(%r15,%r15), %rax
	addq	$-4, %r15
	cmpq	StartSym(%rip), %rax
	jne	.LBB12_5
.LBB12_27:                              # %._crit_edge
	movl	suppress_visible(%rip), %eax
	andl	$63, %eax
	shll	$23, %eax
	movl	$-528482305, %ecx       # imm = 0xE07FFFFF
	andl	40(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	GetScopeSnapshot, .Lfunc_end12-GetScopeSnapshot
	.cfi_endproc

	.globl	LoadScopeSnapshot
	.p2align	4, 0x90
	.type	LoadScopeSnapshot,@function
LoadScopeSnapshot:                      # @LoadScopeSnapshot
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpb	$17, 32(%r14)
	je	.LBB13_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB13_2:
	movq	StartSym(%rip), %rbx
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB13_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB13_4:
	movl	scope_top(%rip), %eax
	cmpl	$300, %eax              # imm = 0x12C
	jl	.LBB13_6
# BB#5:
	leaq	32(%rbx), %r8
	movl	$29, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	scope_top(%rip), %eax
.LBB13_6:                               # %PushScope.exit
	cltq
	movq	%rbx, scope(,%rax,8)
	movl	$0, npars_only(,%rax,4)
	movl	$0, vis_only(,%rax,4)
	movl	$0, body_ok(,%rax,4)
	incl	%eax
	movl	%eax, scope_top(%rip)
	movq	(%r14), %r15
	cmpq	%r14, %r15
	jne	.LBB13_8
	jmp	.LBB13_19
	.p2align	4, 0x90
.LBB13_18:                              # %PushScope.exit28
                                        #   in Loop: Header=BB13_8 Depth=1
	cltq
	movq	%r12, scope(,%rax,8)
	movl	%r13d, npars_only(,%rax,4)
	movl	%ebp, vis_only(,%rax,4)
	movl	$0, body_ok(,%rax,4)
	leal	1(%rax), %ecx
	movl	%ecx, scope_top(%rip)
	movl	40(%rbx), %ecx
	shrl	$22, %ecx
	andl	$1, %ecx
	movl	%ecx, body_ok(,%rax,4)
	movq	(%r15), %r15
	cmpq	%r14, %r15
	je	.LBB13_19
.LBB13_8:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_9 Depth 2
                                        #     Child Loop BB13_13 Depth 2
	leaq	16(%r15), %rbx
	jmp	.LBB13_9
	.p2align	4, 0x90
.LBB13_20:                              #   in Loop: Header=BB13_9 Depth=2
	addq	$16, %rbx
.LBB13_9:                               #   Parent Loop BB13_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB13_20
# BB#10:                                #   in Loop: Header=BB13_8 Depth=1
	cmpb	$-107, %al
	je	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB13_12:                              # %.loopexit
                                        #   in Loop: Header=BB13_8 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB13_13:                              #   Parent Loop BB13_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r12
	leaq	16(%r12), %rax
	cmpb	$0, 32(%r12)
	je	.LBB13_13
# BB#14:                                #   in Loop: Header=BB13_8 Depth=1
	movl	40(%rbx), %r13d
	movl	%r13d, %ebp
	shrl	$12, %ebp
	cmpb	$1, suppress_scope(%rip)
	jne	.LBB13_16
# BB#15:                                #   in Loop: Header=BB13_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB13_16:                              #   in Loop: Header=BB13_8 Depth=1
	andl	$4095, %r13d            # imm = 0xFFF
	andl	$1023, %ebp             # imm = 0x3FF
	movl	scope_top(%rip), %eax
	cmpl	$300, %eax              # imm = 0x12C
	jl	.LBB13_18
# BB#17:                                #   in Loop: Header=BB13_8 Depth=1
	leaq	32(%r12), %r8
	movl	$29, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	scope_top(%rip), %eax
	jmp	.LBB13_18
.LBB13_19:                              # %._crit_edge
	movl	suppress_visible(%rip), %eax
	movl	40(%r14), %ecx
	movl	%ecx, %edx
	shrl	$23, %edx
	andl	$63, %edx
	movl	%edx, suppress_visible(%rip)
	andl	$63, %eax
	shll	$23, %eax
	andl	$-528482305, %ecx       # imm = 0xE07FFFFF
	orl	%eax, %ecx
	movl	%ecx, 40(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	LoadScopeSnapshot, .Lfunc_end13-LoadScopeSnapshot
	.cfi_endproc

	.globl	ClearScopeSnapshot
	.p2align	4, 0x90
	.type	ClearScopeSnapshot,@function
ClearScopeSnapshot:                     # @ClearScopeSnapshot
	.cfi_startproc
# BB#0:
	movq	StartSym(%rip), %rcx
	movslq	scope_top(%rip), %rax
	leaq	scope-8(,%rax,8), %rdx
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	decl	%eax
	cmpq	%rcx, (%rdx)
	leaq	-8(%rdx), %rdx
	jne	.LBB14_1
# BB#2:
	movl	%eax, scope_top(%rip)
	movl	40(%rdi), %eax
	shrl	$23, %eax
	andl	$63, %eax
	movl	%eax, suppress_visible(%rip)
	retq
.Lfunc_end14:
	.size	ClearScopeSnapshot, .Lfunc_end14-ClearScopeSnapshot
	.cfi_endproc

	.globl	InsertSym
	.p2align	4, 0x90
	.type	InsertSym,@function
InsertSym:                              # @InsertSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 96
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r14d
	movl	%ecx, %r15d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, %r12d
	movq	%rdi, %rbx
	callq	LexLegalName
	testl	%eax, %eax
	jne	.LBB15_2
# BB#1:
	movl	$29, %edi
	movl	$3, %esi
	movl	$.L.str.8, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	callq	Error
.LBB15_2:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	104(%rsp), %r8
	movl	96(%rsp), %ebx
	movzbl	%r12b, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB15_4
# BB#3:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB15_5
.LBB15_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r8, %r13
	callq	GetMemory
	movq	%r13, %r8
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB15_5:
	movb	%r12b, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzwl	2(%rsi), %eax
	movw	%ax, 34(%r13)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	4(%rsi), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r13), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r13)
	andl	4(%rsi), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r13)
	movzwl	41(%r13), %eax
	movzbl	43(%r13), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 96(%r13)
	movq	$0, 112(%r13)
	movb	%r15b, 40(%r13)
	andl	$1, %r14d
	shll	$9, %r14d
	andl	$-7446289, %ecx         # imm = 0xFF8E60EF
	andl	$1, %ebp
	shll	$10, %ebp
	orl	%r14d, %ebp
	movw	%bx, 120(%r13)
	movq	%r8, 48(%r13)
	movq	112(%rsp), %r14
	movq	%r14, 56(%r13)
	leal	2097168(%rcx,%rbp), %eax
	movdqu	%xmm0, 80(%r13)
	movdqu	%xmm0, 64(%r13)
	movw	$0, 122(%r13)
	andb	$-65, 126(%r13)
	movb	$0, 124(%r13)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 43(%r13)
	movw	%ax, 41(%r13)
	testq	%r8, %r8
	je	.LBB15_8
# BB#6:
	cmpb	$-111, 32(%r8)
	jne	.LBB15_8
# BB#7:
	orb	$-128, 42(%r8)
	movzwl	41(%r13), %ecx
	movzbl	43(%r13), %eax
	movb	%al, 43(%r13)
	shll	$16, %eax
	orl	%ecx, %eax
	orl	$32768, %eax            # imm = 0x8000
	movw	%ax, 41(%r13)
	movb	32(%r13), %r12b
.LBB15_8:
	andl	$16252915, %eax         # imm = 0xF7FFF3
	movw	%ax, 41(%r13)
	shrl	$16, %eax
	movb	%al, 43(%r13)
	addb	$112, %r12b
	cmpb	$2, %r12b
	movq	24(%rsp), %rbp          # 8-byte Reload
	ja	.LBB15_21
# BB#9:
	movq	48(%r13), %rax
	movzbl	43(%rax), %ecx
	movzwl	41(%rax), %edx
	movw	%dx, 41(%rax)
	orl	$8, %ecx
	movb	%cl, 43(%rax)
	movb	32(%r13), %al
	cmpb	$-112, %al
	jne	.LBB15_11
# BB#10:
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$4, %edx
	movw	%dx, 41(%rax)
	movb	32(%r13), %al
.LBB15_11:
	cmpb	$-110, %al
	jne	.LBB15_13
# BB#12:
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$8, %edx
	movw	%dx, 41(%rax)
	movb	32(%r13), %al
.LBB15_13:
	cmpb	$-111, %al
	jne	.LBB15_21
# BB#14:
	movq	48(%r13), %rdx
	movq	(%rdx), %rcx
	movb	$97, %al
	cmpq	%rdx, %rcx
	je	.LBB15_20
	.p2align	4, 0x90
.LBB15_15:                              # %.preheader514
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB15_15
# BB#16:                                # %.preheader514
	cmpb	$-111, %dl
	jne	.LBB15_20
# BB#17:
	movb	125(%rcx), %cl
	movb	$32, %al
	cmpb	$32, %cl
	je	.LBB15_20
# BB#18:
	cmpb	$122, %cl
	je	.LBB15_20
# BB#19:
	incb	%cl
	movl	%ecx, %eax
.LBB15_20:                              # %.loopexit515
	movb	%al, 125(%r13)
.LBB15_21:                              # %.thread553
	movzwl	41(%r13), %eax
	movzbl	43(%r13), %ebx
	shll	$16, %ebx
	orl	%eax, %ebx
	movl	%ebx, %eax
	andl	$16777023, %eax         # imm = 0xFFFF3F
	movw	%ax, 41(%r13)
	shrl	$16, %eax
	movb	%al, 43(%r13)
	movl	$.L.str.9, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB15_41
# BB#22:
	andl	$-225, %ebx
	movw	%bx, 41(%r13)
	movl	%ebx, %eax
	shrl	$16, %eax
	movb	%al, 43(%r13)
.LBB15_23:                              # %.loopexit513
	andl	$16752636, %ebx         # imm = 0xFF9FFC
	movw	%bx, 41(%r13)
	shrl	$16, %ebx
	movb	%bl, 43(%r13)
	andb	$-64, 126(%r13)
	movq	48(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB15_52
# BB#24:
	cmpb	$-113, 32(%rbx)
	jne	.LBB15_52
# BB#25:
	movl	$.L.str.10, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_27
# BB#26:
	orb	$-128, 42(%rbx)
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$2, %edx
	movw	%dx, 41(%rax)
	orb	$1, 41(%r13)
.LBB15_27:
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_29
# BB#28:
	movq	48(%r13), %rax
	orb	$2, 126(%rax)
	orb	$1, 126(%r13)
.LBB15_29:
	movl	$.L.str.12, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_48
# BB#30:
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$32768, %edx            # imm = 0x8000
	movw	%dx, 41(%rax)
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$16384, %edx            # imm = 0x4000
	movw	%dx, 41(%rax)
	orb	$32, 42(%r13)
	movq	48(%r13), %rcx
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB15_48
	.p2align	4, 0x90
.LBB15_32:                              # %.preheader510
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_33 Depth 2
                                        #     Child Loop BB15_38 Depth 2
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB15_33:                              #   Parent Loop BB15_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB15_33
# BB#34:                                #   in Loop: Header=BB15_32 Depth=1
	movzwl	41(%rdx), %esi
	testb	$32, %sil
	je	.LBB15_31
# BB#35:                                #   in Loop: Header=BB15_32 Depth=1
	movq	56(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB15_31
# BB#36:                                #   in Loop: Header=BB15_32 Depth=1
	movb	32(%rdx), %bl
	andb	$-2, %bl
	cmpb	$6, %bl
	jne	.LBB15_31
# BB#37:                                #   in Loop: Header=BB15_32 Depth=1
	movq	8(%rdx), %rdx
	.p2align	4, 0x90
.LBB15_38:                              #   Parent Loop BB15_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rdx
	movq	(%rdx), %rdx
	movzbl	32(%rdx), %ebx
	testb	%bl, %bl
	je	.LBB15_38
# BB#39:                                #   in Loop: Header=BB15_32 Depth=1
	cmpb	$2, %bl
	jne	.LBB15_31
# BB#40:                                #   in Loop: Header=BB15_32 Depth=1
	movq	80(%rdx), %rcx
	movzwl	41(%rcx), %esi
	movzbl	43(%rcx), %edi
	movb	%dil, 43(%rcx)
	shll	$16, %edi
	orl	%esi, %edi
	orl	$4096, %edi             # imm = 0x1000
	movw	%di, 41(%rcx)
	movq	80(%rdx), %rcx
	movzwl	41(%rcx), %edx
	movzbl	43(%rcx), %esi
	movb	%sil, 43(%rcx)
	shll	$16, %esi
	orl	%edx, %esi
	orl	$2048, %esi             # imm = 0x800
	movw	%si, 41(%rcx)
	movq	48(%r13), %rcx
	.p2align	4, 0x90
.LBB15_31:                              # %.loopexit509
                                        #   in Loop: Header=BB15_32 Depth=1
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.LBB15_32
.LBB15_48:                              # %.loopexit511
	movl	$.L.str.13, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_50
# BB#49:
	movq	48(%r13), %rax
	orb	$8, 126(%rax)
	orb	$4, 126(%r13)
.LBB15_50:
	movl	$.L.str.14, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_52
# BB#51:
	movq	48(%r13), %rax
	orb	$32, 126(%rax)
	orb	$16, 126(%r13)
.LBB15_52:
	leaq	32(%r13), %rbx
	movl	$.L.str.15, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_58
# BB#53:
	cmpb	$-113, (%rbx)
	jne	.LBB15_57
# BB#54:
	movq	48(%r13), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB15_57
# BB#55:
	movzwl	41(%rax), %ecx
	testb	$8, %cl
	jne	.LBB15_125
# BB#56:
	movl	$29, %edi
	movl	$14, %esi
	movl	$.L.str.17, %edx
	movl	$2, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	cmpb	$-110, (%rbx)
	je	.LBB15_59
	jmp	.LBB15_67
.LBB15_41:
	movq	48(%r13), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$64, %edx
	movw	%dx, 41(%rax)
	movzwl	41(%r13), %eax
	movzbl	43(%r13), %ebx
	movb	%bl, 43(%r13)
	shll	$16, %ebx
	orl	%eax, %ebx
	orl	$32, %ebx
	movw	%bx, 41(%r13)
	testq	%r14, %r14
	je	.LBB15_23
# BB#42:
	movq	48(%r13), %rax
	movzwl	41(%rax), %eax
	andl	$16384, %eax            # imm = 0x4000
	je	.LBB15_23
# BB#43:
	movb	32(%r14), %al
	andb	$-2, %al
	cmpb	$6, %al
	jne	.LBB15_23
# BB#44:
	movq	8(%r14), %rax
	cmpq	%rax, (%r14)
	je	.LBB15_23
	.p2align	4, 0x90
.LBB15_45:                              # %.preheader512
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB15_45
# BB#46:                                # %.preheader512
	cmpb	$2, %cl
	jne	.LBB15_23
# BB#47:
	movq	80(%rax), %rcx
	movzwl	41(%rcx), %edx
	movzbl	43(%rcx), %esi
	movb	%sil, 43(%rcx)
	shll	$16, %esi
	orl	%edx, %esi
	orl	$4096, %esi             # imm = 0x1000
	movw	%si, 41(%rcx)
	movq	80(%rax), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %edx
	movb	%dl, 43(%rax)
	shll	$16, %edx
	orl	%ecx, %edx
	orl	$2048, %edx             # imm = 0x800
	movw	%dx, 41(%rax)
	movzwl	41(%r13), %eax
	movzbl	43(%r13), %ebx
	shll	$16, %ebx
	orl	%eax, %ebx
	jmp	.LBB15_23
.LBB15_57:
	movl	$29, %edi
	movl	$4, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
.LBB15_58:
	cmpb	$-110, (%rbx)
	jne	.LBB15_67
.LBB15_59:
	movq	48(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$1, %ah
	je	.LBB15_63
# BB#60:
	movzwl	41(%r13), %eax
	testw	$8193, %ax              # imm = 0x2001
	jne	.LBB15_62
# BB#61:
	testb	$1, 126(%r13)
	je	.LBB15_63
.LBB15_62:
	movl	$29, %edi
	movl	$5, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	cmpb	$-110, (%rbx)
	jne	.LBB15_67
.LBB15_63:                              # %.thread554
	movq	48(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$64, %al
	je	.LBB15_67
# BB#64:
	movzwl	41(%r13), %eax
	testw	$8193, %ax              # imm = 0x2001
	jne	.LBB15_66
# BB#65:
	testb	$1, 126(%r13)
	je	.LBB15_67
.LBB15_66:
	movl	$29, %edi
	movl	$6, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
.LBB15_67:                              # %.thread
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r15
	movzbl	(%rbp), %edx
	decl	%eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB15_77
# BB#68:                                # %.lr.ph.preheader
	leal	-2(%r15), %edi
	incq	%rdi
	cmpq	$8, %rdi
	jb	.LBB15_74
# BB#70:                                # %min.iters.checked
	leal	7(%r15), %esi
	andl	$7, %esi
	movq	%rdi, %rcx
	subq	%rsi, %rcx
	subq	%rsi, %rdi
	je	.LBB15_74
# BB#71:                                # %vector.ph
	subl	%ecx, %eax
	addq	%rbp, %rcx
	movd	%edx, %xmm0
	leaq	5(%rbp), %rdx
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB15_72:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB15_72
# BB#73:                                # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB15_75
	jmp	.LBB15_77
.LBB15_74:
	movq	%rbp, %rcx
.LBB15_75:                              # %.lr.ph.preheader576
	incq	%rcx
	.p2align	4, 0x90
.LBB15_76:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	addl	%esi, %edx
	incq	%rcx
	decl	%eax
	jne	.LBB15_76
.LBB15_77:                              # %._crit_edge527
	movslq	%edx, %rax
	imulq	$-1828311933, %rax, %rcx # imm = 0x93062C83
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$10, %ecx
	addl	%edx, %ecx
	imull	$1783, %ecx, %ecx       # imm = 0x6F7
	subl	%ecx, %eax
	cltq
	shlq	$4, %rax
	leaq	symtab(%rax), %r14
	movq	symtab+8(%rax), %rbp
	cmpq	%r14, %rbp
	je	.LBB15_84
# BB#78:                                # %.preheader508.preheader
	movl	$4095, %r12d            # imm = 0xFFF
	.p2align	4, 0x90
.LBB15_79:                              # %.preheader508
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_80 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB15_80:                              #   Parent Loop BB15_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB15_80
# BB#81:                                #   in Loop: Header=BB15_79 Depth=1
	movl	40(%rbx), %eax
	andl	%r12d, %eax
	cmpl	%r15d, %eax
	jne	.LBB15_83
# BB#82:                                #   in Loop: Header=BB15_79 Depth=1
	leaq	64(%rbx), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB15_88
.LBB15_83:                              #   in Loop: Header=BB15_79 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r14, %rbp
	jne	.LBB15_79
.LBB15_84:                              # %._crit_edge
	movq	%r15, %rax
	shlq	$32, %rax
	movabsq	$292057776128, %rdi     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB15_86
# BB#85:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	Error
	movq	zz_hold(%rip), %rbx
	jmp	.LBB15_96
.LBB15_86:
	movq	zz_free(,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB15_95
# BB#87:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB15_96
.LBB15_88:                              # %.preheader507
	movq	%rbx, %rax
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_89:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_91 Depth 2
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	je	.LBB15_105
# BB#90:                                # %.preheader.preheader
                                        #   in Loop: Header=BB15_89 Depth=1
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB15_91:                              # %.preheader
                                        #   Parent Loop BB15_89 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB15_91
# BB#92:                                #   in Loop: Header=BB15_89 Depth=1
	movq	48(%r13), %rcx
	cmpq	48(%rbp), %rcx
	jne	.LBB15_89
# BB#93:
	addq	$32, %rbp
	movq	%rbp, %rdi
	callq	EchoFilePos
	movq	%rax, (%rsp)
	movl	$29, %edi
	movl	$7, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	callq	Error
	cmpl	$0, AltErrorFormat(%rip)
	je	.LBB15_105
# BB#94:
	movl	$29, %edi
	movl	$13, %esi
	movl	$.L.str.21, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%r14, %r9
	callq	Error
	jmp	.LBB15_105
.LBB15_95:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB15_96:
	movb	zz_size(%rip), %al
	movb	%al, 33(%rbx)
	movb	$11, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	andl	$4095, %r15d            # imm = 0xFFF
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%rbx), %eax
	orl	%r15d, %eax
	movl	%eax, 40(%rbx)
	leaq	64(%rbx), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	strcpy
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB15_98
# BB#97:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB15_99
.LBB15_98:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB15_99:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB15_102
# BB#100:
	testq	%rax, %rax
	je	.LBB15_102
# BB#101:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB15_102:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB15_105
# BB#103:
	testq	%rax, %rax
	je	.LBB15_105
# BB#104:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB15_105:                             # %.loopexit
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB15_107
# BB#106:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB15_108
.LBB15_107:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB15_108:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB15_111
# BB#109:
	testq	%rax, %rax
	je	.LBB15_111
# BB#110:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB15_111:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB15_114
# BB#112:
	testq	%rax, %rax
	je	.LBB15_114
# BB#113:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB15_114:
	cmpq	$0, 48(%r13)
	je	.LBB15_124
# BB#115:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB15_117
# BB#116:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB15_118
.LBB15_117:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB15_118:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	48(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB15_121
# BB#119:
	testq	%rcx, %rcx
	je	.LBB15_121
# BB#120:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB15_121:
	testq	%r13, %r13
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB15_124
# BB#122:
	testq	%rax, %rax
	je	.LBB15_124
# BB#123:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB15_124:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_125:
	movq	%r13, 104(%rax)
	movb	$104, 40(%rax)
	cmpb	$-110, (%rbx)
	je	.LBB15_59
	jmp	.LBB15_67
.Lfunc_end15:
	.size	InsertSym, .Lfunc_end15-InsertSym
	.cfi_endproc

	.globl	InsertAlternativeName
	.p2align	4, 0x90
	.type	InsertAlternativeName,@function
InsertAlternativeName:                  # @InsertAlternativeName
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 80
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %r12
	movzbl	(%r15), %edx
	decl	%eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB16_10
# BB#1:                                 # %.lr.ph.preheader
	leal	-2(%r12), %edi
	incq	%rdi
	cmpq	$8, %rdi
	jb	.LBB16_7
# BB#3:                                 # %min.iters.checked
	leal	7(%r12), %esi
	andl	$7, %esi
	movq	%rdi, %rcx
	subq	%rsi, %rcx
	subq	%rsi, %rdi
	je	.LBB16_7
# BB#4:                                 # %vector.ph
	subl	%ecx, %eax
	addq	%r15, %rcx
	movd	%edx, %xmm0
	leaq	5(%r15), %rdx
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB16_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB16_5
# BB#6:                                 # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB16_8
	jmp	.LBB16_10
.LBB16_7:
	movq	%r15, %rcx
.LBB16_8:                               # %.lr.ph.preheader128
	incq	%rcx
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	addl	%esi, %edx
	incq	%rcx
	decl	%eax
	jne	.LBB16_9
.LBB16_10:                              # %._crit_edge104
	movslq	%edx, %rax
	imulq	$-1828311933, %rax, %rcx # imm = 0x93062C83
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$10, %ecx
	addl	%edx, %ecx
	imull	$1783, %ecx, %ecx       # imm = 0x6F7
	subl	%ecx, %eax
	cltq
	shlq	$4, %rax
	leaq	symtab(%rax), %rbp
	movq	symtab+8(%rax), %r13
	cmpq	%rbp, %r13
	je	.LBB16_17
# BB#11:                                # %.preheader94.preheader
	movl	$4095, %r14d            # imm = 0xFFF
	.p2align	4, 0x90
.LBB16_12:                              # %.preheader94
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_13 Depth 2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB16_13:                              #   Parent Loop BB16_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB16_13
# BB#14:                                #   in Loop: Header=BB16_12 Depth=1
	movl	40(%rbx), %eax
	andl	%r14d, %eax
	cmpl	%r12d, %eax
	jne	.LBB16_16
# BB#15:                                #   in Loop: Header=BB16_12 Depth=1
	leaq	64(%rbx), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB16_21
.LBB16_16:                              #   in Loop: Header=BB16_12 Depth=1
	movq	8(%r13), %r13
	cmpq	%rbp, %r13
	jne	.LBB16_12
.LBB16_17:                              # %._crit_edge
	movq	%r12, %rax
	shlq	$32, %rax
	movabsq	$292057776128, %rdi     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB16_19
# BB#18:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	Error
	movq	zz_hold(%rip), %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB16_28
.LBB16_19:
	movq	zz_free(,%rax,8), %rbx
	testq	%rbx, %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB16_27
# BB#20:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB16_28
.LBB16_21:                              # %.preheader93
	movq	%rbx, %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB16_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_24 Depth 2
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	je	.LBB16_37
# BB#23:                                # %.preheader.preheader
                                        #   in Loop: Header=BB16_22 Depth=1
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB16_24:                              # %.preheader
                                        #   Parent Loop BB16_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB16_24
# BB#25:                                #   in Loop: Header=BB16_22 Depth=1
	movq	48(%r14), %rcx
	cmpq	48(%rdi), %rcx
	jne	.LBB16_22
# BB#26:
	addq	$32, %rdi
	leaq	32(%r14), %r12
	callq	EchoFilePos
	movq	%rax, (%rsp)
	movl	$29, %edi
	movl	$12, %esi
	movl	$.L.str.22, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r15, %r9
	callq	Error
	jmp	.LBB16_37
.LBB16_27:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB16_28:
	movb	zz_size(%rip), %al
	movb	%al, 33(%rbx)
	movb	$11, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	andl	$4095, %r12d            # imm = 0xFFF
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%rbx), %eax
	orl	%r12d, %eax
	movl	%eax, 40(%rbx)
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	callq	strcpy
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB16_30
# BB#29:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB16_31
.LBB16_30:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB16_31:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB16_34
# BB#32:
	testq	%rax, %rax
	je	.LBB16_34
# BB#33:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB16_34:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB16_37
# BB#35:
	testq	%rax, %rax
	je	.LBB16_37
# BB#36:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB16_37:                              # %.loopexit
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB16_39
# BB#38:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB16_40
.LBB16_39:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB16_40:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB16_43
# BB#41:
	testq	%rax, %rax
	je	.LBB16_43
# BB#42:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB16_43:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB16_46
# BB#44:
	testq	%rax, %rax
	je	.LBB16_46
# BB#45:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB16_46:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	InsertAlternativeName, .Lfunc_end16-InsertAlternativeName
	.cfi_endproc

	.globl	SearchSym
	.p2align	4, 0x90
	.type	SearchSym,@function
SearchSym:                              # @SearchSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 144
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movzbl	(%rdi), %ebp
	movl	%esi, %eax
	decl	%eax
	je	.LBB17_15
# BB#1:                                 # %.lr.ph.preheader
	leal	-2(%rsi), %edx
	incq	%rdx
	cmpq	$8, %rdx
	jae	.LBB17_3
# BB#2:
	movq	%rdi, %rcx
	jmp	.LBB17_13
.LBB17_3:                               # %min.iters.checked
	movabsq	$8589934584, %r9        # imm = 0x1FFFFFFF8
	movq	%rdx, %rcx
	andq	%r9, %rcx
	andq	%rdx, %r9
	je	.LBB17_4
# BB#5:                                 # %vector.ph
	movd	%ebp, %xmm2
	leaq	-8(%r9), %rbp
	movq	%rbp, %rbx
	shrq	$3, %rbx
	btl	$3, %ebp
	jb	.LBB17_6
# BB#7:                                 # %vector.body.prol
	movd	1(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movd	5(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm3
	punpcklbw	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3],xmm1[4],xmm3[4],xmm1[5],xmm3[5],xmm1[6],xmm3[6],xmm1[7],xmm3[7]
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	punpcklbw	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3],xmm0[4],xmm3[4],xmm0[5],xmm3[5],xmm0[6],xmm3[6],xmm0[7],xmm3[7]
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	paddd	%xmm2, %xmm1
	movl	$8, %r8d
	movdqa	%xmm1, %xmm2
	testq	%rbx, %rbx
	jne	.LBB17_9
	jmp	.LBB17_11
.LBB17_4:
	movq	%rdi, %rcx
	jmp	.LBB17_13
.LBB17_6:
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
                                        # implicit-def: %XMM1
	testq	%rbx, %rbx
	je	.LBB17_11
.LBB17_9:                               # %vector.ph.new
	movq	%r9, %rbx
	subq	%r8, %rbx
	leaq	13(%rdi,%r8), %rbp
	pxor	%xmm3, %xmm3
	movdqa	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB17_10:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movd	-8(%rbp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3],xmm2[4],xmm3[4],xmm2[5],xmm3[5],xmm2[6],xmm3[6],xmm2[7],xmm3[7]
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	punpcklbw	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1],xmm4[2],xmm3[2],xmm4[3],xmm3[3],xmm4[4],xmm3[4],xmm4[5],xmm3[5],xmm4[6],xmm3[6],xmm4[7],xmm3[7]
	punpcklwd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1],xmm4[2],xmm3[2],xmm4[3],xmm3[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm0, %xmm4
	movd	-4(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3],xmm1[4],xmm3[4],xmm1[5],xmm3[5],xmm1[6],xmm3[6],xmm1[7],xmm3[7]
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	punpcklbw	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3],xmm0[4],xmm3[4],xmm0[5],xmm3[5],xmm0[6],xmm3[6],xmm0[7],xmm3[7]
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	paddd	%xmm2, %xmm1
	paddd	%xmm4, %xmm0
	addq	$16, %rbp
	addq	$-16, %rbx
	jne	.LBB17_10
.LBB17_11:                              # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ebp
	cmpq	%r9, %rdx
	je	.LBB17_15
# BB#12:
	subl	%ecx, %eax
	addq	%rdi, %rcx
.LBB17_13:                              # %.lr.ph.preheader208
	incq	%rcx
	.p2align	4, 0x90
.LBB17_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	addl	%edx, %ebp
	incq	%rcx
	decl	%eax
	jne	.LBB17_14
.LBB17_15:                              # %._crit_edge165
	movslq	%ebp, %rax
	imulq	$-1828311933, %rax, %rcx # imm = 0x93062C83
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$10, %ecx
	addl	%edx, %ecx
	imull	$1783, %ecx, %ecx       # imm = 0x6F7
	subl	%ecx, %eax
	cltq
	shlq	$4, %rax
	leaq	symtab(%rax), %rcx
	movq	symtab+8(%rax), %rbp
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmpq	%rcx, %rbp
	je	.LBB17_38
# BB#16:                                # %.preheader139.lr.ph
	movq	StartSym(%rip), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	suppress_visible(%rip), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movb	suppress_scope(%rip), %cl
	movb	%cl, 11(%rsp)           # 1-byte Spill
	movslq	scope_top(%rip), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%esi, %edx
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
.LBB17_17:                              # %.preheader139
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_18 Depth 2
                                        #     Child Loop BB17_21 Depth 2
                                        #     Child Loop BB17_25 Depth 2
                                        #       Child Loop BB17_27 Depth 3
                                        #         Child Loop BB17_28 Depth 4
                                        #       Child Loop BB17_39 Depth 3
                                        #         Child Loop BB17_40 Depth 4
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_18:                              #   Parent Loop BB17_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB17_18
# BB#19:                                #   in Loop: Header=BB17_17 Depth=1
	movl	40(%rbp), %eax
	movl	$4095, %ecx             # imm = 0xFFF
	andl	%ecx, %eax
	cmpl	%esi, %eax
	jne	.LBB17_37
# BB#20:                                #   in Loop: Header=BB17_17 Depth=1
	leaq	64(%rbp), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB17_21:                              #   Parent Loop BB17_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rax), %ecx
	cmpb	64(%rbp,%rax), %cl
	jne	.LBB17_23
# BB#22:                                #   in Loop: Header=BB17_21 Depth=2
	incq	%rax
	cmpl	%eax, %edx
	jne	.LBB17_21
	jmp	.LBB17_24
	.p2align	4, 0x90
.LBB17_23:                              # %.critedge
                                        #   in Loop: Header=BB17_17 Depth=1
	cmpl	%eax, %edx
	jne	.LBB17_37
.LBB17_24:                              # %.critedge.thread
                                        #   in Loop: Header=BB17_17 Depth=1
	movq	8(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%rsi, 24(%rsp)          # 8-byte Spill
.LBB17_25:                              #   Parent Loop BB17_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_27 Depth 3
                                        #         Child Loop BB17_28 Depth 4
                                        #       Child Loop BB17_39 Depth 3
                                        #         Child Loop BB17_40 Depth 4
	cmpq	%rbp, 16(%rsp)          # 8-byte Folded Reload
	movq	scope-8(,%r15,8), %r14
	je	.LBB17_36
# BB#26:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB17_25 Depth=2
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%r13, %r12
	je	.LBB17_39
	.p2align	4, 0x90
.LBB17_27:                              # %.preheader.us
                                        #   Parent Loop BB17_17 Depth=1
                                        #     Parent Loop BB17_25 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_28 Depth 4
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB17_28:                              #   Parent Loop BB17_17 Depth=1
                                        #     Parent Loop BB17_25 Depth=2
                                        #       Parent Loop BB17_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB17_28
# BB#29:                                #   in Loop: Header=BB17_27 Depth=3
	cmpq	%r14, 48(%rbx)
	jne	.LBB17_35
# BB#30:                                #   in Loop: Header=BB17_27 Depth=3
	cmpb	$-111, %al
	je	.LBB17_32
# BB#31:                                #   in Loop: Header=BB17_27 Depth=3
	movl	npars_only-4(,%r15,4), %eax
	testl	%eax, %eax
	jne	.LBB17_35
.LBB17_32:                              #   in Loop: Header=BB17_27 Depth=3
	testb	$1, 11(%rsp)            # 1-byte Folded Reload
	je	.LBB17_53
# BB#33:                                #   in Loop: Header=BB17_27 Depth=3
	movl	$.L.str.23, %esi
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_53
# BB#34:                                #   in Loop: Header=BB17_27 Depth=3
	movl	$.L.str.24, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_53
	.p2align	4, 0x90
.LBB17_35:                              # %.backedge.us
                                        #   in Loop: Header=BB17_27 Depth=3
	movq	8(%r12), %r12
	cmpq	%rbp, %r12
	jne	.LBB17_27
	jmp	.LBB17_36
	.p2align	4, 0x90
.LBB17_39:                              # %.preheader
                                        #   Parent Loop BB17_17 Depth=1
                                        #     Parent Loop BB17_25 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_40 Depth 4
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB17_40:                              #   Parent Loop BB17_17 Depth=1
                                        #     Parent Loop BB17_25 Depth=2
                                        #       Parent Loop BB17_39 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB17_40
# BB#41:                                #   in Loop: Header=BB17_39 Depth=3
	movq	48(%rbx), %rcx
	cmpq	%r14, %rcx
	jne	.LBB17_44
# BB#42:                                #   in Loop: Header=BB17_39 Depth=3
	cmpb	$-111, %al
	je	.LBB17_45
# BB#43:                                #   in Loop: Header=BB17_39 Depth=3
	movl	npars_only-4(,%r15,4), %edx
	testl	%edx, %edx
	jne	.LBB17_44
.LBB17_45:                              #   in Loop: Header=BB17_39 Depth=3
	cmpl	$0, vis_only-4(,%r15,4)
	je	.LBB17_47
# BB#46:                                #   in Loop: Header=BB17_39 Depth=3
	movzbl	43(%rbx), %edx
	shll	$16, %edx
	testl	$65536, %edx            # imm = 0x10000
	je	.LBB17_44
.LBB17_47:                              #   in Loop: Header=BB17_39 Depth=3
	cmpl	$0, body_ok-4(,%r15,4)
	jne	.LBB17_50
# BB#48:                                #   in Loop: Header=BB17_39 Depth=3
	cmpb	$-110, %al
	jne	.LBB17_50
# BB#49:                                #   in Loop: Header=BB17_39 Depth=3
	movzwl	41(%rcx), %eax
	testb	$1, %ah
	jne	.LBB17_44
.LBB17_50:                              #   in Loop: Header=BB17_39 Depth=3
	testb	$1, 11(%rsp)            # 1-byte Folded Reload
	je	.LBB17_53
# BB#51:                                #   in Loop: Header=BB17_39 Depth=3
	movl	$.L.str.23, %esi
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_53
# BB#52:                                #   in Loop: Header=BB17_39 Depth=3
	movl	$.L.str.24, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_53
	.p2align	4, 0x90
.LBB17_44:                              # %.backedge
                                        #   in Loop: Header=BB17_39 Depth=3
	movq	8(%r13), %r13
	cmpq	%rbp, %r13
	jne	.LBB17_39
.LBB17_36:                              # %._crit_edge
                                        #   in Loop: Header=BB17_25 Depth=2
	decq	%r15
	cmpq	72(%rsp), %r14          # 8-byte Folded Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	jne	.LBB17_25
	.p2align	4, 0x90
.LBB17_37:                              # %.loopexit138
                                        #   in Loop: Header=BB17_17 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rbp
	cmpq	48(%rsp), %rbp          # 8-byte Folded Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB17_17
.LBB17_38:
	xorl	%ebx, %ebx
.LBB17_53:                              # %.loopexit
	movq	%rbx, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	SearchSym, .Lfunc_end17-SearchSym
	.cfi_endproc

	.globl	SymName
	.p2align	4, 0x90
	.type	SymName,@function
SymName:                                # @SymName
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 16
.Lcfi77:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB18_1
# BB#2:
	movq	24(%rdi), %rbx
	.p2align	4, 0x90
.LBB18_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB18_3
# BB#4:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB18_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB18_6:                               # %.loopexit
	addq	$64, %rbx
	jmp	.LBB18_7
.LBB18_1:
	movl	$.L.str.25, %ebx
.LBB18_7:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	SymName, .Lfunc_end18-SymName
	.cfi_endproc

	.globl	FullSymName
	.p2align	4, 0x90
	.type	FullSymName,@function
FullSymName:                            # @FullSymName
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 240
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB19_1
# BB#2:
	movq	48(%r12), %rax
	testq	%rax, %rax
	jne	.LBB19_3
# BB#10:                                # %.preheader
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	48(%r12), %rax
	testq	%rax, %rax
	je	.LBB19_11
.LBB19_3:                               # %.lr.ph32.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph32
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, 16(%rsp,%r15,8)
	movq	%rax, %r12
	incq	%r15
	cmpq	$19, %r15
	jg	.LBB19_6
# BB#5:                                 # %.lr.ph32
                                        #   in Loop: Header=BB19_4 Depth=1
	movq	48(%r12), %rax
	testq	%rax, %rax
	jne	.LBB19_4
.LBB19_6:                               # %._crit_edge33
	movb	$0, FullSymName.buff(%rip)
	cmpl	$2, %r15d
	jl	.LBB19_20
# BB#7:                                 # %.lr.ph
	leaq	32(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB19_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_13 Depth 2
	movq	8(%rsp,%r15,8), %rax
	testq	%rax, %rax
	je	.LBB19_9
# BB#12:                                #   in Loop: Header=BB19_8 Depth=1
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB19_13:                              #   Parent Loop BB19_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB19_13
# BB#14:                                #   in Loop: Header=BB19_8 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB19_16
# BB#15:                                #   in Loop: Header=BB19_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB19_16:                              # %.loopexit.i
                                        #   in Loop: Header=BB19_8 Depth=1
	addq	$64, %rbx
	jmp	.LBB19_17
	.p2align	4, 0x90
.LBB19_9:                               #   in Loop: Header=BB19_8 Depth=1
	movl	$.L.str.25, %ebx
.LBB19_17:                              # %SymName.exit
                                        #   in Loop: Header=BB19_8 Depth=1
	decq	%r15
	movq	%rbx, FullSymName.sname(%rip)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	%r13, %rbp
	movl	$FullSymName.buff, %edi
	callq	strlen
	addq	%rbp, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB19_19
# BB#18:                                #   in Loop: Header=BB19_8 Depth=1
	movl	$29, %edi
	movl	$8, %esi
	movl	$.L.str.29, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	callq	Error
	movq	FullSymName.sname(%rip), %rbx
.LBB19_19:                              #   in Loop: Header=BB19_8 Depth=1
	movl	$FullSymName.buff, %edi
	movq	%rbx, %rsi
	callq	strcat
	movl	$FullSymName.buff, %edi
	movq	%r14, %rsi
	callq	strcat
	cmpq	$1, %r15
	jg	.LBB19_8
	jmp	.LBB19_20
.LBB19_1:
	movl	$.L.str.25, %r14d
	jmp	.LBB19_30
.LBB19_11:                              # %._crit_edge33.thread
	movb	$0, FullSymName.buff(%rip)
.LBB19_20:                              # %._crit_edge
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB19_21
# BB#22:
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB19_23:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB19_23
# BB#24:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB19_26
# BB#25:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB19_26:                              # %.loopexit.i21
	addq	$64, %rbx
	jmp	.LBB19_27
.LBB19_21:
	movl	$.L.str.25, %ebx
.LBB19_27:                              # %SymName.exit23
	movq	%rbx, FullSymName.sname(%rip)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movl	$FullSymName.buff, %edi
	callq	strlen
	addq	%rbp, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB19_29
# BB#28:
	addq	$32, %r12
	movl	$29, %edi
	movl	$9, %esi
	movl	$.L.str.29, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movq	FullSymName.sname(%rip), %rbx
.LBB19_29:
	movl	$FullSymName.buff, %r14d
	movl	$FullSymName.buff, %edi
	movq	%rbx, %rsi
	callq	strcat
.LBB19_30:
	movq	%r14, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	FullSymName, .Lfunc_end19-FullSymName
	.cfi_endproc

	.globl	ChildSym
	.p2align	4, 0x90
	.type	ChildSym,@function
ChildSym:                               # @ChildSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 48
.Lcfi96:
	.cfi_offset %rbx, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	8(%rdi), %rax
	cmpq	%rdi, %rax
	je	.LBB20_1
	.p2align	4, 0x90
.LBB20_7:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_8 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB20_8:                               #   Parent Loop BB20_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB20_8
# BB#9:                                 #   in Loop: Header=BB20_7 Depth=1
	cmpl	%r15d, %ecx
	jne	.LBB20_10
# BB#15:                                #   in Loop: Header=BB20_7 Depth=1
	cmpq	%rdi, 48(%rbp)
	je	.LBB20_14
.LBB20_10:                              # %.backedge
                                        #   in Loop: Header=BB20_7 Depth=1
	movq	8(%rax), %rax
	cmpq	%rdi, %rax
	jne	.LBB20_7
# BB#11:                                # %._crit_edge
	leaq	32(%rdi), %r14
	testq	%rdi, %rdi
	jne	.LBB20_2
# BB#12:
	movl	$.L.str.25, %ebx
	jmp	.LBB20_13
.LBB20_1:                               # %._crit_edge.thread
	leaq	32(%rdi), %r14
.LBB20_2:
	movq	24(%rdi), %rbx
	.p2align	4, 0x90
.LBB20_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB20_3
# BB#4:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB20_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB20_6:                               # %.loopexit.i
	addq	$64, %rbx
.LBB20_13:                              # %SymName.exit
	movl	%r15d, %edi
	callq	Image
	movq	%rax, (%rsp)
	xorl	%ebp, %ebp
	movl	$29, %edi
	movl	$10, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
.LBB20_14:                              # %.loopexit
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	ChildSym, .Lfunc_end20-ChildSym
	.cfi_endproc

	.globl	ChildSymWithCode
	.p2align	4, 0x90
	.type	ChildSymWithCode,@function
ChildSymWithCode:                       # @ChildSymWithCode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 48
.Lcfi105:
	.cfi_offset %rbx, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	80(%r14), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB21_2
	jmp	.LBB21_8
	.p2align	4, 0x90
.LBB21_7:                               # %.loopexit
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.LBB21_8
.LBB21_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_3 Depth 2
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB21_3:                               #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %edx
	testb	%dl, %dl
	je	.LBB21_3
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	cmpb	$-111, %dl
	jne	.LBB21_7
# BB#5:                                 #   in Loop: Header=BB21_2 Depth=1
	cmpq	%rax, 48(%rbp)
	jne	.LBB21_7
# BB#6:                                 #   in Loop: Header=BB21_2 Depth=1
	cmpb	%r15b, 125(%rbp)
	jne	.LBB21_7
	jmp	.LBB21_16
.LBB21_8:                               # %._crit_edge
	addq	$32, %r14
	testq	%rax, %rax
	je	.LBB21_9
# BB#10:
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB21_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB21_11
# BB#12:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB21_14
# BB#13:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB21_14:                              # %.loopexit.i
	addq	$64, %rbx
	jmp	.LBB21_15
.LBB21_9:
	movl	$.L.str.25, %ebx
.LBB21_15:                              # %SymName.exit
	movsbl	%r15b, %eax
	movl	%eax, (%rsp)
	xorl	%ebp, %ebp
	movl	$29, %edi
	movl	$11, %esi
	movl	$.L.str.31, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
.LBB21_16:                              # %.loopexit20
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	ChildSymWithCode, .Lfunc_end21-ChildSymWithCode
	.cfi_endproc

	.type	scope_top,@object       # @scope_top
	.local	scope_top
	.comm	scope_top,4,4
	.type	suppress_scope,@object  # @suppress_scope
	.local	suppress_scope
	.comm	suppress_scope,1,4
	.type	suppress_visible,@object # @suppress_visible
	.local	suppress_visible
	.comm	suppress_visible,4,4
	.type	symtab,@object          # @symtab
	.local	symtab
	.comm	symtab,28528,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"PushScope: suppress_scope!"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"scope depth limit exceeded"
	.size	.L.str.2, 27

	.type	scope,@object           # @scope
	.local	scope
	.comm	scope,2400,16
	.type	npars_only,@object      # @npars_only
	.local	npars_only
	.comm	npars_only,1200,16
	.type	vis_only,@object        # @vis_only
	.local	vis_only
	.comm	vis_only,1200,16
	.type	body_ok,@object         # @body_ok
	.local	body_ok
	.comm	body_ok,1200,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"PopScope: tried to pop empty scope stack"
	.size	.L.str.3, 41

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"PopScope: suppress_scope!"
	.size	.L.str.4, 26

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"word is too long"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"LoadScopeSnapshot: type(ss)!"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"LoadScopeSnapshot: type(x)!"
	.size	.L.str.7, 28

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"invalid symbol name %s"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"@Target"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"@Tag"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"@Optimize"
	.size	.L.str.11, 10

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@Key"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"@Merge"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"@Enclose"
	.size	.L.str.14, 9

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"@Filter"
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s must be a local definition"
	.size	.L.str.16, 30

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s must lie within a symbol with a right parameter"
	.size	.L.str.17, 51

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"a body parameter may not be named %s"
	.size	.L.str.18, 37

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"the right parameter of a galley may not be called %s"
	.size	.L.str.19, 53

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"symbol %s previously defined at%s"
	.size	.L.str.20, 34

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"symbol %s previously defined here"
	.size	.L.str.21, 34

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"symbol name %s previously defined at%s"
	.size	.L.str.22, 39

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"@Include"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"@SysInclude"
	.size	.L.str.24, 12

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"<nilobj>"
	.size	.L.str.25, 9

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SymName: !is_word(type(p))!"
	.size	.L.str.26, 28

	.type	FullSymName.buff,@object # @FullSymName.buff
	.local	FullSymName.buff
	.comm	FullSymName.buff,512,16
	.type	FullSymName.sname,@object # @FullSymName.sname
	.local	FullSymName.sname
	.comm	FullSymName.sname,8,8
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"FullSymName: enclosing(x) == nilobj!"
	.size	.L.str.27, 37

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"full name of symbol is too long"
	.size	.L.str.29, 32

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"symbol %s has missing %s"
	.size	.L.str.30, 25

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"symbol %s has erroneous code %c (database out of date?)"
	.size	.L.str.31, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
