	.text
	.file	"genalign11.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4608533498688228557     # double 1.3
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_2:
	.long	3463342888              # float -1.0E+9
.LCPI0_3:
	.long	3379831806              # float -999999.875
	.text
	.globl	genL__align11
	.p2align	4, 0x90
	.type	genL__align11,@function
genL__align11:                          # @genL__align11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	%edx, 128(%rsp)         # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_OP(%rip), %xmm0
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_ex(%rip), %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	offset(%rip), %xmm0
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movl	genL__align11.orlgth1(%rip), %r15d
	testl	%r15d, %r15d
	jne	.LBB0_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, genL__align11.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, genL__align11.mseq2(%rip)
	movl	genL__align11.orlgth1(%rip), %r15d
.LBB0_2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %r13
	movq	(%rbp), %rdi
	callq	strlen
	movq	%rax, %r9
	cmpl	%r15d, %r13d
	movl	genL__align11.orlgth2(%rip), %r12d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%rbp, %r8
	jg	.LBB0_4
# BB#3:
	cmpl	%r12d, %r9d
	jle	.LBB0_8
.LBB0_4:
	testl	%r15d, %r15d
	jle	.LBB0_7
# BB#5:
	testl	%r12d, %r12d
	jle	.LBB0_7
# BB#6:
	movq	genL__align11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.match(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	genL__align11.largeM(%rip), %rdi
	callq	FreeFloatVec
	movq	genL__align11.Mp(%rip), %rdi
	callq	FreeIntVec
	movq	genL__align11.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	genL__align11.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	genL__align11.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	genL__align11.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	genL__align11.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	genL__align11.orlgth1(%rip), %r15d
	movl	genL__align11.orlgth2(%rip), %r12d
.LBB0_7:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r9d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	leal	100(%r12), %r14d
	movq	%r13, 56(%rsp)          # 8-byte Spill
	leal	102(%r12), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.w1(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.w2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.match(%rip)
	leal	102(%r15), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.initverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.lastverticalw(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.m(%rip)
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, genL__align11.mp(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, genL__align11.largeM(%rip)
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, genL__align11.Mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r12,%r15), %esi
	callq	AllocateCharMtx
	movq	%rax, genL__align11.mseq(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, genL__align11.cpmx1(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	movq	56(%rsp), %r13          # 8-byte Reload
	callq	AllocateFloatMtx
	movq	%rax, genL__align11.cpmx2(%rip)
	cmpl	%r14d, %ebx
	cmovgel	%ebx, %r14d
	addl	$2, %r14d
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, genL__align11.floatwork(%rip)
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	%rax, genL__align11.intwork(%rip)
	movl	%r15d, genL__align11.orlgth1(%rip)
	movl	%r12d, genL__align11.orlgth2(%rip)
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB0_8:
	movq	genL__align11.mseq(%rip), %rax
	movq	(%rax), %rcx
	movq	genL__align11.mseq1(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	8(%rax), %rax
	movq	genL__align11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r15d
	movl	commonAlloc2(%rip), %ebp
	movss	32(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm10        # 16-byte Reload
	jg	.LBB0_11
# BB#9:
	cmpl	%ebp, %r12d
	jg	.LBB0_11
# BB#10:                                # %._crit_edge384
	movq	commonJP(%rip), %r14
	jmp	.LBB0_15
.LBB0_11:                               # %._crit_edge378
	testl	%ebx, %ebx
	je	.LBB0_14
# BB#12:                                # %._crit_edge378
	testl	%ebp, %ebp
	je	.LBB0_14
# BB#13:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movq	commonJP(%rip), %rdi
	callq	FreeIntMtx
	movl	genL__align11.orlgth1(%rip), %r15d
	movl	commonAlloc1(%rip), %ebx
	movl	genL__align11.orlgth2(%rip), %r12d
	movl	commonAlloc2(%rip), %ebp
.LBB0_14:
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	cmpl	%ebp, %r12d
	cmovgel	%r12d, %ebp
	leal	10(%rbx), %r12d
	leal	10(%rbp), %r14d
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	%rax, %r14
	movq	%r14, commonJP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movq	8(%rsp), %r8            # 8-byte Reload
	movss	32(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm10        # 16-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
.LBB0_15:
	movq	commonIP(%rip), %r11
	movq	%r11, genL__align11.ijpi(%rip)
	movq	%r14, genL__align11.ijpj(%rip)
	movq	genL__align11.w1(%rip), %r12
	movq	genL__align11.w2(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	je	.LBB0_22
# BB#16:                                # %.lr.ph.i
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movq	(%r8), %rax
	movq	genL__align11.initverticalw(%rip), %rsi
	movsbq	(%rax), %rdi
	testb	$1, %r13b
	jne	.LBB0_18
# BB#17:
	movl	%r13d, %ebp
	cmpl	$1, %r13d
	jne	.LBB0_20
	jmp	.LBB0_22
.LBB0_18:
	leal	-1(%r13), %ebp
	movsbq	(%rdx), %rax
	incq	%rdx
	movq	%rdi, %rcx
	shlq	$9, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rcx,%rax,4), %xmm0
	movss	%xmm0, (%rsi)
	addq	$4, %rsi
	cmpl	$1, %r13d
	je	.LBB0_22
.LBB0_20:                               # %.lr.ph.i.new
	shlq	$9, %rdi
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rdx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rax,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %ebp
	movsbq	1(%rdx), %rax
	leaq	2(%rdx), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rax,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_21
.LBB0_22:                               # %match_calc.exit
	movb	$1, 7(%rsp)             # 1-byte Folded Spill
	testl	%r9d, %r9d
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	je	.LBB0_50
# BB#23:                                # %.lr.ph.i309
	movq	(%r8), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movsbq	(%rax), %rsi
	testb	$1, %r9b
	jne	.LBB0_25
# BB#24:
	movq	%r12, %rcx
	movl	%r9d, %edi
	cmpl	$1, %r9d
	jne	.LBB0_27
	jmp	.LBB0_29
.LBB0_25:
	leal	-1(%r9), %edi
	movsbq	(%rdx), %rax
	incq	%rdx
	movq	%rsi, %rcx
	shlq	$9, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rcx,%rax,4), %xmm0
	leaq	4(%r12), %rcx
	movss	%xmm0, (%r12)
	cmpl	$1, %r9d
	je	.LBB0_29
.LBB0_27:                               # %.lr.ph.i309.new
	shlq	$9, %rsi
	.p2align	4, 0x90
.LBB0_28:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rdx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rax,4), %xmm0
	movss	%xmm0, (%rcx)
	addl	$-2, %edi
	movsbq	1(%rdx), %rax
	leaq	2(%rdx), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rax,4), %xmm0
	movss	%xmm0, 4(%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB0_28
.LBB0_29:                               # %match_calc.exit314.preheader
	testl	%r9d, %r9d
	setle	%al
	jle	.LBB0_50
# BB#30:                                # %.lr.ph363
	movq	genL__align11.m(%rip), %rsi
	movq	genL__align11.mp(%rip), %rdi
	movq	genL__align11.largeM(%rip), %rbp
	movq	genL__align11.Mp(%rip), %rbx
	leaq	1(%r9), %r10
	movl	%r10d, %edx
	leaq	-1(%rdx), %r14
	cmpq	$8, %r14
	jb	.LBB0_31
# BB#38:                                # %min.iters.checked
	movl	%r9d, %ecx
	andl	$7, %ecx
	movq	%r14, %r15
	subq	%rcx, %r15
	je	.LBB0_31
# BB#39:                                # %vector.memcheck
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movb	%al, 80(%rsp)           # 1-byte Spill
	leaq	4(%rsi), %r8
	leaq	(%rsi,%rdx,4), %rcx
	leaq	4(%rbp), %r11
	leaq	(%rbp,%rdx,4), %r14
	cmpq	%r14, %r8
	sbbb	%al, %al
	cmpq	%rcx, %r11
	sbbb	%r9b, %r9b
	andb	%al, %r9b
	leaq	-4(%r12,%rdx,4), %rax
	cmpq	%rax, %r8
	sbbb	%r8b, %r8b
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	movb	%cl, 56(%rsp)           # 1-byte Spill
	cmpq	%rax, %r11
	leaq	4(%rdi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rdx,4), %r11
	sbbb	%cl, %cl
	cmpq	%r14, %r12
	sbbb	%al, %al
	movb	%al, 120(%rsp)          # 1-byte Spill
	cmpq	%r11, 24(%rsp)          # 8-byte Folded Reload
	leaq	(%rdi,%rdx,4), %r11
	leaq	4(%rbx), %r14
	sbbb	%al, %al
	cmpq	%r11, %r14
	sbbb	%r14b, %r14b
	testb	$1, %r9b
	movl	$1, %r11d
	jne	.LBB0_40
# BB#41:                                # %vector.memcheck
	andb	56(%rsp), %r8b          # 1-byte Folded Reload
	andb	$1, %r8b
	jne	.LBB0_40
# BB#42:                                # %vector.memcheck
	movb	%r14b, 24(%rsp)         # 1-byte Spill
	andb	120(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_43
# BB#44:                                # %vector.memcheck
	andb	24(%rsp), %al           # 1-byte Folded Reload
	andb	$1, %al
	movb	80(%rsp), %cl           # 1-byte Reload
	jne	.LBB0_32
# BB#45:                                # %vector.body.preheader
	leaq	1(%r15), %r11
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_46:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rax,4), %xmm1
	movups	16(%r12,%rax,4), %xmm2
	movups	%xmm1, 4(%rsi,%rax,4)
	movups	%xmm2, 20(%rsi,%rax,4)
	movupd	%xmm0, 4(%rdi,%rax,4)
	movupd	%xmm0, 20(%rdi,%rax,4)
	movupd	(%r12,%rax,4), %xmm1
	movups	16(%r12,%rax,4), %xmm2
	movupd	%xmm1, 4(%rbp,%rax,4)
	movups	%xmm2, 20(%rbp,%rax,4)
	movupd	%xmm0, 4(%rbx,%rax,4)
	movupd	%xmm0, 20(%rbx,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB0_46
# BB#47:                                # %middle.block
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_32
	jmp	.LBB0_48
.LBB0_31:
	movl	%eax, %ecx
	movl	$1, %r11d
.LBB0_32:                               # %match_calc.exit314.preheader465
	subl	%r11d, %r10d
	testb	$1, %r10b
	movq	%r11, %rax
	je	.LBB0_34
# BB#33:                                # %match_calc.exit314.prol
	movl	-4(%r12,%r11,4), %eax
	movl	%eax, (%rsi,%r11,4)
	movl	$0, (%rdi,%r11,4)
	movl	-4(%r12,%r11,4), %eax
	movl	%eax, (%rbp,%r11,4)
	movl	$0, (%rbx,%r11,4)
	leaq	1(%r11), %rax
.LBB0_34:                               # %match_calc.exit314.prol.loopexit
	cmpq	%r11, %r14
	jne	.LBB0_35
.LBB0_48:
	movb	%cl, 7(%rsp)            # 1-byte Spill
	jmp	.LBB0_49
.LBB0_35:                               # %match_calc.exit314.preheader465.new
	movl	%ecx, %r10d
	.p2align	4, 0x90
.LBB0_36:                               # %match_calc.exit314
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r12,%rax,4), %ecx
	movl	%ecx, (%rsi,%rax,4)
	movl	$0, (%rdi,%rax,4)
	movl	-4(%r12,%rax,4), %ecx
	movl	%ecx, (%rbp,%rax,4)
	movl	$0, (%rbx,%rax,4)
	movl	(%r12,%rax,4), %ecx
	movl	%ecx, 4(%rsi,%rax,4)
	movl	$0, 4(%rdi,%rax,4)
	movl	(%r12,%rax,4), %ecx
	movl	%ecx, 4(%rbp,%rax,4)
	movl	$0, 4(%rbx,%rax,4)
	addq	$2, %rax
	cmpq	%rax, %rdx
	jne	.LBB0_36
# BB#37:
	movb	%r10b, 7(%rsp)          # 1-byte Spill
.LBB0_49:                               # %match_calc.exit314._crit_edge
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
.LBB0_50:                               # %match_calc.exit314._crit_edge
	movq	%r9, %rax
	shlq	$32, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$30, %rax
	movl	(%r12,%rax), %eax
	movq	genL__align11.lastverticalw(%rip), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movl	%eax, (%rdx)
	leal	1(%r13,%r9), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	%eax, localstop(%rip)
	testl	%r13d, %r13d
	jle	.LBB0_51
# BB#52:                                # %.lr.ph355
	xorps	.LCPI0_0(%rip), %xmm10
	sarq	$32, %rcx
	incl	%r13d
	movq	genL__align11.initverticalw(%rip), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r13, %rax
	movq	genL__align11.m(%rip), %r13
	movq	genL__align11.largeM(%rip), %rdi
	movq	genL__align11.mp(%rip), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	genL__align11.Mp(%rip), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	%r9d, %eax
	andl	$1, %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	leal	-1(%r9), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%r15d, %r15d
	movl	$1, %esi
	movss	.LCPI0_3(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %r14
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_53:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_58 Depth 2
                                        #     Child Loop BB0_62 Depth 2
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movq	%r12, %rdx
	movq	72(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	movq	168(%rsp), %r9          # 8-byte Reload
	movl	-4(%r9,%rsi,4), %eax
	movl	%eax, (%rdx)
	movq	%rsi, %r12
	je	.LBB0_59
# BB#54:                                # %.lr.ph.i315
                                        #   in Loop: Header=BB0_53 Depth=1
	cmpl	$0, 132(%rsp)           # 4-byte Folded Reload
	movq	(%r8), %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movsbq	(%rax,%r12), %rbx
	jne	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_53 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%edi, %ebp
	cmpl	$1, %edi
	jne	.LBB0_58
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_56:                               #   in Loop: Header=BB0_53 Depth=1
	movsbq	(%rcx), %rax
	incq	%rcx
	movq	%rbx, %rsi
	shlq	$9, %rsi
	cvtsi2ssl	amino_dis(%rsi,%rax,4), %xmm1
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi), %rax
	movss	%xmm1, (%rsi)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %ebp
	cmpl	$1, %edi
	je	.LBB0_59
	.p2align	4, 0x90
.LBB0_58:                               #   Parent Loop BB0_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rcx), %rsi
	movq	%rbx, %rdi
	shlq	$9, %rdi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm1
	movss	%xmm1, (%rax)
	addl	$-2, %ebp
	movsbq	1(%rcx), %rsi
	leaq	2(%rcx), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm1
	movss	%xmm1, 4(%rax)
	leaq	8(%rax), %rax
	jne	.LBB0_58
.LBB0_59:                               # %match_calc.exit320
                                        #   in Loop: Header=BB0_53 Depth=1
	movl	(%r9,%r12,4), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	(%rdx), %eax
	movl	%eax, genL__align11.mi(%rip)
	movl	%eax, genL__align11.Mi(%rip)
	xorl	%r9d, %r9d
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_53 Depth=1
	xorl	%r10d, %r10d
	movq	104(%rsp), %r15         # 8-byte Reload
	jmp	.LBB0_80
	.p2align	4, 0x90
.LBB0_61:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_53 Depth=1
	leaq	-1(%r12), %rbp
	movd	%eax, %xmm1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rsi
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	(%r11,%r12,8), %rcx
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	movdqa	%xmm1, %xmm2
	movdqa	%xmm1, %xmm5
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movaps	%xmm11, %xmm3
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	120(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_131:                              # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=2
	movss	4(%rdx,%r14,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movq	%rax, %r14
	movaps	%xmm4, %xmm0
.LBB0_62:                               # %.lr.ph
                                        #   Parent Loop BB0_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, 4(%rcx,%r14,4)
	movaps	%xmm9, %xmm4
	addss	%xmm2, %xmm4
	ucomiss	%xmm5, %xmm4
	movl	%r14d, %eax
	cmoval	%r10d, %eax
	maxss	%xmm5, %xmm4
	movl	%eax, 4(%rsi,%r14,4)
	ucomiss	%xmm2, %xmm5
	jbe	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_62 Depth=2
	movss	%xmm5, genL__align11.mi(%rip)
	movaps	%xmm5, %xmm2
	movl	%r14d, %r10d
.LBB0_64:                               #   in Loop: Header=BB0_62 Depth=2
	addss	%xmm7, %xmm2
	movss	%xmm2, genL__align11.mi(%rip)
	movss	4(%r13,%r14,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm6
	addss	%xmm5, %xmm6
	ucomiss	%xmm4, %xmm6
	jbe	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_62 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%r14,4), %eax
	movl	%eax, 4(%rcx,%r14,4)
	movl	%r14d, 4(%rsi,%r14,4)
	movaps	%xmm6, %xmm4
.LBB0_66:                               #   in Loop: Header=BB0_62 Depth=2
	movss	(%rdx,%r14,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm6
	jbe	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_62 Depth=2
	movss	%xmm6, 4(%r13,%r14,4)
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%ebp, 4(%rax,%r14,4)
	movaps	%xmm6, %xmm5
.LBB0_68:                               #   in Loop: Header=BB0_62 Depth=2
	addss	%xmm7, %xmm5
	movss	%xmm5, 4(%r13,%r14,4)
	movaps	%xmm8, %xmm5
	addss	%xmm3, %xmm5
	ucomiss	%xmm4, %xmm5
	jbe	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_62 Depth=2
	movl	%r12d, 4(%rcx,%r14,4)
	movl	%r11d, 4(%rsi,%r14,4)
	movaps	%xmm5, %xmm4
.LBB0_70:                               #   in Loop: Header=BB0_62 Depth=2
	ucomiss	%xmm3, %xmm1
	movdqa	%xmm1, %xmm5
	maxss	%xmm3, %xmm5
	cmoval	%ebp, %r12d
	cmoval	%r9d, %r11d
	movss	4(%rdi,%r14,4), %xmm6   # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm6
	movaps	%xmm5, %xmm3
	jbe	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_62 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%r14,4), %r12d
	movaps	%xmm6, %xmm3
	movl	%r14d, %r11d
.LBB0_72:                               #   in Loop: Header=BB0_62 Depth=2
	movd	(%rdx,%r14,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm5
	jbe	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_62 Depth=2
	movd	%xmm5, 4(%rdi,%r14,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%ebp, 4(%rax,%r14,4)
	movd	(%rdx,%r14,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
.LBB0_74:                               #   in Loop: Header=BB0_62 Depth=2
	leaq	1(%r14), %rax
	ucomiss	%xmm1, %xmm5
	jbe	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_62 Depth=2
	movd	%xmm5, genL__align11.Mi(%rip)
	movl	%r14d, %r9d
	movdqa	%xmm5, %xmm1
.LBB0_76:                               #   in Loop: Header=BB0_62 Depth=2
	ucomiss	%xmm0, %xmm4
	cmoval	24(%rsp), %ebx          # 4-byte Folded Reload
	cmoval	%eax, %r15d
	ucomiss	%xmm4, %xmm10
	movaps	%xmm4, %xmm5
	jbe	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_62 Depth=2
	movl	112(%rsp), %edi         # 4-byte Reload
	movl	%edi, 4(%rcx,%r14,4)
	movq	120(%rsp), %rdi         # 8-byte Reload
	movaps	%xmm10, %xmm5
.LBB0_78:                               #   in Loop: Header=BB0_62 Depth=2
	maxss	%xmm0, %xmm4
	movq	32(%rsp), %r8           # 8-byte Reload
	addss	4(%r8,%r14,4), %xmm5
	movss	%xmm5, 4(%r8,%r14,4)
	cmpl	%r14d, 16(%rsp)         # 4-byte Folded Reload
	jne	.LBB0_131
# BB#79:                                #   in Loop: Header=BB0_53 Depth=1
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movaps	%xmm4, %xmm0
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB0_80:                               # %._crit_edge
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r14,4), %eax
	movq	176(%rsp), %rsi         # 8-byte Reload
	movl	%eax, (%rsi,%r12,4)
	incq	%r12
	cmpq	160(%rsp), %r12         # 8-byte Folded Reload
	movq	%r12, %rsi
	movq	%rcx, %r12
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_53
# BB#81:                                # %._crit_edge356
	movl	%r9d, genL__align11.Mpi(%rip)
	movl	%r10d, genL__align11.mpi(%rip)
	movl	localstop(%rip), %ebx
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	jmp	.LBB0_82
.LBB0_51:
	xorl	%eax, %eax
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%r15d, %r15d
	movl	112(%rsp), %ebx         # 4-byte Reload
.LBB0_82:
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%eax, %rcx
	movq	(%r11,%rcx,8), %rax
	movslq	%r15d, %rdx
	cmpl	%ebx, (%rax,%rdx,4)
	jne	.LBB0_84
# BB#83:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movb	$0, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movb	$0, (%rax)
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	jmp	.LBB0_130
.LBB0_84:
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	genL__align11.mseq1(%rip), %r15
	movq	genL__align11.mseq2(%rip), %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	testl	%r12d, %r12d
	js	.LBB0_91
# BB#85:                                # %.lr.ph30.i
	movq	%r12, %rdi
	incq	%rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r8
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB0_88
# BB#86:                                # %.prol.preheader
	movq	48(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_87:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rbp
	movl	%ebx, (%rbp)
	movq	(%r14,%rsi,8), %rbp
	movl	%ebx, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_87
.LBB0_88:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_91
# BB#89:                                # %.lr.ph30.i.new
	subq	%rsi, %rcx
	leaq	24(%r14,%rsi,8), %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	24(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_90:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rdi
	movl	%ebx, (%rdi)
	movq	-24(%rdx), %rdi
	movl	%ebx, (%rdi)
	movq	-16(%rsi), %rdi
	movl	%ebx, (%rdi)
	movq	-16(%rdx), %rdi
	movl	%ebx, (%rdi)
	movq	-8(%rsi), %rdi
	movl	%ebx, (%rdi)
	movq	-8(%rdx), %rdi
	movl	%ebx, (%rdi)
	movq	(%rsi), %rdi
	movl	%ebx, (%rdi)
	movq	(%rdx), %rdi
	movl	%ebx, (%rdi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB0_90
.LBB0_91:                               # %.preheader.i
	testl	%eax, %eax
	js	.LBB0_106
# BB#92:                                # %.lr.ph26.i
	movl	localstop(%rip), %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r9
	movq	(%r14), %r10
	movq	%rax, %rbp
	incq	%rbp
	movl	%ebp, %r11d
	cmpq	$7, %r11
	jbe	.LBB0_93
# BB#100:                               # %min.iters.checked440
	movl	%ebp, %r8d
	andl	$7, %r8d
	movq	%r11, %rdx
	subq	%r8, %rdx
	je	.LBB0_93
# BB#101:                               # %vector.memcheck453
	leaq	(%r10,%r11,4), %rsi
	cmpq	%rsi, %r9
	jae	.LBB0_103
# BB#102:                               # %vector.memcheck453
	leaq	(%r9,%r11,4), %rsi
	cmpq	%rsi, %r10
	jae	.LBB0_103
.LBB0_93:
	xorl	%edx, %edx
.LBB0_94:                               # %scalar.ph438.preheader
	subl	%edx, %ebp
	leaq	-1(%r11), %r8
	subq	%rdx, %r8
	andq	$7, %rbp
	je	.LBB0_97
# BB#95:                                # %scalar.ph438.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB0_96:                               # %scalar.ph438.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%r9,%rdx,4)
	movl	%ecx, (%r10,%rdx,4)
	incq	%rdx
	incq	%rbp
	jne	.LBB0_96
.LBB0_97:                               # %scalar.ph438.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_106
# BB#98:                                # %scalar.ph438.preheader.new
	subq	%rdx, %r11
	leaq	28(%r10,%rdx,4), %rdi
	leaq	28(%r9,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_99:                               # %scalar.ph438
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, -28(%rdx)
	movl	%ecx, -28(%rdi)
	movl	%ecx, -24(%rdx)
	movl	%ecx, -24(%rdi)
	movl	%ecx, -20(%rdx)
	movl	%ecx, -20(%rdi)
	movl	%ecx, -16(%rdx)
	movl	%ecx, -16(%rdi)
	movl	%ecx, -12(%rdx)
	movl	%ecx, -12(%rdi)
	movl	%ecx, -8(%rdx)
	movl	%ecx, -8(%rdi)
	movl	%ecx, -4(%rdx)
	movl	%ecx, -4(%rdi)
	movl	%ecx, (%rdx)
	movl	%ecx, (%rdi)
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-8, %r11
	jne	.LBB0_99
.LBB0_106:                              # %._crit_edge27.i
	leal	(%rax,%r12), %ecx
	movq	(%r15), %rax
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, (%r15)
	movb	$0, (%rax,%rcx)
	movq	(%r13), %rax
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, (%r13)
	movb	$0, (%rax,%rcx)
	testl	%ecx, %ecx
	movl	$0, %r12d
	movl	$0, %r11d
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	js	.LBB0_126
# BB#107:                               # %.lr.ph12.i
	movl	localstop(%rip), %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %eax
	xorl	%r8d, %r8d
	movl	%esi, 24(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_108:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_113 Depth 2
                                        #     Child Loop BB0_119 Depth 2
	movl	%eax, %r12d
	movslq	%edx, %rax
	movslq	%edi, %rcx
	movq	(%r14,%rax,8), %rsi
	movl	(%rsi,%rcx,4), %r11d
	subl	%r12d, %eax
	decl	%eax
	je	.LBB0_109
# BB#110:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_108 Depth=1
	movslq	%eax, %rbp
	movslq	%r12d, %rax
	movq	%rdx, %r10
	leal	-2(%rdx), %ecx
	testb	$1, %bpl
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rdi, %r9
	je	.LBB0_112
# BB#111:                               # %.lr.ph.i308.prol
                                        #   in Loop: Header=BB0_108 Depth=1
	movq	(%rbx), %rdi
	leaq	(%rbp,%rax), %rsi
	movb	(%rdi,%rsi), %r14b
	movq	(%r15), %rdi
	leaq	-1(%rdi), %rsi
	movq	%rsi, (%r15)
	movb	%r14b, -1(%rdi)
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r13)
	movb	$45, -1(%rsi)
	decq	%rbp
.LBB0_112:                              # %.lr.ph.i308.prol.loopexit
                                        #   in Loop: Header=BB0_108 Depth=1
	decl	%r8d
	cmpl	%r12d, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB0_114
	.p2align	4, 0x90
.LBB0_113:                              # %.lr.ph.i308
                                        #   Parent Loop BB0_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rcx
	addq	%rax, %rcx
	movzbl	(%rbp,%rcx), %ecx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%cl, -1(%rsi)
	movq	(%r13), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r13)
	movb	$45, -1(%rcx)
	movq	(%rbx), %rcx
	addq	%rax, %rcx
	movzbl	-1(%rbp,%rcx), %ecx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%cl, -1(%rsi)
	movq	(%r13), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r13)
	movb	$45, -1(%rcx)
	addq	$-2, %rbp
	testl	%ebp, %ebp
	jne	.LBB0_113
.LBB0_114:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_108 Depth=1
	addl	%r10d, %r8d
	subl	%r12d, %r8d
	movq	%r9, %rdi
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_109:                              #   in Loop: Header=BB0_108 Depth=1
	movq	%rdx, %r10
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB0_115:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_108 Depth=1
	movslq	%r11d, %rbp
	movl	%edi, %eax
	subl	%ebp, %eax
	decl	%eax
	je	.LBB0_121
# BB#116:                               # %.lr.ph5.preheader.i
                                        #   in Loop: Header=BB0_108 Depth=1
	cltq
	movq	%rdi, %r9
	leal	-2(%rdi), %ecx
	testb	$1, %al
	je	.LBB0_118
# BB#117:                               # %.lr.ph5.i.prol
                                        #   in Loop: Header=BB0_108 Depth=1
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	$45, -1(%rsi)
	movq	(%rdx), %rsi
	leaq	(%rax,%rbp), %rdi
	movb	(%rsi,%rdi), %r14b
	movq	(%r13), %rdi
	leaq	-1(%rdi), %rsi
	movq	%rsi, (%r13)
	movb	%r14b, -1(%rdi)
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	decq	%rax
.LBB0_118:                              # %.lr.ph5.i.prol.loopexit
                                        #   in Loop: Header=BB0_108 Depth=1
	cmpl	%r11d, %ecx
	je	.LBB0_120
	.p2align	4, 0x90
.LBB0_119:                              # %.lr.ph5.i
                                        #   Parent Loop BB0_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rcx)
	movq	(%rdx), %rcx
	addq	%rbp, %rcx
	movzbl	(%rax,%rcx), %ecx
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r13)
	movb	%cl, -1(%rsi)
	movq	(%r15), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rcx)
	movq	(%rdx), %rcx
	addq	%rbp, %rcx
	movzbl	-1(%rax,%rcx), %ecx
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r13)
	movb	%cl, -1(%rsi)
	addq	$-2, %rax
	testl	%eax, %eax
	jne	.LBB0_119
.LBB0_120:                              # %._crit_edge6.loopexit.i
                                        #   in Loop: Header=BB0_108 Depth=1
	movq	%r9, %rdi
	leal	-1(%rdi), %eax
	subl	%r11d, %eax
	addl	%eax, %r8d
.LBB0_121:                              # %._crit_edge6.i
                                        #   in Loop: Header=BB0_108 Depth=1
	testl	%r10d, %r10d
	jle	.LBB0_126
# BB#122:                               # %._crit_edge6.i
                                        #   in Loop: Header=BB0_108 Depth=1
	testl	%edi, %edi
	jle	.LBB0_126
# BB#123:                               #   in Loop: Header=BB0_108 Depth=1
	movq	(%rbx), %rax
	movslq	%r12d, %rcx
	movb	(%rax,%rcx), %al
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%al, -1(%rsi)
	movq	(%rdx), %rax
	movb	(%rax,%rbp), %al
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r13)
	movb	%al, -1(%rsi)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rbp,4), %eax
	movl	24(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	je	.LBB0_126
# BB#124:                               #   in Loop: Header=BB0_108 Depth=1
	addl	$2, %r8d
	cmpl	32(%rsp), %r8d          # 4-byte Folded Reload
	jg	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_108 Depth=1
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	(%r14,%rcx,8), %rcx
	cmpl	%esi, (%rcx,%rbp,4)
	movl	%r12d, %edx
	movl	%r11d, %edi
	jne	.LBB0_108
.LBB0_126:                              # %gentracking.exit
	cmpl	$-1, %r12d
	movl	$0, %ecx
	cmovel	%ecx, %r12d
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	cmpl	$-1, %r11d
	cmovel	%ecx, %r11d
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	%r11d, (%rax)
	movq	(%r15), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	128(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB0_128
# BB#127:                               # %gentracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_128
.LBB0_129:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	movq	genL__align11.mseq2(%rip), %rax
	movq	(%rax), %rsi
	callq	strcpy
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB0_130:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_103:                              # %vector.ph454
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r9), %rsi
	leaq	16(%r10), %rdi
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB0_104:                              # %vector.body436
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbx
	jne	.LBB0_104
# BB#105:                               # %middle.block437
	testq	%r8, %r8
	movq	64(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_94
	jmp	.LBB0_106
.LBB0_128:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.2, %edi
	callq	ErrorExit
	movq	genL__align11.mseq1(%rip), %rax
	movq	(%rax), %rbp
	jmp	.LBB0_129
.LBB0_40:
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movb	80(%rsp), %cl           # 1-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_32
.LBB0_43:
	movb	80(%rsp), %cl           # 1-byte Reload
	jmp	.LBB0_32
.Lfunc_end0:
	.size	genL__align11, .Lfunc_end0-genL__align11
	.cfi_endproc

	.type	genL__align11.mi,@object # @genL__align11.mi
	.local	genL__align11.mi
	.comm	genL__align11.mi,4,4
	.type	genL__align11.m,@object # @genL__align11.m
	.local	genL__align11.m
	.comm	genL__align11.m,8,8
	.type	genL__align11.Mi,@object # @genL__align11.Mi
	.local	genL__align11.Mi
	.comm	genL__align11.Mi,4,4
	.type	genL__align11.largeM,@object # @genL__align11.largeM
	.local	genL__align11.largeM
	.comm	genL__align11.largeM,8,8
	.type	genL__align11.ijpi,@object # @genL__align11.ijpi
	.local	genL__align11.ijpi
	.comm	genL__align11.ijpi,8,8
	.type	genL__align11.ijpj,@object # @genL__align11.ijpj
	.local	genL__align11.ijpj
	.comm	genL__align11.ijpj,8,8
	.type	genL__align11.mpi,@object # @genL__align11.mpi
	.local	genL__align11.mpi
	.comm	genL__align11.mpi,4,4
	.type	genL__align11.mp,@object # @genL__align11.mp
	.local	genL__align11.mp
	.comm	genL__align11.mp,8,8
	.type	genL__align11.Mpi,@object # @genL__align11.Mpi
	.local	genL__align11.Mpi
	.comm	genL__align11.Mpi,4,4
	.type	genL__align11.Mp,@object # @genL__align11.Mp
	.local	genL__align11.Mp
	.comm	genL__align11.Mp,8,8
	.type	genL__align11.w1,@object # @genL__align11.w1
	.local	genL__align11.w1
	.comm	genL__align11.w1,8,8
	.type	genL__align11.w2,@object # @genL__align11.w2
	.local	genL__align11.w2
	.comm	genL__align11.w2,8,8
	.type	genL__align11.match,@object # @genL__align11.match
	.local	genL__align11.match
	.comm	genL__align11.match,8,8
	.type	genL__align11.initverticalw,@object # @genL__align11.initverticalw
	.local	genL__align11.initverticalw
	.comm	genL__align11.initverticalw,8,8
	.type	genL__align11.lastverticalw,@object # @genL__align11.lastverticalw
	.local	genL__align11.lastverticalw
	.comm	genL__align11.lastverticalw,8,8
	.type	genL__align11.mseq1,@object # @genL__align11.mseq1
	.local	genL__align11.mseq1
	.comm	genL__align11.mseq1,8,8
	.type	genL__align11.mseq2,@object # @genL__align11.mseq2
	.local	genL__align11.mseq2
	.comm	genL__align11.mseq2,8,8
	.type	genL__align11.mseq,@object # @genL__align11.mseq
	.local	genL__align11.mseq
	.comm	genL__align11.mseq,8,8
	.type	genL__align11.cpmx1,@object # @genL__align11.cpmx1
	.local	genL__align11.cpmx1
	.comm	genL__align11.cpmx1,8,8
	.type	genL__align11.cpmx2,@object # @genL__align11.cpmx2
	.local	genL__align11.cpmx2
	.comm	genL__align11.cpmx2,8,8
	.type	genL__align11.intwork,@object # @genL__align11.intwork
	.local	genL__align11.intwork
	.comm	genL__align11.intwork,8,8
	.type	genL__align11.floatwork,@object # @genL__align11.floatwork
	.local	genL__align11.floatwork
	.comm	genL__align11.floatwork,8,8
	.type	genL__align11.orlgth1,@object # @genL__align11.orlgth1
	.local	genL__align11.orlgth1
	.comm	genL__align11.orlgth1,4,4
	.type	genL__align11.orlgth2,@object # @genL__align11.orlgth2
	.local	genL__align11.orlgth2
	.comm	genL__align11.orlgth2,4,4
	.type	localstop,@object       # @localstop
	.local	localstop
	.comm	localstop,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.2, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
