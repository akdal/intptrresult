	.text
	.file	"err.bc"
	.globl	_Z10fatalerroriz
	.p2align	4, 0x90
	.type	_Z10fatalerroriz,@function
_Z10fatalerroriz:                       # @_Z10fatalerroriz
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 224
.Lcfi2:
	.cfi_offset %rbx, -16
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movq	stderr(%rip), %rax
	movslq	%edi, %rbx
	movq	errmsgs(,%rbx,8), %rsi
	movq	%rsp, %rdx
	movq	%rax, %rdi
	callq	vfprintf
	movl	%ebx, %edi
	callq	exit
.Lfunc_end0:
	.size	_Z10fatalerroriz, .Lfunc_end0-_Z10fatalerroriz
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Unknown error."
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Out of memory error."
	.size	.L.str.1, 21

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Usage: %s [-O] [-D] InFile.class [OutFile.java]\n"
	.size	.L.str.2, 49

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Usage: %s [-D] -Ifuncname InFile.class\n"
	.size	.L.str.3, 40

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Not a class."
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Unsupported Class Version."
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"3"
	.size	.L.str.6, 2

	.type	errmsgs,@object         # @errmsgs
	.data
	.globl	errmsgs
	.p2align	4
errmsgs:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.size	errmsgs, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
