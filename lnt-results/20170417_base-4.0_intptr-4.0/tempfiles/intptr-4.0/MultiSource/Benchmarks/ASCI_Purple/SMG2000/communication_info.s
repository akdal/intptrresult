	.text
	.file	"communication_info.bc"
	.globl	hypre_CreateCommInfoFromStencil
	.p2align	4, 0x90
	.type	hypre_CreateCommInfoFromStencil,@function
hypre_CreateCommInfoFromStencil:        # @hypre_CreateCommInfoFromStencil
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi6:
	.cfi_def_cfa_offset 320
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 232(%rsp)          # 8-byte Spill
	movq	%r8, 224(%rsp)          # 8-byte Spill
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	%rsi, %r14
	movq	8(%rdi), %rbp
	movq	24(%rdi), %r15
	movl	8(%rbp), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	8(%rbp), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	8(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	8(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	(%r14), %r14
	movq	(%r15), %rbx
	movq	%r15, 192(%rsp)         # 8-byte Spill
	movq	8(%r15), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, %r13
	callq	hypre_BoxCreate
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	8(%rbx), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movl	8(%rbx), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rbp, %rcx
	movq	%r14, %rdx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%rcx)
	jle	.LBB0_68
# BB#1:                                 # %.lr.ph602
	xorl	%ebp, %ebp
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_6 Depth 3
                                        #         Child Loop BB0_8 Depth 4
                                        #           Child Loop BB0_9 Depth 5
                                        #             Child Loop BB0_12 Depth 6
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_29 Depth 2
                                        #       Child Loop BB0_31 Depth 3
                                        #     Child Loop BB0_36 Depth 2
                                        #       Child Loop BB0_38 Depth 3
                                        #         Child Loop BB0_40 Depth 4
                                        #           Child Loop BB0_41 Depth 5
                                        #             Child Loop BB0_44 Depth 6
                                        #     Child Loop BB0_55 Depth 2
                                        #     Child Loop BB0_62 Depth 2
                                        #       Child Loop BB0_64 Depth 3
	movq	(%rcx), %rax
	leaq	(%rbp,%rbp,2), %rcx
	leaq	(%rax,%rcx,8), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movl	(%rax,%rcx,8), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, 4(%rsi)
	movl	8(%rax,%rcx,8), %edx
	movl	%edx, 8(%rsi)
	movl	12(%rax,%rcx,8), %edx
	movl	%edx, 12(%rsi)
	movl	16(%rax,%rcx,8), %edx
	movl	%edx, 16(%rsi)
	movl	20(%rax,%rcx,8), %eax
	movl	%eax, 20(%rsi)
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jle	.LBB0_24
# BB#3:                                 # %.lr.ph536.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph536
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_6 Depth 3
                                        #         Child Loop BB0_8 Depth 4
                                        #           Child Loop BB0_9 Depth 5
                                        #             Child Loop BB0_12 Depth 6
	leaq	(%rdi,%rdi,2), %rax
	movq	96(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx,%rax,4), %ecx
	movq	184(%rsp), %rsi         # 8-byte Reload
	addl	(%rsi), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx)
	movl	(%rbx,%rax,4), %ecx
	addl	12(%rsi), %ecx
	movl	%ecx, 12(%rdx)
	movl	4(%rbx,%rax,4), %ecx
	addl	4(%rsi), %ecx
	movl	%ecx, 4(%rdx)
	movl	4(%rbx,%rax,4), %ecx
	addl	16(%rsi), %ecx
	movl	%ecx, 16(%rdx)
	movl	8(%rbx,%rax,4), %ecx
	addl	8(%rsi), %ecx
	movl	%ecx, 8(%rdx)
	movl	8(%rbx,%rax,4), %ecx
	addl	20(%rsi), %ecx
	movl	%ecx, 20(%rdx)
	movl	8(%rbx,%rax,4), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	xorl	%esi, %esi
	testl	%edx, %edx
	setg	%sil
	cmpl	%esi, %ecx
	jg	.LBB0_19
# BB#5:                                 # %.preheader507.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=2
	movl	(%rbx,%rax,4), %r8d
	movl	%r8d, %esi
	sarl	$31, %esi
	movl	4(%rbx,%rax,4), %r9d
	movl	%r9d, %eax
	sarl	$31, %eax
	xorl	%ebx, %ebx
	testl	%edx, %edx
	setg	%bl
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	testl	%r9d, %r9d
	setg	%dl
	xorl	%ebx, %ebx
	testl	%r8d, %r8d
	setg	%bl
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	cmpl	%edx, %eax
	setg	%dl
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	cmpl	%ebx, %esi
	setg	%bl
	movslq	%esi, %rsi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movslq	%ecx, %rax
	orb	%dl, %bl
	movb	%bl, 112(%rsp)          # 1-byte Spill
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader507
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_8 Depth 4
                                        #           Child Loop BB0_9 Depth 5
                                        #             Child Loop BB0_12 Depth 6
	leaq	1(%rax), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmpb	$0, 112(%rsp)           # 1-byte Folded Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jne	.LBB0_18
# BB#7:                                 # %.preheader505.preheader
                                        #   in Loop: Header=BB0_6 Depth=3
	movq	104(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader505
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        #       Parent Loop BB0_6 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_9 Depth 5
                                        #             Child Loop BB0_12 Depth 6
	movq	%rax, 56(%rsp)          # 8-byte Spill
	incq	%rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        #       Parent Loop BB0_6 Depth=3
                                        #         Parent Loop BB0_8 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_12 Depth 6
	movq	%rax, %r14
	leaq	1(%r14), %rdx
	imulq	$216, %rbp, %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	addq	40(%rcx), %rax
	movq	%rdx, (%rsp)            # 8-byte Spill
	leaq	(%rdx,%rdx,8), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r15
	testq	%r15, %r15
	jne	.LBB0_12
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_12 Depth=6
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.LBB0_16
.LBB0_12:                               # %.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        #       Parent Loop BB0_6 Depth=3
                                        #         Parent Loop BB0_8 Depth=4
                                        #           Parent Loop BB0_9 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movl	(%r15), %eax
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%eax, %rbx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rdx
	callq	hypre_IntersectBoxes
	movl	20(%r13), %eax
	movl	8(%r13), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	12(%r13), %eax
	movl	16(%r13), %ecx
	movl	4(%r13), %esi
	movl	$0, %ebp
	cmovsl	%ebp, %edx
	movl	%ecx, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %ecx
	movl	(%r13), %ecx
	cmovsl	%ebp, %edi
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%ebp, %esi
	imull	%edi, %esi
	imull	%edx, %esi
	testl	%esi, %esi
	je	.LBB0_10
# BB#13:                                #   in Loop: Header=BB0_12 Depth=6
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_12 Depth=6
	xorl	%edi, %edi
	movq	%rax, %rbp
	callq	hypre_BoxArrayCreate
	movq	%rax, %rsi
	movq	%rsi, (%rbp,%rbx,8)
	movslq	%r12d, %r12
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax,%r12,4)
	incl	%r12d
.LBB0_15:                               #   in Loop: Header=BB0_12 Depth=6
	movq	%r13, %rdi
	callq	hypre_AppendBox
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_16:                               # %.loopexit503
                                        #   in Loop: Header=BB0_9 Depth=5
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	jl	.LBB0_9
# BB#17:                                # %._crit_edge
                                        #   in Loop: Header=BB0_8 Depth=4
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	152(%rsp), %rax         # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jl	.LBB0_8
.LBB0_18:                               # %._crit_edge527
                                        #   in Loop: Header=BB0_6 Depth=3
	movq	136(%rsp), %rax         # 8-byte Reload
	cmpq	120(%rsp), %rax         # 8-byte Folded Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	jl	.LBB0_6
.LBB0_19:                               # %._crit_edge531
                                        #   in Loop: Header=BB0_4 Depth=2
	incq	%rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	cmpq	%rax, %rdi
	jl	.LBB0_4
# BB#20:                                # %.preheader512
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r12d, %r12d
	setg	%al
	movq	8(%rsp), %rcx           # 8-byte Reload
	jle	.LBB0_25
# BB#21:                                # %.lr.ph541.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r12d, %r15d
	xorl	%ebp, %ebp
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph541
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx), %r14
	movq	(%rcx,%r14,8), %rdi
	callq	hypre_UnionBoxes
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r14,8), %rax
	addl	8(%rax), %ebp
	addq	$4, %rbx
	decq	%r15
	jne	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%ebp, %ebx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
.LBB0_26:                               # %._crit_edge542
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebx, %ebx
.LBB0_27:                               # %._crit_edge542
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%ebx, %esi
	callq	hypre_BoxArraySetSize
	movl	$4, %esi
	movl	%ebx, %edi
	callq	hypre_CAlloc
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbp,8)
	testb	%r14b, %r14b
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB0_34
# BB#28:                                # %.lr.ph555
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%r12d, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_29:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_31 Depth 3
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%r14,4), %r12
	movq	(%rax,%r12,8), %rdi
	cmpl	$0, 8(%rdi)
	movq	176(%rsp), %rbx         # 8-byte Reload
	jle	.LBB0_33
# BB#30:                                # %.lr.ph547
                                        #   in Loop: Header=BB0_29 Depth=2
	movq	(%rdi), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r8
	movslq	%r15d, %rsi
	addq	$20, %rax
	leaq	(,%rsi,4), %rcx
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	(%rbp,%rdx,8), %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	20(%r8,%rsi,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%r12,4), %ebp
	movl	%ebp, (%rcx,%rsi,4)
	movl	-20(%rax), %ebp
	movl	%ebp, -20(%rdx)
	movl	-16(%rax), %ebp
	movl	%ebp, -16(%rdx)
	movl	-12(%rax), %ebp
	movl	%ebp, -12(%rdx)
	movl	-8(%rax), %ebp
	movl	%ebp, -8(%rdx)
	movl	-4(%rax), %ebp
	movl	%ebp, -4(%rdx)
	movl	(%rax), %ebp
	movl	%ebp, (%rdx)
	incq	%rsi
	movslq	8(%rdi), %rbp
	addq	$24, %rax
	addq	$24, %rdx
	cmpq	%rbp, %rsi
	jl	.LBB0_31
# BB#32:                                # %._crit_edge548.loopexit
                                        #   in Loop: Header=BB0_29 Depth=2
	addl	%esi, %r15d
.LBB0_33:                               # %._crit_edge548
                                        #   in Loop: Header=BB0_29 Depth=2
	callq	hypre_BoxArrayDestroy
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax,%r12,8)
	incq	%r14
	cmpq	(%rsp), %r14            # 8-byte Folded Reload
	jne	.LBB0_29
.LBB0_34:                               # %.preheader511
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	jle	.LBB0_57
# BB#35:                                # %.lr.ph580.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	96(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph580
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_38 Depth 3
                                        #         Child Loop BB0_40 Depth 4
                                        #           Child Loop BB0_41 Depth 5
                                        #             Child Loop BB0_44 Depth 6
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rdx
	leaq	(%rbp,%rdx,4), %r15
	movl	(%rbp,%rdx,4), %eax
	movl	%eax, %ecx
	negl	%ecx
	movl	%ecx, (%rbp,%rdx,4)
	negl	4(%rbp,%rdx,4)
	negl	8(%rbp,%rdx,4)
	movq	184(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi), %ecx
	subl	%eax, %ecx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, (%rsi)
	movl	(%rbp,%rdx,4), %eax
	addl	12(%rdi), %eax
	movl	%eax, 12(%rsi)
	movl	4(%rbp,%rdx,4), %eax
	addl	4(%rdi), %eax
	movl	%eax, 4(%rsi)
	movl	4(%rbp,%rdx,4), %eax
	addl	16(%rdi), %eax
	movl	%eax, 16(%rsi)
	movl	8(%rbp,%rdx,4), %eax
	addl	8(%rdi), %eax
	movl	%eax, 8(%rsi)
	movl	8(%rbp,%rdx,4), %eax
	addl	20(%rdi), %eax
	movl	%eax, 20(%rsi)
	movl	(%rbp,%rdx,4), %eax
	movl	4(%rbp,%rdx,4), %ecx
	movl	8(%rbp,%rdx,4), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	xorl	%edi, %edi
	testl	%edx, %edx
	setg	%dil
	cmpl	%edi, %esi
	jg	.LBB0_52
# BB#37:                                # %.preheader506.lr.ph
                                        #   in Loop: Header=BB0_36 Depth=2
	movl	%eax, %edi
	sarl	$31, %edi
	movl	%ecx, %ebp
	sarl	$31, %ebp
	xorl	%ebx, %ebx
	testl	%edx, %edx
	setg	%bl
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	testl	%ecx, %ecx
	setg	%dl
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	cmpl	%edx, %ebp
	setg	%al
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	cmpl	%ecx, %edi
	setg	%cl
	movslq	%edi, %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movslq	%ebp, %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movslq	%esi, %rdx
	orb	%al, %cl
	movb	%cl, 104(%rsp)          # 1-byte Spill
	.p2align	4, 0x90
.LBB0_38:                               # %.preheader506
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_40 Depth 4
                                        #           Child Loop BB0_41 Depth 5
                                        #             Child Loop BB0_44 Depth 6
	leaq	1(%rdx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpb	$0, 104(%rsp)           # 1-byte Folded Reload
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	jne	.LBB0_50
# BB#39:                                # %.preheader504.preheader
                                        #   in Loop: Header=BB0_38 Depth=3
	movq	256(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_40:                               # %.preheader504
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_38 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_41 Depth 5
                                        #             Child Loop BB0_44 Depth 6
	movq	%rax, 144(%rsp)         # 8-byte Spill
	incq	%rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_41:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_38 Depth=3
                                        #         Parent Loop BB0_40 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_44 Depth 6
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rdx
	imulq	$216, 16(%rsp), %rax    # 8-byte Folded Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	addq	40(%rcx), %rax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rdx,8), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	testq	%r14, %r14
	jne	.LBB0_44
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_44 Depth=6
	movq	8(%r14), %r14
	testq	%r14, %r14
	je	.LBB0_48
.LBB0_44:                               # %.lr.ph563
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_38 Depth=3
                                        #         Parent Loop BB0_40 Depth=4
                                        #           Parent Loop BB0_41 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movl	(%r14), %eax
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	%eax, %r12
	leaq	(%r12,%r12,2), %rax
	leaq	(%rcx,%rax,8), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rdx
	callq	hypre_IntersectBoxes
	movl	20(%r13), %r8d
	movl	8(%r13), %r9d
	movl	%r8d, %eax
	subl	%r9d, %eax
	incl	%eax
	cmpl	%r9d, %r8d
	movl	16(%r13), %ebx
	movl	(%r13), %edi
	movl	4(%r13), %esi
	movl	$0, %r10d
	cmovsl	%r10d, %eax
	movl	%ebx, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %ebx
	movl	12(%r13), %ecx
	cmovsl	%r10d, %edx
	movl	%ecx, %ebp
	subl	%edi, %ebp
	incl	%ebp
	cmpl	%edi, %ecx
	cmovsl	%r10d, %ebp
	imull	%edx, %ebp
	imull	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB0_42
# BB#45:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_44 Depth=6
	subl	(%r15), %edi
	movl	%edi, (%r13)
	subl	(%r15), %ecx
	movl	%ecx, 12(%r13)
	subl	4(%r15), %esi
	movl	%esi, 4(%r13)
	subl	4(%r15), %ebx
	movl	%ebx, 16(%r13)
	subl	8(%r15), %r9d
	movl	%r9d, 8(%r13)
	subl	8(%r15), %r8d
	movl	%r8d, 20(%r13)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r12,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_44 Depth=6
	xorl	%edi, %edi
	movq	%rax, %rbx
	callq	hypre_BoxArrayCreate
	movq	%rax, %rsi
	movq	%rsi, (%rbx,%r12,8)
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	%ecx, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%r12d, (%rax,%rcx,4)
	incl	%ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB0_47:                               #   in Loop: Header=BB0_44 Depth=6
	movq	%r13, %rdi
	callq	hypre_AppendBox
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_48:                               # %.loopexit
                                        #   in Loop: Header=BB0_41 Depth=5
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	152(%rsp), %rax         # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jl	.LBB0_41
# BB#49:                                # %._crit_edge568
                                        #   in Loop: Header=BB0_40 Depth=4
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpq	136(%rsp), %rax         # 8-byte Folded Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	jl	.LBB0_40
.LBB0_50:                               # %._crit_edge572
                                        #   in Loop: Header=BB0_38 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpq	112(%rsp), %rax         # 8-byte Folded Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	jl	.LBB0_38
# BB#51:                                # %.preheader508.loopexit
                                        #   in Loop: Header=BB0_36 Depth=2
	movl	(%r15), %eax
	movl	4(%r15), %ecx
	movl	8(%r15), %edx
	movq	96(%rsp), %rbp          # 8-byte Reload
.LBB0_52:                               # %.preheader508
                                        #   in Loop: Header=BB0_36 Depth=2
	negl	%eax
	movl	%eax, (%r15)
	negl	%ecx
	movl	%ecx, 4(%r15)
	negl	%edx
	movl	%edx, 8(%r15)
	movq	248(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	cmpq	%rax, %rcx
	jl	.LBB0_36
# BB#53:                                # %.preheader510
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	testl	%ebp, %ebp
	setg	%r14b
	jle	.LBB0_58
# BB#54:                                # %.lr.ph584.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%ebp, %ebp
	xorl	%r12d, %r12d
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph584
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx), %r15
	movq	(%rcx,%r15,8), %rdi
	callq	hypre_UnionBoxes
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r15,8), %rax
	addl	8(%rax), %r12d
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB0_55
# BB#56:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_57:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_58:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
.LBB0_59:                               # %._crit_edge585
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%r12d, %r12d
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_60:                               # %._crit_edge585
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%r12d, %esi
	callq	hypre_BoxArraySetSize
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	testb	%r14b, %r14b
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB0_67
# BB#61:                                # %.lr.ph598
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%ebp, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_62:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_64 Depth 3
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%r12,4), %r14
	movq	(%rax,%r14,8), %rdi
	cmpl	$0, 8(%rdi)
	movq	176(%rsp), %rbx         # 8-byte Reload
	jle	.LBB0_66
# BB#63:                                # %.lr.ph590
                                        #   in Loop: Header=BB0_62 Depth=2
	movq	(%rdi), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r8
	movslq	%r15d, %rsi
	addq	$20, %rax
	leaq	(,%rsi,4), %rcx
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	(%rbp,%rdx,8), %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	20(%r8,%rsi,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_64:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_62 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%r14,4), %ebp
	movl	%ebp, (%rcx,%rsi,4)
	movl	-20(%rax), %ebp
	movl	%ebp, -20(%rdx)
	movl	-16(%rax), %ebp
	movl	%ebp, -16(%rdx)
	movl	-12(%rax), %ebp
	movl	%ebp, -12(%rdx)
	movl	-8(%rax), %ebp
	movl	%ebp, -8(%rdx)
	movl	-4(%rax), %ebp
	movl	%ebp, -4(%rdx)
	movl	(%rax), %ebp
	movl	%ebp, (%rdx)
	incq	%rsi
	movslq	8(%rdi), %rbp
	addq	$24, %rax
	addq	$24, %rdx
	cmpq	%rbp, %rsi
	jl	.LBB0_64
# BB#65:                                # %._crit_edge591.loopexit
                                        #   in Loop: Header=BB0_62 Depth=2
	addl	%esi, %r15d
.LBB0_66:                               # %._crit_edge591
                                        #   in Loop: Header=BB0_62 Depth=2
	callq	hypre_BoxArrayDestroy
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax,%r14,8)
	incq	%r12
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jne	.LBB0_62
.LBB0_67:                               # %._crit_edge599
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movq	240(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_2
.LBB0_68:                               # %._crit_edge603
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_Free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
	movq	%r13, %rdi
	callq	hypre_BoxDestroy
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CreateCommInfoFromStencil, .Lfunc_end0-hypre_CreateCommInfoFromStencil
	.cfi_endproc

	.globl	hypre_CreateCommInfoFromNumGhost
	.p2align	4, 0x90
	.type	hypre_CreateCommInfoFromNumGhost,@function
hypre_CreateCommInfoFromNumGhost:       # @hypre_CreateCommInfoFromNumGhost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 240
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rsi, %rbp
	movq	8(%rdi), %rbx
	movq	16(%rdi), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	24(%rdi), %r14
	movl	8(%rbx), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	8(%rbx), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	8(%rbx), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	8(%rbx), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	(%r14), %r15
	movq	8(%r14), %r12
	movq	16(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, 48(%rsp)          # 8-byte Spill
	callq	hypre_BoxCreate
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	8(%r15), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	8(%r15), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %r13
	cmpl	$0, 8(%rbx)
	jle	.LBB1_47
# BB#1:                                 # %.lr.ph396
	xorl	%esi, %esi
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader348402
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_19 Depth 2
                                        #       Child Loop BB1_21 Depth 3
                                        #     Child Loop BB1_29 Depth 2
                                        #     Child Loop BB1_37 Depth 2
                                        #     Child Loop BB1_41 Depth 2
                                        #       Child Loop BB1_43 Depth 3
	movq	(%rbx), %rdx
	leaq	(%rsi,%rsi,2), %rdi
	movl	(%rdx,%rdi,8), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%rdx,%rdi,8), %eax
	movl	%eax, 4(%rcx)
	movl	8(%rdx,%rdi,8), %eax
	movl	%eax, 8(%rcx)
	movl	12(%rdx,%rdi,8), %eax
	movl	%eax, 12(%rcx)
	movl	16(%rdx,%rdi,8), %eax
	movl	%eax, 16(%rcx)
	movl	20(%rdx,%rdi,8), %eax
	movl	%eax, 20(%rcx)
	movl	(%rdx,%rdi,8), %eax
	subl	(%rbp), %eax
	movl	%eax, (%rcx)
	movl	4(%rbp), %eax
	addl	12(%rdx,%rdi,8), %eax
	movl	%eax, 12(%rcx)
	movl	4(%rdx,%rdi,8), %eax
	subl	8(%rbp), %eax
	movl	%eax, 4(%rcx)
	movl	12(%rbp), %eax
	addl	16(%rdx,%rdi,8), %eax
	movl	%eax, 16(%rcx)
	movl	8(%rdx,%rdi,8), %eax
	subl	16(%rbp), %eax
	movl	%eax, 8(%rcx)
	movl	20(%rbp), %eax
	addl	20(%rdx,%rdi,8), %eax
	movl	%eax, 20(%rcx)
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 8(%rcx)
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	jle	.LBB1_3
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	104(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rsi,4), %eax
	movq	120(%rsp), %rdx         # 8-byte Reload
	cmpl	(%rdx,%rbp,4), %eax
	je	.LBB1_13
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	movq	(%rcx), %rsi
	addq	%r13, %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	hypre_IntersectBoxes
	movl	20(%rbx), %eax
	movl	8(%rbx), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	12(%rbx), %eax
	movl	16(%rbx), %ecx
	movl	4(%rbx), %esi
	movl	$0, %r8d
	cmovsl	%r8d, %edx
	movl	%ecx, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %ecx
	movl	(%rbx), %ecx
	cmovsl	%r8d, %edi
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%r8d, %esi
	imull	%edi, %esi
	imull	%edx, %esi
	testl	%esi, %esi
	je	.LBB1_12
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=2
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%r15,%rbp,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_7 Depth=2
	xorl	%edi, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %rsi
	movq	%rsi, (%r15,%rbp,8)
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	%ecx, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	incl	%ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=2
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_AppendBox
.LBB1_12:                               #   in Loop: Header=BB1_7 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB1_13:                               #   in Loop: Header=BB1_7 Depth=2
	incq	%rbp
	movslq	8(%rcx), %rax
	addq	$24, %r13
	cmpq	%rax, %rbp
	jl	.LBB1_7
# BB#4:                                 # %.preheader347
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%rsp), %r14            # 8-byte Reload
	testl	%r14d, %r14d
	setg	%al
	jle	.LBB1_5
# BB#14:                                # %.lr.ph357.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r14d, %ebp
	xorl	%r15d, %r15d
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph357
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx), %r14
	movq	(%r13,%r14,8), %rdi
	callq	hypre_UnionBoxes
	movq	(%r13,%r14,8), %rax
	addl	8(%rax), %r15d
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB1_15
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%r13, %rbp
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB1_17:                               # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rsi,8), %rdi
	movq	%rsi, %rbx
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %esi
	callq	hypre_BoxArraySetSize
	movl	$4, %esi
	movl	%r15d, %edi
	callq	hypre_CAlloc
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB1_24
# BB#18:                                # %.lr.ph370
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%r14d, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_21 Depth 3
	movslq	(%r13,%rbx,4), %r15
	movq	(%rbp,%r15,8), %rdi
	cmpl	$0, 8(%rdi)
	jle	.LBB1_23
# BB#20:                                # %.lr.ph362
                                        #   in Loop: Header=BB1_19 Depth=2
	movq	(%rdi), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r8
	movslq	%r14d, %rsi
	addq	$20, %rax
	leaq	(,%rsi,4), %rcx
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	(%rbp,%rdx,8), %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	20(%r8,%rsi,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_21:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%r15,4), %ebp
	movl	%ebp, (%rcx,%rsi,4)
	movl	-20(%rax), %ebp
	movl	%ebp, -20(%rdx)
	movl	-16(%rax), %ebp
	movl	%ebp, -16(%rdx)
	movl	-12(%rax), %ebp
	movl	%ebp, -12(%rdx)
	movl	-8(%rax), %ebp
	movl	%ebp, -8(%rdx)
	movl	-4(%rax), %ebp
	movl	%ebp, -4(%rdx)
	movl	(%rax), %ebp
	movl	%ebp, (%rdx)
	incq	%rsi
	movslq	8(%rdi), %rbp
	addq	$24, %rax
	addq	$24, %rdx
	cmpq	%rbp, %rsi
	jl	.LBB1_21
# BB#22:                                # %._crit_edge363.loopexit
                                        #   in Loop: Header=BB1_19 Depth=2
	addl	%esi, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB1_23:                               # %._crit_edge363
                                        #   in Loop: Header=BB1_19 Depth=2
	callq	hypre_BoxArrayDestroy
	movq	$0, (%rbp,%r15,8)
	incq	%rbx
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	jne	.LBB1_19
.LBB1_24:                               # %.preheader346
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 8(%rcx)
	jle	.LBB1_25
# BB#28:                                # %.lr.ph374
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$5, %r15d
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_29:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rsi,4), %eax
	movq	120(%rsp), %rdx         # 8-byte Reload
	cmpl	(%rdx,%rbx,4), %eax
	je	.LBB1_35
# BB#30:                                #   in Loop: Header=BB1_29 Depth=2
	movq	(%rcx), %rax
	movl	-20(%rax,%r15,4), %ecx
	subl	(%r13), %ecx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, (%rsi)
	movl	4(%r13), %ecx
	addl	-8(%rax,%r15,4), %ecx
	movl	%ecx, 12(%rsi)
	movl	-16(%rax,%r15,4), %ecx
	subl	8(%r13), %ecx
	movl	%ecx, 4(%rsi)
	movl	12(%r13), %ecx
	addl	-4(%rax,%r15,4), %ecx
	movl	%ecx, 16(%rsi)
	movl	-12(%rax,%r15,4), %ecx
	subl	16(%r13), %ecx
	movl	%ecx, 8(%rsi)
	movl	20(%r13), %ecx
	addl	(%rax,%r15,4), %ecx
	movl	%ecx, 20(%rsi)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	hypre_IntersectBoxes
	movl	20(%rbp), %eax
	movl	8(%rbp), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	12(%rbp), %eax
	movl	16(%rbp), %ecx
	movl	4(%rbp), %esi
	movl	$0, %r8d
	cmovsl	%r8d, %edx
	movl	%ecx, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %ecx
	movl	(%rbp), %ecx
	cmovsl	%r8d, %edi
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%r8d, %esi
	imull	%edi, %esi
	imull	%edx, %esi
	testl	%esi, %esi
	je	.LBB1_34
# BB#31:                                #   in Loop: Header=BB1_29 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_29 Depth=2
	xorl	%edi, %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %rsi
	movq	%rsi, (%rbp,%rbx,8)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	%ecx, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax,%rcx,4)
	incl	%ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
.LBB1_33:                               #   in Loop: Header=BB1_29 Depth=2
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_AppendBox
.LBB1_34:                               #   in Loop: Header=BB1_29 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB1_35:                               #   in Loop: Header=BB1_29 Depth=2
	incq	%rbx
	movslq	8(%rcx), %rax
	addq	$6, %r15
	cmpq	%rax, %rbx
	jl	.LBB1_29
# BB#26:                                # %.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	setg	%al
	jle	.LBB1_27
# BB#36:                                # %.lr.ph378.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%r15d, %ebp
	xorl	%r14d, %r14d
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph378
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r15), %rbx
	movq	(%r13,%rbx,8), %rdi
	callq	hypre_UnionBoxes
	movq	(%r13,%rbx,8), %rax
	addl	8(%rax), %r14d
	addq	$4, %r15
	decq	%rbp
	jne	.LBB1_37
# BB#38:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%r13, %rbp
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_27:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB1_39:                               # %._crit_edge379
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rsi,8), %rdi
	movq	%rsi, %rbx
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	%r14d, %esi
	callq	hypre_BoxArraySetSize
	movl	$4, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	cmpb	$0, (%rsp)              # 1-byte Folded Reload
	je	.LBB1_46
# BB#40:                                # %.lr.ph392
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%r15d, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_41:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_43 Depth 3
	movslq	(%r13,%r14,4), %rbx
	movq	(%rbp,%rbx,8), %rdi
	cmpl	$0, 8(%rdi)
	jle	.LBB1_45
# BB#42:                                # %.lr.ph384
                                        #   in Loop: Header=BB1_41 Depth=2
	movq	(%rdi), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r8
	movslq	%r15d, %rsi
	addq	$20, %rax
	leaq	(,%rsi,4), %rcx
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	(%rbp,%rdx,8), %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	20(%r8,%rsi,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_43:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%rbx,4), %ebp
	movl	%ebp, (%rcx,%rsi,4)
	movl	-20(%rax), %ebp
	movl	%ebp, -20(%rdx)
	movl	-16(%rax), %ebp
	movl	%ebp, -16(%rdx)
	movl	-12(%rax), %ebp
	movl	%ebp, -12(%rdx)
	movl	-8(%rax), %ebp
	movl	%ebp, -8(%rdx)
	movl	-4(%rax), %ebp
	movl	%ebp, -4(%rdx)
	movl	(%rax), %ebp
	movl	%ebp, (%rdx)
	incq	%rsi
	movslq	8(%rdi), %rbp
	addq	$24, %rax
	addq	$24, %rdx
	cmpq	%rbp, %rsi
	jl	.LBB1_43
# BB#44:                                # %._crit_edge385.loopexit
                                        #   in Loop: Header=BB1_41 Depth=2
	addl	%esi, %r15d
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB1_45:                               # %._crit_edge385
                                        #   in Loop: Header=BB1_41 Depth=2
	callq	hypre_BoxArrayDestroy
	movq	$0, (%rbp,%rbx,8)
	incq	%r14
	cmpq	(%rsp), %r14            # 8-byte Folded Reload
	jne	.LBB1_41
.LBB1_46:                               # %._crit_edge393
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	incq	%rsi
	movq	160(%rsp), %rbx         # 8-byte Reload
	movslq	8(%rbx), %rax
	cmpq	%rax, %rsi
	movq	112(%rsp), %rbp         # 8-byte Reload
	jl	.LBB1_2
.LBB1_47:                               # %._crit_edge397
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	%r13, %rdi
	callq	hypre_Free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_BoxDestroy
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_CreateCommInfoFromNumGhost, .Lfunc_end1-hypre_CreateCommInfoFromNumGhost
	.cfi_endproc

	.globl	hypre_CreateCommInfoFromGrids
	.p2align	4, 0x90
	.type	hypre_CreateCommInfoFromGrids,@function
hypre_CreateCommInfoFromGrids:          # @hypre_CreateCommInfoFromGrids
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 208
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	$0, %eax
                                        # implicit-def: %RCX
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %RCX
	movq	%rcx, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %RDX
                                        # implicit-def: %RCX
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_15:                               # %._crit_edge121
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	callq	hypre_BoxDestroy
	movq	8(%rsp), %rdi
	callq	hypre_BoxArrayDestroy
	movq	24(%rsp), %rdi
	callq	hypre_Free
	movq	$0, 24(%rsp)
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB2_1
# BB#16:                                # %._crit_edge121
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$1, %eax
	movq	80(%rsp), %rdx          # 8-byte Reload
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_2 Depth=1
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB2_2
	jmp	.LBB2_18
.LBB2_1:                                # %.outer.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #       Child Loop BB2_10 Depth 3
	cmpl	$1, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	movq	72(%rsp), %rax          # 8-byte Reload
	jne	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	8(%rdx), %rbp
	movq	8(%rax), %rsi
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	(%rax), %edi
	leaq	8(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	148(%rsp), %r8
	callq	hypre_GatherAllBoxes
	movl	8(%rbp), %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	8(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	callq	hypre_BoxCreate
	movq	%rax, %r15
	cmpl	$0, 8(%rbp)
	jle	.LBB2_15
# BB#7:                                 # %.lr.ph120
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebx, %ebx
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_10 Depth 3
	leaq	(%rbx,%rbx,2), %r13
	shlq	$3, %r13
	addq	(%rbp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %r12
	movq	8(%rsp), %rax
	movl	8(%rax), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%r14,%rbx,8)
	movq	8(%rsp), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB2_14
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_8 Depth=2
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rsi
	addq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	hypre_IntersectBoxes
	movl	20(%r15), %eax
	movl	8(%r15), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	12(%r15), %eax
	movl	16(%r15), %ecx
	movl	4(%r15), %esi
	movl	$0, %ebx
	cmovsl	%ebx, %edx
	movl	%ecx, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %ecx
	movl	(%r15), %ecx
	cmovsl	%ebx, %edi
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	cmovsl	%ebx, %esi
	imull	%edi, %esi
	imull	%edx, %esi
	testl	%esi, %esi
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=3
	movq	24(%rsp), %rax
	movl	(%rax,%rbp,4), %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movslq	8(%r12), %rdx
	movl	%eax, (%rcx,%rdx,4)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	hypre_AppendBox
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=3
	incq	%rbp
	movq	8(%rsp), %rcx
	movslq	8(%rcx), %rax
	addq	$24, %r14
	cmpq	%rax, %rbp
	jl	.LBB2_10
# BB#13:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_8 Depth=2
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	(%r14,%rbx,8), %rax
	movq	136(%rsp), %rbp         # 8-byte Reload
.LBB2_14:                               # %._crit_edge
                                        #   in Loop: Header=BB2_8 Depth=2
	movl	8(%r12), %esi
	shll	$2, %esi
	movq	%rax, %rdi
	callq	hypre_ReAlloc
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	movslq	8(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_8
	jmp	.LBB2_15
.LBB2_18:                               # %.thread
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%r14, (%rax)
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_CreateCommInfoFromGrids, .Lfunc_end2-hypre_CreateCommInfoFromGrids
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
