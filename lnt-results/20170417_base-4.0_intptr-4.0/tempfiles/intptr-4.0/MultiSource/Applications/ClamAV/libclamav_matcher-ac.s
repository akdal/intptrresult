	.text
	.file	"libclamav_matcher-ac.bc"
	.globl	cli_ac_addpatt
	.p2align	4, 0x90
	.type	cli_ac_addpatt,@function
cli_ac_addpatt:                         # @cli_ac_addpatt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movzbl	37(%r15), %ecx
	movzwl	16(%rsi), %eax
	cmpl	%eax, %ecx
	cmovbl	%ecx, %eax
	testl	%eax, %eax
	je	.LBB0_5
# BB#1:                                 # %.lr.ph181
	movq	(%rsi), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %edi
	movzwl	(%rdx,%rdi,2), %edi
	cmpl	$256, %edi              # imm = 0x100
	jae	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	incb	%cl
	movzbl	%cl, %ecx
	cmpl	%eax, %ecx
	jb	.LBB0_2
# BB#4:
	movl	%eax, %ecx
	jmp	.LBB0_6
.LBB0_5:
	xorl	%ecx, %ecx
.LBB0_6:                                # %._crit_edge182
	movzwl	%cx, %edx
	movzbl	36(%r15), %ecx
	movl	$-117, %eax
	cmpl	%ecx, %edx
	jb	.LBB0_53
# BB#7:
	movq	40(%r15), %r14
	xorl	%ebx, %ebx
	testl	%edx, %edx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	je	.LBB0_19
# BB#8:                                 # %.lr.ph175
	leal	-1(%rdx), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%r14, %r12
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	movl	$256, %edi              # imm = 0x100
	movl	$8, %esi
	callq	cli_calloc
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.LBB0_45
.LBB0_11:                               #   in Loop: Header=BB0_9 Depth=1
	movq	(%rsi), %rcx
	movzbl	%bl, %r13d
	movzbl	(%rcx,%r13,2), %ecx
	movq	(%rax,%rcx,8), %r14
	testq	%r14, %r14
	jne	.LBB0_18
# BB#12:                                #   in Loop: Header=BB0_9 Depth=1
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_46
# BB#13:                                #   in Loop: Header=BB0_9 Depth=1
	cmpl	24(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_9 Depth=1
	movb	$1, (%r14)
	jmp	.LBB0_16
.LBB0_15:                               #   in Loop: Header=BB0_9 Depth=1
	movl	$256, %edi              # imm = 0x100
	movl	$8, %esi
	callq	cli_calloc
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	je	.LBB0_50
.LBB0_16:                               #   in Loop: Header=BB0_9 Depth=1
	movl	68(%r15), %esi
	incl	%esi
	movl	%esi, 68(%r15)
	movq	48(%r15), %rdi
	shlq	$3, %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB0_48
# BB#17:                                #   in Loop: Header=BB0_9 Depth=1
	movl	68(%r15), %ecx
	decl	%ecx
	movq	%r14, (%rax,%rcx,8)
	movq	%rax, 48(%r15)
	movq	16(%r12), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi), %rcx
	movzbl	(%rcx,%r13,2), %ecx
	movq	%r14, (%rax,%rcx,8)
	movb	$0, (%r12)
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB0_18:                               #   in Loop: Header=BB0_9 Depth=1
	movb	%bl, %bpl
	incb	%bpl
	movzbl	%bpl, %ebx
	cmpl	%edx, %ebx
	movq	%r14, %r12
	jb	.LBB0_9
	jmp	.LBB0_20
.LBB0_19:
	xorl	%ebp, %ebp
.LBB0_20:                               # %._crit_edge176
	movl	72(%r15), %esi
	incl	%esi
	movl	%esi, 72(%r15)
	movq	56(%r15), %rdi
	shlq	$3, %rsi
	callq	cli_realloc2
	movq	%rax, 56(%r15)
	testq	%rax, %rax
	je	.LBB0_44
# BB#21:
	movl	72(%r15), %ecx
	decl	%ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rsi, (%rax,%rcx,8)
	movb	$1, 1(%r14)
	movb	%bpl, 20(%rsi)
	movq	8(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_42
# BB#22:                                # %.lr.ph170
	movzwl	16(%rsi), %ebp
	movq	%rbp, %rax
	addq	%rax, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_23:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_31 Depth 2
                                        #       Child Loop BB0_36 Depth 3
	cmpw	%bp, 16(%r13)
	jne	.LBB0_41
# BB#24:                                #   in Loop: Header=BB0_23 Depth=1
	movzwl	18(%r13), %ebx
	cmpw	18(%rsi), %bx
	jne	.LBB0_41
# BB#25:                                #   in Loop: Header=BB0_23 Depth=1
	movq	(%r13), %rdi
	movq	(%rsi), %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_41
# BB#26:                                #   in Loop: Header=BB0_23 Depth=1
	movq	8(%r13), %rdi
	movq	8(%rsi), %rsi
	addq	%rbx, %rbx
	movq	%rbx, %rdx
	callq	memcmp
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_41
# BB#27:                                #   in Loop: Header=BB0_23 Depth=1
	movzwl	56(%r13), %ecx
	movzwl	56(%rsi), %eax
	movl	%ecx, 20(%rsp)          # 4-byte Spill
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	orw	%ax, %cx
	je	.LBB0_54
# BB#28:                                # %._crit_edge194
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpw	%ax, 20(%rsp)           # 2-byte Folded Reload
	jne	.LBB0_41
# BB#29:                                # %.preheader134
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpw	$0, 20(%rsp)            # 2-byte Folded Reload
	je	.LBB0_54
# BB#30:                                # %.lr.ph162
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	64(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	64(%rsi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
.LBB0_31:                               #   Parent Loop BB0_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_36 Depth 3
	movzbl	%r12b, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %r15
	movzwl	18(%rbx), %edx
	cmpw	18(%r15), %dx
	jne	.LBB0_41
# BB#32:                                #   in Loop: Header=BB0_31 Depth=2
	movb	(%rbx), %al
	cmpb	(%r15), %al
	jne	.LBB0_41
# BB#33:                                #   in Loop: Header=BB0_31 Depth=2
	testb	%al, %al
	je	.LBB0_36
# BB#34:                                #   in Loop: Header=BB0_31 Depth=2
	movq	8(%rbx), %rdi
	movq	8(%r15), %rsi
	callq	memcmp
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_41
.LBB0_35:                               #   in Loop: Header=BB0_31 Depth=2
	incb	%r12b
	movzbl	%r12b, %eax
	cmpl	20(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB0_31
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph
                                        #   Parent Loop BB0_23 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	16(%rbx), %edx
	cmpw	16(%r15), %dx
	jne	.LBB0_41
# BB#37:                                #   in Loop: Header=BB0_36 Depth=3
	movq	8(%rbx), %rdi
	movq	8(%r15), %rsi
	callq	memcmp
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB0_41
# BB#38:                                #   in Loop: Header=BB0_36 Depth=3
	movq	24(%rbx), %rbx
	movq	24(%r15), %r15
	testq	%rbx, %rbx
	je	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_36 Depth=3
	testq	%r15, %r15
	jne	.LBB0_36
.LBB0_40:                               # %._crit_edge
                                        #   in Loop: Header=BB0_31 Depth=2
	orq	%r15, %rbx
	je	.LBB0_35
	.p2align	4, 0x90
.LBB0_41:                               # %.loopexit
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	80(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_23
.LBB0_42:                               # %._crit_edge171
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%rsi)
	movq	%rsi, 8(%r14)
.LBB0_43:
	xorl	%eax, %eax
	jmp	.LBB0_53
.LBB0_44:
	movl	$.L.str.4, %edi
	jmp	.LBB0_47
.LBB0_45:
	movl	$.L.str, %edi
	jmp	.LBB0_47
.LBB0_46:
	movl	$.L.str.1, %edi
.LBB0_47:
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB0_52
.LBB0_48:
	decl	68(%r15)
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_51
# BB#49:
	callq	free
	jmp	.LBB0_51
.LBB0_50:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_51:
	movq	%r14, %rdi
	callq	free
.LBB0_52:
	movl	$-114, %eax
.LBB0_53:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_54:                               # %.critedge
	movq	88(%r13), %rax
	movq	%rax, 88(%rsi)
	movq	%rsi, 88(%r13)
	jmp	.LBB0_43
.Lfunc_end0:
	.size	cli_ac_addpatt, .Lfunc_end0-cli_ac_addpatt
	.cfi_endproc

	.globl	cli_ac_buildtrie
	.p2align	4, 0x90
	.type	cli_ac_buildtrie,@function
cli_ac_buildtrie:                       # @cli_ac_buildtrie
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	movq	40(%rdi), %r15
	testq	%r15, %r15
	je	.LBB1_3
# BB#24:
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB1_25:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_26
# BB#27:                                #   in Loop: Header=BB1_25 Depth=1
	movq	%r15, 24(%rbx)
	movl	$16, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_30
# BB#28:                                # %bfs_enqueue.exit.thread.i
                                        #   in Loop: Header=BB1_25 Depth=1
	movq	$0, 8(%rax)
	movq	%rbx, (%rax)
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	leaq	8(%rcx), %rcx
	cmoveq	%r12, %rcx
	movq	%r12, %rdx
	cmoveq	%r14, %rdx
	movq	%rax, (%rcx)
	movq	%rax, (%rdx)
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_25 Depth=1
	movq	%r15, (%rax,%rbp,8)
.LBB1_29:                               #   in Loop: Header=BB1_25 Depth=1
	incq	%rbp
	cmpq	$256, %rbp              # imm = 0x100
	jl	.LBB1_25
# BB#4:                                 # %.preheader13.i
	movq	16(%rsp), %rdi
	xorl	%r15d, %r15d
	testq	%rdi, %rdi
	je	.LBB1_31
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
                                        #       Child Loop BB1_12 Depth 3
                                        #       Child Loop BB1_15 Depth 3
	movq	8(%rdi), %rbx
	movq	%rbx, 16(%rsp)
	movq	(%rdi), %r13
	cmpq	8(%rsp), %rdi
	jne	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	$0, 8(%rsp)
.LBB1_7:                                # %bfs_dequeue.exit.i
                                        #   in Loop: Header=BB1_5 Depth=1
	callq	free
	testq	%r13, %r13
	je	.LBB1_31
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=1
	cmpb	$0, (%r13)
	jne	.LBB1_23
# BB#9:                                 # %.preheader12.i
                                        #   in Loop: Header=BB1_5 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_12 Depth 3
                                        #       Child Loop BB1_15 Depth 3
	movq	16(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_21
# BB#11:                                # %.critedge.i.preheader
                                        #   in Loop: Header=BB1_10 Depth=2
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB1_12:                               # %.critedge.i
                                        #   Parent Loop BB1_5 Depth=1
                                        #     Parent Loop BB1_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	24(%rcx), %rcx
	cmpb	$0, (%rcx)
	jne	.LBB1_12
# BB#13:                                #   in Loop: Header=BB1_12 Depth=3
	movq	16(%rcx), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB1_12
# BB#14:                                #   in Loop: Header=BB1_10 Depth=2
	movq	%rax, 24(%rbp)
	leaq	8(%rbp), %rcx
	movq	8(%rbp), %rsi
	testq	%rsi, %rsi
	movq	%rcx, %rdx
	je	.LBB1_17
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader.i
                                        #   Parent Loop BB1_5 Depth=1
                                        #     Parent Loop BB1_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rdx
	movq	80(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_15
# BB#16:                                #   in Loop: Header=BB1_10 Depth=2
	addq	$80, %rdx
.LBB1_17:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_10 Depth=2
	movq	8(%rax), %rax
	movq	%rax, (%rdx)
	cmpq	$0, (%rcx)
	je	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_10 Depth=2
	movb	$1, 1(%rbp)
.LBB1_19:                               #   in Loop: Header=BB1_10 Depth=2
	movl	$16, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_30
# BB#20:                                # %bfs_enqueue.exit60.thread.i
                                        #   in Loop: Header=BB1_10 Depth=2
	movq	$0, 8(%rax)
	movq	%rbp, (%rax)
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	leaq	8(%rcx), %rcx
	cmoveq	%r12, %rcx
	movq	%r12, %rdx
	cmoveq	%r14, %rdx
	movq	%rax, (%rcx)
	movq	%rax, (%rdx)
.LBB1_21:                               #   in Loop: Header=BB1_10 Depth=2
	incq	%rbx
	cmpq	$256, %rbx              # imm = 0x100
	jl	.LBB1_10
# BB#22:                                # %.backedge.loopexit.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	16(%rsp), %rbx
.LBB1_23:                               # %.backedge.i
                                        #   in Loop: Header=BB1_5 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB1_5
	jmp	.LBB1_31
.LBB1_1:
	movl	$-116, %r15d
	jmp	.LBB1_31
.LBB1_30:                               # %bfs_enqueue.exit.i
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %r15d
	jmp	.LBB1_31
.LBB1_3:
	xorl	%r15d, %r15d
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_31:
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_ac_buildtrie, .Lfunc_end1-cli_ac_buildtrie
	.cfi_endproc

	.globl	cli_ac_init
	.p2align	4, 0x90
	.type	cli_ac_init,@function
cli_ac_init:                            # @cli_ac_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.LBB2_3
# BB#1:
	movl	$256, %edi              # imm = 0x100
	movl	$8, %esi
	callq	cli_calloc
	movq	40(%rbx), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	je	.LBB2_4
# BB#2:
	movb	%bpl, 36(%rbx)
	movb	%r14b, 37(%rbx)
	xorl	%eax, %eax
	jmp	.LBB2_6
.LBB2_3:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB2_5
.LBB2_4:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	40(%rbx), %rdi
	callq	free
.LBB2_5:
	movl	$-114, %eax
.LBB2_6:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cli_ac_init, .Lfunc_end2-cli_ac_init
	.cfi_endproc

	.globl	cli_ac_free
	.p2align	4, 0x90
	.type	cli_ac_free,@function
cli_ac_free:                            # @cli_ac_free
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 72(%r14)
	movq	56(%r14), %rdi
	je	.LBB3_16
# BB#1:                                 # %.lr.ph34.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #       Child Loop BB3_9 Depth 3
	movl	%r12d, %eax
	movq	(%rdi,%rax,8), %r15
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%r15), %rdi
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	callq	free
	movq	32(%r15), %rdi
	callq	free
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	callq	free
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movzwl	56(%r15), %eax
	testw	%ax, %ax
	je	.LBB3_15
# BB#7:                                 # %.lr.ph19.preheader.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	64(%r15), %rdi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph19.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_9 Depth 3
	movzwl	%r13w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_13
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.i
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=3
	callq	free
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=3
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB3_9
# BB#12:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB3_8 Depth=2
	movzwl	56(%r15), %eax
	movq	64(%r15), %rdi
.LBB3_13:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_8 Depth=2
	incl	%r13d
	cmpw	%ax, %r13w
	jb	.LBB3_8
# BB#14:                                # %ac_free_alt.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	callq	free
.LBB3_15:                               #   in Loop: Header=BB3_2 Depth=1
	movq	%r15, %rdi
	callq	free
	incl	%r12d
	movq	56(%r14), %rdi
	cmpl	72(%r14), %r12d
	jb	.LBB3_2
.LBB3_16:                               # %._crit_edge35
	testq	%rdi, %rdi
	je	.LBB3_18
# BB#17:
	callq	free
.LBB3_18:                               # %.preheader
	cmpl	$0, 68(%r14)
	movq	48(%r14), %rdi
	je	.LBB3_23
# BB#19:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	movq	(%rdi,%rbx,8), %rax
	cmpb	$0, (%rax)
	jne	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_20 Depth=1
	movq	16(%rax), %rdi
	callq	free
	movq	48(%r14), %rdi
.LBB3_22:                               #   in Loop: Header=BB3_20 Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	leal	1(%rbx), %eax
	movq	48(%r14), %rdi
	cmpl	68(%r14), %eax
	jb	.LBB3_20
.LBB3_23:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB3_25
# BB#24:
	callq	free
.LBB3_25:
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_26
# BB#27:
	movq	16(%rax), %rdi
	callq	free
	movq	40(%r14), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB3_26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cli_ac_free, .Lfunc_end3-cli_ac_free
	.cfi_endproc

	.globl	cli_ac_initdata
	.p2align	4, 0x90
	.type	cli_ac_initdata,@function
cli_ac_initdata:                        # @cli_ac_initdata
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:
	movl	%esi, (%rbx)
	xorl	%ebp, %ebp
	testl	%esi, %esi
	je	.LBB4_5
# BB#3:
	movl	%esi, %edi
	movl	$8, %esi
	callq	cli_calloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB4_5
# BB#4:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %ebp
	jmp	.LBB4_5
.LBB4_1:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %ebp
.LBB4_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cli_ac_initdata, .Lfunc_end4-cli_ac_initdata
	.cfi_endproc

	.globl	cli_ac_freedata
	.p2align	4, 0x90
	.type	cli_ac_freedata,@function
cli_ac_freedata:                        # @cli_ac_freedata
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB5_7
# BB#1:
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.LBB5_7
# BB#2:                                 # %.lr.ph.preheader
	movq	8(%r14), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ebx
	movq	(%rdi,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%rcx), %rdi
	callq	free
	movq	8(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	movl	(%r14), %eax
	movq	8(%r14), %rdi
.LBB5_5:                                #   in Loop: Header=BB5_3 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jb	.LBB5_3
# BB#6:                                 # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB5_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	cli_ac_freedata, .Lfunc_end5-cli_ac_freedata
	.cfi_endproc

	.globl	cli_ac_scanbuff
	.p2align	4, 0x90
	.type	cli_ac_scanbuff,@function
cli_ac_scanbuff:                        # @cli_ac_scanbuff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 224
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%r9d, 52(%rsp)          # 4-byte Spill
	movq	%r8, %r12
	movl	%esi, %r9d
	movq	40(%rcx), %r13
	testq	%r13, %r13
	je	.LBB6_181
# BB#1:
	testq	%r12, %r12
	je	.LBB6_182
# BB#2:
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movq	$0, 160(%rsp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	movl	$0, %ecx
	je	.LBB6_180
# BB#3:                                 # %.preheader262.lr.ph
	movl	232(%rsp), %ecx
	cmpl	$502, %ecx              # imm = 0x1F6
	sete	%al
	cmpl	$508, %ecx              # imm = 0x1FC
	sete	%dl
	orb	%al, %dl
	movb	%dl, 7(%rsp)            # 1-byte Spill
	cmpl	$-1, 240(%rsp)
	setne	%al
	testl	%ecx, %ecx
	setne	%cl
	orb	%al, %cl
	movb	%cl, 15(%rsp)           # 1-byte Spill
	movl	%r9d, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	%r9d, (%rsp)            # 4-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_4:                                # %.preheader262
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
                                        #       Child Loop BB6_18 Depth 3
                                        #         Child Loop BB6_24 Depth 4
                                        #         Child Loop BB6_30 Depth 4
                                        #       Child Loop BB6_41 Depth 3
                                        #         Child Loop BB6_47 Depth 4
                                        #         Child Loop BB6_53 Depth 4
                                        #       Child Loop BB6_140 Depth 3
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_114 Depth 4
                                        #           Child Loop BB6_119 Depth 5
                                        #         Child Loop BB6_128 Depth 4
                                        #         Child Loop BB6_137 Depth 4
                                        #         Child Loop BB6_171 Depth 4
                                        #     Child Loop BB6_5 Depth 2
	movzbl	(%rdi,%r15), %eax
	movq	%r13, %rcx
	cmpb	$0, (%rcx)
	je	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %.critedge232
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rcx), %rcx
	cmpb	$0, (%rcx)
	jne	.LBB6_5
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=1
	movq	16(%rcx), %rdx
	movq	(%rdx,%rax,8), %r13
	testq	%r13, %r13
	je	.LBB6_5
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=1
	cmpb	$0, 1(%r13)
	je	.LBB6_176
# BB#8:                                 #   in Loop: Header=BB6_4 Depth=1
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB6_176
# BB#9:                                 # %.lr.ph353
                                        #   in Loop: Header=BB6_4 Depth=1
	leaq	1(%r15), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	jmp	.LBB6_13
.LBB6_10:                               #   in Loop: Header=BB6_13 Depth=2
	andl	$65280, %esi            # imm = 0xFF00
	movq	%rdi, %rbx
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbx, %rdi
	movl	(%rsp), %r9d            # 4-byte Reload
	jmp	.LBB6_174
.LBB6_11:                               #   in Loop: Header=BB6_13 Depth=2
	andl	$65280, %esi            # imm = 0xFF00
	movq	%rdi, %rbp
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbp, %rdi
	movl	(%rsp), %r9d            # 4-byte Reload
	jmp	.LBB6_175
.LBB6_12:                               #   in Loop: Header=BB6_13 Depth=2
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB6_175
	.p2align	4, 0x90
.LBB6_13:                               #   Parent Loop BB6_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_18 Depth 3
                                        #         Child Loop BB6_24 Depth 4
                                        #         Child Loop BB6_30 Depth 4
                                        #       Child Loop BB6_41 Depth 3
                                        #         Child Loop BB6_47 Depth 4
                                        #         Child Loop BB6_53 Depth 4
                                        #       Child Loop BB6_140 Depth 3
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_114 Depth 4
                                        #           Child Loop BB6_119 Depth 5
                                        #         Child Loop BB6_128 Depth 4
                                        #         Child Loop BB6_137 Depth 4
                                        #         Child Loop BB6_171 Depth 4
	movzbl	20(%rbx), %eax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r10d
	subl	%eax, %r10d
	movzwl	16(%rbx), %r14d
	leal	(%r14,%r10), %ecx
	cmpl	%r9d, %ecx
	ja	.LBB6_175
# BB#14:                                #   in Loop: Header=BB6_13 Depth=2
	movzwl	18(%rbx), %r8d
	movzwl	%r8w, %ecx
	movl	%r10d, %edx
	subl	%ecx, %edx
	jb	.LBB6_175
# BB#15:                                #   in Loop: Header=BB6_13 Depth=2
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpq	120(%rsp), %rcx         # 8-byte Folded Reload
	movl	%ecx, %r11d
	jae	.LBB6_38
# BB#16:                                #   in Loop: Header=BB6_13 Depth=2
	cmpw	%r14w, %ax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r11d
	jae	.LBB6_38
# BB#17:                                # %.lr.ph170.i
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movw	58(%rcx), %bx
	movq	(%rcx), %rbp
	movzwl	%ax, %r15d
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r11d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_24 Depth 4
                                        #         Child Loop BB6_30 Depth 4
	movzwl	(%rbp,%r15,2), %esi
	movl	%esi, %eax
	shrl	$8, %eax
	cmpb	$4, %al
	ja	.LBB6_10
# BB#19:                                #   in Loop: Header=BB6_18 Depth=3
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_20:                               #   in Loop: Header=BB6_18 Depth=3
	movzbl	%sil, %eax
	movl	%r11d, %ecx
	movzbl	(%rdi,%rcx), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_36
	jmp	.LBB6_174
.LBB6_21:                               #   in Loop: Header=BB6_18 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rax
	movzwl	%bx, %ecx
	movq	(%rax,%rcx,8), %rbp
	cmpb	$0, (%rbp)
	je	.LBB6_28
# BB#22:                                # %.preheader133.i
                                        #   in Loop: Header=BB6_18 Depth=3
	movzwl	18(%rbp), %eax
	testw	%ax, %ax
	je	.LBB6_174
# BB#23:                                # %.lr.ph163.i
                                        #   in Loop: Header=BB6_18 Depth=3
	movq	8(%rbp), %rcx
	movl	%r11d, %edx
	movb	(%rdi,%rdx), %dl
	xorl	%esi, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_24:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	%dl, (%rcx,%rsi)
	je	.LBB6_33
# BB#25:                                #   in Loop: Header=BB6_24 Depth=4
	incq	%rsi
	cmpw	%ax, %si
	jb	.LBB6_24
	jmp	.LBB6_174
.LBB6_26:                               #   in Loop: Header=BB6_18 Depth=3
	andl	$240, %esi
	movl	%r11d, %eax
	movzbl	(%rdi,%rax), %eax
	andl	$240, %eax
	cmpl	%eax, %esi
	je	.LBB6_36
	jmp	.LBB6_174
.LBB6_27:                               #   in Loop: Header=BB6_18 Depth=3
	andl	$15, %esi
	movl	%r11d, %eax
	movzbl	(%rdi,%rax), %eax
	andl	$15, %eax
	cmpl	%eax, %esi
	je	.LBB6_36
	jmp	.LBB6_174
.LBB6_28:                               # %.preheader131.i
                                        #   in Loop: Header=BB6_18 Depth=3
	testq	%rbp, %rbp
	je	.LBB6_174
# BB#29:                                # %.lr.ph165.i
                                        #   in Loop: Header=BB6_18 Depth=3
	movl	%ebx, 100(%rsp)         # 4-byte Spill
	movw	%r8w, 12(%rsp)          # 2-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	addq	%rdi, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_30:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	16(%rbp), %ebx
	leal	(%rbx,%r11), %eax
	cmpl	%r9d, %eax
	ja	.LBB6_32
# BB#31:                                #   in Loop: Header=BB6_30 Depth=4
	movq	8(%rbp), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r11, %r12
	callq	memcmp
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r11
	movl	(%rsp), %r9d            # 4-byte Reload
	testl	%eax, %eax
	je	.LBB6_34
.LBB6_32:                               #   in Loop: Header=BB6_30 Depth=4
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB6_30
	jmp	.LBB6_173
.LBB6_33:                               #   in Loop: Header=BB6_18 Depth=3
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB6_35
.LBB6_34:                               #   in Loop: Header=BB6_18 Depth=3
	leal	-1(%r11,%rbx), %r11d
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movzwl	12(%rsp), %r8d          # 2-byte Folded Reload
	movl	100(%rsp), %ebx         # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB6_35:                               #   in Loop: Header=BB6_18 Depth=3
	incl	%ebx
	.p2align	4, 0x90
.LBB6_36:                               #   in Loop: Header=BB6_18 Depth=3
	incl	%r11d
	incq	%r15
	cmpq	%r14, %r15
	jae	.LBB6_38
# BB#37:                                #   in Loop: Header=BB6_18 Depth=3
	cmpl	%r9d, %r11d
	jb	.LBB6_18
.LBB6_38:                               # %.critedge.i
                                        #   in Loop: Header=BB6_13 Depth=2
	testw	%r8w, %r8w
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB6_61
# BB#39:                                # %.critedge.i
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB6_61
# BB#40:                                # %.lr.ph155.i
                                        #   in Loop: Header=BB6_13 Depth=2
	xorl	%ebp, %ebp
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_41:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_47 Depth 4
                                        #         Child Loop BB6_53 Depth 4
	movzwl	(%r12,%rbp,2), %esi
	movl	%esi, %eax
	shrl	$8, %eax
	cmpb	$4, %al
	ja	.LBB6_11
# BB#42:                                #   in Loop: Header=BB6_41 Depth=3
	jmpq	*.LJTI6_1(,%rax,8)
.LBB6_43:                               #   in Loop: Header=BB6_41 Depth=3
	movzbl	%sil, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rcx), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_59
	jmp	.LBB6_175
.LBB6_44:                               #   in Loop: Header=BB6_41 Depth=3
	movq	64(%rbx), %rax
	movzwl	72(%rsp), %ecx          # 2-byte Folded Reload
	movq	(%rax,%rcx,8), %r14
	cmpb	$0, (%r14)
	movq	%rdx, %r12
	je	.LBB6_51
# BB#45:                                # %.preheader127.i
                                        #   in Loop: Header=BB6_41 Depth=3
	movzwl	18(%r14), %eax
	testw	%ax, %ax
	je	.LBB6_175
# BB#46:                                # %.lr.ph.i
                                        #   in Loop: Header=BB6_41 Depth=3
	movq	8(%r14), %rcx
	movl	%edx, %edx
	movb	(%rdi,%rdx), %dl
	xorl	%esi, %esi
	movq	88(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_47:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_41 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	%dl, (%rcx,%rsi)
	je	.LBB6_56
# BB#48:                                #   in Loop: Header=BB6_47 Depth=4
	incq	%rsi
	cmpw	%ax, %si
	jb	.LBB6_47
	jmp	.LBB6_175
.LBB6_49:                               #   in Loop: Header=BB6_41 Depth=3
	andl	$240, %esi
	movl	%edx, %eax
	movzbl	(%rdi,%rax), %eax
	andl	$240, %eax
	cmpl	%eax, %esi
	je	.LBB6_59
	jmp	.LBB6_175
.LBB6_50:                               #   in Loop: Header=BB6_41 Depth=3
	andl	$15, %esi
	movl	%edx, %eax
	movzbl	(%rdi,%rax), %eax
	andl	$15, %eax
	cmpl	%eax, %esi
	je	.LBB6_59
	jmp	.LBB6_175
.LBB6_51:                               # %.preheader.i
                                        #   in Loop: Header=BB6_41 Depth=3
	testq	%r14, %r14
	je	.LBB6_175
# BB#52:                                # %.lr.ph151.i
                                        #   in Loop: Header=BB6_41 Depth=3
	movw	%r8w, 12(%rsp)          # 2-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movl	%edx, %r15d
	addq	%rdi, %r15
	.p2align	4, 0x90
.LBB6_53:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_41 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	16(%r14), %ebx
	leal	(%rbx,%rdx), %eax
	cmpl	%r9d, %eax
	ja	.LBB6_55
# BB#54:                                #   in Loop: Header=BB6_53 Depth=4
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memcmp
	movq	%r12, %rdx
	movl	(%rsp), %r9d            # 4-byte Reload
	testl	%eax, %eax
	je	.LBB6_57
.LBB6_55:                               #   in Loop: Header=BB6_53 Depth=4
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.LBB6_53
	jmp	.LBB6_173
.LBB6_56:                               #   in Loop: Header=BB6_41 Depth=3
	movq	%r12, %rdx
	jmp	.LBB6_58
.LBB6_57:                               #   in Loop: Header=BB6_41 Depth=3
	leal	-1(%rdx,%rbx), %edx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movzwl	12(%rsp), %r8d          # 2-byte Folded Reload
	movq	88(%rsp), %r14          # 8-byte Reload
.LBB6_58:                               # %.loopexit129.i
                                        #   in Loop: Header=BB6_41 Depth=3
	incl	72(%rsp)                # 4-byte Folded Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_59:                               #   in Loop: Header=BB6_41 Depth=3
	incl	%edx
	incq	%rbp
	cmpq	%r14, %rbp
	jb	.LBB6_41
# BB#60:                                # %ac_findmatch.exit.preheader
                                        #   in Loop: Header=BB6_13 Depth=2
	testq	%rbx, %rbx
	je	.LBB6_12
.LBB6_61:                               # %.lr.ph345
                                        #   in Loop: Header=BB6_13 Depth=2
	movl	224(%rsp), %eax
	addl	%eax, %r10d
	addl	%eax, %r11d
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	jmp	.LBB6_140
.LBB6_62:                               #   in Loop: Header=BB6_140 Depth=3
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jmp	.LBB6_66
.LBB6_63:                               #   in Loop: Header=BB6_140 Depth=3
	movl	$1, %edx
	cmpl	$2, %r8d
	je	.LBB6_66
.LBB6_64:                               # %.lr.ph.new
                                        #   in Loop: Header=BB6_140 Depth=3
	leaq	(,%rdx,4), %rsi
	leaq	(%rsi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB6_65:                               #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rbp), %edi
	decl	%edi
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rcx
	addq	%rsi, %rcx
	movq	%rcx, (%rax,%rdx,8)
	movq	8(%r12), %rax
	movq	(%rax,%rdi,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movl	$0, (%rcx)
	movl	(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rdi
	leaq	36(%rdi,%rsi), %rdi
	movq	%rdi, 8(%rax,%rdx,8)
	movq	8(%r12), %rax
	movq	(%rax,%rcx,8), %rcx
	movq	8(%rcx,%rdx,8), %rcx
	movl	$0, (%rcx)
	addq	$2, %rdx
	addq	$72, %rsi
	cmpq	%r8, %rdx
	jb	.LBB6_65
	.p2align	4, 0x90
.LBB6_66:                               # %.loopexit
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rbp
	movzwl	54(%rbx), %eax
	cmpq	$1, %rax
	sete	%cl
	jne	.LBB6_70
# BB#67:                                #   in Loop: Header=BB6_140 Depth=3
	movb	$1, %cl
	movl	$1, %eax
.LBB6_68:                               # %.critedge.thread
                                        #   in Loop: Header=BB6_140 Depth=3
	movq	-8(%rbp,%rax,8), %rax
	movl	(%rax), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%edx, %esi
	andl	$-8, %esi
	movl	%edx, %edi
	subl	%esi, %edi
	negl	%esi
	leal	1(%rdx,%rsi), %edx
	movl	%edx, (%rax)
	movslq	%edi, %rdx
	movl	%r11d, 4(%rax,%rdx,4)
	testb	%cl, %cl
	je	.LBB6_133
# BB#69:                                #   in Loop: Header=BB6_140 Depth=3
	movzwl	52(%rbx), %eax
	movq	-8(%rbp,%rax,8), %rax
	movq	(%rbp), %rcx
	movslq	(%rcx), %rcx
	movl	%r15d, (%rax,%rcx,4)
	jmp	.LBB6_133
.LBB6_70:                               # %.preheader254
                                        #   in Loop: Header=BB6_140 Depth=3
	movq	-16(%rbp,%rax,8), %rsi
	movl	4(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#71:                                #   in Loop: Header=BB6_140 Depth=3
	movl	28(%rbx), %r8d
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_73
# BB#72:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_74
.LBB6_73:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_74:                               # %.thread
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	8(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#75:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_77
# BB#76:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_78
.LBB6_77:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_78:                               # %.thread.1
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	12(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#79:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_81
# BB#80:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_82
.LBB6_81:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_82:                               # %.thread.2
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	16(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#83:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_85
# BB#84:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_86
.LBB6_85:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_86:                               # %.thread.3
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	20(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#87:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_89
# BB#88:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_90
.LBB6_89:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_90:                               # %.thread.4
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	24(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#91:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_93
# BB#92:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_94
.LBB6_93:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_94:                               # %.thread.5
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	28(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#95:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edi
	subl	%edx, %edi
	testl	%r8d, %r8d
	je	.LBB6_97
# BB#96:                                #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %edi
	ja	.LBB6_98
.LBB6_97:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %edi
	jae	.LBB6_102
.LBB6_98:                               # %.thread.6
                                        #   in Loop: Header=BB6_140 Depth=3
	movl	32(%rsi), %edx
	cmpl	$-1, %edx
	je	.LBB6_133
# BB#99:                                #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %esi
	subl	%edx, %esi
	testl	%r8d, %r8d
	je	.LBB6_101
# BB#100:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	%r8d, %esi
	ja	.LBB6_133
.LBB6_101:                              #   in Loop: Header=BB6_140 Depth=3
	cmpl	24(%rbx), %esi
	jb	.LBB6_133
.LBB6_102:                              # %.critedge
                                        #   in Loop: Header=BB6_140 Depth=3
	cmpw	52(%rbx), %ax
	jne	.LBB6_68
# BB#103:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	74(%rbx), %r15d
	testl	%r15d, %r15d
	je	.LBB6_183
# BB#104:                               #   in Loop: Header=BB6_140 Depth=3
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB6_133
# BB#105:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	$502, %r15d             # imm = 0x1F6
	je	.LBB6_108
# BB#106:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	%r15w, %ecx
	cmpl	$529, %ecx              # imm = 0x211
	ja	.LBB6_108
# BB#107:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	8(%rsp), %r15d          # 4-byte Folded Reload
	jle	.LBB6_133
.LBB6_108:                              #   in Loop: Header=BB6_140 Depth=3
	movq	248(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB6_125
# BB#109:                               #   in Loop: Header=BB6_140 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB6_111
# BB#110:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	16(%rcx), %edx
	cmpl	$9, %edx
	ja	.LBB6_125
.LBB6_111:                              #   in Loop: Header=BB6_140 Depth=3
	movzwl	%r15w, %edx
	cmpl	$502, %edx              # imm = 0x1F6
	sete	%sil
	cmpl	$529, %r15d             # imm = 0x211
	seta	%dil
	cmpl	$502, 232(%rsp)         # imm = 0x1F6
	sete	%dl
	testb	%dil, %dl
	jne	.LBB6_113
# BB#112:                               #   in Loop: Header=BB6_140 Depth=3
	andb	7(%rsp), %sil           # 1-byte Folded Reload
	je	.LBB6_125
.LBB6_113:                              # %.preheader.preheader
                                        #   in Loop: Header=BB6_140 Depth=3
	movw	$1, %r14w
	.p2align	4, 0x90
.LBB6_114:                              # %.preheader
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB6_119 Depth 5
	movq	(%rbp), %rax
	movzwl	%r14w, %r14d
	cmpl	$-1, (%rax,%r14,4)
	je	.LBB6_124
# BB#115:                               #   in Loop: Header=BB6_114 Depth=4
	movzwl	52(%rbx), %eax
	movq	-8(%rbp,%rax,8), %rax
	movslq	(%rax,%r14,4), %r12
	testq	%rcx, %rcx
	je	.LBB6_117
# BB#116:                               #   in Loop: Header=BB6_114 Depth=4
	movzwl	16(%rcx), %eax
	cmpl	$9, %eax
	ja	.LBB6_123
.LBB6_117:                              #   in Loop: Header=BB6_114 Depth=4
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB6_191
# BB#118:                               #   in Loop: Header=BB6_114 Depth=4
	movl	%r15d, (%rax)
	movq	%r12, 8(%rax)
	movq	248(%rsp), %rsi
	movq	(%rsi), %rcx
	.p2align	4, 0x90
.LBB6_119:                              #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        #         Parent Loop BB6_114 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	testq	%rcx, %rcx
	je	.LBB6_121
# BB#120:                               #   in Loop: Header=BB6_119 Depth=5
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_119
	jmp	.LBB6_122
.LBB6_121:                              #   in Loop: Header=BB6_114 Depth=4
	movq	%rsi, %rdx
.LBB6_122:                              # %.thread.i
                                        #   in Loop: Header=BB6_114 Depth=4
	movq	%rax, (%rdx)
	movq	(%rsi), %rcx
	incw	16(%rcx)
.LBB6_123:                              #   in Loop: Header=BB6_114 Depth=4
	incl	%r14d
	movzwl	%r14w, %eax
	cmpl	$9, %eax
	jb	.LBB6_114
.LBB6_124:                              # %.critedge8.loopexit
                                        #   in Loop: Header=BB6_140 Depth=3
	movw	52(%rbx), %ax
	movq	112(%rsp), %r12         # 8-byte Reload
.LBB6_125:                              # %.critedge8
                                        #   in Loop: Header=BB6_140 Depth=3
	movq	(%rbp), %rdi
	movzwl	%ax, %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rdx
	movl	$255, %esi
	callq	memset
	movzwl	52(%rbx), %eax
	testq	%rax, %rax
	je	.LBB6_131
# BB#126:                               # %.lr.ph337
                                        #   in Loop: Header=BB6_140 Depth=3
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$7, %rsi
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	je	.LBB6_134
# BB#127:                               # %.prol.preheader534
                                        #   in Loop: Header=BB6_140 Depth=3
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_128:                              #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbp,%rcx,8), %rdi
	movl	$0, (%rdi)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB6_128
	jmp	.LBB6_135
.LBB6_129:                              #   in Loop: Header=BB6_140 Depth=3
	movq	248(%rsp), %rdx
.LBB6_130:                              # %.thread.i243
                                        #   in Loop: Header=BB6_140 Depth=3
	movq	%rax, (%rdx)
	movq	248(%rsp), %rax
	movq	(%rax), %rax
	incw	16(%rax)
	jmp	.LBB6_133
.LBB6_131:                              #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, 8(%rsp)          # 4-byte Spill
.LBB6_132:                              #   in Loop: Header=BB6_140 Depth=3
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_133:                              # %.sink.split
                                        #   in Loop: Header=BB6_140 Depth=3
	movq	88(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_139
	jmp	.LBB6_173
.LBB6_134:                              #   in Loop: Header=BB6_140 Depth=3
	xorl	%ecx, %ecx
.LBB6_135:                              # %.prol.loopexit535
                                        #   in Loop: Header=BB6_140 Depth=3
	cmpq	$7, %rdx
	jae	.LBB6_137
# BB#136:                               #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	jmp	.LBB6_133
	.p2align	4, 0x90
.LBB6_137:                              #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	8(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	16(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	24(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	32(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	40(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	48(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	56(%rbp,%rcx,8), %rdx
	movl	$0, (%rdx)
	addq	$8, %rcx
	cmpq	%rax, %rcx
	jb	.LBB6_137
# BB#138:                               #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	jmp	.LBB6_133
	.p2align	4, 0x90
.LBB6_139:                              # %.sink.split._crit_edge
                                        #   in Loop: Header=BB6_140 Depth=3
	movzwl	18(%rbx), %r8d
.LBB6_140:                              #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_114 Depth 4
                                        #           Child Loop BB6_119 Depth 5
                                        #         Child Loop BB6_128 Depth 4
                                        #         Child Loop BB6_137 Depth 4
                                        #         Child Loop BB6_171 Depth 4
	movzwl	%r8w, %eax
	movl	%r10d, %r15d
	subl	%eax, %r15d
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_142
# BB#141:                               #   in Loop: Header=BB6_140 Depth=3
	cmpb	$0, 72(%rbx)
	je	.LBB6_146
.LBB6_142:                              #   in Loop: Header=BB6_140 Depth=3
	leaq	48(%rbx), %rbp
	cmpl	$0, 48(%rbx)
	je	.LBB6_144
# BB#143:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	54(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB6_147
.LBB6_144:                              #   in Loop: Header=BB6_140 Depth=3
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB6_133
# BB#145:                               #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %edx
	movq	32(%rbx), %r9
	movl	232(%rsp), %edi
	leaq	128(%rsp), %rcx
	movl	240(%rsp), %r8d
	callq	cli_validatesig
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movl	(%rsp), %r9d            # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB6_147
	jmp	.LBB6_133
.LBB6_146:                              # %._crit_edge417
                                        #   in Loop: Header=BB6_140 Depth=3
	leaq	48(%rbx), %rbp
.LBB6_147:                              #   in Loop: Header=BB6_140 Depth=3
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB6_157
# BB#148:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	54(%rbx), %edx
	decl	%ecx
	cmpq	$1, %rdx
	movq	8(%r12), %rax
	je	.LBB6_151
# BB#149:                               #   in Loop: Header=BB6_140 Depth=3
	movq	(%rax,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB6_133
# BB#150:                               #   in Loop: Header=BB6_140 Depth=3
	movq	-16(%rsi,%rdx,8), %rdx
	cmpl	$0, (%rdx)
	je	.LBB6_133
.LBB6_151:                              # %._crit_edge409
                                        #   in Loop: Header=BB6_140 Depth=3
	cmpq	$0, (%rax,%rcx,8)
	jne	.LBB6_66
# BB#152:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	52(%rbx), %edi
	shlq	$3, %rdi
	callq	cli_malloc
	movq	8(%r12), %rcx
	movl	(%rbp), %esi
	decl	%esi
	movq	%rax, (%rcx,%rsi,8)
	movq	8(%r12), %rax
	cmpq	$0, (%rax,%rsi,8)
	je	.LBB6_187
# BB#153:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	52(%rbx), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rdi
	callq	cli_malloc
	movq	8(%r12), %rcx
	movl	(%rbp), %esi
	decl	%esi
	movq	(%rcx,%rsi,8), %rcx
	movq	%rax, (%rcx)
	movq	8(%r12), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB6_188
# BB#154:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	52(%rbx), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rdx
	movl	$255, %esi
	callq	memset
	movq	8(%r12), %rax
	movl	(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rcx
	movq	(%rcx), %rcx
	movl	$0, (%rcx)
	movzwl	52(%rbx), %r8d
	cmpq	$2, %r8
	jb	.LBB6_62
# BB#155:                               # %.lr.ph
                                        #   in Loop: Header=BB6_140 Depth=3
	testb	$1, %r8b
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jne	.LBB6_63
# BB#156:                               #   in Loop: Header=BB6_140 Depth=3
	movl	(%rbp), %edx
	decl	%edx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rsi
	addq	$36, %rsi
	movq	%rsi, 8(%rax)
	movq	8(%r12), %rax
	movq	(%rax,%rdx,8), %rdx
	movq	8(%rdx), %rdx
	movl	$0, (%rdx)
	movl	$2, %edx
	cmpl	$2, %r8d
	jne	.LBB6_64
	jmp	.LBB6_66
	.p2align	4, 0x90
.LBB6_157:                              #   in Loop: Header=BB6_140 Depth=3
	movzwl	74(%rbx), %eax
	testl	%eax, %eax
	je	.LBB6_183
# BB#158:                               #   in Loop: Header=BB6_140 Depth=3
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB6_133
# BB#159:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	$502, %eax              # imm = 0x1F6
	je	.LBB6_162
# BB#160:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	%ax, %ecx
	cmpl	$529, %ecx              # imm = 0x211
	ja	.LBB6_162
# BB#161:                               #   in Loop: Header=BB6_140 Depth=3
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jle	.LBB6_133
.LBB6_162:                              #   in Loop: Header=BB6_140 Depth=3
	movq	32(%rbx), %rsi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	cli_dbgmsg
	movq	248(%rsp), %rax
	testq	%rax, %rax
	movzwl	74(%rbx), %edi
	movl	%edi, 8(%rsp)           # 4-byte Spill
	je	.LBB6_132
# BB#163:                               #   in Loop: Header=BB6_140 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB6_165
# BB#164:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	16(%rax), %ecx
	cmpl	$9, %ecx
	ja	.LBB6_132
.LBB6_165:                              #   in Loop: Header=BB6_140 Depth=3
	cmpl	$502, 232(%rsp)         # imm = 0x1F6
	sete	%sil
	cmpl	$529, %edi              # imm = 0x211
	seta	%dl
	cmpl	$502, %edi              # imm = 0x1F6
	sete	%cl
	testb	%dl, %sil
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jne	.LBB6_167
# BB#166:                               #   in Loop: Header=BB6_140 Depth=3
	andb	7(%rsp), %cl            # 1-byte Folded Reload
	je	.LBB6_133
.LBB6_167:                              #   in Loop: Header=BB6_140 Depth=3
	testq	%rax, %rax
	je	.LBB6_169
# BB#168:                               #   in Loop: Header=BB6_140 Depth=3
	movzwl	16(%rax), %eax
	cmpl	$9, %eax
	ja	.LBB6_133
.LBB6_169:                              #   in Loop: Header=BB6_140 Depth=3
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB6_191
# BB#170:                               #   in Loop: Header=BB6_140 Depth=3
	movl	%r15d, %ecx
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movq	248(%rsp), %rcx
	movq	(%rcx), %rcx
	movl	(%rsp), %r9d            # 4-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_171:                              #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        #       Parent Loop BB6_140 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rcx, %rcx
	je	.LBB6_129
# BB#172:                               #   in Loop: Header=BB6_171 Depth=4
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_171
	jmp	.LBB6_130
.LBB6_173:                              #   in Loop: Header=BB6_13 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
.LBB6_174:                              #   in Loop: Header=BB6_13 Depth=2
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_175:                              # %ac_findmatch.exit.thread
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	80(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_13
.LBB6_176:                              # %.loopexit261
                                        #   in Loop: Header=BB6_4 Depth=1
	incq	%r15
	cmpq	120(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB6_4
# BB#177:                               # %._crit_edge
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_179
# BB#178:
	callq	free
.LBB6_179:                              # %._crit_edge.thread
	xorl	%eax, %eax
	movl	8(%rsp), %ecx           # 4-byte Reload
.LBB6_180:                              # %._crit_edge.thread
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	cmovnel	%ecx, %eax
	jmp	.LBB6_190
.LBB6_181:
	xorl	%eax, %eax
	jmp	.LBB6_190
.LBB6_182:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %eax
	jmp	.LBB6_190
.LBB6_183:
	movq	104(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB6_185
# BB#184:
	movq	%rax, %rcx
	movq	32(%rbx), %rax
	movq	%rax, (%rcx)
.LBB6_185:
	movq	152(%rsp), %rdi
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.LBB6_190
# BB#186:
	callq	free
	movl	$1, %eax
	jmp	.LBB6_190
.LBB6_187:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	jmp	.LBB6_189
.LBB6_188:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	movq	8(%r12), %rax
	movl	(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rdi
	callq	free
	movq	8(%r12), %rax
	movl	(%rbp), %ecx
	decl	%ecx
	movq	$0, (%rax,%rcx,8)
.LBB6_189:
	movl	$-114, %eax
.LBB6_190:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_191:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	152(%rsp), %rdi
	movl	$-114, %eax
	testq	%rdi, %rdi
	je	.LBB6_190
# BB#192:
	callq	free
	jmp	.LBB6_189
.Lfunc_end6:
	.size	cli_ac_scanbuff, .Lfunc_end6-cli_ac_scanbuff
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_20
	.quad	.LBB6_36
	.quad	.LBB6_21
	.quad	.LBB6_26
	.quad	.LBB6_27
.LJTI6_1:
	.quad	.LBB6_43
	.quad	.LBB6_59
	.quad	.LBB6_44
	.quad	.LBB6_49
	.quad	.LBB6_50

	.text
	.globl	cli_ac_addsig
	.p2align	4, 0x90
	.type	cli_ac_addsig,@function
cli_ac_addsig:                          # @cli_ac_addsig
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 144
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r13d
	movl	%ecx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdx, %rdi
	callq	strlen
	shrq	%rax
	movzbl	36(%r15), %ecx
	movl	$-117, %edx
	cmpq	%rcx, %rax
	jb	.LBB7_152
# BB#1:
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$96, %esi
	callq	cli_calloc
	movq	%rax, %r12
	movl	$-114, %edx
	testq	%r12, %r12
	je	.LBB7_152
# BB#2:
	movl	$-114, 52(%rsp)         # 4-byte Folded Spill
	movb	176(%rsp), %al
	movl	160(%rsp), %ecx
	movl	152(%rsp), %edx
	movzwl	144(%rsp), %esi
	movw	%si, 74(%r12)
	movl	%ebp, 48(%r12)
	movw	%r13w, 52(%r12)
	movw	%r14w, 54(%r12)
	movl	%edx, 24(%r12)
	movl	%ecx, 28(%r12)
	movb	%al, 72(%r12)
	movl	$40, %esi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_36
# BB#3:
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB7_78
# BB#4:
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_76
# BB#5:                                 # %.preheader361
	movl	$40, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_79
# BB#6:                                 # %.lr.ph451
	leaq	56(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
.LBB7_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_13 Depth 2
                                        #     Child Loop BB7_22 Depth 2
                                        #       Child Loop BB7_28 Depth 3
	movb	$0, (%r14)
	testq	%rbx, %rbx
	je	.LBB7_165
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=1
	incq	%r14
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	strcat
	movq	%r13, %rdi
	callq	strlen
	movb	$0, 2(%r13,%rax)
	movw	$10536, (%r13,%rax)     # imm = 0x2928
	movl	$41, %esi
	movq	%r14, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB7_165
# BB#9:                                 #   in Loop: Header=BB7_7 Depth=1
	movq	%rbx, %rax
	incq	%rbx
	movb	$0, (%rax)
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB7_179
# BB#10:                                #   in Loop: Header=BB7_7 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movzwl	56(%r12), %eax
	incl	%eax
	movw	%ax, 56(%r12)
	movq	64(%r12), %rdi
	movzwl	%ax, %esi
	shlq	$3, %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_181
# BB#11:                                #   in Loop: Header=BB7_7 Depth=1
	movzwl	56(%r12), %ecx
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, -8(%rax,%rcx,8)
	movq	%rax, 64(%r12)
	movq	%r14, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB7_16
# BB#12:                                # %.lr.ph446.preheader
                                        #   in Loop: Header=BB7_7 Depth=1
	movw	$1, %bx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_13:                               # %.lr.ph446
                                        #   Parent Loop BB7_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$124, (%r14,%rbp)
	jne	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=2
	incw	18(%r15)
.LBB7_15:                               #   in Loop: Header=BB7_13 Depth=2
	movzwl	%bx, %ebp
	movq	%r14, %rdi
	callq	strlen
	leal	1(%rbx), %ebx
	cmpq	%rax, %rbp
	jb	.LBB7_13
.LBB7_16:                               # %._crit_edge447
                                        #   in Loop: Header=BB7_7 Depth=1
	movw	18(%r15), %bp
	testw	%bp, %bp
	je	.LBB7_183
# BB#17:                                #   in Loop: Header=BB7_7 Depth=1
	incl	%ebp
	movw	%bp, 18(%r15)
	movzwl	%bp, %r13d
	leal	-1(%r13,%r13,2), %ebx
	movq	%r14, %rdi
	callq	strlen
	movzwl	%ax, %eax
	cmpl	%eax, %ebx
	jne	.LBB7_20
# BB#18:                                #   in Loop: Header=BB7_7 Depth=1
	movb	$1, (%r15)
	movq	%r13, %rdi
	callq	cli_malloc
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.LBB7_184
# BB#19:                                # %..preheader359_crit_edge
                                        #   in Loop: Header=BB7_7 Depth=1
	movw	18(%r15), %bp
.LBB7_20:                               # %.preheader359
                                        #   in Loop: Header=BB7_7 Depth=1
	testw	%bp, %bp
	movq	40(%rsp), %r13          # 8-byte Reload
	je	.LBB7_34
# BB#21:                                # %.lr.ph449
                                        #   in Loop: Header=BB7_7 Depth=1
	addq	$8, %r15
	movq	%r15, 72(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_22:                               #   Parent Loop BB7_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_28 Depth 3
	movzwl	%bp, %ebx
	movl	$.L.str.18, %edx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	cli_strtok
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB7_96
# BB#23:                                #   in Loop: Header=BB7_22 Depth=2
	movq	%r12, %rdi
	callq	cli_hex2str
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB7_95
# BB#24:                                #   in Loop: Header=BB7_22 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_22 Depth=2
	movb	(%r15), %al
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movb	%al, (%rcx,%rbx)
	movq	%r15, %rdi
	callq	free
	jmp	.LBB7_33
.LBB7_26:                               #   in Loop: Header=BB7_22 Depth=2
	testw	%bp, %bp
	je	.LBB7_31
# BB#27:                                # %.preheader358.preheader
                                        #   in Loop: Header=BB7_22 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_28:                               # %.preheader358
                                        #   Parent Loop BB7_7 Depth=1
                                        #     Parent Loop BB7_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %r13
	movq	24(%r13), %rax
	testq	%rax, %rax
	jne	.LBB7_28
# BB#29:                                #   in Loop: Header=BB7_22 Depth=2
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %rbx
	movq	%rbx, 24(%r13)
	testq	%rbx, %rbx
	je	.LBB7_178
# BB#30:                                #   in Loop: Header=BB7_22 Depth=2
	movq	%rbx, %rax
	addq	$8, %rax
	movl	$-114, 52(%rsp)         # 4-byte Folded Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB7_32
.LBB7_31:                               #   in Loop: Header=BB7_22 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB7_32:                               #   in Loop: Header=BB7_22 Depth=2
	movq	%r15, (%rax)
	movq	%r12, %rdi
	callq	strlen
	shrq	%rax
	movw	%ax, 16(%rbx)
.LBB7_33:                               #   in Loop: Header=BB7_22 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%r12, %rdi
	callq	free
	incl	%ebp
	cmpw	18(%rbx), %bp
	jb	.LBB7_22
.LBB7_34:                               # %.loopexit360
                                        #   in Loop: Header=BB7_7 Depth=1
	movl	$40, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r14
	testq	%r14, %r14
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB7_7
# BB#35:
	xorl	%r14d, %r14d
	jmp	.LBB7_180
.LBB7_36:
	xorl	%r13d, %r13d
.LBB7_37:
	cmpw	$0, 56(%r12)
	movq	%rbp, %rdi
	cmovneq	%r13, %rdi
	callq	cli_hex2ui
	movq	%rax, (%r12)
	movzwl	56(%r12), %ebx
	testq	%rax, %rax
	je	.LBB7_67
# BB#38:
	testw	%bx, %bx
	cmovneq	%r13, %rbp
	movq	%rbp, %rdi
	callq	strlen
	shrq	%rax
	testw	%bx, %bx
	movw	%ax, 16(%r12)
	je	.LBB7_40
# BB#39:
	movq	%r13, %rdi
	callq	free
.LBB7_40:                               # %.preheader357
	movzbl	37(%r15), %r10d
	testl	%r10d, %r10d
	je	.LBB7_50
# BB#41:                                # %.lr.ph438
	movw	16(%r12), %r9w
	movb	$1, %al
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_42:                               # =>This Inner Loop Header: Depth=1
	cmpw	%r9w, %cx
	jae	.LBB7_49
# BB#43:                                #   in Loop: Header=BB7_42 Depth=1
	movq	(%r12), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	cmpl	$255, %edx
	ja	.LBB7_51
# BB#44:                                #   in Loop: Header=BB7_42 Depth=1
	testw	%dx, %dx
	movb	%al, %dl
	je	.LBB7_46
# BB#45:                                #   in Loop: Header=BB7_42 Depth=1
	xorl	%edx, %edx
.LBB7_46:                               #   in Loop: Header=BB7_42 Depth=1
	testb	%al, %al
	je	.LBB7_48
# BB#47:                                #   in Loop: Header=BB7_42 Depth=1
	movl	%edx, %eax
.LBB7_48:                               #   in Loop: Header=BB7_42 Depth=1
	incq	%rcx
	movzwl	%cx, %edx
	cmpl	%r10d, %edx
	jb	.LBB7_42
.LBB7_49:                               # %.critedge277
	testb	%al, %al
	je	.LBB7_108
.LBB7_50:                               # %.critedge277..critedge_crit_edge
	movw	16(%r12), %r9w
.LBB7_51:                               # %.critedge
	movzbl	36(%r15), %eax
	movzwl	%ax, %esi
	leal	1(%r9), %eax
	xorl	%edi, %edi
	subw	%si, %ax
	je	.LBB7_80
# BB#52:                                # %.preheader.lr.ph
	movzwl	%ax, %r8d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_53:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_55 Depth 2
	testb	%r10b, %r10b
	je	.LBB7_63
# BB#54:                                # %.lr.ph410
                                        #   in Loop: Header=BB7_53 Depth=1
	leal	(%r10,%rdi), %ebx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB7_55:                               #   Parent Loop BB7_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdi,%rbp), %ecx
	cmpw	%r9w, %cx
	jae	.LBB7_63
# BB#56:                                #   in Loop: Header=BB7_55 Depth=2
	movq	(%r12), %rdx
	movzwl	%cx, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	cmpl	$255, %ecx
	ja	.LBB7_63
# BB#57:                                #   in Loop: Header=BB7_55 Depth=2
	movzbl	%al, %ecx
	cmpl	%ecx, %ebp
	jl	.LBB7_59
# BB#58:                                #   in Loop: Header=BB7_55 Depth=2
	movb	%bpl, %al
.LBB7_59:                               #   in Loop: Header=BB7_55 Depth=2
	cmovgew	%di, %r11w
	cmpb	%r10b, %al
	jb	.LBB7_62
# BB#60:                                #   in Loop: Header=BB7_55 Depth=2
	movzwl	%r11w, %ecx
	cmpw	$0, (%rdx,%rcx,2)
	jne	.LBB7_63
# BB#61:                                #   in Loop: Header=BB7_55 Depth=2
	cmpw	$0, 2(%rdx,%rcx,2)
	jne	.LBB7_63
.LBB7_62:                               #   in Loop: Header=BB7_55 Depth=2
	leal	(%rdi,%rbp), %ecx
	movzwl	%cx, %ecx
	incl	%ebp
	cmpl	%ebx, %ecx
	jb	.LBB7_55
	.p2align	4, 0x90
.LBB7_63:                               # %.critedge3
                                        #   in Loop: Header=BB7_53 Depth=1
	cmpb	%r10b, %al
	jb	.LBB7_66
# BB#64:                                #   in Loop: Header=BB7_53 Depth=1
	movq	(%r12), %rcx
	movzwl	%r11w, %edx
	cmpw	$0, (%rcx,%rdx,2)
	jne	.LBB7_81
# BB#65:                                #   in Loop: Header=BB7_53 Depth=1
	cmpw	$0, 2(%rcx,%rdx,2)
	jne	.LBB7_81
.LBB7_66:                               #   in Loop: Header=BB7_53 Depth=1
	incl	%edi
	cmpl	%r8d, %edi
	jb	.LBB7_53
	jmp	.LBB7_81
.LBB7_67:
	testw	%bx, %bx
	je	.LBB7_151
# BB#68:
	movq	%r13, %rdi
	callq	free
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_151
# BB#69:                                # %.lr.ph19.preheader.i279
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_70:                               # %.lr.ph19.i281
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_71 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_75
	.p2align	4, 0x90
.LBB7_71:                               # %.lr.ph.i283
                                        #   Parent Loop BB7_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_73
# BB#72:                                #   in Loop: Header=BB7_71 Depth=2
	callq	free
.LBB7_73:                               #   in Loop: Header=BB7_71 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_71
# BB#74:                                # %._crit_edge.loopexit.i286
                                        #   in Loop: Header=BB7_70 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_75:                               # %._crit_edge.i287
                                        #   in Loop: Header=BB7_70 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_70
	jmp	.LBB7_150
.LBB7_76:
	movq	%rbx, %rdi
.LBB7_77:
	callq	free
.LBB7_78:
	movq	%r12, %rdi
	callq	free
	movl	$-114, %edx
	jmp	.LBB7_152
.LBB7_79:
	xorl	%r14d, %r14d
	movq	%rbx, %rsi
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB7_98
.LBB7_80:
	xorl	%eax, %eax
	xorl	%r11d, %r11d
.LBB7_81:                               # %._crit_edge
	cmpb	%sil, %al
	jae	.LBB7_92
# BB#82:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_91
# BB#83:                                # %.lr.ph19.preheader.i290
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_84:                               # %.lr.ph19.i292
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_85 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_89
	.p2align	4, 0x90
.LBB7_85:                               # %.lr.ph.i294
                                        #   Parent Loop BB7_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_87
# BB#86:                                #   in Loop: Header=BB7_85 Depth=2
	callq	free
.LBB7_87:                               #   in Loop: Header=BB7_85 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_85
# BB#88:                                # %._crit_edge.loopexit.i297
                                        #   in Loop: Header=BB7_84 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_89:                               # %._crit_edge.i298
                                        #   in Loop: Header=BB7_84 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_84
# BB#90:                                # %._crit_edge20.i299
	callq	free
.LBB7_91:                               # %ac_free_alt.exit300
	movq	(%r12), %rdi
	jmp	.LBB7_150
.LBB7_92:
	movq	(%r12), %rdx
	movq	%rdx, 8(%r12)
	movw	%r11w, 18(%r12)
	movzwl	%r11w, %esi
	leaq	(%rdx,%rsi,2), %rax
	movq	%rax, (%r12)
	subl	%r11d, %r9d
	movw	%r9w, 16(%r12)
	testw	%si, %si
	je	.LBB7_117
# BB#93:                                # %.lr.ph
	testb	$1, %r11b
	jne	.LBB7_109
# BB#94:
	xorl	%eax, %eax
	cmpl	$1, %esi
	jne	.LBB7_112
	jmp	.LBB7_117
.LBB7_95:
	movq	%r12, %rdi
	callq	free
.LBB7_96:
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB7_97:                               # %.thread346
	movl	$-116, %r14d
.LBB7_98:                               # %.thread346
	movq	%r13, %rdi
	callq	strcat
	movq	%rbx, %rdi
	callq	free
	testl	%r14d, %r14d
	je	.LBB7_37
# BB#99:                                # %.thread346._crit_edge
	movq	%r12, %rbx
	addq	$56, %rbx
	movl	%r14d, %r15d
	cmpw	$0, (%rbx)
	je	.LBB7_139
.LBB7_100:
	movq	%r13, %rdi
	callq	free
	movzwl	(%rbx), %eax
	testw	%ax, %ax
	je	.LBB7_139
# BB#101:                               # %.lr.ph19.preheader.i
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_102:                              # %.lr.ph19.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_103 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_107
	.p2align	4, 0x90
.LBB7_103:                              # %.lr.ph.i
                                        #   Parent Loop BB7_102 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_105
# BB#104:                               #   in Loop: Header=BB7_103 Depth=2
	callq	free
.LBB7_105:                              #   in Loop: Header=BB7_103 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_103
# BB#106:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB7_102 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_107:                              # %._crit_edge.i
                                        #   in Loop: Header=BB7_102 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_102
	jmp	.LBB7_138
.LBB7_108:                              # %.critedge277..loopexit_crit_edge
	movw	16(%r12), %r9w
	cmpw	(%r15), %r9w
	ja	.LBB7_118
	jmp	.LBB7_119
.LBB7_109:
	movzwl	(%rdx), %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	movw	$1, %ax
	cmpl	$512, %ecx              # imm = 0x200
	jne	.LBB7_111
# BB#110:
	incw	58(%r12)
.LBB7_111:                              # %.prol.loopexit
	cmpl	$1, %esi
	je	.LBB7_117
	.p2align	4, 0x90
.LBB7_112:                              # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	cmpl	$512, %ecx              # imm = 0x200
	jne	.LBB7_114
# BB#113:                               #   in Loop: Header=BB7_112 Depth=1
	incw	58(%r12)
.LBB7_114:                              #   in Loop: Header=BB7_112 Depth=1
	leal	1(%rax), %ecx
	movzwl	%cx, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	cmpl	$512, %ecx              # imm = 0x200
	jne	.LBB7_116
# BB#115:                               #   in Loop: Header=BB7_112 Depth=1
	incw	58(%r12)
.LBB7_116:                              #   in Loop: Header=BB7_112 Depth=1
	addl	$2, %eax
	cmpw	%r11w, %ax
	jb	.LBB7_112
.LBB7_117:                              # %.loopexit
	cmpw	(%r15), %r9w
	jbe	.LBB7_119
.LBB7_118:
	movw	%r9w, (%r15)
.LBB7_119:
	movl	$.L.str.21, %esi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strstr
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB7_121
# BB#120:
	movq	%rbp, %rdi
	callq	strlen
	subq	%rax, %rbx
.LBB7_121:
	testb	%bl, %bl
	je	.LBB7_140
# BB#122:
	movzbl	%bl, %ebx
	leaq	1(%rbx), %rdi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.LBB7_153
# BB#123:
	movq	168(%rsp), %rbp
	movq	%rax, %rdi
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	strncpy
	testq	%rbp, %rbp
	je	.LBB7_125
# BB#124:
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.LBB7_166
.LBB7_125:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	cli_ac_addpatt
	testl	%eax, %eax
	je	.LBB7_164
# BB#126:
	movq	8(%r12), %rdi
	movl	%eax, %r15d
	testq	%rdi, %rdi
	jne	.LBB7_128
# BB#127:
	movq	(%r12), %rdi
.LBB7_128:
	callq	free
	movq	32(%r12), %rdi
	callq	free
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_137
# BB#129:                               # %.lr.ph19.preheader.i334
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
.LBB7_130:                              # %.lr.ph19.i336
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_131 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_135
	.p2align	4, 0x90
.LBB7_131:                              # %.lr.ph.i338
                                        #   Parent Loop BB7_130 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_133
# BB#132:                               #   in Loop: Header=BB7_131 Depth=2
	callq	free
.LBB7_133:                              #   in Loop: Header=BB7_131 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_131
# BB#134:                               # %._crit_edge.loopexit.i341
                                        #   in Loop: Header=BB7_130 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_135:                              # %._crit_edge.i342
                                        #   in Loop: Header=BB7_130 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_130
# BB#136:                               # %._crit_edge20.i343
	callq	free
.LBB7_137:                              # %ac_free_alt.exit344
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_139
.LBB7_138:
	callq	free
.LBB7_139:
	movq	%r12, %rdi
	callq	free
	movl	%r15d, %edx
	jmp	.LBB7_152
.LBB7_140:
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB7_142
# BB#141:
	movq	(%r12), %rdi
.LBB7_142:
	callq	free
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_151
# BB#143:                               # %.lr.ph19.preheader.i301
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_144:                              # %.lr.ph19.i303
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_145 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_149
	.p2align	4, 0x90
.LBB7_145:                              # %.lr.ph.i305
                                        #   Parent Loop BB7_144 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_147
# BB#146:                               #   in Loop: Header=BB7_145 Depth=2
	callq	free
.LBB7_147:                              #   in Loop: Header=BB7_145 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_145
# BB#148:                               # %._crit_edge.loopexit.i308
                                        #   in Loop: Header=BB7_144 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_149:                              # %._crit_edge.i309
                                        #   in Loop: Header=BB7_144 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_144
.LBB7_150:                              # %._crit_edge20.i288
	callq	free
.LBB7_151:                              # %ac_free_alt.exit289
	movq	%r12, %rdi
	callq	free
	movl	$-116, %edx
.LBB7_152:                              # %.thread356
	movl	%edx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_153:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB7_155
# BB#154:
	movq	(%r12), %rdi
.LBB7_155:
	callq	free
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_78
# BB#156:                               # %.lr.ph19.preheader.i312
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_157:                              # %.lr.ph19.i314
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_158 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_162
	.p2align	4, 0x90
.LBB7_158:                              # %.lr.ph.i316
                                        #   Parent Loop BB7_157 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_160
# BB#159:                               #   in Loop: Header=BB7_158 Depth=2
	callq	free
.LBB7_160:                              #   in Loop: Header=BB7_158 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_158
# BB#161:                               # %._crit_edge.loopexit.i319
                                        #   in Loop: Header=BB7_157 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_162:                              # %._crit_edge.i320
                                        #   in Loop: Header=BB7_157 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_157
	jmp	.LBB7_77
.LBB7_164:
	xorl	%edx, %edx
	jmp	.LBB7_152
.LBB7_165:                              # %.thread353
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movl	$-116, %r15d
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpw	$0, (%rbx)
	jne	.LBB7_100
	jmp	.LBB7_139
.LBB7_166:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB7_168
# BB#167:
	movq	(%r12), %rdi
.LBB7_168:
	callq	free
	movzwl	56(%r12), %eax
	testw	%ax, %ax
	je	.LBB7_177
# BB#169:                               # %.lr.ph19.preheader.i323
	movq	64(%r12), %rdi
	xorl	%r14d, %r14d
.LBB7_170:                              # %.lr.ph19.i325
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_171 Depth 2
	movzwl	%r14w, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_175
	.p2align	4, 0x90
.LBB7_171:                              # %.lr.ph.i327
                                        #   Parent Loop BB7_170 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rbp
	testq	%rdi, %rdi
	je	.LBB7_173
# BB#172:                               #   in Loop: Header=BB7_171 Depth=2
	callq	free
.LBB7_173:                              #   in Loop: Header=BB7_171 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB7_171
# BB#174:                               # %._crit_edge.loopexit.i330
                                        #   in Loop: Header=BB7_170 Depth=1
	movzwl	56(%r12), %eax
	movq	64(%r12), %rdi
.LBB7_175:                              # %._crit_edge.i331
                                        #   in Loop: Header=BB7_170 Depth=1
	incl	%r14d
	cmpw	%ax, %r14w
	jb	.LBB7_170
# BB#176:                               # %._crit_edge20.i332
	callq	free
.LBB7_177:                              # %ac_free_alt.exit333
	movq	32(%r12), %rdi
	jmp	.LBB7_77
.LBB7_178:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movl	$-114, %r14d
	movq	56(%rsp), %r12          # 8-byte Reload
	movl	$-114, 52(%rsp)         # 4-byte Folded Spill
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB7_182
.LBB7_179:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %r14d
.LBB7_180:                              # %.thread346
	movq	%rbx, %rsi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB7_98
.LBB7_181:
	movq	64(%rsp), %rax          # 8-byte Reload
	decw	(%rax)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %r14d
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB7_182:                              # %.thread346
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB7_98
.LBB7_183:
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB7_97
.LBB7_184:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %r14d
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB7_98
.Lfunc_end7:
	.size	cli_ac_addsig, .Lfunc_end7-cli_ac_addsig
	.cfi_endproc

	.globl	cli_ac_setdepth
	.p2align	4, 0x90
	.type	cli_ac_setdepth,@function
cli_ac_setdepth:                        # @cli_ac_setdepth
	.cfi_startproc
# BB#0:
	movb	%dil, cli_ac_mindepth(%rip)
	movb	%sil, cli_ac_maxdepth(%rip)
	retq
.Lfunc_end8:
	.size	cli_ac_setdepth, .Lfunc_end8-cli_ac_setdepth
	.cfi_endproc

	.type	cli_ac_mindepth,@object # @cli_ac_mindepth
	.data
	.globl	cli_ac_mindepth
cli_ac_mindepth:
	.byte	2                       # 0x2
	.size	cli_ac_mindepth, 1

	.type	cli_ac_maxdepth,@object # @cli_ac_maxdepth
	.globl	cli_ac_maxdepth
cli_ac_maxdepth:
	.byte	3                       # 0x3
	.size	cli_ac_maxdepth, 1

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_ac_addpatt: Can't allocate memory for pt->trans\n"
	.size	.L.str, 53

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cli_ac_addpatt: Can't allocate memory for AC node\n"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"cli_ac_addpatt: Can't allocate memory for next->trans\n"
	.size	.L.str.2, 55

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cli_ac_addpatt: Can't realloc ac_nodetable\n"
	.size	.L.str.3, 44

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cli_ac_addpatt: Can't realloc ac_pattable\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"cli_ac_buildtrie: AC pattern matcher is not initialised\n"
	.size	.L.str.5, 57

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cli_ac_init: Can't allocate memory for ac_root\n"
	.size	.L.str.6, 48

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"cli_ac_init: Can't allocate memory for ac_root->trans\n"
	.size	.L.str.7, 55

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"cli_ac_init: data == NULL\n"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_ac_init: Can't allocate memory for data->offmatrix\n"
	.size	.L.str.9, 56

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_ac_scanbuff: mdata == NULL\n"
	.size	.L.str.10, 32

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cli_ac_scanbuff: Can't allocate memory for mdata->offmatrix[%u]\n"
	.size	.L.str.11, 65

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"cli_ac_scanbuff: Can't allocate memory for mdata->offmatrix[%u][0]\n"
	.size	.L.str.12, 68

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Matched signature for file type %s at %u\n"
	.size	.L.str.13, 42

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"()"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cli_ac_addsig: Can't allocate newalt\n"
	.size	.L.str.15, 38

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"cli_ac_addsig: Can't realloc new->alttable\n"
	.size	.L.str.16, 44

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"cli_ac_addsig: Can't allocate newalt->str\n"
	.size	.L.str.17, 43

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"|"
	.size	.L.str.18, 2

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"cli_ac_addsig: Can't allocate altpt->next\n"
	.size	.L.str.19, 43

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"cli_ac_addsig: Can't find a static subpattern of length %u\n"
	.size	.L.str.20, 60

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" (Clam)"
	.size	.L.str.21, 8

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"cli_ac_addsig: No virus name\n"
	.size	.L.str.22, 30

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"bfs_enqueue: Can't allocate memory for bfs_list\n"
	.size	.L.str.23, 49

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ac_findmatch: Unknown wildcard 0x%x\n"
	.size	.L.str.24, 37

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"cli_ac_addtype: Can't allocate memory for new type node\n"
	.size	.L.str.25, 57


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
