	.text
	.file	"Random.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	new_Random_seed
	.p2align	4, 0x90
	.type	new_Random_seed,@function
new_Random_seed:                        # @new_Random_seed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	%ebp, %ebx
	negl	%ebx
	cmovll	%ebp, %ebx
	movl	$112, %edi
	callq	malloc
	movabsq	$4467570830353629184, %rcx # imm = 0x3E00000000200000
	movq	%rcx, dm1(%rip)
	movl	%ebp, 68(%rax)
	movl	%ebx, %ecx
	andl	$1, %ecx
	leal	(%rbx,%rcx), %edx
	leal	-1(%rbx,%rcx), %ecx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$16, %esi
	leal	-1(%rsi,%rdx), %edx
	movl	%edx, %esi
	andl	$-65536, %esi           # imm = 0xFFFF0000
	subl	%esi, %ecx
	sarl	$16, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	imull	$9069, %ecx, %ecx       # imm = 0x236D
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ecx, %edi
	movl	%edi, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	sarl	$16, %edi
	imull	$9069, %edx, %edx       # imm = 0x236D
	addl	%edi, %edx
	movl	%edx, %edi
	sarl	$31, %edi
	shrl	$17, %edi
	addl	%edx, %edi
	andl	$-32768, %edi           # imm = 0x8000
	subl	%edi, %edx
	subl	%ebp, %ecx
	movl	%edx, %edi
	shll	$16, %edi
	addl	%ecx, %edi
	movl	%edi, (%rax,%rsi,4)
	incq	%rsi
	cmpq	$17, %rsi
	jne	.LBB0_1
# BB#2:                                 # %initialize.exit
	movl	$4, 72(%rax)
	movl	$16, 76(%rax)
	movq	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, 88(%rax)
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 104(%rax)
	movl	$0, 80(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	new_Random_seed, .Lfunc_end0-new_Random_seed
	.cfi_endproc

	.globl	new_Random
	.p2align	4, 0x90
	.type	new_Random,@function
new_Random:                             # @new_Random
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	movl	%ebp, %ebx
	negl	%ebx
	cmovll	%ebp, %ebx
	movl	$112, %edi
	callq	malloc
	movabsq	$4467570830353629184, %rcx # imm = 0x3E00000000200000
	movq	%rcx, dm1(%rip)
	movl	%ebp, 68(%rax)
	movl	%ebx, %ecx
	andl	$1, %ecx
	leal	(%rbx,%rcx), %edx
	leal	-1(%rbx,%rcx), %ecx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$16, %esi
	leal	-1(%rsi,%rdx), %edx
	movl	%edx, %esi
	andl	$-65536, %esi           # imm = 0xFFFF0000
	subl	%esi, %ecx
	sarl	$16, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	imull	$9069, %ecx, %ecx       # imm = 0x236D
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$16, %edi
	addl	%ecx, %edi
	movl	%edi, %ebp
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	sarl	$16, %edi
	imull	$9069, %edx, %edx       # imm = 0x236D
	addl	%edi, %edx
	movl	%edx, %edi
	sarl	$31, %edi
	shrl	$17, %edi
	addl	%edx, %edi
	andl	$-32768, %edi           # imm = 0x8000
	subl	%edi, %edx
	subl	%ebp, %ecx
	movl	%edx, %edi
	shll	$16, %edi
	addl	%ecx, %edi
	movl	%edi, (%rax,%rsi,4)
	incq	%rsi
	cmpq	$17, %rsi
	jne	.LBB1_1
# BB#2:                                 # %initialize.exit
	movl	$4, 72(%rax)
	movl	$16, 76(%rax)
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 88(%rax)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 96(%rax)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 104(%rax)
	movl	$1, 80(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	new_Random, .Lfunc_end1-new_Random
	.cfi_endproc

	.globl	Random_delete
	.p2align	4, 0x90
	.type	Random_delete,@function
Random_delete:                          # @Random_delete
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	Random_delete, .Lfunc_end2-Random_delete
	.cfi_endproc

	.globl	Random_nextDouble
	.p2align	4, 0x90
	.type	Random_nextDouble,@function
Random_nextDouble:                      # @Random_nextDouble
	.cfi_startproc
# BB#0:
	movslq	72(%rdi), %rcx
	movl	(%rdi,%rcx,4), %esi
	movslq	76(%rdi), %rdx
	movl	(%rdi,%rdx,4), %r8d
	movl	%esi, %eax
	subl	%r8d, %eax
	addl	$2147483647, %eax       # imm = 0x7FFFFFFF
	subl	%r8d, %esi
	cmovsl	%eax, %esi
	leal	-1(%rcx), %eax
	testq	%rcx, %rcx
	movl	$16, %r8d
	cmovel	%r8d, %eax
	leal	-1(%rdx), %ecx
	testq	%rdx, %rdx
	movl	%esi, (%rdi,%rdx,4)
	movl	%eax, 72(%rdi)
	cmovel	%r8d, %ecx
	movl	%ecx, 76(%rdi)
	cmpl	$0, 80(%rdi)
	je	.LBB3_2
# BB#1:
	cvtsi2sdl	%esi, %xmm0
	mulsd	dm1(%rip), %xmm0
	mulsd	104(%rdi), %xmm0
	addsd	88(%rdi), %xmm0
	retq
.LBB3_2:
	cvtsi2sdl	%esi, %xmm0
	mulsd	dm1(%rip), %xmm0
	retq
.Lfunc_end3:
	.size	Random_nextDouble, .Lfunc_end3-Random_nextDouble
	.cfi_endproc

	.globl	RandomVector
	.p2align	4, 0x90
	.type	RandomVector,@function
RandomVector:                           # @RandomVector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %r14d
	movslq	%r14d, %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testl	%ebp, %ebp
	jle	.LBB4_6
# BB#1:                                 # %.lr.ph
	movsd	dm1(%rip), %xmm0        # xmm0 = mem[0],zero
	movl	72(%r15), %r9d
	movl	76(%r15), %ebp
	movl	%r14d, %r10d
	movl	$16, %r8d
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movslq	%r9d, %rdi
	movl	(%r15,%rdi,4), %ebx
	movslq	%ebp, %rcx
	movl	(%r15,%rcx,4), %ebp
	movl	%ebx, %edx
	subl	%ebp, %edx
	addl	$2147483647, %edx       # imm = 0x7FFFFFFF
	subl	%ebp, %ebx
	cmovsl	%edx, %ebx
	movl	%ebx, (%r15,%rcx,4)
	leal	-1(%rdi), %r9d
	testl	%edi, %edi
	cmovel	%r8d, %r9d
	movl	%r9d, 72(%r15)
	leal	-1(%rcx), %ebp
	testl	%ecx, %ecx
	cmovel	%r8d, %ebp
	movl	%ebp, 76(%r15)
	cmpl	$0, 80(%r15)
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	104(%r15), %xmm1
	addsd	88(%r15), %xmm1
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
.LBB4_5:                                # %Random_nextDouble.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movsd	%xmm1, (%rsi)
	addq	$8, %rsi
	decq	%r10
	jne	.LBB4_2
.LBB4_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	RandomVector, .Lfunc_end4-RandomVector
	.cfi_endproc

	.globl	RandomMatrix
	.p2align	4, 0x90
	.type	RandomMatrix,@function
RandomMatrix:                           # @RandomMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 96
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movl	%esi, %r12d
	movl	%edi, %ebp
	movslq	%ebp, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_16
# BB#1:                                 # %.preheader26
	testl	%ebp, %ebp
	jle	.LBB5_17
# BB#2:                                 # %.lr.ph29
	movslq	%r12d, %rax
	leaq	(,%rax,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB5_12
# BB#3:                                 # %.lr.ph29.split.us.preheader
	movsd	dm1(%rip), %xmm1        # xmm1 = mem[0],zero
	movl	%r12d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movl	$16, %r12d
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph29.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	malloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	je	.LBB5_15
# BB#5:                                 # %.lr.ph.us
                                        #   in Loop: Header=BB5_4 Depth=1
	movl	72(%r13), %edx
	movl	76(%r13), %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB5_6:                                #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rbp
	movl	(%r13,%rbp,4), %edi
	movslq	%esi, %rbx
	movl	(%r13,%rbx,4), %edx
	movl	%edi, %esi
	subl	%edx, %esi
	addl	$2147483647, %esi       # imm = 0x7FFFFFFF
	subl	%edx, %edi
	cmovsl	%esi, %edi
	movl	%edi, (%r13,%rbx,4)
	leal	-1(%rbp), %edx
	testl	%ebp, %ebp
	cmovel	%r12d, %edx
	movl	%edx, 72(%r13)
	leal	-1(%rbx), %esi
	testl	%ebx, %ebx
	cmovel	%r12d, %esi
	movl	%esi, 76(%r13)
	cmpl	$0, 80(%r13)
	je	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=2
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	104(%r13), %xmm0
	addsd	88(%r13), %xmm0
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=2
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm1, %xmm0
.LBB5_9:                                # %Random_nextDouble.exit.us
                                        #   in Loop: Header=BB5_6 Depth=2
	movsd	%xmm0, (%rax)
	addq	$8, %rax
	decq	%rcx
	jne	.LBB5_6
# BB#10:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_4 Depth=1
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB5_4
	jmp	.LBB5_17
.LBB5_12:                               # %.lr.ph29.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph29.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	malloc
	movq	%rax, (%r15,%rbx,8)
	testq	%rax, %rax
	je	.LBB5_15
# BB#14:                                # %.preheader
                                        #   in Loop: Header=BB5_13 Depth=1
	incq	%rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB5_13
	jmp	.LBB5_17
.LBB5_15:                               # %.us-lcssa.us
	movq	%r15, %rdi
	callq	free
.LBB5_16:                               # %.loopexit
	xorl	%r15d, %r15d
.LBB5_17:                               # %.loopexit
	movq	%r15, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	RandomMatrix, .Lfunc_end5-RandomMatrix
	.cfi_endproc

	.type	dm1,@object             # @dm1
	.local	dm1
	.comm	dm1,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
