	.text
	.file	"mpeg2dec.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	$0, Verbose_Flag(%rip)
	movq	$.L.str.29, Output_Picture_Filename(%rip)
	movl	$0, hiQdither(%rip)
	movl	$0, Output_Type(%rip)
	movl	$0, Frame_Store_Flag(%rip)
	movl	$0, Spatial_Flag(%rip)
	movq	$.L.str.29, Lower_Layer_Picture_Filename(%rip)
	movl	$0, Reference_IDCT_Flag(%rip)
	movl	$0, Trace_Flag(%rip)
	movl	$0, Quiet_Flag(%rip)
	movl	$0, Ersatz_Flag(%rip)
	movq	$.L.str.29, Substitute_Picture_Filename(%rip)
	movl	$0, Two_Streams(%rip)
	movq	$.L.str.29, Enhancement_Layer_Bitstream_Filename(%rip)
	movl	$0, Big_Picture_Flag(%rip)
	movl	$0, Main_Bitstream_Flag(%rip)
	movq	$.L.str.29, Main_Bitstream_Filename(%rip)
	movl	$0, Verify_Flag(%rip)
	movl	$0, Stats_Flag(%rip)
	movl	$0, User_Data_Flag(%rip)
	cmpl	$1, %edi
	jle	.LBB0_156
# BB#1:                                 # %.lr.ph.preheader.i
	movl	$-1, Output_Type(%rip)
	movl	$1, %ebp
	movl	%edi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %r15d
	subl	%ebp, %r15d
	cmpl	$1, %r15d
	movl	$0, %r13d
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	8(%r14,%rax,8), %rax
	xorl	%r13d, %r13d
	cmpb	$45, (%rax)
	sete	%r13b
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %r12
	movq	(%r14,%r12,8), %rbx
	cmpb	$45, (%rbx)
	jne	.LBB0_42
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movsbq	1(%rbx), %rdx
	movl	(%rax,%rdx,4), %eax
	addl	$-66, %eax
	cmpl	$22, %eax
	ja	.LBB0_39
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r15d
	movl	$1, Main_Bitstream_Flag(%rip)
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_9
# BB#40:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	8(%r14,%r12,8), %rax
	movq	%rax, Main_Bitstream_Filename(%rip)
	jmp	.LBB0_41
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.8, %edi
	callq	puts
	jmp	.LBB0_41
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r15d
	movl	$1, Two_Streams(%rip)
	je	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_14
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	8(%r14,%r12,8), %rax
	movq	%rax, Enhancement_Layer_Bitstream_Filename(%rip)
	jmp	.LBB0_41
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, Frame_Store_Flag(%rip)
	jmp	.LBB0_41
.LBB0_18:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, Big_Picture_Flag(%rip)
	jmp	.LBB0_41
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.6, %edi
	callq	puts
	jmp	.LBB0_41
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r15d
	movl	$1, Spatial_Flag(%rip)
	je	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	8(%r14,%r12,8), %rax
	movq	%rax, Lower_Layer_Picture_Filename(%rip)
	jmp	.LBB0_41
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, Output_Type(%rip)
	orl	$1, %eax
	cmpl	$5, %eax
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_2 Depth=1
	movq	$.L.str.13, Output_Picture_Filename(%rip)
	jmp	.LBB0_41
.LBB0_30:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, Quiet_Flag(%rip)
	jmp	.LBB0_41
.LBB0_31:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, Reference_IDCT_Flag(%rip)
	jmp	.LBB0_41
.LBB0_32:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
	jmp	.LBB0_41
.LBB0_33:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, User_Data_Flag(%rip)
.LBB0_34:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	jmp	.LBB0_41
.LBB0_35:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r15d
	movl	$1, Ersatz_Flag(%rip)
	je	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_37
# BB#38:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	8(%r14,%r12,8), %rax
	movq	%rax, Substitute_Picture_Filename(%rip)
	jmp	.LBB0_41
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr.9, %edi
	callq	puts
	jmp	.LBB0_41
.LBB0_26:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r15d
	je	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_28
# BB#29:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	movq	8(%r14,%r12,8), %rax
	movq	%rax, Output_Picture_Filename(%rip)
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_2 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB0_42:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%edi, %ebp
	jl	.LBB0_2
# BB#43:                                # %._crit_edge.i
	cmpl	$1, Main_Bitstream_Flag(%rip)
	je	.LBB0_45
# BB#44:
	movl	$.Lstr, %edi
	callq	puts
.LBB0_45:
	movl	Output_Type(%rip), %eax
	movl	%eax, %ecx
	orl	$1, %ecx
	cmpl	$5, %ecx
	sete	%cl
	cmpl	$0, Frame_Store_Flag(%rip)
	setne	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	movl	%ecx, Display_Progressive_Flag(%rip)
	cmpl	$-1, %eax
	jne	.LBB0_47
# BB#46:
	movl	$9, Output_Type(%rip)
	movq	$.L.str.13, Output_Picture_Filename(%rip)
.LBB0_47:                               # %Process_Options.exit
	movq	$base, ld(%rip)
	movq	Main_Bitstream_Filename(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, base(%rip)
	testl	%eax, %eax
	js	.LBB0_157
# BB#48:
	je	.LBB0_57
# BB#49:
	callq	Initialize_Buffer
	movl	$8, %edi
	callq	Show_Bits
	cmpl	$71, %eax
	je	.LBB0_158
# BB#50:
	callq	next_start_code
	movl	$32, %edi
	callq	Show_Bits
	cmpl	$435, %eax              # imm = 0x1B3
	je	.LBB0_55
# BB#51:
	cmpl	$480, %eax              # imm = 0x1E0
	je	.LBB0_54
# BB#52:
	cmpl	$442, %eax              # imm = 0x1BA
	jne	.LBB0_159
# BB#53:
	movl	$1, System_Stream_Flag(%rip)
.LBB0_54:
	movl	$1, System_Stream_Flag(%rip)
.LBB0_55:
	movl	base(%rip), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	callq	Initialize_Buffer
	movl	base(%rip), %edi
	testl	%edi, %edi
	je	.LBB0_57
# BB#56:
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
.LBB0_57:                               # %.thread
	callq	Initialize_Buffer
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_60
# BB#58:
	movq	$enhan, ld(%rip)
	movq	Enhancement_Layer_Bitstream_Filename(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, enhan(%rip)
	testl	%eax, %eax
	js	.LBB0_160
# BB#59:
	callq	Initialize_Buffer
	movq	$base, ld(%rip)
.LBB0_60:
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, Clip(%rip)
	testq	%rax, %rax
	je	.LBB0_161
# BB#61:
	movq	%rax, %rcx
	addq	$384, %rcx              # imm = 0x180
	movq	%rcx, Clip(%rip)
	movw	$0, (%rax)
	movq	$-382, %rax             # imm = 0xFE82
	.p2align	4, 0x90
.LBB0_62:                               # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	Clip(%rip), %rdx
	cmpq	$255, %rax
	movl	%eax, %ebx
	jl	.LBB0_64
# BB#63:                                # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	$-1, %bl
.LBB0_64:                               # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$0, %esi
	js	.LBB0_66
# BB#65:                                # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	%bl, %sil
.LBB0_66:                               # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	%sil, (%rdx,%rax)
	leaq	1(%rax), %rsi
	movq	Clip(%rip), %rdx
	cmpq	$255, %rsi
	jl	.LBB0_68
# BB#67:                                # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	$-1, %sil
.LBB0_68:                               # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	cmpq	$-1, %rax
	jl	.LBB0_70
# BB#69:                                # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	%sil, %cl
.LBB0_70:                               # %._crit_edge.i5.._crit_edge.i5_crit_edge
                                        #   in Loop: Header=BB0_62 Depth=1
	movb	%cl, 1(%rdx,%rax)
	addq	$2, %rax
	cmpq	$640, %rax              # imm = 0x280
	jne	.LBB0_62
# BB#71:
	cmpl	$0, Reference_IDCT_Flag(%rip)
	je	.LBB0_73
# BB#72:
	callq	Initialize_Reference_IDCT
	jmp	.LBB0_74
.LBB0_73:
	callq	Initialize_Fast_IDCT
.LBB0_74:                               # %video_sequence.exit.i.preheader
	xorl	%r15d, %r15d
	jmp	.LBB0_75
	.p2align	4, 0x90
.LBB0_152:                              #   in Loop: Header=BB0_75 Depth=1
	movq	lltmp(%rip), %rdi
	callq	free
.LBB0_75:                               # %video_sequence.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_134 Depth 2
	movq	$base, ld(%rip)
	callq	Get_Hdr
	movl	%eax, %ebp
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_80
# BB#76:                                #   in Loop: Header=BB0_75 Depth=1
	movq	$enhan, ld(%rip)
	callq	Get_Hdr
	cmpl	%ebp, %eax
	je	.LBB0_79
# BB#77:                                #   in Loop: Header=BB0_75 Depth=1
	movl	Quiet_Flag(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_78
.LBB0_79:                               #   in Loop: Header=BB0_75 Depth=1
	movq	$base, ld(%rip)
.LBB0_80:                               # %Headers.exit.i
                                        #   in Loop: Header=BB0_75 Depth=1
	cmpl	$1, %ebp
	jne	.LBB0_153
# BB#81:                                #   in Loop: Header=BB0_75 Depth=1
	movl	base+3148(%rip), %edi
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_84
# BB#82:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$3, enhan+3148(%rip)
	je	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$1, %edi
	jne	.LBB0_162
.LBB0_84:                               #   in Loop: Header=BB0_75 Depth=1
	movl	base+3144(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_86
# BB#85:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movl	progressive_sequence(%rip), %ecx
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_86:                               #   in Loop: Header=BB0_75 Depth=1
	movl	$1, progressive_sequence(%rip)
	movl	$1, progressive_frame(%rip)
	movl	$3, picture_structure(%rip)
	movl	$1, frame_pred_frame_dct(%rip)
	movl	$1, chroma_format(%rip)
	movl	$5, matrix_coefficients(%rip)
	movl	$1, %ecx
.LBB0_87:                               #   in Loop: Header=BB0_75 Depth=1
	movl	horizontal_size(%rip), %edx
	leal	15(%rdx), %esi
	sarl	$31, %esi
	shrl	$28, %esi
	leal	15(%rdx,%rsi), %r13d
	sarl	$4, %r13d
	testl	%eax, %eax
	movl	%r13d, mb_width(%rip)
	movl	vertical_size(%rip), %eax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	je	.LBB0_90
# BB#88:                                #   in Loop: Header=BB0_75 Depth=1
	testl	%ecx, %ecx
	jne	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_75 Depth=1
	leal	31(%rax), %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	leal	31(%rax,%rcx), %eax
	sarl	$5, %eax
	addl	%eax, %eax
	jmp	.LBB0_91
	.p2align	4, 0x90
.LBB0_90:                               #   in Loop: Header=BB0_75 Depth=1
	leal	15(%rax), %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	leal	15(%rax,%rcx), %eax
	sarl	$4, %eax
.LBB0_91:                               #   in Loop: Header=BB0_75 Depth=1
	movl	%eax, mb_height(%rip)
	movl	%r13d, %edx
	shll	$4, %edx
	movl	%edx, Coded_Picture_Width(%rip)
	shll	$4, %eax
	movl	%eax, Coded_Picture_Height(%rip)
	movslq	chroma_format(%rip), %rsi
	cmpq	$1, %rsi
	sete	%cl
	shll	$3, %r13d
	cmpq	$3, %rsi
	cmovel	%edx, %r13d
	movl	%r13d, Chroma_Width(%rip)
	movl	%eax, %r12d
	sarl	%cl, %r12d
	movl	%r12d, Chroma_Height(%rip)
	movl	Initialize_Sequence.Table_6_20-4(,%rsi,4), %ecx
	movl	%ecx, block_count(%rip)
	movl	Ersatz_Flag(%rip), %ebx
	movl	lower_layer_prediction_horizontal_size(%rip), %ecx
	movl	lower_layer_prediction_vertical_size(%rip), %r14d
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	imull	%ecx, %r14d
	imull	%edx, %eax
	movslq	%eax, %rbp
	movq	%rbp, %rdi
	callq	malloc
	cmpl	$0, %ebx
	movq	%rax, backward_reference_frame(%rip)
	je	.LBB0_95
# BB#92:                                # %.split.preheader.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_102
# BB#93:                                #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#103:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe(%rip)
	testq	%rax, %rax
	je	.LBB0_132
# BB#104:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, substitute_frame(%rip)
	testq	%rax, %rax
	je	.LBB0_163
# BB#105:                               #   in Loop: Header=BB0_75 Depth=1
	cmpl	$2, %ebx
	jne	.LBB0_108
# BB#106:                               #   in Loop: Header=BB0_75 Depth=1
	movslq	%r14d, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe0(%rip)
	testq	%rax, %rax
	je	.LBB0_121
# BB#107:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe1(%rip)
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_143
.LBB0_108:                              # %.split.141.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	imull	%r13d, %r12d
	movslq	%r12d, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, backward_reference_frame+8(%rip)
	testq	%rax, %rax
	je	.LBB0_102
# BB#109:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame+8(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#110:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe+8(%rip)
	testq	%rax, %rax
	je	.LBB0_132
# BB#111:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, substitute_frame+8(%rip)
	testq	%rax, %rax
	je	.LBB0_163
# BB#112:                               #   in Loop: Header=BB0_75 Depth=1
	cmpl	$2, %ebx
	jne	.LBB0_115
# BB#113:                               #   in Loop: Header=BB0_75 Depth=1
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r14d, %eax
	sarl	$2, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe0+8(%rip)
	testq	%rax, %rax
	je	.LBB0_121
# BB#114:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe1+8(%rip)
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_143
.LBB0_115:                              # %.split.242.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, backward_reference_frame+16(%rip)
	testq	%rax, %rax
	je	.LBB0_102
# BB#116:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame+16(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#117:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe+16(%rip)
	testq	%rax, %rax
	je	.LBB0_132
# BB#118:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, substitute_frame+16(%rip)
	testq	%rax, %rax
	jne	.LBB0_119
	jmp	.LBB0_163
	.p2align	4, 0x90
.LBB0_95:                               # %.split.us.preheader.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_102
# BB#96:                                #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#97:                                #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe(%rip)
	testq	%rax, %rax
	je	.LBB0_132
# BB#98:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$2, %ebx
	jne	.LBB0_101
# BB#99:                                #   in Loop: Header=BB0_75 Depth=1
	movslq	%r14d, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe0(%rip)
	testq	%rax, %rax
	je	.LBB0_121
# BB#100:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe1(%rip)
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_143
.LBB0_101:                              # %.split.us.138.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	imull	%r13d, %r12d
	movslq	%r12d, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, backward_reference_frame+8(%rip)
	testq	%rax, %rax
	je	.LBB0_102
# BB#124:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame+8(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#125:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe+8(%rip)
	testq	%rax, %rax
	je	.LBB0_132
# BB#126:                               #   in Loop: Header=BB0_75 Depth=1
	cmpl	$2, %ebx
	jne	.LBB0_129
# BB#127:                               #   in Loop: Header=BB0_75 Depth=1
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r14d, %eax
	sarl	$2, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe0+8(%rip)
	testq	%rax, %rax
	je	.LBB0_121
# BB#128:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe1+8(%rip)
	testq	%rax, %rax
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_143
.LBB0_129:                              # %.split.us.239.i.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, backward_reference_frame+16(%rip)
	testq	%rax, %rax
	je	.LBB0_102
# BB#130:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, forward_reference_frame+16(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#131:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, auxframe+16(%rip)
	testq	%rax, %rax
	je	.LBB0_132
.LBB0_119:                              #   in Loop: Header=BB0_75 Depth=1
	cmpl	$2, %ebx
	jne	.LBB0_133
# BB#120:                               #   in Loop: Header=BB0_75 Depth=1
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r14d, %eax
	sarl	$2, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe0+16(%rip)
	testq	%rax, %rax
	je	.LBB0_121
# BB#142:                               #   in Loop: Header=BB0_75 Depth=1
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, llframe1+16(%rip)
	testq	%rax, %rax
	je	.LBB0_143
# BB#122:                               #   in Loop: Header=BB0_75 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	imull	vertical_subsampling_factor_n(%rip), %eax
	cltd
	idivl	vertical_subsampling_factor_m(%rip)
	imull	20(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %rdi
	addq	%rdi, %rdi
	callq	malloc
	movq	%rax, lltmp(%rip)
	testq	%rax, %rax
	je	.LBB0_123
.LBB0_133:                              # %Initialize_Sequence.exit.i.i
                                        #   in Loop: Header=BB0_75 Depth=1
	xorl	%esi, %esi
	movl	%r15d, %edi
	callq	Decode_Picture
	xorl	%ebp, %ebp
	cmpl	$0, Second_Field(%rip)
	sete	%bpl
	addl	%ebp, %r15d
	jmp	.LBB0_134
.LBB0_141:                              #   in Loop: Header=BB0_134 Depth=2
	incl	%r15d
	incl	%ebp
	.p2align	4, 0x90
.LBB0_134:                              #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$base, ld(%rip)
	callq	Get_Hdr
	movl	%eax, %ebx
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_139
# BB#135:                               #   in Loop: Header=BB0_134 Depth=2
	movq	$enhan, ld(%rip)
	callq	Get_Hdr
	cmpl	%ebx, %eax
	je	.LBB0_138
# BB#136:                               #   in Loop: Header=BB0_134 Depth=2
	movl	Quiet_Flag(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_137
.LBB0_138:                              #   in Loop: Header=BB0_134 Depth=2
	movq	$base, ld(%rip)
.LBB0_139:                              # %Headers.exit.i.i
                                        #   in Loop: Header=BB0_134 Depth=2
	testl	%ebx, %ebx
	je	.LBB0_144
# BB#140:                               #   in Loop: Header=BB0_134 Depth=2
	movl	%r15d, %edi
	movl	%ebp, %esi
	callq	Decode_Picture
	cmpl	$0, Second_Field(%rip)
	jne	.LBB0_134
	jmp	.LBB0_141
.LBB0_137:                              #   in Loop: Header=BB0_134 Depth=2
	movq	stderr(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_138
	.p2align	4, 0x90
.LBB0_144:                              #   in Loop: Header=BB0_75 Depth=1
	testl	%ebp, %ebp
	je	.LBB0_146
# BB#145:                               #   in Loop: Header=BB0_75 Depth=1
	movl	%r15d, %edi
	callq	Output_Last_Frame_of_Sequence
.LBB0_146:                              #   in Loop: Header=BB0_75 Depth=1
	movl	$0, base+3144(%rip)
	movq	backward_reference_frame(%rip), %rdi
	callq	free
	movq	forward_reference_frame(%rip), %rdi
	callq	free
	movq	auxframe(%rip), %rdi
	callq	free
	cmpl	$2, base+3148(%rip)
	jne	.LBB0_148
# BB#147:                               #   in Loop: Header=BB0_75 Depth=1
	movq	llframe0(%rip), %rdi
	callq	free
	movq	llframe1(%rip), %rdi
	callq	free
.LBB0_148:                              #   in Loop: Header=BB0_75 Depth=1
	movq	backward_reference_frame+8(%rip), %rdi
	callq	free
	movq	forward_reference_frame+8(%rip), %rdi
	callq	free
	movq	auxframe+8(%rip), %rdi
	callq	free
	cmpl	$2, base+3148(%rip)
	jne	.LBB0_150
# BB#149:                               #   in Loop: Header=BB0_75 Depth=1
	movq	llframe0+8(%rip), %rdi
	callq	free
	movq	llframe1+8(%rip), %rdi
	callq	free
.LBB0_150:                              #   in Loop: Header=BB0_75 Depth=1
	movq	backward_reference_frame+16(%rip), %rdi
	callq	free
	movq	forward_reference_frame+16(%rip), %rdi
	callq	free
	movq	auxframe+16(%rip), %rdi
	callq	free
	cmpl	$2, base+3148(%rip)
	jne	.LBB0_75
# BB#151:                               #   in Loop: Header=BB0_75 Depth=1
	movq	llframe0+16(%rip), %rdi
	callq	free
	movq	llframe1+16(%rip), %rdi
	callq	free
	cmpl	$2, base+3148(%rip)
	jne	.LBB0_75
	jmp	.LBB0_152
.LBB0_78:                               #   in Loop: Header=BB0_75 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_79
.LBB0_153:                              # %Decode_Bitstream.exit
	movl	base(%rip), %edi
	callq	close
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_155
# BB#154:
	movl	enhan(%rip), %edi
	callq	close
.LBB0_155:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_102:                              # %.us-lcssa.us.i.i.i
	movl	$.L.str.22, %edi
	callq	Error
.LBB0_94:                               # %.us-lcssa26.us.i.i.i
	movl	$.L.str.23, %edi
	callq	Error
.LBB0_132:                              # %.us-lcssa27.us.i.i.i
	movl	$.L.str.24, %edi
	callq	Error
.LBB0_163:                              # %.us-lcssa28.i.i.i
	movl	$.L.str.25, %edi
	callq	Error
.LBB0_121:                              # %.us-lcssa29.us.i.i.i
	movl	$.L.str.26, %edi
	callq	Error
.LBB0_143:                              # %.us-lcssa30.us.i.i.i
	movl	$.L.str.27, %edi
	callq	Error
.LBB0_162:
	movl	$.L.str.21, %edi
	callq	Error
.LBB0_39:
	movq	stderr(%rip), %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.LBB0_123:
	movl	$.L.str.28, %edi
	callq	Error
.LBB0_37:
	movl	$.Lstr.1, %edi
	jmp	.LBB0_15
.LBB0_14:
	movl	$.Lstr.7, %edi
	jmp	.LBB0_15
.LBB0_22:
	movl	$.Lstr.5, %edi
	jmp	.LBB0_15
.LBB0_156:
	movl	$.L.str.6, %edi
	movl	$Version, %esi
	movl	$Author, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB0_157:
	movq	stderr(%rip), %rdi
	movq	Main_Bitstream_Filename(%rip), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_161:
	movl	$.L.str.5, %edi
	callq	Error
.LBB0_28:
	movl	$.Lstr.4, %edi
.LBB0_15:
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB0_158:
	movups	.L.str.1+32(%rip), %xmm0
	movaps	%xmm0, Error_Text+32(%rip)
	movups	.L.str.1+16(%rip), %xmm0
	movaps	%xmm0, Error_Text+16(%rip)
	movups	.L.str.1(%rip), %xmm0
	movaps	%xmm0, Error_Text(%rip)
	movl	$684909, Error_Text+48(%rip) # imm = 0xA736D
	movl	$Error_Text, %edi
	callq	Error
.LBB0_159:
	movups	.L.str.2+16(%rip), %xmm0
	movaps	%xmm0, Error_Text+16(%rip)
	movups	.L.str.2(%rip), %xmm0
	movaps	%xmm0, Error_Text(%rip)
	movb	$0, Error_Text+32(%rip)
	movl	$Error_Text, %edi
	callq	Error
.LBB0_160:
	movq	Enhancement_Layer_Bitstream_Filename(%rip), %rdx
	movl	$Error_Text, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$Error_Text, %edi
	callq	Error
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_7
	.quad	.LBB0_11
	.quad	.LBB0_39
	.quad	.LBB0_12
	.quad	.LBB0_17
	.quad	.LBB0_18
	.quad	.LBB0_39
	.quad	.LBB0_19
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_20
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_24
	.quad	.LBB0_39
	.quad	.LBB0_30
	.quad	.LBB0_31
	.quad	.LBB0_39
	.quad	.LBB0_32
	.quad	.LBB0_33
	.quad	.LBB0_34
	.quad	.LBB0_39
	.quad	.LBB0_35

	.text
	.globl	Error
	.p2align	4, 0x90
	.type	Error,@function
Error:                                  # @Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	Error, .Lfunc_end1-Error
	.cfi_endproc

	.globl	Print_Bits
	.p2align	4, 0x90
	.type	Print_Bits,@function
Print_Bits:                             # @Print_Bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movl	%edi, %r14d
	testl	%ebx, %ebx
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph
	decl	%ebp
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movl	%r14d, %esi
	movl	%ebp, %ecx
	shrl	%cl, %esi
	andl	$1, %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	decl	%ebp
	decl	%ebx
	jne	.LBB2_2
.LBB2_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Print_Bits, .Lfunc_end2-Print_Bits
	.cfi_endproc

	.type	Version,@object         # @Version
	.data
	.globl	Version
	.p2align	4
Version:
	.asciz	"mpeg2decode V1.2a, 96/07/19"
	.size	Version, 28

	.type	Author,@object          # @Author
	.globl	Author
	.p2align	4
Author:
	.asciz	"(C) 1996, MPEG Software Simulation Group"
	.size	Author, 41

	.type	scan,@object            # @scan
	.globl	scan
	.p2align	4
scan:
	.ascii	"\000\001\b\020\t\002\003\n\021\030 \031\022\013\004\005\f\023\032!(0)\"\033\024\r\006\007\016\025\034#*1892+$\035\026\017\027\036%,3:;4-&\037'.5<=6/7>?"
	.ascii	"\000\b\020\030\001\t\002\n\021\031 (0891)!\032\022\003\013\004\f\023\033\"*2:#+3;\024\034\005\r\006\016\025\035$,4<%-5=\026\036\007\017\027\037&.6>'/7?"
	.size	scan, 128

	.type	default_intra_quantizer_matrix,@object # @default_intra_quantizer_matrix
	.globl	default_intra_quantizer_matrix
	.p2align	4
default_intra_quantizer_matrix:
	.ascii	"\b\020\023\026\032\033\035\"\020\020\026\030\033\035\"%\023\026\032\033\035\"\"&\026\026\032\033\035\"%(\026\032\033\035 #(0\032\033\035 #(0:\032\033\035\"&.8E\033\035#&.8ES"
	.size	default_intra_quantizer_matrix, 64

	.type	Non_Linear_quantizer_scale,@object # @Non_Linear_quantizer_scale
	.globl	Non_Linear_quantizer_scale
	.p2align	4
Non_Linear_quantizer_scale:
	.ascii	"\000\001\002\003\004\005\006\007\b\n\f\016\020\022\024\026\030\034 $(,048@HPX`hp"
	.size	Non_Linear_quantizer_scale, 32

	.type	Inverse_Table_6_9,@object # @Inverse_Table_6_9
	.globl	Inverse_Table_6_9
	.p2align	4
Inverse_Table_6_9:
	.long	117504                  # 0x1cb00
	.long	138453                  # 0x21cd5
	.long	13954                   # 0x3682
	.long	34903                   # 0x8857
	.long	117504                  # 0x1cb00
	.long	138453                  # 0x21cd5
	.long	13954                   # 0x3682
	.long	34903                   # 0x8857
	.long	104597                  # 0x19895
	.long	132201                  # 0x20469
	.long	25675                   # 0x644b
	.long	53279                   # 0xd01f
	.long	104597                  # 0x19895
	.long	132201                  # 0x20469
	.long	25675                   # 0x644b
	.long	53279                   # 0xd01f
	.long	104448                  # 0x19800
	.long	132798                  # 0x206be
	.long	24759                   # 0x60b7
	.long	53109                   # 0xcf75
	.long	104597                  # 0x19895
	.long	132201                  # 0x20469
	.long	25675                   # 0x644b
	.long	53279                   # 0xd01f
	.long	104597                  # 0x19895
	.long	132201                  # 0x20469
	.long	25675                   # 0x644b
	.long	53279                   # 0xd01f
	.long	117579                  # 0x1cb4b
	.long	136230                  # 0x21426
	.long	16907                   # 0x420b
	.long	35559                   # 0x8ae7
	.size	Inverse_Table_6_9, 128

	.type	base,@object            # @base
	.comm	base,4712,8
	.type	ld,@object              # @ld
	.comm	ld,8,8
	.type	Main_Bitstream_Filename,@object # @Main_Bitstream_Filename
	.comm	Main_Bitstream_Filename,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Base layer input file %s not found\n"
	.size	.L.str, 36

	.type	Error_Text,@object      # @Error_Text
	.comm	Error_Text,256,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Decoder currently does not parse transport streams\n"
	.size	.L.str.1, 52

	.type	System_Stream_Flag,@object # @System_Stream_Flag
	.comm	System_Stream_Flag,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Unable to recognize stream type\n"
	.size	.L.str.2, 33

	.type	Two_Streams,@object     # @Two_Streams
	.comm	Two_Streams,4,4
	.type	enhan,@object           # @enhan
	.comm	enhan,4712,8
	.type	Enhancement_Layer_Bitstream_Filename,@object # @Enhancement_Layer_Bitstream_Filename
	.comm	Enhancement_Layer_Bitstream_Filename,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"enhancment layer bitstream file %s not found\n"
	.size	.L.str.3, 46

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d"
	.size	.L.str.4, 3

	.type	Output_Type,@object     # @Output_Type
	.comm	Output_Type,4,4
	.type	hiQdither,@object       # @hiQdither
	.comm	hiQdither,4,4
	.type	Quiet_Flag,@object      # @Quiet_Flag
	.comm	Quiet_Flag,4,4
	.type	Trace_Flag,@object      # @Trace_Flag
	.comm	Trace_Flag,4,4
	.type	Fault_Flag,@object      # @Fault_Flag
	.comm	Fault_Flag,4,4
	.type	Verbose_Flag,@object    # @Verbose_Flag
	.comm	Verbose_Flag,4,4
	.type	Spatial_Flag,@object    # @Spatial_Flag
	.comm	Spatial_Flag,4,4
	.type	Reference_IDCT_Flag,@object # @Reference_IDCT_Flag
	.comm	Reference_IDCT_Flag,4,4
	.type	Frame_Store_Flag,@object # @Frame_Store_Flag
	.comm	Frame_Store_Flag,4,4
	.type	Display_Progressive_Flag,@object # @Display_Progressive_Flag
	.comm	Display_Progressive_Flag,4,4
	.type	Ersatz_Flag,@object     # @Ersatz_Flag
	.comm	Ersatz_Flag,4,4
	.type	Big_Picture_Flag,@object # @Big_Picture_Flag
	.comm	Big_Picture_Flag,4,4
	.type	Verify_Flag,@object     # @Verify_Flag
	.comm	Verify_Flag,4,4
	.type	Stats_Flag,@object      # @Stats_Flag
	.comm	Stats_Flag,4,4
	.type	User_Data_Flag,@object  # @User_Data_Flag
	.comm	User_Data_Flag,4,4
	.type	Main_Bitstream_Flag,@object # @Main_Bitstream_Flag
	.comm	Main_Bitstream_Flag,4,4
	.type	Output_Picture_Filename,@object # @Output_Picture_Filename
	.comm	Output_Picture_Filename,8,8
	.type	Substitute_Picture_Filename,@object # @Substitute_Picture_Filename
	.comm	Substitute_Picture_Filename,8,8
	.type	Clip,@object            # @Clip
	.comm	Clip,8,8
	.type	backward_reference_frame,@object # @backward_reference_frame
	.comm	backward_reference_frame,24,16
	.type	forward_reference_frame,@object # @forward_reference_frame
	.comm	forward_reference_frame,24,16
	.type	auxframe,@object        # @auxframe
	.comm	auxframe,24,16
	.type	current_frame,@object   # @current_frame
	.comm	current_frame,24,16
	.type	substitute_frame,@object # @substitute_frame
	.comm	substitute_frame,24,16
	.type	llframe0,@object        # @llframe0
	.comm	llframe0,24,16
	.type	llframe1,@object        # @llframe1
	.comm	llframe1,24,16
	.type	lltmp,@object           # @lltmp
	.comm	lltmp,8,8
	.type	Lower_Layer_Picture_Filename,@object # @Lower_Layer_Picture_Filename
	.comm	Lower_Layer_Picture_Filename,8,8
	.type	Coded_Picture_Width,@object # @Coded_Picture_Width
	.comm	Coded_Picture_Width,4,4
	.type	Coded_Picture_Height,@object # @Coded_Picture_Height
	.comm	Coded_Picture_Height,4,4
	.type	Chroma_Width,@object    # @Chroma_Width
	.comm	Chroma_Width,4,4
	.type	Chroma_Height,@object   # @Chroma_Height
	.comm	Chroma_Height,4,4
	.type	block_count,@object     # @block_count
	.comm	block_count,4,4
	.type	Second_Field,@object    # @Second_Field
	.comm	Second_Field,4,4
	.type	profile,@object         # @profile
	.comm	profile,4,4
	.type	level,@object           # @level
	.comm	level,4,4
	.type	horizontal_size,@object # @horizontal_size
	.comm	horizontal_size,4,4
	.type	vertical_size,@object   # @vertical_size
	.comm	vertical_size,4,4
	.type	mb_width,@object        # @mb_width
	.comm	mb_width,4,4
	.type	mb_height,@object       # @mb_height
	.comm	mb_height,4,4
	.type	bit_rate,@object        # @bit_rate
	.comm	bit_rate,8,8
	.type	frame_rate,@object      # @frame_rate
	.comm	frame_rate,8,8
	.type	aspect_ratio_information,@object # @aspect_ratio_information
	.comm	aspect_ratio_information,4,4
	.type	frame_rate_code,@object # @frame_rate_code
	.comm	frame_rate_code,4,4
	.type	bit_rate_value,@object  # @bit_rate_value
	.comm	bit_rate_value,4,4
	.type	vbv_buffer_size,@object # @vbv_buffer_size
	.comm	vbv_buffer_size,4,4
	.type	constrained_parameters_flag,@object # @constrained_parameters_flag
	.comm	constrained_parameters_flag,4,4
	.type	profile_and_level_indication,@object # @profile_and_level_indication
	.comm	profile_and_level_indication,4,4
	.type	progressive_sequence,@object # @progressive_sequence
	.comm	progressive_sequence,4,4
	.type	chroma_format,@object   # @chroma_format
	.comm	chroma_format,4,4
	.type	low_delay,@object       # @low_delay
	.comm	low_delay,4,4
	.type	frame_rate_extension_n,@object # @frame_rate_extension_n
	.comm	frame_rate_extension_n,4,4
	.type	frame_rate_extension_d,@object # @frame_rate_extension_d
	.comm	frame_rate_extension_d,4,4
	.type	video_format,@object    # @video_format
	.comm	video_format,4,4
	.type	color_description,@object # @color_description
	.comm	color_description,4,4
	.type	color_primaries,@object # @color_primaries
	.comm	color_primaries,4,4
	.type	transfer_characteristics,@object # @transfer_characteristics
	.comm	transfer_characteristics,4,4
	.type	matrix_coefficients,@object # @matrix_coefficients
	.comm	matrix_coefficients,4,4
	.type	display_horizontal_size,@object # @display_horizontal_size
	.comm	display_horizontal_size,4,4
	.type	display_vertical_size,@object # @display_vertical_size
	.comm	display_vertical_size,4,4
	.type	temporal_reference,@object # @temporal_reference
	.comm	temporal_reference,4,4
	.type	picture_coding_type,@object # @picture_coding_type
	.comm	picture_coding_type,4,4
	.type	vbv_delay,@object       # @vbv_delay
	.comm	vbv_delay,4,4
	.type	full_pel_forward_vector,@object # @full_pel_forward_vector
	.comm	full_pel_forward_vector,4,4
	.type	forward_f_code,@object  # @forward_f_code
	.comm	forward_f_code,4,4
	.type	full_pel_backward_vector,@object # @full_pel_backward_vector
	.comm	full_pel_backward_vector,4,4
	.type	backward_f_code,@object # @backward_f_code
	.comm	backward_f_code,4,4
	.type	f_code,@object          # @f_code
	.comm	f_code,16,16
	.type	intra_dc_precision,@object # @intra_dc_precision
	.comm	intra_dc_precision,4,4
	.type	picture_structure,@object # @picture_structure
	.comm	picture_structure,4,4
	.type	top_field_first,@object # @top_field_first
	.comm	top_field_first,4,4
	.type	frame_pred_frame_dct,@object # @frame_pred_frame_dct
	.comm	frame_pred_frame_dct,4,4
	.type	concealment_motion_vectors,@object # @concealment_motion_vectors
	.comm	concealment_motion_vectors,4,4
	.type	intra_vlc_format,@object # @intra_vlc_format
	.comm	intra_vlc_format,4,4
	.type	repeat_first_field,@object # @repeat_first_field
	.comm	repeat_first_field,4,4
	.type	chroma_420_type,@object # @chroma_420_type
	.comm	chroma_420_type,4,4
	.type	progressive_frame,@object # @progressive_frame
	.comm	progressive_frame,4,4
	.type	composite_display_flag,@object # @composite_display_flag
	.comm	composite_display_flag,4,4
	.type	v_axis,@object          # @v_axis
	.comm	v_axis,4,4
	.type	field_sequence,@object  # @field_sequence
	.comm	field_sequence,4,4
	.type	sub_carrier,@object     # @sub_carrier
	.comm	sub_carrier,4,4
	.type	burst_amplitude,@object # @burst_amplitude
	.comm	burst_amplitude,4,4
	.type	sub_carrier_phase,@object # @sub_carrier_phase
	.comm	sub_carrier_phase,4,4
	.type	frame_center_horizontal_offset,@object # @frame_center_horizontal_offset
	.comm	frame_center_horizontal_offset,12,4
	.type	frame_center_vertical_offset,@object # @frame_center_vertical_offset
	.comm	frame_center_vertical_offset,12,4
	.type	layer_id,@object        # @layer_id
	.comm	layer_id,4,4
	.type	lower_layer_prediction_horizontal_size,@object # @lower_layer_prediction_horizontal_size
	.comm	lower_layer_prediction_horizontal_size,4,4
	.type	lower_layer_prediction_vertical_size,@object # @lower_layer_prediction_vertical_size
	.comm	lower_layer_prediction_vertical_size,4,4
	.type	horizontal_subsampling_factor_m,@object # @horizontal_subsampling_factor_m
	.comm	horizontal_subsampling_factor_m,4,4
	.type	horizontal_subsampling_factor_n,@object # @horizontal_subsampling_factor_n
	.comm	horizontal_subsampling_factor_n,4,4
	.type	vertical_subsampling_factor_m,@object # @vertical_subsampling_factor_m
	.comm	vertical_subsampling_factor_m,4,4
	.type	vertical_subsampling_factor_n,@object # @vertical_subsampling_factor_n
	.comm	vertical_subsampling_factor_n,4,4
	.type	lower_layer_temporal_reference,@object # @lower_layer_temporal_reference
	.comm	lower_layer_temporal_reference,4,4
	.type	lower_layer_horizontal_offset,@object # @lower_layer_horizontal_offset
	.comm	lower_layer_horizontal_offset,4,4
	.type	lower_layer_vertical_offset,@object # @lower_layer_vertical_offset
	.comm	lower_layer_vertical_offset,4,4
	.type	spatial_temporal_weight_code_table_index,@object # @spatial_temporal_weight_code_table_index
	.comm	spatial_temporal_weight_code_table_index,4,4
	.type	lower_layer_progressive_frame,@object # @lower_layer_progressive_frame
	.comm	lower_layer_progressive_frame,4,4
	.type	lower_layer_deinterlaced_field_select,@object # @lower_layer_deinterlaced_field_select
	.comm	lower_layer_deinterlaced_field_select,4,4
	.type	copyright_flag,@object  # @copyright_flag
	.comm	copyright_flag,4,4
	.type	copyright_identifier,@object # @copyright_identifier
	.comm	copyright_identifier,4,4
	.type	original_or_copy,@object # @original_or_copy
	.comm	original_or_copy,4,4
	.type	copyright_number_1,@object # @copyright_number_1
	.comm	copyright_number_1,4,4
	.type	copyright_number_2,@object # @copyright_number_2
	.comm	copyright_number_2,4,4
	.type	copyright_number_3,@object # @copyright_number_3
	.comm	copyright_number_3,4,4
	.type	drop_flag,@object       # @drop_flag
	.comm	drop_flag,4,4
	.type	hour,@object            # @hour
	.comm	hour,4,4
	.type	minute,@object          # @minute
	.comm	minute,4,4
	.type	sec,@object             # @sec
	.comm	sec,4,4
	.type	frame,@object           # @frame
	.comm	frame,4,4
	.type	closed_gop,@object      # @closed_gop
	.comm	closed_gop,4,4
	.type	broken_link,@object     # @broken_link
	.comm	broken_link,4,4
	.type	Decode_Layer,@object    # @Decode_Layer
	.comm	Decode_Layer,4,4
	.type	global_MBA,@object      # @global_MBA
	.comm	global_MBA,4,4
	.type	global_pic,@object      # @global_pic
	.comm	global_pic,4,4
	.type	True_Framenum,@object   # @True_Framenum
	.comm	True_Framenum,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Clip[] malloc failed\n"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n%s, %s\n"
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Usage:  mpeg2decode {options}\nOptions: -b  file  main bitstream (base or spatial enhancement layer)\n         -cn file  conformance report (n: level)\n         -e  file  enhancement layer bitstream (SNR or Data Partitioning)\n         -f        store/display interlaced video in frame format\n         -g        concatenated file format for substitution method (-x)\n         -in file  information & statistics report  (n: level)\n         -l  file  file name pattern for lower layer sequence\n                   (for spatial scalability)\n         -on file  output format (0:YUV 1:SIF 2:TGA 3:PPM 4:X11 5:X11HiQ)\n         -q        disable warnings to stderr\n         -r        use double precision reference IDCT\n         -t        enable low level tracing to stdout\n         -u  file  print user_data to stdio or file\n         -vn       verbose output (n: level)\n         -x  file  filename pattern of picture substitution sequence\n\nFile patterns:  for sequential filenames, \"printf\" style, e.g. rec%%d\n                 or rec%%d%%c for fieldwise storage\nLevels:        0:none 1:sequence 2:picture 3:slice 4:macroblock 5:block\n\nExample:       mpeg2decode -b bitstream.mpg -f -r -o0 rec%%d\n         \n"
	.size	.L.str.7, 1195

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.zero	1
	.size	.L.str.13, 1

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"undefined option -%c ignored. Exiting program\n"
	.size	.L.str.18, 47

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"streams out of sync\n"
	.size	.L.str.20, 21

	.type	Initialize_Sequence.Table_6_20,@object # @Initialize_Sequence.Table_6_20
	.section	.rodata,"a",@progbits
	.p2align	2
Initialize_Sequence.Table_6_20:
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	12                      # 0xc
	.size	Initialize_Sequence.Table_6_20, 12

	.type	.L.str.21,@object       # @.str.21
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.21:
	.asciz	"unsupported scalability mode\n"
	.size	.L.str.21, 30

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"backward_reference_frame[] malloc failed\n"
	.size	.L.str.22, 42

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"forward_reference_frame[] malloc failed\n"
	.size	.L.str.23, 41

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"auxframe[] malloc failed\n"
	.size	.L.str.24, 26

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"substitute_frame[] malloc failed\n"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"llframe0 malloc failed\n"
	.size	.L.str.26, 24

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"llframe1 malloc failed\n"
	.size	.L.str.27, 24

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"lltmp malloc failed\n"
	.size	.L.str.28, 21

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	" "
	.size	.L.str.29, 2

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"There must be a main bitstream specified (-b filename)"
	.size	.Lstr, 55

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"ERROR: -x must be followed by filename"
	.size	.Lstr.1, 39

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"This program not compiled for -v option"
	.size	.Lstr.2, 40

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"WARNING: This program not compiled for -t option"
	.size	.Lstr.3, 49

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"ERROR: -o must be followed by filename"
	.size	.Lstr.4, 39

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"ERROR: -l must be followed by filename"
	.size	.Lstr.5, 39

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"WARNING: This program not compiled for -i option"
	.size	.Lstr.6, 49

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"ERROR: -e must be followed by filename"
	.size	.Lstr.7, 39

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"This program not compiled for Verify_Flag option"
	.size	.Lstr.8, 49

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"ERROR: -b must be followed the main bitstream filename"
	.size	.Lstr.9, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
