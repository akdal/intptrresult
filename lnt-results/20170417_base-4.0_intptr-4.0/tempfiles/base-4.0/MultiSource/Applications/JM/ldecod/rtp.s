	.text
	.file	"rtp.bc"
	.globl	OpenRTPFile
	.p2align	4, 0x90
	.type	OpenRTPFile,@function
OpenRTPFile:                            # @OpenRTPFile
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %esi
	callq	fopen64
	movq	%rax, bits(%rip)
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	popq	%rax
	retq
.LBB0_2:
	movq	input(%rip), %rcx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end0:
	.size	OpenRTPFile, .Lfunc_end0-OpenRTPFile
	.cfi_endproc

	.globl	CloseRTPFile
	.p2align	4, 0x90
	.type	CloseRTPFile,@function
CloseRTPFile:                           # @CloseRTPFile
	.cfi_startproc
# BB#0:
	movq	bits(%rip), %rdi
	jmp	fclose                  # TAILCALL
.Lfunc_end1:
	.size	CloseRTPFile, .Lfunc_end1-CloseRTPFile
	.cfi_endproc

	.globl	GetRTPNALU
	.p2align	4, 0x90
	.type	GetRTPNALU,@function
GetRTPNALU:                             # @GetRTPNALU
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB2_2:
	movl	$65508, %edi            # imm = 0xFFE4
	callq	malloc
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	jne	.LBB2_4
# BB#3:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB2_4:
	movl	$65508, %edi            # imm = 0xFFE4
	callq	malloc
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	jne	.LBB2_6
# BB#5:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB2_6:
	movq	bits(%rip), %rsi
	movq	%rbx, %rdi
	callq	RTPReadPacket
	movl	$1, 20(%r14)
	movl	$0, 4(%r14)
	testl	%eax, %eax
	js	.LBB2_7
# BB#8:
	testl	%eax, %eax
	je	.LBB2_9
# BB#10:
	movl	48(%rbx), %edx
	movl	%edx, 4(%r14)
	movq	24(%r14), %rdi
	movq	40(%rbx), %r15
	movq	%r15, %rsi
	callq	memcpy
	movq	24(%r14), %rax
	movzbl	(%rax), %ecx
	shrl	$7, %ecx
	movl	%ecx, 20(%r14)
	movzbl	(%rax), %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	movl	%ecx, 16(%r14)
	movzbl	(%rax), %eax
	andl	$31, %eax
	movl	%eax, 12(%r14)
	movq	%r15, %rdi
	callq	free
	movq	56(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	4(%r14), %eax
	jmp	.LBB2_11
.LBB2_7:
	movl	$-1, %eax
	jmp	.LBB2_11
.LBB2_9:
	xorl	%eax, %eax
.LBB2_11:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	GetRTPNALU, .Lfunc_end2-GetRTPNALU
	.cfi_endproc

	.globl	RTPReadPacket
	.p2align	4, 0x90
	.type	RTPReadPacket,@function
RTPReadPacket:                          # @RTPReadPacket
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	callq	ftell
	movq	%rax, %r12
	leaq	64(%r15), %r14
	movl	$1, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	fread
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	$4, %rcx
	jne	.LBB3_5
# BB#1:
	leaq	4(%rsp), %rdi
	movl	$1, %esi
	movl	$4, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$4, %rax
	jne	.LBB3_6
# BB#2:
	movl	64(%r15), %r12d
	movq	56(%r15), %rdi
	movl	$1, %esi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	fread
	cmpq	%rax, %r12
	jne	.LBB3_7
# BB#3:
	movq	%r15, %rdi
	callq	DecomposeRTPpacket
	testl	%eax, %eax
	js	.LBB3_8
# BB#4:
	movl	(%r14), %eax
.LBB3_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_6:
	movslq	%r12d, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB3_7:
	movl	(%r14), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.LBB3_8:
	movl	$.Lstr, %edi
	callq	puts
	movl	$-700, %edi             # imm = 0xFD44
	callq	exit
.Lfunc_end3:
	.size	RTPReadPacket, .Lfunc_end3-RTPReadPacket
	.cfi_endproc

	.globl	DecomposeRTPpacket
	.p2align	4, 0x90
	.type	DecomposeRTPpacket,@function
DecomposeRTPpacket:                     # @DecomposeRTPpacket
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %rsi
	movzbl	(%rsi), %eax
	shrl	$6, %eax
	movl	%eax, (%rbx)
	movzbl	(%rsi), %edx
	shrl	$5, %edx
	movl	%edx, %ecx
	andl	$1, %ecx
	movl	%ecx, 4(%rbx)
	movzbl	(%rsi), %edi
	shrl	$4, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	movl	%ecx, 8(%rbx)
	movzbl	(%rsi), %r8d
	movl	%r8d, %ecx
	andl	$15, %ecx
	movl	%ecx, 12(%rbx)
	movzbl	1(%rsi), %ecx
	shrl	$7, %ecx
	movl	%ecx, 16(%rbx)
	movzbl	1(%rsi), %ecx
	andl	$127, %ecx
	movl	%ecx, 20(%rbx)
	movzwl	2(%rsi), %ecx
	movw	%cx, 24(%rbx)
	rolw	$8, %cx
	movzwl	%cx, %ecx
	movl	%ecx, 24(%rbx)
	movl	4(%rsi), %ecx
	bswapl	%ecx
	movl	%ecx, 32(%rbx)
	movl	8(%rsi), %ecx
	bswapl	%ecx
	movl	%ecx, 36(%rbx)
	orl	%edx, %edi
	testb	$1, %dil
	jne	.LBB4_3
# BB#1:
	cmpl	$2, %eax
	jne	.LBB4_3
# BB#2:
	andb	$15, %r8b
	jne	.LBB4_3
# BB#4:
	movl	64(%rbx), %edx
	addl	$-12, %edx
	movl	%edx, 48(%rbx)
	movq	40(%rbx), %rdi
	addq	$12, %rsi
	callq	memcpy
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB4_3:
	movl	$.Lstr.2, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	DumpRTPHeader
	movl	$-1, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	DecomposeRTPpacket, .Lfunc_end4-DecomposeRTPpacket
	.cfi_endproc

	.globl	DumpRTPHeader
	.p2align	4, 0x90
	.type	DumpRTPHeader,@function
DumpRTPHeader:                          # @DumpRTPHeader
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movzbl	(%rax,%rbx), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	$30, %rbx
	jne	.LBB5_1
# BB#2:
	movl	(%r14), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%r14), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%r14), %esi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%r14), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	16(%r14), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	20(%r14), %esi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	movl	24(%r14), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movl	32(%r14), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	36(%r14), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.Lfunc_end5:
	.size	DumpRTPHeader, .Lfunc_end5-DumpRTPHeader
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rb"
	.size	.L.str, 3

	.type	bits,@object            # @bits
	.comm	bits,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Cannot open RTP file '%s'"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"GetRTPNALU-1"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"GetRTPNALU-2"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"GetRTPNALU-3"
	.size	.L.str.4, 13

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%02x "
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Version (V): %d\n"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Padding (P): %d\n"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Extension (X): %d\n"
	.size	.L.str.9, 19

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"CSRC count (CC): %d\n"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Marker bit (M): %d\n"
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Payload Type (PT): %d\n"
	.size	.L.str.12, 23

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Sequence Number: %d\n"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Timestamp: %d\n"
	.size	.L.str.14, 15

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SSRC: %d\n"
	.size	.L.str.15, 10

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"RTPReadPacket: File corruption, could not read %d bytes\n"
	.size	.L.str.17, 57

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Errors reported by DecomposePacket(), exit"
	.size	.Lstr, 43

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"RTPReadPacket: File corruption, could not read Timestamp, exit"
	.size	.Lstr.1, 63

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"DecomposeRTPpacket, RTP header consistency problem, header follows"
	.size	.Lstr.2, 67


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
