	.text
	.file	"Falign.bc"
	.globl	Fgetlag
	.p2align	4, 0x90
	.type	Fgetlag,@function
Fgetlag:                                # @Fgetlag
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	144(%rsp), %ebx
	movl	$0, 52(%rsp)
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	(%r14), %rdi
	callq	strlen
	cmpl	%eax, %ebp
	movq	%rax, 80(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	cmovgel	%ebp, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r12d
	leal	(%r12,%r12), %ecx
	cmpl	%r12d, %eax
	jge	.LBB0_1
# BB#2:
	movl	Fgetlag.localalloclen(%rip), %eax
	testl	%eax, %eax
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	jne	.LBB0_13
# BB#3:
	movl	$20, %edi
	callq	AllocateIntVec
	movq	%rax, Fgetlag.kouho(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Fgetlag.cut1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Fgetlag.cut2(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpptr1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpptr2(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.result1(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.result2(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpres1(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpres2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, Fgetlag.segment(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r15
	movq	%rax, Fgetlag.segment1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, Fgetlag.segment2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, Fgetlag.sortedseg1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, Fgetlag.sortedseg2(%rip)
	testq	%rax, %rax
	je	.LBB0_8
# BB#4:
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_8
# BB#5:
	testq	%r15, %r15
	je	.LBB0_8
# BB#6:
	testq	%rbx, %rbx
	je	.LBB0_8
# BB#7:
	testq	%rbp, %rbp
	jne	.LBB0_9
.LBB0_8:
	movl	$.L.str, %edi
	callq	ErrorExit
.LBB0_9:
	cmpl	$-1, scoremtx(%rip)
	je	.LBB0_10
# BB#11:
	cmpl	$1, fftscore(%rip)
	movl	$2, %ecx
	movl	$20, %eax
	cmovel	%ecx, %eax
	jmp	.LBB0_12
.LBB0_10:
	movl	$4, %eax
.LBB0_12:
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	%eax, n20or4or2(%rip)
	movl	Fgetlag.localalloclen(%rip), %eax
.LBB0_13:
	cmpl	%r12d, %eax
	jge	.LBB0_17
# BB#14:
	testl	%eax, %eax
	je	.LBB0_16
# BB#15:
	movq	Fgetlag.seqVector1(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Fgetlag.seqVector2(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Fgetlag.naisekiNoWa(%rip), %rdi
	callq	FreeFukusosuuVec
	movq	Fgetlag.naiseki(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Fgetlag.soukan(%rip), %rdi
	callq	FreeDoubleVec
	movq	Fgetlag.tmpseq1(%rip), %rdi
	callq	FreeCharMtx
	movq	Fgetlag.tmpseq2(%rip), %rdi
	callq	FreeCharMtx
.LBB0_16:
	movl	njob(%rip), %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpseq1(%rip)
	movl	njob(%rip), %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, Fgetlag.tmpseq2(%rip)
	movl	%r12d, %edi
	callq	AllocateFukusosuuVec
	movq	%rax, Fgetlag.naisekiNoWa(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%r12d, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Fgetlag.naiseki(%rip)
	movl	n20or4or2(%rip), %edi
	incl	%edi
	leal	1(%r12), %ebx
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Fgetlag.seqVector1(%rip)
	movl	n20or4or2(%rip), %edi
	incl	%edi
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Fgetlag.seqVector2(%rip)
	movl	%ebx, %edi
	callq	AllocateDoubleVec
	movq	%rax, Fgetlag.soukan(%rip)
	movl	%r12d, Fgetlag.localalloclen(%rip)
.LBB0_17:                               # %.preheader401
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_20
# BB#18:                                # %.lr.ph480.preheader
	movl	20(%rsp), %ebx          # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph480
                                        # =>This Inner Loop Header: Depth=1
	movq	Fgetlag.tmpseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r13,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_19
.LBB0_20:                               # %.preheader400
	testl	%r15d, %r15d
	jle	.LBB0_23
# BB#21:                                # %.lr.ph477.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph477
                                        # =>This Inner Loop Header: Depth=1
	movq	Fgetlag.tmpseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r14,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_22
.LBB0_23:                               # %._crit_edge478
	movq	%r13, 72(%rsp)          # 8-byte Spill
	cmpl	$0, fftkeika(%rip)
	jne	.LBB0_24
.LBB0_25:                               # %.preheader399
	movslq	n20or4or2(%rip), %rbp
	testq	%rbp, %rbp
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jle	.LBB0_31
# BB#26:                                # %.lr.ph474
	testl	%r12d, %r12d
	je	.LBB0_31
# BB#27:                                # %.lr.ph474.split.preheader
	leal	-1(%r12), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Fgetlag.seqVector1(%rip), %rbx
	leaq	-1(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	xorl	%r13d, %r13d
	andq	$7, %r14
	je	.LBB0_29
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph474.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%r13
	cmpq	%r13, %r14
	jne	.LBB0_28
.LBB0_29:                               # %.lr.ph474.split.prol.loopexit
	cmpq	$7, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_31
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph474.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbx,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %r13
	cmpq	%rbp, %r13
	jl	.LBB0_30
.LBB0_31:                               # %._crit_edge475
	movl	fftscore(%rip), %r8d
	testl	%r8d, %r8d
	je	.LBB0_33
# BB#32:                                # %._crit_edge475
	cmpl	$-1, scoremtx(%rip)
	je	.LBB0_33
# BB#42:                                # %.preheader396
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
	jle	.LBB0_55
# BB#43:                                # %.lr.ph470
	movq	Fgetlag.seqVector1(%rip), %r9
	movq	Fgetlag.tmpseq1(%rip), %r11
	movq	(%r9), %r10
	movl	20(%rsp), %r14d         # 4-byte Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_44:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_46 Depth 2
                                        #     Child Loop BB0_51 Depth 2
	movq	(%r11,%rbx,8), %rdi
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_54
# BB#45:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_44 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	leaq	1(%rdi), %rsi
	movq	%rsi, %rax
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph.i
                                        #   Parent Loop BB0_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$19, %rcx
	ja	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_46 Depth=2
	movsd	polarity(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
.LBB0_48:                               #   in Loop: Header=BB0_46 Depth=2
	addq	$16, %rdx
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_46
# BB#49:                                # %seq_vec_2.exit
                                        #   in Loop: Header=BB0_44 Depth=1
	movb	(%rdi), %al
	testb	%al, %al
	je	.LBB0_54
# BB#50:                                # %.lr.ph.i350.preheader
                                        #   in Loop: Header=BB0_44 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movq	8(%r9), %rcx
	.p2align	4, 0x90
.LBB0_51:                               # %.lr.ph.i350
                                        #   Parent Loop BB0_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$19, %rax
	ja	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_51 Depth=2
	movsd	volume(,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
.LBB0_53:                               #   in Loop: Header=BB0_51 Depth=2
	addq	$16, %rcx
	movzbl	(%rsi), %eax
	incq	%rsi
	testb	%al, %al
	jne	.LBB0_51
.LBB0_54:                               # %seq_vec_2.exit351
                                        #   in Loop: Header=BB0_44 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB0_44
	jmp	.LBB0_55
.LBB0_33:                               # %.preheader397
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
	jle	.LBB0_55
# BB#34:                                # %.lr.ph472
	movq	Fgetlag.seqVector1(%rip), %rax
	movq	Fgetlag.tmpseq1(%rip), %r9
	movl	20(%rsp), %edx          # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_37 Depth 2
	movq	(%r9,%rsi,8), %rdi
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_41
# BB#36:                                # %.lr.ph.i352.preheader
                                        #   in Loop: Header=BB0_35 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i352
                                        #   Parent Loop BB0_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	testq	%rcx, %rcx
	js	.LBB0_40
# BB#38:                                # %.lr.ph.i352
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpl	%ebp, %ecx
	jge	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_37 Depth=2
	movq	(%rax,%rcx,8), %rcx
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx,%rbx)
.LBB0_40:                               #   in Loop: Header=BB0_37 Depth=2
	movzbl	(%rdi), %ecx
	addq	$16, %rbx
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB0_37
.LBB0_41:                               # %seq_vec_3.exit
                                        #   in Loop: Header=BB0_35 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB0_35
.LBB0_55:                               # %.preheader395
	testl	%ebp, %ebp
	jle	.LBB0_62
# BB#56:                                # %.lr.ph467
	testl	%r12d, %r12d
	je	.LBB0_62
# BB#57:                                # %.lr.ph467.split.preheader
	leal	-1(%r12), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Fgetlag.seqVector2(%rip), %r13
	leaq	-1(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	xorl	%ebx, %ebx
	andq	$7, %r14
	je	.LBB0_59
	.p2align	4, 0x90
.LBB0_58:                               # %.lr.ph467.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB0_58
.LBB0_59:                               # %.lr.ph467.split.prol.loopexit
	cmpq	$7, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_61
	.p2align	4, 0x90
.LBB0_60:                               # %.lr.ph467.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_60
.LBB0_61:                               # %._crit_edge468.loopexit485
	movl	fftscore(%rip), %r8d
	movl	12(%rsp), %r15d         # 4-byte Reload
.LBB0_62:                               # %._crit_edge468
	movl	%r12d, %eax
	shrl	$31, %eax
	movq	%rax, %r14
	testl	%r8d, %r8d
	je	.LBB0_64
# BB#63:                                # %._crit_edge468
	cmpl	$-1, scoremtx(%rip)
	je	.LBB0_64
# BB#73:                                # %.preheader392
	testl	%r15d, %r15d
	jle	.LBB0_86
# BB#74:                                # %.lr.ph463
	movq	Fgetlag.seqVector2(%rip), %r8
	movq	Fgetlag.tmpseq2(%rip), %r10
	movq	(%r8), %r9
	movl	%r15d, %r11d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_75:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_77 Depth 2
                                        #     Child Loop BB0_82 Depth 2
	movq	(%r10,%rdi,8), %rsi
	movb	(%rsi), %al
	testb	%al, %al
	je	.LBB0_85
# BB#76:                                # %.lr.ph.i365.preheader
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	leaq	1(%rsi), %rdx
	movq	%rdx, %rbx
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph.i365
                                        #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$19, %rax
	ja	.LBB0_79
# BB#78:                                #   in Loop: Header=BB0_77 Depth=2
	movsd	polarity(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
.LBB0_79:                               #   in Loop: Header=BB0_77 Depth=2
	addq	$16, %rcx
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB0_77
# BB#80:                                # %seq_vec_2.exit366
                                        #   in Loop: Header=BB0_75 Depth=1
	movb	(%rsi), %cl
	testb	%cl, %cl
	je	.LBB0_85
# BB#81:                                # %.lr.ph.i361.preheader
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	8(%r8), %rax
	.p2align	4, 0x90
.LBB0_82:                               # %.lr.ph.i361
                                        #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$19, %rcx
	ja	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_82 Depth=2
	movsd	volume(,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rax), %xmm1
	movsd	%xmm1, (%rax)
.LBB0_84:                               #   in Loop: Header=BB0_82 Depth=2
	addq	$16, %rax
	movzbl	(%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jne	.LBB0_82
.LBB0_85:                               # %seq_vec_2.exit362
                                        #   in Loop: Header=BB0_75 Depth=1
	incq	%rdi
	cmpq	%r11, %rdi
	jne	.LBB0_75
	jmp	.LBB0_86
.LBB0_64:                               # %.preheader393
	testl	%r15d, %r15d
	jle	.LBB0_86
# BB#65:                                # %.lr.ph465
	movq	Fgetlag.seqVector2(%rip), %rax
	movq	Fgetlag.tmpseq2(%rip), %r8
	movl	%r15d, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_66:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_68 Depth 2
	movq	(%r8,%rsi,8), %rdi
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_72
# BB#67:                                # %.lr.ph.i353.preheader
                                        #   in Loop: Header=BB0_66 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_68:                               # %.lr.ph.i353
                                        #   Parent Loop BB0_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	testq	%rcx, %rcx
	js	.LBB0_71
# BB#69:                                # %.lr.ph.i353
                                        #   in Loop: Header=BB0_68 Depth=2
	cmpl	%ebp, %ecx
	jge	.LBB0_71
# BB#70:                                #   in Loop: Header=BB0_68 Depth=2
	movq	(%rax,%rcx,8), %rcx
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx,%rbx)
.LBB0_71:                               #   in Loop: Header=BB0_68 Depth=2
	movzbl	(%rdi), %ecx
	addq	$16, %rbx
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB0_68
.LBB0_72:                               # %seq_vec_3.exit358
                                        #   in Loop: Header=BB0_66 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB0_66
.LBB0_86:                               # %.preheader391
	movq	%r14, %r15
	addl	%r12d, %r15d
	testl	%ebp, %ebp
	jle	.LBB0_100
# BB#87:                                # %.lr.ph461.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_88:                               # %.lr.ph461
                                        # =>This Inner Loop Header: Depth=1
	movq	Fgetlag.seqVector2(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	testq	%rbx, %rbx
	sete	%dl
	movl	%r12d, %edi
	callq	fft
	movq	Fgetlag.seqVector1(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	fft
	incq	%rbx
	movslq	n20or4or2(%rip), %rbp
	cmpq	%rbp, %rbx
	jl	.LBB0_88
# BB#89:                                # %.preheader390
	testl	%ebp, %ebp
	jle	.LBB0_100
# BB#90:                                # %.preheader389.lr.ph
	testl	%r12d, %r12d
	jle	.LBB0_91
# BB#95:                                # %.preheader389.us.preheader
	movq	%r15, %r14
	movl	%r12d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_96:                               # %.preheader389.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_97 Depth 2
	movq	%r13, %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_97:                               #   Parent Loop BB0_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Fgetlag.naiseki(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	%r15, %rdi
	movq	Fgetlag.seqVector1(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	movq	Fgetlag.seqVector2(%rip), %rax
	movq	(%rax,%rbx,8), %rdx
	addq	%r15, %rdx
	callq	calcNaiseki
	addq	$16, %r15
	decq	%rbp
	jne	.LBB0_97
# BB#98:                                # %._crit_edge458.us
                                        #   in Loop: Header=BB0_96 Depth=1
	incq	%rbx
	movslq	n20or4or2(%rip), %rbp
	cmpq	%rbp, %rbx
	jl	.LBB0_96
# BB#99:
	movq	%r14, %r15
	jmp	.LBB0_100
.LBB0_91:                               # %.preheader389.preheader
	leal	-1(%rbp), %ecx
	movl	%ebp, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB0_93
	.p2align	4, 0x90
.LBB0_92:                               # %.preheader389.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB0_92
.LBB0_93:                               # %.preheader389.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB0_100
	.p2align	4, 0x90
.LBB0_94:                               # %.preheader389
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	%ebp, %eax
	jl	.LBB0_94
.LBB0_100:                              # %.preheader388
	sarl	%r15d
	movq	Fgetlag.naisekiNoWa(%rip), %r13
	testl	%r12d, %r12d
	jle	.LBB0_111
# BB#101:                               # %.lr.ph454
	testl	%ebp, %ebp
	jle	.LBB0_219
# BB#102:                               # %.lr.ph454.split.us.preheader
	movq	Fgetlag.naiseki(%rip), %rax
	movl	%ebp, %r9d
	movl	%r12d, %r10d
	leaq	-1(%r9), %r11
	movl	%r9d, %edi
	andl	$3, %edi
	leaq	24(%rax), %r8
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_103:                              # %.lr.ph454.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_106 Depth 2
                                        #     Child Loop BB0_109 Depth 2
	movq	%r14, %rbp
	shlq	$4, %rbp
	leaq	(%r13,%rbp), %rdx
	testq	%rdi, %rdi
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r13,%rbp)
	je	.LBB0_104
# BB#105:                               # %.prol.preheader638
                                        #   in Loop: Header=BB0_103 Depth=1
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_106:                              #   Parent Loop BB0_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rsi
	movupd	(%rsi,%rbp), %xmm1
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	incq	%rbx
	cmpq	%rbx, %rdi
	jne	.LBB0_106
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_104:                              #   in Loop: Header=BB0_103 Depth=1
	xorl	%ebx, %ebx
.LBB0_107:                              # %.prol.loopexit639
                                        #   in Loop: Header=BB0_103 Depth=1
	cmpq	$3, %r11
	jb	.LBB0_110
# BB#108:                               # %.lr.ph454.split.us.new
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	%r9, %rsi
	subq	%rbx, %rsi
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB0_109:                              #   Parent Loop BB0_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	-16(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	movq	-8(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB0_109
.LBB0_110:                              # %._crit_edge452.us
                                        #   in Loop: Header=BB0_103 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB0_103
.LBB0_111:                              # %._crit_edge455
	movl	%r12d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	fft
	cmpl	$-1, %r12d
	jge	.LBB0_112
	jmp	.LBB0_114
.LBB0_219:                              # %._crit_edge455.thread
	leal	-1(%r12), %edx
	shlq	$4, %rdx
	addq	$16, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	movl	%r12d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	fft
.LBB0_112:                              # %.lr.ph448
	movq	Fgetlag.soukan(%rip), %rax
	movslq	%r15d, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	addq	Fgetlag.naisekiNoWa(%rip), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB0_113:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdi
	movq	%rdi, 8(%rax,%rsi,8)
	incq	%rsi
	addq	$-16, %rdx
	cmpq	%rsi, %rcx
	jg	.LBB0_113
.LBB0_114:                              # %.preheader387
	leal	1(%r15), %ecx
	cmpl	%r12d, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	jge	.LBB0_115
# BB#116:                               # %.lr.ph444
	movq	Fgetlag.naisekiNoWa(%rip), %rax
	movq	Fgetlag.soukan(%rip), %rdx
	movslq	%ecx, %rcx
	leal	3(%r12), %edi
	subl	%r15d, %edi
	leal	-2(%r12), %esi
	subl	%r15d, %esi
	andl	$3, %edi
	je	.LBB0_119
# BB#117:                               # %.prol.preheader633
	leal	-1(%r12), %ebx
	negl	%edi
	.p2align	4, 0x90
.LBB0_118:                              # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movq	%rbx, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rdx,%rcx,8)
	incq	%rcx
	decl	%ebx
	incl	%edi
	jne	.LBB0_118
.LBB0_119:                              # %.prol.loopexit634
	cmpl	$3, %esi
	jb	.LBB0_122
# BB#120:                               # %.lr.ph444.new
	leaq	24(%rdx,%rcx,8), %rsi
	movq	%rcx, %rdi
	addq	$3, %rdi
	movl	%r15d, %r8d
	subl	%edi, %r8d
	leal	2(%rcx), %edi
	movl	%r15d, %r9d
	subl	%edi, %r9d
	movl	%r15d, %ebx
	subl	%ecx, %ebx
	leal	1(%rcx), %edi
	subl	%edi, %r15d
	movl	%r12d, %edi
	.p2align	4, 0x90
.LBB0_121:                              # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -24(%rsi)
	leal	(%r15,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -16(%rsi)
	leal	(%r9,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -8(%rsi)
	leal	(%r8,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rsi)
	addl	$-4, %edi
	addq	$32, %rsi
	cmpl	%edi, %ecx
	jne	.LBB0_121
	jmp	.LBB0_122
.LBB0_115:                              # %.preheader387.._crit_edge445_crit_edge
	movq	Fgetlag.soukan(%rip), %rdx
.LBB0_122:                              # %._crit_edge445
	movq	Fgetlag.kouho(%rip), %rdi
	movl	$20, %esi
	movl	%r12d, %ecx
	callq	getKouho
	movl	$0, 16(%rsp)
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_123:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_129 Depth 2
	movq	Fgetlag.kouho(%rip), %rax
	movl	(%rax,%r15,4), %r12d
	movq	Fgetlag.tmpptr1(%rip), %r9
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	%r12d, %edi
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movl	%r13d, %edx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	pushq	Fgetlag.tmpptr2(%rip)
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	zurasu2
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	Fgetlag.tmpptr1(%rip), %rdx
	movq	Fgetlag.tmpptr2(%rip), %rcx
	movslq	%ebx, %r14
	leaq	(%r14,%r14,2), %rax
	shlq	$4, %rax
	addq	Fgetlag.segment(%rip), %rax
	subq	$8, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %edi
	movl	%r13d, %esi
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	alignableReagion
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	addl	%ebp, %r14d
	cmpl	$99998, %r14d           # imm = 0x1869E
	jl	.LBB0_125
# BB#124:                               #   in Loop: Header=BB0_123 Depth=1
	movl	$.L.str.2, %edi
	callq	ErrorExit
.LBB0_125:                              #   in Loop: Header=BB0_123 Depth=1
	testl	%ebp, %ebp
	je	.LBB0_126
# BB#127:                               # %.preheader386
                                        #   in Loop: Header=BB0_123 Depth=1
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB0_133
# BB#128:                               # %.lr.ph439
                                        #   in Loop: Header=BB0_123 Depth=1
	movq	Fgetlag.segment(%rip), %rax
	movq	Fgetlag.segment1(%rip), %r8
	movq	Fgetlag.segment2(%rip), %rdx
	incl	%ebp
	.p2align	4, 0x90
.LBB0_129:                              #   Parent Loop BB0_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$4, %rsi
	movl	(%rax,%rsi), %edi
	testl	%r12d, %r12d
	jle	.LBB0_131
# BB#130:                               #   in Loop: Header=BB0_129 Depth=2
	movl	%edi, (%r8,%rsi)
	movslq	16(%rsp), %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	4(%rax,%rsi), %edi
	movl	%edi, 4(%r8,%rsi)
	movl	8(%rax,%rsi), %edi
	movl	%edi, 8(%r8,%rsi)
	movq	16(%rax,%rsi), %rdi
	movq	%rdi, 16(%r8,%rsi)
	movl	(%rax,%rsi), %edi
	addl	%r12d, %edi
	movl	%edi, (%rdx,%rsi)
	movl	16(%rsp), %esi
	movslq	%esi, %rcx
	movq	%rcx, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	addl	%r12d, %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
	addl	%r12d, %ebx
	jmp	.LBB0_132
	.p2align	4, 0x90
.LBB0_131:                              #   in Loop: Header=BB0_129 Depth=2
	subl	%r12d, %edi
	movl	%edi, (%r8,%rsi)
	movslq	16(%rsp), %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	4(%rax,%rcx), %esi
	subl	%r12d, %esi
	movl	%esi, 4(%r8,%rcx)
	movl	8(%rax,%rcx), %esi
	subl	%r12d, %esi
	movl	%esi, 8(%r8,%rcx)
	movq	16(%rax,%rcx), %rsi
	movq	%rsi, 16(%r8,%rcx)
	movl	(%rax,%rcx), %esi
	movl	%esi, (%rdx,%rcx)
	movl	16(%rsp), %esi
	movslq	%esi, %rcx
	movq	%rcx, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
.LBB0_132:                              #   in Loop: Header=BB0_129 Depth=2
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movl	%ebx, 8(%rdx,%rcx)
	movslq	%esi, %rbx
	movq	%rbx, %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	16(%rax,%rcx), %rsi
	leaq	(%rdx,%rcx), %rdi
	movq	%rsi, 16(%rdx,%rcx)
	leaq	(%r8,%rcx), %rsi
	movq	%rdi, 32(%r8,%rcx)
	movq	%rsi, 32(%rdx,%rcx)
	incl	%ebx
	movl	%ebx, 16(%rsp)
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB0_129
.LBB0_133:                              # %._crit_edge440
                                        #   in Loop: Header=BB0_123 Depth=1
	incq	%r15
	cmpq	$20, %r15
	jl	.LBB0_123
	jmp	.LBB0_134
.LBB0_126:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB0_134:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	testl	%ebx, %ebx
	jne	.LBB0_137
# BB#135:
	movl	fftNoAnchStop(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_137
# BB#136:                               # %.preheader385.thread
	movl	$.L.str.4, %edi
	callq	ErrorExit
	jmp	.LBB0_138
.LBB0_137:                              # %.preheader385
	testl	%ebx, %ebx
	setg	%r15b
	jle	.LBB0_138
# BB#139:                               # %.lr.ph436
	movq	Fgetlag.segment1(%rip), %rax
	movq	Fgetlag.sortedseg1(%rip), %rdx
	movq	Fgetlag.segment2(%rip), %rcx
	movq	Fgetlag.sortedseg2(%rip), %r9
	movslq	%ebx, %r8
	cmpl	$4, %ebx
	jae	.LBB0_141
# BB#140:
	xorl	%ebp, %ebp
	jmp	.LBB0_149
.LBB0_138:                              # %.preheader385.._crit_edge437_crit_edge
	movq	Fgetlag.sortedseg1(%rip), %rdx
	xorl	%r15d, %r15d
	jmp	.LBB0_151
.LBB0_141:                              # %min.iters.checked
	movq	%r8, %rbp
	andq	$-4, %rbp
	je	.LBB0_142
# BB#143:                               # %vector.memcheck
	leaq	-8(%r9,%r8,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB0_146
# BB#144:                               # %vector.memcheck
	leaq	-8(%rdx,%r8,8), %rsi
	cmpq	%rsi, %r9
	jae	.LBB0_146
# BB#145:
	xorl	%ebp, %ebp
	jmp	.LBB0_149
.LBB0_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_25
.LBB0_142:
	xorl	%ebp, %ebp
	jmp	.LBB0_149
.LBB0_146:                              # %vector.body.preheader
	xorl	%edi, %edi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_147:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rdi), %r10
	leaq	48(%rax,%rdi), %r11
	leaq	96(%rax,%rdi), %r14
	leaq	144(%rax,%rdi), %r12
	movd	%r11, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r14, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%rdx,%rsi,8)
	movdqu	%xmm2, 16(%rdx,%rsi,8)
	leaq	(%rcx,%rdi), %r10
	leaq	48(%rcx,%rdi), %r11
	leaq	96(%rcx,%rdi), %r14
	leaq	144(%rcx,%rdi), %r12
	movd	%r11, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r14, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%r9,%rsi,8)
	movdqu	%xmm2, 16(%r9,%rsi,8)
	addq	$4, %rsi
	addq	$192, %rdi
	cmpq	%rsi, %rbp
	jne	.LBB0_147
# BB#148:                               # %middle.block
	cmpq	%rbp, %r8
	movq	24(%rsp), %r14          # 8-byte Reload
	je	.LBB0_151
.LBB0_149:                              # %scalar.ph.preheader
	movq	%rbp, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	addq	%rsi, %rax
	addq	%rsi, %rcx
	.p2align	4, 0x90
.LBB0_150:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rdx,%rbp,8)
	movq	%rcx, (%r9,%rbp,8)
	incq	%rbp
	addq	$48, %rax
	addq	$48, %rcx
	cmpq	%r8, %rbp
	jl	.LBB0_150
.LBB0_151:                              # %._crit_edge437
	leal	-1(%rbx), %ebp
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	movq	Fgetlag.sortedseg2(%rip), %rdx
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	testb	%r15b, %r15b
	je	.LBB0_157
# BB#152:                               # %.lr.ph434
	movq	Fgetlag.sortedseg1(%rip), %rcx
	movslq	%ebx, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_153:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB0_153
# BB#154:                               # %.preheader384
	testb	%r15b, %r15b
	je	.LBB0_157
# BB#155:                               # %.lr.ph430
	movq	Fgetlag.sortedseg2(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_156:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB0_156
.LBB0_157:                              # %._crit_edge431
	leal	2(%rbx), %r15d
	cmpl	%r15d, Fgetlag.crossscoresize(%rip)
	jl	.LBB0_158
# BB#161:                               # %.preheader383
	cmpl	$-1, %ebx
	jl	.LBB0_165
.LBB0_162:                              # %.preheader382.us.preheader
	movq	Fgetlag.crossscore(%rip), %r14
	incl	%ebx
	leaq	8(,%rbx,8), %r12
	movslq	%r15d, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_163:                              # %.preheader382.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_163
# BB#164:                               # %.preheader381
	cmpl	$0, 16(%rsp)
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB0_165
# BB#166:                               # %.lr.ph423
	movq	Fgetlag.segment1(%rip), %rax
	movq	Fgetlag.crossscore(%rip), %r8
	movq	Fgetlag.sortedseg1(%rip), %rdx
	movq	Fgetlag.cut1(%rip), %rdi
	movq	Fgetlag.sortedseg2(%rip), %rcx
	movq	Fgetlag.cut2(%rip), %rsi
	addq	$40, %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_167:                              # =>This Inner Loop Header: Depth=1
	movslq	(%rax), %rbx
	movq	8(%r8,%rbx,8), %r9
	movq	-24(%rax), %r10
	movq	-8(%rax), %rbx
	movslq	40(%rbx), %rbx
	movq	%r10, 8(%r9,%rbx,8)
	movq	(%rdx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rdi,%rbp,4)
	movq	(%rcx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rsi,%rbp,4)
	incq	%rbp
	movslq	16(%rsp), %rbx
	addq	$48, %rax
	cmpq	%rbx, %rbp
	jl	.LBB0_167
	jmp	.LBB0_168
.LBB0_158:
	movl	%r15d, Fgetlag.crossscoresize(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movq	Fgetlag.crossscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_160
# BB#159:
	callq	FreeDoubleMtx
.LBB0_160:
	movl	Fgetlag.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, Fgetlag.crossscore(%rip)
	cmpl	$-1, %ebx
	jge	.LBB0_162
.LBB0_165:                              # %.preheader381.._crit_edge424_crit_edge
	movq	Fgetlag.crossscore(%rip), %r8
	movq	Fgetlag.cut1(%rip), %rdi
	movq	Fgetlag.cut2(%rip), %rsi
	movq	Fgetlag.sortedseg1(%rip), %rdx
	movq	Fgetlag.sortedseg2(%rip), %rcx
.LBB0_168:
	movl	20(%rsp), %r15d         # 4-byte Reload
	movq	(%r8), %rax
	movabsq	$4711630319722168320, %rbp # imm = 0x416312D000000000
	movq	%rbp, (%rax)
	movl	$0, (%rdi)
	movl	$0, (%rsi)
	movslq	16(%rsp), %rax
	movq	8(%r8,%rax,8), %rbx
	movq	%rbp, 8(%rbx,%rax,8)
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rdi,%rax,4)
	movslq	16(%rsp), %rax
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rsi,%rax,4)
	movl	16(%rsp), %ebx
	addl	$2, %ebx
	movl	%ebx, 16(%rsp)
	leaq	16(%rsp), %r9
	callq	blockAlign2
	cmpl	$0, fftkeika(%rip)
	je	.LBB0_171
# BB#169:                               # %._crit_edge424
	cmpl	16(%rsp), %ebx
	jle	.LBB0_171
# BB#170:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, fftRepeatStop(%rip)
	jne	.LBB0_220
.LBB0_171:                              # %.preheader380
	testl	%r15d, %r15d
	jle	.LBB0_177
# BB#172:                               # %.lr.ph421
	movq	Fgetlag.result1(%rip), %rcx
	movl	%r15d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB0_174
	.p2align	4, 0x90
.LBB0_173:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_173
.LBB0_174:                              # %.prol.loopexit629
	cmpq	$7, %rdx
	jb	.LBB0_177
# BB#175:                               # %.lr.ph421.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB0_176:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB0_176
.LBB0_177:                              # %.preheader379
	testl	%r13d, %r13d
	jle	.LBB0_183
# BB#178:                               # %.lr.ph419
	movq	Fgetlag.result2(%rip), %rcx
	movl	%r13d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB0_180
	.p2align	4, 0x90
.LBB0_179:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_179
.LBB0_180:                              # %.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_183
# BB#181:                               # %.lr.ph419.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB0_182:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB0_182
.LBB0_183:                              # %.preheader378
	cmpl	$2, 16(%rsp)
	jl	.LBB0_193
# BB#184:                               # %.preheader377.lr.ph
	cmpl	$1, %r15d
	sete	%al
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	$1, %ecx
	sete	%dl
	andb	%al, %dl
	movb	%dl, 56(%rsp)           # 1-byte Spill
	movl	%r15d, %r13d
	movl	%ecx, %ebx
	xorl	%r12d, %r12d
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB0_185:                              # %.preheader377
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_187 Depth 2
                                        #     Child Loop BB0_190 Depth 2
                                        #     Child Loop BB0_214 Depth 2
                                        #     Child Loop BB0_217 Depth 2
	testl	%r15d, %r15d
	movq	72(%rsp), %r15          # 8-byte Reload
	jle	.LBB0_188
# BB#186:                               # %.lr.ph407
                                        #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rcx
	movq	Fgetlag.cut1(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_187:                              #   Parent Loop BB0_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbp,8), %rdi
	movslq	(%rax,%r12,4), %rcx
	movq	(%r15,%rbp,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r12,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Fgetlag.tmpres1(%rip), %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	Fgetlag.cut1(%rip), %rax
	movslq	4(%rax,%r12,4), %rsi
	movslq	(%rax,%r12,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB0_187
.LBB0_188:                              # %.preheader376
                                        #   in Loop: Header=BB0_185 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_191
# BB#189:                               # %.lr.ph409
                                        #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres2(%rip), %rcx
	movq	Fgetlag.cut2(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_190:                              #   Parent Loop BB0_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbp,8), %rdi
	movslq	(%rax,%r12,4), %rcx
	movq	(%r14,%rbp,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r12,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Fgetlag.tmpres2(%rip), %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	Fgetlag.cut2(%rip), %rax
	movslq	4(%rax,%r12,4), %rsi
	movslq	(%rax,%r12,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_190
.LBB0_191:                              # %._crit_edge410
                                        #   in Loop: Header=BB0_185 Depth=1
	movsbl	alg(%rip), %edx
	leal	-65(%rdx), %eax
	cmpl	$32, %eax
	movl	20(%rsp), %r15d         # 4-byte Reload
	ja	.LBB0_209
# BB#192:                               # %._crit_edge410
                                        #   in Loop: Header=BB0_185 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_202:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rdi
	movq	Fgetlag.tmpres2(%rip), %rsi
	cmpb	$0, 56(%rsp)            # 1-byte Folded Reload
	movl	144(%rsp), %ebp
	jne	.LBB0_203
# BB#204:                               #   in Loop: Header=BB0_185 Depth=1
	subq	$8, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	leaq	92(%rsp), %rax
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	A__align
	addq	$64, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB0_210
.LBB0_205:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rdi
	movq	Fgetlag.tmpres2(%rip), %rsi
	cmpb	$0, 56(%rsp)            # 1-byte Folded Reload
	movl	144(%rsp), %ebp
	jne	.LBB0_203
# BB#206:                               #   in Loop: Header=BB0_185 Depth=1
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	leaq	92(%rsp), %rax
	pushq	%rax
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	H__align
	addq	$64, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB0_210
.LBB0_201:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rdi
	movq	Fgetlag.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	184(%rsp), %ebp
	pushq	%rbp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm
	addq	$48, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_210
.LBB0_207:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rdi
	movq	Fgetlag.tmpres2(%rip), %rsi
	cmpb	$0, 56(%rsp)            # 1-byte Folded Reload
	movl	144(%rsp), %ebp
	je	.LBB0_208
.LBB0_203:                              #   in Loop: Header=BB0_185 Depth=1
	movl	%ebp, %edx
	callq	G__align11
	jmp	.LBB0_210
.LBB0_200:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rdi
	movq	Fgetlag.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	xorl	%eax, %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	movl	152(%rsp), %ebp
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	Aalign
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_210
.LBB0_208:                              #   in Loop: Header=BB0_185 Depth=1
	subq	$8, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	leaq	92(%rsp), %rax
	pushq	%rax
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	Q__align
	addq	$64, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB0_210
.LBB0_209:                              #   in Loop: Header=BB0_185 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$.L.str.8, %edi
	callq	ErrorExit
	movl	144(%rsp), %ebp
	.p2align	4, 0x90
.LBB0_210:                              #   in Loop: Header=BB0_185 Depth=1
	movq	Fgetlag.tmpres1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movl	24(%rsp), %ecx          # 4-byte Reload
	addl	%eax, %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpl	%ebp, %ecx
	jle	.LBB0_212
# BB#211:                               #   in Loop: Header=BB0_185 Depth=1
	movl	$.L.str.9, %edi
	callq	ErrorExit
.LBB0_212:                              # %.preheader375
                                        #   in Loop: Header=BB0_185 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_215
# BB#213:                               # %.lr.ph412.preheader
                                        #   in Loop: Header=BB0_185 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_214:                              # %.lr.ph412
                                        #   Parent Loop BB0_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Fgetlag.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Fgetlag.tmpres1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB0_214
.LBB0_215:                              # %.preheader374
                                        #   in Loop: Header=BB0_185 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_218
# BB#216:                               # %.lr.ph414.preheader
                                        #   in Loop: Header=BB0_185 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_217:                              # %.lr.ph414
                                        #   Parent Loop BB0_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Fgetlag.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Fgetlag.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_217
.LBB0_218:                              # %._crit_edge415
                                        #   in Loop: Header=BB0_185 Depth=1
	incq	%r12
	movslq	16(%rsp), %rax
	decq	%rax
	cmpq	%rax, %r12
	jl	.LBB0_185
.LBB0_193:                              # %.preheader373
	testl	%r15d, %r15d
	movl	%r15d, %eax
	movq	72(%rsp), %r15          # 8-byte Reload
	jle	.LBB0_196
# BB#194:                               # %.lr.ph405.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_195:                              # %.lr.ph405
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	Fgetlag.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_195
.LBB0_196:                              # %.preheader
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB0_199
# BB#197:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_198:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	Fgetlag.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_198
.LBB0_199:                              # %._crit_edge
	xorpd	%xmm0, %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_220:
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	Fgetlag, .Lfunc_end0-Fgetlag
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_202
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_205
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_201
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_207
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_209
	.quad	.LBB0_200

	.text
	.p2align	4, 0x90
	.type	mymergesort,@function
mymergesort:                            # @mymergesort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movl	%edi, %r13d
	cmpl	%r15d, mymergesort.allo(%rip)
	jge	.LBB1_4
# BB#1:
	movl	%r15d, mymergesort.allo(%rip)
	movq	mymergesort.work(%rip), %rdi
	testq	%rdi, %rdi
	movl	%r15d, %eax
	je	.LBB1_3
# BB#2:
	callq	free
	movl	mymergesort.allo(%rip), %eax
.LBB1_3:
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	incl	%ecx
	movslq	%ecx, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, mymergesort.work(%rip)
.LBB1_4:
	cmpl	%r15d, %r13d
	jge	.LBB1_40
# BB#5:
	leal	(%r15,%r13), %eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	addl	%eax, %r12d
	sarl	%r12d
	movl	%r13d, %edi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	mymergesort
	leal	1(%r12), %ebp
	movl	%ebp, %edi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	mymergesort
	movl	$0, mymergesort.p(%rip)
	movl	%r13d, mymergesort.i(%rip)
	xorl	%ecx, %ecx
	cmpl	%r13d, %r12d
	movl	$0, %eax
	jl	.LBB1_21
# BB#6:                                 # %.lr.ph37
	movq	mymergesort.work(%rip), %r9
	movl	%r13d, %esi
	cmovgel	%r12d, %esi
	subl	%r13d, %esi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB1_7
# BB#8:                                 # %min.iters.checked
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rsi, %rax
	je	.LBB1_7
# BB#9:                                 # %vector.memcheck
	cmpl	%r13d, %r12d
	movl	%r13d, %edx
	cmovgel	%r12d, %edx
	subl	%r13d, %edx
	movslq	%r13d, %r10
	leaq	(%r10,%rdx), %rdi
	leaq	8(%r14,%rdi,8), %rdi
	cmpq	%rdi, %r9
	jae	.LBB1_11
# BB#10:                                # %vector.memcheck
	leaq	8(%r9,%rdx,8), %rdx
	leaq	(%r14,%r10,8), %rdi
	cmpq	%rdx, %rdi
	jae	.LBB1_11
.LBB1_7:
	xorl	%eax, %eax
	movl	%r13d, %edi
.LBB1_18:                               # %scalar.ph.preheader
	movslq	%edi, %rdx
	leaq	(%r14,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB1_19:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rdx
	movq	%rdx, (%r9,%rax,8)
	incq	%rax
	leal	1(%rdi), %edx
	addq	$8, %rsi
	cmpl	%r12d, %edi
	movl	%edx, %edi
	jl	.LBB1_19
.LBB1_20:                               # %._crit_edge
	movl	%eax, mymergesort.p(%rip)
	movl	%edx, mymergesort.i(%rip)
.LBB1_21:
	movl	%ebp, mymergesort.i(%rip)
	movl	$0, mymergesort.j(%rip)
	movl	%r13d, mymergesort.k(%rip)
	cmpl	%r15d, %r12d
	jge	.LBB1_26
# BB#22:                                # %.lr.ph34
	testl	%eax, %eax
	jle	.LBB1_26
# BB#23:                                # %.lr.ph64.preheader
	movq	mymergesort.work(%rip), %r9
	movslq	%r13d, %r13
	xorl	%ecx, %ecx
	movl	$mymergesort.i, %r8d
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph64
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rcx
	leaq	(%r9,%rcx,8), %rdi
	movq	(%r9,%rcx,8), %rsi
	movl	8(%rsi), %esi
	movslq	%ebp, %rbp
	leaq	(%r14,%rbp,8), %rbx
	movq	(%r14,%rbp,8), %rdx
	cmpl	8(%rdx), %esi
	cmovlel	%ecx, %ebp
	movl	$mymergesort.j, %ecx
	cmovgq	%r8, %rcx
	cmovleq	%rdi, %rbx
	incl	%ebp
	movl	%ebp, (%rcx)
	movq	(%rbx), %rcx
	movq	%rcx, (%r14,%r13,8)
	movl	mymergesort.i(%rip), %ebp
	movl	mymergesort.j(%rip), %ecx
	incq	%r13
	cmpl	%r15d, %ebp
	jg	.LBB1_25
# BB#41:                                # %.lr.ph64
                                        #   in Loop: Header=BB1_24 Depth=1
	cmpl	%eax, %ecx
	jl	.LBB1_24
.LBB1_25:                               # %.critedge.preheader.loopexit
	movl	%r13d, mymergesort.k(%rip)
.LBB1_26:                               # %.critedge.preheader
	cmpl	%eax, %ecx
	jge	.LBB1_40
# BB#27:                                # %.lr.ph
	movq	mymergesort.work(%rip), %rsi
	movslq	%r13d, %rdx
	movslq	%ecx, %rcx
	cltq
	movq	%rax, %r9
	subq	%rcx, %r9
	cmpq	$3, %r9
	jbe	.LBB1_38
# BB#28:                                # %min.iters.checked84
	movq	%r9, %r8
	andq	$-4, %r8
	je	.LBB1_38
# BB#29:                                # %vector.memcheck101
	leaq	(%r14,%rdx,8), %rdi
	leaq	(%rsi,%rax,8), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB1_31
# BB#30:                                # %vector.memcheck101
	leaq	(%rdx,%rax), %rdi
	subq	%rcx, %rdi
	leaq	(%r14,%rdi,8), %rdi
	leaq	(%rsi,%rcx,8), %rbp
	cmpq	%rdi, %rbp
	jb	.LBB1_38
.LBB1_31:                               # %vector.body80.preheader
	leaq	-4(%r8), %rdi
	movq	%rdi, %rbp
	shrq	$2, %rbp
	btl	$2, %edi
	jb	.LBB1_32
# BB#33:                                # %vector.body80.prol
	movups	(%rsi,%rcx,8), %xmm0
	movups	16(%rsi,%rcx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	movl	$4, %edi
	testq	%rbp, %rbp
	jne	.LBB1_35
	jmp	.LBB1_37
.LBB1_11:                               # %vector.body.preheader
	leaq	-4(%rax), %rdx
	movq	%rdx, %r8
	shrq	$2, %r8
	btl	$2, %edx
	jb	.LBB1_12
# BB#13:                                # %vector.body.prol
	movups	(%r14,%r10,8), %xmm0
	movups	16(%r14,%r10,8), %xmm1
	movups	%xmm0, (%r9)
	movups	%xmm1, 16(%r9)
	movl	$4, %edi
	testq	%r8, %r8
	jne	.LBB1_15
	jmp	.LBB1_17
.LBB1_32:
	xorl	%edi, %edi
	testq	%rbp, %rbp
	je	.LBB1_37
.LBB1_35:                               # %vector.body80.preheader.new
	movq	%r8, %rbp
	subq	%rdi, %rbp
	leaq	(%rdi,%rdx), %rbx
	leaq	48(%r14,%rbx,8), %rbx
	addq	%rcx, %rdi
	leaq	48(%rsi,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB1_36:                               # %vector.body80
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$64, %rbx
	addq	$64, %rdi
	addq	$-8, %rbp
	jne	.LBB1_36
.LBB1_37:                               # %middle.block81
	addq	%r8, %rcx
	addq	%r8, %rdx
	cmpq	%r8, %r9
	je	.LBB1_39
	.p2align	4, 0x90
.LBB1_38:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rcx,8), %rdi
	incq	%rcx
	movq	%rdi, (%r14,%rdx,8)
	incq	%rdx
	cmpq	%rax, %rcx
	jl	.LBB1_38
.LBB1_39:                               # %.critedge..loopexit_crit_edge
	movl	%ecx, mymergesort.j(%rip)
	movl	%edx, mymergesort.k(%rip)
.LBB1_40:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_12:
	xorl	%edi, %edi
	testq	%r8, %r8
	je	.LBB1_17
.LBB1_15:                               # %vector.body.preheader.new
	movl	%r13d, %r8d
	.p2align	4, 0x90
.LBB1_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r8,%rdi), %rdx
	movslq	%edx, %rdx
	movups	(%r14,%rdx,8), %xmm0
	movups	16(%r14,%rdx,8), %xmm1
	movups	%xmm0, (%r9,%rdi,8)
	movups	%xmm1, 16(%r9,%rdi,8)
	addl	$4, %edx
	movslq	%edx, %rdx
	movups	(%r14,%rdx,8), %xmm0
	movups	16(%r14,%rdx,8), %xmm1
	movups	%xmm0, 32(%r9,%rdi,8)
	movups	%xmm1, 48(%r9,%rdi,8)
	addq	$8, %rdi
	cmpq	%rdi, %rax
	jne	.LBB1_16
.LBB1_17:                               # %middle.block
	leal	(%rax,%r13), %edi
	cmpq	%rax, %rsi
	movl	%edi, %edx
	jne	.LBB1_18
	jmp	.LBB1_20
.Lfunc_end1:
	.size	mymergesort, .Lfunc_end1-mymergesort
	.cfi_endproc

	.globl	Falign
	.p2align	4, 0x90
	.type	Falign,@function
Falign:                                 # @Falign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 176
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %r14d
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	$0, 84(%rsp)
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	(%r15), %rdi
	callq	strlen
	cmpl	%eax, %ebp
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	cmovgel	%ebp, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r13d
	leal	(%r13,%r13), %ecx
	cmpl	%r13d, %eax
	jge	.LBB2_1
# BB#2:
	movl	Falign.prevalloclen(%rip), %eax
	movl	176(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB2_6
# BB#3:
	testl	%eax, %eax
	je	.LBB2_5
# BB#4:
	movq	Falign.result1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign.result2(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign.tmpres1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign.tmpres2(%rip), %rdi
	callq	FreeCharMtx
.LBB2_5:
	movl	njob(%rip), %edi
	movl	176(%rsp), %ebp
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.result1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.result2(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpres1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpres2(%rip)
	movl	%ebp, Falign.prevalloclen(%rip)
.LBB2_6:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	Falign.localalloclen(%rip), %eax
	testl	%eax, %eax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jne	.LBB2_17
# BB#7:
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign.sgap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign.egap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign.sgap2(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign.egap2(%rip)
	movl	$20, %edi
	callq	AllocateIntVec
	movq	%rax, Falign.kouho(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign.cut1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign.cut2(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpptr1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpptr2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, Falign.segment(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, Falign.segment1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r14, Falign.segment2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, Falign.sortedseg1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, Falign.sortedseg2(%rip)
	testq	%rax, %rax
	je	.LBB2_12
# BB#8:
	testq	%rbx, %rbx
	je	.LBB2_12
# BB#9:
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB2_12
# BB#10:
	testq	%r14, %r14
	je	.LBB2_12
# BB#11:
	testq	%rbp, %rbp
	jne	.LBB2_13
.LBB2_12:
	movl	$.L.str, %edi
	callq	ErrorExit
.LBB2_13:
	cmpl	$-1, scoremtx(%rip)
	je	.LBB2_14
# BB#15:
	cmpl	$0, fftscore(%rip)
	movl	$1, %ecx
	movl	$20, %eax
	cmovnel	%ecx, %eax
	jmp	.LBB2_16
.LBB2_14:
	movl	$1, %eax
.LBB2_16:
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	%eax, n20or4or2(%rip)
	movl	Falign.localalloclen(%rip), %eax
.LBB2_17:
	cmpl	%r13d, %eax
	jge	.LBB2_25
# BB#18:
	testl	%eax, %eax
	je	.LBB2_22
# BB#19:
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB2_21
# BB#20:
	movq	Falign.seqVector1(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign.seqVector2(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign.naisekiNoWa(%rip), %rdi
	callq	FreeFukusosuuVec
	movq	Falign.naiseki(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign.soukan(%rip), %rdi
	callq	FreeDoubleVec
.LBB2_21:
	movq	Falign.tmpseq1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign.tmpseq2(%rip), %rdi
	callq	FreeCharMtx
.LBB2_22:
	movl	njob(%rip), %edi
	movl	%r13d, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpseq1(%rip)
	movl	njob(%rip), %edi
	movl	%r13d, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign.tmpseq2(%rip)
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB2_24
# BB#23:
	movl	%r13d, %edi
	callq	AllocateFukusosuuVec
	movq	%rax, Falign.naisekiNoWa(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%r13d, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign.naiseki(%rip)
	movl	n20or4or2(%rip), %edi
	incl	%edi
	leal	1(%r13), %ebp
	movl	%ebp, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign.seqVector1(%rip)
	movl	n20or4or2(%rip), %edi
	incl	%edi
	movl	%ebp, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign.seqVector2(%rip)
	movl	%ebp, %edi
	callq	AllocateDoubleVec
	movq	%rax, Falign.soukan(%rip)
.LBB2_24:
	movl	%r13d, Falign.localalloclen(%rip)
.LBB2_25:                               # %.preheader470
	testl	%r14d, %r14d
	jle	.LBB2_28
# BB#26:                                # %.lr.ph568.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_27:                               # %.lr.ph568
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign.tmpseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r12,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_27
.LBB2_28:                               # %.preheader469
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_31
# BB#29:                                # %.lr.ph565.preheader
	movl	32(%rsp), %ebx          # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_30:                               # %.lr.ph565
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign.tmpseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r15,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_30
.LBB2_31:                               # %._crit_edge566
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	je	.LBB2_34
# BB#32:                                # %.thread
	movl	$0, 8(%rsp)
	jmp	.LBB2_33
.LBB2_34:
	cmpl	$0, fftkeika(%rip)
	jne	.LBB2_35
.LBB2_36:                               # %.preheader468
	movslq	n20or4or2(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB2_42
# BB#37:                                # %.lr.ph562
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB2_42
# BB#38:                                # %.lr.ph562.split.preheader
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %r14d
	shlq	$4, %r14
	addq	$16, %r14
	movq	Falign.seqVector1(%rip), %r13
	leaq	-1(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	xorl	%ebp, %ebp
	andq	$7, %r15
	je	.LBB2_40
	.p2align	4, 0x90
.LBB2_39:                               # %.lr.ph562.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB2_39
.LBB2_40:                               # %.lr.ph562.split.prol.loopexit
	cmpq	$7, 48(%rsp)            # 8-byte Folded Reload
	jb	.LBB2_42
	.p2align	4, 0x90
.LBB2_41:                               # %.lr.ph562.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	8(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	16(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	24(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	32(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	40(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	48(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	56(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	addq	$8, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB2_41
.LBB2_42:                               # %._crit_edge563
	movl	fftscore(%rip), %r8d
	testl	%r8d, %r8d
	je	.LBB2_44
# BB#43:                                # %._crit_edge563
	cmpl	$-1, scoremtx(%rip)
	je	.LBB2_44
# BB#53:                                # %.preheader465
	movq	24(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	movq	40(%rsp), %r15          # 8-byte Reload
	jle	.LBB2_61
# BB#54:                                # %.lr.ph558
	movq	Falign.seqVector1(%rip), %rax
	movq	Falign.tmpseq1(%rip), %rcx
	movq	(%rax), %r9
	movl	%edx, %esi
	addq	$8, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_55:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_57 Depth 2
	movq	(%rcx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB2_60
# BB#56:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_55 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB2_57:                               # %.lr.ph.i
                                        #   Parent Loop BB2_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$20, %rax
	jg	.LBB2_59
# BB#58:                                #   in Loop: Header=BB2_57 Depth=2
	movsd	polarity(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rdx), %xmm1
	movsd	%xmm1, -8(%rdx)
	movsd	volume(,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
.LBB2_59:                               #   in Loop: Header=BB2_57 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rdx
	incq	%rbp
	testb	%al, %al
	jne	.LBB2_57
.LBB2_60:                               # %seq_vec_5.exit
                                        #   in Loop: Header=BB2_55 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB2_55
	jmp	.LBB2_61
.LBB2_44:                               # %.preheader466
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	40(%rsp), %r15          # 8-byte Reload
	jle	.LBB2_61
# BB#45:                                # %.lr.ph560
	movq	Falign.seqVector1(%rip), %rcx
	movq	Falign.tmpseq1(%rip), %r9
	movl	%eax, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_46:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_48 Depth 2
	movq	(%r9,%rdi,8), %rbp
	movb	(%rbp), %dl
	testb	%dl, %dl
	je	.LBB2_52
# BB#47:                                # %.lr.ph.i418.preheader
                                        #   in Loop: Header=BB2_46 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_48:                               # %.lr.ph.i418
                                        #   Parent Loop BB2_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%dl, %rdx
	movslq	amino_n(,%rdx,4), %rdx
	testq	%rdx, %rdx
	js	.LBB2_51
# BB#49:                                # %.lr.ph.i418
                                        #   in Loop: Header=BB2_48 Depth=2
	cmpl	%ebx, %edx
	jge	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_48 Depth=2
	movq	(%rcx,%rdx,8), %rdx
	movsd	(%rdx,%rax), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rax)
.LBB2_51:                               #   in Loop: Header=BB2_48 Depth=2
	movzbl	(%rbp), %edx
	addq	$16, %rax
	incq	%rbp
	testb	%dl, %dl
	jne	.LBB2_48
.LBB2_52:                               # %seq_vec_3.exit
                                        #   in Loop: Header=BB2_46 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB2_46
.LBB2_61:                               # %.preheader464
	testl	%ebx, %ebx
	jle	.LBB2_68
# BB#62:                                # %.lr.ph555
	testl	%r15d, %r15d
	je	.LBB2_68
# BB#63:                                # %.lr.ph555.split.preheader
	leal	-1(%r15), %r14d
	shlq	$4, %r14
	addq	$16, %r14
	movq	Falign.seqVector2(%rip), %r13
	leaq	-1(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	xorl	%ebp, %ebp
	andq	$7, %r15
	je	.LBB2_65
	.p2align	4, 0x90
.LBB2_64:                               # %.lr.ph555.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB2_64
.LBB2_65:                               # %.lr.ph555.split.prol.loopexit
	cmpq	$7, 48(%rsp)            # 8-byte Folded Reload
	jb	.LBB2_67
	.p2align	4, 0x90
.LBB2_66:                               # %.lr.ph555.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	8(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	16(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	24(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	32(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	40(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	48(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	56(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	addq	$8, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB2_66
.LBB2_67:                               # %._crit_edge556.loopexit573
	movl	fftscore(%rip), %r8d
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB2_68:                               # %._crit_edge556
	movl	%r15d, %r14d
	shrl	$31, %r14d
	testl	%r8d, %r8d
	je	.LBB2_70
# BB#69:                                # %._crit_edge556
	cmpl	$-1, scoremtx(%rip)
	je	.LBB2_70
# BB#79:                                # %.preheader461
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_87
# BB#80:                                # %.lr.ph551
	movq	Falign.seqVector2(%rip), %rax
	movq	Falign.tmpseq2(%rip), %rbp
	movq	(%rax), %r8
	movl	32(%rsp), %edx          # 4-byte Reload
	addq	$8, %r8
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_81:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_83 Depth 2
	movq	(%rbp,%rsi,8), %rdi
	movb	(%rdi), %al
	testb	%al, %al
	je	.LBB2_86
# BB#82:                                # %.lr.ph.i429.preheader
                                        #   in Loop: Header=BB2_81 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB2_83:                               # %.lr.ph.i429
                                        #   Parent Loop BB2_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$20, %rax
	jg	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_83 Depth=2
	movsd	polarity(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rcx), %xmm1
	movsd	%xmm1, -8(%rcx)
	movsd	volume(,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
.LBB2_85:                               #   in Loop: Header=BB2_83 Depth=2
	movzbl	(%rdi), %eax
	addq	$16, %rcx
	incq	%rdi
	testb	%al, %al
	jne	.LBB2_83
.LBB2_86:                               # %seq_vec_5.exit430
                                        #   in Loop: Header=BB2_81 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_81
	jmp	.LBB2_87
.LBB2_70:                               # %.preheader462
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_87
# BB#71:                                # %.lr.ph553
	movq	Falign.seqVector2(%rip), %rax
	movq	Falign.tmpseq2(%rip), %r8
	movl	32(%rsp), %edx          # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_72:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_74 Depth 2
	movq	(%r8,%rsi,8), %rdi
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB2_78
# BB#73:                                # %.lr.ph.i421.preheader
                                        #   in Loop: Header=BB2_72 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_74:                               # %.lr.ph.i421
                                        #   Parent Loop BB2_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	testq	%rcx, %rcx
	js	.LBB2_77
# BB#75:                                # %.lr.ph.i421
                                        #   in Loop: Header=BB2_74 Depth=2
	cmpl	%ebx, %ecx
	jge	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_74 Depth=2
	movq	(%rax,%rcx,8), %rcx
	movsd	(%rcx,%rbp), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx,%rbp)
.LBB2_77:                               #   in Loop: Header=BB2_74 Depth=2
	movzbl	(%rdi), %ecx
	addq	$16, %rbp
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB2_74
.LBB2_78:                               # %seq_vec_3.exit426
                                        #   in Loop: Header=BB2_72 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_72
.LBB2_87:                               # %.preheader460
	addl	%r15d, %r14d
	testl	%ebx, %ebx
	jle	.LBB2_101
# BB#88:                                # %.lr.ph549.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_89:                               # %.lr.ph549
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign.seqVector2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	xorl	%edx, %edx
	testq	%rbp, %rbp
	sete	%dl
	movl	%r15d, %edi
	callq	fft
	movq	Falign.seqVector1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	fft
	incq	%rbp
	movslq	n20or4or2(%rip), %rbx
	cmpq	%rbx, %rbp
	jl	.LBB2_89
# BB#90:                                # %.preheader459
	testl	%ebx, %ebx
	jle	.LBB2_101
# BB#91:                                # %.preheader458.lr.ph
	testl	%r15d, %r15d
	jle	.LBB2_92
# BB#96:                                # %.preheader458.us.preheader
	movl	%r15d, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_97:                               # %.preheader458.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_98 Depth 2
	movq	%r13, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_98:                               #   Parent Loop BB2_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign.naiseki(%rip), %rax
	movq	(%rax,%r15,8), %rdi
	addq	%rbp, %rdi
	movq	Falign.seqVector1(%rip), %rax
	movq	(%rax,%r15,8), %rsi
	addq	%rbp, %rsi
	movq	Falign.seqVector2(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	addq	%rbp, %rdx
	callq	calcNaiseki
	addq	$16, %rbp
	decq	%rbx
	jne	.LBB2_98
# BB#99:                                # %._crit_edge546.us
                                        #   in Loop: Header=BB2_97 Depth=1
	incq	%r15
	movslq	n20or4or2(%rip), %rbx
	cmpq	%rbx, %r15
	jl	.LBB2_97
# BB#100:
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB2_101
.LBB2_92:                               # %.preheader458.preheader
	leal	-1(%rbx), %ecx
	movl	%ebx, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB2_94
	.p2align	4, 0x90
.LBB2_93:                               # %.preheader458.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB2_93
.LBB2_94:                               # %.preheader458.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB2_101
	.p2align	4, 0x90
.LBB2_95:                               # %.preheader458
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	%ebx, %eax
	jl	.LBB2_95
.LBB2_101:                              # %.preheader457
	sarl	%r14d
	movq	Falign.naisekiNoWa(%rip), %r13
	testl	%r15d, %r15d
	jle	.LBB2_112
# BB#102:                               # %.lr.ph542
	testl	%ebx, %ebx
	jle	.LBB2_257
# BB#103:                               # %.lr.ph542.split.us.preheader
	movq	Falign.naiseki(%rip), %rax
	movl	%ebx, %r9d
	movl	40(%rsp), %r10d         # 4-byte Reload
	leaq	-1(%r9), %r11
	movl	%r9d, %edi
	andl	$3, %edi
	leaq	24(%rax), %r8
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_104:                              # %.lr.ph542.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_107 Depth 2
                                        #     Child Loop BB2_110 Depth 2
	movq	%r15, %rbx
	shlq	$4, %rbx
	leaq	(%r13,%rbx), %rdx
	testq	%rdi, %rdi
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r13,%rbx)
	je	.LBB2_105
# BB#106:                               # %.prol.preheader749
                                        #   in Loop: Header=BB2_104 Depth=1
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_107:                              #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,8), %rsi
	movupd	(%rsi,%rbx), %xmm1
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	incq	%rbp
	cmpq	%rbp, %rdi
	jne	.LBB2_107
	jmp	.LBB2_108
	.p2align	4, 0x90
.LBB2_105:                              #   in Loop: Header=BB2_104 Depth=1
	xorl	%ebp, %ebp
.LBB2_108:                              # %.prol.loopexit750
                                        #   in Loop: Header=BB2_104 Depth=1
	cmpq	$3, %r11
	jb	.LBB2_111
# BB#109:                               # %.lr.ph542.split.us.new
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	%r9, %rsi
	subq	%rbp, %rsi
	leaq	(%r8,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB2_110:                              #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	-16(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	movq	-8(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	addq	$32, %rbp
	addq	$-4, %rsi
	jne	.LBB2_110
.LBB2_111:                              # %._crit_edge540.us
                                        #   in Loop: Header=BB2_104 Depth=1
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB2_104
.LBB2_112:                              # %._crit_edge543
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	fft
	cmpl	$-1, %r15d
	jge	.LBB2_113
	jmp	.LBB2_115
.LBB2_257:                              # %._crit_edge543.thread
	movq	40(%rsp), %r15          # 8-byte Reload
	leal	-1(%r15), %edx
	shlq	$4, %rdx
	addq	$16, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	movl	%r15d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	fft
.LBB2_113:                              # %.lr.ph536
	movq	Falign.soukan(%rip), %rax
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	addq	Falign.naisekiNoWa(%rip), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB2_114:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdi
	movq	%rdi, 8(%rax,%rsi,8)
	incq	%rsi
	addq	$-16, %rdx
	cmpq	%rsi, %rcx
	jg	.LBB2_114
.LBB2_115:                              # %.preheader456
	leal	1(%r14), %ecx
	cmpl	%r15d, %ecx
	jge	.LBB2_116
# BB#117:                               # %.lr.ph532
	movq	Falign.naisekiNoWa(%rip), %rax
	movq	Falign.soukan(%rip), %rdx
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	subl	%r14d, %edi
	leal	-2(%r15), %esi
	subl	%r14d, %esi
	andl	$3, %edi
	je	.LBB2_120
# BB#118:                               # %.prol.preheader744
	leal	-1(%r15), %ebx
	negl	%edi
	.p2align	4, 0x90
.LBB2_119:                              # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movq	%rbx, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rdx,%rcx,8)
	incq	%rcx
	decl	%ebx
	incl	%edi
	jne	.LBB2_119
.LBB2_120:                              # %.prol.loopexit745
	cmpl	$3, %esi
	jb	.LBB2_123
# BB#121:                               # %.lr.ph532.new
	leaq	24(%rdx,%rcx,8), %rsi
	movq	%rcx, %rdi
	addq	$3, %rdi
	movl	%r14d, %r8d
	subl	%edi, %r8d
	leal	2(%rcx), %edi
	movl	%r14d, %r9d
	subl	%edi, %r9d
	movl	%r14d, %ebx
	subl	%ecx, %ebx
	leal	1(%rcx), %edi
	subl	%edi, %r14d
	movl	%r15d, %edi
	.p2align	4, 0x90
.LBB2_122:                              # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -24(%rsi)
	leal	(%r14,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -16(%rsi)
	leal	(%r9,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -8(%rsi)
	leal	(%r8,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rsi)
	addl	$-4, %edi
	addq	$32, %rsi
	cmpl	%edi, %ecx
	jne	.LBB2_122
	jmp	.LBB2_123
.LBB2_116:                              # %.preheader456.._crit_edge533_crit_edge
	movq	Falign.soukan(%rip), %rdx
.LBB2_123:                              # %._crit_edge533
	movq	Falign.kouho(%rip), %rdi
	movl	$20, %esi
	movl	%r15d, %ecx
	callq	getKouho
	cmpl	$0, kobetsubunkatsu(%rip)
	movl	$0, 8(%rsp)
	je	.LBB2_124
.LBB2_33:
	movq	Falign.kouho(%rip), %rax
	movl	$0, (%rax)
	movl	$1, %r12d
.LBB2_125:
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r9d
	negl	%r9d
	xorl	%r13d, %r13d
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB2_126:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_133 Depth 2
	movq	Falign.kouho(%rip), %rax
	movl	(%rax,%r13,4), %r15d
	cmpl	%r9d, %r15d
	jle	.LBB2_137
# BB#127:                               #   in Loop: Header=BB2_126 Depth=1
	cmpl	%r15d, 16(%rsp)         # 4-byte Folded Reload
	jle	.LBB2_137
# BB#128:                               #   in Loop: Header=BB2_126 Depth=1
	movq	Falign.tmpptr1(%rip), %r9
	subq	$8, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	movl	%r15d, %edi
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %esi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	pushq	Falign.tmpptr2(%rip)
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	callq	zurasu2
	addq	$16, %rsp
.Lcfi84:
	.cfi_adjust_cfa_offset -16
	movq	Falign.tmpptr1(%rip), %rdx
	movq	Falign.tmpptr2(%rip), %rcx
	movslq	8(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rax
	shlq	$4, %rax
	addq	Falign.segment(%rip), %rax
	subq	$8, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	callq	alignableReagion
	addq	$16, %rsp
.Lcfi87:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r14d
	leal	(%rbx,%r14), %eax
	cmpl	$99998, %eax            # imm = 0x1869E
	jl	.LBB2_130
# BB#129:                               #   in Loop: Header=BB2_126 Depth=1
	movl	$.L.str.2, %edi
	callq	ErrorExit
.LBB2_130:                              #   in Loop: Header=BB2_126 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_139
# BB#131:                               # %.preheader454
                                        #   in Loop: Header=BB2_126 Depth=1
	movl	48(%rsp), %r9d          # 4-byte Reload
	jle	.LBB2_137
# BB#132:                               # %.lr.ph528
                                        #   in Loop: Header=BB2_126 Depth=1
	movq	Falign.segment(%rip), %rax
	movq	Falign.segment1(%rip), %rcx
	movq	Falign.segment2(%rip), %rdx
	incl	%r14d
	.p2align	4, 0x90
.LBB2_133:                              #   Parent Loop BB2_126 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$4, %rsi
	movl	(%rax,%rsi), %edi
	testl	%r15d, %r15d
	jle	.LBB2_135
# BB#134:                               #   in Loop: Header=BB2_133 Depth=2
	movl	%edi, (%rcx,%rsi)
	movslq	8(%rsp), %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	4(%rax,%rsi), %edi
	movl	%edi, 4(%rcx,%rsi)
	movl	8(%rax,%rsi), %edi
	movl	%edi, 8(%rcx,%rsi)
	movq	16(%rax,%rsi), %rdi
	movq	%rdi, 16(%rcx,%rsi)
	movl	(%rax,%rsi), %edi
	addl	%r15d, %edi
	movl	%edi, (%rdx,%rsi)
	movl	8(%rsp), %esi
	movslq	%esi, %rbp
	movq	%rbp, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	addl	%r15d, %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
	addl	%r15d, %ebx
	jmp	.LBB2_136
	.p2align	4, 0x90
.LBB2_135:                              #   in Loop: Header=BB2_133 Depth=2
	subl	%r15d, %edi
	movl	%edi, (%rcx,%rsi)
	movslq	8(%rsp), %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	4(%rax,%rsi), %edi
	subl	%r15d, %edi
	movl	%edi, 4(%rcx,%rsi)
	movl	8(%rax,%rsi), %edi
	subl	%r15d, %edi
	movl	%edi, 8(%rcx,%rsi)
	movq	16(%rax,%rsi), %rdi
	movq	%rdi, 16(%rcx,%rsi)
	movl	(%rax,%rsi), %edi
	movl	%edi, (%rdx,%rsi)
	movl	8(%rsp), %esi
	movslq	%esi, %rbp
	movq	%rbp, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
.LBB2_136:                              #   in Loop: Header=BB2_133 Depth=2
	leaq	(%rbp,%rbp,2), %rdi
	shlq	$4, %rdi
	movl	%ebx, 8(%rdx,%rdi)
	movslq	%esi, %rbx
	movq	%rbx, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movq	16(%rax,%rsi), %rdi
	leaq	(%rdx,%rsi), %rbp
	movq	%rdi, 16(%rdx,%rsi)
	leaq	(%rcx,%rsi), %rdi
	movq	%rbp, 32(%rcx,%rsi)
	movq	%rdi, 32(%rdx,%rsi)
	incl	%ebx
	movl	%ebx, 8(%rsp)
	decl	%r14d
	cmpl	$1, %r14d
	jg	.LBB2_133
.LBB2_137:                              # %.loopexit455
                                        #   in Loop: Header=BB2_126 Depth=1
	incq	%r13
	cmpq	%r12, %r13
	jl	.LBB2_126
# BB#138:                               # %.loopexit455._crit_edge
	movl	8(%rsp), %ebx
.LBB2_139:                              # %.loopexit717
	testl	%ebx, %ebx
	movq	72(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_142
# BB#140:                               # %.loopexit717
	movl	fftNoAnchStop(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_142
# BB#141:                               # %.preheader453.thread
	movl	$.L.str.4, %edi
	callq	ErrorExit
	jmp	.LBB2_143
.LBB2_142:                              # %.preheader453
	testl	%ebx, %ebx
	setg	%r14b
	jle	.LBB2_143
# BB#144:                               # %.lr.ph525
	movq	Falign.segment1(%rip), %rax
	movq	Falign.sortedseg1(%rip), %rdx
	movq	Falign.segment2(%rip), %rcx
	movq	Falign.sortedseg2(%rip), %r9
	movslq	%ebx, %r8
	cmpl	$4, %ebx
	jae	.LBB2_146
# BB#145:
	xorl	%ebp, %ebp
	jmp	.LBB2_154
.LBB2_143:                              # %.preheader453.._crit_edge526_crit_edge
	movq	Falign.sortedseg1(%rip), %rdx
	xorl	%r14d, %r14d
	jmp	.LBB2_156
.LBB2_146:                              # %min.iters.checked
	movq	%r8, %rbp
	andq	$-4, %rbp
	je	.LBB2_147
# BB#148:                               # %vector.memcheck
	leaq	-8(%r9,%r8,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB2_151
# BB#149:                               # %vector.memcheck
	leaq	-8(%rdx,%r8,8), %rsi
	cmpq	%rsi, %r9
	jae	.LBB2_151
# BB#150:
	xorl	%ebp, %ebp
	jmp	.LBB2_154
.LBB2_124:
	movl	$20, %r12d
	jmp	.LBB2_125
.LBB2_147:
	xorl	%ebp, %ebp
	jmp	.LBB2_154
.LBB2_151:                              # %vector.body.preheader
	xorl	%edi, %edi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_152:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rdi), %r10
	leaq	48(%rax,%rdi), %r11
	leaq	96(%rax,%rdi), %r15
	leaq	144(%rax,%rdi), %r13
	movd	%r11, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r13, %xmm0
	movd	%r15, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%rdx,%rsi,8)
	movdqu	%xmm2, 16(%rdx,%rsi,8)
	leaq	(%rcx,%rdi), %r10
	leaq	48(%rcx,%rdi), %r11
	leaq	96(%rcx,%rdi), %r15
	leaq	144(%rcx,%rdi), %r13
	movd	%r11, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r13, %xmm0
	movd	%r15, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%r9,%rsi,8)
	movdqu	%xmm2, 16(%r9,%rsi,8)
	addq	$4, %rsi
	addq	$192, %rdi
	cmpq	%rsi, %rbp
	jne	.LBB2_152
# BB#153:                               # %middle.block
	cmpq	%rbp, %r8
	movq	72(%rsp), %r15          # 8-byte Reload
	je	.LBB2_156
.LBB2_154:                              # %scalar.ph.preheader
	movq	%rbp, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	addq	%rsi, %rax
	addq	%rsi, %rcx
	.p2align	4, 0x90
.LBB2_155:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rdx,%rbp,8)
	movq	%rcx, (%r9,%rbp,8)
	incq	%rbp
	addq	$48, %rax
	addq	$48, %rcx
	cmpq	%r8, %rbp
	jl	.LBB2_155
.LBB2_156:                              # %._crit_edge526
	leal	-1(%rbx), %ebp
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	movq	Falign.sortedseg2(%rip), %rdx
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	testb	%r14b, %r14b
	je	.LBB2_162
# BB#157:                               # %.lr.ph523
	movq	Falign.sortedseg1(%rip), %rcx
	movslq	%ebx, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_158:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_158
# BB#159:                               # %.preheader452
	testb	%r14b, %r14b
	je	.LBB2_162
# BB#160:                               # %.lr.ph518
	movq	Falign.sortedseg2(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_161:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_161
.LBB2_162:                              # %._crit_edge519
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB2_182
# BB#163:                               # %.preheader451
	testb	%r14b, %r14b
	je	.LBB2_164
# BB#165:                               # %.lr.ph515
	movq	Falign.sortedseg1(%rip), %rdx
	movq	Falign.cut1(%rip), %rax
	movq	Falign.sortedseg2(%rip), %rsi
	movq	Falign.cut2(%rip), %rcx
	xorl	%edi, %edi
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_166:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%rax,%rdi,4)
	movq	(%rsi,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%rcx,%rdi,4)
	incq	%rdi
	movslq	8(%rsp), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB2_166
	jmp	.LBB2_167
.LBB2_182:
	leal	2(%rbx), %r14d
	cmpl	%r14d, Falign.crossscoresize(%rip)
	jge	.LBB2_188
# BB#183:
	movl	%r14d, Falign.crossscoresize(%rip)
	cmpl	$0, fftkeika(%rip)
	jne	.LBB2_184
.LBB2_185:
	movq	Falign.crossscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_187
# BB#186:
	callq	FreeDoubleMtx
.LBB2_187:
	movl	Falign.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, Falign.crossscore(%rip)
.LBB2_188:                              # %.preheader450
	cmpl	$-1, %ebx
	jl	.LBB2_192
# BB#189:                               # %.preheader449.us.preheader
	movq	Falign.crossscore(%rip), %r15
	incl	%ebx
	leaq	8(,%rbx,8), %r13
	movslq	%r14d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_190:                              # %.preheader449.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r13, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB2_190
# BB#191:                               # %.preheader448
	cmpl	$0, 8(%rsp)
	movq	72(%rsp), %r15          # 8-byte Reload
	jle	.LBB2_192
# BB#193:                               # %.lr.ph508
	movq	Falign.segment1(%rip), %rax
	movq	Falign.crossscore(%rip), %r8
	movq	Falign.sortedseg1(%rip), %rdx
	movq	Falign.cut1(%rip), %rdi
	movq	Falign.sortedseg2(%rip), %rcx
	movq	Falign.cut2(%rip), %rsi
	addq	$40, %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_194:                              # =>This Inner Loop Header: Depth=1
	movslq	(%rax), %rbx
	movq	8(%r8,%rbx,8), %r9
	movq	-24(%rax), %r10
	movq	-8(%rax), %rbx
	movslq	40(%rbx), %rbx
	movq	%r10, 8(%r9,%rbx,8)
	movq	(%rdx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rdi,%rbp,4)
	movq	(%rcx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rsi,%rbp,4)
	incq	%rbp
	movslq	8(%rsp), %rbx
	addq	$48, %rax
	cmpq	%rbx, %rbp
	jl	.LBB2_194
	jmp	.LBB2_195
.LBB2_164:                              # %.preheader451.._crit_edge516_crit_edge
	movq	Falign.cut1(%rip), %rax
	movq	Falign.cut2(%rip), %rcx
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB2_167:                              # %._crit_edge516
	movl	$0, (%rax)
	movl	$0, (%rcx)
	movslq	8(%rsp), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 4(%rax,%rdx,4)
	movslq	8(%rsp), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, 4(%rcx,%rax,4)
	addl	$2, 8(%rsp)
	jmp	.LBB2_168
.LBB2_192:                              # %.preheader448.._crit_edge509_crit_edge
	movq	Falign.crossscore(%rip), %r8
	movq	Falign.cut1(%rip), %rdi
	movq	Falign.cut2(%rip), %rsi
	movq	Falign.sortedseg1(%rip), %rdx
	movq	Falign.sortedseg2(%rip), %rcx
.LBB2_195:
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	(%r8), %rax
	movabsq	$4711630319722168320, %rbp # imm = 0x416312D000000000
	movq	%rbp, (%rax)
	movl	$0, (%rdi)
	movl	$0, (%rsi)
	movslq	8(%rsp), %rax
	movq	8(%r8,%rax,8), %rbx
	movq	%rbp, 8(%rbx,%rax,8)
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rdi,%rax,4)
	movslq	8(%rsp), %rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rsi,%rax,4)
	movl	8(%rsp), %ebx
	addl	$2, %ebx
	movl	%ebx, 8(%rsp)
	leaq	8(%rsp), %r9
	callq	blockAlign2
	movl	fftkeika(%rip), %eax
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB2_198
# BB#196:                               # %._crit_edge509
	testl	%eax, %eax
	jne	.LBB2_197
.LBB2_198:
	testl	%eax, %eax
	je	.LBB2_168
.LBB2_199:
	cmpl	8(%rsp), %ebx
	jle	.LBB2_168
# BB#200:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, fftRepeatStop(%rip)
	jne	.LBB2_201
.LBB2_168:                              # %.preheader447
	testl	%r14d, %r14d
	movq	32(%rsp), %rbp          # 8-byte Reload
	jle	.LBB2_174
# BB#169:                               # %.lr.ph506
	movq	%rbp, %rbx
	movq	Falign.result1(%rip), %rcx
	movl	%r14d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB2_171
	.p2align	4, 0x90
.LBB2_170:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB2_170
.LBB2_171:                              # %.prol.loopexit740
	cmpq	$7, %rdx
	movq	%rbx, %rbp
	jb	.LBB2_174
# BB#172:                               # %.lr.ph506.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB2_173:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB2_173
.LBB2_174:                              # %.preheader446
	testl	%ebp, %ebp
	jle	.LBB2_180
# BB#175:                               # %.lr.ph503
	movq	Falign.result2(%rip), %rcx
	movq	%rbp, %rbx
	movl	%ebp, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB2_177
	.p2align	4, 0x90
.LBB2_176:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB2_176
.LBB2_177:                              # %.prol.loopexit
	cmpq	$7, %rdx
	movq	%rbx, %rbp
	jb	.LBB2_180
# BB#178:                               # %.lr.ph503.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB2_179:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB2_179
.LBB2_180:                              # %._crit_edge504
	movq	184(%rsp), %rcx
	movl	$-1, (%rcx)
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	cmpl	$2, 8(%rsp)
	jl	.LBB2_204
# BB#181:                               # %.lr.ph501
	cmpl	$1, %r14d
	sete	%al
	cmpl	$1, %ebp
	sete	%dl
	andb	%al, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leal	-1(%r14), %eax
	incq	%rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leal	-1(%rbp), %eax
	incq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r14d, %r15d
	movl	%ebp, %r14d
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	$-1, %eax
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	jmp	.LBB2_212
	.p2align	4, 0x90
.LBB2_211:                              # %.loopexit._crit_edge
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	184(%rsp), %rcx
	movl	(%rcx), %eax
	movl	%r13d, %edx
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB2_212:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_225 Depth 2
                                        #     Child Loop BB2_231 Depth 2
                                        #     Child Loop BB2_253 Depth 2
                                        #     Child Loop BB2_256 Depth 2
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	incl	%eax
	movl	%eax, (%rcx)
	movq	Falign.cut1(%rip), %r13
	cmpl	$0, (%r13,%r12,4)
	je	.LBB2_213
# BB#258:                               #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.sgap1(%rip), %rdi
	movq	Falign.tmpres1(%rip), %rsi
	movq	40(%rsp), %rbx          # 8-byte Reload
	decl	%ebx
	movl	%ebx, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
	movq	Falign.sgap2(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	getkyokaigap
	movq	Falign.cut1(%rip), %r13
	jmp	.LBB2_217
	.p2align	4, 0x90
.LBB2_213:                              # %.preheader445
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_215
# BB#214:                               # %.lr.ph479
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.sgap1(%rip), %rdi
	movl	$111, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	memset
.LBB2_215:                              # %.preheader443
                                        #   in Loop: Header=BB2_212 Depth=1
	testl	%ebp, %ebp
	jle	.LBB2_217
# BB#216:                               # %.lr.ph481
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.sgap2(%rip), %rdi
	movl	$111, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	memset
.LBB2_217:                              # %.loopexit444
                                        #   in Loop: Header=BB2_212 Depth=1
	movl	4(%r13,%r12,4), %edx
	cmpl	88(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB2_222
# BB#218:                               # %.preheader442
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jle	.LBB2_220
# BB#219:                               # %.lr.ph483
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.egap1(%rip), %rdi
	movl	$111, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	memset
.LBB2_220:                              # %.preheader441
                                        #   in Loop: Header=BB2_212 Depth=1
	testl	%ebp, %ebp
	jle	.LBB2_223
# BB#221:                               # %.lr.ph485
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.egap2(%rip), %rdi
	movl	$111, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	memset
	jmp	.LBB2_223
	.p2align	4, 0x90
.LBB2_222:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.egap1(%rip), %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
	movq	Falign.egap2(%rip), %rdi
	movq	Falign.cut2(%rip), %rax
	movl	4(%rax,%r12,4), %edx
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rsi
	movl	%ebp, %ecx
	callq	getkyokaigap
.LBB2_223:                              # %.preheader440
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	jle	.LBB2_226
# BB#224:                               # %.lr.ph487.preheader
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rcx
	movq	Falign.cut1(%rip), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_225:                              # %.lr.ph487
                                        #   Parent Loop BB2_212 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbx,8), %rdi
	movslq	(%rax,%r12,4), %rcx
	movq	(%rbp,%rbx,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r12,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Falign.tmpres1(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdx
	movq	Falign.cut1(%rip), %rax
	movslq	4(%rax,%r12,4), %rsi
	movslq	(%rax,%r12,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB2_225
.LBB2_226:                              # %._crit_edge488
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB2_229
# BB#227:                               # %._crit_edge488
                                        #   in Loop: Header=BB2_212 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_229
# BB#228:                               #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	commongappick
.LBB2_229:                              # %.preheader439
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_232
# BB#230:                               # %.lr.ph490.preheader
                                        #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres2(%rip), %rcx
	movq	Falign.cut2(%rip), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_231:                              # %.lr.ph490
                                        #   Parent Loop BB2_212 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbx,8), %rdi
	movslq	(%rax,%r12,4), %rcx
	movq	(%r13,%rbx,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r12,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Falign.tmpres2(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdx
	movq	Falign.cut2(%rip), %rax
	movslq	4(%rax,%r12,4), %rsi
	movslq	(%rax,%r12,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB2_231
.LBB2_232:                              # %._crit_edge491
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB2_235
# BB#233:                               # %._crit_edge491
                                        #   in Loop: Header=BB2_212 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_235
# BB#234:                               #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres2(%rip), %rsi
	movl	%ebx, %edi
	callq	commongappick
.LBB2_235:                              #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, constraint(%rip)
	jne	.LBB2_259
# BB#236:                               #   in Loop: Header=BB2_212 Depth=1
	movsbl	alg(%rip), %edx
	leal	-65(%rdx), %eax
	cmpl	$32, %eax
	ja	.LBB2_248
# BB#237:                               #   in Loop: Header=BB2_212 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_240:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_241
# BB#243:                               #   in Loop: Header=BB2_212 Depth=1
	subq	$8, %rsp
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	pushq	Falign.egap2(%rip)
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.egap1(%rip)
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap2(%rip)
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap1(%rip)
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	leaq	124(%rsp), %rax
	pushq	%rax
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	movl	232(%rsp), %ebx
	pushq	%rbx
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	callq	A__align
	addq	$64, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_242
.LBB2_239:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	pushq	Falign.egap2(%rip)
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.egap1(%rip)
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap2(%rip)
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap1(%rip)
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	movl	216(%rsp), %ebx
	pushq	%rbx
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm
	addq	$48, %rsp
.Lcfi103:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB2_242
.LBB2_246:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_241
# BB#247:                               #   in Loop: Header=BB2_212 Depth=1
	subq	$8, %rsp
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	pushq	Falign.egap2(%rip)
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.egap1(%rip)
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap2(%rip)
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap1(%rip)
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	leaq	124(%rsp), %rax
	pushq	%rax
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	movl	232(%rsp), %ebx
	pushq	%rbx
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	callq	Q__align
	addq	$64, %rsp
.Lcfi112:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_242
.LBB2_244:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB2_245
.LBB2_241:                              #   in Loop: Header=BB2_212 Depth=1
	movl	176(%rsp), %ebx
	movl	%ebx, %edx
	callq	G__align11
	jmp	.LBB2_242
.LBB2_238:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rdi
	movq	Falign.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	xorl	%eax, %eax
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	movl	184(%rsp), %ebx
	pushq	%rbx
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	callq	Aalign
	addq	$16, %rsp
.Lcfi115:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_242
.LBB2_245:                              #   in Loop: Header=BB2_212 Depth=1
	subq	$8, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	pushq	Falign.egap2(%rip)
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.egap1(%rip)
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap2(%rip)
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	Falign.sgap1(%rip)
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	leaq	124(%rsp), %rax
	pushq	%rax
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	movl	232(%rsp), %ebx
	pushq	%rbx
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	callq	H__align
	addq	$64, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset -64
	.p2align	4, 0x90
.LBB2_242:                              #   in Loop: Header=BB2_212 Depth=1
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
.LBB2_249:                              #   in Loop: Header=BB2_212 Depth=1
	movq	Falign.tmpres1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %r13d
	cmpl	%ebx, %r13d
	jg	.LBB2_250
# BB#251:                               # %.preheader438
                                        #   in Loop: Header=BB2_212 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_254
.LBB2_252:                              # %.lr.ph493.preheader
                                        #   in Loop: Header=BB2_212 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_253:                              # %.lr.ph493
                                        #   Parent Loop BB2_212 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign.result1(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	Falign.tmpres1(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	callq	strcat
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB2_253
.LBB2_254:                              # %.preheader437
                                        #   in Loop: Header=BB2_212 Depth=1
	incq	%r12
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_202
# BB#255:                               # %.lr.ph495.preheader
                                        #   in Loop: Header=BB2_212 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_256:                              # %.lr.ph495
                                        #   Parent Loop BB2_212 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign.result2(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	Falign.tmpres2(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	callq	strcat
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB2_256
.LBB2_202:                              # %.loopexit
                                        #   in Loop: Header=BB2_212 Depth=1
	movslq	8(%rsp), %rax
	decq	%rax
	cmpq	%rax, %r12
	jl	.LBB2_211
	jmp	.LBB2_203
.LBB2_248:                              #   in Loop: Header=BB2_212 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$.L.str.8, %edi
	callq	ErrorExit
	movl	176(%rsp), %ebx
	jmp	.LBB2_249
.LBB2_250:                              #   in Loop: Header=BB2_212 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ebx, %r8d
	callq	fprintf
	movl	$.L.str.9, %edi
	callq	ErrorExit
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jg	.LBB2_252
	jmp	.LBB2_254
.LBB2_203:
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB2_204:                              # %.preheader436
	testl	%r14d, %r14d
	movq	96(%rsp), %r12          # 8-byte Reload
	jle	.LBB2_207
# BB#205:                               # %.lr.ph477.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_206:                              # %.lr.ph477
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	Falign.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_206
.LBB2_207:                              # %.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB2_210
# BB#208:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_209:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	Falign.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_209
.LBB2_210:                              # %._crit_edge
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_35:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB2_36
.LBB2_197:
	movq	stderr(%rip), %rdi
	movl	8(%rsp), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	jne	.LBB2_199
	jmp	.LBB2_168
.LBB2_184:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	jmp	.LBB2_185
.LBB2_259:
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB2_201:
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	Falign, .Lfunc_end2-Falign
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_240
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_244
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_239
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_246
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_248
	.quad	.LBB2_238

	.text
	.globl	Falign_noudp
	.p2align	4, 0x90
	.type	Falign_noudp,@function
Falign_noudp:                           # @Falign_noudp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 176
.Lcfi132:
	.cfi_offset %rbx, -56
.Lcfi133:
	.cfi_offset %r12, -48
.Lcfi134:
	.cfi_offset %r13, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%r8d, %r15d
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	176(%rsp), %ebp
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r14), %rdi
	callq	strlen
	cmpl	%eax, %ebx
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	cmovgel	%ebx, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	cmpl	%edx, %eax
	jge	.LBB3_1
# BB#2:
	movl	Falign_noudp.prevalloclen(%rip), %eax
	cmpl	%ebp, %eax
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	je	.LBB3_6
# BB#3:
	testl	%eax, %eax
	je	.LBB3_5
# BB#4:
	movq	Falign_noudp.result1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_noudp.result2(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_noudp.tmpres1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_noudp.tmpres2(%rip), %rdi
	callq	FreeCharMtx
.LBB3_5:
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.result1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.result2(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpres1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpres2(%rip)
	movl	%ebp, Falign_noudp.prevalloclen(%rip)
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB3_6:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	Falign_noudp.localalloclen(%rip), %eax
	testl	%eax, %eax
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jne	.LBB3_17
# BB#7:
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_noudp.sgap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_noudp.egap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_noudp.sgap2(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_noudp.egap2(%rip)
	movl	$100, %edi
	callq	AllocateIntVec
	movq	%rax, Falign_noudp.kouho(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign_noudp.cut1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign_noudp.cut2(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpptr1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpptr2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, Falign_noudp.segment(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, Falign_noudp.segment1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, Falign_noudp.segment2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, Falign_noudp.sortedseg1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, Falign_noudp.sortedseg2(%rip)
	testq	%rax, %rax
	je	.LBB3_12
# BB#8:
	testq	%r13, %r13
	je	.LBB3_12
# BB#9:
	testq	%rbp, %rbp
	je	.LBB3_12
# BB#10:
	testq	%rbx, %rbx
	je	.LBB3_12
# BB#11:
	testq	%r12, %r12
	jne	.LBB3_13
.LBB3_12:
	movl	$.L.str, %edi
	callq	ErrorExit
.LBB3_13:
	cmpl	$-1, scoremtx(%rip)
	je	.LBB3_15
# BB#14:
	cmpl	$0, fftscore(%rip)
	movl	$1, %ecx
	movl	$20, %eax
	cmovnel	%ecx, %eax
	jmp	.LBB3_16
.LBB3_15:
	movl	$1, %eax
.LBB3_16:
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%eax, n20or4or2(%rip)
	movl	Falign_noudp.localalloclen(%rip), %eax
.LBB3_17:
	cmpl	%edx, %eax
	jge	.LBB3_25
# BB#18:
	testl	%eax, %eax
	je	.LBB3_22
# BB#19:
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB3_21
# BB#20:
	movq	Falign_noudp.seqVector1(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_noudp.seqVector2(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_noudp.naisekiNoWa(%rip), %rdi
	callq	FreeFukusosuuVec
	movq	Falign_noudp.naiseki(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_noudp.soukan(%rip), %rdi
	callq	FreeDoubleVec
.LBB3_21:
	movq	Falign_noudp.tmpseq1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_noudp.tmpseq2(%rip), %rdi
	callq	FreeCharMtx
.LBB3_22:
	movl	njob(%rip), %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpseq1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_noudp.tmpseq2(%rip)
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB3_24
# BB#23:
	movl	%ebp, %edi
	callq	AllocateFukusosuuVec
	movq	%rax, Falign_noudp.naisekiNoWa(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_noudp.naiseki(%rip)
	movl	n20or4or2(%rip), %edi
	leal	1(%rbp), %ebx
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_noudp.seqVector1(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_noudp.seqVector2(%rip)
	movl	%ebx, %edi
	callq	AllocateDoubleVec
	movq	%rax, Falign_noudp.soukan(%rip)
.LBB3_24:
	movl	%ebp, Falign_noudp.localalloclen(%rip)
.LBB3_25:                               # %.preheader513
	testl	%r15d, %r15d
	jle	.LBB3_28
# BB#26:                                # %.lr.ph624.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph624
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_noudp.tmpseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r12,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_27
.LBB3_28:                               # %.preheader512
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB3_31
# BB#29:                                # %.lr.ph621.preheader
	movl	(%rsp), %ebx            # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph621
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_noudp.tmpseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r14,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_30
.LBB3_31:                               # %._crit_edge622
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	je	.LBB3_51
.LBB3_32:                               # %.thread789
	movq	Falign_noudp.kouho(%rip), %rax
	movl	$0, (%rax)
	movl	$1, %eax
.LBB3_33:                               # %.lr.ph576
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r13d
	negl	%r13d
	movslq	%eax, %r10
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r10, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_44 Depth 2
                                        #     Child Loop BB3_42 Depth 2
	movq	Falign_noudp.kouho(%rip), %rax
	movl	(%rax,%r14,4), %r15d
	cmpl	%r13d, %r15d
	jle	.LBB3_46
# BB#35:                                #   in Loop: Header=BB3_34 Depth=1
	cmpl	%r15d, 32(%rsp)         # 4-byte Folded Reload
	jle	.LBB3_46
# BB#36:                                #   in Loop: Header=BB3_34 Depth=1
	movq	Falign_noudp.tmpptr1(%rip), %r9
	subq	$8, %rsp
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	movl	%r15d, %edi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, %edx
	movq	%r12, %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
	pushq	Falign_noudp.tmpptr2(%rip)
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	callq	zurasu2
	addq	$16, %rsp
.Lcfi140:
	.cfi_adjust_cfa_offset -16
	movq	Falign_noudp.tmpptr1(%rip), %rdx
	movq	Falign_noudp.tmpptr2(%rip), %rcx
	movslq	24(%rsp), %r12          # 4-byte Folded Reload
	leaq	(%r12,%r12,2), %rax
	shlq	$4, %rax
	addq	Falign_noudp.segment(%rip), %rax
	subq	$8, %rsp
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	movl	%ebx, %edi
	movl	%ebp, %esi
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	callq	alignableReagion
	addq	$16, %rsp
.Lcfi143:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
	leal	(%r12,%rbx), %r11d
	cmpl	$99998, %r11d           # imm = 0x1869E
	jl	.LBB3_38
# BB#37:                                #   in Loop: Header=BB3_34 Depth=1
	movl	$.L.str.2, %edi
	movl	%r11d, %ebp
	callq	ErrorExit
	movl	%ebp, %r11d
.LBB3_38:                               #   in Loop: Header=BB3_34 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_47
# BB#39:                                # %.preheader494
                                        #   in Loop: Header=BB3_34 Depth=1
	movq	72(%rsp), %r10          # 8-byte Reload
	jle	.LBB3_46
# BB#40:                                # %.lr.ph571
                                        #   in Loop: Header=BB3_34 Depth=1
	testl	%r15d, %r15d
	movq	Falign_noudp.segment(%rip), %rax
	movq	Falign_noudp.segment1(%rip), %rcx
	movq	Falign_noudp.segment2(%rip), %rdx
	jle	.LBB3_43
# BB#41:                                # %.lr.ph571.split.us.preheader
                                        #   in Loop: Header=BB3_34 Depth=1
	shlq	$4, %r12
	leaq	(%r12,%r12,2), %rsi
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_42:                               # %.lr.ph571.split.us
                                        #   Parent Loop BB3_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-16(%rsi,%rax), %edi
	leaq	(%rsi,%rcx), %r8
	movl	%edi, (%rsi,%rcx)
	movl	-12(%rsi,%rax), %edi
	movl	%edi, 4(%rsi,%rcx)
	movl	-8(%rsi,%rax), %edi
	movl	%edi, 8(%rsi,%rcx)
	movq	(%rsi,%rax), %rdi
	movq	%rdi, 16(%rsi,%rcx)
	movl	-16(%rsi,%rax), %edi
	addl	%r15d, %edi
	leaq	(%rsi,%rdx), %rbp
	movl	%edi, (%rsi,%rdx)
	movl	-12(%rsi,%rax), %edi
	addl	%r15d, %edi
	movl	%edi, 4(%rsi,%rdx)
	movl	-8(%rsi,%rax), %edi
	addl	%r15d, %edi
	movl	%edi, 8(%rsi,%rdx)
	movq	(%rsi,%rax), %rdi
	movq	%rdi, 16(%rsi,%rdx)
	movq	%rbp, 32(%rsi,%rcx)
	movq	%r8, 32(%rsi,%rdx)
	addq	$48, %rax
	addq	$48, %rcx
	addq	$48, %rdx
	decl	%ebx
	jg	.LBB3_42
	jmp	.LBB3_45
.LBB3_43:                               # %.lr.ph571.split.preheader
                                        #   in Loop: Header=BB3_34 Depth=1
	shlq	$4, %r12
	leaq	(%r12,%r12,2), %rsi
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_44:                               # %.lr.ph571.split
                                        #   Parent Loop BB3_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-16(%rsi,%rax), %edi
	subl	%r15d, %edi
	leaq	(%rsi,%rcx), %r8
	movl	%edi, (%rsi,%rcx)
	movl	-12(%rsi,%rax), %edi
	subl	%r15d, %edi
	movl	%edi, 4(%rsi,%rcx)
	movl	-8(%rsi,%rax), %edi
	subl	%r15d, %edi
	movl	%edi, 8(%rsi,%rcx)
	movq	(%rsi,%rax), %rdi
	movq	%rdi, 16(%rsi,%rcx)
	movl	-16(%rsi,%rax), %edi
	leaq	(%rsi,%rdx), %rbp
	movl	%edi, (%rsi,%rdx)
	movl	-12(%rsi,%rax), %edi
	movl	%edi, 4(%rsi,%rdx)
	movl	-8(%rsi,%rax), %edi
	movl	%edi, 8(%rsi,%rdx)
	movq	(%rsi,%rax), %rdi
	movq	%rdi, 16(%rsi,%rdx)
	movq	%rbp, 32(%rsi,%rcx)
	movq	%r8, 32(%rsi,%rdx)
	addq	$48, %rdx
	addq	$48, %rcx
	addq	$48, %rax
	decl	%ebx
	jg	.LBB3_44
.LBB3_45:                               # %.loopexit495.loopexit625
                                        #   in Loop: Header=BB3_34 Depth=1
	movl	%r11d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB3_46:                               # %.loopexit495
                                        #   in Loop: Header=BB3_34 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	movq	40(%rsp), %r12          # 8-byte Reload
	jl	.LBB3_34
.LBB3_47:                               # %._crit_edge577
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB3_67
# BB#48:
	testl	%r12d, %r12d
	jne	.LBB3_68
.LBB3_49:
	movl	fftNoAnchStop(%rip), %eax
	testl	%eax, %eax
	je	.LBB3_68
# BB#50:                                # %.preheader493.thread
	movl	$.L.str.4, %edi
	callq	ErrorExit
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB3_71
.LBB3_51:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movslq	n20or4or2(%rip), %r14
	testq	%r14, %r14
	jle	.LBB3_57
# BB#52:                                # %.lr.ph618
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB3_57
# BB#53:                                # %.lr.ph618.split.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Falign_noudp.seqVector1(%rip), %rbp
	leaq	-1(%r14), %r12
	movq	%r14, %r13
	xorl	%ebx, %ebx
	andq	$7, %r13
	je	.LBB3_55
	.p2align	4, 0x90
.LBB3_54:                               # %.lr.ph618.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB3_54
.LBB3_55:                               # %.lr.ph618.split.prol.loopexit
	cmpq	$7, %r12
	jb	.LBB3_57
	.p2align	4, 0x90
.LBB3_56:                               # %.lr.ph618.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbx
	cmpq	%r14, %rbx
	jl	.LBB3_56
.LBB3_57:                               # %._crit_edge619
	movl	scoremtx(%rip), %r8d
	cmpl	$-1, %r8d
	je	.LBB3_180
# BB#58:
	cmpl	$0, fftscore(%rip)
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB3_194
# BB#59:                                # %.preheader510
	testl	%esi, %esi
	jle	.LBB3_203
# BB#60:                                # %.lr.ph615
	movq	Falign_noudp.seqVector1(%rip), %rax
	movq	Falign_noudp.tmpseq1(%rip), %rcx
	movq	(%rax), %rbx
	movl	%esi, %esi
	addq	$8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_61:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_63 Depth 2
	movq	(%rcx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB3_66
# BB#62:                                # %.lr.ph.i461.preheader
                                        #   in Loop: Header=BB3_61 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB3_63:                               # %.lr.ph.i461
                                        #   Parent Loop BB3_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$20, %rax
	jg	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_63 Depth=2
	movsd	polarity(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rdx), %xmm1
	movsd	%xmm1, -8(%rdx)
	movsd	volume(,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
.LBB3_65:                               #   in Loop: Header=BB3_63 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rdx
	incq	%rbp
	testb	%al, %al
	jne	.LBB3_63
.LBB3_66:                               # %seq_vec_5.exit
                                        #   in Loop: Header=BB3_61 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB3_61
	jmp	.LBB3_203
.LBB3_67:
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	fprintf
	testl	%r12d, %r12d
	je	.LBB3_49
.LBB3_68:                               # %.preheader493
	testl	%r12d, %r12d
	setg	%r14b
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jle	.LBB3_71
# BB#69:                                # %.lr.ph568
	movq	Falign_noudp.segment1(%rip), %r9
	movq	Falign_noudp.sortedseg1(%rip), %rdx
	movq	Falign_noudp.segment2(%rip), %rcx
	movq	Falign_noudp.sortedseg2(%rip), %r10
	movl	%r12d, %eax
	cmpl	$4, %r12d
	jae	.LBB3_72
# BB#70:
	xorl	%ebx, %ebx
	jmp	.LBB3_80
.LBB3_71:                               # %.preheader493.._crit_edge569_crit_edge
	movq	Falign_noudp.sortedseg1(%rip), %rdx
	xorl	%r14d, %r14d
	jmp	.LBB3_87
.LBB3_72:                               # %min.iters.checked
	movl	%r12d, %r8d
	andl	$3, %r8d
	movq	%rax, %rbx
	subq	%r8, %rbx
	je	.LBB3_76
# BB#73:                                # %vector.memcheck
	leaq	-8(%r10,%rax,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB3_77
# BB#74:                                # %vector.memcheck
	leaq	-8(%rdx,%rax,8), %rsi
	cmpq	%rsi, %r10
	jae	.LBB3_77
.LBB3_76:
	xorl	%ebx, %ebx
	jmp	.LBB3_80
.LBB3_77:                               # %vector.body.preheader
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_78:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r9,%rsi), %r11
	leaq	48(%r9,%rsi), %rdi
	leaq	96(%r9,%rsi), %r15
	leaq	144(%r9,%rsi), %r12
	movd	%rdi, %xmm0
	movd	%r11, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r15, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%rdx,%rbp,8)
	movdqu	%xmm2, 16(%rdx,%rbp,8)
	leaq	(%rcx,%rsi), %r11
	leaq	48(%rcx,%rsi), %rdi
	leaq	96(%rcx,%rsi), %r15
	leaq	144(%rcx,%rsi), %r12
	movd	%rdi, %xmm0
	movd	%r11, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r15, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%r10,%rbp,8)
	movdqu	%xmm2, 16(%r10,%rbp,8)
	addq	$4, %rbp
	addq	$192, %rsi
	cmpq	%rbp, %rbx
	jne	.LBB3_78
# BB#79:                                # %middle.block
	testl	%r8d, %r8d
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB3_87
.LBB3_80:                               # %scalar.ph.preheader
	movl	%eax, %esi
	subl	%ebx, %esi
	leaq	-1(%rax), %r8
	subq	%rbx, %r8
	andq	$3, %rsi
	je	.LBB3_83
# BB#81:                                # %scalar.ph.prol.preheader
	movq	%rbx, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rbp
	leaq	(%rcx,%rbp), %rdi
	addq	%r9, %rbp
	negq	%rsi
	.p2align	4, 0x90
.LBB3_82:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rdx,%rbx,8)
	movq	%rdi, (%r10,%rbx,8)
	incq	%rbx
	addq	$48, %rdi
	addq	$48, %rbp
	incq	%rsi
	jne	.LBB3_82
.LBB3_83:                               # %scalar.ph.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_86
# BB#84:                                # %scalar.ph.preheader.new
	subq	%rbx, %rax
	leaq	24(%r10,%rbx,8), %rdi
	leaq	24(%rdx,%rbx,8), %rbp
	shlq	$4, %rbx
	leaq	(%rbx,%rbx,2), %rsi
	addq	%rsi, %rcx
	addq	%rsi, %r9
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_85:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r9,%rsi), %rbx
	movq	%rbx, -24(%rbp)
	leaq	(%rcx,%rsi), %rbx
	movq	%rbx, -24(%rdi)
	leaq	48(%r9,%rsi), %rbx
	movq	%rbx, -16(%rbp)
	leaq	48(%rcx,%rsi), %rbx
	movq	%rbx, -16(%rdi)
	leaq	96(%r9,%rsi), %rbx
	movq	%rbx, -8(%rbp)
	leaq	96(%rcx,%rsi), %rbx
	movq	%rbx, -8(%rdi)
	leaq	144(%r9,%rsi), %rbx
	movq	%rbx, (%rbp)
	leaq	144(%rcx,%rsi), %rbx
	movq	%rbx, (%rdi)
	addq	$32, %rdi
	addq	$192, %rsi
	addq	$32, %rbp
	addq	$-4, %rax
	jne	.LBB3_85
.LBB3_86:
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB3_87:                               # %._crit_edge569
	leal	-1(%r12), %ebx
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	mymergesort
	movq	Falign_noudp.sortedseg2(%rip), %rdx
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	mymergesort
	testb	%r14b, %r14b
	je	.LBB3_97
# BB#88:                                # %.lr.ph566
	movq	Falign_noudp.sortedseg1(%rip), %rcx
	movl	%r12d, %eax
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$7, %rdi
	je	.LBB3_90
	.p2align	4, 0x90
.LBB3_89:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbp
	movl	%edx, 40(%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB3_89
.LBB3_90:                               # %.prol.loopexit831
	cmpq	$7, %rsi
	jb	.LBB3_92
	.p2align	4, 0x90
.LBB3_91:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	leal	1(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	leal	2(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	leal	3(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	32(%rcx,%rdx,8), %rsi
	leal	4(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	40(%rcx,%rdx,8), %rsi
	leal	5(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	48(%rcx,%rdx,8), %rsi
	leal	6(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	56(%rcx,%rdx,8), %rsi
	leal	7(%rdx), %edi
	movl	%edi, 40(%rsi)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.LBB3_91
.LBB3_92:                               # %.preheader492
	testb	%r14b, %r14b
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB3_97
# BB#93:                                # %.lr.ph562
	movq	Falign_noudp.sortedseg2(%rip), %rcx
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$7, %rdi
	je	.LBB3_95
	.p2align	4, 0x90
.LBB3_94:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbp
	movl	%edx, 40(%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB3_94
.LBB3_95:                               # %.prol.loopexit826
	cmpq	$7, %rsi
	movq	(%rsp), %rbp            # 8-byte Reload
	jb	.LBB3_97
	.p2align	4, 0x90
.LBB3_96:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	leal	1(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	leal	2(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	leal	3(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	32(%rcx,%rdx,8), %rsi
	leal	4(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	40(%rcx,%rdx,8), %rsi
	leal	5(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	48(%rcx,%rdx,8), %rsi
	leal	6(%rdx), %edi
	movl	%edi, 40(%rsi)
	movq	56(%rcx,%rdx,8), %rsi
	leal	7(%rdx), %edi
	movl	%edi, 40(%rsi)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.LBB3_96
.LBB3_97:                               # %._crit_edge563
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB3_101
# BB#98:                                # %.preheader491
	testb	%r14b, %r14b
	je	.LBB3_113
# BB#99:                                # %.lr.ph559
	movq	Falign_noudp.sortedseg1(%rip), %rcx
	movq	Falign_noudp.cut1(%rip), %r11
	movq	Falign_noudp.sortedseg2(%rip), %rdx
	movq	Falign_noudp.cut2(%rip), %rax
	movl	%r12d, %esi
	testb	$1, %sil
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	184(%rsp), %rbx
	jne	.LBB3_114
# BB#100:
	xorl	%edi, %edi
	cmpl	$1, %r12d
	jne	.LBB3_115
	jmp	.LBB3_117
.LBB3_101:
	movq	Falign_noudp.cut1(%rip), %r11
	movl	$0, (%r11)
	movq	Falign_noudp.cut2(%rip), %rax
	movl	$0, (%rax)
	xorl	%r9d, %r9d
	testb	%r14b, %r14b
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB3_112
# BB#102:                               # %.lr.ph555
	movq	Falign_noudp.sortedseg1(%rip), %r10
	movl	%r12d, %r8d
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB3_103:                              # =>This Inner Loop Header: Depth=1
	movq	(%r10,%rdi,8), %rsi
	movl	8(%rsi), %ebx
	movslq	%r9d, %rbp
	cmpl	(%r11,%rbp,4), %ebx
	jle	.LBB3_106
# BB#104:                               #   in Loop: Header=BB3_103 Depth=1
	movq	32(%rsi), %rcx
	movl	8(%rcx), %edx
	cmpl	(%rax,%rbp,4), %edx
	jle	.LBB3_106
# BB#105:                               #   in Loop: Header=BB3_103 Depth=1
	leal	1(%rbp), %r9d
	movl	%ebx, 4(%r11,%rbp,4)
	movl	8(%rcx), %ecx
	movl	%ecx, 4(%rax,%rbp,4)
	jmp	.LBB3_111
	.p2align	4, 0x90
.LBB3_106:                              #   in Loop: Header=BB3_103 Depth=1
	testq	%rdi, %rdi
	je	.LBB3_111
# BB#107:                               #   in Loop: Header=BB3_103 Depth=1
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	-8(%r10,%rdi,8), %rcx
	ucomisd	16(%rcx), %xmm0
	jbe	.LBB3_111
# BB#108:                               #   in Loop: Header=BB3_103 Depth=1
	cmpl	-4(%r11,%rbp,4), %ebx
	jle	.LBB3_111
# BB#109:                               #   in Loop: Header=BB3_103 Depth=1
	leaq	-1(%rbp), %rdx
	movq	32(%rsi), %rcx
	movl	8(%rcx), %esi
	cmpl	(%rax,%rdx,4), %esi
	jle	.LBB3_111
# BB#110:                               #   in Loop: Header=BB3_103 Depth=1
	movl	%ebx, (%r11,%rbp,4)
	movl	8(%rcx), %ecx
	movl	%ecx, (%rax,%rbp,4)
	.p2align	4, 0x90
.LBB3_111:                              #   in Loop: Header=BB3_103 Depth=1
	incq	%rdi
	cmpq	%rdi, %r8
	jne	.LBB3_103
.LBB3_112:                              # %._crit_edge556
	movslq	%r9d, %rdx
	leaq	4(%r11,%rdx,4), %rsi
	incq	%rdx
	movl	%r9d, %r12d
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	184(%rsp), %rbx
	jmp	.LBB3_118
.LBB3_113:                              # %.preheader491.._crit_edge560_crit_edge
	movq	Falign_noudp.cut1(%rip), %r11
	movq	Falign_noudp.cut2(%rip), %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	184(%rsp), %rbx
	jmp	.LBB3_117
.LBB3_114:
	movq	(%rcx), %rdi
	movl	8(%rdi), %edi
	movl	%edi, 4(%r11)
	movq	(%rdx), %rdi
	movl	8(%rdi), %edi
	movl	%edi, 4(%rax)
	movl	$1, %edi
	cmpl	$1, %r12d
	je	.LBB3_117
	.p2align	4, 0x90
.LBB3_115:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%r11,%rdi,4)
	movq	(%rdx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%rax,%rdi,4)
	movq	8(%rcx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 8(%r11,%rdi,4)
	movq	8(%rdx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 8(%rax,%rdi,4)
	leaq	2(%rdi), %rdi
	cmpq	%rdi, %rsi
	jne	.LBB3_115
# BB#116:
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB3_117:                              # %._crit_edge560
	movl	$0, (%r11)
	movl	$0, (%rax)
	movslq	%r12d, %rdx
	leaq	4(%r11,%rdx,4), %rsi
	incq	%rdx
.LBB3_118:
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, (%rsi)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, (%rax,%rdx,4)
	testl	%r15d, %r15d
	jle	.LBB3_124
# BB#119:                               # %.lr.ph550
	movq	Falign_noudp.result1(%rip), %rcx
	movl	%r15d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB3_121
	.p2align	4, 0x90
.LBB3_120:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB3_120
.LBB3_121:                              # %.prol.loopexit817
	cmpq	$7, %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	jb	.LBB3_124
# BB#122:                               # %.lr.ph550.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB3_123:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB3_123
.LBB3_124:                              # %.preheader490
	testl	%ebp, %ebp
	jle	.LBB3_130
# BB#125:                               # %.lr.ph546
	movq	Falign_noudp.result2(%rip), %rcx
	movl	%ebp, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB3_127
	.p2align	4, 0x90
.LBB3_126:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB3_126
.LBB3_127:                              # %.prol.loopexit
	cmpq	$7, %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	jb	.LBB3_130
# BB#128:                               # %.lr.ph546.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB3_129:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB3_129
.LBB3_130:                              # %._crit_edge547
	movl	$-1, (%rbx)
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	testl	%r12d, %r12d
	jns	.LBB3_138
.LBB3_131:                              # %.preheader480
	testl	%r15d, %r15d
	movq	%r15, %rax
	movq	40(%rsp), %r15          # 8-byte Reload
	jle	.LBB3_134
# BB#132:                               # %.lr.ph519.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_133:                              # %.lr.ph519
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	Falign_noudp.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_133
.LBB3_134:                              # %.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_137
# BB#135:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_136:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	Falign_noudp.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_136
.LBB3_137:                              # %._crit_edge
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_138:                              # %.lr.ph544
	leal	-1(%r15), %eax
	incq	%rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leal	-1(%rbp), %eax
	incq	%rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movslq	%r12d, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	incl	%r12d
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	movl	%r15d, %r13d
	movl	%eax, %r12d
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movl	$-1, %eax
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	jmp	.LBB3_140
	.p2align	4, 0x90
.LBB3_139:                              # %.loopexit._crit_edge
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	184(%rsp), %rbx
	movl	(%rbx), %eax
	movq	Falign_noudp.cut1(%rip), %r11
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	%r14d, %ecx
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB3_140:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_154 Depth 2
                                        #     Child Loop BB3_161 Depth 2
                                        #     Child Loop BB3_172 Depth 2
                                        #     Child Loop BB3_175 Depth 2
	incl	%eax
	movl	%eax, (%rbx)
	cmpl	$0, (%r11,%r15,4)
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	jne	.LBB3_145
# BB#141:                               # %.preheader489
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_143
# BB#142:                               # %.lr.ph521
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.sgap1(%rip), %rdi
	movl	$111, %esi
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%r11, %rbx
	callq	memset
	movq	%rbx, %r11
.LBB3_143:                              # %.preheader487
                                        #   in Loop: Header=BB3_140 Depth=1
	testl	%ebp, %ebp
	jle	.LBB3_146
# BB#144:                               # %.lr.ph523
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.sgap2(%rip), %rdi
	movl	$111, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%r11, %rbx
	callq	memset
	movq	%rbx, %r11
	jmp	.LBB3_146
	.p2align	4, 0x90
.LBB3_145:                              #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.sgap1(%rip), %rdi
	movq	Falign_noudp.tmpres1(%rip), %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	decl	%ebx
	movl	%ebx, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
	movq	Falign_noudp.sgap2(%rip), %rdi
	movq	Falign_noudp.tmpres2(%rip), %rsi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	getkyokaigap
	movq	Falign_noudp.cut1(%rip), %r11
.LBB3_146:                              # %.loopexit488
                                        #   in Loop: Header=BB3_140 Depth=1
	leaq	1(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	4(%r11,%r15,4), %edx
	cmpl	80(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB3_151
# BB#147:                               # %.preheader486
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	jle	.LBB3_149
# BB#148:                               # %.lr.ph525
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.egap1(%rip), %rdi
	movl	$111, %esi
	movq	96(%rsp), %rdx          # 8-byte Reload
	callq	memset
.LBB3_149:                              # %.preheader484
                                        #   in Loop: Header=BB3_140 Depth=1
	testl	%ebp, %ebp
	jle	.LBB3_152
# BB#150:                               # %.lr.ph527
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.egap2(%rip), %rdi
	movl	$111, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	memset
	jmp	.LBB3_152
	.p2align	4, 0x90
.LBB3_151:                              #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.egap1(%rip), %rdi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
	movq	Falign_noudp.egap2(%rip), %rdi
	movq	Falign_noudp.cut2(%rip), %rax
	movl	4(%rax,%r15,4), %edx
	movq	%r14, %rsi
	movl	%ebp, %ecx
	callq	getkyokaigap
.LBB3_152:                              # %.loopexit485
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_155
# BB#153:                               # %.lr.ph530.preheader
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.tmpres1(%rip), %rcx
	movq	Falign_noudp.cut1(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_154:                              # %.lr.ph530
                                        #   Parent Loop BB3_140 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbp,8), %rdi
	movslq	(%rax,%r15,4), %rcx
	movq	(%rbx,%rbp,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r15,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Falign_noudp.tmpres1(%rip), %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	Falign_noudp.cut1(%rip), %rax
	movslq	4(%rax,%r15,4), %rsi
	movslq	(%rax,%r15,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB3_154
.LBB3_155:                              # %._crit_edge531
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB3_158
# BB#156:                               # %._crit_edge531
                                        #   in Loop: Header=BB3_140 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB3_158
# BB#157:                               #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.tmpres1(%rip), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	commongappick
.LBB3_158:                              # %.preheader483
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB3_163
# BB#159:                               # %.lr.ph533.preheader
                                        #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.cut2(%rip), %rax
	xorl	%ebp, %ebp
	jmp	.LBB3_161
.LBB3_160:                              #   in Loop: Header=BB3_161 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	Falign_noudp.cut2(%rip), %rax
	movl	(%rax,%r15,4), %ecx
	movl	4(%rax,%r15,4), %edx
	jmp	.LBB3_162
	.p2align	4, 0x90
.LBB3_161:                              # %.lr.ph533
                                        #   Parent Loop BB3_140 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rax,%r15,4), %edx
	movl	(%rax,%r15,4), %ecx
	cmpl	%ecx, %edx
	jle	.LBB3_160
.LBB3_162:                              #   in Loop: Header=BB3_161 Depth=2
	movq	Falign_noudp.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movslq	%ecx, %rax
	movq	(%r14,%rbp,8), %rsi
	addq	%rax, %rsi
	subl	%eax, %edx
	movslq	%edx, %rdx
	callq	strncpy
	movq	Falign_noudp.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	Falign_noudp.cut2(%rip), %rax
	movslq	4(%rax,%r15,4), %rdx
	movslq	(%rax,%r15,4), %rsi
	subq	%rsi, %rdx
	movb	$0, (%rcx,%rdx)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB3_161
.LBB3_163:                              # %._crit_edge534
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB3_166
# BB#164:                               # %._crit_edge534
                                        #   in Loop: Header=BB3_140 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB3_166
# BB#165:                               #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.tmpres2(%rip), %rsi
	movl	%ebp, %edi
	callq	commongappick
.LBB3_166:                              #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, constraint(%rip)
	jne	.LBB3_284
# BB#167:                               #   in Loop: Header=BB3_140 Depth=1
	movsbl	alg(%rip), %edx
	cmpl	$77, %edx
	jne	.LBB3_177
# BB#168:                               #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.tmpres1(%rip), %rdi
	movq	Falign_noudp.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebp, %r9d
	pushq	Falign_noudp.egap2(%rip)
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_noudp.egap1(%rip)
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_noudp.sgap2(%rip)
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_noudp.sgap1(%rip)
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	movl	216(%rsp), %ebx
	pushq	%rbx
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm
	addq	$48, %rsp
.Lcfi150:
	.cfi_adjust_cfa_offset -48
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
.LBB3_169:                              #   in Loop: Header=BB3_140 Depth=1
	movq	Falign_noudp.tmpres1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	112(%rsp), %rdx         # 8-byte Reload
	leal	(%rax,%rdx), %r14d
	cmpl	%ebx, %r14d
	jg	.LBB3_178
# BB#170:                               # %.preheader482
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_173
.LBB3_171:                              # %.lr.ph536.preheader
                                        #   in Loop: Header=BB3_140 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_172:                              # %.lr.ph536
                                        #   Parent Loop BB3_140 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_noudp.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Falign_noudp.tmpres1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB3_172
.LBB3_173:                              # %.preheader481
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB3_176
# BB#174:                               # %.lr.ph538.preheader
                                        #   in Loop: Header=BB3_140 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_175:                              # %.lr.ph538
                                        #   Parent Loop BB3_140 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_noudp.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Falign_noudp.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB3_175
.LBB3_176:                              # %.loopexit
                                        #   in Loop: Header=BB3_140 Depth=1
	cmpq	104(%rsp), %r15         # 8-byte Folded Reload
	jl	.LBB3_139
	jmp	.LBB3_179
.LBB3_177:                              #   in Loop: Header=BB3_140 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.8, %edi
	callq	ErrorExit
	movl	176(%rsp), %ebx
	jmp	.LBB3_169
.LBB3_178:                              #   in Loop: Header=BB3_140 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ebx, %r8d
	callq	fprintf
	movl	$.L.str.9, %edi
	callq	ErrorExit
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jg	.LBB3_171
	jmp	.LBB3_173
.LBB3_179:
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_131
.LBB3_180:                              # %.preheader507
	movq	16(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB3_203
# BB#181:                               # %.lr.ph611
	movq	Falign_noudp.seqVector1(%rip), %rax
	movq	Falign_noudp.tmpseq1(%rip), %rcx
	movq	(%rax), %rbx
	movl	%edx, %esi
	addq	$8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_182:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_184 Depth 2
	movq	(%rcx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB3_193
# BB#183:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_182 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB3_184:                              # %.lr.ph.i
                                        #   Parent Loop BB3_182 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	$-97, %al
	cmpb	$19, %al
	ja	.LBB3_192
# BB#185:                               # %.lr.ph.i
                                        #   in Loop: Header=BB3_184 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_186:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	jmp	.LBB3_191
	.p2align	4, 0x90
.LBB3_187:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	jmp	.LBB3_189
	.p2align	4, 0x90
.LBB3_188:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
.LBB3_189:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	%xmm1, (%rdx)
	jmp	.LBB3_192
	.p2align	4, 0x90
.LBB3_190:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
.LBB3_191:                              #   in Loop: Header=BB3_184 Depth=2
	movsd	%xmm1, -8(%rdx)
.LBB3_192:                              #   in Loop: Header=BB3_184 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rdx
	incq	%rbp
	testb	%al, %al
	jne	.LBB3_184
.LBB3_193:                              # %seq_vec_4.exit
                                        #   in Loop: Header=BB3_182 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB3_182
	jmp	.LBB3_203
.LBB3_194:                              # %.preheader508
	testl	%esi, %esi
	jle	.LBB3_203
# BB#195:                               # %.lr.ph613
	movq	Falign_noudp.seqVector1(%rip), %rcx
	movq	Falign_noudp.tmpseq1(%rip), %rdx
	movl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_196:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_198 Depth 2
	movq	(%rdx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB3_202
# BB#197:                               # %.lr.ph.i473.preheader
                                        #   in Loop: Header=BB3_196 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_198:                              # %.lr.ph.i473
                                        #   Parent Loop BB3_196 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	testq	%rax, %rax
	js	.LBB3_201
# BB#199:                               # %.lr.ph.i473
                                        #   in Loop: Header=BB3_198 Depth=2
	cmpl	%r14d, %eax
	jge	.LBB3_201
# BB#200:                               #   in Loop: Header=BB3_198 Depth=2
	movq	(%rcx,%rax,8), %rax
	movsd	(%rax,%rbx), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rbx)
.LBB3_201:                              #   in Loop: Header=BB3_198 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rbx
	incq	%rbp
	testb	%al, %al
	jne	.LBB3_198
.LBB3_202:                              # %seq_vec_3.exit478
                                        #   in Loop: Header=BB3_196 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB3_196
.LBB3_203:                              # %.preheader506
	testl	%r14d, %r14d
	jle	.LBB3_210
# BB#204:                               # %.lr.ph608
	movq	8(%rsp), %rcx           # 8-byte Reload
	testl	%ecx, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
	je	.LBB3_211
# BB#205:                               # %.lr.ph608.split.preheader
	leal	-1(%rcx), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Falign_noudp.seqVector2(%rip), %rbp
	leaq	-1(%r14), %r12
	movq	%r14, %r13
	xorl	%ebx, %ebx
	andq	$7, %r13
	je	.LBB3_207
	.p2align	4, 0x90
.LBB3_206:                              # %.lr.ph608.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB3_206
.LBB3_207:                              # %.lr.ph608.split.prol.loopexit
	cmpq	$7, %r12
	jb	.LBB3_209
	.p2align	4, 0x90
.LBB3_208:                              # %.lr.ph608.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbp,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbx
	cmpq	%r14, %rbx
	jl	.LBB3_208
.LBB3_209:                              # %._crit_edge609.loopexit630
	movl	scoremtx(%rip), %r8d
.LBB3_210:                              # %._crit_edge609
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB3_211:                              # %._crit_edge609
	movl	%ecx, %r12d
	shrl	$31, %r12d
	cmpl	$-1, %r8d
	je	.LBB3_221
# BB#212:
	cmpl	$0, fftscore(%rip)
	je	.LBB3_235
# BB#213:                               # %.preheader504
	testl	%eax, %eax
	jle	.LBB3_244
# BB#214:                               # %.lr.ph606
	movq	Falign_noudp.seqVector2(%rip), %rcx
	movq	Falign_noudp.tmpseq2(%rip), %rax
	movq	(%rcx), %rbp
	movl	(%rsp), %edx            # 4-byte Reload
	addq	$8, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_215:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_217 Depth 2
	movq	(%rax,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB3_220
# BB#216:                               # %.lr.ph.i465.preheader
                                        #   in Loop: Header=BB3_215 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB3_217:                              # %.lr.ph.i465
                                        #   Parent Loop BB3_215 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rbx
	movslq	amino_n(,%rbx,4), %rbx
	cmpq	$20, %rbx
	jg	.LBB3_219
# BB#218:                               #   in Loop: Header=BB3_217 Depth=2
	movsd	polarity(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rcx), %xmm1
	movsd	%xmm1, -8(%rcx)
	movsd	volume(,%rbx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
.LBB3_219:                              #   in Loop: Header=BB3_217 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rcx
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB3_217
.LBB3_220:                              # %seq_vec_5.exit466
                                        #   in Loop: Header=BB3_215 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB3_215
	jmp	.LBB3_244
.LBB3_221:                              # %.preheader501
	testl	%eax, %eax
	jle	.LBB3_244
# BB#222:                               # %.lr.ph602
	movq	Falign_noudp.seqVector2(%rip), %rcx
	movq	Falign_noudp.tmpseq2(%rip), %rax
	movq	(%rcx), %rbp
	movl	(%rsp), %edx            # 4-byte Reload
	addq	$8, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_223:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_225 Depth 2
	movq	(%rax,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB3_234
# BB#224:                               # %.lr.ph.i469.preheader
                                        #   in Loop: Header=BB3_223 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB3_225:                              # %.lr.ph.i469
                                        #   Parent Loop BB3_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_233
# BB#226:                               # %.lr.ph.i469
                                        #   in Loop: Header=BB3_225 Depth=2
	movzbl	%bl, %ebx
	jmpq	*.LJTI3_1(,%rbx,8)
.LBB3_227:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	jmp	.LBB3_232
	.p2align	4, 0x90
.LBB3_228:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	jmp	.LBB3_230
	.p2align	4, 0x90
.LBB3_229:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
.LBB3_230:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	%xmm1, (%rcx)
	jmp	.LBB3_233
	.p2align	4, 0x90
.LBB3_231:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
.LBB3_232:                              #   in Loop: Header=BB3_225 Depth=2
	movsd	%xmm1, -8(%rcx)
.LBB3_233:                              #   in Loop: Header=BB3_225 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rcx
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB3_225
.LBB3_234:                              # %seq_vec_4.exit470
                                        #   in Loop: Header=BB3_223 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB3_223
	jmp	.LBB3_244
.LBB3_235:                              # %.preheader502
	testl	%eax, %eax
	jle	.LBB3_244
# BB#236:                               # %.lr.ph604
	movq	Falign_noudp.seqVector2(%rip), %rax
	movq	Falign_noudp.tmpseq2(%rip), %rcx
	movl	(%rsp), %edx            # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_237:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_239 Depth 2
	movq	(%rcx,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB3_243
# BB#238:                               # %.lr.ph.i462.preheader
                                        #   in Loop: Header=BB3_237 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_239:                              # %.lr.ph.i462
                                        #   Parent Loop BB3_237 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rbx
	movslq	amino_n(,%rbx,4), %rbx
	testq	%rbx, %rbx
	js	.LBB3_242
# BB#240:                               # %.lr.ph.i462
                                        #   in Loop: Header=BB3_239 Depth=2
	cmpl	%r14d, %ebx
	jge	.LBB3_242
# BB#241:                               #   in Loop: Header=BB3_239 Depth=2
	movq	(%rax,%rbx,8), %rbx
	movsd	(%rbx,%rbp), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rbp)
.LBB3_242:                              #   in Loop: Header=BB3_239 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rbp
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB3_239
.LBB3_243:                              # %seq_vec_3.exit
                                        #   in Loop: Header=BB3_237 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB3_237
.LBB3_244:                              # %.preheader500
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%esi, %r12d
	testl	%r14d, %r14d
	jle	.LBB3_258
# BB#245:                               # %.lr.ph600.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_246:                              # %.lr.ph600
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_noudp.seqVector2(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	testq	%rbx, %rbx
	sete	%dl
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	fft
	movq	Falign_noudp.seqVector1(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	fft
	movq	8(%rsp), %rsi           # 8-byte Reload
	incq	%rbx
	movslq	n20or4or2(%rip), %r14
	cmpq	%r14, %rbx
	jl	.LBB3_246
# BB#247:                               # %.preheader499
	testl	%r14d, %r14d
	jle	.LBB3_258
# BB#248:                               # %.preheader498.lr.ph
	testl	%esi, %esi
	jle	.LBB3_254
# BB#249:                               # %.preheader498.us.preheader
	movl	%esi, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_250:                              # %.preheader498.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_251 Depth 2
	movq	%r15, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_251:                              #   Parent Loop BB3_250 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_noudp.naiseki(%rip), %rax
	movq	(%rax,%r13,8), %rdi
	addq	%rbp, %rdi
	movq	Falign_noudp.seqVector1(%rip), %rax
	movq	(%rax,%r13,8), %rsi
	addq	%rbp, %rsi
	movq	Falign_noudp.seqVector2(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	addq	%rbp, %rdx
	callq	calcNaiseki
	addq	$16, %rbp
	decq	%rbx
	jne	.LBB3_251
# BB#252:                               # %._crit_edge597.us
                                        #   in Loop: Header=BB3_250 Depth=1
	incq	%r13
	movslq	n20or4or2(%rip), %r14
	cmpq	%r14, %r13
	jl	.LBB3_250
# BB#253:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB3_258
.LBB3_254:                              # %.preheader498.preheader
	leal	-1(%r14), %ecx
	movl	%r14d, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB3_256
.LBB3_255:                              # %.preheader498.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB3_255
.LBB3_256:                              # %.preheader498.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB3_258
.LBB3_257:                              # %.preheader498
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	%r14d, %eax
	jl	.LBB3_257
.LBB3_258:                              # %.preheader497
	sarl	%r12d
	movq	Falign_noudp.naisekiNoWa(%rip), %r15
	testl	%esi, %esi
	jle	.LBB3_269
# BB#259:                               # %.lr.ph593
	testl	%r14d, %r14d
	jle	.LBB3_270
# BB#260:                               # %.lr.ph593.split.us.preheader
	movq	Falign_noudp.naiseki(%rip), %rax
	movl	%r14d, %r9d
	movl	8(%rsp), %r10d          # 4-byte Reload
	leaq	-1(%r9), %r11
	movl	%r9d, %edi
	andl	$3, %edi
	leaq	24(%rax), %r8
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_261:                              # %.lr.ph593.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_263 Depth 2
                                        #     Child Loop BB3_267 Depth 2
	movq	%r14, %rbp
	shlq	$4, %rbp
	leaq	(%r15,%rbp), %rdx
	testq	%rdi, %rdi
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r15,%rbp)
	je	.LBB3_264
# BB#262:                               # %.prol.preheader843
                                        #   in Loop: Header=BB3_261 Depth=1
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_263:                              #   Parent Loop BB3_261 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rsi
	movupd	(%rsi,%rbp), %xmm1
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	incq	%rbx
	cmpq	%rbx, %rdi
	jne	.LBB3_263
	jmp	.LBB3_265
	.p2align	4, 0x90
.LBB3_264:                              #   in Loop: Header=BB3_261 Depth=1
	xorl	%ebx, %ebx
.LBB3_265:                              # %.prol.loopexit844
                                        #   in Loop: Header=BB3_261 Depth=1
	cmpq	$3, %r11
	jb	.LBB3_268
# BB#266:                               # %.lr.ph593.split.us.new
                                        #   in Loop: Header=BB3_261 Depth=1
	movq	%r9, %rsi
	subq	%rbx, %rsi
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB3_267:                              #   Parent Loop BB3_261 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	-16(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	movq	-8(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	(%rbx), %rcx
	movupd	(%rcx,%rbp), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB3_267
.LBB3_268:                              # %._crit_edge591.us
                                        #   in Loop: Header=BB3_261 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB3_261
.LBB3_269:                              # %._crit_edge594
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	fft
	cmpl	$-1, %ebx
	jge	.LBB3_271
	jmp	.LBB3_273
.LBB3_270:                              # %._crit_edge594.thread
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rbx
	leal	-1(%rbx), %edx
	shlq	$4, %rdx
	addq	$16, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
	movl	%ebx, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	fft
.LBB3_271:                              # %.lr.ph587
	movq	Falign_noudp.soukan(%rip), %rax
	movslq	%r12d, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	addq	Falign_noudp.naisekiNoWa(%rip), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB3_272:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdi
	movq	%rdi, 8(%rax,%rsi,8)
	incq	%rsi
	addq	$-16, %rdx
	cmpq	%rsi, %rcx
	jg	.LBB3_272
.LBB3_273:                              # %.preheader496
	leal	1(%r12), %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	%esi, %ecx
	jge	.LBB3_280
# BB#274:                               # %.lr.ph583
	movq	Falign_noudp.naisekiNoWa(%rip), %rax
	movq	Falign_noudp.soukan(%rip), %rdx
	movslq	%ecx, %rcx
	movq	%rsi, %rbp
	leal	3(%rbp), %edi
	subl	%r12d, %edi
	leal	-2(%rbp), %esi
	subl	%r12d, %esi
	andl	$3, %edi
	je	.LBB3_277
# BB#275:                               # %.prol.preheader838
	leal	-1(%rbp), %ebx
	negl	%edi
	.p2align	4, 0x90
.LBB3_276:                              # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movq	%rbx, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rdx,%rcx,8)
	incq	%rcx
	decl	%ebx
	incl	%edi
	jne	.LBB3_276
.LBB3_277:                              # %.prol.loopexit839
	cmpl	$3, %esi
	jb	.LBB3_281
# BB#278:                               # %.lr.ph583.new
	leaq	24(%rdx,%rcx,8), %rsi
	movq	%rcx, %rdi
	addq	$3, %rdi
	movl	%r12d, %r8d
	subl	%edi, %r8d
	leal	2(%rcx), %edi
	movl	%r12d, %r9d
	subl	%edi, %r9d
	movl	%r12d, %ebx
	subl	%ecx, %ebx
	leal	1(%rcx), %edi
	subl	%edi, %r12d
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	.p2align	4, 0x90
.LBB3_279:                              # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -24(%rsi)
	leal	(%r12,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -16(%rsi)
	leal	(%r9,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -8(%rsi)
	leal	(%r8,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rsi)
	addl	$-4, %edi
	addq	$32, %rsi
	cmpl	%edi, %ecx
	jne	.LBB3_279
	jmp	.LBB3_281
.LBB3_280:                              # %.preheader496.._crit_edge584_crit_edge
	movq	Falign_noudp.soukan(%rip), %rdx
.LBB3_281:                              # %._crit_edge584
	movq	Falign_noudp.kouho(%rip), %rdi
	movl	$100, %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getKouho
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_32
# BB#282:
	testl	%eax, %eax
	jg	.LBB3_33
# BB#283:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB3_47
.LBB3_284:
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	Falign_noudp, .Lfunc_end3-Falign_noudp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_186
	.quad	.LBB3_192
	.quad	.LBB3_187
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_188
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_192
	.quad	.LBB3_190
.LJTI3_1:
	.quad	.LBB3_227
	.quad	.LBB3_233
	.quad	.LBB3_228
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_229
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_233
	.quad	.LBB3_231

	.text
	.globl	Falign_udpari_long
	.p2align	4, 0x90
	.type	Falign_udpari_long,@function
Falign_udpari_long:                     # @Falign_udpari_long
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 176
.Lcfi158:
	.cfi_offset %rbx, -56
.Lcfi159:
	.cfi_offset %r12, -48
.Lcfi160:
	.cfi_offset %r13, -40
.Lcfi161:
	.cfi_offset %r14, -32
.Lcfi162:
	.cfi_offset %r15, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	176(%rsp), %ebx
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	(%r13), %rdi
	callq	strlen
	cmpl	%eax, %ebp
	movq	%rax, 56(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	cmovgel	%ebp, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	leal	(%rbp,%rbp), %ecx
	cmpl	%ebp, %eax
	jge	.LBB4_1
# BB#2:
	movl	Falign_udpari_long.prevalloclen(%rip), %eax
	cmpl	%ebx, %eax
	je	.LBB4_6
# BB#3:
	testl	%eax, %eax
	je	.LBB4_5
# BB#4:
	movq	Falign_udpari_long.result1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_udpari_long.result2(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_udpari_long.tmpres1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_udpari_long.tmpres2(%rip), %rdi
	callq	FreeCharMtx
.LBB4_5:
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.result1(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.result2(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpres1(%rip)
	movl	njob(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpres2(%rip)
	movl	%ebx, Falign_udpari_long.prevalloclen(%rip)
.LBB4_6:
	movl	Falign_udpari_long.localalloclen(%rip), %eax
	testl	%eax, %eax
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jne	.LBB4_17
# BB#7:
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_udpari_long.sgap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_udpari_long.egap1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_udpari_long.sgap2(%rip)
	movl	njob(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, Falign_udpari_long.egap2(%rip)
	movl	$100, %edi
	callq	AllocateIntVec
	movq	%rax, Falign_udpari_long.kouho(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign_udpari_long.cut1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	callq	AllocateIntVec
	movq	%rax, Falign_udpari_long.cut2(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpptr1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpptr2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r15
	movq	%rax, Falign_udpari_long.segment(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, Falign_udpari_long.segment1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, Falign_udpari_long.segment2(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r14, Falign_udpari_long.sortedseg1(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$8, %esi
	callq	calloc
	movq	%rax, Falign_udpari_long.sortedseg2(%rip)
	testq	%rax, %rax
	je	.LBB4_12
# BB#8:
	testq	%r15, %r15
	je	.LBB4_12
# BB#9:
	testq	%rbp, %rbp
	je	.LBB4_12
# BB#10:
	testq	%rbx, %rbx
	je	.LBB4_12
# BB#11:
	testq	%r14, %r14
	jne	.LBB4_13
.LBB4_12:
	movl	$.L.str, %edi
	callq	ErrorExit
.LBB4_13:
	cmpl	$-1, scoremtx(%rip)
	je	.LBB4_15
# BB#14:
	cmpl	$0, fftscore(%rip)
	movl	$1, %ecx
	movl	$20, %eax
	cmovnel	%ecx, %eax
	jmp	.LBB4_16
.LBB4_15:
	movl	$1, %eax
.LBB4_16:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	%eax, n20or4or2(%rip)
	movl	Falign_udpari_long.localalloclen(%rip), %eax
.LBB4_17:
	cmpl	%ebp, %eax
	jge	.LBB4_25
# BB#18:
	testl	%eax, %eax
	je	.LBB4_22
# BB#19:
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB4_21
# BB#20:
	movq	Falign_udpari_long.seqVector1(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_udpari_long.seqVector2(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_udpari_long.naisekiNoWa(%rip), %rdi
	callq	FreeFukusosuuVec
	movq	Falign_udpari_long.naiseki(%rip), %rdi
	callq	FreeFukusosuuMtx
	movq	Falign_udpari_long.soukan(%rip), %rdi
	callq	FreeDoubleVec
.LBB4_21:
	movq	Falign_udpari_long.tmpseq1(%rip), %rdi
	callq	FreeCharMtx
	movq	Falign_udpari_long.tmpseq2(%rip), %rdi
	callq	FreeCharMtx
.LBB4_22:
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpseq1(%rip)
	movl	njob(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateCharMtx
	movq	%rax, Falign_udpari_long.tmpseq2(%rip)
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB4_24
# BB#23:
	movl	%ebp, %edi
	callq	AllocateFukusosuuVec
	movq	%rax, Falign_udpari_long.naisekiNoWa(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%ebp, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_udpari_long.naiseki(%rip)
	movl	n20or4or2(%rip), %edi
	leal	1(%rbp), %ebx
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_udpari_long.seqVector1(%rip)
	movl	n20or4or2(%rip), %edi
	movl	%ebx, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, Falign_udpari_long.seqVector2(%rip)
	movl	%ebx, %edi
	callq	AllocateDoubleVec
	movq	%rax, Falign_udpari_long.soukan(%rip)
.LBB4_24:
	movl	%ebp, Falign_udpari_long.localalloclen(%rip)
.LBB4_25:                               # %.preheader455
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_28
# BB#26:                                # %.lr.ph563.preheader
	movl	32(%rsp), %ebx          # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_27:                               # %.lr.ph563
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_udpari_long.tmpseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r12,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_27
.LBB4_28:                               # %.preheader454
	testl	%r14d, %r14d
	jle	.LBB4_31
# BB#29:                                # %.lr.ph560.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_30:                               # %.lr.ph560
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_udpari_long.tmpseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r13,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_30
.LBB4_31:                               # %._crit_edge561
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r13, 96(%rsp)          # 8-byte Spill
	je	.LBB4_60
# BB#32:                                # %.thread
	movl	$0, 12(%rsp)
.LBB4_33:                               # %.thread727
	movq	Falign_udpari_long.kouho(%rip), %rax
	movl	$0, (%rax)
	movl	$1, %eax
.LBB4_34:                               # %.lr.ph517
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r8d
	negl	%r8d
	movslq	%eax, %r9
	xorl	%r12d, %r12d
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_42 Depth 2
	movq	Falign_udpari_long.kouho(%rip), %rax
	movl	(%rax,%r12,4), %r15d
	cmpl	%r8d, %r15d
	jle	.LBB4_46
# BB#36:                                #   in Loop: Header=BB4_35 Depth=1
	cmpl	%r15d, 56(%rsp)         # 4-byte Folded Reload
	jle	.LBB4_46
# BB#37:                                #   in Loop: Header=BB4_35 Depth=1
	movq	Falign_udpari_long.tmpptr1(%rip), %r9
	subq	$8, %rsp
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	movl	%r15d, %edi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %esi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	pushq	Falign_udpari_long.tmpptr2(%rip)
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	callq	zurasu2
	addq	$16, %rsp
.Lcfi166:
	.cfi_adjust_cfa_offset -16
	movq	Falign_udpari_long.tmpptr1(%rip), %rdx
	movq	Falign_udpari_long.tmpptr2(%rip), %rcx
	movslq	12(%rsp), %r13
	leaq	(%r13,%r13,2), %rax
	shlq	$4, %rax
	addq	Falign_udpari_long.segment(%rip), %rax
	subq	$8, %rsp
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %edi
	movl	%ebx, %esi
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	callq	alignableReagion
	addq	$16, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r14d
	leal	(%r13,%r14), %eax
	cmpl	$99998, %eax            # imm = 0x1869E
	jl	.LBB4_39
# BB#38:                                #   in Loop: Header=BB4_35 Depth=1
	movl	$.L.str.2, %edi
	callq	ErrorExit
.LBB4_39:                               #   in Loop: Header=BB4_35 Depth=1
	testl	%r14d, %r14d
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	je	.LBB4_47
# BB#40:                                # %.preheader436
                                        #   in Loop: Header=BB4_35 Depth=1
	jle	.LBB4_46
# BB#41:                                # %.lr.ph514
                                        #   in Loop: Header=BB4_35 Depth=1
	movq	Falign_udpari_long.segment(%rip), %rax
	movq	Falign_udpari_long.segment1(%rip), %rcx
	movq	Falign_udpari_long.segment2(%rip), %rdx
	incl	%r14d
	.p2align	4, 0x90
.LBB4_42:                               #   Parent Loop BB4_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r13d, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$4, %rsi
	movl	(%rax,%rsi), %edi
	testl	%r15d, %r15d
	jle	.LBB4_44
# BB#43:                                #   in Loop: Header=BB4_42 Depth=2
	movl	%edi, (%rcx,%rsi)
	movslq	12(%rsp), %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	4(%rax,%rsi), %edi
	movl	%edi, 4(%rcx,%rsi)
	movl	8(%rax,%rsi), %edi
	movl	%edi, 8(%rcx,%rsi)
	movq	16(%rax,%rsi), %rdi
	movq	%rdi, 16(%rcx,%rsi)
	movl	(%rax,%rsi), %edi
	addl	%r15d, %edi
	movl	%edi, (%rdx,%rsi)
	movl	12(%rsp), %esi
	movslq	%esi, %rbp
	movq	%rbp, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	addl	%r15d, %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
	addl	%r15d, %ebx
	jmp	.LBB4_45
	.p2align	4, 0x90
.LBB4_44:                               #   in Loop: Header=BB4_42 Depth=2
	subl	%r15d, %edi
	movl	%edi, (%rcx,%rsi)
	movslq	12(%rsp), %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	4(%rax,%rsi), %edi
	subl	%r15d, %edi
	movl	%edi, 4(%rcx,%rsi)
	movl	8(%rax,%rsi), %edi
	subl	%r15d, %edi
	movl	%edi, 8(%rcx,%rsi)
	movq	16(%rax,%rsi), %rdi
	movq	%rdi, 16(%rcx,%rsi)
	movl	(%rax,%rsi), %edi
	movl	%edi, (%rdx,%rsi)
	movl	12(%rsp), %esi
	movslq	%esi, %rbp
	movq	%rbp, %rdi
	shlq	$4, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rax,%rdi), %ebx
	movl	%ebx, 4(%rdx,%rdi)
	movl	8(%rax,%rdi), %ebx
.LBB4_45:                               #   in Loop: Header=BB4_42 Depth=2
	leaq	(%rbp,%rbp,2), %rdi
	shlq	$4, %rdi
	movl	%ebx, 8(%rdx,%rdi)
	movslq	%esi, %r13
	movq	%r13, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movq	16(%rax,%rsi), %rdi
	leaq	(%rdx,%rsi), %rbp
	movq	%rdi, 16(%rdx,%rsi)
	leaq	(%rcx,%rsi), %rdi
	movq	%rbp, 32(%rcx,%rsi)
	movq	%rdi, 32(%rdx,%rsi)
	incl	%r13d
	movl	%r13d, 12(%rsp)
	decl	%r14d
	cmpl	$1, %r14d
	jg	.LBB4_42
.LBB4_46:                               # %.loopexit437
                                        #   in Loop: Header=BB4_35 Depth=1
	incq	%r12
	cmpq	%r9, %r12
	jl	.LBB4_35
.LBB4_47:                               # %._crit_edge518
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB4_76
# BB#48:                                # %._crit_edge518._crit_edge
	movl	12(%rsp), %r15d
.LBB4_49:
	movq	96(%rsp), %r13          # 8-byte Reload
	testl	%r15d, %r15d
	jne	.LBB4_52
# BB#50:
	movl	fftNoAnchStop(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_52
# BB#51:                                # %.preheader435.thread
	movl	$.L.str.4, %edi
	callq	ErrorExit
	movq	48(%rsp), %r12          # 8-byte Reload
	jmp	.LBB4_55
.LBB4_52:                               # %.preheader435
	testl	%r15d, %r15d
	setg	%r14b
	movq	48(%rsp), %r12          # 8-byte Reload
	jle	.LBB4_55
# BB#53:                                # %.lr.ph511
	movq	Falign_udpari_long.segment1(%rip), %rax
	movq	Falign_udpari_long.sortedseg1(%rip), %rdx
	movq	Falign_udpari_long.segment2(%rip), %rcx
	movq	Falign_udpari_long.sortedseg2(%rip), %r9
	movslq	%r15d, %r8
	cmpl	$4, %r15d
	jae	.LBB4_56
# BB#54:
	xorl	%ebp, %ebp
	jmp	.LBB4_81
.LBB4_55:                               # %.preheader435.._crit_edge512_crit_edge
	movq	Falign_udpari_long.sortedseg1(%rip), %rdx
	xorl	%r14d, %r14d
	jmp	.LBB4_83
.LBB4_56:                               # %min.iters.checked
	movq	%r8, %rbp
	andq	$-4, %rbp
	je	.LBB4_77
# BB#57:                                # %vector.memcheck
	leaq	-8(%r9,%r8,8), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB4_78
# BB#58:                                # %vector.memcheck
	leaq	-8(%rdx,%r8,8), %rdi
	cmpq	%rdi, %r9
	jae	.LBB4_78
# BB#59:
	xorl	%ebp, %ebp
	jmp	.LBB4_81
.LBB4_60:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movslq	n20or4or2(%rip), %r14
	testq	%r14, %r14
	jle	.LBB4_66
# BB#61:                                # %.lr.ph557
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB4_66
# BB#62:                                # %.lr.ph557.split.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Falign_udpari_long.seqVector1(%rip), %rbx
	leaq	-1(%r14), %r12
	movq	%r14, %r13
	xorl	%ebp, %ebp
	andq	$7, %r13
	je	.LBB4_64
	.p2align	4, 0x90
.LBB4_63:                               # %.lr.ph557.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB4_63
.LBB4_64:                               # %.lr.ph557.split.prol.loopexit
	cmpq	$7, %r12
	jb	.LBB4_66
	.p2align	4, 0x90
.LBB4_65:                               # %.lr.ph557.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbp
	cmpq	%r14, %rbp
	jl	.LBB4_65
.LBB4_66:                               # %._crit_edge558
	movl	scoremtx(%rip), %r8d
	cmpl	$-1, %r8d
	je	.LBB4_174
# BB#67:
	cmpl	$0, fftscore(%rip)
	movq	32(%rsp), %rsi          # 8-byte Reload
	je	.LBB4_189
# BB#68:                                # %.preheader452
	testl	%esi, %esi
	jle	.LBB4_198
# BB#69:                                # %.lr.ph554
	movq	Falign_udpari_long.seqVector1(%rip), %rax
	movq	Falign_udpari_long.tmpseq1(%rip), %rcx
	movq	(%rax), %rbx
	movl	%esi, %esi
	addq	$8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_70:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_72 Depth 2
	movq	(%rcx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB4_75
# BB#71:                                # %.lr.ph.i395.preheader
                                        #   in Loop: Header=BB4_70 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB4_72:                               # %.lr.ph.i395
                                        #   Parent Loop BB4_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	cmpq	$20, %rax
	jg	.LBB4_74
# BB#73:                                #   in Loop: Header=BB4_72 Depth=2
	movsd	polarity(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rdx), %xmm1
	movsd	%xmm1, -8(%rdx)
	movsd	volume(,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
.LBB4_74:                               #   in Loop: Header=BB4_72 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rdx
	incq	%rbp
	testb	%al, %al
	jne	.LBB4_72
.LBB4_75:                               # %seq_vec_5.exit
                                        #   in Loop: Header=BB4_70 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB4_70
	jmp	.LBB4_198
.LBB4_76:
	movq	stderr(%rip), %rdi
	movl	12(%rsp), %r15d
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	jmp	.LBB4_49
.LBB4_77:
	xorl	%ebp, %ebp
	jmp	.LBB4_81
.LBB4_78:                               # %vector.body.preheader
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_79:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rbx), %r10
	leaq	48(%rax,%rbx), %rsi
	leaq	96(%rax,%rbx), %r11
	leaq	144(%rax,%rbx), %r12
	movd	%rsi, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r11, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%rdx,%rdi,8)
	movdqu	%xmm2, 16(%rdx,%rdi,8)
	leaq	(%rcx,%rbx), %r10
	leaq	48(%rcx,%rbx), %rsi
	leaq	96(%rcx,%rbx), %r11
	leaq	144(%rcx,%rbx), %r12
	movd	%rsi, %xmm0
	movd	%r10, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r12, %xmm0
	movd	%r11, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, (%r9,%rdi,8)
	movdqu	%xmm2, 16(%r9,%rdi,8)
	addq	$4, %rdi
	addq	$192, %rbx
	cmpq	%rdi, %rbp
	jne	.LBB4_79
# BB#80:                                # %middle.block
	cmpq	%rbp, %r8
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB4_83
.LBB4_81:                               # %scalar.ph.preheader
	movq	%rbp, %rsi
	shlq	$4, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	addq	%rsi, %rax
	addq	%rsi, %rcx
	.p2align	4, 0x90
.LBB4_82:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rdx,%rbp,8)
	movq	%rcx, (%r9,%rbp,8)
	incq	%rbp
	addq	$48, %rax
	addq	$48, %rcx
	cmpq	%r8, %rbp
	jl	.LBB4_82
.LBB4_83:                               # %._crit_edge512
	leal	-1(%r15), %ebp
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	movq	Falign_udpari_long.sortedseg2(%rip), %rdx
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	mymergesort
	testb	%r14b, %r14b
	je	.LBB4_89
# BB#84:                                # %.lr.ph509
	movq	Falign_udpari_long.sortedseg1(%rip), %rcx
	movslq	%r15d, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_85:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB4_85
# BB#86:                                # %.preheader434
	testb	%r14b, %r14b
	je	.LBB4_89
# BB#87:                                # %.lr.ph504
	movq	Falign_udpari_long.sortedseg2(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_88:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 40(%rsi)
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB4_88
.LBB4_89:                               # %._crit_edge505
	cmpl	$0, kobetsubunkatsu(%rip)
	je	.LBB4_93
# BB#90:                                # %.preheader433
	testb	%r14b, %r14b
	je	.LBB4_104
# BB#91:                                # %.lr.ph501
	movq	Falign_udpari_long.sortedseg1(%rip), %rdx
	movq	Falign_udpari_long.cut1(%rip), %rax
	movq	Falign_udpari_long.sortedseg2(%rip), %rsi
	movq	Falign_udpari_long.cut2(%rip), %rcx
	xorl	%edi, %edi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_92:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%rax,%rdi,4)
	movq	(%rsi,%rdi,8), %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, 4(%rcx,%rdi,4)
	incq	%rdi
	movslq	12(%rsp), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB4_92
	jmp	.LBB4_105
.LBB4_93:
	leal	2(%r15), %r14d
	cmpl	%r14d, Falign_udpari_long.crossscoresize(%rip)
	jge	.LBB4_98
# BB#94:
	movl	%r14d, Falign_udpari_long.crossscoresize(%rip)
	cmpl	$0, fftkeika(%rip)
	jne	.LBB4_248
.LBB4_95:
	movq	Falign_udpari_long.crossscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_97
# BB#96:
	callq	FreeDoubleMtx
.LBB4_97:
	movl	Falign_udpari_long.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, Falign_udpari_long.crossscore(%rip)
.LBB4_98:                               # %.preheader432
	cmpl	$-1, %r15d
	jl	.LBB4_106
# BB#99:                                # %.preheader431.us.preheader
	movq	Falign_udpari_long.crossscore(%rip), %r12
	incl	%r15d
	leaq	8(,%r15,8), %r15
	movslq	%r14d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_100:                              # %.preheader431.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB4_100
# BB#101:                               # %.preheader430
	cmpl	$0, 12(%rsp)
	movq	48(%rsp), %r12          # 8-byte Reload
	jle	.LBB4_106
# BB#102:                               # %.lr.ph494
	movq	Falign_udpari_long.segment1(%rip), %rax
	movq	Falign_udpari_long.crossscore(%rip), %r8
	movq	Falign_udpari_long.sortedseg1(%rip), %rdx
	movq	Falign_udpari_long.cut1(%rip), %rdi
	movq	Falign_udpari_long.sortedseg2(%rip), %rcx
	movq	Falign_udpari_long.cut2(%rip), %rsi
	addq	$40, %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_103:                              # =>This Inner Loop Header: Depth=1
	movslq	(%rax), %rbx
	movq	8(%r8,%rbx,8), %r9
	movq	-24(%rax), %r10
	movq	-8(%rax), %rbx
	movslq	40(%rbx), %rbx
	movq	%r10, 8(%r9,%rbx,8)
	movq	(%rdx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rdi,%rbp,4)
	movq	(%rcx,%rbp,8), %rbx
	movl	8(%rbx), %ebx
	movl	%ebx, 4(%rsi,%rbp,4)
	incq	%rbp
	movslq	12(%rsp), %rbx
	addq	$48, %rax
	cmpq	%rbx, %rbp
	jl	.LBB4_103
	jmp	.LBB4_107
.LBB4_104:                              # %.preheader433.._crit_edge502_crit_edge
	movq	Falign_udpari_long.cut1(%rip), %rax
	movq	Falign_udpari_long.cut2(%rip), %rcx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB4_105:                              # %._crit_edge502
	movl	$0, (%rax)
	movl	$0, (%rcx)
	movslq	12(%rsp), %rdx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 4(%rax,%rdx,4)
	movslq	12(%rsp), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	%edx, 4(%rcx,%rax,4)
	addl	$2, 12(%rsp)
	testl	%r15d, %r15d
	jg	.LBB4_113
	jmp	.LBB4_118
.LBB4_106:                              # %.preheader430.._crit_edge495_crit_edge
	movq	Falign_udpari_long.crossscore(%rip), %r8
	movq	Falign_udpari_long.cut1(%rip), %rdi
	movq	Falign_udpari_long.cut2(%rip), %rsi
	movq	Falign_udpari_long.sortedseg1(%rip), %rdx
	movq	Falign_udpari_long.sortedseg2(%rip), %rcx
.LBB4_107:
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%r8), %rax
	movabsq	$4711630319722168320, %rbp # imm = 0x416312D000000000
	movq	%rbp, (%rax)
	movl	$0, (%rdi)
	movl	$0, (%rsi)
	movslq	12(%rsp), %rax
	movq	8(%r8,%rax,8), %rbx
	movq	%rbp, 8(%rbx,%rax,8)
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rdi,%rax,4)
	movslq	12(%rsp), %rax
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rsi,%rax,4)
	movl	12(%rsp), %ebx
	addl	$2, %ebx
	movl	%ebx, 12(%rsp)
	leaq	12(%rsp), %r9
	callq	blockAlign2
	movl	fftkeika(%rip), %eax
	cmpl	$0, kobetsubunkatsu(%rip)
	jne	.LBB4_109
# BB#108:                               # %._crit_edge495
	testl	%eax, %eax
	jne	.LBB4_188
.LBB4_109:
	testl	%eax, %eax
	je	.LBB4_112
.LBB4_110:
	cmpl	12(%rsp), %ebx
	jle	.LBB4_112
# BB#111:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, fftRepeatStop(%rip)
	jne	.LBB4_279
.LBB4_112:                              # %.preheader429
	testl	%r15d, %r15d
	jle	.LBB4_118
.LBB4_113:                              # %.lr.ph492
	movq	Falign_udpari_long.result1(%rip), %rcx
	movl	%r15d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB4_115
	.p2align	4, 0x90
.LBB4_114:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_114
.LBB4_115:                              # %.prol.loopexit755
	cmpq	$7, %rdx
	jb	.LBB4_118
# BB#116:                               # %.lr.ph492.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB4_117:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB4_117
.LBB4_118:                              # %.preheader428
	testl	%r14d, %r14d
	jle	.LBB4_124
# BB#119:                               # %.lr.ph489
	movq	Falign_udpari_long.result2(%rip), %rcx
	movl	%r14d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB4_121
	.p2align	4, 0x90
.LBB4_120:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbp
	movb	$0, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_120
.LBB4_121:                              # %.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB4_124
# BB#122:                               # %.lr.ph489.new
	subq	%rsi, %rax
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB4_123:                              # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB4_123
.LBB4_124:                              # %._crit_edge490
	movq	184(%rsp), %rcx
	movl	$-1, (%rcx)
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	cmpl	$2, 12(%rsp)
	jge	.LBB4_132
# BB#125:                               # %.preheader418
	testl	%r15d, %r15d
	jle	.LBB4_128
.LBB4_126:                              # %.lr.ph462.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_127:                              # %.lr.ph462
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	Falign_udpari_long.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_127
.LBB4_128:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB4_131
# BB#129:                               # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_130:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	movq	Falign_udpari_long.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_130
.LBB4_131:                              # %._crit_edge
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_132:                              # %.lr.ph487
	leal	-1(%r15), %eax
	incq	%rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leal	-1(%r14), %eax
	incq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r15d, %r12d
	movl	%r14d, %ebx
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movl	$-1, %eax
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	jmp	.LBB4_134
	.p2align	4, 0x90
.LBB4_133:                              # %.loopexit._crit_edge
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rcx
	movl	(%rcx), %eax
	movq	%rdx, %r14
	movl	%r15d, %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB4_134:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_148 Depth 2
                                        #     Child Loop BB4_155 Depth 2
                                        #     Child Loop BB4_166 Depth 2
                                        #     Child Loop BB4_169 Depth 2
	incl	%eax
	movl	%eax, (%rcx)
	movq	Falign_udpari_long.cut1(%rip), %rbp
	cmpl	$0, (%rbp,%r14,4)
	jne	.LBB4_139
# BB#135:                               # %.preheader427
                                        #   in Loop: Header=BB4_134 Depth=1
	testl	%r15d, %r15d
	jle	.LBB4_137
# BB#136:                               # %.lr.ph464
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.sgap1(%rip), %rdi
	movl	$111, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	memset
.LBB4_137:                              # %.preheader425
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_140
# BB#138:                               # %.lr.ph466
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.sgap2(%rip), %rdi
	movl	$111, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	memset
	jmp	.LBB4_140
	.p2align	4, 0x90
.LBB4_139:                              #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.sgap1(%rip), %rdi
	movq	Falign_udpari_long.tmpres1(%rip), %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	decl	%ebp
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	getkyokaigap
	movq	Falign_udpari_long.sgap2(%rip), %rdi
	movq	Falign_udpari_long.tmpres2(%rip), %rsi
	movl	%ebp, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
	movq	Falign_udpari_long.cut1(%rip), %rbp
.LBB4_140:                              # %.loopexit426
                                        #   in Loop: Header=BB4_134 Depth=1
	leaq	1(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	4(%rbp,%r14,4), %edx
	cmpl	80(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB4_145
# BB#141:                               # %.preheader424
                                        #   in Loop: Header=BB4_134 Depth=1
	testl	%r15d, %r15d
	jle	.LBB4_143
# BB#142:                               # %.lr.ph468
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.egap1(%rip), %rdi
	movl	$111, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	memset
.LBB4_143:                              # %.preheader422
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_146
# BB#144:                               # %.lr.ph470
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.egap2(%rip), %rdi
	movl	$111, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	memset
	jmp	.LBB4_146
	.p2align	4, 0x90
.LBB4_145:                              #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.egap1(%rip), %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %ecx
	callq	getkyokaigap
	movq	Falign_udpari_long.egap2(%rip), %rdi
	movq	Falign_udpari_long.cut2(%rip), %rax
	movl	4(%rax,%r14,4), %edx
	movq	%r13, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	getkyokaigap
.LBB4_146:                              # %.loopexit423
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	stderr(%rip), %rdi
	movl	12(%rsp), %ecx
	decl	%ecx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	56(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	testl	%r15d, %r15d
	movq	48(%rsp), %r15          # 8-byte Reload
	jle	.LBB4_149
# BB#147:                               # %.lr.ph473.preheader
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.tmpres1(%rip), %rcx
	movq	Falign_udpari_long.cut1(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_148:                              # %.lr.ph473
                                        #   Parent Loop BB4_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rbp,8), %rdi
	movslq	(%rax,%r14,4), %rcx
	movq	(%r15,%rbp,8), %rsi
	addq	%rcx, %rsi
	movslq	4(%rax,%r14,4), %rdx
	subq	%rcx, %rdx
	callq	strncpy
	movq	Falign_udpari_long.tmpres1(%rip), %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	Falign_udpari_long.cut1(%rip), %rax
	movslq	4(%rax,%r14,4), %rsi
	movslq	(%rax,%r14,4), %rdi
	subq	%rdi, %rsi
	movb	$0, (%rdx,%rsi)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB4_148
.LBB4_149:                              # %._crit_edge474
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB4_152
# BB#150:                               # %._crit_edge474
                                        #   in Loop: Header=BB4_134 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_152
# BB#151:                               #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.tmpres1(%rip), %rsi
	movl	%r15d, %edi
	callq	commongappick
.LBB4_152:                              # %.preheader421
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_157
# BB#153:                               # %.lr.ph476.preheader
                                        #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.cut2(%rip), %rax
	xorl	%ebp, %ebp
	jmp	.LBB4_155
.LBB4_154:                              #   in Loop: Header=BB4_155 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	Falign_udpari_long.cut2(%rip), %rax
	movl	(%rax,%r14,4), %ecx
	movl	4(%rax,%r14,4), %edx
	jmp	.LBB4_156
	.p2align	4, 0x90
.LBB4_155:                              # %.lr.ph476
                                        #   Parent Loop BB4_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rax,%r14,4), %edx
	movl	(%rax,%r14,4), %ecx
	cmpl	%ecx, %edx
	jle	.LBB4_154
.LBB4_156:                              #   in Loop: Header=BB4_155 Depth=2
	movq	Falign_udpari_long.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movslq	%ecx, %rax
	movq	(%r13,%rbp,8), %rsi
	addq	%rax, %rsi
	subl	%eax, %edx
	movslq	%edx, %rdx
	callq	strncpy
	movq	Falign_udpari_long.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	Falign_udpari_long.cut2(%rip), %rax
	movslq	4(%rax,%r14,4), %rdx
	movslq	(%rax,%r14,4), %rsi
	subq	%rsi, %rdx
	movb	$0, (%rcx,%rdx)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_155
.LBB4_157:                              # %._crit_edge477
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, kobetsubunkatsu(%rip)
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB4_160
# BB#158:                               # %._crit_edge477
                                        #   in Loop: Header=BB4_134 Depth=1
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_160
# BB#159:                               #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.tmpres2(%rip), %rsi
	movl	%r14d, %edi
	callq	commongappick
.LBB4_160:                              #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, constraint(%rip)
	movl	176(%rsp), %ebp
	jne	.LBB4_278
# BB#161:                               #   in Loop: Header=BB4_134 Depth=1
	movsbl	alg(%rip), %edx
	cmpl	$77, %edx
	jne	.LBB4_171
# BB#162:                               #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.tmpres1(%rip), %rdi
	movq	Falign_udpari_long.tmpres2(%rip), %rsi
	subq	$8, %rsp
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	pushq	Falign_udpari_long.egap2(%rip)
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_udpari_long.egap1(%rip)
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_udpari_long.sgap2(%rip)
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	pushq	Falign_udpari_long.sgap1(%rip)
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm
	addq	$48, %rsp
.Lcfi176:
	.cfi_adjust_cfa_offset -48
	movss	44(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
.LBB4_163:                              #   in Loop: Header=BB4_134 Depth=1
	movq	Falign_udpari_long.tmpres1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %r14
	movq	88(%rsp), %rdx          # 8-byte Reload
	leal	(%r14,%rdx), %r15d
	cmpl	%ebp, %r15d
	jg	.LBB4_172
# BB#164:                               # %.preheader420
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_167
.LBB4_165:                              # %.lr.ph479.preheader
                                        #   in Loop: Header=BB4_134 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_166:                              # %.lr.ph479
                                        #   Parent Loop BB4_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_udpari_long.result1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Falign_udpari_long.tmpres1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB4_166
.LBB4_167:                              # %.preheader419
                                        #   in Loop: Header=BB4_134 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_170
# BB#168:                               # %.lr.ph481.preheader
                                        #   in Loop: Header=BB4_134 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_169:                              # %.lr.ph481
                                        #   Parent Loop BB4_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_udpari_long.result2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	Falign_udpari_long.tmpres2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcat
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_169
.LBB4_170:                              # %.loopexit
                                        #   in Loop: Header=BB4_134 Depth=1
	movslq	12(%rsp), %rax
	decq	%rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	cmpq	%rax, %rdx
	jl	.LBB4_133
	jmp	.LBB4_173
.LBB4_171:                              #   in Loop: Header=BB4_134 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.8, %edi
	callq	ErrorExit
	jmp	.LBB4_163
.LBB4_172:                              #   in Loop: Header=BB4_134 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	callq	fprintf
	movl	$.L.str.9, %edi
	callq	ErrorExit
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jg	.LBB4_165
	jmp	.LBB4_167
.LBB4_173:
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	jg	.LBB4_126
	jmp	.LBB4_128
.LBB4_174:                              # %.preheader449
	movq	32(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB4_198
# BB#175:                               # %.lr.ph550
	movq	Falign_udpari_long.seqVector1(%rip), %rax
	movq	Falign_udpari_long.tmpseq1(%rip), %rcx
	movq	(%rax), %rbx
	movl	%edx, %esi
	addq	$8, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_176:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_178 Depth 2
	movq	(%rcx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB4_187
# BB#177:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_176 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB4_178:                              # %.lr.ph.i
                                        #   Parent Loop BB4_176 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	$-97, %al
	cmpb	$19, %al
	ja	.LBB4_186
# BB#179:                               # %.lr.ph.i
                                        #   in Loop: Header=BB4_178 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_180:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	jmp	.LBB4_185
	.p2align	4, 0x90
.LBB4_181:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	jmp	.LBB4_183
	.p2align	4, 0x90
.LBB4_182:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
.LBB4_183:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	%xmm1, (%rdx)
	jmp	.LBB4_186
	.p2align	4, 0x90
.LBB4_184:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
.LBB4_185:                              #   in Loop: Header=BB4_178 Depth=2
	movsd	%xmm1, -8(%rdx)
.LBB4_186:                              #   in Loop: Header=BB4_178 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rdx
	incq	%rbp
	testb	%al, %al
	jne	.LBB4_178
.LBB4_187:                              # %seq_vec_4.exit
                                        #   in Loop: Header=BB4_176 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB4_176
	jmp	.LBB4_198
.LBB4_188:
	movq	stderr(%rip), %rdi
	movl	12(%rsp), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	fftkeika(%rip), %eax
	testl	%eax, %eax
	jne	.LBB4_110
	jmp	.LBB4_112
.LBB4_189:                              # %.preheader450
	testl	%esi, %esi
	jle	.LBB4_198
# BB#190:                               # %.lr.ph552
	movq	Falign_udpari_long.seqVector1(%rip), %rcx
	movq	Falign_udpari_long.tmpseq1(%rip), %rdx
	movl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_191:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_193 Depth 2
	movq	(%rdx,%rdi,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB4_197
# BB#192:                               # %.lr.ph.i407.preheader
                                        #   in Loop: Header=BB4_191 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_193:                              # %.lr.ph.i407
                                        #   Parent Loop BB4_191 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	movslq	amino_n(,%rax,4), %rax
	testq	%rax, %rax
	js	.LBB4_196
# BB#194:                               # %.lr.ph.i407
                                        #   in Loop: Header=BB4_193 Depth=2
	cmpl	%r14d, %eax
	jge	.LBB4_196
# BB#195:                               #   in Loop: Header=BB4_193 Depth=2
	movq	(%rcx,%rax,8), %rax
	movsd	(%rax,%rbx), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rbx)
.LBB4_196:                              #   in Loop: Header=BB4_193 Depth=2
	movzbl	(%rbp), %eax
	addq	$16, %rbx
	incq	%rbp
	testb	%al, %al
	jne	.LBB4_193
.LBB4_197:                              # %seq_vec_3.exit412
                                        #   in Loop: Header=BB4_191 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jne	.LBB4_191
.LBB4_198:                              # %.preheader448
	testl	%r14d, %r14d
	jle	.LBB4_205
# BB#199:                               # %.lr.ph547
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB4_206
# BB#200:                               # %.lr.ph547.split.preheader
	leal	-1(%rax), %r15d
	shlq	$4, %r15
	addq	$16, %r15
	movq	Falign_udpari_long.seqVector2(%rip), %rbx
	leaq	-1(%r14), %r12
	movq	%r14, %r13
	xorl	%ebp, %ebp
	andq	$7, %r13
	je	.LBB4_202
	.p2align	4, 0x90
.LBB4_201:                              # %.lr.ph547.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB4_201
.LBB4_202:                              # %.lr.ph547.split.prol.loopexit
	cmpq	$7, %r12
	jb	.LBB4_204
	.p2align	4, 0x90
.LBB4_203:                              # %.lr.ph547.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbp
	cmpq	%r14, %rbp
	jl	.LBB4_203
.LBB4_204:                              # %._crit_edge548.loopexit569
	movl	scoremtx(%rip), %r8d
.LBB4_205:                              # %._crit_edge548
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB4_206:                              # %._crit_edge548
	movl	%eax, %r15d
	shrl	$31, %r15d
	cmpl	$-1, %r8d
	je	.LBB4_216
# BB#207:
	cmpl	$0, fftscore(%rip)
	je	.LBB4_230
# BB#208:                               # %.preheader446
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_239
# BB#209:                               # %.lr.ph545
	movq	Falign_udpari_long.seqVector2(%rip), %rcx
	movq	Falign_udpari_long.tmpseq2(%rip), %rax
	movq	(%rcx), %rbp
	movl	16(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_210:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_212 Depth 2
	movq	(%rax,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB4_215
# BB#211:                               # %.lr.ph.i399.preheader
                                        #   in Loop: Header=BB4_210 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB4_212:                              # %.lr.ph.i399
                                        #   Parent Loop BB4_210 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rbx
	movslq	amino_n(,%rbx,4), %rbx
	cmpq	$20, %rbx
	jg	.LBB4_214
# BB#213:                               #   in Loop: Header=BB4_212 Depth=2
	movsd	polarity(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rcx), %xmm1
	movsd	%xmm1, -8(%rcx)
	movsd	volume(,%rbx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
.LBB4_214:                              #   in Loop: Header=BB4_212 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rcx
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB4_212
.LBB4_215:                              # %seq_vec_5.exit400
                                        #   in Loop: Header=BB4_210 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB4_210
	jmp	.LBB4_239
.LBB4_216:                              # %.preheader443
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_239
# BB#217:                               # %.lr.ph541
	movq	Falign_udpari_long.seqVector2(%rip), %rcx
	movq	Falign_udpari_long.tmpseq2(%rip), %rax
	movq	(%rcx), %rbp
	movl	16(%rsp), %edx          # 4-byte Reload
	addq	$8, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_218:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_220 Depth 2
	movq	(%rax,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB4_229
# BB#219:                               # %.lr.ph.i403.preheader
                                        #   in Loop: Header=BB4_218 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB4_220:                              # %.lr.ph.i403
                                        #   Parent Loop BB4_218 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB4_228
# BB#221:                               # %.lr.ph.i403
                                        #   in Loop: Header=BB4_220 Depth=2
	movzbl	%bl, %ebx
	jmpq	*.LJTI4_1(,%rbx,8)
.LBB4_222:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	jmp	.LBB4_227
	.p2align	4, 0x90
.LBB4_223:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	jmp	.LBB4_225
	.p2align	4, 0x90
.LBB4_224:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
.LBB4_225:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	%xmm1, (%rcx)
	jmp	.LBB4_228
	.p2align	4, 0x90
.LBB4_226:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
.LBB4_227:                              #   in Loop: Header=BB4_220 Depth=2
	movsd	%xmm1, -8(%rcx)
.LBB4_228:                              #   in Loop: Header=BB4_220 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rcx
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB4_220
.LBB4_229:                              # %seq_vec_4.exit404
                                        #   in Loop: Header=BB4_218 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB4_218
	jmp	.LBB4_239
.LBB4_230:                              # %.preheader444
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_239
# BB#231:                               # %.lr.ph543
	movq	Falign_udpari_long.seqVector2(%rip), %rax
	movq	Falign_udpari_long.tmpseq2(%rip), %rcx
	movl	16(%rsp), %edx          # 4-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_232:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_234 Depth 2
	movq	(%rcx,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB4_238
# BB#233:                               # %.lr.ph.i396.preheader
                                        #   in Loop: Header=BB4_232 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_234:                              # %.lr.ph.i396
                                        #   Parent Loop BB4_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rbx
	movslq	amino_n(,%rbx,4), %rbx
	testq	%rbx, %rbx
	js	.LBB4_237
# BB#235:                               # %.lr.ph.i396
                                        #   in Loop: Header=BB4_234 Depth=2
	cmpl	%r14d, %ebx
	jge	.LBB4_237
# BB#236:                               #   in Loop: Header=BB4_234 Depth=2
	movq	(%rax,%rbx,8), %rbx
	movsd	(%rbx,%rbp), %xmm1      # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rbp)
.LBB4_237:                              #   in Loop: Header=BB4_234 Depth=2
	movzbl	(%rdi), %ebx
	addq	$16, %rbp
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB4_234
.LBB4_238:                              # %seq_vec_3.exit
                                        #   in Loop: Header=BB4_232 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB4_232
.LBB4_239:                              # %.preheader442
	addl	24(%rsp), %r15d         # 4-byte Folded Reload
	testl	%r14d, %r14d
	jle	.LBB4_253
# BB#240:                               # %.lr.ph539.preheader
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_241:                              # %.lr.ph539
                                        # =>This Inner Loop Header: Depth=1
	movq	Falign_udpari_long.seqVector2(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	testq	%rbx, %rbx
	sete	%dl
	movl	%ebp, %edi
	callq	fft
	movq	Falign_udpari_long.seqVector1(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	fft
	incq	%rbx
	movslq	n20or4or2(%rip), %r14
	cmpq	%r14, %rbx
	jl	.LBB4_241
# BB#242:                               # %.preheader441
	testl	%r14d, %r14d
	jle	.LBB4_253
# BB#243:                               # %.preheader440.lr.ph
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB4_249
# BB#244:                               # %.preheader440.us.preheader
	movl	%eax, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_245:                              # %.preheader440.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_246 Depth 2
	movq	%r12, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_246:                              #   Parent Loop BB4_245 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Falign_udpari_long.naiseki(%rip), %rax
	movq	(%rax,%r13,8), %rdi
	addq	%rbp, %rdi
	movq	Falign_udpari_long.seqVector1(%rip), %rax
	movq	(%rax,%r13,8), %rsi
	addq	%rbp, %rsi
	movq	Falign_udpari_long.seqVector2(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	addq	%rbp, %rdx
	callq	calcNaiseki
	addq	$16, %rbp
	decq	%rbx
	jne	.LBB4_246
# BB#247:                               # %._crit_edge536.us
                                        #   in Loop: Header=BB4_245 Depth=1
	incq	%r13
	movslq	n20or4or2(%rip), %r14
	cmpq	%r14, %r13
	jl	.LBB4_245
	jmp	.LBB4_253
.LBB4_248:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	jmp	.LBB4_95
.LBB4_249:                              # %.preheader440.preheader
	leal	-1(%r14), %ecx
	movl	%r14d, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB4_251
.LBB4_250:                              # %.preheader440.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB4_250
.LBB4_251:                              # %.preheader440.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB4_253
.LBB4_252:                              # %.preheader440
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	%r14d, %eax
	jl	.LBB4_252
.LBB4_253:                              # %.preheader439
	sarl	%r15d
	movq	Falign_udpari_long.naisekiNoWa(%rip), %r12
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_264
# BB#254:                               # %.lr.ph532
	testl	%r14d, %r14d
	jle	.LBB4_265
# BB#255:                               # %.lr.ph532.split.us.preheader
	movq	Falign_udpari_long.naiseki(%rip), %rax
	movl	%r14d, %r9d
	movl	24(%rsp), %r10d         # 4-byte Reload
	leaq	-1(%r9), %r11
	movl	%r9d, %edi
	andl	$3, %edi
	leaq	24(%rax), %r8
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_256:                              # %.lr.ph532.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_258 Depth 2
                                        #     Child Loop BB4_262 Depth 2
	movq	%r14, %rbx
	shlq	$4, %rbx
	leaq	(%r12,%rbx), %rdx
	testq	%rdi, %rdi
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r12,%rbx)
	je	.LBB4_259
# BB#257:                               # %.prol.preheader764
                                        #   in Loop: Header=BB4_256 Depth=1
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_258:                              #   Parent Loop BB4_256 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,8), %rsi
	movupd	(%rsi,%rbx), %xmm1
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	incq	%rbp
	cmpq	%rbp, %rdi
	jne	.LBB4_258
	jmp	.LBB4_260
	.p2align	4, 0x90
.LBB4_259:                              #   in Loop: Header=BB4_256 Depth=1
	xorl	%ebp, %ebp
.LBB4_260:                              # %.prol.loopexit765
                                        #   in Loop: Header=BB4_256 Depth=1
	cmpq	$3, %r11
	jb	.LBB4_263
# BB#261:                               # %.lr.ph532.split.us.new
                                        #   in Loop: Header=BB4_256 Depth=1
	movq	%r9, %rsi
	subq	%rbp, %rsi
	leaq	(%r8,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB4_262:                              #   Parent Loop BB4_256 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	-16(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	movq	-8(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	movq	(%rbp), %rcx
	movupd	(%rcx,%rbx), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdx)
	addq	$32, %rbp
	addq	$-4, %rsi
	jne	.LBB4_262
.LBB4_263:                              # %._crit_edge530.us
                                        #   in Loop: Header=BB4_256 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB4_256
.LBB4_264:                              # %._crit_edge533
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	fft
	cmpl	$-1, %r14d
	jge	.LBB4_266
	jmp	.LBB4_268
.LBB4_265:                              # %._crit_edge533.thread
	movq	24(%rsp), %r14          # 8-byte Reload
	leal	-1(%r14), %edx
	shlq	$4, %rdx
	addq	$16, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
	movl	%r14d, %edi
	negl	%edi
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	fft
.LBB4_266:                              # %.lr.ph526
	movq	Falign_udpari_long.soukan(%rip), %rax
	movslq	%r15d, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	addq	Falign_udpari_long.naisekiNoWa(%rip), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB4_267:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdi
	movq	%rdi, 8(%rax,%rsi,8)
	incq	%rsi
	addq	$-16, %rdx
	cmpq	%rsi, %rcx
	jg	.LBB4_267
.LBB4_268:                              # %.preheader438
	leal	1(%r15), %ecx
	cmpl	%r14d, %ecx
	jge	.LBB4_275
# BB#269:                               # %.lr.ph522
	movq	Falign_udpari_long.naisekiNoWa(%rip), %rax
	movq	Falign_udpari_long.soukan(%rip), %rdx
	movslq	%ecx, %rcx
	leal	3(%r14), %edi
	subl	%r15d, %edi
	leal	-2(%r14), %esi
	subl	%r15d, %esi
	andl	$3, %edi
	je	.LBB4_272
# BB#270:                               # %.prol.preheader759
	leal	-1(%r14), %ebx
	negl	%edi
	.p2align	4, 0x90
.LBB4_271:                              # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movq	%rbx, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rdx,%rcx,8)
	incq	%rcx
	decl	%ebx
	incl	%edi
	jne	.LBB4_271
.LBB4_272:                              # %.prol.loopexit760
	cmpl	$3, %esi
	jb	.LBB4_276
# BB#273:                               # %.lr.ph522.new
	leaq	24(%rdx,%rcx,8), %rsi
	movq	%rcx, %rdi
	addq	$3, %rdi
	movl	%r15d, %r8d
	subl	%edi, %r8d
	leal	2(%rcx), %edi
	movl	%r15d, %r9d
	subl	%edi, %r9d
	movl	%r15d, %ebx
	subl	%ecx, %ebx
	leal	1(%rcx), %edi
	subl	%edi, %r15d
	movl	%r14d, %edi
	.p2align	4, 0x90
.LBB4_274:                              # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -24(%rsi)
	leal	(%r15,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -16(%rsi)
	leal	(%r9,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, -8(%rsi)
	leal	(%r8,%rdi), %ebp
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	movq	(%rax,%rbp), %rbp
	movq	%rbp, (%rsi)
	addl	$-4, %edi
	addq	$32, %rsi
	cmpl	%edi, %ecx
	jne	.LBB4_274
	jmp	.LBB4_276
.LBB4_275:                              # %.preheader438.._crit_edge523_crit_edge
	movq	Falign_udpari_long.soukan(%rip), %rdx
.LBB4_276:                              # %._crit_edge523
	movq	Falign_udpari_long.kouho(%rip), %rdi
	movl	$100, %esi
	movl	%r14d, %ecx
	callq	getKouho
	cmpl	$0, kobetsubunkatsu(%rip)
	movl	$0, 12(%rsp)
	jne	.LBB4_33
# BB#277:
	testl	%eax, %eax
	jg	.LBB4_34
	jmp	.LBB4_47
.LBB4_278:
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB4_279:
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	Falign_udpari_long, .Lfunc_end4-Falign_udpari_long
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_180
	.quad	.LBB4_186
	.quad	.LBB4_181
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_182
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_186
	.quad	.LBB4_184
.LJTI4_1:
	.quad	.LBB4_222
	.quad	.LBB4_228
	.quad	.LBB4_223
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_224
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_228
	.quad	.LBB4_226

	.type	Fgetlag.crossscoresize,@object # @Fgetlag.crossscoresize
	.local	Fgetlag.crossscoresize
	.comm	Fgetlag.crossscoresize,4,4
	.type	Fgetlag.tmpseq1,@object # @Fgetlag.tmpseq1
	.local	Fgetlag.tmpseq1
	.comm	Fgetlag.tmpseq1,8,8
	.type	Fgetlag.tmpseq2,@object # @Fgetlag.tmpseq2
	.local	Fgetlag.tmpseq2
	.comm	Fgetlag.tmpseq2,8,8
	.type	Fgetlag.tmpptr1,@object # @Fgetlag.tmpptr1
	.local	Fgetlag.tmpptr1
	.comm	Fgetlag.tmpptr1,8,8
	.type	Fgetlag.tmpptr2,@object # @Fgetlag.tmpptr2
	.local	Fgetlag.tmpptr2
	.comm	Fgetlag.tmpptr2,8,8
	.type	Fgetlag.tmpres1,@object # @Fgetlag.tmpres1
	.local	Fgetlag.tmpres1
	.comm	Fgetlag.tmpres1,8,8
	.type	Fgetlag.tmpres2,@object # @Fgetlag.tmpres2
	.local	Fgetlag.tmpres2
	.comm	Fgetlag.tmpres2,8,8
	.type	Fgetlag.result1,@object # @Fgetlag.result1
	.local	Fgetlag.result1
	.comm	Fgetlag.result1,8,8
	.type	Fgetlag.result2,@object # @Fgetlag.result2
	.local	Fgetlag.result2
	.comm	Fgetlag.result2,8,8
	.type	Fgetlag.seqVector1,@object # @Fgetlag.seqVector1
	.local	Fgetlag.seqVector1
	.comm	Fgetlag.seqVector1,8,8
	.type	Fgetlag.seqVector2,@object # @Fgetlag.seqVector2
	.local	Fgetlag.seqVector2
	.comm	Fgetlag.seqVector2,8,8
	.type	Fgetlag.naiseki,@object # @Fgetlag.naiseki
	.local	Fgetlag.naiseki
	.comm	Fgetlag.naiseki,8,8
	.type	Fgetlag.naisekiNoWa,@object # @Fgetlag.naisekiNoWa
	.local	Fgetlag.naisekiNoWa
	.comm	Fgetlag.naisekiNoWa,8,8
	.type	Fgetlag.soukan,@object  # @Fgetlag.soukan
	.local	Fgetlag.soukan
	.comm	Fgetlag.soukan,8,8
	.type	Fgetlag.crossscore,@object # @Fgetlag.crossscore
	.local	Fgetlag.crossscore
	.comm	Fgetlag.crossscore,8,8
	.type	Fgetlag.kouho,@object   # @Fgetlag.kouho
	.local	Fgetlag.kouho
	.comm	Fgetlag.kouho,8,8
	.type	Fgetlag.segment,@object # @Fgetlag.segment
	.local	Fgetlag.segment
	.comm	Fgetlag.segment,8,8
	.type	Fgetlag.segment1,@object # @Fgetlag.segment1
	.local	Fgetlag.segment1
	.comm	Fgetlag.segment1,8,8
	.type	Fgetlag.segment2,@object # @Fgetlag.segment2
	.local	Fgetlag.segment2
	.comm	Fgetlag.segment2,8,8
	.type	Fgetlag.sortedseg1,@object # @Fgetlag.sortedseg1
	.local	Fgetlag.sortedseg1
	.comm	Fgetlag.sortedseg1,8,8
	.type	Fgetlag.sortedseg2,@object # @Fgetlag.sortedseg2
	.local	Fgetlag.sortedseg2
	.comm	Fgetlag.sortedseg2,8,8
	.type	Fgetlag.cut1,@object    # @Fgetlag.cut1
	.local	Fgetlag.cut1
	.comm	Fgetlag.cut1,8,8
	.type	Fgetlag.cut2,@object    # @Fgetlag.cut2
	.local	Fgetlag.cut2
	.comm	Fgetlag.cut2,8,8
	.type	Fgetlag.localalloclen,@object # @Fgetlag.localalloclen
	.local	Fgetlag.localalloclen
	.comm	Fgetlag.localalloclen,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Allocation error\n"
	.size	.L.str, 18

	.type	n20or4or2,@object       # @n20or4or2
	.local	n20or4or2
	.comm	n20or4or2,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" FFT ... "
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"TOO MANY SEGMENTS.\n"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"done. (%d anchors)\r"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Cannot detect anchor!"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"####################################################################################################################################allocating crossscore, size = %d\n"
	.size	.L.str.5, 166

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"REPEAT!? \n"
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"alg = %c\n"
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ERROR IN SOURCE FILE Falign.c"
	.size	.L.str.8, 30

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"LENGTH OVER in Falign\n "
	.size	.L.str.9, 24

	.type	Falign.prevalloclen,@object # @Falign.prevalloclen
	.local	Falign.prevalloclen
	.comm	Falign.prevalloclen,4,4
	.type	Falign.crossscoresize,@object # @Falign.crossscoresize
	.local	Falign.crossscoresize
	.comm	Falign.crossscoresize,4,4
	.type	Falign.tmpseq1,@object  # @Falign.tmpseq1
	.local	Falign.tmpseq1
	.comm	Falign.tmpseq1,8,8
	.type	Falign.tmpseq2,@object  # @Falign.tmpseq2
	.local	Falign.tmpseq2
	.comm	Falign.tmpseq2,8,8
	.type	Falign.tmpptr1,@object  # @Falign.tmpptr1
	.local	Falign.tmpptr1
	.comm	Falign.tmpptr1,8,8
	.type	Falign.tmpptr2,@object  # @Falign.tmpptr2
	.local	Falign.tmpptr2
	.comm	Falign.tmpptr2,8,8
	.type	Falign.tmpres1,@object  # @Falign.tmpres1
	.local	Falign.tmpres1
	.comm	Falign.tmpres1,8,8
	.type	Falign.tmpres2,@object  # @Falign.tmpres2
	.local	Falign.tmpres2
	.comm	Falign.tmpres2,8,8
	.type	Falign.result1,@object  # @Falign.result1
	.local	Falign.result1
	.comm	Falign.result1,8,8
	.type	Falign.result2,@object  # @Falign.result2
	.local	Falign.result2
	.comm	Falign.result2,8,8
	.type	Falign.seqVector1,@object # @Falign.seqVector1
	.local	Falign.seqVector1
	.comm	Falign.seqVector1,8,8
	.type	Falign.seqVector2,@object # @Falign.seqVector2
	.local	Falign.seqVector2
	.comm	Falign.seqVector2,8,8
	.type	Falign.naiseki,@object  # @Falign.naiseki
	.local	Falign.naiseki
	.comm	Falign.naiseki,8,8
	.type	Falign.naisekiNoWa,@object # @Falign.naisekiNoWa
	.local	Falign.naisekiNoWa
	.comm	Falign.naisekiNoWa,8,8
	.type	Falign.soukan,@object   # @Falign.soukan
	.local	Falign.soukan
	.comm	Falign.soukan,8,8
	.type	Falign.crossscore,@object # @Falign.crossscore
	.local	Falign.crossscore
	.comm	Falign.crossscore,8,8
	.type	Falign.kouho,@object    # @Falign.kouho
	.local	Falign.kouho
	.comm	Falign.kouho,8,8
	.type	Falign.segment,@object  # @Falign.segment
	.local	Falign.segment
	.comm	Falign.segment,8,8
	.type	Falign.segment1,@object # @Falign.segment1
	.local	Falign.segment1
	.comm	Falign.segment1,8,8
	.type	Falign.segment2,@object # @Falign.segment2
	.local	Falign.segment2
	.comm	Falign.segment2,8,8
	.type	Falign.sortedseg1,@object # @Falign.sortedseg1
	.local	Falign.sortedseg1
	.comm	Falign.sortedseg1,8,8
	.type	Falign.sortedseg2,@object # @Falign.sortedseg2
	.local	Falign.sortedseg2
	.comm	Falign.sortedseg2,8,8
	.type	Falign.cut1,@object     # @Falign.cut1
	.local	Falign.cut1
	.comm	Falign.cut1,8,8
	.type	Falign.cut2,@object     # @Falign.cut2
	.local	Falign.cut2
	.comm	Falign.cut2,8,8
	.type	Falign.sgap1,@object    # @Falign.sgap1
	.local	Falign.sgap1
	.comm	Falign.sgap1,8,8
	.type	Falign.egap1,@object    # @Falign.egap1
	.local	Falign.egap1
	.comm	Falign.egap1,8,8
	.type	Falign.sgap2,@object    # @Falign.sgap2
	.local	Falign.sgap2
	.comm	Falign.sgap2,8,8
	.type	Falign.egap2,@object    # @Falign.egap2
	.local	Falign.egap2
	.comm	Falign.egap2,8,8
	.type	Falign.localalloclen,@object # @Falign.localalloclen
	.local	Falign.localalloclen
	.comm	Falign.localalloclen,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"######allocating crossscore, size = %d\n"
	.size	.L.str.10, 40

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d anchors found\n"
	.size	.L.str.11, 18

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Not supported\n"
	.size	.L.str.12, 15

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"totallen=%d +  nlen=%d > alloclen = %d\n"
	.size	.L.str.13, 40

	.type	Falign_noudp.prevalloclen,@object # @Falign_noudp.prevalloclen
	.local	Falign_noudp.prevalloclen
	.comm	Falign_noudp.prevalloclen,4,4
	.type	Falign_noudp.tmpseq1,@object # @Falign_noudp.tmpseq1
	.local	Falign_noudp.tmpseq1
	.comm	Falign_noudp.tmpseq1,8,8
	.type	Falign_noudp.tmpseq2,@object # @Falign_noudp.tmpseq2
	.local	Falign_noudp.tmpseq2
	.comm	Falign_noudp.tmpseq2,8,8
	.type	Falign_noudp.tmpptr1,@object # @Falign_noudp.tmpptr1
	.local	Falign_noudp.tmpptr1
	.comm	Falign_noudp.tmpptr1,8,8
	.type	Falign_noudp.tmpptr2,@object # @Falign_noudp.tmpptr2
	.local	Falign_noudp.tmpptr2
	.comm	Falign_noudp.tmpptr2,8,8
	.type	Falign_noudp.tmpres1,@object # @Falign_noudp.tmpres1
	.local	Falign_noudp.tmpres1
	.comm	Falign_noudp.tmpres1,8,8
	.type	Falign_noudp.tmpres2,@object # @Falign_noudp.tmpres2
	.local	Falign_noudp.tmpres2
	.comm	Falign_noudp.tmpres2,8,8
	.type	Falign_noudp.result1,@object # @Falign_noudp.result1
	.local	Falign_noudp.result1
	.comm	Falign_noudp.result1,8,8
	.type	Falign_noudp.result2,@object # @Falign_noudp.result2
	.local	Falign_noudp.result2
	.comm	Falign_noudp.result2,8,8
	.type	Falign_noudp.seqVector1,@object # @Falign_noudp.seqVector1
	.local	Falign_noudp.seqVector1
	.comm	Falign_noudp.seqVector1,8,8
	.type	Falign_noudp.seqVector2,@object # @Falign_noudp.seqVector2
	.local	Falign_noudp.seqVector2
	.comm	Falign_noudp.seqVector2,8,8
	.type	Falign_noudp.naiseki,@object # @Falign_noudp.naiseki
	.local	Falign_noudp.naiseki
	.comm	Falign_noudp.naiseki,8,8
	.type	Falign_noudp.naisekiNoWa,@object # @Falign_noudp.naisekiNoWa
	.local	Falign_noudp.naisekiNoWa
	.comm	Falign_noudp.naisekiNoWa,8,8
	.type	Falign_noudp.soukan,@object # @Falign_noudp.soukan
	.local	Falign_noudp.soukan
	.comm	Falign_noudp.soukan,8,8
	.type	Falign_noudp.kouho,@object # @Falign_noudp.kouho
	.local	Falign_noudp.kouho
	.comm	Falign_noudp.kouho,8,8
	.type	Falign_noudp.segment,@object # @Falign_noudp.segment
	.local	Falign_noudp.segment
	.comm	Falign_noudp.segment,8,8
	.type	Falign_noudp.segment1,@object # @Falign_noudp.segment1
	.local	Falign_noudp.segment1
	.comm	Falign_noudp.segment1,8,8
	.type	Falign_noudp.segment2,@object # @Falign_noudp.segment2
	.local	Falign_noudp.segment2
	.comm	Falign_noudp.segment2,8,8
	.type	Falign_noudp.sortedseg1,@object # @Falign_noudp.sortedseg1
	.local	Falign_noudp.sortedseg1
	.comm	Falign_noudp.sortedseg1,8,8
	.type	Falign_noudp.sortedseg2,@object # @Falign_noudp.sortedseg2
	.local	Falign_noudp.sortedseg2
	.comm	Falign_noudp.sortedseg2,8,8
	.type	Falign_noudp.cut1,@object # @Falign_noudp.cut1
	.local	Falign_noudp.cut1
	.comm	Falign_noudp.cut1,8,8
	.type	Falign_noudp.cut2,@object # @Falign_noudp.cut2
	.local	Falign_noudp.cut2
	.comm	Falign_noudp.cut2,8,8
	.type	Falign_noudp.sgap1,@object # @Falign_noudp.sgap1
	.local	Falign_noudp.sgap1
	.comm	Falign_noudp.sgap1,8,8
	.type	Falign_noudp.egap1,@object # @Falign_noudp.egap1
	.local	Falign_noudp.egap1
	.comm	Falign_noudp.egap1,8,8
	.type	Falign_noudp.sgap2,@object # @Falign_noudp.sgap2
	.local	Falign_noudp.sgap2
	.comm	Falign_noudp.sgap2,8,8
	.type	Falign_noudp.egap2,@object # @Falign_noudp.egap2
	.local	Falign_noudp.egap2
	.comm	Falign_noudp.egap2,8,8
	.type	Falign_noudp.localalloclen,@object # @Falign_noudp.localalloclen
	.local	Falign_noudp.localalloclen
	.comm	Falign_noudp.localalloclen,4,4
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"done. (%d anchors) "
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"DP %05d / %05d \b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
	.size	.L.str.15, 33

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"### cut2[i+1]=%d, cut2[i]=%d\n"
	.size	.L.str.16, 30

	.type	Falign_udpari_long.prevalloclen,@object # @Falign_udpari_long.prevalloclen
	.local	Falign_udpari_long.prevalloclen
	.comm	Falign_udpari_long.prevalloclen,4,4
	.type	Falign_udpari_long.crossscoresize,@object # @Falign_udpari_long.crossscoresize
	.local	Falign_udpari_long.crossscoresize
	.comm	Falign_udpari_long.crossscoresize,4,4
	.type	Falign_udpari_long.tmpseq1,@object # @Falign_udpari_long.tmpseq1
	.local	Falign_udpari_long.tmpseq1
	.comm	Falign_udpari_long.tmpseq1,8,8
	.type	Falign_udpari_long.tmpseq2,@object # @Falign_udpari_long.tmpseq2
	.local	Falign_udpari_long.tmpseq2
	.comm	Falign_udpari_long.tmpseq2,8,8
	.type	Falign_udpari_long.tmpptr1,@object # @Falign_udpari_long.tmpptr1
	.local	Falign_udpari_long.tmpptr1
	.comm	Falign_udpari_long.tmpptr1,8,8
	.type	Falign_udpari_long.tmpptr2,@object # @Falign_udpari_long.tmpptr2
	.local	Falign_udpari_long.tmpptr2
	.comm	Falign_udpari_long.tmpptr2,8,8
	.type	Falign_udpari_long.tmpres1,@object # @Falign_udpari_long.tmpres1
	.local	Falign_udpari_long.tmpres1
	.comm	Falign_udpari_long.tmpres1,8,8
	.type	Falign_udpari_long.tmpres2,@object # @Falign_udpari_long.tmpres2
	.local	Falign_udpari_long.tmpres2
	.comm	Falign_udpari_long.tmpres2,8,8
	.type	Falign_udpari_long.result1,@object # @Falign_udpari_long.result1
	.local	Falign_udpari_long.result1
	.comm	Falign_udpari_long.result1,8,8
	.type	Falign_udpari_long.result2,@object # @Falign_udpari_long.result2
	.local	Falign_udpari_long.result2
	.comm	Falign_udpari_long.result2,8,8
	.type	Falign_udpari_long.seqVector1,@object # @Falign_udpari_long.seqVector1
	.local	Falign_udpari_long.seqVector1
	.comm	Falign_udpari_long.seqVector1,8,8
	.type	Falign_udpari_long.seqVector2,@object # @Falign_udpari_long.seqVector2
	.local	Falign_udpari_long.seqVector2
	.comm	Falign_udpari_long.seqVector2,8,8
	.type	Falign_udpari_long.naiseki,@object # @Falign_udpari_long.naiseki
	.local	Falign_udpari_long.naiseki
	.comm	Falign_udpari_long.naiseki,8,8
	.type	Falign_udpari_long.naisekiNoWa,@object # @Falign_udpari_long.naisekiNoWa
	.local	Falign_udpari_long.naisekiNoWa
	.comm	Falign_udpari_long.naisekiNoWa,8,8
	.type	Falign_udpari_long.soukan,@object # @Falign_udpari_long.soukan
	.local	Falign_udpari_long.soukan
	.comm	Falign_udpari_long.soukan,8,8
	.type	Falign_udpari_long.crossscore,@object # @Falign_udpari_long.crossscore
	.local	Falign_udpari_long.crossscore
	.comm	Falign_udpari_long.crossscore,8,8
	.type	Falign_udpari_long.kouho,@object # @Falign_udpari_long.kouho
	.local	Falign_udpari_long.kouho
	.comm	Falign_udpari_long.kouho,8,8
	.type	Falign_udpari_long.segment,@object # @Falign_udpari_long.segment
	.local	Falign_udpari_long.segment
	.comm	Falign_udpari_long.segment,8,8
	.type	Falign_udpari_long.segment1,@object # @Falign_udpari_long.segment1
	.local	Falign_udpari_long.segment1
	.comm	Falign_udpari_long.segment1,8,8
	.type	Falign_udpari_long.segment2,@object # @Falign_udpari_long.segment2
	.local	Falign_udpari_long.segment2
	.comm	Falign_udpari_long.segment2,8,8
	.type	Falign_udpari_long.sortedseg1,@object # @Falign_udpari_long.sortedseg1
	.local	Falign_udpari_long.sortedseg1
	.comm	Falign_udpari_long.sortedseg1,8,8
	.type	Falign_udpari_long.sortedseg2,@object # @Falign_udpari_long.sortedseg2
	.local	Falign_udpari_long.sortedseg2
	.comm	Falign_udpari_long.sortedseg2,8,8
	.type	Falign_udpari_long.cut1,@object # @Falign_udpari_long.cut1
	.local	Falign_udpari_long.cut1
	.comm	Falign_udpari_long.cut1,8,8
	.type	Falign_udpari_long.cut2,@object # @Falign_udpari_long.cut2
	.local	Falign_udpari_long.cut2
	.comm	Falign_udpari_long.cut2,8,8
	.type	Falign_udpari_long.sgap1,@object # @Falign_udpari_long.sgap1
	.local	Falign_udpari_long.sgap1
	.comm	Falign_udpari_long.sgap1,8,8
	.type	Falign_udpari_long.egap1,@object # @Falign_udpari_long.egap1
	.local	Falign_udpari_long.egap1
	.comm	Falign_udpari_long.egap1,8,8
	.type	Falign_udpari_long.sgap2,@object # @Falign_udpari_long.sgap2
	.local	Falign_udpari_long.sgap2
	.comm	Falign_udpari_long.sgap2,8,8
	.type	Falign_udpari_long.egap2,@object # @Falign_udpari_long.egap2
	.local	Falign_udpari_long.egap2
	.comm	Falign_udpari_long.egap2,8,8
	.type	Falign_udpari_long.localalloclen,@object # @Falign_udpari_long.localalloclen
	.local	Falign_udpari_long.localalloclen
	.comm	Falign_udpari_long.localalloclen,4,4
	.type	mymergesort.i,@object   # @mymergesort.i
	.local	mymergesort.i
	.comm	mymergesort.i,4,4
	.type	mymergesort.j,@object   # @mymergesort.j
	.local	mymergesort.j
	.comm	mymergesort.j,4,4
	.type	mymergesort.k,@object   # @mymergesort.k
	.local	mymergesort.k
	.comm	mymergesort.k,4,4
	.type	mymergesort.p,@object   # @mymergesort.p
	.local	mymergesort.p
	.comm	mymergesort.p,4,4
	.type	mymergesort.allo,@object # @mymergesort.allo
	.local	mymergesort.allo
	.comm	mymergesort.allo,4,4
	.type	mymergesort.work,@object # @mymergesort.work
	.local	mymergesort.work
	.comm	mymergesort.work,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
