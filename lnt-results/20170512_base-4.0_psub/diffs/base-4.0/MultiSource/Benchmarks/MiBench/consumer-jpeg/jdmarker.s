	.text
	.file	"jdmarker.bc"
	.globl	jpeg_resync_to_restart
	.p2align	4, 0x90
	.type	jpeg_resync_to_restart,@function
jpeg_resync_to_restart:                 # @jpeg_resync_to_restart
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	524(%rbx), %ebp
	movq	(%rbx), %rax
	movl	$117, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	%r14d, 48(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	leal	1(%r14), %eax
	andl	$7, %eax
	orl	$208, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	2(%r14), %r12d
	andl	$7, %r12d
	orl	$208, %r12d
	leal	7(%r14), %r13d
	andl	$7, %r13d
	orl	$208, %r13d
	addl	$6, %r14d
	andl	$7, %r14d
	orl	$208, %r14d
	cmpl	$191, %ebp
	jg	.LBB0_5
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_11:
	movl	524(%rbx), %ebp
	cmpl	$191, %ebp
	jg	.LBB0_5
.LBB0_2:                                # %.outer.split.us.preheader
	movq	(%rbx), %rax
	movl	$96, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$2, 48(%rax)
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_5:                                # %.outer.split
	movl	%ebp, %eax
	andl	$-8, %eax
	cmpl	$208, %eax
	jne	.LBB0_12
# BB#6:                                 # %.outer.split.split.us.preheader
	xorl	%r15d, %r15d
	cmpl	%r14d, %ebp
	sete	%r15b
	incl	%r15d
	cmpl	%r13d, %ebp
	movl	$2, %eax
	cmovel	%eax, %r15d
	cmpl	%r12d, %ebp
	movl	$3, %eax
	cmovel	%eax, %r15d
	cmpl	4(%rsp), %ebp           # 4-byte Folded Reload
	cmovel	%eax, %r15d
	.p2align	4, 0x90
.LBB0_7:                                # %.outer.split.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movl	$96, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	%r15d, 48(%rax)
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	%r15d, %eax
	andb	$3, %al
	cmpb	$3, %al
	je	.LBB0_13
# BB#8:                                 # %.outer.split.split.us
                                        #   in Loop: Header=BB0_7 Depth=1
	cmpb	$2, %al
	je	.LBB0_3
# BB#9:                                 # %.outer.split.split.us
                                        #   in Loop: Header=BB0_7 Depth=1
	cmpb	$1, %al
	jne	.LBB0_7
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_3:                                # %.us-lcssa37.us
	movq	%rbx, %rdi
	callq	next_marker
	testl	%eax, %eax
	jne	.LBB0_11
# BB#4:
	xorl	%eax, %eax
	jmp	.LBB0_14
.LBB0_10:                               # %.us-lcssa.us
	movl	$0, 524(%rbx)
.LBB0_13:                               # %.loopexit.loopexit
	movl	$1, %eax
.LBB0_14:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:                               # %.outer.split.split.preheader
	movq	(%rbx), %rax
	movl	$96, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$3, 48(%rax)
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	jmp	.LBB0_13
.Lfunc_end0:
	.size	jpeg_resync_to_restart, .Lfunc_end0-jpeg_resync_to_restart
	.cfi_endproc

	.p2align	4, 0x90
	.type	next_marker,@function
next_marker:                            # @next_marker
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	32(%r14), %r15
	movq	(%r15), %r12
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_5
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_4:
	movq	(%r15), %r12
	movq	8(%r15), %rbx
.LBB1_5:                                # %.preheader66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
	decq	%rbx
	cmpb	$-1, (%r12)
	leaq	1(%r12), %r12
	je	.LBB1_9
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	568(%r14), %rax
	incl	172(%rax)
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	testq	%rbx, %rbx
	jne	.LBB1_5
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jne	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_9 Depth=2
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB1_8
# BB#11:                                #   in Loop: Header=BB1_9 Depth=2
	movq	(%r15), %r12
	movq	8(%r15), %rbx
.LBB1_12:                               #   in Loop: Header=BB1_9 Depth=2
	decq	%rbx
	movzbl	(%r12), %ebp
	incq	%r12
	cmpl	$255, %ebp
	je	.LBB1_9
# BB#1:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	568(%r14), %rcx
	movl	172(%rcx), %eax
	testb	%bpl, %bpl
	jne	.LBB1_13
# BB#2:                                 #   in Loop: Header=BB1_5 Depth=1
	addl	$2, %eax
	movl	%eax, 172(%rcx)
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	testq	%rbx, %rbx
	jne	.LBB1_5
	.p2align	4, 0x90
.LBB1_3:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	jne	.LBB1_4
.LBB1_8:
	xorl	%eax, %eax
.LBB1_16:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_13:
	testl	%eax, %eax
	je	.LBB1_15
# BB#14:
	movq	(%r14), %rcx
	movl	$112, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	%ebp, 48(%rcx)
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	*8(%rcx)
	movq	568(%r14), %rax
	movl	$0, 172(%rax)
.LBB1_15:
	movl	%ebp, 524(%r14)
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movl	$1, %eax
	jmp	.LBB1_16
.Lfunc_end1:
	.size	next_marker, .Lfunc_end1-next_marker
	.cfi_endproc

	.globl	jinit_marker_reader
	.p2align	4, 0x90
	.type	jinit_marker_reader,@function
jinit_marker_reader:                    # @jinit_marker_reader
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$176, %edx
	callq	*(%rax)
	movq	%rax, 568(%rbx)
	movq	$reset_marker_reader, (%rax)
	movl	$read_restart_marker, %ecx
	movd	%rcx, %xmm0
	movl	$read_markers, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rax)
	movl	$skip_variable, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, 24(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 40(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 48(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 56(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 64(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 72(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 80(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 88(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 96(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 104(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 112(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 120(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 128(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 136(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 144(%rax)
	movq	568(%rbx), %rax
	movq	$skip_variable, 152(%rax)
	movq	568(%rbx), %rax
	movq	$get_app0, 32(%rax)
	movq	568(%rbx), %rax
	movq	$get_app14, 144(%rax)
	movq	$0, 296(%rbx)
	movl	$0, 164(%rbx)
	movl	$0, 524(%rbx)
	movq	568(%rbx), %rax
	movl	$0, 160(%rax)
	movl	$0, 164(%rax)
	movl	$0, 172(%rax)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	jinit_marker_reader, .Lfunc_end2-jinit_marker_reader
	.cfi_endproc

	.p2align	4, 0x90
	.type	reset_marker_reader,@function
reset_marker_reader:                    # @reset_marker_reader
	.cfi_startproc
# BB#0:
	movq	$0, 296(%rdi)
	movl	$0, 164(%rdi)
	movl	$0, 524(%rdi)
	movq	568(%rdi), %rax
	movl	$0, 160(%rax)
	movl	$0, 164(%rax)
	movl	$0, 172(%rax)
	retq
.Lfunc_end3:
	.size	reset_marker_reader, .Lfunc_end3-reset_marker_reader
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_markers,@function
read_markers:                           # @read_markers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi31:
	.cfi_def_cfa_offset 384
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	360(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	524(%r15), %ebp
	testl	%ebp, %ebp
	jne	.LBB4_84
	jmp	.LBB4_70
	.p2align	4, 0x90
.LBB4_86:                               #   in Loop: Header=BB4_84 Depth=1
	movq	568(%r15), %rax
	movslq	%ebp, %rcx
	movq	%r15, %rdi
	callq	*-1760(%rax,%rcx,8)
	testl	%eax, %eax
	jne	.LBB4_69
	jmp	.LBB4_226
	.p2align	4, 0x90
.LBB4_87:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$91, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	jmp	.LBB4_69
	.p2align	4, 0x90
.LBB4_88:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$59, 40(%rax)
.LBB4_89:                               #   in Loop: Header=BB4_84 Depth=1
	movl	%ebp, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB4_69
.LBB4_90:                               #   in Loop: Header=BB4_84 Depth=1
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	jmp	.LBB4_93
.LBB4_91:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$67, 40(%rax)
	jmp	.LBB4_89
.LBB4_92:                               #   in Loop: Header=BB4_84 Depth=1
	xorl	%r13d, %r13d
	movl	$1, %esi
.LBB4_93:                               #   in Loop: Header=BB4_84 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_100
.LBB4_94:                               #   in Loop: Header=BB4_84 Depth=1
	movq	32(%r15), %r13
	movq	8(%r13), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_97
# BB#95:                                #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	je	.LBB4_226
# BB#96:                                #   in Loop: Header=BB4_84 Depth=1
	movq	8(%r13), %rbp
.LBB4_97:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r13), %r14
	movzbl	(%r14), %ebx
	decq	%rbp
	je	.LBB4_126
# BB#98:                                #   in Loop: Header=BB4_84 Depth=1
	incq	%r14
	jmp	.LBB4_128
.LBB4_99:                               #   in Loop: Header=BB4_84 Depth=1
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$1, %edx
.LBB4_100:                              #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	get_sof
	testl	%eax, %eax
	jne	.LBB4_69
	jmp	.LBB4_227
.LBB4_101:                              #   in Loop: Header=BB4_84 Depth=1
	movl	$1, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	get_sof
	testl	%eax, %eax
	jne	.LBB4_69
	jmp	.LBB4_226
.LBB4_102:                              #   in Loop: Header=BB4_84 Depth=1
	movq	32(%r15), %r12
	movq	8(%r12), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_105
# BB#103:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#104:                               #   in Loop: Header=BB4_84 Depth=1
	movq	8(%r12), %rbp
.LBB4_105:                              #   in Loop: Header=BB4_84 Depth=1
	movq	(%r12), %rbx
	movzbl	(%rbx), %r14d
	decq	%rbp
	je	.LBB4_2
# BB#106:                               #   in Loop: Header=BB4_84 Depth=1
	incq	%rbx
	jmp	.LBB4_4
.LBB4_107:                              #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$101, 40(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	568(%r15), %rax
	cmpl	$0, 160(%rax)
	je	.LBB4_109
# BB#108:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$60, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	movq	568(%r15), %rax
.LBB4_109:                              # %get_soi.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	movb	$0, 312(%r15)
	movb	$1, 328(%r15)
	movb	$5, 344(%r15)
	movb	$0, 313(%r15)
	movb	$1, 329(%r15)
	movb	$5, 345(%r15)
	movb	$0, 314(%r15)
	movb	$1, 330(%r15)
	movb	$5, 346(%r15)
	movb	$0, 315(%r15)
	movb	$1, 331(%r15)
	movb	$5, 347(%r15)
	movb	$0, 316(%r15)
	movb	$1, 332(%r15)
	movb	$5, 348(%r15)
	movb	$0, 317(%r15)
	movb	$1, 333(%r15)
	movb	$5, 349(%r15)
	movb	$0, 318(%r15)
	movb	$1, 334(%r15)
	movb	$5, 350(%r15)
	movb	$0, 319(%r15)
	movb	$1, 335(%r15)
	movb	$5, 351(%r15)
	movb	$0, 320(%r15)
	movb	$1, 336(%r15)
	movb	$5, 352(%r15)
	movb	$0, 321(%r15)
	movb	$1, 337(%r15)
	movb	$5, 353(%r15)
	movb	$0, 322(%r15)
	movb	$1, 338(%r15)
	movb	$5, 354(%r15)
	movb	$0, 323(%r15)
	movb	$1, 339(%r15)
	movb	$5, 355(%r15)
	movb	$0, 324(%r15)
	movb	$1, 340(%r15)
	movb	$5, 356(%r15)
	movb	$0, 325(%r15)
	movb	$1, 341(%r15)
	movb	$5, 357(%r15)
	movb	$0, 326(%r15)
	movb	$1, 342(%r15)
	movb	$5, 358(%r15)
	movb	$0, 327(%r15)
	movb	$1, 343(%r15)
	movb	$5, 359(%r15)
	movl	$0, 52(%r15)
	movl	$0, 384(%r15)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movb	$0, 8(%rcx)
	movq	$0, (%rcx)
	movw	$1, 370(%r15)
	movw	$1, 372(%r15)
	movl	$0, 376(%r15)
	movb	$0, 380(%r15)
	movl	$1, 160(%rax)
	jmp	.LBB4_69
.LBB4_110:                              #   in Loop: Header=BB4_84 Depth=1
	movq	32(%r15), %rbx
	movq	8(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_113
# BB#111:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB4_226
# BB#112:                               #   in Loop: Header=BB4_84 Depth=1
	movq	8(%rbx), %rbp
.LBB4_113:                              #   in Loop: Header=BB4_84 Depth=1
	movq	(%rbx), %r12
	movzbl	(%r12), %r14d
	decq	%rbp
	movq	%rbx, (%rsp)            # 8-byte Spill
	je	.LBB4_21
# BB#114:                               #   in Loop: Header=BB4_84 Depth=1
	incq	%r12
	jmp	.LBB4_23
.LBB4_115:                              #   in Loop: Header=BB4_84 Depth=1
	movq	32(%r15), %r14
	movq	8(%r14), %r12
	testq	%r12, %r12
	jne	.LBB4_118
# BB#116:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#117:                               #   in Loop: Header=BB4_84 Depth=1
	movq	8(%r14), %r12
.LBB4_118:                              #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbp
	movzbl	(%rbp), %r13d
	decq	%r12
	je	.LBB4_53
# BB#119:                               #   in Loop: Header=BB4_84 Depth=1
	incq	%rbp
	jmp	.LBB4_55
.LBB4_120:                              #   in Loop: Header=BB4_84 Depth=1
	movq	32(%r15), %r14
	movq	8(%r14), %r13
	testq	%r13, %r13
	jne	.LBB4_123
# BB#121:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#122:                               #   in Loop: Header=BB4_84 Depth=1
	movq	8(%r14), %r13
.LBB4_123:                              #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbx
	movzbl	(%rbx), %r12d
	decq	%r13
	je	.LBB4_56
# BB#124:                               #   in Loop: Header=BB4_84 Depth=1
	incq	%rbx
	jmp	.LBB4_58
.LBB4_125:                              #   in Loop: Header=BB4_84 Depth=1
	movq	568(%r15), %rax
	movq	%r15, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	jne	.LBB4_69
	jmp	.LBB4_226
.LBB4_126:                              #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	je	.LBB4_226
# BB#127:                               #   in Loop: Header=BB4_84 Depth=1
	movq	(%r13), %r14
	movq	8(%r13), %rbp
.LBB4_128:                              #   in Loop: Header=BB4_84 Depth=1
	shlq	$8, %rbx
	decq	%rbp
	movzbl	(%r14), %r12d
	incq	%r14
	orq	%rbx, %r12
	cmpq	$3, %r12
	jb	.LBB4_1
# BB#129:                               # %.lr.ph173.i
                                        #   in Loop: Header=BB4_84 Depth=1
	addq	$-2, %r12
.LBB4_130:                              #   Parent Loop BB4_84 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_134 Depth 3
                                        #       Child Loop BB4_143 Depth 3
	movq	%r12, (%rsp)            # 8-byte Spill
	testq	%rbp, %rbp
	jne	.LBB4_133
# BB#131:                               #   in Loop: Header=BB4_130 Depth=2
	movq	%r15, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	je	.LBB4_226
# BB#132:                               #   in Loop: Header=BB4_130 Depth=2
	movq	(%r13), %r14
	movq	8(%r13), %rbp
.LBB4_133:                              #   in Loop: Header=BB4_130 Depth=2
	movzbl	(%r14), %ecx
	movq	(%r15), %rax
	movl	$79, 40(%rax)
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movb	$0, 32(%rsp)
	incq	%r14
	decq	%rbp
	xorl	%r12d, %r12d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB4_134:                              #   Parent Loop BB4_84 Depth=1
                                        #     Parent Loop BB4_130 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	jne	.LBB4_137
# BB#135:                               #   in Loop: Header=BB4_134 Depth=3
	movq	%r15, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	je	.LBB4_226
# BB#136:                               #   in Loop: Header=BB4_134 Depth=3
	movq	(%r13), %r14
	movq	8(%r13), %rbp
.LBB4_137:                              #   in Loop: Header=BB4_134 Depth=3
	movzbl	(%r14), %eax
	movb	%al, 32(%rsp,%rbx)
	addl	%eax, %r12d
	incq	%rbx
	incq	%r14
	decq	%rbp
	cmpq	$17, %rbx
	jl	.LBB4_134
# BB#138:                               #   in Loop: Header=BB4_130 Depth=2
	movq	(%rsp), %rbx            # 8-byte Reload
	addq	$-17, %rbx
	movq	(%r15), %rax
	movd	33(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 44(%rax)
	movd	37(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 60(%rax)
	movl	$85, 40(%rax)
	movl	$2, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	(%r15), %rax
	movd	41(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 44(%rax)
	movd	45(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	pxor	%xmm1, %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 60(%rax)
	movl	$85, 40(%rax)
	movl	$2, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	cmpl	$256, %r12d             # imm = 0x100
	movslq	%r12d, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jg	.LBB4_140
# BB#139:                               #   in Loop: Header=BB4_130 Depth=2
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jge	.LBB4_141
.LBB4_140:                              #   in Loop: Header=BB4_130 Depth=2
	movq	(%r15), %rax
	movl	$28, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_141:                              # %.preheader.i
                                        #   in Loop: Header=BB4_130 Depth=2
	movq	%rbx, (%rsp)            # 8-byte Spill
	testl	%r12d, %r12d
	jle	.LBB4_147
# BB#142:                               # %.lr.ph.i57.preheader
                                        #   in Loop: Header=BB4_130 Depth=2
	xorl	%ebx, %ebx
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_143:                              # %.lr.ph.i57
                                        #   Parent Loop BB4_84 Depth=1
                                        #     Parent Loop BB4_130 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	jne	.LBB4_146
# BB#144:                               #   in Loop: Header=BB4_143 Depth=3
	movq	%r15, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	je	.LBB4_226
# BB#145:                               #   in Loop: Header=BB4_143 Depth=3
	movq	(%r13), %r14
	movq	8(%r13), %rbp
.LBB4_146:                              #   in Loop: Header=BB4_143 Depth=3
	decq	%rbp
	movzbl	(%r14), %eax
	incq	%r14
	movb	%al, 64(%rsp,%rbx)
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB4_143
.LBB4_147:                              # %._crit_edge.i59
                                        #   in Loop: Header=BB4_130 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	-16(%rdx), %rax
	movl	%edx, %ecx
	andl	$16, %ecx
	cmovel	%edx, %eax
	testl	%ecx, %ecx
	leaq	128(%r15,%rdx,8), %rcx
	leaq	224(%r15,%rdx,8), %rbx
	cmovneq	%rcx, %rbx
	cmpl	$4, %eax
	jb	.LBB4_149
# BB#148:                               #   in Loop: Header=BB4_130 Depth=2
	movq	(%r15), %rcx
	movl	$29, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB4_149:                              #   in Loop: Header=BB4_130 Depth=2
	movq	(%rsp), %r12            # 8-byte Reload
	subq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_151
# BB#150:                               #   in Loop: Header=BB4_130 Depth=2
	movq	%r15, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, (%rbx)
.LBB4_151:                              #   in Loop: Header=BB4_130 Depth=2
	movb	48(%rsp), %cl
	movb	%cl, 16(%rax)
	movdqa	32(%rsp), %xmm0
	movdqu	%xmm0, (%rax)
	movq	(%rbx), %rdi
	addq	$17, %rdi
	movl	$256, %edx              # imm = 0x100
	leaq	64(%rsp), %rsi
	callq	memcpy
	testq	%r12, %r12
	jg	.LBB4_130
.LBB4_1:                                # %get_dht.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	movq	%r14, (%r13)
	movq	%rbp, 8(%r13)
	jmp	.LBB4_69
	.p2align	4, 0x90
.LBB4_70:
	movq	568(%r15), %rax
	cmpl	$0, 160(%rax)
	je	.LBB4_73
# BB#71:
	movq	%r15, %rdi
	callq	next_marker
	testl	%eax, %eax
	je	.LBB4_226
# BB#72:                                # %.thread-pre-split_crit_edge
	movl	524(%r15), %ebp
	jmp	.LBB4_84
	.p2align	4, 0x90
.LBB4_73:
	movq	32(%r15), %r14
	movq	8(%r14), %r12
	testq	%r12, %r12
	jne	.LBB4_76
# BB#74:
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#75:
	movq	8(%r14), %r12
.LBB4_76:
	movq	(%r14), %rbx
	decq	%r12
	movzbl	(%rbx), %r13d
	je	.LBB4_78
# BB#77:
	incq	%rbx
	jmp	.LBB4_80
.LBB4_78:
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#79:
	movq	(%r14), %rbx
	movq	8(%r14), %r12
.LBB4_80:
	decq	%r12
	movb	(%rbx), %al
	incq	%rbx
	movzbl	%al, %ebp
	cmpb	$-1, %r13b
	jne	.LBB4_82
# BB#81:
	cmpb	$-40, %al
	je	.LBB4_83
.LBB4_82:
	movq	(%r15), %rax
	movl	$52, 40(%rax)
	movl	%r13d, 44(%rax)
	movl	%ebp, 48(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_83:                               # %first_marker.exit
	movl	%ebp, 524(%r15)
	movq	%rbx, (%r14)
	movq	%r12, 8(%r14)
	jmp	.LBB4_84
.LBB4_2:                                #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#3:                                 #   in Loop: Header=BB4_84 Depth=1
	movq	(%r12), %rbx
	movq	8(%r12), %rbp
.LBB4_4:                                #   in Loop: Header=BB4_84 Depth=1
	shlq	$8, %r14
	decq	%rbp
	movzbl	(%rbx), %eax
	incq	%rbx
	orq	%r14, %rax
	movq	%rax, %r14
	cmpq	$3, %r14
	jb	.LBB4_20
# BB#5:                                 # %.lr.ph.i45
                                        #   in Loop: Header=BB4_84 Depth=1
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_8 Depth=2
	movl	%r14d, %eax
	andb	$15, %al
	movb	%al, 312(%r15,%r13)
	shrb	$4, %r14b
	movb	%r14b, 328(%r15,%r13)
	cmpb	%r14b, %al
	movq	(%rsp), %r14            # 8-byte Reload
	jbe	.LBB4_19
# BB#7:                                 #   in Loop: Header=BB4_8 Depth=2
	movq	(%r15), %rax
	movl	$27, 40(%rax)
	movl	%r12d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbp, %rbp
	jne	.LBB4_11
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=2
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#10:                                #   in Loop: Header=BB4_8 Depth=2
	movq	(%r12), %rbx
	movq	8(%r12), %rbp
.LBB4_11:                               #   in Loop: Header=BB4_8 Depth=2
	decq	%rbp
	movzbl	(%rbx), %r13d
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_8 Depth=2
	incq	%rbx
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_13:                               #   in Loop: Header=BB4_8 Depth=2
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#14:                                #   in Loop: Header=BB4_8 Depth=2
	movq	(%r12), %rbx
	movq	8(%r12), %rbp
.LBB4_15:                               #   in Loop: Header=BB4_8 Depth=2
	addq	$-2, %r14
	movq	%r14, (%rsp)            # 8-byte Spill
	movzbl	(%rbx), %r14d
	movzbl	%r14b, %r12d
	movq	(%r15), %rax
	movl	$78, 40(%rax)
	movl	%r13d, 44(%rax)
	movl	%r12d, 48(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	cmpb	$32, %r13b
	jb	.LBB4_17
# BB#16:                                # %.thread.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movq	(%r15), %rax
	movl	$26, 40(%rax)
	movl	%r13d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_8 Depth=2
	cmpb	$16, %r13b
	jb	.LBB4_6
.LBB4_18:                               #   in Loop: Header=BB4_8 Depth=2
	movb	%r14b, 328(%r15,%r13)
	movq	(%rsp), %r14            # 8-byte Reload
.LBB4_19:                               # %.backedge.i
                                        #   in Loop: Header=BB4_8 Depth=2
	decq	%rbp
	incq	%rbx
	cmpq	$2, %r14
	movq	8(%rsp), %r12           # 8-byte Reload
	jg	.LBB4_8
.LBB4_20:                               # %get_dac.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	movq	%rbx, (%r12)
	movq	%rbp, 8(%r12)
	jmp	.LBB4_69
.LBB4_21:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB4_226
# BB#22:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%rbx), %r12
	movq	8(%rbx), %rbp
.LBB4_23:                               #   in Loop: Header=BB4_84 Depth=1
	shlq	$8, %r14
	decq	%rbp
	movzbl	(%r12), %r13d
	incq	%r12
	orq	%r14, %r13
	cmpq	$3, %r13
	jb	.LBB4_52
# BB#24:                                # %.lr.ph.i64
                                        #   in Loop: Header=BB4_84 Depth=1
	addq	$-2, %r13
.LBB4_25:                               #   Parent Loop BB4_84 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_34 Depth 3
                                        #       Child Loop BB4_43 Depth 3
                                        #       Child Loop BB4_50 Depth 3
	testq	%rbp, %rbp
	jne	.LBB4_28
# BB#26:                                #   in Loop: Header=BB4_25 Depth=2
	movq	%r15, %rdi
	movq	(%rsp), %rbx            # 8-byte Reload
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB4_226
# BB#27:                                #   in Loop: Header=BB4_25 Depth=2
	movq	(%rbx), %r12
	movq	8(%rbx), %rbp
.LBB4_28:                               #   in Loop: Header=BB4_25 Depth=2
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movzbl	(%r12), %ebx
	movl	%ebx, %r14d
	shrl	$4, %r14d
	andl	$15, %ebx
	movq	(%r15), %rax
	movl	$80, 40(%rax)
	movl	%ebx, 44(%rax)
	movl	%r14d, 48(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	cmpl	$4, %ebx
	jb	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_25 Depth=2
	movq	(%r15), %rax
	movl	$30, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_30:                               #   in Loop: Header=BB4_25 Depth=2
	movl	%ebx, %ebx
	movq	192(%r15,%rbx,8), %r13
	testq	%r13, %r13
	jne	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_25 Depth=2
	movq	%r15, %rdi
	callq	jpeg_alloc_quant_table
	movq	%rax, %r13
	movq	%r13, 192(%r15,%rbx,8)
.LBB4_32:                               #   in Loop: Header=BB4_25 Depth=2
	incq	%r12
	decq	%rbp
	testl	%r14d, %r14d
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	je	.LBB4_42
# BB#33:                                # %.split.i.preheader
                                        #   in Loop: Header=BB4_25 Depth=2
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_34:                               # %.split.i
                                        #   Parent Loop BB4_84 Depth=1
                                        #     Parent Loop BB4_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	jne	.LBB4_37
# BB#35:                                #   in Loop: Header=BB4_34 Depth=3
	movq	%r15, %rdi
	movq	(%rsp), %rbx            # 8-byte Reload
	callq	*24(%rbx)
	testl	%eax, %eax
	je	.LBB4_226
# BB#36:                                #   in Loop: Header=BB4_34 Depth=3
	movq	(%rbx), %r12
	movq	8(%rbx), %rbp
.LBB4_37:                               #   in Loop: Header=BB4_34 Depth=3
	movzbl	(%r12), %ebx
	decq	%rbp
	je	.LBB4_39
# BB#38:                                #   in Loop: Header=BB4_34 Depth=3
	incq	%r12
	jmp	.LBB4_41
	.p2align	4, 0x90
.LBB4_39:                               #   in Loop: Header=BB4_34 Depth=3
	movq	%r15, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB4_226
# BB#40:                                #   in Loop: Header=BB4_34 Depth=3
	movq	(%rbp), %r12
	movq	8(%rbp), %rbp
.LBB4_41:                               #   in Loop: Header=BB4_34 Depth=3
	shll	$8, %ebx
	movzbl	(%r12), %eax
	orl	%eax, %ebx
	movslq	jpeg_natural_order(,%r14,4), %rax
	movw	%bx, (%r13,%rax,2)
	incq	%r14
	incq	%r12
	decq	%rbp
	cmpq	$64, %r14
	jl	.LBB4_34
	jmp	.LBB4_47
.LBB4_42:                               # %.split.us.i.preheader
                                        #   in Loop: Header=BB4_25 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_43:                               # %.split.us.i
                                        #   Parent Loop BB4_84 Depth=1
                                        #     Parent Loop BB4_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	jne	.LBB4_46
# BB#44:                                #   in Loop: Header=BB4_43 Depth=3
	movq	%r15, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	callq	*24(%rbp)
	testl	%eax, %eax
	je	.LBB4_226
# BB#45:                                #   in Loop: Header=BB4_43 Depth=3
	movq	(%rbp), %r12
	movq	8(%rbp), %rbp
.LBB4_46:                               #   in Loop: Header=BB4_43 Depth=3
	movzbl	(%r12), %eax
	movslq	jpeg_natural_order(,%rbx,4), %rcx
	movw	%ax, (%r13,%rcx,2)
	incq	%rbx
	incq	%r12
	decq	%rbp
	cmpq	$64, %rbx
	jl	.LBB4_43
.LBB4_47:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB4_25 Depth=2
	movq	(%r15), %rax
	cmpl	$2, 124(%rax)
	movl	16(%rsp), %r14d         # 4-byte Reload
	jl	.LBB4_51
# BB#48:                                # %.preheader.i72.preheader
                                        #   in Loop: Header=BB4_25 Depth=2
	movl	$8, %ebx
	jmp	.LBB4_50
	.p2align	4, 0x90
.LBB4_49:                               # %.preheader..preheader_crit_edge.i
                                        #   in Loop: Header=BB4_50 Depth=3
	movq	(%r15), %rax
	addq	$8, %rbx
.LBB4_50:                               # %.preheader.i72
                                        #   Parent Loop BB4_84 Depth=1
                                        #     Parent Loop BB4_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%r13,%rbx,2), %xmm0 # xmm0 = mem[0],zero
	pxor	%xmm1, %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 44(%rax)
	movq	-8(%r13,%rbx,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, 60(%rax)
	movl	$92, 40(%rax)
	movl	$2, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	cmpq	$63, %rbx
	jle	.LBB4_49
.LBB4_51:                               # %.loopexit.i73
                                        #   in Loop: Header=BB4_25 Depth=2
	testl	%r14d, %r14d
	movq	$-129, %rax
	movq	$-65, %rcx
	cmoveq	%rcx, %rax
	movq	8(%rsp), %r13           # 8-byte Reload
	addq	%rax, %r13
	testq	%r13, %r13
	jg	.LBB4_25
.LBB4_52:                               # %get_dqt.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r12, (%rax)
	movq	%rbp, 8(%rax)
	jmp	.LBB4_69
.LBB4_53:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#54:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbp
	movq	8(%r14), %r12
.LBB4_55:                               # %skip_variable.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	shlq	$8, %r13
	decq	%r12
	movzbl	(%rbp), %ebx
	incq	%rbp
	orq	%r13, %rbx
	movq	(%r15), %rax
	movl	$90, 40(%rax)
	movl	524(%r15), %ecx
	movl	%ecx, 44(%rax)
	movl	%ebx, 48(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%rbp, (%r14)
	movq	%r12, 8(%r14)
	movq	32(%r15), %rax
	addq	$-2, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	*32(%rax)
	jmp	.LBB4_69
.LBB4_56:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#57:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbx
	movq	8(%r14), %r13
.LBB4_58:                               #   in Loop: Header=BB4_84 Depth=1
	shlq	$8, %r12
	decq	%r13
	movzbl	(%rbx), %eax
	orq	%r12, %rax
	cmpq	$4, %rax
	je	.LBB4_60
# BB#59:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%r15), %rax
	movl	$9, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_60:                               #   in Loop: Header=BB4_84 Depth=1
	testq	%r13, %r13
	je	.LBB4_62
# BB#61:                                #   in Loop: Header=BB4_84 Depth=1
	incq	%rbx
	jmp	.LBB4_64
.LBB4_62:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#63:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbx
	movq	8(%r14), %r13
.LBB4_64:                               #   in Loop: Header=BB4_84 Depth=1
	movzbl	(%rbx), %r12d
	decq	%r13
	je	.LBB4_66
# BB#65:                                #   in Loop: Header=BB4_84 Depth=1
	incq	%rbx
	jmp	.LBB4_68
.LBB4_66:                               #   in Loop: Header=BB4_84 Depth=1
	movq	%r15, %rdi
	callq	*24(%r14)
	testl	%eax, %eax
	je	.LBB4_226
# BB#67:                                #   in Loop: Header=BB4_84 Depth=1
	movq	(%r14), %rbx
	movq	8(%r14), %r13
.LBB4_68:                               # %get_dri.exit
                                        #   in Loop: Header=BB4_84 Depth=1
	shll	$8, %r12d
	decq	%r13
	movzbl	(%rbx), %ebp
	incq	%rbx
	orl	%r12d, %ebp
	movq	(%r15), %rax
	movl	$81, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movl	%ebp, 360(%r15)
	movq	%rbx, (%r14)
	movq	%r13, 8(%r14)
	.p2align	4, 0x90
.LBB4_69:                               #   in Loop: Header=BB4_84 Depth=1
	movl	$0, 524(%r15)
	xorl	%ebp, %ebp
	testl	%ebp, %ebp
	je	.LBB4_70
.LBB4_84:                               # %thread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_25 Depth 2
                                        #       Child Loop BB4_34 Depth 3
                                        #       Child Loop BB4_43 Depth 3
                                        #       Child Loop BB4_50 Depth 3
                                        #     Child Loop BB4_8 Depth 2
                                        #     Child Loop BB4_130 Depth 2
                                        #       Child Loop BB4_134 Depth 3
                                        #       Child Loop BB4_143 Depth 3
	leal	-1(%rbp), %eax
	cmpl	$253, %eax
	ja	.LBB4_91
# BB#85:                                # %thread-pre-split
                                        #   in Loop: Header=BB4_84 Depth=1
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_152:
	movq	(%r15), %rax
	movl	$84, 40(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movl	$0, 524(%r15)
	movl	$2, %r13d
	jmp	.LBB4_227
.LBB4_153:
	movq	32(%r15), %r12
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
	movq	568(%r15), %rax
	cmpl	$0, 164(%rax)
	jne	.LBB4_155
# BB#154:
	movq	(%r15), %rax
	movl	$61, 40(%rax)
	movq	%r15, %rdi
	movq	%rcx, %rbx
	callq	*(%rax)
	movq	%rbx, %rcx
.LBB4_155:
	testq	%rcx, %rcx
	jne	.LBB4_158
# BB#156:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#157:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_158:
	movzbl	(%rbp), %r14d
	decq	%rcx
	je	.LBB4_163
# BB#159:
	incq	%rbp
	jmp	.LBB4_165
.LBB4_163:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#164:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_165:
	movzbl	(%rbp), %ebx
	decq	%rcx
	je	.LBB4_167
# BB#166:
	incq	%rbp
	jmp	.LBB4_169
.LBB4_167:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#168:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_169:
	movq	%rcx, %r13
	movzbl	(%rbp), %ecx
	movl	%ecx, %eax
	decb	%al
	cmpb	$3, %al
	ja	.LBB4_171
# BB#170:
	shlq	$8, %r14
	orq	%r14, %rbx
	leaq	6(%rcx,%rcx), %rax
	cmpq	%rax, %rbx
	je	.LBB4_172
.LBB4_171:
	movq	(%r15), %rax
	movl	$9, 40(%rax)
	movq	%r15, %rdi
	movq	%rcx, %rbx
	callq	*(%rax)
	movq	%rbx, %rcx
.LBB4_172:                              # %._crit_edge198.i
	movq	(%r15), %rax
	movl	$102, 40(%rax)
	movl	%ecx, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rcx, %rbx
	callq	*8(%rax)
	movl	%ebx, 416(%r15)
	incq	%rbp
	movq	%r13, %rcx
	decq	%rcx
	sete	%al
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testb	%bl, %bl
	je	.LBB4_187
# BB#173:                               # %.lr.ph188.i
	xorl	%ebx, %ebx
	movq	%r12, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_174:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_183 Depth 2
	testb	$1, %al
	je	.LBB4_177
# BB#175:                               #   in Loop: Header=BB4_174 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#176:                               #   in Loop: Header=BB4_174 Depth=1
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_177:                              #   in Loop: Header=BB4_174 Depth=1
	decq	%rcx
	movzbl	(%rbp), %r13d
	je	.LBB4_179
# BB#178:                               #   in Loop: Header=BB4_174 Depth=1
	incq	%rbp
	jmp	.LBB4_181
	.p2align	4, 0x90
.LBB4_179:                              #   in Loop: Header=BB4_174 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#180:                               #   in Loop: Header=BB4_174 Depth=1
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_181:                              #   in Loop: Header=BB4_174 Depth=1
	movzbl	(%rbp), %r12d
	movq	296(%r15), %r14
	movl	48(%r15), %eax
	testl	%eax, %eax
	movq	%rcx, (%rsp)            # 8-byte Spill
	jle	.LBB4_185
# BB#182:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_174 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_183:                              # %.lr.ph.i
                                        #   Parent Loop BB4_174 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	(%r14), %r13d
	je	.LBB4_186
# BB#184:                               #   in Loop: Header=BB4_183 Depth=2
	incl	%ecx
	addq	$96, %r14
	cmpl	%eax, %ecx
	jl	.LBB4_183
.LBB4_185:                              # %._crit_edge.i
                                        #   in Loop: Header=BB4_174 Depth=1
	movq	(%r15), %rax
	movl	$5, 40(%rax)
	movl	%r13d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_186:                              # %.loopexit.i
                                        #   in Loop: Header=BB4_174 Depth=1
	movq	%r14, 424(%r15,%rbx,8)
	movl	%r12d, %eax
	shrl	$4, %eax
	movl	%eax, 20(%r14)
	andl	$15, %r12d
	movl	%r12d, 24(%r14)
	movq	(%r15), %rax
	movl	%r13d, 44(%rax)
	movl	20(%r14), %ecx
	movl	%ecx, 48(%rax)
	movl	24(%r14), %ecx
	movl	%ecx, 52(%rax)
	movl	$103, 40(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	incq	%rbx
	incq	%rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	decq	%rcx
	sete	%al
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	jl	.LBB4_174
.LBB4_187:                              # %._crit_edge189.i
	testb	%al, %al
	je	.LBB4_190
# BB#188:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#189:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_190:
	decq	%rcx
	movzbl	(%rbp), %eax
	movl	%eax, 508(%r15)
	je	.LBB4_198
# BB#191:
	incq	%rbp
	jmp	.LBB4_200
.LBB4_198:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#199:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_200:
	decq	%rcx
	movzbl	(%rbp), %eax
	movl	%eax, 512(%r15)
	je	.LBB4_203
# BB#201:
	incq	%rbp
	jmp	.LBB4_205
.LBB4_203:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB4_226
# BB#204:
	movq	(%r12), %rbp
	movq	8(%r12), %rcx
.LBB4_205:
	decq	%rcx
	movzbl	(%rbp), %eax
	incq	%rbp
	movq	%rcx, %rbx
	movl	%eax, %ecx
	shrl	$4, %ecx
	movl	%ecx, 516(%r15)
	andl	$15, %eax
	movl	%eax, 520(%r15)
	movq	(%r15), %rax
	movl	508(%r15), %ecx
	movl	%ecx, 44(%rax)
	movl	512(%r15), %ecx
	movl	%ecx, 48(%rax)
	movl	516(%r15), %ecx
	movl	%ecx, 52(%rax)
	movl	520(%r15), %ecx
	movl	%ecx, 56(%rax)
	movl	$104, 40(%rax)
	movl	$1, %r13d
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	568(%r15), %rax
	movl	$0, 168(%rax)
	incl	164(%r15)
	movq	%rbp, (%r12)
	movq	%rbx, 8(%r12)
	movl	$0, 524(%r15)
	jmp	.LBB4_227
.LBB4_226:
	xorl	%r13d, %r13d
.LBB4_227:                              # %first_marker.exit.thread
	movl	%r13d, %eax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	read_markers, .Lfunc_end4-read_markers
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_87
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_90
	.quad	.LBB4_90
	.quad	.LBB4_92
	.quad	.LBB4_88
	.quad	.LBB4_94
	.quad	.LBB4_88
	.quad	.LBB4_88
	.quad	.LBB4_88
	.quad	.LBB4_88
	.quad	.LBB4_99
	.quad	.LBB4_101
	.quad	.LBB4_88
	.quad	.LBB4_102
	.quad	.LBB4_88
	.quad	.LBB4_88
	.quad	.LBB4_88
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_87
	.quad	.LBB4_107
	.quad	.LBB4_152
	.quad	.LBB4_153
	.quad	.LBB4_110
	.quad	.LBB4_115
	.quad	.LBB4_120
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_91
	.quad	.LBB4_125

	.text
	.p2align	4, 0x90
	.type	read_restart_marker,@function
read_restart_marker:                    # @read_restart_marker
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	524(%rbx), %eax
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	%rbx, %rdi
	callq	next_marker
	testl	%eax, %eax
	je	.LBB5_7
# BB#2:                                 # %._crit_edge
	movl	524(%rbx), %eax
.LBB5_3:
	movq	568(%rbx), %rcx
	movl	168(%rcx), %esi
	leal	208(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.LBB5_6
# BB#4:
	movq	(%rbx), %rax
	movl	$97, 40(%rax)
	movl	%esi, 44(%rax)
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	$0, 524(%rbx)
	jmp	.LBB5_5
.LBB5_6:
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB5_7
.LBB5_5:
	movq	568(%rbx), %rax
	movl	168(%rax), %ecx
	incl	%ecx
	andl	$7, %ecx
	movl	%ecx, 168(%rax)
	movl	$1, %eax
	popq	%rbx
	retq
.LBB5_7:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	read_restart_marker, .Lfunc_end5-read_restart_marker
	.cfi_endproc

	.p2align	4, 0x90
	.type	skip_variable,@function
skip_variable:                          # @skip_variable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	32(%r12), %r15
	movq	8(%r15), %r13
	testq	%r13, %r13
	jne	.LBB6_3
# BB#1:
	movq	%r12, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB6_10
# BB#2:
	movq	8(%r15), %r13
.LBB6_3:
	movq	(%r15), %rbx
	movzbl	(%rbx), %r14d
	decq	%r13
	je	.LBB6_5
# BB#4:
	incq	%rbx
	jmp	.LBB6_7
.LBB6_5:
	movq	%r12, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB6_10
# BB#6:
	movq	(%r15), %rbx
	movq	8(%r15), %r13
.LBB6_7:
	shlq	$8, %r14
	decq	%r13
	movzbl	(%rbx), %ebp
	incq	%rbx
	orq	%r14, %rbp
	movq	(%r12), %rax
	movl	$90, 40(%rax)
	movl	524(%r12), %ecx
	movl	%ecx, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$1, %r14d
	movl	$1, %esi
	movq	%r12, %rdi
	callq	*8(%rax)
	movq	%rbx, (%r15)
	movq	%r13, 8(%r15)
	movq	32(%r12), %rax
	addq	$-2, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*32(%rax)
	jmp	.LBB6_8
.LBB6_10:
	xorl	%r14d, %r14d
.LBB6_8:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	skip_variable, .Lfunc_end6-skip_variable
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_app0,@function
get_app0:                               # @get_app0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 80
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	32(%r15), %r12
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.LBB7_3
# BB#1:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB7_25
# BB#2:
	movq	8(%r12), %r13
.LBB7_3:
	movq	(%r12), %rbp
	movzbl	(%rbp), %ebx
	decq	%r13
	je	.LBB7_5
# BB#4:
	incq	%rbp
	jmp	.LBB7_7
.LBB7_5:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB7_25
# BB#6:
	movq	(%r12), %rbp
	movq	8(%r12), %r13
.LBB7_7:
	shlq	$8, %rbx
	decq	%r13
	movzbl	(%rbp), %r14d
	incq	%rbp
	orq	%rbx, %r14
	leaq	-2(%r14), %rbx
	cmpq	$14, %rbx
	jl	.LBB7_20
# BB#8:                                 # %.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	testq	%r13, %r13
	jne	.LBB7_12
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB7_25
# BB#11:                                #   in Loop: Header=BB7_9 Depth=1
	movq	(%r12), %rbp
	movq	8(%r12), %r13
.LBB7_12:                               #   in Loop: Header=BB7_9 Depth=1
	decq	%r13
	movzbl	(%rbp), %eax
	incq	%rbp
	movb	%al, 2(%rsp,%rbx)
	incq	%rbx
	cmpq	$14, %rbx
	jl	.LBB7_9
# BB#13:
	addq	$-16, %r14
	cmpb	$74, 2(%rsp)
	jne	.LBB7_18
# BB#14:
	cmpb	$70, 3(%rsp)
	jne	.LBB7_18
# BB#15:
	cmpb	$73, 4(%rsp)
	jne	.LBB7_18
# BB#16:
	cmpb	$70, 5(%rsp)
	jne	.LBB7_18
# BB#17:
	cmpb	$0, 6(%rsp)
	je	.LBB7_27
.LBB7_18:
	movq	(%r15), %rax
	movl	$76, 40(%rax)
	leal	14(%r14), %ecx
	movl	%ecx, 44(%rax)
.LBB7_19:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	jmp	.LBB7_21
.LBB7_20:
	movq	(%r15), %rax
	movl	$76, 40(%rax)
	movl	%ebx, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%rbx, %r14
.LBB7_21:
	movq	%rbp, (%r12)
	movq	%r13, 8(%r12)
	movl	$1, %ebp
	testq	%r14, %r14
	jle	.LBB7_26
# BB#22:
	movq	32(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*32(%rax)
	jmp	.LBB7_26
.LBB7_25:
	xorl	%ebp, %ebp
.LBB7_26:                               # %.loopexit
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_27:
	movzbl	7(%rsp), %eax
	cmpl	$1, %eax
	jne	.LBB7_30
# BB#28:
	movzbl	8(%rsp), %eax
	cmpl	$2, %eax
	jbe	.LBB7_32
# BB#29:
	movq	(%r15), %rcx
	movl	$88, 40(%rcx)
	movl	$1, 44(%rcx)
	movl	%eax, 48(%rcx)
	movl	$1, %esi
	jmp	.LBB7_31
.LBB7_30:
	movq	(%r15), %rcx
	movl	$115, 40(%rcx)
	movl	%eax, 44(%rcx)
	movzbl	8(%rsp), %eax
	movl	%eax, 48(%rcx)
	movl	$-1, %esi
.LBB7_31:
	movq	%r15, %rdi
	callq	*8(%rcx)
.LBB7_32:
	movl	$1, 364(%r15)
	movzbl	9(%rsp), %eax
	movb	%al, 368(%r15)
	movzbl	10(%rsp), %ecx
	shll	$8, %ecx
	movzbl	11(%rsp), %edx
	orl	%ecx, %edx
	movw	%dx, 370(%r15)
	movzbl	12(%rsp), %ecx
	shll	$8, %ecx
	movzbl	13(%rsp), %esi
	orl	%ecx, %esi
	movw	%si, 372(%r15)
	movq	(%r15), %rcx
	movzwl	%dx, %edx
	movl	%edx, 44(%rcx)
	movzwl	%si, %edx
	movl	%edx, 48(%rcx)
	movl	%eax, 52(%rcx)
	movl	$86, 40(%rcx)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rcx)
	movzbl	14(%rsp), %ecx
	movzbl	15(%rsp), %ebx
	movl	%ebx, %eax
	orb	%cl, %al
	je	.LBB7_34
# BB#33:
	movq	(%r15), %rax
	movl	$89, 40(%rax)
	movl	%ecx, 44(%rax)
	movl	%ebx, 48(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	callq	*8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB7_34:
	imulq	%rcx, %rbx
	leaq	(%rbx,%rbx,2), %rax
	cmpq	%rax, %r14
	je	.LBB7_21
# BB#35:
	movq	(%r15), %rax
	movl	$87, 40(%rax)
	movl	%r14d, 44(%rax)
	jmp	.LBB7_19
.Lfunc_end7:
	.size	get_app0, .Lfunc_end7-get_app0
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_app14,@function
get_app14:                              # @get_app14
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 80
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	32(%r15), %r12
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.LBB8_3
# BB#1:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB8_25
# BB#2:
	movq	8(%r12), %r13
.LBB8_3:
	movq	(%r12), %rbp
	movzbl	(%rbp), %ebx
	decq	%r13
	je	.LBB8_5
# BB#4:
	incq	%rbp
	jmp	.LBB8_7
.LBB8_5:
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB8_25
# BB#6:
	movq	(%r12), %rbp
	movq	8(%r12), %r13
.LBB8_7:
	shlq	$8, %rbx
	decq	%r13
	movzbl	(%rbp), %r14d
	incq	%rbp
	orq	%rbx, %r14
	leaq	-2(%r14), %rbx
	cmpq	$12, %rbx
	jl	.LBB8_19
# BB#8:                                 # %.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	testq	%r13, %r13
	jne	.LBB8_12
# BB#10:                                #   in Loop: Header=BB8_9 Depth=1
	movq	%r15, %rdi
	callq	*24(%r12)
	testl	%eax, %eax
	je	.LBB8_25
# BB#11:                                #   in Loop: Header=BB8_9 Depth=1
	movq	(%r12), %rbp
	movq	8(%r12), %r13
.LBB8_12:                               #   in Loop: Header=BB8_9 Depth=1
	decq	%r13
	movzbl	(%rbp), %eax
	incq	%rbp
	movb	%al, 12(%rsp,%rbx)
	incq	%rbx
	cmpq	$12, %rbx
	jl	.LBB8_9
# BB#13:
	addq	$-14, %r14
	cmpb	$65, 12(%rsp)
	jne	.LBB8_20
# BB#14:
	cmpb	$100, 13(%rsp)
	jne	.LBB8_20
# BB#15:
	cmpb	$111, 14(%rsp)
	jne	.LBB8_20
# BB#16:
	cmpb	$98, 15(%rsp)
	jne	.LBB8_20
# BB#17:
	cmpb	$101, 16(%rsp)
	jne	.LBB8_20
# BB#18:
	movzbl	17(%rsp), %eax
	shll	$8, %eax
	movzbl	18(%rsp), %ecx
	orl	%eax, %ecx
	movzbl	19(%rsp), %eax
	shll	$8, %eax
	movzbl	20(%rsp), %edx
	orl	%eax, %edx
	movzbl	21(%rsp), %eax
	shll	$8, %eax
	movzbl	22(%rsp), %esi
	orl	%eax, %esi
	movzbl	23(%rsp), %ebx
	movq	(%r15), %rax
	movl	%ecx, 44(%rax)
	movl	%edx, 48(%rax)
	movl	%esi, 52(%rax)
	movl	%ebx, 56(%rax)
	movl	$75, 40(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movl	$1, 376(%r15)
	movb	%bl, 380(%r15)
	jmp	.LBB8_21
.LBB8_19:
	movq	(%r15), %rax
	movl	$77, 40(%rax)
	movl	%ebx, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%rbx, %r14
	jmp	.LBB8_21
.LBB8_25:
	xorl	%ebp, %ebp
	jmp	.LBB8_26
.LBB8_20:
	movq	(%r15), %rax
	movl	$77, 40(%rax)
	leal	12(%r14), %ecx
	movl	%ecx, 44(%rax)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB8_21:
	movq	%rbp, (%r12)
	movq	%r13, 8(%r12)
	movl	$1, %ebp
	testq	%r14, %r14
	jle	.LBB8_26
# BB#22:
	movq	32(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*32(%rax)
.LBB8_26:                               # %.loopexit
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	get_app14, .Lfunc_end8-get_app14
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_sof,@function
get_sof:                                # @get_sof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 64
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	32(%r14), %r15
	movq	(%r15), %r12
	movq	8(%r15), %r13
	movl	%esi, 304(%r14)
	movl	%edx, 308(%r14)
	testq	%r13, %r13
	jne	.LBB9_3
# BB#1:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#2:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_3:
	movzbl	(%r12), %ebp
	decq	%r13
	je	.LBB9_5
# BB#4:
	incq	%r12
	jmp	.LBB9_7
.LBB9_5:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#6:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_7:
	movzbl	(%r12), %ebx
	decq	%r13
	je	.LBB9_9
# BB#8:
	incq	%r12
	jmp	.LBB9_11
.LBB9_9:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#10:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_11:
	decq	%r13
	movzbl	(%r12), %eax
	movl	%eax, 288(%r14)
	je	.LBB9_13
# BB#12:
	incq	%r12
	jmp	.LBB9_15
.LBB9_13:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#14:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_15:
	movzbl	(%r12), %eax
	shll	$8, %eax
	decq	%r13
	movl	%eax, 44(%r14)
	je	.LBB9_17
# BB#16:
	incq	%r12
	jmp	.LBB9_19
.LBB9_17:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#18:
	movq	(%r15), %r12
	movq	8(%r15), %r13
	movl	44(%r14), %eax
.LBB9_19:
	movzbl	(%r12), %ecx
	addl	%eax, %ecx
	decq	%r13
	movl	%ecx, 44(%r14)
	je	.LBB9_21
# BB#20:
	incq	%r12
	jmp	.LBB9_23
.LBB9_21:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#22:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_23:
	movzbl	(%r12), %eax
	shll	$8, %eax
	decq	%r13
	movl	%eax, 40(%r14)
	je	.LBB9_25
# BB#24:
	incq	%r12
	jmp	.LBB9_27
.LBB9_25:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#26:
	movq	(%r15), %r12
	movq	8(%r15), %r13
	movl	40(%r14), %eax
.LBB9_27:
	movzbl	(%r12), %ecx
	addl	%eax, %ecx
	decq	%r13
	movl	%ecx, 40(%r14)
	je	.LBB9_30
# BB#28:
	incq	%r12
	jmp	.LBB9_32
.LBB9_30:
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#31:
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_32:
	shlq	$8, %rbp
	orq	%rbp, %rbx
	movzbl	(%r12), %eax
	movl	%eax, 48(%r14)
	movq	(%r14), %rax
	movl	524(%r14), %ecx
	movl	%ecx, 44(%rax)
	movl	40(%r14), %ecx
	movl	%ecx, 48(%rax)
	movl	44(%r14), %ecx
	movl	%ecx, 52(%rax)
	movl	48(%r14), %ecx
	movl	%ecx, 56(%rax)
	movl	$99, 40(%rax)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	568(%r14), %rax
	cmpl	$0, 164(%rax)
	je	.LBB9_34
# BB#33:
	movq	(%r14), %rax
	movl	$57, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB9_34:
	addq	$-8, %rbx
	cmpl	$0, 44(%r14)
	je	.LBB9_37
# BB#35:
	cmpl	$0, 40(%r14)
	je	.LBB9_37
# BB#36:
	movl	48(%r14), %eax
	testl	%eax, %eax
	jg	.LBB9_38
.LBB9_37:
	movq	(%r14), %rax
	movl	$31, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	48(%r14), %eax
.LBB9_38:
	leal	(%rax,%rax,2), %eax
	cltq
	cmpq	%rax, %rbx
	je	.LBB9_40
# BB#39:
	movq	(%r14), %rax
	movl	$9, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB9_40:
	movq	296(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_42
# BB#41:
	movq	8(%r14), %rax
	movslq	48(%r14), %rcx
	shlq	$5, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
	movq	%rbx, 296(%r14)
.LBB9_42:
	incq	%r12
	decq	%r13
	cmpl	$0, 48(%r14)
	jle	.LBB9_56
# BB#43:                                # %.lr.ph
	addq	$16, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_44:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, -12(%rbx)
	testq	%r13, %r13
	jne	.LBB9_47
# BB#45:                                #   in Loop: Header=BB9_44 Depth=1
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#46:                                #   in Loop: Header=BB9_44 Depth=1
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_47:                               #   in Loop: Header=BB9_44 Depth=1
	decq	%r13
	movzbl	(%r12), %eax
	movl	%eax, -16(%rbx)
	je	.LBB9_49
# BB#48:                                #   in Loop: Header=BB9_44 Depth=1
	incq	%r12
	jmp	.LBB9_51
	.p2align	4, 0x90
.LBB9_49:                               #   in Loop: Header=BB9_44 Depth=1
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#50:                                #   in Loop: Header=BB9_44 Depth=1
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_51:                               #   in Loop: Header=BB9_44 Depth=1
	movzbl	(%r12), %eax
	movl	%eax, %ecx
	shrl	$4, %ecx
	andl	$15, %eax
	decq	%r13
	movl	%ecx, -8(%rbx)
	movl	%eax, -4(%rbx)
	je	.LBB9_53
# BB#52:                                #   in Loop: Header=BB9_44 Depth=1
	incq	%r12
	jmp	.LBB9_55
	.p2align	4, 0x90
.LBB9_53:                               #   in Loop: Header=BB9_44 Depth=1
	movq	%r14, %rdi
	callq	*24(%r15)
	testl	%eax, %eax
	je	.LBB9_66
# BB#54:                                #   in Loop: Header=BB9_44 Depth=1
	movq	(%r15), %r12
	movq	8(%r15), %r13
.LBB9_55:                               #   in Loop: Header=BB9_44 Depth=1
	movzbl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	(%r14), %rax
	movl	-16(%rbx), %ecx
	movl	%ecx, 44(%rax)
	movl	-8(%rbx), %ecx
	movl	%ecx, 48(%rax)
	movl	-4(%rbx), %ecx
	movl	%ecx, 52(%rax)
	movl	(%rbx), %ecx
	movl	%ecx, 56(%rax)
	movl	$100, 40(%rax)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	incl	%ebp
	incq	%r12
	decq	%r13
	addq	$96, %rbx
	cmpl	48(%r14), %ebp
	jl	.LBB9_44
.LBB9_56:                               # %._crit_edge
	movq	568(%r14), %rax
	movl	$1, 164(%rax)
	movq	%r12, (%r15)
	movq	%r13, 8(%r15)
	movl	$1, %eax
	jmp	.LBB9_67
.LBB9_66:
	xorl	%eax, %eax
.LBB9_67:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	get_sof, .Lfunc_end9-get_sof
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
