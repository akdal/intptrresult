	.text
	.file	"clock.bc"
	.globl	clock_Init
	.p2align	4, 0x90
	.type	clock_Init,@function
clock_Init:                             # @clock_Init
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, clock_Akku(%rip)
	movq	$0, clock_Akku+16(%rip)
	retq
.Lfunc_end0:
	.size	clock_Init, .Lfunc_end0-clock_Init
	.cfi_endproc

	.globl	clock_InitCounter
	.p2align	4, 0x90
	.type	clock_InitCounter,@function
clock_InitCounter:                      # @clock_InitCounter
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movl	$0, clock_Akku(,%rax,4)
	retq
.Lfunc_end1:
	.size	clock_InitCounter, .Lfunc_end1-clock_InitCounter
	.cfi_endproc

	.globl	clock_StartCounter
	.p2align	4, 0x90
	.type	clock_StartCounter,@function
clock_StartCounter:                     # @clock_StartCounter
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	clock_StartCounter, .Lfunc_end2-clock_StartCounter
	.cfi_endproc

	.globl	clock_StopPassedTime
	.p2align	4, 0x90
	.type	clock_StopPassedTime,@function
clock_StopPassedTime:                   # @clock_StopPassedTime
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	clock_StopPassedTime, .Lfunc_end3-clock_StopPassedTime
	.cfi_endproc

	.globl	clock_StopAddPassedTime
	.p2align	4, 0x90
	.type	clock_StopAddPassedTime,@function
clock_StopAddPassedTime:                # @clock_StopAddPassedTime
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	clock_StopAddPassedTime, .Lfunc_end4-clock_StopAddPassedTime
	.cfi_endproc

	.globl	clock_GetSeconds
	.p2align	4, 0x90
	.type	clock_GetSeconds,@function
clock_GetSeconds:                       # @clock_GetSeconds
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end5:
	.size	clock_GetSeconds, .Lfunc_end5-clock_GetSeconds
	.cfi_endproc

	.globl	clock_PrintTime
	.p2align	4, 0x90
	.type	clock_PrintTime,@function
clock_PrintTime:                        # @clock_PrintTime
	.cfi_startproc
# BB#0:
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$28, %esi
	movl	$1, %edx
	jmp	fwrite                  # TAILCALL
.Lfunc_end6:
	.size	clock_PrintTime, .Lfunc_end6-clock_PrintTime
	.cfi_endproc

	.type	clock_Akku,@object      # @clock_Akku
	.comm	clock_Akku,24,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" No Timing on this machine. "
	.size	.L.str, 29

	.type	clock_Counters,@object  # @clock_Counters
	.comm	clock_Counters,96,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
