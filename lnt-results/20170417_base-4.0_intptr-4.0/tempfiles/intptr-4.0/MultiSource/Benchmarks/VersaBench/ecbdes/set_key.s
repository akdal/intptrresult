	.text
	.file	"set_key.bc"
	.globl	des_set_odd_parity
	.p2align	4, 0x90
	.type	des_set_odd_parity,@function
des_set_odd_parity:                     # @des_set_odd_parity
	.cfi_startproc
# BB#0:
	movzbl	(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, (%rdi)
	movzbl	1(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 1(%rdi)
	movzbl	2(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 2(%rdi)
	movzbl	3(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 3(%rdi)
	movzbl	4(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 4(%rdi)
	movzbl	5(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 5(%rdi)
	movzbl	6(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 6(%rdi)
	movzbl	7(%rdi), %eax
	movb	odd_parity(%rax), %al
	movb	%al, 7(%rdi)
	retq
.Lfunc_end0:
	.size	des_set_odd_parity, .Lfunc_end0-des_set_odd_parity
	.cfi_endproc

	.globl	des_is_weak_key
	.p2align	4, 0x90
	.type	des_is_weak_key,@function
des_is_weak_key:                        # @des_is_weak_key
	.cfi_startproc
# BB#0:
	movq	weak_keys(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#1:
	movq	weak_keys+8(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#2:
	movq	weak_keys+16(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#3:
	movq	weak_keys+24(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#4:
	movq	weak_keys+32(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#5:
	movq	weak_keys+40(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#6:
	movq	weak_keys+48(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#7:
	movq	weak_keys+56(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#8:
	movq	weak_keys+64(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#9:
	movq	weak_keys+72(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#10:
	movq	weak_keys+80(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#11:
	movq	weak_keys+88(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#12:
	movq	weak_keys+96(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#13:
	movq	weak_keys+104(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#14:
	movq	weak_keys+112(%rip), %rax
	cmpq	(%rdi), %rax
	je	.LBB1_16
# BB#15:
	movq	weak_keys+120(%rip), %rcx
	xorl	%eax, %eax
	cmpq	(%rdi), %rcx
	sete	%al
	retq
.LBB1_16:
	movl	$1, %eax
	retq
.Lfunc_end1:
	.size	des_is_weak_key, .Lfunc_end1-des_is_weak_key
	.cfi_endproc

	.globl	des_set_key
	.p2align	4, 0x90
	.type	des_set_key,@function
des_set_key:                            # @des_set_key
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movzbl	(%rdi), %ebx
	cmpl	$0, des_check_key(%rip)
	je	.LBB2_1
# BB#2:
	movl	$-1, %eax
	cmpb	odd_parity(%rbx), %bl
	jne	.LBB2_13
# BB#3:
	movzbl	1(%rdi), %r15d
	cmpb	odd_parity(%r15), %r15b
	jne	.LBB2_13
# BB#4:
	movzbl	2(%rdi), %r12d
	cmpb	odd_parity(%r12), %r12b
	jne	.LBB2_13
# BB#5:
	movzbl	3(%rdi), %r13d
	cmpb	odd_parity(%r13), %r13b
	jne	.LBB2_13
# BB#6:
	movzbl	4(%rdi), %esi
	cmpb	odd_parity(%rsi), %sil
	jne	.LBB2_13
# BB#7:
	movzbl	5(%rdi), %ecx
	cmpb	odd_parity(%rcx), %cl
	jne	.LBB2_13
# BB#8:
	movzbl	6(%rdi), %edx
	cmpb	odd_parity(%rdx), %dl
	jne	.LBB2_13
# BB#9:                                 # %check_parity.exit
	movzbl	7(%rdi), %ebp
	cmpb	odd_parity(%rbp), %bpl
	jne	.LBB2_13
# BB#10:
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	des_is_weak_key
	movq	%rbp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	%eax, %ecx
	movl	$-2, %eax
	testl	%ecx, %ecx
	jne	.LBB2_13
	jmp	.LBB2_11
.LBB2_1:                                # %._crit_edge
	movb	1(%rdi), %r15b
	movb	2(%rdi), %r12b
	movb	3(%rdi), %r13b
	movb	4(%rdi), %sil
	movb	5(%rdi), %r8b
	movb	6(%rdi), %bpl
	movb	7(%rdi), %dil
.LBB2_11:
	movzbl	%r15b, %eax
	shlq	$8, %rax
	orq	%rbx, %rax
	movzbl	%r12b, %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	%r13b, %ecx
	shlq	$24, %rcx
	orq	%rdx, %rcx
	movzbl	%sil, %eax
	movzbl	%r8b, %edx
	shlq	$8, %rdx
	orq	%rax, %rdx
	movzbl	%bpl, %eax
	shlq	$16, %rax
	orq	%rdx, %rax
	movzbl	%dil, %esi
	shlq	$24, %rsi
	orq	%rax, %rsi
	movl	%esi, %edx
	shrl	$4, %edx
	xorl	%ecx, %edx
	andl	$252645135, %edx        # imm = 0xF0F0F0F
	xorq	%rdx, %rcx
	shlq	$4, %rdx
	xorq	%rsi, %rdx
	movl	%ecx, %eax
	shll	$18, %eax
	xorl	%ecx, %eax
	andl	$-859045888, %eax       # imm = 0xCCCC0000
	xorq	%rax, %rcx
	shrq	$18, %rax
	xorq	%rcx, %rax
	movl	%edx, %esi
	shll	$18, %esi
	xorl	%edx, %esi
	andl	$-859045888, %esi       # imm = 0xCCCC0000
	xorq	%rsi, %rdx
	shrq	$18, %rsi
	xorq	%rdx, %rsi
	movl	%esi, %ecx
	shrl	%ecx
	xorl	%eax, %ecx
	andl	$1431655765, %ecx       # imm = 0x55555555
	xorq	%rcx, %rax
	addq	%rcx, %rcx
	xorq	%rsi, %rcx
	movl	%eax, %r9d
	shrl	$8, %r9d
	xorl	%ecx, %r9d
	andl	$16711935, %r9d         # imm = 0xFF00FF
	xorq	%r9, %rcx
	shlq	$8, %r9
	xorq	%rax, %r9
	movl	%ecx, %eax
	shrl	%eax
	xorl	%r9d, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	xorq	%rax, %r9
	addq	%rax, %rax
	xorq	%rcx, %rax
	movq	%rax, %rcx
	shlq	$16, %rcx
	andl	$16711680, %ecx         # imm = 0xFF0000
	movl	%eax, %esi
	andl	$65280, %esi            # imm = 0xFF00
	shrl	$16, %eax
	movzbl	%al, %eax
	movq	%r9, %rdx
	shrq	$4, %rdx
	andl	$251658240, %edx        # imm = 0xF000000
	orq	%rsi, %rdx
	orq	%rcx, %rdx
	orq	%rax, %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	andl	$268435455, %r9d        # imm = 0xFFFFFFF
	movl	$32508, %esi            # imm = 0x7EFC
	movl	%r8d, %ecx
	shrq	%cl, %rsi
	andl	$1, %esi
	movl	$27, %ebp
	subl	%esi, %ebp
	incl	%esi
	movq	%r9, %rdi
	movl	%esi, %ecx
	shrq	%cl, %rdi
	movl	%ebp, %ecx
	shlq	%cl, %r9
	orq	%rdi, %r9
	movq	%rdx, %r10
	movl	%esi, %ecx
	shrq	%cl, %r10
	movl	%ebp, %ecx
	shlq	%cl, %rdx
	orq	%r10, %rdx
	movl	%edx, %ecx
	andl	$268435455, %ecx        # imm = 0xFFFFFFF
	movl	%edi, %ebx
	andl	$63, %ebx
	movl	%edi, %esi
	shrl	$6, %esi
	andl	$3, %esi
	movq	%rdi, %rbp
	shrq	$7, %rbp
	andl	$60, %ebp
	orq	%rsi, %rbp
	movq	des_skb+512(,%rbp,8), %rsi
	orq	des_skb(,%rbx,8), %rsi
	movl	%edi, %ebp
	shrl	$13, %ebp
	andl	$15, %ebp
	movq	%rdi, %rbx
	shrq	$14, %rbx
	andl	$48, %ebx
	orq	%rbp, %rbx
	orq	des_skb+1024(,%rbx,8), %rsi
	movl	%edi, %ebp
	shrl	$20, %ebp
	andl	$1, %ebp
	shrq	$21, %rdi
	andl	$6, %edi
	orq	%rbp, %rdi
	movq	%r9, %rbp
	shrq	$22, %rbp
	andl	$56, %ebp
	orq	%rdi, %rbp
	orq	des_skb+1536(,%rbp,8), %rsi
	movl	%r10d, %ebp
	andl	$63, %ebp
	movl	%r10d, %edi
	shrl	$7, %edi
	andl	$3, %edi
	movq	%r10, %rbx
	shrq	$8, %rbx
	andl	$60, %ebx
	orq	%rdi, %rbx
	movq	des_skb+2560(,%rbx,8), %rbx
	orq	des_skb+2048(,%rbp,8), %rbx
	movq	%r10, %rbp
	shrq	$12, %rbp
	andl	$504, %ebp              # imm = 0x1F8
	orq	des_skb+3072(%rbp), %rbx
	shrl	$21, %r10d
	andl	$15, %r10d
	shrq	$22, %rdx
	andl	$48, %edx
	orq	%r10, %rdx
	orq	des_skb+3584(,%rdx,8), %rbx
	movzwl	%si, %edx
	movq	%rbx, %rdi
	andq	$65535, %rdi            # imm = 0xFFFF
	shlq	$16, %rdi
	orl	%edi, %edx
	shll	$2, %edx
	shrq	$30, %rdi
	orq	%rdi, %rdx
	movq	%rdx, (%r14)
	shrq	$16, %rsi
	andl	$-65536, %ebx           # imm = 0xFFFF0000
	orq	%rsi, %rbx
	movq	%rbx, %rdx
	shrq	$26, %rdx
	andq	$67108863, %rbx         # imm = 0x3FFFFFF
	shlq	$6, %rbx
	orq	%rdx, %rbx
	movq	%rbx, 8(%r14)
	incq	%r8
	addq	$16, %r14
	cmpq	$16, %r8
	movq	%rcx, %rdx
	jne	.LBB2_12
.LBB2_13:                               # %check_parity.exit.thread
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	des_set_key, .Lfunc_end2-des_set_key
	.cfi_endproc

	.globl	des_key_sched
	.p2align	4, 0x90
	.type	des_key_sched,@function
des_key_sched:                          # @des_key_sched
	.cfi_startproc
# BB#0:
	jmp	des_set_key             # TAILCALL
.Lfunc_end3:
	.size	des_key_sched, .Lfunc_end3-des_key_sched
	.cfi_endproc

	.type	des_check_key,@object   # @des_check_key
	.bss
	.globl	des_check_key
	.p2align	2
des_check_key:
	.long	0                       # 0x0
	.size	des_check_key, 4

	.type	odd_parity,@object      # @odd_parity
	.section	.rodata,"a",@progbits
	.p2align	4
odd_parity:
	.ascii	"\001\001\002\002\004\004\007\007\b\b\013\013\r\r\016\016\020\020\023\023\025\025\026\026\031\031\032\032\034\034\037\037  ##%%&&))**,,//1122447788;;==>>@@CCEEFFIIJJLLOOQQRRTTWWXX[[]]^^aabbddgghhkkmmnnppssuuvvyyzz||\177\177\200\200\203\203\205\205\206\206\211\211\212\212\214\214\217\217\221\221\222\222\224\224\227\227\230\230\233\233\235\235\236\236\241\241\242\242\244\244\247\247\250\250\253\253\255\255\256\256\260\260\263\263\265\265\266\266\271\271\272\272\274\274\277\277\301\301\302\302\304\304\307\307\310\310\313\313\315\315\316\316\320\320\323\323\325\325\326\326\331\331\332\332\334\334\337\337\340\340\343\343\345\345\346\346\351\351\352\352\354\354\357\357\361\361\362\362\364\364\367\367\370\370\373\373\375\375\376\376"
	.size	odd_parity, 256

	.type	weak_keys,@object       # @weak_keys
	.data
	.p2align	4
weak_keys:
	.zero	8,1
	.zero	8,254
	.zero	8,31
	.zero	8,224
	.ascii	"\001\376\001\376\001\376\001\376"
	.ascii	"\376\001\376\001\376\001\376\001"
	.ascii	"\037\340\037\340\016\361\016\361"
	.ascii	"\340\037\340\037\361\016\361\016"
	.ascii	"\001\340\001\340\001\361\001\361"
	.ascii	"\340\001\340\001\361\001\361\001"
	.ascii	"\037\376\037\376\016\376\016\376"
	.ascii	"\376\037\376\037\376\016\376\016"
	.ascii	"\001\037\001\037\001\016\001\016"
	.ascii	"\037\001\037\001\016\001\016\001"
	.ascii	"\340\376\340\376\361\376\361\376"
	.ascii	"\376\340\376\340\376\361\376\361"
	.size	weak_keys, 128

	.type	des_skb,@object         # @des_skb
	.section	.rodata,"a",@progbits
	.p2align	4
des_skb:
	.quad	0                       # 0x0
	.quad	16                      # 0x10
	.quad	536870912               # 0x20000000
	.quad	536870928               # 0x20000010
	.quad	65536                   # 0x10000
	.quad	65552                   # 0x10010
	.quad	536936448               # 0x20010000
	.quad	536936464               # 0x20010010
	.quad	2048                    # 0x800
	.quad	2064                    # 0x810
	.quad	536872960               # 0x20000800
	.quad	536872976               # 0x20000810
	.quad	67584                   # 0x10800
	.quad	67600                   # 0x10810
	.quad	536938496               # 0x20010800
	.quad	536938512               # 0x20010810
	.quad	32                      # 0x20
	.quad	48                      # 0x30
	.quad	536870944               # 0x20000020
	.quad	536870960               # 0x20000030
	.quad	65568                   # 0x10020
	.quad	65584                   # 0x10030
	.quad	536936480               # 0x20010020
	.quad	536936496               # 0x20010030
	.quad	2080                    # 0x820
	.quad	2096                    # 0x830
	.quad	536872992               # 0x20000820
	.quad	536873008               # 0x20000830
	.quad	67616                   # 0x10820
	.quad	67632                   # 0x10830
	.quad	536938528               # 0x20010820
	.quad	536938544               # 0x20010830
	.quad	524288                  # 0x80000
	.quad	524304                  # 0x80010
	.quad	537395200               # 0x20080000
	.quad	537395216               # 0x20080010
	.quad	589824                  # 0x90000
	.quad	589840                  # 0x90010
	.quad	537460736               # 0x20090000
	.quad	537460752               # 0x20090010
	.quad	526336                  # 0x80800
	.quad	526352                  # 0x80810
	.quad	537397248               # 0x20080800
	.quad	537397264               # 0x20080810
	.quad	591872                  # 0x90800
	.quad	591888                  # 0x90810
	.quad	537462784               # 0x20090800
	.quad	537462800               # 0x20090810
	.quad	524320                  # 0x80020
	.quad	524336                  # 0x80030
	.quad	537395232               # 0x20080020
	.quad	537395248               # 0x20080030
	.quad	589856                  # 0x90020
	.quad	589872                  # 0x90030
	.quad	537460768               # 0x20090020
	.quad	537460784               # 0x20090030
	.quad	526368                  # 0x80820
	.quad	526384                  # 0x80830
	.quad	537397280               # 0x20080820
	.quad	537397296               # 0x20080830
	.quad	591904                  # 0x90820
	.quad	591920                  # 0x90830
	.quad	537462816               # 0x20090820
	.quad	537462832               # 0x20090830
	.quad	0                       # 0x0
	.quad	33554432                # 0x2000000
	.quad	8192                    # 0x2000
	.quad	33562624                # 0x2002000
	.quad	2097152                 # 0x200000
	.quad	35651584                # 0x2200000
	.quad	2105344                 # 0x202000
	.quad	35659776                # 0x2202000
	.quad	4                       # 0x4
	.quad	33554436                # 0x2000004
	.quad	8196                    # 0x2004
	.quad	33562628                # 0x2002004
	.quad	2097156                 # 0x200004
	.quad	35651588                # 0x2200004
	.quad	2105348                 # 0x202004
	.quad	35659780                # 0x2202004
	.quad	1024                    # 0x400
	.quad	33555456                # 0x2000400
	.quad	9216                    # 0x2400
	.quad	33563648                # 0x2002400
	.quad	2098176                 # 0x200400
	.quad	35652608                # 0x2200400
	.quad	2106368                 # 0x202400
	.quad	35660800                # 0x2202400
	.quad	1028                    # 0x404
	.quad	33555460                # 0x2000404
	.quad	9220                    # 0x2404
	.quad	33563652                # 0x2002404
	.quad	2098180                 # 0x200404
	.quad	35652612                # 0x2200404
	.quad	2106372                 # 0x202404
	.quad	35660804                # 0x2202404
	.quad	268435456               # 0x10000000
	.quad	301989888               # 0x12000000
	.quad	268443648               # 0x10002000
	.quad	301998080               # 0x12002000
	.quad	270532608               # 0x10200000
	.quad	304087040               # 0x12200000
	.quad	270540800               # 0x10202000
	.quad	304095232               # 0x12202000
	.quad	268435460               # 0x10000004
	.quad	301989892               # 0x12000004
	.quad	268443652               # 0x10002004
	.quad	301998084               # 0x12002004
	.quad	270532612               # 0x10200004
	.quad	304087044               # 0x12200004
	.quad	270540804               # 0x10202004
	.quad	304095236               # 0x12202004
	.quad	268436480               # 0x10000400
	.quad	301990912               # 0x12000400
	.quad	268444672               # 0x10002400
	.quad	301999104               # 0x12002400
	.quad	270533632               # 0x10200400
	.quad	304088064               # 0x12200400
	.quad	270541824               # 0x10202400
	.quad	304096256               # 0x12202400
	.quad	268436484               # 0x10000404
	.quad	301990916               # 0x12000404
	.quad	268444676               # 0x10002404
	.quad	301999108               # 0x12002404
	.quad	270533636               # 0x10200404
	.quad	304088068               # 0x12200404
	.quad	270541828               # 0x10202404
	.quad	304096260               # 0x12202404
	.quad	0                       # 0x0
	.quad	1                       # 0x1
	.quad	262144                  # 0x40000
	.quad	262145                  # 0x40001
	.quad	16777216                # 0x1000000
	.quad	16777217                # 0x1000001
	.quad	17039360                # 0x1040000
	.quad	17039361                # 0x1040001
	.quad	2                       # 0x2
	.quad	3                       # 0x3
	.quad	262146                  # 0x40002
	.quad	262147                  # 0x40003
	.quad	16777218                # 0x1000002
	.quad	16777219                # 0x1000003
	.quad	17039362                # 0x1040002
	.quad	17039363                # 0x1040003
	.quad	512                     # 0x200
	.quad	513                     # 0x201
	.quad	262656                  # 0x40200
	.quad	262657                  # 0x40201
	.quad	16777728                # 0x1000200
	.quad	16777729                # 0x1000201
	.quad	17039872                # 0x1040200
	.quad	17039873                # 0x1040201
	.quad	514                     # 0x202
	.quad	515                     # 0x203
	.quad	262658                  # 0x40202
	.quad	262659                  # 0x40203
	.quad	16777730                # 0x1000202
	.quad	16777731                # 0x1000203
	.quad	17039874                # 0x1040202
	.quad	17039875                # 0x1040203
	.quad	134217728               # 0x8000000
	.quad	134217729               # 0x8000001
	.quad	134479872               # 0x8040000
	.quad	134479873               # 0x8040001
	.quad	150994944               # 0x9000000
	.quad	150994945               # 0x9000001
	.quad	151257088               # 0x9040000
	.quad	151257089               # 0x9040001
	.quad	134217730               # 0x8000002
	.quad	134217731               # 0x8000003
	.quad	134479874               # 0x8040002
	.quad	134479875               # 0x8040003
	.quad	150994946               # 0x9000002
	.quad	150994947               # 0x9000003
	.quad	151257090               # 0x9040002
	.quad	151257091               # 0x9040003
	.quad	134218240               # 0x8000200
	.quad	134218241               # 0x8000201
	.quad	134480384               # 0x8040200
	.quad	134480385               # 0x8040201
	.quad	150995456               # 0x9000200
	.quad	150995457               # 0x9000201
	.quad	151257600               # 0x9040200
	.quad	151257601               # 0x9040201
	.quad	134218242               # 0x8000202
	.quad	134218243               # 0x8000203
	.quad	134480386               # 0x8040202
	.quad	134480387               # 0x8040203
	.quad	150995458               # 0x9000202
	.quad	150995459               # 0x9000203
	.quad	151257602               # 0x9040202
	.quad	151257603               # 0x9040203
	.quad	0                       # 0x0
	.quad	1048576                 # 0x100000
	.quad	256                     # 0x100
	.quad	1048832                 # 0x100100
	.quad	8                       # 0x8
	.quad	1048584                 # 0x100008
	.quad	264                     # 0x108
	.quad	1048840                 # 0x100108
	.quad	4096                    # 0x1000
	.quad	1052672                 # 0x101000
	.quad	4352                    # 0x1100
	.quad	1052928                 # 0x101100
	.quad	4104                    # 0x1008
	.quad	1052680                 # 0x101008
	.quad	4360                    # 0x1108
	.quad	1052936                 # 0x101108
	.quad	67108864                # 0x4000000
	.quad	68157440                # 0x4100000
	.quad	67109120                # 0x4000100
	.quad	68157696                # 0x4100100
	.quad	67108872                # 0x4000008
	.quad	68157448                # 0x4100008
	.quad	67109128                # 0x4000108
	.quad	68157704                # 0x4100108
	.quad	67112960                # 0x4001000
	.quad	68161536                # 0x4101000
	.quad	67113216                # 0x4001100
	.quad	68161792                # 0x4101100
	.quad	67112968                # 0x4001008
	.quad	68161544                # 0x4101008
	.quad	67113224                # 0x4001108
	.quad	68161800                # 0x4101108
	.quad	131072                  # 0x20000
	.quad	1179648                 # 0x120000
	.quad	131328                  # 0x20100
	.quad	1179904                 # 0x120100
	.quad	131080                  # 0x20008
	.quad	1179656                 # 0x120008
	.quad	131336                  # 0x20108
	.quad	1179912                 # 0x120108
	.quad	135168                  # 0x21000
	.quad	1183744                 # 0x121000
	.quad	135424                  # 0x21100
	.quad	1184000                 # 0x121100
	.quad	135176                  # 0x21008
	.quad	1183752                 # 0x121008
	.quad	135432                  # 0x21108
	.quad	1184008                 # 0x121108
	.quad	67239936                # 0x4020000
	.quad	68288512                # 0x4120000
	.quad	67240192                # 0x4020100
	.quad	68288768                # 0x4120100
	.quad	67239944                # 0x4020008
	.quad	68288520                # 0x4120008
	.quad	67240200                # 0x4020108
	.quad	68288776                # 0x4120108
	.quad	67244032                # 0x4021000
	.quad	68292608                # 0x4121000
	.quad	67244288                # 0x4021100
	.quad	68292864                # 0x4121100
	.quad	67244040                # 0x4021008
	.quad	68292616                # 0x4121008
	.quad	67244296                # 0x4021108
	.quad	68292872                # 0x4121108
	.quad	0                       # 0x0
	.quad	268435456               # 0x10000000
	.quad	65536                   # 0x10000
	.quad	268500992               # 0x10010000
	.quad	4                       # 0x4
	.quad	268435460               # 0x10000004
	.quad	65540                   # 0x10004
	.quad	268500996               # 0x10010004
	.quad	536870912               # 0x20000000
	.quad	805306368               # 0x30000000
	.quad	536936448               # 0x20010000
	.quad	805371904               # 0x30010000
	.quad	536870916               # 0x20000004
	.quad	805306372               # 0x30000004
	.quad	536936452               # 0x20010004
	.quad	805371908               # 0x30010004
	.quad	1048576                 # 0x100000
	.quad	269484032               # 0x10100000
	.quad	1114112                 # 0x110000
	.quad	269549568               # 0x10110000
	.quad	1048580                 # 0x100004
	.quad	269484036               # 0x10100004
	.quad	1114116                 # 0x110004
	.quad	269549572               # 0x10110004
	.quad	537919488               # 0x20100000
	.quad	806354944               # 0x30100000
	.quad	537985024               # 0x20110000
	.quad	806420480               # 0x30110000
	.quad	537919492               # 0x20100004
	.quad	806354948               # 0x30100004
	.quad	537985028               # 0x20110004
	.quad	806420484               # 0x30110004
	.quad	4096                    # 0x1000
	.quad	268439552               # 0x10001000
	.quad	69632                   # 0x11000
	.quad	268505088               # 0x10011000
	.quad	4100                    # 0x1004
	.quad	268439556               # 0x10001004
	.quad	69636                   # 0x11004
	.quad	268505092               # 0x10011004
	.quad	536875008               # 0x20001000
	.quad	805310464               # 0x30001000
	.quad	536940544               # 0x20011000
	.quad	805376000               # 0x30011000
	.quad	536875012               # 0x20001004
	.quad	805310468               # 0x30001004
	.quad	536940548               # 0x20011004
	.quad	805376004               # 0x30011004
	.quad	1052672                 # 0x101000
	.quad	269488128               # 0x10101000
	.quad	1118208                 # 0x111000
	.quad	269553664               # 0x10111000
	.quad	1052676                 # 0x101004
	.quad	269488132               # 0x10101004
	.quad	1118212                 # 0x111004
	.quad	269553668               # 0x10111004
	.quad	537923584               # 0x20101000
	.quad	806359040               # 0x30101000
	.quad	537989120               # 0x20111000
	.quad	806424576               # 0x30111000
	.quad	537923588               # 0x20101004
	.quad	806359044               # 0x30101004
	.quad	537989124               # 0x20111004
	.quad	806424580               # 0x30111004
	.quad	0                       # 0x0
	.quad	134217728               # 0x8000000
	.quad	8                       # 0x8
	.quad	134217736               # 0x8000008
	.quad	1024                    # 0x400
	.quad	134218752               # 0x8000400
	.quad	1032                    # 0x408
	.quad	134218760               # 0x8000408
	.quad	131072                  # 0x20000
	.quad	134348800               # 0x8020000
	.quad	131080                  # 0x20008
	.quad	134348808               # 0x8020008
	.quad	132096                  # 0x20400
	.quad	134349824               # 0x8020400
	.quad	132104                  # 0x20408
	.quad	134349832               # 0x8020408
	.quad	1                       # 0x1
	.quad	134217729               # 0x8000001
	.quad	9                       # 0x9
	.quad	134217737               # 0x8000009
	.quad	1025                    # 0x401
	.quad	134218753               # 0x8000401
	.quad	1033                    # 0x409
	.quad	134218761               # 0x8000409
	.quad	131073                  # 0x20001
	.quad	134348801               # 0x8020001
	.quad	131081                  # 0x20009
	.quad	134348809               # 0x8020009
	.quad	132097                  # 0x20401
	.quad	134349825               # 0x8020401
	.quad	132105                  # 0x20409
	.quad	134349833               # 0x8020409
	.quad	33554432                # 0x2000000
	.quad	167772160               # 0xa000000
	.quad	33554440                # 0x2000008
	.quad	167772168               # 0xa000008
	.quad	33555456                # 0x2000400
	.quad	167773184               # 0xa000400
	.quad	33555464                # 0x2000408
	.quad	167773192               # 0xa000408
	.quad	33685504                # 0x2020000
	.quad	167903232               # 0xa020000
	.quad	33685512                # 0x2020008
	.quad	167903240               # 0xa020008
	.quad	33686528                # 0x2020400
	.quad	167904256               # 0xa020400
	.quad	33686536                # 0x2020408
	.quad	167904264               # 0xa020408
	.quad	33554433                # 0x2000001
	.quad	167772161               # 0xa000001
	.quad	33554441                # 0x2000009
	.quad	167772169               # 0xa000009
	.quad	33555457                # 0x2000401
	.quad	167773185               # 0xa000401
	.quad	33555465                # 0x2000409
	.quad	167773193               # 0xa000409
	.quad	33685505                # 0x2020001
	.quad	167903233               # 0xa020001
	.quad	33685513                # 0x2020009
	.quad	167903241               # 0xa020009
	.quad	33686529                # 0x2020401
	.quad	167904257               # 0xa020401
	.quad	33686537                # 0x2020409
	.quad	167904265               # 0xa020409
	.quad	0                       # 0x0
	.quad	256                     # 0x100
	.quad	524288                  # 0x80000
	.quad	524544                  # 0x80100
	.quad	16777216                # 0x1000000
	.quad	16777472                # 0x1000100
	.quad	17301504                # 0x1080000
	.quad	17301760                # 0x1080100
	.quad	16                      # 0x10
	.quad	272                     # 0x110
	.quad	524304                  # 0x80010
	.quad	524560                  # 0x80110
	.quad	16777232                # 0x1000010
	.quad	16777488                # 0x1000110
	.quad	17301520                # 0x1080010
	.quad	17301776                # 0x1080110
	.quad	2097152                 # 0x200000
	.quad	2097408                 # 0x200100
	.quad	2621440                 # 0x280000
	.quad	2621696                 # 0x280100
	.quad	18874368                # 0x1200000
	.quad	18874624                # 0x1200100
	.quad	19398656                # 0x1280000
	.quad	19398912                # 0x1280100
	.quad	2097168                 # 0x200010
	.quad	2097424                 # 0x200110
	.quad	2621456                 # 0x280010
	.quad	2621712                 # 0x280110
	.quad	18874384                # 0x1200010
	.quad	18874640                # 0x1200110
	.quad	19398672                # 0x1280010
	.quad	19398928                # 0x1280110
	.quad	512                     # 0x200
	.quad	768                     # 0x300
	.quad	524800                  # 0x80200
	.quad	525056                  # 0x80300
	.quad	16777728                # 0x1000200
	.quad	16777984                # 0x1000300
	.quad	17302016                # 0x1080200
	.quad	17302272                # 0x1080300
	.quad	528                     # 0x210
	.quad	784                     # 0x310
	.quad	524816                  # 0x80210
	.quad	525072                  # 0x80310
	.quad	16777744                # 0x1000210
	.quad	16778000                # 0x1000310
	.quad	17302032                # 0x1080210
	.quad	17302288                # 0x1080310
	.quad	2097664                 # 0x200200
	.quad	2097920                 # 0x200300
	.quad	2621952                 # 0x280200
	.quad	2622208                 # 0x280300
	.quad	18874880                # 0x1200200
	.quad	18875136                # 0x1200300
	.quad	19399168                # 0x1280200
	.quad	19399424                # 0x1280300
	.quad	2097680                 # 0x200210
	.quad	2097936                 # 0x200310
	.quad	2621968                 # 0x280210
	.quad	2622224                 # 0x280310
	.quad	18874896                # 0x1200210
	.quad	18875152                # 0x1200310
	.quad	19399184                # 0x1280210
	.quad	19399440                # 0x1280310
	.quad	0                       # 0x0
	.quad	67108864                # 0x4000000
	.quad	262144                  # 0x40000
	.quad	67371008                # 0x4040000
	.quad	2                       # 0x2
	.quad	67108866                # 0x4000002
	.quad	262146                  # 0x40002
	.quad	67371010                # 0x4040002
	.quad	8192                    # 0x2000
	.quad	67117056                # 0x4002000
	.quad	270336                  # 0x42000
	.quad	67379200                # 0x4042000
	.quad	8194                    # 0x2002
	.quad	67117058                # 0x4002002
	.quad	270338                  # 0x42002
	.quad	67379202                # 0x4042002
	.quad	32                      # 0x20
	.quad	67108896                # 0x4000020
	.quad	262176                  # 0x40020
	.quad	67371040                # 0x4040020
	.quad	34                      # 0x22
	.quad	67108898                # 0x4000022
	.quad	262178                  # 0x40022
	.quad	67371042                # 0x4040022
	.quad	8224                    # 0x2020
	.quad	67117088                # 0x4002020
	.quad	270368                  # 0x42020
	.quad	67379232                # 0x4042020
	.quad	8226                    # 0x2022
	.quad	67117090                # 0x4002022
	.quad	270370                  # 0x42022
	.quad	67379234                # 0x4042022
	.quad	2048                    # 0x800
	.quad	67110912                # 0x4000800
	.quad	264192                  # 0x40800
	.quad	67373056                # 0x4040800
	.quad	2050                    # 0x802
	.quad	67110914                # 0x4000802
	.quad	264194                  # 0x40802
	.quad	67373058                # 0x4040802
	.quad	10240                   # 0x2800
	.quad	67119104                # 0x4002800
	.quad	272384                  # 0x42800
	.quad	67381248                # 0x4042800
	.quad	10242                   # 0x2802
	.quad	67119106                # 0x4002802
	.quad	272386                  # 0x42802
	.quad	67381250                # 0x4042802
	.quad	2080                    # 0x820
	.quad	67110944                # 0x4000820
	.quad	264224                  # 0x40820
	.quad	67373088                # 0x4040820
	.quad	2082                    # 0x822
	.quad	67110946                # 0x4000822
	.quad	264226                  # 0x40822
	.quad	67373090                # 0x4040822
	.quad	10272                   # 0x2820
	.quad	67119136                # 0x4002820
	.quad	272416                  # 0x42820
	.quad	67381280                # 0x4042820
	.quad	10274                   # 0x2822
	.quad	67119138                # 0x4002822
	.quad	272418                  # 0x42822
	.quad	67381282                # 0x4042822
	.size	des_skb, 4096


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
