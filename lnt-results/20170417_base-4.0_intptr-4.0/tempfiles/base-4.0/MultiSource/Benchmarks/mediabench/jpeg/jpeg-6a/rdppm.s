	.text
	.file	"rdppm.bc"
	.globl	jinit_read_ppm
	.p2align	4, 0x90
	.type	jinit_read_ppm,@function
jinit_read_ppm:                         # @jinit_read_ppm
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rax
	movl	$1, %esi
	movl	$80, %edx
	callq	*(%rax)
	movq	$start_input_ppm, (%rax)
	movq	$finish_input_ppm, 16(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	jinit_read_ppm, .Lfunc_end0-jinit_read_ppm
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_ppm,@function
start_input_ppm:                        # @start_input_ppm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	24(%r15), %rdi
	callq	_IO_getc
	cmpl	$80, %eax
	je	.LBB1_2
# BB#1:
	movq	(%r13), %rax
	movl	$1027, 40(%rax)         # imm = 0x403
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_2:
	movq	24(%r15), %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%r15), %rbx
	movabsq	$8589954048, %r14       # imm = 0x200004C00
	.p2align	4, 0x90
.LBB1_3:                                # %.critedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	cmpl	$35, %r12d
	jne	.LBB1_6
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader.i.i
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	cmpl	$10, %r12d
	je	.LBB1_6
# BB#5:                                 # %.preheader.i.i
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$-1, %r12d
	jne	.LBB1_4
.LBB1_6:                                # %pbm_getc.exit.i
                                        #   in Loop: Header=BB1_3 Depth=1
	leal	1(%r12), %eax
	cmpl	$33, %eax
	ja	.LBB1_10
# BB#7:                                 # %pbm_getc.exit.i
                                        #   in Loop: Header=BB1_3 Depth=1
	btq	%rax, %r14
	jb	.LBB1_3
# BB#8:                                 # %pbm_getc.exit.i
	testq	%rax, %rax
	jne	.LBB1_10
# BB#9:                                 # %.thread.i
	movq	(%r13), %rax
	movl	$42, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	$-49, %r12d
	jmp	.LBB1_11
.LBB1_10:
	addl	$-48, %r12d
	cmpl	$10, %r12d
	jb	.LBB1_12
.LBB1_11:
	movq	(%r13), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r13, %rdi
	callq	*(%rax)
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_15:                               # %pbm_getc.exit33.i
                                        #   in Loop: Header=BB1_12 Depth=1
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	leal	(%r12,%r12,4), %ecx
	leal	(%rax,%rcx,2), %r12d
.LBB1_12:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_13 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB1_15
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader.i31.i
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB1_15
# BB#14:                                # %.preheader.i31.i
                                        #   in Loop: Header=BB1_13 Depth=2
	cmpl	$-1, %eax
	jne	.LBB1_13
	jmp	.LBB1_15
.LBB1_17:                               # %read_pbm_integer.exit
	movq	24(%r15), %rbx
	.p2align	4, 0x90
.LBB1_18:                               # %.critedge.i133
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	jne	.LBB1_21
	.p2align	4, 0x90
.LBB1_19:                               # %.preheader.i.i134
                                        #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB1_21
# BB#20:                                # %.preheader.i.i134
                                        #   in Loop: Header=BB1_19 Depth=2
	cmpl	$-1, %ebp
	jne	.LBB1_19
.LBB1_21:                               # %pbm_getc.exit.i136
                                        #   in Loop: Header=BB1_18 Depth=1
	leal	1(%rbp), %eax
	cmpl	$33, %eax
	ja	.LBB1_25
# BB#22:                                # %pbm_getc.exit.i136
                                        #   in Loop: Header=BB1_18 Depth=1
	btq	%rax, %r14
	jb	.LBB1_18
# BB#23:                                # %pbm_getc.exit.i136
	testq	%rax, %rax
	jne	.LBB1_25
# BB#24:                                # %.thread.i137
	movq	(%r13), %rax
	movl	$42, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	$-49, %ebp
	jmp	.LBB1_26
.LBB1_25:
	addl	$-48, %ebp
	cmpl	$10, %ebp
	jb	.LBB1_27
.LBB1_26:
	movq	(%r13), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r13, %rdi
	callq	*(%rax)
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_30:                               # %pbm_getc.exit33.i150
                                        #   in Loop: Header=BB1_27 Depth=1
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_27 Depth=1
	leal	(%rbp,%rbp,4), %ecx
	leal	(%rax,%rcx,2), %ebp
.LBB1_27:                               # %.preheader.i146
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_28 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB1_30
	.p2align	4, 0x90
.LBB1_28:                               # %.preheader.i31.i147
                                        #   Parent Loop BB1_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB1_30
# BB#29:                                # %.preheader.i31.i147
                                        #   in Loop: Header=BB1_28 Depth=2
	cmpl	$-1, %eax
	jne	.LBB1_28
	jmp	.LBB1_30
.LBB1_32:                               # %read_pbm_integer.exit151
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	24(%r15), %r13
	.p2align	4, 0x90
.LBB1_33:                               # %.critedge.i152
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_34 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB1_36
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader.i.i153
                                        #   Parent Loop BB1_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB1_36
# BB#35:                                # %.preheader.i.i153
                                        #   in Loop: Header=BB1_34 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB1_34
.LBB1_36:                               # %pbm_getc.exit.i155
                                        #   in Loop: Header=BB1_33 Depth=1
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB1_40
# BB#37:                                # %pbm_getc.exit.i155
                                        #   in Loop: Header=BB1_33 Depth=1
	btq	%rax, %r14
	jb	.LBB1_33
# BB#38:                                # %pbm_getc.exit.i155
	testq	%rax, %rax
	jne	.LBB1_40
# BB#39:                                # %.thread.i156
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB1_42
.LBB1_40:
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB1_43
# BB#41:                                # %._crit_edge.i160
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB1_42:
	movq	(%r14), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_43:                               # %.preheader.i165.preheader
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_47:                               # %pbm_getc.exit33.i169
                                        #   in Loop: Header=BB1_44 Depth=1
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_44 Depth=1
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB1_44:                               # %.preheader.i165
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_45 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB1_47
	.p2align	4, 0x90
.LBB1_45:                               # %.preheader.i31.i166
                                        #   Parent Loop BB1_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB1_47
# BB#46:                                # %.preheader.i31.i166
                                        #   in Loop: Header=BB1_45 Depth=2
	cmpl	$-1, %eax
	jne	.LBB1_45
	jmp	.LBB1_47
.LBB1_49:                               # %read_pbm_integer.exit170
	testl	%r12d, %r12d
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB1_52
# BB#50:                                # %read_pbm_integer.exit170
	testl	%ebp, %ebp
	je	.LBB1_52
# BB#51:                                # %read_pbm_integer.exit170
	testl	%ebx, %ebx
	jne	.LBB1_53
.LBB1_52:
	movq	(%r13), %rax
	movl	$1027, 40(%rax)         # imm = 0x403
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_53:
	movl	$8, 64(%r13)
	movl	%r12d, 40(%r13)
	movl	%ebp, 44(%r13)
	addl	$-50, %r14d
	cmpl	$4, %r14d
	ja	.LBB1_65
# BB#54:
	jmpq	*.LJTI1_0(,%r14,8)
.LBB1_71:
	leaq	48(%r13), %r14
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 48(%r13)
	movq	(%r13), %rax
	movl	$1029, 40(%rax)         # imm = 0x405
	movl	%r12d, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	movl	$get_text_gray_row, %eax
	jmp	.LBB1_72
.LBB1_55:
	leaq	48(%r13), %r14
	movabsq	$8589934595, %rax       # imm = 0x200000003
	movq	%rax, 48(%r13)
	movq	(%r13), %rax
	movl	$1031, 40(%rax)         # imm = 0x407
	movl	%r12d, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	movl	$get_text_rgb_row, %eax
.LBB1_72:                               # %.thread.sink.split
	movq	%rax, 8(%r15)
	leaq	8(%r13), %rax
	movl	$1, %ebp
	jmp	.LBB1_73
.LBB1_56:
	leaq	48(%r13), %r14
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 48(%r13)
	movq	(%r13), %rax
	movl	$1028, 40(%rax)         # imm = 0x404
	movl	%r12d, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	cmpl	$256, %ebx              # imm = 0x100
	jb	.LBB1_58
# BB#57:
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	movq	$get_word_gray_row, 8(%r15)
	jmp	.LBB1_67
.LBB1_61:
	leaq	48(%r13), %r14
	movabsq	$8589934595, %rax       # imm = 0x200000003
	movq	%rax, 48(%r13)
	movq	(%r13), %rax
	movl	$1030, 40(%rax)         # imm = 0x406
	movl	%r12d, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*8(%rax)
	cmpl	$256, %ebx              # imm = 0x100
	jb	.LBB1_63
# BB#62:
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	movq	$get_word_rgb_row, 8(%r15)
	jmp	.LBB1_67
.LBB1_65:
	movq	(%r13), %rax
	movl	$1027, 40(%rax)         # imm = 0x403
	movq	%r13, %rdi
	callq	*(%rax)
	leaq	48(%r13), %r14
	jmp	.LBB1_66
.LBB1_58:
	cmpl	$255, %ebx
	je	.LBB1_59
# BB#60:
	movq	$get_scaled_gray_row, 8(%r15)
	jmp	.LBB1_66
.LBB1_63:
	cmpl	$255, %ebx
	jne	.LBB1_64
.LBB1_59:
	movq	$get_raw_row, 8(%r15)
	xorl	%r13d, %r13d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_68
.LBB1_64:
	movq	$get_scaled_rgb_row, 8(%r15)
.LBB1_66:
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
.LBB1_67:
	movb	$1, %r13b
.LBB1_68:
	movl	%r12d, %eax
	movslq	(%r14), %rcx
	xorl	%edx, %edx
	cmpl	$255, %ebx
	seta	%dl
	incq	%rdx
	imulq	%rax, %rdx
	imulq	%rcx, %rdx
	movq	%rdx, 64(%r15)
	movq	%r14, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%r14), %rax
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, 48(%r15)
	testb	%r13b, %r13b
	je	.LBB1_70
# BB#69:
	leaq	8(%r14), %rax
	movq	%r14, %r13
	movq	%rbp, %r14
	movl	8(%rsp), %ebp           # 4-byte Reload
.LBB1_73:                               # %.thread
	movq	(%rax), %rax
	imull	(%r14), %r12d
	movl	$1, %esi
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%r12d, %edx
	callq	*16(%rax)
	movq	%rax, 32(%r15)
	jmp	.LBB1_74
.LBB1_70:
	leaq	56(%r15), %rcx
	movq	%rax, 56(%r15)
	movq	%rcx, 32(%r15)
	movq	%r14, %r13
	movl	8(%rsp), %ebp           # 4-byte Reload
.LBB1_74:
	movl	$1, 40(%r15)
	testl	%ebp, %ebp
	je	.LBB1_82
# BB#75:
	movq	8(%r13), %rax
	movl	%ebx, %ebp
	leaq	1(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*(%rax)
	movq	%rax, %rsi
	movq	%rsi, 72(%r15)
	movl	%ebx, %ecx
	shrl	%ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	movb	%al, (%rsi)
	testl	%ebx, %ebx
	je	.LBB1_82
# BB#76:                                # %._crit_edge.preheader
	testb	$1, %bpl
	jne	.LBB1_78
# BB#77:
	movl	$1, %eax
	cmpl	$1, %ebx
	jne	.LBB1_80
	jmp	.LBB1_82
.LBB1_78:                               # %._crit_edge.prol
	movq	72(%r15), %rsi
	leaq	255(%rcx), %rax
	xorl	%edx, %edx
	divq	%rbp
	movb	%al, 1(%rsi)
	movl	$2, %eax
	cmpl	$1, %ebx
	je	.LBB1_82
.LBB1_80:                               # %._crit_edge.preheader.new
	movq	%rax, %rdx
	shlq	$8, %rdx
	subq	%rax, %rdx
	leaq	-1(%rax), %rsi
	leaq	255(%rdx,%rcx), %rcx
	.p2align	4, 0x90
.LBB1_81:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r15), %rdi
	leaq	-255(%rcx), %rax
	cqto
	idivq	%rbp
	movb	%al, 1(%rdi,%rsi)
	movq	72(%r15), %rdi
	movq	%rcx, %rax
	cqto
	idivq	%rbp
	movb	%al, 2(%rdi,%rsi)
	addq	$2, %rsi
	addq	$510, %rcx              # imm = 0x1FE
	cmpq	%rsi, %rbp
	jne	.LBB1_81
.LBB1_82:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_input_ppm, .Lfunc_end1-start_input_ppm
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_71
	.quad	.LBB1_55
	.quad	.LBB1_65
	.quad	.LBB1_56
	.quad	.LBB1_61

	.text
	.p2align	4, 0x90
	.type	finish_input_ppm,@function
finish_input_ppm:                       # @finish_input_ppm
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	finish_input_ppm, .Lfunc_end2-finish_input_ppm
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_text_gray_row,@function
get_text_gray_row:                      # @get_text_gray_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	40(%r14), %r13d
	testl	%r13d, %r13d
	je	.LBB3_17
# BB#1:                                 # %.critedge.i.preheader.lr.ph
	movq	24(%rsi), %r15
	movq	72(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%rsi), %rax
	movq	(%rax), %rbp
	movabsq	$8589954048, %r12       # imm = 0x200004C00
	.p2align	4, 0x90
.LBB3_2:                                # %.critedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #     Child Loop BB3_11 Depth 2
                                        #       Child Loop BB3_12 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader.i.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB3_5
# BB#4:                                 # %.preheader.i.i
                                        #   in Loop: Header=BB3_3 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB3_3
.LBB3_5:                                # %pbm_getc.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB3_9
# BB#6:                                 # %pbm_getc.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	btq	%rax, %r12
	jb	.LBB3_2
# BB#7:                                 # %pbm_getc.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	testq	%rax, %rax
	jne	.LBB3_9
# BB#8:                                 # %.thread.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB3_10
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB3_11
.LBB3_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%r14), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r14, %rdi
	callq	*(%rax)
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_14:                               # %pbm_getc.exit33.i
                                        #   in Loop: Header=BB3_11 Depth=2
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_11 Depth=2
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB3_11:                               # %.preheader.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_12 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB3_14
	.p2align	4, 0x90
.LBB3_12:                               # %.preheader.i31.i
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB3_14
# BB#13:                                # %.preheader.i31.i
                                        #   in Loop: Header=BB3_12 Depth=3
	cmpl	$-1, %eax
	jne	.LBB3_12
	jmp	.LBB3_14
.LBB3_16:                               # %read_pbm_integer.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%ebx, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	(%rcx,%rax), %al
	movb	%al, (%rbp)
	incq	%rbp
	decl	%r13d
	jne	.LBB3_2
.LBB3_17:                               # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	get_text_gray_row, .Lfunc_end3-get_text_gray_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_text_rgb_row,@function
get_text_rgb_row:                       # @get_text_rgb_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	40(%r14), %r13d
	testl	%r13d, %r13d
	je	.LBB4_47
# BB#1:                                 # %.critedge.i.preheader.lr.ph
	movq	24(%rsi), %r15
	movq	72(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%rsi), %rax
	movq	(%rax), %rbp
	movabsq	$8589954048, %r12       # imm = 0x200004C00
	.p2align	4, 0x90
.LBB4_2:                                # %.critedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_3 Depth 2
                                        #     Child Loop BB4_11 Depth 2
                                        #       Child Loop BB4_12 Depth 3
                                        #     Child Loop BB4_17 Depth 2
                                        #       Child Loop BB4_18 Depth 3
                                        #     Child Loop BB4_26 Depth 2
                                        #       Child Loop BB4_27 Depth 3
                                        #     Child Loop BB4_32 Depth 2
                                        #       Child Loop BB4_33 Depth 3
                                        #     Child Loop BB4_41 Depth 2
                                        #       Child Loop BB4_42 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB4_5
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.i.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB4_5
# BB#4:                                 # %.preheader.i.i
                                        #   in Loop: Header=BB4_3 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB4_3
.LBB4_5:                                # %pbm_getc.exit.i
                                        #   in Loop: Header=BB4_2 Depth=1
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB4_9
# BB#6:                                 # %pbm_getc.exit.i
                                        #   in Loop: Header=BB4_2 Depth=1
	btq	%rax, %r12
	jb	.LBB4_2
# BB#7:                                 # %pbm_getc.exit.i
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%rax, %rax
	jne	.LBB4_9
# BB#8:                                 # %.thread.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB4_10
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=1
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB4_11
.LBB4_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r14, %rdi
	callq	*(%rax)
	jmp	.LBB4_11
	.p2align	4, 0x90
.LBB4_14:                               # %pbm_getc.exit33.i
                                        #   in Loop: Header=BB4_11 Depth=2
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_11 Depth=2
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB4_11:                               # %.preheader.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_12 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB4_14
	.p2align	4, 0x90
.LBB4_12:                               # %.preheader.i31.i
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB4_14
# BB#13:                                # %.preheader.i31.i
                                        #   in Loop: Header=BB4_12 Depth=3
	cmpl	$-1, %eax
	jne	.LBB4_12
	jmp	.LBB4_14
.LBB4_16:                               # %read_pbm_integer.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%ebx, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	(%rcx,%rax), %al
	movb	%al, (%rbp)
	.p2align	4, 0x90
.LBB4_17:                               # %.critedge.i28
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_18 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB4_20
	.p2align	4, 0x90
.LBB4_18:                               # %.preheader.i.i29
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB4_20
# BB#19:                                # %.preheader.i.i29
                                        #   in Loop: Header=BB4_18 Depth=3
	cmpl	$-1, %ebx
	jne	.LBB4_18
.LBB4_20:                               # %pbm_getc.exit.i31
                                        #   in Loop: Header=BB4_17 Depth=2
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB4_24
# BB#21:                                # %pbm_getc.exit.i31
                                        #   in Loop: Header=BB4_17 Depth=2
	btq	%rax, %r12
	jb	.LBB4_17
# BB#22:                                # %pbm_getc.exit.i31
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%rax, %rax
	jne	.LBB4_24
# BB#23:                                # %.thread.i32
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB4_25
.LBB4_24:                               #   in Loop: Header=BB4_2 Depth=1
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB4_26
.LBB4_25:                               # %._crit_edge.i36
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r14, %rdi
	callq	*(%rax)
	jmp	.LBB4_26
	.p2align	4, 0x90
.LBB4_29:                               # %pbm_getc.exit33.i45
                                        #   in Loop: Header=BB4_26 Depth=2
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_26 Depth=2
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB4_26:                               # %.preheader.i41
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_27 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB4_29
	.p2align	4, 0x90
.LBB4_27:                               # %.preheader.i31.i42
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB4_29
# BB#28:                                # %.preheader.i31.i42
                                        #   in Loop: Header=BB4_27 Depth=3
	cmpl	$-1, %eax
	jne	.LBB4_27
	jmp	.LBB4_29
.LBB4_31:                               # %read_pbm_integer.exit46
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%ebx, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	(%rcx,%rax), %al
	movb	%al, 1(%rbp)
	.p2align	4, 0x90
.LBB4_32:                               # %.critedge.i47
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_33 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB4_35
	.p2align	4, 0x90
.LBB4_33:                               # %.preheader.i.i48
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB4_35
# BB#34:                                # %.preheader.i.i48
                                        #   in Loop: Header=BB4_33 Depth=3
	cmpl	$-1, %ebx
	jne	.LBB4_33
.LBB4_35:                               # %pbm_getc.exit.i50
                                        #   in Loop: Header=BB4_32 Depth=2
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB4_39
# BB#36:                                # %pbm_getc.exit.i50
                                        #   in Loop: Header=BB4_32 Depth=2
	btq	%rax, %r12
	jb	.LBB4_32
# BB#37:                                # %pbm_getc.exit.i50
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%rax, %rax
	jne	.LBB4_39
# BB#38:                                # %.thread.i51
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB4_40
.LBB4_39:                               #   in Loop: Header=BB4_2 Depth=1
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB4_41
.LBB4_40:                               # %._crit_edge.i55
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
	movl	$1026, 40(%rax)         # imm = 0x402
	movq	%r14, %rdi
	callq	*(%rax)
	jmp	.LBB4_41
	.p2align	4, 0x90
.LBB4_44:                               # %pbm_getc.exit33.i64
                                        #   in Loop: Header=BB4_41 Depth=2
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB4_46
# BB#45:                                #   in Loop: Header=BB4_41 Depth=2
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB4_41:                               # %.preheader.i60
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_42 Depth 3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB4_44
	.p2align	4, 0x90
.LBB4_42:                               # %.preheader.i31.i61
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB4_44
# BB#43:                                # %.preheader.i31.i61
                                        #   in Loop: Header=BB4_42 Depth=3
	cmpl	$-1, %eax
	jne	.LBB4_42
	jmp	.LBB4_44
.LBB4_46:                               # %read_pbm_integer.exit65
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%ebx, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	(%rcx,%rax), %al
	movb	%al, 2(%rbp)
	addq	$3, %rbp
	decl	%r13d
	jne	.LBB4_2
.LBB4_47:                               # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_text_rgb_row, .Lfunc_end4-get_text_rgb_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_word_gray_row,@function
get_word_gray_row:                      # @get_word_gray_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r12, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	72(%r15), %r12
	movq	24(%r15), %rcx
	movq	48(%r15), %rdi
	movq	64(%r15), %rdx
	movl	$1, %esi
	callq	fread
	cmpq	64(%r15), %rax
	je	.LBB5_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB5_2:
	movl	40(%r14), %esi
	testl	%esi, %esi
	je	.LBB5_8
# BB#3:                                 # %.lr.ph.preheader
	movq	32(%r15), %rcx
	movq	48(%r15), %rax
	movq	(%rcx), %rcx
	testb	$1, %sil
	jne	.LBB5_5
# BB#4:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB5_7
	jmp	.LBB5_8
.LBB5_5:                                # %.lr.ph.prol
	movzbl	(%rax), %edx
	movzbl	1(%rax), %edi
	addq	$2, %rax
	shlq	$8, %rdi
	orq	%rdx, %rdi
	movb	(%r12,%rdi), %dl
	movb	%dl, (%rcx)
	incq	%rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %esi
	movzbl	1(%rax), %edi
	shlq	$8, %rdi
	orq	%rsi, %rdi
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, (%rcx)
	movzbl	2(%rax), %esi
	movzbl	3(%rax), %edi
	shlq	$8, %rdi
	orq	%rsi, %rdi
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, 1(%rcx)
	addq	$4, %rax
	addq	$2, %rcx
	addl	$-2, %edx
	jne	.LBB5_7
.LBB5_8:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	get_word_gray_row, .Lfunc_end5-get_word_gray_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_raw_row,@function
get_raw_row:                            # @get_raw_row
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%rbx), %rcx
	movq	48(%rbx), %rdi
	movq	64(%rbx), %rdx
	movl	$1, %esi
	callq	fread
	cmpq	64(%rbx), %rax
	je	.LBB6_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB6_2:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	get_raw_row, .Lfunc_end6-get_raw_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_scaled_gray_row,@function
get_scaled_gray_row:                    # @get_scaled_gray_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 48
.Lcfi59:
	.cfi_offset %rbx, -40
.Lcfi60:
	.cfi_offset %r12, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	72(%r15), %r12
	movq	24(%r15), %rcx
	movq	48(%r15), %rdi
	movq	64(%r15), %rdx
	movl	$1, %esi
	callq	fread
	cmpq	64(%r15), %rax
	je	.LBB7_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB7_2:
	movl	40(%r14), %eax
	testl	%eax, %eax
	je	.LBB7_8
# BB#3:                                 # %.lr.ph.preheader
	movq	32(%r15), %rdx
	movq	48(%r15), %rcx
	movq	(%rdx), %rdx
	leal	-1(%rax), %r8d
	movl	%eax, %edi
	andl	$3, %edi
	je	.LBB7_6
# BB#4:                                 # %.lr.ph.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	incq	%rcx
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, (%rdx)
	incq	%rdx
	decl	%eax
	incl	%edi
	jne	.LBB7_5
.LBB7_6:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, (%rdx)
	movzbl	1(%rcx), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 1(%rdx)
	movzbl	2(%rcx), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 2(%rdx)
	movzbl	3(%rcx), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 3(%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	addl	$-4, %eax
	jne	.LBB7_7
.LBB7_8:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	get_scaled_gray_row, .Lfunc_end7-get_scaled_gray_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_word_rgb_row,@function
get_word_rgb_row:                       # @get_word_rgb_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 48
.Lcfi68:
	.cfi_offset %rbx, -40
.Lcfi69:
	.cfi_offset %r12, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	72(%r15), %r12
	movq	24(%r15), %rcx
	movq	48(%r15), %rdi
	movq	64(%r15), %rdx
	movl	$1, %esi
	callq	fread
	cmpq	64(%r15), %rax
	je	.LBB8_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB8_2:
	movl	40(%r14), %eax
	testl	%eax, %eax
	je	.LBB8_5
# BB#3:                                 # %.lr.ph.preheader
	movq	32(%r15), %rdx
	movq	48(%r15), %rcx
	movq	(%rdx), %rdx
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	movzbl	1(%rcx), %edi
	shlq	$8, %rdi
	orq	%rsi, %rdi
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, (%rdx)
	movzbl	2(%rcx), %esi
	movzbl	3(%rcx), %edi
	shlq	$8, %rdi
	orq	%rsi, %rdi
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, 1(%rdx)
	movzbl	4(%rcx), %esi
	movzbl	5(%rcx), %edi
	shlq	$8, %rdi
	orq	%rsi, %rdi
	movzbl	(%r12,%rdi), %ebx
	movb	%bl, 2(%rdx)
	addq	$6, %rcx
	addq	$3, %rdx
	decl	%eax
	jne	.LBB8_4
.LBB8_5:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	get_word_rgb_row, .Lfunc_end8-get_word_rgb_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_scaled_rgb_row,@function
get_scaled_rgb_row:                     # @get_scaled_rgb_row
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 48
.Lcfi77:
	.cfi_offset %rbx, -40
.Lcfi78:
	.cfi_offset %r12, -32
.Lcfi79:
	.cfi_offset %r14, -24
.Lcfi80:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	72(%r15), %r12
	movq	24(%r15), %rcx
	movq	48(%r15), %rdi
	movq	64(%r15), %rdx
	movl	$1, %esi
	callq	fread
	cmpq	64(%r15), %rax
	je	.LBB9_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB9_2:
	movl	40(%r14), %esi
	testl	%esi, %esi
	je	.LBB9_8
# BB#3:                                 # %.lr.ph.preheader
	movq	32(%r15), %rcx
	movq	48(%r15), %rax
	movq	(%rcx), %rcx
	testb	$1, %sil
	jne	.LBB9_5
# BB#4:
	movl	%esi, %edx
	cmpl	$1, %esi
	jne	.LBB9_7
	jmp	.LBB9_8
.LBB9_5:                                # %.lr.ph.prol
	movzbl	(%rax), %edx
	movb	(%r12,%rdx), %dl
	movb	%dl, (%rcx)
	movzbl	1(%rax), %edx
	movb	(%r12,%rdx), %dl
	movb	%dl, 1(%rcx)
	movzbl	2(%rax), %edx
	addq	$3, %rax
	movb	(%r12,%rdx), %dl
	movb	%dl, 2(%rcx)
	addq	$3, %rcx
	leal	-1(%rsi), %edx
	cmpl	$1, %esi
	je	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rax), %esi
	movzbl	(%r12,%rsi), %ebx
	movb	%bl, 5(%rcx)
	addq	$6, %rax
	addq	$6, %rcx
	addl	$-2, %edx
	jne	.LBB9_7
.LBB9_8:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	get_scaled_rgb_row, .Lfunc_end9-get_scaled_rgb_row
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
