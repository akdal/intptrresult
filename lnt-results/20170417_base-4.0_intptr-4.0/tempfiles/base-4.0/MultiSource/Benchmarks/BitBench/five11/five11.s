	.text
	.file	"five11.bc"
	.globl	cons1
	.p2align	4, 0x90
	.type	cons1,@function
cons1:                                  # @cons1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$16, %edi
	callq	malloc
	movl	%ebp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cons1, .Lfunc_end0-cons1
	.cfi_endproc

	.globl	cons2
	.p2align	4, 0x90
	.type	cons2,@function
cons2:                                  # @cons2
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movq	%r14, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	cons2, .Lfunc_end1-cons2
	.cfi_endproc

	.globl	free_list1
	.p2align	4, 0x90
	.type	free_list1,@function
free_list1:                             # @free_list1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	free_list1, .Lfunc_end2-free_list1
	.cfi_endproc

	.globl	free_list2
	.p2align	4, 0x90
	.type	free_list2,@function
free_list2:                             # @free_list2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
	movq	(%r15), %rdi
	movq	8(%r15), %r14
	testq	%rdi, %rdi
	je	.LBB3_3
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB3_2
.LBB3_3:                                # %free_list1.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	%r15, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %r15
	jne	.LBB3_1
.LBB3_4:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	free_list2, .Lfunc_end3-free_list2
	.cfi_endproc

	.globl	read_x_bits
	.p2align	4, 0x90
	.type	read_x_bits,@function
read_x_bits:                            # @read_x_bits
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%esi, %eax
	sarl	$3, %eax
	andl	$7, %esi
	movl	$8, %ecx
	subl	%esi, %ecx
	movslq	%eax, %r9
	movzbl	(%rdi,%r9), %r8d
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	%r8d, %eax
	cmpl	%edx, %ecx
	jge	.LBB4_8
# BB#1:                                 # %.lr.ph.preheader
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	leal	7(%rsi,%rdx), %r8d
	leal	-9(%rsi,%rdx), %r10d
	movl	%r10d, %r11d
	shrl	$3, %r11d
	incl	%r11d
	andl	$3, %r11d
	je	.LBB4_4
# BB#2:                                 # %.lr.ph.prol.preheader
	negl	%r11d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rdi,%r9), %ebx
	incq	%r9
	shll	$8, %eax
	orl	%ebx, %eax
	addl	$8, %ecx
	incl	%r11d
	jne	.LBB4_3
.LBB4_4:                                # %.lr.ph.prol.loopexit
	andl	$-8, %r8d
	cmpl	$24, %r10d
	jb	.LBB4_7
# BB#5:                                 # %.lr.ph.preheader.new
	leaq	4(%rdi,%r9), %rdi
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi), %ebx
	shll	$8, %eax
	orl	%ebx, %eax
	movzbl	-2(%rdi), %ebx
	shll	$8, %eax
	orl	%ebx, %eax
	movzbl	-1(%rdi), %ebx
	shll	$8, %eax
	orl	%ebx, %eax
	movzbl	(%rdi), %ebx
	shll	$8, %eax
	orl	%ebx, %eax
	addl	$32, %ecx
	addq	$4, %rdi
	cmpl	%edx, %ecx
	jl	.LBB4_6
.LBB4_7:                                # %._crit_edge.loopexit
	subl	%esi, %r8d
	movl	%r8d, %ecx
	popq	%rbx
.LBB4_8:                                # %._crit_edge
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	retq
.Lfunc_end4:
	.size	read_x_bits, .Lfunc_end4-read_x_bits
	.cfi_endproc

	.globl	read_11_bits
	.p2align	4, 0x90
	.type	read_11_bits,@function
read_11_bits:                           # @read_11_bits
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	sarl	$3, %eax
	andl	$7, %esi
	movl	$8, %edx
	subl	%esi, %edx
	movslq	%eax, %rsi
	movzbl	(%rdi,%rsi), %r8d
	movzbl	1(%rdi,%rsi), %eax
	movl	$11, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r8d
	cmpl	$3, %edx
	jb	.LBB5_2
# BB#1:
	addl	$-3, %edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	orl	%r8d, %eax
	andl	$2047, %eax             # imm = 0x7FF
	retq
.LBB5_2:
	movzbl	2(%rdi,%rsi), %esi
	movl	$3, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	%r8d, %eax
	addl	$5, %edx
	movl	%edx, %ecx
	shrl	%cl, %esi
	andl	$2047, %eax             # imm = 0x7FF
	orl	%esi, %eax
	retq
.Lfunc_end5:
	.size	read_11_bits, .Lfunc_end5-read_11_bits
	.cfi_endproc

	.globl	five11
	.p2align	4, 0x90
	.type	five11,@function
five11:                                 # @five11
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 96
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movzbl	(%r14), %ecx
	movzbl	1(%r14), %eax
	shll	$8, %ecx
	xorl	%ebp, %ebp
	orl	%eax, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	je	.LBB6_1
# BB#2:                                 # %.lr.ph55.preheader
	movl	$16, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	movl	%r12d, %edx
	sarl	$3, %edx
	movl	%r12d, %eax
	andl	$7, %eax
	movl	$8, %ecx
	subl	%eax, %ecx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %esi
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	andl	%esi, %edi
	cmpl	$4, %ecx
	ja	.LBB6_5
# BB#4:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB6_3 Depth=1
	leal	12(%rax), %ecx
	movzbl	1(%r14,%rdx), %edx
	shll	$8, %edi
	orl	%edx, %edi
	andl	$24, %ecx
	subl	%eax, %ecx
.LBB6_5:                                # %read_x_bits.exit46
                                        #   in Loop: Header=BB6_3 Depth=1
	addl	$-5, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	addl	$5, %r12d
	testl	%edi, %edi
	jle	.LBB6_6
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	imull	$11, %edi, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movl	%edi, %r15d
	xorl	%ebp, %ebp
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %ecx
	sarl	$3, %ecx
	movl	%r12d, %edx
	andl	$7, %edx
	movl	$8, %eax
	subl	%edx, %eax
	movslq	%ecx, %rsi
	movzbl	(%r14,%rsi), %edx
	movzbl	1(%r14,%rsi), %r13d
	movl	$11, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	cmpl	$3, %eax
	jb	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=2
	addl	$-3, %eax
	movl	%eax, %ecx
	shrl	%cl, %r13d
	orl	%edx, %r13d
	andl	$2047, %r13d            # imm = 0x7FF
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_8 Depth=2
	movzbl	2(%r14,%rsi), %esi
	movl	$3, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	orl	%edx, %r13d
	addl	$5, %eax
	movl	%eax, %ecx
	shrl	%cl, %esi
	andl	$2047, %r13d            # imm = 0x7FF
	orl	%esi, %r13d
.LBB6_11:                               # %read_11_bits.exit
                                        #   in Loop: Header=BB6_8 Depth=2
	addl	$11, %r12d
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r13d, (%rbx)
	movq	%rbp, 8(%rbx)
	decl	%r15d
	movq	%rbx, %rbp
	jne	.LBB6_8
# BB#12:                                # %read_x_bits.exit.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	movl	20(%rsp), %r12d         # 4-byte Reload
	addl	12(%rsp), %r12d         # 4-byte Folded Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	16(%rsp), %edi          # 4-byte Reload
	jmp	.LBB6_13
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_3 Depth=1
	xorl	%ebx, %ebx
.LBB6_13:                               # %read_x_bits.exit
                                        #   in Loop: Header=BB6_3 Depth=1
	imull	$-11, %edi, %eax
	addl	$3, %eax
	andl	$7, %eax
	addl	%eax, %r12d
	movl	$16, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movq	%r15, 8(%rax)
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	movq	%rax, %r15
	jne	.LBB6_3
	jmp	.LBB6_14
.LBB6_1:
	xorl	%eax, %eax
.LBB6_14:                               # %read_x_bits.exit._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	five11, .Lfunc_end6-five11
	.cfi_endproc

	.globl	pad_size
	.p2align	4, 0x90
	.type	pad_size,@function
pad_size:                               # @pad_size
	.cfi_startproc
# BB#0:
	imull	$-11, %edi, %eax
	addl	$3, %eax
	andl	$7, %eax
	retq
.Lfunc_end7:
	.size	pad_size, .Lfunc_end7-pad_size
	.cfi_endproc

	.globl	calc_sum2
	.p2align	4, 0x90
	.type	calc_sum2,@function
calc_sum2:                              # @calc_sum2
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB8_2
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                # %calc_sum1.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	addl	%ecx, %eax
	testq	%rdi, %rdi
	je	.LBB8_6
.LBB8_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	testq	%rdx, %rdx
	movl	$0, %ecx
	je	.LBB8_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph.i
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	(%rdx), %ecx
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB8_4
	jmp	.LBB8_5
.LBB8_6:                                # %._crit_edge
	retq
.Lfunc_end8:
	.size	calc_sum2, .Lfunc_end8-calc_sum2
	.cfi_endproc

	.globl	calc_sum1
	.p2align	4, 0x90
	.type	calc_sum1,@function
calc_sum1:                              # @calc_sum1
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	(%rdi), %eax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_1
.LBB9_3:                                # %._crit_edge
	retq
.Lfunc_end9:
	.size	calc_sum1, .Lfunc_end9-calc_sum1
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 96
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %ebx
	movl	$150000, %edi           # imm = 0x249F0
	callq	malloc
	movq	%rax, %r14
	cmpl	$2, %ebx
	jl	.LBB10_3
# BB#1:
	movq	8(%rbp), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB10_17
# BB#2:
	decl	%ebx
	cmpl	$1, %ebx
	je	.LBB10_5
	jmp	.LBB10_18
.LBB10_3:
	movq	stdin(%rip), %rcx
	cmpl	$1, %ebx
	jne	.LBB10_18
.LBB10_5:
	movl	$1, %esi
	movl	$150000, %edx           # imm = 0x249F0
	movq	%r14, %rdi
	callq	fread
	xorl	%r15d, %r15d
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	.p2align	4, 0x90
.LBB10_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_8 Depth 2
                                        #       Child Loop BB10_10 Depth 3
                                        #     Child Loop BB10_12 Depth 2
                                        #       Child Loop BB10_13 Depth 3
	movq	%r14, %rdi
	callq	five11
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movl	$0, %r12d
	je	.LBB10_15
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_6 Depth=1
	xorl	%r12d, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph.i
                                        #   Parent Loop BB10_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_10 Depth 3
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movl	$0, %ecx
	je	.LBB10_11
# BB#9:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB10_8 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_10:                              # %.lr.ph.i.i
                                        #   Parent Loop BB10_6 Depth=1
                                        #     Parent Loop BB10_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	(%rdx), %ecx
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB10_10
.LBB10_11:                              # %calc_sum1.exit.i
                                        #   in Loop: Header=BB10_8 Depth=2
	addl	%ecx, %r12d
	testq	%rax, %rax
	jne	.LBB10_8
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph.i23
                                        #   Parent Loop BB10_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_13 Depth 3
	movq	(%rbx), %rdi
	movq	8(%rbx), %r13
	testq	%rdi, %rdi
	je	.LBB10_14
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph.i.i24
                                        #   Parent Loop BB10_6 Depth=1
                                        #     Parent Loop BB10_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbp
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.LBB10_13
.LBB10_14:                              # %free_list1.exit.i
                                        #   in Loop: Header=BB10_12 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%r13, %r13
	movq	%r13, %rbx
	jne	.LBB10_12
.LBB10_15:                              # %free_list2.exit
                                        #   in Loop: Header=BB10_6 Depth=1
	incl	%r15d
	cmpl	$10000, %r15d           # imm = 0x2710
	jne	.LBB10_6
# BB#16:
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB10_18:
	movl	$.Lstr, %edi
	callq	puts
	movl	$2, %edi
	callq	exit
.LBB10_17:
	movq	8(%rbp), %rdi
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	main, .Lfunc_end10-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d\n"
	.size	.L.str.2, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Usage: five11 [infile]"
	.size	.Lstr, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
