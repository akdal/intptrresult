	.text
	.file	"IOtest.bc"
	.globl	getac
	.p2align	4, 0x90
	.type	getac,@function
getac:                                  # @getac
	.cfi_startproc
# BB#0:
	movq	array_count(%rip), %rax
	retq
.Lfunc_end0:
	.size	getac, .Lfunc_end0-getac
	.cfi_endproc

	.globl	setac
	.p2align	4, 0x90
	.type	setac,@function
setac:                                  # @setac
	.cfi_startproc
# BB#0:
	movabsq	$7206577043584905943, %rcx # imm = 0x6402E7A3995CF2D7
	movq	%rdi, %rax
	mulq	%rcx
	shrq	$20, %rdx
	imulq	$2684050, %rdx, %rax    # imm = 0x28F492
	subq	%rax, %rdi
	movq	%rdi, array_count(%rip)
	retq
.Lfunc_end1:
	.size	setac, .Lfunc_end1-setac
	.cfi_endproc

	.globl	initarray
	.p2align	4, 0x90
	.type	initarray,@function
initarray:                              # @initarray
	.cfi_startproc
# BB#0:
	xorl	%ecx, %ecx
	movabsq	$-9187201950435737471, %rdi # imm = 0x8080808080808081
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%rdi
	shrl	$7, %edx
	negl	%edx
	movl	%ecx, %eax
	subl	%edx, %eax
	movq	testarray(%rip), %rdx
	movb	%al, (%rdx,%rcx)
	leaq	1(%rcx), %rsi
	movq	%rsi, %rax
	mulq	%rdi
	shrl	$7, %edx
	negl	%edx
	subl	%edx, %esi
	movq	testarray(%rip), %rax
	movb	%sil, 1(%rax,%rcx)
	addq	$2, %rcx
	cmpq	$2684050, %rcx          # imm = 0x28F492
	jne	.LBB2_1
# BB#2:
	retq
.Lfunc_end2:
	.size	initarray, .Lfunc_end2-initarray
	.cfi_endproc

	.globl	array
	.p2align	4, 0x90
	.type	array,@function
array:                                  # @array
	.cfi_startproc
# BB#0:
	movq	testarray(%rip), %rax
	movb	(%rax,%rdi), %al
	retq
.Lfunc_end3:
	.size	array, .Lfunc_end3-array
	.cfi_endproc

	.globl	min
	.p2align	4, 0x90
	.type	min,@function
min:                                    # @min
	.cfi_startproc
# BB#0:
	cmpb	%sil, %dil
	cmovgel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end4:
	.size	min, .Lfunc_end4-min
	.cfi_endproc

	.globl	max
	.p2align	4, 0x90
	.type	max,@function
max:                                    # @max
	.cfi_startproc
# BB#0:
	cmpb	%sil, %dil
	cmovgl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end5:
	.size	max, .Lfunc_end5-max
	.cfi_endproc

	.globl	add
	.p2align	4, 0x90
	.type	add,@function
add:                                    # @add
	.cfi_startproc
# BB#0:
	addb	%dil, %sil
	movl	%esi, %eax
	retq
.Lfunc_end6:
	.size	add, .Lfunc_end6-add
	.cfi_endproc

	.globl	mult
	.p2align	4, 0x90
	.type	mult,@function
mult:                                   # @mult
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	mulb	%dil
	retq
.Lfunc_end7:
	.size	mult, .Lfunc_end7-mult
	.cfi_endproc

	.globl	loop
	.p2align	4, 0x90
	.type	loop,@function
loop:                                   # @loop
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rax
	movq	%r15, %rdi
	callq	*%rax
	movl	$2684050, %ebx          # imm = 0x28F492
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	*%r14
	decq	%rbx
	jne	.LBB8_1
# BB#2:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	loop, .Lfunc_end8-loop
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$2684050, %edi          # imm = 0x28F492
	callq	malloc
	movq	%rax, testarray(%rip)
	callq	testA
	callq	testB
	callq	testC
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	main, .Lfunc_end9-main
	.cfi_endproc

	.type	array_count,@object     # @array_count
	.comm	array_count,8,8
	.type	testarray,@object       # @testarray
	.comm	testarray,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
