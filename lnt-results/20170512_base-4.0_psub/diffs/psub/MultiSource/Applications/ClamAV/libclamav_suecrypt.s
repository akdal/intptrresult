	.text
	.file	"libclamav_suecrypt.bc"
	.globl	sudecrypt
	.p2align	4, 0x90
	.type	sudecrypt,@function
sudecrypt:                              # @sudecrypt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%ecx, %r14d
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB0_39
# BB#1:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%rax, %r15
	callq	lseek
	movl	%ebp, %edi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cltq
	cmpq	%rbx, %rax
	jne	.LBB0_33
# BB#2:
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	128(%rsp), %r13d
	movl	20(%rsp), %eax          # 4-byte Reload
	roll	$16, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	62(%rcx), %ebx
	movl	%ebx, %ebp
	xorl	%eax, %ebp
	je	.LBB0_5
# BB#3:
	cmpl	$956, %ebp              # imm = 0x3BC
	je	.LBB0_5
# BB#4:
	cmpl	$520, %ebp              # imm = 0x208
	jne	.LBB0_6
.LBB0_5:
	movl	70(%rcx), %ebx
	xorl	%ebx, %eax
	movl	%eax, %ebp
.LBB0_6:
	cmpl	%r13d, %ebp
	je	.LBB0_11
# BB#7:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	cli_dbgmsg
	movzbl	%bpl, %eax
	movl	%ebx, %ecx
	andl	$-256, %ecx
	orl	%eax, %ecx
	cmpl	%r13d, %ecx
	je	.LBB0_11
# BB#8:
	movzwl	%bp, %eax
	movl	%ebx, %ecx
	andl	$-65536, %ecx           # imm = 0xFFFF0000
	orl	%eax, %ecx
	cmpl	%r13d, %ecx
	je	.LBB0_11
# BB#9:
	movl	%ebp, %eax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	andl	$-16777216, %ebx        # imm = 0xFF000000
	orl	%eax, %ebx
	cmpl	%r13d, %ebx
	je	.LBB0_11
# BB#10:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %r13d
.LBB0_11:                               # %.thread
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	-88(%rsi), %rax
	movzwl	%r14w, %ecx
	movq	%rcx, %r12
	testw	%cx, %cx
	je	.LBB0_34
# BB#12:                                # %.thread.split.us.preheader
	movd	%r13d, %xmm0
	pshufd	$0, %xmm0, %xmm2        # xmm2 = xmm0[0,0,0,0]
	addq	$12, 32(%rsp)           # 8-byte Folded Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	16(%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	movdqa	%xmm2, 48(%rsp)         # 16-byte Spill
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.us
                                        #   in Loop: Header=BB0_31 Depth=1
	movl	-84(%rsi,%rcx), %ebp
	xorl	%eax, %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	cli_dbgmsg
	leal	-1(%rbp), %ecx
	leal	(%rbp,%r14), %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ebx
	cmpl	%ebx, %ecx
	jae	.LBB0_18
# BB#15:                                #   in Loop: Header=BB0_14 Depth=2
	movl	-12(%rsi), %edx
	cmpl	%edx, %r14d
	jb	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_14 Depth=2
	cmpl	%edx, %edi
	jbe	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_14 Depth=2
	addl	%edx, %ebx
	cmpl	%ebx, %edi
	jbe	.LBB0_22
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_14 Depth=2
	incq	%rax
	addq	$36, %rsi
	movq	%r12, %rdx
	cmpq	%rdx, %rax
	jl	.LBB0_14
.LBB0_19:                               # %.loopexit.us.loopexit160
                                        #   in Loop: Header=BB0_31 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_20:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_31 Depth=1
	movq	%r12, %rcx
	cmpl	%ecx, %eax
	je	.LBB0_36
# BB#21:                                #   in Loop: Header=BB0_31 Depth=1
	incq	%r15
	leaq	-88(%rsi,%r15,8), %rax
	leaq	35(,%r15,8), %rcx
	cmpq	$190, %rcx
	leaq	(,%r15,8), %rcx
	jb	.LBB0_31
	jmp	.LBB0_37
.LBB0_22:                               #   in Loop: Header=BB0_31 Depth=1
	cmpl	$3, %ebp
	jbe	.LBB0_19
# BB#23:                                # %.lr.ph152.us.preheader
                                        #   in Loop: Header=BB0_31 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movl	%r14d, %r9d
	subq	%r9, %rcx
	movl	-4(%rsi), %esi
	addq	%rsi, %rcx
	leal	-4(%rbp), %r8d
	shrl	$2, %r8d
	incl	%r8d
	cmpl	$7, %r8d
	jbe	.LBB0_29
# BB#24:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_31 Depth=1
	movl	%r8d, %edi
	andl	$7, %r8d
	movq	%rdi, %rbx
	subq	%r8, %rbx
	subq	%r8, %rdi
	je	.LBB0_29
# BB#25:                                # %vector.ph
                                        #   in Loop: Header=BB0_31 Depth=1
	shll	$2, %ebx
	subl	%ebx, %ebp
	leaq	(%rcx,%rdi,4), %rcx
	subq	%r9, %rsi
	addq	40(%rsp), %rsi          # 8-byte Folded Reload
	addq	%rdx, %rsi
	movdqa	48(%rsp), %xmm2         # 16-byte Reload
	.p2align	4, 0x90
.LBB0_26:                               # %vector.body
                                        #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	pxor	%xmm2, %xmm0
	pxor	%xmm2, %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB0_26
# BB#27:                                # %middle.block
                                        #   in Loop: Header=BB0_31 Depth=1
	testl	%r8d, %r8d
	movq	8(%rsp), %rsi           # 8-byte Reload
	jne	.LBB0_30
	jmp	.LBB0_20
.LBB0_29:                               #   in Loop: Header=BB0_31 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph152.us
                                        #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%r13d, (%rcx)
	addq	$4, %rcx
	addl	$-4, %ebp
	cmpl	$3, %ebp
	ja	.LBB0_30
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_31:                               # %.thread.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_26 Depth 2
                                        #     Child Loop BB0_30 Depth 2
	movl	(%rax), %r14d
	movl	20(%rsp), %eax          # 4-byte Reload
	xorl	%eax, %r14d
	jne	.LBB0_13
	jmp	.LBB0_41
.LBB0_33:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	jmp	.LBB0_38
.LBB0_34:                               # %.thread.split
	movl	(%rax), %edx
	movl	20(%rsp), %eax          # 4-byte Reload
	xorl	%eax, %edx
	je	.LBB0_41
# BB#35:                                # %.us-lcssa157.us.loopexit
	xorl	-84(%rsi), %eax
	movl	$.L.str.5, %edi
	xorl	%esi, %esi
	movl	%eax, %ecx
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_36:                               # %.us-lcssa157.us
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_37:                               # %.us-lcssa156.us
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB0_38:
	callq	free
.LBB0_39:
	xorl	%eax, %eax
.LBB0_40:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_41:                               # %.us-lcssa.us
	movl	136(%rsp), %ebx
	xorl	-116(%rsi), %eax
	movl	$.L.str.7, %edi
	movl	%eax, %ebp
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	%ebx, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	addq	%rax, %rcx
	movq	%r12, %rbx
	movb	%bl, 6(%rdx,%rax)
	movb	%bh, 7(%rdx,%rax)  # NOREX
	movl	%ebp, 40(%rdx,%rax)
	movzwl	20(%rdx,%rax), %eax
	addq	%rcx, %rax
	leaq	(%rbx,%rbx,4), %rcx
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rax,%rcx,8)
	movdqu	%xmm0, 24(%rax,%rcx,8)
	movq	$0, 56(%rax,%rcx,8)
	movq	%rdx, %rax
	jmp	.LBB0_40
.Lfunc_end0:
	.size	sudecrypt, .Lfunc_end0-sudecrypt
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in suecrypt\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SUE: Can't read %d bytes\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SUE: Key seems not (entirely) encrypted\n\tpossible key: 0%08x\n\tcrypted key:  0%08x\n\tplain key:    0%08x\n"
	.size	.L.str.2, 104

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SUE: let's roll the dice...\n"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SUE: Decrypting with 0%08x\n"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SUE: Hunk #%d RVA:%x size:%d\n"
	.size	.L.str.5, 30

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SUE: Hunk out of file or cross sections\n"
	.size	.L.str.6, 41

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SUE: found OEP: @%x\n"
	.size	.L.str.7, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
