	.text
	.file	"irred.bc"
	.globl	irredundant
	.p2align	4, 0x90
	.type	irredundant,@function
irredundant:                            # @irredundant
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	mark_irredundant
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	sf_inactive             # TAILCALL
.Lfunc_end0:
	.size	irredundant, .Lfunc_end0-irredundant
	.cfi_endproc

	.globl	mark_irredundant
	.p2align	4, 0x90
	.type	mark_irredundant,@function
mark_irredundant:                       # @mark_irredundant
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -48
.Lcfi9:
	.cfi_offset %r12, -40
.Lcfi10:
	.cfi_offset %r13, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	leaq	16(%rsp), %r8
	callq	irred_split_cover
	movq	24(%rsp), %r13
	movq	16(%rsp), %r15
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	irred_derive_table
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_minimum_cover
	movq	%rax, %r12
	movq	24(%rbx), %rax
	movl	(%rbx), %ecx
	movl	12(%rbx), %edx
	imull	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph55.preheader
	movslq	%edx, %rcx
	leaq	(%rax,%rcx,4), %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	andb	$-37, 1(%rsi)
	movslq	(%rbx), %rcx
	leaq	(%rsi,%rcx,4), %rsi
	cmpq	%rdx, %rsi
	jb	.LBB1_2
.LBB1_3:                                # %._crit_edge56
	movslq	(%r13), %rsi
	movslq	12(%r13), %rdx
	imulq	%rsi, %rdx
	testl	%edx, %edx
	jle	.LBB1_7
# BB#4:                                 # %.lr.ph51.preheader
	movq	24(%r13), %rsi
	movzwl	2(%rsi), %edi
	imull	%ecx, %edi
	orl	$9216, (%rax,%rdi,4)    # imm = 0x2400
	movslq	(%r13), %rdi
	cmpl	%edx, %edi
	jge	.LBB1_7
# BB#5:                                 # %._crit_edge57.lr.ph
	leaq	(%rsi,%rdx,4), %rcx
	leaq	(%rsi,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB1_6:                                # %._crit_edge57
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rdx), %esi
	imull	(%rbx), %esi
	orl	$9216, (%rax,%rsi,4)    # imm = 0x2400
	movslq	(%r13), %rsi
	leaq	(%rdx,%rsi,4), %rdx
	cmpq	%rcx, %rdx
	jb	.LBB1_6
.LBB1_7:                                # %._crit_edge52
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	4(%rcx), %rdx
	movslq	(%rbx), %rsi
	imulq	%rdx, %rsi
	orl	$8192, (%rax,%rsi,4)    # imm = 0x2000
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_8
.LBB1_10:                               # %._crit_edge
	testb	$32, debug(%rip)
	je	.LBB1_12
# BB#11:
	movl	12(%rbx), %esi
	movl	12(%r13), %edx
	movq	8(%rsp), %rax
	movl	12(%rax), %r8d
	movl	12(%r15), %r9d
	leal	(%r9,%r8), %ecx
	movl	4(%r12), %ebx
	leal	(%rbx,%rdx), %r10d
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str, %edi
	movl	$0, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
.LBB1_12:
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_free
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_row_free
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	mark_irredundant, .Lfunc_end1-mark_irredundant
	.cfi_endproc

	.globl	irred_split_cover
	.p2align	4, 0x90
	.type	irred_split_cover,@function
irred_split_cover:                      # @irred_split_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 112
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movslq	(%r13), %rax
	movslq	12(%r13), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph98.preheader
	movq	24(%r13), %rax
	leaq	(%rax,%rcx,4), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph98
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rax), %esi
	orl	%edx, %esi
	movl	%esi, (%rax)
	movslq	(%r13), %rsi
	leaq	(%rax,%rsi,4), %rax
	addl	$65536, %edx            # imm = 0x10000
	cmpq	%rcx, %rax
	jb	.LBB2_2
.LBB2_3:                                # %._crit_edge99
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%r12)
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	callq	cube2list
	movq	%rax, %rbx
	movslq	(%r13), %rcx
	movslq	12(%r13), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_11
# BB#4:                                 # %.lr.ph93.preheader
	movq	24(%r13), %r14
	leaq	(%r14,%rax,4), %rbp
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph93
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cofactor
	movq	%rax, %rdi
	callq	tautology
	testl	%eax, %eax
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	sf_addset
	movq	%rax, %r15
	testb	$64, debug+1(%rip)
	jne	.LBB2_9
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=1
	movq	(%r12), %rdi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	sf_addset
	movq	%rax, (%r12)
	testb	$64, debug+1(%rip)
	je	.LBB2_10
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	12(%rax), %r15d
	movq	(%r12), %rax
	movq	%r13, %rbp
	movl	12(%rax), %r13d
	movl	12(%rbp), %r12d
	leal	(%r13,%r15), %eax
	subl	%eax, %r12d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rbx, %r9
	movq	%rax, %rbx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	%r13d, %edx
	movq	%rbp, %r13
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	%r12d, %ecx
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r9, %rbx
	callq	printf
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=1
	movslq	(%r13), %rax
	leaq	(%r14,%rax,4), %r14
	cmpq	%rbp, %r14
	jb	.LBB2_5
.LBB2_11:                               # %._crit_edge94
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:
	callq	free
.LBB2_13:
	movq	%rbx, %rdi
	callq	free
	movq	(%r12), %rdi
	xorl	%eax, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	cube2list
	movq	%rax, %r14
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB2_18
# BB#14:                                # %.lr.ph.preheader
	movq	24(%r15), %rbp
	leaq	(%rbp,%rax,4), %r13
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	cofactor
	movq	%rax, %rdi
	callq	tautology
	testl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmoveq	24(%rsp), %rbx          # 8-byte Folded Reload
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	sf_addset
	movq	%rax, (%rbx)
	testb	$64, debug+1(%rip)
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_15 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%r15, %r14
	movl	12(%rax), %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	12(%rax), %r12d
	movl	12(%r14), %ebx
	leal	(%r12,%r15), %eax
	subl	%eax, %ebx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %r8
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movq	%r14, %r15
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%r12d, %edx
	movl	%ebx, %ecx
	callq	printf
.LBB2_17:                               #   in Loop: Header=BB2_15 Depth=1
	movslq	(%r15), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r13, %rbp
	jb	.LBB2_15
.LBB2_18:                               # %._crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_20
# BB#19:
	callq	free
.LBB2_20:
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%r15, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_free                 # TAILCALL
.Lfunc_end2:
	.size	irred_split_cover, .Lfunc_end2-irred_split_cover
	.cfi_endproc

	.globl	irred_derive_table
	.p2align	4, 0x90
	.type	irred_derive_table,@function
irred_derive_table:                     # @irred_derive_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 96
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movslq	(%rdi), %rax
	movslq	12(%rdi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph88.preheader
	movq	24(%rdi), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	andb	$-17, 1(%rax)
	movslq	(%rdi), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB3_2
.LBB3_3:                                # %._crit_edge89
	movslq	(%rsi), %rax
	movslq	12(%rsi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB3_6
# BB#4:                                 # %.lr.ph84.preheader
	movq	24(%rsi), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	andb	$-17, 1(%rax)
	movslq	(%rsi), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB3_5
.LBB3_6:                                # %._crit_edge85
	movslq	(%r14), %rax
	movslq	12(%r14), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB3_9
# BB#7:                                 # %.lr.ph80.preheader
	movq	24(%r14), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	orb	$16, 1(%rax)
	movslq	(%r14), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB3_8
.LBB3_9:                                # %._crit_edge81
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	cube3list
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	callq	sm_alloc
	movq	%rax, %r12
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB3_17
# BB#10:                                # %.lr.ph
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	movzwl	2(%rbx), %eax
	movl	%eax, Rp_current(%rip)
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	cofactor
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	ftautology
	andb	$-17, 1(%rbx)
	testb	$64, debug+1(%rip)
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_11 Depth=1
	movq	%r14, %r13
	movl	12(%r13), %r14d
	leal	(%r15,%r14), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	48(%r12), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	72(%r12), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, (%rsp)
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %r8d          # 4-byte Reload
	movl	%ebp, %esi
	movl	%r14d, %edx
	movq	%r13, %r14
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	printf
.LBB3_13:                               #   in Loop: Header=BB3_11 Depth=1
	movl	48(%r12), %eax
	movl	28(%rsp), %r13d         # 4-byte Reload
	subl	%r13d, %eax
	cmpl	$1001, %eax             # imm = 0x3E9
	jl	.LBB3_16
# BB#14:                                #   in Loop: Header=BB3_11 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_row_dominance
	movl	48(%r12), %r13d
	testb	$64, debug+1(%rip)
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_11 Depth=1
	movl	72(%r12), %edx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
.LBB3_16:                               #   in Loop: Header=BB3_11 Depth=1
	incl	%ebp
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	decl	%r15d
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB3_11
.LBB3_17:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_19
# BB#18:
	callq	free
.LBB3_19:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	%r12, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	irred_derive_table, .Lfunc_end3-irred_derive_table
	.cfi_endproc

	.globl	cube_is_covered
	.p2align	4, 0x90
	.type	cube_is_covered,@function
cube_is_covered:                        # @cube_is_covered
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	cofactor
	movq	%rax, %rdi
	popq	%rax
	jmp	tautology               # TAILCALL
.Lfunc_end4:
	.size	cube_is_covered, .Lfunc_end4-cube_is_covered
	.cfi_endproc

	.globl	tautology
	.p2align	4, 0x90
	.type	tautology,@function
tautology:                              # @tautology
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -48
.Lcfi51:
	.cfi_offset %r12, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testb	$2, debug+1(%rip)
	je	.LBB5_2
# BB#1:
	movl	tautology.taut_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, tautology.taut_level(%rip)
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB5_2:
	movq	%rbx, %rdi
	callq	taut_special_cases
	movl	%eax, %ebp
	cmpl	$2, %ebp
	jne	.LBB5_17
# BB#3:
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB5_5
# BB#4:
	movl	$8, %edi
	jmp	.LBB5_6
.LBB5_5:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB5_6:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r14
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB5_8
# BB#7:
	movl	$8, %edi
	jmp	.LBB5_9
.LBB5_8:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB5_9:
	callq	malloc
	movq	%rax, %rcx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	$512, %ecx              # imm = 0x200
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	binate_split_select
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	tautology
	testl	%eax, %eax
	je	.LBB5_11
# BB#10:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	tautology
	testl	%eax, %eax
	setne	%r12b
.LBB5_11:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_13
# BB#12:
	callq	free
.LBB5_13:
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB5_15
# BB#14:
	movq	%r14, %rdi
	callq	free
.LBB5_15:
	movzbl	%r12b, %ebp
	testq	%r15, %r15
	je	.LBB5_17
# BB#16:
	movq	%r15, %rdi
	callq	free
.LBB5_17:
	testb	$2, debug+1(%rip)
	je	.LBB5_19
# BB#18:
	movl	tautology.taut_level(%rip), %esi
	decl	%esi
	movl	%esi, tautology.taut_level(%rip)
	cmpl	$1, %ebp
	movl	$.L.str.8, %eax
	movl	$.L.str.9, %ecx
	cmoveq	%rax, %rcx
	testl	%ebp, %ebp
	movl	$.L.str.7, %edx
	cmovneq	%rcx, %rdx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
.LBB5_19:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	tautology, .Lfunc_end5-tautology
	.cfi_endproc

	.globl	taut_special_cases
	.p2align	4, 0x90
	.type	taut_special_cases,@function
taut_special_cases:                     # @taut_special_cases
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 112
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	cube+80(%rip), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	leaq	16(%r14), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	addq	$8, %rbx
	movq	(%r14), %rsi
	xorl	%eax, %eax
	callq	full_row
	testl	%eax, %eax
	je	.LBB6_1
	jmp	.LBB6_66
.LBB6_3:                                # %.preheader105
	leaq	24(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_20 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rbp
	jmp	.LBB6_6
.LBB6_9:                                # %vector.body198.preheader
                                        #   in Loop: Header=BB6_20 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB6_13
# BB#10:                                # %vector.body198.prol.preheader
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rbx
	leaq	-12(%rax,%rcx,4), %rbp
	negq	%rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_11:                               # %vector.body198.prol
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	%xmm0, (%rbx,%rdi,4)
	movups	%xmm1, -16(%rbx,%rdi,4)
	addq	$-8, %rdi
	incq	%rdx
	jne	.LBB6_11
# BB#12:                                # %vector.body198.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB6_20 Depth=1
	negq	%rdi
	jmp	.LBB6_14
.LBB6_13:                               #   in Loop: Header=BB6_20 Depth=1
	xorl	%edi, %edi
.LBB6_14:                               # %vector.body198.prol.loopexit
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	movl	$2, %r12d
	cmpq	$24, %r9
	jb	.LBB6_17
# BB#15:                                # %vector.body198.preheader.new
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	%rsi, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rcx,4), %rbp
	leaq	-12(%rax,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB6_16:                               # %vector.body198
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rdi,4), %xmm0
	movups	-16(%rbx,%rdi,4), %xmm1
	movups	%xmm0, (%rbp,%rdi,4)
	movups	%xmm1, -16(%rbp,%rdi,4)
	movups	-32(%rbx,%rdi,4), %xmm0
	movups	-48(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -32(%rbp,%rdi,4)
	movups	%xmm1, -48(%rbp,%rdi,4)
	movups	-64(%rbx,%rdi,4), %xmm0
	movups	-80(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -64(%rbp,%rdi,4)
	movups	%xmm1, -80(%rbp,%rdi,4)
	movups	-96(%rbx,%rdi,4), %xmm0
	movups	-112(%rbx,%rdi,4), %xmm1
	movups	%xmm0, -96(%rbp,%rdi,4)
	movups	%xmm1, -112(%rbp,%rdi,4)
	addq	$-32, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB6_16
.LBB6_17:                               # %middle.block199
                                        #   in Loop: Header=BB6_20 Depth=1
	cmpq	%r8, %rsi
	je	.LBB6_29
# BB#18:                                #   in Loop: Header=BB6_20 Depth=1
	subq	%r8, %rcx
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_20 Depth=1
	movl	cdata+36(%rip), %esi
	subq	%r14, %r12
	sarq	$3, %r12
	addq	$-3, %r12
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	printf
	jmp	.LBB6_20
.LBB6_5:                                #   in Loop: Header=BB6_6 Depth=2
	movq	%rbx, (%r12)
	addq	$8, %r12
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=2
	addq	$8, %rbp
	movq	(%r14), %rdx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	set_or
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB6_6
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_20 Depth=1
	movq	$0, (%r12)
	addq	$8, %r12
	movq	%r12, 8(%r14)
	testb	$2, debug+1(%rip)
	jne	.LBB6_19
.LBB6_20:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_11 Depth 2
                                        #     Child Loop BB6_16 Depth 2
                                        #     Child Loop BB6_28 Depth 2
                                        #     Child Loop BB6_30 Depth 2
                                        #       Child Loop BB6_38 Depth 3
                                        #       Child Loop BB6_42 Depth 3
                                        #     Child Loop BB6_50 Depth 2
                                        #       Child Loop BB6_59 Depth 3
                                        #       Child Loop BB6_63 Depth 3
                                        #     Child Loop BB6_6 Depth 2
	movq	(%r14), %rax
	movl	(%rax), %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	leaq	1(%rcx), %rsi
	cmpq	$8, %rsi
	jb	.LBB6_25
# BB#21:                                # %min.iters.checked202
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	%rsi, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB6_25
# BB#22:                                # %vector.memcheck217
                                        #   in Loop: Header=BB6_20 Depth=1
	leaq	4(%rax,%rcx,4), %rdx
	cmpq	%rdx, %r13
	movq	16(%rsp), %r11          # 8-byte Reload
	jae	.LBB6_9
# BB#23:                                # %vector.memcheck217
                                        #   in Loop: Header=BB6_20 Depth=1
	leaq	(%r11,%rcx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB6_9
# BB#24:                                #   in Loop: Header=BB6_20 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	jmp	.LBB6_26
	.p2align	4, 0x90
.LBB6_25:                               #   in Loop: Header=BB6_20 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB6_26:                               # %scalar.ph200.preheader
                                        #   in Loop: Header=BB6_20 Depth=1
	movl	$2, %r12d
.LBB6_27:                               # %scalar.ph200.preheader
                                        #   in Loop: Header=BB6_20 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB6_28:                               # %scalar.ph200
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rcx,4), %edx
	movl	%edx, -4(%r13,%rcx,4)
	decq	%rcx
	jg	.LBB6_28
.LBB6_29:                               # %.preheader104
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB6_44
	.p2align	4, 0x90
.LBB6_30:                               # %.lr.ph
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_38 Depth 3
                                        #       Child Loop BB6_42 Depth 3
	movl	(%r13), %esi
	movl	%esi, %edx
	andl	$1023, %edx             # imm = 0x3FF
	testw	$1023, %si              # imm = 0x3FF
	movl	$1, %r9d
	cmovneq	%rdx, %r9
	cmpq	$8, %r9
	jb	.LBB6_41
# BB#31:                                # %min.iters.checked154
                                        #   in Loop: Header=BB6_30 Depth=2
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB6_41
# BB#32:                                # %vector.memcheck176
                                        #   in Loop: Header=BB6_30 Depth=2
	leaq	1(%rdx), %rsi
	testl	%edx, %edx
	cmovneq	%r12, %rsi
	leaq	(%r10,%rsi,4), %rdi
	leaq	4(%rax,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB6_34
# BB#33:                                # %vector.memcheck176
                                        #   in Loop: Header=BB6_30 Depth=2
	leaq	(%r11,%rdx,4), %rdi
	leaq	-4(%rax,%rsi,4), %rsi
	cmpq	%rdi, %rsi
	jb	.LBB6_41
.LBB6_34:                               # %vector.body149.preheader
                                        #   in Loop: Header=BB6_30 Depth=2
	leaq	-8(%r8), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB6_36
# BB#35:                                # %vector.body149.prol
                                        #   in Loop: Header=BB6_30 Depth=2
	movups	-12(%r13,%rdx,4), %xmm0
	movups	-28(%r13,%rdx,4), %xmm1
	movups	-12(%rax,%rdx,4), %xmm2
	movups	-28(%rax,%rdx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rdx,4)
	movups	%xmm3, -28(%r13,%rdx,4)
	movl	$8, %ebp
	testq	%rsi, %rsi
	jne	.LBB6_37
	jmp	.LBB6_39
.LBB6_36:                               #   in Loop: Header=BB6_30 Depth=2
	xorl	%ebp, %ebp
	testq	%rsi, %rsi
	je	.LBB6_39
.LBB6_37:                               # %vector.body149.preheader.new
                                        #   in Loop: Header=BB6_30 Depth=2
	movq	%r9, %rsi
	andq	$-8, %rsi
	negq	%rsi
	negq	%rbp
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rdx,4), %rbx
	leaq	-12(%rax,%rdx,4), %rdi
	.p2align	4, 0x90
.LBB6_38:                               # %vector.body149
                                        #   Parent Loop BB6_20 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rbx,%rbp,4), %xmm0
	movups	-16(%rbx,%rbp,4), %xmm1
	movups	(%rdi,%rbp,4), %xmm2
	movups	-16(%rdi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rbp,4)
	movups	%xmm3, -16(%rbx,%rbp,4)
	movups	-32(%rbx,%rbp,4), %xmm0
	movups	-48(%rbx,%rbp,4), %xmm1
	movups	-32(%rdi,%rbp,4), %xmm2
	movups	-48(%rdi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rbp,4)
	movups	%xmm3, -48(%rbx,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rsi
	jne	.LBB6_38
.LBB6_39:                               # %middle.block150
                                        #   in Loop: Header=BB6_30 Depth=2
	cmpq	%r8, %r9
	je	.LBB6_43
# BB#40:                                #   in Loop: Header=BB6_30 Depth=2
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB6_41:                               # %scalar.ph151.preheader
                                        #   in Loop: Header=BB6_30 Depth=2
	incq	%rdx
	.p2align	4, 0x90
.LBB6_42:                               # %scalar.ph151
                                        #   Parent Loop BB6_20 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rax,%rdx,4), %esi
	orl	%esi, -4(%r13,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB6_42
.LBB6_43:                               # %.loopexit103
                                        #   in Loop: Header=BB6_30 Depth=2
	movq	(%rcx), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.LBB6_30
.LBB6_44:                               # %._crit_edge
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB6_69
# BB#45:                                #   in Loop: Header=BB6_20 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	massive_count
	movl	cdata+36(%rip), %eax
	movl	cdata+32(%rip), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_69
# BB#46:                                #   in Loop: Header=BB6_20 Depth=1
	cmpl	$1, %ecx
	je	.LBB6_66
# BB#47:                                #   in Loop: Header=BB6_20 Depth=1
	testl	%eax, %eax
	je	.LBB6_73
# BB#48:                                #   in Loop: Header=BB6_20 Depth=1
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_copy
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rbp
	jle	.LBB6_6
# BB#49:                                # %.lr.ph111
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	cdata+24(%rip), %r9
	movq	cube+72(%rip), %r8
	xorl	%edx, %edx
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB6_50:                               #   Parent Loop BB6_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_59 Depth 3
                                        #       Child Loop BB6_63 Depth 3
	cmpl	$0, (%r9,%rdx,4)
	je	.LBB6_65
# BB#51:                                #   in Loop: Header=BB6_50 Depth=2
	movl	(%r13), %eax
	movq	(%r8,%rdx,8), %rsi
	movl	%eax, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %ax              # imm = 0x3FF
	movq	%rdi, %r11
	movl	$1, %eax
	cmoveq	%rax, %r11
	cmpq	$8, %r11
	jb	.LBB6_62
# BB#52:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_50 Depth=2
	movq	%r11, %r10
	andq	$1016, %r10             # imm = 0x3F8
	je	.LBB6_62
# BB#53:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_50 Depth=2
	leaq	1(%rdi), %rax
	testl	%edi, %edi
	movl	$2, %ecx
	cmovneq	%rcx, %rax
	leaq	(%r12,%rax,4), %rcx
	leaq	4(%rsi,%rdi,4), %rbx
	cmpq	%rbx, %rcx
	jae	.LBB6_55
# BB#54:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_50 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rcx
	leaq	-4(%rsi,%rax,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB6_62
.LBB6_55:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_50 Depth=2
	leaq	-8(%r10), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	jb	.LBB6_57
# BB#56:                                # %vector.body.prol
                                        #   in Loop: Header=BB6_50 Depth=2
	movups	-12(%r13,%rdi,4), %xmm0
	movups	-28(%r13,%rdi,4), %xmm1
	movups	-12(%rsi,%rdi,4), %xmm2
	movups	-28(%rsi,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rdi,4)
	movups	%xmm3, -28(%r13,%rdi,4)
	movl	$8, %ecx
	testq	%rax, %rax
	jne	.LBB6_58
	jmp	.LBB6_60
.LBB6_57:                               #   in Loop: Header=BB6_50 Depth=2
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB6_60
.LBB6_58:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_50 Depth=2
	movq	%r11, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rdi,4), %rbp
	leaq	-12(%rsi,%rdi,4), %rax
	.p2align	4, 0x90
.LBB6_59:                               # %vector.body
                                        #   Parent Loop BB6_20 Depth=1
                                        #     Parent Loop BB6_50 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rbp,%rcx,4), %xmm0
	movups	-16(%rbp,%rcx,4), %xmm1
	movups	(%rax,%rcx,4), %xmm2
	movups	-16(%rax,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rcx,4)
	movups	%xmm3, -16(%rbp,%rcx,4)
	movups	-32(%rbp,%rcx,4), %xmm0
	movups	-48(%rbp,%rcx,4), %xmm1
	movups	-32(%rax,%rcx,4), %xmm2
	movups	-48(%rax,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rcx,4)
	movups	%xmm3, -48(%rbp,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB6_59
.LBB6_60:                               # %middle.block
                                        #   in Loop: Header=BB6_50 Depth=2
	cmpq	%r10, %r11
	je	.LBB6_64
# BB#61:                                #   in Loop: Header=BB6_50 Depth=2
	subq	%r10, %rdi
	.p2align	4, 0x90
.LBB6_62:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_50 Depth=2
	incq	%rdi
	.p2align	4, 0x90
.LBB6_63:                               # %scalar.ph
                                        #   Parent Loop BB6_20 Depth=1
                                        #     Parent Loop BB6_50 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi,%rdi,4), %eax
	orl	%eax, -4(%r13,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB6_63
.LBB6_64:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB6_50 Depth=2
	movl	cube+4(%rip), %eax
.LBB6_65:                               # %.loopexit
                                        #   in Loop: Header=BB6_50 Depth=2
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB6_50
	jmp	.LBB6_4
.LBB6_66:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_68
# BB#67:
	callq	free
.LBB6_68:
	movq	%r14, %rdi
	callq	free
	movl	$1, %ebx
	jmp	.LBB6_72
.LBB6_69:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_71
# BB#70:
	callq	free
.LBB6_71:
	movq	%r14, %rdi
	callq	free
	xorl	%ebx, %ebx
.LBB6_72:
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_73:
	movq	cdata+8(%rip), %rax
	movslq	cdata+40(%rip), %rcx
	movslq	(%rax,%rcx,4), %rax
	movq	8(%r14), %rcx
	subq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-3(%rcx), %rdx
	shrq	$63, %rdx
	leaq	-3(%rcx,%rdx), %rcx
	sarq	%rcx
	movl	$2, %ebx
	cmpq	%rcx, %rax
	jge	.LBB6_72
# BB#74:
	movl	debug(%rip), %ecx
	andl	$512, %ecx              # imm = 0x200
	leaq	48(%rsp), %rsi
	leaq	24(%rsp), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	cubelist_partition
	testl	%eax, %eax
	je	.LBB6_72
# BB#75:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_77
# BB#76:
	callq	free
.LBB6_77:
	movq	%r14, %rdi
	callq	free
	movq	48(%rsp), %rdi
	callq	tautology
	testl	%eax, %eax
	movq	24(%rsp), %rdi
	je	.LBB6_81
# BB#78:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB6_80
# BB#79:
	movq	%rax, %rdi
	callq	free
	movq	24(%rsp), %rax
	movq	$0, (%rax)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_82
.LBB6_80:                               # %.thread
	callq	free
	movq	$0, 24(%rsp)
	movl	$1, %ebx
	jmp	.LBB6_72
.LBB6_81:
	callq	tautology
	movl	%eax, %ebx
	jmp	.LBB6_72
.LBB6_82:
	movl	$1, %ebx
	jmp	.LBB6_72
.Lfunc_end6:
	.size	taut_special_cases, .Lfunc_end6-taut_special_cases
	.cfi_endproc

	.p2align	4, 0x90
	.type	ftautology,@function
ftautology:                             # @ftautology
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 112
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	testb	$2, debug+1(%rip)
	je	.LBB7_2
# BB#1:
	movl	ftautology.ftaut_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, ftautology.ftaut_level(%rip)
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB7_2:
	movq	cube+80(%rip), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rbx
	leaq	24(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#3:
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph88.i
                                        # =>This Inner Loop Header: Depth=1
	testb	$16, 1(%rdi)
	jne	.LBB7_5
# BB#18:                                #   in Loop: Header=BB7_4 Depth=1
	movq	(%r12), %rsi
	xorl	%eax, %eax
	callq	full_row
	testl	%eax, %eax
	jne	.LBB7_19
.LBB7_5:                                # %.backedge78.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rbp), %rdi
	addq	$8, %rbp
	testq	%rdi, %rdi
	jne	.LBB7_4
.LBB7_6:                                # %.preheader75.i
	leaq	16(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	massive_count
	movl	cdata+36(%rip), %eax
	cmpl	cdata+32(%rip), %eax
	je	.LBB7_22
# BB#7:                                 # %.lr.ph85.i
	leaq	-4(%rbx), %rcx
	leaq	4(%rbx), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	$1, %r14d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_11 Depth 2
                                        #       Child Loop BB7_35 Depth 3
                                        #       Child Loop BB7_39 Depth 3
                                        #     Child Loop BB7_43 Depth 2
	testl	%eax, %eax
	je	.LBB7_49
# BB#9:                                 #   in Loop: Header=BB7_8 Depth=1
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_copy
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rbp
	jle	.LBB7_43
# BB#10:                                # %.lr.ph84.i
                                        #   in Loop: Header=BB7_8 Depth=1
	movq	cdata+24(%rip), %r9
	movq	cube+72(%rip), %r8
	xorl	%edx, %edx
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_11:                               #   Parent Loop BB7_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_35 Depth 3
                                        #       Child Loop BB7_39 Depth 3
	cmpl	$0, (%r9,%rdx,4)
	je	.LBB7_41
# BB#12:                                #   in Loop: Header=BB7_11 Depth=2
	movl	(%rbx), %eax
	movq	(%r8,%rdx,8), %rsi
	movl	%eax, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %ax              # imm = 0x3FF
	movq	%rdi, %r11
	cmoveq	%r14, %r11
	cmpq	$8, %r11
	jb	.LBB7_38
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_11 Depth=2
	movq	%r11, %r10
	andq	$1016, %r10             # imm = 0x3F8
	je	.LBB7_38
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_11 Depth=2
	leaq	1(%rdi), %rax
	testl	%edi, %edi
	movl	$2, %ecx
	cmovneq	%rcx, %rax
	leaq	(%r15,%rax,4), %rcx
	leaq	4(%rsi,%rdi,4), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB7_16
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_11 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rcx
	leaq	-4(%rsi,%rax,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB7_38
.LBB7_16:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_11 Depth=2
	leaq	-8(%r10), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB7_17
# BB#32:                                # %vector.body.prol
                                        #   in Loop: Header=BB7_11 Depth=2
	movups	-12(%rbx,%rdi,4), %xmm0
	movups	-28(%rbx,%rdi,4), %xmm1
	movups	-12(%rsi,%rdi,4), %xmm2
	movups	-28(%rsi,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rdi,4)
	movups	%xmm3, -28(%rbx,%rdi,4)
	movl	$8, %eax
	testq	%rcx, %rcx
	jne	.LBB7_34
	jmp	.LBB7_36
.LBB7_17:                               #   in Loop: Header=BB7_11 Depth=2
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB7_36
.LBB7_34:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_11 Depth=2
	movq	%r11, %r14
	andq	$-8, %r14
	negq	%r14
	negq	%rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rcx
	leaq	-12(%rsi,%rdi,4), %rbp
	.p2align	4, 0x90
.LBB7_35:                               # %vector.body
                                        #   Parent Loop BB7_8 Depth=1
                                        #     Parent Loop BB7_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rcx,%rax,4), %xmm0
	movups	-16(%rcx,%rax,4), %xmm1
	movups	(%rbp,%rax,4), %xmm2
	movups	-16(%rbp,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rax,4)
	movups	%xmm3, -16(%rcx,%rax,4)
	movups	-32(%rcx,%rax,4), %xmm0
	movups	-48(%rcx,%rax,4), %xmm1
	movups	-32(%rbp,%rax,4), %xmm2
	movups	-48(%rbp,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rax,4)
	movups	%xmm3, -48(%rcx,%rax,4)
	addq	$-16, %rax
	cmpq	%rax, %r14
	jne	.LBB7_35
.LBB7_36:                               # %middle.block
                                        #   in Loop: Header=BB7_11 Depth=2
	cmpq	%r10, %r11
	movl	$1, %r14d
	je	.LBB7_40
# BB#37:                                #   in Loop: Header=BB7_11 Depth=2
	subq	%r10, %rdi
	.p2align	4, 0x90
.LBB7_38:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_11 Depth=2
	incq	%rdi
	.p2align	4, 0x90
.LBB7_39:                               # %scalar.ph
                                        #   Parent Loop BB7_8 Depth=1
                                        #     Parent Loop BB7_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi,%rdi,4), %eax
	orl	%eax, -4(%rbx,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB7_39
.LBB7_40:                               # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB7_11 Depth=2
	movl	cube+4(%rip), %eax
.LBB7_41:                               # %.loopexit.i
                                        #   in Loop: Header=BB7_11 Depth=2
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB7_11
# BB#42:                                #   in Loop: Header=BB7_8 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rbp
	jmp	.LBB7_43
.LBB7_45:                               #   in Loop: Header=BB7_43 Depth=2
	movq	%r14, (%rbp)
	addq	$8, %rbp
	.p2align	4, 0x90
.LBB7_43:                               #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.LBB7_46
# BB#44:                                #   in Loop: Header=BB7_43 Depth=2
	addq	$8, %r15
	movq	(%r12), %rdx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	set_or
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB7_43
	jmp	.LBB7_45
	.p2align	4, 0x90
.LBB7_46:                               #   in Loop: Header=BB7_8 Depth=1
	movq	$0, (%rbp)
	addq	$8, %rbp
	movq	%rbp, 8(%r12)
	testb	$2, debug+1(%rip)
	je	.LBB7_48
# BB#47:                                #   in Loop: Header=BB7_8 Depth=1
	movl	cdata+36(%rip), %esi
	subq	%r12, %rbp
	sarq	$3, %rbp
	addq	$-3, %rbp
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	printf
.LBB7_48:                               # %.backedge77.i
                                        #   in Loop: Header=BB7_8 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	massive_count
	movl	cdata+36(%rip), %eax
	cmpl	cdata+32(%rip), %eax
	movl	$1, %r14d
	jne	.LBB7_8
.LBB7_22:                               # %._crit_edge86.i
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	40(%r13), %rax
	testq	%rax, %rax
	je	.LBB7_24
# BB#23:
	movl	(%rax), %eax
	incl	%eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB7_24:
	movl	Rp_current(%rip), %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	sm_insert
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB7_29
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	testb	$16, 1(%rbx)
	je	.LBB7_28
# BB#26:                                #   in Loop: Header=BB7_25 Depth=1
	movq	(%r12), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	full_row
	testl	%eax, %eax
	je	.LBB7_28
# BB#27:                                #   in Loop: Header=BB7_25 Depth=1
	movzwl	2(%rbx), %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	sm_insert
.LBB7_28:                               # %.backedge.i
                                        #   in Loop: Header=BB7_25 Depth=1
	movq	(%rbp), %rbx
	addq	$8, %rbp
	testq	%rbx, %rbx
	jne	.LBB7_25
.LBB7_29:                               # %._crit_edge.i
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_31
# BB#30:
	callq	free
.LBB7_31:
	movq	%r12, %rdi
.LBB7_61:                               # %ftaut_special_cases.exit.thread
	callq	free
	testb	$2, debug+1(%rip)
	je	.LBB7_63
	jmp	.LBB7_64
.LBB7_19:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_21
# BB#20:
	callq	free
.LBB7_21:
	movq	%r12, %rdi
	callq	free
	movq	8(%rsp), %r13           # 8-byte Reload
	testb	$2, debug+1(%rip)
	je	.LBB7_63
	jmp	.LBB7_64
.LBB7_49:                               # %ftaut_special_cases.exit
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	movq	8(%rsp), %r13           # 8-byte Reload
	jge	.LBB7_51
# BB#50:
	movl	$8, %edi
	jmp	.LBB7_52
.LBB7_51:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB7_52:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r14
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB7_54
# BB#53:
	movl	$8, %edi
	jmp	.LBB7_55
.LBB7_54:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB7_55:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	$512, %ecx              # imm = 0x200
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	binate_split_select
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	scofactor
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	ftautology
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	scofactor
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	ftautology
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_57
# BB#56:
	callq	free
.LBB7_57:
	movq	%r12, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB7_59
# BB#58:
	movq	%r14, %rdi
	callq	free
.LBB7_59:
	testq	%r15, %r15
	je	.LBB7_62
# BB#60:
	movq	%r15, %rdi
	jmp	.LBB7_61
.LBB7_62:                               # %ftaut_special_cases.exit.thread
	testb	$2, debug+1(%rip)
	jne	.LBB7_64
.LBB7_63:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_64:
	movl	ftautology.ftaut_level(%rip), %esi
	decl	%esi
	movl	%esi, ftautology.ftaut_level(%rip)
	movl	48(%r13), %edx
	movl	72(%r13), %ecx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end7:
	.size	ftautology, .Lfunc_end7-ftautology
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"# IRRED: F=%d E=%d R=%d Rt=%d Rp=%d Rc=%d Final=%d Bound=%d\n"
	.size	.L.str, 61

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"IRRED1: zr=%d ze=%d to-go=%d time=%s\n"
	.size	.L.str.1, 38

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"IRRED1: zr=%d zrt=%d to-go=%d time=%s\n"
	.size	.L.str.2, 39

	.type	Rp_current,@object      # @Rp_current
	.local	Rp_current
	.comm	Rp_current,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"IRRED1: %d of %d to-go=%d, table=%dx%d time=%s\n"
	.size	.L.str.3, 48

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"IRRED1: delete redundant rows, now %dx%d\n"
	.size	.L.str.4, 42

	.type	tautology.taut_level,@object # @tautology.taut_level
	.local	tautology.taut_level
	.comm	tautology.taut_level,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"TAUTOLOGY"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"exit TAUTOLOGY[%d]: %s\n"
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FALSE"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"TRUE"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"MAYBE"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"UNATE_REDUCTION: %d unate variables, reduced to %d\n"
	.size	.L.str.10, 52

	.type	ftautology.ftaut_level,@object # @ftautology.ftaut_level
	.local	ftautology.ftaut_level
	.comm	ftautology.ftaut_level,4,4
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FIND_TAUTOLOGY"
	.size	.L.str.11, 15

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"exit FIND_TAUTOLOGY[%d]: table is %d by %d\n"
	.size	.L.str.12, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
