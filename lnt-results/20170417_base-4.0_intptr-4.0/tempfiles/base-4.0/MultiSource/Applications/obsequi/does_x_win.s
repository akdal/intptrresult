	.text
	.file	"does_x_win.bc"
	.globl	does_next_player_win
	.p2align	4, 0x90
	.type	does_next_player_win,@function
does_next_player_win:                   # @does_next_player_win
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movl	%edi, %r15d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movslq	%r15d, %r9
	leaq	(%r9,%r9,2), %rax
	movl	g_info_totals(,%rax,4), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	g_empty_squares(%rip), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	g_board_size(,%r9,4), %r8
	testq	%r8, %r8
	jle	.LBB0_1
# BB#8:                                 # %.lr.ph60.preheader.i
	shlq	$7, %r9
	movl	g_board+4(%r9), %r10d
	movl	g_board+8(%r9), %r13d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph60.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
	movq	%r12, %rcx
	movl	%r11d, %edi
	movl	%eax, %r14d
	orl	%edi, %r14d
	orl	%esi, %r14d
	orl	%r10d, %r14d
	movl	%r13d, %r10d
	leaq	1(%rcx), %r12
	movl	76(%rsp,%rcx,4), %r11d
	orl	%r10d, %r14d
	orl	%r11d, %r14d
	movl	%r14d, %esi
	shrl	%esi
	orl	%r14d, %esi
	movl	g_board(%r9,%rcx,4), %ebp
	movl	%ebp, %edx
	shrl	%edx
	andl	%ebp, %edx
	movl	g_board+12(%r9,%rcx,4), %r13d
	movl	%r13d, %ebp
	shrl	%ebp
	andl	%r13d, %ebp
	orl	%edx, %ebp
	notl	%esi
	andl	%ebp, %esi
	je	.LBB0_10
# BB#12:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	68(%rsp,%rcx,4), %eax
	movl	72(%rsp,%rcx,4), %edi
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	negl	%edx
	andl	%esi, %edx
	leal	(%rdx,%rdx), %ebp
	orl	%edx, %ebp
	orl	%ebp, %eax
	orl	%ebp, %edi
	notl	%ebp
	incl	%ebx
	andl	%ebp, %esi
	jne	.LBB0_13
# BB#9:                                 # %..loopexit_crit_edge.i
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	%eax, 68(%rsp,%rcx,4)
	movl	%edi, 72(%rsp,%rcx,4)
.LBB0_10:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	%eax, %esi
	cmpq	%r8, %r12
	movl	%edi, %eax
	jl	.LBB0_11
	jmp	.LBB0_2
.LBB0_1:
	xorl	%ebx, %ebx
.LBB0_2:                                # %pack_prot.exit
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	20(%rsp), %rax
	leaq	72(%rsp), %rbp
	leaq	32(%rsp), %rdx
	leaq	44(%rsp), %rcx
	leaq	48(%rsp), %r8
	leaq	40(%rsp), %r9
	movq	%rbp, %rdi
	movl	%r15d, %esi
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	pack_vuln
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	leaq	28(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	20(%rsp), %r8
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	pack_safe
	movl	44(%rsp), %r14d         # 4-byte Reload
	testl	%r14d, %r14d
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_3
.LBB0_4:
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	andl	$-2, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB0_5
.LBB0_27:
	decl	%ebx
	addl	$2, %ebp
	jmp	.LBB0_28
.LBB0_5:
	movslq	24(%rsp), %rax
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_14
.LBB0_6:
	leal	-1(%rax), %ecx
	movl	%ecx, 24(%rsp)
	incl	%ebp
	movl	36(%rsp), %ecx
	cmpl	%eax, %ecx
	jl	.LBB0_28
# BB#7:
	decl	%ecx
	movl	%ecx, 36(%rsp)
	jmp	.LBB0_28
.LBB0_14:
	movl	40(%rsp), %ecx
	testb	$1, %cl
	jne	.LBB0_15
# BB#17:
	movl	28(%rsp), %edx
	testb	$1, %dl
	jne	.LBB0_18
# BB#19:
	movl	16(%rsp), %edx
	testb	$1, %dl
	jne	.LBB0_20
# BB#21:
	movl	20(%rsp), %edx
	testb	$1, %dl
	jne	.LBB0_22
# BB#23:
	testl	%eax, %eax
	jg	.LBB0_6
# BB#24:
	testl	%ecx, %ecx
	jle	.LBB0_25
.LBB0_15:
	leal	-1(%rcx), %eax
	movl	%eax, 40(%rsp)
	incl	%ebp
	movl	32(%rsp), %eax
	cmpl	%ecx, %eax
	jl	.LBB0_28
# BB#16:
	decl	%eax
	movl	%eax, 32(%rsp)
	jmp	.LBB0_28
.LBB0_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	24(%rsp), %ecx
	movl	36(%rsp), %r8d
	movl	40(%rsp), %r9d
	movl	32(%rsp), %r10d
	movl	12(%rsp), %r11d
	movl	$.L.str.1, %esi
	movl	$0, %eax
	movl	%ebx, %edx
	pushq	%r12
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$32, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -32
	movq	stderr(%rip), %rdi
	movl	28(%rsp), %edx
	movl	16(%rsp), %ecx
	movl	20(%rsp), %r8d
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_4
.LBB0_18:
	decl	%edx
	movl	%edx, 28(%rsp)
	addl	$3, 12(%rsp)
	jmp	.LBB0_28
.LBB0_20:
	decl	%edx
	movl	%edx, 16(%rsp)
	addl	$2, 12(%rsp)
	jmp	.LBB0_28
.LBB0_22:
	decl	%edx
	movl	%edx, 20(%rsp)
	incl	12(%rsp)
.LBB0_28:
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	andl	$-2, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB0_29
# BB#30:
	decl	%ebx
	movl	24(%rsp), %eax
	addl	$2, %eax
	movl	%eax, 24(%rsp)
	jmp	.LBB0_31
.LBB0_29:                               # %._crit_edge
	movl	24(%rsp), %eax
.LBB0_31:
	movslq	%eax, %r8
	imulq	$1431655766, %r8, %rdx  # imm = 0x55555556
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$32, %rdx
	addl	%eax, %edx
	movl	40(%rsp), %r10d
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	addl	%ebp, %ebx
	addl	%edx, %ebx
	addl	%eax, %ebx
	leal	(%rdx,%rdx,2), %edx
	movl	%r10d, %edi
	andl	$1, %edi
	movl	%r8d, %ebp
	subl	%edx, %ebp
	je	.LBB0_34
# BB#32:
	testl	%edi, %edi
	je	.LBB0_34
# BB#33:                                # %.thread
	incl	%ebx
	decl	12(%rsp)
	movl	28(%rsp), %r9d
	movl	%r9d, %esi
	shrl	$31, %esi
	addl	%r9d, %esi
	andl	$-2, %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$1, %edi
	jne	.LBB0_38
.LBB0_37:
	decl	%r9d
	movl	%r9d, 28(%rsp)
	incl	16(%rsp)
	jmp	.LBB0_38
.LBB0_34:
	movl	28(%rsp), %r9d
	movl	%r9d, %edx
	shrl	$31, %edx
	addl	%r9d, %edx
	andl	$-2, %edx
	movl	%r9d, %esi
	subl	%edx, %esi
	testl	%ebp, %ebp
	jne	.LBB0_40
# BB#35:
	testl	%edi, %edi
	jne	.LBB0_40
# BB#36:
	cmpl	$1, %esi
	je	.LBB0_37
	jmp	.LBB0_38
.LBB0_40:
	cmpl	$1, %esi
	jne	.LBB0_42
# BB#41:
	addl	$3, 12(%rsp)
.LBB0_38:
	movl	16(%rsp), %edi
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	movl	%edi, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	jne	.LBB0_46
# BB#39:
	decl	%edi
	movl	%edi, 16(%rsp)
	incl	20(%rsp)
	jmp	.LBB0_46
.LBB0_42:
	movl	16(%rsp), %edi
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	movl	%edi, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	jne	.LBB0_44
# BB#43:
	addl	$2, 12(%rsp)
	jmp	.LBB0_46
.LBB0_44:
	movl	20(%rsp), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	andl	$-2, %esi
	subl	%esi, %edx
	cmpl	$1, %edx
	jne	.LBB0_46
# BB#45:
	incl	12(%rsp)
.LBB0_46:
	movl	36(%rsp), %edx
	movl	%r8d, %esi
	subl	%edx, %esi
	movslq	%esi, %rsi
	imulq	$1431655766, %rsi, %rsi # imm = 0x55555556
	movq	%rsi, %rbp
	shrq	$63, %rbp
	shrq	$32, %rsi
	addl	%ebp, %esi
	imulq	$1431655765, %r8, %rbp  # imm = 0x55555555
	shrq	$32, %rbp
	subl	%r8d, %ebp
	movl	%ebp, %ecx
	shrl	$31, %ecx
	sarl	%ebp
	addl	%ecx, %ebp
	addl	%edx, %ebp
	addl	12(%rsp), %ebp
	addl	%esi, %ebp
	movl	32(%rsp), %ecx
	subl	%ecx, %r10d
	movl	%r10d, %edx
	shrl	$31, %edx
	addl	%r10d, %edx
	sarl	%edx
	subl	%eax, %ecx
	addl	%ebp, %ecx
	addl	%edx, %ecx
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	leal	(%rax,%rax,2), %eax
	addl	%ecx, %eax
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	andl	$-2, %ecx
	addl	%eax, %ecx
	movl	20(%rsp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	addl	%ecx, %edx
	movl	%edx, 12(%rsp)
	leal	(%rbx,%rbx), %eax
	subl	%eax, %r12d
	subl	%edx, %r12d
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
	movl	%ebx, %ebp
	subl	%edx, %ebp
	testl	%r14d, %r14d
	je	.LBB0_49
# BB#47:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	testl	%ebp, %ebp
	js	.LBB0_49
# BB#48:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB0_49:
	movl	%ebp, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_25:
	testl	%ebx, %ebx
	jg	.LBB0_27
# BB#26:
	movl	$-1, %ebp
	jmp	.LBB0_49
.Lfunc_end0:
	.size	does_next_player_win, .Lfunc_end0-does_next_player_win
	.cfi_endproc

	.p2align	4, 0x90
	.type	pack_vuln,@function
pack_vuln:                              # @pack_vuln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movq	%r8, -16(%rsp)          # 8-byte Spill
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	cmpl	$0, g_board_size(,%rcx,4)
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph201.preheader
	movl	(%rdi), %ebp
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shlq	$7, %rsi
	xorl	%r11d, %r11d
	movl	$0, -68(%rsp)           # 4-byte Folded Spill
	xorl	%r15d, %r15d
	movl	$0, -64(%rsp)           # 4-byte Folded Spill
	xorl	%ebx, %ebx
	movl	$0, -60(%rsp)           # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph201
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	movq	%rdx, %r13
	leaq	1(%r13), %rdx
	movl	4(%rdi,%r13,4), %r10d
	movl	g_board+4(%rsi,%r13,4), %r9d
	orl	%r10d, %r9d
	cmpl	$-1, %r9d
	je	.LBB1_38
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movl	g_board+8(%rsi,%r13,4), %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	notl	%ecx
	orl	%ecx, %ebp
	movl	%edx, %r8d
	shrl	%r8d
	orl	%edx, %r8d
	notl	%r8d
	orl	%ebp, %r8d
	andl	g_board(%rsi,%r13,4), %edx
	notl	%r9d
	xorl	%r14d, %r14d
	movl	%r15d, %eax
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %ecx
	movl	%r9d, %r12d
	negl	%r12d
	andl	%r9d, %r12d
	movl	%r12d, %ebp
	shrl	%ebp
	testl	%ecx, %ebp
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	movl	%r14d, %r15d
.LBB1_13:                               #   in Loop: Header=BB1_5 Depth=2
	xorl	%r12d, %r9d
	testl	%r12d, %edx
	je	.LBB1_22
# BB#14:                                #   in Loop: Header=BB1_5 Depth=2
	testl	%r15d, %r15d
	je	.LBB1_15
# BB#16:                                #   in Loop: Header=BB1_5 Depth=2
	leal	-1(%r15), %ecx
	cmpl	$3, %ecx
	ja	.LBB1_17
# BB#18:                                #   in Loop: Header=BB1_5 Depth=2
	xorl	%r14d, %r14d
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_19:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$3, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=2
	xorl	%r15d, %r15d
	cmpl	$3, %r14d
	je	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=2
	cmpl	$1, %r14d
	jne	.LBB1_13
# BB#9:                                 #   in Loop: Header=BB1_5 Depth=2
	incl	-68(%rsp)               # 4-byte Folded Spill
	orl	%ecx, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$2, %r14d
	testl	%r15d, %r15d
	je	.LBB1_23
# BB#24:                                #   in Loop: Header=BB1_5 Depth=2
	leal	-1(%r15), %ecx
	cmpl	$3, %ecx
	ja	.LBB1_25
# BB#26:                                #   in Loop: Header=BB1_5 Depth=2
	jmpq	*.LJTI1_1(,%rcx,8)
.LBB1_27:                               #   in Loop: Header=BB1_5 Depth=2
	orl	%r12d, %ebp
	orl	%ebp, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	testl	%r8d, %ebp
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_5 Depth=2
	incl	%ebx
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$1, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_23:                               #   in Loop: Header=BB1_5 Depth=2
	movl	%r12d, %r11d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_10:                               #   in Loop: Header=BB1_5 Depth=2
	leal	(%r11,%r11), %ecx
	orl	%r11d, %ecx
	orl	%ecx, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	testl	%r8d, %ecx
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_5 Depth=2
	incl	%ebx
	jmp	.LBB1_13
.LBB1_17:                               #   in Loop: Header=BB1_5 Depth=2
	movl	%r15d, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_20:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$4, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_21:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$3, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_30:                               #   in Loop: Header=BB1_5 Depth=2
	leal	(%r11,%r11), %ecx
	orl	%r11d, %ecx
	orl	%ecx, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	testl	%r8d, %ecx
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_5 Depth=2
	incl	-60(%rsp)               # 4-byte Folded Spill
	jmp	.LBB1_33
.LBB1_40:                               #   in Loop: Header=BB1_5 Depth=2
	leal	(%r11,%r11), %ecx
	orl	%r11d, %ecx
	orl	%ecx, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	xorl	%ebp, %ebp
	andl	%r8d, %ecx
	setne	%bpl
	movl	%eax, %esi
	xorl	%eax, %eax
	testl	%ecx, %ecx
	sete	%al
	addl	%ebp, %ebx
	addl	%eax, %esi
	movl	%esi, %eax
.LBB1_41:                               #   in Loop: Header=BB1_5 Depth=2
	movl	%r12d, %r11d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_12:                               #   in Loop: Header=BB1_5 Depth=2
	incl	%eax
	jmp	.LBB1_13
.LBB1_25:                               #   in Loop: Header=BB1_5 Depth=2
	movl	%r15d, %r14d
	testl	%r9d, %r9d
	jne	.LBB1_5
	jmp	.LBB1_35
.LBB1_29:                               #   in Loop: Header=BB1_5 Depth=2
	incl	%eax
	jmp	.LBB1_33
.LBB1_32:                               #   in Loop: Header=BB1_5 Depth=2
	incl	-64(%rsp)               # 4-byte Folded Spill
.LBB1_33:                               # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=2
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_34:                               # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=2
	testl	%r9d, %r9d
	jne	.LBB1_5
.LBB1_35:                               # %._crit_edge
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%eax, %r15d
	cmpl	$1, %r14d
	je	.LBB1_42
# BB#36:                                # %._crit_edge
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpl	$3, %r14d
	jne	.LBB1_37
# BB#43:                                #   in Loop: Header=BB1_3 Depth=1
	leal	(%r11,%r11), %eax
	orl	%r11d, %eax
	orl	%eax, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	testl	%r8d, %eax
	je	.LBB1_45
# BB#44:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebx
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_42:                               #   in Loop: Header=BB1_3 Depth=1
	incl	-68(%rsp)               # 4-byte Folded Spill
	orl	%r12d, %r10d
	movl	%r10d, 4(%rdi,%r13,4)
	jmp	.LBB1_37
.LBB1_45:                               #   in Loop: Header=BB1_3 Depth=1
	incl	%r15d
	.p2align	4, 0x90
.LBB1_37:                               # %.backedge175
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	movq	-40(%rsp), %rdx         # 8-byte Reload
.LBB1_38:                               # %.backedge175
                                        #   in Loop: Header=BB1_3 Depth=1
	movslq	g_board_size(,%rcx,4), %rax
	cmpq	%rax, %rdx
	movl	%r10d, %ebp
	jl	.LBB1_3
	jmp	.LBB1_39
.LBB1_1:
	movl	$0, -60(%rsp)           # 4-byte Folded Spill
	xorl	%ebx, %ebx
	movl	$0, -64(%rsp)           # 4-byte Folded Spill
	xorl	%r15d, %r15d
	movl	$0, -68(%rsp)           # 4-byte Folded Spill
.LBB1_39:                               # %._crit_edge202
	movl	-60(%rsp), %ecx         # 4-byte Reload
	addl	%ebx, %ecx
	movq	-32(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax)
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	%ebx, (%rax)
	movl	-64(%rsp), %ecx         # 4-byte Reload
	addl	%r15d, %ecx
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax)
	movq	-8(%rsp), %rax          # 8-byte Reload
	movl	%r15d, (%rax)
	movq	56(%rsp), %rax
	movl	-68(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	pack_vuln, .Lfunc_end1-pack_vuln
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_34
	.quad	.LBB1_19
	.quad	.LBB1_20
	.quad	.LBB1_21
.LJTI1_1:
	.quad	.LBB1_27
	.quad	.LBB1_30
	.quad	.LBB1_40
	.quad	.LBB1_41

	.text
	.p2align	4, 0x90
	.type	pack_safe,@function
pack_safe:                              # @pack_safe
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%r8, -24(%rsp)          # 8-byte Spill
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movslq	%esi, %r9
	movl	g_board_size(,%r9,4), %r8d
	testl	%r8d, %r8d
	jle	.LBB2_1
# BB#3:                                 # %.lr.ph129.preheader
	movq	%r9, %rcx
	shlq	$7, %rcx
	movl	g_board+4(%rcx), %ebp
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	$0, -52(%rsp)           # 4-byte Folded Spill
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph129
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
	movl	%ebp, %r11d
	movl	g_board+8(%rcx,%r13,4), %ebp
	movl	g_board(%rcx,%r13,4), %edx
	andl	%ebp, %edx
	leaq	1(%r13), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movl	4(%rdi,%r13,4), %esi
	orl	%esi, %r11d
	movl	%r11d, %ebx
	shrl	%ebx
	orl	%r11d, %ebx
	notl	%ebx
	movl	%edx, %r14d
	shrl	%r14d
	andl	%edx, %r14d
	andl	%ebx, %r14d
	je	.LBB2_5
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_6 Depth=1
	leaq	g_board(%rcx,%r13,4), %rbx
	movl	(%rdi,%r13,4), %r9d
	movl	8(%rdi,%r13,4), %r8d
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %eax
	movl	%r14d, %ecx
	movl	%ecx, %edx
	negl	%edx
	andl	%ecx, %edx
	leal	(%rdx,%rdx), %r11d
	orl	%edx, %r11d
	movl	%r11d, %r14d
	notl	%r14d
	andl	%ecx, %r14d
	orl	%eax, %r11d
	movl	%r9d, %eax
	orl	%r11d, %eax
	orl	%r8d, %eax
	movl	%edx, %r10d
	shrl	%r10d
	testl	%r10d, %eax
	je	.LBB2_9
# BB#17:                                #   in Loop: Header=BB2_8 Depth=2
	movl	%r14d, %eax
	orl	%r11d, %eax
	orl	%r9d, %eax
	orl	%r8d, %eax
	shll	$2, %edx
	testl	%edx, %eax
	jne	.LBB2_26
# BB#18:                                #   in Loop: Header=BB2_8 Depth=2
	orl	%edx, %r11d
	orl	%edx, %esi
	movl	%esi, 4(%rdi,%r13,4)
	testl	(%rbx), %edx
	je	.LBB2_19
.LBB2_23:                               #   in Loop: Header=BB2_8 Depth=2
	movl	8(%rbx), %ebp
	testl	%edx, %ebp
	je	.LBB2_24
.LBB2_27:                               #   in Loop: Header=BB2_8 Depth=2
	incl	%r12d
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_8 Depth=2
	orl	%r10d, %r11d
	orl	%r10d, %esi
	movl	%esi, 4(%rdi,%r13,4)
	testl	(%rbx), %r10d
	je	.LBB2_10
.LBB2_14:                               #   in Loop: Header=BB2_8 Depth=2
	movl	8(%rbx), %ebp
	testl	%r10d, %ebp
	je	.LBB2_15
.LBB2_16:                               #   in Loop: Header=BB2_8 Depth=2
	incl	%r12d
	jmp	.LBB2_26
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=2
	testl	-4(%rbx), %r10d
	je	.LBB2_14
# BB#11:                                #   in Loop: Header=BB2_8 Depth=2
	movl	8(%rbx), %ebp
	testl	%r10d, %ebp
	jne	.LBB2_25
# BB#12:                                #   in Loop: Header=BB2_8 Depth=2
	testl	12(%rbx), %r10d
	je	.LBB2_25
# BB#13:                                #   in Loop: Header=BB2_8 Depth=2
	incl	-52(%rsp)               # 4-byte Folded Spill
	jmp	.LBB2_26
.LBB2_15:                               #   in Loop: Header=BB2_8 Depth=2
	testl	12(%rbx), %r10d
	jne	.LBB2_25
	jmp	.LBB2_16
.LBB2_19:                               #   in Loop: Header=BB2_8 Depth=2
	testl	-4(%rbx), %edx
	je	.LBB2_23
# BB#20:                                #   in Loop: Header=BB2_8 Depth=2
	movl	8(%rbx), %ebp
	testl	%edx, %ebp
	jne	.LBB2_25
# BB#21:                                #   in Loop: Header=BB2_8 Depth=2
	testl	12(%rbx), %edx
	je	.LBB2_25
# BB#22:                                #   in Loop: Header=BB2_8 Depth=2
	incl	-52(%rsp)               # 4-byte Folded Spill
	jmp	.LBB2_26
.LBB2_24:                               #   in Loop: Header=BB2_8 Depth=2
	testl	12(%rbx), %edx
	je	.LBB2_27
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_8 Depth=2
	incl	%r15d
.LBB2_26:                               # %.backedge
                                        #   in Loop: Header=BB2_8 Depth=2
	testl	%r14d, %r14d
	jne	.LBB2_8
# BB#4:                                 # %.loopexit.loopexit
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	-8(%rsp), %r9           # 8-byte Reload
	movl	g_board_size(,%r9,4), %r8d
	movq	-16(%rsp), %rcx         # 8-byte Reload
.LBB2_5:                                # %.loopexit
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	-48(%rsp), %r13         # 8-byte Reload
	movslq	%r8d, %rax
	cmpq	%rax, %r13
	jl	.LBB2_6
	jmp	.LBB2_2
.LBB2_1:
	movl	$0, -52(%rsp)           # 4-byte Folded Spill
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB2_2:                                # %._crit_edge
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	-52(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, (%rax)
	movq	-32(%rsp), %rax         # 8-byte Reload
	movl	%r15d, (%rax)
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	pack_safe, .Lfunc_end2-pack_safe
	.cfi_endproc

	.globl	does_who_just_moved_win
	.p2align	4, 0x90
	.type	does_who_just_moved_win,@function
does_who_just_moved_win:                # @does_who_just_moved_win
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 256
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movl	%edi, %r15d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movslq	%r15d, %r9
	leaq	(%r9,%r9,2), %rax
	movl	g_info_totals(,%rax,4), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	g_empty_squares(%rip), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	g_board_size(,%r9,4), %r8
	testq	%r8, %r8
	jle	.LBB3_1
# BB#6:                                 # %.lr.ph60.preheader.i
	shlq	$7, %r9
	movl	g_board+4(%r9), %r10d
	movl	g_board+8(%r9), %r13d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph60.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
	movq	%r12, %rcx
	movl	%r11d, %edi
	movl	%eax, %r14d
	orl	%edi, %r14d
	orl	%esi, %r14d
	orl	%r10d, %r14d
	movl	%r13d, %r10d
	leaq	1(%rcx), %r12
	movl	76(%rsp,%rcx,4), %r11d
	orl	%r10d, %r14d
	orl	%r11d, %r14d
	movl	%r14d, %esi
	shrl	%esi
	orl	%r14d, %esi
	movl	g_board(%r9,%rcx,4), %ebp
	movl	%ebp, %edx
	shrl	%edx
	andl	%ebp, %edx
	movl	g_board+12(%r9,%rcx,4), %r13d
	movl	%r13d, %ebp
	shrl	%ebp
	andl	%r13d, %ebp
	orl	%edx, %ebp
	notl	%esi
	andl	%ebp, %esi
	je	.LBB3_8
# BB#10:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	68(%rsp,%rcx,4), %eax
	movl	72(%rsp,%rcx,4), %edi
	.p2align	4, 0x90
.LBB3_11:                               #   Parent Loop BB3_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	negl	%edx
	andl	%esi, %edx
	leal	(%rdx,%rdx), %ebp
	orl	%edx, %ebp
	orl	%ebp, %eax
	orl	%ebp, %edi
	notl	%ebp
	incl	%ebx
	andl	%ebp, %esi
	jne	.LBB3_11
# BB#7:                                 # %..loopexit_crit_edge.i
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	%eax, 68(%rsp,%rcx,4)
	movl	%edi, 72(%rsp,%rcx,4)
.LBB3_8:                                # %.loopexit.i
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	%eax, %esi
	cmpq	%r8, %r12
	movl	%edi, %eax
	jl	.LBB3_9
	jmp	.LBB3_2
.LBB3_1:
	xorl	%ebx, %ebx
.LBB3_2:                                # %pack_prot.exit
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %rax
	leaq	72(%rsp), %rbp
	leaq	36(%rsp), %rdx
	leaq	48(%rsp), %rcx
	leaq	44(%rsp), %r8
	leaq	40(%rsp), %r9
	movq	%rbp, %rdi
	movl	%r15d, %esi
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	pack_vuln
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	leaq	24(%rsp), %rdx
	leaq	12(%rsp), %rcx
	leaq	20(%rsp), %r8
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	pack_safe
	movl	44(%rsp), %r14d         # 4-byte Reload
	testl	%r14d, %r14d
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_3
.LBB3_4:
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	andl	$-2, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB3_5
# BB#12:
	decl	%ebx
	movl	28(%rsp), %eax
	addl	$2, %eax
	movl	%eax, 28(%rsp)
	jmp	.LBB3_13
.LBB3_5:                                # %._crit_edge
	movl	28(%rsp), %eax
.LBB3_13:
	movslq	%eax, %r8
	imulq	$1431655766, %r8, %rdx  # imm = 0x55555556
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$32, %rdx
	addl	%eax, %edx
	movl	36(%rsp), %r10d
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	addl	%ebp, %ebx
	addl	%edx, %ebx
	addl	%eax, %ebx
	leal	(%rdx,%rdx,2), %edx
	movl	%r10d, %edi
	andl	$1, %edi
	movl	%r8d, %ebp
	subl	%edx, %ebp
	je	.LBB3_16
# BB#14:
	testl	%edi, %edi
	je	.LBB3_16
# BB#15:                                # %.thread
	incl	%ebx
	decl	16(%rsp)
	movl	24(%rsp), %r9d
	movl	%r9d, %esi
	shrl	$31, %esi
	addl	%r9d, %esi
	andl	$-2, %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$1, %edi
	jne	.LBB3_20
.LBB3_19:
	decl	%r9d
	movl	%r9d, 24(%rsp)
	incl	12(%rsp)
	jmp	.LBB3_20
.LBB3_16:
	movl	24(%rsp), %r9d
	movl	%r9d, %edx
	shrl	$31, %edx
	addl	%r9d, %edx
	andl	$-2, %edx
	movl	%r9d, %esi
	subl	%edx, %esi
	testl	%ebp, %ebp
	jne	.LBB3_22
# BB#17:
	testl	%edi, %edi
	jne	.LBB3_22
# BB#18:
	cmpl	$1, %esi
	je	.LBB3_19
	jmp	.LBB3_20
.LBB3_22:
	cmpl	$1, %esi
	jne	.LBB3_24
# BB#23:
	addl	$3, 16(%rsp)
.LBB3_20:
	movl	12(%rsp), %edi
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	movl	%edi, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	jne	.LBB3_28
# BB#21:
	decl	%edi
	movl	%edi, 12(%rsp)
	incl	20(%rsp)
.LBB3_28:
	movl	40(%rsp), %edx
	movl	%r8d, %esi
	subl	%edx, %esi
	movslq	%esi, %rsi
	imulq	$1431655766, %rsi, %rsi # imm = 0x55555556
	movq	%rsi, %rbp
	shrq	$63, %rbp
	shrq	$32, %rsi
	addl	%ebp, %esi
	imulq	$1431655765, %r8, %rbp  # imm = 0x55555555
	shrq	$32, %rbp
	subl	%r8d, %ebp
	movl	%ebp, %ecx
	shrl	$31, %ecx
	sarl	%ebp
	addl	%ecx, %ebp
	addl	%edx, %ebp
	addl	16(%rsp), %ebp
	addl	%esi, %ebp
	movl	32(%rsp), %ecx
	subl	%ecx, %r10d
	movl	%r10d, %edx
	shrl	$31, %edx
	addl	%r10d, %edx
	sarl	%edx
	subl	%eax, %ecx
	addl	%ebp, %ecx
	addl	%edx, %ecx
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	leal	(%rax,%rax,2), %eax
	addl	%ecx, %eax
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	andl	$-2, %ecx
	addl	%eax, %ecx
	movl	20(%rsp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	addl	%ecx, %edx
	movl	%edx, 16(%rsp)
	leal	(%rbx,%rbx), %eax
	subl	%eax, %r12d
	subl	%edx, %r12d
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
	movl	%ebx, %ebp
	subl	%edx, %ebp
	testl	%r14d, %r14d
	je	.LBB3_31
# BB#29:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	testl	%ebp, %ebp
	js	.LBB3_31
# BB#30:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB3_31:
	movl	%ebp, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_24:
	movl	12(%rsp), %edi
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	movl	%edi, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	jne	.LBB3_26
# BB#25:
	addl	$2, 16(%rsp)
	jmp	.LBB3_28
.LBB3_26:
	movl	20(%rsp), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	andl	$-2, %esi
	subl	%esi, %edx
	cmpl	$1, %edx
	jne	.LBB3_28
# BB#27:
	incl	16(%rsp)
	jmp	.LBB3_28
.LBB3_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	28(%rsp), %ecx
	movl	40(%rsp), %r8d
	movl	36(%rsp), %r9d
	movl	32(%rsp), %r10d
	movl	16(%rsp), %r11d
	movl	$.L.str.1, %esi
	movl	$0, %eax
	movl	%ebx, %edx
	pushq	%r12
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$32, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -32
	movq	stderr(%rip), %rdi
	movl	24(%rsp), %edx
	movl	12(%rsp), %ecx
	movl	20(%rsp), %r8d
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB3_4
.Lfunc_end3:
	.size	does_who_just_moved_win, .Lfunc_end3-does_who_just_moved_win
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d moves next, do they win?\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"prot %d, vuln2 %d(%d), vuln1 %d(%d), safe %d, unused %d, empty %d.\n"
	.size	.L.str.1, 68

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"safe_op2 %d, safe_op1 %d, safe_op0 %d.\n"
	.size	.L.str.2, 40

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"moves:%d, opp:%d.\n"
	.size	.L.str.3, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d just moved, do they win?\n"
	.size	.L.str.5, 29

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"H WINS"
	.size	.Lstr.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
