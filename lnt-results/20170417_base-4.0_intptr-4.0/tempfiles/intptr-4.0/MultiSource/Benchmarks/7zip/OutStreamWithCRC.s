	.text
	.file	"OutStreamWithCRC.bc"
	.globl	_ZN17COutStreamWithCRC5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRC5WriteEPKvjPj,@function
_ZN17COutStreamWithCRC5WriteEPKvjPj:    # @_ZN17COutStreamWithCRC5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	%edx, 4(%rsp)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	movq	%r15, %rsi
	callq	*40(%rax)
	movl	%eax, %ebp
	cmpb	$0, 36(%rbx)
	jne	.LBB0_4
	jmp	.LBB0_5
.LBB0_1:
	xorl	%ebp, %ebp
	cmpb	$0, 36(%rbx)
	je	.LBB0_5
.LBB0_4:
	movl	32(%rbx), %edi
	movl	4(%rsp), %edx
	movq	%r15, %rsi
	callq	CrcUpdate
	movl	%eax, 32(%rbx)
.LBB0_5:
	movl	4(%rsp), %eax
	addq	%rax, 24(%rbx)
	testq	%r14, %r14
	je	.LBB0_7
# BB#6:
	movl	%eax, (%r14)
.LBB0_7:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN17COutStreamWithCRC5WriteEPKvjPj, .Lfunc_end0-_ZN17COutStreamWithCRC5WriteEPKvjPj
	.cfi_endproc

	.section	.text._ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv,@function
_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv: # @_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB1_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB1_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB1_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB1_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB1_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB1_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB1_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB1_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB1_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB1_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB1_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB1_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB1_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB1_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB1_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB1_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB1_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv, .Lfunc_end1-_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN17COutStreamWithCRC6AddRefEv,"axG",@progbits,_ZN17COutStreamWithCRC6AddRefEv,comdat
	.weak	_ZN17COutStreamWithCRC6AddRefEv
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRC6AddRefEv,@function
_ZN17COutStreamWithCRC6AddRefEv:        # @_ZN17COutStreamWithCRC6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN17COutStreamWithCRC6AddRefEv, .Lfunc_end2-_ZN17COutStreamWithCRC6AddRefEv
	.cfi_endproc

	.section	.text._ZN17COutStreamWithCRC7ReleaseEv,"axG",@progbits,_ZN17COutStreamWithCRC7ReleaseEv,comdat
	.weak	_ZN17COutStreamWithCRC7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRC7ReleaseEv,@function
_ZN17COutStreamWithCRC7ReleaseEv:       # @_ZN17COutStreamWithCRC7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB3_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB3_2:
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN17COutStreamWithCRC7ReleaseEv, .Lfunc_end3-_ZN17COutStreamWithCRC7ReleaseEv
	.cfi_endproc

	.section	.text._ZN17COutStreamWithCRCD2Ev,"axG",@progbits,_ZN17COutStreamWithCRCD2Ev,comdat
	.weak	_ZN17COutStreamWithCRCD2Ev
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRCD2Ev,@function
_ZN17COutStreamWithCRCD2Ev:             # @_ZN17COutStreamWithCRCD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV17COutStreamWithCRC+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB4_1:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	retq
.Lfunc_end4:
	.size	_ZN17COutStreamWithCRCD2Ev, .Lfunc_end4-_ZN17COutStreamWithCRCD2Ev
	.cfi_endproc

	.section	.text._ZN17COutStreamWithCRCD0Ev,"axG",@progbits,_ZN17COutStreamWithCRCD0Ev,comdat
	.weak	_ZN17COutStreamWithCRCD0Ev
	.p2align	4, 0x90
	.type	_ZN17COutStreamWithCRCD0Ev,@function
_ZN17COutStreamWithCRCD0Ev:             # @_ZN17COutStreamWithCRCD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV17COutStreamWithCRC+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB5_2:                                # %_ZN17COutStreamWithCRCD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN17COutStreamWithCRCD0Ev, .Lfunc_end5-_ZN17COutStreamWithCRCD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV17COutStreamWithCRC,@object # @_ZTV17COutStreamWithCRC
	.section	.rodata,"a",@progbits
	.globl	_ZTV17COutStreamWithCRC
	.p2align	3
_ZTV17COutStreamWithCRC:
	.quad	0
	.quad	_ZTI17COutStreamWithCRC
	.quad	_ZN17COutStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.quad	_ZN17COutStreamWithCRC6AddRefEv
	.quad	_ZN17COutStreamWithCRC7ReleaseEv
	.quad	_ZN17COutStreamWithCRCD2Ev
	.quad	_ZN17COutStreamWithCRCD0Ev
	.quad	_ZN17COutStreamWithCRC5WriteEPKvjPj
	.size	_ZTV17COutStreamWithCRC, 64

	.type	_ZTS17COutStreamWithCRC,@object # @_ZTS17COutStreamWithCRC
	.globl	_ZTS17COutStreamWithCRC
	.p2align	4
_ZTS17COutStreamWithCRC:
	.asciz	"17COutStreamWithCRC"
	.size	_ZTS17COutStreamWithCRC, 20

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI17COutStreamWithCRC,@object # @_ZTI17COutStreamWithCRC
	.section	.rodata,"a",@progbits
	.globl	_ZTI17COutStreamWithCRC
	.p2align	4
_ZTI17COutStreamWithCRC:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17COutStreamWithCRC
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI17COutStreamWithCRC, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
