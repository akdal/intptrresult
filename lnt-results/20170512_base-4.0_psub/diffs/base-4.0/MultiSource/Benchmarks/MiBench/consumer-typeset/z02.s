	.text
	.file	"z02.bc"
	.globl	LexLegalName
	.p2align	4, 0x90
	.type	LexLegalName,@function
LexLegalName:                           # @LexLegalName
	.cfi_startproc
# BB#0:
	movzbl	(%rdi), %eax
	movb	chtbl(%rax), %al
	cmpb	$3, %al
	je	.LBB0_4
# BB#1:
	cmpb	$1, %al
	je	.LBB0_4
# BB#2:
	testb	%al, %al
	jne	.LBB0_3
# BB#6:                                 # %.preheader15.preheader
	incq	%rdi
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader15
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	incq	%rdi
	cmpb	$0, chtbl(%rax)
	je	.LBB0_7
	jmp	.LBB0_8
.LBB0_4:                                # %.preheader.preheader
	incq	%rdi
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	incq	%rdi
	cmpb	$1, chtbl(%rax)
	je	.LBB0_5
.LBB0_8:
	testb	%al, %al
	sete	%al
	movzbl	%al, %eax
	retq
.LBB0_3:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end0:
	.size	LexLegalName, .Lfunc_end0-LexLegalName
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16,1
	.text
	.globl	LexInit
	.p2align	4, 0x90
	.type	LexInit,@function
LexInit:                                # @LexInit
	.cfi_startproc
# BB#0:                                 # %initchtbl.exit.preheader55
	movb	$1, chtbl+95(%rip)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	movups	%xmm0, chtbl+75(%rip)
	movaps	%xmm0, chtbl+64(%rip)
	movups	%xmm0, chtbl+107(%rip)
	movups	%xmm0, chtbl+97(%rip)
	movabsq	$72340172838076673, %rax # imm = 0x101010101010101
	movq	%rax, chtbl+207(%rip)
	movaps	%xmm0, chtbl+192(%rip)
	movups	%xmm0, chtbl+231(%rip)
	movups	%xmm0, chtbl+216(%rip)
	movq	%rax, chtbl+248(%rip)
	movb	$2, chtbl+34(%rip)
	movb	$3, chtbl+92(%rip)
	movb	$4, chtbl+35(%rip)
	movb	$5, chtbl+32(%rip)
	movb	$6, chtbl+12(%rip)
	movb	$7, chtbl+9(%rip)
	movb	$8, chtbl+10(%rip)
	movb	$9, chtbl(%rip)
	movl	$-1, stack_free(%rip)
	retq
.Lfunc_end1:
	.size	LexInit, .Lfunc_end1-LexInit
	.cfi_endproc

	.globl	LexPush
	.p2align	4, 0x90
	.type	LexPush,@function
LexPush:                                # @LexPush
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %ebp
	movl	%esi, %ebx
	movl	%edi, %r15d
	movl	stack_free(%rip), %eax
	cmpl	$9, %eax
	jl	.LBB2_8
# BB#1:
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	movzwl	%r15w, %r14d
	movl	%r14d, %edi
	callq	PosOfFile
	movq	%rax, %rbx
	movl	%r14d, %edi
	cmpl	$1, %ebp
	jne	.LBB2_3
# BB#2:
	callq	FullFileName
	movq	%rax, %r9
	movl	$2, %edi
	movl	$1, %esi
	movl	$.L.str.19, %edx
	jmp	.LBB2_4
.LBB2_3:
	callq	FileName
	movq	%rax, %r9
	movl	$2, %edi
	movl	$2, %esi
	movl	$.L.str.20, %edx
.LBB2_4:
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movslq	stack_free(%rip), %rbx
	testq	%rbx, %rbx
	movq	no_fpos(%rip), %r14
	jle	.LBB2_7
# BB#5:                                 # %.lr.ph.preheader
	imulq	$120, %rbx, %rax
	incq	%rbx
	leaq	lex_stack-72(%rax), %r12
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r12), %edi
	callq	EchoFileSource
	movq	%rax, %rbp
	movl	$2, %edi
	movl	$23, %esi
	movl	$.L.str.21, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	callq	Error
	movq	no_fpos(%rip), %r14
	decq	%rbx
	addq	$-120, %r12
	cmpq	$1, %rbx
	jg	.LBB2_6
.LBB2_7:                                # %._crit_edge
	movl	$2, %edi
	movl	$24, %esi
	movl	$.L.str.22, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movl	stack_free(%rip), %eax
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	20(%rsp), %r12d         # 4-byte Reload
	movl	16(%rsp), %ebx          # 4-byte Reload
.LBB2_8:
	testl	%eax, %eax
	js	.LBB2_10
# BB#9:
	movq	chpt(%rip), %rcx
	movslq	%eax, %rdx
	imulq	$120, %rdx, %rdx
	movq	%rcx, lex_stack(%rdx)
	movq	frst(%rip), %rcx
	movq	%rcx, lex_stack+8(%rdx)
	movq	limit(%rip), %rcx
	movq	%rcx, lex_stack+16(%rdx)
	movq	buf(%rip), %rcx
	movq	%rcx, lex_stack+24(%rdx)
	movl	blksize(%rip), %ecx
	movl	%ecx, lex_stack+32(%rdx)
	movb	last_char(%rip), %cl
	movb	%cl, lex_stack+36(%rdx)
	movq	startline(%rip), %rcx
	movq	%rcx, lex_stack+40(%rdx)
	movzwl	this_file(%rip), %ecx
	movw	%cx, lex_stack+48(%rdx)
	movq	fp(%rip), %rcx
	movq	%rcx, lex_stack+56(%rdx)
	movzwl	ftype(%rip), %ecx
	movw	%cx, lex_stack+72(%rdx)
	movq	next_token(%rip), %rcx
	movq	%rcx, lex_stack+80(%rdx)
	movl	offset(%rip), %ecx
	movl	%ecx, lex_stack+88(%rdx)
	movl	first_line_num(%rip), %ecx
	movl	%ecx, lex_stack+92(%rdx)
	movl	same_file(%rip), %ecx
	movl	%ecx, lex_stack+96(%rdx)
	movq	mem_block(%rip), %rcx
	movq	%rcx, lex_stack+112(%rdx)
	movzwl	file_pos+2(%rip), %ecx
	movw	%cx, lex_stack+66(%rdx)
	movl	file_pos+4(%rip), %ecx
	movl	%ecx, lex_stack+68(%rdx)
.LBB2_10:
	incl	%eax
	movl	%eax, stack_free(%rip)
	movl	$10242, %edi            # imm = 0x2802
	callq	malloc
	movq	%rax, mem_block(%rip)
	testq	%rax, %rax
	jne	.LBB2_12
# BB#11:
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	movl	%ebx, %r13d
	movzwl	%r15w, %ebx
	movl	%ebx, %edi
	callq	PosOfFile
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	FullFileName
	movq	%rax, %rbx
	movl	$2, %edi
	movl	$3, %esi
	movl	$.L.str.23, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	movl	%r13d, %ebx
	movl	12(%rsp), %r13d         # 4-byte Reload
	callq	Error
	movq	mem_block(%rip), %rax
.LBB2_12:
	leaq	2048(%rax), %rcx
	movq	%rcx, chpt(%rip)
	movq	%rcx, buf(%rip)
	movb	$10, last_char(%rip)
	movw	%r15w, this_file(%rip)
	movl	%ebx, offset(%rip)
	movl	%r12d, first_line_num(%rip)
	movl	%r13d, same_file(%rip)
	movw	%bp, ftype(%rip)
	movq	$0, next_token(%rip)
	movb	$0, 2048(%rax)
	testl	%r13d, %r13d
	je	.LBB2_14
# BB#13:
	movq	fp(%rip), %rdi
	callq	ftell
	movslq	stack_free(%rip), %rcx
	imulq	$120, %rcx, %rcx
	movq	%rax, lex_stack-16(%rcx)
	jmp	.LBB2_15
.LBB2_14:
	movq	$0, fp(%rip)
.LBB2_15:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	LexPush, .Lfunc_end2-LexPush
	.cfi_endproc

	.globl	LexPop
	.p2align	4, 0x90
	.type	LexPop,@function
LexPop:                                 # @LexPop
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	stack_free(%rip), %eax
	testl	%eax, %eax
	jg	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.24, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	stack_free(%rip), %eax
.LBB3_2:
	decl	%eax
	movl	%eax, stack_free(%rip)
	cmpl	$0, same_file(%rip)
	movq	fp(%rip), %rdi
	je	.LBB3_4
# BB#3:
	cltq
	imulq	$120, %rax, %rax
	movq	lex_stack+104(%rax), %rsi
	xorl	%edx, %edx
	callq	fseek
	jmp	.LBB3_6
.LBB3_4:
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:
	callq	fclose
.LBB3_6:
	movq	mem_block(%rip), %rdi
	callq	free
	movslq	stack_free(%rip), %rax
	imulq	$120, %rax, %rax
	movq	lex_stack+112(%rax), %rcx
	movq	%rcx, mem_block(%rip)
	movq	lex_stack(%rax), %rcx
	movq	%rcx, chpt(%rip)
	movq	lex_stack+8(%rax), %rcx
	movq	%rcx, frst(%rip)
	movq	lex_stack+16(%rax), %rcx
	movq	%rcx, limit(%rip)
	movq	lex_stack+24(%rax), %rcx
	movq	%rcx, buf(%rip)
	movl	lex_stack+32(%rax), %ecx
	movl	%ecx, blksize(%rip)
	movb	lex_stack+36(%rax), %cl
	movb	%cl, last_char(%rip)
	movq	lex_stack+40(%rax), %rcx
	movq	%rcx, startline(%rip)
	movzwl	lex_stack+48(%rax), %ecx
	movw	%cx, this_file(%rip)
	movq	lex_stack+56(%rax), %rcx
	movq	%rcx, fp(%rip)
	movzwl	lex_stack+72(%rax), %ecx
	movw	%cx, ftype(%rip)
	movq	lex_stack+80(%rax), %rcx
	movq	%rcx, next_token(%rip)
	movl	lex_stack+88(%rax), %ecx
	movl	%ecx, offset(%rip)
	movl	lex_stack+92(%rax), %ecx
	movl	%ecx, first_line_num(%rip)
	movl	lex_stack+96(%rax), %ecx
	movl	%ecx, same_file(%rip)
	movzwl	lex_stack+66(%rax), %ecx
	movw	%cx, file_pos+2(%rip)
	movl	lex_stack+68(%rax), %eax
	movl	%eax, file_pos+4(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	LexPop, .Lfunc_end3-LexPop
	.cfi_endproc

	.globl	LexNextTokenPos
	.p2align	4, 0x90
	.type	LexNextTokenPos,@function
LexNextTokenPos:                        # @LexNextTokenPos
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 16
	movq	next_token(%rip), %r8
	testq	%r8, %r8
	je	.LBB4_2
# BB#1:
	addq	$32, %r8
	movl	$2, %edi
	movl	$4, %esi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movq	fp(%rip), %rdi
	callq	ftell
	subq	limit(%rip), %rax
	addq	chpt(%rip), %rax
	subq	buf(%rip), %rax
	addq	frst(%rip), %rax
	popq	%rcx
	retq
.Lfunc_end4:
	.size	LexNextTokenPos, .Lfunc_end4-LexNextTokenPos
	.cfi_endproc

	.globl	LexGetToken
	.p2align	4, 0x90
	.type	LexGetToken,@function
LexGetToken:                            # @LexGetToken
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	next_token(%rip), %r14
	testq	%r14, %r14
	je	.LBB5_3
# BB#1:
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB5_102
# BB#2:
	movq	%rax, zz_res(%rip)
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, next_token(%rip)
	jmp	.LBB5_175
.LBB5_3:
	movq	chpt(%rip), %r12
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	jmp	.LBB5_41
.LBB5_102:
	xorl	%eax, %eax
	movq	%rax, next_token(%rip)
	jmp	.LBB5_175
.LBB5_5:                                #   in Loop: Header=BB5_41 Depth=1
	incq	%r12
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.24, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB5_41
.LBB5_6:                                #   in Loop: Header=BB5_41 Depth=1
	movzwl	120(%r14), %eax
	leal	-112(%rax), %ecx
	movzwl	%cx, %ecx
	cmpl	$2, %ecx
	jae	.LBB5_11
# BB#7:                                 #   in Loop: Header=BB5_41 Depth=1
	movq	%r15, chpt(%rip)
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 16(%rsp)
	cmpb	$11, 32(%rbx)
	jne	.LBB5_13
# BB#8:                                 #   in Loop: Header=BB5_41 Depth=1
	movzbl	64(%rbx), %ecx
	movl	$123, %eax
	subl	%ecx, %eax
	jne	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_41 Depth=1
	movzbl	65(%rbx), %eax
	negl	%eax
.LBB5_10:                               #   in Loop: Header=BB5_41 Depth=1
	testl	%eax, %eax
	sete	%bpl
	testb	%bpl, %bpl
	je	.LBB5_14
	jmp	.LBB5_20
.LBB5_11:                               #   in Loop: Header=BB5_41 Depth=1
	testw	%ax, %ax
	jne	.LBB5_16
# BB#12:                                #   in Loop: Header=BB5_41 Depth=1
	movzbl	40(%r14), %r8d
	movl	$2, %edi
	jmp	.LBB5_18
.LBB5_13:                               #   in Loop: Header=BB5_41 Depth=1
	xorl	%ebp, %ebp
	testb	%bpl, %bpl
	jne	.LBB5_20
.LBB5_14:                               #   in Loop: Header=BB5_41 Depth=1
	cmpb	$102, 32(%rbx)
	je	.LBB5_20
# BB#15:                                #   in Loop: Header=BB5_41 Depth=1
	addq	$32, %rbx
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$2, %edi
	movl	$9, %esi
	movl	$.L.str.33, %edx
	movl	$2, %ecx
	movl	$.L.str.32, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	16(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_39
.LBB5_16:                               #   in Loop: Header=BB5_41 Depth=1
	movb	40(%r14), %cl
	movzwl	%ax, %edx
	movzbl	%al, %edi
	movzbl	%cl, %r8d
	cmpl	$105, %edx
	jne	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_41 Depth=1
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	jmp	.LBB5_19
.LBB5_18:                               # %.loopexit
                                        #   in Loop: Header=BB5_41 Depth=1
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %r9
.LBB5_19:                               # %.loopexit
                                        #   in Loop: Header=BB5_41 Depth=1
	callq	NewToken
	movq	%rax, %r14
	movq	%r15, %r12
	testq	%r14, %r14
	je	.LBB5_41
	jmp	.LBB5_172
.LBB5_20:                               #   in Loop: Header=BB5_41 Depth=1
	testb	%bpl, %bpl
	je	.LBB5_28
# BB#21:                                #   in Loop: Header=BB5_41 Depth=1
	callq	UnSuppressScope
	movq	16(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movzbl	zz_lengths+102(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_26
# BB#22:                                #   in Loop: Header=BB5_41 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_27
.LBB5_23:                               #   in Loop: Header=BB5_41 Depth=1
	movq	StartSym(%rip), %r9
	movl	$105, %edi
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	callq	NewToken
	movq	%rax, %r14
	movl	$111, %edi
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	NewToken
	movq	%rax, next_token(%rip)
	jmp	.LBB5_25
.LBB5_24:                               #   in Loop: Header=BB5_41 Depth=1
	movq	FilterOutSym(%rip), %r9
	movl	$105, %edi
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	callq	NewToken
	movq	%rax, %r14
.LBB5_25:                               # %.loopexit
                                        #   in Loop: Header=BB5_41 Depth=1
	movq	%r12, startline(%rip)
	testq	%r14, %r14
	je	.LBB5_41
	jmp	.LBB5_172
.LBB5_26:                               #   in Loop: Header=BB5_41 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_27:                               #   in Loop: Header=BB5_41 Depth=1
	movb	$102, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 16(%rsp)
.LBB5_28:                               #   in Loop: Header=BB5_41 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	16(%rsp), %rdi
	callq	Parse
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbx
	testb	%bpl, %bpl
	je	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_41 Depth=1
	callq	SuppressScope
.LBB5_30:                               #   in Loop: Header=BB5_41 Depth=1
	leaq	32(%rbx), %r12
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB5_38
# BB#31:                                #   in Loop: Header=BB5_41 Depth=1
	leaq	64(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	$4294967293, %ecx       # imm = 0xFFFFFFFD
	addq	%rcx, %rax
	testl	%eax, %eax
	js	.LBB5_37
# BB#32:                                #   in Loop: Header=BB5_41 Depth=1
	cltq
	cmpb	$46, (%rbp,%rax)
	jne	.LBB5_37
# BB#33:                                #   in Loop: Header=BB5_41 Depth=1
	cmpb	$108, 1(%rbp,%rax)
	jne	.LBB5_37
# BB#34:                                #   in Loop: Header=BB5_41 Depth=1
	cmpb	$116, 2(%rbp,%rax)
	jne	.LBB5_37
# BB#35:                                #   in Loop: Header=BB5_41 Depth=1
	cmpb	$0, 3(%rbp,%rax)
	jne	.LBB5_37
# BB#36:                                #   in Loop: Header=BB5_41 Depth=1
	movb	$0, (%rbp,%rax)
.LBB5_37:                               # %.thread509
                                        #   in Loop: Header=BB5_41 Depth=1
	movzwl	120(%r14), %eax
	xorl	%r8d, %r8d
	cmpl	$112, %eax
	setne	%r8b
	incl	%r8d
	movl	$.L.str.36, %esi
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	DefineFile
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	movq	%rbx, %rsi
	addq	$33, %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rbx)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	movzwl	%ax, %edi
	xorl	%esi, %esi
	movl	$1, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	LexPush
	callq	LexGetToken
	movq	%rax, %r14
	incl	%r13d
	movq	chpt(%rip), %r12
	testq	%r14, %r14
	je	.LBB5_41
	jmp	.LBB5_172
.LBB5_38:                               #   in Loop: Header=BB5_41 Depth=1
	movl	$2, %edi
	movl	$10, %esi
	movl	$.L.str.34, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbx, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB5_39:                               # %.loopexit
                                        #   in Loop: Header=BB5_41 Depth=1
	xorl	%r14d, %r14d
	movq	%r15, %r12
	testq	%r14, %r14
	je	.LBB5_41
	jmp	.LBB5_172
	.p2align	4, 0x90
.LBB5_40:                               #   in Loop: Header=BB5_41 Depth=1
	incq	%r12
	movq	%r12, chpt(%rip)
	callq	srcnext
	movl	file_pos+4(%rip), %eax
	incl	%eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	%eax, file_pos+4(%rip)
	incl	%r13d
	movq	chpt(%rip), %r12
	leaq	-1(%r12), %rax
	movq	%rax, startline(%rip)
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_41 Depth=1
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	StartSym(%rip), %r9
	movzbl	%al, %r8d
	movl	$102, %edi
	movl	$file_pos, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	NewToken
	movq	%rax, %r14
	addq	$3, %r12
	testq	%r14, %r14
	jne	.LBB5_172
.LBB5_41:                               # %.thread330.outer.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_60 Depth 2
                                        #       Child Loop BB5_70 Depth 3
                                        #         Child Loop BB5_72 Depth 4
                                        #         Child Loop BB5_63 Depth 4
                                        #         Child Loop BB5_80 Depth 4
                                        #         Child Loop BB5_82 Depth 4
                                        #       Child Loop BB5_85 Depth 3
	movq	%r12, %r15
	movl	12(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB5_60
.LBB5_42:                               #   in Loop: Header=BB5_60 Depth=2
	movq	%r15, chpt(%rip)
	callq	srcnext
	movl	file_pos+4(%rip), %eax
	incl	%eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	%eax, file_pos+4(%rip)
	movq	chpt(%rip), %r15
	leaq	-1(%r15), %rax
	movq	%rax, startline(%rip)
	jmp	.LBB5_60
.LBB5_58:                               #   in Loop: Header=BB5_60 Depth=2
	movq	buf(%rip), %rax
	movq	%rax, chpt(%rip)
	movq	%rax, limit(%rip)
	movq	%rax, frst(%rip)
	movl	$0, blksize(%rip)
	movb	$10, last_char(%rip)
	callq	srcnext
	movq	chpt(%rip), %r15
	leaq	-1(%r15), %rax
	movq	%rax, startline(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB5_60
.LBB5_43:                               #   in Loop: Header=BB5_60 Depth=2
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	cmpl	$0, same_file(%rip)
	jne	.LBB5_52
# BB#44:                                #   in Loop: Header=BB5_60 Depth=2
	movq	fp(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB5_47
# BB#45:                                #   in Loop: Header=BB5_60 Depth=2
	callq	fclose
	movq	$0, fp(%rip)
	cmpw	$0, ftype(%rip)
	je	.LBB5_48
# BB#46:                                #   in Loop: Header=BB5_60 Depth=2
	xorl	%ebx, %ebx
	jmp	.LBB5_49
.LBB5_47:                               # %thread-pre-split
                                        #   in Loop: Header=BB5_60 Depth=2
	movw	this_file(%rip), %bx
	testw	%bx, %bx
	jne	.LBB5_50
	jmp	.LBB5_52
.LBB5_48:                               #   in Loop: Header=BB5_60 Depth=2
	movzwl	this_file(%rip), %edi
	callq	NextFile
	movw	%ax, %bx
.LBB5_49:                               # %.sink.split
                                        #   in Loop: Header=BB5_60 Depth=2
	movw	%bx, this_file(%rip)
	testw	%bx, %bx
	je	.LBB5_52
.LBB5_50:                               #   in Loop: Header=BB5_60 Depth=2
	movw	%bx, file_pos+2(%rip)
	movl	$1, file_pos+4(%rip)
	movzwl	%bx, %edi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	OpenFile
	movq	%rax, %rdi
	movq	%rdi, fp(%rip)
	testq	%rdi, %rdi
	jne	.LBB5_53
# BB#51:                                #   in Loop: Header=BB5_60 Depth=2
	movzwl	this_file(%rip), %edi
	callq	FullFileName
	movq	%rax, %r9
	xorl	%ebx, %ebx
	movl	$2, %edi
	movl	$7, %esi
	movl	$.L.str.28, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	cmpw	$0, ftype(%rip)
	jne	.LBB5_49
	jmp	.LBB5_48
.LBB5_52:                               #   in Loop: Header=BB5_60 Depth=2
	movq	fp(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB5_55
.LBB5_53:                               # %.thread
                                        #   in Loop: Header=BB5_60 Depth=2
	movslq	offset(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB5_58
# BB#54:                                #   in Loop: Header=BB5_60 Depth=2
	xorl	%edx, %edx
	callq	fseek
	movl	$0, offset(%rip)
	movl	first_line_num(%rip), %eax
	andl	%ebp, %eax
	movl	file_pos+4(%rip), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, file_pos+4(%rip)
	jmp	.LBB5_58
.LBB5_55:                               #   in Loop: Header=BB5_60 Depth=2
	movswl	ftype(%rip), %eax
	cmpl	$10, %eax
	ja	.LBB5_5
# BB#56:                                #   in Loop: Header=BB5_60 Depth=2
	jmpq	*.LJTI5_2(,%rax,8)
.LBB5_57:                               #   in Loop: Header=BB5_60 Depth=2
	callq	LexPop
	xorl	%r14d, %r14d
	movq	chpt(%rip), %r15
	jmp	.LBB5_60
.LBB5_59:                               # %.thread330.outer.outer.loopexit
                                        #   in Loop: Header=BB5_60 Depth=2
	decq	%r15
	.p2align	4, 0x90
.LBB5_60:                               # %.thread330.outer.outer
                                        #   Parent Loop BB5_41 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_70 Depth 3
                                        #         Child Loop BB5_72 Depth 4
                                        #         Child Loop BB5_63 Depth 4
                                        #         Child Loop BB5_80 Depth 4
                                        #         Child Loop BB5_82 Depth 4
                                        #       Child Loop BB5_85 Depth 3
	movl	%r14d, %eax
	jmp	.LBB5_70
.LBB5_61:                               # %.loopexit342.loopexit
                                        #   in Loop: Header=BB5_70 Depth=3
	addq	%r13, %r15
	movl	8(%rsp), %r13d          # 4-byte Reload
	testq	%r14, %r14
	jne	.LBB5_65
	jmp	.LBB5_131
.LBB5_68:                               #   in Loop: Header=BB5_70 Depth=3
	addl	$8, %r14d
.LBB5_69:                               # %.thread330.outer
                                        #   in Loop: Header=BB5_70 Depth=3
	incq	%r12
	movl	%r14d, %eax
	movq	%r12, %r15
	jmp	.LBB5_70
.LBB5_62:                               #   in Loop: Header=BB5_70 Depth=3
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	startline(%rip), %eax
	movl	%r12d, %ecx
	subl	%eax, %ecx
	shll	$20, %ecx
	movl	file_pos+4(%rip), %eax
	andl	%ebp, %eax
	orl	%ecx, %eax
	movl	%eax, file_pos+4(%rip)
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB5_63:                               #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        #       Parent Loop BB5_70 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	1(%r15), %eax
	incq	%r15
	cmpb	$1, chtbl(%rax)
	je	.LBB5_63
# BB#64:                                #   in Loop: Header=BB5_70 Depth=3
	movl	%r15d, %esi
	subl	%r12d, %esi
	movq	%r12, %rdi
	callq	SearchSym
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_131
.LBB5_65:                               #   in Loop: Header=BB5_70 Depth=3
	cmpb	$-114, 32(%r14)
	jne	.LBB5_6
# BB#66:                                #   in Loop: Header=BB5_70 Depth=3
	movzwl	41(%r14), %eax
	testb	$4, %ah
	jne	.LBB5_147
# BB#67:                                #   in Loop: Header=BB5_70 Depth=3
	movq	56(%r14), %rdi
	movl	$file_pos, %esi
	callq	CopyTokenList
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	jne	.LBB5_149
	.p2align	4, 0x90
.LBB5_70:                               # %.thread330.outer
                                        #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_72 Depth 4
                                        #         Child Loop BB5_63 Depth 4
                                        #         Child Loop BB5_80 Depth 4
                                        #         Child Loop BB5_82 Depth 4
	movl	%eax, %r14d
	movq	%r15, %r12
	movl	$1048575, %ebp          # imm = 0xFFFFF
	jmp	.LBB5_72
	.p2align	4, 0x90
.LBB5_71:                               #   in Loop: Header=BB5_72 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.24, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.40, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %r12
.LBB5_72:                               # %.thread330
                                        #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        #       Parent Loop BB5_70 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	1(%r12), %rbx
	movzbl	(%r12), %eax
	movzbl	chtbl(%rax), %eax
	cmpq	$9, %rax
	ja	.LBB5_71
# BB#73:                                # %.thread330
                                        #   in Loop: Header=BB5_72 Depth=4
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_74:                               #   in Loop: Header=BB5_72 Depth=4
	movzwl	ftype(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB5_77
# BB#75:                                #   in Loop: Header=BB5_72 Depth=4
	movzbl	1(%r12), %eax
	movl	%eax, %ecx
	addb	$-97, %cl
	cmpb	$25, %cl
	ja	.LBB5_77
# BB#76:                                #   in Loop: Header=BB5_72 Depth=4
	cmpb	$123, 2(%r12)
	je	.LBB5_4
.LBB5_77:                               #   in Loop: Header=BB5_72 Depth=4
	movl	startline(%rip), %eax
	movl	%r12d, %ecx
	subl	%eax, %ecx
	shll	$20, %ecx
	movl	file_pos+4(%rip), %eax
	andl	%ebp, %eax
	orl	%ecx, %eax
	movl	%eax, file_pos+4(%rip)
	movzbl	(%r12), %r9d
	movl	$2, %edi
	movl	$6, %esi
	movl	$.L.str.27, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %r12
	jmp	.LBB5_72
.LBB5_78:                               #   in Loop: Header=BB5_70 Depth=3
	incl	%r14d
	jmp	.LBB5_69
.LBB5_79:                               #   in Loop: Header=BB5_70 Depth=3
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movl	startline(%rip), %eax
	movl	%r12d, %ecx
	subl	%eax, %ecx
	shll	$20, %ecx
	movl	file_pos+4(%rip), %eax
	andl	%ebp, %eax
	orl	%ecx, %eax
	movl	%eax, file_pos+4(%rip)
	leaq	1(%r12), %r15
	.p2align	4, 0x90
.LBB5_80:                               #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        #       Parent Loop BB5_70 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r15), %eax
	incq	%r15
	cmpb	$0, chtbl(%rax)
	je	.LBB5_80
# BB#81:                                #   in Loop: Header=BB5_70 Depth=3
	movl	$4294967293, %eax       # imm = 0xFFFFFFFD
	leaq	2(%rax), %rbx
	subq	%r12, %rbx
	addq	%r15, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_82:                               #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        #       Parent Loop BB5_70 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rbx,%r13), %ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	SearchSym
	movq	%rax, %r14
	decq	%r13
	cmpl	$2, %ebp
	jl	.LBB5_61
# BB#83:                                #   in Loop: Header=BB5_82 Depth=4
	testq	%r14, %r14
	je	.LBB5_82
	jmp	.LBB5_61
.LBB5_84:                               # %.preheader341.loopexit
                                        #   in Loop: Header=BB5_60 Depth=2
	incq	%r12
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB5_85:                               # %.preheader341
                                        #   Parent Loop BB5_41 Depth=1
                                        #     Parent Loop BB5_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15), %eax
	incq	%r15
	testb	%al, %al
	je	.LBB5_59
# BB#86:                                # %.preheader341
                                        #   in Loop: Header=BB5_85 Depth=3
	cmpb	$10, %al
	jne	.LBB5_85
	jmp	.LBB5_42
.LBB5_87:
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movl	startline(%rip), %eax
	movl	%r12d, %ecx
	subl	%eax, %ecx
	shll	$20, %ecx
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	file_pos+4(%rip), %eax
	orl	%ecx, %eax
	movl	%eax, file_pos+4(%rip)
	leaq	1(%r12), %r14
	movq	%r14, %rbp
	movq	%r14, %rbx
	jmp	.LBB5_89
	.p2align	4, 0x90
.LBB5_88:                               #   in Loop: Header=BB5_89 Depth=1
	movl	$2, %edi
	movl	$14, %esi
	movl	$.L.str.39, %edx
	xorl	%ecx, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
.LBB5_89:                               # %.thread325
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r15
	movq	%rbp, %r13
	leaq	1(%r15), %rbx
	movzbl	(%r15), %eax
	leaq	1(%r13), %rbp
	movb	%al, (%r13)
	movzbl	chtbl(%rax), %eax
	cmpq	$9, %rax
	ja	.LBB5_88
# BB#90:                                # %.thread325
                                        #   in Loop: Header=BB5_89 Depth=1
	jmpq	*.LJTI5_1(,%rax,8)
.LBB5_91:                               #   in Loop: Header=BB5_89 Depth=1
	movzbl	(%rbx), %eax
	movzbl	chtbl(%rax), %ecx
	andb	$-2, %cl
	cmpb	$8, %cl
	je	.LBB5_105
# BB#92:                                #   in Loop: Header=BB5_89 Depth=1
	movl	%eax, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB5_100
# BB#93:                                # %.preheader
                                        #   in Loop: Header=BB5_89 Depth=1
	leaq	2(%r15), %rbx
	addl	$-48, %eax
	movzbl	2(%r15), %ecx
	cmpl	$48, %ecx
	jb	.LBB5_98
# BB#94:                                #   in Loop: Header=BB5_89 Depth=1
	cmpb	$56, %cl
	jae	.LBB5_98
# BB#95:                                # %.preheader.1
                                        #   in Loop: Header=BB5_89 Depth=1
	leaq	3(%r15), %rbx
	leal	-48(%rcx,%rax,8), %eax
	movzbl	3(%r15), %ecx
	cmpl	$48, %ecx
	jb	.LBB5_98
# BB#96:                                #   in Loop: Header=BB5_89 Depth=1
	cmpb	$55, %cl
	ja	.LBB5_98
# BB#97:                                # %.preheader.2
                                        #   in Loop: Header=BB5_89 Depth=1
	addq	$4, %r15
	leal	-48(%rcx,%rax,8), %eax
	movq	%r15, %rbx
.LBB5_98:                               # %.critedge
                                        #   in Loop: Header=BB5_89 Depth=1
	testl	%eax, %eax
	je	.LBB5_101
# BB#99:                                #   in Loop: Header=BB5_89 Depth=1
	movb	%al, (%r13)
	jmp	.LBB5_89
.LBB5_100:                              #   in Loop: Header=BB5_89 Depth=1
	addq	$2, %r15
	movb	%al, (%r13)
	movq	%r15, %rbx
	jmp	.LBB5_89
.LBB5_101:                              #   in Loop: Header=BB5_89 Depth=1
	movl	$2, %edi
	movl	$13, %esi
	movl	$.L.str.38, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	%r13, %rbp
	jmp	.LBB5_89
.LBB5_103:
	movl	$2, %edi
	movl	$11, %esi
	movl	$.L.str.37, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	%r13, %rbx
	movq	%r14, %rbp
	subq	%r14, %rbx
	movq	%rbx, %rdi
	shlq	$32, %rdi
	movabsq	$292057776128, %rax     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_109
# BB#104:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r14
	jmp	.LBB5_116
.LBB5_105:
	movl	$2, %edi
	movl	$12, %esi
	movl	$.L.str.37, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	%r13, %r15
	movq	%r14, %rbp
	subq	%r14, %r15
	movq	%r15, %rdi
	shlq	$32, %rdi
	movabsq	$292057776128, %rax     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_111
# BB#106:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r14
	jmp	.LBB5_134
.LBB5_107:
	movq	%r13, %r15
	movq	%r14, %rbp
	subq	%r14, %r15
	movq	%r15, %rdi
	shlq	$32, %rdi
	movabsq	$292057776128, %rax     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_113
# BB#108:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r14
	jmp	.LBB5_155
.LBB5_109:
	movq	zz_free(,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB5_115
# BB#110:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_116
.LBB5_111:
	movq	zz_free(,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB5_133
# BB#112:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_134
.LBB5_113:
	movq	zz_free(,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB5_154
# BB#114:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_155
.LBB5_115:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_116:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r14)
	movb	$12, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r14)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r14)
	testq	%rbx, %rbx
	jle	.LBB5_127
# BB#117:                               # %.lr.ph412.preheader
	cmpq	$32, %rbx
	jb	.LBB5_121
# BB#118:                               # %min.iters.checked678
	movq	%rbx, %rax
	andq	$-32, %rax
	je	.LBB5_121
# BB#119:                               # %vector.memcheck692
	leaq	64(%r14), %rcx
	cmpq	%r13, %rcx
	jae	.LBB5_151
# BB#120:                               # %vector.memcheck692
	leaq	64(%r14,%rbx), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB5_151
.LBB5_121:
	xorl	%eax, %eax
.LBB5_122:                              # %.lr.ph412.preheader766
	movl	%r13d, %ecx
	subl	%eax, %ecx
	subl	%ebp, %ecx
	decq	%r13
	subq	%rax, %r13
	subq	%rbp, %r13
	andq	$7, %rcx
	je	.LBB5_125
# BB#123:                               # %.lr.ph412.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_124:                              # %.lr.ph412.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %edx
	movb	%dl, 64(%r14,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB5_124
.LBB5_125:                              # %.lr.ph412.prol.loopexit
	cmpq	$7, %r13
	jb	.LBB5_126
# BB#128:                               # %.lr.ph412.preheader766.new
	movl	8(%rsp), %r13d          # 4-byte Reload
	.p2align	4, 0x90
.LBB5_129:                              # %.lr.ph412
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, 64(%r14,%rax)
	movzbl	2(%r12,%rax), %ecx
	movb	%cl, 65(%r14,%rax)
	movzbl	3(%r12,%rax), %ecx
	movb	%cl, 66(%r14,%rax)
	movzbl	4(%r12,%rax), %ecx
	movb	%cl, 67(%r14,%rax)
	movzbl	5(%r12,%rax), %ecx
	movb	%cl, 68(%r14,%rax)
	movzbl	6(%r12,%rax), %ecx
	movb	%cl, 69(%r14,%rax)
	movzbl	7(%r12,%rax), %ecx
	movb	%cl, 70(%r14,%rax)
	movzbl	8(%r12,%rax), %ecx
	movb	%cl, 71(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rbx
	jne	.LBB5_129
# BB#130:                               # %._crit_edge413
	leaq	64(%r14,%rbx), %rax
	jmp	.LBB5_170
.LBB5_127:
	xorl	%ebx, %ebx
.LBB5_126:
	movl	8(%rsp), %r13d          # 4-byte Reload
	leaq	64(%r14,%rbx), %rax
	jmp	.LBB5_170
.LBB5_131:
	movq	%r15, %rbp
	subq	%r12, %rbp
	movq	%rbp, %rdi
	shlq	$32, %rdi
	movabsq	$292057776128, %rax     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_179
# BB#132:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r14
	jmp	.LBB5_199
.LBB5_133:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_134:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r14)
	movb	$12, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r14)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r14)
	testq	%r15, %r15
	jle	.LBB5_166
# BB#135:                               # %.lr.ph422.preheader
	cmpq	$32, %r15
	jb	.LBB5_139
# BB#136:                               # %min.iters.checked
	movq	%r15, %rax
	andq	$-32, %rax
	je	.LBB5_139
# BB#137:                               # %vector.memcheck
	leaq	64(%r14), %rcx
	cmpq	%r13, %rcx
	jae	.LBB5_176
# BB#138:                               # %vector.memcheck
	leaq	64(%r14,%r15), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB5_176
.LBB5_139:
	xorl	%eax, %eax
.LBB5_140:                              # %.lr.ph422.preheader768
	movl	%r13d, %ecx
	subl	%eax, %ecx
	subl	%ebp, %ecx
	decq	%r13
	subq	%rax, %r13
	subq	%rbp, %r13
	andq	$7, %rcx
	je	.LBB5_143
# BB#141:                               # %.lr.ph422.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_142:                              # %.lr.ph422.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %edx
	movb	%dl, 64(%r14,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB5_142
.LBB5_143:                              # %.lr.ph422.prol.loopexit
	cmpq	$7, %r13
	jae	.LBB5_145
# BB#144:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB5_169
.LBB5_145:                              # %.lr.ph422.preheader768.new
	movl	8(%rsp), %r13d          # 4-byte Reload
	.p2align	4, 0x90
.LBB5_146:                              # %.lr.ph422
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, 64(%r14,%rax)
	movzbl	2(%r12,%rax), %ecx
	movb	%cl, 65(%r14,%rax)
	movzbl	3(%r12,%rax), %ecx
	movb	%cl, 66(%r14,%rax)
	movzbl	4(%r12,%rax), %ecx
	movb	%cl, 67(%r14,%rax)
	movzbl	5(%r12,%rax), %ecx
	movb	%cl, 68(%r14,%rax)
	movzbl	6(%r12,%rax), %ecx
	movb	%cl, 69(%r14,%rax)
	movzbl	7(%r12,%rax), %ecx
	movb	%cl, 70(%r14,%rax)
	movzbl	8(%r12,%rax), %ecx
	movb	%cl, 71(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB5_146
	jmp	.LBB5_169
.LBB5_147:
	movl	$2, %edi
	movl	$8, %esi
	movl	$.L.str.31, %edx
	movl	$2, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	%r15, %rbp
	subq	%r12, %rbp
	movq	%rbp, %rdi
	shlq	$32, %rdi
	movabsq	$292057776128, %rax     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_181
# BB#148:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r14
	jmp	.LBB5_208
.LBB5_149:
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB5_186
# BB#150:
	movq	%rax, zz_res(%rip)
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	jmp	.LBB5_187
.LBB5_151:                              # %vector.body674.preheader
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_188
# BB#152:                               # %vector.body674.prol.preheader
	negq	%rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_153:                              # %vector.body674.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	addq	$32, %rcx
	incq	%rsi
	jne	.LBB5_153
	jmp	.LBB5_189
.LBB5_154:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_155:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r14)
	movb	$12, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r14)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r14)
	testq	%r15, %r15
	jle	.LBB5_166
# BB#156:                               # %.lr.ph417.preheader
	cmpq	$32, %r15
	jb	.LBB5_160
# BB#157:                               # %min.iters.checked648
	movq	%r15, %rax
	andq	$-32, %rax
	je	.LBB5_160
# BB#158:                               # %vector.memcheck662
	leaq	64(%r14), %rcx
	cmpq	%r13, %rcx
	jae	.LBB5_183
# BB#159:                               # %vector.memcheck662
	leaq	64(%r14,%r15), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB5_183
.LBB5_160:
	xorl	%eax, %eax
.LBB5_161:                              # %.lr.ph417.preheader767
	movl	%r13d, %ecx
	subl	%eax, %ecx
	subl	%ebp, %ecx
	decq	%r13
	subq	%rax, %r13
	subq	%rbp, %r13
	andq	$7, %rcx
	je	.LBB5_164
# BB#162:                               # %.lr.ph417.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_163:                              # %.lr.ph417.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %edx
	movb	%dl, 64(%r14,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB5_163
.LBB5_164:                              # %.lr.ph417.prol.loopexit
	cmpq	$7, %r13
	jae	.LBB5_167
# BB#165:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB5_169
.LBB5_166:
	xorl	%r15d, %r15d
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB5_169
.LBB5_167:                              # %.lr.ph417.preheader767.new
	movl	8(%rsp), %r13d          # 4-byte Reload
	.p2align	4, 0x90
.LBB5_168:                              # %.lr.ph417
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, 64(%r14,%rax)
	movzbl	2(%r12,%rax), %ecx
	movb	%cl, 65(%r14,%rax)
	movzbl	3(%r12,%rax), %ecx
	movb	%cl, 66(%r14,%rax)
	movzbl	4(%r12,%rax), %ecx
	movb	%cl, 67(%r14,%rax)
	movzbl	5(%r12,%rax), %ecx
	movb	%cl, 68(%r14,%rax)
	movzbl	6(%r12,%rax), %ecx
	movb	%cl, 69(%r14,%rax)
	movzbl	7(%r12,%rax), %ecx
	movb	%cl, 70(%r14,%rax)
	movzbl	8(%r12,%rax), %ecx
	movb	%cl, 71(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB5_168
.LBB5_169:                              # %._crit_edge418
	leaq	64(%r14,%r15), %rax
	movq	%rbx, %r15
.LBB5_170:                              # %.thread334.sink.split
	movb	$0, (%rax)
.LBB5_171:                              # %.thread334
	movq	%r15, %r12
.LBB5_172:                              # %.thread334
	movq	%r12, %rax
	subq	startline(%rip), %rax
	cmpq	$2048, %rax             # imm = 0x800
	jl	.LBB5_174
# BB#173:
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	file_pos+4(%rip), %eax
	orl	$1048576, %eax          # imm = 0x100000
	movl	%eax, file_pos+4(%rip)
	movl	$2, %edi
	movl	$15, %esi
	movl	$.L.str.41, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
.LBB5_174:
	movq	%r12, chpt(%rip)
	movb	%r13b, 42(%r14)
	movl	12(%rsp), %eax          # 4-byte Reload
	movb	%al, 41(%r14)
.LBB5_175:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_176:                              # %vector.body.preheader
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_193
# BB#177:                               # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ecx, %ecx
.LBB5_178:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	addq	$32, %rcx
	incq	%rsi
	jne	.LBB5_178
	jmp	.LBB5_194
.LBB5_179:
	movq	zz_free(,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB5_198
# BB#180:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_199
.LBB5_181:
	movq	zz_free(,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB5_207
# BB#182:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_208
.LBB5_183:                              # %vector.body644.preheader
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_216
# BB#184:                               # %vector.body644.prol.preheader
	negq	%rsi
	xorl	%ecx, %ecx
.LBB5_185:                              # %vector.body644.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	addq	$32, %rcx
	incq	%rsi
	jne	.LBB5_185
	jmp	.LBB5_217
.LBB5_186:
	xorl	%eax, %eax
.LBB5_187:                              # %.thread334
	movq	%rax, next_token(%rip)
	jmp	.LBB5_171
.LBB5_188:
	xorl	%ecx, %ecx
.LBB5_189:                              # %vector.body674.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_191
	.p2align	4, 0x90
.LBB5_190:                              # %vector.body674
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	movups	33(%r12,%rcx), %xmm0
	movups	49(%r12,%rcx), %xmm1
	movups	%xmm0, 96(%r14,%rcx)
	movups	%xmm1, 112(%r14,%rcx)
	movups	65(%r12,%rcx), %xmm0
	movups	81(%r12,%rcx), %xmm1
	movups	%xmm0, 128(%r14,%rcx)
	movups	%xmm1, 144(%r14,%rcx)
	movups	97(%r12,%rcx), %xmm0
	movups	113(%r12,%rcx), %xmm1
	movups	%xmm0, 160(%r14,%rcx)
	movups	%xmm1, 176(%r14,%rcx)
	subq	$-128, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_190
.LBB5_191:                              # %middle.block675
	cmpq	%rax, %rbx
	jne	.LBB5_122
	jmp	.LBB5_126
.LBB5_193:
	xorl	%ecx, %ecx
.LBB5_194:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_196
.LBB5_195:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	movups	33(%r12,%rcx), %xmm0
	movups	49(%r12,%rcx), %xmm1
	movups	%xmm0, 96(%r14,%rcx)
	movups	%xmm1, 112(%r14,%rcx)
	movups	65(%r12,%rcx), %xmm0
	movups	81(%r12,%rcx), %xmm1
	movups	%xmm0, 128(%r14,%rcx)
	movups	%xmm1, 144(%r14,%rcx)
	movups	97(%r12,%rcx), %xmm0
	movups	113(%r12,%rcx), %xmm1
	movups	%xmm0, 160(%r14,%rcx)
	movups	%xmm1, 176(%r14,%rcx)
	subq	$-128, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_195
.LBB5_196:                              # %middle.block
	cmpq	%rax, %r15
	jne	.LBB5_140
# BB#197:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB5_169
.LBB5_198:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_199:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r14)
	movb	$11, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r14)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r14)
	testq	%rbp, %rbp
	jle	.LBB5_202
# BB#200:                               # %.lr.ph.preheader
	cmpq	$32, %rbp
	jae	.LBB5_203
# BB#201:
	xorl	%eax, %eax
	jmp	.LBB5_233
.LBB5_203:                              # %min.iters.checked738
	movq	%rbp, %rax
	andq	$-32, %rax
	je	.LBB5_221
# BB#204:                               # %vector.memcheck752
	leaq	64(%r14), %rcx
	cmpq	%r15, %rcx
	jae	.LBB5_223
# BB#205:                               # %vector.memcheck752
	leaq	64(%r14,%rbp), %rcx
	cmpq	%rcx, %r12
	jae	.LBB5_223
# BB#206:
	xorl	%eax, %eax
	jmp	.LBB5_233
.LBB5_207:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_208:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r14)
	movb	$11, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r14)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r14)
	testq	%rbp, %rbp
	jle	.LBB5_202
# BB#209:                               # %.lr.ph407.preheader
	cmpq	$32, %rbp
	jae	.LBB5_212
# BB#210:
	xorl	%eax, %eax
	jmp	.LBB5_243
.LBB5_202:
	xorl	%ebp, %ebp
	leaq	64(%r14,%rbp), %rax
	jmp	.LBB5_170
.LBB5_212:                              # %min.iters.checked708
	movq	%rbp, %rax
	andq	$-32, %rax
	je	.LBB5_222
# BB#213:                               # %vector.memcheck722
	leaq	64(%r14), %rcx
	cmpq	%r15, %rcx
	jae	.LBB5_226
# BB#214:                               # %vector.memcheck722
	leaq	64(%r14,%rbp), %rcx
	cmpq	%rcx, %r12
	jae	.LBB5_226
# BB#215:
	xorl	%eax, %eax
	jmp	.LBB5_243
.LBB5_216:
	xorl	%ecx, %ecx
.LBB5_217:                              # %vector.body644.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_219
.LBB5_218:                              # %vector.body644
                                        # =>This Inner Loop Header: Depth=1
	movups	1(%r12,%rcx), %xmm0
	movups	17(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	movups	33(%r12,%rcx), %xmm0
	movups	49(%r12,%rcx), %xmm1
	movups	%xmm0, 96(%r14,%rcx)
	movups	%xmm1, 112(%r14,%rcx)
	movups	65(%r12,%rcx), %xmm0
	movups	81(%r12,%rcx), %xmm1
	movups	%xmm0, 128(%r14,%rcx)
	movups	%xmm1, 144(%r14,%rcx)
	movups	97(%r12,%rcx), %xmm0
	movups	113(%r12,%rcx), %xmm1
	movups	%xmm0, 160(%r14,%rcx)
	movups	%xmm1, 176(%r14,%rcx)
	subq	$-128, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_218
.LBB5_219:                              # %middle.block645
	cmpq	%rax, %r15
	jne	.LBB5_161
# BB#220:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB5_169
.LBB5_221:
	xorl	%eax, %eax
	jmp	.LBB5_233
.LBB5_222:
	xorl	%eax, %eax
	jmp	.LBB5_243
.LBB5_223:                              # %vector.body734.preheader
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_229
# BB#224:                               # %vector.body734.prol.preheader
	negq	%rsi
	xorl	%ecx, %ecx
.LBB5_225:                              # %vector.body734.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rcx), %xmm0
	movups	16(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	addq	$32, %rcx
	incq	%rsi
	jne	.LBB5_225
	jmp	.LBB5_230
.LBB5_226:                              # %vector.body704.preheader
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_239
# BB#227:                               # %vector.body704.prol.preheader
	negq	%rsi
	xorl	%ecx, %ecx
.LBB5_228:                              # %vector.body704.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rcx), %xmm0
	movups	16(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	addq	$32, %rcx
	incq	%rsi
	jne	.LBB5_228
	jmp	.LBB5_240
.LBB5_229:
	xorl	%ecx, %ecx
.LBB5_230:                              # %vector.body734.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_232
.LBB5_231:                              # %vector.body734
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rcx), %xmm0
	movups	16(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	movups	32(%r12,%rcx), %xmm0
	movups	48(%r12,%rcx), %xmm1
	movups	%xmm0, 96(%r14,%rcx)
	movups	%xmm1, 112(%r14,%rcx)
	movups	64(%r12,%rcx), %xmm0
	movups	80(%r12,%rcx), %xmm1
	movups	%xmm0, 128(%r14,%rcx)
	movups	%xmm1, 144(%r14,%rcx)
	movups	96(%r12,%rcx), %xmm0
	movups	112(%r12,%rcx), %xmm1
	movups	%xmm0, 160(%r14,%rcx)
	movups	%xmm1, 176(%r14,%rcx)
	subq	$-128, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_231
.LBB5_232:                              # %middle.block735
	cmpq	%rax, %rbp
	je	.LBB5_238
.LBB5_233:                              # %.lr.ph.preheader764
	movl	%r15d, %ecx
	subl	%eax, %ecx
	subl	%r12d, %ecx
	leaq	-1(%r15), %rdx
	subq	%rax, %rdx
	subq	%r12, %rdx
	andq	$7, %rcx
	je	.LBB5_236
# BB#234:                               # %.lr.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_235:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rax), %ebx
	movb	%bl, 64(%r14,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB5_235
.LBB5_236:                              # %.lr.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_238
.LBB5_237:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rax), %ecx
	movb	%cl, 64(%r14,%rax)
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, 65(%r14,%rax)
	movzbl	2(%r12,%rax), %ecx
	movb	%cl, 66(%r14,%rax)
	movzbl	3(%r12,%rax), %ecx
	movb	%cl, 67(%r14,%rax)
	movzbl	4(%r12,%rax), %ecx
	movb	%cl, 68(%r14,%rax)
	movzbl	5(%r12,%rax), %ecx
	movb	%cl, 69(%r14,%rax)
	movzbl	6(%r12,%rax), %ecx
	movb	%cl, 70(%r14,%rax)
	movzbl	7(%r12,%rax), %ecx
	movb	%cl, 71(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rbp
	jne	.LBB5_237
.LBB5_238:                              # %._crit_edge
	leaq	64(%r14,%rbp), %rax
	jmp	.LBB5_170
.LBB5_239:
	xorl	%ecx, %ecx
.LBB5_240:                              # %vector.body704.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_242
.LBB5_241:                              # %vector.body704
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rcx), %xmm0
	movups	16(%r12,%rcx), %xmm1
	movups	%xmm0, 64(%r14,%rcx)
	movups	%xmm1, 80(%r14,%rcx)
	movups	32(%r12,%rcx), %xmm0
	movups	48(%r12,%rcx), %xmm1
	movups	%xmm0, 96(%r14,%rcx)
	movups	%xmm1, 112(%r14,%rcx)
	movups	64(%r12,%rcx), %xmm0
	movups	80(%r12,%rcx), %xmm1
	movups	%xmm0, 128(%r14,%rcx)
	movups	%xmm1, 144(%r14,%rcx)
	movups	96(%r12,%rcx), %xmm0
	movups	112(%r12,%rcx), %xmm1
	movups	%xmm0, 160(%r14,%rcx)
	movups	%xmm1, 176(%r14,%rcx)
	subq	$-128, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_241
.LBB5_242:                              # %middle.block705
	cmpq	%rax, %rbp
	je	.LBB5_248
.LBB5_243:                              # %.lr.ph407.preheader765
	movl	%r15d, %ecx
	subl	%eax, %ecx
	subl	%r12d, %ecx
	leaq	-1(%r15), %rdx
	subq	%rax, %rdx
	subq	%r12, %rdx
	andq	$7, %rcx
	je	.LBB5_246
# BB#244:                               # %.lr.ph407.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB5_245:                              # %.lr.ph407.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rax), %ebx
	movb	%bl, 64(%r14,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB5_245
.LBB5_246:                              # %.lr.ph407.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_248
.LBB5_247:                              # %.lr.ph407
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rax), %ecx
	movb	%cl, 64(%r14,%rax)
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, 65(%r14,%rax)
	movzbl	2(%r12,%rax), %ecx
	movb	%cl, 66(%r14,%rax)
	movzbl	3(%r12,%rax), %ecx
	movb	%cl, 67(%r14,%rax)
	movzbl	4(%r12,%rax), %ecx
	movb	%cl, 68(%r14,%rax)
	movzbl	5(%r12,%rax), %ecx
	movb	%cl, 69(%r14,%rax)
	movzbl	6(%r12,%rax), %ecx
	movb	%cl, 70(%r14,%rax)
	movzbl	7(%r12,%rax), %ecx
	movb	%cl, 71(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rbp
	jne	.LBB5_247
.LBB5_248:                              # %._crit_edge408
	leaq	64(%r14,%rbp), %rax
	jmp	.LBB5_170
.Lfunc_end5:
	.size	LexGetToken, .Lfunc_end5-LexGetToken
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_79
	.quad	.LBB5_62
	.quad	.LBB5_87
	.quad	.LBB5_74
	.quad	.LBB5_84
	.quad	.LBB5_78
	.quad	.LBB5_78
	.quad	.LBB5_68
	.quad	.LBB5_40
	.quad	.LBB5_43
.LJTI5_1:
	.quad	.LBB5_89
	.quad	.LBB5_89
	.quad	.LBB5_107
	.quad	.LBB5_91
	.quad	.LBB5_89
	.quad	.LBB5_89
	.quad	.LBB5_89
	.quad	.LBB5_89
	.quad	.LBB5_103
	.quad	.LBB5_103
.LJTI5_2:
	.quad	.LBB5_23
	.quad	.LBB5_57
	.quad	.LBB5_5
	.quad	.LBB5_23
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_24

	.text
	.p2align	4, 0x90
	.type	srcnext,@function
srcnext:                                # @srcnext
	.cfi_startproc
# BB#0:
	movq	chpt(%rip), %rsi
	movq	limit(%rip), %rcx
	cmpl	$0, blksize(%rip)
	je	.LBB6_5
# BB#1:
	cmpq	%rcx, %rsi
	jae	.LBB6_5
# BB#2:
	movq	buf(%rip), %rdx
	incq	%rcx
	incq	%rdx
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	decq	%rcx
	decq	%rdx
	cmpb	$10, %al
	jne	.LBB6_3
# BB#4:
	movq	%rdx, frst(%rip)
	movq	%rcx, limit(%rip)
	movl	$0, blksize(%rip)
.LBB6_5:                                # %._crit_edge
	cmpq	%rcx, %rsi
	jb	.LBB6_16
# BB#6:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	jbe	.LBB6_8
# BB#7:
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	file_pos+4(%rip), %eax
	orl	$1048576, %eax          # imm = 0x100000
	movl	%eax, file_pos+4(%rip)
	movl	$2, %edi
	movl	$5, %esi
	movl	$.L.str.41, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
.LBB6_8:
	movq	frst(%rip), %rax
	movq	%rax, chpt(%rip)
	movq	buf(%rip), %rdi
	movq	fp(%rip), %rcx
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	callq	fread
	movl	%eax, blksize(%rip)
	testl	%eax, %eax
	jle	.LBB6_9
# BB#10:
	movq	buf(%rip), %rcx
	movslq	%eax, %rdx
	movb	-1(%rcx,%rdx), %cl
	movb	%cl, last_char(%rip)
	cmpl	$8191, %eax             # imm = 0x1FFF
	jle	.LBB6_12
	jmp	.LBB6_15
.LBB6_9:                                # %._crit_edge7
	movb	last_char(%rip), %cl
	cmpl	$8191, %eax             # imm = 0x1FFF
	jg	.LBB6_15
.LBB6_12:
	cmpb	$10, %cl
	je	.LBB6_15
# BB#13:
	movslq	%eax, %rcx
	leaq	1(%rcx), %rax
	movl	%eax, blksize(%rip)
	movq	buf(%rip), %rdx
	movb	$10, (%rdx,%rcx)
	movb	$10, last_char(%rip)
	movzwl	ftype(%rip), %ecx
	cmpl	$3, %ecx
	jne	.LBB6_15
# BB#14:
	movl	$0, file_pos+4(%rip)
	movl	$2, %edi
	movl	$25, %esi
	movl	$.L.str.55, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movl	blksize(%rip), %eax
.LBB6_15:
	movq	buf(%rip), %rcx
	movq	%rcx, frst(%rip)
	cltq
	leaq	(%rcx,%rax), %rdx
	movq	%rdx, limit(%rip)
	movb	$10, (%rcx,%rax)
	movq	chpt(%rip), %rsi
	movq	limit(%rip), %rcx
	addq	$8, %rsp
.LBB6_16:
	cmpq	%rcx, %rsi
	jb	.LBB6_18
# BB#17:
	movq	buf(%rip), %rax
	movq	%rax, limit(%rip)
	movq	%rax, chpt(%rip)
	movb	$0, (%rax)
.LBB6_18:
	retq
.Lfunc_end6:
	.size	srcnext, .Lfunc_end6-srcnext
	.cfi_endproc

	.globl	LexScanVerbatim
	.p2align	4, 0x90
	.type	LexScanVerbatim,@function
LexScanVerbatim:                        # @LexScanVerbatim
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$1096, %rsp             # imm = 0x448
.Lcfi35:
	.cfi_def_cfa_offset 1152
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movq	%rdx, %rbx
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movq	%rdi, %r15
	cmpq	$0, next_token(%rip)
	je	.LBB7_2
# BB#1:
	movl	$2, %edi
	movl	$16, %esi
	movl	$.L.str.42, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
.LBB7_2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	chpt(%rip), %r12
	movl	$0, 12(%rsp)
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	$.L.str.44, %eax
	movl	$.L.str.45, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_20:                               #   in Loop: Header=BB7_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	64(%rsp), %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
.LBB7_3:                                # %.backedge252.thread
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #       Child Loop BB7_5 Depth 3
                                        #         Child Loop BB7_38 Depth 4
                                        #         Child Loop BB7_40 Depth 4
                                        #         Child Loop BB7_24 Depth 4
                                        #         Child Loop BB7_26 Depth 4
                                        #         Child Loop BB7_9 Depth 4
                                        #         Child Loop BB7_12 Depth 4
                                        #         Child Loop BB7_105 Depth 4
                                        #         Child Loop BB7_108 Depth 4
                                        #         Child Loop BB7_119 Depth 4
                                        #         Child Loop BB7_122 Depth 4
                                        #         Child Loop BB7_70 Depth 4
                                        #         Child Loop BB7_73 Depth 4
                                        #       Child Loop BB7_132 Depth 3
                                        #       Child Loop BB7_135 Depth 3
                                        #       Child Loop BB7_153 Depth 3
                                        #       Child Loop BB7_148 Depth 3
                                        #       Child Loop BB7_57 Depth 3
                                        #       Child Loop BB7_59 Depth 3
                                        #       Child Loop BB7_87 Depth 3
                                        #       Child Loop BB7_90 Depth 3
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_4
.LBB7_95:                               # %._crit_edge286
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	je	.LBB7_96
# BB#100:                               #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	jmp	.LBB7_101
.LBB7_96:                               #   in Loop: Header=BB7_4 Depth=2
	movslq	12(%rsp), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	jg	.LBB7_101
# BB#97:                                #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_99
# BB#98:                                #   in Loop: Header=BB7_4 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_101
.LBB7_152:                              # %.us-lcssa.us.us
                                        #   in Loop: Header=BB7_148 Depth=3
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_151
	.p2align	4, 0x90
.LBB7_148:                              #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	12(%rsp), %rcx
	cmpq	$511, %rcx              # imm = 0x1FF
	jg	.LBB7_151
# BB#149:                               #   in Loop: Header=BB7_148 Depth=3
	movslq	%eax, %rdx
	cmpb	$8, chtbl(%rdx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB7_152
# BB#150:                               #   in Loop: Header=BB7_148 Depth=3
	leal	1(%rcx), %edx
	movl	%edx, 12(%rsp)
	movb	%al, 64(%rsp,%rcx)
.LBB7_151:                              # %.backedge.us.us
                                        #   in Loop: Header=BB7_148 Depth=3
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB7_148
.LBB7_154:                              # %.outer._crit_edge
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	%rbp, %rdi
	callq	fclose
	xorl	%ebp, %ebp
	jmp	.LBB7_4
.LBB7_79:                               #   in Loop: Header=BB7_4 Depth=2
	leal	1(%rax), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rax)
.LBB7_81:                               #   in Loop: Header=BB7_4 Depth=2
	incl	28(%rsp)                # 4-byte Folded Spill
	xorl	%ebp, %ebp
	jmp	.LBB7_4
.LBB7_99:                               #   in Loop: Header=BB7_4 Depth=2
	leal	1(%rax), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rax)
.LBB7_101:                              #   in Loop: Header=BB7_4 Depth=2
	decl	28(%rsp)                # 4-byte Folded Spill
	xorl	%ebp, %ebp
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_128:                              #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	xorl	%ebp, %ebp
.LBB7_4:                                # %.backedge252.thread
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_5 Depth 3
                                        #         Child Loop BB7_38 Depth 4
                                        #         Child Loop BB7_40 Depth 4
                                        #         Child Loop BB7_24 Depth 4
                                        #         Child Loop BB7_26 Depth 4
                                        #         Child Loop BB7_9 Depth 4
                                        #         Child Loop BB7_12 Depth 4
                                        #         Child Loop BB7_105 Depth 4
                                        #         Child Loop BB7_108 Depth 4
                                        #         Child Loop BB7_119 Depth 4
                                        #         Child Loop BB7_122 Depth 4
                                        #         Child Loop BB7_70 Depth 4
                                        #         Child Loop BB7_73 Depth 4
                                        #       Child Loop BB7_132 Depth 3
                                        #       Child Loop BB7_135 Depth 3
                                        #       Child Loop BB7_153 Depth 3
                                        #       Child Loop BB7_148 Depth 3
                                        #       Child Loop BB7_57 Depth 3
                                        #       Child Loop BB7_59 Depth 3
                                        #       Child Loop BB7_87 Depth 3
                                        #       Child Loop BB7_90 Depth 3
	xorl	%r13d, %r13d
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_17:                               # %._crit_edge312
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB7_128
.LBB7_18:                               #   in Loop: Header=BB7_5 Depth=3
	movslq	12(%rsp), %rax
	xorl	%ebp, %ebp
	cmpq	$511, %rax              # imm = 0x1FF
	movl	$0, %r13d
	jg	.LBB7_5
	jmp	.LBB7_19
.LBB7_39:                               # %.lr.ph303.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	xorl	%ebx, %ebx
	leaq	64(%rsp), %r13
	.p2align	4, 0x90
.LBB7_40:                               # %.lr.ph303.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_44
# BB#41:                                #   in Loop: Header=BB7_40 Depth=4
	movzbl	576(%rsp,%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_42
# BB#43:                                #   in Loop: Header=BB7_40 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_44
	.p2align	4, 0x90
.LBB7_42:                               #   in Loop: Header=BB7_40 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_44:                               #   in Loop: Header=BB7_40 Depth=4
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB7_40
# BB#45:                                #   in Loop: Header=BB7_5 Depth=3
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB7_46:                               #   in Loop: Header=BB7_5 Depth=3
	xorl	%r13d, %r13d
.LBB7_47:                               # %.loopexit245
                                        #   in Loop: Header=BB7_5 Depth=3
	movb	(%r14), %al
	movslq	%r13d, %rcx
	incl	%r13d
	movb	%al, 576(%rsp,%rcx)
.LBB7_48:                               #   in Loop: Header=BB7_5 Depth=3
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	cmovnel	%eax, %ebp
	movq	%r12, chpt(%rip)
	callq	srcnext
	movl	file_pos+4(%rip), %eax
	incl	%eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	%eax, file_pos+4(%rip)
	movq	chpt(%rip), %r12
	leaq	-1(%r12), %rax
	movq	%rax, startline(%rip)
	jmp	.LBB7_5
.LBB7_121:                              # %.lr.ph279.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	leaq	64(%rsp), %r13
	.p2align	4, 0x90
.LBB7_122:                              # %.lr.ph279.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_126
# BB#123:                               #   in Loop: Header=BB7_122 Depth=4
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_124
# BB#125:                               #   in Loop: Header=BB7_122 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_126
	.p2align	4, 0x90
.LBB7_124:                              #   in Loop: Header=BB7_122 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_126:                              #   in Loop: Header=BB7_122 Depth=4
	incq	%rbx
	decq	%rbp
	jne	.LBB7_122
.LBB7_127:                              # %._crit_edge280
                                        #   in Loop: Header=BB7_5 Depth=3
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	jne	.LBB7_128
	jmp	.LBB7_18
.LBB7_33:                               # %.loopexit
                                        #   in Loop: Header=BB7_5 Depth=3
	movb	(%r14), %al
	movslq	%r13d, %rcx
	incl	%r13d
	movb	%al, 576(%rsp,%rcx)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_5:                                # %.backedge252.thread
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_38 Depth 4
                                        #         Child Loop BB7_40 Depth 4
                                        #         Child Loop BB7_24 Depth 4
                                        #         Child Loop BB7_26 Depth 4
                                        #         Child Loop BB7_9 Depth 4
                                        #         Child Loop BB7_12 Depth 4
                                        #         Child Loop BB7_105 Depth 4
                                        #         Child Loop BB7_108 Depth 4
                                        #         Child Loop BB7_119 Depth 4
                                        #         Child Loop BB7_122 Depth 4
                                        #         Child Loop BB7_70 Depth 4
                                        #         Child Loop BB7_73 Depth 4
	movq	%r12, %r14
	leaq	1(%r14), %r12
	movzbl	(%r14), %r9d
	movzbl	chtbl(%r9), %eax
	cmpq	$9, %rax
	ja	.LBB7_155
# BB#6:                                 # %.backedge252.thread
                                        #   in Loop: Header=BB7_5 Depth=3
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_7:                                # %.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	testl	%r13d, %r13d
	jle	.LBB7_10
# BB#8:                                 # %.lr.ph311
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	je	.LBB7_11
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph311.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_9
.LBB7_10:                               #   in Loop: Header=BB7_5 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_21:                               #   in Loop: Header=BB7_5 Depth=3
	testl	%ebp, %ebp
	jne	.LBB7_5
# BB#22:                                #   in Loop: Header=BB7_5 Depth=3
	cmpl	$512, %r13d             # imm = 0x200
	jne	.LBB7_33
# BB#23:                                # %.lr.ph307
                                        #   in Loop: Header=BB7_5 Depth=3
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.LBB7_25
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph307.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	576(%rsp,%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB7_24
	jmp	.LBB7_32
.LBB7_155:                              #   in Loop: Header=BB7_5 Depth=3
	movl	$2, %edi
	movl	$22, %esi
	movl	$.L.str.53, %edx
	xorl	%ecx, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	Error
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.24, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB7_5
.LBB7_52:                               #   in Loop: Header=BB7_5 Depth=3
	cmpb	$125, %r9b
	je	.LBB7_83
# BB#53:                                #   in Loop: Header=BB7_5 Depth=3
	cmpb	$123, %r9b
	je	.LBB7_54
# BB#68:                                # %.preheader246
                                        #   in Loop: Header=BB7_5 Depth=3
	testl	%r13d, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	jle	.LBB7_17
# BB#69:                                # %.lr.ph297
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	je	.LBB7_72
	.p2align	4, 0x90
.LBB7_70:                               # %.lr.ph297.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_70
# BB#71:                                #   in Loop: Header=BB7_5 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_17
.LBB7_102:                              #   in Loop: Header=BB7_5 Depth=3
	cmpb	$64, %r9b
	jne	.LBB7_103
# BB#113:                               #   in Loop: Header=BB7_5 Depth=3
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB7_115
# BB#114:                               #   in Loop: Header=BB7_5 Depth=3
	movl	$.L.str.47, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB7_156
.LBB7_115:                              #   in Loop: Header=BB7_5 Depth=3
	movl	$.L.str.48, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB7_129
# BB#116:                               #   in Loop: Header=BB7_5 Depth=3
	movl	$.L.str.49, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB7_129
# BB#117:                               # %.preheader249
                                        #   in Loop: Header=BB7_5 Depth=3
	testl	%r13d, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	jle	.LBB7_127
# BB#118:                               # %.lr.ph279
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	je	.LBB7_121
	.p2align	4, 0x90
.LBB7_119:                              # %.lr.ph279.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_119
# BB#120:                               #   in Loop: Header=BB7_5 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_127
.LBB7_34:                               #   in Loop: Header=BB7_5 Depth=3
	testl	%ebp, %ebp
	jne	.LBB7_48
# BB#35:                                #   in Loop: Header=BB7_5 Depth=3
	cmpl	$512, %r13d             # imm = 0x200
	jne	.LBB7_47
# BB#36:                                # %.lr.ph303
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB7_39
# BB#37:                                # %.lr.ph303.split.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_38:                               # %.lr.ph303.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	576(%rsp,%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB7_38
	jmp	.LBB7_46
.LBB7_49:                               #   in Loop: Header=BB7_5 Depth=3
	movl	$2, %edi
	testq	%r15, %r15
	je	.LBB7_50
# BB#51:                                #   in Loop: Header=BB7_5 Depth=3
	movl	$17, %esi
	movl	$.L.str.46, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	Error
	jmp	.LBB7_5
.LBB7_11:                               # %.lr.ph311.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	leaq	64(%rsp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph311.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_16
# BB#13:                                #   in Loop: Header=BB7_12 Depth=4
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_14
# BB#15:                                #   in Loop: Header=BB7_12 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_16
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_12 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_16:                               #   in Loop: Header=BB7_12 Depth=4
	incq	%rbx
	decq	%rbp
	jne	.LBB7_12
	jmp	.LBB7_17
.LBB7_103:                              # %.preheader251
                                        #   in Loop: Header=BB7_5 Depth=3
	testl	%r13d, %r13d
	jle	.LBB7_106
# BB#104:                               # %.lr.ph
                                        #   in Loop: Header=BB7_5 Depth=3
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	je	.LBB7_107
	.p2align	4, 0x90
.LBB7_105:                              # %.lr.ph.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_105
.LBB7_106:                              #   in Loop: Header=BB7_5 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_17
.LBB7_50:                               #   in Loop: Header=BB7_5 Depth=3
	movl	$22, %esi
	movl	$.L.str.43, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	Error
	jmp	.LBB7_5
.LBB7_25:                               # %.lr.ph307.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	leaq	64(%rsp), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_26:                               # %.lr.ph307.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_30
# BB#27:                                #   in Loop: Header=BB7_26 Depth=4
	movzbl	576(%rsp,%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_28
# BB#29:                                #   in Loop: Header=BB7_26 Depth=4
	movq	%rax, %rdi
	movq	%rbp, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_30
	.p2align	4, 0x90
.LBB7_28:                               #   in Loop: Header=BB7_26 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_30:                               #   in Loop: Header=BB7_26 Depth=4
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB7_26
# BB#31:                                #   in Loop: Header=BB7_5 Depth=3
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB7_32:                               #   in Loop: Header=BB7_5 Depth=3
	xorl	%r13d, %r13d
	jmp	.LBB7_33
.LBB7_107:                              # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	leaq	64(%rsp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_108:                              # %.lr.ph.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_112
# BB#109:                               #   in Loop: Header=BB7_108 Depth=4
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_110
# BB#111:                               #   in Loop: Header=BB7_108 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_112
	.p2align	4, 0x90
.LBB7_110:                              #   in Loop: Header=BB7_108 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_112:                              #   in Loop: Header=BB7_108 Depth=4
	incq	%rbx
	decq	%rbp
	jne	.LBB7_108
	jmp	.LBB7_17
.LBB7_72:                               # %.lr.ph297.split.us.preheader
                                        #   in Loop: Header=BB7_5 Depth=3
	leaq	64(%rsp), %r13
	.p2align	4, 0x90
.LBB7_73:                               # %.lr.ph297.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_5 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_77
# BB#74:                                #   in Loop: Header=BB7_73 Depth=4
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_75
# BB#76:                                #   in Loop: Header=BB7_73 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_77
	.p2align	4, 0x90
.LBB7_75:                               #   in Loop: Header=BB7_73 Depth=4
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_77:                               #   in Loop: Header=BB7_73 Depth=4
	incq	%rbx
	decq	%rbp
	jne	.LBB7_73
	jmp	.LBB7_17
.LBB7_19:                               #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %ecx
	cmpb	$8, chtbl(%rcx)
	je	.LBB7_20
# BB#78:                                #   in Loop: Header=BB7_4 Depth=2
	leal	1(%rax), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rax)
	jmp	.LBB7_4
.LBB7_129:                              #   in Loop: Header=BB7_4 Depth=2
	movl	$.L.str.49, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%r13d, %r13d
	jle	.LBB7_130
# BB#131:                               # %.lr.ph264
                                        #   in Loop: Header=BB7_4 Depth=2
	testq	%r15, %r15
	movl	%r13d, %ebp
	movl	%eax, %r12d
	leaq	576(%rsp), %rbx
	je	.LBB7_134
	.p2align	4, 0x90
.LBB7_132:                              # %.lr.ph264.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_132
	jmp	.LBB7_133
.LBB7_83:                               #   in Loop: Header=BB7_4 Depth=2
	movl	28(%rsp), %eax          # 4-byte Reload
	orl	32(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB7_156
# BB#84:                                # %.preheader248
                                        #   in Loop: Header=BB7_4 Depth=2
	testl	%r13d, %r13d
	jle	.LBB7_85
# BB#86:                                # %.lr.ph285
                                        #   in Loop: Header=BB7_4 Depth=2
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	leaq	64(%rsp), %r13
	je	.LBB7_89
	.p2align	4, 0x90
.LBB7_87:                               # %.lr.ph285.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_87
# BB#88:                                #   in Loop: Header=BB7_4 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_95
.LBB7_54:                               # %.preheader247
                                        #   in Loop: Header=BB7_4 Depth=2
	testl	%r13d, %r13d
	movq	16(%rsp), %rdi          # 8-byte Reload
	jle	.LBB7_55
# BB#56:                                # %.lr.ph291
                                        #   in Loop: Header=BB7_4 Depth=2
	testq	%r15, %r15
	movl	%r13d, %ebp
	leaq	576(%rsp), %rbx
	leaq	64(%rsp), %r13
	je	.LBB7_59
	.p2align	4, 0x90
.LBB7_57:                               # %.lr.ph291.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_57
# BB#58:                                #   in Loop: Header=BB7_4 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB7_64
	.p2align	4, 0x90
.LBB7_59:                               # %.lr.ph291.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	12(%rsp), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	jg	.LBB7_63
# BB#60:                                #   in Loop: Header=BB7_59 Depth=3
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_61
# BB#62:                                #   in Loop: Header=BB7_59 Depth=3
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	movq	%rax, %rdi
	jmp	.LBB7_63
	.p2align	4, 0x90
.LBB7_61:                               #   in Loop: Header=BB7_59 Depth=3
	leal	1(%rax), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rax)
.LBB7_63:                               #   in Loop: Header=BB7_59 Depth=3
	incq	%rbx
	decq	%rbp
	jne	.LBB7_59
	jmp	.LBB7_64
.LBB7_130:                              #   in Loop: Header=BB7_4 Depth=2
	movl	%eax, %r12d
.LBB7_133:                              #   in Loop: Header=BB7_4 Depth=2
	leaq	64(%rsp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_140
.LBB7_55:                               #   in Loop: Header=BB7_4 Depth=2
	leaq	64(%rsp), %r13
.LBB7_64:                               # %._crit_edge292
                                        #   in Loop: Header=BB7_4 Depth=2
	testq	%r15, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB7_65
# BB#80:                                #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	jmp	.LBB7_81
.LBB7_65:                               #   in Loop: Header=BB7_4 Depth=2
	movslq	12(%rsp), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	jg	.LBB7_81
# BB#66:                                #   in Loop: Header=BB7_4 Depth=2
	movzbl	(%r14), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_79
# BB#67:                                #   in Loop: Header=BB7_4 Depth=2
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_81
.LBB7_85:                               #   in Loop: Header=BB7_4 Depth=2
	leaq	64(%rsp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_95
.LBB7_134:                              # %.lr.ph264.split.us.preheader
                                        #   in Loop: Header=BB7_4 Depth=2
	leaq	64(%rsp), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_135:                              # %.lr.ph264.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_139
# BB#136:                               #   in Loop: Header=BB7_135 Depth=3
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_137
# BB#138:                               #   in Loop: Header=BB7_135 Depth=3
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_139
	.p2align	4, 0x90
.LBB7_137:                              #   in Loop: Header=BB7_135 Depth=3
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_139:                              #   in Loop: Header=BB7_135 Depth=3
	incq	%rbx
	decq	%rbp
	jne	.LBB7_135
.LBB7_140:                              # %._crit_edge265
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movl	%r12d, %ebx
	testl	%ebx, %ebx
	setne	%al
	leaq	(%rax,%rax,2), %rax
	leaq	8(%r14,%rax), %rax
	movq	%rax, chpt(%rip)
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 56(%rsp)
	cmpb	$102, 32(%rbp)
	je	.LBB7_142
# BB#141:                               #   in Loop: Header=BB7_4 Depth=2
	addq	$32, %rbp
	testl	%ebx, %ebx
	movl	$.L.str.48, %eax
	movl	$.L.str.49, %ecx
	cmovneq	%rcx, %rax
	movq	%rax, (%rsp)
	movl	$2, %edi
	movl	$18, %esi
	movl	$.L.str.50, %edx
	movl	$1, %ecx
	movl	$.L.str.32, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB7_142:                              #   in Loop: Header=BB7_4 Depth=2
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	56(%rsp), %rdi
	callq	Parse
	movq	chpt(%rip), %r12
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	leaq	32(%rbp), %r14
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB7_144
# BB#143:                               #   in Loop: Header=BB7_4 Depth=2
	movl	$2, %edi
	movl	$19, %esi
	movl	$.L.str.51, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
.LBB7_144:                              # %._crit_edge393
                                        #   in Loop: Header=BB7_4 Depth=2
	leaq	64(%rbp), %rdi
	cmpl	$1, %ebx
	movl	$1, %r8d
	sbbl	$-1, %r8d
	movl	$.L.str.36, %esi
	movl	$1, %ecx
	movq	%r14, %rdx
	callq	DefineFile
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	movq	%rbp, %rsi
	addq	$33, %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rbp)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	movzwl	%ax, %r14d
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	OpenFile
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB7_146
# BB#145:                               #   in Loop: Header=BB7_4 Depth=2
	movl	%r14d, %edi
	callq	PosOfFile
	movq	%rax, %r13
	movl	%r14d, %edi
	callq	FullFileName
	movq	%rax, %r9
	movl	$2, %edi
	movl	$20, %esi
	movl	$.L.str.52, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	leaq	64(%rsp), %r13
	callq	Error
.LBB7_146:                              # %.preheader250
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB7_154
# BB#147:                               # %.lr.ph267.lr.ph
                                        #   in Loop: Header=BB7_4 Depth=2
	testq	%r15, %r15
	je	.LBB7_148
	.p2align	4, 0x90
.LBB7_153:                              # %.lr.ph267.split
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB7_153
	jmp	.LBB7_154
.LBB7_89:                               # %.lr.ph285.split.us.preheader
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_90:                               # %.lr.ph285.split.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	12(%rsp), %rsi
	cmpq	$511, %rsi              # imm = 0x1FF
	jg	.LBB7_94
# BB#91:                                #   in Loop: Header=BB7_90 Depth=3
	movzbl	(%rbx), %ecx
	cmpb	$8, chtbl(%rcx)
	jne	.LBB7_92
# BB#93:                                #   in Loop: Header=BB7_90 Depth=3
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	12(%rsp), %rdx
	callq	BuildLines
	jmp	.LBB7_94
	.p2align	4, 0x90
.LBB7_92:                               #   in Loop: Header=BB7_90 Depth=3
	leal	1(%rsi), %edx
	movl	%edx, 12(%rsp)
	movb	%cl, 64(%rsp,%rsi)
.LBB7_94:                               #   in Loop: Header=BB7_90 Depth=3
	incq	%rbx
	decq	%rbp
	jne	.LBB7_90
	jmp	.LBB7_95
.LBB7_156:                              # %.backedge252
	testq	%r15, %r15
	je	.LBB7_157
# BB#161:
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	jmp	.LBB7_162
.LBB7_157:
	movslq	12(%rsp), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	jg	.LBB7_162
# BB#158:
	cmpb	$8, chtbl+10(%rip)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB7_160
# BB#159:
	leaq	64(%rsp), %rsi
	leaq	12(%rsp), %rdx
	movq	%rbp, %rdi
	callq	BuildLines
	movq	%rax, %rbp
	jmp	.LBB7_163
.LBB7_162:
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB7_163:
	movq	%r14, %rax
	subq	startline(%rip), %rax
	cmpq	$2048, %rax             # imm = 0x800
	jl	.LBB7_165
# BB#164:
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	file_pos+4(%rip), %eax
	orl	$1048576, %eax          # imm = 0x100000
	movl	%eax, file_pos+4(%rip)
	movl	$2, %edi
	movl	$21, %esi
	movl	$.L.str.41, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
.LBB7_165:
	movq	%r14, chpt(%rip)
	testq	%r15, %r15
	jne	.LBB7_168
# BB#166:
	testq	%rbp, %rbp
	jne	.LBB7_168
# BB#167:
	movl	$11, %edi
	movl	$.L.str.36, %esi
	movl	$file_pos, %edx
	callq	MakeWord
	movq	%rax, %rbp
.LBB7_168:
	movq	%rbp, %rax
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_160:
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rsp)
	movb	$10, 64(%rsp,%rax)
	jmp	.LBB7_163
.Lfunc_end7:
	.size	LexScanVerbatim, .Lfunc_end7-LexScanVerbatim
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_52
	.quad	.LBB7_102
	.quad	.LBB7_7
	.quad	.LBB7_7
	.quad	.LBB7_7
	.quad	.LBB7_21
	.quad	.LBB7_21
	.quad	.LBB7_21
	.quad	.LBB7_34
	.quad	.LBB7_49

	.text
	.p2align	4, 0x90
	.type	BuildLines,@function
BuildLines:                             # @BuildLines
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r12, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	(%rbx), %eax
	addl	$68, %eax
	movslq	%eax, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB8_2
# BB#1:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	movl	$file_pos, %r8d
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %r12
	jmp	.LBB8_5
.LBB8_2:
	movq	zz_free(,%rax,8), %r12
	testq	%r12, %r12
	je	.LBB8_3
# BB#4:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB8_5
.LBB8_3:
	movl	$file_pos, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB8_5:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r12)
	movb	$11, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r12)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r12)
	cmpl	$0, (%rbx)
	jle	.LBB8_6
# BB#7:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	movb	%cl, 64(%r12,%rax)
	incq	%rax
	movslq	(%rbx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB8_8
	jmp	.LBB8_9
.LBB8_6:
	xorl	%eax, %eax
.LBB8_9:                                # %._crit_edge
	movb	$0, 64(%r12,%rax)
	movl	$0, (%rbx)
	testq	%r15, %r15
	je	.LBB8_10
# BB#11:
	cmpb	$11, 32(%r15)
	jne	.LBB8_12
# BB#13:
	movzbl	zz_lengths+19(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB8_14
# BB#15:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB8_16
.LBB8_10:
	movq	%r12, %r14
	jmp	.LBB8_54
.LBB8_12:
	movq	%r15, %r14
	jmp	.LBB8_24
.LBB8_14:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB8_16:
	movb	$19, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%r14)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r14), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r14)
	andl	36(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_17
# BB#18:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_19
.LBB8_17:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_19:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB8_22
# BB#20:
	testq	%rax, %rax
	je	.LBB8_22
# BB#21:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB8_22:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB8_24
# BB#23:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB8_24:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB8_25
# BB#26:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB8_27
.LBB8_25:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB8_27:
	movb	$1, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	andb	$-4, 45(%r15)
	movzwl	file_pos+2(%rip), %eax
	movw	%ax, 34(%r15)
	movl	file_pos+4(%rip), %eax
	movl	%eax, 36(%r15)
	movl	$11, %edi
	movl	$.L.str.56, %esi
	movl	$file_pos, %edx
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_28
# BB#29:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_30
.LBB8_28:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_30:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB8_33
# BB#31:
	testq	%rax, %rax
	je	.LBB8_33
# BB#32:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB8_33:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB8_36
# BB#34:
	testq	%rax, %rax
	je	.LBB8_36
# BB#35:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB8_36:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_37
# BB#38:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_39
.LBB8_37:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_39:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB8_42
# BB#40:
	testq	%rax, %rax
	je	.LBB8_42
# BB#41:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB8_42:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB8_45
# BB#43:
	testq	%rax, %rax
	je	.LBB8_45
# BB#44:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB8_45:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_46
# BB#47:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_48
.LBB8_46:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_48:
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB8_51
# BB#49:
	testq	%rax, %rax
	je	.LBB8_51
# BB#50:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB8_51:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB8_54
# BB#52:
	testq	%rax, %rax
	je	.LBB8_54
# BB#53:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB8_54:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	BuildLines, .Lfunc_end8-BuildLines
	.cfi_endproc

	.type	chtbl,@object           # @chtbl
	.local	chtbl
	.comm	chtbl,256,16
	.type	stack_free,@object      # @stack_free
	.local	stack_free
	.comm	stack_free,4,4
	.type	.L.str.19,@object       # @.str.19
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.19:
	.asciz	"too many open files when opening include file %s; open files are:"
	.size	.L.str.19, 66

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"too many open files when opening database file %s; open files are:"
	.size	.L.str.20, 67

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"  %s"
	.size	.L.str.21, 5

	.type	lex_stack,@object       # @lex_stack
	.local	lex_stack
	.comm	lex_stack,1200,16
	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"exiting now"
	.size	.L.str.22, 12

	.type	chpt,@object            # @chpt
	.local	chpt
	.comm	chpt,8,8
	.type	frst,@object            # @frst
	.local	frst
	.comm	frst,8,8
	.type	limit,@object           # @limit
	.local	limit
	.comm	limit,8,8
	.type	buf,@object             # @buf
	.local	buf
	.comm	buf,8,8
	.type	blksize,@object         # @blksize
	.local	blksize
	.comm	blksize,4,4
	.type	last_char,@object       # @last_char
	.local	last_char
	.comm	last_char,1,1
	.type	startline,@object       # @startline
	.local	startline
	.comm	startline,8,8
	.type	this_file,@object       # @this_file
	.local	this_file
	.comm	this_file,2,2
	.type	fp,@object              # @fp
	.local	fp
	.comm	fp,8,8
	.type	ftype,@object           # @ftype
	.local	ftype
	.comm	ftype,2,2
	.type	next_token,@object      # @next_token
	.local	next_token
	.comm	next_token,8,8
	.type	offset,@object          # @offset
	.local	offset
	.comm	offset,4,4
	.type	first_line_num,@object  # @first_line_num
	.local	first_line_num
	.comm	first_line_num,4,4
	.type	same_file,@object       # @same_file
	.local	same_file
	.comm	same_file,4,4
	.type	mem_block,@object       # @mem_block
	.local	mem_block
	.comm	mem_block,8,8
	.type	file_pos,@object        # @file_pos
	.local	file_pos
	.comm	file_pos,8,4
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"run out of memory when opening file %s"
	.size	.L.str.23, 39

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"assert failed in %s"
	.size	.L.str.24, 20

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"LexPop: stack_free <= 0!"
	.size	.L.str.25, 25

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"illegal macro invocation in database"
	.size	.L.str.26, 37

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"character %c outside quoted string"
	.size	.L.str.27, 35

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"cannot open file %s"
	.size	.L.str.28, 20

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"unknown file type"
	.size	.L.str.29, 18

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"word is too long"
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"recursion in macro"
	.size	.L.str.31, 19

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"{"
	.size	.L.str.32, 2

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s expected (after %s)"
	.size	.L.str.33, 23

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"name of include file expected here"
	.size	.L.str.34, 35

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.zero	1
	.size	.L.str.36, 1

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"unterminated string"
	.size	.L.str.37, 20

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"skipping null character in string"
	.size	.L.str.38, 34

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"LexGetToken: error in quoted string"
	.size	.L.str.39, 36

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"LexGetToken: bad chtbl[]"
	.size	.L.str.40, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"line is too long (or final newline missing)"
	.size	.L.str.41, 44

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"filter parameter in macro"
	.size	.L.str.42, 26

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"end of file reached while reading %s"
	.size	.L.str.43, 37

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"@RawVerbatim"
	.size	.L.str.44, 13

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"@Verbatim"
	.size	.L.str.45, 10

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"end of file reached while reading filter parameter"
	.size	.L.str.46, 51

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"@End"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"@Include"
	.size	.L.str.48, 9

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"@SysInclude"
	.size	.L.str.49, 12

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"expected %s here (after %s)"
	.size	.L.str.50, 28

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"expected file name here"
	.size	.L.str.51, 24

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"cannot open include file %s"
	.size	.L.str.52, 28

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"unreadable character (octal %o)"
	.size	.L.str.53, 32

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"LexScanVerbatim: bad chtbl[]"
	.size	.L.str.54, 29

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"a database file must end with a newline; this one doesn't"
	.size	.L.str.55, 58

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"1vx"
	.size	.L.str.56, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
