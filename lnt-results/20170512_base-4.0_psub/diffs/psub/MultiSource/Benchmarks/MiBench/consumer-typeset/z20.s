	.text
	.file	"z20.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	FlushGalley
	.p2align	4, 0x90
	.type	FlushGalley,@function
FlushGalley:                            # @FlushGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1368, %rsp             # imm = 0x558
.Lcfi6:
	.cfi_def_cfa_offset 1424
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movzbl	43(%rdi), %edx
	andl	$1, %edx
	leal	8(,%rdx,8), %eax
	leaq	8(%rdi), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movl	%edx, %ecx
	xorl	$1, %ecx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	cwtl
	movl	%eax, 276(%rsp)         # 4-byte Spill
	leaq	336(%rsp), %r13
                                        # implicit-def: %R14D
                                        # implicit-def: %EAX
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
                                        # implicit-def: %EAX
	movl	%eax, 156(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 152(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 148(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 144(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 212(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 120(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 108(%rsp)         # 4-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	jmp	.LBB0_4
.LBB0_3:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB0_4
.LBB0_1:                                # %._crit_edge1287
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, zz_size(%rip)
.LBB0_2:                                #   in Loop: Header=BB0_4 Depth=1
	movzbl	%al, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	%r12, zz_hold(%rip)
	movzbl	32(%r12), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r12), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r12)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	.p2align	4, 0x90
.LBB0_4:                                # %.backedge1031
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_280 Depth 2
                                        #     Child Loop BB0_31 Depth 2
                                        #       Child Loop BB0_33 Depth 3
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_109 Depth 3
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_126 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_43 Depth 3
                                        #         Child Loop BB0_44 Depth 4
                                        #       Child Loop BB0_57 Depth 3
                                        #       Child Loop BB0_77 Depth 3
                                        #     Child Loop BB0_185 Depth 2
                                        #       Child Loop BB0_199 Depth 3
                                        #     Child Loop BB0_266 Depth 2
                                        #     Child Loop BB0_273 Depth 2
                                        #     Child Loop BB0_249 Depth 2
                                        #     Child Loop BB0_254 Depth 2
                                        #     Child Loop BB0_260 Depth 2
                                        #     Child Loop BB0_174 Depth 2
                                        #     Child Loop BB0_319 Depth 2
                                        #     Child Loop BB0_321 Depth 2
                                        #     Child Loop BB0_323 Depth 2
                                        #     Child Loop BB0_436 Depth 2
                                        #     Child Loop BB0_460 Depth 2
                                        #       Child Loop BB0_461 Depth 3
                                        #       Child Loop BB0_464 Depth 3
                                        #     Child Loop BB0_473 Depth 2
                                        #       Child Loop BB0_474 Depth 3
                                        #       Child Loop BB0_477 Depth 3
                                        #     Child Loop BB0_491 Depth 2
                                        #       Child Loop BB0_493 Depth 3
                                        #     Child Loop BB0_506 Depth 2
                                        #       Child Loop BB0_508 Depth 3
                                        #     Child Loop BB0_526 Depth 2
                                        #     Child Loop BB0_533 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpb	$8, 32(%rax)
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %r12
	cmpq	%rax, %r12
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %r12
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	cmpb	$118, %al
	jg	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=2
	testb	%al, %al
	je	.LBB0_8
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_4 Depth=1
	cmpb	$119, %al
	je	.LBB0_563
# BB#12:                                #   in Loop: Header=BB0_4 Depth=1
	cmpb	$120, %al
	je	.LBB0_18
# BB#13:                                #   in Loop: Header=BB0_4 Depth=1
	cmpb	$122, %al
	jne	.LBB0_10
# BB#14:                                #   in Loop: Header=BB0_4 Depth=1
	movq	80(%r12), %rax
	movq	80(%rax), %rax
	cmpq	InputSym(%rip), %rax
	jne	.LBB0_26
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	leaq	88(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	AttachGalley
	movq	24(%rbx), %r12
	.p2align	4, 0x90
.LBB0_19:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %r12
	cmpb	$0, 32(%r12)
	je	.LBB0_19
# BB#20:                                #   in Loop: Header=BB0_4 Depth=1
	cmpl	$5, %eax
	ja	.LBB0_24
# BB#21:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%eax, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_22:                               #   in Loop: Header=BB0_4 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_283
# BB#23:                                #   in Loop: Header=BB0_4 Depth=1
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jmp	.LBB0_165
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	%al, %edi
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	jmp	.LBB0_25
.LBB0_24:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
.LBB0_25:                               #   in Loop: Header=BB0_4 Depth=1
	callq	Error
.LBB0_26:                               #   in Loop: Header=BB0_4 Depth=1
	movq	80(%r12), %r9
	movl	40(%r9), %r8d
	testl	$1610612736, %r8d       # imm = 0x60000000
	je	.LBB0_28
# BB#27:                                # %._crit_edge1529
                                        #   in Loop: Header=BB0_4 Depth=1
	movw	42(%r9), %r8w
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_28:                               #   in Loop: Header=BB0_4 Depth=1
	andl	$-1610612737, %r8d      # imm = 0x9FFFFFFF
	orl	$536870912, %r8d        # imm = 0x20000000
	movl	%r8d, 40(%r9)
	shrl	$16, %r8d
.LBB0_29:                               #   in Loop: Header=BB0_4 Depth=1
	movq	$0, 88(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
	cmpq	%rax, %r15
	je	.LBB0_538
# BB#30:                                # %.lr.ph1239
                                        #   in Loop: Header=BB0_4 Depth=1
	andl	276(%rsp), %r8d         # 4-byte Folded Reload
	leaq	16(%r9), %rax
	leaq	24(%r9), %rcx
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	movq	%rax, %rdx
	cmoveq	%rcx, %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	cmpw	$0, 128(%rsp)           # 2-byte Folded Reload
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	cmoveq	%rcx, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$0, 108(%rsp)           # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	movl	%r8d, 40(%rsp)          # 4-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
.LBB0_31:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_33 Depth 3
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_109 Depth 3
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_126 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_43 Depth 3
                                        #         Child Loop BB0_44 Depth 4
                                        #       Child Loop BB0_57 Depth 3
                                        #       Child Loop BB0_77 Depth 3
	leaq	16(%r15), %rdi
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_32:                               #   in Loop: Header=BB0_33 Depth=3
	addq	$16, %rdi
.LBB0_33:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB0_32
# BB#34:                                #   in Loop: Header=BB0_31 Depth=2
	cmpb	$9, %cl
	jne	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_31 Depth=2
	leaq	8(%rdi), %rax
	cmpw	$0, 128(%rsp)           # 2-byte Folded Reload
	movq	%rdi, 56(%rsp)
	cmovneq	%rdi, %rax
	movq	(%rax), %rdi
	.p2align	4, 0x90
.LBB0_36:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB0_36
.LBB0_37:                               # %.loopexit1021
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	%rdi, 56(%rsp)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB0_105
# BB#38:                                # %.loopexit1021
                                        #   in Loop: Header=BB0_31 Depth=2
	movl	$1, %eax
	movl	%ecx, %edx
	addb	$-128, %dl
	movzbl	%dl, %edx
	jmpq	*.LJTI0_1(,%rdx,8)
.LBB0_39:                               #   in Loop: Header=BB0_31 Depth=2
	movl	40(%r9), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%rdi), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rdi)
	cmpw	$0, 128(%rsp)           # 2-byte Folded Reload
	je	.LBB0_51
# BB#40:                                # %.preheader1020
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	8(%r15), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB0_49
.LBB0_43:                               # %.preheader
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_44 Depth 4
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB0_44:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        #       Parent Loop BB0_43 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %ebx
	testb	%bl, %bl
	je	.LBB0_44
# BB#45:                                #   in Loop: Header=BB0_43 Depth=3
	movl	%ebx, %edx
	addb	$-121, %dl
	cmpb	$2, %dl
	jb	.LBB0_164
# BB#46:                                #   in Loop: Header=BB0_43 Depth=3
	cmpb	$1, %bl
	jne	.LBB0_42
# BB#47:                                #   in Loop: Header=BB0_43 Depth=3
	testb	$2, 45(%rcx)
	jne	.LBB0_42
# BB#48:                                #   in Loop: Header=BB0_43 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	8(%rax), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB0_43
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_42:                               # %.backedge
                                        #   in Loop: Header=BB0_43 Depth=3
	movq	8(%rax), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB0_43
.LBB0_49:                               # %._crit_edge
                                        #   in Loop: Header=BB0_31 Depth=2
	cmpb	$18, 32(%rdi)
	jne	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %ebx
	callq	VerticalHyphenate
	movl	%ebx, %r10d
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	.p2align	4, 0x90
.LBB0_51:                               #   in Loop: Header=BB0_31 Depth=2
	testw	%r8w, %r8w
	je	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_31 Depth=2
	movl	%r14d, %r13d
	jmp	.LBB0_72
.LBB0_41:                               #   in Loop: Header=BB0_31 Depth=2
	movl	40(%r9), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%rdi), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rdi)
	movl	%r10d, %eax
	jmp	.LBB0_82
.LBB0_53:                               #   in Loop: Header=BB0_31 Depth=2
	movl	%r14d, %r13d
	movq	112(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB0_70
# BB#54:                                #   in Loop: Header=BB0_31 Depth=2
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	224(%rsp), %rcx         # 8-byte Reload
	cmpq	(%rcx), %rax
	je	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	movl	%r10d, %ebx
	callq	Error
	movl	%ebx, %r10d
.LBB0_56:                               #   in Loop: Header=BB0_31 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB0_57:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB0_57
# BB#58:                                #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	testw	%dx, %dx
	sete	%cl
	testw	%dx, %dx
	setne	%dl
	cmpb	$19, %al
	sete	%bl
	cmpb	$17, %al
	sete	%al
	testb	%bl, %dl
	jne	.LBB0_61
# BB#59:                                #   in Loop: Header=BB0_31 Depth=2
	andb	%al, %cl
	jne	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
.LBB0_61:                               #   in Loop: Header=BB0_31 Depth=2
	leaq	284(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%esi, %esi
	leaq	232(%rsp), %rdx
	leaq	288(%rsp), %rcx
	leaq	296(%rsp), %r8
	leaq	328(%rsp), %r9
	callq	SetNeighbours
	cmpq	$0, 232(%rsp)
	jne	.LBB0_64
# BB#62:                                #   in Loop: Header=BB0_31 Depth=2
	movq	56(%rsp), %rax
	movb	32(%rax), %al
	addb	$-2, %al
	cmpb	$7, %al
	jb	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_64:                               #   in Loop: Header=BB0_31 Depth=2
	cmpq	$0, 296(%rsp)
	je	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_66:                               #   in Loop: Header=BB0_31 Depth=2
	cmpl	$153, 284(%rsp)
	je	.LBB0_69
# BB#67:                                #   in Loop: Header=BB0_31 Depth=2
	movq	56(%rsp), %rax
	movb	32(%rax), %al
	addb	$-2, %al
	cmpb	$7, %al
	jb	.LBB0_69
# BB#68:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_69:                               #   in Loop: Header=BB0_31 Depth=2
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	48(%r14,%rdx,4), %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	56(%r14,%rdx,4), %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rbx         # 8-byte Reload
	movl	48(%r14,%rbx,4), %r13d
	movl	56(%r14,%rbx,4), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	%r14, %rdi
	leaq	184(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	320(%rsp), %rbp
	movq	%rbp, %rcx
	callq	Constrained
	movq	%r14, %rdi
	leaq	304(%rsp), %rsi
	movl	%ebx, %edx
	movq	%rbp, %rcx
	movq	%r14, %rbx
	callq	Constrained
	movl	188(%rsp), %eax
	cmpl	$8388607, 192(%rsp)     # imm = 0x7FFFFF
	movl	%eax, %edx
	movl	$0, %ecx
	cmovel	%ecx, %edx
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	cmovnel	%eax, %edx
	cmpl	$8388607, 184(%rsp)     # imm = 0x7FFFFF
	cmovnel	%eax, %edx
	movl	%edx, 212(%rsp)         # 4-byte Spill
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	96(%rsp), %r10d         # 4-byte Reload
.LBB0_70:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax
	movb	32(%rax), %cl
	addb	$-2, %cl
	cmpb	$7, %cl
	jae	.LBB0_86
# BB#71:                                #   in Loop: Header=BB0_31 Depth=2
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
.LBB0_72:                               #   in Loop: Header=BB0_31 Depth=2
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	movq	120(%rsp), %rbp         # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
.LBB0_73:                               #   in Loop: Header=BB0_31 Depth=2
	movl	52(%rsp), %ecx          # 4-byte Reload
	movb	$1, %al
	testl	%ecx, %ecx
	jne	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_31 Depth=2
	movb	42(%r12), %al
	andb	$32, %al
	shrb	$5, %al
.LBB0_75:                               #   in Loop: Header=BB0_31 Depth=2
	movzbl	%al, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cmpq	$0, 88(%rsp)
	je	.LBB0_81
# BB#76:                                #   in Loop: Header=BB0_31 Depth=2
	movq	8(%r15), %r14
	cmpb	$0, 32(%r14)
	jne	.LBB0_377
	.p2align	4, 0x90
.LBB0_77:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	$16, %r14
	movq	(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB0_77
# BB#78:                                #   in Loop: Header=BB0_31 Depth=2
	cmpb	$1, %al
	je	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rbx, %rbp
	movq	%r13, %rbx
	movq	%r15, %r13
	movl	%r10d, %r15d
	callq	Error
	movl	%r15d, %r10d
	movq	%r13, %r15
	movq	%rbx, %r13
	movq	%rbp, %rbx
	movq	120(%rsp), %rbp         # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
.LBB0_80:                               # %.loopexit1019
                                        #   in Loop: Header=BB0_31 Depth=2
	cmpb	$0, 44(%r14)
	jns	.LBB0_376
.LBB0_81:                               #   in Loop: Header=BB0_31 Depth=2
	movl	%r13d, %r14d
	movl	%ebx, %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	336(%rsp), %r13
.LBB0_82:                               #   in Loop: Header=BB0_31 Depth=2
	movq	8(%r15), %r15
	cmpq	32(%rsp), %r15          # 8-byte Folded Reload
	movl	%eax, %r10d
	jne	.LBB0_31
	jmp	.LBB0_83
.LBB0_86:                               #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	56(%rax,%rdx,4), %ebp
	movq	288(%rsp), %rcx
	movl	56(%rcx,%rdx,4), %ebx
	movl	48(%rax,%rdx,4), %esi
	movq	232(%rsp), %rcx
	addq	$44, %rcx
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	(%r14,%rax), %r9d
	subl	%ebx, %r9d
	movl	%ebx, %edi
	movl	%ebp, %edx
	movl	212(%rsp), %r8d         # 4-byte Reload
	callq	ActualGap
	movq	232(%rsp), %rcx
	movzwl	44(%rcx), %edx
	andl	$7168, %edx             # imm = 0x1C00
	cmpl	$2048, %edx             # imm = 0x800
	jne	.LBB0_89
# BB#87:                                #   in Loop: Header=BB0_31 Depth=2
	movswl	46(%rcx), %ecx
	cmpl	$4096, %ecx             # imm = 0x1000
	movl	184(%rsp), %ecx
	movq	72(%rsp), %r9           # 8-byte Reload
	jg	.LBB0_419
# BB#88:                                #   in Loop: Header=BB0_31 Depth=2
	cmpl	%ecx, 160(%rsp)         # 4-byte Folded Reload
	movl	%r13d, %r14d
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	96(%rsp), %r10d         # 4-byte Reload
	jle	.LBB0_90
	jmp	.LBB0_420
.LBB0_89:                               #   in Loop: Header=BB0_31 Depth=2
	movl	184(%rsp), %ecx
	cmpl	%ecx, %r14d
	movl	%r13d, %r14d
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	96(%rsp), %r10d         # 4-byte Reload
	jg	.LBB0_420
.LBB0_90:                               #   in Loop: Header=BB0_31 Depth=2
	addl	120(%rsp), %ebp         # 4-byte Folded Reload
	subl	%ebx, %ebp
	addl	%eax, %ebp
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	(%rbp,%rax), %eax
	cmpl	188(%rsp), %eax
	jg	.LBB0_420
# BB#91:                                #   in Loop: Header=BB0_31 Depth=2
	cmpl	192(%rsp), %ebp
	jg	.LBB0_420
# BB#92:                                #   in Loop: Header=BB0_31 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_31 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 160(%rax)
	jle	.LBB0_420
.LBB0_94:                               #   in Loop: Header=BB0_31 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	testb	$32, 42(%rax)
	movq	56(%rsp), %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	48(%rax,%rcx,4), %ebx
	jne	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_31 Depth=2
	cmpl	%ebx, %r14d
	movl	%ebx, %r13d
	cmovgel	%r14d, %r13d
	movl	56(%rax,%rcx,4), %ebx
	jmp	.LBB0_97
.LBB0_96:                               #   in Loop: Header=BB0_31 Depth=2
	addl	56(%rax,%rcx,4), %ebx
	xorl	%r13d, %r13d
.LBB0_97:                               #   in Loop: Header=BB0_31 Depth=2
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	cmpl	304(%rsp), %r13d
	jg	.LBB0_414
# BB#98:                                #   in Loop: Header=BB0_31 Depth=2
	leal	(%rbx,%r13), %ecx
	cmpl	308(%rsp), %ecx
	jg	.LBB0_414
# BB#99:                                #   in Loop: Header=BB0_31 Depth=2
	cmpl	312(%rsp), %ebx
	jg	.LBB0_414
# BB#100:                               #   in Loop: Header=BB0_31 Depth=2
	testq	%rdi, %rdi
	movq	%rax, 288(%rsp)
	movl	$1, 108(%rsp)           # 4-byte Folded Spill
	je	.LBB0_73
# BB#101:                               #   in Loop: Header=BB0_31 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	decl	160(%rax)
	jmp	.LBB0_73
.LBB0_102:                              #   in Loop: Header=BB0_31 Depth=2
	cmpq	$0, 88(%rsp)
	jne	.LBB0_143
# BB#103:                               #   in Loop: Header=BB0_31 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_141
# BB#104:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_142
.LBB0_105:                              #   in Loop: Header=BB0_31 Depth=2
	movzbl	%cl, %edi
	movq	no_fpos(%rip), %rbx
	movl	%r14d, 68(%rsp)         # 4-byte Spill
	movq	%r15, %r14
	movl	%r10d, %r15d
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	%r15d, %eax
	movq	%r14, %r15
	movl	68(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_82
.LBB0_106:                              #   in Loop: Header=BB0_31 Depth=2
	movl	40(%r9), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%rdi), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rdi)
	movq	%rdi, 232(%rsp)
	testw	%r8w, %r8w
	je	.LBB0_117
# BB#107:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%r15, %rax
	jmp	.LBB0_119
.LBB0_108:                              #   in Loop: Header=BB0_31 Depth=2
	movq	8(%rdi), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_109:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB0_109
# BB#110:                               #   in Loop: Header=BB0_31 Depth=2
	movq	24(%rcx), %rbx
	cmpq	16(%rcx), %rbx
	je	.LBB0_123
	.p2align	4, 0x90
.LBB0_111:                              # %.preheader1017
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_111
# BB#112:                               # %.preheader1017
                                        #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	cmpb	$124, %al
	je	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_114:                              # %.loopexit1018
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	CheckComponentOrder
	cmpl	$157, %eax
	je	.LBB0_125
# BB#115:                               # %.loopexit1018
                                        #   in Loop: Header=BB0_31 Depth=2
	cmpl	$155, %eax
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	96(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_445
# BB#116:                               # %.loopexit1018
                                        #   in Loop: Header=BB0_31 Depth=2
	cmpl	$156, %eax
	movl	%r10d, %eax
	jne	.LBB0_82
	jmp	.LBB0_165
.LBB0_117:                              #   in Loop: Header=BB0_31 Depth=2
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	je	.LBB0_120
# BB#118:                               #   in Loop: Header=BB0_31 Depth=2
	cmpb	$0, 44(%rdi)
	movl	156(%rsp), %eax         # 4-byte Reload
	cmovnsl	84(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movl	152(%rsp), %eax         # 4-byte Reload
	cmovnsl	%r14d, %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movl	148(%rsp), %eax         # 4-byte Reload
	cmovnsl	120(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 148(%rsp)         # 4-byte Spill
	movl	144(%rsp), %eax         # 4-byte Reload
	cmovnsl	160(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	cmovnsq	%r15, %rax
.LBB0_119:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, 136(%rsp)         # 8-byte Spill
.LBB0_120:                              #   in Loop: Header=BB0_31 Depth=2
	testb	$2, 45(%rdi)
	jne	.LBB0_122
# BB#121:                               #   in Loop: Header=BB0_31 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	orb	$32, 42(%rax)
.LBB0_122:                              #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %eax
	jmp	.LBB0_82
.LBB0_123:                              #   in Loop: Header=BB0_31 Depth=2
	movq	(%r15), %r15
	movq	8(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_153
# BB#124:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_154
.LBB0_125:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	96(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_132
	.p2align	4, 0x90
.LBB0_126:                              # %.lr.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_126 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB0_128:                              #   in Loop: Header=BB0_126 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_130
# BB#129:                               #   in Loop: Header=BB0_126 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_130:                              #   in Loop: Header=BB0_126 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB0_126
# BB#131:                               # %..preheader1016_crit_edge
                                        #   in Loop: Header=BB0_31 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB0_132:                              # %.preheader1016
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB0_139
	.p2align	4, 0x90
.LBB0_133:                              # %.lr.ph1203
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_135
# BB#134:                               #   in Loop: Header=BB0_133 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB0_135:                              #   in Loop: Header=BB0_133 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_137
# BB#136:                               #   in Loop: Header=BB0_133 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_137:                              #   in Loop: Header=BB0_133 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB0_133
# BB#138:                               # %._crit_edge1204
                                        #   in Loop: Header=BB0_31 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB0_139:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	(%r15), %r15
	movq	8(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_158
# BB#140:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_159
.LBB0_141:                              #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r10d, %ebx
	callq	GetMemory
	movl	%ebx, %r10d
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB0_142:                              #   in Loop: Header=BB0_31 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 88(%rsp)
.LBB0_143:                              #   in Loop: Header=BB0_31 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_145
# BB#144:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_146
.LBB0_145:                              #   in Loop: Header=BB0_31 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r10d, %ebx
	callq	GetMemory
	movl	%ebx, %r10d
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB0_146:                              #   in Loop: Header=BB0_31 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	88(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_149
# BB#147:                               #   in Loop: Header=BB0_31 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_149
# BB#148:                               #   in Loop: Header=BB0_31 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_149:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, zz_res(%rip)
	movq	56(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_152
# BB#150:                               #   in Loop: Header=BB0_31 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_152
# BB#151:                               #   in Loop: Header=BB0_31 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_152:                              #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %eax
	jmp	.LBB0_82
.LBB0_153:                              #   in Loop: Header=BB0_31 Depth=2
	xorl	%ecx, %ecx
.LBB0_154:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_156
# BB#155:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_156:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB0_163
# BB#157:                               #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %eax
	jmp	.LBB0_82
.LBB0_158:                              #   in Loop: Header=BB0_31 Depth=2
	xorl	%ecx, %ecx
.LBB0_159:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_161
# BB#160:                               #   in Loop: Header=BB0_31 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_161:                              #   in Loop: Header=BB0_31 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB0_163
# BB#162:                               #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %eax
	jmp	.LBB0_82
.LBB0_163:                              #   in Loop: Header=BB0_31 Depth=2
	movl	%r10d, %ebx
	callq	DisposeObject
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	%ebx, %eax
	jmp	.LBB0_82
	.p2align	4, 0x90
.LBB0_164:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, 56(%rsp)
.LBB0_165:                              # %.loopexit1030
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_167
# BB#166:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
.LBB0_167:                              #   in Loop: Header=BB0_4 Depth=1
	movq	136(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB0_170
# BB#168:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	Promote
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	je	.LBB0_170
# BB#169:                               #   in Loop: Header=BB0_4 Depth=1
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movl	144(%rsp), %esi         # 4-byte Reload
	movl	148(%rsp), %edx         # 4-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
	movq	%rbx, %rdi
	movl	152(%rsp), %esi         # 4-byte Reload
	movl	156(%rsp), %edx         # 4-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
.LBB0_170:                              #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %rax
	cmpb	$121, 32(%rax)
	leaq	280(%rsp), %r15
	jne	.LBB0_244
# BB#171:                               #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_181
# BB#172:                               #   in Loop: Header=BB0_4 Depth=1
	movl	AllowCrossDb(%rip), %edx
	testl	%edx, %edx
	je	.LBB0_181
# BB#173:                               #   in Loop: Header=BB0_4 Depth=1
	movl	%r14d, 68(%rsp)         # 4-byte Spill
	movq	8(%rcx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_174:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r12
	leaq	16(%r12), %rax
	cmpb	$0, 32(%r12)
	je	.LBB0_174
# BB#175:                               #   in Loop: Header=BB0_4 Depth=1
	xorl	%edi, %edi
	callq	SwitchScope
	leaq	34(%r12), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	40(%r12), %rsi
	movl	36(%r12), %edx
	movzwl	34(%r12), %edi
	callq	ReadFromFile
	movq	%rax, %r14
	xorl	%edi, %edi
	callq	UnSwitchScope
	testq	%r14, %r14
	jne	.LBB0_177
# BB#176:                               #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %rbx
	addq	$32, %rbx
	movq	136(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %edi
	callq	FileName
	movq	%rax, %rbp
	movl	$20, %edi
	movl	$1, %esi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
.LBB0_177:                              #   in Loop: Header=BB0_4 Depth=1
	cmpb	$2, 32(%r14)
	je	.LBB0_179
# BB#178:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_179:                              #   in Loop: Header=BB0_4 Depth=1
	movzbl	zz_lengths+120(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_284
# BB#180:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_285
.LBB0_181:                              #   in Loop: Header=BB0_4 Depth=1
	movzwl	42(%rax), %ecx
	testb	$64, %cl
	je	.LBB0_244
# BB#182:                               #   in Loop: Header=BB0_4 Depth=1
	movl	AllowCrossDb(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_244
# BB#183:                               #   in Loop: Header=BB0_4 Depth=1
	movl	%r14d, 68(%rsp)         # 4-byte Spill
	movq	80(%rax), %rax
	movq	80(%rax), %rdi
	movq	%r13, %rsi
	callq	FirstExternTarget
	jmp	.LBB0_185
	.p2align	4, 0x90
.LBB0_184:                              #   in Loop: Header=BB0_185 Depth=2
	movq	56(%rsp), %rax
	movq	80(%rax), %rax
	movq	80(%rax), %rdi
	leaq	336(%rsp), %r13
	movq	%r13, %rsi
	callq	NextExternTarget
.LBB0_185:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_199 Depth 3
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_243
# BB#186:                               # %.lr.ph1292
                                        #   in Loop: Header=BB0_185 Depth=2
	movq	56(%rsp), %rax
	movq	80(%rax), %rsi
	addq	$32, %rsi
	movq	%r14, %rdi
	callq	GallTargEval
	movq	%rax, %rbx
	movzbl	zz_lengths+132(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_188
# BB#187:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_189
	.p2align	4, 0x90
.LBB0_188:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_189:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$-124, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rbx, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_191
# BB#190:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_192
	.p2align	4, 0x90
.LBB0_191:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_192:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	56(%rsp), %rcx
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_195
# BB#193:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_195
# BB#194:                               #   in Loop: Header=BB0_185 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_195:                              #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_198
# BB#196:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_198
# BB#197:                               #   in Loop: Header=BB0_185 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_198:                              #   in Loop: Header=BB0_185 Depth=2
	movq	(%rbx), %rbp
	.p2align	4, 0x90
.LBB0_199:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_185 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_199
# BB#200:                               #   in Loop: Header=BB0_185 Depth=2
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB0_202
# BB#201:                               #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_202:                              # %.loopexit1011
                                        #   in Loop: Header=BB0_185 Depth=2
	movq	OldCrossDb(%rip), %rdi
	addq	$64, %rbp
	leaq	256(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	%r15, 8(%rsp)
	leaq	264(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$1, %esi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	leaq	848(%rsp), %r8
	leaq	182(%rsp), %r9
	callq	DbRetrieve
	testl	%eax, %eax
	je	.LBB0_184
# BB#203:                               #   in Loop: Header=BB0_185 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 96(%rax)
	jne	.LBB0_208
# BB#204:                               #   in Loop: Header=BB0_185 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_206
# BB#205:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_207
.LBB0_206:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_207:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 96(%rcx)
.LBB0_208:                              #   in Loop: Header=BB0_185 Depth=2
	movzbl	zz_lengths+147(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_210
# BB#209:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_211
	.p2align	4, 0x90
.LBB0_210:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_211:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$-109, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	182(%rsp), %eax
	movw	%ax, 34(%rbx)
	movq	264(%rsp), %rax
	movq	%rax, 40(%rbx)
	movl	280(%rsp), %eax
	movl	%eax, 36(%rbx)
	movq	%r14, 56(%rbx)
	movq	256(%rsp), %rax
	movq	%rax, 48(%rbx)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%rbp, %rsi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_213
# BB#212:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_214
	.p2align	4, 0x90
.LBB0_213:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_214:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_217
# BB#215:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_217
# BB#216:                               #   in Loop: Header=BB0_185 Depth=2
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_217:                              #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_220
# BB#218:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_220
# BB#219:                               #   in Loop: Header=BB0_185 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_220:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	leaq	848(%rsp), %rsi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_222
# BB#221:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_223
	.p2align	4, 0x90
.LBB0_222:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_223:                              #   in Loop: Header=BB0_185 Depth=2
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_226
# BB#224:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_226
# BB#225:                               #   in Loop: Header=BB0_185 Depth=2
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_226:                              #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_229
# BB#227:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_229
# BB#228:                               #   in Loop: Header=BB0_185 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_229:                              #   in Loop: Header=BB0_185 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_231
# BB#230:                               #   in Loop: Header=BB0_185 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_232
	.p2align	4, 0x90
.LBB0_231:                              #   in Loop: Header=BB0_185 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_232:                              #   in Loop: Header=BB0_185 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_235
# BB#233:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_185 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_235:                              #   in Loop: Header=BB0_185 Depth=2
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_184
# BB#236:                               #   in Loop: Header=BB0_185 Depth=2
	testq	%rax, %rax
	je	.LBB0_184
# BB#237:                               #   in Loop: Header=BB0_185 Depth=2
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB0_184
.LBB0_238:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testb	$16, 43(%rax)
	jne	.LBB0_279
# BB#239:                               #   in Loop: Header=BB0_4 Depth=1
	movq	80(%r12), %rax
	movq	80(%rax), %rax
	cmpq	ForceGalleySym(%rip), %rax
	je	.LBB0_279
# BB#240:                               #   in Loop: Header=BB0_4 Depth=1
	movb	$1, %al
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_242
# BB#241:                               #   in Loop: Header=BB0_4 Depth=1
	movb	42(%r12), %al
	andb	$32, %al
	shrb	$5, %al
.LBB0_242:                              #   in Loop: Header=BB0_4 Depth=1
	movzbl	%al, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	jmp	.LBB0_282
.LBB0_243:                              # %._crit_edge1293
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %rax
	andw	$-65, 42(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	$0, 96(%rcx)
	movl	68(%rsp), %r14d         # 4-byte Reload
	jne	.LBB0_4
	.p2align	4, 0x90
.LBB0_244:                              #   in Loop: Header=BB0_4 Depth=1
	cmpb	$121, 32(%rax)
	jne	.LBB0_246
# BB#245:                               #   in Loop: Header=BB0_4 Depth=1
	testb	$1, 42(%rax)
	jne	.LBB0_265
.LBB0_246:                              #   in Loop: Header=BB0_4 Depth=1
	cmpb	$122, 32(%rax)
	jne	.LBB0_545
# BB#247:                               #   in Loop: Header=BB0_4 Depth=1
	testb	$1, 42(%rax)
	je	.LBB0_545
# BB#248:                               #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rax), %rbx
	cmpq	%rax, %rbx
	je	.LBB0_253
	.p2align	4, 0x90
.LBB0_249:                              # %.preheader1025
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_249
# BB#250:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	$0, 104(%rbx)
	je	.LBB0_252
# BB#251:                               #   in Loop: Header=BB0_4 Depth=1
	movq	80(%rax), %rsi
	movq	%rbx, %rdi
	callq	GazumpOptimize
.LBB0_252:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	DetachGalley
	jmp	.LBB0_4
.LBB0_253:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, xx_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_254:                              # %.lr.ph1302
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_256
# BB#255:                               #   in Loop: Header=BB0_254 Depth=2
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_256:                              #   in Loop: Header=BB0_254 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_258
# BB#257:                               #   in Loop: Header=BB0_254 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_258:                              #   in Loop: Header=BB0_254 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %eax
	movq	zz_free(,%rax,8), %rdx
	movq	%rdx, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_hold(%rip), %rdx
	movq	24(%rdx), %rcx
	cmpq	%rdx, %rcx
	jne	.LBB0_254
# BB#259:                               # %.preheader1024
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%eax, zz_size(%rip)
	movq	8(%rdx), %rcx
	cmpq	%rdx, %rcx
	movq	%rcx, %rax
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_260:                              # %.lr.ph1304
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_262
# BB#261:                               #   in Loop: Header=BB0_260 Depth=2
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_262:                              #   in Loop: Header=BB0_260 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_264
# BB#263:                               #   in Loop: Header=BB0_260 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_264:                              #   in Loop: Header=BB0_260 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movq	zz_free(,%rdx,8), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_260
	jmp	.LBB0_278
.LBB0_265:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, xx_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_272
	.p2align	4, 0x90
.LBB0_266:                              # %.lr.ph1295
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_268
# BB#267:                               #   in Loop: Header=BB0_266 Depth=2
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_268:                              #   in Loop: Header=BB0_266 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_270
# BB#269:                               #   in Loop: Header=BB0_266 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_270:                              #   in Loop: Header=BB0_266 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movq	zz_free(,%rdx,8), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %rax
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_266
# BB#271:                               # %..preheader1026_crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, zz_size(%rip)
.LBB0_272:                              # %.preheader1026
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_273:                              # %.lr.ph1297
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_275
# BB#274:                               #   in Loop: Header=BB0_273 Depth=2
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_275:                              #   in Loop: Header=BB0_273 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_277
# BB#276:                               #   in Loop: Header=BB0_273 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_277:                              #   in Loop: Header=BB0_273 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movq	zz_free(,%rdx,8), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_273
.LBB0_278:                              # %._crit_edge1298
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, zz_size(%rip)
	jmp	.LBB0_3
.LBB0_279:                              #   in Loop: Header=BB0_4 Depth=1
	movq	24(%r12), %rsi
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB0_280:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_280
# BB#281:                               #   in Loop: Header=BB0_4 Depth=1
	movzwl	42(%r12), %eax
	testb	$1, %al
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	$1, %ecx
	cmovel	%ecx, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %r8
	leaq	88(%rsp), %rdx
	movq	%rsi, %rcx
	callq	FreeGalley
.LBB0_282:                              #   in Loop: Header=BB0_4 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
.LBB0_283:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%esi, %esi
	callq	FlushInners
	jmp	.LBB0_4
.LBB0_284:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_285:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$120, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	$0, 88(%rbp)
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_287
# BB#286:                               #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_288
.LBB0_287:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, zz_hold(%rip)
.LBB0_288:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movb	$8, 32(%rsi)
	movq	%rsi, 24(%rsi)
	movq	%rsi, 16(%rsi)
	movq	%rsi, 8(%rsi)
	movq	%rsi, (%rsi)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rsi)
	movl	36(%r14), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rsi), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rsi)
	movl	36(%r14), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rsi)
	movq	80(%r14), %rax
	movq	%rax, 80(%rsi)
	movq	$0, 128(%rsi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rsi)
	movq	80(%r14), %rax
	movzbl	43(%rax), %eax
	movzwl	42(%rsi), %ecx
	andl	$32, %eax
	andl	$65149, %ecx            # imm = 0xFE7D
	movq	$0, 96(%rsi)
	leal	128(%rcx,%rax,8), %eax
	movw	%ax, 42(%rsi)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_290
# BB#289:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_291
.LBB0_290:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_291:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_294
# BB#292:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_294
# BB#293:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_294:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_297
# BB#295:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_297
# BB#296:                               #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_297:                              #   in Loop: Header=BB0_4 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_299
# BB#298:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_300
.LBB0_299:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_300:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_303
# BB#301:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_303
# BB#302:                               #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_303:                              #   in Loop: Header=BB0_4 Depth=1
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB0_306
# BB#304:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_306
# BB#305:                               #   in Loop: Header=BB0_4 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_306:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	SetTarget
	movb	$-127, 40(%rbx)
	movq	80(%rbx), %rax
	testb	$32, 126(%rax)
	jne	.LBB0_308
# BB#307:                               #   in Loop: Header=BB0_4 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_309
.LBB0_308:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	BuildEnclose
.LBB0_309:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 136(%rcx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rcx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_311
# BB#310:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_312
.LBB0_311:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_312:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	56(%rsp), %rcx
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_315
# BB#313:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_315
# BB#314:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_315:                              #   in Loop: Header=BB0_4 Depth=1
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB0_318
# BB#316:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_318
# BB#317:                               #   in Loop: Header=BB0_4 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_318:                              #   in Loop: Header=BB0_4 Depth=1
	leaq	40(%r12), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	leaq	36(%r12), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	8(%r12), %r15
	.p2align	4, 0x90
.LBB0_319:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %r15
	cmpb	$0, 32(%r15)
	je	.LBB0_319
# BB#320:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%r12), %r14
	leaq	336(%rsp), %r13
	.p2align	4, 0x90
.LBB0_321:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB0_321
# BB#322:                               # %.preheader1023
                                        #   in Loop: Header=BB0_4 Depth=1
	leaq	48(%r12), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	64(%r15), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	64(%r14), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 216(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_323:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	OldCrossDb(%rip), %rdi
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	%rax, 16(%rsp)
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%rax, 8(%rsp)
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	leaq	256(%rsp), %rsi
	leaq	264(%rsp), %rdx
	leaq	848(%rsp), %rcx
	movq	%r13, %r8
	movq	136(%rsp), %r9          # 8-byte Reload
	callq	DbRetrieveNext
	testl	%eax, %eax
	je	.LBB0_367
# BB#324:                               #   in Loop: Header=BB0_323 Depth=2
	movl	256(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_367
# BB#325:                               #   in Loop: Header=BB0_323 Depth=2
	movq	264(%rsp), %rax
	cmpq	56(%r12), %rax
	jne	.LBB0_367
# BB#326:                               #   in Loop: Header=BB0_323 Depth=2
	leaq	848(%rsp), %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_367
# BB#327:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_364
# BB#328:                               #   in Loop: Header=BB0_323 Depth=2
	xorl	%edi, %edi
	callq	SwitchScope
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	224(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	movq	136(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %edi
	callq	ReadFromFile
	movq	%rax, %rbp
	xorl	%edi, %edi
	callq	UnSwitchScope
	testq	%rbp, %rbp
	jne	.LBB0_330
# BB#329:                               #   in Loop: Header=BB0_323 Depth=2
	movq	56(%rsp), %rbx
	addq	$32, %rbx
	movq	136(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %edi
	movq	%rbp, %r13
	callq	FileName
	movq	%rax, %rbp
	movl	$20, %edi
	movl	$2, %esi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	leaq	336(%rsp), %rbx
	callq	Error
	movq	%r13, %rbp
	movq	%rbx, %r13
.LBB0_330:                              #   in Loop: Header=BB0_323 Depth=2
	cmpb	$2, 32(%rbp)
	je	.LBB0_332
# BB#331:                               #   in Loop: Header=BB0_323 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_332:                              #   in Loop: Header=BB0_323 Depth=2
	movq	80(%rbp), %rax
	testb	$8, 126(%rax)
	jne	.LBB0_334
# BB#333:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%rbp, %rdi
	callq	DisposeObject
	jmp	.LBB0_364
.LBB0_334:                              #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpb	$17, 32(%rax)
	jne	.LBB0_336
# BB#335:                               #   in Loop: Header=BB0_323 Depth=2
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_354
.LBB0_336:                              #   in Loop: Header=BB0_323 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB0_338
# BB#337:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_339
.LBB0_338:                              #   in Loop: Header=BB0_323 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB0_339:                              #   in Loop: Header=BB0_323 Depth=2
	movb	$17, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_341
# BB#340:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB0_341:                              #   in Loop: Header=BB0_323 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB0_344
# BB#342:                               #   in Loop: Header=BB0_323 Depth=2
	testq	%rax, %rax
	je	.LBB0_344
# BB#343:                               #   in Loop: Header=BB0_323 Depth=2
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_344:                              #   in Loop: Header=BB0_323 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_346
# BB#345:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_347
.LBB0_346:                              #   in Loop: Header=BB0_323 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_347:                              #   in Loop: Header=BB0_323 Depth=2
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	movq	216(%rsp), %rsi         # 8-byte Reload
	je	.LBB0_350
# BB#348:                               #   in Loop: Header=BB0_323 Depth=2
	testq	%rax, %rax
	je	.LBB0_350
# BB#349:                               #   in Loop: Header=BB0_323 Depth=2
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_350:                              #   in Loop: Header=BB0_323 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB0_353
# BB#351:                               #   in Loop: Header=BB0_323 Depth=2
	testq	%rax, %rax
	je	.LBB0_353
# BB#352:                               #   in Loop: Header=BB0_323 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rdi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_353:                              #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, 216(%rsp)         # 8-byte Spill
.LBB0_354:                              #   in Loop: Header=BB0_323 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_356
# BB#355:                               #   in Loop: Header=BB0_323 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_357
.LBB0_356:                              #   in Loop: Header=BB0_323 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_357:                              #   in Loop: Header=BB0_323 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB0_360
# BB#358:                               #   in Loop: Header=BB0_323 Depth=2
	testq	%rax, %rax
	je	.LBB0_360
# BB#359:                               #   in Loop: Header=BB0_323 Depth=2
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_360:                              #   in Loop: Header=BB0_323 Depth=2
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB0_363
# BB#361:                               #   in Loop: Header=BB0_323 Depth=2
	testq	%rax, %rax
	je	.LBB0_363
# BB#362:                               #   in Loop: Header=BB0_323 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_363:                              # %.thread961
                                        #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB0_364:                              # %.thread961
                                        #   in Loop: Header=BB0_323 Depth=2
	movq	%r13, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_323
# BB#365:                               # %.critedge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	24(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_380
# BB#366:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_381
.LBB0_367:                              # %.critedge885
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	24(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_369
# BB#368:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_370
.LBB0_369:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%ecx, %ecx
.LBB0_370:                              #   in Loop: Header=BB0_4 Depth=1
	movl	68(%rsp), %r14d         # 4-byte Reload
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_372
# BB#371:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_372:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_374
# BB#373:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
.LBB0_374:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	96(%rax), %rax
	cmpq	%rax, 8(%rax)
	jne	.LBB0_411
# BB#375:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, 96(%rax)
	jmp	.LBB0_411
.LBB0_376:                              # %.loopexit1019..thread_crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r15), %r14
.LBB0_377:                              # %.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	Promote
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	je	.LBB0_379
# BB#378:                               #   in Loop: Header=BB0_4 Depth=1
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	movq	160(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %edx
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
	movq	%r14, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
.LBB0_379:                              # %.critedge849
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	88(%rsp), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	FlushInners
	movl	%r13d, %r14d
	movl	%ebx, %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	336(%rsp), %r13
	jmp	.LBB0_4
.LBB0_380:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%ecx, %ecx
.LBB0_381:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_383
# BB#382:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_383:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_385
# BB#384:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
.LBB0_385:                              #   in Loop: Header=BB0_4 Depth=1
	movq	24(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_387
# BB#386:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_388
.LBB0_387:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%ecx, %ecx
.LBB0_388:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_390
# BB#389:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_390:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_392
# BB#391:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
.LBB0_392:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	leaq	848(%rsp), %rsi
	callq	MakeWord
	movq	%rax, %rbp
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	leaq	336(%rsp), %rsi
	callq	MakeWord
	movq	%rax, %r14
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_394
# BB#393:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_395
.LBB0_394:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_395:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB0_398
# BB#396:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_398
# BB#397:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_398:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_401
# BB#399:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_4 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_401:                              #   in Loop: Header=BB0_4 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_403
# BB#402:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_404
.LBB0_403:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_404:                              #   in Loop: Header=BB0_4 Depth=1
	leaq	336(%rsp), %r13
	testq	%r12, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB0_407
# BB#405:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_407
# BB#406:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_407:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_410
# BB#408:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_410
# BB#409:                               #   in Loop: Header=BB0_4 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_410:                              #   in Loop: Header=BB0_4 Depth=1
	movl	68(%rsp), %r14d         # 4-byte Reload
.LBB0_411:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpb	$17, 32(%rax)
	jne	.LBB0_413
# BB#412:                               #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	ConvertGalleyList
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_413:                              #   in Loop: Header=BB0_4 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	FlushGalley
	jmp	.LBB0_4
.LBB0_414:                              #   in Loop: Header=BB0_4 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_416
# BB#415:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, 104(%rax)
.LBB0_416:                              #   in Loop: Header=BB0_4 Depth=1
	cmpw	$0, 128(%rsp)           # 2-byte Folded Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_418
# BB#417:                               #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rsp), %r8
	addq	$32, %r8
	movl	$20, %edi
	movl	$3, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	72(%rsp), %r9           # 8-byte Reload
.LBB0_418:                              #   in Loop: Header=BB0_4 Depth=1
	movl	96(%rsp), %r10d         # 4-byte Reload
	jmp	.LBB0_448
.LBB0_419:                              #   in Loop: Header=BB0_4 Depth=1
	movl	%r13d, %r14d
	movl	96(%rsp), %r10d         # 4-byte Reload
.LBB0_420:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 104(%rax)
	je	.LBB0_447
# BB#421:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	zz_lengths+26(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	movq	112(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_423
# BB#422:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_424
.LBB0_423:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movl	96(%rsp), %r10d         # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
	movl	184(%rsp), %ecx
.LBB0_424:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$26, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movl	%ecx, 64(%rbx)
	movl	188(%rsp), %eax
	movl	%eax, 68(%rbx)
	movl	192(%rsp), %eax
	movl	%eax, 72(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_426
# BB#425:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_427
.LBB0_426:                              #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movl	96(%rsp), %r10d         # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB0_427:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	112(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_430
# BB#428:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_430
# BB#429:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_430:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_433
# BB#431:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rax, %rax
	je	.LBB0_433
# BB#432:                               #   in Loop: Header=BB0_4 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_433:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	120(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_439
# BB#434:                               #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_439
# BB#435:                               #   in Loop: Header=BB0_4 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_436:                              #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB0_436
# BB#437:                               #   in Loop: Header=BB0_4 Depth=1
	movl	48(%rcx), %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	160(%rsi), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 160(%rsi)
	movq	24(%rcx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_440
# BB#438:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_441
.LBB0_439:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$65535, 160(%rax)       # imm = 0xFFFF
	jmp	.LBB0_448
.LBB0_440:                              #   in Loop: Header=BB0_4 Depth=1
	xorl	%ecx, %ecx
.LBB0_441:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_443
# BB#442:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_443:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_448
# BB#444:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
	movl	96(%rsp), %r10d         # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_448
.LBB0_445:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_447
# BB#446:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
	movl	96(%rsp), %r10d         # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, 104(%rax)
.LBB0_447:                              #   in Loop: Header=BB0_4 Depth=1
	movq	112(%rsp), %rbp         # 8-byte Reload
.LBB0_448:                              #   in Loop: Header=BB0_4 Depth=1
	movq	80(%r9), %rax
	cmpq	PrintSym(%rip), %rax
	jne	.LBB0_450
# BB#449:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	movl	%r10d, %ebx
	callq	Error
	movl	%ebx, %r10d
.LBB0_450:                              #   in Loop: Header=BB0_4 Depth=1
	movl	%r10d, %ebx
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_452
# BB#451:                               #   in Loop: Header=BB0_4 Depth=1
	callq	DisposeObject
.LBB0_452:                              #   in Loop: Header=BB0_4 Depth=1
	movl	%r14d, 68(%rsp)         # 4-byte Spill
	movq	136(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB0_455
# BB#453:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	Promote
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	je	.LBB0_455
# BB#454:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, %rdi
	movl	144(%rsp), %esi         # 4-byte Reload
	movl	148(%rsp), %edx         # 4-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
	movq	%rbp, %rdi
	movl	152(%rsp), %esi         # 4-byte Reload
	movl	156(%rsp), %edx         # 4-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
.LBB0_455:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	cmpw	$0, 40(%rsp)            # 2-byte Folded Reload
	jne	.LBB0_484
# BB#456:                               #   in Loop: Header=BB0_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_484
# BB#457:                               # %.preheader1028
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	cmpq	%r15, %rax
	je	.LBB0_484
# BB#458:                               # %.preheader1014.lr.ph.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpw	$0, 128(%rsp)           # 2-byte Folded Reload
	je	.LBB0_472
# BB#459:                               #   in Loop: Header=BB0_4 Depth=1
	movq	240(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_460:                              # %.preheader1014
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_461 Depth 3
                                        #       Child Loop BB0_464 Depth 3
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB0_461:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_460 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_461
# BB#462:                               #   in Loop: Header=BB0_460 Depth=2
	cmpb	$9, %dl
	jne	.LBB0_465
# BB#463:                               #   in Loop: Header=BB0_460 Depth=2
	movq	(%rcx), %rsi
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB0_464:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_460 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rbx
	movzbl	32(%rbx), %edx
	leaq	16(%rbx), %rsi
	testb	%dl, %dl
	je	.LBB0_464
	jmp	.LBB0_466
.LBB0_465:                              # %.loopexit1013.loopexit2151
                                        #   in Loop: Header=BB0_460 Depth=2
	movq	%rcx, %rbx
.LBB0_466:                              # %.loopexit1013
                                        #   in Loop: Header=BB0_460 Depth=2
	andb	$-4, %dl
	cmpb	$20, %dl
	jne	.LBB0_470
# BB#467:                               #   in Loop: Header=BB0_460 Depth=2
	cmpq	%rbx, %rcx
	je	.LBB0_469
# BB#468:                               #   in Loop: Header=BB0_460 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_469:                              #   in Loop: Header=BB0_460 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	HandleHeader
	movq	(%rbp), %rax
	cmpq	%r15, %rax
	jne	.LBB0_460
	jmp	.LBB0_484
.LBB0_470:                              # %.outer
                                        #   in Loop: Header=BB0_460 Depth=2
	movq	%rax, %rbp
	movq	8(%rax), %rax
	addq	$8, %rbp
	cmpq	%r15, %rax
	jne	.LBB0_460
	jmp	.LBB0_484
.LBB0_472:                              # %.preheader1014.lr.ph.us.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	240(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_473:                              # %.preheader1014.us.us
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_474 Depth 3
                                        #       Child Loop BB0_477 Depth 3
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB0_474:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_473 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_474
# BB#475:                               #   in Loop: Header=BB0_473 Depth=2
	cmpb	$9, %dl
	jne	.LBB0_478
# BB#476:                               #   in Loop: Header=BB0_473 Depth=2
	movq	8(%rcx), %rsi
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB0_477:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_473 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rbx
	movzbl	32(%rbx), %edx
	testb	%dl, %dl
	leaq	16(%rbx), %rsi
	je	.LBB0_477
	jmp	.LBB0_479
.LBB0_478:                              # %.loopexit1013.us.us.loopexit2150
                                        #   in Loop: Header=BB0_473 Depth=2
	movq	%rcx, %rbx
.LBB0_479:                              # %.loopexit1013.us.us
                                        #   in Loop: Header=BB0_473 Depth=2
	andb	$-4, %dl
	cmpb	$20, %dl
	jne	.LBB0_471
# BB#480:                               #   in Loop: Header=BB0_473 Depth=2
	cmpq	%rbx, %rcx
	je	.LBB0_482
# BB#481:                               #   in Loop: Header=BB0_473 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_482:                              #   in Loop: Header=BB0_473 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	HandleHeader
	movq	(%rbp), %rax
	cmpq	%r15, %rax
	jne	.LBB0_473
	jmp	.LBB0_484
.LBB0_471:                              # %.us-lcssa.us.us
                                        #   in Loop: Header=BB0_473 Depth=2
	movq	%rax, %rbp
	movq	8(%rax), %rax
	addq	$8, %rbp
	cmpq	%r15, %rax
	jne	.LBB0_473
.LBB0_484:                              # %.loopexit1029
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	144(%rax), %rax
	testq	%rax, %rax
	leaq	336(%rsp), %r13
	je	.LBB0_521
# BB#485:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rax, 8(%rax)
	jne	.LBB0_487
# BB#486:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_487:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	jne	.LBB0_489
# BB#488:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_489:                              #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	144(%rax), %rax
	movq	8(%rax), %rbx
	cmpq	%rax, %rbx
	je	.LBB0_521
# BB#490:                               # %.lr.ph1280
                                        #   in Loop: Header=BB0_4 Depth=1
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB0_506
	.p2align	4, 0x90
.LBB0_491:                              # %.lr.ph1280.split
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_493 Depth 3
	leaq	16(%rbx), %rax
	jmp	.LBB0_493
	.p2align	4, 0x90
.LBB0_492:                              #   in Loop: Header=BB0_493 Depth=3
	addq	$16, %rax
.LBB0_493:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_491 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_492
# BB#494:                               #   in Loop: Header=BB0_491 Depth=2
	addb	$-15, %cl
	cmpb	$2, %cl
	jae	.LBB0_496
# BB#495:                               #   in Loop: Header=BB0_491 Depth=2
	movq	%rax, 56(%rsp)
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_497
.LBB0_496:                              # %.loopexit1012.loopexit
                                        #   in Loop: Header=BB0_491 Depth=2
	movq	%rax, 56(%rsp)
.LBB0_497:                              # %.loopexit1012
                                        #   in Loop: Header=BB0_491 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_499
# BB#498:                               #   in Loop: Header=BB0_491 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_500
.LBB0_499:                              #   in Loop: Header=BB0_491 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_500:                              #   in Loop: Header=BB0_491 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_502
# BB#501:                               #   in Loop: Header=BB0_491 Depth=2
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_502:                              #   in Loop: Header=BB0_491 Depth=2
	movq	%rax, zz_res(%rip)
	movq	56(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_505
# BB#503:                               #   in Loop: Header=BB0_491 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_505
# BB#504:                               #   in Loop: Header=BB0_491 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_505:                              #   in Loop: Header=BB0_491 Depth=2
	incl	%ebp
	movq	8(%rbx), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	144(%rax), %rbx
	jne	.LBB0_491
	jmp	.LBB0_519
	.p2align	4, 0x90
.LBB0_506:                              # %.lr.ph1280.split.us
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_508 Depth 3
	leaq	16(%rbx), %rax
	jmp	.LBB0_508
	.p2align	4, 0x90
.LBB0_507:                              #   in Loop: Header=BB0_508 Depth=3
	addq	$16, %rax
.LBB0_508:                              #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_506 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_507
# BB#509:                               #   in Loop: Header=BB0_506 Depth=2
	addb	$-15, %cl
	cmpb	$1, %cl
	ja	.LBB0_511
# BB#510:                               #   in Loop: Header=BB0_506 Depth=2
	movq	%rax, 56(%rsp)
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_512
.LBB0_511:                              # %.loopexit1012.us.loopexit
                                        #   in Loop: Header=BB0_506 Depth=2
	movq	%rax, 56(%rsp)
.LBB0_512:                              # %.loopexit1012.us
                                        #   in Loop: Header=BB0_506 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_514
# BB#513:                               #   in Loop: Header=BB0_506 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_515
.LBB0_514:                              #   in Loop: Header=BB0_506 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_515:                              #   in Loop: Header=BB0_506 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	56(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_518
# BB#516:                               #   in Loop: Header=BB0_506 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_518
# BB#517:                               #   in Loop: Header=BB0_506 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_518:                              #   in Loop: Header=BB0_506 Depth=2
	incl	%ebp
	movq	8(%rbx), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	144(%rax), %rbx
	jne	.LBB0_506
.LBB0_519:                              # %._crit_edge1281
                                        #   in Loop: Header=BB0_4 Depth=1
	testb	$1, %bpl
	leaq	336(%rsp), %r13
	je	.LBB0_521
# BB#520:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_521:                              # %._crit_edge1281.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	DetachGalley
	cmpb	$122, 32(%r12)
	je	.LBB0_523
# BB#522:                               #   in Loop: Header=BB0_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_523:                              #   in Loop: Header=BB0_4 Depth=1
	movb	$1, %al
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	movl	68(%rsp), %r14d         # 4-byte Reload
	jne	.LBB0_525
# BB#524:                               #   in Loop: Header=BB0_4 Depth=1
	movb	42(%r12), %al
	andb	$32, %al
	shrb	$5, %al
.LBB0_525:                              #   in Loop: Header=BB0_4 Depth=1
	movq	%r12, xx_hold(%rip)
	movq	24(%r12), %rcx
	cmpq	%r12, %rcx
	je	.LBB0_532
	.p2align	4, 0x90
.LBB0_526:                              # %.lr.ph1284
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB0_528
# BB#527:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_528:                              #   in Loop: Header=BB0_526 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB0_530
# BB#529:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB0_530:                              #   in Loop: Header=BB0_526 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %r12
	movq	24(%r12), %rcx
	cmpq	%r12, %rcx
	jne	.LBB0_526
# BB#531:                               # %..preheader1027_crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, zz_size(%rip)
.LBB0_532:                              # %.preheader1027
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%r12), %rcx
	cmpq	%r12, %rcx
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_533:                              # %.lr.ph1286
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB0_535
# BB#534:                               #   in Loop: Header=BB0_533 Depth=2
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB0_535:                              #   in Loop: Header=BB0_533 Depth=2
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB0_537
# BB#536:                               #   in Loop: Header=BB0_533 Depth=2
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB0_537:                              #   in Loop: Header=BB0_533 Depth=2
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %r12
	movq	8(%r12), %rcx
	cmpq	%r12, %rcx
	jne	.LBB0_533
	jmp	.LBB0_1
.LBB0_83:                               # %._crit_edge1240
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#84:
	callq	DisposeObject
.LBB0_85:                               # %._crit_edge1240.thread
	movq	112(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB0_539
.LBB0_538:
	xorl	%ebx, %ebx
	movl	$0, 108(%rsp)           # 4-byte Folded Spill
.LBB0_539:                              # %._crit_edge1240.thread
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 8(%rax)
	je	.LBB0_542
# BB#540:
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rsi
	movq	%r12, %rdx
	callq	Promote
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	je	.LBB0_542
# BB#541:
	movq	%rbx, %rdi
	movq	160(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	120(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	84(%rsp), %edx          # 4-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
.LBB0_542:
	movq	%rbx, %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 104(%rax)
	movl	52(%rsp), %r14d         # 4-byte Reload
	je	.LBB0_561
# BB#543:
	movzbl	zz_lengths+26(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_549
# BB#544:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_550
.LBB0_545:
	orb	$32, 42(%rax)
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_563
# BB#546:
	movq	24(%r12), %rdi
	.p2align	4, 0x90
.LBB0_547:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_547
	jmp	.LBB0_548
.LBB0_549:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_550:
	movb	$26, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	testq	%rbp, %rbp
	je	.LBB0_552
# BB#551:
	movl	184(%rsp), %eax
	movl	%eax, 64(%rbx)
	movl	188(%rsp), %eax
	movl	%eax, 68(%rbx)
	movl	192(%rsp), %eax
	jmp	.LBB0_553
.LBB0_552:
	movabsq	$36028792732385279, %rax # imm = 0x7FFFFF007FFFFF
	movq	%rax, 64(%rbx)
	movl	$8388607, %eax          # imm = 0x7FFFFF
.LBB0_553:
	movl	%eax, 72(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_555
# BB#554:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_556
.LBB0_555:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_556:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	112(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_559
# BB#557:
	testq	%rcx, %rcx
	je	.LBB0_559
# BB#558:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_559:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_561
# BB#560:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_561:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	DetachGalley
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	KillGalley
	movl	$1, %edx
	movl	%r14d, %edi
	movq	%r12, %rsi
.LBB0_562:                              # %ParentFlush.exit954
	callq	ParentFlush
	jmp	.LBB0_563
.LBB0_15:
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_563
# BB#16:
	movq	24(%r12), %rdi
	.p2align	4, 0x90
.LBB0_17:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_17
.LBB0_548:
	callq	FlushGalley
.LBB0_563:                              # %ParentFlush.exit954
	addq	$1368, %rsp             # imm = 0x558
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_564:
	cmpq	$0, 88(%rsp)
	je	.LBB0_563
# BB#565:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	jmp	.LBB0_578
.LBB0_566:
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_570
# BB#567:
	movq	24(%r12), %rdi
.LBB0_568:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_568
# BB#569:
	callq	FlushGalley
.LBB0_570:                              # %ParentFlush.exit
	cmpq	$0, 88(%rsp)
	je	.LBB0_563
# BB#571:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	jmp	.LBB0_578
.LBB0_572:
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_576
# BB#573:
	movq	24(%r12), %rdi
.LBB0_574:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_574
# BB#575:
	callq	FlushGalley
.LBB0_576:                              # %ParentFlush.exit951
	cmpq	$0, 88(%rsp)
	je	.LBB0_563
# BB#577:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
.LBB0_578:                              # %ParentFlush.exit954
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_563
.LBB0_579:
	movq	80(%r12), %rax
	movq	80(%rax), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	88(%rcx), %r8
	xorl	%r15d, %r15d
	cmpq	%r8, %rax
	sete	%r14b
	testb	$16, 43(%rcx)
	jne	.LBB0_581
# BB#580:
	testb	$32, 42(%r12)
	movl	$1, %ecx
	movl	52(%rsp), %ebp          # 4-byte Reload
	cmovel	%ebp, %ecx
	cmpq	%r8, %rax
	cmovel	%ecx, %ebp
	jmp	.LBB0_587
.LBB0_581:
	movq	24(%r12), %rsi
	movq	%rsi, %rdi
	movl	52(%rsp), %ebp          # 4-byte Reload
.LBB0_582:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB0_582
# BB#583:
	cmpq	%r8, %rax
	jne	.LBB0_586
# BB#584:
	movzwl	42(%r12), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testw	%cx, %cx
	jne	.LBB0_586
# BB#585:
	orl	$1, %eax
	movw	%ax, 42(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %r8
	movl	$1, %ebp
.LBB0_586:
	leaq	88(%rsp), %rdx
	movq	%rsi, %rcx
	callq	FreeGalley
.LBB0_587:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	DetachGalley
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	KillGalley
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_589
# BB#588:
	xorl	%esi, %esi
	callq	FlushInners
	jmp	.LBB0_563
.LBB0_589:
	movb	%r14b, %r15b
	movl	%ebp, %edi
	movq	%r12, %rsi
	movl	%r15d, %edx
	jmp	.LBB0_562
.Lfunc_end0:
	.size	FlushGalley, .Lfunc_end0-FlushGalley
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_564
	.quad	.LBB0_566
	.quad	.LBB0_572
	.quad	.LBB0_22
	.quad	.LBB0_579
	.quad	.LBB0_238
.LJTI0_1:
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_106
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_82
	.quad	.LBB0_82
	.quad	.LBB0_82
	.quad	.LBB0_82
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_105
	.quad	.LBB0_102
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_105
	.quad	.LBB0_102
	.quad	.LBB0_108
	.quad	.LBB0_105
	.quad	.LBB0_41

	.text
	.p2align	4, 0x90
	.type	ParentFlush,@function
ParentFlush:                            # @ParentFlush
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB1_20
# BB#1:
	movq	24(%rsi), %rax
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_2
# BB#3:
	testl	%edx, %edx
	je	.LBB1_19
# BB#4:
	movq	%rsi, xx_hold(%rip)
	cmpq	%rsi, %rax
	je	.LBB1_11
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph36
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_9:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB1_5
# BB#10:                                # %..preheader25_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB1_11:                               # %.preheader25
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB1_18
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph31
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_14:                               #   in Loop: Header=BB1_12 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_16:                               #   in Loop: Header=BB1_12 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB1_12
# BB#17:                                # %._crit_edge32
	movl	%ecx, zz_size(%rip)
.LBB1_18:
	movq	%rsi, zz_hold(%rip)
	movzbl	32(%rsi), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rsi), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rsi)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB1_19:
	jmp	FlushGalley             # TAILCALL
.LBB1_20:
	testl	%edx, %edx
	je	.LBB1_36
# BB#21:
	movq	%rsi, xx_hold(%rip)
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB1_28
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph29
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_24:                               #   in Loop: Header=BB1_22 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_22 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_26:                               #   in Loop: Header=BB1_22 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB1_22
# BB#27:                                # %..preheader_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB1_28:                               # %.preheader
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB1_35
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_31:                               #   in Loop: Header=BB1_29 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_29 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_33:                               #   in Loop: Header=BB1_29 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB1_29
# BB#34:                                # %._crit_edge
	movl	%ecx, zz_size(%rip)
.LBB1_35:
	movq	%rsi, zz_hold(%rip)
	movzbl	32(%rsi), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rsi), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rsi)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB1_36:
	retq
.Lfunc_end1:
	.size	ParentFlush, .Lfunc_end1-ParentFlush
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FlushGalley: type(hd) != HEAD!"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FlushGalley: resume found no parent to hd!"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"FlushGalley/ATTACH_KILLED: inners!=nilobj!"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"FlushGalley/ATTACH_INPUT: inners!=nilobj!"
	.size	.L.str.4, 42

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"FlushGalley/ATTACH_NOTARG: inners!=nilobj!"
	.size	.L.str.5, 43

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"FlushGalley: attach_status"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"assert failed in %s %s"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"FlushGalley: dest_index"
	.size	.L.str.8, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Flush: PRECEDES!"
	.size	.L.str.10, 17

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FlushG: UpDims!"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"FlushGalley: dest != VCAT or ACAT!"
	.size	.L.str.12, 35

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"FlushGalley: prec_gap == nilobj && !is_indefinite(type(y))!"
	.size	.L.str.13, 60

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"FlushGalley: succ_gap != nilobj!"
	.size	.L.str.14, 33

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"FlushGalley: dest_side != FWD || !is_indefinite(type(y))!"
	.size	.L.str.15, 58

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"component too wide for available space"
	.size	.L.str.16, 39

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"FlushGalley:  tgp!"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"FlushGalley:"
	.size	.L.str.18, 13

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"FlushGalley: reject print!"
	.size	.L.str.19, 27

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"FlushGalley: header under SPLIT!"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"FlushGalley/REJECT: headers!"
	.size	.L.str.21, 29

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"FlushGalley/REJECT: first_link!"
	.size	.L.str.22, 32

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FlushGalley/REJECT THR!"
	.size	.L.str.23, 24

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"FlushGalley/REJECT: headers_count!"
	.size	.L.str.24, 35

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FlushGalley/REJECT: dest_index!"
	.size	.L.str.25, 32

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"error in database file %s"
	.size	.L.str.26, 26

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"AttachG: db CLOSURE!"
	.size	.L.str.27, 21

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"FlushGalley: cr is_word(type(tag))!"
	.size	.L.str.28, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
