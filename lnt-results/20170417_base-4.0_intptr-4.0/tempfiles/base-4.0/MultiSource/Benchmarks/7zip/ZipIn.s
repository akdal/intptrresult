	.text
	.file	"ZipIn.bc"
	.globl	_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy,@function
_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy: # @_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	$0, 32(%rbx)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 64(%rbx)
.LBB0_2:                                # %_ZN9CInBuffer13ReleaseStreamEv.exit.i
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%rbx)
.LBB0_4:                                # %_ZN8NArchive4NZip10CInArchive5CloseEv.exit
	movq	(%r14), %rax
	leaq	16(%rbx), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB0_10
# BB#5:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy
	testl	%eax, %eax
	jne	.LBB0_10
# BB#6:
	movq	(%r14), %rax
	movq	24(%rbx), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB0_10
# BB#7:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_9:                                # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%r14, (%rbx)
	xorl	%eax, %eax
.LBB0_10:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy, .Lfunc_end0-_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive5CloseEv,@function
_ZN8NArchive4NZip10CInArchive5CloseEv:  # @_ZN8NArchive4NZip10CInArchive5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 64(%rbx)
.LBB1_2:                                # %_ZN9CInBuffer13ReleaseStreamEv.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%rbx)
.LBB1_4:                                # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NZip10CInArchive5CloseEv, .Lfunc_end1-_ZN8NArchive4NZip10CInArchive5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy,@function
_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy: # @_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 80
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%r14)
	cmpq	$0, 120(%r14)
	je	.LBB2_4
# BB#1:
	leaq	120(%r14), %rbx
	movq	128(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_3
# BB#2:
	callq	_ZdaPv
.LBB2_3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.LBB2_4:                                # %_ZN8NArchive4NZip14CInArchiveInfo5ClearEv.exit
	movq	16(%r14), %rax
	movq	%rax, 24(%r14)
	leaq	4(%rsp), %rsi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB2_44
# BB#5:
	addq	$4, 24(%r14)
	movl	4(%rsp), %eax
	movl	%eax, 8(%r14)
	xorl	%ebp, %ebp
	cmpl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %eax
	je	.LBB2_44
# BB#6:
	cmpl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %eax
	je	.LBB2_44
# BB#7:
.Ltmp0:
	movl	$65536, %edi            # imm = 0x10000
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#8:                                 # %_ZN14CDynamicBufferIhE14EnsureCapacityEm.exit
	movb	7(%rsp), %al
	movb	%al, 2(%rbx)
	movzwl	5(%rsp), %eax
	movw	%ax, (%rbx)
	movq	16(%r14), %r13
	incq	%r13
	testq	%r12, %r12
	je	.LBB2_9
# BB#16:                                # %_ZN14CDynamicBufferIhE14EnsureCapacityEm.exit.split.preheader
	movl	$1, %ebp
	cmpq	$0, (%r12)
	je	.LBB2_43
# BB#17:                                # %.lr.ph183.preheader
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	$3, %r12d
.LBB2_18:                               # %.lr.ph183
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_36 Depth 2
	movl	$65536, %eax            # imm = 0x10000
	subl	%r12d, %eax
	movq	%rax, 8(%rsp)
	movl	%r12d, %esi
	addq	%rbx, %rsi
.Ltmp3:
	movq	%r15, %rdi
	leaq	8(%rsp), %rdx
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
.Ltmp4:
# BB#19:                                #   in Loop: Header=BB2_18 Depth=1
	testl	%eax, %eax
	jne	.LBB2_20
# BB#31:                                #   in Loop: Header=BB2_18 Depth=1
	movq	8(%rsp), %rax
	addq	%rax, 24(%r14)
	addl	%r12d, %eax
	cmpl	$5, %eax
	jbe	.LBB2_32
# BB#33:                                #   in Loop: Header=BB2_18 Depth=1
	addl	$-5, %eax
	je	.LBB2_34
# BB#35:                                # %.lr.ph
                                        #   in Loop: Header=BB2_18 Depth=1
	movl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %edx
	movl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %esi
	movl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_36:                               #   Parent Loop BB2_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$80, (%rbx,%rcx)
	jne	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_36 Depth=2
	movl	(%rbx,%rcx), %edi
	movl	%edi, 8(%r14)
	cmpl	%edx, %edi
	jne	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_36 Depth=2
	cmpw	$0, 4(%rbx,%rcx)
	jne	.LBB2_41
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_36 Depth=2
	cmpl	%esi, %edi
	jne	.LBB2_41
# BB#40:                                # %_ZN8NArchive4NZipL20TestMarkerCandidate2EPKhRj.exit
                                        #   in Loop: Header=BB2_36 Depth=2
	cmpb	$0, 4(%rbx,%rcx)
	jns	.LBB2_26
	.p2align	4, 0x90
.LBB2_41:                               # %_ZN8NArchive4NZipL20TestMarkerCandidate2EPKhRj.exit.thread
                                        #   in Loop: Header=BB2_36 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB2_36
	jmp	.LBB2_42
.LBB2_34:                               #   in Loop: Header=BB2_18 Depth=1
	xorl	%eax, %eax
.LBB2_42:                               # %.thread101
                                        #   in Loop: Header=BB2_18 Depth=1
	addq	%rax, %r13
	movl	(%rbx,%rax), %ecx
	movb	4(%rbx,%rax), %al
	movb	%al, 4(%rbx)
	movl	%ecx, (%rbx)
	movq	%r13, %rax
	subq	16(%r14), %rax
	movl	$5, %r12d
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpq	(%rcx), %rax
	jbe	.LBB2_18
	jmp	.LBB2_43
.LBB2_9:                                # %_ZN14CDynamicBufferIhE14EnsureCapacityEm.exit.split.us.preheader
	movl	$3, %r12d
	jmp	.LBB2_10
.LBB2_14:                               #   in Loop: Header=BB2_10 Depth=1
	xorl	%esi, %esi
.LBB2_15:                               # %.thread101.us
                                        #   in Loop: Header=BB2_10 Depth=1
	addq	%rsi, %r13
	movl	(%rbx,%rsi), %eax
	movb	4(%rbx,%rsi), %cl
	movb	%cl, 4(%rbx)
	movl	%eax, (%rbx)
	movl	$5, %r12d
.LBB2_10:                               # %_ZN14CDynamicBufferIhE14EnsureCapacityEm.exit.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_22 Depth 2
	movl	$65536, %eax            # imm = 0x10000
	subl	%r12d, %eax
	movq	%rax, 8(%rsp)
	movl	%r12d, %esi
	addq	%rbx, %rsi
.Ltmp6:
	movq	%r15, %rdi
	leaq	8(%rsp), %rdx
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	movl	%eax, %ebp
.Ltmp7:
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	testl	%ebp, %ebp
	jne	.LBB2_43
# BB#12:                                #   in Loop: Header=BB2_10 Depth=1
	movq	8(%rsp), %rax
	addq	%rax, 24(%r14)
	addl	%r12d, %eax
	cmpl	$6, %eax
	jb	.LBB2_32
# BB#13:                                #   in Loop: Header=BB2_10 Depth=1
	addl	$-5, %eax
	je	.LBB2_14
# BB#21:                                # %.lr.ph.us
                                        #   in Loop: Header=BB2_10 Depth=1
	movl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %edi
	movl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %ebp
	movl	%eax, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$80, (%rbx,%rcx)
	jne	.LBB2_28
# BB#23:                                #   in Loop: Header=BB2_22 Depth=2
	movl	(%rbx,%rcx), %eax
	movl	%eax, 8(%r14)
	cmpl	%edi, %eax
	jne	.LBB2_24
# BB#27:                                #   in Loop: Header=BB2_22 Depth=2
	cmpw	$0, 4(%rbx,%rcx)
	jne	.LBB2_28
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_24:                               #   in Loop: Header=BB2_22 Depth=2
	cmpl	%ebp, %eax
	jne	.LBB2_28
# BB#25:                                # %_ZN8NArchive4NZipL20TestMarkerCandidate2EPKhRj.exit.us
                                        #   in Loop: Header=BB2_22 Depth=2
	cmpb	$0, 4(%rbx,%rcx)
	jns	.LBB2_26
	.p2align	4, 0x90
.LBB2_28:                               # %_ZN8NArchive4NZipL20TestMarkerCandidate2EPKhRj.exit.thread.us
                                        #   in Loop: Header=BB2_22 Depth=2
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB2_22
	jmp	.LBB2_15
.LBB2_26:                               # %.thread
	leaq	(%rcx,%r13), %rax
	movq	%rax, 96(%r14)
	leaq	4(%rcx,%r13), %rax
	movq	%rax, 24(%r14)
	xorl	%ebp, %ebp
.LBB2_43:                               # %_ZN7CBufferIhED2Ev.exit87
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB2_44:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_32:                               # %.thread102
	movl	$1, %ebp
	jmp	.LBB2_43
.LBB2_20:
	movl	%eax, %ebp
	jmp	.LBB2_43
.LBB2_30:                               # %.thread103
.Ltmp2:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB2_45:                               # %.us-lcssa
.Ltmp5:
	jmp	.LBB2_46
.LBB2_29:                               # %.us-lcssa.us
.Ltmp8:
.LBB2_46:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy, .Lfunc_end2-_ZN8NArchive4NZip10CInArchive17FindAndReadMarkerEP9IInStreamPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive4SeekEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive4SeekEy,@function
_ZN8NArchive4NZip10CInArchive4SeekEy:   # @_ZN8NArchive4NZip10CInArchive4SeekEy
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end3:
	.size	_ZN8NArchive4NZip10CInArchive4SeekEy, .Lfunc_end3-_ZN8NArchive4NZip10CInArchive4SeekEy
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB4_1:
	retq
.Lfunc_end4:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end4-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj,@function
_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj: # @_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 80
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	%r15d, %r13d
	movq	%r13, 8(%rsp)
	cmpb	$0, 32(%r12)
	je	.LBB6_21
# BB#1:
	leaq	40(%r12), %rbp
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	movl	%ecx, %edx
	subl	%eax, %edx
	cmpl	%r15d, %edx
	jae	.LBB6_2
# BB#9:                                 # %.lr.ph.preheader.i
	movq	%r14, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	cmpq	%rcx, %rax
	jb	.LBB6_14
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_15:                               # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB6_14 Depth=1
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	cmpq	%rcx, %rax
	jb	.LBB6_14
.LBB6_11:
.Ltmp9:
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp10:
# BB#12:                                # %.noexc
	testb	%al, %al
	je	.LBB6_16
# BB#13:                                # %._crit_edge36.i
	movq	(%rbp), %rax
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	movb	%al, (%rbx,%r14)
	incq	%r14
	cmpq	%r13, %r14
	jb	.LBB6_15
	jmp	.LBB6_17
.LBB6_21:
	movq	(%r12), %rdi
	leaq	8(%rsp), %rdx
	movq	%rbx, %rsi
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	movl	%eax, %ebx
	jmp	.LBB6_22
.LBB6_2:                                # %.preheader24.i
	testl	%r15d, %r15d
	je	.LBB6_3
# BB#4:                                 # %.lr.ph29.i.preheader
	leaq	-1(%r13), %rdx
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph29.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %eax
	movb	%al, (%rbx,%rcx)
	incq	%rcx
	movq	(%rbp), %rax
	cmpq	%rcx, %rsi
	jne	.LBB6_5
.LBB6_6:                                # %.lr.ph29.i.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB6_7
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph29.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %eax
	movb	%al, (%rbx,%rcx)
	movq	(%rbp), %rax
	movzbl	1(%rax,%rcx), %eax
	movb	%al, 1(%rbx,%rcx)
	movq	(%rbp), %rax
	movzbl	2(%rax,%rcx), %eax
	movb	%al, 2(%rbx,%rcx)
	movq	(%rbp), %rax
	movzbl	3(%rax,%rcx), %eax
	movb	%al, 3(%rbx,%rcx)
	addq	$4, %rcx
	movq	(%rbp), %rax
	cmpq	%rcx, %r13
	jne	.LBB6_8
	jmp	.LBB6_7
.LBB6_3:
	xorl	%r13d, %r13d
.LBB6_7:                                # %._crit_edge30.i
	addq	%rax, %r13
	movq	%r13, (%rbp)
	jmp	.LBB6_18
.LBB6_16:                               # %.noexc._ZN9CInBuffer9ReadBytesEPhj.exit.loopexit_crit_edge
	movl	%r14d, %r15d
.LBB6_17:                               # %_ZN9CInBuffer9ReadBytesEPhj.exit
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB6_18:                               # %_ZN9CInBuffer9ReadBytesEPhj.exit
	movl	%r15d, %eax
	movq	%rax, 8(%rsp)
	xorl	%ebx, %ebx
.LBB6_22:
	testq	%r14, %r14
	movq	8(%rsp), %rax
	je	.LBB6_24
# BB#23:
	movl	%eax, (%r14)
.LBB6_24:                               # %._crit_edge
	addq	%rax, 24(%r12)
.LBB6_25:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_19:
.Ltmp11:
	movq	%rax, %rdi
	cmpl	$1, %edx
	jne	.LBB6_26
# BB#20:
	callq	__cxa_begin_catch
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB6_25
.LBB6_26:
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj, .Lfunc_end6-_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\244"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	3                       #   On action: 2
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	0                       # >> Action Record 1 <<
                                        #   Cleanup
	.byte	0                       #   No further actions
	.byte	1                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 1
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive4SkipEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive4SkipEy,@function
_ZN8NArchive4NZip10CInArchive4SkipEy:   # @_ZN8NArchive4NZip10CInArchive4SkipEy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -48
.Lcfi41:
	.cfi_offset %r12, -40
.Lcfi42:
	.cfi_offset %r13, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	testq	%r13, %r13
	je	.LBB7_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	leaq	11(%rsp), %r15
	leaq	12(%rsp), %r12
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB7_3
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpl	$1, 12(%rsp)
	jne	.LBB7_6
# BB#7:                                 # %_ZN8NArchive4NZip10CInArchive8ReadByteEv.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jb	.LBB7_2
.LBB7_8:                                # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB7_3:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB7_4
.LBB7_6:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB7_4:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end7:
	.size	_ZN8NArchive4NZip10CInArchive4SkipEy, .Lfunc_end7-_ZN8NArchive4NZip10CInArchive4SkipEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive8ReadByteEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive8ReadByteEv,@function
_ZN8NArchive4NZip10CInArchive8ReadByteEv: # @_ZN8NArchive4NZip10CInArchive8ReadByteEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 16
	leaq	3(%rsp), %rsi
	leaq	4(%rsp), %rcx
	movl	$1, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB8_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$1, 4(%rsp)
	jne	.LBB8_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movb	3(%rsp), %al
	popq	%rcx
	retq
.LBB8_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB8_2
.LBB8_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB8_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end8:
	.size	_ZN8NArchive4NZip10CInArchive8ReadByteEv, .Lfunc_end8-_ZN8NArchive4NZip10CInArchive8ReadByteEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy,@function
_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy: # @_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 16
	movq	(%rdi), %rax
	movq	(%rax), %r8
	leaq	24(%rdi), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	callq	*48(%r8)
	testl	%eax, %eax
	jne	.LBB9_2
# BB#1:
	popq	%rax
	retq
.LBB9_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$7, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end9:
	.size	_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy, .Lfunc_end9-_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj,@function
_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj: # @_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -16
	movl	%edx, %ebx
	leaq	12(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB10_2
# BB#1:
	cmpl	%ebx, 12(%rsp)
	sete	%al
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB10_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end10:
	.size	_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj, .Lfunc_end10-_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj,@function
_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj: # @_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -16
	movl	%edx, %ebx
	leaq	12(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB11_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit
	cmpl	%ebx, 12(%rsp)
	jne	.LBB11_4
# BB#5:
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB11_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB11_2
.LBB11_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB11_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end11:
	.size	_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj, .Lfunc_end11-_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj,@function
_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj: # @_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	%r13d, %r15d
	movq	8(%rbx), %rbp
	cmpq	%r15, %rbp
	je	.LBB12_8
# BB#1:
	testl	%r13d, %r13d
	je	.LBB12_2
# BB#3:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbp, %rbp
	je	.LBB12_5
# BB#4:
	movq	16(%rbx), %rsi
	cmpq	%r15, %rbp
	cmovaeq	%r15, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	memmove
	jmp	.LBB12_5
.LBB12_2:
	xorl	%r12d, %r12d
.LBB12_5:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_7
# BB#6:
	callq	_ZdaPv
.LBB12_7:
	movq	%r12, 16(%rbx)
	movq	%r15, 8(%rbx)
.LBB12_8:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
	testl	%r13d, %r13d
	je	.LBB12_14
# BB#9:
	movq	16(%rbx), %rsi
	leaq	4(%rsp), %rcx
	movq	%r14, %rdi
	movl	%r13d, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB12_10
# BB#12:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	%r13d, 4(%rsp)
	jne	.LBB12_13
.LBB12_14:                              # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_10:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB12_11
.LBB12_13:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB12_11:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end12:
	.size	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj, .Lfunc_end12-_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev,@function
_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev: # @_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 16
	leaq	2(%rsp), %rsi
	leaq	4(%rsp), %rcx
	movl	$2, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB13_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$2, 4(%rsp)
	jne	.LBB13_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movzwl	2(%rsp), %eax
	popq	%rcx
	retq
.LBB13_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB13_2
.LBB13_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB13_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end13:
	.size	_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev, .Lfunc_end13-_ZN8NArchive4NZip10CInArchive10ReadUInt16Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev,@function
_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev: # @_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	movq	%rsp, %rsi
	leaq	4(%rsp), %rcx
	movl	$4, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB14_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$4, 4(%rsp)
	jne	.LBB14_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movl	(%rsp), %eax
	popq	%rcx
	retq
.LBB14_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB14_2
.LBB14_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB14_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end14:
	.size	_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev, .Lfunc_end14-_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev,@function
_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev: # @_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 32
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	$8, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB15_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$8, 12(%rsp)
	jne	.LBB15_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movq	16(%rsp), %rax
	addq	$24, %rsp
	retq
.LBB15_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB15_2
.LBB15_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB15_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end15:
	.size	_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev, .Lfunc_end15-_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj,@function
_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj: # @_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	$4, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB16_5
# BB#1:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit
	cmpl	$4, 12(%rsp)
	jne	.LBB16_2
# BB#3:
	movl	8(%rsp), %eax
	movl	%eax, (%rbx)
	movb	$1, %al
	jmp	.LBB16_4
.LBB16_2:
	xorl	%eax, %eax
.LBB16_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB16_5:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end16:
	.size	_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj, .Lfunc_end16-_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE,@function
_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE: # @_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %r15
	testl	%r13d, %r13d
	jne	.LBB17_2
# BB#1:
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
.LBB17_2:
	movl	12(%r14), %ebp
	cmpl	%r13d, %ebp
	jg	.LBB17_30
# BB#3:
	leal	1(%r13), %ebx
	cmpl	%ebp, %ebx
	je	.LBB17_30
# BB#4:
	movslq	%ebx, %rax
	cmpl	$-1, %r13d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebp, %ebp
	jle	.LBB17_5
# BB#6:                                 # %.preheader.i.i
	leaq	8(%r14), %rbp
	movslq	8(%r14), %rax
	testq	%rax, %rax
	movq	(%r14), %rdi
	movl	%ebx, (%rsp)            # 4-byte Spill
	jle	.LBB17_26
# BB#7:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %eax
	jbe	.LBB17_8
# BB#15:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB17_8
# BB#16:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB17_18
# BB#17:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB17_18
.LBB17_8:
	xorl	%ecx, %ecx
.LBB17_9:                               # %.lr.ph.i.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB17_12
# BB#10:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB17_11:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB17_11
.LBB17_12:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB17_27
# BB#13:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB17_14:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB17_14
	jmp	.LBB17_27
.LBB17_5:                               # %._crit_edge17.i.i
	leaq	8(%r14), %rbp
	jmp	.LBB17_29
.LBB17_26:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB17_28
.LBB17_27:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB17_28:
	movl	(%rsp), %ebx            # 4-byte Reload
.LBB17_29:
	movq	%r12, (%r14)
	movslq	(%rbp), %rax
	movb	$0, (%r12,%rax)
	movl	%ebx, 12(%r14)
.LBB17_30:                              # %_ZN11CStringBaseIcE9GetBufferEi.exit
	movq	(%r14), %rbx
	leaq	4(%rsp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB17_31
# BB#33:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	%r13d, 4(%rsp)
	jne	.LBB17_34
# BB#35:                                # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movl	%r13d, %eax
	movb	$0, (%rbx,%rax)
	movq	(%r14), %rax
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movl	$-1, %ecx
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB17_36:                              # =>This Inner Loop Header: Depth=1
	addq	%rsi, %rdx
	incl	%ecx
	cmpb	$0, (%rdi)
	leaq	1(%rdi), %rdi
	jne	.LBB17_36
# BB#37:                                # %_ZN11CStringBaseIcE13ReleaseBufferEv.exit
	sarq	$32, %rdx
	movb	$0, (%rax,%rdx)
	movl	%ecx, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_18:                              # %vector.body.preheader
	movq	%rbp, %rbx
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB17_19
# BB#20:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_21:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r12,%rbp)
	movups	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB17_21
	jmp	.LBB17_22
.LBB17_19:
	xorl	%ebp, %ebp
.LBB17_22:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB17_25
# BB#23:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r12,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB17_24:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB17_24
.LBB17_25:                              # %middle.block
	cmpq	%rcx, %rax
	movq	%rbx, %rbp
	jne	.LBB17_9
	jmp	.LBB17_27
.LBB17_31:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB17_32
.LBB17_34:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB17_32:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end17:
	.size	_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE, .Lfunc_end17-_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj,@function
_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj: # @_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 192
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	%esi, %r12d
	movq	%rdi, %r13
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$4, %r12d
	jb	.LBB18_1
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_64 Depth 2
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	leaq	64(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp12:
	movl	$2, %edx
	movq	%r13, %rdi
	leaq	16(%rsp), %rsi
	leaq	20(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp13:
# BB#3:                                 # %.noexc
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_4
# BB#6:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$2, 20(%rsp)
	jne	.LBB18_7
# BB#9:                                 #   in Loop: Header=BB18_2 Depth=1
	movzwl	16(%rsp), %ebx
	movw	%bx, 48(%rsp)
.Ltmp18:
	movl	$2, %edx
	movq	%r13, %rdi
	leaq	18(%rsp), %rsi
	leaq	24(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp19:
# BB#10:                                # %.noexc65
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_11
# BB#13:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i64
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$2, 24(%rsp)
	jne	.LBB18_14
# BB#17:                                #   in Loop: Header=BB18_2 Depth=1
	movzwl	18(%rsp), %r15d
	leal	-4(%r12), %r14d
	cmpl	%r14d, %r15d
	cmoval	%r14d, %r15d
	movzwl	%bx, %eax
	cmpl	$1, %eax
	jne	.LBB18_74
# BB#18:                                #   in Loop: Header=BB18_2 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, (%rax)
	jne	.LBB18_30
# BB#19:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, %r15d
	jb	.LBB18_83
# BB#20:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp35:
	movl	$8, %edx
	movq	%r13, %rdi
	leaq	120(%rsp), %rsi
	leaq	28(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp36:
# BB#21:                                # %.noexc70
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_22
# BB#24:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i69
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, 28(%rsp)
	jne	.LBB18_25
# BB#29:                                #   in Loop: Header=BB18_2 Depth=1
	movq	120(%rsp), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addl	$-12, %r12d
	addl	$-8, %r15d
	movl	%r12d, %r14d
.LBB18_30:                              #   in Loop: Header=BB18_2 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, (%rax)
	leaq	11(%rsp), %r12
	leaq	12(%rsp), %rbp
	jne	.LBB18_40
# BB#31:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, %r15d
	jb	.LBB18_83
# BB#32:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp41:
	movl	$8, %edx
	movq	%r13, %rdi
	leaq	128(%rsp), %rsi
	leaq	32(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp42:
# BB#33:                                # %.noexc74
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_34
# BB#36:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i73
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, 32(%rsp)
	jne	.LBB18_37
# BB#39:                                #   in Loop: Header=BB18_2 Depth=1
	movq	128(%rsp), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addl	$-8, %r14d
	addl	$-8, %r15d
	leaq	11(%rsp), %r12
	leaq	12(%rsp), %rbp
.LBB18_40:                              #   in Loop: Header=BB18_2 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, (%rax)
	jne	.LBB18_50
# BB#41:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, %r15d
	jb	.LBB18_83
# BB#42:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp47:
	movl	$8, %edx
	movq	%r13, %rdi
	leaq	112(%rsp), %rsi
	leaq	36(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp48:
# BB#43:                                # %.noexc79
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_44
# BB#46:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i78
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$8, 36(%rsp)
	leaq	11(%rsp), %r12
	leaq	12(%rsp), %rbp
	jne	.LBB18_47
# BB#49:                                #   in Loop: Header=BB18_2 Depth=1
	movq	112(%rsp), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addl	$-8, %r14d
	addl	$-8, %r15d
.LBB18_50:                              #   in Loop: Header=BB18_2 Depth=1
	movq	192(%rsp), %rax
	cmpl	$65535, (%rax)          # imm = 0xFFFF
	jne	.LBB18_60
# BB#51:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	$4, %r15d
	jb	.LBB18_83
# BB#52:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp53:
	movl	$4, %edx
	movq	%r13, %rdi
	leaq	40(%rsp), %rsi
	leaq	44(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp54:
# BB#53:                                # %.noexc84
                                        #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_54
# BB#56:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i83
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$4, 44(%rsp)
	leaq	11(%rsp), %r12
	leaq	12(%rsp), %rbp
	jne	.LBB18_57
# BB#59:                                #   in Loop: Header=BB18_2 Depth=1
	movl	40(%rsp), %eax
	movq	192(%rsp), %rcx
	movl	%eax, (%rcx)
	addl	$-4, %r14d
	addl	$-4, %r15d
.LBB18_60:                              #   in Loop: Header=BB18_2 Depth=1
	testl	%r15d, %r15d
	je	.LBB18_61
# BB#63:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_64:                              # %.lr.ph
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp61:
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp62:
# BB#65:                                # %.noexc88
                                        #   in Loop: Header=BB18_64 Depth=2
	testl	%eax, %eax
	jne	.LBB18_66
# BB#68:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i87
                                        #   in Loop: Header=BB18_64 Depth=2
	cmpl	$1, 12(%rsp)
	jne	.LBB18_69
# BB#71:                                #   in Loop: Header=BB18_64 Depth=2
	incl	%ebx
	cmpl	%r15d, %ebx
	jb	.LBB18_64
	jmp	.LBB18_62
	.p2align	4, 0x90
.LBB18_74:                              #   in Loop: Header=BB18_2 Depth=1
.Ltmp26:
	movq	%r13, %rdi
	leaq	56(%rsp), %rsi
	movl	%r15d, %edx
	callq	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
.Ltmp27:
# BB#75:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp28:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp29:
# BB#76:                                # %.noexc91
                                        #   in Loop: Header=BB18_2 Depth=1
	movzwl	48(%rsp), %eax
	movw	%ax, (%rbp)
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movq	64(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB18_79
# BB#77:                                # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB18_2 Depth=1
.Ltmp30:
	movq	%rbx, %rdi
	callq	_Znam
.Ltmp31:
# BB#78:                                # %.noexc.i
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rax, 24(%rbp)
	movq	%rbx, 16(%rbp)
	movq	72(%rsp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memmove
.LBB18_79:                              # %_ZN8NArchive4NZip14CExtraSubBlockC2ERKS1_.exit.i
                                        #   in Loop: Header=BB18_2 Depth=1
.Ltmp33:
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp34:
# BB#80:                                #   in Loop: Header=BB18_2 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	movq	72(%rsp), %rdi
	subl	%r15d, %r14d
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	testq	%rdi, %rdi
	je	.LBB18_82
# BB#81:                                #   in Loop: Header=BB18_2 Depth=1
	callq	_ZdaPv
	jmp	.LBB18_82
.LBB18_61:                              #   in Loop: Header=BB18_2 Depth=1
	xorl	%r15d, %r15d
.LBB18_62:                              # %.thread149
                                        #   in Loop: Header=BB18_2 Depth=1
	subl	%r15d, %r14d
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
.LBB18_82:                              # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$3, %r14d
	movl	%r14d, %r12d
	ja	.LBB18_2
	jmp	.LBB18_83
.LBB18_1:
	movl	%r12d, %r14d
.LBB18_83:                              # %.loopexit105
	testl	%r14d, %r14d
	je	.LBB18_97
# BB#84:                                # %.lr.ph.i
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	leaq	112(%rsp), %r14
	leaq	48(%rsp), %r15
	.p2align	4, 0x90
.LBB18_85:                              # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r15, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB18_86
# BB#94:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i.i
                                        #   in Loop: Header=BB18_85 Depth=1
	cmpl	$1, 48(%rsp)
	jne	.LBB18_95
# BB#96:                                # %_ZN8NArchive4NZip10CInArchive8ReadByteEv.exit.i
                                        #   in Loop: Header=BB18_85 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jb	.LBB18_85
.LBB18_97:                              # %_ZN8NArchive4NZip10CInArchive4SkipEy.exit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_69:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp66:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp67:
# BB#70:                                # %.noexc90
.LBB18_66:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp64:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp65:
# BB#67:                                # %.noexc89
.LBB18_86:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB18_87
.LBB18_95:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB18_87:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB18_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp14:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp15:
# BB#5:                                 # %.noexc62
.LBB18_11:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp21:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp22:
# BB#12:                                # %.noexc66
.LBB18_7:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp16:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp17:
# BB#8:                                 # %.noexc63
.LBB18_14:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp23:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp24:
# BB#15:                                # %.noexc67
.LBB18_22:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp37:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp38:
# BB#23:                                # %.noexc71
.LBB18_25:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp39:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp40:
# BB#26:                                # %.noexc72
.LBB18_34:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp43:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp44:
# BB#35:                                # %.noexc75
.LBB18_37:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp45:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp46:
# BB#38:                                # %.noexc76
.LBB18_44:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp49:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp50:
# BB#45:                                # %.noexc80
.LBB18_47:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp51:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp52:
# BB#48:                                # %.noexc81
.LBB18_54:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp56:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp57:
# BB#55:                                # %.noexc85
.LBB18_57:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp58:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp59:
# BB#58:                                # %.noexc86
.LBB18_28:                              # %.loopexit.split-lp101
.Ltmp60:
	jmp	.LBB18_90
.LBB18_16:                              # %.loopexit.split-lp96
.Ltmp25:
	jmp	.LBB18_90
.LBB18_73:                              # %.loopexit.split-lp
.Ltmp68:
	jmp	.LBB18_90
.LBB18_88:
.Ltmp32:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB18_91
.LBB18_89:                              # %.loopexit95
.Ltmp20:
	jmp	.LBB18_90
.LBB18_27:                              # %.loopexit100
.Ltmp55:
	jmp	.LBB18_90
.LBB18_72:                              # %.loopexit94
.Ltmp63:
.LBB18_90:                              # %.body
	movq	%rax, %rbx
.LBB18_91:                              # %.body
	movq	$_ZTV7CBufferIhE+16, 56(%rsp)
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_93
# BB#92:
	callq	_ZdaPv
.LBB18_93:                              # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit93
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj, .Lfunc_end18-_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\344\003"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\341\003"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp12         #   Call between .Ltmp12 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp54-.Ltmp35         #   Call between .Ltmp35 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin2   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp55-.Lfunc_begin2   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp33-.Ltmp31         #   Call between .Ltmp31 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp55-.Lfunc_begin2   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp66-.Ltmp34         #   Call between .Ltmp34 and .Ltmp66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin2   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp64-.Ltmp67         #   Call between .Ltmp67 and .Ltmp64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp68-.Lfunc_begin2   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp14-.Ltmp65         #   Call between .Ltmp65 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp21-.Ltmp15         #   Call between .Ltmp15 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp16-.Ltmp22         #   Call between .Ltmp22 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 19 <<
	.long	.Ltmp23-.Ltmp17         #   Call between .Ltmp17 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 20 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 21 <<
	.long	.Ltmp37-.Ltmp24         #   Call between .Ltmp24 and .Ltmp37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 22 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 23 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 24 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 25 <<
	.long	.Ltmp43-.Ltmp40         #   Call between .Ltmp40 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 26 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 27 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin2   # >> Call Site 28 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 29 <<
	.long	.Ltmp49-.Ltmp46         #   Call between .Ltmp46 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 30 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 31 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin2   # >> Call Site 32 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 33 <<
	.long	.Ltmp56-.Ltmp52         #   Call between .Ltmp52 and .Ltmp56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 34 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 35 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 36 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 37 <<
	.long	.Lfunc_end18-.Ltmp59    #   Call between .Ltmp59 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE,@function
_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE: # @_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 96
.Lcfi102:
	.cfi_offset %rbx, -32
.Lcfi103:
	.cfi_offset %r14, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rcx
	movl	$26, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB19_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$26, 16(%rsp)
	jne	.LBB19_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movb	32(%rsp), %al
	movb	%al, (%rbx)
	movb	33(%rsp), %al
	movb	%al, 1(%rbx)
	movzwl	34(%rsp), %eax
	movw	%ax, 2(%rbx)
	movzwl	36(%rsp), %eax
	movw	%ax, 4(%rbx)
	movl	38(%rsp), %eax
	movl	%eax, 8(%rbx)
	movl	42(%rsp), %eax
	movl	%eax, 12(%rbx)
	movq	46(%rsp), %xmm0         # xmm0 = mem[0],zero
	pxor	%xmm1, %xmm1
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, 16(%rbx)
	movzwl	54(%rsp), %ebp
	movzwl	56(%rsp), %eax
	movw	%ax, 184(%rbx)
	leaq	32(%rbx), %rdx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE
	addl	$30, %ebp
	movl	%ebp, 180(%rbx)
	movzwl	184(%rbx), %esi
	testl	%esi, %esi
	je	.LBB19_7
# BB#6:
	movq	$0, 16(%rsp)
	movl	$0, 28(%rsp)
	leaq	16(%rbx), %r8
	leaq	24(%rbx), %rcx
	leaq	48(%rbx), %rdx
	leaq	28(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	16(%rsp), %r9
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj
.LBB19_7:
	xorl	%eax, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB19_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB19_2
.LBB19_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB19_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end19:
	.size	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE, .Lfunc_end19-_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE,@function
_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE: # @_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 32
	subq	$208, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 240
.Lcfi109:
	.cfi_offset %rbx, -32
.Lcfi110:
	.cfi_offset %r14, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	xorl	%eax, %eax
	cmpb	$0, 176(%rbx)
	jne	.LBB20_29
# BB#1:
	movq	88(%rbx), %rsi
	addq	88(%rbp), %rsi
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp69:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
.Ltmp70:
# BB#2:                                 # %_ZN8NArchive4NZip10CInArchive4SeekEy.exit
	testl	%eax, %eax
	jne	.LBB20_29
# BB#3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
.Ltmp71:
	movl	$4, %edi
	callq	_Znam
.Ltmp72:
# BB#4:
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	$4, 60(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 64(%rsp)
	movups	%xmm0, 144(%rsp)
	movq	$8, 160(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 136(%rsp)
	movq	$_ZTV7CBufferIhE+16, 168(%rsp)
	movups	%xmm0, 176(%rsp)
	movb	$0, 194(%rsp)
	movw	$0, 192(%rsp)
.Ltmp74:
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	$4, %edx
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp75:
# BB#5:                                 # %.noexc
	testl	%eax, %eax
	jne	.LBB20_6
# BB#8:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
	cmpl	$4, 12(%rsp)
	jne	.LBB20_9
# BB#11:
	movl	8(%rsp), %eax
	movl	$1, %r14d
	cmpl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %eax
	jne	.LBB20_23
# BB#12:
.Ltmp80:
	leaq	16(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
.Ltmp81:
# BB#13:
	movzwl	4(%rbx), %edx
	cmpw	20(%rsp), %dx
	jne	.LBB20_23
# BB#14:
	movw	2(%rbx), %cx
	movw	18(%rsp), %ax
	cmpw	%ax, %cx
	je	.LBB20_16
# BB#15:                                # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit
	movzwl	%dx, %edx
	cmpl	$7, %edx
	movl	$32767, %esi            # imm = 0x7FFF
	movl	$65535, %edi            # imm = 0xFFFF
	cmovbl	%esi, %edi
	cmpl	$8, %edx
	movl	$32761, %edx            # imm = 0x7FF9
	cmovnel	%edi, %edx
	xorl	%eax, %ecx
	testl	%edx, %ecx
	jne	.LBB20_23
.LBB20_16:                              # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit.thread
	testb	$8, %al
	jne	.LBB20_20
# BB#17:
	movl	12(%rbx), %eax
	cmpl	28(%rsp), %eax
	jne	.LBB20_23
# BB#18:
	movq	16(%rbx), %rax
	cmpq	32(%rsp), %rax
	jne	.LBB20_23
# BB#19:
	movq	24(%rbx), %rax
	cmpq	40(%rsp), %rax
	jne	.LBB20_23
.LBB20_20:
	movl	40(%rbx), %eax
	cmpl	56(%rsp), %eax
	jne	.LBB20_23
# BB#21:
	movl	196(%rsp), %eax
	movl	%eax, 180(%rbx)
	movzwl	200(%rsp), %eax
	movw	%ax, 184(%rbx)
	leaq	48(%rbx), %rdi
	leaq	64(%rsp), %rsi
.Ltmp82:
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp83:
# BB#22:                                # %_ZN8NArchive4NZip11CExtraBlockaSERKS1_.exit
	movb	$1, 176(%rbx)
	xorl	%r14d, %r14d
.LBB20_23:                              # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit.thread35
.Ltmp88:
	leaq	16(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp89:
# BB#24:
	movl	%r14d, %eax
.LBB20_29:
	addq	$208, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB20_6:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp76:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp77:
# BB#7:                                 # %.noexc32
.LBB20_9:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp78:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp79:
# BB#10:                                # %.noexc33
.LBB20_26:
.Ltmp90:
	jmp	.LBB20_27
.LBB20_25:
.Ltmp84:
	movq	%rax, %rbx
.Ltmp85:
	leaq	16(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp86:
	jmp	.LBB20_28
.LBB20_30:
.Ltmp87:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_31:
.Ltmp73:
.LBB20_27:
	movq	%rax, %rbx
.LBB20_28:
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movl	$1, %eax
	jmp	.LBB20_29
.Lfunc_end20:
	.size	_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE, .Lfunc_end20-_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp72-.Ltmp69         #   Call between .Ltmp69 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin3   #     jumps to .Ltmp73
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp83-.Ltmp74         #   Call between .Ltmp74 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp76-.Ltmp89         #   Call between .Ltmp89 and .Ltmp76
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp77-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp86-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Lfunc_end20-.Ltmp86    #   Call between .Ltmp86 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip5CItemD2Ev,"axG",@progbits,_ZN8NArchive4NZip5CItemD2Ev,comdat
	.weak	_ZN8NArchive4NZip5CItemD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemD2Ev,@function
_ZN8NArchive4NZip5CItemD2Ev:            # @_ZN8NArchive4NZip5CItemD2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -32
.Lcfi116:
	.cfi_offset %r14, -24
.Lcfi117:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV7CBufferIhE+16, 152(%r15)
	movq	168(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	callq	_ZdaPv
.LBB21_2:                               # %_ZN7CBufferIhED2Ev.exit
	leaq	120(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%r15)
.Ltmp91:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp92:
# BB#3:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp97:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp98:
# BB#4:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp109:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp110:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp115:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp116:
# BB#6:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB21_16
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB21_16:                              # %_ZN8NArchive4NZip10CLocalItemD2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB21_12:
.Ltmp117:
	movq	%rax, %r14
	jmp	.LBB21_13
.LBB21_10:
.Ltmp111:
	movq	%rax, %r14
.Ltmp112:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp113:
	jmp	.LBB21_13
.LBB21_11:
.Ltmp114:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_17:
.Ltmp99:
	movq	%rax, %r14
	jmp	.LBB21_18
.LBB21_8:
.Ltmp93:
	movq	%rax, %r14
.Ltmp94:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
.LBB21_18:                              # %.body
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp100:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp101:
# BB#19:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i2
.Ltmp106:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp107:
.LBB21_13:                              # %.body.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB21_15
# BB#14:
	callq	_ZdaPv
.LBB21_15:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_9:
.Ltmp96:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_22:
.Ltmp108:
	movq	%rax, %r14
	jmp	.LBB21_23
.LBB21_20:
.Ltmp102:
	movq	%rax, %r14
.Ltmp103:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp104:
.LBB21_23:                              # %.body.i5
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB21_25
# BB#24:
	callq	_ZdaPv
.LBB21_25:                              # %.body7
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB21_21:
.Ltmp105:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN8NArchive4NZip5CItemD2Ev, .Lfunc_end21-_ZN8NArchive4NZip5CItemD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp91-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin4   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin4   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin4  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin4  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin4  #     jumps to .Ltmp114
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin4   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin4  #     jumps to .Ltmp102
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin4  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp103-.Ltmp107       #   Call between .Ltmp107 and .Ltmp103
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin4  #     jumps to .Ltmp105
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE,@function
_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE: # @_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 56
	subq	$4120, %rsp             # imm = 0x1018
.Lcfi124:
	.cfi_def_cfa_offset 4176
.Lcfi125:
	.cfi_offset %rbx, -56
.Lcfi126:
	.cfi_offset %r12, -48
.Lcfi127:
	.cfi_offset %r13, -40
.Lcfi128:
	.cfi_offset %r14, -32
.Lcfi129:
	.cfi_offset %r15, -24
.Lcfi130:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	testb	$8, 2(%r12)
	jne	.LBB22_1
# BB#34:
	movq	16(%r12), %rsi
	movq	(%r14), %rdi
	movq	(%rdi), %rax
	addq	$24, %r14
	movl	$1, %edx
	movq	%r14, %rcx
	callq	*48(%rax)
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB22_36
	jmp	.LBB22_8
.LBB22_1:
	leaq	16(%rsp), %rbx
	leaq	12(%rsp), %rcx
	movl	$4096, %edx             # imm = 0x1000
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB22_36
# BB#2:                                 # %.lr.ph101.preheader
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	jmp	.LBB22_3
	.p2align	4, 0x90
.LBB22_4:                               # %.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	leal	-16(%r15), %ecx
	xorl	%eax, %eax
	movl	_ZN8NArchive4NZip10NSignature15kDataDescriptorE(%rip), %esi
	.p2align	4, 0x90
.LBB22_5:                               #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	movl	24(%rsp,%rdx), %edi
	addl	%r13d, %eax
	cmpl	%edi, %eax
	jne	.LBB22_9
# BB#6:                                 #   in Loop: Header=BB22_5 Depth=2
	cmpl	%esi, 16(%rsp,%rdx)
	je	.LBB22_7
.LBB22_9:                               #   in Loop: Header=BB22_5 Depth=2
	leal	1(%rdx), %eax
	cmpl	%ecx, %eax
	jbe	.LBB22_5
# BB#10:                                # %_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy.exit75
                                        #   in Loop: Header=BB22_3 Depth=1
	xorl	%ebp, %ebp
	movl	%r15d, %r8d
	subl	%eax, %r8d
	jbe	.LBB22_20
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	%eax, %ebp
	leal	-1(%r9,%r11), %r10d
	subl	%eax, %r10d
	incq	%r10
	cmpq	$31, %r10
	jbe	.LBB22_12
# BB#21:                                # %min.iters.checked
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	%r10, %rcx
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	andq	%rsi, %rcx
	je	.LBB22_12
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	leal	-1(%r9,%r11), %esi
	subl	%eax, %esi
	leaq	(%rbp,%rsi), %rdi
	leaq	17(%rsp), %r8
	addq	%r8, %rdi
	cmpq	%rdi, %rbx
	jae	.LBB22_25
# BB#23:                                # %vector.memcheck
                                        #   in Loop: Header=BB22_3 Depth=1
	addq	%r8, %rsi
	leaq	16(%rsp,%rbp), %rdi
	cmpq	%rsi, %rdi
	jae	.LBB22_25
# BB#24:                                #   in Loop: Header=BB22_3 Depth=1
	xorl	%ecx, %ecx
	movl	8(%rsp), %r8d           # 4-byte Reload
	jmp	.LBB22_13
.LBB22_12:                              #   in Loop: Header=BB22_3 Depth=1
	xorl	%ecx, %ecx
.LBB22_13:                              # %.lr.ph.preheader145
                                        #   in Loop: Header=BB22_3 Depth=1
	leal	1(%r9,%r11), %esi
	subl	%eax, %esi
	movl	%ecx, %edi
	orl	$1, %edi
	addl	$3, %esi
	subl	%eax, %r15d
	subl	%edi, %r15d
	testb	$3, %sil
	je	.LBB22_16
# BB#14:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	leal	3(%r9,%r11), %esi
	subb	%dl, %sil
	movzbl	%sil, %esi
	andl	$3, %esi
	negl	%esi
	.p2align	4, 0x90
.LBB22_15:                              # %.lr.ph.prol
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	16(%rsp,%rbp), %ebx
	movb	%bl, 16(%rsp,%rcx)
	incq	%rbp
	incq	%rcx
	incl	%esi
	jne	.LBB22_15
.LBB22_16:                              # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpl	$3, %r15d
	jb	.LBB22_19
# BB#17:                                # %.lr.ph.preheader145.new
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	19(%rsp), %rsi
	movq	%rsi, %rdi
	addq	%rdi, %rbp
	leal	-1(%r9,%r11), %esi
	subl	%ecx, %esi
	subl	%edx, %esi
	addq	%rdi, %rcx
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-3(%rbp), %edx
	movb	%dl, -3(%rcx)
	movzbl	-2(%rbp), %edx
	movb	%dl, -2(%rcx)
	movzbl	-1(%rbp), %edx
	movb	%dl, -1(%rcx)
	movzbl	(%rbp), %edx
	movb	%dl, (%rcx)
	addq	$4, %rbp
	addq	$4, %rcx
	addl	$-4, %esi
	jne	.LBB22_18
.LBB22_19:                              # %.thread80.loopexit
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	%r8d, %ebp
	leaq	16(%rsp), %rbx
.LBB22_20:                              # %.thread80
                                        #   in Loop: Header=BB22_3 Depth=1
	addl	%eax, %r13d
	movl	%ebp, %eax
	leaq	16(%rsp,%rax), %rsi
	movl	$4096, %edx             # imm = 0x1000
	subl	%ebp, %edx
	movq	%r14, %rdi
	leaq	12(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	movl	%ebp, %r9d
	je	.LBB22_3
	jmp	.LBB22_36
.LBB22_25:                              # %vector.body.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	%r12, %r8
	leaq	-32(%rcx), %rbx
	movl	%ebx, %esi
	shrl	$5, %esi
	incl	%esi
	testb	$3, %sil
	je	.LBB22_26
# BB#27:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	32(%rsp), %rsi
	leaq	(%rsi,%rbp), %r12
	leal	-2(%r9,%r11), %edi
	subl	%edx, %edi
	incl	%edi
	andl	$96, %edi
	addl	$-32, %edi
	shrl	$5, %edi
	incl	%edi
	andl	$3, %edi
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB22_28:                              # %vector.body.prol
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%r12,%rsi), %xmm0
	movups	(%r12,%rsi), %xmm1
	movaps	%xmm0, 16(%rsp,%rsi)
	movaps	%xmm1, 32(%rsp,%rsi)
	addq	$32, %rsi
	incq	%rdi
	jne	.LBB22_28
	jmp	.LBB22_29
.LBB22_26:                              #   in Loop: Header=BB22_3 Depth=1
	xorl	%esi, %esi
.LBB22_29:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpq	$96, %rbx
	jb	.LBB22_32
# BB#30:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB22_3 Depth=1
	leal	-2(%r9,%r11), %edi
	subl	%edx, %edi
	incq	%rdi
	movabsq	$8589934560, %rbx       # imm = 0x1FFFFFFE0
	andq	%rbx, %rdi
	subq	%rsi, %rdi
	leaq	128(%rsp), %rbx
	addq	%rbx, %rsi
	.p2align	4, 0x90
.LBB22_31:                              # %vector.body
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp,%rsi), %xmm0
	movups	-96(%rbp,%rsi), %xmm1
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movups	-80(%rbp,%rsi), %xmm0
	movups	-64(%rbp,%rsi), %xmm1
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movups	-48(%rbp,%rsi), %xmm0
	movups	-32(%rbp,%rsi), %xmm1
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movups	-16(%rbp,%rsi), %xmm0
	movups	(%rbp,%rsi), %xmm1
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-128, %rdi
	jne	.LBB22_31
.LBB22_32:                              # %middle.block
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpq	%rcx, %r10
	movq	%r8, %r12
	movl	8(%rsp), %r8d           # 4-byte Reload
	je	.LBB22_19
# BB#33:                                #   in Loop: Header=BB22_3 Depth=1
	leaq	(%rbp,%rcx), %rbp
	jmp	.LBB22_13
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph101
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_5 Depth 2
                                        #     Child Loop BB22_28 Depth 2
                                        #     Child Loop BB22_31 Depth 2
                                        #     Child Loop BB22_15 Depth 2
                                        #     Child Loop BB22_18 Depth 2
	movl	12(%rsp), %r11d
	leal	(%r11,%r9), %r15d
	cmpl	$16, %r15d
	jae	.LBB22_4
# BB#35:                                # %.thread83
	movl	$1, %eax
	jmp	.LBB22_36
.LBB22_7:
	movl	20(%rsp,%rdx), %eax
	movl	%eax, 12(%r12)
	movq	%rdi, 16(%r12)
	movl	28(%rsp,%rdx), %eax
	movq	%rax, 24(%r12)
	movl	$16, %eax
	subl	%r15d, %eax
	addl	%edx, %eax
	movslq	%eax, %rsi
	movq	(%r14), %rdi
	movq	(%rdi), %rax
	addq	$24, %r14
	movl	$1, %edx
	movq	%r14, %rcx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB22_8
# BB#37:                                # %.thread
	xorl	%eax, %eax
.LBB22_36:                              # %_ZN8NArchive4NZip10CInArchive20IncreaseRealPositionEy.exit
	addq	$4120, %rsp             # imm = 0x1018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_8:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$7, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end22:
	.size	_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE, .Lfunc_end22-_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE,@function
_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE: # @_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 80
.Lcfi137:
	.cfi_offset %rbx, -48
.Lcfi138:
	.cfi_offset %r12, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorl	%ebp, %ebp
	cmpb	$0, 176(%rbx)
	jne	.LBB23_42
# BB#1:
.Ltmp118:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE
	movl	%eax, %ebp
.Ltmp119:
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB23_42
# BB#3:
	testb	$8, 2(%rbx)
	jne	.LBB23_4
.LBB23_41:
	xorl	%ebp, %ebp
	jmp	.LBB23_42
.LBB23_4:
	movq	88(%rbx), %rsi
	movl	180(%rbx), %eax
	movzwl	184(%rbx), %ecx
	addq	88(%r14), %rsi
	addq	%rax, %rsi
	addq	%rcx, %rsi
	addq	16(%rbx), %rsi
	movq	(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp120:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp121:
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive4SeekEy.exit
	testl	%ebp, %ebp
	jne	.LBB23_42
# BB#6:
.Ltmp122:
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp123:
# BB#7:                                 # %.noexc45
	testl	%eax, %eax
	jne	.LBB23_8
# BB#10:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i44
	cmpl	$4, 12(%rsp)
	jne	.LBB23_11
# BB#13:
	movl	8(%rsp), %eax
	movl	$1, %ebp
	cmpl	_ZN8NArchive4NZip10NSignature15kDataDescriptorE(%rip), %eax
	jne	.LBB23_42
# BB#14:
.Ltmp128:
	leaq	16(%rsp), %rsi
	leaq	20(%rsp), %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp129:
# BB#15:                                # %.noexc50
	testl	%eax, %eax
	jne	.LBB23_16
# BB#18:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i49
	cmpl	$4, 20(%rsp)
	jne	.LBB23_19
# BB#21:
	movl	16(%rsp), %r15d
.Ltmp135:
	leaq	24(%rsp), %rsi
	leaq	28(%rsp), %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp136:
# BB#22:                                # %.noexc55
	testl	%eax, %eax
	jne	.LBB23_23
# BB#25:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i54
	cmpl	$4, 28(%rsp)
	jne	.LBB23_26
# BB#28:
	movl	24(%rsp), %r12d
.Ltmp141:
	movq	%rsp, %rsi
	leaq	4(%rsp), %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp142:
# BB#29:                                # %.noexc
	testl	%eax, %eax
	jne	.LBB23_30
# BB#32:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
	cmpl	$4, 4(%rsp)
	jne	.LBB23_33
# BB#35:
	cmpl	12(%rbx), %r15d
	jne	.LBB23_42
# BB#36:
	cmpq	%r12, 16(%rbx)
	jne	.LBB23_42
# BB#37:
	movl	(%rsp), %eax
	xorl	%ebp, %ebp
	cmpq	%rax, 24(%rbx)
	setne	%al
	je	.LBB23_41
# BB#38:
	movb	%al, %bpl
.LBB23_42:                              # %.thread
	movl	%ebp, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_8:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp124:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp125:
# BB#9:                                 # %.noexc46
.LBB23_11:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp126:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp127:
# BB#12:                                # %.noexc47
.LBB23_16:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp130:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp131:
# BB#17:                                # %.noexc51
.LBB23_19:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp132:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp133:
# BB#20:                                # %.noexc52
.LBB23_23:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp137:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp138:
# BB#24:                                # %.noexc56
.LBB23_26:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp139:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp140:
# BB#27:                                # %.noexc57
.LBB23_30:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp143:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp144:
# BB#31:                                # %.noexc41
.LBB23_33:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.Ltmp145:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp146:
# BB#34:                                # %.noexc42
.LBB23_39:
.Ltmp147:
	jmp	.LBB23_40
.LBB23_43:
.Ltmp134:
.LBB23_40:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movl	$1, %ebp
	jmp	.LBB23_42
.Lfunc_end23:
	.size	_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE, .Lfunc_end23-_ZN8NArchive4NZip10CInArchive28ReadLocalItemAfterCdItemFullERNS0_7CItemExE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Ltmp118-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp129-.Ltmp118       #   Call between .Ltmp118 and .Ltmp129
	.long	.Ltmp134-.Lfunc_begin5  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp135-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp142-.Ltmp135       #   Call between .Ltmp135 and .Ltmp142
	.long	.Ltmp147-.Lfunc_begin5  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp124-.Ltmp142       #   Call between .Ltmp142 and .Ltmp124
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp134-.Lfunc_begin5  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp125-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp134-.Lfunc_begin5  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp127-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp130-.Ltmp127       #   Call between .Ltmp127 and .Ltmp130
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp134-.Lfunc_begin5  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp131-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin5  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp133-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp137-.Ltmp133       #   Call between .Ltmp133 and .Ltmp137
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp147-.Lfunc_begin5  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp138-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp147-.Lfunc_begin5  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp140-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp143-.Ltmp140       #   Call between .Ltmp140 and .Ltmp143
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp147-.Lfunc_begin5  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp144-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin5  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp146-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Lfunc_end23-.Ltmp146   #   Call between .Ltmp146 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE,@function
_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE: # @_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi148:
	.cfi_def_cfa_offset 128
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	$1, 177(%rbx)
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %r15
	movl	$42, %edx
	movq	%r15, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB24_1
# BB#3:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$42, 12(%rsp)
	jne	.LBB24_4
# BB#5:                                 # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movb	16(%rsp), %al
	movb	%al, 80(%rbx)
	movb	17(%rsp), %al
	movb	%al, 81(%rbx)
	movb	18(%rsp), %al
	movb	%al, (%rbx)
	movb	19(%rsp), %al
	movb	%al, 1(%rbx)
	movzwl	20(%rsp), %eax
	movw	%ax, 2(%rbx)
	movzwl	22(%rsp), %eax
	movw	%ax, 4(%rbx)
	movl	24(%rsp), %eax
	movl	%eax, 8(%rbx)
	movl	28(%rsp), %eax
	movl	%eax, 12(%rbx)
	movq	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	pxor	%xmm1, %xmm1
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqu	%xmm0, 16(%rbx)
	movzwl	42(%rsp), %r13d
	movzwl	44(%rsp), %r12d
	movzwl	40(%rsp), %esi
	movzwl	46(%rsp), %ebp
	movl	%ebp, 12(%rsp)
	movzwl	48(%rsp), %eax
	movw	%ax, 82(%rbx)
	movl	50(%rsp), %eax
	movl	%eax, 84(%rbx)
	movl	54(%rsp), %eax
	movq	%rax, 88(%rbx)
	leaq	32(%rbx), %rdx
	movq	%r14, %rdi
	callq	_ZN8NArchive4NZip10CInArchive12ReadFileNameEjR11CStringBaseIcE
	testl	%r13d, %r13d
	je	.LBB24_7
# BB#6:
	leaq	16(%rbx), %r8
	leaq	88(%rbx), %r9
	leaq	24(%rbx), %rcx
	leaq	120(%rbx), %rdx
	movq	%r15, (%rsp)
	movq	%r14, %rdi
	movl	%r13d, %esi
	callq	_ZN8NArchive4NZip10CInArchive9ReadExtraEjRNS0_11CExtraBlockERyS4_S4_Rj
	movl	12(%rsp), %ebp
.LBB24_7:
	testl	%ebp, %ebp
	jne	.LBB24_8
# BB#9:
	addq	$152, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%r12d, %edx
	callq	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_1:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB24_2
.LBB24_4:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
	jmp	.LBB24_2
.LBB24_8:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$5, (%rax)
.LBB24_2:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end24:
	.size	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE, .Lfunc_end24-_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE,@function
_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE: # @_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 96
.Lcfi158:
	.cfi_offset %rbx, -24
.Lcfi159:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB25_5
# BB#1:
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	$56, %edx
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB25_6
# BB#2:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit
	movl	$1, %eax
	cmpl	$56, 12(%rsp)
	jne	.LBB25_5
# BB#3:
	movl	16(%rsp), %ecx
	cmpl	_ZN8NArchive4NZip10NSignature21kZip64EndOfCentralDirE(%rip), %ecx
	jne	.LBB25_5
# BB#4:
	movups	56(%rsp), %xmm0
	movups	%xmm0, (%r14)
	xorl	%eax, %eax
.LBB25_5:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_6:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end25:
	.size	_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE, .Lfunc_end25-_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE,@function
_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE: # @_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi166:
	.cfi_def_cfa_offset 160
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	leaq	40(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	callq	*48(%rax)
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB26_37
# BB#1:
.Ltmp148:
	movl	$65578, %edi            # imm = 0x1002A
	callq	_Znam
.Ltmp149:
# BB#2:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	40(%rsp), %r15
	cmpq	$65578, %r15            # imm = 0x1002A
	movl	$65578, %ebp            # imm = 0x1002A
	cmovbq	%r15, %rbp
	cmpl	$22, %ebp
	jae	.LBB26_4
# BB#3:
	movl	$1, %ebx
	jmp	.LBB26_36
.LBB26_4:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %r13
	subq	%rbp, %r13
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	leaq	24(%r12), %r14
.Ltmp151:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rcx
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp152:
# BB#5:
	testl	%ebx, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB26_36
# BB#6:
	cmpq	%r13, (%r14)
	jne	.LBB26_30
# BB#7:
.Ltmp153:
	leaq	32(%rsp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%ebp, %edx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp154:
# BB#8:                                 # %.noexc98
	testl	%eax, %eax
	jne	.LBB26_40
# BB#9:
	cmpl	%ebp, 32(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB26_30
# BB#10:
	addl	$-22, %ebp
	js	.LBB26_30
# BB#11:                                # %.lr.ph
	notq	%r15
	cmpq	$-65579, %r15           # imm = 0xFFFEFFD5
	movq	$-65579, %r14           # imm = 0xFFFEFFD5
	cmovaq	%r15, %r14
	movq	$-23, %r15
	subq	%r14, %r15
	movq	%rax, %rbx
	subq	%r14, %rbx
	negq	%r14
	addq	$-7, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_12:                              # =>This Inner Loop Header: Depth=1
	movl	-16(%rbx,%rbp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %eax
	jne	.LBB26_28
# BB#13:                                #   in Loop: Header=BB26_12 Depth=1
	leal	-23(%r14,%rbp), %eax
	cmpl	$20, %eax
	jb	.LBB26_27
# BB#14:                                #   in Loop: Header=BB26_12 Depth=1
	movl	-36(%rbx,%rbp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature28kZip64EndOfCentralDirLocatorE(%rip), %eax
	jne	.LBB26_27
# BB#15:                                #   in Loop: Header=BB26_12 Depth=1
	movq	-28(%rbx,%rbp), %r13
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp158:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	callq	*48(%rax)
.Ltmp159:
# BB#16:                                # %.noexc103
                                        #   in Loop: Header=BB26_12 Depth=1
	testl	%eax, %eax
	jne	.LBB26_21
# BB#17:                                #   in Loop: Header=BB26_12 Depth=1
.Ltmp160:
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	48(%rsp), %rsi
	leaq	36(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp161:
# BB#18:                                # %.noexc104
                                        #   in Loop: Header=BB26_12 Depth=1
	testl	%eax, %eax
	jne	.LBB26_42
# BB#19:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i100
                                        #   in Loop: Header=BB26_12 Depth=1
	cmpl	$56, 36(%rsp)
	jne	.LBB26_21
# BB#20:                                #   in Loop: Header=BB26_12 Depth=1
	movl	48(%rsp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature21kZip64EndOfCentralDirE(%rip), %eax
	je	.LBB26_38
.LBB26_21:                              # %_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE.exit106.thread
                                        #   in Loop: Header=BB26_12 Depth=1
	addq	96(%r12), %r13
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp164:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	callq	*48(%rax)
.Ltmp165:
# BB#22:                                # %.noexc95
                                        #   in Loop: Header=BB26_12 Depth=1
	testl	%eax, %eax
	jne	.LBB26_27
# BB#23:                                #   in Loop: Header=BB26_12 Depth=1
.Ltmp166:
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	48(%rsp), %rsi
	leaq	28(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp167:
# BB#24:                                # %.noexc96
                                        #   in Loop: Header=BB26_12 Depth=1
	testl	%eax, %eax
	jne	.LBB26_44
# BB#25:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
                                        #   in Loop: Header=BB26_12 Depth=1
	cmpl	$56, 28(%rsp)
	jne	.LBB26_27
# BB#26:                                #   in Loop: Header=BB26_12 Depth=1
	movl	48(%rsp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature21kZip64EndOfCentralDirE(%rip), %eax
	je	.LBB26_39
.LBB26_27:                              # %.thread121
                                        #   in Loop: Header=BB26_12 Depth=1
	cmpl	$0, -12(%rbx,%rbp)
	je	.LBB26_33
.LBB26_28:                              #   in Loop: Header=BB26_12 Depth=1
	leaq	-23(%r14,%rbp), %rax
	decq	%rbp
	testq	%rax, %rax
	jg	.LBB26_12
# BB#29:
	movl	$1, %ebx
	jmp	.LBB26_35
.LBB26_30:
	movl	$1, %ebx
	jmp	.LBB26_36
.LBB26_33:
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	%r15, %rax
	movl	12(%rbp,%rax), %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rcx, (%rsi)
	movl	16(%rbp,%rax), %edx
	movq	%rdx, 8(%rsi)
	movq	40(%rsp), %rax
	subq	%rcx, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rbp), %rcx
	xorl	%ebx, %ebx
	cmpq	$22, %rcx
	je	.LBB26_35
# BB#34:
	leaq	-22(%rax,%rbp), %rax
	movq	%rax, 88(%r12)
.LBB26_35:                              # %_ZN7CBufferIhED2Ev.exit94
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB26_36:                              # %_ZN7CBufferIhED2Ev.exit94
	movq	%rax, %rdi
	callq	_ZdaPv
.LBB26_37:
	movl	%ebx, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_38:                              # %_ZN8NArchive4NZip10CInArchive8TryEcd64EyRNS0_7CCdInfoE.exit106
	movups	88(%rsp), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movups	%xmm0, (%rax)
	xorl	%ebx, %ebx
	jmp	.LBB26_35
.LBB26_39:
	movups	88(%rsp), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movups	%xmm0, (%rax)
	movq	96(%r12), %rax
	movq	%rax, 88(%r12)
	xorl	%ebx, %ebx
	jmp	.LBB26_35
.LBB26_40:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp155:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp156:
# BB#41:                                # %.noexc99
.LBB26_42:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp162:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp163:
# BB#43:                                # %.noexc105
.LBB26_44:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp169:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp170:
# BB#45:                                # %.noexc97
.LBB26_46:                              # %.loopexit.split-lp
.Ltmp171:
	jmp	.LBB26_49
.LBB26_47:                              # %.loopexit
.Ltmp168:
	jmp	.LBB26_49
.LBB26_48:
.Ltmp157:
.LBB26_49:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB26_50:
.Ltmp150:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE, .Lfunc_end26-_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp148-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp148
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin6  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp154-.Ltmp151       #   Call between .Ltmp151 and .Ltmp154
	.long	.Ltmp157-.Lfunc_begin6  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp167-.Ltmp158       #   Call between .Ltmp158 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin6  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp155-.Ltmp167       #   Call between .Ltmp167 and .Ltmp155
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin6  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp162-.Ltmp156       #   Call between .Ltmp156 and .Ltmp162
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp171-.Lfunc_begin6  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp169-.Ltmp163       #   Call between .Ltmp163 and .Ltmp169
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin6  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Lfunc_end26-.Ltmp170   #   Call between .Ltmp170 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE,@function
_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE: # @_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi179:
	.cfi_def_cfa_offset 288
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	(%r15), %rdi
	movq	(%rdi), %rax
	leaq	24(%r15), %rbx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rcx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB27_29
# BB#1:
	cmpq	%r13, (%rbx)
	jne	.LBB27_17
# BB#2:
	leaq	40(%r15), %r14
	movl	$32768, %esi            # imm = 0x8000
	movq	%r14, %rdi
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB27_29
# BB#3:
	movq	(%r15), %rsi
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	movb	$1, 32(%r15)
	movq	24(%r15), %rcx
	subq	%r13, %rcx
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB27_18
# BB#4:                                 # %.split.preheader
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jae	.LBB27_28
# BB#5:
	leaq	40(%rsp), %rbp
	movq	%r13, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB27_6:                               # %.lr.ph111
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %edx
	movq	%r15, %rdi
	leaq	12(%rsp), %rsi
	movq	%rbp, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB27_34
# BB#7:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
                                        #   in Loop: Header=BB27_6 Depth=1
	cmpl	$4, 40(%rsp)
	jne	.LBB27_33
# BB#8:                                 # %_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev.exit
                                        #   in Loop: Header=BB27_6 Depth=1
	movl	12(%rsp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature18kCentralFileHeaderE(%rip), %eax
	jne	.LBB27_31
# BB#9:                                 #   in Loop: Header=BB27_6 Depth=1
	movq	$0, 80(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 72(%rsp)
	movb	$0, (%rax)
	movl	$4, 84(%rsp)
	leaq	96(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 112(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 88(%rsp)
	movups	%xmm0, 72(%rax)
	movq	$8, 184(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 160(%rsp)
	movq	$_ZTV7CBufferIhE+16, 192(%rsp)
	movups	%xmm0, 104(%rax)
	movb	$0, 122(%rax)
	movw	$0, 120(%rax)
.Ltmp172:
	movq	%r15, %rdi
	movq	%rbp, %r13
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
.Ltmp173:
# BB#10:                                #   in Loop: Header=BB27_6 Depth=1
.Ltmp174:
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp175:
# BB#11:                                # %.noexc
                                        #   in Loop: Header=BB27_6 Depth=1
.Ltmp176:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp177:
# BB#12:                                #   in Loop: Header=BB27_6 Depth=1
	leaq	96(%rsp), %rax
	movq	%rax, %rcx
	movzwl	128(%rcx), %eax
	movw	%ax, 184(%r14)
	movl	124(%rcx), %eax
	movl	%eax, 180(%r14)
.Ltmp179:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp180:
# BB#13:                                #   in Loop: Header=BB27_6 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rsi
	movq	%r14, (%rax,%rsi,8)
	incq	%rsi
	movl	%esi, 12(%r12)
	movslq	%esi, %rax
	imulq	$274877907, %rax, %rcx  # imm = 0x10624DD3
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$38, %rcx
	addl	%edx, %ecx
	imull	$1000, %ecx, %ecx       # imm = 0x3E8
	cmpl	%ecx, %eax
	jne	.LBB27_16
# BB#14:                                #   in Loop: Header=BB27_6 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp181:
	callq	*8(%rax)
	movl	%eax, %ebp
.Ltmp182:
# BB#15:                                #   in Loop: Header=BB27_6 Depth=1
	testl	%ebp, %ebp
	jne	.LBB27_32
.LBB27_16:                              # %.thread
                                        #   in Loop: Header=BB27_6 Depth=1
	movq	%r13, %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	movq	(%rbx), %rcx
	movq	32(%rsp), %r13          # 8-byte Reload
	subq	%r13, %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jb	.LBB27_6
	jmp	.LBB27_28
.LBB27_17:
	movl	$1, %ebp
	jmp	.LBB27_29
.LBB27_18:                              # %.split.us.preheader
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jae	.LBB27_28
# BB#19:                                # %.lr.ph.preheader
	leaq	40(%rsp), %rbp
	.p2align	4, 0x90
.LBB27_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %edx
	movq	%r15, %rdi
	leaq	12(%rsp), %rsi
	movq	%rbp, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB27_34
# BB#21:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i.us
                                        #   in Loop: Header=BB27_20 Depth=1
	cmpl	$4, 40(%rsp)
	jne	.LBB27_33
# BB#22:                                # %_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev.exit.us
                                        #   in Loop: Header=BB27_20 Depth=1
	movl	12(%rsp), %eax
	cmpl	_ZN8NArchive4NZip10NSignature18kCentralFileHeaderE(%rip), %eax
	jne	.LBB27_31
# BB#23:                                #   in Loop: Header=BB27_20 Depth=1
	movq	$0, 80(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 72(%rsp)
	movb	$0, (%rax)
	movl	$4, 84(%rsp)
	leaq	96(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 112(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 88(%rsp)
	movups	%xmm0, 72(%rax)
	movq	$8, 184(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 160(%rsp)
	movq	$_ZTV7CBufferIhE+16, 192(%rsp)
	movups	%xmm0, 104(%rax)
	movb	$0, 122(%rax)
	movw	$0, 120(%rax)
.Ltmp184:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
.Ltmp185:
# BB#24:                                #   in Loop: Header=BB27_20 Depth=1
.Ltmp186:
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp187:
# BB#25:                                # %.noexc.us
                                        #   in Loop: Header=BB27_20 Depth=1
.Ltmp188:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp189:
# BB#26:                                #   in Loop: Header=BB27_20 Depth=1
	leaq	96(%rsp), %rax
	movq	%rax, %rcx
	movzwl	128(%rcx), %eax
	movw	%ax, 184(%r14)
	movl	124(%rcx), %eax
	movl	%eax, 180(%r14)
.Ltmp191:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp192:
# BB#27:                                # %.split.us
                                        #   in Loop: Header=BB27_20 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	movq	(%rbx), %rcx
	subq	%r13, %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jb	.LBB27_20
.LBB27_28:                              # %.us-lcssa.us
	xorl	%ebp, %ebp
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	setne	%bpl
	jmp	.LBB27_29
.LBB27_31:
	movl	$1, %ebp
.LBB27_29:                              # %.loopexit
	movl	%ebp, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_32:
	leaq	40(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	jmp	.LBB27_29
.LBB27_33:                              # %.us-lcssa47.us
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
	jmp	.LBB27_35
.LBB27_34:                              # %.us-lcssa46.us
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.LBB27_35:                              # %.us-lcssa46.us
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB27_36:                              # %.us-lcssa48.us
.Ltmp190:
	jmp	.LBB27_38
.LBB27_37:                              # %.us-lcssa48
.Ltmp178:
.LBB27_38:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB27_42
.LBB27_39:                              # %.us-lcssa49.us
.Ltmp193:
	jmp	.LBB27_41
.LBB27_40:                              # %.us-lcssa49
.Ltmp183:
.LBB27_41:                              # %.body
	movq	%rax, %rbx
.LBB27_42:                              # %.body
.Ltmp194:
	leaq	40(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp195:
# BB#43:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB27_44:
.Ltmp196:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE, .Lfunc_end27-_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp172-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp172
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp175-.Ltmp172       #   Call between .Ltmp172 and .Ltmp175
	.long	.Ltmp183-.Lfunc_begin7  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin7  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp182-.Ltmp179       #   Call between .Ltmp179 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin7  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp184-.Ltmp182       #   Call between .Ltmp182 and .Ltmp184
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp187-.Ltmp184       #   Call between .Ltmp184 and .Ltmp187
	.long	.Ltmp193-.Lfunc_begin7  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin7  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin7  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp194-.Ltmp192       #   Call between .Ltmp192 and .Ltmp194
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin7  #     jumps to .Ltmp196
	.byte	1                       #   On action: 1
	.long	.Ltmp195-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Lfunc_end27-.Ltmp195   #   Call between .Ltmp195 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE,@function
_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE: # @_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi189:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi190:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi191:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi192:
	.cfi_def_cfa_offset 80
.Lcfi193:
	.cfi_offset %rbx, -56
.Lcfi194:
	.cfi_offset %r12, -48
.Lcfi195:
	.cfi_offset %r13, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$0, 88(%rbx)
	leaq	8(%rsp), %rsi
	callq	_ZN8NArchive4NZip10CInArchive6FindCdERNS0_7CCdInfoE
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB28_9
# BB#1:
	movq	8(%rsp), %rax
	movq	%rax, (%r12)
	movq	%r13, %r8
	movq	16(%rsp), %r13
	movq	%r13, (%r14)
	movq	88(%rbx), %rdx
	addq	%r13, %rdx
	movq	(%r12), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r8, %r14
	callq	_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE
	movl	%eax, %ebp
	cmpl	$1, %ebp
	jne	.LBB28_5
# BB#2:
	movl	$1, %ebp
	cmpq	$0, 88(%rbx)
	jne	.LBB28_5
# BB#3:
	addq	96(%rbx), %r13
	movq	(%r12), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r14, %r8
	callq	_ZN8NArchive4NZip10CInArchive9TryReadCdER13CObjectVectorINS0_7CItemExEEyyPNS0_13CProgressVirtE
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB28_5
# BB#4:
	movq	96(%rbx), %rax
	movq	%rax, 88(%rbx)
	xorl	%ebp, %ebp
.LBB28_5:
	movq	%rsp, %rsi
	leaq	4(%rsp), %rcx
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB28_10
# BB#6:                                 # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	cmpl	$4, 4(%rsp)
	jne	.LBB28_7
# BB#8:
	movl	(%rsp), %eax
	movl	%eax, 8(%rbx)
	jmp	.LBB28_9
.LBB28_7:                               # %_ZN8NArchive4NZip10CInArchive10ReadUInt32ERj.exit
	movl	$1, %ebp
.LBB28_9:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_10:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end28:
	.size	_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE, .Lfunc_end28-_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI29_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi,@function
_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi: # @_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi203:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi205:
	.cfi_def_cfa_offset 512
.Lcfi206:
	.cfi_offset %rbx, -56
.Lcfi207:
	.cfi_offset %r12, -48
.Lcfi208:
	.cfi_offset %r13, -40
.Lcfi209:
	.cfi_offset %r14, -32
.Lcfi210:
	.cfi_offset %r15, -24
.Lcfi211:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	$0, (%rbp)
	movl	8(%r14), %eax
	cmpl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %eax
	movq	%r15, 40(%rsp)          # 8-byte Spill
	jne	.LBB29_15
# BB#1:                                 # %.lr.ph229
	movq	%r13, 8(%rsp)           # 8-byte Spill
	leaq	320(%rsp), %r12
	leaq	24(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	12(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	je	.LBB29_16
# BB#2:
	leaq	264(%rsp), %r15
	.p2align	4, 0x90
.LBB29_3:                               # %.lr.ph229.split
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 304(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 296(%rsp)
	movb	$0, (%rax)
	movl	$4, 308(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	$8, 336(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 312(%rsp)
	movups	%xmm0, 72(%r12)
	movq	$8, 408(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 384(%rsp)
	movq	$_ZTV7CBufferIhE+16, 416(%rsp)
	movups	%xmm0, 104(%r12)
	movb	$0, 122(%r12)
	movw	$0, 120(%r12)
	movq	24(%r14), %rax
	addq	$-4, %rax
	subq	16(%r14), %rax
	movq	%rax, 352(%rsp)
.Ltmp197:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
.Ltmp198:
# BB#4:                                 #   in Loop: Header=BB29_3 Depth=1
	movb	$1, 440(%rsp)
.Ltmp199:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE
.Ltmp200:
# BB#5:                                 #   in Loop: Header=BB29_3 Depth=1
.Ltmp201:
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp202:
# BB#6:                                 # %.noexc
                                        #   in Loop: Header=BB29_3 Depth=1
.Ltmp203:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp204:
# BB#7:                                 #   in Loop: Header=BB29_3 Depth=1
	movzwl	128(%r12), %eax
	movw	%ax, 184(%rbp)
	movl	124(%r12), %eax
	movl	%eax, 180(%rbp)
.Ltmp206:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp207:
# BB#8:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rsi
	movq	%rbp, (%rax,%rsi,8)
	incq	%rsi
	movl	%esi, 12(%rbx)
	movslq	%esi, %rax
	imulq	$1374389535, %rax, %rcx # imm = 0x51EB851F
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$37, %rcx
	addl	%edx, %ecx
	imull	$100, %ecx, %ecx
	cmpl	%ecx, %eax
	jne	.LBB29_11
# BB#9:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp208:
	callq	*8(%rax)
	movl	%eax, %r13d
.Ltmp209:
# BB#10:                                #   in Loop: Header=BB29_3 Depth=1
	testl	%r13d, %r13d
	jne	.LBB29_105
.LBB29_11:                              #   in Loop: Header=BB29_3 Depth=1
.Ltmp210:
	movl	$4, %edx
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	leaq	20(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp211:
# BB#12:                                # %.noexc151
                                        #   in Loop: Header=BB29_3 Depth=1
	testl	%eax, %eax
	jne	.LBB29_107
# BB#13:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
                                        #   in Loop: Header=BB29_3 Depth=1
	cmpl	$4, 20(%rsp)
	jne	.LBB29_26
# BB#14:                                # %.thread
                                        #   in Loop: Header=BB29_3 Depth=1
	movl	16(%rsp), %eax
	movl	%eax, 8(%r14)
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	movl	8(%r14), %eax
	cmpl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %eax
	je	.LBB29_3
	jmp	.LBB29_27
.LBB29_15:                              # %..loopexit174_crit_edge
	leaq	24(%r14), %rax
	leaq	12(%rbx), %r12
	jmp	.LBB29_28
.LBB29_16:                              # %.lr.ph229.split.us.preheader
	leaq	264(%rsp), %r13
	leaq	20(%rsp), %r15
	.p2align	4, 0x90
.LBB29_17:                              # %.lr.ph229.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 304(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 296(%rsp)
	movb	$0, (%rax)
	movl	$4, 308(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	$8, 336(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 312(%rsp)
	movups	%xmm0, 72(%r12)
	movq	$8, 408(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 384(%rsp)
	movq	$_ZTV7CBufferIhE+16, 416(%rsp)
	movups	%xmm0, 104(%r12)
	movb	$0, 122(%r12)
	movw	$0, 120(%r12)
	movq	24(%r14), %rax
	addq	$-4, %rax
	subq	16(%r14), %rax
	movq	%rax, 352(%rsp)
.Ltmp213:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN8NArchive4NZip10CInArchive13ReadLocalItemERNS0_7CItemExE
.Ltmp214:
# BB#18:                                #   in Loop: Header=BB29_17 Depth=1
	movb	$1, 440(%rsp)
.Ltmp215:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN8NArchive4NZip10CInArchive23ReadLocalItemDescriptorERNS0_7CItemExE
.Ltmp216:
# BB#19:                                #   in Loop: Header=BB29_17 Depth=1
.Ltmp217:
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp218:
# BB#20:                                # %.noexc.us
                                        #   in Loop: Header=BB29_17 Depth=1
.Ltmp219:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp220:
# BB#21:                                #   in Loop: Header=BB29_17 Depth=1
	movzwl	128(%r12), %eax
	movw	%ax, 184(%rbp)
	movl	124(%r12), %eax
	movl	%eax, 180(%rbp)
.Ltmp222:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp223:
# BB#22:                                #   in Loop: Header=BB29_17 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbx)
.Ltmp224:
	movl	$4, %edx
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	movq	%r15, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp225:
# BB#23:                                # %.noexc151.us
                                        #   in Loop: Header=BB29_17 Depth=1
	testl	%eax, %eax
	jne	.LBB29_107
# BB#24:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.us
                                        #   in Loop: Header=BB29_17 Depth=1
	cmpl	$4, 20(%rsp)
	jne	.LBB29_26
# BB#25:                                # %.thread.us
                                        #   in Loop: Header=BB29_17 Depth=1
	movl	16(%rsp), %eax
	movl	%eax, 8(%r14)
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	movl	8(%r14), %eax
	cmpl	_ZN8NArchive4NZip10NSignature16kLocalFileHeaderE(%rip), %eax
	je	.LBB29_17
	jmp	.LBB29_27
.LBB29_26:                              # %.thread164
	leaq	264(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.LBB29_27:
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB29_28:                              # %.loopexit174
	movq	(%rax), %rax
	addq	$-4, %rax
	movq	%rax, (%r13)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB29_103
# BB#29:                                # %.lr.ph225
	leaq	128(%rsp), %rbp
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB29_91
	.p2align	4, 0x90
.LBB29_30:                              #   in Loop: Header=BB29_91 Depth=1
	movl	$1, %r13d
	cmpl	_ZN8NArchive4NZip10NSignature18kCentralFileHeaderE(%rip), %eax
	jne	.LBB29_104
# BB#31:                                #   in Loop: Header=BB29_91 Depth=1
	movq	$0, 112(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 104(%rsp)
	movb	$0, (%rax)
	movl	$4, 116(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$8, 144(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%rsp)
	movups	%xmm0, 72(%rbp)
	movq	$8, 216(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 192(%rsp)
	movq	$_ZTV7CBufferIhE+16, 224(%rsp)
	movups	%xmm0, 104(%rbp)
	movb	$0, 122(%rbp)
	movw	$0, 120(%rbp)
.Ltmp232:
	movq	%r14, %rdi
	leaq	72(%rsp), %rsi
	callq	_ZN8NArchive4NZip10CInArchive10ReadCdItemERNS0_7CItemExE
.Ltmp233:
# BB#32:                                #   in Loop: Header=BB29_91 Depth=1
	movl	(%r12), %eax
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB29_39
# BB#33:                                # %.preheader170
                                        #   in Loop: Header=BB29_91 Depth=1
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB29_38
	.p2align	4, 0x90
.LBB29_34:                              # %.lr.ph219
                                        #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	32(%rbx), %rdi
	movq	104(%rsp), %rsi
.Ltmp235:
	callq	_Z15MyStringComparePKcS0_
.Ltmp236:
# BB#35:                                #   in Loop: Header=BB29_34 Depth=2
	testl	%eax, %eax
	je	.LBB29_37
# BB#36:                                #   in Loop: Header=BB29_34 Depth=2
	incq	%rbp
	movslq	(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB29_34
	jmp	.LBB29_38
.LBB29_37:                              # %.critedge
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	88(%rbx), %rax
	subq	160(%rsp), %rax
	movq	%rax, 88(%r14)
	movl	(%r12), %eax
.LBB29_38:                              # %.loopexit171
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpl	%eax, %ebp
	je	.LBB29_101
.LBB29_39:                              # %._crit_edge
                                        #   in Loop: Header=BB29_91 Depth=1
	testl	%eax, %eax
	jle	.LBB29_101
# BB#40:                                # %.lr.ph222
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %r8
	movq	88(%r14), %rdx
	xorl	%edi, %edi
	movq	160(%rsp), %rsi
	.p2align	4, 0x90
.LBB29_41:                              #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rax,%rdi), %ebx
	movl	%ebx, %ebp
	shrl	$31, %ebp
	addl	%ebx, %ebp
	sarl	%ebp
	movslq	%ebp, %rbx
	movq	(%r8,%rbx,8), %r15
	movq	88(%r15), %rbx
	subq	%rdx, %rbx
	cmpq	%rbx, %rsi
	je	.LBB29_43
# BB#42:                                # %.thread165
                                        #   in Loop: Header=BB29_41 Depth=2
	leal	1(%rbp), %ecx
	cmpq	%rbx, %rsi
	cmovbl	%edi, %ecx
	cmovbl	%ebp, %eax
	cmpl	%eax, %ecx
	movl	%ecx, %edi
	jl	.LBB29_41
	jmp	.LBB29_101
	.p2align	4, 0x90
.LBB29_43:                              #   in Loop: Header=BB29_91 Depth=1
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movzwl	152(%rsp), %eax
	movw	%ax, 80(%r15)
	leaq	120(%r15), %rdi
.Ltmp238:
	leaq	192(%rsp), %rsi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp239:
# BB#44:                                # %_ZN8NArchive4NZip11CExtraBlockaSERKS1_.exit
                                        #   in Loop: Header=BB29_91 Depth=1
	movzwl	4(%r15), %edx
	cmpw	76(%rsp), %dx
	jne	.LBB29_101
# BB#45:                                #   in Loop: Header=BB29_91 Depth=1
	movw	2(%r15), %ax
	movw	74(%rsp), %cx
	cmpw	%cx, %ax
	je	.LBB29_47
# BB#46:                                # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit
                                        #   in Loop: Header=BB29_91 Depth=1
	movzwl	%dx, %edx
	cmpl	$7, %edx
	movl	$65535, %esi            # imm = 0xFFFF
	movl	$32767, %edi            # imm = 0x7FFF
	cmovbl	%edi, %esi
	cmpl	$8, %edx
	movl	$32761, %edx            # imm = 0x7FF9
	cmovel	%edx, %esi
	xorl	%eax, %ecx
	testl	%esi, %ecx
	jne	.LBB29_101
.LBB29_47:                              # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit.thread
                                        #   in Loop: Header=BB29_91 Depth=1
	movl	12(%r15), %eax
	cmpl	84(%rsp), %eax
	jne	.LBB29_101
# BB#48:                                #   in Loop: Header=BB29_91 Depth=1
	movl	40(%r15), %eax
	cmpl	112(%rsp), %eax
	jne	.LBB29_101
# BB#49:                                #   in Loop: Header=BB29_91 Depth=1
	movq	16(%r15), %rax
	cmpq	88(%rsp), %rax
	jne	.LBB29_101
# BB#50:                                #   in Loop: Header=BB29_91 Depth=1
	movq	24(%r15), %rax
	cmpq	96(%rsp), %rax
	jne	.LBB29_101
# BB#51:                                #   in Loop: Header=BB29_91 Depth=1
	leaq	72(%rsp), %rax
	cmpq	%r15, %rax
	je	.LBB29_74
# BB#52:                                #   in Loop: Header=BB29_91 Depth=1
	movl	$0, 40(%r15)
	movq	32(%r15), %rax
	movb	$0, (%rax)
	movslq	112(%rsp), %rax
	leaq	1(%rax), %rcx
	movl	44(%r15), %r12d
	cmpl	%r12d, %ecx
	jne	.LBB29_54
# BB#53:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	32(%r15), %rbp
	jmp	.LBB29_71
.LBB29_54:                              #   in Loop: Header=BB29_91 Depth=1
	cmpl	$-1, %eax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp240:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp241:
# BB#55:                                # %.noexc163
                                        #   in Loop: Header=BB29_91 Depth=1
	testl	%r12d, %r12d
	jle	.LBB29_70
# BB#56:                                # %.preheader.i.i
                                        #   in Loop: Header=BB29_91 Depth=1
	movslq	40(%r15), %rax
	testq	%rax, %rax
	movq	32(%r15), %rdi
	jle	.LBB29_68
# BB#57:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpl	$31, %eax
	jbe	.LBB29_61
# BB#58:                                # %min.iters.checked
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB29_61
# BB#59:                                # %vector.memcheck
                                        #   in Loop: Header=BB29_91 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB29_83
# BB#60:                                # %vector.memcheck
                                        #   in Loop: Header=BB29_91 Depth=1
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB29_83
.LBB29_61:                              #   in Loop: Header=BB29_91 Depth=1
	xorl	%ecx, %ecx
.LBB29_62:                              # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB29_91 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB29_65
# BB#63:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB29_91 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB29_64:                              # %.lr.ph.i.i.prol
                                        #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB29_64
.LBB29_65:                              # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpq	$7, %rdx
	jb	.LBB29_69
# BB#66:                                # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB29_91 Depth=1
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB29_67:                              # %.lr.ph.i.i
                                        #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB29_67
	jmp	.LBB29_69
.LBB29_68:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB29_91 Depth=1
	testq	%rdi, %rdi
	je	.LBB29_70
.LBB29_69:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB29_91 Depth=1
	callq	_ZdaPv
.LBB29_70:                              # %._crit_edge17.i.i
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	%rbp, 32(%r15)
	movslq	40(%r15), %rax
	movb	$0, (%rbp,%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 44(%r15)
.LBB29_71:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	104(%rsp), %rax
	.p2align	4, 0x90
.LBB29_72:                              #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB29_72
# BB#73:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB29_91 Depth=1
	movl	112(%rsp), %eax
	movl	%eax, 40(%r15)
.LBB29_74:                              # %_ZN11CStringBaseIcEaSERKS0_.exit
                                        #   in Loop: Header=BB29_91 Depth=1
	movzwl	154(%rsp), %eax
	movw	%ax, 82(%r15)
	movl	156(%rsp), %eax
	movl	%eax, 84(%r15)
	movq	168(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB29_76
# BB#75:                                #   in Loop: Header=BB29_91 Depth=1
	callq	_ZdaPv
.LBB29_76:                              # %_ZN7CBufferIhE4FreeEv.exit.i
                                        #   in Loop: Header=BB29_91 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r15)
	movq	232(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB29_79
# BB#77:                                # %_ZN7CBufferIhE11SetCapacityEm.exit.i
                                        #   in Loop: Header=BB29_91 Depth=1
.Ltmp242:
	movq	%rbp, %rdi
	callq	_Znam
.Ltmp243:
# BB#78:                                # %.noexc161
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	%rax, 168(%r15)
	movq	%rbp, 160(%r15)
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB29_79:                              # %_ZN7CBufferIhEaSERKS0_.exit
                                        #   in Loop: Header=BB29_91 Depth=1
	movb	249(%rsp), %al
	movb	%al, 177(%r15)
.Ltmp244:
	movl	$4, %edx
	movq	%r14, %rdi
	leaq	56(%rsp), %rsi
	leaq	60(%rsp), %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
.Ltmp245:
# BB#80:                                # %.noexc156
                                        #   in Loop: Header=BB29_91 Depth=1
	testl	%eax, %eax
	leaq	128(%rsp), %rbp
	jne	.LBB29_109
# BB#81:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i154
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpl	$4, 60(%rsp)
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jne	.LBB29_101
# BB#82:                                #   in Loop: Header=BB29_91 Depth=1
	movl	56(%rsp), %eax
	movl	%eax, 8(%r14)
	leaq	72(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
	movq	8(%rsp), %rcx           # 8-byte Reload
	incl	%ecx
	movq	64(%rsp), %rax          # 8-byte Reload
	incl	(%rax)
	movl	(%r12), %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %ecx
	movq	48(%rsp), %rbx          # 8-byte Reload
	jl	.LBB29_91
	jmp	.LBB29_96
.LBB29_83:                              # %vector.body.preheader
                                        #   in Loop: Header=BB29_91 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB29_86
# BB#84:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB29_91 Depth=1
	negq	%rsi
	xorl	%edx, %edx
.LBB29_85:                              # %vector.body.prol
                                        #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB29_85
	jmp	.LBB29_87
.LBB29_86:                              #   in Loop: Header=BB29_91 Depth=1
	xorl	%edx, %edx
.LBB29_87:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpq	$96, %r8
	jb	.LBB29_90
# BB#88:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB29_91 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB29_89:                              # %vector.body
                                        #   Parent Loop BB29_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB29_89
.LBB29_90:                              # %middle.block
                                        #   in Loop: Header=BB29_91 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB29_62
	jmp	.LBB29_69
	.p2align	4, 0x90
.LBB29_91:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_34 Depth 2
                                        #     Child Loop BB29_41 Depth 2
                                        #     Child Loop BB29_85 Depth 2
                                        #     Child Loop BB29_89 Depth 2
                                        #     Child Loop BB29_64 Depth 2
                                        #     Child Loop BB29_67 Depth 2
                                        #     Child Loop BB29_72 Depth 2
	testq	%r15, %r15
	je	.LBB29_94
# BB#92:                                #   in Loop: Header=BB29_91 Depth=1
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	imulq	$274877907, %rcx, %rdx  # imm = 0x10624DD3
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$38, %rdx
	addl	%esi, %edx
	imull	$1000, %edx, %edx       # imm = 0x3E8
	subl	%edx, %ecx
	jne	.LBB29_94
# BB#93:                                #   in Loop: Header=BB29_91 Depth=1
	movq	(%r15), %rcx
	movslq	%eax, %rsi
	movq	%r15, %rdi
	callq	*8(%rcx)
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jne	.LBB29_104
.LBB29_94:                              #   in Loop: Header=BB29_91 Depth=1
	movl	8(%r14), %eax
	cmpl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %eax
	jne	.LBB29_30
# BB#95:                                # %..preheader.loopexit_crit_edge
	movl	(%r12), %eax
.LBB29_96:                              # %.preheader
	testl	%eax, %eax
	jle	.LBB29_103
# BB#97:                                # %.lr.ph
	movq	88(%r14), %rcx
	movq	16(%rbx), %rdx
	movslq	%eax, %rbp
	leaq	-1(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	%rbp, %rax
	xorl	%esi, %esi
	andq	$3, %rax
	je	.LBB29_99
	.p2align	4, 0x90
.LBB29_98:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rbx
	subq	%rcx, 88(%rbx)
	incq	%rsi
	cmpq	%rsi, %rax
	jne	.LBB29_98
.LBB29_99:                              # %.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB29_104
	.p2align	4, 0x90
.LBB29_100:                             # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	subq	%rcx, 88(%rax)
	movq	8(%rdx,%rsi,8), %rax
	subq	%rcx, 88(%rax)
	movq	16(%rdx,%rsi,8), %rax
	subq	%rcx, 88(%rax)
	movq	24(%rdx,%rsi,8), %rax
	subq	%rcx, 88(%rax)
	addq	$4, %rsi
	cmpq	%rbp, %rsi
	jl	.LBB29_100
	jmp	.LBB29_104
.LBB29_101:                             # %_ZN8NArchive4NZipL12FlagsAreSameERNS0_5CItemES2_.exit.thread166.thread
	leaq	72(%rsp), %rdi
.LBB29_102:                             # %.loopexit
	callq	_ZN8NArchive4NZip5CItemD2Ev
	jmp	.LBB29_104
.LBB29_103:
	xorl	%r13d, %r13d
.LBB29_104:                             # %.loopexit
	movl	%r13d, %eax
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_105:                             # %.us-lcssa232
	leaq	264(%rsp), %rdi
	jmp	.LBB29_102
.LBB29_107:                             # %.us-lcssa233.us
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp227:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp228:
# BB#108:                               # %.noexc152
.LBB29_109:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
.Ltmp247:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp248:
# BB#110:                               # %.noexc157
.LBB29_111:                             # %.loopexit.split-lp
.Ltmp249:
	jmp	.LBB29_117
.LBB29_112:                             # %.loopexit.split-lp176
.Ltmp229:
	jmp	.LBB29_122
.LBB29_113:
.Ltmp234:
	jmp	.LBB29_117
.LBB29_114:                             # %.loopexit173
.Ltmp246:
	jmp	.LBB29_117
.LBB29_115:                             # %.us-lcssa.us
.Ltmp221:
	jmp	.LBB29_119
.LBB29_116:
.Ltmp237:
.LBB29_117:
	movq	%rax, %rbx
.Ltmp250:
	leaq	72(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp251:
	jmp	.LBB29_124
.LBB29_118:                             # %.us-lcssa
.Ltmp205:
.LBB29_119:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB29_123
.LBB29_120:                             # %.loopexit175.us-lcssa.us
.Ltmp226:
	jmp	.LBB29_122
.LBB29_121:                             # %.loopexit175.us-lcssa
.Ltmp212:
.LBB29_122:                             # %.body
	movq	%rax, %rbx
.LBB29_123:                             # %.body
.Ltmp230:
	leaq	264(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp231:
.LBB29_124:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB29_125:
.Ltmp252:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi, .Lfunc_end29-_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp197-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp197
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp202-.Ltmp197       #   Call between .Ltmp197 and .Ltmp202
	.long	.Ltmp212-.Lfunc_begin8  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin8  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp211-.Ltmp206       #   Call between .Ltmp206 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin8  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp213-.Ltmp211       #   Call between .Ltmp211 and .Ltmp213
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp218-.Ltmp213       #   Call between .Ltmp213 and .Ltmp218
	.long	.Ltmp226-.Lfunc_begin8  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin8  #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp222-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp225-.Ltmp222       #   Call between .Ltmp222 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin8  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp232-.Ltmp225       #   Call between .Ltmp225 and .Ltmp232
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin8  #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin8  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp243-.Ltmp238       #   Call between .Ltmp238 and .Ltmp243
	.long	.Ltmp246-.Lfunc_begin8  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin8  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Ltmp227-.Ltmp245       #   Call between .Ltmp245 and .Ltmp227
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin8  # >> Call Site 16 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin8  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin8  # >> Call Site 17 <<
	.long	.Ltmp247-.Ltmp228       #   Call between .Ltmp228 and .Ltmp247
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin8  # >> Call Site 18 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin8  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin8  # >> Call Site 19 <<
	.long	.Ltmp231-.Ltmp250       #   Call between .Ltmp250 and .Ltmp231
	.long	.Ltmp252-.Lfunc_begin8  #     jumps to .Ltmp252
	.byte	1                       #   On action: 1
	.long	.Ltmp231-.Lfunc_begin8  # >> Call Site 20 <<
	.long	.Lfunc_end29-.Ltmp231   #   Call between .Ltmp231 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip4CEcd5ParseEPKh
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip4CEcd5ParseEPKh,@function
_ZN8NArchive4NZip4CEcd5ParseEPKh:       # @_ZN8NArchive4NZip4CEcd5ParseEPKh
	.cfi_startproc
# BB#0:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%rdi)
	movzwl	4(%rsi), %eax
	movw	%ax, 4(%rdi)
	movzwl	6(%rsi), %eax
	movw	%ax, 6(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movzwl	16(%rsi), %eax
	movw	%ax, 16(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN8NArchive4NZip4CEcd5ParseEPKh, .Lfunc_end30-_ZN8NArchive4NZip4CEcd5ParseEPKh
	.cfi_endproc

	.globl	_ZN8NArchive4NZip6CEcd645ParseEPKh
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip6CEcd645ParseEPKh,@function
_ZN8NArchive4NZip6CEcd645ParseEPKh:     # @_ZN8NArchive4NZip6CEcd645ParseEPKh
	.cfi_startproc
# BB#0:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movq	12(%rsi), %rax
	movq	%rax, 16(%rdi)
	movq	20(%rsi), %rax
	movq	%rax, 24(%rdi)
	movq	28(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	36(%rsi), %rax
	movq	%rax, 40(%rdi)
	retq
.Lfunc_end31:
	.size	_ZN8NArchive4NZip6CEcd645ParseEPKh, .Lfunc_end31-_ZN8NArchive4NZip6CEcd645ParseEPKh
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE,@function
_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE: # @_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi216:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi218:
	.cfi_def_cfa_offset 208
.Lcfi219:
	.cfi_offset %rbx, -56
.Lcfi220:
	.cfi_offset %r12, -48
.Lcfi221:
	.cfi_offset %r13, -40
.Lcfi222:
	.cfi_offset %r14, -32
.Lcfi223:
	.cfi_offset %r15, -24
.Lcfi224:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movb	$0, 136(%r15)
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp253:
	leaq	24(%rsp), %rdx
	leaq	120(%rsp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %r8
	callq	_ZN8NArchive4NZip10CInArchive6ReadCdER13CObjectVectorINS0_7CItemExEERyS6_PNS0_13CProgressVirtE
	movl	%eax, %ebp
.Ltmp254:
# BB#1:
	cmpl	$1, %ebp
	ja	.LBB32_62
# BB#2:
	leaq	12(%rbx), %r13
	movl	12(%rbx), %eax
	movl	%eax, 12(%rsp)
	cmpl	$1, %ebp
	jne	.LBB32_3
.LBB32_6:
	movb	$0, 32(%r15)
	movq	$0, 88(%r15)
	movq	(%r15), %rdi
	movq	96(%r15), %rsi
	movq	(%rdi), %rax
	leaq	24(%r15), %rcx
	xorl	%edx, %edx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB32_62
# BB#7:
	movq	24(%r15), %rax
	movl	$1, %ebp
	cmpq	96(%r15), %rax
	jne	.LBB32_62
# BB#8:
	leaq	8(%rsp), %rsi
	leaq	48(%rsp), %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#11:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i66
	cmpl	$4, 48(%rsp)
	jne	.LBB32_62
# BB#12:
	movl	8(%rsp), %eax
	movl	%eax, 8(%r15)
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r8
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	_ZN8NArchive4NZip10CInArchive15ReadLocalsAndCdER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtERyRi
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB32_62
# BB#13:
	movq	24(%r15), %rbp
	leaq	-4(%rbp), %rax
	movq	24(%rsp), %rcx
	subq	%rcx, %rax
	movq	%rax, 120(%rsp)
	movq	88(%r15), %rbx
	subq	%rbx, %rcx
	movq	%rcx, 24(%rsp)
	jmp	.LBB32_14
.LBB32_3:                               # %._crit_edge
	movq	24(%r15), %rbp
	movq	88(%r15), %rbx
.LBB32_14:
	movl	8(%r15), %eax
	cmpl	_ZN8NArchive4NZip10NSignature21kZip64EndOfCentralDirE(%rip), %eax
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jne	.LBB32_15
# BB#16:
	movb	$1, 136(%r15)
	leaq	48(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movl	$8, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#17:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i
	cmpl	$8, 8(%rsp)
	jne	.LBB32_18
# BB#19:                                # %_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev.exit
	movq	48(%rsp), %r12
	leaq	48(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movl	$44, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#20:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i67
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%r13, 96(%rsp)          # 8-byte Spill
	cmpl	$44, 8(%rsp)
	jne	.LBB32_18
# BB#21:                                # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit68
	movl	52(%rsp), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	56(%rsp), %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movq	60(%rsp), %r13
	movq	68(%rsp), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	76(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	84(%rsp), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	$-44, %r12
	je	.LBB32_26
# BB#22:                                # %.lr.ph.i
	xorl	%r14d, %r14d
	leaq	36(%rsp), %rbx
	leaq	8(%rsp), %rbp
	.p2align	4, 0x90
.LBB32_23:                              # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rcx
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#24:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i.i
                                        #   in Loop: Header=BB32_23 Depth=1
	cmpl	$1, 8(%rsp)
	jne	.LBB32_18
# BB#25:                                # %_ZN8NArchive4NZip10CInArchive8ReadByteEv.exit.i
                                        #   in Loop: Header=BB32_23 Depth=1
	incq	%r14
	cmpq	%r12, %r14
	jb	.LBB32_23
.LBB32_26:                              # %_ZN8NArchive4NZip10CInArchive4SkipEy.exit
	leaq	36(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#27:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i69
	cmpl	$4, 8(%rsp)
	movq	%r13, %r12
	movq	144(%rsp), %rbp         # 8-byte Reload
	jne	.LBB32_35
# BB#28:
	movl	36(%rsp), %eax
	movl	%eax, 8(%r15)
	movl	132(%rsp), %ecx         # 4-byte Reload
	orl	40(%rsp), %ecx          # 4-byte Folded Reload
	movq	136(%rsp), %r14         # 8-byte Reload
	jne	.LBB32_29
# BB#30:
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpq	120(%rsp), %rcx
	movq	96(%rsp), %r13          # 8-byte Reload
	jne	.LBB32_35
# BB#31:
	movslq	12(%rsp), %rcx
	cmpq	%rcx, %r12
	jne	.LBB32_35
# BB#32:
	cmpq	%rcx, 104(%rsp)         # 8-byte Folded Reload
	jne	.LBB32_35
# BB#33:
	cmpq	24(%rsp), %r14
	je	.LBB32_36
# BB#34:
	cmpl	$0, (%r13)
	je	.LBB32_36
.LBB32_35:                              # %.thread116
	movl	$1, %ebp
	jmp	.LBB32_62
.LBB32_15:
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
.LBB32_37:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpl	_ZN8NArchive4NZip10NSignature28kZip64EndOfCentralDirLocatorE(%rip), %eax
	jne	.LBB32_48
# BB#38:
	leaq	8(%rsp), %rsi
	leaq	48(%rsp), %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#39:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i72
	cmpl	$4, 48(%rsp)
	jne	.LBB32_18
# BB#40:                                # %_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev.exit
	leaq	48(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movl	$8, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#41:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i73
	cmpl	$8, 8(%rsp)
	jne	.LBB32_18
# BB#42:                                # %_ZN8NArchive4NZip10CInArchive10ReadUInt64Ev.exit74
	movq	48(%rsp), %rbx
	leaq	8(%rsp), %rsi
	leaq	48(%rsp), %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#43:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i.i75
	cmpl	$4, 48(%rsp)
	jne	.LBB32_18
# BB#44:                                # %_ZN8NArchive4NZip10CInArchive10ReadUInt32Ev.exit76
	addq	$-4, %rbp
	subq	112(%rsp), %rbp         # 8-byte Folded Reload
	movq	%rbp, %rax
	movl	$1, %ebp
	cmpq	%rbx, %rax
	jne	.LBB32_62
# BB#45:
	leaq	8(%rsp), %rsi
	leaq	48(%rsp), %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#46:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i77
	cmpl	$4, 48(%rsp)
	jne	.LBB32_62
# BB#47:                                # %.thread118
	movl	8(%rsp), %eax
	movl	%eax, 8(%r15)
.LBB32_48:
	movl	$1, %ebp
	cmpl	_ZN8NArchive4NZip10NSignature16kEndOfCentralDirE(%rip), %eax
	jne	.LBB32_62
# BB#49:
	movq	%r12, 112(%rsp)         # 8-byte Spill
	leaq	48(%rsp), %rsi
	leaq	8(%rsp), %rcx
	movl	$18, %edx
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive9ReadBytesEPvjPj
	testl	%eax, %eax
	jne	.LBB32_9
# BB#50:                                # %_ZN8NArchive4NZip10CInArchive20ReadBytesAndTestSizeEPvj.exit.i
	movq	%r14, %r12
	movq	%r13, 96(%rsp)          # 8-byte Spill
	cmpl	$18, 8(%rsp)
	jne	.LBB32_18
# BB#51:                                # %_ZN8NArchive4NZip10CInArchive13SafeReadBytesEPvj.exit
	movzwl	48(%rsp), %ebx
	movzwl	50(%rsp), %r14d
	movzwl	52(%rsp), %ebp
	movzwl	54(%rsp), %r13d
	movl	56(%rsp), %esi
	movl	60(%rsp), %eax
	movzwl	64(%rsp), %edx
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	je	.LBB32_52
# BB#53:                                # %.thread124
	xorl	%ecx, %ecx
	cmpl	$65535, %ebx            # imm = 0xFFFF
	cmovel	%ecx, %ebx
	cmpl	$65535, %r14d           # imm = 0xFFFF
	cmovel	%ecx, %r14d
	cmpl	$65535, %ebp            # imm = 0xFFFF
	cmoveq	112(%rsp), %rbp         # 8-byte Folded Reload
	cmpl	$65535, %r13d           # imm = 0xFFFF
	cmoveq	104(%rsp), %r13         # 8-byte Folded Reload
	cmpl	$-1, %esi
	cmoveq	16(%rsp), %rsi          # 8-byte Folded Reload
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	cmpl	$-1, %eax
	jne	.LBB32_54
	jmp	.LBB32_55
.LBB32_52:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
.LBB32_54:
	movq	%rax, %r12
.LBB32_55:
	leaq	112(%r15), %rsi
	movq	%r15, %rdi
	callq	_ZN8NArchive4NZip10CInArchive10ReadBufferER7CBufferIhEj
	orl	%r14d, %ebx
	jne	.LBB32_29
# BB#56:
	movzwl	%bp, %eax
	movl	12(%rsp), %ebx
	movzwl	%bx, %ecx
	movl	$1, %ebp
	cmpl	%ecx, %eax
	jne	.LBB32_62
# BB#57:
	movzwl	%r13w, %ecx
	cmpl	%eax, %ecx
	jne	.LBB32_62
# BB#58:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	120(%rsp), %eax
	jne	.LBB32_62
# BB#59:
	cmpl	24(%rsp), %r12d
	movq	96(%rsp), %r12          # 8-byte Reload
	je	.LBB32_61
# BB#60:
	cmpl	$0, (%r12)
	jne	.LBB32_62
.LBB32_61:
	movb	$0, 32(%r15)
	leaq	40(%r15), %rdi
	callq	_ZN9CInBuffer4FreeEv
	cmpl	(%r12), %ebx
	sete	137(%r15)
	movq	24(%r15), %rax
	movq	%rax, 104(%r15)
	xorl	%ebp, %ebp
.LBB32_62:
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_36:
	movb	$1, %cl
	jmp	.LBB32_37
.LBB32_9:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$6, (%rax)
	jmp	.LBB32_10
.LBB32_18:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
.LBB32_10:
	movl	$_ZTIN8NArchive4NZip19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB32_29:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$5, (%rax)
	jmp	.LBB32_10
.LBB32_4:
.Ltmp255:
	movq	%rax, %rdi
	cmpl	$1, %edx
	jne	.LBB32_63
# BB#5:                                 # %.thread115
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	leaq	12(%rbx), %r13
	movl	12(%rbx), %eax
	movl	%eax, 12(%rsp)
	jmp	.LBB32_6
.LBB32_63:
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE, .Lfunc_end32-_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	49                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp253-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp253
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin9  #     jumps to .Ltmp255
	.byte	3                       #   On action: 2
	.long	.Ltmp254-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Lfunc_end32-.Ltmp254   #   Call between .Ltmp254 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	0                       # >> Action Record 1 <<
                                        #   Cleanup
	.byte	0                       #   No further actions
	.byte	1                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 1
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIN8NArchive4NZip19CInArchiveExceptionE # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy,@function
_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy: # @_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi227:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi229:
	.cfi_def_cfa_offset 48
.Lcfi230:
	.cfi_offset %rbx, -40
.Lcfi231:
	.cfi_offset %r12, -32
.Lcfi232:
	.cfi_offset %r14, -24
.Lcfi233:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 8(%rbx)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
	addq	88(%r15), %r12
	movq	(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp256:
	movq	%rsp, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	*48(%rax)
.Ltmp257:
# BB#1:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB33_3
# BB#2:
	movq	(%r15), %rax
.Ltmp258:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp259:
.LBB33_3:                               # %.noexc10
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp260:
	callq	*16(%rax)
.Ltmp261:
.LBB33_5:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit12
	movq	%r15, 16(%rbx)
	movq	%r14, 24(%rbx)
	movq	$0, 32(%rbx)
	movb	$0, 40(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB33_6:
.Ltmp262:
	movq	%rax, %r14
	movq	(%rbx), %rax
.Ltmp263:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp264:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_8:
.Ltmp265:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy, .Lfunc_end33-_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp256-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp256
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp261-.Ltmp256       #   Call between .Ltmp256 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin10 #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin10 #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp264-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp264   #   Call between .Ltmp264 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy,@function
_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy: # @_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi234:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi235:
	.cfi_def_cfa_offset 32
.Lcfi236:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	leaq	8(%rsp), %rcx
	xorl	%edx, %edx
	callq	*48(%rax)
	testl	%eax, %eax
	sete	%cl
	cmpq	%rbx, 8(%rsp)
	sete	%al
	andb	%cl, %al
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end34:
	.size	_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy, .Lfunc_end34-_ZN8NArchive4NZip10CInArchive13SeekInArchiveEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CInArchive12CreateStreamEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CInArchive12CreateStreamEv,@function
_ZN8NArchive4NZip10CInArchive12CreateStreamEv: # @_ZN8NArchive4NZip10CInArchive12CreateStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 16
.Lcfi238:
	.cfi_offset %rbx, -16
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB35_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB35_2:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end35:
	.size	_ZN8NArchive4NZip10CInArchive12CreateStreamEv, .Lfunc_end35-_ZN8NArchive4NZip10CInArchive12CreateStreamEv
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 16
.Lcfi240:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB36_2
# BB#1:
	callq	_ZdaPv
.LBB36_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end36:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end36-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi243:
	.cfi_def_cfa_offset 32
.Lcfi244:
	.cfi_offset %rbx, -24
.Lcfi245:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp266:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp267:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB37_2:
.Ltmp268:
	movq	%rax, %r14
.Ltmp269:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp270:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp271:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev, .Lfunc_end37-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp266-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin11 #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp269-.Ltmp267       #   Call between .Ltmp267 and .Ltmp269
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin11 #     jumps to .Ltmp271
	.byte	1                       #   On action: 1
	.long	.Ltmp270-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp270   #   Call between .Ltmp270 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi248:
	.cfi_def_cfa_offset 32
.Lcfi249:
	.cfi_offset %rbx, -24
.Lcfi250:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp272:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp273:
# BB#1:
.Ltmp278:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp279:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_5:
.Ltmp280:
	movq	%rax, %r14
	jmp	.LBB38_6
.LBB38_3:
.Ltmp274:
	movq	%rax, %r14
.Ltmp275:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp276:
.LBB38_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB38_4:
.Ltmp277:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev, .Lfunc_end38-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp272-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin12 #     jumps to .Ltmp274
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin12 #     jumps to .Ltmp280
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin12 #     jumps to .Ltmp277
	.byte	1                       #   On action: 1
	.long	.Ltmp276-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end38-.Ltmp276   #   Call between .Ltmp276 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi251:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi254:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi255:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 64
.Lcfi258:
	.cfi_offset %rbx, -56
.Lcfi259:
	.cfi_offset %r12, -48
.Lcfi260:
	.cfi_offset %r13, -40
.Lcfi261:
	.cfi_offset %r14, -32
.Lcfi262:
	.cfi_offset %r15, -24
.Lcfi263:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB39_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB39_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB39_6
# BB#3:                                 #   in Loop: Header=BB39_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB39_5
# BB#4:                                 #   in Loop: Header=BB39_2 Depth=1
	callq	_ZdaPv
.LBB39_5:                               # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit
                                        #   in Loop: Header=BB39_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB39_6:                               #   in Loop: Header=BB39_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB39_2
.LBB39_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end39:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii, .Lfunc_end39-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI40_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi267:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi268:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi270:
	.cfi_def_cfa_offset 64
.Lcfi271:
	.cfi_offset %rbx, -56
.Lcfi272:
	.cfi_offset %r12, -48
.Lcfi273:
	.cfi_offset %r13, -40
.Lcfi274:
	.cfi_offset %r14, -32
.Lcfi275:
	.cfi_offset %r15, -24
.Lcfi276:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	12(%rbx), %r13d
	movl	12(%r15), %esi
	addl	%r13d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r13d, %r13d
	jle	.LBB40_6
# BB#1:                                 # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB40_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movzwl	(%r14), %eax
	movw	%ax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB40_5
# BB#3:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB40_2 Depth=1
.Ltmp281:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp282:
# BB#4:                                 # %.noexc.i
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB40_5:                               # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE3AddERKS2_.exit
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB40_2
.LBB40_6:                               # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEpLERKS3_.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB40_7:
.Ltmp283:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_, .Lfunc_end40-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp281-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin13 #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Lfunc_end40-.Ltmp282   #   Call between .Ltmp282 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI41_0:
	.zero	16
	.section	.text._ZN8NArchive4NZip5CItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip5CItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip5CItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemC2ERKS1_,@function
_ZN8NArchive4NZip5CItemC2ERKS1_:        # @_ZN8NArchive4NZip5CItemC2ERKS1_
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r15
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi279:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi280:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi281:
	.cfi_def_cfa_offset 48
.Lcfi282:
	.cfi_offset %rbx, -48
.Lcfi283:
	.cfi_offset %r12, -40
.Lcfi284:
	.cfi_offset %r13, -32
.Lcfi285:
	.cfi_offset %r14, -24
.Lcfi286:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movslq	40(%r14), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB41_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	%r15d, 44(%rbx)
	jmp	.LBB41_3
.LBB41_1:
	xorl	%eax, %eax
.LBB41_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	leaq	32(%rbx), %r12
	movq	32(%r14), %rcx
	.p2align	4, 0x90
.LBB41_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB41_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movl	40(%r14), %eax
	movl	%eax, 40(%rbx)
	leaq	48(%rbx), %r15
	leaq	48(%r14), %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	$8, 72(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%rbx)
.Ltmp284:
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp285:
# BB#6:                                 # %_ZN8NArchive4NZip10CLocalItemC2ERKS1_.exit
	movq	112(%r14), %rax
	movq	%rax, 112(%rbx)
	movups	80(%r14), %xmm0
	movups	96(%r14), %xmm1
	movups	%xmm1, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	leaq	120(%rbx), %r12
	leaq	120(%r14), %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rbx)
	movq	$8, 144(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%rbx)
.Ltmp290:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_
.Ltmp291:
# BB#7:                                 # %_ZN8NArchive4NZip11CExtraBlockC2ERKS1_.exit
	movq	$_ZTV7CBufferIhE+16, 152(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movq	160(%r14), %r13
	testq	%r13, %r13
	je	.LBB41_10
# BB#8:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i
.Ltmp296:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp297:
# BB#9:                                 # %.noexc
	movq	%rax, 168(%rbx)
	movq	%r13, 160(%rbx)
	movq	160(%r14), %rdx
	movq	168(%r14), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB41_10:                              # %_ZN7CBufferIhEC2ERKS0_.exit
	movb	178(%r14), %al
	movb	%al, 178(%rbx)
	movzwl	176(%r14), %eax
	movw	%ax, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB41_18:
.Ltmp298:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%r12)
.Ltmp299:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp300:
# BB#19:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp305:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp306:
	jmp	.LBB41_20
.LBB41_31:
.Ltmp307:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_16:
.Ltmp301:
	movq	%rax, %r14
.Ltmp302:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp303:
	jmp	.LBB41_32
.LBB41_17:
.Ltmp304:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_15:
.Ltmp292:
	movq	%rax, %r14
.Ltmp293:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp294:
.LBB41_20:                              # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%r15)
.Ltmp308:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp309:
# BB#21:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp314:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp315:
# BB#22:                                # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB41_24
	jmp	.LBB41_25
.LBB41_14:
.Ltmp295:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_28:
.Ltmp316:
	movq	%rax, %r14
	jmp	.LBB41_29
.LBB41_26:
.Ltmp310:
	movq	%rax, %r14
.Ltmp311:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp312:
.LBB41_29:                              # %.body.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB41_30
.LBB41_32:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_30:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_27:
.Ltmp313:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_12:
.Ltmp286:
	movq	%rax, %r14
.Ltmp287:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
# BB#13:                                # %.body.i13
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB41_25
.LBB41_24:
	callq	_ZdaPv
.LBB41_25:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_11:
.Ltmp289:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN8NArchive4NZip5CItemC2ERKS1_, .Lfunc_end41-_ZN8NArchive4NZip5CItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp284-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp284
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin14 #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin14 #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp299-.Ltmp297       #   Call between .Ltmp297 and .Ltmp299
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin14 #     jumps to .Ltmp301
	.byte	1                       #   On action: 1
	.long	.Ltmp305-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp306-.Ltmp305       #   Call between .Ltmp305 and .Ltmp306
	.long	.Ltmp307-.Lfunc_begin14 #     jumps to .Ltmp307
	.byte	1                       #   On action: 1
	.long	.Ltmp302-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin14 #     jumps to .Ltmp304
	.byte	1                       #   On action: 1
	.long	.Ltmp293-.Lfunc_begin14 # >> Call Site 9 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin14 #     jumps to .Ltmp295
	.byte	1                       #   On action: 1
	.long	.Ltmp308-.Lfunc_begin14 # >> Call Site 10 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin14 #     jumps to .Ltmp310
	.byte	1                       #   On action: 1
	.long	.Ltmp314-.Lfunc_begin14 # >> Call Site 11 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin14 #     jumps to .Ltmp316
	.byte	1                       #   On action: 1
	.long	.Ltmp311-.Lfunc_begin14 # >> Call Site 12 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin14 #     jumps to .Ltmp313
	.byte	1                       #   On action: 1
	.long	.Ltmp287-.Lfunc_begin14 # >> Call Site 13 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin14 #     jumps to .Ltmp289
	.byte	1                       #   On action: 1
	.long	.Ltmp288-.Lfunc_begin14 # >> Call Site 14 <<
	.long	.Lfunc_end41-.Ltmp288   #   Call between .Ltmp288 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTSN8NArchive4NZip19CInArchiveExceptionE,@object # @_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.section	.rodata._ZTSN8NArchive4NZip19CInArchiveExceptionE,"aG",@progbits,_ZTSN8NArchive4NZip19CInArchiveExceptionE,comdat
	.weak	_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.p2align	4
_ZTSN8NArchive4NZip19CInArchiveExceptionE:
	.asciz	"N8NArchive4NZip19CInArchiveExceptionE"
	.size	_ZTSN8NArchive4NZip19CInArchiveExceptionE, 38

	.type	_ZTIN8NArchive4NZip19CInArchiveExceptionE,@object # @_ZTIN8NArchive4NZip19CInArchiveExceptionE
	.section	.rodata._ZTIN8NArchive4NZip19CInArchiveExceptionE,"aG",@progbits,_ZTIN8NArchive4NZip19CInArchiveExceptionE,comdat
	.weak	_ZTIN8NArchive4NZip19CInArchiveExceptionE
	.p2align	3
_ZTIN8NArchive4NZip19CInArchiveExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.size	_ZTIN8NArchive4NZip19CInArchiveExceptionE, 16

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 50

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
