	.text
	.file	"z47.bc"
	.globl	EnvInit
	.p2align	4, 0x90
	.type	EnvInit,@function
EnvInit:                                # @EnvInit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, stat_reads(%rip)
	movl	$0, stat_read_hits(%rip)
	movl	$0, stat_writes(%rip)
	movl	$0, stat_write_hits(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_3:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, env_cache(%rip)
	movl	$0, cache_count(%rip)
	movl	$tab, %edi
	xorl	%esi, %esi
	movl	$1688, %edx             # imm = 0x698
	callq	memset
	popq	%rax
	retq
.Lfunc_end0:
	.size	EnvInit, .Lfunc_end0-EnvInit
	.cfi_endproc

	.globl	EnvWriteRetrieve
	.p2align	4, 0x90
	.type	EnvWriteRetrieve,@function
EnvWriteRetrieve:                       # @EnvWriteRetrieve
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	incl	stat_writes(%rip)
	leal	(%rdi,%rsi), %eax
	movl	$2605477791, %ebx       # imm = 0x9B4C6F9F
	imulq	%rax, %rbx
	shrq	$39, %rbx
	imull	$211, %ebx, %ebx
	subl	%ebx, %eax
	movq	tab(,%rax,8), %r9
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.LBB1_17
# BB#1:                                 # %.preheader
	movq	8(%r9), %r10
	cmpq	%r9, %r10
	jne	.LBB1_3
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_10:                               # %.backedge
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	8(%r10), %r10
	cmpq	%r9, %r10
	je	.LBB1_17
.LBB1_3:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #     Child Loop BB1_6 Depth 2
	leaq	16(%r10), %r11
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %r8
	leaq	16(%r8), %r11
	cmpb	$0, 32(%r8)
	je	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	8(%r8), %r14
	addq	$16, %r14
	.p2align	4, 0x90
.LBB1_6:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rbx
	leaq	16(%rbx), %r14
	cmpb	$0, 32(%rbx)
	je	.LBB1_6
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpq	%rdi, %rbx
	jne	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpw	%si, 34(%r8)
	jne	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_3 Depth=1
	testb	$2, 42(%r8)
	jne	.LBB1_10
# BB#11:
	movq	(%r11), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rsi
	cmpq	%rax, %rsi
	je	.LBB1_13
# BB#12:
	movq	%rsi, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rsi
	movq	(%rsi), %rdi
	movq	%rax, 8(%rdi)
	movq	%rsi, 8(%rsi)
	movq	%rsi, (%rsi)
	movq	xx_link(%rip), %rax
.LBB1_13:
	movq	%rax, zz_res(%rip)
	movq	env_cache(%rip), %rsi
	movq	%rsi, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_16
# BB#14:
	testq	%rsi, %rsi
	je	.LBB1_16
# BB#15:
	movq	(%rsi), %rdi
	movq	%rdi, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rsi
	movq	(%rsi), %rdi
	movq	%rax, 8(%rdi)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rsi)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rsi
	movq	%rax, 8(%rsi)
.LBB1_16:
	movl	52(%r8), %eax
	movl	%eax, (%rdx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r8), %eax
	movl	%eax, (%rcx)
	incl	stat_write_hits(%rip)
	movl	$1, %eax
.LBB1_17:                               # %.loopexit
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	EnvWriteRetrieve, .Lfunc_end1-EnvWriteRetrieve
	.cfi_endproc

	.globl	EnvWriteInsert
	.p2align	4, 0x90
	.type	EnvWriteInsert,@function
EnvWriteInsert:                         # @EnvWriteInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %r14
	movl	cache_count(%rip), %eax
	cmpl	$180, %eax
	jl	.LBB2_15
# BB#1:
	movq	env_cache(%rip), %rcx
	movq	8(%rcx), %rcx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB2_2
# BB#3:
	movq	24(%rcx), %rdx
	movq	%rdx, xx_link(%rip)
	movq	%rdx, zz_hold(%rip)
	movq	24(%rdx), %rsi
	cmpq	%rdx, %rsi
	je	.LBB2_5
# BB#4:
	movq	%rsi, zz_res(%rip)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rsi)
	movq	16(%rdx), %rdi
	movq	%rsi, 24(%rdi)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
.LBB2_5:
	movq	%rdx, zz_hold(%rip)
	movq	8(%rdx), %rsi
	cmpq	%rdx, %rsi
	je	.LBB2_7
# BB#6:
	movq	%rsi, zz_res(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
	movq	zz_res(%rip), %rdx
	movq	zz_hold(%rip), %rsi
	movq	(%rsi), %rdi
	movq	%rdx, 8(%rdi)
	movq	%rsi, 8(%rsi)
	movq	%rsi, (%rsi)
	movq	xx_link(%rip), %rdx
.LBB2_7:
	movq	%rdx, zz_hold(%rip)
	movzbl	32(%rdx), %esi
	leaq	zz_lengths(%rsi), %rdi
	movl	%esi, %ebx
	addb	$-11, %bl
	leaq	33(%rdx), %rsi
	cmpb	$2, %bl
	cmovbq	%rsi, %rdi
	movzbl	(%rdi), %esi
	movl	%esi, zz_size(%rip)
	movq	zz_free(,%rsi,8), %rdi
	movq	%rdi, (%rdx)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rsi,8)
	movq	24(%rcx), %rcx
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB2_8
# BB#9:
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
	jmp	.LBB2_10
.LBB2_8:
	xorl	%edx, %edx
.LBB2_10:
	movq	%rdx, xx_tmp(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB2_12
# BB#11:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB2_12:
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_14
# BB#13:
	callq	DisposeObject
	movl	cache_count(%rip), %eax
.LBB2_14:
	decl	%eax
	movl	%eax, cache_count(%rip)
.LBB2_15:
	movzwl	%r12w, %ebp
	addl	%r14d, %ebp
	movl	$2605477791, %eax       # imm = 0x9B4C6F9F
	imulq	%rbp, %rax
	shrq	$39, %rax
	imull	$211, %eax, %eax
	subl	%eax, %ebp
	cmpq	$0, tab(,%rbp,8)
	jne	.LBB2_20
# BB#16:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_17
# BB#18:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_19
.LBB2_17:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_19:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, tab(,%rbp,8)
.LBB2_20:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_21
# BB#22:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_23
.LBB2_21:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_23:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movw	%r12w, 34(%rbx)
	movl	%r15d, 52(%rbx)
	andl	$1048575, %r13d         # imm = 0xFFFFF
	movl	$-1048576, %eax         # imm = 0xFFF00000
	andl	36(%rbx), %eax
	orl	%r13d, %eax
	movl	%eax, 36(%rbx)
	andb	$-3, 42(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_26
.LBB2_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_26:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	tab(,%rbp,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB2_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_29:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_32
# BB#30:
	testq	%rax, %rax
	je	.LBB2_32
# BB#31:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_32:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_33
# BB#34:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_35
.LBB2_33:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_35:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	env_cache(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_38
# BB#36:
	testq	%rcx, %rcx
	je	.LBB2_38
# BB#37:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_38:
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_41
# BB#39:
	testq	%rax, %rax
	je	.LBB2_41
# BB#40:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_41:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_42
# BB#43:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_44
.LBB2_42:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_44:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_47
# BB#45:
	testq	%rax, %rax
	je	.LBB2_47
# BB#46:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_47:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_50
# BB#48:
	testq	%rax, %rax
	je	.LBB2_50
# BB#49:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_50:
	incl	cache_count(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	EnvWriteInsert, .Lfunc_end2-EnvWriteInsert
	.cfi_endproc

	.globl	EnvReadRetrieve
	.p2align	4, 0x90
	.type	EnvReadRetrieve,@function
EnvReadRetrieve:                        # @EnvReadRetrieve
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	incl	stat_reads(%rip)
	leal	(%rdi,%rsi), %eax
	cltq
	imulq	$81421181, %rax, %rcx   # imm = 0x4DA637D
	movq	%rcx, %r8
	shrq	$63, %r8
	sarq	$34, %rcx
	addl	%r8d, %ecx
	imull	$211, %ecx, %ecx
	subl	%ecx, %eax
	cltq
	movq	tab(,%rax,8), %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.LBB3_19
# BB#1:                                 # %.preheader
	movq	8(%r8), %r9
	cmpq	%r8, %r9
	jne	.LBB3_3
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_10:                               # %.backedge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	8(%r9), %r9
	cmpq	%r8, %r9
	je	.LBB3_19
.LBB3_3:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
                                        #     Child Loop BB3_6 Depth 2
	leaq	16(%r9), %r11
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %r10
	leaq	16(%r10), %r11
	cmpb	$0, 32(%r10)
	je	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	8(%r10), %rcx
	.p2align	4, 0x90
.LBB3_6:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpw	%di, 34(%r10)
	jne	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	%esi, 52(%r10)
	jne	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	testb	$2, 42(%r10)
	je	.LBB3_10
# BB#11:
	movq	(%r11), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_13
# BB#12:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_13:
	movq	%rax, zz_res(%rip)
	movq	env_cache(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_16
# BB#14:
	testq	%rcx, %rcx
	je	.LBB3_16
# BB#15:
	movq	(%rcx), %rsi
	movq	%rsi, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rsi
	movq	%rax, 8(%rsi)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_16:
	movq	8(%r10), %rax
	.p2align	4, 0x90
.LBB3_17:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	movq	%rax, (%rdx)
	cmpb	$0, 32(%rax)
	je	.LBB3_17
# BB#18:
	incl	stat_read_hits(%rip)
	movl	$1, %eax
.LBB3_19:                               # %.loopexit
	retq
.Lfunc_end3:
	.size	EnvReadRetrieve, .Lfunc_end3-EnvReadRetrieve
	.cfi_endproc

	.globl	EnvReadInsert
	.p2align	4, 0x90
	.type	EnvReadInsert,@function
EnvReadInsert:                          # @EnvReadInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movl	%edi, %r12d
	movl	cache_count(%rip), %eax
	cmpl	$180, %eax
	jl	.LBB4_15
# BB#1:
	movq	env_cache(%rip), %rcx
	movq	8(%rcx), %rcx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB4_2
# BB#3:
	movq	24(%rcx), %rdx
	movq	%rdx, xx_link(%rip)
	movq	%rdx, zz_hold(%rip)
	movq	24(%rdx), %rsi
	cmpq	%rdx, %rsi
	je	.LBB4_5
# BB#4:
	movq	%rsi, zz_res(%rip)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rsi)
	movq	16(%rdx), %rdi
	movq	%rsi, 24(%rdi)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
.LBB4_5:
	movq	%rdx, zz_hold(%rip)
	movq	8(%rdx), %rsi
	cmpq	%rdx, %rsi
	je	.LBB4_7
# BB#6:
	movq	%rsi, zz_res(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
	movq	zz_res(%rip), %rdx
	movq	zz_hold(%rip), %rsi
	movq	(%rsi), %rdi
	movq	%rdx, 8(%rdi)
	movq	%rsi, 8(%rsi)
	movq	%rsi, (%rsi)
	movq	xx_link(%rip), %rdx
.LBB4_7:
	movq	%rdx, zz_hold(%rip)
	movzbl	32(%rdx), %esi
	leaq	zz_lengths(%rsi), %rdi
	movl	%esi, %ebx
	addb	$-11, %bl
	leaq	33(%rdx), %rsi
	cmpb	$2, %bl
	cmovbq	%rsi, %rdi
	movzbl	(%rdi), %esi
	movl	%esi, zz_size(%rip)
	movq	zz_free(,%rsi,8), %rdi
	movq	%rdi, (%rdx)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rsi,8)
	movq	24(%rcx), %rcx
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB4_8
# BB#9:
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
	jmp	.LBB4_10
.LBB4_8:
	xorl	%edx, %edx
.LBB4_10:
	movq	%rdx, xx_tmp(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB4_12
# BB#11:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB4_12:
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_14
# BB#13:
	callq	DisposeObject
	movl	cache_count(%rip), %eax
.LBB4_14:
	decl	%eax
	movl	%eax, cache_count(%rip)
.LBB4_15:
	movzwl	%r12w, %eax
	addl	%r15d, %eax
	cltq
	imulq	$81421181, %rax, %rcx   # imm = 0x4DA637D
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	imull	$211, %ecx, %ecx
	subl	%ecx, %eax
	movslq	%eax, %rbx
	cmpq	$0, tab(,%rbx,8)
	jne	.LBB4_20
# BB#16:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_17
# BB#18:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_19
.LBB4_17:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_19:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, tab(,%rbx,8)
.LBB4_20:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_21
# BB#22:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_23
.LBB4_21:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB4_23:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	%r12w, 34(%rbp)
	movl	%r15d, 52(%rbp)
	orb	$2, 42(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_26
.LBB4_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_26:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	tab(,%rbx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB4_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_29:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB4_32
# BB#30:
	testq	%rax, %rax
	je	.LBB4_32
# BB#31:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_32:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_33
# BB#34:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_35
.LBB4_33:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_35:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	env_cache(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_38
# BB#36:
	testq	%rcx, %rcx
	je	.LBB4_38
# BB#37:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_38:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB4_41
# BB#39:
	testq	%rax, %rax
	je	.LBB4_41
# BB#40:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_41:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_42
# BB#43:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_44
.LBB4_42:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_44:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB4_47
# BB#45:
	testq	%rax, %rax
	je	.LBB4_47
# BB#46:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_47:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB4_50
# BB#48:
	testq	%rax, %rax
	je	.LBB4_50
# BB#49:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_50:
	incl	cache_count(%rip)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	EnvReadInsert, .Lfunc_end4-EnvReadInsert
	.cfi_endproc

	.type	stat_reads,@object      # @stat_reads
	.local	stat_reads
	.comm	stat_reads,4,4
	.type	stat_read_hits,@object  # @stat_read_hits
	.local	stat_read_hits
	.comm	stat_read_hits,4,4
	.type	stat_writes,@object     # @stat_writes
	.local	stat_writes
	.comm	stat_writes,4,4
	.type	stat_write_hits,@object # @stat_write_hits
	.local	stat_write_hits
	.comm	stat_write_hits,4,4
	.type	env_cache,@object       # @env_cache
	.local	env_cache
	.comm	env_cache,8,8
	.type	cache_count,@object     # @cache_count
	.local	cache_count
	.comm	cache_count,4,4
	.type	tab,@object             # @tab
	.local	tab
	.comm	tab,1688,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
