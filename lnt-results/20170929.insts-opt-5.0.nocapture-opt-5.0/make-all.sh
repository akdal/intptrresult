branch1=insts-opt-5.0
branch2=nocapture-opt-5.0
for i in 2 4 ; do
python2 ../../lnt/parse.py \
  /home/juneyoung.lee/inttoptr/remote-result/freeze${i}/lntresult.0929.${branch1}.${branch2}/mysandbox-copyforrsync/mysandbox cc${i}.csv rt${i}.csv
echo "-----"

for j in 1 5 ;  do
for k in cc rt ; do
  echo "Making cc${i}.sorted.${j}per.csv : "
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 1 4 ${k}${i}.${branch1}.csv
  python2 ../../lnt/columns-get.py ../20170924.base-5.0.insts-5.0.nocapture-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 5 8 ${k}${i}.${branch2}.csv
  echo "sort.py ${k}${i}.${branch1}.csv"
  python2 ../../lnt/sort.py ${k}${i}.${branch1}.csv ${k}${i}.${branch1}.sorted.${j}per.csv ${j}
  echo "sort.py ${k}${i}.${branch2}.csv"
  python2 ../../lnt/sort.py ${k}${i}.${branch2}.csv ${k}${i}.${branch2}.sorted.${j}per.csv ${j}
done
done
echo "-----"
#./filter-and-sort.sh
done

for i in 3 ; do
python2 ../../lnt/parse.py \
  /home/juneyoung.lee/inttoptr/remote-result/freeze${i}/lntresult.0929.${branch1}.${branch2}/mysandbox-copyforrsync/mysandbox cc${i}.csv rt${i}.csv
echo "-----"

for j in 1 5 ;  do
for k in cc rt ; do
  echo "Making cc${i}.sorted.${j}per.csv : "
  python2 ../../lnt/columns-get.py ../20170928.machine3.base-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 1 4 ${k}${i}.${branch1}.csv
  python2 ../../lnt/columns-get.py ../20170928.machine3.base-5.0/${k}${i}.csv 0 4 ${k}${i}.csv 5 8 ${k}${i}.${branch2}.csv
  echo "sort.py ${k}${i}.insts-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.${branch1}.csv ${k}${i}.${branch1}.sorted.${j}per.csv ${j}
  echo "sort.py ${k}${i}.nocapture-5.0.csv"
  python2 ../../lnt/sort.py ${k}${i}.${branch2}.csv ${k}${i}.${branch2}.sorted.${j}per.csv ${j}
done
done
echo "-----"
#./filter-and-sort.sh
done

python2 ../../lnt/maketable.py . rt 2,3,4 ${branch1},${branch2} sorted.5per runtime.csv
python2 ../../lnt/maketable.py . cc 2,3,4 ${branch1},${branch2} sorted.5per compilationtime.csv
