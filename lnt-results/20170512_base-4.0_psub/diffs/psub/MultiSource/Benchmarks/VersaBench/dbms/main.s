	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi6:
	.cfi_def_cfa_offset 368
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %ebp
	leaq	88(%rsp), %rdi
	callq	initMetricsData
	movq	$0, 8(%rsp)
	movq	$0, 40(%rsp)
	movq	$0, 32(%rsp)
	movslq	%ebp, %rdi
	leaq	8(%rsp), %rdx
	leaq	40(%rsp), %rcx
	leaq	32(%rsp), %r8
	movq	%r12, %rsi
	callq	openFiles
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	cmpq	$1, %rcx
	je	.LBB0_41
	jmp	.LBB0_40
.LBB0_2:
	xorl	%edi, %edi
	callq	createIndexNode
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB0_33
# BB#3:
	movq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	getNextCommandCode
	testq	%rax, %rax
	je	.LBB0_34
# BB#4:
	decq	%rax
	cmpq	$1, %rax
	jbe	.LBB0_37
.LBB0_5:
	movq	8(%rsp), %rdi
	callq	clearLine
	cmpl	$4, 4(%rsp)
	je	.LBB0_32
# BB#6:                                 # %.lr.ph89
	leaq	4(%rsp), %r14
	leaq	88(%rsp), %r15
	leaq	56(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_28 Depth 2
                                        #     Child Loop BB0_23 Depth 2
	movq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	getNextCommandCode
	testq	%rax, %rax
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	cmpq	$2, %rax
	je	.LBB0_13
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=1
	cmpq	$1, %rax
	jne	.LBB0_31
	jmp	.LBB0_40
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_7 Depth=1
	movl	4(%rsp), %esi
	movq	%r15, %rdi
	callq	setMetricsData
	movl	4(%rsp), %eax
	cmpq	$5, %rax
	ja	.LBB0_30
# BB#11:                                #   in Loop: Header=BB0_7 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_12:                               #   in Loop: Header=BB0_7 Depth=1
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	callq	errorMessage
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_7 Depth=1
	movq	(%r12), %rdi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
	jmp	.LBB0_31
.LBB0_14:                               #   in Loop: Header=BB0_7 Depth=1
	callq	getTime
	movq	%rax, %r13
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	getInsertCommand
	movq	%rax, %rbp
	callq	getTime
	subq	%r13, %rax
	addq	%rax, 96(%rsp)
	testq	%rbp, %rbp
	jne	.LBB0_18
# BB#15:                                #   in Loop: Header=BB0_7 Depth=1
	movq	56(%rsp), %rsi
	movq	48(%rsp), %rdx
	leaq	24(%rsp), %rdi
	callq	insert
	cmpq	$2, %rax
	je	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_7 Depth=1
	cmpq	$3, %rax
	je	.LBB0_40
# BB#17:                                #   in Loop: Header=BB0_7 Depth=1
	cmpq	$1, %rax
	jne	.LBB0_30
	jmp	.LBB0_40
.LBB0_18:                               #   in Loop: Header=BB0_7 Depth=1
	movq	(%r12), %rdi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
	jmp	.LBB0_30
.LBB0_19:                               #   in Loop: Header=BB0_7 Depth=1
	callq	getTime
	movq	%rax, %r13
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	leaq	16(%rsp), %rdx
	callq	getQueryCommand
	movq	%rax, %rbp
	callq	getTime
	subq	%r13, %rax
	addq	%rax, 96(%rsp)
	testq	%rbp, %rbp
	jne	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_7 Depth=1
	movq	24(%rsp), %rdi
	movq	16(%rsp), %rdx
	movl	$1, %ecx
	movl	$outputQuery, %r8d
	movq	%rbx, %rsi
	callq	query
	decq	%rax
	cmpq	$1, %rax
	ja	.LBB0_22
.LBB0_21:                               #   in Loop: Header=BB0_7 Depth=1
	movq	(%r12), %rdi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
.LBB0_22:                               #   in Loop: Header=BB0_7 Depth=1
	movq	16(%rsp), %r13
	testq	%r13, %r13
	je	.LBB0_30
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph86
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rdi
	movq	16(%r13), %rbp
	callq	free
	movq	%r13, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %r13
	jne	.LBB0_23
	jmp	.LBB0_30
.LBB0_24:                               #   in Loop: Header=BB0_7 Depth=1
	callq	getTime
	movq	%rax, %r13
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	leaq	16(%rsp), %rdx
	callq	getDeleteCommand
	movq	%rax, %rbp
	callq	getTime
	subq	%r13, %rax
	addq	%rax, 96(%rsp)
	testq	%rbp, %rbp
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_7 Depth=1
	movq	16(%rsp), %rdx
	leaq	24(%rsp), %rdi
	movq	%rbx, %rsi
	callq	delete
	decq	%rax
	cmpq	$1, %rax
	ja	.LBB0_27
.LBB0_26:                               #   in Loop: Header=BB0_7 Depth=1
	movq	(%r12), %rdi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
.LBB0_27:                               #   in Loop: Header=BB0_7 Depth=1
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_29
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movq	16(%rbp), %rbx
	callq	free
	movq	%rbp, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB0_28
.LBB0_29:                               # %._crit_edge
                                        #   in Loop: Header=BB0_7 Depth=1
	leaq	56(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_7 Depth=1
	movq	%r15, %rdi
	callq	updateMetricsData
	callq	getTime
	movq	%rax, %rbp
	callq	flushOutputBuffer
	callq	getTime
	subq	%rbp, %rax
	addq	%rax, 104(%rsp)
.LBB0_31:                               #   in Loop: Header=BB0_7 Depth=1
	movq	8(%rsp), %rdi
	callq	clearLine
	cmpl	$4, 4(%rsp)
	jne	.LBB0_7
.LBB0_32:                               # %._crit_edge90
	movq	24(%rsp), %rdi
	callq	deleteIndexNode
	movq	8(%rsp), %rdi
	movq	40(%rsp), %rsi
	movq	32(%rsp), %rdx
	callq	closeFiles
	xorl	%edi, %edi
	callq	exit
.LBB0_33:
	movl	$.L.str, %edi
	jmp	.LBB0_38
.LBB0_34:
	cmpl	$0, 4(%rsp)
	je	.LBB0_36
# BB#35:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	jmp	.LBB0_39
.LBB0_36:
	callq	getTime
	movq	%rax, %r14
	movq	8(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	getInitCommand
	movq	%rax, %rbp
	callq	getTime
	subq	%r14, %rax
	addq	%rax, 96(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_5
.LBB0_37:
	movl	$.L.str.1, %edi
.LBB0_38:
	movl	$1, %esi
.LBB0_39:
	callq	errorMessage
.LBB0_40:
	movq	(%r12), %rdi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
	movl	$-1, %eax
.LBB0_41:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_12
	.quad	.LBB0_14
	.quad	.LBB0_19
	.quad	.LBB0_24
	.quad	.LBB0_30
	.quad	.LBB0_18

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"root node"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Can't read first command (INIT)"
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"First command is not INIT command"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Additional INIT command read"
	.size	.L.str.3, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
