	.text
	.file	"distray.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4647151865492930560     # double 480
.LCPI0_1:
	.quad	4648840715353194496     # double 640
.LCPI0_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI0_5:
	.quad	4560344982675364250     # double 7.8125000000000004E-4
.LCPI0_6:
	.quad	4742290407612743680     # double 1073741823
.LCPI0_7:
	.quad	4607182418800017408     # double 1
.LCPI0_8:
	.quad	4562446662501470481     # double 0.0010416666666666667
.LCPI0_9:
	.quad	-4481081629242032128    # double -1073741823
.LCPI0_10:
	.quad	4643176031446892544     # double 255
.LCPI0_11:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi6:
	.cfi_def_cfa_offset 368
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %rdi
	movl	$.L.str.4, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_25
# BB#1:
	xorl	%r15d, %r15d
	movl	$.L.str.6, %esi
	movl	$DISTRIB, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	%rbx, %rdi
	callq	fclose
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$640, %edx              # imm = 0x280
	movl	$480, %ecx              # imm = 0x1E0
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	DISTRIB(%rip), %eax
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
	leaq	96(%rsp), %r14
	leaq	128(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_13 Depth 3
	movl	$240, %ecx
	subl	%r15d, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	movq	%r15, %rcx
	shlq	$7, %rcx
	leaq	(%rcx,%rcx,4), %r12
	movapd	%xmm0, 208(%rsp)        # 16-byte Spill
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_13 Depth 3
	testl	%eax, %eax
	jle	.LBB0_4
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB0_3 Depth=2
	leal	-320(%r13), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_1(%rip), %xmm0
	movupd	Cameraright(%rip), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	movupd	Cameradir(%rip), %xmm1
	addpd	%xmm2, %xmm1
	movupd	Cameraup(%rip), %xmm2
	mulpd	192(%rsp), %xmm2        # 16-byte Folded Reload
	addpd	%xmm1, %xmm2
	mulsd	Cameraright+16(%rip), %xmm0
	addsd	Cameradir+16(%rip), %xmm0
	movsd	Cameraup+16(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	208(%rsp), %xmm1        # 16-byte Folded Reload
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	andpd	.LCPI0_2(%rip), %xmm0
	ucomisd	.LCPI0_3(%rip), %xmm0
	movapd	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movaps	%xmm4, %xmm5
	mulsd	%xmm5, %xmm5
	addsd	%xmm0, %xmm5
	movapd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm5, %xmm0
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movapd	%xmm2, 176(%rsp)        # 16-byte Spill
	movapd	%xmm1, 160(%rsp)        # 16-byte Spill
	jbe	.LBB0_6
# BB#12:                                # %.lr.ph.split.us.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=2
	movaps	%xmm4, %xmm0
	mulsd	%xmm1, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm8
	xorpd	.LCPI0_4(%rip), %xmm8
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	%xmm8, %xmm6
	unpcklpd	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0]
	mulpd	%xmm1, %xmm6
	mulsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm5, %xmm7
	addsd	%xmm0, %xmm7
	movapd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm5, %xmm5
	addsd	%xmm1, %xmm5
	xorpd	%xmm2, %xmm2
	xorpd	%xmm1, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	addsd	%xmm2, %xmm5
	movapd	%xmm5, 64(%rsp)         # 16-byte Spill
	movapd	%xmm6, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm6, 272(%rsp)        # 16-byte Spill
	movapd	%xmm6, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm7, 256(%rsp)        # 16-byte Spill
	mulsd	%xmm7, %xmm7
	addsd	%xmm2, %xmm7
	movapd	%xmm7, 48(%rsp)         # 16-byte Spill
	unpcklpd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0]
	movapd	%xmm4, 288(%rsp)        # 16-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm4, %xmm4
	sqrtsd	%xmm0, %xmm4
	ucomisd	%xmm4, %xmm4
	jnp	.LBB0_15
# BB#14:                                # %call.sqrt
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	callq	sqrt
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm4
.LBB0_15:                               # %.lr.ph.split.us.i.split
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_17
# BB#16:                                # %call.sqrt65
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	movsd	%xmm4, 16(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	16(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB0_17:                               # %.lr.ph.split.us.i.split.split
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	%xmm4, %xmm1
	divsd	%xmm0, %xmm1
	mulsd	.LCPI0_5(%rip), %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	divsd	%xmm3, %xmm5
	movsd	.LCPI0_7(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm6
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	mulpd	288(%rsp), %xmm6        # 16-byte Folded Reload
	mulsd	.LCPI0_11, %xmm5
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_19
# BB#18:                                # %call.sqrt66
                                        #   in Loop: Header=BB0_13 Depth=3
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	movsd	%xmm4, 16(%rsp)         # 8-byte Spill
	movapd	%xmm5, 240(%rsp)        # 16-byte Spill
	movapd	%xmm6, 224(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	224(%rsp), %xmm6        # 16-byte Reload
	movapd	240(%rsp), %xmm5        # 16-byte Reload
	movsd	16(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	.LCPI0_7(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB0_19:                               # %.lr.ph.split.us.i.split.split.split
                                        #   in Loop: Header=BB0_13 Depth=3
	divsd	%xmm0, %xmm4
	mulsd	.LCPI0_8(%rip), %xmm4
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm0
	movapd	256(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm0, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	272(%rsp), %xmm0        # 16-byte Folded Reload
	movapd	%xmm6, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm6
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	addsd	%xmm1, %xmm5
	addpd	176(%rsp), %xmm6        # 16-byte Folded Reload
	movapd	%xmm6, 96(%rsp)
	addsd	160(%rsp), %xmm5        # 16-byte Folded Reload
	movsd	%xmm5, 112(%rsp)
	movl	$Camerapos, %edi
	movl	$6, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	TraceLine
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	addpd	128(%rsp), %xmm0
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	144(%rsp), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	incl	%ebp
	movl	DISTRIB(%rip), %eax
	cmpl	%eax, %ebp
	jl	.LBB0_13
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=2
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.split.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=2
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	addsd	%xmm1, %xmm5
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	xorl	%ebp, %ebp
	movapd	%xmm5, 16(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.split.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm6, %xmm6
	sqrtsd	%xmm0, %xmm6
	ucomisd	%xmm6, %xmm6
	jnp	.LBB0_9
# BB#8:                                 # %call.sqrt67
                                        #   in Loop: Header=BB0_7 Depth=3
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	callq	sqrt
	movapd	16(%rsp), %xmm5         # 16-byte Reload
	movapd	%xmm0, %xmm6
.LBB0_9:                                # %.lr.ph.split.i.split
                                        #   in Loop: Header=BB0_7 Depth=3
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm5, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_11
# BB#10:                                # %call.sqrt68
                                        #   in Loop: Header=BB0_7 Depth=3
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	movsd	%xmm6, 48(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	48(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
.LBB0_11:                               # %.lr.ph.split.i.split.split
                                        #   in Loop: Header=BB0_7 Depth=3
	movapd	%xmm6, %xmm1
	divsd	%xmm0, %xmm1
	mulsd	.LCPI0_5(%rip), %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm8
	divsd	%xmm8, %xmm2
	movsd	.LCPI0_7(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	64(%rsp), %xmm5         # 16-byte Reload
	movapd	%xmm5, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	176(%rsp), %xmm7        # 16-byte Reload
	movapd	%xmm7, %xmm2
	mulsd	%xmm0, %xmm2
	xorpd	%xmm9, %xmm9
	mulsd	%xmm9, %xmm0
	mulsd	.LCPI0_8(%rip), %xmm6
	imull	$1103515245, %eax, %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	cvtsi2sdl	%eax, %xmm3
	divsd	%xmm8, %xmm3
	subsd	%xmm3, %xmm4
	mulsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm9, %xmm4
	addsd	%xmm4, %xmm1
	subsd	%xmm2, %xmm4
	addsd	%xmm7, %xmm1
	movsd	%xmm1, 96(%rsp)
	addsd	%xmm5, %xmm4
	movsd	%xmm4, 104(%rsp)
	addsd	160(%rsp), %xmm0        # 16-byte Folded Reload
	movsd	%xmm0, 112(%rsp)
	movl	$Camerapos, %edi
	movl	$6, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	TraceLine
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	addpd	128(%rsp), %xmm0
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	144(%rsp), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	incl	%ebp
	movl	DISTRIB(%rip), %eax
	cmpl	%eax, %ebp
	movsd	.LCPI0_9(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	16(%rsp), %xmm5         # 16-byte Reload
	jl	.LBB0_7
.LBB0_20:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_3 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_7(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	32(%rsp), %xmm4         # 16-byte Reload
	movapd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	mulsd	%xmm1, %xmm4
	mulsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	movsd	.LCPI0_10(%rip), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %ecx
	leaq	(%r13,%r12), %rdx
	movb	%cl, memory(%rdx,%rdx,2)
	mulsd	%xmm2, %xmm4
	cvttsd2si	%xmm4, %ecx
	movb	%cl, memory+1(%rdx,%rdx,2)
	mulsd	%xmm2, %xmm1
	cvttsd2si	%xmm1, %ecx
	movb	%cl, memory+2(%rdx,%rdx,2)
	incq	%r13
	cmpq	$640, %r13              # imm = 0x280
	jne	.LBB0_3
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	incq	%r15
	cmpq	$480, %r15              # imm = 0x1E0
	jne	.LBB0_2
# BB#22:                                # %TraceScene.exit
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movzbl	memory(%rbx), %edi
	andl	$254, %edi
	movq	stdout(%rip), %rsi
	callq	fputc
	movzbl	memory+1(%rbx), %edi
	andl	$254, %edi
	movq	stdout(%rip), %rsi
	callq	fputc
	movzbl	memory+2(%rbx), %edi
	addq	$3, %rbx
	andl	$254, %edi
	movq	stdout(%rip), %rsi
	callq	fputc
	cmpq	$921600, %rbx           # imm = 0xE1000
	jl	.LBB0_23
# BB#24:
	xorl	%eax, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_25:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI1_1:
	.quad	4607182418800017408     # double 1
.LCPI1_3:
	.quad	4603909380663173798     # double 0.63661977000000003
.LCPI1_5:
	.quad	4742290407612743680     # double 1073741823
.LCPI1_6:
	.quad	-4481081629242032128    # double -1073741823
.LCPI1_7:
	.quad	-4611686018427387904    # double -2
.LCPI1_8:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI1_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.p2align	4, 0x90
	.type	TraceLine,@function
TraceLine:                              # @TraceLine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$584, %rsp              # imm = 0x248
.Lcfi19:
	.cfi_def_cfa_offset 640
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %r12
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	testl	%r14d, %r14d
	jle	.LBB1_68
# BB#1:
	leaq	288(%rsp), %rdx
	leaq	512(%rsp), %rcx
	leaq	320(%rsp), %r8
	movq	%r12, %rsi
	callq	IntersectObjs
	ucomisd	.LCPI1_0(%rip), %xmm0
	jbe	.LBB1_56
# BB#2:
	movl	$6, %ebp
	subl	%r14d, %ebp
	movupd	Lightpos(%rip), %xmm3
	subpd	288(%rsp), %xmm3
	movapd	%xmm3, 368(%rsp)
	movsd	Lightpos+16(%rip), %xmm2 # xmm2 = mem[0],zero
	subsd	304(%rsp), %xmm2
	movsd	%xmm2, 384(%rsp)
	movapd	512(%rsp), %xmm1
	movapd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, 224(%rsp)        # 16-byte Spill
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movapd	%xmm3, 160(%rsp)        # 16-byte Spill
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movaps	%xmm1, 464(%rsp)        # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	mulsd	%xmm3, %xmm1
	addsd	%xmm0, %xmm1
	movsd	528(%rsp), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	xorpd	%xmm1, %xmm1
	movapd	%xmm3, 496(%rsp)        # 16-byte Spill
	ucomisd	%xmm1, %xmm3
	movapd	%xmm0, 352(%rsp)        # 16-byte Spill
	jbe	.LBB1_29
# BB#3:
	cmpl	$2, %ebp
	movapd	%xmm2, 448(%rsp)        # 16-byte Spill
	jg	.LBB1_26
# BB#4:
	movsd	Lightr(%rip), %xmm0     # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movapd	160(%rsp), %xmm0        # 16-byte Reload
	mulsd	%xmm0, %xmm0
	movapd	48(%rsp), %xmm3         # 16-byte Reload
	mulsd	%xmm3, %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movsd	%xmm3, 144(%rsp)        # 8-byte Spill
	addsd	%xmm3, %xmm0
	movapd	%xmm0, 192(%rsp)        # 16-byte Spill
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_6
# BB#5:                                 # %call.sqrt
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	callq	sqrt
	movapd	448(%rsp), %xmm2        # 16-byte Reload
	xorpd	%xmm1, %xmm1
.LBB1_6:                                # %.split
	cmpl	$0, DISTRIB(%rip)
	jle	.LBB1_29
# BB#7:                                 # %.lr.ph113
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	movapd	.LCPI1_2(%rip), %xmm0   # xmm0 = [nan,nan]
	andpd	%xmm2, %xmm0
	ucomisd	.LCPI1_0(%rip), %xmm0
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	jbe	.LBB1_8
# BB#18:                                # %.lr.ph113.split.us.preheader
	movapd	48(%rsp), %xmm4         # 16-byte Reload
	movapd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	movapd	160(%rsp), %xmm3        # 16-byte Reload
	movapd	%xmm3, %xmm6
	mulsd	%xmm2, %xmm6
	movapd	.LCPI1_4(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm6, %xmm0
	movapd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	mulpd	%xmm1, %xmm2
	movapd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm1
	mulsd	%xmm6, %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm6, %xmm6
	addsd	%xmm1, %xmm6
	xorpd	%xmm1, %xmm1
	addsd	%xmm1, %xmm6
	movapd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm2, 208(%rsp)        # 16-byte Spill
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm3, 144(%rsp)        # 8-byte Spill
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	unpcklpd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0]
	movapd	%xmm5, 64(%rsp)         # 16-byte Spill
	xorl	%r13d, %r13d
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	sqrtsd	%xmm0, %xmm0
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	movapd	%xmm6, 480(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm6, %xmm0
	movsd	%xmm0, 272(%rsp)        # 8-byte Spill
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	%xmm3, 440(%rsp)        # 8-byte Spill
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm3, %xmm0
	movsd	%xmm0, 264(%rsp)        # 8-byte Spill
	leaq	312(%rsp), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph113.split.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	104(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm3
	jnp	.LBB1_21
# BB#20:                                # %call.sqrt165
                                        #   in Loop: Header=BB1_19 Depth=1
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	callq	sqrt
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm3
.LBB1_21:                               # %.lr.ph113.split.us.split
                                        #   in Loop: Header=BB1_19 Depth=1
	movsd	272(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_23
# BB#22:                                # %call.sqrt166
                                        #   in Loop: Header=BB1_19 Depth=1
	movapd	480(%rsp), %xmm0        # 16-byte Reload
	movsd	%xmm3, 176(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	176(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
.LBB1_23:                               # %.lr.ph113.split.us.split.split
                                        #   in Loop: Header=BB1_19 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	movsd	80(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm7, %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm5
	addsd	%xmm4, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm6
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	mulpd	64(%rsp), %xmm6         # 16-byte Folded Reload
	mulsd	.LCPI1_8, %xmm5
	movsd	264(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_25
# BB#24:                                # %call.sqrt167
                                        #   in Loop: Header=BB1_19 Depth=1
	movsd	440(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm3, 176(%rsp)        # 8-byte Spill
	movapd	%xmm5, 416(%rsp)        # 16-byte Spill
	movapd	%xmm6, 400(%rsp)        # 16-byte Spill
	callq	sqrt
	movsd	80(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	400(%rsp), %xmm6        # 16-byte Reload
	movapd	416(%rsp), %xmm5        # 16-byte Reload
	movsd	176(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB1_25:                               # %.lr.ph113.split.us.split.split.split
                                        #   in Loop: Header=BB1_19 Depth=1
	divsd	%xmm0, %xmm3
	mulsd	%xmm7, %xmm3
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm0
	movsd	144(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	208(%rsp), %xmm0        # 16-byte Folded Reload
	movapd	%xmm6, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm6
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	addsd	%xmm1, %xmm5
	movq	384(%rsp), %rax
	movq	%rax, 32(%rsp)
	movapd	368(%rsp), %xmm0
	movapd	%xmm0, 16(%rsp)
	addpd	16(%rsp), %xmm6
	movapd	%xmm6, 16(%rsp)
	addsd	32(%rsp), %xmm5
	movsd	%xmm5, 32(%rsp)
	leaq	288(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	560(%rsp), %rdx
	leaq	536(%rsp), %rcx
	movq	%rbp, %r8
	callq	IntersectObjs
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	ucomisd	%xmm4, %xmm0
	seta	%cl
	orb	%al, %cl
	movzbl	%cl, %eax
	addl	%eax, %r13d
	incl	%r15d
	cmpl	DISTRIB(%rip), %r15d
	jl	.LBB1_19
	jmp	.LBB1_14
.LBB1_56:
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%r12), %xmm0          # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_58
# BB#57:                                # %call.sqrt178
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB1_58:                               # %.split177
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	jbe	.LBB1_60
# BB#59:
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	andpd	.LCPI1_2(%rip), %xmm0
	divsd	%xmm1, %xmm0
	callq	atan
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	mulsd	.LCPI1_3(%rip), %xmm0
.LBB1_60:
	subsd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movsd	Skycolor+32(%rip), %xmm2 # xmm2 = mem[0],zero
	movhpd	Skycolor+24(%rip), %xmm2 # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm1, %xmm2
	movapd	%xmm4, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movsd	Skycolor+8(%rip), %xmm3 # xmm3 = mem[0],zero
	movhpd	Skycolor(%rip), %xmm3   # xmm3 = xmm3[0],mem[0]
	mulpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movupd	%xmm1, (%rbx)
	movapd	%xmm3, %xmm1
	mulsd	Skycolor+40(%rip), %xmm0
	mulsd	Skycolor+16(%rip), %xmm4
	addsd	%xmm0, %xmm4
	jmp	.LBB1_61
.LBB1_26:
	leaq	288(%rsp), %rdi
	leaq	368(%rsp), %rsi
	leaq	560(%rsp), %rdx
	leaq	536(%rsp), %rcx
	leaq	312(%rsp), %r8
	callq	IntersectObjs
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	DISTRIB(%rip), %r13d
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	ja	.LBB1_14
# BB#27:
	ucomisd	.LCPI1_1(%rip), %xmm0
	ja	.LBB1_14
# BB#28:
	movl	12(%rsp), %ebp          # 4-byte Reload
	xorpd	%xmm1, %xmm1
	jmp	.LBB1_29
.LBB1_8:                                # %.lr.ph113.split.preheader
	xorpd	%xmm0, %xmm0
	movsd	144(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	xorl	%r13d, %r13d
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	sqrtsd	%xmm0, %xmm0
	movsd	%xmm0, 176(%rsp)        # 8-byte Spill
	movsd	%xmm1, 144(%rsp)        # 8-byte Spill
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	leaq	312(%rsp), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph113.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	176(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	ucomisd	%xmm6, %xmm6
	jnp	.LBB1_11
# BB#10:                                # %call.sqrt168
                                        #   in Loop: Header=BB1_9 Depth=1
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	callq	sqrt
	movapd	%xmm0, %xmm6
.LBB1_11:                               # %.lr.ph113.split.split
                                        #   in Loop: Header=BB1_9 Depth=1
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_13
# BB#12:                                # %call.sqrt169
                                        #   in Loop: Header=BB1_9 Depth=1
	movsd	144(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm6, 208(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	208(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
.LBB1_13:                               # %.lr.ph113.split.split.split
                                        #   in Loop: Header=BB1_9 Depth=1
	movapd	%xmm6, %xmm1
	divsd	%xmm0, %xmm1
	movsd	80(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	.LCPI1_5(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	divsd	%xmm4, %xmm2
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm5
	movapd	%xmm5, %xmm7
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	mulsd	%xmm0, %xmm1
	movapd	160(%rsp), %xmm2        # 16-byte Reload
	mulsd	%xmm0, %xmm2
	xorpd	%xmm5, %xmm5
	mulsd	%xmm5, %xmm0
	mulsd	%xmm3, %xmm6
	imull	$1103515245, %eax, %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	divsd	%xmm4, %xmm3
	movapd	%xmm7, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm5, %xmm4
	addsd	%xmm4, %xmm1
	subsd	%xmm2, %xmm4
	movq	384(%rsp), %rax
	movq	%rax, 32(%rsp)
	movapd	368(%rsp), %xmm2
	movapd	%xmm2, 16(%rsp)
	addsd	16(%rsp), %xmm1
	movsd	%xmm1, 16(%rsp)
	addsd	24(%rsp), %xmm4
	movsd	%xmm4, 24(%rsp)
	addsd	32(%rsp), %xmm0
	movsd	%xmm0, 32(%rsp)
	leaq	288(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	560(%rsp), %rdx
	leaq	536(%rsp), %rcx
	movq	%rbp, %r8
	callq	IntersectObjs
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	ucomisd	.LCPI1_1(%rip), %xmm0
	seta	%cl
	orb	%al, %cl
	movzbl	%cl, %eax
	addl	%eax, %r13d
	incl	%r15d
	cmpl	DISTRIB(%rip), %r15d
	jl	.LBB1_9
.LBB1_14:                               # %select.unfold
	testl	%r13d, %r13d
	movl	12(%rsp), %ebp          # 4-byte Reload
	xorpd	%xmm1, %xmm1
	jle	.LBB1_29
# BB#15:
	movapd	224(%rsp), %xmm0        # 16-byte Reload
	mulsd	%xmm0, %xmm0
	movapd	464(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	352(%rsp), %xmm0        # 16-byte Reload
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movapd	160(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm1, %xmm1
	movapd	48(%rsp), %xmm2         # 16-byte Reload
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movapd	448(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_17
# BB#16:                                # %call.sqrt171
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB1_17:                               # %.split170
	movapd	496(%rsp), %xmm2        # 16-byte Reload
	divsd	%xmm0, %xmm2
	movq	320(%rsp), %rax
	mulsd	24(%rax), %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	mulsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	DISTRIB(%rip), %xmm0
	divsd	%xmm0, %xmm1
.LBB1_29:                               # %.thread
	movq	320(%rsp), %r13
	movsd	Ambient(%rip), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	mulsd	(%r13), %xmm0
	movsd	%xmm0, (%rbx)
	addsd	Ambient(%rip), %xmm1
	movapd	%xmm1, %xmm2
	movupd	8(%r13), %xmm1
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	movupd	%xmm2, 8(%rbx)
	movsd	32(%r13), %xmm1         # xmm1 = mem[0],zero
	ucomisd	.LCPI1_0(%rip), %xmm1
	jbe	.LBB1_30
# BB#31:
	movupd	(%r12), %xmm1
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	movapd	224(%rsp), %xmm5        # 16-byte Reload
	mulsd	%xmm5, %xmm2
	movapd	%xmm5, %xmm3
	mulsd	%xmm3, %xmm3
	movapd	464(%rsp), %xmm7        # 16-byte Reload
	movapd	%xmm7, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	movapd	352(%rsp), %xmm6        # 16-byte Reload
	movapd	%xmm6, %xmm3
	mulsd	%xmm3, %xmm3
	addsd	%xmm4, %xmm3
	movapd	%xmm1, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	mulsd	%xmm7, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm6, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	.LCPI1_7(%rip), %xmm2
	divsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm6
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm2, %xmm5
	addpd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
	movapd	%xmm1, 16(%rsp)
	addsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm2
	movsd	%xmm6, 32(%rsp)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 240(%rsp)
	movq	$0, 256(%rsp)
	cmpl	$2, %ebp
	jg	.LBB1_54
# BB#32:
	movsd	40(%r13), %xmm5         # xmm5 = mem[0],zero
	ucomisd	.LCPI1_0(%rip), %xmm5
	jbe	.LBB1_54
# BB#33:                                # %.preheader
	movl	DISTRIB(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_34
# BB#35:                                # %.lr.ph
	movapd	.LCPI1_2(%rip), %xmm0   # xmm0 = [nan,nan]
	andpd	%xmm2, %xmm0
	decl	%r14d
	ucomisd	.LCPI1_0(%rip), %xmm0
	movapd	%xmm1, %xmm6
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	movapd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm0
	movaps	%xmm6, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm0, %xmm4
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm0, 176(%rsp)        # 16-byte Spill
	jbe	.LBB1_36
# BB#43:                                # %.lr.ph.split.us.preheader
	movaps	%xmm6, %xmm9
	mulsd	%xmm2, %xmm9
	movapd	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	movapd	.LCPI1_4(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm4, %xmm8
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movapd	%xmm8, %xmm7
	unpcklpd	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0]
	mulpd	%xmm2, %xmm7
	mulsd	%xmm9, %xmm6
	mulsd	%xmm4, %xmm1
	addsd	%xmm6, %xmm1
	movapd	%xmm1, 224(%rsp)        # 16-byte Spill
	movapd	%xmm9, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm4, %xmm4
	addsd	%xmm1, %xmm4
	xorpd	%xmm1, %xmm1
	addsd	%xmm1, %xmm4
	movapd	%xmm7, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm7, 144(%rsp)        # 16-byte Spill
	movapd	%xmm7, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movapd	224(%rsp), %xmm3        # 16-byte Reload
	mulsd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	unpcklpd	%xmm8, %xmm9    # xmm9 = xmm9[0],xmm8[0]
	movapd	%xmm9, 208(%rsp)        # 16-byte Spill
	xorpd	%xmm1, %xmm1
	movapd	%xmm1, 80(%rsp)         # 16-byte Spill
	movl	$1, %r15d
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	movsd	%xmm1, 104(%rsp)        # 8-byte Spill
	movapd	%xmm4, 400(%rsp)        # 16-byte Spill
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm4, %xmm1
	movsd	%xmm1, 272(%rsp)        # 8-byte Spill
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm3, 352(%rsp)        # 16-byte Spill
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm3, %xmm1
	movsd	%xmm1, 264(%rsp)        # 8-byte Spill
	leaq	112(%rsp), %r12
	leaq	328(%rsp), %rbp
	xorpd	%xmm1, %xmm1
	movsd	%xmm1, 160(%rsp)        # 8-byte Spill
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_51:                               # %.lr.ph.split.us..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB1_44 Depth=1
	movsd	%xmm1, 160(%rsp)        # 8-byte Spill
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movsd	40(%r13), %xmm5         # xmm5 = mem[0],zero
	incl	%r15d
	movapd	176(%rsp), %xmm0        # 16-byte Reload
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
.LBB1_44:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movsd	104(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	ucomisd	%xmm6, %xmm6
	movsd	%xmm5, 48(%rsp)         # 8-byte Spill
	jnp	.LBB1_46
# BB#45:                                # %call.sqrt172
                                        #   in Loop: Header=BB1_44 Depth=1
	callq	sqrt
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm0, %xmm6
.LBB1_46:                               # %.lr.ph.split.us.split
                                        #   in Loop: Header=BB1_44 Depth=1
	movsd	272(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_48
# BB#47:                                # %call.sqrt173
                                        #   in Loop: Header=BB1_44 Depth=1
	movapd	400(%rsp), %xmm0        # 16-byte Reload
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
.LBB1_48:                               # %.lr.ph.split.us.split.split
                                        #   in Loop: Header=BB1_44 Depth=1
	movapd	%xmm6, %xmm1
	divsd	%xmm0, %xmm1
	mulsd	%xmm5, %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	divsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm7
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	mulpd	208(%rsp), %xmm7        # 16-byte Folded Reload
	mulsd	.LCPI1_8, %xmm3
	movsd	264(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_50
# BB#49:                                # %call.sqrt174
                                        #   in Loop: Header=BB1_44 Depth=1
	movapd	352(%rsp), %xmm0        # 16-byte Reload
	movapd	%xmm3, 192(%rsp)        # 16-byte Spill
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	movapd	%xmm7, 416(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	416(%rsp), %xmm7        # 16-byte Reload
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	192(%rsp), %xmm3        # 16-byte Reload
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI1_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
.LBB1_50:                               # %.lr.ph.split.us.split.split.split
                                        #   in Loop: Header=BB1_44 Depth=1
	divsd	%xmm0, %xmm6
	mulsd	%xmm6, %xmm5
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	mulsd	%xmm5, %xmm0
	movapd	224(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm0, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	144(%rsp), %xmm0        # 16-byte Folded Reload
	movapd	%xmm7, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm7
	movsd	%xmm2, %xmm7            # xmm7 = xmm2[0],xmm7[1]
	addsd	%xmm1, %xmm3
	movq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	movaps	16(%rsp), %xmm0
	movaps	%xmm0, 112(%rsp)
	addpd	112(%rsp), %xmm7
	movapd	%xmm7, 112(%rsp)
	addsd	128(%rsp), %xmm3
	movsd	%xmm3, 128(%rsp)
	leaq	288(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	callq	TraceLine
	movsd	336(%rsp), %xmm0        # xmm0 = mem[0],zero
	movhpd	328(%rsp), %xmm0        # xmm0 = xmm0[0],mem[0]
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	160(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	344(%rsp), %xmm1
	movl	DISTRIB(%rip), %eax
	cmpl	%eax, %r15d
	jl	.LBB1_51
	jmp	.LBB1_52
.LBB1_30:
	movapd	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movapd	%xmm2, %xmm1
	ucomisd	.LCPI1_1(%rip), %xmm0
	ja	.LBB1_63
	jmp	.LBB1_64
.LBB1_54:
	decl	%r14d
	leaq	288(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	240(%rsp), %rdx
	movl	%r14d, %ecx
	callq	TraceLine
	movsd	248(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	256(%rsp), %xmm4        # xmm4 = mem[0],zero
	movhpd	240(%rsp), %xmm0        # xmm0 = xmm0[0],mem[0]
	jmp	.LBB1_55
.LBB1_34:
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	jmp	.LBB1_53
.LBB1_36:                               # %.lr.ph.split.preheader
	xorpd	%xmm2, %xmm2
	addsd	%xmm2, %xmm4
	movapd	%xmm4, 272(%rsp)        # 16-byte Spill
	xorpd	%xmm2, %xmm2
	movapd	%xmm2, 80(%rsp)         # 16-byte Spill
	movl	$1, %r15d
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	movsd	%xmm2, 208(%rsp)        # 8-byte Spill
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm4, %xmm2
	movsd	%xmm2, 144(%rsp)        # 8-byte Spill
	leaq	112(%rsp), %r12
	leaq	328(%rsp), %rbp
	movapd	%xmm1, 224(%rsp)        # 16-byte Spill
	xorpd	%xmm1, %xmm1
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_42:                               # %.lr.ph.split..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB1_37 Depth=1
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movsd	40(%r13), %xmm5         # xmm5 = mem[0],zero
	incl	%r15d
	movapd	176(%rsp), %xmm0        # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB1_37:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 160(%rsp)        # 8-byte Spill
	movsd	208(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	ucomisd	%xmm3, %xmm3
	jnp	.LBB1_39
# BB#38:                                # %call.sqrt175
                                        #   in Loop: Header=BB1_37 Depth=1
	movsd	%xmm5, 48(%rsp)         # 8-byte Spill
	callq	sqrt
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm0, %xmm3
.LBB1_39:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB1_37 Depth=1
	movsd	144(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_41
# BB#40:                                # %call.sqrt176
                                        #   in Loop: Header=BB1_37 Depth=1
	movapd	272(%rsp), %xmm0        # 16-byte Reload
	movsd	%xmm5, 48(%rsp)         # 8-byte Spill
	movsd	%xmm3, 104(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	104(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
.LBB1_41:                               # %.lr.ph.split.split.split
                                        #   in Loop: Header=BB1_37 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	mulsd	%xmm5, %xmm1
	imull	$1103515245, rnd(%rip), %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	.LCPI1_5(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	divsd	%xmm4, %xmm2
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm7
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	224(%rsp), %xmm2        # 16-byte Reload
	mulsd	%xmm0, %xmm2
	xorps	%xmm6, %xmm6
	mulsd	%xmm6, %xmm0
	mulsd	%xmm3, %xmm5
	imull	$1103515245, %eax, %eax # imm = 0x41C64E6D
	addl	$12345, %eax            # imm = 0x3039
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, rnd(%rip)
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	divsd	%xmm4, %xmm3
	movapd	%xmm7, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm5, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm1
	subsd	%xmm2, %xmm4
	movq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	movapd	16(%rsp), %xmm2
	movapd	%xmm2, 112(%rsp)
	addsd	112(%rsp), %xmm1
	movsd	%xmm1, 112(%rsp)
	addsd	120(%rsp), %xmm4
	movsd	%xmm4, 120(%rsp)
	addsd	128(%rsp), %xmm0
	movsd	%xmm0, 128(%rsp)
	leaq	288(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	callq	TraceLine
	movsd	336(%rsp), %xmm0        # xmm0 = mem[0],zero
	movhpd	328(%rsp), %xmm0        # xmm0 = xmm0[0],mem[0]
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	160(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	344(%rsp), %xmm1
	movl	DISTRIB(%rip), %eax
	cmpl	%eax, %r15d
	jl	.LBB1_42
.LBB1_52:                               # %._crit_edge
	movhpd	%xmm0, 240(%rsp)
	movlpd	%xmm0, 248(%rsp)
	movsd	%xmm1, 256(%rsp)
	movapd	%xmm1, %xmm2
	movapd	%xmm0, %xmm1
.LBB1_53:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	divsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm1, %xmm0
	movhpd	%xmm0, 240(%rsp)
	movlpd	%xmm0, 248(%rsp)
	mulsd	%xmm2, %xmm4
	movsd	%xmm4, 256(%rsp)
.LBB1_55:
	movsd	32(%r13), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm4
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movhpd	(%rbx), %xmm0           # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movupd	%xmm0, (%rbx)
	addsd	16(%rbx), %xmm4
.LBB1_61:                               # %.sink.split
	movsd	%xmm4, 16(%rbx)
	movapd	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	ucomisd	.LCPI1_1(%rip), %xmm0
	jbe	.LBB1_64
.LBB1_63:
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%rbx)
.LBB1_64:
	ucomisd	.LCPI1_1(%rip), %xmm1
	jbe	.LBB1_66
# BB#65:
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 8(%rbx)
.LBB1_66:
	ucomisd	.LCPI1_1(%rip), %xmm4
	jbe	.LBB1_68
# BB#67:
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 16(%rbx)
.LBB1_68:
	addq	$584, %rsp              # imm = 0x248
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	TraceLine, .Lfunc_end1-TraceLine
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI2_2:
	.quad	-4616189618054758400    # double -1
.LCPI2_3:
	.quad	4681608360884174848     # double 1.0E+5
.LCPI2_4:
	.quad	4677104761256804352     # double 5.0E+4
.LCPI2_5:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	IntersectObjs,@function
IntersectObjs:                          # @IntersectObjs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 128
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movapd	.LCPI2_0(%rip), %xmm1   # xmm1 = [nan,nan]
	andpd	%xmm0, %xmm1
	ucomisd	.LCPI2_1(%rip), %xmm1
	jbe	.LBB2_4
# BB#1:
	movsd	Groundpos(%rip), %xmm8  # xmm8 = mem[0],zero
	leaq	16(%r13), %rbp
	movsd	16(%r13), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm1, %xmm8
	divsd	%xmm0, %xmm8
	movsd	.LCPI2_2(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	.LCPI2_1(%rip), %xmm8
	jbe	.LBB2_6
# BB#2:
	movsd	.LCPI2_3(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm8, %xmm3
	jbe	.LBB2_6
# BB#3:
	movupd	(%r13), %xmm2
	movupd	(%rbx), %xmm3
	movapd	%xmm8, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm4
	movupd	%xmm4, (%r12)
	mulsd	%xmm8, %xmm0
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%r12)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r15)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 16(%r15)
	movsd	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	addsd	8(%r12), %xmm0
	cvttsd2si	%xmm0, %ecx
	addl	%eax, %ecx
	andl	$1, %ecx
	leaq	(%rcx,%rcx,2), %rax
	shlq	$4, %rax
	leaq	Groundtxt(%rax), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	jmp	.LBB2_7
.LBB2_4:                                # %..preheader_crit_edge
	leaq	16(%r13), %rbp
	movsd	.LCPI2_2(%rip), %xmm8   # xmm8 = mem[0],zero
	jmp	.LBB2_7
.LBB2_6:
	movapd	%xmm2, %xmm8
.LBB2_7:                                # %.preheader
	xorl	%r14d, %r14d
	movsd	.LCPI2_5(%rip), %xmm4   # xmm4 = mem[0],zero
	xorps	%xmm7, %xmm7
	movsd	.LCPI2_1(%rip), %xmm9   # xmm9 = mem[0],zero
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movapd	objs(%r14), %xmm6
	movsd	objs+16(%r14), %xmm10   # xmm10 = mem[0],zero
	movupd	(%r13), %xmm0
	subpd	%xmm0, %xmm6
	subsd	(%rbp), %xmm10
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm5          # xmm5 = mem[0],zero
	movapd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm10, %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	movapd	%xmm4, %xmm2
	divsd	%xmm0, %xmm2
	movapd	%xmm6, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm5
	addsd	%xmm1, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm2, %xmm5
	movsd	objs+24(%r14), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	movapd	%xmm6, %xmm3
	mulsd	%xmm3, %xmm3
	subsd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm10, %xmm0
	mulsd	%xmm0, %xmm0
	subsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	ucomisd	%xmm7, %xmm0
	jbe	.LBB2_15
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_11
# BB#10:                                # %call.sqrt
                                        #   in Loop: Header=BB2_8 Depth=1
	movapd	%xmm8, 48(%rsp)         # 16-byte Spill
	movsd	%xmm10, 8(%rsp)         # 8-byte Spill
	movapd	%xmm6, 32(%rsp)         # 16-byte Spill
	movapd	%xmm5, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm5         # 16-byte Reload
	movapd	32(%rsp), %xmm6         # 16-byte Reload
	movsd	8(%rsp), %xmm10         # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movsd	.LCPI2_1(%rip), %xmm7   # xmm7 = mem[0],zero
	movaps	%xmm7, %xmm9
	xorps	%xmm7, %xmm7
	movsd	.LCPI2_5(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	48(%rsp), %xmm8         # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB2_11:                               # %.split
                                        #   in Loop: Header=BB2_8 Depth=1
	movapd	%xmm5, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm5
	movapd	%xmm2, %xmm0
	cmpltsd	%xmm9, %xmm0
	andpd	%xmm0, %xmm5
	andnpd	%xmm2, %xmm0
	orpd	%xmm5, %xmm0
	ucomisd	%xmm9, %xmm0
	jbe	.LBB2_15
# BB#12:                                #   in Loop: Header=BB2_8 Depth=1
	ucomisd	%xmm8, %xmm7
	ja	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_8 Depth=1
	ucomisd	%xmm0, %xmm8
	jbe	.LBB2_15
.LBB2_14:                               #   in Loop: Header=BB2_8 Depth=1
	movupd	(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	movupd	%xmm2, (%r12)
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%r12)
	subpd	%xmm6, %xmm2
	movupd	%xmm2, (%r15)
	subsd	%xmm10, %xmm1
	movsd	%xmm1, 16(%r15)
	movupd	(%r13), %xmm1
	movupd	(%r12), %xmm2
	addpd	%xmm1, %xmm2
	movupd	%xmm2, (%r12)
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	addsd	16(%r12), %xmm1
	movsd	%xmm1, 16(%r12)
	leaq	objs+32(%r14), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	movapd	%xmm0, %xmm8
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_8 Depth=1
	addq	$80, %r14
	cmpq	$320, %r14              # imm = 0x140
	jne	.LBB2_8
# BB#16:
	movapd	%xmm8, %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	IntersectObjs, .Lfunc_end2-IntersectObjs
	.cfi_endproc

	.type	objs,@object            # @objs
	.data
	.globl	objs
	.p2align	4
objs:
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	0                       # double 0
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4581421828931458171     # double 0.02
	.quad	-4616189618054758400    # double -1
	.quad	4613937818241073152     # double 3
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4602678819172646912     # double 0.5
	.quad	4599075939470750515     # double 0.29999999999999999
	.quad	4607182418800017408     # double 1
	.quad	4602678819172646912     # double 0.5
	.quad	4606281698874543309     # double 0.90000000000000002
	.quad	4576918229304087675     # double 0.01
	.quad	-4624296097384025293    # double -0.29999999999999999
	.quad	4607182418800017408     # double 1
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4591870180066957722     # double 0.10000000000000001
	.quad	4606732058837280358     # double 0.94999999999999996
	.quad	4596373779694328218     # double 0.20000000000000001
	.quad	4603579539098121011     # double 0.59999999999999998
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4576918229304087675     # double 0.01
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4605921410904353669     # double 0.85999999999999999
	.quad	4605651194926711439     # double 0.82999999999999996
	.quad	0                       # double 0
	.quad	4604480259023595110     # double 0.69999999999999996
	.quad	4603579539098121011     # double 0.59999999999999998
	.quad	4576918229304087675     # double 0.01
	.size	objs, 320

	.type	Groundpos,@object       # @Groundpos
	.bss
	.globl	Groundpos
	.p2align	3
Groundpos:
	.quad	0                       # double 0
	.size	Groundpos, 8

	.type	Groundtxt,@object       # @Groundtxt
	.data
	.globl	Groundtxt
	.p2align	4
Groundtxt:
	.quad	0                       # double 0
	.quad	4591870180066957722     # double 0.10000000000000001
	.quad	4602678819172646912     # double 0.5
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4601597955262077993     # double 0.44
	.quad	4581421828931458171     # double 0.02
	.quad	4603579539098121011     # double 0.59999999999999998
	.quad	4607182418800017408     # double 1
	.quad	4602678819172646912     # double 0.5
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4601597955262077993     # double 0.44
	.quad	4576918229304087675     # double 0.01
	.size	Groundtxt, 96

	.type	Lightpos,@object        # @Lightpos
	.globl	Lightpos
	.p2align	3
Lightpos:
	.quad	-4609434218613702656    # double -3
	.quad	4607182418800017408     # double 1
	.quad	4617315517961601024     # double 5
	.size	Lightpos, 24

	.type	Lightr,@object          # @Lightr
	.globl	Lightr
	.p2align	3
Lightr:
	.quad	4600877379321698714     # double 0.40000000000000002
	.size	Lightr, 8

	.type	Camerapos,@object       # @Camerapos
	.globl	Camerapos
	.p2align	3
Camerapos:
	.quad	4609434218613702656     # double 1.5
	.quad	-4614388178203810202    # double -1.3999999999999999
	.quad	4608083138725491507     # double 1.2
	.size	Camerapos, 24

	.type	Cameraright,@object     # @Cameraright
	.globl	Cameraright
	.p2align	3
Cameraright:
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.size	Cameraright, 24

	.type	Cameradir,@object       # @Cameradir
	.globl	Cameradir
	.p2align	3
Cameradir:
	.quad	-4616189618054758400    # double -1
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.size	Cameradir, 24

	.type	Cameraup,@object        # @Cameraup
	.globl	Cameraup
	.p2align	3
Cameraup:
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	4612523012418134711     # double 2.3717000000000001
	.size	Cameraup, 24

	.type	Ambient,@object         # @Ambient
	.globl	Ambient
	.p2align	3
Ambient:
	.quad	4599075939470750515     # double 0.29999999999999999
	.size	Ambient, 8

	.type	Skycolor,@object        # @Skycolor
	.globl	Skycolor
	.p2align	4
Skycolor:
	.quad	4602678819172646912     # double 0.5
	.quad	4599075939470750515     # double 0.29999999999999999
	.quad	4604480259023595110     # double 0.69999999999999996
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	4596373779694328218     # double 0.20000000000000001
	.size	Skycolor, 48

	.type	rnd,@object             # @rnd
	.globl	rnd
	.p2align	3
rnd:
	.quad	1380328551              # 0x52462467
	.size	rnd, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compile date: %s\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"today"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"r"
	.size	.L.str.4, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d"
	.size	.L.str.6, 3

	.type	DISTRIB,@object         # @DISTRIB
	.comm	DISTRIB,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"P6"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%d %d"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"255"
	.size	.L.str.9, 4

	.type	memory,@object          # @memory
	.comm	memory,921600,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ERROR: Could not open indata file"
	.size	.Lstr, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
