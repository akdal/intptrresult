	.text
	.file	"PpmdZip.bc"
	.globl	_ZN9NCompress8NPpmdZip8CDecoderC2Eb
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoderC2Eb,@function
_ZN9NCompress8NPpmdZip8CDecoderC2Eb:    # @_ZN9NCompress8NPpmdZip8CDecoderC2Eb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$0, 8(%rbx)
	movq	$_ZTVN9NCompress8NPpmdZip8CDecoderE+16, (%rbx)
	leaq	16(%rbx), %r14
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN14CByteInBufWrapC1Ev
.Ltmp1:
# BB#1:
	movq	$0, 80(%rbx)
	movb	%bpl, 7480(%rbx)
	leaq	88(%rbx), %rdi
	movq	%r14, 208(%rbx)
.Ltmp3:
	callq	Ppmd8_Construct
.Ltmp4:
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
.Ltmp5:
	movq	%rax, %r15
	movq	80(%rbx), %rdi
.Ltmp6:
	callq	MidFree
.Ltmp7:
# BB#5:                                 # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit
.Ltmp8:
	movq	%r14, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp9:
# BB#6:                                 # %_ZN14CByteInBufWrapD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_7:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_3:
.Ltmp2:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN9NCompress8NPpmdZip8CDecoderC2Eb, .Lfunc_end0-_ZN9NCompress8NPpmdZip8CDecoderC2Eb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress8NPpmdZip8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoderD2Ev,@function
_ZN9NCompress8NPpmdZip8CDecoderD2Ev:    # @_ZN9NCompress8NPpmdZip8CDecoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN9NCompress8NPpmdZip8CDecoderE+16, (%rbx)
	leaq	88(%rbx), %rdi
.Ltmp11:
	movl	$_ZN9NCompress8NPpmdZipL10g_BigAllocE, %esi
	callq	Ppmd8_Free
.Ltmp12:
# BB#1:
	movq	80(%rbx), %rdi
.Ltmp16:
	callq	MidFree
.Ltmp17:
# BB#2:                                 # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit
	addq	$16, %rbx
.Ltmp22:
	movq	%rbx, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp23:
# BB#3:                                 # %_ZN14CByteInBufWrapD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_5:
.Ltmp24:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_6:
.Ltmp18:
	movq	%rax, %r14
	jmp	.LBB2_7
.LBB2_4:
.Ltmp13:
	movq	%rax, %r14
	movq	80(%rbx), %rdi
.Ltmp14:
	callq	MidFree
.Ltmp15:
.LBB2_7:                                # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit4
	addq	$16, %rbx
.Ltmp19:
	movq	%rbx, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp20:
# BB#8:                                 # %_ZN14CByteInBufWrapD2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_9:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN9NCompress8NPpmdZip8CDecoderD2Ev, .Lfunc_end2-_ZN9NCompress8NPpmdZip8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp23         #   Call between .Ltmp23 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp14         #   Call between .Ltmp14 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end2-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NPpmdZip8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoderD0Ev,@function
_ZN9NCompress8NPpmdZip8CDecoderD0Ev:    # @_ZN9NCompress8NPpmdZip8CDecoderD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp25:
	callq	_ZN9NCompress8NPpmdZip8CDecoderD2Ev
.Ltmp26:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp27:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN9NCompress8NPpmdZip8CDecoderD0Ev, .Lfunc_end3-_ZN9NCompress8NPpmdZip8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 128
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, %rbx
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	cmpq	$0, 80(%r12)
	jne	.LBB4_2
# BB#1:                                 # %_ZN9NCompress8NPpmdZip4CBuf5AllocEv.exit
	movl	$1048576, %edi          # imm = 0x100000
	callq	MidAlloc
	movq	%rax, 80(%r12)
	testq	%rax, %rax
	je	.LBB4_58
.LBB4_2:                                # %_ZN9NCompress8NPpmdZip4CBuf5AllocEv.exit.thread
	leaq	16(%r12), %rbp
	movl	$1048576, %esi          # imm = 0x100000
	movq	%rbp, %rdi
	callq	_ZN14CByteInBufWrap5AllocEj
	movl	$-2147024882, %edx      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB4_59
# BB#3:                                 # %_ZN14CByteInBufWrap8ReadByteEv.exit
	movq	%r14, 56(%r12)
	movq	40(%r12), %rax
	movq	%rax, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 64(%r12)
	movb	$0, 72(%r12)
	movl	$0, 76(%r12)
	movq	%rbp, %rdi
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movq	24(%r12), %rcx
	cmpq	32(%r12), %rcx
	movb	%al, 16(%rsp)
	je	.LBB4_5
# BB#4:
	leaq	1(%rcx), %rax
	movq	%rax, 24(%r12)
	movb	(%rcx), %al
	jmp	.LBB4_6
.LBB4_5:
	movq	%rbp, %rdi
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
.LBB4_6:                                # %_ZN14CByteInBufWrap8ReadByteEv.exit.1
	movb	%al, 17(%rsp)
	movl	$1, %edx
	cmpb	$0, 72(%r12)
	jne	.LBB4_59
# BB#7:
	movzwl	16(%rsp), %esi
	cmpl	$12287, %esi            # imm = 0x2FFF
	ja	.LBB4_59
# BB#8:
	movl	%esi, %ebp
	andl	$15, %ebp
	incl	%ebp
	cmpl	$2, %ebp
	jb	.LBB4_59
# BB#9:
	movl	%esi, %r14d
	shrl	$12, %r14d
	movl	$-2147467263, %edx      # imm = 0x80004001
	cmpl	$2, %r14d
	je	.LBB4_59
# BB#10:
	leaq	88(%r12), %rdi
	shll	$16, %esi
	andl	$267386880, %esi        # imm = 0xFF00000
	addl	$1048576, %esi          # imm = 0x100000
	movl	$_ZN9NCompress8NPpmdZipL10g_BigAllocE, %edx
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	Ppmd8_Alloc
	testl	%eax, %eax
	je	.LBB4_58
# BB#11:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Ppmd8_RangeDec_Init
	testl	%eax, %eax
	movl	$1, %edx
	je	.LBB4_59
# BB#12:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	Ppmd8_Init
	movq	$0, 16(%rsp)
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %EDX
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
.LBB4_13:                               # %.thread126.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_15 Depth 3
                                        #         Child Loop BB4_17 Depth 4
                                        #     Child Loop BB4_31 Depth 2
                                        #       Child Loop BB4_32 Depth 3
	testq	%rbx, %rbx
	je	.LBB4_30
.LBB4_14:                               # %.thread126.outer179
                                        #   Parent Loop BB4_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_15 Depth 3
                                        #         Child Loop BB4_17 Depth 4
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB4_15:                               # %.thread126
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_17 Depth 4
	movq	(%rbx), %r14
	subq	16(%rsp), %r14
	jbe	.LBB4_50
# BB#16:                                # %.critedge
                                        #   in Loop: Header=BB4_15 Depth=3
	cmpq	$1048576, %r14          # imm = 0x100000
	movl	$1048576, %eax          # imm = 0x100000
	cmovaeq	%rax, %r14
	movq	80(%r12), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_17:                               #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Ppmd8_DecodeSymbol
	movl	%eax, %r15d
	movl	%r15d, %ebp
	shrl	$31, %ebp
	testl	%r15d, %r15d
	js	.LBB4_22
# BB#18:                                #   in Loop: Header=BB4_17 Depth=4
	movzbl	72(%r12), %eax
	testb	%al, %al
	jne	.LBB4_22
# BB#19:                                #   in Loop: Header=BB4_17 Depth=4
	movb	%r15b, (%r13,%rbx)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_17
# BB#20:                                #   in Loop: Header=BB4_15 Depth=3
	xorl	%ebp, %ebp
	jmp	.LBB4_23
.LBB4_22:                               #   in Loop: Header=BB4_15 Depth=3
	movq	%rbx, %r14
.LBB4_23:                               #   in Loop: Header=BB4_15 Depth=3
	addq	%r14, 16(%rsp)
	movq	80(%r12), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	$1, %ecx
	testl	%eax, %eax
	movq	64(%rsp), %r13          # 8-byte Reload
	jne	.LBB4_46
# BB#24:                                #   in Loop: Header=BB4_15 Depth=3
	movl	76(%r12), %edx
	testl	%edx, %edx
	jne	.LBB4_47
# BB#25:                                #   in Loop: Header=BB4_15 Depth=3
	cmpb	$0, 72(%r12)
	jne	.LBB4_62
# BB#26:                                #   in Loop: Header=BB4_15 Depth=3
	testb	%bpl, %bpl
	jne	.LBB4_43
# BB#27:                                #   in Loop: Header=BB4_15 Depth=3
	testq	%r13, %r13
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	je	.LBB4_15
# BB#28:                                #   in Loop: Header=BB4_14 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %edx
	movq	24(%r12), %rax
	addq	64(%r12), %rax
	subq	40(%r12), %rax
	movq	%rax, 48(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	leaq	48(%rsp), %rsi
	movl	%edx, %ebp
	leaq	16(%rsp), %rdx
	callq	*40(%rax)
	movl	%ebp, %edx
	testl	%eax, %eax
	je	.LBB4_14
# BB#29:                                # %.loopexit131.loopexit174.loopexit181
                                        #   in Loop: Header=BB4_13 Depth=1
	setne	%cl
	movzbl	%cl, %ecx
	movl	%eax, %edx
	andb	$7, %cl
	je	.LBB4_13
	jmp	.LBB4_49
.LBB4_30:                               # %.thread126.us.outer.preheader
                                        #   in Loop: Header=BB4_13 Depth=1
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB4_31:                               # %.thread126.us
                                        #   Parent Loop BB4_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_32 Depth 3
	movq	80(%r12), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_32:                               #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	Ppmd8_DecodeSymbol
	movl	%eax, %r15d
	movl	%r15d, %ebx
	shrl	$31, %ebx
	testl	%r15d, %r15d
	js	.LBB4_36
# BB#33:                                #   in Loop: Header=BB4_32 Depth=3
	movzbl	72(%r12), %eax
	testb	%al, %al
	jne	.LBB4_36
# BB#34:                                #   in Loop: Header=BB4_32 Depth=3
	movb	%r15b, (%r14,%rbp)
	incq	%rbp
	cmpq	$1048576, %rbp          # imm = 0x100000
	jne	.LBB4_32
# BB#35:                                #   in Loop: Header=BB4_31 Depth=2
	xorl	%ebx, %ebx
	movl	$1048576, %ebp          # imm = 0x100000
.LBB4_36:                               #   in Loop: Header=BB4_31 Depth=2
	addq	%rbp, 16(%rsp)
	movq	80(%r12), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB4_46
# BB#37:                                #   in Loop: Header=BB4_31 Depth=2
	movl	76(%r12), %edx
	testl	%edx, %edx
	jne	.LBB4_48
# BB#38:                                #   in Loop: Header=BB4_31 Depth=2
	cmpb	$0, 72(%r12)
	jne	.LBB4_62
# BB#39:                                #   in Loop: Header=BB4_31 Depth=2
	testb	%bl, %bl
	jne	.LBB4_43
# BB#40:                                #   in Loop: Header=BB4_31 Depth=2
	testq	%r13, %r13
	je	.LBB4_31
# BB#41:                                #   in Loop: Header=BB4_31 Depth=2
	testl	%eax, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	24(%r12), %rax
	addq	64(%r12), %rax
	subq	40(%r12), %rax
	movq	%rax, 48(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	leaq	48(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB4_31
# BB#42:                                # %.loopexit131.loopexit.loopexit173
                                        #   in Loop: Header=BB4_13 Depth=1
	setne	%cl
	movzbl	%cl, %ecx
	jmp	.LBB4_46
.LBB4_43:                               # %.us-lcssa.us.loopexit178
                                        #   in Loop: Header=BB4_13 Depth=1
	testl	%eax, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$-1, %r15d
	sete	%cl
	movb	$1, %dl
	je	.LBB4_45
# BB#44:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB4_13 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
.LBB4_45:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB4_13 Depth=1
	movb	%cl, %al
	leal	1(%rax,%rax,4), %ecx
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	$1, %esi
	cmovnel	%esi, %eax
                                        # kill: %DL<def> %DL<kill> %RDX<def>
	movq	%rdx, 40(%rsp)          # 8-byte Spill
.LBB4_46:                               #   in Loop: Header=BB4_13 Depth=1
	movl	%eax, %edx
	movq	24(%rsp), %rbx          # 8-byte Reload
	andb	$7, %cl
	je	.LBB4_13
	jmp	.LBB4_49
.LBB4_47:                               #   in Loop: Header=BB4_13 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	andb	$7, %cl
	je	.LBB4_13
	jmp	.LBB4_49
.LBB4_48:                               #   in Loop: Header=BB4_13 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	andb	$7, %cl
	je	.LBB4_13
.LBB4_49:                               # %.loopexit131
	cmpb	$6, %cl
	jne	.LBB4_59
.LBB4_50:                               # %.loopexit
	movl	76(%r12), %edx
	testl	%edx, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	jne	.LBB4_59
# BB#51:
	cmpb	$0, 7480(%r12)
	je	.LBB4_57
# BB#52:
	testb	$1, 40(%rsp)            # 1-byte Folded Reload
	jne	.LBB4_56
# BB#53:
	callq	Ppmd8_DecodeSymbol
	movl	76(%r12), %edx
	testl	%edx, %edx
	jne	.LBB4_59
# BB#54:
	cmpl	$-1, %eax
	movl	$1, %edx
	jne	.LBB4_59
# BB#55:
	movb	72(%r12), %al
	testb	%al, %al
	jne	.LBB4_59
.LBB4_56:                               # %.thread127
	movl	$1, %edx
	cmpl	$0, 196(%r12)
	jne	.LBB4_59
.LBB4_57:
	xorl	%edx, %edx
	jmp	.LBB4_59
.LBB4_58:
	movl	$-2147024882, %edx      # imm = 0x8007000E
.LBB4_59:
	movl	%edx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_62:
	movl	$1, %edx
	jmp	.LBB4_59
.Lfunc_end4:
	.size	_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end4-_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress8NPpmdZip8CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoderD2Ev,@function
_ZN9NCompress8NPpmdZip8CEncoderD2Ev:    # @_ZN9NCompress8NPpmdZip8CEncoderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN9NCompress8NPpmdZip8CEncoderE+16, (%rbx)
	leaq	88(%rbx), %rdi
.Ltmp28:
	movl	$_ZN9NCompress8NPpmdZipL10g_BigAllocE, %esi
	callq	Ppmd8_Free
.Ltmp29:
# BB#1:
	movq	80(%rbx), %rdi
.Ltmp33:
	callq	MidFree
.Ltmp34:
# BB#2:                                 # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit
	addq	$16, %rbx
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp40:
# BB#3:                                 # %_ZN15CByteOutBufWrapD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_5:
.Ltmp41:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_6:
.Ltmp35:
	movq	%rax, %r14
	jmp	.LBB5_7
.LBB5_4:
.Ltmp30:
	movq	%rax, %r14
	movq	80(%rbx), %rdi
.Ltmp31:
	callq	MidFree
.Ltmp32:
.LBB5_7:                                # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit4
	addq	$16, %rbx
.Ltmp36:
	movq	%rbx, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp37:
# BB#8:                                 # %_ZN15CByteOutBufWrapD2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_9:
.Ltmp38:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress8NPpmdZip8CEncoderD2Ev, .Lfunc_end5-_ZN9NCompress8NPpmdZip8CEncoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin3   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp31-.Ltmp40         #   Call between .Ltmp40 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp37-.Ltmp31         #   Call between .Ltmp31 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin3   #     jumps to .Ltmp38
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Lfunc_end5-.Ltmp37     #   Call between .Ltmp37 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NPpmdZip8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoderD0Ev,@function
_ZN9NCompress8NPpmdZip8CEncoderD0Ev:    # @_ZN9NCompress8NPpmdZip8CEncoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp42:
	callq	_ZN9NCompress8NPpmdZip8CEncoderD2Ev
.Ltmp43:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp44:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN9NCompress8NPpmdZip8CEncoderD0Ev, .Lfunc_end6-_ZN9NCompress8NPpmdZip8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp42-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin4   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp43     #   Call between .Ltmp43 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB7_13
# BB#1:                                 # %.lr.ph
	addq	$8, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	-8(%rdx), %r9d
	cmpl	$19, %r9d
	jne	.LBB7_14
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	(%rdx), %r9d
	movl	(%rsi,%r8,4), %r10d
	cmpl	$2, %r10d
	je	.LBB7_8
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	$3, %r10d
	je	.LBB7_10
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	$12, %r10d
	jne	.LBB7_14
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	$1, %r9d
	ja	.LBB7_14
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	%r9d, 7488(%rdi)
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	leal	-1048576(%r9), %r10d
	cmpl	$267386880, %r10d       # imm = 0xFF00000
	ja	.LBB7_14
# BB#9:                                 #   in Loop: Header=BB7_2 Depth=1
	shrl	$20, %r9d
	movl	%r9d, 7480(%rdi)
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_2 Depth=1
	leal	-2(%r9), %r10d
	cmpl	$14, %r10d
	ja	.LBB7_14
# BB#11:                                #   in Loop: Header=BB7_2 Depth=1
	movzbl	%r9b, %eax
	movl	%eax, 7484(%rdi)
.LBB7_12:                               #   in Loop: Header=BB7_2 Depth=1
	addq	$16, %rdx
	incq	%r8
	cmpl	%ecx, %r8d
	jb	.LBB7_2
.LBB7_13:
	xorl	%eax, %eax
.LBB7_14:                               # %.thread
	retq
.Lfunc_end7:
	.size	_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end7-_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN9NCompress8NPpmdZip8CEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoderC2Ev,@function
_ZN9NCompress8NPpmdZip8CEncoderC2Ev:    # @_ZN9NCompress8NPpmdZip8CEncoderC2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 8(%rbx)
	movq	$_ZTVN9NCompress8NPpmdZip8CEncoderE+16, (%rbx)
	leaq	16(%rbx), %r14
.Ltmp45:
	movq	%r14, %rdi
	callq	_ZN15CByteOutBufWrapC1Ev
.Ltmp46:
# BB#1:
	movq	$0, 80(%rbx)
	movl	$16, 7480(%rbx)
	movl	$6, 7484(%rbx)
	movl	$0, 7488(%rbx)
	leaq	88(%rbx), %rdi
	movq	%r14, 208(%rbx)
.Ltmp48:
	callq	Ppmd8_Construct
.Ltmp49:
# BB#2:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB8_4:
.Ltmp50:
	movq	%rax, %r15
	movq	80(%rbx), %rdi
.Ltmp51:
	callq	MidFree
.Ltmp52:
# BB#5:                                 # %_ZN9NCompress8NPpmdZip4CBufD2Ev.exit
.Ltmp53:
	movq	%r14, %rdi
	callq	_ZN15CByteOutBufWrap4FreeEv
.Ltmp54:
# BB#6:                                 # %_ZN15CByteOutBufWrapD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB8_7:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_3:
.Ltmp47:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN9NCompress8NPpmdZip8CEncoderC2Ev, .Lfunc_end8-_ZN9NCompress8NPpmdZip8CEncoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp45-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin5   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin5   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp54-.Ltmp51         #   Call between .Ltmp51 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin5   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp54     #   Call between .Ltmp54 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 96
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	cmpq	$0, 80(%r13)
	jne	.LBB9_3
# BB#1:                                 # %_ZN9NCompress8NPpmdZip4CBuf5AllocEv.exit
	movl	$1048576, %edi          # imm = 0x100000
	callq	MidAlloc
	movq	%rax, 80(%r13)
	testq	%rax, %rax
	je	.LBB9_2
.LBB9_3:                                # %_ZN9NCompress8NPpmdZip4CBuf5AllocEv.exit.thread
	leaq	16(%r13), %r15
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r15, %rdi
	callq	_ZN15CByteOutBufWrap5AllocEm
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB9_26
# BB#4:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leaq	88(%r13), %r12
	movl	7480(%r13), %esi
	shll	$20, %esi
	movl	$_ZN9NCompress8NPpmdZipL10g_BigAllocE, %edx
	movq	%r12, %rdi
	callq	Ppmd8_Alloc
	testl	%eax, %eax
	je	.LBB9_26
# BB#5:
	movq	%r14, 56(%r13)
	movq	40(%r13), %rax
	movq	%rax, 24(%r13)
	addq	48(%r13), %rax
	movq	%rax, 32(%r13)
	movq	$0, 64(%r13)
	movl	$0, 72(%r13)
	movl	$0, 200(%r13)
	movl	$-1, 192(%r13)
	movl	7484(%r13), %esi
	movl	7488(%r13), %edx
	movq	%r12, %rdi
	callq	Ppmd8_Init
	movl	7480(%r13), %eax
	shll	$4, %eax
	movl	7488(%r13), %ecx
	shll	$12, %ecx
	addl	7484(%r13), %eax
	leal	-17(%rcx,%rax), %ebx
	movq	24(%r13), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%r13)
	movb	%bl, (%rax)
	movq	24(%r13), %rax
	cmpq	32(%r13), %rax
	jne	.LBB9_7
# BB#6:
	movq	%r15, %rdi
	callq	_ZN15CByteOutBufWrap5FlushEv
	movq	24(%r13), %rax
.LBB9_7:                                # %_ZN15CByteOutBufWrap9WriteByteEh.exit
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%r13)
	movb	%bh, (%rax)  # NOREX
	movq	24(%r13), %rax
	cmpq	32(%r13), %rax
	jne	.LBB9_9
# BB#8:
	movq	%r15, %rdi
	callq	_ZN15CByteOutBufWrap5FlushEv
.LBB9_9:                                # %_ZN15CByteOutBufWrap9WriteByteEh.exit53
	movl	72(%r13), %ebp
	testl	%ebp, %ebp
	jne	.LBB9_26
# BB#10:
	movq	$0, 16(%rsp)
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB9_15
# BB#11:
	leaq	4(%rsp), %r14
.LBB9_12:                               # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_23 Depth 2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movq	80(%r13), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB9_26
# BB#13:                                #   in Loop: Header=BB9_12 Depth=1
	cmpl	$0, 4(%rsp)
	je	.LBB9_21
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_12 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_23:                               # %.lr.ph
                                        #   Parent Loop BB9_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	80(%r13), %rax
	movl	%ebx, %ecx
	movzbl	(%rax,%rcx), %esi
	movq	%r12, %rdi
	callq	Ppmd8_EncodeSymbol
	movl	72(%r13), %ebp
	testl	%ebp, %ebp
	jne	.LBB9_26
# BB#22:                                #   in Loop: Header=BB9_23 Depth=2
	incl	%ebx
	movl	4(%rsp), %eax
	cmpl	%eax, %ebx
	jb	.LBB9_23
# BB#24:                                # %._crit_edge
                                        #   in Loop: Header=BB9_12 Depth=1
	addq	%rax, 16(%rsp)
	movq	24(%r13), %rax
	addq	64(%r13), %rax
	subq	40(%r13), %rax
	movq	%rax, 32(%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	leaq	32(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB9_12
	jmp	.LBB9_26
.LBB9_2:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
.LBB9_26:
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_15:                               # %.split.us.preheader
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movq	80(%r13), %rsi
	leaq	4(%rsp), %rcx
	movl	$1048576, %edx          # imm = 0x100000
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB9_26
# BB#16:                                # %.lr.ph105.preheader
	leaq	4(%rsp), %r14
.LBB9_17:                               # %.lr.ph105
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_20 Depth 2
	cmpl	$0, 4(%rsp)
	je	.LBB9_21
# BB#18:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB9_17 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_20:                               # %.lr.ph.us
                                        #   Parent Loop BB9_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	80(%r13), %rax
	movl	%ebx, %ecx
	movzbl	(%rax,%rcx), %esi
	movq	%r12, %rdi
	callq	Ppmd8_EncodeSymbol
	movl	72(%r13), %ebp
	testl	%ebp, %ebp
	jne	.LBB9_26
# BB#19:                                #   in Loop: Header=BB9_20 Depth=2
	incl	%ebx
	movl	4(%rsp), %eax
	cmpl	%eax, %ebx
	jb	.LBB9_20
# BB#25:                                # %.split.us.backedge
                                        #   in Loop: Header=BB9_17 Depth=1
	addq	%rax, 16(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movq	80(%r13), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB9_17
	jmp	.LBB9_26
.LBB9_21:                               # %.thread57
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	Ppmd8_EncodeSymbol
	movq	%r12, %rdi
	callq	Ppmd8_RangeEnc_FlushData
	movq	%r15, %rdi
	callq	_ZN15CByteOutBufWrap5FlushEv
	movl	%eax, %ebp
	jmp	.LBB9_26
.Lfunc_end9:
	.size	_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end9-_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB10_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB10_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB10_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB10_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB10_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB10_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB10_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB10_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB10_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB10_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB10_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB10_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB10_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB10_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB10_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB10_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB10_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end10-_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv,@function
_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv: # @_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv, .Lfunc_end11-_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv,@function
_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv: # @_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB12_2:
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv, .Lfunc_end12-_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB13_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB13_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB13_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB13_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB13_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB13_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB13_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB13_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB13_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB13_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB13_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB13_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB13_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB13_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB13_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv,@function
_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv: # @_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv, .Lfunc_end14-_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv,@function
_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv: # @_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv, .Lfunc_end15-_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm,@function
_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm: # @_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end16:
	.size	_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm, .Lfunc_end16-_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_,@function
_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_: # @_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end17:
	.size	_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_, .Lfunc_end17-_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress8NPpmdZip8CDecoderE,@object # @_ZTVN9NCompress8NPpmdZip8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NPpmdZip8CDecoderE
	.p2align	3
_ZTVN9NCompress8NPpmdZip8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NPpmdZip8CDecoderE
	.quad	_ZN9NCompress8NPpmdZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NPpmdZip8CDecoder6AddRefEv
	.quad	_ZN9NCompress8NPpmdZip8CDecoder7ReleaseEv
	.quad	_ZN9NCompress8NPpmdZip8CDecoderD2Ev
	.quad	_ZN9NCompress8NPpmdZip8CDecoderD0Ev
	.quad	_ZN9NCompress8NPpmdZip8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.size	_ZTVN9NCompress8NPpmdZip8CDecoderE, 64

	.type	_ZN9NCompress8NPpmdZipL10g_BigAllocE,@object # @_ZN9NCompress8NPpmdZipL10g_BigAllocE
	.data
	.p2align	3
_ZN9NCompress8NPpmdZipL10g_BigAllocE:
	.quad	_ZN9NCompress8NPpmdZipL10SzBigAllocEPvm
	.quad	_ZN9NCompress8NPpmdZipL9SzBigFreeEPvS1_
	.size	_ZN9NCompress8NPpmdZipL10g_BigAllocE, 16

	.type	_ZTVN9NCompress8NPpmdZip8CEncoderE,@object # @_ZTVN9NCompress8NPpmdZip8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NPpmdZip8CEncoderE
	.p2align	3
_ZTVN9NCompress8NPpmdZip8CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NPpmdZip8CEncoderE
	.quad	_ZN9NCompress8NPpmdZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NPpmdZip8CEncoder6AddRefEv
	.quad	_ZN9NCompress8NPpmdZip8CEncoder7ReleaseEv
	.quad	_ZN9NCompress8NPpmdZip8CEncoderD2Ev
	.quad	_ZN9NCompress8NPpmdZip8CEncoderD0Ev
	.quad	_ZN9NCompress8NPpmdZip8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.size	_ZTVN9NCompress8NPpmdZip8CEncoderE, 64

	.type	_ZTSN9NCompress8NPpmdZip8CDecoderE,@object # @_ZTSN9NCompress8NPpmdZip8CDecoderE
	.globl	_ZTSN9NCompress8NPpmdZip8CDecoderE
	.p2align	4
_ZTSN9NCompress8NPpmdZip8CDecoderE:
	.asciz	"N9NCompress8NPpmdZip8CDecoderE"
	.size	_ZTSN9NCompress8NPpmdZip8CDecoderE, 31

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress8NPpmdZip8CDecoderE,@object # @_ZTIN9NCompress8NPpmdZip8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress8NPpmdZip8CDecoderE
	.p2align	4
_ZTIN9NCompress8NPpmdZip8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NPpmdZip8CDecoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress8NPpmdZip8CDecoderE, 56

	.type	_ZTSN9NCompress8NPpmdZip8CEncoderE,@object # @_ZTSN9NCompress8NPpmdZip8CEncoderE
	.globl	_ZTSN9NCompress8NPpmdZip8CEncoderE
	.p2align	4
_ZTSN9NCompress8NPpmdZip8CEncoderE:
	.asciz	"N9NCompress8NPpmdZip8CEncoderE"
	.size	_ZTSN9NCompress8NPpmdZip8CEncoderE, 31

	.type	_ZTIN9NCompress8NPpmdZip8CEncoderE,@object # @_ZTIN9NCompress8NPpmdZip8CEncoderE
	.globl	_ZTIN9NCompress8NPpmdZip8CEncoderE
	.p2align	4
_ZTIN9NCompress8NPpmdZip8CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NPpmdZip8CEncoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress8NPpmdZip8CEncoderE, 56


	.globl	_ZN9NCompress8NPpmdZip8CDecoderC1Eb
	.type	_ZN9NCompress8NPpmdZip8CDecoderC1Eb,@function
_ZN9NCompress8NPpmdZip8CDecoderC1Eb = _ZN9NCompress8NPpmdZip8CDecoderC2Eb
	.globl	_ZN9NCompress8NPpmdZip8CDecoderD1Ev
	.type	_ZN9NCompress8NPpmdZip8CDecoderD1Ev,@function
_ZN9NCompress8NPpmdZip8CDecoderD1Ev = _ZN9NCompress8NPpmdZip8CDecoderD2Ev
	.globl	_ZN9NCompress8NPpmdZip8CEncoderD1Ev
	.type	_ZN9NCompress8NPpmdZip8CEncoderD1Ev,@function
_ZN9NCompress8NPpmdZip8CEncoderD1Ev = _ZN9NCompress8NPpmdZip8CEncoderD2Ev
	.globl	_ZN9NCompress8NPpmdZip8CEncoderC1Ev
	.type	_ZN9NCompress8NPpmdZip8CEncoderC1Ev,@function
_ZN9NCompress8NPpmdZip8CEncoderC1Ev = _ZN9NCompress8NPpmdZip8CEncoderC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
