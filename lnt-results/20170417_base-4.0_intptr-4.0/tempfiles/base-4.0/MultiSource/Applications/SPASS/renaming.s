	.text
	.file	"renaming.bc"
	.globl	ren_Init
	.p2align	4, 0x90
	.type	ren_Init,@function
ren_Init:                               # @ren_Init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	term_GetStampID
	movl	%eax, ren_STAMPID(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	ren_Init, .Lfunc_end0-ren_Init
	.cfi_endproc

	.globl	ren_Rename
	.p2align	4, 0x90
	.type	ren_Rename,@function
ren_Rename:                             # @ren_Rename
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 160
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	ren_STAMPID(%rip), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB1_2
# BB#1:
	movq	%r15, %rdi
	callq	ren_ResetTermStamp
.LBB1_2:
	incl	term_STAMP(%rip)
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r15, %rsi
	callq	ren_GetRenamings
	movq	%rax, %r13
	testl	%ebx, %ebx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	je	.LBB1_36
# BB#3:
	testq	%r13, %r13
	je	.LBB1_40
# BB#4:                                 # %.lr.ph95.i.preheader
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph95.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
                                        #     Child Loop BB1_10 Depth 2
                                        #     Child Loop BB1_13 Depth 2
                                        #     Child Loop BB1_17 Depth 2
                                        #       Child Loop BB1_18 Depth 3
                                        #     Child Loop BB1_23 Depth 2
                                        #     Child Loop BB1_27 Depth 2
	movq	8(%rcx), %rbx
	testq	%r13, %r13
	je	.LBB1_9
# BB#6:                                 # %.lr.ph.i63.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.i63.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 8(%rax)
	je	.LBB1_32
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_7
.LBB1_9:                                # %.loopexit84.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	fol_Generalizations
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%r14, (%rbp)
	movq	%rbp, %rdi
	callq	fol_MostGeneralFormula
	movq	%rax, %r12
	testq	%rbp, %rbp
	je	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph.i71.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB1_10
.LBB1_11:                               # %list_Delete.exit72.i
                                        #   in Loop: Header=BB1_5 Depth=1
	testq	%r13, %r13
	movl	$0, %r14d
	je	.LBB1_15
# BB#12:                                # %.lr.ph.i76.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.i76.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, 8(%rax)
	je	.LBB1_31
# BB#14:                                #   in Loop: Header=BB1_13 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_13
.LBB1_15:                               # %.loopexit82.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, %rdi
	movq	%rax, %r15
	movq	%r12, 8(%r15)
	movq	%r13, (%r15)
	movq	%r12, %rsi
	callq	fol_Instances
	movq	%rax, %r13
	movq	%r12, %rdi
	callq	ren_Polarity
	movl	%eax, %ebp
	testq	%r13, %r13
	je	.LBB1_24
# BB#16:                                # %.lr.ph.i46
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rbx
	testq	%r15, %r15
	je	.LBB1_23
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph.split.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_18 Depth 3
	movq	8(%rbx), %rdi
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.i58.i
                                        #   Parent Loop BB1_5 Depth=1
                                        #     Parent Loop BB1_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdi, 8(%rax)
	je	.LBB1_21
# BB#19:                                #   in Loop: Header=BB1_18 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_18
# BB#20:                                # %.loopexit.i47
                                        #   in Loop: Header=BB1_17 Depth=2
	callq	ren_Polarity
	cmpl	%ebp, %eax
	cmovnel	%r14d, %ebp
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_21:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB1_17 Depth=2
	movq	$0, 8(%rbx)
.LBB1_22:                               #   in Loop: Header=BB1_17 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_17
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	ren_Polarity
	cmpl	%ebp, %eax
	cmovnel	%r14d, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_23
.LBB1_24:                               # %._crit_edge.i48
                                        #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	list_Copy
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_29
# BB#25:                                #   in Loop: Header=BB1_5 Depth=1
	testq	%r15, %r15
	je	.LBB1_30
# BB#26:                                # %.preheader.i.i52.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB1_27:                               # %.preheader.i.i52
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_27
# BB#28:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB1_30
.LBB1_29:                               #   in Loop: Header=BB1_5 Depth=1
	movq	%r15, %r13
.LBB1_30:                               # %list_Nconc.exit.i54
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	$24, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, (%rbx)
	movq	%r14, 8(%rbx)
	movl	%ebp, 20(%rbx)
	movl	$0, 16(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB1_31:                               # %list_PointerMember.exit67.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB1_32:                               # %list_PointerMember.exit67.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_5
# BB#33:                                # %._crit_edge96.i
	testq	%r13, %r13
	je	.LBB1_35
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph.i.i58
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_34
.LBB1_35:
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB1_41
.LBB1_36:                               # %.preheader
	testq	%r13, %r13
	je	.LBB1_40
# BB#37:                                # %.lr.ph69.preheader
	xorl	%r12d, %r12d
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movq	%r15, %rdi
	callq	ren_Polarity
	movl	%eax, %r14d
	movl	$24, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, (%rbp)
	movq	$0, 8(%rbp)
	movl	%r14d, 20(%rbp)
	movl	$0, 16(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB1_38
	jmp	.LBB1_41
.LBB1_40:
	xorl	%eax, %eax
.LBB1_41:                               # %ren_FurtherMatches.exit
	movq	%rax, %rdi
	callq	ren_FreeRenaming
	movl	$ren_RootDistanceSmaller, %esi
	movq	%rax, %rdi
	callq	list_Sort
	movq	%rax, %rdi
	callq	ren_SolveDependencies
	movq	%rax, %rdi
	callq	ren_FreeRenaming
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	setne	%al
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	setne	%r14b
	andb	%al, %r14b
	cmpb	$1, %r14b
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB1_45
# BB#42:
	movl	$.L.str, %edi
	callq	puts
	movq	%rbp, %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.1, %edi
	callq	puts
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_97
# BB#43:                                # %.lr.ph.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_44:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	ren_PrettyPrint
	movq	(%rbx), %rbx
	movl	$.L.str.1, %edi
	callq	puts
	testq	%rbx, %rbx
	jne	.LBB1_44
.LBB1_45:                               # %.loopexit
	movb	%r14b, 7(%rsp)          # 1-byte Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB1_96
# BB#46:                                # %.lr.ph141.i
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	fol_AND(%rip), %r14d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_47:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_62 Depth 2
                                        #     Child Loop BB1_69 Depth 2
                                        #     Child Loop BB1_72 Depth 2
                                        #       Child Loop BB1_75 Depth 3
                                        #       Child Loop BB1_80 Depth 3
                                        #     Child Loop BB1_86 Depth 2
                                        #     Child Loop BB1_93 Depth 2
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rax
	movq	8(%rax), %r15
	movq	(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	(%r15), %r12
	movq	8(%r12), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	list_Length
	movl	%eax, %edi
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	symbol_CreateSkolemPredicate
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movslq	%eax, %r14
	movq	88(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%r15, %r14
	movq	%rax, (%rbx)
	cmpl	$0, 16(%r14)
	je	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_47 Depth=1
	movq	%r12, %rdi
	callq	term_Copy
	movq	%rax, %r12
.LBB1_49:                               #   in Loop: Header=BB1_47 Depth=1
	movl	$term_Copy, %esi
	movq	%r13, %rdi
	callq	list_CopyWithElement
	movl	40(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movl	20(%r14), %ecx
	cmpl	$-1, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB1_53
# BB#50:                                #   in Loop: Header=BB1_47 Depth=1
	cmpl	$1, %ecx
	je	.LBB1_54
# BB#51:                                #   in Loop: Header=BB1_47 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_57
# BB#52:                                #   in Loop: Header=BB1_47 Depth=1
	movl	fol_EQUIV(%rip), %ebp
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_47 Depth=1
	movl	fol_IMPLIES(%rip), %ebp
	movq	%rax, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	%r12, %rbx
	jmp	.LBB1_56
	.p2align	4, 0x90
.LBB1_54:                               #   in Loop: Header=BB1_47 Depth=1
	movl	fol_IMPLIES(%rip), %ebp
	movq	%rax, %rdi
.LBB1_55:                               # %.sink.split.i
                                        #   in Loop: Header=BB1_47 Depth=1
	callq	term_Copy
	movq	%rax, %rbx
	movq	%r12, %r14
.LBB1_56:                               # %.sink.split.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r14, 8(%r15)
	movq	$0, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB1_57:                               #   in Loop: Header=BB1_47 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rbp, 8(%rax)
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rbp, 8(%rax)
	testq	%r13, %r13
	je	.LBB1_59
# BB#58:                                #   in Loop: Header=BB1_47 Depth=1
	movl	fol_ALL(%rip), %r14d
	movl	$term_Copy, %esi
	movq	%r13, %rdi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbp
.LBB1_59:                               #   in Loop: Header=BB1_47 Depth=1
	movq	16(%r15), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB1_64
# BB#60:                                #   in Loop: Header=BB1_47 Depth=1
	testq	%rax, %rax
	movq	64(%rsp), %rdi          # 8-byte Reload
	je	.LBB1_65
# BB#61:                                # %.preheader.i120.i.preheader
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB1_62:                               # %.preheader.i120.i
                                        #   Parent Loop BB1_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_62
# BB#63:                                #   in Loop: Header=BB1_47 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_64:                               #   in Loop: Header=BB1_47 Depth=1
	movq	%rax, %rbx
	movq	64(%rsp), %rdi          # 8-byte Reload
.LBB1_65:                               # %list_Nconc.exit122.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	%rbx, 16(%r15)
	cmpl	$0, 16(%r14)
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_47 Depth=1
	callq	term_Delete
	jmp	.LBB1_71
	.p2align	4, 0x90
.LBB1_67:                               #   in Loop: Header=BB1_47 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rdi)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_71
	.p2align	4, 0x90
.LBB1_69:                               # %.lr.ph.i45
                                        #   Parent Loop BB1_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, 8(%rax)
	je	.LBB1_70
# BB#68:                                #   in Loop: Header=BB1_69 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_69
	jmp	.LBB1_71
.LBB1_70:                               #   in Loop: Header=BB1_47 Depth=1
	movq	%rdi, 8(%rax)
	.p2align	4, 0x90
.LBB1_71:                               # %.loopexit127.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.LBB1_89
	.p2align	4, 0x90
.LBB1_72:                               # %.lr.ph136.i
                                        #   Parent Loop BB1_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_75 Depth 3
                                        #       Child Loop BB1_80 Depth 3
	movq	8(%r15), %rbx
	movq	8(%rbx), %rbp
	movl	$term_Copy, %esi
	movq	%r13, %rdi
	callq	list_CopyWithElement
	movl	40(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	unify_MatchFlexible
	testl	%eax, %eax
	je	.LBB1_101
# BB#73:                                #   in Loop: Header=BB1_72 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	cont_ApplyBindingsModuloMatching
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB1_76
# BB#74:                                # %.lr.ph.i116.i.preheader
                                        #   in Loop: Header=BB1_72 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB1_75:                               # %.lr.ph.i116.i
                                        #   Parent Loop BB1_47 Depth=1
                                        #     Parent Loop BB1_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB1_75
.LBB1_76:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_72 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_78
# BB#77:                                #   in Loop: Header=BB1_72 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB1_78:                               # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB1_72 Depth=2
	movq	%rbp, 8(%r14)
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB1_82
	.p2align	4, 0x90
.LBB1_80:                               # %.lr.ph133.i
                                        #   Parent Loop BB1_47 Depth=1
                                        #     Parent Loop BB1_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB1_81
# BB#79:                                #   in Loop: Header=BB1_80 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_80
	jmp	.LBB1_82
	.p2align	4, 0x90
.LBB1_81:                               #   in Loop: Header=BB1_72 Depth=2
	movq	%r14, 8(%rax)
.LBB1_82:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_72 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB1_72
# BB#83:                                # %._crit_edge.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	8(%r14), %rax
	testq	%rax, %rax
	movq	72(%rsp), %rsi          # 8-byte Reload
	je	.LBB1_90
# BB#84:                                #   in Loop: Header=BB1_47 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB1_88
# BB#85:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB1_86:                               # %.preheader.i.i
                                        #   Parent Loop BB1_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_86
# BB#87:                                #   in Loop: Header=BB1_47 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx)
.LBB1_88:                               # %list_Nconc.exit.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_92
	.p2align	4, 0x90
.LBB1_89:                               #   in Loop: Header=BB1_47 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB1_91
	.p2align	4, 0x90
.LBB1_90:                               #   in Loop: Header=BB1_47 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB1_91:                               # %list_Nconc.exit.i
                                        #   in Loop: Header=BB1_47 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB1_92:                               # %list_Nconc.exit.i
                                        #   in Loop: Header=BB1_47 Depth=1
	testq	%r13, %r13
	movq	$0, 8(%r14)
	je	.LBB1_94
	.p2align	4, 0x90
.LBB1_93:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_93
.LBB1_94:                               # %list_Delete.exit.backedge.i
                                        #   in Loop: Header=BB1_47 Depth=1
	testq	%rsi, %rsi
	jne	.LBB1_47
# BB#95:                                # %list_Delete.exit._crit_edge.i
	movl	$term_Delete, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	list_DeleteWithElement
	movq	48(%rsp), %r13          # 8-byte Reload
.LBB1_96:                               # %ren_FormulaRename.exit
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB1_98
.LBB1_97:                               # %ren_FormulaRename.exit.thread
	movl	$.L.str.2, %edi
	callq	puts
	movq	%rbp, %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.1, %edi
	callq	puts
.LBB1_98:
	movl	$ren_Delete, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	list_DeleteWithElement
	testq	%r13, %r13
	je	.LBB1_100
	.p2align	4, 0x90
.LBB1_99:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_99
.LBB1_100:                              # %list_Delete.exit
	movq	%rbp, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_101:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$1298, %ecx             # imm = 0x512
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end1:
	.size	ren_Rename, .Lfunc_end1-ren_Rename
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_ResetTermStamp,@function
ren_ResetTermStamp:                     # @ren_ResetTermStamp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movl	symbol_TYPEMASK(%rip), %eax
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_1 Depth=1
	movq	(%rbx), %rcx
	movq	8(%rcx), %rdi
.LBB2_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 24(%rdi)
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jns	.LBB2_3
# BB#2:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	%ecx, %edx
	negl	%edx
	andl	%eax, %edx
	cmpl	$2, %edx
	je	.LBB2_8
.LBB2_3:                                # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB2_1 Depth=1
	cmpl	%ecx, fol_ALL(%rip)
	movq	16(%rdi), %rbx
	je	.LBB2_5
# BB#4:                                 # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB2_1 Depth=1
	cmpl	%ecx, fol_EXIST(%rip)
	je	.LBB2_5
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	8(%rbx), %rdi
	callq	ren_ResetTermStamp
	movq	(%rbx), %rbx
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jne	.LBB2_6
.LBB2_8:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	ren_ResetTermStamp, .Lfunc_end2-ren_ResetTermStamp
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_GetRenamings,@function
ren_GetRenamings:                       # @ren_GetRenamings
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph117
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rbx
	negl	%ebp
	movl	(%rbx), %r12d
	cmpl	%eax, %r12d
	je	.LBB3_1
	jmp	.LBB3_3
.LBB3_2:
	movl	%eax, %r12d
.LBB3_3:                                # %._crit_edge
	testl	%r12d, %r12d
	jns	.LBB3_6
# BB#4:                                 # %term_IsAtom.exit
	movl	%r12d, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB3_6
# BB#5:
	xorl	%r15d, %r15d
	jmp	.LBB3_57
.LBB3_6:                                # %term_IsAtom.exit.thread
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_24
# BB#7:
	movl	(%rax), %eax
	xorl	%r15d, %r15d
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB3_33
# BB#8:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB3_33
# BB#9:
	cmpl	$-1, %ebp
	je	.LBB3_25
# BB#10:
	cmpl	$1, %ebp
	je	.LBB3_27
# BB#11:
	testl	%ebp, %ebp
	jne	.LBB3_76
# BB#12:
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	movl	%eax, %r13d
	movq	%rbx, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %r15d
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_AFactorOk
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_BFactorOk
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	je	.LBB3_16
# BB#13:
	testl	%r13d, %r13d
	je	.LBB3_16
# BB#14:
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB3_16
# BB#15:
	testl	%eax, %eax
	jne	.LBB3_29
.LBB3_16:
	testl	%r13d, %r13d
	je	.LBB3_20
# BB#17:
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB3_20
# BB#18:
	movq	%rbx, %rdi
	movl	%eax, %r15d
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	jne	.LBB3_29
# BB#19:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_AExtraFactorOk
	testl	%eax, %eax
	movl	%r15d, %eax
	jne	.LBB3_29
.LBB3_20:
	xorl	%r15d, %r15d
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB3_33
# BB#21:
	testl	%eax, %eax
	je	.LBB3_33
# BB#22:
	movq	%rbx, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	jne	.LBB3_29
# BB#23:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_BExtraFactorOk
	testl	%eax, %eax
	jne	.LBB3_29
	jmp	.LBB3_24
.LBB3_25:
	movq	%rbx, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB3_24
# BB#26:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_BFactorOk
	testl	%eax, %eax
	jne	.LBB3_29
	jmp	.LBB3_24
.LBB3_27:
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB3_32
# BB#28:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	je	.LBB3_24
.LBB3_29:                               # %ren_HasBenefit.exit.thread
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	term_STAMP(%rip), %eax
	movl	%eax, 24(%rbx)
	movq	%rbx, %r14
	cmpl	%r12d, fol_ALL(%rip)
	jne	.LBB3_34
	jmp	.LBB3_43
.LBB3_24:
	xorl	%r15d, %r15d
	cmpl	%r12d, fol_ALL(%rip)
	jne	.LBB3_34
	jmp	.LBB3_43
.LBB3_32:
	xorl	%r15d, %r15d
.LBB3_33:                               # %ren_HasBenefit.exit.thread98
	cmpl	%r12d, fol_ALL(%rip)
	je	.LBB3_43
.LBB3_34:                               # %ren_HasBenefit.exit.thread98
	cmpl	%r12d, fol_EXIST(%rip)
	je	.LBB3_43
# BB#35:
	cmpl	fol_AND(%rip), %r12d
	je	.LBB3_48
# BB#36:
	cmpl	fol_OR(%rip), %r12d
	je	.LBB3_48
# BB#37:
	cmpl	fol_IMPLIES(%rip), %r12d
	jne	.LBB3_58
# BB#38:
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movl	%ebp, %edx
	negl	%edx
	movq	%r14, %rdi
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_64
# BB#39:
	testq	%rax, %rax
	je	.LBB3_65
# BB#40:                                # %.preheader.i74.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_41:                               # %.preheader.i74
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_41
# BB#42:
	movq	%rax, (%rcx)
	jmp	.LBB3_65
.LBB3_43:
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_56
# BB#44:
	testq	%rax, %rax
	je	.LBB3_57
# BB#45:                                # %.preheader.i92.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_46:                               # %.preheader.i92
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_46
.LBB3_47:
	movq	%rax, (%rcx)
	jmp	.LBB3_57
.LBB3_48:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_57
	.p2align	4, 0x90
.LBB3_50:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_53 Depth 2
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_55
# BB#51:                                #   in Loop: Header=BB3_50 Depth=1
	testq	%rax, %rax
	je	.LBB3_49
# BB#52:                                # %.preheader.i80.preheader
                                        #   in Loop: Header=BB3_50 Depth=1
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_53:                               # %.preheader.i80
                                        #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_53
# BB#54:                                #   in Loop: Header=BB3_50 Depth=1
	movq	%rax, (%rcx)
.LBB3_49:                               # %list_Nconc.exit82
                                        #   in Loop: Header=BB3_50 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_50
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_55:                               #   in Loop: Header=BB3_50 Depth=1
	movq	%rax, %r15
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_50
	jmp	.LBB3_57
.LBB3_58:
	cmpl	fol_EQUIV(%rip), %r12d
	jne	.LBB3_77
# BB#59:
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_70
# BB#60:
	testq	%rax, %rax
	je	.LBB3_71
# BB#61:                                # %.preheader.i68.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_62:                               # %.preheader.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_62
# BB#63:
	movq	%rax, (%rcx)
	jmp	.LBB3_71
.LBB3_64:
	movq	%rax, %r15
.LBB3_65:                               # %list_Nconc.exit76
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_56
# BB#66:
	testq	%rax, %rax
	je	.LBB3_57
# BB#67:                                # %.preheader.i.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_68:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_68
	jmp	.LBB3_47
.LBB3_70:
	movq	%rax, %r15
.LBB3_71:                               # %list_Nconc.exit70
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	ren_GetRenamings
	testq	%r15, %r15
	je	.LBB3_56
# BB#72:
	testq	%rax, %rax
	je	.LBB3_57
# BB#73:                                # %.preheader.i86.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB3_74:                               # %.preheader.i86
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_74
	jmp	.LBB3_47
.LBB3_56:
	movq	%rax, %r15
.LBB3_57:                               # %list_Nconc.exit94
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_76:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$966, %ecx              # imm = 0x3C6
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.12, %edi
	jmp	.LBB3_78
.LBB3_77:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$1070, %ecx             # imm = 0x42E
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.10, %edi
.LBB3_78:
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end3:
	.size	ren_GetRenamings, .Lfunc_end3-ren_GetRenamings
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_Polarity,@function
ren_Polarity:                           # @ren_Polarity
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	8(%rdi), %rsi
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.LBB4_12
# BB#1:                                 # %.lr.ph
	movl	fol_AND(%rip), %ecx
	movl	fol_OR(%rip), %edx
	movl	fol_ALL(%rip), %r9d
	movl	fol_EXIST(%rip), %r8d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	(%rdi), %esi
	cmpl	%ecx, %esi
	je	.LBB4_6
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	%edx, %esi
	je	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	%esi, %r9d
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	%esi, %r8d
	jne	.LBB4_7
.LBB4_6:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.LBB4_2
	jmp	.LBB4_12
.LBB4_7:
	cmpl	fol_NOT(%rip), %esi
	jne	.LBB4_9
# BB#8:
	callq	ren_Polarity
	negl	%eax
	jmp	.LBB4_12
.LBB4_9:
	xorl	%eax, %eax
	cmpl	fol_EQUIV(%rip), %esi
	je	.LBB4_12
# BB#10:
	cmpl	fol_IMPLIES(%rip), %esi
	jne	.LBB4_13
# BB#11:
	movq	16(%rdi), %rax
	movq	8(%rax), %r14
	callq	ren_Polarity
	movl	%eax, %ecx
	negl	%ecx
	cmpq	%rbx, %r14
	cmovnel	%eax, %ecx
	movl	%ecx, %eax
.LBB4_12:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_13:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$1104, %ecx             # imm = 0x450
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end4:
	.size	ren_Polarity, .Lfunc_end4-ren_Polarity
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_FreeRenaming,@function
ren_FreeRenaming:                       # @ren_FreeRenaming
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 96
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB5_60
# BB#1:                                 # %.lr.ph.preheader
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
                                        #     Child Loop BB5_45 Depth 2
                                        #     Child Loop BB5_39 Depth 2
                                        #     Child Loop BB5_41 Depth 2
                                        #     Child Loop BB5_57 Depth 2
	movq	8(%r13), %r14
	cmpq	$0, 8(%r14)
	jne	.LBB5_59
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	(%r14), %r15
	movl	term_STAMP(%rip), %eax
	movq	8(%r15), %rcx
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	cmpl	24(%rbx), %eax
	je	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_4
.LBB5_6:                                # %.critedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	$0, 24(%r15)
	cmpl	$0, 16(%r14)
	je	.LBB5_7
.LBB5_56:                               # %ren_HasNonZeroBenefit.exit.thread36
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB5_58
	.p2align	4, 0x90
.LBB5_57:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB5_57
.LBB5_58:                               # %ren_Delete.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	memory_ARRAY+192(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+192(%rip), %rax
	movq	%r14, (%rax)
	movq	$0, 8(%r13)
	jmp	.LBB5_59
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	callq	ren_Polarity
	movl	20(%r14), %ecx
	cmpl	$-1, %ecx
	je	.LBB5_52
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpl	$1, %ecx
	je	.LBB5_47
# BB#9:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	%eax, 28(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	jne	.LBB5_63
# BB#10:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_PFactorOk
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AFactorOk
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_BFactorOk
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	testl	%r12d, %r12d
	je	.LBB5_11
# BB#12:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	setne	%r12b
	jmp	.LBB5_13
.LBB5_52:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_BFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB5_61
# BB#53:                                #   in Loop: Header=BB5_2 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_23
# BB#54:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	jne	.LBB5_23
	jmp	.LBB5_56
.LBB5_47:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB5_50
# BB#48:                                #   in Loop: Header=BB5_2 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_23
# BB#49:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	jne	.LBB5_23
	jmp	.LBB5_56
.LBB5_11:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%r12d, %r12d
.LBB5_13:                               #   in Loop: Header=BB5_2 Depth=1
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	je	.LBB5_14
# BB#15:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	setne	%al
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB5_18
	jmp	.LBB5_17
.LBB5_61:                               #   in Loop: Header=BB5_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB5_56
# BB#62:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_BExtraFactorOk
	testl	%eax, %eax
	jne	.LBB5_23
	jmp	.LBB5_56
.LBB5_50:                               #   in Loop: Header=BB5_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB5_56
# BB#51:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AExtraFactorOk
	testl	%eax, %eax
	jne	.LBB5_23
	jmp	.LBB5_56
.LBB5_14:                               #   in Loop: Header=BB5_2 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB5_17
.LBB5_18:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AExtraFactorOk
	testl	%eax, %eax
	setne	%bpl
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB5_24
	jmp	.LBB5_20
.LBB5_17:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%ebp, %ebp
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB5_20
.LBB5_24:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_BExtraFactorOk
	testl	%eax, %eax
	setne	%cl
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB5_21
# BB#25:                                #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB5_21
# BB#26:                                #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB5_21
# BB#27:                                #   in Loop: Header=BB5_2 Depth=1
	testl	%eax, %eax
	setne	%al
	orb	%al, %bpl
	orb	%bpl, %r12b
	movl	8(%rsp), %eax           # 4-byte Reload
	orb	%r12b, %al
	je	.LBB5_28
	jmp	.LBB5_23
.LBB5_20:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%ecx, %ecx
.LBB5_21:                               # %.thread.i
                                        #   in Loop: Header=BB5_2 Depth=1
	testb	%bpl, %r12b
	jne	.LBB5_23
# BB#22:                                # %.thread.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	andb	%cl, %al
	jne	.LBB5_23
.LBB5_28:                               #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_PFactorBigger3
	testl	%eax, %eax
	jne	.LBB5_23
.LBB5_30:                               #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB5_32
# BB#31:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rdi
	callq	ren_NotPFactorBigger3
	testl	%eax, %eax
	jne	.LBB5_23
.LBB5_32:                               #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB5_34
# BB#33:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_AFactorBigger3
	testl	%eax, %eax
	jne	.LBB5_23
.LBB5_34:                               #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB5_36
# BB#35:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ren_BFactorBigger3
	testl	%eax, %eax
	jne	.LBB5_23
.LBB5_36:                               #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB5_43
# BB#37:                                # %.ren_HasNEquivFathers.exit.thread_crit_edge.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	fol_EQUIV(%rip), %eax
.LBB5_38:                               # %ren_HasNEquivFathers.exit.thread.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_39:                               #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB5_56
# BB#40:                                #   in Loop: Header=BB5_39 Depth=2
	cmpl	%eax, (%rcx)
	jne	.LBB5_39
	.p2align	4, 0x90
.LBB5_41:                               # %.outer.i97.1118.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB5_56
# BB#42:                                #   in Loop: Header=BB5_41 Depth=2
	cmpl	%eax, (%rcx)
	jne	.LBB5_41
	jmp	.LBB5_23
.LBB5_43:                               #   in Loop: Header=BB5_2 Depth=1
	movl	fol_EQUIV(%rip), %eax
	cmpl	%eax, (%r15)
	je	.LBB5_23
# BB#44:                                # %.outer.i.preheader.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rcx
.LBB5_45:                               # %.outer.i.preheader.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB5_38
# BB#46:                                #   in Loop: Header=BB5_45 Depth=2
	cmpl	%eax, (%rcx)
	jne	.LBB5_45
.LBB5_23:                               # %ren_HasNonZeroBenefit.exit.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	term_STAMP(%rip), %eax
	movl	%eax, 24(%r15)
	.p2align	4, 0x90
.LBB5_59:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB5_2
.LBB5_60:                               # %._crit_edge
	xorl	%esi, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	list_PointerDeleteElement # TAILCALL
.LBB5_63:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$1018, %ecx             # imm = 0x3FA
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end5:
	.size	ren_FreeRenaming, .Lfunc_end5-ren_FreeRenaming
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_RootDistanceSmaller,@function
ren_RootDistanceSmaller:                # @ren_RootDistanceSmaller
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	jmp	term_RootDistanceSmaller # TAILCALL
.Lfunc_end6:
	.size	ren_RootDistanceSmaller, .Lfunc_end6-ren_RootDistanceSmaller
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_SolveDependencies,@function
ren_SolveDependencies:                  # @ren_SolveDependencies
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB7_5
# BB#1:
	movq	(%r14), %rax
	movq	8(%r14), %rcx
	movq	8(%rcx), %rbx
	testq	%rbx, %rbx
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	callq	ren_RemoveAllSubterms
	movq	%rax, (%r14)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_2
.LBB7_4:                                # %._crit_edge
	movq	%rax, %rdi
	callq	ren_SolveDependencies
	movq	%rax, (%r14)
	movq	%r14, %rax
	jmp	.LBB7_6
.LBB7_5:
	xorl	%eax, %eax
.LBB7_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	ren_SolveDependencies, .Lfunc_end7-ren_SolveDependencies
	.cfi_endproc

	.globl	ren_PrettyPrint
	.p2align	4, 0x90
	.type	ren_PrettyPrint,@function
ren_PrettyPrint:                        # @ren_PrettyPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.3, %edi
	callq	puts
	movl	$.L.str.4, %edi
	callq	puts
	movq	(%r14), %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.5, %edi
	callq	puts
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.1, %edi
	callq	puts
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %._crit_edge
	movl	20(%r14), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%esi, %esi
	cmpl	$0, 16(%r14)
	setne	%sil
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.Lfunc_end8:
	.size	ren_PrettyPrint, .Lfunc_end8-ren_PrettyPrint
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_Delete,@function
ren_Delete:                             # @ren_Delete
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB9_2
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB9_1
.LBB9_2:                                # %list_Delete.exit
	movq	memory_ARRAY+192(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+192(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end9:
	.size	ren_Delete, .Lfunc_end9-ren_Delete
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end10:
	.size	misc_DumpCore, .Lfunc_end10-misc_DumpCore
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_PFactorOk,@function
ren_PFactorOk:                          # @ren_PFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	term_STAMP(%rip), %eax
	xorl	%r14d, %r14d
	cmpl	24(%rdi), %eax
	je	.LBB11_20
# BB#1:                                 # %.lr.ph49
	movl	fol_EQUIV(%rip), %ecx
	movl	fol_AND(%rip), %edx
	movl	fol_NOT(%rip), %esi
	movl	fol_ALL(%rip), %r10d
	movl	fol_EXIST(%rip), %r9d
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	jns	.LBB11_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movl	%ebx, %ebp
	negl	%ebp
	andl	%r8d, %ebp
	cmpl	$2, %ebp
	je	.LBB11_20
.LBB11_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB11_2 Depth=1
	cmpl	%ecx, %ebx
	je	.LBB11_19
# BB#5:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB11_2 Depth=1
	cmpl	%edx, %ebx
	je	.LBB11_19
# BB#6:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpl	%esi, %ebx
	je	.LBB11_21
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpl	%ebx, %r10d
	je	.LBB11_9
# BB#8:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpl	%ebx, %r9d
	jne	.LBB11_10
.LBB11_9:                               # %tailrecurse
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	cmpl	24(%rdi), %eax
	jne	.LBB11_2
	jmp	.LBB11_20
.LBB11_21:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	ren_NotPFactorOk        # TAILCALL
.LBB11_10:
	cmpl	fol_IMPLIES(%rip), %ebx
	jne	.LBB11_14
# BB#11:
	movq	16(%rdi), %rbx
	movq	8(%rbx), %rdi
	callq	ren_NotPFactorOk
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB11_13
# BB#12:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	setne	%cl
.LBB11_13:
	movzbl	%cl, %r14d
	jmp	.LBB11_20
.LBB11_14:
	cmpl	fol_OR(%rip), %ebx
	jne	.LBB11_20
# BB#15:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_18
	jmp	.LBB11_20
	.p2align	4, 0x90
.LBB11_16:                              #   in Loop: Header=BB11_18 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB11_20
.LBB11_18:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB11_16
.LBB11_19:
	movl	$1, %r14d
.LBB11_20:                              # %.critedge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	ren_PFactorOk, .Lfunc_end11-ren_PFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_NotPFactorOk,@function
ren_NotPFactorOk:                       # @ren_NotPFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	term_STAMP(%rip), %eax
	xorl	%r14d, %r14d
	cmpl	24(%rdi), %eax
	je	.LBB12_17
# BB#1:                                 # %.lr.ph45
	movl	fol_EQUIV(%rip), %r11d
	movl	fol_OR(%rip), %edx
	movl	fol_IMPLIES(%rip), %esi
	movl	fol_NOT(%rip), %ebx
	movl	fol_ALL(%rip), %r10d
	movl	fol_EXIST(%rip), %r8d
	movl	symbol_TYPEMASK(%rip), %r9d
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jns	.LBB12_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	%ecx, %ebp
	negl	%ebp
	andl	%r9d, %ebp
	cmpl	$2, %ebp
	je	.LBB12_17
.LBB12_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpl	%r11d, %ecx
	je	.LBB12_16
# BB#5:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpl	%edx, %ecx
	je	.LBB12_16
# BB#6:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpl	%esi, %ecx
	je	.LBB12_16
# BB#7:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	%ebx, %ecx
	je	.LBB12_18
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	%ecx, %r10d
	je	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpl	%ecx, %r8d
	jne	.LBB12_11
.LBB12_10:                              # %tailrecurse
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	16(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdi
	cmpl	24(%rdi), %eax
	jne	.LBB12_2
	jmp	.LBB12_17
.LBB12_18:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	ren_PFactorOk           # TAILCALL
.LBB12_11:
	cmpl	fol_AND(%rip), %ecx
	jne	.LBB12_17
# BB#12:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_15
	jmp	.LBB12_17
	.p2align	4, 0x90
.LBB12_13:                              #   in Loop: Header=BB12_15 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB12_17
.LBB12_15:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB12_13
.LBB12_16:
	movl	$1, %r14d
.LBB12_17:                              # %.critedge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	ren_NotPFactorOk, .Lfunc_end12-ren_NotPFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_AFactorOk,@function
ren_AFactorOk:                          # @ren_AFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 80
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%eax, %eax
	cmpq	%rbp, %r14
	je	.LBB13_31
# BB#1:                                 # %.lr.ph75
	movl	fol_AND(%rip), %r12d
	movl	fol_ALL(%rip), %r13d
	movl	fol_EXIST(%rip), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	fol_NOT(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	fol_OR(%rip), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
	movq	%rbp, %rbx
	movq	8(%rbx), %rbp
	movl	(%rbp), %eax
	cmpl	%r12d, %eax
	je	.LBB13_5
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	%eax, %r13d
	je	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	%eax, 20(%rsp)          # 4-byte Folded Reload
	je	.LBB13_5
# BB#7:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB13_32
# BB#8:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB13_15
# BB#9:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	16(%rbp), %r15
	testq	%r15, %r15
	jne	.LBB13_11
	jmp	.LBB13_5
	.p2align	4, 0x90
.LBB13_14:                              #   in Loop: Header=BB13_11 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB13_5
.LBB13_11:                              # %.lr.ph
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rdi
	cmpq	%rbx, %rdi
	je	.LBB13_14
# BB#12:                                #   in Loop: Header=BB13_11 Depth=2
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB13_14
	jmp	.LBB13_13
	.p2align	4, 0x90
.LBB13_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB13_2 Depth=1
	cmpq	%r14, %rbp
	jne	.LBB13_2
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB13_31
.LBB13_13:
	movl	$1, %eax
.LBB13_31:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_15:
	cmpl	fol_IMPLIES(%rip), %eax
	jne	.LBB13_20
# BB#16:
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	cmpq	%rbx, %rdi
	je	.LBB13_32
# BB#17:
	callq	ren_NotPFactorOk
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB13_19
# BB#18:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	setne	%cl
.LBB13_19:
	movzbl	%cl, %eax
	jmp	.LBB13_31
.LBB13_32:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_BFactorOk           # TAILCALL
.LBB13_20:
	cmpl	fol_EQUIV(%rip), %eax
	jne	.LBB13_33
# BB#21:
	movq	%rbp, %rdi
	callq	ren_Polarity
	movb	$1, %r15b
	testl	%eax, %eax
	je	.LBB13_30
# BB#22:
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rdi
	cmpq	%rbx, %rdi
	jne	.LBB13_24
# BB#23:
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdi
.LBB13_24:
	cmpl	$1, %eax
	jne	.LBB13_27
# BB#25:
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB13_30
# BB#26:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	jmp	.LBB13_29
.LBB13_27:
	callq	ren_PFactorOk
	testl	%eax, %eax
	jne	.LBB13_30
# BB#28:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
.LBB13_29:
	testl	%eax, %eax
	setne	%r15b
.LBB13_30:
	movzbl	%r15b, %eax
	jmp	.LBB13_31
.LBB13_33:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$539, %ecx              # imm = 0x21B
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end13:
	.size	ren_AFactorOk, .Lfunc_end13-ren_AFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_BFactorOk,@function
ren_BFactorOk:                          # @ren_BFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 80
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%eax, %eax
	cmpq	%rbp, %r14
	je	.LBB14_31
# BB#1:                                 # %.lr.ph79
	movl	fol_OR(%rip), %r12d
	movl	fol_ALL(%rip), %r13d
	movl	fol_EXIST(%rip), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	fol_NOT(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	fol_AND(%rip), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_11 Depth 2
	movq	%rbp, %rbx
	movq	8(%rbx), %rbp
	movl	(%rbp), %eax
	cmpl	%r12d, %eax
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	%eax, %r13d
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	%eax, 20(%rsp)          # 4-byte Folded Reload
	je	.LBB14_5
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB14_32
# BB#8:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB14_15
# BB#9:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rbp), %r15
	testq	%r15, %r15
	jne	.LBB14_11
	jmp	.LBB14_5
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_11 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB14_5
.LBB14_11:                              # %.lr.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rdi
	cmpq	%rbx, %rdi
	je	.LBB14_14
# BB#12:                                #   in Loop: Header=BB14_11 Depth=2
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB14_14
	jmp	.LBB14_13
.LBB14_15:                              #   in Loop: Header=BB14_2 Depth=1
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB14_20
# BB#16:                                #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rbp), %rax
	cmpq	%rbx, 8(%rax)
	je	.LBB14_17
	.p2align	4, 0x90
.LBB14_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB14_2 Depth=1
	cmpq	%r14, %rbp
	jne	.LBB14_2
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB14_31
.LBB14_13:
	movl	$1, %eax
.LBB14_31:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_32:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_AFactorOk           # TAILCALL
.LBB14_20:
	cmpl	fol_EQUIV(%rip), %eax
	jne	.LBB14_33
# BB#21:
	movq	%rbp, %rdi
	callq	ren_Polarity
	movb	$1, %r15b
	testl	%eax, %eax
	je	.LBB14_30
# BB#22:
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rdi
	cmpq	%rbx, %rdi
	jne	.LBB14_24
# BB#23:
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdi
.LBB14_24:
	cmpl	$1, %eax
	jne	.LBB14_27
# BB#25:
	callq	ren_PFactorOk
	testl	%eax, %eax
	jne	.LBB14_30
# BB#26:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	jmp	.LBB14_29
.LBB14_17:
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	ren_PFactorOk
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB14_19
# BB#18:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	setne	%cl
.LBB14_19:
	movzbl	%cl, %eax
	jmp	.LBB14_31
.LBB14_27:
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB14_30
# BB#28:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
.LBB14_29:
	testl	%eax, %eax
	setne	%r15b
.LBB14_30:
	movzbl	%r15b, %eax
	jmp	.LBB14_31
.LBB14_33:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$765, %ecx              # imm = 0x2FD
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end14:
	.size	ren_BFactorOk, .Lfunc_end14-ren_BFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_PExtraFactorOk,@function
ren_PExtraFactorOk:                     # @ren_PExtraFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	term_STAMP(%rip), %ecx
	xorl	%eax, %eax
	cmpl	24(%rbx), %ecx
	je	.LBB15_35
# BB#1:                                 # %.lr.ph84
	movl	fol_ALL(%rip), %esi
	movl	fol_EXIST(%rip), %edi
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	jns	.LBB15_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	movl	%ebp, %edx
	negl	%edx
	andl	%r8d, %edx
	cmpl	$2, %edx
	je	.LBB15_35
.LBB15_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpl	%ebp, %esi
	je	.LBB15_6
# BB#5:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpl	%ebp, %edi
	jne	.LBB15_7
.LBB15_6:                               # %tailrecurse
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %rbx
	cmpl	24(%rbx), %ecx
	jne	.LBB15_2
	jmp	.LBB15_35
.LBB15_7:
	cmpl	fol_NOT(%rip), %ebp
	jne	.LBB15_9
# BB#8:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	ren_NotPExtraFactorOk   # TAILCALL
.LBB15_9:
	cmpl	fol_EQUIV(%rip), %ebp
	jne	.LBB15_15
# BB#10:
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	8(%rcx), %r14
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	movb	$1, %bpl
	testl	%eax, %eax
	jne	.LBB15_14
# BB#11:
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB15_14
# BB#12:
	movq	%rbx, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB15_14
# BB#13:
	movq	%r14, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	setne	%bpl
.LBB15_14:
	movzbl	%bpl, %eax
	jmp	.LBB15_35
.LBB15_15:
	cmpl	fol_AND(%rip), %ebp
	jne	.LBB15_20
# BB#16:
	movq	16(%rbx), %rdi
	callq	list_Length
	movb	$1, %bpl
	cmpl	$2, %eax
	ja	.LBB15_19
# BB#17:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	jne	.LBB15_19
# BB#18:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	setne	%bpl
.LBB15_19:
	movzbl	%bpl, %eax
	jmp	.LBB15_35
.LBB15_20:
	cmpl	fol_IMPLIES(%rip), %ebp
	jne	.LBB15_24
# BB#21:
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	8(%rcx), %rbx
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB15_31
# BB#22:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB15_34
# BB#23:
	movq	%r14, %rdi
	callq	ren_NotPExtraFactorOk
	jmp	.LBB15_33
.LBB15_24:
	cmpl	fol_OR(%rip), %ebp
	jne	.LBB15_35
# BB#25:
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB15_35
# BB#26:                                # %.lr.ph.preheader
	xorl	%r14d, %r14d
.LBB15_27:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB15_30
# BB#28:                                #   in Loop: Header=BB15_27 Depth=1
	testl	%r14d, %r14d
	jne	.LBB15_37
# BB#29:                                #   in Loop: Header=BB15_27 Depth=1
	movq	%rbx, %rdi
	callq	ren_PExtraFactorOk
	movl	$1, %r14d
	testl	%eax, %eax
	movl	$1, %eax
	jne	.LBB15_35
.LBB15_30:                              #   in Loop: Header=BB15_27 Depth=1
	movq	(%rbp), %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	jne	.LBB15_27
	jmp	.LBB15_35
.LBB15_31:
	testl	%ebp, %ebp
	je	.LBB15_36
# BB#32:
	movq	%rbx, %rdi
	callq	ren_PExtraFactorOk
.LBB15_33:                              # %.thread
	testl	%eax, %eax
	setne	%al
	jmp	.LBB15_34
.LBB15_36:
	xorl	%eax, %eax
.LBB15_34:                              # %.thread
	movzbl	%al, %eax
.LBB15_35:                              # %.critedge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB15_37:
	movl	$1, %eax
	jmp	.LBB15_35
.Lfunc_end15:
	.size	ren_PExtraFactorOk, .Lfunc_end15-ren_PExtraFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_AExtraFactorOk,@function
ren_AExtraFactorOk:                     # @ren_AExtraFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 64
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	cmpq	%rbp, %r14
	je	.LBB16_6
# BB#1:                                 # %.lr.ph108
	movl	fol_AND(%rip), %ecx
	movl	fol_ALL(%rip), %edx
	movl	fol_EXIST(%rip), %esi
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rbx
	movq	8(%rbx), %rbp
	movl	(%rbp), %eax
	cmpl	%ecx, %eax
	je	.LBB16_5
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	cmpl	%eax, %edx
	je	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	cmpl	%eax, %esi
	jne	.LBB16_7
.LBB16_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB16_2 Depth=1
	cmpq	%r14, %rbp
	jne	.LBB16_2
.LBB16_6:                               # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_7:
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB16_9
.LBB16_8:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_BExtraFactorOk      # TAILCALL
.LBB16_9:
	cmpl	fol_OR(%rip), %eax
	jne	.LBB16_16
# BB#10:
	movq	16(%rbp), %r13
	xorl	%r15d, %r15d
	testq	%r13, %r13
	je	.LBB16_21
	.p2align	4, 0x90
.LBB16_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %r12
	cmpq	%rbx, %r12
	je	.LBB16_11
# BB#13:                                #   in Loop: Header=BB16_12 Depth=1
	movq	%r12, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB16_11
# BB#14:                                #   in Loop: Header=BB16_12 Depth=1
	testl	%r15d, %r15d
	jne	.LBB16_42
# BB#15:                                #   in Loop: Header=BB16_12 Depth=1
	movq	%r12, %rdi
	callq	ren_PExtraFactorOk
	movl	$1, %r15d
	testl	%eax, %eax
	jne	.LBB16_6
.LBB16_11:                              #   in Loop: Header=BB16_12 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB16_12
.LBB16_21:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	je	.LBB16_35
# BB#22:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB16_46
	jmp	.LBB16_23
.LBB16_16:
	cmpl	fol_IMPLIES(%rip), %eax
	jne	.LBB16_24
# BB#17:
	movq	16(%rbp), %rax
	movq	8(%rax), %r12
	cmpq	%rbx, %r12
	je	.LBB16_8
# BB#18:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB16_40
# BB#19:
	testl	%r15d, %r15d
	jne	.LBB16_23
# BB#20:
	xorl	%eax, %eax
	jmp	.LBB16_46
.LBB16_24:
	cmpl	fol_EQUIV(%rip), %eax
	jne	.LBB16_50
# BB#25:
	movq	16(%rbp), %rax
	movq	8(%rax), %r12
	cmpq	%rbx, %r12
	jne	.LBB16_27
# BB#26:
	movq	(%rax), %rax
	movq	8(%rax), %r12
.LBB16_27:
	movq	%rbp, %rdi
	callq	ren_Polarity
	cmpl	$-1, %eax
	je	.LBB16_36
# BB#28:
	cmpl	$1, %eax
	je	.LBB16_39
# BB#29:
	testl	%eax, %eax
	jne	.LBB16_50
# BB#30:
	movq	%r12, %rdi
	callq	ren_PFactorOk
	movb	$1, %bl
	testl	%eax, %eax
	jne	.LBB16_34
# BB#31:
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB16_34
# BB#32:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	jne	.LBB16_34
# BB#33:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
	testl	%eax, %eax
	setne	%bl
.LBB16_34:
	movzbl	%bl, %r15d
	jmp	.LBB16_6
.LBB16_35:
	xorl	%eax, %eax
	jmp	.LBB16_46
.LBB16_36:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB16_43
# BB#37:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB16_46
# BB#38:
	movq	%r12, %rdi
	callq	ren_PExtraFactorOk
	jmp	.LBB16_45
.LBB16_39:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB16_47
.LBB16_40:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB16_46
# BB#41:
	movq	%r12, %rdi
	callq	ren_NotPExtraFactorOk
	jmp	.LBB16_45
.LBB16_42:
	movl	$1, %r15d
	jmp	.LBB16_6
.LBB16_43:
	testl	%r15d, %r15d
	je	.LBB16_49
# BB#44:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BExtraFactorOk
	jmp	.LBB16_45
.LBB16_47:
	testl	%r15d, %r15d
	je	.LBB16_49
.LBB16_23:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AExtraFactorOk
.LBB16_45:                              # %.thread90
	testl	%eax, %eax
	setne	%al
.LBB16_46:                              # %.thread90
	movzbl	%al, %r15d
	jmp	.LBB16_6
.LBB16_49:
	xorl	%eax, %eax
	jmp	.LBB16_46
.LBB16_50:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$616, %ecx              # imm = 0x268
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end16:
	.size	ren_AExtraFactorOk, .Lfunc_end16-ren_AExtraFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_NotPExtraFactorOk,@function
ren_NotPExtraFactorOk:                  # @ren_NotPExtraFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 48
.Lcfi120:
	.cfi_offset %rbx, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	term_STAMP(%rip), %eax
	xorl	%r14d, %r14d
	cmpl	24(%rbx), %eax
	je	.LBB17_29
# BB#1:                                 # %.lr.ph82
	movl	fol_NOT(%rip), %ecx
	movl	fol_ALL(%rip), %esi
	movl	fol_EXIST(%rip), %edi
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	jns	.LBB17_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB17_2 Depth=1
	movl	%ebp, %edx
	negl	%edx
	andl	%r8d, %edx
	cmpl	$2, %edx
	je	.LBB17_29
.LBB17_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpl	%ecx, %ebp
	je	.LBB17_31
# BB#5:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpl	%ebp, %esi
	je	.LBB17_7
# BB#6:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpl	%ebp, %edi
	jne	.LBB17_8
.LBB17_7:                               # %tailrecurse
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %rbx
	cmpl	24(%rbx), %eax
	jne	.LBB17_2
	jmp	.LBB17_29
.LBB17_31:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_PExtraFactorOk      # TAILCALL
.LBB17_8:
	cmpl	fol_EQUIV(%rip), %ebp
	jne	.LBB17_14
# BB#9:
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rbp
	movq	8(%rcx), %r14
	movq	%rbp, %rdi
	callq	ren_PFactorOk
	movb	$1, %bl
	testl	%eax, %eax
	jne	.LBB17_13
# BB#10:
	movq	%r14, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	jne	.LBB17_13
# BB#11:
	movq	%rbp, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB17_13
# BB#12:
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	setne	%bl
.LBB17_13:
	movzbl	%bl, %r14d
	jmp	.LBB17_29
.LBB17_14:
	cmpl	fol_OR(%rip), %ebp
	jne	.LBB17_18
# BB#15:
	movq	16(%rbx), %rdi
	callq	list_Length
	movl	$1, %r14d
	cmpl	$2, %eax
	ja	.LBB17_29
# BB#16:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB17_29
# BB#17:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	ren_NotPFactorOk
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	movl	%ecx, %eax
	jmp	.LBB17_30
.LBB17_18:
	cmpl	fol_IMPLIES(%rip), %ebp
	jne	.LBB17_21
# BB#19:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %rdi
	callq	ren_PFactorOk
	movl	$1, %r14d
	testl	%eax, %eax
	jne	.LBB17_29
# BB#20:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	ren_NotPFactorOk
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	jmp	.LBB17_29
.LBB17_21:
	cmpl	fol_AND(%rip), %ebp
	jne	.LBB17_29
# BB#22:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB17_29
# BB#23:                                # %.lr.ph.preheader
	xorl	%r15d, %r15d
.LBB17_24:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movq	%rbp, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB17_28
# BB#25:                                #   in Loop: Header=BB17_24 Depth=1
	testl	%r15d, %r15d
	jne	.LBB17_26
# BB#27:                                #   in Loop: Header=BB17_24 Depth=1
	movq	%rbp, %rdi
	callq	ren_NotPExtraFactorOk
	movl	$1, %r15d
	testl	%eax, %eax
	movl	$1, %r14d
	jne	.LBB17_29
.LBB17_28:                              #   in Loop: Header=BB17_24 Depth=1
	movq	(%rbx), %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB17_24
	jmp	.LBB17_29
.LBB17_26:
	movl	$1, %r14d
.LBB17_29:                              # %.critedge
	movl	%r14d, %eax
.LBB17_30:                              # %.critedge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	ren_NotPExtraFactorOk, .Lfunc_end17-ren_NotPExtraFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_BExtraFactorOk,@function
ren_BExtraFactorOk:                     # @ren_BExtraFactorOk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 64
.Lcfi131:
	.cfi_offset %rbx, -56
.Lcfi132:
	.cfi_offset %r12, -48
.Lcfi133:
	.cfi_offset %r13, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	cmpq	%rbp, %r14
	je	.LBB18_48
# BB#1:                                 # %.lr.ph118
	movl	fol_OR(%rip), %eax
	movl	fol_ALL(%rip), %ecx
	movl	fol_EXIST(%rip), %edx
	movl	fol_NOT(%rip), %esi
	movl	fol_AND(%rip), %r9d
	movl	fol_IMPLIES(%rip), %r8d
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rbx
	movq	8(%rbx), %rbp
	movl	(%rbp), %edi
	cmpl	%eax, %edi
	je	.LBB18_5
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	%edi, %ecx
	je	.LBB18_5
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	%edi, %edx
	je	.LBB18_5
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	%esi, %edi
	je	.LBB18_49
# BB#7:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	%r9d, %edi
	je	.LBB18_8
# BB#20:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%r8d, %edi
	jne	.LBB18_26
# BB#21:                                #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rbp), %rdi
	cmpq	%rbx, 8(%rdi)
	je	.LBB18_22
.LBB18_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	%r14, %rbp
	jne	.LBB18_2
	jmp	.LBB18_48
.LBB18_49:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_AExtraFactorOk      # TAILCALL
.LBB18_8:
	movq	16(%rbp), %r13
	xorl	%r15d, %r15d
	testq	%r13, %r13
	jne	.LBB18_10
	jmp	.LBB18_16
	.p2align	4, 0x90
.LBB18_15:                              #   in Loop: Header=BB18_10 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	je	.LBB18_16
.LBB18_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %r12
	cmpq	%rbx, %r12
	je	.LBB18_15
# BB#11:                                #   in Loop: Header=BB18_10 Depth=1
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB18_15
# BB#12:                                #   in Loop: Header=BB18_10 Depth=1
	testl	%r15d, %r15d
	jne	.LBB18_13
# BB#14:                                #   in Loop: Header=BB18_10 Depth=1
	movq	%r12, %rdi
	callq	ren_NotPExtraFactorOk
	movl	$1, %r15d
	testl	%eax, %eax
	je	.LBB18_15
	jmp	.LBB18_48
.LBB18_26:
	cmpl	fol_EQUIV(%rip), %edi
	jne	.LBB18_50
# BB#27:
	movq	16(%rbp), %rax
	movq	8(%rax), %r12
	cmpq	%rbx, %r12
	jne	.LBB18_29
# BB#28:
	movq	(%rax), %rax
	movq	8(%rax), %r12
.LBB18_29:
	movq	%rbp, %rdi
	callq	ren_Polarity
	cmpl	$-1, %eax
	je	.LBB18_42
# BB#30:
	cmpl	$1, %eax
	je	.LBB18_37
# BB#31:
	testl	%eax, %eax
	jne	.LBB18_50
# BB#32:
	movq	%r12, %rdi
	callq	ren_PFactorOk
	movb	$1, %bl
	testl	%eax, %eax
	jne	.LBB18_36
# BB#33:
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	jne	.LBB18_36
# BB#34:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	jne	.LBB18_36
# BB#35:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
	testl	%eax, %eax
	setne	%bl
.LBB18_36:
	movzbl	%bl, %r15d
	jmp	.LBB18_48
.LBB18_22:
	movq	(%rdi), %rax
	movq	8(%rax), %r15
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB18_25
# BB#23:
	movb	$1, %al
	testl	%ebx, %ebx
	jne	.LBB18_47
# BB#24:
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	jmp	.LBB18_19
.LBB18_16:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
	testl	%eax, %eax
	je	.LBB18_46
# BB#17:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB18_47
	jmp	.LBB18_18
.LBB18_25:
	testl	%ebx, %ebx
	jne	.LBB18_41
	jmp	.LBB18_46
.LBB18_42:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BFactorOk
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB18_45
# BB#43:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB18_47
# BB#44:
	movq	%r12, %rdi
	callq	ren_NotPExtraFactorOk
	jmp	.LBB18_19
.LBB18_37:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AFactorOk
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB18_40
# BB#38:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB18_47
# BB#39:
	movq	%r12, %rdi
	callq	ren_PExtraFactorOk
	jmp	.LBB18_19
.LBB18_13:
	movl	$1, %r15d
	jmp	.LBB18_48
.LBB18_45:
	testl	%r15d, %r15d
	je	.LBB18_46
.LBB18_18:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_BExtraFactorOk
	jmp	.LBB18_19
.LBB18_40:
	testl	%r15d, %r15d
	je	.LBB18_46
.LBB18_41:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ren_AExtraFactorOk
.LBB18_19:                              # %.thread90
	testl	%eax, %eax
	setne	%al
	jmp	.LBB18_47
.LBB18_46:
	xorl	%eax, %eax
.LBB18_47:                              # %.thread90
	movzbl	%al, %r15d
.LBB18_48:                              # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_50:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$842, %ecx              # imm = 0x34A
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end18:
	.size	ren_BExtraFactorOk, .Lfunc_end18-ren_BExtraFactorOk
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_PFactorBigger3,@function
ren_PFactorBigger3:                     # @ren_PFactorBigger3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi140:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi143:
	.cfi_def_cfa_offset 80
.Lcfi144:
	.cfi_offset %rbx, -56
.Lcfi145:
	.cfi_offset %r12, -48
.Lcfi146:
	.cfi_offset %r13, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	term_STAMP(%rip), %edx
	xorl	%eax, %eax
	cmpl	24(%rbp), %edx
	je	.LBB19_45
# BB#1:                                 # %.lr.ph109
	movl	fol_ALL(%rip), %esi
	movl	fol_EXIST(%rip), %edi
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ebx
	testl	%ebx, %ebx
	jns	.LBB19_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	andl	%r8d, %ecx
	cmpl	$2, %ecx
	je	.LBB19_45
.LBB19_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	%ebx, %esi
	je	.LBB19_6
# BB#5:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	%ebx, %edi
	jne	.LBB19_7
.LBB19_6:                               # %tailrecurse
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	16(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rbp
	cmpl	24(%rbp), %edx
	jne	.LBB19_2
	jmp	.LBB19_45
.LBB19_7:
	cmpl	fol_NOT(%rip), %ebx
	jne	.LBB19_9
# BB#8:
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_NotPFactorBigger3   # TAILCALL
.LBB19_9:
	cmpl	fol_AND(%rip), %ebx
	jne	.LBB19_21
# BB#10:
	movq	16(%rbp), %rdi
	callq	list_Length
	movl	%eax, %r14d
	cmpb	$3, %r14b
	ja	.LBB19_33
# BB#11:
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB19_33
	.p2align	4, 0x90
.LBB19_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r15
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB19_18
# BB#13:                                #   in Loop: Header=BB19_12 Depth=1
	movl	%r14d, %ebx
	incb	%bl
	cmpb	$3, %bl
	ja	.LBB19_44
# BB#14:                                #   in Loop: Header=BB19_12 Depth=1
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	je	.LBB19_19
# BB#15:                                #   in Loop: Header=BB19_12 Depth=1
	movl	%r14d, %ebx
	addb	$2, %bl
	cmpb	$3, %bl
	ja	.LBB19_43
# BB#16:                                #   in Loop: Header=BB19_12 Depth=1
	movq	8(%rbp), %rdi
	callq	ren_PFactorBigger3
	testl	%eax, %eax
	je	.LBB19_19
# BB#17:                                #   in Loop: Header=BB19_12 Depth=1
	addb	$3, %r14b
.LBB19_18:                              #   in Loop: Header=BB19_12 Depth=1
	movl	%r14d, %ebx
.LBB19_19:                              #   in Loop: Header=BB19_12 Depth=1
	cmpb	$3, %bl
	ja	.LBB19_44
# BB#20:                                #   in Loop: Header=BB19_12 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movb	%bl, %r14b
	jne	.LBB19_12
	jmp	.LBB19_44
.LBB19_21:
	movq	16(%rbp), %rbp
	cmpl	fol_OR(%rip), %ebx
	jne	.LBB19_28
# BB#22:                                # %.preheader
	testq	%rbp, %rbp
	je	.LBB19_45
# BB#23:                                # %.lr.ph100.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_24:                              # %.lr.ph100
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB19_27
# BB#25:                                #   in Loop: Header=BB19_24 Depth=1
	testl	%r14d, %r14d
	jne	.LBB19_50
# BB#26:                                #   in Loop: Header=BB19_24 Depth=1
	movq	%rbx, %rdi
	callq	ren_PFactorBigger3
	movl	$1, %r14d
	testl	%eax, %eax
	movl	$1, %eax
	jne	.LBB19_45
.LBB19_27:                              #   in Loop: Header=BB19_24 Depth=1
	movq	(%rbp), %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	jne	.LBB19_24
	jmp	.LBB19_45
.LBB19_33:
	movl	%r14d, %ebx
.LBB19_44:                              # %.critedge
	xorl	%eax, %eax
	cmpb	$3, %bl
	seta	%al
.LBB19_45:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_28:
	movq	(%rbp), %rax
	movq	8(%rbp), %r15
	movq	8(%rax), %r14
	cmpl	fol_IMPLIES(%rip), %ebx
	jne	.LBB19_34
# BB#29:
	movq	%r14, %rdi
	callq	ren_PFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB19_46
# BB#30:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB19_49
# BB#31:
	movq	%r15, %rdi
	callq	ren_NotPFactorBigger3
	jmp	.LBB19_48
.LBB19_34:
	cmpl	fol_EQUIV(%rip), %ebx
	jne	.LBB19_56
# BB#35:
	movq	%r15, %rdi
	callq	ren_PFactorOk
	movl	%eax, %ebp
	xorl	%r12d, %r12d
	testl	%ebp, %ebp
	setne	%r12b
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %r13d
	movq	%r14, %rdi
	callq	ren_PFactorOk
	xorl	%ebx, %ebx
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bl
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	xorl	%ecx, %ecx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%cl
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	cmpl	$1, %r13d
	sbbl	$-1, %r12d
	addl	%ebx, %r12d
	addl	%ecx, %r12d
	movb	$1, %bl
	cmpl	$1, %r12d
	ja	.LBB19_55
# BB#36:
	testl	%ebp, %ebp
	je	.LBB19_38
# BB#37:
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	jne	.LBB19_55
.LBB19_38:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB19_40
# BB#39:
	movq	%r14, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	jne	.LBB19_55
.LBB19_40:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB19_52
# BB#41:
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	sete	%cl
	testl	%eax, %eax
	setne	%bl
	jne	.LBB19_55
# BB#42:
	testb	%cl, %cl
	je	.LBB19_53
	jmp	.LBB19_55
.LBB19_43:
	movb	$4, %bl
	jmp	.LBB19_44
.LBB19_46:
	testl	%ebp, %ebp
	je	.LBB19_51
# BB#47:
	movq	%r14, %rdi
	callq	ren_PFactorBigger3
.LBB19_48:                              # %.thread
	testl	%eax, %eax
	setne	%al
.LBB19_49:                              # %.thread
	movzbl	%al, %eax
	jmp	.LBB19_45
.LBB19_50:
	movl	$1, %eax
	jmp	.LBB19_45
.LBB19_51:
	xorl	%eax, %eax
	movzbl	%al, %eax
	jmp	.LBB19_45
.LBB19_52:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB19_54
.LBB19_53:
	movq	%r14, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	setne	%bl
	jmp	.LBB19_55
.LBB19_54:
	xorl	%ebx, %ebx
.LBB19_55:
	movzbl	%bl, %eax
	jmp	.LBB19_45
.LBB19_56:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$399, %ecx              # imm = 0x18F
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end19:
	.size	ren_PFactorBigger3, .Lfunc_end19-ren_PFactorBigger3
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_NotPFactorBigger3,@function
ren_NotPFactorBigger3:                  # @ren_NotPFactorBigger3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi153:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi154:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi156:
	.cfi_def_cfa_offset 80
.Lcfi157:
	.cfi_offset %rbx, -56
.Lcfi158:
	.cfi_offset %r12, -48
.Lcfi159:
	.cfi_offset %r13, -40
.Lcfi160:
	.cfi_offset %r14, -32
.Lcfi161:
	.cfi_offset %r15, -24
.Lcfi162:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	term_STAMP(%rip), %edx
	xorl	%eax, %eax
	cmpl	24(%rbp), %edx
	je	.LBB20_45
# BB#1:                                 # %.lr.ph109
	movl	fol_ALL(%rip), %esi
	movl	fol_EXIST(%rip), %edi
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ebx
	testl	%ebx, %ebx
	jns	.LBB20_4
# BB#3:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	andl	%r8d, %ecx
	cmpl	$2, %ecx
	je	.LBB20_45
.LBB20_4:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpl	%ebx, %esi
	je	.LBB20_6
# BB#5:                                 # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpl	%ebx, %edi
	jne	.LBB20_7
.LBB20_6:                               # %tailrecurse
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	16(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rbp
	cmpl	24(%rbp), %edx
	jne	.LBB20_2
	jmp	.LBB20_45
.LBB20_7:
	cmpl	fol_NOT(%rip), %ebx
	jne	.LBB20_9
# BB#8:
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_PFactorBigger3      # TAILCALL
.LBB20_9:
	cmpl	fol_OR(%rip), %ebx
	jne	.LBB20_21
# BB#10:
	movq	16(%rbp), %rdi
	callq	list_Length
	movl	%eax, %r14d
	cmpb	$3, %r14b
	ja	.LBB20_33
# BB#11:
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB20_33
	.p2align	4, 0x90
.LBB20_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r15
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB20_18
# BB#13:                                #   in Loop: Header=BB20_12 Depth=1
	movl	%r14d, %ebx
	incb	%bl
	cmpb	$3, %bl
	ja	.LBB20_44
# BB#14:                                #   in Loop: Header=BB20_12 Depth=1
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	je	.LBB20_19
# BB#15:                                #   in Loop: Header=BB20_12 Depth=1
	movl	%r14d, %ebx
	addb	$2, %bl
	cmpb	$3, %bl
	ja	.LBB20_43
# BB#16:                                #   in Loop: Header=BB20_12 Depth=1
	movq	8(%rbp), %rdi
	callq	ren_NotPFactorBigger3
	testl	%eax, %eax
	je	.LBB20_19
# BB#17:                                #   in Loop: Header=BB20_12 Depth=1
	addb	$3, %r14b
.LBB20_18:                              #   in Loop: Header=BB20_12 Depth=1
	movl	%r14d, %ebx
.LBB20_19:                              #   in Loop: Header=BB20_12 Depth=1
	cmpb	$3, %bl
	ja	.LBB20_44
# BB#20:                                #   in Loop: Header=BB20_12 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movb	%bl, %r14b
	jne	.LBB20_12
	jmp	.LBB20_44
.LBB20_21:
	movq	16(%rbp), %rbp
	cmpl	fol_AND(%rip), %ebx
	jne	.LBB20_28
# BB#22:                                # %.preheader
	testq	%rbp, %rbp
	je	.LBB20_45
# BB#23:                                # %.lr.ph100.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB20_24:                              # %.lr.ph100
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB20_27
# BB#25:                                #   in Loop: Header=BB20_24 Depth=1
	testl	%r14d, %r14d
	jne	.LBB20_50
# BB#26:                                #   in Loop: Header=BB20_24 Depth=1
	movq	%rbx, %rdi
	callq	ren_NotPFactorBigger3
	movl	$1, %r14d
	testl	%eax, %eax
	movl	$1, %eax
	jne	.LBB20_45
.LBB20_27:                              #   in Loop: Header=BB20_24 Depth=1
	movq	(%rbp), %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	jne	.LBB20_24
	jmp	.LBB20_45
.LBB20_33:
	movl	%r14d, %ebx
.LBB20_44:                              # %.critedge
	xorl	%eax, %eax
	cmpb	$3, %bl
	seta	%al
.LBB20_45:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_28:
	movq	(%rbp), %rax
	movq	8(%rbp), %r15
	movq	8(%rax), %r14
	cmpl	fol_IMPLIES(%rip), %ebx
	jne	.LBB20_34
# BB#29:
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB20_46
# BB#30:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB20_49
# BB#31:
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	jmp	.LBB20_48
.LBB20_34:
	cmpl	fol_EQUIV(%rip), %ebx
	jne	.LBB20_56
# BB#35:
	movq	%r15, %rdi
	callq	ren_PFactorOk
	movl	%eax, %ebp
	xorl	%r12d, %r12d
	testl	%ebp, %ebp
	setne	%r12b
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %r13d
	movq	%r14, %rdi
	callq	ren_PFactorOk
	xorl	%ebx, %ebx
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bl
	movq	%r14, %rdi
	callq	ren_NotPFactorOk
	xorl	%ecx, %ecx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%cl
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	cmpl	$1, %r13d
	sbbl	$-1, %r12d
	addl	%ebx, %r12d
	addl	%ecx, %r12d
	movb	$1, %bl
	cmpl	$1, %r12d
	ja	.LBB20_55
# BB#36:
	testl	%ebp, %ebp
	je	.LBB20_38
# BB#37:
	movq	%r15, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	jne	.LBB20_55
.LBB20_38:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB20_40
# BB#39:
	movq	%r14, %rdi
	callq	ren_PExtraFactorOk
	testl	%eax, %eax
	jne	.LBB20_55
.LBB20_40:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB20_52
# BB#41:
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	sete	%cl
	testl	%eax, %eax
	setne	%bl
	jne	.LBB20_55
# BB#42:
	testb	%cl, %cl
	je	.LBB20_53
	jmp	.LBB20_55
.LBB20_43:
	movb	$4, %bl
	jmp	.LBB20_44
.LBB20_46:
	testl	%ebp, %ebp
	je	.LBB20_51
# BB#47:
	movq	%r14, %rdi
	callq	ren_NotPExtraFactorOk
.LBB20_48:                              # %.thread
	testl	%eax, %eax
	setne	%al
.LBB20_49:                              # %.thread
	movzbl	%al, %eax
	jmp	.LBB20_45
.LBB20_50:
	movl	$1, %eax
	jmp	.LBB20_45
.LBB20_51:
	xorl	%eax, %eax
	movzbl	%al, %eax
	jmp	.LBB20_45
.LBB20_52:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB20_54
.LBB20_53:
	movq	%r14, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	setne	%bl
	jmp	.LBB20_55
.LBB20_54:
	xorl	%ebx, %ebx
.LBB20_55:
	movzbl	%bl, %eax
	jmp	.LBB20_45
.LBB20_56:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$479, %ecx              # imm = 0x1DF
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end20:
	.size	ren_NotPFactorBigger3, .Lfunc_end20-ren_NotPFactorBigger3
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_AFactorBigger3,@function
ren_AFactorBigger3:                     # @ren_AFactorBigger3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi166:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi167:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi169:
	.cfi_def_cfa_offset 80
.Lcfi170:
	.cfi_offset %rbx, -56
.Lcfi171:
	.cfi_offset %r12, -48
.Lcfi172:
	.cfi_offset %r13, -40
.Lcfi173:
	.cfi_offset %r14, -32
.Lcfi174:
	.cfi_offset %r15, -24
.Lcfi175:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	cmpq	%r13, %r14
	je	.LBB21_6
# BB#1:                                 # %.lr.ph131
	movl	fol_AND(%rip), %ecx
	movl	fol_ALL(%rip), %edx
	movl	fol_EXIST(%rip), %esi
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rbx
	movq	8(%rbx), %r13
	movl	(%r13), %eax
	cmpl	%ecx, %eax
	je	.LBB21_5
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	cmpl	%eax, %edx
	je	.LBB21_5
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	cmpl	%eax, %esi
	jne	.LBB21_7
.LBB21_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB21_2 Depth=1
	cmpq	%r14, %r13
	jne	.LBB21_2
.LBB21_6:                               # %.loopexit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_7:
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB21_9
.LBB21_8:
	movq	%r14, %rdi
	movq	%r13, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_BFactorBigger3      # TAILCALL
.LBB21_9:
	cmpl	fol_OR(%rip), %eax
	jne	.LBB21_16
# BB#10:
	movq	16(%r13), %rbp
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	je	.LBB21_21
	.p2align	4, 0x90
.LBB21_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r12
	cmpq	%rbx, %r12
	je	.LBB21_11
# BB#13:                                #   in Loop: Header=BB21_12 Depth=1
	movq	%r12, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB21_11
# BB#14:                                #   in Loop: Header=BB21_12 Depth=1
	testl	%r15d, %r15d
	jne	.LBB21_42
# BB#15:                                #   in Loop: Header=BB21_12 Depth=1
	movq	%r12, %rdi
	callq	ren_PFactorBigger3
	movl	$1, %r15d
	testl	%eax, %eax
	jne	.LBB21_6
.LBB21_11:                              #   in Loop: Header=BB21_12 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_12
.LBB21_21:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	testl	%eax, %eax
	je	.LBB21_35
# BB#22:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB21_50
	jmp	.LBB21_48
.LBB21_16:
	cmpl	fol_IMPLIES(%rip), %eax
	jne	.LBB21_23
# BB#17:
	movq	16(%r13), %rax
	movq	8(%rax), %rbp
	cmpq	%rbx, %rbp
	je	.LBB21_8
# BB#18:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	movl	%eax, %r15d
	movq	%rbp, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB21_43
# BB#19:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB21_50
# BB#20:
	movq	%rbp, %rdi
	callq	ren_NotPFactorBigger3
	jmp	.LBB21_49
.LBB21_23:
	cmpl	fol_EQUIV(%rip), %eax
	jne	.LBB21_59
# BB#24:
	movq	16(%r13), %rax
	movq	8(%rax), %r15
	cmpq	%rbx, %r15
	jne	.LBB21_26
# BB#25:
	movq	(%rax), %rax
	movq	8(%rax), %r15
.LBB21_26:
	movq	%r13, %rdi
	callq	ren_Polarity
	cmpl	$-1, %eax
	je	.LBB21_36
# BB#27:
	cmpl	$1, %eax
	je	.LBB21_39
# BB#28:
	testl	%eax, %eax
	jne	.LBB21_59
# BB#29:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	xorl	%ebx, %ebx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bl
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorOk
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	ren_PFactorOk
	xorl	%ebp, %ebp
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bpl
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	cmpl	$1, %r12d
	sbbl	$-1, %ebx
	addl	%ebp, %ebx
	addl	%eax, %ebx
	movb	$1, %bpl
	cmpl	$1, %ebx
	ja	.LBB21_34
# BB#30:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB21_32
# BB#31:
	movq	%r15, %rdi
	movl	%ecx, %ebx
	callq	ren_PExtraFactorOk
	movl	%ebx, %ecx
	testl	%eax, %eax
	jne	.LBB21_34
.LBB21_32:
	testl	%ecx, %ecx
	je	.LBB21_51
# BB#33:
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	je	.LBB21_51
.LBB21_34:
	movzbl	%bpl, %r15d
	jmp	.LBB21_6
.LBB21_35:
	xorl	%eax, %eax
	jmp	.LBB21_50
.LBB21_36:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB21_45
# BB#37:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB21_50
# BB#38:
	movq	%r15, %rdi
	callq	ren_PFactorBigger3
	jmp	.LBB21_49
.LBB21_39:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB21_47
# BB#40:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB21_50
# BB#41:
	movq	%r15, %rdi
	callq	ren_NotPFactorBigger3
	jmp	.LBB21_49
.LBB21_42:
	movl	$1, %r15d
	jmp	.LBB21_6
.LBB21_43:
	testl	%r15d, %r15d
	jne	.LBB21_48
# BB#44:
	xorl	%eax, %eax
	jmp	.LBB21_50
.LBB21_45:
	testl	%ebp, %ebp
	je	.LBB21_55
# BB#46:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorBigger3
	jmp	.LBB21_49
.LBB21_47:
	testl	%ebp, %ebp
	je	.LBB21_55
.LBB21_48:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorBigger3
.LBB21_49:                              # %.thread113
	testl	%eax, %eax
	setne	%al
.LBB21_50:                              # %.thread113
	movzbl	%al, %r15d
	jmp	.LBB21_6
.LBB21_55:
	xorl	%eax, %eax
	jmp	.LBB21_50
.LBB21_51:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB21_56
# BB#52:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AExtraFactorOk
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	sete	%cl
	testl	%eax, %eax
	setne	%bpl
	jne	.LBB21_34
# BB#53:
	testb	%cl, %cl
	je	.LBB21_57
	jmp	.LBB21_34
.LBB21_56:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB21_58
.LBB21_57:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BExtraFactorOk
	testl	%eax, %eax
	setne	%bpl
	movzbl	%bpl, %r15d
	jmp	.LBB21_6
.LBB21_58:
	xorl	%ebp, %ebp
	jmp	.LBB21_34
.LBB21_59:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$705, %ecx              # imm = 0x2C1
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end21:
	.size	ren_AFactorBigger3, .Lfunc_end21-ren_AFactorBigger3
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_BFactorBigger3,@function
ren_BFactorBigger3:                     # @ren_BFactorBigger3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi178:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi179:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi180:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi182:
	.cfi_def_cfa_offset 80
.Lcfi183:
	.cfi_offset %rbx, -56
.Lcfi184:
	.cfi_offset %r12, -48
.Lcfi185:
	.cfi_offset %r13, -40
.Lcfi186:
	.cfi_offset %r14, -32
.Lcfi187:
	.cfi_offset %r15, -24
.Lcfi188:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	cmpq	%r13, %r14
	je	.LBB22_52
# BB#1:                                 # %.lr.ph142
	movl	fol_ALL(%rip), %eax
	movl	fol_EXIST(%rip), %ecx
	movl	fol_OR(%rip), %edx
	movl	fol_NOT(%rip), %esi
	movl	fol_AND(%rip), %edi
	movl	fol_IMPLIES(%rip), %r8d
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rbx
	movq	8(%rbx), %r13
	movl	(%r13), %ebp
	cmpl	%ebp, %eax
	je	.LBB22_5
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	cmpl	%ebp, %ecx
	je	.LBB22_5
# BB#4:                                 #   in Loop: Header=BB22_2 Depth=1
	cmpl	%edx, %ebp
	je	.LBB22_5
# BB#6:                                 #   in Loop: Header=BB22_2 Depth=1
	cmpl	%esi, %ebp
	je	.LBB22_53
# BB#7:                                 #   in Loop: Header=BB22_2 Depth=1
	cmpl	%edi, %ebp
	je	.LBB22_8
# BB#20:                                #   in Loop: Header=BB22_2 Depth=1
	cmpl	%r8d, %ebp
	jne	.LBB22_26
# BB#21:                                #   in Loop: Header=BB22_2 Depth=1
	movq	16(%r13), %rbp
	cmpq	%rbx, 8(%rbp)
	je	.LBB22_22
.LBB22_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB22_2 Depth=1
	cmpq	%r14, %r13
	jne	.LBB22_2
	jmp	.LBB22_52
.LBB22_53:
	movq	%r14, %rdi
	movq	%r13, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ren_AFactorBigger3      # TAILCALL
.LBB22_8:
	movq	16(%r13), %rbp
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	jne	.LBB22_10
	jmp	.LBB22_16
	.p2align	4, 0x90
.LBB22_15:                              #   in Loop: Header=BB22_10 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB22_16
.LBB22_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r12
	cmpq	%rbx, %r12
	je	.LBB22_15
# BB#11:                                #   in Loop: Header=BB22_10 Depth=1
	movq	%r12, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB22_15
# BB#12:                                #   in Loop: Header=BB22_10 Depth=1
	testl	%r15d, %r15d
	jne	.LBB22_13
# BB#14:                                #   in Loop: Header=BB22_10 Depth=1
	movq	%r12, %rdi
	callq	ren_NotPFactorBigger3
	movl	$1, %r15d
	testl	%eax, %eax
	je	.LBB22_15
	jmp	.LBB22_52
.LBB22_26:
	cmpl	fol_EQUIV(%rip), %ebp
	jne	.LBB22_54
# BB#27:
	movq	16(%r13), %rax
	movq	8(%rax), %r15
	cmpq	%rbx, %r15
	jne	.LBB22_29
# BB#28:
	movq	(%rax), %rax
	movq	8(%rax), %r15
.LBB22_29:
	movq	%r13, %rdi
	callq	ren_Polarity
	cmpl	$-1, %eax
	je	.LBB22_46
# BB#30:
	cmpl	$1, %eax
	je	.LBB22_23
# BB#31:
	testl	%eax, %eax
	jne	.LBB22_54
# BB#32:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	xorl	%ebx, %ebx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bl
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorOk
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	ren_PFactorOk
	xorl	%ebp, %ebp
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	setne	%bpl
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	cmpl	$1, %r12d
	sbbl	$-1, %ebx
	addl	%ebp, %ebx
	addl	%eax, %ebx
	movb	$1, %bpl
	cmpl	$1, %ebx
	ja	.LBB22_43
# BB#33:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB22_35
# BB#34:
	movq	%r15, %rdi
	movl	%ecx, %ebx
	callq	ren_PExtraFactorOk
	movl	%ebx, %ecx
	testl	%eax, %eax
	jne	.LBB22_43
.LBB22_35:
	testl	%ecx, %ecx
	je	.LBB22_37
# BB#36:
	movq	%r15, %rdi
	callq	ren_NotPExtraFactorOk
	testl	%eax, %eax
	je	.LBB22_37
.LBB22_43:
	movzbl	%bpl, %r15d
	jmp	.LBB22_52
.LBB22_22:
	movq	(%rbp), %rax
	movq	8(%rax), %r15
.LBB22_23:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_PFactorOk
	testl	%eax, %eax
	je	.LBB22_44
# BB#24:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB22_51
# BB#25:
	movq	%r15, %rdi
	callq	ren_PFactorBigger3
	jmp	.LBB22_19
.LBB22_16:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorOk
	testl	%eax, %eax
	je	.LBB22_50
# BB#17:
	movb	$1, %al
	testl	%r15d, %r15d
	jne	.LBB22_51
	jmp	.LBB22_18
.LBB22_44:
	testl	%ebp, %ebp
	je	.LBB22_50
# BB#45:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AFactorBigger3
	jmp	.LBB22_19
.LBB22_46:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorOk
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	ren_NotPFactorOk
	testl	%eax, %eax
	je	.LBB22_49
# BB#47:
	movb	$1, %al
	testl	%ebp, %ebp
	jne	.LBB22_51
# BB#48:
	movq	%r15, %rdi
	callq	ren_NotPFactorBigger3
	jmp	.LBB22_19
.LBB22_13:
	movl	$1, %r15d
	jmp	.LBB22_52
.LBB22_49:
	testl	%ebp, %ebp
	je	.LBB22_50
.LBB22_18:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BFactorBigger3
.LBB22_19:                              # %.thread113
	testl	%eax, %eax
	setne	%al
	jmp	.LBB22_51
.LBB22_50:
	xorl	%eax, %eax
.LBB22_51:                              # %.thread113
	movzbl	%al, %r15d
.LBB22_52:                              # %.loopexit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_37:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB22_41
# BB#38:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_AExtraFactorOk
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	sete	%cl
	testl	%eax, %eax
	setne	%bpl
	jne	.LBB22_43
# BB#39:
	testb	%cl, %cl
	je	.LBB22_40
	jmp	.LBB22_43
.LBB22_41:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB22_42
.LBB22_40:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	ren_BExtraFactorOk
	testl	%eax, %eax
	setne	%bpl
	movzbl	%bpl, %r15d
	jmp	.LBB22_52
.LBB22_42:
	xorl	%ebp, %ebp
	jmp	.LBB22_43
.LBB22_54:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	movl	$930, %ecx              # imm = 0x3A2
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end22:
	.size	ren_BFactorBigger3, .Lfunc_end22-ren_BFactorBigger3
	.cfi_endproc

	.p2align	4, 0x90
	.type	ren_RemoveAllSubterms,@function
ren_RemoveAllSubterms:                  # @ren_RemoveAllSubterms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi192:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 48
.Lcfi194:
	.cfi_offset %rbx, -48
.Lcfi195:
	.cfi_offset %r12, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	symbol_TYPEMASK(%rip), %r15d
	testq	%r14, %r14
	jne	.LBB23_2
	.p2align	4, 0x90
.LBB23_9:                               # %ren_RemoveTerm.exit
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %r14
	movl	(%r12), %eax
	testl	%eax, %eax
	jns	.LBB23_11
# BB#10:                                # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB23_9 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r15d, %ecx
	cmpl	$2, %ecx
	je	.LBB23_16
.LBB23_11:                              # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%r12), %rbx
	je	.LBB23_13
# BB#12:                                # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB23_15
.LBB23_13:                              #   in Loop: Header=BB23_9 Depth=1
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	testq	%r14, %r14
	je	.LBB23_9
.LBB23_2:                               # %.lr.ph.i
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	8(%rbx), %rdi
	cmpq	%r12, (%rbx)
	je	.LBB23_4
# BB#7:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	%r12, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 8(%rbx)
	jmp	.LBB23_8
	.p2align	4, 0x90
.LBB23_4:                               #   in Loop: Header=BB23_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB23_5
# BB#6:                                 #   in Loop: Header=BB23_3 Depth=1
	movl	$1, 16(%rbx)
	jmp	.LBB23_8
.LBB23_5:                               # %ren_Delete.exit.i
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	memory_ARRAY+192(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+192(%rip), %rax
	movq	%rbx, (%rax)
	movq	$0, 8(%rbp)
	.p2align	4, 0x90
.LBB23_8:                               #   in Loop: Header=BB23_3 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_3
	jmp	.LBB23_9
	.p2align	4, 0x90
.LBB23_14:                              # %.lr.ph
                                        #   in Loop: Header=BB23_15 Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	ren_RemoveAllSubterms
	movq	%rax, %r14
	movq	(%rbx), %rbx
.LBB23_15:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jne	.LBB23_14
.LBB23_16:                              # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	ren_RemoveAllSubterms, .Lfunc_end23-ren_RemoveAllSubterms
	.cfi_endproc

	.type	ren_STAMPID,@object     # @ren_STAMPID
	.local	ren_STAMPID
	.comm	ren_STAMPID,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\n\t Renaming term:"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\n\t Renamed term:"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\t Renaming:"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\t ========= \n"
	.size	.L.str.4, 15

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n\n\t Instances:"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n\t Polarity: %d\n"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n\t General : %d\n"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.8, 31

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/renaming.c"
	.size	.L.str.9, 80

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"In ren_GetRenamings: Unknown first-order operator."
	.size	.L.str.10, 51

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.11, 133

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"In ren_HasBenefit: Unknown polarity."
	.size	.L.str.12, 37

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"In ren_AFactorOk: Unknown first order operator."
	.size	.L.str.13, 48

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"In ren_BFactorOk: Unknown first order operator."
	.size	.L.str.14, 48

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"In ren_AExtraFactorOk: Unknown first order operator."
	.size	.L.str.15, 53

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"In ren_BExtraFactorOk: Unknown first order operator."
	.size	.L.str.16, 53

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n\n"
	.size	.L.str.17, 3

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"In ren_Polarity: Unknown first-order operator."
	.size	.L.str.18, 47

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"In ren_HasNonZeroBenefit: Unknown polarity."
	.size	.L.str.19, 44

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" \n In ren_PFactorBigger3: unknown first order operator."
	.size	.L.str.20, 56

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" \n In ren_NotPFactorBigger3: unknown first order operator."
	.size	.L.str.21, 59

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"In ren_AFactorBigger3: Unknown first order operator."
	.size	.L.str.22, 53

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"In ren_BFactorBigger3: Unknown first order operator."
	.size	.L.str.23, 53

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\n In ren_FormulaRename: Further match is no instance of hit.\n"
	.size	.L.str.24, 62


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
