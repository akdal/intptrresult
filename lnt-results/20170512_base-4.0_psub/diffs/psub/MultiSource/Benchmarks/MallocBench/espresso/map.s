	.text
	.file	"map.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	minterms
	.p2align	4, 0x90
	.type	minterms,@function
minterms:                               # @minterms
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	cube+4(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph21
	movq	cube+32(%rip), %rcx
	movl	$1, %ebx
	cmpl	$8, %eax
	jae	.LBB0_4
# BB#3:
	xorl	%edx, %edx
	jmp	.LBB0_14
.LBB0_1:
	movl	$1, %ebx
	movl	$8, %edi
	jmp	.LBB0_18
.LBB0_4:                                # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB0_5
# BB#6:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_7
# BB#8:                                 # %vector.body.prol.preheader
	negq	%rsi
	movdqa	.LCPI0_0(%rip), %xmm3   # xmm3 = [1,1,1,1]
	xorl	%edi, %edi
	movdqa	%xmm3, %xmm2
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rcx,%rdi,4), %xmm0
	movdqu	16(%rcx,%rdi,4), %xmm1
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addq	$8, %rdi
	incq	%rsi
	movdqa	%xmm0, %xmm3
	movdqa	%xmm1, %xmm2
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_5:
	xorl	%edx, %edx
	jmp	.LBB0_14
.LBB0_7:
	xorl	%edi, %edi
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movdqa	%xmm0, %xmm1
.LBB0_10:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB0_13
# BB#11:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	112(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-112(%rdi), %xmm3
	movdqu	-96(%rdi), %xmm4
	movdqu	-80(%rdi), %xmm5
	movdqu	-64(%rdi), %xmm2
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm6, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	pshufd	$245, %xmm4, %xmm0      # xmm0 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	pshufd	$245, %xmm5, %xmm0      # xmm0 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm1      # xmm1 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqu	-48(%rdi), %xmm0
	movdqu	-32(%rdi), %xmm3
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm5      # xmm5 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm5, %xmm4      # xmm4 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subq	$-128, %rdi
	addq	$-32, %rsi
	jne	.LBB0_12
.LBB0_13:                               # %middle.block
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	je	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	(%rcx,%rdx,4), %ebx
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB0_14
.LBB0_15:                               # %._crit_edge22
	cmpl	$33, %ebx
	jge	.LBB0_17
# BB#16:
	movl	$8, %edi
	jmp	.LBB0_18
.LBB0_17:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_18:                               # %._crit_edge22.thread
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, Gminterm(%rip)
	movq	24(%r14), %rcx
	movq	%rcx, Gcube(%rip)
	movslq	(%r14), %rsi
	movslq	12(%r14), %rdx
	imulq	%rsi, %rdx
	testl	%edx, %edx
	jle	.LBB0_22
# BB#19:                                # %.lr.ph.preheader
	leaq	(%rcx,%rdx,4), %rbx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	cube+4(%rip), %edi
	decl	%edi
	xorl	%esi, %esi
	callq	explode
	movslq	(%r14), %rax
	shlq	$2, %rax
	addq	Gcube(%rip), %rax
	movq	%rax, Gcube(%rip)
	cmpq	%rbx, %rax
	jb	.LBB0_20
# BB#21:                                # %._crit_edge.loopexit
	movq	Gminterm(%rip), %rax
.LBB0_22:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	minterms, .Lfunc_end0-minterms
	.cfi_endproc

	.globl	explode
	.p2align	4, 0x90
	.type	explode,@function
explode:                                # @explode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %r14d
	movq	cube+24(%rip), %rcx
	movslq	%r14d, %rax
	movl	(%rcx,%rax,4), %r15d
	movq	cube+16(%rip), %rcx
	movl	(%rcx,%rax,4), %ebp
	cmpl	%r15d, %ebp
	jg	.LBB1_10
# BB#1:                                 # %.lr.ph
	movq	cube+32(%rip), %rcx
	imull	(%rcx,%rax,4), %ebx
	testl	%r14d, %r14d
	je	.LBB1_6
# BB#2:
	decl	%r14d
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	Gcube(%rip), %rax
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rax,%rcx,4), %eax
	btl	%ebp, %eax
	jae	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	explode
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%ebx
	cmpl	%r15d, %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB1_3
	jmp	.LBB1_10
.LBB1_6:                                # %.lr.ph.split.us.preheader
	movq	Gcube(%rip), %rax
	movq	Gminterm(%rip), %rdx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rax,%rcx,4), %ecx
	btl	%ebp, %ecx
	jae	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	movl	$1, %esi
	movl	%ebx, %ecx
	shll	%cl, %esi
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rdx,%rcx,4)
.LBB1_9:                                #   in Loop: Header=BB1_7 Depth=1
	incl	%ebx
	cmpl	%r15d, %ebp
	leal	1(%rbp), %ecx
	movl	%ecx, %ebp
	jl	.LBB1_7
.LBB1_10:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	explode, .Lfunc_end1-explode
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	map
	.p2align	4, 0x90
	.type	map,@function
map:                                    # @map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 112
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	cube+4(%rip), %rax
	testq	%rax, %rax
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph21.i
	movq	cube+32(%rip), %rcx
	movl	$1, %ebx
	cmpl	$8, %eax
	jae	.LBB2_4
# BB#3:
	xorl	%edx, %edx
	jmp	.LBB2_14
.LBB2_1:
	movl	$1, %ebx
	movl	$8, %edi
	jmp	.LBB2_19
.LBB2_4:                                # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB2_5
# BB#6:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_7
# BB#8:                                 # %vector.body.prol.preheader
	negq	%rsi
	movdqa	.LCPI2_0(%rip), %xmm3   # xmm3 = [1,1,1,1]
	xorl	%edi, %edi
	movdqa	%xmm3, %xmm2
	.p2align	4, 0x90
.LBB2_9:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rcx,%rdi,4), %xmm0
	movdqu	16(%rcx,%rdi,4), %xmm1
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	addq	$8, %rdi
	incq	%rsi
	movdqa	%xmm0, %xmm3
	movdqa	%xmm1, %xmm2
	jne	.LBB2_9
	jmp	.LBB2_10
.LBB2_5:
	xorl	%edx, %edx
	jmp	.LBB2_14
.LBB2_7:
	xorl	%edi, %edi
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movdqa	%xmm0, %xmm1
.LBB2_10:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB2_13
# BB#11:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	112(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-112(%rdi), %xmm3
	movdqu	-96(%rdi), %xmm4
	movdqu	-80(%rdi), %xmm5
	movdqu	-64(%rdi), %xmm2
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm6, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	pshufd	$245, %xmm4, %xmm0      # xmm0 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	pshufd	$245, %xmm5, %xmm0      # xmm0 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm1      # xmm1 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqu	-48(%rdi), %xmm0
	movdqu	-32(%rdi), %xmm3
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm5      # xmm5 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	pshufd	$245, %xmm0, %xmm2      # xmm2 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm5, %xmm4      # xmm4 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subq	$-128, %rdi
	addq	$-32, %rsi
	jne	.LBB2_12
.LBB2_13:                               # %middle.block
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	je	.LBB2_16
.LBB2_14:                               # %scalar.ph.preheader
	subq	%rdx, %rax
	leaq	(%rcx,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB2_15:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	(%rcx), %ebx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB2_15
.LBB2_16:                               # %._crit_edge22.i
	cmpl	$33, %ebx
	jge	.LBB2_18
# BB#17:
	movl	$8, %edi
	jmp	.LBB2_19
.LBB2_18:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_19:                               # %._crit_edge22.thread.i
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, Gminterm(%rip)
	movq	24(%r14), %rsi
	movq	%rsi, Gcube(%rip)
	movslq	(%r14), %rdx
	movslq	12(%r14), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_23
# BB#20:                                # %.lr.ph.i.preheader
	leaq	(%rsi,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB2_21:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	cube+4(%rip), %edi
	decl	%edi
	xorl	%esi, %esi
	callq	explode
	movslq	(%r14), %rax
	shlq	$2, %rax
	addq	Gcube(%rip), %rax
	movq	%rax, Gcube(%rip)
	cmpq	%rbx, %rax
	jb	.LBB2_21
# BB#22:                                # %._crit_edge.loopexit.i
	movq	Gminterm(%rip), %rax
.LBB2_23:                               # %minterms.exit
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	cube+8(%rip), %ecx
	movl	$1, %ebp
	movl	%ecx, 20(%rsp)          # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movq	cube+32(%rip), %rax
	movslq	cube+4(%rip), %rcx
	movl	-4(%rax,%rcx,4), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB2_44
# BB#24:                                # %.lr.ph.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_25:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_26 Depth 2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_28 Depth 4
	movl	%edx, %eax
	movl	20(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%edx, %esi
	callq	printf
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_26:                               #   Parent Loop BB2_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_28 Depth 4
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%eax, %r14d
	shll	$8, %r14d
	movl	$mapindex, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_27:                               # %.preheader
                                        #   Parent Loop BB2_25 Depth=1
                                        #     Parent Loop BB2_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_28 Depth 4
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	$-16, %r12
	movq	%r13, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_25 Depth=1
                                        #     Parent Loop BB2_26 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%r13), %eax
	addl	%r14d, %eax
	cmpl	%ebp, %eax
	jge	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_28 Depth=4
	addl	28(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	4(%rdx,%rcx,4), %ecx
	btl	%eax, %ecx
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	46(%rax,%rax,2), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movl	$1, %ebx
.LBB2_30:                               #   in Loop: Header=BB2_28 Depth=4
	leaq	17(%r12), %r15
	testb	$3, %r15b
	jne	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_28 Depth=4
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB2_32:                               #   in Loop: Header=BB2_28 Depth=4
	testb	$7, %r15b
	jne	.LBB2_33
# BB#46:                                #   in Loop: Header=BB2_28 Depth=4
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_33:                               # %.backedge
                                        #   in Loop: Header=BB2_28 Depth=4
	addq	$4, %r13
	incq	%r12
	jne	.LBB2_28
# BB#34:                                #   in Loop: Header=BB2_27 Depth=3
	testl	%ebx, %ebx
	je	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_27 Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB2_36:                               #   in Loop: Header=BB2_27 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %rbx
	testb	$3, %bl
	movq	48(%rsp), %r13          # 8-byte Reload
	jne	.LBB2_40
# BB#37:                                #   in Loop: Header=BB2_27 Depth=3
	cmpq	$15, %rax
	je	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_27 Depth=3
	movq	%rbx, %rax
	shlq	$6, %rax
	cmpl	%ebp, mapindex(%rax)
	jge	.LBB2_42
.LBB2_39:                               #   in Loop: Header=BB2_27 Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB2_40:                               #   in Loop: Header=BB2_27 Depth=3
	testb	$7, %bl
	jne	.LBB2_41
# BB#47:                                #   in Loop: Header=BB2_27 Depth=3
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB2_41:                               # %.backedge59
                                        #   in Loop: Header=BB2_27 Depth=3
	addq	$64, %r13
	cmpq	$16, %rbx
	jl	.LBB2_27
.LBB2_42:                               #   in Loop: Header=BB2_26 Depth=2
	movl	cube+8(%rip), %eax
	addl	$-8, %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	%eax, %ecx
	leal	1(%rcx), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jl	.LBB2_26
# BB#43:                                #   in Loop: Header=BB2_25 Depth=1
	movl	24(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB2_25
.LBB2_44:                               # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$56, %rsp
	testq	%rdi, %rdi
	je	.LBB2_45
# BB#48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB2_45:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	map, .Lfunc_end2-map
	.cfi_endproc

	.type	Gminterm,@object        # @Gminterm
	.local	Gminterm
	.comm	Gminterm,8,8
	.type	Gcube,@object           # @Gcube
	.local	Gcube
	.comm	Gcube,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\nOutput space # %d\n"
	.size	.L.str, 21

	.type	mapindex,@object        # @mapindex
	.section	.rodata,"a",@progbits
	.p2align	4
mapindex:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	80                      # 0x50
	.long	81                      # 0x51
	.long	83                      # 0x53
	.long	82                      # 0x52
	.long	64                      # 0x40
	.long	65                      # 0x41
	.long	67                      # 0x43
	.long	66                      # 0x42
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	23                      # 0x17
	.long	22                      # 0x16
	.long	84                      # 0x54
	.long	85                      # 0x55
	.long	87                      # 0x57
	.long	86                      # 0x56
	.long	68                      # 0x44
	.long	69                      # 0x45
	.long	71                      # 0x47
	.long	70                      # 0x46
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	15                      # 0xf
	.long	14                      # 0xe
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	92                      # 0x5c
	.long	93                      # 0x5d
	.long	95                      # 0x5f
	.long	94                      # 0x5e
	.long	76                      # 0x4c
	.long	77                      # 0x4d
	.long	79                      # 0x4f
	.long	78                      # 0x4e
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	26                      # 0x1a
	.long	88                      # 0x58
	.long	89                      # 0x59
	.long	91                      # 0x5b
	.long	90                      # 0x5a
	.long	72                      # 0x48
	.long	73                      # 0x49
	.long	75                      # 0x4b
	.long	74                      # 0x4a
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	34                      # 0x22
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	51                      # 0x33
	.long	50                      # 0x32
	.long	112                     # 0x70
	.long	113                     # 0x71
	.long	115                     # 0x73
	.long	114                     # 0x72
	.long	96                      # 0x60
	.long	97                      # 0x61
	.long	99                      # 0x63
	.long	98                      # 0x62
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	39                      # 0x27
	.long	38                      # 0x26
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	55                      # 0x37
	.long	54                      # 0x36
	.long	116                     # 0x74
	.long	117                     # 0x75
	.long	119                     # 0x77
	.long	118                     # 0x76
	.long	100                     # 0x64
	.long	101                     # 0x65
	.long	103                     # 0x67
	.long	102                     # 0x66
	.long	44                      # 0x2c
	.long	45                      # 0x2d
	.long	47                      # 0x2f
	.long	46                      # 0x2e
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	63                      # 0x3f
	.long	62                      # 0x3e
	.long	124                     # 0x7c
	.long	125                     # 0x7d
	.long	127                     # 0x7f
	.long	126                     # 0x7e
	.long	108                     # 0x6c
	.long	109                     # 0x6d
	.long	111                     # 0x6f
	.long	110                     # 0x6e
	.long	40                      # 0x28
	.long	41                      # 0x29
	.long	43                      # 0x2b
	.long	42                      # 0x2a
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	59                      # 0x3b
	.long	58                      # 0x3a
	.long	120                     # 0x78
	.long	121                     # 0x79
	.long	123                     # 0x7b
	.long	122                     # 0x7a
	.long	104                     # 0x68
	.long	105                     # 0x69
	.long	107                     # 0x6b
	.long	106                     # 0x6a
	.long	160                     # 0xa0
	.long	161                     # 0xa1
	.long	163                     # 0xa3
	.long	162                     # 0xa2
	.long	176                     # 0xb0
	.long	177                     # 0xb1
	.long	179                     # 0xb3
	.long	178                     # 0xb2
	.long	240                     # 0xf0
	.long	241                     # 0xf1
	.long	243                     # 0xf3
	.long	242                     # 0xf2
	.long	224                     # 0xe0
	.long	225                     # 0xe1
	.long	227                     # 0xe3
	.long	226                     # 0xe2
	.long	164                     # 0xa4
	.long	165                     # 0xa5
	.long	167                     # 0xa7
	.long	166                     # 0xa6
	.long	180                     # 0xb4
	.long	181                     # 0xb5
	.long	183                     # 0xb7
	.long	182                     # 0xb6
	.long	244                     # 0xf4
	.long	245                     # 0xf5
	.long	247                     # 0xf7
	.long	246                     # 0xf6
	.long	228                     # 0xe4
	.long	229                     # 0xe5
	.long	231                     # 0xe7
	.long	230                     # 0xe6
	.long	172                     # 0xac
	.long	173                     # 0xad
	.long	175                     # 0xaf
	.long	174                     # 0xae
	.long	188                     # 0xbc
	.long	189                     # 0xbd
	.long	191                     # 0xbf
	.long	190                     # 0xbe
	.long	252                     # 0xfc
	.long	253                     # 0xfd
	.long	255                     # 0xff
	.long	254                     # 0xfe
	.long	236                     # 0xec
	.long	237                     # 0xed
	.long	239                     # 0xef
	.long	238                     # 0xee
	.long	168                     # 0xa8
	.long	169                     # 0xa9
	.long	171                     # 0xab
	.long	170                     # 0xaa
	.long	184                     # 0xb8
	.long	185                     # 0xb9
	.long	187                     # 0xbb
	.long	186                     # 0xba
	.long	248                     # 0xf8
	.long	249                     # 0xf9
	.long	251                     # 0xfb
	.long	250                     # 0xfa
	.long	232                     # 0xe8
	.long	233                     # 0xe9
	.long	235                     # 0xeb
	.long	234                     # 0xea
	.long	128                     # 0x80
	.long	129                     # 0x81
	.long	131                     # 0x83
	.long	130                     # 0x82
	.long	144                     # 0x90
	.long	145                     # 0x91
	.long	147                     # 0x93
	.long	146                     # 0x92
	.long	208                     # 0xd0
	.long	209                     # 0xd1
	.long	211                     # 0xd3
	.long	210                     # 0xd2
	.long	192                     # 0xc0
	.long	193                     # 0xc1
	.long	195                     # 0xc3
	.long	194                     # 0xc2
	.long	132                     # 0x84
	.long	133                     # 0x85
	.long	135                     # 0x87
	.long	134                     # 0x86
	.long	148                     # 0x94
	.long	149                     # 0x95
	.long	151                     # 0x97
	.long	150                     # 0x96
	.long	212                     # 0xd4
	.long	213                     # 0xd5
	.long	215                     # 0xd7
	.long	214                     # 0xd6
	.long	196                     # 0xc4
	.long	197                     # 0xc5
	.long	199                     # 0xc7
	.long	198                     # 0xc6
	.long	140                     # 0x8c
	.long	141                     # 0x8d
	.long	143                     # 0x8f
	.long	142                     # 0x8e
	.long	156                     # 0x9c
	.long	157                     # 0x9d
	.long	159                     # 0x9f
	.long	158                     # 0x9e
	.long	220                     # 0xdc
	.long	221                     # 0xdd
	.long	223                     # 0xdf
	.long	222                     # 0xde
	.long	204                     # 0xcc
	.long	205                     # 0xcd
	.long	207                     # 0xcf
	.long	206                     # 0xce
	.long	136                     # 0x88
	.long	137                     # 0x89
	.long	139                     # 0x8b
	.long	138                     # 0x8a
	.long	152                     # 0x98
	.long	153                     # 0x99
	.long	155                     # 0x9b
	.long	154                     # 0x9a
	.long	216                     # 0xd8
	.long	217                     # 0xd9
	.long	219                     # 0xdb
	.long	218                     # 0xda
	.long	200                     # 0xc8
	.long	201                     # 0xc9
	.long	203                     # 0xcb
	.long	202                     # 0xca
	.size	mapindex, 1024

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"  "
	.size	.L.str.1, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
