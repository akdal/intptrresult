	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, failure(%rip)
	movl	$0, lineno(%rip)
	callq	getargs
	callq	openfiles
	callq	reader
	callq	set_derives
	callq	set_nullable
	callq	generate_states
	callq	lalr
	callq	initialize_conflicts
	cmpl	$0, verboseflag(%rip)
	je	.LBB0_2
# BB#1:
	callq	verbose
	jmp	.LBB0_3
.LBB0_2:
	callq	terse
.LBB0_3:
	callq	output
	movl	failure(%rip), %edi
	callq	done
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	fatal
	.p2align	4, 0x90
	.type	fatal,@function
fatal:                                  # @fatal
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	movq	infile(%rip), %rdx
	movq	stderr(%rip), %rdi
	testq	%rdx, %rdx
	jne	.LBB1_2
# BB#1:
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r8, %rdx
	callq	fprintf
	jmp	.LBB1_3
.LBB1_2:
	movl	lineno(%rip), %ecx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB1_3:
	movl	$1, %edi
	popq	%rax
	jmp	done                    # TAILCALL
.Lfunc_end1:
	.size	fatal, .Lfunc_end1-fatal
	.cfi_endproc

	.globl	fatals
	.p2align	4, 0x90
	.type	fatals,@function
fatals:                                 # @fatals
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	subq	$200, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 224
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movl	%r9d, %r10d
	movl	%r8d, %r9d
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	224(%rsp), %r11d
	movl	232(%rsp), %r14d
	movl	240(%rsp), %ebx
	movq	%rsp, %rdi
	movl	$0, %eax
	pushq	%rbx
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	callq	sprintf
	addq	$32, %rsp
.Lcfi11:
	.cfi_adjust_cfa_offset -32
	movq	infile(%rip), %rdx
	movq	stderr(%rip), %rdi
	testq	%rdx, %rdx
	jne	.LBB2_2
# BB#1:
	movq	%rsp, %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_3
.LBB2_2:
	movl	lineno(%rip), %ecx
	movq	%rsp, %r8
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_3:                                # %fatal.exit
	movl	$1, %edi
	callq	done
	addq	$200, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	fatals, .Lfunc_end2-fatals
	.cfi_endproc

	.globl	toomany
	.p2align	4, 0x90
	.type	toomany,@function
toomany:                                # @toomany
	.cfi_startproc
# BB#0:
	subq	$200, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 208
	movq	%rdi, %rcx
	movq	%rsp, %rdi
	movl	$.L.str.2, %esi
	movl	$32767, %edx            # imm = 0x7FFF
	xorl	%eax, %eax
	callq	sprintf
	movq	infile(%rip), %rdx
	movq	stderr(%rip), %rdi
	testq	%rdx, %rdx
	jne	.LBB3_2
# BB#1:
	movq	%rsp, %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB3_3
.LBB3_2:
	movl	lineno(%rip), %ecx
	movq	%rsp, %r8
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB3_3:                                # %fatal.exit
	movl	$1, %edi
	callq	done
	addq	$200, %rsp
	retq
.Lfunc_end3:
	.size	toomany, .Lfunc_end3-toomany
	.cfi_endproc

	.globl	berror
	.p2align	4, 0x90
	.type	berror,@function
berror:                                 # @berror
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	callq	abort
.Lfunc_end4:
	.size	berror, .Lfunc_end4-berror
	.cfi_endproc

	.type	failure,@object         # @failure
	.comm	failure,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"fatal error: %s\n"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\"%s\", line %d: %s\n"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"limit of %d exceeded, too many %s"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"internal error, %s\n"
	.size	.L.str.3, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
