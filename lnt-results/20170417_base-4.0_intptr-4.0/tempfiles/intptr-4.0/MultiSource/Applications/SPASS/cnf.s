	.text
	.file	"cnf.bc"
	.globl	cnf_Init
	.p2align	4, 0x90
	.type	cnf_Init,@function
cnf_Init:                               # @cnf_Init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	cmpl	$0, 216(%rdi)
	je	.LBB0_1
# BB#2:
	movl	$8004, %edi             # imm = 0x1F44
	callq	memory_Malloc
	jmp	.LBB0_3
.LBB0_1:
	xorl	%eax, %eax
.LBB0_3:
	movq	%rax, cnf_VARIABLEDEPTHARRAY(%rip)
	callq	prfs_Create
	movq	%rax, cnf_SEARCHCOPY(%rip)
	callq	prfs_Create
	movq	%rax, cnf_HAVEPROOFPS(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	cnf_Init, .Lfunc_end0-cnf_Init
	.cfi_endproc

	.globl	cnf_Free
	.p2align	4, 0x90
	.type	cnf_Free,@function
cnf_Free:                               # @cnf_Free
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	cmpl	$0, 216(%rdi)
	je	.LBB1_6
# BB#1:
	movq	cnf_VARIABLEDEPTHARRAY(%rip), %rdi
	movl	memory_ALIGN(%rip), %ecx
	movl	$8004, %esi             # imm = 0x1F44
	movl	$8004, %eax             # imm = 0x1F44
	xorl	%edx, %edx
	divl	%ecx
	addl	$8004, %ecx             # imm = 0x1F44
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %r9
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r9, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_3
# BB#2:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rdx)
.LBB1_3:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_5
# BB#4:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_5:                                # %memory_Free.exit
	addq	$-16, %rdi
	callq	free
	movq	$0, cnf_VARIABLEDEPTHARRAY(%rip)
.LBB1_6:
	movq	cnf_SEARCHCOPY(%rip), %rdi
	callq	prfs_Delete
	movq	$0, cnf_SEARCHCOPY(%rip)
	movq	cnf_HAVEPROOFPS(%rip), %rdi
	callq	prfs_Delete
	movq	$0, cnf_HAVEPROOFPS(%rip)
	popq	%rax
	retq
.Lfunc_end1:
	.size	cnf_Free, .Lfunc_end1-cnf_Free
	.cfi_endproc

	.globl	cnf_ContainsDefinition
	.p2align	4, 0x90
	.type	cnf_ContainsDefinition,@function
cnf_ContainsDefinition:                 # @cnf_ContainsDefinition
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	movl	$1, %esi
	movq	%rax, %rdx
	jmp	cnf_ContainsDefinitionIntern # TAILCALL
.Lfunc_end2:
	.size	cnf_ContainsDefinition, .Lfunc_end2-cnf_ContainsDefinition
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_ContainsDefinitionIntern,@function
cnf_ContainsDefinitionIntern:           # @cnf_ContainsDefinitionIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movl	(%rbx), %edx
	movl	fol_AND(%rip), %ebp
	cmpl	%ebp, %edx
	sete	%al
	cmpl	$1, %r13d
	sete	%r12b
	jne	.LBB3_4
# BB#1:
	cmpl	%ebp, %edx
	jne	.LBB3_4
.LBB3_2:
	xorl	%eax, %eax
.LBB3_3:                                # %cnf_IsDefinition.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph122
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_29 Depth 2
                                        #     Child Loop BB3_6 Depth 2
                                        #     Child Loop BB3_16 Depth 2
	movl	%r13d, %r15d
	negl	%r15d
	cmpl	$-1, %r13d
	je	.LBB3_16
# BB#5:                                 # %.lr.ph122.split
                                        #   in Loop: Header=BB3_4 Depth=1
	testb	$1, %r12b
	je	.LBB3_27
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph122.split.split.us
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$1, %al
	jne	.LBB3_39
# BB#7:                                 # %.lr.ph122.split.split.us
                                        #   in Loop: Header=BB3_6 Depth=2
	cmpl	fol_OR(%rip), %edx
	je	.LBB3_39
# BB#8:                                 #   in Loop: Header=BB3_6 Depth=2
	cmpl	%edx, fol_ALL(%rip)
	je	.LBB3_14
# BB#9:                                 #   in Loop: Header=BB3_6 Depth=2
	cmpl	%edx, fol_EXIST(%rip)
	je	.LBB3_14
# BB#10:                                #   in Loop: Header=BB3_6 Depth=2
	cmpl	fol_NOT(%rip), %edx
	je	.LBB3_34
# BB#11:                                #   in Loop: Header=BB3_6 Depth=2
	cmpl	fol_IMPLIES(%rip), %edx
	jne	.LBB3_45
# BB#12:                                #   in Loop: Header=BB3_6 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	cnf_ContainsDefinitionIntern
	testl	%eax, %eax
	jne	.LBB3_98
# BB#13:                                # %.tailrecurse.backedge.us151_crit_edge
                                        #   in Loop: Header=BB3_6 Depth=2
	addq	$16, %rbx
	movl	fol_AND(%rip), %ebp
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_6 Depth=2
	addq	$16, %rbx
.LBB3_15:                               # %tailrecurse.backedge.us151
                                        #   in Loop: Header=BB3_6 Depth=2
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %edx
	xorl	%eax, %eax
	cmpl	%ebp, %edx
	jne	.LBB3_6
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph122.split.us
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	fol_OR(%rip), %edx
	je	.LBB3_2
# BB#17:                                #   in Loop: Header=BB3_16 Depth=2
	testb	$1, %al
	jne	.LBB3_38
# BB#18:                                #   in Loop: Header=BB3_16 Depth=2
	cmpl	%edx, fol_ALL(%rip)
	je	.LBB3_25
# BB#19:                                #   in Loop: Header=BB3_16 Depth=2
	cmpl	%edx, fol_EXIST(%rip)
	je	.LBB3_25
# BB#20:                                #   in Loop: Header=BB3_16 Depth=2
	cmpl	fol_NOT(%rip), %edx
	je	.LBB3_34
# BB#21:                                #   in Loop: Header=BB3_16 Depth=2
	cmpl	fol_IMPLIES(%rip), %edx
	jne	.LBB3_46
# BB#22:                                #   in Loop: Header=BB3_16 Depth=2
	testb	$1, %r12b
	je	.LBB3_2
# BB#23:                                #   in Loop: Header=BB3_16 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	cnf_ContainsDefinitionIntern
	testl	%eax, %eax
	jne	.LBB3_98
# BB#24:                                # %.tailrecurse.backedge.us_crit_edge
                                        #   in Loop: Header=BB3_16 Depth=2
	addq	$16, %rbx
	movl	fol_AND(%rip), %ebp
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_16 Depth=2
	addq	$16, %rbx
.LBB3_26:                               # %tailrecurse.backedge.us
                                        #   in Loop: Header=BB3_16 Depth=2
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %edx
	cmpl	%ebp, %edx
	sete	%al
	testb	%al, %r12b
	je	.LBB3_16
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph122.split.split.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	fol_OR(%rip), %esi
	cmpl	%esi, %edx
	sete	%cl
	orb	%cl, %al
	testb	$1, %al
	jne	.LBB3_39
# BB#28:                                #   in Loop: Header=BB3_4 Depth=1
	movl	fol_ALL(%rip), %eax
	movl	fol_EXIST(%rip), %edi
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph330
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%edx, %eax
	je	.LBB3_31
# BB#30:                                # %.lr.ph330
                                        #   in Loop: Header=BB3_29 Depth=2
	cmpl	%edx, %edi
	jne	.LBB3_33
.LBB3_31:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB3_29 Depth=2
	movq	16(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rbx
	movl	(%rbx), %edx
	cmpl	%ebp, %edx
	je	.LBB3_39
# BB#32:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB3_29 Depth=2
	cmpl	%esi, %edx
	jne	.LBB3_29
	jmp	.LBB3_39
.LBB3_33:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	fol_NOT(%rip), %edx
	jne	.LBB3_2
.LBB3_34:                               # %tailrecurse.outer
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %edx
	cmpl	%ebp, %edx
	sete	%al
	cmpl	$1, %r15d
	sete	%r12b
	movl	%r15d, %r13d
	jne	.LBB3_4
# BB#35:                                # %tailrecurse.outer
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpl	%ebp, %edx
	movl	%r15d, %r13d
	jne	.LBB3_4
	jmp	.LBB3_2
.LBB3_38:
	movl	$-1, %r13d
.LBB3_39:                               # %.us-lcssa.us
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_2
	.p2align	4, 0x90
.LBB3_40:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	cnf_ContainsDefinitionIntern
	testl	%eax, %eax
	jne	.LBB3_98
# BB#41:                                #   in Loop: Header=BB3_40 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_40
	jmp	.LBB3_2
.LBB3_45:
	movb	$1, %r12b
.LBB3_46:                               # %.us-lcssa131.us
	testb	$1, %r12b
	je	.LBB3_2
# BB#47:                                # %.us-lcssa131.us
	cmpl	fol_EQUIV(%rip), %edx
	jne	.LBB3_2
# BB#48:
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jns	.LBB3_50
# BB#49:                                # %symbol_IsPredicate.exit.i
	movl	%ecx, %edi
	negl	%edi
	movl	symbol_TYPEMASK(%rip), %edx
	andl	%edx, %edi
	cmpl	$2, %edi
	je	.LBB3_56
.LBB3_50:
	movq	(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	%rsi, 8(%rax)
	movq	16(%rbx), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB3_2
# BB#55:                                # %.symbol_IsPredicate.exit51.i_crit_edge
	movl	symbol_TYPEMASK(%rip), %edx
.LBB3_56:                               # %symbol_IsPredicate.exit51.i
	negl	%ecx
	andl	%edx, %ecx
	cmpl	$2, %ecx
	jne	.LBB3_2
# BB#57:
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	fol_FreeVariables
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	term_ListOfVariables
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_60
# BB#58:                                # %.lr.ph.i37.preheader
	movq	%r15, %rbp
.LBB3_59:                               # %.lr.ph.i37
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movl	$term_Equal, %edx
	movq	%r12, %rdi
	callq	list_DeleteElement
	movq	%rax, %r12
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_59
.LBB3_60:                               # %._crit_edge.i
	testq	%r12, %r12
	je	.LBB3_67
.LBB3_61:                               # %.lr.ph.i49.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB3_61
# BB#62:                                # %list_Delete.exit50.i
	testq	%r15, %r15
	je	.LBB3_2
.LBB3_63:                               # %.lr.ph.i44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB3_63
	jmp	.LBB3_2
.LBB3_67:
	testq	%r15, %r15
	je	.LBB3_69
.LBB3_68:                               # %.lr.ph.i.i38
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB3_68
.LBB3_69:                               # %cnf_IsDefinition.exit
	movq	16(%rbx), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	movl	$0, %eax
	je	.LBB3_3
# BB#70:                                # %.lr.ph115.preheader
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	callq	term_ListOfVariables
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r12
	xorl	%ebp, %ebp
.LBB3_71:                               # %.lr.ph115
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	%rbp, (%r15)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%r15, %rbp
	jne	.LBB3_71
# BB#72:                                # %.preheader71
	testq	%r15, %r15
	je	.LBB3_97
# BB#73:                                # %.lr.ph112.preheader
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebx
	movq	%r15, %rbp
	jmp	.LBB3_75
.LBB3_74:                               #   in Loop: Header=BB3_75 Depth=1
	negl	%ebx
	jmp	.LBB3_80
.LBB3_75:                               # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r12
	movslq	%ebx, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 8(%rbp)
	movl	(%rax), %ecx
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB3_77
# BB#76:                                #   in Loop: Header=BB3_75 Depth=1
	negl	%ebx
	jmp	.LBB3_80
.LBB3_77:                               #   in Loop: Header=BB3_75 Depth=1
	cmpl	fol_IMPLIES(%rip), %ecx
	jne	.LBB3_79
# BB#78:                                #   in Loop: Header=BB3_75 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rbp), %rdx
	cmpq	8(%rdx), %rcx
	je	.LBB3_74
.LBB3_79:                               #   in Loop: Header=BB3_75 Depth=1
	movl	(%rax), %eax
	cmpl	fol_EQUIV(%rip), %eax
	movl	$0, %eax
	cmovel	%eax, %ebx
.LBB3_80:                               #   in Loop: Header=BB3_75 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_75
# BB#81:                                # %.lr.ph108.preheader
	xorl	%r12d, %r12d
	movq	%r15, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB3_82:                               # %.lr.ph108
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_89 Depth 2
                                        #       Child Loop BB3_90 Depth 3
                                        #     Child Loop BB3_95 Depth 2
	testq	%rbp, %rbp
	je	.LBB3_97
# BB#83:                                #   in Loop: Header=BB3_82 Depth=1
	movq	8(%r13), %rcx
	movq	8(%rcx), %rax
	movl	(%rcx), %ecx
	movl	(%rax), %edx
	movl	fol_ALL(%rip), %esi
	cmpl	%edx, %esi
	movl	fol_EXIST(%rip), %edi
	je	.LBB3_85
# BB#84:                                #   in Loop: Header=BB3_82 Depth=1
	cmpl	%edx, %edi
	jne	.LBB3_96
.LBB3_85:                               #   in Loop: Header=BB3_82 Depth=1
	cmpl	%edx, %edi
	sete	%dil
	cmpl	%edx, %esi
	sete	%dl
	cmpl	$1, %ecx
	sete	%bl
	cmpl	$-1, %ecx
	sete	%cl
	testb	%dl, %bl
	jne	.LBB3_94
# BB#86:                                #   in Loop: Header=BB3_82 Depth=1
	andb	%dil, %cl
	jne	.LBB3_94
# BB#87:                                #   in Loop: Header=BB3_82 Depth=1
	testl	%r12d, %r12d
	jne	.LBB3_102
# BB#88:                                #   in Loop: Header=BB3_82 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.LBB3_96
.LBB3_89:                               # %.lr.ph97.split
                                        #   Parent Loop BB3_82 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_90 Depth 3
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rax), %rbx
	movq	%rbp, %r12
.LBB3_90:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_82 Depth=1
                                        #     Parent Loop BB3_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB3_99
# BB#91:                                #   in Loop: Header=BB3_90 Depth=3
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB3_90
# BB#92:                                # %.loopexit69
                                        #   in Loop: Header=BB3_89 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_89
# BB#93:                                #   in Loop: Header=BB3_82 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB3_96
.LBB3_94:                               #   in Loop: Header=BB3_82 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbx
	movl	$1, %r12d
	testq	%rbx, %rbx
	je	.LBB3_96
.LBB3_95:                               # %.lr.ph101
                                        #   Parent Loop BB3_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movl	$term_Equal, %edx
	movq	%rbp, %rdi
	callq	list_DeleteElement
	movq	%rax, %rbp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_95
.LBB3_96:                               # %.thread
                                        #   in Loop: Header=BB3_82 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB3_82
.LBB3_97:                               # %.critedge
	movl	$list_PairFree, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
.LBB3_98:                               # %cnf_IsDefinition.exit.thread
	movl	$1, %eax
	jmp	.LBB3_3
.LBB3_99:                               # %.lr.ph.i42
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB3_99
.LBB3_100:                              # %list_Delete.exit
	movl	$list_PairFree, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
	jmp	.LBB3_2
.LBB3_102:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB3_102
	jmp	.LBB3_100
.Lfunc_end3:
	.size	cnf_ContainsDefinitionIntern, .Lfunc_end3-cnf_ContainsDefinitionIntern
	.cfi_endproc

	.globl	cnf_ContainsPredicate
	.p2align	4, 0x90
	.type	cnf_ContainsPredicate,@function
cnf_ContainsPredicate:                  # @cnf_ContainsPredicate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movq	%r8, %r10
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%r9, (%rsp)
	movl	$1, %edx
	movq	%rax, %r8
	movq	%r10, %r9
	callq	cnf_ContainsPredicateIntern
	popq	%rcx
	retq
.Lfunc_end4:
	.size	cnf_ContainsPredicate, .Lfunc_end4-cnf_ContainsPredicate
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_ContainsPredicateIntern,@function
cnf_ContainsPredicateIntern:            # @cnf_ContainsPredicateIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 96
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%edx, %r13d
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	(%r14), %ebp
	movl	fol_AND(%rip), %edi
	cmpl	%edi, %ebp
	sete	%dl
	cmpl	$1, %r13d
	sete	15(%rsp)                # 1-byte Folded Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jne	.LBB5_11
# BB#1:
	cmpl	%edi, %ebp
	jne	.LBB5_11
.LBB5_2:                                # %tailrecurse.outer._crit_edge
	movq	%r14, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	callq	term_FindSubterm
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_39
# BB#3:
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB5_40
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
	movq	8(%rbp), %rdi
	callq	fol_FreeVariables
	movq	(%r15), %rcx
	testq	%rax, %rax
	je	.LBB5_9
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	testq	%rcx, %rcx
	je	.LBB5_10
# BB#6:                                 # %.preheader.i137.preheader
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader.i137
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_7
# BB#8:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, (%rdx)
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
.LBB5_10:                               # %list_Nconc.exit139
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rax, (%r15)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_4
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph211
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_31 Depth 2
                                        #     Child Loop BB5_21 Depth 2
	movl	%r13d, %eax
	negl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmpl	$-1, %r13d
	je	.LBB5_21
# BB#12:                                # %.lr.ph211.split
                                        #   in Loop: Header=BB5_11 Depth=1
	cmpl	$1, %r13d
	je	.LBB5_31
# BB#13:                                # %.lr.ph211.split.split.us
                                        #   in Loop: Header=BB5_11 Depth=1
	cmpl	fol_IMPLIES(%rip), %ebp
	je	.LBB5_2
# BB#14:                                # %.lr.ph211.split.split.us
                                        #   in Loop: Header=BB5_11 Depth=1
	cmpl	fol_EQUIV(%rip), %ebp
	je	.LBB5_2
# BB#15:                                #   in Loop: Header=BB5_11 Depth=1
	cmpl	fol_OR(%rip), %ebp
	sete	%cl
	orb	%cl, %dl
	testb	$1, %dl
	jne	.LBB5_55
# BB#16:                                #   in Loop: Header=BB5_11 Depth=1
	xorl	%r12d, %r12d
	cmpl	%ebp, fol_ALL(%rip)
	je	.LBB5_61
# BB#17:                                #   in Loop: Header=BB5_11 Depth=1
	cmpl	%ebp, fol_EXIST(%rip)
	je	.LBB5_61
# BB#18:                                #   in Loop: Header=BB5_11 Depth=1
	cmpl	fol_NOT(%rip), %ebp
	jne	.LBB5_44
.LBB5_19:                               # %tailrecurse.outer
                                        #   in Loop: Header=BB5_11 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %r14
	movl	(%r14), %ebp
	cmpl	%edi, %ebp
	sete	%dl
	movl	20(%rsp), %r13d         # 4-byte Reload
	cmpl	$1, %r13d
	sete	15(%rsp)                # 1-byte Folded Spill
	jne	.LBB5_11
# BB#20:                                # %tailrecurse.outer
                                        #   in Loop: Header=BB5_11 Depth=1
	cmpl	%edi, %ebp
	movl	20(%rsp), %r13d         # 4-byte Reload
	jne	.LBB5_11
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph211.split.us
                                        #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	fol_OR(%rip), %ebp
	je	.LBB5_2
# BB#22:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	$1, %r13d
	setne	%bl
	movl	fol_IMPLIES(%rip), %esi
	cmpl	%esi, %ebp
	sete	%cl
	testb	%cl, %bl
	jne	.LBB5_2
# BB#23:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	fol_EQUIV(%rip), %ebp
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB5_2
# BB#24:                                #   in Loop: Header=BB5_21 Depth=2
	testb	$1, %dl
	jne	.LBB5_53
# BB#25:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	%ebp, fol_ALL(%rip)
	movb	$1, %r12b
	je	.LBB5_60
# BB#26:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	%ebp, fol_EXIST(%rip)
	je	.LBB5_60
# BB#27:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	fol_NOT(%rip), %ebp
	je	.LBB5_19
# BB#28:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	%esi, %ebp
	jne	.LBB5_44
# BB#29:                                #   in Loop: Header=BB5_21 Depth=2
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	cnf_ContainsPredicateIntern
	testl	%eax, %eax
	jne	.LBB5_42
# BB#30:                                # %tailrecurse.us
                                        #   in Loop: Header=BB5_21 Depth=2
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	movl	(%r14), %ebp
	movl	fol_AND(%rip), %edi
	cmpl	%edi, %ebp
	sete	%dl
	testb	15(%rsp), %dl           # 1-byte Folded Reload
	je	.LBB5_21
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph211.split.split
                                        #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	fol_EQUIV(%rip), %ebp
	je	.LBB5_2
# BB#32:                                #   in Loop: Header=BB5_31 Depth=2
	cmpl	fol_OR(%rip), %ebp
	sete	%cl
	orb	%cl, %dl
	testb	$1, %dl
	jne	.LBB5_54
# BB#33:                                #   in Loop: Header=BB5_31 Depth=2
	xorl	%r12d, %r12d
	cmpl	%ebp, fol_ALL(%rip)
	movl	$1, %r13d
	je	.LBB5_61
# BB#34:                                #   in Loop: Header=BB5_31 Depth=2
	cmpl	%ebp, fol_EXIST(%rip)
	je	.LBB5_61
# BB#35:                                #   in Loop: Header=BB5_31 Depth=2
	cmpl	fol_NOT(%rip), %ebp
	je	.LBB5_19
# BB#36:                                #   in Loop: Header=BB5_31 Depth=2
	cmpl	fol_IMPLIES(%rip), %ebp
	jne	.LBB5_44
# BB#37:                                #   in Loop: Header=BB5_31 Depth=2
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	cnf_ContainsPredicateIntern
	testl	%eax, %eax
	jne	.LBB5_42
# BB#38:                                # %tailrecurse
                                        #   in Loop: Header=BB5_31 Depth=2
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	movl	(%r14), %ebp
	movl	fol_AND(%rip), %edi
	cmpl	%edi, %ebp
	sete	%dl
	testb	15(%rsp), %dl           # 1-byte Folded Reload
	je	.LBB5_31
	jmp	.LBB5_2
.LBB5_39:
	xorl	%eax, %eax
	jmp	.LBB5_43
.LBB5_40:                               # %.._crit_edge_crit_edge
	movq	(%r15), %rax
.LBB5_41:                               # %._crit_edge
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, (%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbx, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
.LBB5_42:                               # %.loopexit
	movl	$1, %eax
.LBB5_43:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_44:                               # %.us-lcssa220.us
	xorl	%eax, %eax
	cmpl	16(%rsp), %ebp          # 4-byte Folded Reload
	jne	.LBB5_43
# BB#45:
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB5_72
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph203
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_49 Depth 2
	movq	8(%rbp), %rdi
	callq	fol_FreeVariables
	movq	(%r15), %rcx
	testq	%rax, %rax
	je	.LBB5_51
# BB#47:                                #   in Loop: Header=BB5_46 Depth=1
	testq	%rcx, %rcx
	je	.LBB5_52
# BB#48:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB5_46 Depth=1
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB5_49:                               # %.preheader.i
                                        #   Parent Loop BB5_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_49
# BB#50:                                #   in Loop: Header=BB5_46 Depth=1
	movq	%rcx, (%rdx)
	jmp	.LBB5_52
	.p2align	4, 0x90
.LBB5_51:                               #   in Loop: Header=BB5_46 Depth=1
	movq	%rcx, %rax
.LBB5_52:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB5_46 Depth=1
	movq	%rax, (%r15)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_46
	jmp	.LBB5_73
.LBB5_53:
	movl	$-1, %r13d
	jmp	.LBB5_55
.LBB5_60:
	movl	$-1, %r13d
.LBB5_61:                               # %.us-lcssa218.us
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%r13d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	cnf_ContainsPredicateIntern
	testl	%eax, %eax
	je	.LBB5_71
# BB#62:
	movl	(%r14), %eax
	cmpl	fol_ALL(%rip), %eax
	sete	%cl
	testb	15(%rsp), %cl           # 1-byte Folded Reload
	jne	.LBB5_64
# BB#63:
	cmpl	fol_EXIST(%rip), %eax
	sete	%al
	andb	%al, %r12b
	movl	$1, %eax
	cmpb	$1, %r12b
	jne	.LBB5_43
.LBB5_64:                               # %.preheader
	movq	(%r15), %r12
	movl	$1, %eax
	testq	%r12, %r12
	je	.LBB5_43
	.p2align	4, 0x90
.LBB5_65:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_67 Depth 2
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB5_70
# BB#66:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB5_65 Depth=1
	movq	8(%r12), %rbx
	.p2align	4, 0x90
.LBB5_67:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB5_69
# BB#68:                                #   in Loop: Header=BB5_67 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_67
	jmp	.LBB5_70
	.p2align	4, 0x90
.LBB5_69:                               # %term_ListContainsTerm.exit
                                        #   in Loop: Header=BB5_65 Depth=1
	movq	8(%r12), %rbx
	movq	96(%rsp), %rax
	movq	%rax, %r15
	movq	(%r15), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%r15)
.LBB5_70:                               # %term_ListContainsTerm.exit.thread
                                        #   in Loop: Header=BB5_65 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB5_65
	jmp	.LBB5_42
.LBB5_54:
	movl	$1, %r13d
.LBB5_55:                               # %.us-lcssa.us
	movq	16(%r14), %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB5_43
	.p2align	4, 0x90
.LBB5_56:                               # %.lr.ph194
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%r13d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	cnf_ContainsPredicateIntern
	testl	%eax, %eax
	jne	.LBB5_42
# BB#57:                                #   in Loop: Header=BB5_56 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_56
# BB#58:
	xorl	%eax, %eax
	jmp	.LBB5_43
.LBB5_71:
	xorl	%eax, %eax
	jmp	.LBB5_43
.LBB5_72:                               # %.._crit_edge204_crit_edge
	movq	(%r15), %rax
.LBB5_73:                               # %._crit_edge204
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, (%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
	movq	%r14, (%rbx)
	jmp	.LBB5_42
.Lfunc_end5:
	.size	cnf_ContainsPredicateIntern, .Lfunc_end5-cnf_ContainsPredicateIntern
	.cfi_endproc

	.globl	cnf_ApplyDefinitionOnce
	.p2align	4, 0x90
	.type	cnf_ApplyDefinitionOnce,@function
cnf_ApplyDefinitionOnce:                # @cnf_ApplyDefinitionOnce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%rbx, %rdi
	callq	term_MaxVar
	movl	%eax, %ebp
	movq	%r12, %rdi
	callq	term_MaxVar
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	movl	%ebp, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r12, %rdi
	callq	fol_BoundVariables
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_6
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movq	%rbx, %rdi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movl	symbol_STANDARDVARCOUNTER(%rip), %edx
	incl	%edx
	movl	%edx, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r12, %rdi
	callq	term_ExchangeVariable
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB6_2
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB6_5
.LBB6_6:                                # %list_Delete.exit
	leaq	12(%rsp), %r8
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	cnf_ApplyDefinitionInternOnce
	movq	%rax, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 148(%rax)
	je	.LBB6_9
# BB#7:                                 # %list_Delete.exit
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB6_9
# BB#8:
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fol_PrettyPrint
	movl	$.L.str.1, %edi
	callq	puts
.LBB6_9:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	cnf_ApplyDefinitionOnce, .Lfunc_end6-cnf_ApplyDefinitionOnce
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_ApplyDefinitionInternOnce,@function
cnf_ApplyDefinitionInternOnce:          # @cnf_ApplyDefinitionInternOnce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%r12), %eax
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB7_29
# BB#1:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB7_29
# BB#2:
	cmpl	%eax, fol_AND(%rip)
	je	.LBB7_30
# BB#3:
	cmpl	%eax, fol_OR(%rip)
	je	.LBB7_30
# BB#4:
	cmpl	%eax, fol_NOT(%rip)
	je	.LBB7_30
# BB#5:
	cmpl	%eax, fol_IMPLIED(%rip)
	je	.LBB7_30
# BB#6:
	cmpl	%eax, fol_VARLIST(%rip)
	je	.LBB7_30
# BB#7:
	cmpl	%eax, fol_IMPLIES(%rip)
	je	.LBB7_30
# BB#8:
	cmpl	%eax, fol_EQUIV(%rip)
	je	.LBB7_30
# BB#9:
	cmpq	%r13, %r12
	jne	.LBB7_35
# BB#10:
	cmpl	(%rbx), %eax
	jne	.LBB7_35
# BB#11:
	movq	16(%rbx), %rbx
	movq	16(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	vec_MAX(%rip), %r13
	leal	1(%r13), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%r14, vec_VECTOR(,%r13,8)
	testq	%rbx, %rbx
	je	.LBB7_22
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph53.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_19 Depth 2
                                        #     Child Loop BB7_14 Depth 2
	movslq	%eax, %rsi
	leal	-1(%rsi), %ecx
	movl	%ecx, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rsi,8), %rbp
	movl	(%rbp), %edx
	testl	%edx, %edx
	jle	.LBB7_17
# BB#13:                                # %.lr.ph52.us.i.preheader
                                        #   in Loop: Header=BB7_12 Depth=1
	movq	8(%rbx), %rax
	cmpl	(%rax), %edx
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, %rsi
	je	.LBB7_16
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph69
                                        #   Parent Loop BB7_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB7_21
# BB#15:                                # %..lr.ph52.us_crit_edge.i
                                        #   in Loop: Header=BB7_14 Depth=2
	movq	(%rax), %rax
	movq	8(%rsi), %rdi
	cmpl	(%rdi), %edx
	jne	.LBB7_14
.LBB7_16:                               # %.thread.i
                                        #   in Loop: Header=BB7_12 Depth=1
	movq	8(%rax), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rbp)
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rbp)
	movl	vec_MAX(%rip), %ecx
	jmp	.LBB7_21
	.p2align	4, 0x90
.LBB7_17:                               #   in Loop: Header=BB7_12 Depth=1
	movq	16(%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB7_21
# BB#18:                                # %.lr.ph.us.i.preheader
                                        #   in Loop: Header=BB7_12 Depth=1
	decq	%rsi
	leaq	vec_VECTOR(,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.us.i
                                        #   Parent Loop BB7_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdx
	addq	$8, %rcx
	incl	%eax
	testq	%rdx, %rdx
	jne	.LBB7_19
# BB#20:                                # %.critedge.backedge.us.i.loopexit82
                                        #   in Loop: Header=BB7_12 Depth=1
	decl	%eax
	movl	%eax, %ecx
.LBB7_21:                               # %.critedge.backedge.us.i
                                        #   in Loop: Header=BB7_12 Depth=1
	cmpl	%ecx, %r13d
	movl	%ecx, %eax
	jne	.LBB7_12
	jmp	.LBB7_28
	.p2align	4, 0x90
.LBB7_22:                               # %.lr.ph53.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_25 Depth 2
	movslq	%eax, %rsi
	leal	-1(%rsi), %edx
	movl	%edx, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rsi,8), %rcx
	cmpl	$0, (%rcx)
	jg	.LBB7_27
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_27
# BB#24:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_22 Depth=1
	decq	%rsi
	leaq	vec_VECTOR(,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph.i
                                        #   Parent Loop BB7_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	addq	$8, %rdx
	incl	%eax
	testq	%rcx, %rcx
	jne	.LBB7_25
# BB#26:                                # %.critedge.backedge.i.loopexit
                                        #   in Loop: Header=BB7_22 Depth=1
	decl	%eax
	movl	%eax, %edx
.LBB7_27:                               # %.critedge.backedge.i
                                        #   in Loop: Header=BB7_22 Depth=1
	cmpl	%edx, %r13d
	movl	%edx, %eax
	jne	.LBB7_22
.LBB7_28:                               # %cnf_RplacVar.exit
	movl	%r13d, vec_MAX(%rip)
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	movq	%r12, %rdi
	callq	term_Delete
	movl	$1, (%r15)
	jmp	.LBB7_36
.LBB7_29:
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	cnf_ApplyDefinitionInternOnce
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%r12, 8(%rax)
	movq	%r12, %rax
	jmp	.LBB7_37
.LBB7_30:                               # %fol_IsJunctor.exit.thread
	movq	16(%r12), %rbp
	testq	%rbp, %rbp
	je	.LBB7_35
	.p2align	4, 0x90
.LBB7_31:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	cnf_ApplyDefinitionInternOnce
	movq	%rax, 8(%rbp)
	movq	%r12, 8(%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB7_31
.LBB7_35:
	movq	%r12, %r14
.LBB7_36:                               # %.loopexit
	movq	%r14, %rax
.LBB7_37:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cnf_ApplyDefinitionInternOnce, .Lfunc_end7-cnf_ApplyDefinitionInternOnce
	.cfi_endproc

	.globl	cnf_NegationNormalFormula
	.p2align	4, 0x90
	.type	cnf_NegationNormalFormula,@function
cnf_NegationNormalFormula:              # @cnf_NegationNormalFormula
	.cfi_startproc
# BB#0:                                 # %.lr.ph114.preheader
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 80
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movslq	vec_MAX(%rip), %r15
	leal	1(%r15), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, vec_VECTOR(,%r15,8)
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph114
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_16 Depth 2
                                        #     Child Loop BB8_19 Depth 2
                                        #     Child Loop BB8_11 Depth 2
                                        #     Child Loop BB8_4 Depth 2
                                        #     Child Loop BB8_6 Depth 2
                                        #     Child Loop BB8_23 Depth 2
	leal	-1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	vec_VECTOR-8(,%rax,8), %rbx
	movl	(%rbx), %ebp
	cmpl	fol_NOT(%rip), %ebp
	jne	.LBB8_21
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %r12
	movl	(%r12), %ecx
	cmpl	%ebp, %ecx
	jne	.LBB8_8
# BB#3:                                 #   in Loop: Header=BB8_1 Depth=1
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rbx)
	testq	%rax, %rax
	je	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph.i98
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB8_4
.LBB8_5:                                # %list_Delete.exit99
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB8_7
	.p2align	4, 0x90
.LBB8_6:                                # %.lr.ph.i93
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB8_6
.LBB8_7:                                # %list_Delete.exit94
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r12, (%rax)
	movslq	vec_MAX(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	movq	%rbx, vec_VECTOR(,%rax,8)
	jmp	.LBB8_21
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_1 Depth=1
	movl	fol_ALL(%rip), %edx
	cmpl	%ecx, %edx
	movl	fol_EXIST(%rip), %r13d
	je	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_1 Depth=1
	cmpl	%ecx, %r13d
	je	.LBB8_10
# BB#12:                                #   in Loop: Header=BB8_1 Depth=1
	movl	fol_OR(%rip), %edx
	cmpl	%edx, %ecx
	movl	fol_AND(%rip), %esi
	je	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_1 Depth=1
	cmpl	%esi, %ecx
	jne	.LBB8_21
.LBB8_14:                               # %cnf_GetDualSymbol.exit
                                        #   in Loop: Header=BB8_1 Depth=1
	cmpl	%esi, %ecx
	movl	%ecx, %r13d
	cmovel	%edx, %r13d
	cmpl	%edx, %ecx
	cmovel	%esi, %r13d
	movq	16(%r12), %r14
	testq	%r14, %r14
	jne	.LBB8_16
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB8_16 Depth=2
	movl	fol_NOT(%rip), %ebp
.LBB8_16:                               # %.lr.ph..lr.ph_crit_edge
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 8(%r14)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB8_15
# BB#17:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	16(%rbx), %rax
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB8_18:                               # %._crit_edge
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	%r13d, (%rbx)
	testq	%rax, %rax
	je	.LBB8_20
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph.i
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB8_19
	jmp	.LBB8_20
.LBB8_10:                               # %cnf_GetDualSymbol.exit89
                                        #   in Loop: Header=BB8_1 Depth=1
	cmpl	%ecx, %edx
	cmovnel	%edx, %r13d
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	movl	%r13d, (%rbx)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_20
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph.i85
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB8_11
.LBB8_20:                               # %list_Delete.exit
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	16(%r12), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rbx)
	movq	%r12, %rdi
	callq	term_Delete
	.p2align	4, 0x90
.LBB8_21:                               # %.lr.ph114._crit_edge
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	movl	vec_MAX(%rip), %eax
	je	.LBB8_24
# BB#22:                                #   in Loop: Header=BB8_1 Depth=1
	cltq
	.p2align	4, 0x90
.LBB8_23:                               # %.lr.ph112
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	%rdx, vec_VECTOR(,%rax,8)
	incq	%rax
	movl	%eax, vec_MAX(%rip)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_23
.LBB8_24:                               # %.backedge
                                        #   in Loop: Header=BB8_1 Depth=1
	cmpl	%eax, %r15d
	jne	.LBB8_1
# BB#25:                                # %._crit_edge115
	movl	%r15d, vec_MAX(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	cnf_NegationNormalFormula, .Lfunc_end8-cnf_NegationNormalFormula
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_Equal,@function
symbol_Equal:                           # @symbol_Equal
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	sete	%al
	retq
.Lfunc_end9:
	.size	symbol_Equal, .Lfunc_end9-symbol_Equal
	.cfi_endproc

	.globl	cnf_ComputeLiteralLists
	.p2align	4, 0x90
	.type	cnf_ComputeLiteralLists,@function
cnf_ComputeLiteralLists:                # @cnf_ComputeLiteralLists
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 80
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	fol_OR(%rip), %eax
	jne	.LBB10_29
# BB#1:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB10_2
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph107
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_7 Depth 2
                                        #       Child Loop BB10_8 Depth 3
                                        #         Child Loop BB10_18 Depth 4
                                        #     Child Loop BB10_14 Depth 2
                                        #     Child Loop BB10_25 Depth 2
                                        #     Child Loop BB10_28 Depth 2
	movq	8(%rbx), %rdi
	callq	cnf_ComputeLiteralLists
	testq	%rax, %rax
	je	.LBB10_4
# BB#5:                                 # %.preheader.lr.ph
                                        #   in Loop: Header=BB10_3 Depth=1
	testq	%r14, %r14
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB10_14
# BB#6:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	xorl	%r13d, %r13d
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB10_7:                               # %.preheader
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_8 Depth 3
                                        #         Child Loop BB10_18 Depth 4
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB10_8:                               #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_18 Depth 4
	movq	8(%rbp), %rbx
	movq	8(%r15), %r12
	cmpq	$0, (%r15)
	je	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_8 Depth=3
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_CopyWithElement
	movq	%rax, %rbx
.LBB10_10:                              #   in Loop: Header=BB10_8 Depth=3
	cmpq	$0, (%rbp)
	je	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_8 Depth=3
	movl	$term_Copy, %esi
	movq	%r12, %rdi
	callq	list_CopyWithElement
	movq	%rax, %r12
.LBB10_12:                              #   in Loop: Header=BB10_8 Depth=3
	testq	%rbx, %rbx
	je	.LBB10_13
# BB#16:                                #   in Loop: Header=BB10_8 Depth=3
	testq	%r12, %r12
	je	.LBB10_20
# BB#17:                                # %.preheader.i83.preheader
                                        #   in Loop: Header=BB10_8 Depth=3
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB10_18:                              # %.preheader.i83
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_7 Depth=2
                                        #       Parent Loop BB10_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_18
# BB#19:                                #   in Loop: Header=BB10_8 Depth=3
	movq	%r12, (%rax)
	jmp	.LBB10_20
	.p2align	4, 0x90
.LBB10_13:                              #   in Loop: Header=BB10_8 Depth=3
	movq	%r12, %rbx
.LBB10_20:                              # %list_Nconc.exit85
                                        #   in Loop: Header=BB10_8 Depth=3
	movl	$term_Equal, %esi
	movl	$term_Delete, %edx
	movq	%rbx, %rdi
	callq	list_DeleteDuplicatesFree
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	%r13, (%r12)
	movq	(%r15), %r15
	testq	%r15, %r15
	movq	%r12, %r13
	jne	.LBB10_8
# BB#21:                                # %._crit_edge
                                        #   in Loop: Header=BB10_7 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r12, %r13
	jne	.LBB10_7
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_14:                              # %.preheader.us
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB10_14
# BB#15:                                #   in Loop: Header=BB10_3 Depth=1
	xorl	%r12d, %r12d
.LBB10_22:                              # %._crit_edge98
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB10_23
# BB#24:                                # %.lr.ph.i78.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB10_25:                              # %.lr.ph.i78
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB10_25
	jmp	.LBB10_26
	.p2align	4, 0x90
.LBB10_4:                               #   in Loop: Header=BB10_3 Depth=1
	xorl	%r12d, %r12d
	testq	%r14, %r14
	jne	.LBB10_28
	jmp	.LBB10_27
	.p2align	4, 0x90
.LBB10_23:                              #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB10_26:                              # %list_Delete.exit79
                                        #   in Loop: Header=BB10_3 Depth=1
	testq	%r14, %r14
	je	.LBB10_27
	.p2align	4, 0x90
.LBB10_28:                              # %.lr.ph.i
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB10_28
.LBB10_27:                              # %list_Delete.exit.backedge
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%r12, %r14
	jne	.LBB10_3
	jmp	.LBB10_42
.LBB10_29:
	cmpl	fol_AND(%rip), %eax
	jne	.LBB10_38
# BB#30:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	cnf_ComputeLiteralLists
	movq	16(%rbx), %rbx
	jmp	.LBB10_31
.LBB10_38:
	cmpl	fol_NOT(%rip), %eax
	je	.LBB10_41
# BB#39:
	testl	%eax, %eax
	jns	.LBB10_43
# BB#40:                                # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB10_43
.LBB10_41:
	movq	%rbx, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	$0, (%r12)
	jmp	.LBB10_42
.LBB10_2:
	movq	%r14, %r12
	jmp	.LBB10_42
.LBB10_37:                              #   in Loop: Header=BB10_31 Depth=1
	movq	%r12, (%rcx)
.LBB10_31:                              # %list_Nconc.exit.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_32 Depth 2
                                        #     Child Loop BB10_36 Depth 2
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB10_32:                              # %list_Nconc.exit
                                        #   Parent Loop BB10_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB10_42
# BB#33:                                #   in Loop: Header=BB10_32 Depth=2
	movq	8(%rbx), %rdi
	callq	cnf_ComputeLiteralLists
	testq	%rax, %rax
	je	.LBB10_32
# BB#34:                                #   in Loop: Header=BB10_31 Depth=1
	testq	%r12, %r12
	je	.LBB10_31
# BB#35:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB10_31 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB10_36:                              # %.preheader.i
                                        #   Parent Loop BB10_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB10_36
	jmp	.LBB10_37
.LBB10_42:                              # %.loopexit
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_43:                              # %symbol_IsPredicate.exit.thread
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	movl	$1306, %ecx             # imm = 0x51A
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end10:
	.size	cnf_ComputeLiteralLists, .Lfunc_end10-cnf_ComputeLiteralLists
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end11:
	.size	misc_DumpCore, .Lfunc_end11-misc_DumpCore
	.cfi_endproc

	.globl	cnf_FPrintClause
	.p2align	4, 0x90
	.type	cnf_FPrintClause,@function
cnf_FPrintClause:                       # @cnf_FPrintClause
	.cfi_startproc
# BB#0:                                 # %.lr.ph20.preheader
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movslq	vec_MAX(%rip), %rbx
	leal	1(%rbx), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rdi, vec_VECTOR(,%rbx,8)
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_4 Depth 2
	leal	-1(%rax), %edx
	movl	%edx, vec_MAX(%rip)
	movslq	%eax, %rcx
	movq	vec_VECTOR-8(,%rcx,8), %rsi
	movl	(%rsi), %ecx
	cmpl	fol_OR(%rip), %ecx
	jne	.LBB12_8
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB12_6
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_1 Depth=1
	movslq	%edx, %rdx
	leaq	vec_VECTOR(,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB12_4:                               # %.lr.ph
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	incl	%eax
	addq	$8, %rdx
	testq	%rcx, %rcx
	jne	.LBB12_4
# BB#5:                                 # %.backedge.loopexit
                                        #   in Loop: Header=BB12_1 Depth=1
	decl	%eax
	movl	%eax, %edx
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_1 Depth=1
	movq	%r14, %rdi
	callq	term_FPrint
	movl	vec_MAX(%rip), %edx
.LBB12_6:                               # %.backedge
                                        #   in Loop: Header=BB12_1 Depth=1
	cmpl	%edx, %ebx
	movl	%edx, %eax
	jne	.LBB12_1
# BB#7:                                 # %._crit_edge
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	%ebx, vec_MAX(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	cnf_FPrintClause, .Lfunc_end12-cnf_FPrintClause
	.cfi_endproc

	.globl	cnf_FPrint
	.p2align	4, 0x90
	.type	cnf_FPrint,@function
cnf_FPrint:                             # @cnf_FPrint
	.cfi_startproc
# BB#0:                                 # %.lr.ph24
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movslq	vec_MAX(%rip), %r14
	leal	1(%r14), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rdi, vec_VECTOR(,%r14,8)
	movl	symbol_TYPEMASK(%rip), %r15d
	.p2align	4, 0x90
.LBB13_1:                               # %.backedge._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
                                        #       Child Loop BB13_14 Depth 3
                                        #     Child Loop BB13_4 Depth 2
	leal	-1(%rax), %ebp
	movl	%ebp, vec_MAX(%rip)
	movslq	%eax, %rcx
	movq	vec_VECTOR-8(,%rcx,8), %rcx
	movl	(%rcx), %edx
	cmpl	fol_AND(%rip), %edx
	jne	.LBB13_6
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_19
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB13_1 Depth=1
	movslq	%ebp, %rdx
	leaq	vec_VECTOR(,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB13_4:                               # %.lr.ph
                                        #   Parent Loop BB13_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	incl	%eax
	addq	$8, %rdx
	testq	%rcx, %rcx
	jne	.LBB13_4
# BB#5:                                 # %.backedge.loopexit
                                        #   in Loop: Header=BB13_1 Depth=1
	decl	%eax
	movl	%eax, %ebp
	jmp	.LBB13_19
	.p2align	4, 0x90
.LBB13_6:                               #   in Loop: Header=BB13_1 Depth=1
	movl	fol_OR(%rip), %ecx
	cmpl	%ecx, %edx
	je	.LBB13_10
# BB#7:                                 #   in Loop: Header=BB13_1 Depth=1
	testl	%edx, %edx
	jns	.LBB13_9
# BB#8:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB13_1 Depth=1
	movl	%edx, %esi
	negl	%esi
	andl	%r15d, %esi
	cmpl	$2, %esi
	je	.LBB13_10
.LBB13_9:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB13_1 Depth=1
	cmpl	fol_NOT(%rip), %edx
	jne	.LBB13_19
.LBB13_10:                              #   in Loop: Header=BB13_1 Depth=1
	movl	%eax, vec_MAX(%rip)
	jmp	.LBB13_11
	.p2align	4, 0x90
.LBB13_17:                              # %.backedge.i..lr.ph20.i_crit_edge
                                        #   in Loop: Header=BB13_11 Depth=2
	movl	fol_OR(%rip), %ecx
	movl	%edx, %eax
.LBB13_11:                              # %.lr.ph20.i
                                        #   Parent Loop BB13_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_14 Depth 3
	movslq	%eax, %rdi
	leal	-1(%rdi), %edx
	movl	%edx, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rdi,8), %rsi
	cmpl	%ecx, (%rsi)
	jne	.LBB13_21
# BB#12:                                #   in Loop: Header=BB13_11 Depth=2
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB13_16
# BB#13:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB13_11 Depth=2
	decq	%rdi
	leaq	vec_VECTOR(,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB13_14:                              # %.lr.ph.i
                                        #   Parent Loop BB13_1 Depth=1
                                        #     Parent Loop BB13_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	addq	$8, %rdx
	incl	%eax
	testq	%rcx, %rcx
	jne	.LBB13_14
# BB#15:                                # %.backedge.i.loopexit
                                        #   in Loop: Header=BB13_11 Depth=2
	decl	%eax
	movl	%eax, %edx
	cmpl	%edx, %ebp
	jne	.LBB13_17
	jmp	.LBB13_18
	.p2align	4, 0x90
.LBB13_21:                              #   in Loop: Header=BB13_11 Depth=2
	movq	%rbx, %rdi
	callq	term_FPrint
	movl	vec_MAX(%rip), %edx
.LBB13_16:                              # %.backedge.i
                                        #   in Loop: Header=BB13_11 Depth=2
	cmpl	%edx, %ebp
	jne	.LBB13_17
.LBB13_18:                              # %cnf_FPrintClause.exit
                                        #   in Loop: Header=BB13_1 Depth=1
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	%ebp, vec_MAX(%rip)
.LBB13_19:                              # %.backedge
                                        #   in Loop: Header=BB13_1 Depth=1
	cmpl	%ebp, %r14d
	movl	%ebp, %eax
	jne	.LBB13_1
# BB#20:                                # %._crit_edge
	movl	%r14d, vec_MAX(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	cnf_FPrint, .Lfunc_end13-cnf_FPrint
	.cfi_endproc

	.globl	cnf_StdoutPrint
	.p2align	4, 0x90
	.type	cnf_StdoutPrint,@function
cnf_StdoutPrint:                        # @cnf_StdoutPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB14_11
# BB#1:                                 # %.lr.ph35
	movl	symbol_TYPEMASK(%rip), %r14d
	.p2align	4, 0x90
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_8 Depth 2
	movq	8(%rbx), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB14_4
# BB#3:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r14d, %ecx
	cmpl	$2, %ecx
	je	.LBB14_9
.LBB14_4:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rdi), %rbp
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB14_7
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB14_7
# BB#6:                                 # %fol_IsNegativeLiteral.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	negl	%eax
	andl	%r14d, %eax
	cmpl	$2, %eax
	je	.LBB14_9
	.p2align	4, 0x90
.LBB14_7:                               # %fol_IsNegativeLiteral.exit.thread
                                        #   in Loop: Header=BB14_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB14_9
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	callq	term_Print
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB14_8
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_9:                               # %.thread
                                        #   in Loop: Header=BB14_2 Depth=1
	callq	term_Print
.LBB14_10:                              #   in Loop: Header=BB14_2 Depth=1
	movl	$.L.str.7, %edi
	callq	puts
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_2
.LBB14_11:                              # %._crit_edge36
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	cnf_StdoutPrint, .Lfunc_end14-cnf_StdoutPrint
	.cfi_endproc

	.globl	cnf_FilePrint
	.p2align	4, 0x90
	.type	cnf_FilePrint,@function
cnf_FilePrint:                          # @cnf_FilePrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.LBB15_11
# BB#1:                                 # %.lr.ph38
	movl	symbol_TYPEMASK(%rip), %r15d
	.p2align	4, 0x90
.LBB15_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_8 Depth 2
	movq	8(%r14), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB15_4
# BB#3:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r15d, %ecx
	cmpl	$2, %ecx
	je	.LBB15_9
.LBB15_4:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	16(%rsi), %rbp
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB15_7
# BB#5:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB15_7
# BB#6:                                 # %fol_IsNegativeLiteral.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	negl	%eax
	andl	%r15d, %eax
	cmpl	$2, %eax
	je	.LBB15_9
	.p2align	4, 0x90
.LBB15_7:                               # %fol_IsNegativeLiteral.exit.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB15_9
	.p2align	4, 0x90
.LBB15_8:                               # %.lr.ph
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_FPrint
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_8
	jmp	.LBB15_10
	.p2align	4, 0x90
.LBB15_9:                               # %.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%rbx, %rdi
	callq	term_FPrint
.LBB15_10:                              #   in Loop: Header=BB15_2 Depth=1
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB15_2
.LBB15_11:                              # %._crit_edge39
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	cnf_FilePrint, .Lfunc_end15-cnf_FilePrint
	.cfi_endproc

	.globl	cnf_FilePrintPrefix
	.p2align	4, 0x90
	.type	cnf_FilePrintPrefix,@function
cnf_FilePrintPrefix:                    # @cnf_FilePrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 48
.Lcfi116:
	.cfi_offset %rbx, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.LBB16_12
# BB#1:                                 # %.lr.ph41
	movl	symbol_TYPEMASK(%rip), %r15d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_8 Depth 2
	movq	8(%r14), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB16_4
# BB#3:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB16_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r15d, %ecx
	cmpl	$2, %ecx
	je	.LBB16_10
.LBB16_4:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	16(%rsi), %rbp
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB16_7
# BB#5:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB16_7
# BB#6:                                 # %fol_IsNegativeLiteral.exit
                                        #   in Loop: Header=BB16_2 Depth=1
	negl	%eax
	andl	%r15d, %eax
	cmpl	$2, %eax
	je	.LBB16_10
	.p2align	4, 0x90
.LBB16_7:                               # %fol_IsNegativeLiteral.exit.thread
                                        #   in Loop: Header=BB16_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB16_10
	.p2align	4, 0x90
.LBB16_8:                               # %.lr.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_FPrintPrefix
	cmpq	$0, (%rbp)
	je	.LBB16_11
# BB#9:                                 #   in Loop: Header=BB16_8 Depth=2
	movl	$.L.str.8, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB16_8
	jmp	.LBB16_11
	.p2align	4, 0x90
.LBB16_10:                              # %.thread
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%rbx, %rdi
	callq	term_FPrintPrefix
.LBB16_11:                              #   in Loop: Header=BB16_2 Depth=1
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB16_2
.LBB16_12:                              # %._crit_edge42
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	cnf_FilePrintPrefix, .Lfunc_end16-cnf_FilePrintPrefix
	.cfi_endproc

	.globl	cnf_Flatten
	.p2align	4, 0x90
	.type	cnf_Flatten,@function
cnf_Flatten:                            # @cnf_Flatten
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -48
.Lcfi126:
	.cfi_offset %r12, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	cmpl	%r15d, (%r14)
	jne	.LBB17_5
# BB#1:
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB17_5
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %r12
	movq	8(%rbp), %rbx
	cmpl	%r15d, (%rbx)
	jne	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	cnf_Flatten
	movq	16(%rbx), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rdi
	callq	list_NInsert
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%rbp)
	movq	16(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB17_4:                               # %.backedge
                                        #   in Loop: Header=BB17_2 Depth=1
	testq	%r12, %r12
	movq	%r12, %rbp
	jne	.LBB17_2
.LBB17_5:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	cnf_Flatten, .Lfunc_end17-cnf_Flatten
	.cfi_endproc

	.globl	cnf_RemoveTrivialAtoms
	.p2align	4, 0x90
	.type	cnf_RemoveTrivialAtoms,@function
cnf_RemoveTrivialAtoms:                 # @cnf_RemoveTrivialAtoms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 32
.Lcfi133:
	.cfi_offset %rbx, -32
.Lcfi134:
	.cfi_offset %r14, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_43:                              # %tailrecurse.backedge
                                        #   in Loop: Header=BB18_1 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, (%r14)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
.LBB18_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_2 Depth 2
                                        #     Child Loop BB18_53 Depth 2
                                        #     Child Loop BB18_42 Depth 2
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB18_59
.LBB18_2:                               # %.lr.ph254
                                        #   Parent Loop BB18_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %ebp
	cmpl	fol_AND(%rip), %ebp
	je	.LBB18_3
# BB#12:                                # %.lr.ph340
                                        #   in Loop: Header=BB18_2 Depth=2
	cmpl	fol_OR(%rip), %ebp
	je	.LBB18_13
# BB#20:                                #   in Loop: Header=BB18_2 Depth=2
	cmpl	%ebp, fol_ALL(%rip)
	sete	%cl
	cmpl	%ebp, fol_EXIST(%rip)
	sete	%al
	orb	%cl, %al
	jne	.LBB18_22
# BB#21:                                #   in Loop: Header=BB18_2 Depth=2
	cmpl	fol_NOT(%rip), %ebp
	je	.LBB18_22
# BB#37:                                #   in Loop: Header=BB18_2 Depth=2
	cmpl	fol_IMPLIES(%rip), %ebp
	je	.LBB18_38
# BB#47:                                #   in Loop: Header=BB18_2 Depth=2
	cmpl	fol_EQUIV(%rip), %ebp
	jne	.LBB18_59
# BB#48:                                #   in Loop: Header=BB18_2 Depth=2
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %ecx
	cmpl	fol_FALSE(%rip), %ecx
	jne	.LBB18_50
# BB#49:                                #   in Loop: Header=BB18_2 Depth=2
	movl	fol_NOT(%rip), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	movl	$fol_IsFalse, %esi
	movl	$term_Delete, %edx
	callq	list_DeleteElementIfFree
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	testq	%rbx, %rbx
	jne	.LBB18_2
	jmp	.LBB18_11
	.p2align	4, 0x90
.LBB18_38:                              #   in Loop: Header=BB18_1 Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %edx
	movl	fol_TRUE(%rip), %ecx
	cmpl	fol_FALSE(%rip), %edx
	je	.LBB18_6
# BB#39:                                #   in Loop: Header=BB18_1 Depth=1
	cmpl	%ecx, %edx
	jne	.LBB18_44
# BB#40:                                #   in Loop: Header=BB18_1 Depth=1
	movq	%rax, %rdi
	callq	term_Delete
	movq	16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB18_43
# BB#41:                                # %.lr.ph.i189.preheader
                                        #   in Loop: Header=BB18_1 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB18_43
	.p2align	4, 0x90
.LBB18_42:                              # %.lr.ph.i189..lr.ph.i189_crit_edge
                                        #   Parent Loop BB18_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB18_42
	jmp	.LBB18_43
.LBB18_50:                              #   in Loop: Header=BB18_1 Depth=1
	cmpl	fol_TRUE(%rip), %ecx
	jne	.LBB18_54
# BB#51:                                #   in Loop: Header=BB18_1 Depth=1
	movq	%rax, %rdi
	callq	term_Delete
	movq	16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB18_43
# BB#52:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_1 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB18_43
	.p2align	4, 0x90
.LBB18_53:                              # %.lr.ph.i..lr.ph.i_crit_edge
                                        #   Parent Loop BB18_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB18_53
	jmp	.LBB18_43
.LBB18_3:                               # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %ecx
	movl	$1, %eax
	cmpl	fol_TRUE(%rip), %ecx
	je	.LBB18_8
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=1
	cmpl	fol_FALSE(%rip), %ecx
	movl	%ebp, %eax
	je	.LBB18_6
.LBB18_8:                               #   in Loop: Header=BB18_4 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movl	%eax, %ebp
	jne	.LBB18_4
# BB#9:                                 # %._crit_edge
	testl	%eax, %eax
	je	.LBB18_59
# BB#10:
	movq	16(%r14), %rdi
	movl	$fol_IsTrue, %esi
	movl	$term_Delete, %edx
	callq	list_DeleteElementIfFree
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	jne	.LBB18_59
.LBB18_11:
	movl	fol_TRUE(%rip), %eax
	movl	%eax, (%r14)
	jmp	.LBB18_59
.LBB18_13:                              # %.lr.ph248.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_14:                              # %.lr.ph248
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %ecx
	movl	$1, %eax
	cmpl	fol_FALSE(%rip), %ecx
	je	.LBB18_16
# BB#15:                                #   in Loop: Header=BB18_14 Depth=1
	cmpl	fol_TRUE(%rip), %ecx
	movl	%ebp, %eax
	je	.LBB18_6
.LBB18_16:                              #   in Loop: Header=BB18_14 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movl	%eax, %ebp
	jne	.LBB18_14
# BB#17:                                # %._crit_edge249
	testl	%eax, %eax
	je	.LBB18_59
# BB#18:
	movq	16(%r14), %rdi
	movl	$fol_IsFalse, %esi
	movl	$term_Delete, %edx
	callq	list_DeleteElementIfFree
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	jne	.LBB18_59
# BB#19:
	movl	fol_FALSE(%rip), %eax
	movl	%eax, (%r14)
	jmp	.LBB18_59
.LBB18_6:
	movl	%ecx, (%r14)
.LBB18_7:                               # %.loopexit
	movq	16(%r14), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, 16(%r14)
.LBB18_59:                              # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB18_22:
	testb	$1, %al
	je	.LBB18_24
# BB#23:
	movq	(%rbx), %rbx
.LBB18_24:
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %ecx
	movl	fol_FALSE(%rip), %eax
	movl	fol_NOT(%rip), %esi
	movl	fol_TRUE(%rip), %edx
	cmpl	%eax, %ecx
	jne	.LBB18_26
# BB#25:
	cmpl	%esi, %ebp
	je	.LBB18_30
.LBB18_26:
	cmpl	%edx, %ecx
	jne	.LBB18_33
# BB#27:
	cmpl	%ebp, fol_ALL(%rip)
	je	.LBB18_29
# BB#28:
	cmpl	%ebp, fol_EXIST(%rip)
	je	.LBB18_29
# BB#31:
	cmpl	%edx, %ecx
	jne	.LBB18_33
# BB#32:
	cmpl	%esi, %ebp
	je	.LBB18_36
.LBB18_33:                              # %.thread
	cmpl	%eax, %ecx
	jne	.LBB18_59
# BB#34:
	cmpl	%ebp, fol_ALL(%rip)
	je	.LBB18_36
# BB#35:
	cmpl	%ebp, fol_EXIST(%rip)
	jne	.LBB18_59
	jmp	.LBB18_36
.LBB18_44:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %eax
	cmpl	fol_TRUE(%rip), %eax
	jne	.LBB18_45
.LBB18_36:
	movl	%eax, (%r14)
	jmp	.LBB18_7
.LBB18_54:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	cnf_RemoveTrivialAtoms
	movl	(%rax), %ecx
	cmpl	fol_FALSE(%rip), %ecx
	je	.LBB18_46
# BB#55:
	cmpl	fol_TRUE(%rip), %ecx
	jne	.LBB18_59
# BB#56:
	movq	%rax, %rdi
	callq	term_Delete
	movq	16(%r14), %rcx
	movq	8(%rcx), %rax
	testq	%rcx, %rcx
	je	.LBB18_58
.LBB18_57:                              # %.lr.ph.i184
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB18_57
.LBB18_58:                              # %list_Delete.exit185
	movl	(%rax), %ecx
	movl	%ecx, (%r14)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB18_59
.LBB18_45:
	cmpl	fol_FALSE(%rip), %eax
	jne	.LBB18_59
.LBB18_46:
	movl	fol_NOT(%rip), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	movl	$fol_IsFalse, %esi
	movl	$term_Delete, %edx
	callq	list_DeleteElementIfFree
	movq	%rax, 16(%r14)
	jmp	.LBB18_59
.LBB18_29:
	movl	%ecx, %edx
.LBB18_30:                              # %._crit_edge287
	movl	%edx, (%r14)
	jmp	.LBB18_7
.Lfunc_end18:
	.size	cnf_RemoveTrivialAtoms, .Lfunc_end18-cnf_RemoveTrivialAtoms
	.cfi_endproc

	.globl	cnf_ObviousSimplifications
	.p2align	4, 0x90
	.type	cnf_ObviousSimplifications,@function
cnf_ObviousSimplifications:             # @cnf_ObviousSimplifications
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 16
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	popq	%rax
	jmp	cnf_SimplifyQuantors    # TAILCALL
.Lfunc_end19:
	.size	cnf_ObviousSimplifications, .Lfunc_end19-cnf_ObviousSimplifications
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_RemoveTrivialOperators,@function
cnf_RemoveTrivialOperators:             # @cnf_RemoveTrivialOperators
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	symbol_TYPEMASK(%rip), %eax
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_8:                               # %list_Delete.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+256(%rip), %rdx
	movq	%r14, (%rdx)
	movq	%rcx, %r14
.LBB20_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jns	.LBB20_3
# BB#2:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movl	%ecx, %edx
	negl	%edx
	andl	%eax, %edx
	cmpl	$2, %edx
	je	.LBB20_11
.LBB20_3:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB20_1 Depth=1
	cmpl	fol_AND(%rip), %ecx
	movq	16(%r14), %rbx
	je	.LBB20_5
# BB#4:                                 # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB20_1 Depth=1
	cmpl	fol_OR(%rip), %ecx
	jne	.LBB20_10
.LBB20_5:                               #   in Loop: Header=BB20_1 Depth=1
	cmpq	$0, (%rbx)
	jne	.LBB20_10
# BB#6:                                 #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rbx), %rcx
	movq	8(%r14), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB20_8
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph.i
                                        #   Parent Loop BB20_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbx
	addq	%rbx, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rdx, (%rdi)
	testq	%rsi, %rsi
	movq	%rsi, %rdx
	jne	.LBB20_7
	jmp	.LBB20_8
	.p2align	4, 0x90
.LBB20_9:                               # %.lr.ph
                                        #   in Loop: Header=BB20_10 Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, 8(%rbx)
	movq	%r14, 8(%rax)
	movq	(%rbx), %rbx
.LBB20_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jne	.LBB20_9
.LBB20_11:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	cnf_RemoveTrivialOperators, .Lfunc_end20-cnf_RemoveTrivialOperators
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_SimplifyQuantors,@function
cnf_SimplifyQuantors:                   # @cnf_SimplifyQuantors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 64
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	symbol_TYPEMASK(%rip), %r13d
	jmp	.LBB21_1
	.p2align	4, 0x90
.LBB21_17:                              # %.loopexit96
                                        #   in Loop: Header=BB21_1 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
	movq	%rbx, %r14
.LBB21_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_18 Depth 2
                                        #       Child Loop BB21_22 Depth 3
                                        #       Child Loop BB21_26 Depth 3
                                        #     Child Loop BB21_10 Depth 2
                                        #     Child Loop BB21_16 Depth 2
	movl	(%r14), %eax
	testl	%eax, %eax
	jns	.LBB21_3
# BB#2:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB21_1 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r13d, %ecx
	cmpl	$2, %ecx
	je	.LBB21_31
.LBB21_3:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB21_1 Depth=1
	cmpl	fol_VARLIST(%rip), %eax
	je	.LBB21_31
# BB#4:                                 #   in Loop: Header=BB21_1 Depth=1
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_1 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB21_28
.LBB21_6:                               #   in Loop: Header=BB21_1 Depth=1
	movq	16(%r14), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rdx
	movq	8(%rsi), %rcx
	leaq	16(%rdx), %rsi
	cmpl	%eax, (%rcx)
	jne	.LBB21_7
	.p2align	4, 0x90
.LBB21_18:                              # %.lr.ph110
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_22 Depth 3
                                        #       Child Loop BB21_26 Depth 3
	movq	(%rsi), %rsi
	movq	16(%rcx), %rdi
	movq	8(%rdi), %rdi
	movq	16(%rdi), %rdi
	testq	%rsi, %rsi
	je	.LBB21_19
# BB#20:                                #   in Loop: Header=BB21_18 Depth=2
	testq	%rdi, %rdi
	je	.LBB21_24
# BB#21:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB21_18 Depth=2
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB21_22:                              # %.preheader.i
                                        #   Parent Loop BB21_1 Depth=1
                                        #     Parent Loop BB21_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %rbx
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_22
# BB#23:                                #   in Loop: Header=BB21_18 Depth=2
	movq	%rdi, (%rbx)
	jmp	.LBB21_24
	.p2align	4, 0x90
.LBB21_19:                              #   in Loop: Header=BB21_18 Depth=2
	movq	%rdi, %rsi
.LBB21_24:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB21_18 Depth=2
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rdx
	movq	8(%rdx), %rdx
	movq	memory_ARRAY+256(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+256(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	16(%rcx), %rsi
	movq	(%rsi), %rdx
	movq	8(%rdx), %r12
	testq	%rsi, %rsi
	je	.LBB21_27
# BB#25:                                # %.lr.ph.i83.preheader
                                        #   in Loop: Header=BB21_18 Depth=2
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rsi)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rsi, (%rdi)
	testq	%rdx, %rdx
	je	.LBB21_27
	.p2align	4, 0x90
.LBB21_26:                              # %.lr.ph.i83..lr.ph.i83_crit_edge
                                        #   Parent Loop BB21_1 Depth=1
                                        #     Parent Loop BB21_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rsi
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rdx, (%rdi)
	testq	%rsi, %rsi
	movq	%rsi, %rdx
	jne	.LBB21_26
.LBB21_27:                              # %list_Delete.exit84
                                        #   in Loop: Header=BB21_18 Depth=2
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+256(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	16(%r14), %rcx
	movq	(%rcx), %rcx
	movq	%r12, 8(%rcx)
	movq	16(%r14), %rcx
	movq	8(%rcx), %rdx
	leaq	16(%rdx), %rsi
	cmpl	%eax, (%r12)
	movq	%r12, %rcx
	je	.LBB21_18
	jmp	.LBB21_8
	.p2align	4, 0x90
.LBB21_7:                               #   in Loop: Header=BB21_1 Depth=1
	movq	%rcx, %r12
.LBB21_8:                               # %.preheader
                                        #   in Loop: Header=BB21_1 Depth=1
	movq	(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB21_28
# BB#9:                                 # %.lr.ph115.preheader
                                        #   in Loop: Header=BB21_1 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB21_10:                              # %.lr.ph115
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	fol_VarOccursFreely
	testl	%eax, %eax
	jne	.LBB21_12
# BB#11:                                #   in Loop: Header=BB21_10 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB21_12:                              #   in Loop: Header=BB21_10 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_10
# BB#13:                                # %._crit_edge
                                        #   in Loop: Header=BB21_1 Depth=1
	testq	%r15, %r15
	je	.LBB21_28
# BB#14:                                #   in Loop: Header=BB21_1 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %rbx
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	callq	list_NPointerDifference
	movq	%rax, 16(%rbx)
	movl	$term_Delete, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
	cmpq	$0, 16(%rbx)
	jne	.LBB21_28
# BB#15:                                #   in Loop: Header=BB21_1 Depth=1
	movq	16(%r14), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rbx
	callq	term_Delete
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB21_17
	.p2align	4, 0x90
.LBB21_16:                              # %.lr.ph.i
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB21_16
	jmp	.LBB21_17
.LBB21_28:                              # %.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB21_31
	.p2align	4, 0x90
.LBB21_29:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_29
.LBB21_31:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	cnf_SimplifyQuantors, .Lfunc_end21-cnf_SimplifyQuantors
	.cfi_endproc

	.globl	cnf_HaveProof
	.p2align	4, 0x90
	.type	cnf_HaveProof,@function
cnf_HaveProof:                          # @cnf_HaveProof
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi158:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi159:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi161:
	.cfi_def_cfa_offset 96
.Lcfi162:
	.cfi_offset %rbx, -56
.Lcfi163:
	.cfi_offset %r12, -48
.Lcfi164:
	.cfi_offset %r13, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	cnf_HAVEPROOFPS(%rip), %rbx
	movq	$0, 8(%rsp)
	movq	112(%rbx), %rsi
	movd	flag_CLEAN(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm0, 16(%rsi)
	movdqu	%xmm0, 32(%rsi)
	movdqu	%xmm0, 48(%rsi)
	movdqu	%xmm0, 64(%rsi)
	movdqu	%xmm0, 80(%rsi)
	movdqu	%xmm0, 96(%rsi)
	movdqu	%xmm0, 112(%rsi)
	movdqu	%xmm0, 128(%rsi)
	movdqu	%xmm0, 144(%rsi)
	movdqu	%xmm0, 160(%rsi)
	movdqu	%xmm0, 176(%rsi)
	movdqu	%xmm0, 192(%rsi)
	movdqu	%xmm0, 208(%rsi)
	movdqu	%xmm0, 224(%rsi)
	movdqu	%xmm0, 240(%rsi)
	movdqu	%xmm0, 256(%rsi)
	movdqu	%xmm0, 272(%rsi)
	movdqu	%xmm0, 288(%rsi)
	movdqu	%xmm0, 304(%rsi)
	movdqu	%xmm0, 320(%rsi)
	movdqu	%xmm0, 336(%rsi)
	movdqu	%xmm0, 352(%rsi)
	movdqu	%xmm0, 368(%rsi)
	movq	%rdx, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	callq	flag_InitFlotterSubproofFlags
	movq	104(%rbx), %r12
	leaq	16000(%rbp), %rax
	cmpq	%rax, %r12
	jae	.LBB22_2
# BB#1:                                 # %vector.body
	leaq	16000(%r12), %rax
	cmpq	%rbp, %rax
	jbe	.LBB22_2
# BB#4:                                 # %scalar.ph86.preheader
	movl	$7, %eax
	.p2align	4, 0x90
.LBB22_5:                               # %scalar.ph86
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbp,%rax,4), %ecx
	movl	%ecx, -28(%r12,%rax,4)
	movl	-24(%rbp,%rax,4), %ecx
	movl	%ecx, -24(%r12,%rax,4)
	movl	-20(%rbp,%rax,4), %ecx
	movl	%ecx, -20(%r12,%rax,4)
	movl	-16(%rbp,%rax,4), %ecx
	movl	%ecx, -16(%r12,%rax,4)
	movl	-12(%rbp,%rax,4), %ecx
	movl	%ecx, -12(%r12,%rax,4)
	movl	-8(%rbp,%rax,4), %ecx
	movl	%ecx, -8(%r12,%rax,4)
	movl	-4(%rbp,%rax,4), %ecx
	movl	%ecx, -4(%r12,%rax,4)
	movl	(%rbp,%rax,4), %ecx
	movl	%ecx, (%r12,%rax,4)
	addq	$8, %rax
	cmpq	$4007, %rax             # imm = 0xFA7
	jne	.LBB22_5
	jmp	.LBB22_6
.LBB22_2:                               # %vector.body84.preheader
	movl	$36, %eax
	.p2align	4, 0x90
.LBB22_3:                               # %vector.body84
                                        # =>This Inner Loop Header: Depth=1
	movups	-144(%rbp,%rax,4), %xmm0
	movups	-128(%rbp,%rax,4), %xmm1
	movups	%xmm0, -144(%r12,%rax,4)
	movups	%xmm1, -128(%r12,%rax,4)
	movups	-112(%rbp,%rax,4), %xmm0
	movups	-96(%rbp,%rax,4), %xmm1
	movups	%xmm0, -112(%r12,%rax,4)
	movups	%xmm1, -96(%r12,%rax,4)
	movups	-80(%rbp,%rax,4), %xmm0
	movups	-64(%rbp,%rax,4), %xmm1
	movups	%xmm0, -80(%r12,%rax,4)
	movups	%xmm1, -64(%r12,%rax,4)
	movups	-48(%rbp,%rax,4), %xmm0
	movups	-32(%rbp,%rax,4), %xmm1
	movups	%xmm0, -48(%r12,%rax,4)
	movups	%xmm1, -32(%r12,%rax,4)
	movdqu	-16(%rbp,%rax,4), %xmm0
	movups	(%rbp,%rax,4), %xmm1
	movdqu	%xmm0, -16(%r12,%rax,4)
	movups	%xmm1, (%r12,%rax,4)
	addq	$40, %rax
	cmpq	$4036, %rax             # imm = 0xFC4
	jne	.LBB22_3
.LBB22_6:                               # %symbol_TransferPrecedence.exit.preheader
	testq	%r13, %r13
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	je	.LBB22_7
# BB#8:                                 # %.lr.ph78.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_9:                               # %.lr.ph78
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_11 Depth 2
                                        #     Child Loop BB22_16 Depth 2
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%r13), %rdi
	callq	term_Copy
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	term_AddFatherLinks
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movq	%r12, %rsi
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdx
	callq	ren_Rename
	movq	%rax, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	cnf_SkolemFormula
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB22_12
# BB#10:                                # %.lr.ph.i71.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB22_11:                              # %.lr.ph.i71
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	fol_OR(%rip), %edi
	movq	8(%rbp), %rsi
	callq	term_Create
	movq	%rax, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_11
.LBB22_12:                              # %cnf_DistributiveFormula.exit72
                                        #   in Loop: Header=BB22_9 Depth=1
	movl	fol_AND(%rip), %edi
	movq	%rbx, %rsi
	callq	term_Create
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	term_Delete
	movq	%r14, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_13
# BB#14:                                #   in Loop: Header=BB22_9 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB22_18
# BB#15:                                # %.preheader.i63.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_16:                              # %.preheader.i63
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_16
# BB#17:                                #   in Loop: Header=BB22_9 Depth=1
	movq	%rdx, (%rax)
	jmp	.LBB22_18
	.p2align	4, 0x90
.LBB22_13:                              #   in Loop: Header=BB22_9 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB22_18:                              # %list_Nconc.exit65
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movq	(%r13), %r13
	testq	%r13, %r13
	movq	%rbp, %rax
	jne	.LBB22_9
	jmp	.LBB22_19
.LBB22_7:
	xorl	%ebp, %ebp
.LBB22_19:                              # %symbol_TransferPrecedence.exit._crit_edge
	movl	fol_NOT(%rip), %r14d
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	movq	%r14, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	leaq	8(%rsp), %r15
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	ren_Rename
	movq	%rax, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	cnf_SkolemFormula
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB22_22
# BB#20:                                # %.lr.ph.i.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_OR(%rip), %edi
	movq	8(%rbx), %rsi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_21
.LBB22_22:                              # %cnf_DistributiveFormula.exit
	movl	fol_AND(%rip), %edi
	movq	%r15, %rsi
	callq	term_Create
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	term_Delete
	movq	%r15, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB22_23
# BB#24:
	testq	%rbp, %rbp
	je	.LBB22_30
# BB#25:
	movq	%rbx, %rax
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB22_26:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB22_26
# BB#27:
	movq	%rbp, (%rcx)
	jmp	.LBB22_28
.LBB22_23:
	movq	%rbp, %rbx
	movq	(%rsp), %r14            # 8-byte Reload
.LBB22_28:                              # %list_Nconc.exit
	movq	%r15, %rdi
	callq	term_Delete
	testq	%rbx, %rbx
	jne	.LBB22_31
# BB#29:
	xorl	%ebx, %ebx
	jmp	.LBB22_33
.LBB22_30:                              # %list_Nconc.exit.thread
	movq	%r15, %rdi
	callq	term_Delete
	movq	(%rsp), %r14            # 8-byte Reload
.LBB22_31:                              # %.lr.ph.preheader
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB22_32:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	orl	$8, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB22_32
.LBB22_33:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	cnf_SatUnit
	testq	%rax, %rax
	je	.LBB22_34
# BB#35:
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rdi
	callq	clause_DeleteClauseList
	movl	$1, %ebx
	jmp	.LBB22_36
.LBB22_34:
	xorl	%ebx, %ebx
.LBB22_36:
	movq	%r14, %rdi
	callq	prfs_Clean
	movq	8(%rsp), %rdi
	movl	$symbol_Delete, %esi
	callq	list_DeleteWithElement
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	cnf_HaveProof, .Lfunc_end22-cnf_HaveProof
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_MakeClauseList,@function
cnf_MakeClauseList:                     # @cnf_MakeClauseList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi174:
	.cfi_def_cfa_offset 80
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movl	(%rdi), %eax
	cmpl	%eax, fol_TRUE(%rip)
	jne	.LBB23_2
# BB#1:
	xorl	%r14d, %r14d
	jmp	.LBB23_102
.LBB23_2:
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB23_5
# BB#3:
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB23_5
# BB#4:                                 # %fol_IsNegativeLiteral.exit
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB23_7
.LBB23_5:                               # %fol_IsNegativeLiteral.exit.thread
	testl	%eax, %eax
	jns	.LBB23_9
# BB#6:                                 # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB23_9
.LBB23_7:
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	$0, (%rbx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %r8
	movq	%r14, %r9
	callq	clause_CreateFromLiterals
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r15, 8(%r14)
	movq	$0, (%r14)
	callq	term_StartMinRenaming
	movq	56(%r15), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	testq	%rbx, %rbx
	je	.LBB23_102
	.p2align	4, 0x90
.LBB23_8:                               # %.lr.ph.i34
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB23_8
	jmp	.LBB23_102
.LBB23_9:                               # %symbol_IsPredicate.exit.thread
	callq	cnf_MakeOneAnd
	movq	%rax, %rbx
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB23_30
# BB#10:                                # %.lr.ph52.i.i
	movl	fol_OR(%rip), %eax
	.p2align	4, 0x90
.LBB23_11:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rcx
	cmpl	%eax, (%rcx)
	je	.LBB23_12
# BB#21:                                #   in Loop: Header=BB23_11 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_11
	jmp	.LBB23_30
.LBB23_12:                              # %.lr.ph48.i.i
	movl	symbol_TYPEMASK(%rip), %r13d
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jns	.LBB23_19
	jmp	.LBB23_14
	.p2align	4, 0x90
.LBB23_20:                              # %symbol_IsPredicate.exit.thread._crit_edge.i.i
                                        #   in Loop: Header=BB23_19 Depth=1
	movq	8(%rbp), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	js	.LBB23_14
.LBB23_19:                              # %symbol_IsPredicate.exit.thread.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_20
	jmp	.LBB23_22
	.p2align	4, 0x90
.LBB23_14:                              # %symbol_IsPredicate.exit.i.i
	movl	%eax, %edx
	negl	%edx
	andl	%r13d, %edx
	cmpl	$2, %edx
	jne	.LBB23_19
# BB#15:                                # %symbol_IsPredicate.exit.i.i
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB23_19
# BB#16:
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB23_19
# BB#17:                                # %fol_IsNegativeLiteral.exit.i.i
	negl	%eax
	andl	%r13d, %eax
	cmpl	$2, %eax
	jne	.LBB23_19
# BB#18:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r15, %r12
	movq	%r14, %r15
	movq	8(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, %r14
	movq	%r12, %r15
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, %r12
	jmp	.LBB23_19
.LBB23_22:                              # %.preheader.i.i
	testq	%r12, %r12
	je	.LBB23_30
# BB#23:                                # %.lr.ph.i.preheader.i
	movq	16(%rbx), %rax
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB23_24:                              # %.lr.ph.i.i37
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%rax, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_24
# BB#25:                                # %._crit_edge.i.i
	testq	%rax, %rax
	je	.LBB23_29
# BB#26:                                # %.preheader.i.i.i.preheader
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB23_27:                              # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB23_27
# BB#28:
	movq	%rax, (%rcx)
.LBB23_29:                              # %list_Nconc.exit.i.i
	movq	%r12, 16(%rbx)
.LBB23_30:                              # %cnf_MakeOneAndTerm.exit
	movl	(%rbx), %eax
	cmpl	fol_AND(%rip), %eax
	jne	.LBB23_48
# BB#31:                                # %.preheader88
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB23_103
# BB#32:                                # %.lr.ph116
	xorl	%r12d, %r12d
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB23_33:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_43 Depth 2
                                        #     Child Loop BB23_45 Depth 2
	movq	8(%rbp), %rdi
	callq	cnf_MakeOneOrTerm
	movq	%rax, 8(%rbp)
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jns	.LBB23_35
# BB#34:                                # %symbol_IsPredicate.exit62
                                        #   in Loop: Header=BB23_33 Depth=1
	movl	%ecx, %edx
	negl	%edx
	andl	symbol_TYPEMASK(%rip), %edx
	cmpl	$2, %edx
	je	.LBB23_39
.LBB23_35:                              # %symbol_IsPredicate.exit62.thread
                                        #   in Loop: Header=BB23_33 Depth=1
	movq	16(%rax), %rdi
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB23_38
# BB#36:                                #   in Loop: Header=BB23_33 Depth=1
	movq	8(%rdi), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB23_38
# BB#37:                                # %fol_IsNegativeLiteral.exit79
                                        #   in Loop: Header=BB23_33 Depth=1
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB23_38
.LBB23_39:                              #   in Loop: Header=BB23_33 Depth=1
	movq	%rax, %rdi
	callq	term_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	$0, (%rbx)
	testq	%rbx, %rbx
	jne	.LBB23_41
	jmp	.LBB23_47
	.p2align	4, 0x90
.LBB23_38:                              # %fol_IsNegativeLiteral.exit79.thread
                                        #   in Loop: Header=BB23_33 Depth=1
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movl	$term_Equal, %esi
	movl	$term_Delete, %edx
	movq	%rax, %rdi
	callq	list_DeleteDuplicatesFree
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB23_47
.LBB23_41:                              #   in Loop: Header=BB23_33 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %r8
	movq	%r14, %r15
	movq	%r14, %r9
	callq	clause_CreateFromLiterals
	movq	%rax, %r13
	callq	term_StartMinRenaming
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB23_44
# BB#42:                                # %.lr.ph111
                                        #   in Loop: Header=BB23_33 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_43:                              #   Parent Loop BB23_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r13), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%r14
	movslq	68(%r13), %rax
	movslq	72(%r13), %rcx
	movslq	64(%r13), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r14
	jl	.LBB23_43
.LBB23_44:                              # %.lr.ph.i66.preheader
                                        #   in Loop: Header=BB23_33 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r12, (%rax)
	.p2align	4, 0x90
.LBB23_45:                              # %.lr.ph.i66
                                        #   Parent Loop BB23_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB23_45
# BB#46:                                #   in Loop: Header=BB23_33 Depth=1
	movq	%rax, %r12
	movq	%r15, %r14
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB23_47:                              # %list_Delete.exit68
                                        #   in Loop: Header=BB23_33 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_33
	jmp	.LBB23_61
.LBB23_48:
	movq	%rbx, %rdi
	callq	cnf_MakeOneOrTerm
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jns	.LBB23_50
# BB#49:                                # %symbol_IsPredicate.exit61
	movl	%ecx, %edx
	negl	%edx
	andl	symbol_TYPEMASK(%rip), %edx
	cmpl	$2, %edx
	je	.LBB23_54
.LBB23_50:                              # %symbol_IsPredicate.exit61.thread
	movq	16(%rax), %rdi
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB23_53
# BB#51:
	movq	8(%rdi), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB23_53
# BB#52:                                # %fol_IsNegativeLiteral.exit60
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB23_53
.LBB23_54:
	movq	%rax, %rdi
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	$0, (%rbx)
	testq	%rbx, %rbx
	jne	.LBB23_56
	jmp	.LBB23_103
.LBB23_53:                              # %fol_IsNegativeLiteral.exit60.thread
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movl	$term_Equal, %esi
	movl	$term_Delete, %edx
	movq	%rax, %rdi
	callq	list_DeleteDuplicatesFree
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB23_103
.LBB23_56:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %r8
	movq	%r14, %r9
	callq	clause_CreateFromLiterals
	movq	%rax, %r13
	callq	term_StartMinRenaming
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB23_59
# BB#57:                                # %.lr.ph123
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_58:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbp
	movslq	68(%r13), %rax
	movslq	72(%r13), %rcx
	movslq	64(%r13), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbp
	jl	.LBB23_58
.LBB23_59:                              # %.lr.ph.i47.preheader
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r13, 8(%r12)
	movq	$0, (%r12)
	.p2align	4, 0x90
.LBB23_60:                              # %.lr.ph.i47
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB23_60
.LBB23_61:                              # %list_Delete.exit49
	testq	%r12, %r12
	sete	%r13b
	je	.LBB23_103
# BB#62:                                # %.lr.ph104.preheader
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB23_63:                              # %.lr.ph104
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_65 Depth 2
	movq	8(%rbp), %rdi
	callq	cond_CondFast
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB23_66
# BB#64:                                #   in Loop: Header=BB23_63 Depth=1
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	clause_DeleteLiterals
	.p2align	4, 0x90
.LBB23_65:                              # %.lr.ph.i41
                                        #   Parent Loop BB23_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB23_65
.LBB23_66:                              # %list_Delete.exit43
                                        #   in Loop: Header=BB23_63 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_63
# BB#67:                                # %._crit_edge105
	callq	st_IndexCreate
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB23_68
# BB#69:                                # %.lr.ph54.i.preheader
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB23_70:                              # %.lr.ph54.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	res_InsertClauseIndex
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_70
# BB#71:                                # %.lr.ph49.i.preheader
	xorl	%r15d, %r15d
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB23_72:                              # %.lr.ph49.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	res_DeleteClauseIndex
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	res_BackSubWithLength
	testl	%eax, %eax
	jne	.LBB23_74
# BB#73:                                #   in Loop: Header=BB23_72 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	res_InsertClauseIndex
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB23_74:                              #   in Loop: Header=BB23_72 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_72
	jmp	.LBB23_75
.LBB23_103:                             # %._crit_edge105.thread
	movb	$1, %r13b
	xorl	%r12d, %r12d
	callq	st_IndexCreate
	movq	%rax, %r14
	xorl	%r15d, %r15d
.LBB23_75:                              # %._crit_edge50.i
	movq	%r15, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movq	%r12, %rdi
	callq	list_Length
	cmpl	%eax, %ebx
	jne	.LBB23_76
# BB#80:
	testb	%r13b, %r13b
	jne	.LBB23_82
	.p2align	4, 0x90
.LBB23_81:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB23_81
	jmp	.LBB23_82
.LBB23_76:                              # %.preheader.i26
	testq	%r15, %r15
	je	.LBB23_104
# BB#77:
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB23_78:                              # %.lr.ph.i27
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %r12
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_78
# BB#79:                                # %._crit_edge.i
	movq	%r12, %rdi
	callq	clause_DeleteClauseList
.LBB23_82:                              # %cnf_SubsumeClauseList.exit
	movq	%r14, %rdi
	callq	st_IndexDelete
	testq	%r15, %r15
	je	.LBB23_83
# BB#91:                                # %.lr.ph101.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_92:                              # %.lr.ph101
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_96 Depth 2
	movq	%r15, %rdi
	callq	res_SelectLightestClause
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%r14, %r14
	je	.LBB23_93
# BB#94:                                #   in Loop: Header=BB23_92 Depth=1
	testq	%rax, %rax
	je	.LBB23_98
# BB#95:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB23_92 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB23_96:                              # %.preheader.i
                                        #   Parent Loop BB23_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB23_96
# BB#97:                                #   in Loop: Header=BB23_92 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB23_98
	.p2align	4, 0x90
.LBB23_93:                              #   in Loop: Header=BB23_92 Depth=1
	movq	%rax, %r14
.LBB23_98:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB23_92 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB23_92
# BB#84:                                # %list_Delete.exit.preheader
	testq	%r14, %r14
	je	.LBB23_83
# BB#85:                                # %.lr.ph97.preheader
	xorl	%r15d, %r15d
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB23_86:                              # %.lr.ph97
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	callq	res_HasTautology
	testl	%eax, %eax
	je	.LBB23_88
# BB#87:                                #   in Loop: Header=BB23_86 Depth=1
	movq	8(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB23_88:                              # %list_Delete.exit
                                        #   in Loop: Header=BB23_86 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_86
# BB#89:                                # %.preheader
	testq	%r15, %r15
	je	.LBB23_90
# BB#99:
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB23_100:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %r14
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_100
	jmp	.LBB23_101
.LBB23_104:                             # %cnf_SubsumeClauseList.exit.thread
	movq	%r14, %rdi
	callq	st_IndexDelete
.LBB23_83:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.LBB23_101:                             # %._crit_edge
	movq	%r15, %rdi
	callq	clause_DeleteClauseList
.LBB23_102:                             # %list_Delete.exit36
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_68:
	xorl	%r15d, %r15d
	movb	$1, %r13b
	jmp	.LBB23_75
.LBB23_90:
	xorl	%r15d, %r15d
	jmp	.LBB23_101
.Lfunc_end23:
	.size	cnf_MakeClauseList, .Lfunc_end23-cnf_MakeClauseList
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_SatUnit,@function
cnf_SatUnit:                            # @cnf_SatUnit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi181:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi182:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi184:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi185:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi187:
	.cfi_def_cfa_offset 96
.Lcfi188:
	.cfi_offset %rbx, -56
.Lcfi189:
	.cfi_offset %r12, -48
.Lcfi190:
	.cfi_offset %r13, -40
.Lcfi191:
	.cfi_offset %r14, -32
.Lcfi192:
	.cfi_offset %r15, -24
.Lcfi193:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	104(%rbp), %r13
	movq	112(%rbp), %r12
	movl	220(%r12), %r14d
	movq	$0, 24(%rsp)
	movq	%rsi, %rdi
	callq	clause_ListSortWeighed
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB24_1
# BB#2:                                 # %.lr.ph.lr.ph
	movq	%rbp, 8(%rsp)           # 8-byte Spill
.LBB24_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_4 Depth 2
                                        #     Child Loop BB24_19 Depth 2
                                        #     Child Loop BB24_26 Depth 2
                                        #     Child Loop BB24_31 Depth 2
                                        #     Child Loop BB24_37 Depth 2
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB24_4:                               #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rbx
	movq	%r15, %rcx
	movq	8(%rcx), %rsi
	movq	(%rax), %r15
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	%rbp, %rdi
	movl	red_ALL(%rip), %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB24_5
# BB#6:                                 #   in Loop: Header=BB24_4 Depth=2
	cmpl	$0, 68(%r13)
	jne	.LBB24_12
# BB#7:                                 #   in Loop: Header=BB24_4 Depth=2
	cmpl	$0, 72(%r13)
	jne	.LBB24_12
# BB#8:                                 # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB24_4 Depth=2
	cmpl	$0, 64(%r13)
	jne	.LBB24_12
# BB#9:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%rsp)
	jmp	.LBB24_10
	.p2align	4, 0x90
.LBB24_5:                               # %.list_Delete.exit.backedge_crit_edge
                                        #   in Loop: Header=BB24_4 Depth=2
	movq	24(%rsp), %rax
.LBB24_10:                              # %list_Delete.exit.backedge
                                        #   in Loop: Header=BB24_4 Depth=2
	testq	%r15, %r15
	movq	%rbx, %r13
	je	.LBB24_36
# BB#11:                                # %list_Delete.exit.backedge
                                        #   in Loop: Header=BB24_4 Depth=2
	testq	%rax, %rax
	movq	%r15, %rax
	je	.LBB24_4
	jmp	.LBB24_36
	.p2align	4, 0x90
.LBB24_12:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	red_USABLE(%rip), %edx
	callq	red_BackReduction
	testl	%r14d, %r14d
	je	.LBB24_13
# BB#14:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	movq	48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %r14
	movq	%r14, %rcx
	movq	%rbx, %rbp
	movq	%rbp, %r8
	callq	inf_BoundedDepthUnitResolution
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rax), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rcx
	movq	%rbp, %r8
	callq	inf_BoundedDepthUnitResolution
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB24_15
# BB#16:                                #   in Loop: Header=BB24_3 Depth=1
	testq	%rax, %rax
	je	.LBB24_17
# BB#18:                                # %.preheader.i60.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rdi, %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	4(%rsp), %r14d          # 4-byte Reload
	.p2align	4, 0x90
.LBB24_19:                              # %.preheader.i60
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB24_19
# BB#20:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB24_21
	.p2align	4, 0x90
.LBB24_13:                              #   in Loop: Header=BB24_3 Depth=1
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB24_22
.LBB24_15:                              #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, %rdi
.LBB24_17:                              #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB24_21:                              # %list_Nconc.exit62
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rdi, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	list_Length
	subl	%eax, %r14d
	movl	$0, %eax
	cmovbl	%eax, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
.LBB24_22:                              #   in Loop: Header=BB24_3 Depth=1
	testq	%rax, %rax
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	je	.LBB24_23
# BB#24:                                #   in Loop: Header=BB24_3 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB24_28
# BB#25:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB24_26:                              # %.preheader.i
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB24_26
# BB#27:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%rsi, (%rdx)
	jmp	.LBB24_28
	.p2align	4, 0x90
.LBB24_23:                              #   in Loop: Header=BB24_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB24_28:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, %rdi
	leaq	24(%rsp), %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %r14
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	prfs_InsertUsableClause
	testq	%r14, %r14
	je	.LBB24_29
# BB#30:                                # %.lr.ph77.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%r14, %rbp
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB24_31:                              # %.lr.ph77
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	clause_InsertWeighed
	movq	%rax, %r15
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB24_31
# BB#32:                                # %._crit_edge
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r14, %r14
	je	.LBB24_33
	.p2align	4, 0x90
.LBB24_37:                              # %.lr.ph.i
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB24_37
.LBB24_33:                              #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB24_34
	.p2align	4, 0x90
.LBB24_29:                              #   in Loop: Header=BB24_3 Depth=1
	movq	%rbx, %r13
.LBB24_34:                              # %list_Delete.exit.outer.backedge
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r15, %r15
	movl	4(%rsp), %r14d          # 4-byte Reload
	je	.LBB24_36
# BB#35:                                # %list_Delete.exit.outer.backedge
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB24_3
	jmp	.LBB24_36
.LBB24_1:
	xorl	%r15d, %r15d
.LBB24_36:                              # %.critedge
	movq	%r15, %rdi
	callq	clause_DeleteClauseList
	movq	24(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	cnf_SatUnit, .Lfunc_end24-cnf_SatUnit
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.zero	16
	.text
	.globl	cnf_GetSkolemFunctions
	.p2align	4, 0x90
	.type	cnf_GetSkolemFunctions,@function
cnf_GetSkolemFunctions:                 # @cnf_GetSkolemFunctions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi197:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 64
.Lcfi201:
	.cfi_offset %rbx, -56
.Lcfi202:
	.cfi_offset %r12, -48
.Lcfi203:
	.cfi_offset %r13, -40
.Lcfi204:
	.cfi_offset %r14, -32
.Lcfi205:
	.cfi_offset %r15, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%r14), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%eax, %ecx
	movl	fol_EXIST(%rip), %edx
	je	.LBB25_2
.LBB25_1:
	cmpl	%eax, %edx
	jne	.LBB25_3
.LBB25_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	movl	(%r14), %eax
	cmpl	%eax, %ecx
	jne	.LBB25_1
	jmp	.LBB25_2
.LBB25_3:                               # %tailrecurse._crit_edge
	testl	%eax, %eax
	jns	.LBB25_32
# BB#4:                                 # %symbol_IsFunction.exit
	negl	%eax
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%eax, %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB25_32
# BB#5:
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testb	$1, 20(%rax)
	jne	.LBB25_6
.LBB25_32:                              # %symbol_IsFunction.exit.thread
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB25_35
	.p2align	4, 0x90
.LBB25_33:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	cnf_GetSkolemFunctions
	movq	%rax, %rbx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB25_33
.LBB25_35:                              # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_6:
	movq	16(%r14), %rdi
	callq	list_Length
	movl	%eax, %r15d
	movq	%rbx, %rdi
	callq	list_Length
	cmpl	%eax, %r15d
	jbe	.LBB25_8
# BB#7:
	movl	$term_Delete, %esi
	movq	%rbx, %rdi
	callq	list_DeleteWithElement
	movq	16(%r14), %rbx
	jmp	.LBB25_9
.LBB25_8:
	movq	16(%r14), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
.LBB25_9:
	movq	$0, 16(%r14)
	movq	(%r12), %rdi
	callq	list_Length
	movq	(%r12), %r13
	cmpl	%r15d, %eax
	jbe	.LBB25_20
# BB#10:                                # %.preheader
	testl	%r15d, %r15d
	movq	%r13, %rax
	je	.LBB25_16
# BB#11:                                # %.lr.ph126.preheader
	leal	-1(%r15), %ecx
	movl	%r15d, %esi
	xorl	%edx, %edx
	movq	%r13, %rax
	andl	$7, %esi
	je	.LBB25_13
	.p2align	4, 0x90
.LBB25_12:                              # %.lr.ph126.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%edx
	movq	(%rax), %rax
	cmpl	%edx, %esi
	jne	.LBB25_12
.LBB25_13:                              # %.lr.ph126.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB25_16
# BB#14:                                # %.lr.ph126.preheader.new
	movl	%r15d, %ecx
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB25_15:                              # %.lr.ph126
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	addl	$-8, %ecx
	jne	.LBB25_15
.LBB25_16:                              # %._crit_edge
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB25_20
# BB#17:                                # %.lr.ph120.preheader
	movl	(%r14), %ecx
	.p2align	4, 0x90
.LBB25_18:                              # %.lr.ph120
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	cmpl	%ecx, 8(%rdx)
	je	.LBB25_36
# BB#19:                                #   in Loop: Header=BB25_18 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB25_18
.LBB25_20:                              # %.critedge.thread.preheader
	testl	%r15d, %r15d
	je	.LBB25_21
	.p2align	4, 0x90
.LBB25_22:                              # %.lr.ph115
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %r12
	testq	%r12, %r12
	jne	.LBB25_24
# BB#23:                                #   in Loop: Header=BB25_22 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	%r12, (%r13)
.LBB25_24:                              # %.critedge.thread
                                        #   in Loop: Header=BB25_22 Depth=1
	decl	%r15d
	movq	%r12, %r13
	jne	.LBB25_22
	jmp	.LBB25_25
.LBB25_21:
	movq	%r13, %r12
.LBB25_25:                              # %.critedge.thread._crit_edge
	movslq	symbol_STANDARDVARCOUNTER(%rip), %r13
	incq	%r13
	movl	%r13d, symbol_STANDARDVARCOUNTER(%rip)
	movslq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%r13, (%r15)
	movq	8(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	testq	%rbp, %rbp
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	je	.LBB25_26
# BB#27:
	testq	%rax, %rax
	je	.LBB25_31
# BB#28:                                # %.preheader.i.preheader
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB25_29:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB25_29
# BB#30:
	movq	%rax, (%rcx)
	jmp	.LBB25_31
.LBB25_26:
	movq	%rax, %rbp
.LBB25_31:                              # %.sink.split
	movq	%rbp, 8(%r12)
	movl	%r13d, (%r14)
	jmp	.LBB25_35
.LBB25_36:                              # %.critedge
	movl	(%rdx), %eax
	movl	%eax, (%r14)
	jmp	.LBB25_35
.Lfunc_end25:
	.size	cnf_GetSkolemFunctions, .Lfunc_end25-cnf_GetSkolemFunctions
	.cfi_endproc

	.globl	cnf_ReplaceVariable
	.p2align	4, 0x90
	.type	cnf_ReplaceVariable,@function
cnf_ReplaceVariable:                    # @cnf_ReplaceVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 32
.Lcfi210:
	.cfi_offset %rbx, -32
.Lcfi211:
	.cfi_offset %r14, -24
.Lcfi212:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	cmpl	%ebp, (%rdi)
	jne	.LBB26_2
# BB#1:
	movl	%r14d, (%rdi)
	jmp	.LBB26_5
.LBB26_2:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_5
	.p2align	4, 0x90
.LBB26_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	cnf_ReplaceVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_3
.LBB26_5:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end26:
	.size	cnf_ReplaceVariable, .Lfunc_end26-cnf_ReplaceVariable
	.cfi_endproc

	.globl	cnf_RemoveSkolemFunctions
	.p2align	4, 0x90
	.type	cnf_RemoveSkolemFunctions,@function
cnf_RemoveSkolemFunctions:              # @cnf_RemoveSkolemFunctions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi216:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi217:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi219:
	.cfi_def_cfa_offset 64
.Lcfi220:
	.cfi_offset %rbx, -56
.Lcfi221:
	.cfi_offset %r12, -48
.Lcfi222:
	.cfi_offset %r13, -40
.Lcfi223:
	.cfi_offset %r14, -32
.Lcfi224:
	.cfi_offset %r15, -24
.Lcfi225:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r13
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB27_1
# BB#2:                                 # %.lr.ph69
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB27_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	cnf_GetSkolemFunctions
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	incq	%r12
	movslq	68(%r13), %rsi
	movslq	72(%r13), %rcx
	movslq	64(%r13), %rdx
	addq	%rsi, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r12
	movq	%rax, %r15
	jl	.LBB27_3
# BB#4:                                 # %._crit_edge70
	testq	%rbx, %rbx
	je	.LBB27_21
# BB#5:                                 # %.preheader.lr.ph
	testq	%rax, %rax
	movq	%rax, %r12
	movq	%rbx, %r15
	movq	%rbx, %rbp
	je	.LBB27_14
	.p2align	4, 0x90
.LBB27_6:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_7 Depth 2
                                        #     Child Loop BB27_11 Depth 2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB27_7:                               #   Parent Loop BB27_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movl	8(%r14), %edx
	callq	cnf_ReplaceVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB27_7
# BB#8:                                 # %._crit_edge
                                        #   in Loop: Header=BB27_6 Depth=1
	cmpq	$0, (%r14)
	jne	.LBB27_13
# BB#9:                                 #   in Loop: Header=BB27_6 Depth=1
	movslq	symbol_STANDARDVARCOUNTER(%rip), %rbx
	incq	%rbx
	movl	%ebx, symbol_STANDARDVARCOUNTER(%rip)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB27_13
# BB#10:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB27_6 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB27_11:                              # %.preheader.i
                                        #   Parent Loop BB27_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB27_11
# BB#12:                                #   in Loop: Header=BB27_6 Depth=1
	movq	%rax, (%rcx)
.LBB27_13:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB27_6 Depth=1
	movq	(%r14), %r14
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r12, %rax
	jne	.LBB27_6
	jmp	.LBB27_20
	.p2align	4, 0x90
.LBB27_14:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_17 Depth 2
	cmpq	$0, (%r14)
	jne	.LBB27_19
# BB#15:                                #   in Loop: Header=BB27_14 Depth=1
	movslq	symbol_STANDARDVARCOUNTER(%rip), %rbx
	incq	%rbx
	movl	%ebx, symbol_STANDARDVARCOUNTER(%rip)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB27_19
# BB#16:                                # %.preheader.i.us.preheader
                                        #   in Loop: Header=BB27_14 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB27_17:                              # %.preheader.i.us
                                        #   Parent Loop BB27_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB27_17
# BB#18:                                #   in Loop: Header=BB27_14 Depth=1
	movq	%rax, (%rcx)
.LBB27_19:                              # %list_Nconc.exit.us
                                        #   in Loop: Header=BB27_14 Depth=1
	movq	(%r14), %r14
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB27_14
.LBB27_20:                              # %._crit_edge61
	movl	$term_Delete, %esi
	movq	%r15, %rdi
	callq	list_DeleteWithElement
	movq	%r12, %rax
	jmp	.LBB27_21
.LBB27_1:
	xorl	%eax, %eax
.LBB27_21:                              # %._crit_edge70.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	cnf_RemoveSkolemFunctions, .Lfunc_end27-cnf_RemoveSkolemFunctions
	.cfi_endproc

	.globl	cnf_DeSkolemFormula
	.p2align	4, 0x90
	.type	cnf_DeSkolemFormula,@function
cnf_DeSkolemFormula:                    # @cnf_DeSkolemFormula
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi230:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi232:
	.cfi_def_cfa_offset 96
.Lcfi233:
	.cfi_offset %rbx, -56
.Lcfi234:
	.cfi_offset %r12, -48
.Lcfi235:
	.cfi_offset %r13, -40
.Lcfi236:
	.cfi_offset %r14, -32
.Lcfi237:
	.cfi_offset %r15, -24
.Lcfi238:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, 24(%rsp)
	movslq	symbol_STANDARDVARCOUNTER(%rip), %rbx
	incq	%rbx
	movl	%ebx, symbol_STANDARDVARCOUNTER(%rip)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	fol_AND(%rip), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%r13, %r13
	jne	.LBB28_4
	jmp	.LBB28_2
	.p2align	4, 0x90
.LBB28_8:                               #   in Loop: Header=BB28_4 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	16(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 16(%rbx)
	movq	(%r13), %r13
	testq	%r13, %r13
	je	.LBB28_2
.LBB28_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_6 Depth 2
	movq	8(%r13), %rdi
	leaq	24(%rsp), %rsi
	movq	%r15, %rdx
	callq	cnf_RemoveSkolemFunctions
	movl	fol_OR(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB28_8
# BB#5:                                 #   in Loop: Header=BB28_4 Depth=1
	movl	$term_Copy, %esi
	movq	%r12, %rdi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB28_6:                               # %.lr.ph.i109
                                        #   Parent Loop BB28_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB28_6
# BB#7:                                 # %list_Delete.exit110
                                        #   in Loop: Header=BB28_4 Depth=1
	movl	fol_ALL(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %r14
	jmp	.LBB28_8
.LBB28_2:                               # %.preheader123
	movq	24(%rsp), %r12
	testq	%r12, %r12
	je	.LBB28_3
# BB#9:                                 # %.lr.ph132.preheader
	movl	$1, %r14d
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB28_10:                              # %.lr.ph132
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_12 Depth 2
                                        #     Child Loop BB28_14 Depth 2
                                        #     Child Loop BB28_24 Depth 2
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.LBB28_18
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB28_10 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_12:                              # %.lr.ph
                                        #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rax
	movl	(%rax), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%rbx, (%r15)
	movq	8(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%r13), %r13
	testq	%r13, %r13
	movq	%r15, %rbx
	jne	.LBB28_12
# BB#13:                                # %._crit_edge
                                        #   in Loop: Header=BB28_10 Depth=1
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.LBB28_15
	.p2align	4, 0x90
.LBB28_14:                              # %.lr.ph.i104
                                        #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB28_14
.LBB28_15:                              # %list_Delete.exit105
                                        #   in Loop: Header=BB28_10 Depth=1
	movq	$0, 8(%r12)
	movl	fol_EXIST(%rip), %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	cmpl	%ebp, (%rbx)
	jne	.LBB28_27
# BB#16:                                #   in Loop: Header=BB28_10 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB28_17
# BB#22:                                #   in Loop: Header=BB28_10 Depth=1
	testq	%r15, %r15
	je	.LBB28_26
# BB#23:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB28_10 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB28_24:                              # %.preheader.i
                                        #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB28_24
# BB#25:                                #   in Loop: Header=BB28_10 Depth=1
	movq	%r15, (%rdx)
	jmp	.LBB28_26
	.p2align	4, 0x90
.LBB28_18:                              #   in Loop: Header=BB28_10 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	(%rbx), %eax
	cmpl	fol_ALL(%rip), %eax
	jne	.LBB28_20
# BB#19:                                #   in Loop: Header=BB28_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %rbp
	movq	16(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 16(%rbx)
.LBB28_20:                              #   in Loop: Header=BB28_10 Depth=1
	testl	%r14d, %r14d
	jne	.LBB28_31
# BB#21:                                #   in Loop: Header=BB28_10 Depth=1
	movl	fol_ALL(%rip), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	8(%rbp), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r13, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	36(%rsp), %edi          # 4-byte Reload
	jmp	.LBB28_30
	.p2align	4, 0x90
.LBB28_27:                              #   in Loop: Header=BB28_10 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbx
	movq	8(%rsp), %r15           # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB28_31
	jmp	.LBB28_29
.LBB28_17:                              #   in Loop: Header=BB28_10 Depth=1
	movq	%r15, %rcx
.LBB28_26:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB28_10 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%rcx, 16(%rax)
	testl	%r14d, %r14d
	jne	.LBB28_31
.LBB28_29:                              #   in Loop: Header=BB28_10 Depth=1
	movl	fol_ALL(%rip), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	8(%rbp), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r13, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	(%rsp), %edi            # 4-byte Reload
.LBB28_30:                              #   in Loop: Header=BB28_10 Depth=1
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbx
	movq	(%rbp), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
.LBB28_31:                              #   in Loop: Header=BB28_10 Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	(%r12), %r12
	xorl	%r14d, %r14d
	testq	%r12, %r12
	jne	.LBB28_10
# BB#32:                                # %._crit_edge133
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	je	.LBB28_34
	.p2align	4, 0x90
.LBB28_33:                              # %.lr.ph.i99
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rdi, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rdi
	jne	.LBB28_33
	jmp	.LBB28_34
.LBB28_3:
	movq	(%rsp), %rax            # 8-byte Reload
.LBB28_34:                              # %list_Delete.exit100
	testq	%r15, %r15
	je	.LBB28_36
	.p2align	4, 0x90
.LBB28_35:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r15
	jne	.LBB28_35
.LBB28_36:                              # %list_Delete.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	cnf_DeSkolemFormula, .Lfunc_end28-cnf_DeSkolemFormula
	.cfi_endproc

	.p2align	4, 0x90
	.type	list_PairFree,@function
list_PairFree:                          # @list_PairFree
	.cfi_startproc
# BB#0:
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end29:
	.size	list_PairFree, .Lfunc_end29-list_PairFree
	.cfi_endproc

	.globl	cnf_Flotter
	.p2align	4, 0x90
	.type	cnf_Flotter,@function
cnf_Flotter:                            # @cnf_Flotter
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi245:
	.cfi_def_cfa_offset 320
.Lcfi246:
	.cfi_offset %rbx, -56
.Lcfi247:
	.cfi_offset %r12, -48
.Lcfi248:
	.cfi_offset %r13, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	328(%rsp), %rbp
	movq	320(%rsp), %rbx
	callq	prfs_Create
	movq	%rax, %r14
	movq	112(%r14), %r13
	movd	flag_CLEAN(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%r13)
	movdqu	%xmm0, 16(%r13)
	movdqu	%xmm0, 32(%r13)
	movdqu	%xmm0, 48(%r13)
	movdqu	%xmm0, 64(%r13)
	movdqu	%xmm0, 80(%r13)
	movdqu	%xmm0, 96(%r13)
	movdqu	%xmm0, 112(%r13)
	movdqu	%xmm0, 128(%r13)
	movdqu	%xmm0, 144(%r13)
	movdqu	%xmm0, 160(%r13)
	movdqu	%xmm0, 176(%r13)
	movdqu	%xmm0, 192(%r13)
	movdqu	%xmm0, 208(%r13)
	movdqu	%xmm0, 224(%r13)
	movdqu	%xmm0, 240(%r13)
	movdqu	%xmm0, 256(%r13)
	movdqu	%xmm0, 272(%r13)
	movdqu	%xmm0, 288(%r13)
	movdqu	%xmm0, 304(%r13)
	movdqu	%xmm0, 320(%r13)
	movdqu	%xmm0, 336(%r13)
	movdqu	%xmm0, 352(%r13)
	movdqu	%xmm0, 368(%r13)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	flag_InitFlotterFlags
	movq	%rbp, %rdx
	movq	104(%r14), %rsi
	leaq	16000(%rsi), %rax
	leaq	16000(%rdx), %rdi
	cmpq	%rdi, %rsi
	movq	%rax, 144(%rsp)         # 8-byte Spill
	jae	.LBB30_4
# BB#1:                                 # %vector.body
	cmpq	%rdx, %rax
	jbe	.LBB30_4
# BB#2:                                 # %scalar.ph440.preheader
	movl	$7, %eax
	.p2align	4, 0x90
.LBB30_3:                               # %scalar.ph440
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx,%rax,4), %ecx
	movl	%ecx, -28(%rsi,%rax,4)
	movl	-24(%rdx,%rax,4), %ecx
	movl	%ecx, -24(%rsi,%rax,4)
	movl	-20(%rdx,%rax,4), %ecx
	movl	%ecx, -20(%rsi,%rax,4)
	movl	-16(%rdx,%rax,4), %ecx
	movl	%ecx, -16(%rsi,%rax,4)
	movl	-12(%rdx,%rax,4), %ecx
	movl	%ecx, -12(%rsi,%rax,4)
	movl	-8(%rdx,%rax,4), %ecx
	movl	%ecx, -8(%rsi,%rax,4)
	movl	-4(%rdx,%rax,4), %ecx
	movl	%ecx, -4(%rsi,%rax,4)
	movl	(%rdx,%rax,4), %ecx
	movl	%ecx, (%rsi,%rax,4)
	addq	$8, %rax
	cmpq	$4007, %rax             # imm = 0xFA7
	jne	.LBB30_3
	jmp	.LBB30_6
.LBB30_4:                               # %vector.body438.preheader
	movl	$36, %eax
	.p2align	4, 0x90
.LBB30_5:                               # %vector.body438
                                        # =>This Inner Loop Header: Depth=1
	movups	-144(%rdx,%rax,4), %xmm0
	movups	-128(%rdx,%rax,4), %xmm1
	movups	%xmm0, -144(%rsi,%rax,4)
	movups	%xmm1, -128(%rsi,%rax,4)
	movups	-112(%rdx,%rax,4), %xmm0
	movups	-96(%rdx,%rax,4), %xmm1
	movups	%xmm0, -112(%rsi,%rax,4)
	movups	%xmm1, -96(%rsi,%rax,4)
	movups	-80(%rdx,%rax,4), %xmm0
	movups	-64(%rdx,%rax,4), %xmm1
	movups	%xmm0, -80(%rsi,%rax,4)
	movups	%xmm1, -64(%rsi,%rax,4)
	movups	-48(%rdx,%rax,4), %xmm0
	movups	-32(%rdx,%rax,4), %xmm1
	movups	%xmm0, -48(%rsi,%rax,4)
	movups	%xmm1, -32(%rsi,%rax,4)
	movdqu	-16(%rdx,%rax,4), %xmm0
	movups	(%rdx,%rax,4), %xmm1
	movdqu	%xmm0, -16(%rsi,%rax,4)
	movups	%xmm1, (%rsi,%rax,4)
	addq	$40, %rax
	cmpq	$4036, %rax             # imm = 0xFC4
	jne	.LBB30_5
.LBB30_6:                               # %symbol_TransferPrecedence.exit275
	cmpl	$0, 36(%r13)
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	je	.LBB30_9
# BB#7:
	callq	sharing_IndexCreate
	movq	%rax, 88(%r14)
	cmpl	$0, 36(%r13)
	movq	$0, 88(%rsp)
	je	.LBB30_10
# BB#8:
	callq	hsh_Create
	jmp	.LBB30_11
.LBB30_9:                               # %.thread
	movq	$0, 88(%rsp)
.LBB30_10:
	xorl	%eax, %eax
.LBB30_11:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	callq	symbol_ReinitGenericNameCounters
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB30_18
# BB#12:
	leaq	88(%rsp), %r12
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_13:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r15
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	fol_RemoveImplied
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movq	%rbx, %rdi
	callq	fol_NormalizeVars
	cmpl	$0, 232(%r13)
	je	.LBB30_15
# BB#14:                                #   in Loop: Header=BB30_13 Depth=1
	movq	%rbx, %rdi
	callq	cnf_PropagateSubstEquations
.LBB30_15:                              #   in Loop: Header=BB30_13 Depth=1
	movq	%rbx, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbx
	cmpl	$0, 224(%r13)
	je	.LBB30_17
# BB#16:                                #   in Loop: Header=BB30_13 Depth=1
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movl	228(%r13), %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	ren_Rename
	movq	%rax, %rbx
.LBB30_17:                              #   in Loop: Header=BB30_13 Depth=1
	movq	%rbx, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, (%r15)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB30_13
.LBB30_18:                              # %.preheader374
	movq	48(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%r14, 40(%rsp)          # 8-byte Spill
	je	.LBB30_36
# BB#19:                                # %.lr.ph404
	leaq	160(%rsp), %r14
	movq	%rax, %rbp
	xorl	%r12d, %r12d
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB30_20
	.p2align	4, 0x90
.LBB30_21:                              #   in Loop: Header=BB30_20 Depth=1
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r14, %rbx
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	string_StringCopy
	movq	%rax, %rcx
	movq	8(%rbp), %rax
	movq	%rcx, 8(%rax)
	cmpl	$0, 36(%r13)
	je	.LBB30_24
# BB#22:                                #   in Loop: Header=BB30_20 Depth=1
	cmpl	$0, 100(%r13)
	je	.LBB30_24
# BB#23:                                #   in Loop: Header=BB30_20 Depth=1
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	callq	fol_PrettyPrintDFG
	jmp	.LBB30_24
	.p2align	4, 0x90
.LBB30_20:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	cmpq	$0, 8(%rax)
	je	.LBB30_21
.LBB30_24:                              #   in Loop: Header=BB30_20 Depth=1
	movq	8(%rbp), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdi
	callq	fol_RemoveImplied
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movq	%rbx, %rdi
	callq	fol_NormalizeVars
	cmpl	$0, 232(%r13)
	je	.LBB30_26
# BB#25:                                #   in Loop: Header=BB30_20 Depth=1
	movq	%rbx, %rdi
	callq	cnf_PropagateSubstEquations
.LBB30_26:                              #   in Loop: Header=BB30_20 Depth=1
	movq	%rbx, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbx
	cmpl	$0, 224(%r13)
	je	.LBB30_28
# BB#27:                                #   in Loop: Header=BB30_20 Depth=1
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movl	228(%r13), %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	88(%rsp), %rdx
	callq	ren_Rename
	movq	%rax, %rbx
.LBB30_28:                              #   in Loop: Header=BB30_20 Depth=1
	movq	%rbx, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	8(%rbp), %rcx
	movq	%rax, (%rcx)
	incl	%r12d
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB30_20
# BB#29:                                # %._crit_edge405
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB30_37
# BB#30:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	list_Copy
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB30_34
# BB#31:                                # %.preheader.i332.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB30_32:                              # %.preheader.i332
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB30_32
# BB#33:                                # %list_Append.exit
	testq	%rdi, %rdi
	movq	%rsi, (%rcx)
	je	.LBB30_38
.LBB30_34:                              # %.lr.ph400.preheader
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB30_35:                              # %.lr.ph400
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_35
	jmp	.LBB30_38
.LBB30_36:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB30_38
.LBB30_37:
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB30_38:                              # %._crit_edge401
	movq	$0, 160(%rsp)
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	movq	%r13, 80(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB30_65
# BB#39:                                # %.lr.ph398.preheader
	xorl	%ebx, %ebx
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB30_40:                              # %.lr.ph398
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_42 Depth 2
                                        #     Child Loop BB30_46 Depth 2
                                        #       Child Loop BB30_50 Depth 3
                                        #       Child Loop BB30_53 Depth 3
                                        #     Child Loop BB30_58 Depth 2
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	movq	(%rax), %rdi
	callq	term_Copy
	movq	%rax, %rdi
	movq	%r15, %rsi
	leaq	160(%rsp), %rdx
	callq	cnf_SkolemFormula
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_43
# BB#41:                                # %.lr.ph.i327.preheader
                                        #   in Loop: Header=BB30_40 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB30_42:                              # %.lr.ph.i327
                                        #   Parent Loop BB30_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	fol_OR(%rip), %edi
	movq	8(%rbp), %rsi
	callq	term_Create
	movq	%rax, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB30_42
.LBB30_43:                              # %cnf_DistributiveFormula.exit
                                        #   in Loop: Header=BB30_40 Depth=1
	movl	fol_AND(%rip), %edi
	movq	%rbx, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_Delete
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$0, 36(%rbx)
	je	.LBB30_55
# BB#44:                                # %cnf_DistributiveFormula.exit
                                        #   in Loop: Header=BB30_40 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB30_55
# BB#45:                                # %.lr.ph394
                                        #   in Loop: Header=BB30_40 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_46:                              #   Parent Loop BB30_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_50 Depth 3
                                        #       Child Loop BB30_53 Depth 3
	movq	8(%r15), %r12
	movq	8(%r13), %rax
	movq	8(%rax), %rbx
	movq	%r12, %rax
	movabsq	$1908283869694091547, %rcx # imm = 0x1A7B9611A7B9611B
	mulq	%rcx
	movq	%r12, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rcx
	movq	%r12, %rax
	subq	%rcx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB30_47
	.p2align	4, 0x90
.LBB30_50:                              # %.lr.ph.i324
                                        #   Parent Loop BB30_40 Depth=1
                                        #     Parent Loop BB30_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %r14
	cmpq	%r12, 8(%r14)
	je	.LBB30_51
# BB#49:                                #   in Loop: Header=BB30_50 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_50
.LBB30_47:                              # %._crit_edge.i325
                                        #   in Loop: Header=BB30_46 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	%rbp, (%rbx)
	jmp	.LBB30_48
	.p2align	4, 0x90
.LBB30_51:                              #   in Loop: Header=BB30_46 Depth=2
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB30_48
	.p2align	4, 0x90
.LBB30_53:                              # %.lr.ph.i.i
                                        #   Parent Loop BB30_40 Depth=1
                                        #     Parent Loop BB30_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB30_54
# BB#52:                                #   in Loop: Header=BB30_53 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_53
.LBB30_48:                              # %list_PointerMember.exit.sink.split.i
                                        #   in Loop: Header=BB30_46 Depth=2
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%r14)
.LBB30_54:                              # %hsh_Put.exit
                                        #   in Loop: Header=BB30_46 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB30_46
.LBB30_55:                              # %.loopexit373
                                        #   in Loop: Header=BB30_40 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmoveq	%rsi, %rbx
	je	.LBB30_60
# BB#56:                                # %.loopexit373
                                        #   in Loop: Header=BB30_40 Depth=1
	testq	%rsi, %rsi
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	je	.LBB30_61
# BB#57:                                # %.preheader.i318.preheader
                                        #   in Loop: Header=BB30_40 Depth=1
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB30_58:                              # %.preheader.i318
                                        #   Parent Loop BB30_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_58
# BB#59:                                #   in Loop: Header=BB30_40 Depth=1
	movq	%rsi, (%rax)
	movq	%rdx, %rbx
	jmp	.LBB30_61
	.p2align	4, 0x90
.LBB30_60:                              #   in Loop: Header=BB30_40 Depth=1
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
.LBB30_61:                              # %list_Nconc.exit320
                                        #   in Loop: Header=BB30_40 Depth=1
	callq	term_Delete
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB30_40
# BB#62:                                # %.preheader371
	testq	%rbx, %rbx
	je	.LBB30_65
# BB#63:
	movq	%rbx, %rax
	movq	40(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_64:                              # %.lr.ph391
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	orl	$8, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_64
	jmp	.LBB30_66
.LBB30_65:
	xorl	%ebx, %ebx
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB30_66:                              # %._crit_edge392
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	red_SatUnit
	testq	%rax, %rax
	je	.LBB30_68
# BB#67:
	movq	%rax, %rdi
	callq	clause_DeleteClauseList
	movl	$1, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
.LBB30_68:
	movq	56(%r14), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_72
# BB#69:                                # %.lr.ph388.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB30_70:                              # %.lr.ph388
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	prfs_MoveUsableWorkedOff
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_70
	.p2align	4, 0x90
.LBB30_71:                              # %.lr.ph.i312
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB30_71
.LBB30_72:                              # %list_Delete.exit314
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	336(%rsp), %rax
	movq	$0, (%rax)
	je	.LBB30_128
# BB#73:                                # %.lr.ph385
	movabsq	$1908283869694091547, %r13 # imm = 0x1A7B9611A7B9611B
	movq	24(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_74:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_76 Depth 2
                                        #     Child Loop BB30_79 Depth 2
                                        #     Child Loop BB30_82 Depth 2
                                        #     Child Loop BB30_86 Depth 2
                                        #     Child Loop BB30_90 Depth 2
                                        #     Child Loop BB30_96 Depth 2
                                        #       Child Loop BB30_99 Depth 3
                                        #       Child Loop BB30_103 Depth 3
                                        #       Child Loop BB30_109 Depth 3
                                        #       Child Loop BB30_113 Depth 3
                                        #     Child Loop BB30_120 Depth 2
                                        #     Child Loop BB30_126 Depth 2
	movq	$0, 64(%rsp)
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	8(%rax), %r15
	movq	(%r15), %rdi
	callq	term_Copy
	movq	8(%r15), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	leaq	64(%rsp), %rcx
	movq	336(%rsp), %r8
	movq	104(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	cnf_OptimizedSkolemization
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB30_80
# BB#75:                                # %.lr.ph.i306.preheader
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB30_76:                              # %.lr.ph.i306
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, 8(%rcx)
	je	.LBB30_78
# BB#77:                                #   in Loop: Header=BB30_76 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_76
	jmp	.LBB30_80
	.p2align	4, 0x90
.LBB30_78:                              # %list_PointerMember.exit.preheader
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB30_80
	.p2align	4, 0x90
.LBB30_79:                              # %list_PointerMember.exit
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	orl	$8, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_79
.LBB30_80:                              # %list_PointerMember.exit.thread
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	je	.LBB30_117
# BB#81:                                #   in Loop: Header=BB30_74 Depth=1
	movq	8(%r15), %r14
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	list_Copy
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	strlen
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB30_82:                              #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%r14,%rcx), %rdx
	addq	%rdx, %r12
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB30_82
# BB#83:                                # %hsh_StringHashKey.exit.i
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	%r12, %rax
	mulq	%r13
	movq	%r12, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	subq	%rax, %r12
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rbx
	testq	%rbx, %rbx
	je	.LBB30_84
	.p2align	4, 0x90
.LBB30_86:                              # %.lr.ph.i298
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	8(%rbp), %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB30_87
# BB#85:                                #   in Loop: Header=BB30_86 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_86
.LBB30_84:                              # %._crit_edge.i305
                                        #   in Loop: Header=BB30_74 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%r12,8), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, (%rbx,%r12,8)
	jmp	.LBB30_93
.LBB30_87:                              #   in Loop: Header=BB30_74 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB30_154
# BB#88:                                #   in Loop: Header=BB30_74 Depth=1
	movq	72(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB30_92
# BB#89:                                # %.preheader.i.i302.preheader
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB30_90:                              # %.preheader.i.i302
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB30_90
# BB#91:                                #   in Loop: Header=BB30_74 Depth=1
	movq	%rsi, (%rcx)
	jmp	.LBB30_92
.LBB30_154:                             #   in Loop: Header=BB30_74 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB30_92:                              # %list_Nconc.exit.i304
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	%rax, (%rbp)
.LBB30_93:                              # %hsh_PutListWithCompareFunc.exit
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	8(%r15), %rbx
	movq	64(%rsp), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 64(%rsp)
	movl	$cnf_LabelEqual, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %rdi
	movq	%rdi, 64(%rsp)
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB30_117
# BB#94:                                # %.lr.ph383.preheader
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB30_96
	.p2align	4, 0x90
.LBB30_95:                              # %hsh_PutList.exit..lr.ph383_crit_edge
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	64(%rsp), %rdi
.LBB30_96:                              # %.lr.ph383
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_99 Depth 3
                                        #       Child Loop BB30_103 Depth 3
                                        #       Child Loop BB30_109 Depth 3
                                        #       Child Loop BB30_113 Depth 3
	movq	8(%r15), %rbp
	callq	list_Copy
	movq	%rax, %r12
	movq	%rbp, %rax
	mulq	%r13
	movq	%rbp, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rbp, %rbx
	subq	%rax, %rbx
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB30_97
	.p2align	4, 0x90
.LBB30_99:                              # %.lr.ph.i284
                                        #   Parent Loop BB30_74 Depth=1
                                        #     Parent Loop BB30_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rax
	cmpq	%rbp, 8(%rax)
	je	.LBB30_100
# BB#98:                                #   in Loop: Header=BB30_99 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_99
.LBB30_97:                              # %._crit_edge.i293
                                        #   in Loop: Header=BB30_96 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	%r12, (%r14)
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%rbx,8), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, (%rbp,%rbx,8)
	jmp	.LBB30_106
	.p2align	4, 0x90
.LBB30_100:                             #   in Loop: Header=BB30_96 Depth=2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB30_122
# BB#101:                               #   in Loop: Header=BB30_96 Depth=2
	testq	%r12, %r12
	je	.LBB30_105
# BB#102:                               # %.preheader.i.i290.preheader
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB30_103:                             # %.preheader.i.i290
                                        #   Parent Loop BB30_74 Depth=1
                                        #     Parent Loop BB30_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB30_103
# BB#104:                               #   in Loop: Header=BB30_96 Depth=2
	movq	%r12, (%rdx)
	jmp	.LBB30_105
.LBB30_122:                             #   in Loop: Header=BB30_96 Depth=2
	movq	%r12, %rcx
.LBB30_105:                             # %list_Nconc.exit.i292
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	%rcx, (%rax)
.LBB30_106:                             # %hsh_PutList.exit294
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	8(%r15), %rbp
	movq	64(%rsp), %rdi
	callq	list_Copy
	movq	%rax, %r12
	movq	%rbp, %rax
	mulq	%r13
	movq	%rbp, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rbp, %rbx
	subq	%rax, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB30_107
	.p2align	4, 0x90
.LBB30_109:                             # %.lr.ph.i271
                                        #   Parent Loop BB30_74 Depth=1
                                        #     Parent Loop BB30_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rax
	cmpq	%rbp, 8(%rax)
	je	.LBB30_110
# BB#108:                               #   in Loop: Header=BB30_109 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_109
.LBB30_107:                             # %._crit_edge.i
                                        #   in Loop: Header=BB30_96 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	%r12, (%r14)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbp
	movq	(%rbp,%rbx,8), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, (%rbp,%rbx,8)
	jmp	.LBB30_116
	.p2align	4, 0x90
.LBB30_110:                             #   in Loop: Header=BB30_96 Depth=2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB30_123
# BB#111:                               #   in Loop: Header=BB30_96 Depth=2
	testq	%r12, %r12
	je	.LBB30_115
# BB#112:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB30_113:                             # %.preheader.i.i
                                        #   Parent Loop BB30_74 Depth=1
                                        #     Parent Loop BB30_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB30_113
# BB#114:                               #   in Loop: Header=BB30_96 Depth=2
	movq	%r12, (%rdx)
	jmp	.LBB30_115
.LBB30_123:                             #   in Loop: Header=BB30_96 Depth=2
	movq	%r12, %rcx
.LBB30_115:                             # %list_Nconc.exit.i
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	%rcx, (%rax)
.LBB30_116:                             # %hsh_PutList.exit
                                        #   in Loop: Header=BB30_96 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB30_95
.LBB30_117:                             # %.loopexit
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB30_124
# BB#118:                               #   in Loop: Header=BB30_74 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	je	.LBB30_125
# BB#119:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB30_120:                             # %.preheader.i
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB30_120
# BB#121:                               #   in Loop: Header=BB30_74 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx)
	jmp	.LBB30_125
	.p2align	4, 0x90
.LBB30_124:                             #   in Loop: Header=BB30_74 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB30_125:                             # %list_Nconc.exit
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx)
	movq	64(%rsp), %rax
	testq	%rax, %rax
	je	.LBB30_127
	.p2align	4, 0x90
.LBB30_126:                             # %.lr.ph.i269
                                        #   Parent Loop BB30_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB30_126
.LBB30_127:                             # %list_Delete.exit270
                                        #   in Loop: Header=BB30_74 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_74
.LBB30_128:                             # %vector.memcheck470
	movq	328(%rsp), %rdx
	cmpq	%rdx, 144(%rsp)         # 8-byte Folded Reload
	jbe	.LBB30_132
# BB#129:                               # %vector.memcheck470
	cmpq	136(%rsp), %r15         # 8-byte Folded Reload
	jae	.LBB30_132
# BB#130:                               # %.preheader.preheader
	movl	$7, %eax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_131:                             # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%r15,%rax,4), %ecx
	movl	%ecx, -28(%rdx,%rax,4)
	movl	-24(%r15,%rax,4), %ecx
	movl	%ecx, -24(%rdx,%rax,4)
	movl	-20(%r15,%rax,4), %ecx
	movl	%ecx, -20(%rdx,%rax,4)
	movl	-16(%r15,%rax,4), %ecx
	movl	%ecx, -16(%rdx,%rax,4)
	movl	-12(%r15,%rax,4), %ecx
	movl	%ecx, -12(%rdx,%rax,4)
	movl	-8(%r15,%rax,4), %ecx
	movl	%ecx, -8(%rdx,%rax,4)
	movl	-4(%r15,%rax,4), %ecx
	movl	%ecx, -4(%rdx,%rax,4)
	movl	(%r15,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	addq	$8, %rax
	cmpq	$4007, %rax             # imm = 0xFA7
	jne	.LBB30_131
	jmp	.LBB30_134
.LBB30_132:                             # %vector.body457.preheader
	movl	$36, %eax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_133:                             # %vector.body457
                                        # =>This Inner Loop Header: Depth=1
	movups	-144(%r15,%rax,4), %xmm0
	movups	-128(%r15,%rax,4), %xmm1
	movups	%xmm0, -144(%rdx,%rax,4)
	movups	%xmm1, -128(%rdx,%rax,4)
	movups	-112(%r15,%rax,4), %xmm0
	movups	-96(%r15,%rax,4), %xmm1
	movups	%xmm0, -112(%rdx,%rax,4)
	movups	%xmm1, -96(%rdx,%rax,4)
	movups	-80(%r15,%rax,4), %xmm0
	movups	-64(%r15,%rax,4), %xmm1
	movups	%xmm0, -80(%rdx,%rax,4)
	movups	%xmm1, -64(%rdx,%rax,4)
	movups	-48(%r15,%rax,4), %xmm0
	movups	-32(%r15,%rax,4), %xmm1
	movups	%xmm0, -48(%rdx,%rax,4)
	movups	%xmm1, -32(%rdx,%rax,4)
	movdqu	-16(%r15,%rax,4), %xmm0
	movups	(%r15,%rax,4), %xmm1
	movdqu	%xmm0, -16(%rdx,%rax,4)
	movups	%xmm1, (%rdx,%rax,4)
	addq	$40, %rax
	cmpq	$4036, %rax             # imm = 0xFC4
	jne	.LBB30_133
.LBB30_134:                             # %symbol_TransferPrecedence.exit
	testq	%rsi, %rsi
	je	.LBB30_136
	.p2align	4, 0x90
.LBB30_135:                             # %.lr.ph.i261
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB30_135
.LBB30_136:                             # %list_Delete.exit262
	cmpl	$0, 36(%rbp)
	je	.LBB30_138
# BB#137:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	hsh_Delete
.LBB30_138:
	cmpl	$0, 8(%rbp)
	jne	.LBB30_141
# BB#139:
	movq	336(%rsp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB30_141
	.p2align	4, 0x90
.LBB30_140:                             # %.lr.ph.i256
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB30_140
.LBB30_141:                             # %list_Delete.exit257
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movq	152(%rsp), %r12         # 8-byte Reload
	movq	$0, (%r12)
	je	.LBB30_146
# BB#142:                               # %.lr.ph
	movq	%rcx, %r13
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB30_143:                             # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movq	(%rbp), %rdi
	callq	term_Delete
	movq	8(%rbp), %r14
	movq	(%r12), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_143
# BB#144:                               # %.lr.ph.i251.preheader
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_145:                             # %.lr.ph.i251
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB30_145
.LBB30_146:                             # %list_Delete.exit252
	movq	160(%rsp), %rax
	testq	%rax, %rax
	je	.LBB30_148
	.p2align	4, 0x90
.LBB30_147:                             # %.lr.ph.i246
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB30_147
.LBB30_148:                             # %list_Delete.exit247
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB30_150
	.p2align	4, 0x90
.LBB30_149:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB30_149
.LBB30_150:                             # %list_Delete.exit
	cmpl	$0, 8(%rbp)
	je	.LBB30_152
# BB#151:
	movq	%rbx, %rdi
	callq	prfs_DeleteDocProof
	jmp	.LBB30_153
.LBB30_152:
	movl	$0, symbol_ACTSKOLEMFINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMCINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMPINDEX(%rip)
	movl	$0, symbol_ACTSKOLEMAINDEX(%rip)
	movq	%rbx, %rdi
	callq	prfs_Delete
	xorl	%ebx, %ebx
.LBB30_153:
	movq	%rbx, %rax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	cnf_Flotter, .Lfunc_end30-cnf_Flotter
	.cfi_endproc

	.globl	cnf_PropagateSubstEquations
	.p2align	4, 0x90
	.type	cnf_PropagateSubstEquations,@function
cnf_PropagateSubstEquations:            # @cnf_PropagateSubstEquations
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi256:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi258:
	.cfi_def_cfa_offset 64
.Lcfi259:
	.cfi_offset %rbx, -56
.Lcfi260:
	.cfi_offset %r12, -48
.Lcfi261:
	.cfi_offset %r13, -40
.Lcfi262:
	.cfi_offset %r14, -32
.Lcfi263:
	.cfi_offset %r15, -24
.Lcfi264:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	fol_GetSubstEquations
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB31_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB31_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movq	16(%rbp), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB31_6
# BB#4:                                 #   in Loop: Header=BB31_3 Depth=1
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_ContainsVariable
	testl	%eax, %eax
	je	.LBB31_5
.LBB31_6:                               # %.thread
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB31_13
# BB#7:                                 #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rax), %rdi
	callq	term_ContainsVariable
	testl	%eax, %eax
	jne	.LBB31_13
# BB#8:                                 #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	fol_GetBindingQuantifier
	movq	%rax, %r12
	movq	16(%rbp), %rax
	movq	8(%rax), %r15
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	fol_PolarCheck
	testl	%eax, %eax
	jne	.LBB31_9
	jmp	.LBB31_13
	.p2align	4, 0x90
.LBB31_5:                               #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	fol_GetBindingQuantifier
	movq	%rax, %r12
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r15
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	fol_PolarCheck
	testl	%eax, %eax
	je	.LBB31_6
.LBB31_9:                               # %.thread76
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	fol_DeleteQuantifierVariable
	movq	%r14, %rdi
	movl	%r13d, %esi
	movq	%r15, %rdx
	callq	term_ReplaceVariable
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	movl	(%r12), %eax
	cmpl	fol_EQUALITY(%rip), %eax
	jne	.LBB31_11
# BB#10:                                #   in Loop: Header=BB31_3 Depth=1
	movq	%r12, %rdi
	jmp	.LBB31_12
.LBB31_11:                              #   in Loop: Header=BB31_3 Depth=1
	movq	%rbp, %rdi
.LBB31_12:                              # %.thread75
                                        #   in Loop: Header=BB31_3 Depth=1
	callq	fol_SetTrue
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
.LBB31_13:                              # %.thread75
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB31_3
	jmp	.LBB31_14
.LBB31_1:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB31_14:                              # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	cnf_PropagateSubstEquations, .Lfunc_end31-cnf_PropagateSubstEquations
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_RemoveEquivImplFromFormula,@function
cnf_RemoveEquivImplFromFormula:         # @cnf_RemoveEquivImplFromFormula
	.cfi_startproc
# BB#0:                                 # %.lr.ph74.preheader
	pushq	%rbp
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi269:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi270:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi271:
	.cfi_def_cfa_offset 112
.Lcfi272:
	.cfi_offset %rbx, -56
.Lcfi273:
	.cfi_offset %r12, -48
.Lcfi274:
	.cfi_offset %r13, -40
.Lcfi275:
	.cfi_offset %r14, -32
.Lcfi276:
	.cfi_offset %r15, -24
.Lcfi277:
	.cfi_offset %rbp, -16
	movslq	vec_MAX(%rip), %r13
	leal	1(%r13), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, vec_VECTOR(,%r13,8)
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB32_1:                               # %.lr.ph74
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_6 Depth 2
                                        #       Child Loop BB32_20 Depth 3
                                        #     Child Loop BB32_31 Depth 2
	movslq	%eax, %rcx
	leal	-1(%rcx), %r10d
	movl	%r10d, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rcx,8), %r15
	movl	(%r15), %r8d
	movl	fol_IMPLIES(%rip), %r9d
	cmpl	%r9d, %r8d
	jne	.LBB32_3
# BB#2:                                 #   in Loop: Header=BB32_1 Depth=1
	movl	fol_OR(%rip), %eax
	movl	%eax, (%r15)
	movq	16(%r15), %rbp
	addq	$16, %r15
	movl	fol_NOT(%rip), %r14d
	movq	8(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 8(%rbp)
	movq	%r15, %r12
	jmp	.LBB32_29
	.p2align	4, 0x90
.LBB32_3:                               #   in Loop: Header=BB32_1 Depth=1
	cmpl	fol_EQUIV(%rip), %r8d
	jne	.LBB32_4
# BB#5:                                 #   in Loop: Header=BB32_1 Depth=1
	leaq	-1(%rcx), %rsi
	movq	$1, vec_VECTOR(,%rsi,8)
	leal	1(%rcx), %ebp
	movl	%ebp, vec_MAX(%rip)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, vec_VECTOR(,%rcx,8)
	.p2align	4, 0x90
.LBB32_6:                               # %.loopexit._crit_edge.i
                                        #   Parent Loop BB32_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB32_20 Depth 3
	movslq	%ebp, %rbx
	movq	vec_VECTOR-8(,%rbx,8), %rdi
	leal	-2(%rbx), %esi
	movl	%esi, vec_MAX(%rip)
	movq	vec_VECTOR-16(,%rbx,8), %r13
	cmpq	%r15, %rdi
	je	.LBB32_23
# BB#7:                                 #   in Loop: Header=BB32_6 Depth=2
	movl	(%rdi), %ecx
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB32_9
# BB#8:                                 #   in Loop: Header=BB32_6 Depth=2
	leal	-1(%rbx), %ecx
	leaq	-1(%rbx), %rsi
	movq	%rbx, %rdx
	addq	$-2, %rdx
	movl	%r13d, %eax
	negl	%eax
	cltq
	movl	%ecx, vec_MAX(%rip)
	movq	%rax, vec_VECTOR(,%rdx,8)
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	%ebx, vec_MAX(%rip)
	movq	%rax, vec_VECTOR(,%rsi,8)
	movl	(%rdi), %ecx
	movl	%ebp, %esi
.LBB32_9:                               #   in Loop: Header=BB32_6 Depth=2
	cmpl	fol_EXIST(%rip), %ecx
	je	.LBB32_11
# BB#10:                                #   in Loop: Header=BB32_6 Depth=2
	cmpl	fol_ALL(%rip), %ecx
	je	.LBB32_11
# BB#12:                                #   in Loop: Header=BB32_6 Depth=2
	cmpl	%r9d, %ecx
	jne	.LBB32_14
# BB#13:                                #   in Loop: Header=BB32_6 Depth=2
	movslq	%r13d, %rax
	negl	%r13d
	movslq	%r13d, %rcx
	leal	1(%rsi), %edx
	movl	%edx, vec_MAX(%rip)
	movslq	%esi, %rdx
	movq	%rcx, vec_VECTOR(,%rdx,8)
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, vec_VECTOR+8(,%rdx,8)
	leal	3(%rsi), %ecx
	movl	%ecx, vec_MAX(%rip)
	movq	%rax, vec_VECTOR+16(,%rdx,8)
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	addl	$4, %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rax, vec_VECTOR+24(,%rdx,8)
	jmp	.LBB32_21
	.p2align	4, 0x90
.LBB32_11:                              #   in Loop: Header=BB32_6 Depth=2
	movslq	%r13d, %rax
	leal	1(%rsi), %ecx
	movl	%ecx, vec_MAX(%rip)
	movslq	%esi, %rcx
	movq	%rax, vec_VECTOR(,%rcx,8)
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	addl	$2, %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rax, vec_VECTOR+8(,%rcx,8)
	jmp	.LBB32_21
	.p2align	4, 0x90
.LBB32_14:                              #   in Loop: Header=BB32_6 Depth=2
	cmpl	%r8d, %ecx
	jne	.LBB32_16
# BB#15:                                #   in Loop: Header=BB32_6 Depth=2
	leal	1(%rsi), %eax
	movl	%eax, vec_MAX(%rip)
	movslq	%esi, %rax
	movq	$0, vec_VECTOR(,%rax,8)
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, vec_VECTOR+8(,%rax,8)
	leal	3(%rsi), %ecx
	movl	%ecx, vec_MAX(%rip)
	movq	$0, vec_VECTOR+16(,%rax,8)
	movq	16(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	addl	$4, %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rcx, vec_VECTOR+24(,%rax,8)
	jmp	.LBB32_21
.LBB32_16:                              #   in Loop: Header=BB32_6 Depth=2
	cmpl	fol_AND(%rip), %ecx
	je	.LBB32_18
# BB#17:                                #   in Loop: Header=BB32_6 Depth=2
	cmpl	fol_OR(%rip), %ecx
	jne	.LBB32_21
.LBB32_18:                              #   in Loop: Header=BB32_6 Depth=2
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB32_21
# BB#19:                                # %.lr.ph.i
                                        #   in Loop: Header=BB32_6 Depth=2
	movslq	%r13d, %rcx
	movslq	%esi, %rax
	leaq	vec_VECTOR+8(,%rax,8), %rbp
	.p2align	4, 0x90
.LBB32_20:                              #   Parent Loop BB32_1 Depth=1
                                        #     Parent Loop BB32_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	1(%rsi), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rcx, -8(%rbp)
	movq	8(%rdi), %rax
	addl	$2, %esi
	movl	%esi, vec_MAX(%rip)
	movq	%rax, (%rbp)
	movq	(%rdi), %rdi
	addq	$16, %rbp
	testq	%rdi, %rdi
	jne	.LBB32_20
	.p2align	4, 0x90
.LBB32_21:                              # %.loopexit.i
                                        #   in Loop: Header=BB32_6 Depth=2
	cmpl	%esi, %r10d
	movl	%esi, %ebp
	jne	.LBB32_6
	jmp	.LBB32_22
	.p2align	4, 0x90
.LBB32_4:                               # %._crit_edge80
                                        #   in Loop: Header=BB32_1 Depth=1
	addq	$16, %r15
	movq	%r15, %r12
	jmp	.LBB32_29
	.p2align	4, 0x90
.LBB32_23:                              # %cnf_GetFormulaPolarity.exit
                                        #   in Loop: Header=BB32_1 Depth=1
	movl	%r10d, vec_MAX(%rip)
	leaq	16(%r15), %r12
	movq	16(%r15), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %r14
	movl	fol_NOT(%rip), %ebp
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	fol_NOT(%rip), %ebx
	movq	%r14, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movl	%r13d, %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB32_25
# BB#24:                                #   in Loop: Header=BB32_1 Depth=1
	movl	fol_AND(%rip), %ecx
	movl	%ecx, (%r15)
	movq	16(%r15), %r13
	movl	fol_OR(%rip), %ebx
	movl	$16, %edi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 8(%r13)
	movq	16(%r15), %r15
	movl	fol_OR(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%ebx, %edi
	jmp	.LBB32_27
.LBB32_25:                              #   in Loop: Header=BB32_1 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpl	$-1, %r13d
	jne	.LBB32_28
# BB#26:                                #   in Loop: Header=BB32_1 Depth=1
	movl	fol_OR(%rip), %ecx
	movl	%ecx, (%r15)
	movq	16(%r15), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	fol_AND(%rip), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	$16, %edi
	movq	%rax, %r13
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r13, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	28(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	16(%r15), %r15
	movl	fol_AND(%rip), %r13d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%r13d, %edi
.LBB32_27:                              #   in Loop: Header=BB32_1 Depth=1
	movq	%rax, %rsi
	callq	term_Create
	movq	(%r15), %rcx
	movq	%rax, 8(%rcx)
.LBB32_28:                              #   in Loop: Header=BB32_1 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB32_29:                              #   in Loop: Header=BB32_1 Depth=1
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	movl	vec_MAX(%rip), %eax
	je	.LBB32_32
# BB#30:                                #   in Loop: Header=BB32_1 Depth=1
	cltq
	.p2align	4, 0x90
.LBB32_31:                              # %.lr.ph
                                        #   Parent Loop BB32_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	%rdx, vec_VECTOR(,%rax,8)
	incq	%rax
	movl	%eax, vec_MAX(%rip)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB32_31
.LBB32_32:                              # %.backedge
                                        #   in Loop: Header=BB32_1 Depth=1
	cmpl	%eax, %r13d
	jne	.LBB32_1
# BB#33:                                # %._crit_edge
	movl	%r13d, vec_MAX(%rip)
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_22:
	movl	%r10d, vec_MAX(%rip)
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	movl	$176, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end32:
	.size	cnf_RemoveEquivImplFromFormula, .Lfunc_end32-cnf_RemoveEquivImplFromFormula
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_AntiPrenex,@function
cnf_AntiPrenex:                         # @cnf_AntiPrenex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi280:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi281:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi282:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi283:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi284:
	.cfi_def_cfa_offset 112
.Lcfi285:
	.cfi_offset %rbx, -56
.Lcfi286:
	.cfi_offset %r12, -48
.Lcfi287:
	.cfi_offset %r13, -40
.Lcfi288:
	.cfi_offset %r14, -32
.Lcfi289:
	.cfi_offset %r15, -24
.Lcfi290:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	(%rbp), %r13d
	movl	fol_ALL(%rip), %edx
	cmpl	%r13d, %edx
	movl	fol_EXIST(%rip), %ecx
	je	.LBB33_2
# BB#1:
	cmpl	%r13d, %ecx
	je	.LBB33_2
# BB#54:
	cmpl	fol_NOT(%rip), %r13d
	je	.LBB33_60
# BB#55:
	testl	%r13d, %r13d
	jns	.LBB33_57
# BB#56:                                # %symbol_IsPredicate.exit91
	negl	%r13d
	andl	symbol_TYPEMASK(%rip), %r13d
	cmpl	$2, %r13d
	je	.LBB33_60
.LBB33_57:                              # %symbol_IsPredicate.exit91.thread
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB33_60
	.p2align	4, 0x90
.LBB33_58:                              # %.lr.ph119
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_AntiPrenex
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB33_58
	jmp	.LBB33_60
.LBB33_2:
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r15
	movl	(%r15), %eax
	testl	%eax, %eax
	jns	.LBB33_4
# BB#3:                                 # %symbol_IsPredicate.exit
	movl	%eax, %esi
	negl	%esi
	andl	symbol_TYPEMASK(%rip), %esi
	cmpl	$2, %esi
	je	.LBB33_60
.LBB33_4:                               # %symbol_IsPredicate.exit.thread
	cmpl	fol_NOT(%rip), %eax
	je	.LBB33_60
# BB#5:
	cmpl	%r13d, %edx
	movl	$fol_AND, %esi
	movl	$fol_OR, %edi
	cmoveq	%rsi, %rdi
	movl	(%rdi), %ebx
	cmpl	%eax, %edx
	je	.LBB33_7
# BB#6:
	cmpl	%eax, %ecx
	jne	.LBB33_8
.LBB33_7:
	movq	%r15, %rdi
	callq	cnf_AntiPrenex
	movl	(%r15), %eax
.LBB33_8:
	cmpl	%ebx, %eax
	jne	.LBB33_26
# BB#9:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %r14
	movq	%r15, %rdi
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	callq	cnf_Flatten
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rax), %r15
	testq	%r15, %r15
	jne	.LBB33_11
	jmp	.LBB33_21
	.p2align	4, 0x90
.LBB33_20:                              #   in Loop: Header=BB33_11 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB33_21
.LBB33_11:                              # %.lr.ph113
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_16 Depth 2
	movq	8(%r15), %rbx
	movq	%rbx, %rdi
	callq	fol_FreeVariables
	movl	$term_Equal, %edx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	list_NIntersect
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB33_20
# BB#12:                                #   in Loop: Header=BB33_11 Depth=1
	movl	(%rbx), %r12d
	movl	$term_Copy, %esi
	movq	%rbp, %rdi
	callq	list_NMapCar
	cmpl	%r12d, %r13d
	jne	.LBB33_19
# BB#13:                                #   in Loop: Header=BB33_11 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB33_14
# BB#15:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB33_11 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB33_16:                              # %.preheader.i
                                        #   Parent Loop BB33_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB33_16
# BB#17:                                #   in Loop: Header=BB33_11 Depth=1
	movq	%rbp, (%rdx)
	movq	%rcx, 16(%rax)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB33_11
	jmp	.LBB33_21
	.p2align	4, 0x90
.LBB33_19:                              #   in Loop: Header=BB33_11 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, 8(%r15)
	jmp	.LBB33_20
.LBB33_14:                              #   in Loop: Header=BB33_11 Depth=1
	movq	%rbp, %rcx
	movq	%rcx, 16(%rax)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB33_11
.LBB33_21:                              # %._crit_edge
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB33_23
	.p2align	4, 0x90
.LBB33_22:                              # %.lr.ph.i94
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB33_22
.LBB33_23:                              # %list_Delete.exit
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbp)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movq	%rax, 16(%rbp)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdx, (%rax)
	movq	16(%rbp), %rbx
	jmp	.LBB33_25
	.p2align	4, 0x90
.LBB33_24:                              # %.lr.ph
                                        #   in Loop: Header=BB33_25 Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_AntiPrenex
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
.LBB33_25:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jne	.LBB33_24
	jmp	.LBB33_60
.LBB33_26:
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB33_60
# BB#27:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB33_60
# BB#28:
	movl	(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movl	(%rdi), %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	callq	cnf_Flatten
	movq	%rax, %r13
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB33_45
# BB#29:                                # %.lr.ph117.i
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB33_30:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_32 Depth 2
                                        #     Child Loop BB33_39 Depth 2
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB33_44
# BB#31:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB33_30 Depth=1
	movq	8(%r15), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB33_32:                              # %.lr.ph.i
                                        #   Parent Loop BB33_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fol_VarOccursFreely
	testl	%eax, %eax
	jne	.LBB33_34
# BB#33:                                #   in Loop: Header=BB33_32 Depth=2
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB33_34:                              #   in Loop: Header=BB33_32 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB33_32
# BB#35:                                # %._crit_edge.i
                                        #   in Loop: Header=BB33_30 Depth=1
	testq	%r12, %r12
	je	.LBB33_44
# BB#36:                                #   in Loop: Header=BB33_30 Depth=1
	movq	%r13, %rbp
	movq	16(%r13), %rdi
	movq	%r12, %rsi
	callq	list_NPointerDifference
	movq	%rax, %r13
	cmpq	$0, (%r13)
	je	.LBB33_37
# BB#41:                                #   in Loop: Header=BB33_30 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%r13, %rsi
	callq	term_Create
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rbx, %rsi
	movq	%rax, %rdx
	jmp	.LBB33_42
.LBB33_37:                              #   in Loop: Header=BB33_30 Depth=1
	movq	8(%r13), %rbx
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	(%rbx), %eax
	jne	.LBB33_40
# BB#38:                                #   in Loop: Header=BB33_30 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 16(%rcx)
	.p2align	4, 0x90
.LBB33_39:                              # %.lr.ph.i102.i
                                        #   Parent Loop BB33_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB33_39
	jmp	.LBB33_43
.LBB33_40:                              #   in Loop: Header=BB33_30 Depth=1
	movl	%eax, %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	movq	%r13, %rdx
.LBB33_42:                              # %list_Delete.exit103.i
                                        #   in Loop: Header=BB33_30 Depth=1
	callq	fol_CreateQuantifier
	movq	%rax, %rbx
.LBB33_43:                              # %list_Delete.exit103.i
                                        #   in Loop: Header=BB33_30 Depth=1
	movq	%rbp, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 16(%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbx)
.LBB33_44:                              # %._crit_edge.thread.i
                                        #   in Loop: Header=BB33_30 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB33_30
.LBB33_45:                              # %._crit_edge118.i
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	cmpq	$0, 16(%rax)
	movq	32(%rsp), %rdi          # 8-byte Reload
	jne	.LBB33_49
# BB#46:
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB33_48
	.p2align	4, 0x90
.LBB33_47:                              # %.lr.ph.i93.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB33_47
.LBB33_48:                              # %list_Delete.exit94.i
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbp)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbp)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r13, (%rax)
.LBB33_49:
	testq	%rdi, %rdi
	je	.LBB33_51
	.p2align	4, 0x90
.LBB33_50:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB33_50
.LBB33_51:                              # %cnf_DistrQuantorNoVarSub.exit.preheader
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB33_60
	.p2align	4, 0x90
.LBB33_53:                              # %cnf_DistrQuantorNoVarSub.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_AntiPrenex
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB33_53
.LBB33_60:                              # %.loopexit
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	cnf_AntiPrenex, .Lfunc_end33-cnf_AntiPrenex
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_SkolemFormula,@function
cnf_SkolemFormula:                      # @cnf_SkolemFormula
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi291:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi292:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi294:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi295:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi296:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi297:
	.cfi_def_cfa_offset 80
.Lcfi298:
	.cfi_offset %rbx, -56
.Lcfi299:
	.cfi_offset %r12, -48
.Lcfi300:
	.cfi_offset %r13, -40
.Lcfi301:
	.cfi_offset %r14, -32
.Lcfi302:
	.cfi_offset %r15, -24
.Lcfi303:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	(%r14), %eax
	cmpl	%eax, fol_ALL(%rip)
	sete	%cl
	je	.LBB34_2
# BB#1:
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB34_9
.LBB34_2:                               # %.lr.ph109
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB34_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_17 Depth 2
                                        #     Child Loop BB34_19 Depth 2
                                        #     Child Loop BB34_22 Depth 2
                                        #     Child Loop BB34_6 Depth 2
	testb	$1, %cl
	je	.LBB34_15
# BB#4:                                 #   in Loop: Header=BB34_3 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB34_7
# BB#5:                                 # %.lr.ph.i88.preheader
                                        #   in Loop: Header=BB34_3 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB34_7
	.p2align	4, 0x90
.LBB34_6:                               # %.lr.ph.i88..lr.ph.i88_crit_edge
                                        #   Parent Loop BB34_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB34_6
	jmp	.LBB34_7
	.p2align	4, 0x90
.LBB34_15:                              #   in Loop: Header=BB34_3 Depth=1
	movq	%r14, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	list_Length
	movq	16(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	%rbp, %rbx
	movq	16(%rcx), %r15
	testq	%r15, %r15
	je	.LBB34_18
# BB#16:                                # %.lr.ph106.preheader
                                        #   in Loop: Header=BB34_3 Depth=1
	movl	%eax, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB34_17:                              # %.lr.ph106
                                        #   Parent Loop BB34_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	callq	symbol_CreateSkolemFunction
	movslq	%eax, %rbp
	movq	%r14, %r12
	movq	(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%r12, %r14
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%rax, (%rbx)
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	8(%r15), %rax
	movl	(%rax), %esi
	movq	%rbp, %rdx
	callq	fol_ReplaceVariable
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rbp, (%rcx)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB34_17
.LBB34_18:                              # %._crit_edge
                                        #   in Loop: Header=BB34_3 Depth=1
	testq	%r13, %r13
	je	.LBB34_20
	.p2align	4, 0x90
.LBB34_19:                              # %.lr.ph.i83
                                        #   Parent Loop BB34_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB34_19
.LBB34_20:                              # %list_Delete.exit84
                                        #   in Loop: Header=BB34_3 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	movq	%rbx, %rbp
	je	.LBB34_7
# BB#21:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB34_3 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB34_7
	.p2align	4, 0x90
.LBB34_22:                              # %.lr.ph.i..lr.ph.i_crit_edge
                                        #   Parent Loop BB34_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB34_22
	.p2align	4, 0x90
.LBB34_7:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB34_3 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, (%r14)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movl	(%r14), %eax
	cmpl	%eax, fol_ALL(%rip)
	sete	%cl
	movl	fol_EXIST(%rip), %edx
	je	.LBB34_3
# BB#8:                                 # %tailrecurse.backedge
                                        #   in Loop: Header=BB34_3 Depth=1
	cmpl	%eax, %edx
	je	.LBB34_3
.LBB34_9:                               # %tailrecurse._crit_edge
	cmpl	fol_AND(%rip), %eax
	je	.LBB34_11
# BB#10:                                # %tailrecurse._crit_edge
	cmpl	fol_OR(%rip), %eax
	jne	.LBB34_14
.LBB34_11:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB34_14
	.p2align	4, 0x90
.LBB34_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	cnf_SkolemFormula
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB34_12
.LBB34_14:                              # %.loopexit
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	cnf_SkolemFormula, .Lfunc_end34-cnf_SkolemFormula
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_OptimizedSkolemization,@function
cnf_OptimizedSkolemization:             # @cnf_OptimizedSkolemization
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi304:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi305:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi306:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi307:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi308:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi310:
	.cfi_def_cfa_offset 96
.Lcfi311:
	.cfi_offset %rbx, -56
.Lcfi312:
	.cfi_offset %r12, -48
.Lcfi313:
	.cfi_offset %r13, -40
.Lcfi314:
	.cfi_offset %r14, -32
.Lcfi315:
	.cfi_offset %r15, -24
.Lcfi316:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movq	%r8, %rbp
	movq	%rcx, %r13
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	104(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	112(%r12), %r15
	cmpl	$0, 216(%r15)
	je	.LBB35_2
# BB#1:                                 # %.preheader
	movq	cnf_VARIABLEDEPTHARRAY(%rip), %rdi
	addq	$4, %rdi
	movl	$255, %esi
	movl	$8000, %edx             # imm = 0x1F40
	callq	memset
.LBB35_2:                               # %.loopexit
	cmpl	$0, 132(%r15)
	jne	.LBB35_4
# BB#3:
	cmpl	$0, 136(%r15)
	je	.LBB35_5
.LBB35_4:
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	fol_PrettyPrintDFG
.LBB35_5:
	movl	(%r14), %eax
	testl	%eax, %eax
	jns	.LBB35_7
# BB#6:                                 # %symbol_IsPredicate.exit6.i
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB35_16
.LBB35_7:                               # %symbol_IsPredicate.exit6.thread.i
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB35_10
# BB#8:
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB35_10
# BB#9:                                 # %fol_IsLiteral.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB35_16
.LBB35_10:                              # %fol_IsLiteral.exit.thread
	cmpl	$0, 212(%r15)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB35_11
# BB#14:                                # %.thread
	movq	%rbx, %r13
	movq	%rbp, %rbx
	movl	fol_AND(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rbx, %rbp
	movq	%r13, %rbx
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r13
	jmp	.LBB35_15
.LBB35_11:
	cmpl	$0, 216(%r15)
	movq	%r14, %r13
	je	.LBB35_12
.LBB35_15:
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %r8
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi317:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
.Lcfi318:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi319:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi320:
	.cfi_adjust_cfa_offset 8
	callq	cnf_OptimizedSkolemFormula
	addq	$32, %rsp
.Lcfi321:
	.cfi_adjust_cfa_offset -32
	movq	%r13, %r14
.LBB35_16:                              # %fol_IsLiteral.exit.thread3
	cmpl	$0, 132(%r15)
	jne	.LBB35_18
# BB#17:
	cmpl	$0, 136(%r15)
	je	.LBB35_19
.LBB35_18:
	movq	stdout(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	term_Print
.LBB35_19:
	movq	%r14, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB35_22
# BB#20:                                # %.lr.ph.i.preheader
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB35_21:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_OR(%rip), %edi
	movq	8(%rbp), %rsi
	callq	term_Create
	movq	%rax, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB35_21
.LBB35_22:                              # %cnf_DistributiveFormula.exit
	movl	fol_AND(%rip), %edi
	movq	%rbx, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	term_Delete
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	cnf_MakeClauseList
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	term_Delete
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_12:
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movq	%r14, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	cnf_SkolemFormula
	movq	%rax, %r14
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB35_16
	.p2align	4, 0x90
.LBB35_13:                              # %.lr.ph.i2
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB35_13
	jmp	.LBB35_16
.Lfunc_end35:
	.size	cnf_OptimizedSkolemization, .Lfunc_end35-cnf_OptimizedSkolemization
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_LabelEqual,@function
cnf_LabelEqual:                         # @cnf_LabelEqual
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi322:
	.cfi_def_cfa_offset 16
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end36:
	.size	cnf_LabelEqual, .Lfunc_end36-cnf_LabelEqual
	.cfi_endproc

	.globl	cnf_QueryFlotter
	.p2align	4, 0x90
	.type	cnf_QueryFlotter,@function
cnf_QueryFlotter:                       # @cnf_QueryFlotter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi323:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi324:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi325:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi326:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi327:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi328:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi329:
	.cfi_def_cfa_offset 112
.Lcfi330:
	.cfi_offset %rbx, -56
.Lcfi331:
	.cfi_offset %r12, -48
.Lcfi332:
	.cfi_offset %r13, -40
.Lcfi333:
	.cfi_offset %r14, -32
.Lcfi334:
	.cfi_offset %r15, -24
.Lcfi335:
	.cfi_offset %rbp, -16
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	104(%r14), %rbx
	movq	112(%r14), %r12
	movq	cnf_SEARCHCOPY(%rip), %rax
	movq	112(%rax), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB37_1:                               # =>This Inner Loop Header: Depth=1
	callq	flag_DefaultStore
	movl	(%rax,%rbp,4), %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	flag_SetFlagValue
	incq	%rbp
	cmpq	$96, %rbp
	jne	.LBB37_1
# BB#2:                                 # %flag_InitStoreByDefaults.exit
	movl	36(%r12), %edx
	movl	$9, %esi
	movq	%r15, %rdi
	callq	flag_SetFlagValue
	movq	cnf_SEARCHCOPY(%rip), %rsi
	movq	104(%rsi), %rax
	leaq	16000(%rbx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB37_4
# BB#3:                                 # %flag_InitStoreByDefaults.exit
	leaq	16000(%rax), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB37_4
# BB#6:                                 # %scalar.ph.preheader
	movl	$7, %ecx
	.p2align	4, 0x90
.LBB37_7:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbx,%rcx,4), %edx
	movl	%edx, -28(%rax,%rcx,4)
	movl	-24(%rbx,%rcx,4), %edx
	movl	%edx, -24(%rax,%rcx,4)
	movl	-20(%rbx,%rcx,4), %edx
	movl	%edx, -20(%rax,%rcx,4)
	movl	-16(%rbx,%rcx,4), %edx
	movl	%edx, -16(%rax,%rcx,4)
	movl	-12(%rbx,%rcx,4), %edx
	movl	%edx, -12(%rax,%rcx,4)
	movl	-8(%rbx,%rcx,4), %edx
	movl	%edx, -8(%rax,%rcx,4)
	movl	-4(%rbx,%rcx,4), %edx
	movl	%edx, -4(%rax,%rcx,4)
	movl	(%rbx,%rcx,4), %edx
	movl	%edx, (%rax,%rcx,4)
	addq	$8, %rcx
	cmpq	$4007, %rcx             # imm = 0xFA7
	jne	.LBB37_7
	jmp	.LBB37_8
.LBB37_4:                               # %vector.body.preheader
	movl	$36, %ecx
	.p2align	4, 0x90
.LBB37_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-144(%rbx,%rcx,4), %xmm0
	movups	-128(%rbx,%rcx,4), %xmm1
	movups	%xmm0, -144(%rax,%rcx,4)
	movups	%xmm1, -128(%rax,%rcx,4)
	movups	-112(%rbx,%rcx,4), %xmm0
	movups	-96(%rbx,%rcx,4), %xmm1
	movups	%xmm0, -112(%rax,%rcx,4)
	movups	%xmm1, -96(%rax,%rcx,4)
	movups	-80(%rbx,%rcx,4), %xmm0
	movups	-64(%rbx,%rcx,4), %xmm1
	movups	%xmm0, -80(%rax,%rcx,4)
	movups	%xmm1, -64(%rax,%rcx,4)
	movups	-48(%rbx,%rcx,4), %xmm0
	movups	-32(%rbx,%rcx,4), %xmm1
	movups	%xmm0, -48(%rax,%rcx,4)
	movups	%xmm1, -32(%rax,%rcx,4)
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, -16(%rax,%rcx,4)
	movups	%xmm1, (%rax,%rcx,4)
	addq	$40, %rcx
	cmpq	$4036, %rcx             # imm = 0xFC4
	jne	.LBB37_5
.LBB37_8:                               # %symbol_TransferPrecedence.exit
	movq	$0, 16(%rsp)
	movq	$0, 24(%rsp)
	movq	%r14, %rdi
	callq	prfs_CopyIndices
	movl	fol_NOT(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	fol_NormalizeVars
	movq	%rbp, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbp
	cmpl	$0, 224(%r12)
	movq	%r12, 32(%rsp)          # 8-byte Spill
	je	.LBB37_10
# BB#9:
	movq	%rbp, %rdi
	callq	term_AddFatherLinks
	movl	228(%r12), %ecx
	leaq	24(%rsp), %rdx
	movl	$1, %r8d
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	ren_Rename
	movq	%rax, %rbp
.LBB37_10:
	movq	%rbp, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	term_Copy
	leaq	16(%rsp), %rdx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	cnf_SkolemFormula
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB37_13
# BB#11:                                # %.lr.ph.i84.preheader
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB37_12:                              # %.lr.ph.i84
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_OR(%rip), %edi
	movq	8(%rbp), %rsi
	callq	term_Create
	movq	%rax, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB37_12
.LBB37_13:                              # %cnf_DistributiveFormula.exit
	movl	fol_AND(%rip), %edi
	movq	%r14, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	term_Delete
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	term_Delete
	testq	%rbx, %rbx
	je	.LBB37_16
# BB#14:                                # %.lr.ph95.preheader
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB37_15:                              # %.lr.ph95
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	orl	$8, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB37_15
.LBB37_16:                              # %._crit_edge96
	movq	cnf_SEARCHCOPY(%rip), %rdi
	movq	%rbx, %rsi
	callq	red_SatUnit
	testq	%rax, %rax
	je	.LBB37_17
# BB#18:
	movq	%rax, %rdi
	callq	clause_DeleteClauseList
	movl	$1, %r14d
	jmp	.LBB37_20
.LBB37_17:
	xorl	%r14d, %r14d
	jmp	.LBB37_20
	.p2align	4, 0x90
.LBB37_19:                              # %.lr.ph92
                                        #   in Loop: Header=BB37_20 Depth=1
	movq	8(%rax), %rsi
	callq	prfs_MoveUsableWorkedOff
.LBB37_20:                              # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	movq	cnf_SEARCHCOPY(%rip), %rdi
	movq	56(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB37_19
# BB#21:                                # %._crit_edge93
	movq	$0, 48(%rsp)
	movl	36(%r15), %ebx
	movl	$9, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	flag_SetFlagValue
	movq	cnf_SEARCHCOPY(%rip), %rbp
	movq	%r13, %rdi
	callq	term_Copy
	movq	$0, (%rsp)
	leaq	48(%rsp), %rcx
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	%r14d, %r9d
	callq	cnf_OptimizedSkolemization
	movq	%rax, %rbp
	cmpl	$0, %ebx
	je	.LBB37_23
# BB#22:
	movl	$9, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	flag_SetFlagValue
.LBB37_23:
	movq	%r13, %rdi
	callq	term_Delete
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB37_25
	.p2align	4, 0x90
.LBB37_24:                              # %.lr.ph.i79
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB37_24
.LBB37_25:                              # %list_Delete.exit80
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB37_27
	.p2align	4, 0x90
.LBB37_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB37_26
.LBB37_27:                              # %list_Delete.exit
	movq	cnf_SEARCHCOPY(%rip), %rdi
	callq	prfs_Clean
	testq	%rbp, %rbp
	je	.LBB37_30
# BB#28:                                # %.lr.ph.preheader
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB37_29:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	orl	$8, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB37_29
.LBB37_30:                              # %._crit_edge
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	cnf_QueryFlotter, .Lfunc_end37-cnf_QueryFlotter
	.cfi_endproc

	.p2align	4, 0x90
	.type	flag_SetFlagValue,@function
flag_SetFlagValue:                      # @flag_SetFlagValue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi336:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi337:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi338:
	.cfi_def_cfa_offset 32
.Lcfi339:
	.cfi_offset %rbx, -32
.Lcfi340:
	.cfi_offset %r14, -24
.Lcfi341:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB38_1
# BB#3:
	movl	%ebp, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB38_4
# BB#5:                                 # %flag_CheckFlagValueInRange.exit
	movl	%ebp, %eax
	movl	%ebx, (%r14,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB38_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.30, %edi
	jmp	.LBB38_2
.LBB38_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.31, %edi
.LBB38_2:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end38:
	.size	flag_SetFlagValue, .Lfunc_end38-flag_SetFlagValue
	.cfi_endproc

	.globl	cnf_DefTargetConvert
	.p2align	4, 0x90
	.type	cnf_DefTargetConvert,@function
cnf_DefTargetConvert:                   # @cnf_DefTargetConvert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi342:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi343:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi344:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi345:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi346:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi347:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi348:
	.cfi_def_cfa_offset 96
.Lcfi349:
	.cfi_offset %rbx, -56
.Lcfi350:
	.cfi_offset %r12, -48
.Lcfi351:
	.cfi_offset %r13, -40
.Lcfi352:
	.cfi_offset %r14, -32
.Lcfi353:
	.cfi_offset %r15, -24
.Lcfi354:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	120(%rsp), %rbp
	movq	104(%rsp), %rax
	cmpl	$0, 148(%rax)
	je	.LBB39_2
# BB#1:
	movl	$.L.str.11, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	fol_PrettyPrint
.LBB39_2:
	movq	96(%rsp), %r14
	movl	$0, (%rbp)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cnf_RemoveImplFromFormulaPath
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	cnf_NegationNormalFormulaPath
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	cnf_AntiPrenexPath
	movslq	vec_MAX(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, vec_MAX(%rip)
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, vec_VECTOR(,%rcx,8)
	.p2align	4, 0x90
.LBB39_3:                               # %.backedge._crit_edge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_20 Depth 2
                                        #       Child Loop BB39_23 Depth 3
                                        #       Child Loop BB39_27 Depth 3
                                        #     Child Loop BB39_12 Depth 2
                                        #       Child Loop BB39_16 Depth 3
	leal	-1(%rdx), %ecx
	movl	%ecx, vec_MAX(%rip)
	movslq	%edx, %rax
	movq	vec_VECTOR-8(,%rax,8), %r13
	movq	16(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB39_4
# BB#11:                                # %.lr.ph95.i
                                        #   in Loop: Header=BB39_3 Depth=1
	testq	%r14, %r14
	je	.LBB39_12
	.p2align	4, 0x90
.LBB39_20:                              # %.lr.ph95.split.i
                                        #   Parent Loop BB39_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB39_23 Depth 3
                                        #       Child Loop BB39_27 Depth 3
	movq	8(%rbp), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB39_30
# BB#21:                                #   in Loop: Header=BB39_20 Depth=2
	movl	(%r12), %eax
	cmpl	fol_ALL(%rip), %eax
	jne	.LBB39_29
# BB#22:                                # %.lr.ph92.i
                                        #   in Loop: Header=BB39_20 Depth=2
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB39_23:                              #   Parent Loop BB39_3 Depth=1
                                        #     Parent Loop BB39_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rsi
	movl	$term_Equal, %edx
	movl	$term_Delete, %ecx
	movq	%rax, %rdi
	callq	list_DeleteElementFree
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB39_23
# BB#24:                                # %._crit_edge93.i
                                        #   in Loop: Header=BB39_20 Depth=2
	testq	%rax, %rax
	movq	16(%r12), %rcx
	je	.LBB39_26
# BB#25:                                #   in Loop: Header=BB39_20 Depth=2
	movq	8(%rcx), %rcx
	movq	%rax, 16(%rcx)
	jmp	.LBB39_29
.LBB39_26:                              #   in Loop: Header=BB39_20 Depth=2
	movq	(%rcx), %rax
	movq	8(%rcx), %rcx
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+256(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB39_28
	.p2align	4, 0x90
.LBB39_27:                              # %.lr.ph.i.i
                                        #   Parent Loop BB39_3 Depth=1
                                        #     Parent Loop BB39_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB39_27
.LBB39_28:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB39_20 Depth=2
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%r12, (%rcx)
	movq	%rax, 8(%rbp)
	movq	%r13, 8(%rax)
	.p2align	4, 0x90
.LBB39_29:                              #   in Loop: Header=BB39_20 Depth=2
	movq	8(%rbp), %rax
	movslq	vec_MAX(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, vec_MAX(%rip)
	movq	%rax, vec_VECTOR(,%rcx,8)
.LBB39_30:                              #   in Loop: Header=BB39_20 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_20
	jmp	.LBB39_4
	.p2align	4, 0x90
.LBB39_12:                              # %.lr.ph95.split.us.i
                                        #   Parent Loop BB39_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB39_16 Depth 3
	movq	8(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB39_19
# BB#13:                                #   in Loop: Header=BB39_12 Depth=2
	movl	(%rbx), %eax
	cmpl	fol_ALL(%rip), %eax
	jne	.LBB39_18
# BB#14:                                #   in Loop: Header=BB39_12 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rcx
	cmpq	$0, 16(%rcx)
	jne	.LBB39_18
# BB#15:                                #   in Loop: Header=BB39_12 Depth=2
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+256(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB39_17
	.p2align	4, 0x90
.LBB39_16:                              # %.lr.ph.i.us.i
                                        #   Parent Loop BB39_3 Depth=1
                                        #     Parent Loop BB39_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB39_16
.LBB39_17:                              # %list_Delete.exit.us.i
                                        #   in Loop: Header=BB39_12 Depth=2
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rbx, (%rcx)
	movq	%rax, 8(%rbp)
	movq	%r13, 8(%rax)
	.p2align	4, 0x90
.LBB39_18:                              #   in Loop: Header=BB39_12 Depth=2
	movq	8(%rbp), %rax
	movslq	vec_MAX(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, vec_MAX(%rip)
	movq	%rax, vec_VECTOR(,%rcx,8)
.LBB39_19:                              #   in Loop: Header=BB39_12 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_12
.LBB39_4:                               # %.backedge.i
                                        #   in Loop: Header=BB39_3 Depth=1
	movl	vec_MAX(%rip), %edx
	cmpl	%edx, 8(%rsp)           # 4-byte Folded Reload
	jne	.LBB39_3
# BB#5:                                 # %.preheader.i
	testq	%r14, %r14
	je	.LBB39_8
# BB#6:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB39_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Copy
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB39_7
.LBB39_8:                               # %._crit_edge.i
	movl	fol_ALL(%rip), %ebx
	movq	32(%rsp), %r13          # 8-byte Reload
	cmpl	%ebx, (%r13)
	jne	.LBB39_36
# BB#9:
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbx
	movq	%r14, %rdi
	callq	list_Copy
	testq	%rbx, %rbx
	je	.LBB39_10
# BB#31:
	testq	%rax, %rax
	je	.LBB39_35
# BB#32:                                # %.preheader.i.i.preheader
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB39_33:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB39_33
# BB#34:
	movq	%rax, (%rcx)
	jmp	.LBB39_35
.LBB39_36:
	movq	%r14, %rdi
	callq	list_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, 8(%r13)
	movq	%rax, %r13
	jmp	.LBB39_37
.LBB39_10:
	movq	%rax, %rbx
.LBB39_35:                              # %list_Nconc.exit.i
	movl	$term_Equal, %esi
	movl	$term_Delete, %edx
	movq	%rbx, %rdi
	callq	list_DeleteDuplicatesFree
	movq	16(%r13), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, 16(%rcx)
.LBB39_37:                              # %cnf_MovePredicateVariablesUp.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, vec_MAX(%rip)
	movl	(%r13), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%ecx, %eax
	movq	%r13, %rbp
	movl	%eax, %edx
	jne	.LBB39_40
# BB#38:                                # %.lr.ph59.i.preheader
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB39_39:                              # %.lr.ph59.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %rbp
	movl	(%rbp), %edx
	cmpl	%eax, %edx
	je	.LBB39_39
.LBB39_40:                              # %._crit_edge60.i
	cmpl	fol_OR(%rip), %edx
	jne	.LBB39_45
# BB#41:
	movq	16(%rbp), %r12
	testq	%r12, %r12
	je	.LBB39_45
	.p2align	4, 0x90
.LBB39_42:                              # %.lr.ph54.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_49 Depth 2
	movq	%r12, %r14
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB39_43
# BB#47:                                #   in Loop: Header=BB39_42 Depth=1
	movl	(%rbx), %esi
	cmpl	fol_OR(%rip), %esi
	jne	.LBB39_43
# BB#48:                                #   in Loop: Header=BB39_42 Depth=1
	movq	%rbx, %rdi
	callq	cnf_Flatten
	movq	16(%rbx), %rax
	.p2align	4, 0x90
.LBB39_49:                              # %.lr.ph.i116
                                        #   Parent Loop BB39_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movq	%rbp, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB39_49
# BB#50:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB39_42 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rsi
	movq	%r14, %rdi
	callq	list_NInsert
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%r14)
	movq	16(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB39_43:                              # %.backedge.i113
                                        #   in Loop: Header=BB39_42 Depth=1
	testq	%r12, %r12
	jne	.LBB39_42
# BB#44:                                # %cnf_FlattenPath.exit.loopexit
	movl	(%r13), %eax
	movl	fol_ALL(%rip), %ecx
.LBB39_45:                              # %cnf_FlattenPath.exit
	cmpl	%ecx, %eax
	jne	.LBB39_51
# BB#46:
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbp
	jmp	.LBB39_52
.LBB39_51:
	movq	%r13, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	%r13, %rbp
.LBB39_52:
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	(%r14), %eax
	cmpl	fol_OR(%rip), %eax
	jne	.LBB39_62
# BB#53:
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB39_54
# BB#55:                                # %.lr.ph136.preheader
	addq	$16, %rbp
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB39_56:                              # %.lr.ph136
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movq	8(%rbp), %rsi
	movq	%r15, %rdi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	jne	.LBB39_58
# BB#57:                                #   in Loop: Header=BB39_56 Depth=1
	cmpq	%r15, 8(%rbp)
	je	.LBB39_58
# BB#59:                                #   in Loop: Header=BB39_56 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB39_56
	jmp	.LBB39_60
.LBB39_62:
	movq	%r14, %rdi
	callq	term_Delete
	jmp	.LBB39_74
.LBB39_58:
	movq	8(%rbx), %rsi
	movl	$term_Delete, %edx
	movq	%r12, %rdi
	callq	list_PointerDeleteElementFree
	movq	%rax, %r12
.LBB39_60:                              # %.loopexit
	movq	%r12, 16(%r14)
	testq	%r12, %r12
	movq	(%rsp), %r12            # 8-byte Reload
	je	.LBB39_61
# BB#63:
	movq	%r14, %rdi
	callq	term_MaxVar
	movl	%eax, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r12, %rdi
	callq	fol_BoundVariables
	movl	$term_Equal, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	120(%rsp), %r15
	je	.LBB39_67
# BB#64:                                # %.lr.ph131.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB39_65:                              # %.lr.ph131
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	movl	symbol_STANDARDVARCOUNTER(%rip), %edx
	incl	%edx
	movl	%edx, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r12, %rdi
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB39_65
	.p2align	4, 0x90
.LBB39_66:                              # %.lr.ph.i117
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB39_66
.LBB39_67:                              # %list_Delete.exit.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB39_69
	.p2align	4, 0x90
.LBB39_68:                              # %list_Delete.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	callq	term_ReplaceVariable
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	testq	%rbp, %rbp
	jne	.LBB39_68
.LBB39_69:                              # %list_Delete.exit._crit_edge
	movl	fol_NOT(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rbx
	movl	fol_IMPLIES(%rip), %r14d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	fol_FreeVariables
	movq	%rax, %rbp
	movl	$term_Copy, %esi
	movq	%rbp, %rdi
	callq	list_NMapCar
	movl	fol_ALL(%rip), %r14d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbx
	movq	104(%rsp), %rbp
	cmpl	$0, 148(%rbp)
	je	.LBB39_71
# BB#70:
	movl	$.L.str.12, %edi
	callq	puts
	movq	%r13, %rdi
	callq	fol_PrettyPrint
.LBB39_71:
	movq	%rbx, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rbx
	cmpl	$0, 148(%rbp)
	je	.LBB39_73
# BB#72:
	movl	$.L.str.13, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	fol_PrettyPrint
.LBB39_73:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	112(%rsp), %rcx
	callq	cnf_HaveProof
	movl	%eax, (%r15)
.LBB39_74:
	movq	%rbx, %rdi
	jmp	.LBB39_75
.LBB39_54:                              # %.loopexit.thread
	movq	$0, 16(%r14)
	movq	(%rsp), %r12            # 8-byte Reload
.LBB39_61:                              # %.critedge
	movq	%r14, %rdi
	callq	term_Delete
	movq	%r12, %rdi
.LBB39_75:
	callq	term_Delete
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	cnf_DefTargetConvert, .Lfunc_end39-cnf_DefTargetConvert
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_RemoveImplFromFormulaPath,@function
cnf_RemoveImplFromFormulaPath:          # @cnf_RemoveImplFromFormulaPath
	.cfi_startproc
# BB#0:                                 # %.lr.ph33.preheader
	pushq	%rbp
.Lcfi355:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi356:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi357:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi358:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi359:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi360:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi361:
	.cfi_def_cfa_offset 64
.Lcfi362:
	.cfi_offset %rbx, -56
.Lcfi363:
	.cfi_offset %r12, -48
.Lcfi364:
	.cfi_offset %r13, -40
.Lcfi365:
	.cfi_offset %r14, -32
.Lcfi366:
	.cfi_offset %r15, -24
.Lcfi367:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movslq	vec_MAX(%rip), %r12
	leal	1(%r12), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%r14, vec_VECTOR(,%r12,8)
	.p2align	4, 0x90
.LBB40_1:                               # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_9 Depth 2
	leal	-1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	vec_VECTOR-8(,%rax,8), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB40_2
# BB#4:                                 #   in Loop: Header=BB40_1 Depth=1
	movl	(%rbx), %eax
	cmpl	fol_IMPLIES(%rip), %eax
	jne	.LBB40_5
# BB#6:                                 #   in Loop: Header=BB40_1 Depth=1
	movl	fol_OR(%rip), %eax
	movl	%eax, (%rbx)
	movl	fol_NOT(%rip), %r13d
	movq	16(%rbx), %rax
	movq	8(%rax), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	%rax, %rsi
	callq	term_CreateAddFather
	movq	16(%rbx), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, 8(%rax)
	leaq	16(%rbx), %rbx
	jmp	.LBB40_7
	.p2align	4, 0x90
.LBB40_5:                               # %._crit_edge34
                                        #   in Loop: Header=BB40_1 Depth=1
	addq	$16, %rbx
.LBB40_7:                               #   in Loop: Header=BB40_1 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB40_2
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB40_1 Depth=1
	movslq	vec_MAX(%rip), %rcx
	.p2align	4, 0x90
.LBB40_9:                               # %.lr.ph
                                        #   Parent Loop BB40_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rdx
	movq	%rdx, vec_VECTOR(,%rcx,8)
	incq	%rcx
	movl	%ecx, vec_MAX(%rip)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB40_9
.LBB40_2:                               # %.backedge
                                        #   in Loop: Header=BB40_1 Depth=1
	movl	vec_MAX(%rip), %eax
	cmpl	%eax, %r12d
	jne	.LBB40_1
# BB#3:                                 # %._crit_edge
	movl	%r12d, vec_MAX(%rip)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	cnf_RemoveImplFromFormulaPath, .Lfunc_end40-cnf_RemoveImplFromFormulaPath
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_NegationNormalFormulaPath,@function
cnf_NegationNormalFormulaPath:          # @cnf_NegationNormalFormulaPath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi368:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi369:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi370:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi371:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi372:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi373:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi374:
	.cfi_def_cfa_offset 80
.Lcfi375:
	.cfi_offset %rbx, -56
.Lcfi376:
	.cfi_offset %r12, -48
.Lcfi377:
	.cfi_offset %r13, -40
.Lcfi378:
	.cfi_offset %r14, -32
.Lcfi379:
	.cfi_offset %r15, -24
.Lcfi380:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB41_37
# BB#1:                                 # %select.unfold.loopexit.preheader.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB41_2:                               # %select.unfold.loopexit.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_23 Depth 2
                                        #     Child Loop BB41_26 Depth 2
                                        #     Child Loop BB41_28 Depth 2
                                        #     Child Loop BB41_33 Depth 2
                                        #     Child Loop BB41_14 Depth 2
                                        #     Child Loop BB41_16 Depth 2
                                        #     Child Loop BB41_5 Depth 2
                                        #     Child Loop BB41_7 Depth 2
                                        #     Child Loop BB41_10 Depth 2
	movl	(%rbx), %ebp
	cmpl	fol_NOT(%rip), %ebp
	jne	.LBB41_31
# BB#3:                                 # %.lr.ph202
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %r12
	movl	(%r12), %ecx
	cmpl	%ebp, %ecx
	jne	.LBB41_11
# BB#4:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rbx)
	testq	%rax, %rax
	je	.LBB41_6
	.p2align	4, 0x90
.LBB41_5:                               # %.lr.ph.i142
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB41_5
.LBB41_6:                               # %list_Delete.exit143
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB41_8
	.p2align	4, 0x90
.LBB41_7:                               # %.lr.ph.i137
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB41_7
.LBB41_8:                               # %list_Delete.exit138
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r12, (%rax)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB41_2
	.p2align	4, 0x90
.LBB41_10:                              # %.lr.ph
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movq	%rbx, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB41_10
	jmp	.LBB41_2
.LBB41_11:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB41_2 Depth=1
	movl	fol_ALL(%rip), %edx
	cmpl	%ecx, %edx
	movl	fol_EXIST(%rip), %r13d
	je	.LBB41_13
# BB#12:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB41_2 Depth=1
	cmpl	%ecx, %r13d
	je	.LBB41_13
# BB#18:                                #   in Loop: Header=BB41_2 Depth=1
	movl	fol_OR(%rip), %edx
	cmpl	%edx, %ecx
	movl	fol_AND(%rip), %esi
	je	.LBB41_20
# BB#19:                                #   in Loop: Header=BB41_2 Depth=1
	cmpl	%esi, %ecx
	jne	.LBB41_31
.LBB41_20:                              # %cnf_GetDualSymbol.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	cmpl	%esi, %ecx
	movl	%ecx, %edi
	cmovel	%edx, %edi
	cmpl	%edx, %ecx
	cmovel	%esi, %edi
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.LBB41_25
# BB#21:                                # %.lr.ph166.preheader
                                        #   in Loop: Header=BB41_2 Depth=1
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	8(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	jmp	.LBB41_23
	.p2align	4, 0x90
.LBB41_22:                              # %.lr.ph166..lr.ph166_crit_edge
                                        #   in Loop: Header=BB41_23 Depth=2
	movl	fol_NOT(%rip), %r14d
	movq	8(%r13), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
.LBB41_23:                              # %.lr.ph166..lr.ph166_crit_edge
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rsi
	callq	term_CreateAddFather
	movq	%rax, 8(%r13)
	movq	%r12, 8(%rax)
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB41_22
# BB#24:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
.LBB41_25:                              # %._crit_edge
                                        #   in Loop: Header=BB41_2 Depth=1
	movl	%edi, (%rbx)
	testq	%rax, %rax
	je	.LBB41_27
	.p2align	4, 0x90
.LBB41_26:                              # %.lr.ph.i
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB41_26
.LBB41_27:                              # %list_Delete.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%r12), %rax
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB41_30
	.p2align	4, 0x90
.LBB41_28:                              # %.lr.ph168
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movq	%rbx, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB41_28
.LBB41_30:                              # %._crit_edge169
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	$0, 16(%r12)
	movq	%r12, %rdi
	callq	term_Delete
.LBB41_31:                              # %.loopexit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB41_33
	jmp	.LBB41_37
	.p2align	4, 0x90
.LBB41_34:                              #   in Loop: Header=BB41_33 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB41_37
.LBB41_33:                              # %.lr.ph171
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB41_34
# BB#35:                                #   in Loop: Header=BB41_2 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB41_2
	jmp	.LBB41_37
.LBB41_13:                              # %cnf_GetDualSymbol.exit133
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmpl	%ecx, %edx
	cmovnel	%edx, %r13d
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_CreateAddFather
	movq	%rax, %rbp
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	%rbp, 8(%rax)
	movq	%r12, 8(%rbp)
	movl	%r13d, (%rbx)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB41_15
	.p2align	4, 0x90
.LBB41_14:                              # %.lr.ph.i129
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB41_14
.LBB41_15:                              # %list_Delete.exit130
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	16(%r12), %rax
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB41_17
	.p2align	4, 0x90
.LBB41_16:                              # %.lr.ph173
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movq	%rbx, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB41_16
.LBB41_17:                              # %._crit_edge174
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	$0, 16(%r12)
	movq	%r12, %rdi
	callq	term_Delete
	movq	%rbp, %rbx
	testq	%rbx, %rbx
	jne	.LBB41_2
.LBB41_37:                              # %select.unfold.outer._crit_edge
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end41:
	.size	cnf_NegationNormalFormulaPath, .Lfunc_end41-cnf_NegationNormalFormulaPath
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_AntiPrenexPath,@function
cnf_AntiPrenexPath:                     # @cnf_AntiPrenexPath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi381:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi382:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi383:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi384:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi385:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi387:
	.cfi_def_cfa_offset 112
.Lcfi388:
	.cfi_offset %rbx, -56
.Lcfi389:
	.cfi_offset %r12, -48
.Lcfi390:
	.cfi_offset %r13, -40
.Lcfi391:
	.cfi_offset %r14, -32
.Lcfi392:
	.cfi_offset %r15, -24
.Lcfi393:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movl	(%r14), %r15d
	movl	fol_ALL(%rip), %edx
	cmpl	%r15d, %edx
	movl	fol_EXIST(%rip), %ecx
	je	.LBB42_2
# BB#1:
	cmpl	%r15d, %ecx
	je	.LBB42_2
# BB#62:
	cmpl	fol_NOT(%rip), %r15d
	je	.LBB42_70
# BB#63:
	testl	%r15d, %r15d
	jns	.LBB42_65
# BB#64:                                # %symbol_IsPredicate.exit116
	negl	%r15d
	andl	symbol_TYPEMASK(%rip), %r15d
	cmpl	$2, %r15d
	je	.LBB42_70
.LBB42_65:                              # %symbol_IsPredicate.exit116.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB42_67
	jmp	.LBB42_70
	.p2align	4, 0x90
.LBB42_69:                              #   in Loop: Header=BB42_67 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB42_70
.LBB42_67:                              # %.lr.ph165
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%rbp, %rdi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB42_69
# BB#68:                                #   in Loop: Header=BB42_67 Depth=1
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	cnf_AntiPrenexPath
	jmp	.LBB42_69
.LBB42_2:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r12
	movl	(%r12), %eax
	cmpl	fol_NOT(%rip), %eax
	je	.LBB42_70
# BB#3:
	testl	%eax, %eax
	jns	.LBB42_5
# BB#4:                                 # %symbol_IsPredicate.exit
	movl	%eax, %esi
	negl	%esi
	andl	symbol_TYPEMASK(%rip), %esi
	cmpl	$2, %esi
	je	.LBB42_70
.LBB42_5:                               # %symbol_IsPredicate.exit.thread
	cmpl	%r15d, %edx
	movl	$fol_AND, %esi
	movl	$fol_OR, %edi
	cmoveq	%rsi, %rdi
	movl	(%rdi), %ebx
	cmpl	%eax, %edx
	je	.LBB42_7
# BB#6:                                 # %symbol_IsPredicate.exit.thread
	cmpl	%eax, %ecx
	jne	.LBB42_8
.LBB42_7:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	cnf_AntiPrenexPath
	movl	(%r12), %eax
.LBB42_8:
	cmpl	%ebx, %eax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jne	.LBB42_28
# BB#9:
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %r13
	movq	%r12, %rdi
	movl	%ebx, (%rsp)            # 4-byte Spill
	movl	%ebx, %esi
	callq	cnf_Flatten
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB42_11
	jmp	.LBB42_21
	.p2align	4, 0x90
.LBB42_20:                              #   in Loop: Header=BB42_11 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB42_21
.LBB42_11:                              # %.lr.ph146
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_16 Depth 2
	movq	8(%rbp), %r14
	movq	%r14, %rdi
	callq	fol_FreeVariables
	movl	$term_Equal, %edx
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	list_NIntersect
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB42_20
# BB#12:                                #   in Loop: Header=BB42_11 Depth=1
	movl	(%r14), %r12d
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_NMapCar
	cmpl	%r12d, %r15d
	jne	.LBB42_19
# BB#13:                                #   in Loop: Header=BB42_11 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB42_14
# BB#15:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB42_11 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB42_16:                              # %.preheader.i
                                        #   Parent Loop BB42_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB42_16
# BB#17:                                #   in Loop: Header=BB42_11 Depth=1
	movq	%rbx, (%rdx)
	movq	%rcx, 16(%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB42_11
	jmp	.LBB42_21
	.p2align	4, 0x90
.LBB42_19:                              #   in Loop: Header=BB42_11 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifierAddFather
	movq	%rax, 8(%rbp)
	jmp	.LBB42_20
.LBB42_14:                              #   in Loop: Header=BB42_11 Depth=1
	movq	%rbx, %rcx
	movq	%rcx, 16(%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB42_11
.LBB42_21:                              # %._crit_edge
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%r14), %rax
	testq	%rax, %rax
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB42_23
	.p2align	4, 0x90
.LBB42_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB42_22
.LBB42_23:                              # %list_Delete.exit
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, (%r14)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movq	%rax, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdx, (%rax)
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB42_25
	jmp	.LBB42_70
	.p2align	4, 0x90
.LBB42_27:                              #   in Loop: Header=BB42_25 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB42_70
.LBB42_25:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	%r14, 8(%rax)
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	je	.LBB42_27
# BB#26:                                #   in Loop: Header=BB42_25 Depth=1
	movl	$.L.str.32, %edi
	callq	puts
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	cnf_AntiPrenexPath
	movq	%rax, 8(%rbx)
	movq	%r14, 8(%rax)
	jmp	.LBB42_27
.LBB42_28:
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB42_70
# BB#29:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB42_70
# BB#30:
	movl	(%r14), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movl	(%rdi), %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	callq	cnf_Flatten
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB42_49
# BB#31:                                # %.lr.ph161
	movq	40(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB42_32:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_34 Depth 2
                                        #     Child Loop BB42_41 Depth 2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB42_48
# BB#33:                                # %.lr.ph157.preheader
                                        #   in Loop: Header=BB42_32 Depth=1
	movq	8(%r15), %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB42_34:                              # %.lr.ph157
                                        #   Parent Loop BB42_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	callq	fol_VarOccursFreely
	testl	%eax, %eax
	jne	.LBB42_36
# BB#35:                                #   in Loop: Header=BB42_34 Depth=2
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB42_36:                              #   in Loop: Header=BB42_34 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB42_34
# BB#37:                                # %._crit_edge158
                                        #   in Loop: Header=BB42_32 Depth=1
	testq	%r12, %r12
	je	.LBB42_48
# BB#38:                                #   in Loop: Header=BB42_32 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rdi
	movq	%r12, %rsi
	callq	list_NPointerDifference
	movq	%rax, %r14
	cmpq	$0, (%r14)
	je	.LBB42_39
# BB#43:                                #   in Loop: Header=BB42_32 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r14, %rsi
	callq	term_CreateAddFather
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%rbx, %rsi
	movq	%rax, %rdx
	jmp	.LBB42_44
.LBB42_39:                              #   in Loop: Header=BB42_32 Depth=1
	movq	8(%r14), %rbx
	movl	16(%rsp), %eax          # 4-byte Reload
	cmpl	(%rbx), %eax
	jne	.LBB42_42
# BB#40:                                #   in Loop: Header=BB42_32 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbp
	movq	16(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rbp)
	.p2align	4, 0x90
.LBB42_41:                              # %.lr.ph.i.i
                                        #   Parent Loop BB42_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB42_41
	jmp	.LBB42_45
.LBB42_42:                              #   in Loop: Header=BB42_32 Depth=1
	movl	%eax, %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	movq	%r14, %rdx
.LBB42_44:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB42_32 Depth=1
	callq	fol_CreateQuantifierAddFather
	movq	%rax, %rbx
.LBB42_45:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB42_32 Depth=1
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	term_HasProperSuperterm
	testl	%eax, %eax
	je	.LBB42_47
# BB#46:                                #   in Loop: Header=BB42_32 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cnf_AntiPrenexPath
	movq	%rax, %rbx
.LBB42_47:                              #   in Loop: Header=BB42_32 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, 16(%rcx)
	movq	%rcx, 8(%rbx)
	movq	16(%r14), %rax
	movq	8(%rax), %rbx
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbx)
.LBB42_48:                              # %._crit_edge158.thread
                                        #   in Loop: Header=BB42_32 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB42_32
.LBB42_49:                              # %._crit_edge162
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	cmpq	$0, 16(%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	jne	.LBB42_55
# BB#50:
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB42_52
	.p2align	4, 0x90
.LBB42_51:                              # %.lr.ph.i120.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB42_51
.LBB42_52:                              # %list_Delete.exit121.i
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r14)
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	16(%rdx), %rax
	movq	%rax, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdx, (%rax)
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB42_55
	.p2align	4, 0x90
.LBB42_53:                              # %.lr.ph152
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	%r14, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB42_53
.LBB42_55:                              # %.loopexit140
	testq	%rdi, %rdi
	je	.LBB42_57
	.p2align	4, 0x90
.LBB42_56:                              # %.lr.ph.i125.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB42_56
.LBB42_57:                              # %cnf_DistrQuantorNoVarSubPath.exit.preheader
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB42_59
	jmp	.LBB42_70
	.p2align	4, 0x90
.LBB42_61:                              # %cnf_DistrQuantorNoVarSubPath.exit
                                        #   in Loop: Header=BB42_59 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB42_70
.LBB42_59:                              # %.lr.ph149
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	je	.LBB42_61
# BB#60:                                #   in Loop: Header=BB42_59 Depth=1
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	cnf_AntiPrenexPath
	jmp	.LBB42_61
.LBB42_70:                              # %.loopexit
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end42:
	.size	cnf_AntiPrenexPath, .Lfunc_end42-cnf_AntiPrenexPath
	.cfi_endproc

	.globl	cnf_DefConvert
	.p2align	4, 0x90
	.type	cnf_DefConvert,@function
cnf_DefConvert:                         # @cnf_DefConvert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi394:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi395:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi396:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi397:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi398:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi399:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi400:
	.cfi_def_cfa_offset 80
.Lcfi401:
	.cfi_offset %rbx, -56
.Lcfi402:
	.cfi_offset %r12, -48
.Lcfi403:
	.cfi_offset %r13, -40
.Lcfi404:
	.cfi_offset %r14, -32
.Lcfi405:
	.cfi_offset %r15, -24
.Lcfi406:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	callq	cnf_RemoveImplFromFormulaPath
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	cnf_NegationNormalFormulaPath
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rbx), %r13
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	term_AddFatherLinks
	movq	8(%r13), %rbx
	cmpq	%r12, %rbx
	jne	.LBB43_2
	jmp	.LBB43_18
	.p2align	4, 0x90
.LBB43_12:                              # %._crit_edge89.i
                                        #   in Loop: Header=BB43_2 Depth=1
	movl	(%rbx), %eax
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB43_14
# BB#13:                                # %._crit_edge89.i
                                        #   in Loop: Header=BB43_2 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB43_1
.LBB43_14:                              #   in Loop: Header=BB43_2 Depth=1
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	callq	list_PointerDeleteOneElement
	movq	%rax, 16(%r14)
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	movl	(%rbx), %edi
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 16(%rbx)
	movl	fol_OR(%rip), %eax
	movl	%eax, (%rbx)
	movq	16(%rbx), %rax
	jmp	.LBB43_16
	.p2align	4, 0x90
.LBB43_15:                              # %.lr.ph93.i
                                        #   in Loop: Header=BB43_16 Depth=2
	movq	8(%rax), %rcx
	movq	%rbx, 8(%rcx)
	movq	(%rax), %rax
.LBB43_16:                              # %.lr.ph93.i
                                        #   Parent Loop BB43_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	jne	.LBB43_15
	jmp	.LBB43_1
	.p2align	4, 0x90
.LBB43_17:                              #   in Loop: Header=BB43_2 Depth=1
	movq	%rbx, %rdi
	callq	cnf_Flatten
	movq	%rax, %rbx
.LBB43_1:                               #   in Loop: Header=BB43_2 Depth=1
	cmpq	%r12, %rbx
	je	.LBB43_18
.LBB43_2:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_3 Depth 2
                                        #     Child Loop BB43_9 Depth 2
                                        #     Child Loop BB43_16 Depth 2
	movl	fol_OR(%rip), %esi
	jmp	.LBB43_3
	.p2align	4, 0x90
.LBB43_5:                               # %.lr.ph.i
                                        #   in Loop: Header=BB43_3 Depth=2
	movq	8(%rbx), %rbx
.LBB43_3:                               # %.preheader.i
                                        #   Parent Loop BB43_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpq	%rbx, %r12
	je	.LBB43_6
# BB#4:                                 # %.preheader.i
                                        #   in Loop: Header=BB43_3 Depth=2
	cmpl	%eax, %esi
	je	.LBB43_5
.LBB43_6:                               # %._crit_edge.i
                                        #   in Loop: Header=BB43_2 Depth=1
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB43_8
# BB#7:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB43_2 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB43_17
.LBB43_8:                               #   in Loop: Header=BB43_2 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	cnf_Flatten
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB43_12
	.p2align	4, 0x90
.LBB43_9:                               # %.lr.ph88.i
                                        #   Parent Loop BB43_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movq	%r13, %rsi
	callq	fol_VarOccursFreely
	testl	%eax, %eax
	movq	(%rbp), %r15
	je	.LBB43_11
# BB#10:                                #   in Loop: Header=BB43_9 Depth=2
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movq	%rbx, %rdi
	callq	fol_DeleteQuantifierVariable
.LBB43_11:                              # %.backedge.i
                                        #   in Loop: Header=BB43_9 Depth=2
	testq	%r15, %r15
	movq	%r15, %rbp
	jne	.LBB43_9
	jmp	.LBB43_12
.LBB43_18:                              # %._crit_edge95.i
	movq	%r12, %rdi
	callq	fol_FreeVariables
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB43_20
# BB#19:
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_NMapCar
	movl	fol_ALL(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %r12
.LBB43_20:                              # %cnf_RemoveQuantFromPathAndFlatten.exit
	movq	%r12, %rdi
	callq	term_AddFatherLinks
	movl	(%r12), %eax
	cmpl	fol_ALL(%rip), %eax
	jne	.LBB43_24
# BB#21:
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
	cmpl	fol_OR(%rip), %eax
	jne	.LBB43_27
# BB#22:
	movq	16(%rbx), %rdi
	callq	list_Length
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rbx
	cmpl	$1, %eax
	jne	.LBB43_27
# BB#23:
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%rcx)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%r12, 8(%rax)
	jmp	.LBB43_31
.LBB43_24:
	cmpl	fol_OR(%rip), %eax
	movq	%r12, %rbx
	jne	.LBB43_27
# BB#25:
	movq	16(%r12), %rdi
	callq	list_Length
	cmpl	$1, %eax
	movq	%r12, %rbx
	jne	.LBB43_28
# BB#26:
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%r12, (%rcx)
	movq	16(%rax), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, 8(%rcx)
	movq	%rax, %r12
	jmp	.LBB43_31
.LBB43_27:                              # %._crit_edge
	testq	%rbx, %rbx
	je	.LBB43_31
.LBB43_28:                              # %.thread77
	movl	(%rbx), %eax
	cmpl	fol_EQUIV(%rip), %eax
	jne	.LBB43_30
# BB#29:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rax)
	jmp	.LBB43_31
.LBB43_30:
	movq	16(%rbx), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
	movq	%r15, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbx)
	movq	%rbx, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%rbp, (%r13)
	movl	fol_NOT(%rip), %r14d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, (%r13)
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, (%r13)
	movq	%rax, %rdi
	callq	term_AddFatherLinks
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, (%rbx)
	movl	fol_NOT(%rip), %r14d
	movl	fol_OR(%rip), %edi
	movq	16(%rbx), %rsi
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 16(%rbx)
	movq	%r12, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	term_AddFatherLinks
.LBB43_31:                              # %.thread
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end43:
	.size	cnf_DefConvert, .Lfunc_end43-cnf_DefConvert
	.cfi_endproc

	.globl	cnf_HandleDefinition
	.p2align	4, 0x90
	.type	cnf_HandleDefinition,@function
cnf_HandleDefinition:                   # @cnf_HandleDefinition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi407:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi408:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi409:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi410:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi411:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi412:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi413:
	.cfi_def_cfa_offset 144
.Lcfi414:
	.cfi_offset %rbx, -56
.Lcfi415:
	.cfi_offset %r12, -48
.Lcfi416:
	.cfi_offset %r13, -40
.Lcfi417:
	.cfi_offset %r14, -32
.Lcfi418:
	.cfi_offset %r15, -24
.Lcfi419:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movq	104(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	112(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rbp), %rbx
	movq	$0, (%rsp)
	movq	%rsp, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	cnf_ContainsDefinitionIntern
	testl	%eax, %eax
	je	.LBB44_45
# BB#1:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	callq	list_Copy
	movq	%rax, %r13
	movq	%r14, %rdi
	callq	list_Copy
	testq	%r13, %r13
	je	.LBB44_2
# BB#3:
	testq	%rax, %rax
	je	.LBB44_46
# BB#4:
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB44_5:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB44_5
# BB#6:
	movq	%rax, (%rdx)
	jmp	.LBB44_7
.LBB44_2:
	movq	%rax, %r13
.LBB44_7:                               # %list_Nconc.exit
	movq	%r12, %rdi
	callq	list_Copy
	movq	%rax, %rbx
	testq	%r13, %r13
	jne	.LBB44_8
	jmp	.LBB44_13
.LBB44_46:                              # %list_Nconc.exit.thread
	movq	%r12, %rdi
	callq	list_Copy
	movq	%rax, %rbx
.LBB44_8:
	testq	%rbx, %rbx
	je	.LBB44_12
# BB#9:                                 # %.preheader.i103.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB44_10:                              # %.preheader.i103
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB44_10
# BB#11:
	movq	%rbx, (%rax)
.LBB44_12:                              # %list_Nconc.exit105
	movq	%r13, %rbx
.LBB44_13:                              # %list_Nconc.exit105
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 148(%rax)
	je	.LBB44_15
# BB#14:
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rsp), %rax
	movl	(%rax), %edi
	callq	symbol_Print
.LBB44_15:
	movq	(%rsp), %rsi
	leaq	56(%rsp), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	cnf_DefConvert
	movq	%rax, %r12
	movq	56(%rsp), %r13
	movq	%r12, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	(%r15), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, (%r15)
	testq	%r13, %r13
	jne	.LBB44_18
# BB#16:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	148(%rax), %eax
	testl	%eax, %eax
	je	.LBB44_18
# BB#17:
	movq	stdout(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	fol_PrettyPrint
.LBB44_18:
	testq	%rbx, %rbx
	je	.LBB44_42
# BB#19:                                # %.lr.ph
	movq	(%rsp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB44_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_25 Depth 2
                                        #     Child Loop BB44_27 Depth 2
                                        #     Child Loop BB44_34 Depth 2
                                        #     Child Loop BB44_36 Depth 2
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.LBB44_38
# BB#21:                                #   in Loop: Header=BB44_20 Depth=1
	movq	(%r15), %rbp
	cmpq	%rbp, %r12
	je	.LBB44_38
# BB#22:                                #   in Loop: Header=BB44_20 Depth=1
	movq	$0, 8(%rsp)
	movq	$0, 24(%rsp)
	movl	$0, 36(%rsp)
	movq	(%rsp), %rax
	movl	(%rax), %esi
	subq	$8, %rsp
.Lcfi420:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edx
	movq	%rbp, %rdi
	leaq	56(%rsp), %rcx
	leaq	88(%rsp), %r8
	leaq	32(%rsp), %r9
	leaq	16(%rsp), %rax
	pushq	%rax
.Lcfi421:
	.cfi_adjust_cfa_offset 8
	callq	cnf_ContainsPredicateIntern
	addq	$16, %rsp
.Lcfi422:
	.cfi_adjust_cfa_offset -16
	testq	%r13, %r13
	je	.LBB44_30
# BB#23:                                #   in Loop: Header=BB44_20 Depth=1
	testl	%eax, %eax
	je	.LBB44_39
# BB#24:                                #   in Loop: Header=BB44_20 Depth=1
	movq	%r13, %rdi
	callq	term_Copy
	movq	80(%rsp), %rsi
	movq	(%rsp), %rcx
	movq	16(%rcx), %rcx
	movq	48(%rsp), %rdx
	movq	16(%rdx), %r8
	movq	%rbp, %rdi
	movq	%rax, %rdx
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi423:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi424:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi425:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)
.Lcfi426:
	.cfi_adjust_cfa_offset 8
	callq	cnf_DefTargetConvert
	addq	$32, %rsp
.Lcfi427:
	.cfi_adjust_cfa_offset -32
	movq	24(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB44_26
	.p2align	4, 0x90
.LBB44_25:                              # %.lr.ph.i98
                                        #   Parent Loop BB44_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB44_25
.LBB44_26:                              # %list_Delete.exit99
                                        #   in Loop: Header=BB44_20 Depth=1
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB44_28
	.p2align	4, 0x90
.LBB44_27:                              # %.lr.ph.i93
                                        #   Parent Loop BB44_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB44_27
.LBB44_28:                              # %list_Delete.exit94
                                        #   in Loop: Header=BB44_20 Depth=1
	movq	$0, 8(%rsp)
	movq	$0, 24(%rsp)
	movq	%rax, (%r15)
	cmpl	$0, 36(%rsp)
	je	.LBB44_39
# BB#29:                                #   in Loop: Header=BB44_20 Depth=1
	movq	(%rsp), %rdi
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rdx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	cnf_ApplyDefinitionOnce
	movq	%rax, (%r15)
	testq	%r14, %r14
	jne	.LBB44_20
	jmp	.LBB44_40
	.p2align	4, 0x90
.LBB44_38:                              #   in Loop: Header=BB44_20 Depth=1
	movq	(%r14), %r14
.LBB44_39:                              #   in Loop: Header=BB44_20 Depth=1
	testq	%r14, %r14
	jne	.LBB44_20
	jmp	.LBB44_40
.LBB44_30:                              #   in Loop: Header=BB44_20 Depth=1
	testl	%eax, %eax
	je	.LBB44_32
# BB#31:                                #   in Loop: Header=BB44_20 Depth=1
	movq	(%rsp), %rdi
	movq	(%r15), %rdx
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	cnf_ApplyDefinitionOnce
	movq	%rax, (%r15)
	jmp	.LBB44_33
.LBB44_32:                              #   in Loop: Header=BB44_20 Depth=1
	movq	(%r14), %r14
.LBB44_33:                              #   in Loop: Header=BB44_20 Depth=1
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB44_35
	.p2align	4, 0x90
.LBB44_34:                              # %.lr.ph.i88
                                        #   Parent Loop BB44_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB44_34
.LBB44_35:                              # %list_Delete.exit89
                                        #   in Loop: Header=BB44_20 Depth=1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB44_37
	.p2align	4, 0x90
.LBB44_36:                              # %.lr.ph.i83
                                        #   Parent Loop BB44_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB44_36
.LBB44_37:                              # %list_Delete.exit84
                                        #   in Loop: Header=BB44_20 Depth=1
	movq	$0, 8(%rsp)
	movq	$0, 24(%rsp)
	testq	%r14, %r14
	jne	.LBB44_20
	.p2align	4, 0x90
.LBB44_40:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB44_40
# BB#41:                                # %list_Delete.exit.loopexit
	movq	56(%rsp), %r13
.LBB44_42:                              # %list_Delete.exit
	testq	%r13, %r13
	je	.LBB44_44
# BB#43:
	movq	%r13, %rdi
	callq	term_Delete
.LBB44_44:
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%r12, (%rbp)
.LBB44_45:
	movq	%rbp, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	cnf_HandleDefinition, .Lfunc_end44-cnf_HandleDefinition
	.cfi_endproc

	.globl	cnf_ApplyDefinitionToClause
	.p2align	4, 0x90
	.type	cnf_ApplyDefinitionToClause,@function
cnf_ApplyDefinitionToClause:            # @cnf_ApplyDefinitionToClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi428:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi429:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi430:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi431:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi432:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi433:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi434:
	.cfi_def_cfa_offset 112
.Lcfi435:
	.cfi_offset %rbx, -56
.Lcfi436:
	.cfi_offset %r12, -48
.Lcfi437:
	.cfi_offset %r13, -40
.Lcfi438:
	.cfi_offset %r14, -32
.Lcfi439:
	.cfi_offset %r15, -24
.Lcfi440:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	xorl	%ebx, %ebx
	addl	72(%r12), %eax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	jne	.LBB45_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB45_4
.LBB45_2:                               # %.lr.ph97
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB45_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	incl	%ebx
	movl	68(%r12), %ecx
	addl	64(%r12), %ecx
	addl	72(%r12), %ecx
	cmpl	%ecx, %ebx
	movq	%rax, %rbp
	jb	.LBB45_3
.LBB45_4:                               # %._crit_edge98
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	fol_OR(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	16(%rbp), %r12
	testq	%r12, %r12
	movq	%r15, %rbx
	je	.LBB45_31
# BB#5:                                 # %.lr.ph90.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB45_6:                               # %.lr.ph90
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB45_15 Depth 2
	movq	8(%r12), %rdx
	movl	(%rdx), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB45_7
# BB#8:                                 #   in Loop: Header=BB45_6 Depth=1
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
	movl	$1, %r13d
	jmp	.LBB45_9
	.p2align	4, 0x90
.LBB45_7:                               #   in Loop: Header=BB45_6 Depth=1
	xorl	%r13d, %r13d
.LBB45_9:                               #   in Loop: Header=BB45_6 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB45_13
# BB#10:                                #   in Loop: Header=BB45_6 Depth=1
	callq	subst_ExtractMatcher
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	subst_Free
	testl	%r13d, %r13d
	je	.LBB45_12
# BB#11:                                #   in Loop: Header=BB45_6 Depth=1
	movl	fol_NOT(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
.LBB45_12:                              #   in Loop: Header=BB45_6 Depth=1
	movq	8(%r12), %rdi
	callq	term_Delete
	movq	%r14, 8(%r12)
	movl	$1, %ebp
.LBB45_13:                              #   in Loop: Header=BB45_6 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB45_16
# BB#14:                                # %.lr.ph.i81.preheader
                                        #   in Loop: Header=BB45_6 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB45_15:                              # %.lr.ph.i81
                                        #   Parent Loop BB45_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB45_15
.LBB45_16:                              # %._crit_edge.i
                                        #   in Loop: Header=BB45_6 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB45_18
# BB#17:                                #   in Loop: Header=BB45_6 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB45_18:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB45_6 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB45_6
# BB#19:                                # %._crit_edge
	testl	%ebp, %ebp
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB45_31
# BB#20:
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpl	$0, 148(%r15)
	je	.LBB45_22
# BB#21:
	movl	$.L.str.16, %edi
	callq	puts
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	clause_Print
	movl	$.L.str.17, %edi
	callq	puts
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fol_PrettyPrint
	movl	$.L.str.18, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	fol_PrettyPrint
.LBB45_22:
	movq	$0, 16(%rsp)
	movq	%rbp, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	leaq	16(%rsp), %rbp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	ren_Rename
	movq	%rax, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	cnf_SkolemFormula
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB45_25
# BB#23:                                # %.lr.ph.i80.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB45_24:                              # %.lr.ph.i80
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_OR(%rip), %edi
	movq	8(%rbx), %rsi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_24
.LBB45_25:                              # %cnf_DistributiveFormula.exit
	movl	fol_AND(%rip), %edi
	movq	%rbp, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_Delete
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, %rbp
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB45_27
	.p2align	4, 0x90
.LBB45_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB45_26
.LBB45_27:                              # %list_Delete.exit
	movq	%rbx, %rdi
	callq	term_Delete
	cmpl	$0, 148(%r15)
	je	.LBB45_33
# BB#28:
	movl	$.L.str.19, %edi
	callq	puts
	testq	%rbp, %rbp
	je	.LBB45_32
# BB#29:                                # %.lr.ph.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB45_30:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_30
	jmp	.LBB45_33
.LBB45_31:                              # %._crit_edge.thread
	movq	%rbp, %rdi
	callq	term_Delete
.LBB45_32:                              # %.loopexit
	xorl	%ebp, %ebp
.LBB45_33:                              # %.loopexit
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	cnf_ApplyDefinitionToClause, .Lfunc_end45-cnf_ApplyDefinitionToClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_IsTrue,@function
fol_IsTrue:                             # @fol_IsTrue
	.cfi_startproc
# BB#0:
	movl	fol_TRUE(%rip), %ecx
	xorl	%eax, %eax
	cmpl	(%rdi), %ecx
	sete	%al
	retq
.Lfunc_end46:
	.size	fol_IsTrue, .Lfunc_end46-fol_IsTrue
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_IsFalse,@function
fol_IsFalse:                            # @fol_IsFalse
	.cfi_startproc
# BB#0:
	movl	fol_FALSE(%rip), %ecx
	xorl	%eax, %eax
	cmpl	(%rdi), %ecx
	sete	%al
	retq
.Lfunc_end47:
	.size	fol_IsFalse, .Lfunc_end47-fol_IsFalse
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_MakeOneOrTerm,@function
cnf_MakeOneOrTerm:                      # @cnf_MakeOneOrTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi441:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi442:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi443:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi444:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi445:
	.cfi_def_cfa_offset 48
.Lcfi446:
	.cfi_offset %rbx, -48
.Lcfi447:
	.cfi_offset %r12, -40
.Lcfi448:
	.cfi_offset %r14, -32
.Lcfi449:
	.cfi_offset %r15, -24
.Lcfi450:
	.cfi_offset %rbp, -16
	callq	cnf_MakeOneOr
	movq	%rax, %r15
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB48_23
# BB#1:                                 # %.lr.ph.i.i
	movl	fol_AND(%rip), %eax
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB48_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpl	%eax, (%rdx)
	je	.LBB48_5
# BB#3:                                 #   in Loop: Header=BB48_2 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB48_2
	jmp	.LBB48_23
	.p2align	4, 0x90
.LBB48_5:                               # %.lr.ph57.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	cmpl	%eax, (%rcx)
	je	.LBB48_6
# BB#4:                                 # %cnf_TopIsAnd.exit.i
                                        #   in Loop: Header=BB48_5 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB48_5
	jmp	.LBB48_23
.LBB48_6:                               # %.preheader47.i
	testq	%rbx, %rbx
	je	.LBB48_23
# BB#7:                                 # %.lr.ph54.i
	movl	symbol_TYPEMASK(%rip), %r12d
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jns	.LBB48_10
	jmp	.LBB48_9
	.p2align	4, 0x90
.LBB48_24:                              # %fol_IsNegativeLiteral.exit.thread._crit_edge.i
	movq	8(%rbx), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	jns	.LBB48_10
.LBB48_9:                               # %symbol_IsPredicate.exit.i
	movl	%eax, %edx
	negl	%edx
	andl	%r12d, %edx
	cmpl	$2, %edx
	je	.LBB48_13
.LBB48_10:                              # %symbol_IsPredicate.exit.thread.i
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB48_14
# BB#11:
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB48_14
# BB#12:                                # %fol_IsNegativeLiteral.exit.i
	negl	%eax
	andl	%r12d, %eax
	cmpl	$2, %eax
	jne	.LBB48_14
.LBB48_13:
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB48_14:                              # %fol_IsNegativeLiteral.exit.thread.i
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB48_24
# BB#15:                                # %.preheader.i
	testq	%r14, %r14
	je	.LBB48_23
# BB#16:                                # %.lr.ph.i.preheader
	movq	16(%r15), %rax
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB48_17:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%r15)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB48_17
# BB#18:                                # %._crit_edge.i
	testq	%rax, %rax
	je	.LBB48_22
# BB#19:                                # %.preheader.i.i.preheader
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB48_20:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB48_20
# BB#21:
	movq	%rax, (%rcx)
.LBB48_22:                              # %list_Nconc.exit.i
	movq	%r14, 16(%r15)
.LBB48_23:                              # %cnf_MakeOneOrPredicate.exit
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	cnf_MakeOneOrTerm, .Lfunc_end48-cnf_MakeOneOrTerm
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_MakeOneAnd,@function
cnf_MakeOneAnd:                         # @cnf_MakeOneAnd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi451:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi452:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi453:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi454:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi455:
	.cfi_def_cfa_offset 48
.Lcfi456:
	.cfi_offset %rbx, -40
.Lcfi457:
	.cfi_offset %r12, -32
.Lcfi458:
	.cfi_offset %r14, -24
.Lcfi459:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	(%r14), %eax
	cmpl	fol_AND(%rip), %eax
	jne	.LBB49_11
# BB#1:
	movq	16(%r14), %r15
	testq	%r15, %r15
	jne	.LBB49_3
	jmp	.LBB49_16
	.p2align	4, 0x90
.LBB49_10:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB49_3 Depth=1
	movq	%r12, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB49_2:                               #   in Loop: Header=BB49_3 Depth=1
	testq	%r15, %r15
	je	.LBB49_16
.LBB49_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB49_8 Depth 2
	movq	8(%r15), %rbx
	movq	%rbx, %rdi
	callq	cnf_MakeOneAnd
	movl	(%rbx), %eax
	movq	(%r15), %r15
	cmpl	fol_AND(%rip), %eax
	jne	.LBB49_2
# BB#4:                                 #   in Loop: Header=BB49_3 Depth=1
	movq	16(%rbx), %r12
	movq	16(%r14), %rdi
	movq	%rbx, %rsi
	callq	list_PointerDeleteElement
	testq	%r12, %r12
	je	.LBB49_5
# BB#6:                                 #   in Loop: Header=BB49_3 Depth=1
	testq	%rax, %rax
	je	.LBB49_10
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB49_3 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB49_8:                               # %.preheader.i
                                        #   Parent Loop BB49_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB49_8
# BB#9:                                 #   in Loop: Header=BB49_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB49_10
.LBB49_5:                               #   in Loop: Header=BB49_3 Depth=1
	movq	%rax, %r12
	jmp	.LBB49_10
.LBB49_11:
	testl	%eax, %eax
	jns	.LBB49_13
# BB#12:                                # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB49_16
.LBB49_13:                              # %symbol_IsPredicate.exit.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB49_16
	.p2align	4, 0x90
.LBB49_14:                              # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_MakeOneAnd
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB49_14
.LBB49_16:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	cnf_MakeOneAnd, .Lfunc_end49-cnf_MakeOneAnd
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_MakeOneOr,@function
cnf_MakeOneOr:                          # @cnf_MakeOneOr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi460:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi461:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi462:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi463:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi464:
	.cfi_def_cfa_offset 48
.Lcfi465:
	.cfi_offset %rbx, -40
.Lcfi466:
	.cfi_offset %r12, -32
.Lcfi467:
	.cfi_offset %r14, -24
.Lcfi468:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	(%r14), %eax
	cmpl	fol_OR(%rip), %eax
	jne	.LBB50_11
# BB#1:
	movq	16(%r14), %r15
	testq	%r15, %r15
	jne	.LBB50_3
	jmp	.LBB50_16
	.p2align	4, 0x90
.LBB50_10:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB50_3 Depth=1
	movq	%r12, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB50_2:                               #   in Loop: Header=BB50_3 Depth=1
	testq	%r15, %r15
	je	.LBB50_16
.LBB50_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB50_8 Depth 2
	movq	8(%r15), %rbx
	movq	%rbx, %rdi
	callq	cnf_MakeOneOr
	movl	(%rbx), %eax
	movq	(%r15), %r15
	cmpl	fol_OR(%rip), %eax
	jne	.LBB50_2
# BB#4:                                 #   in Loop: Header=BB50_3 Depth=1
	movq	16(%rbx), %r12
	movq	16(%r14), %rdi
	movq	%rbx, %rsi
	callq	list_PointerDeleteElement
	testq	%r12, %r12
	je	.LBB50_5
# BB#6:                                 #   in Loop: Header=BB50_3 Depth=1
	testq	%rax, %rax
	je	.LBB50_10
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB50_3 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB50_8:                               # %.preheader.i
                                        #   Parent Loop BB50_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB50_8
# BB#9:                                 #   in Loop: Header=BB50_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB50_10
.LBB50_5:                               #   in Loop: Header=BB50_3 Depth=1
	movq	%rax, %r12
	jmp	.LBB50_10
.LBB50_11:
	testl	%eax, %eax
	jns	.LBB50_13
# BB#12:                                # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB50_16
.LBB50_13:                              # %symbol_IsPredicate.exit.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB50_16
	.p2align	4, 0x90
.LBB50_14:                              # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	cnf_MakeOneOr
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB50_14
.LBB50_16:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end50:
	.size	cnf_MakeOneOr, .Lfunc_end50-cnf_MakeOneOr
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_OptimizedSkolemFormula,@function
cnf_OptimizedSkolemFormula:             # @cnf_OptimizedSkolemFormula
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi469:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi470:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi471:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi472:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi473:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi474:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi475:
	.cfi_def_cfa_offset 224
.Lcfi476:
	.cfi_offset %rbx, -56
.Lcfi477:
	.cfi_offset %r12, -48
.Lcfi478:
	.cfi_offset %r13, -40
.Lcfi479:
	.cfi_offset %r14, -32
.Lcfi480:
	.cfi_offset %r15, -24
.Lcfi481:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	248(%rsp), %esi
	movq	224(%rsp), %r15
	movl	(%r13), %eax
	cmpl	%eax, fol_ALL(%rip)
	sete	%cl
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r9, 104(%rsp)          # 8-byte Spill
	je	.LBB51_2
# BB#1:
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB51_251
.LBB51_2:                               # %.lr.ph314.lr.ph
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB51_240
.LBB51_3:                               #   in Loop: Header=BB51_240 Depth=1
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	104(%rbp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	callq	fol_FreeVariables
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	16(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
	cmpl	fol_AND(%rip), %edx
	jne	.LBB51_61
# BB#4:                                 #   in Loop: Header=BB51_240 Depth=1
	cmpl	$0, 212(%rbx)
	movl	232(%rsp), %r15d
	je	.LBB51_54
# BB#5:                                 #   in Loop: Header=BB51_240 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB51_54
# BB#6:                                 # %.lr.ph323.lr.ph
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_7:                               #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_13 Depth 3
                                        #       Child Loop BB51_17 Depth 3
                                        #       Child Loop BB51_20 Depth 3
                                        #         Child Loop BB51_23 Depth 4
                                        #         Child Loop BB51_26 Depth 4
                                        #       Child Loop BB51_35 Depth 3
                                        #       Child Loop BB51_43 Depth 3
                                        #       Child Loop BB51_48 Depth 3
	testl	%r15d, %r15d
	jne	.LBB51_52
# BB#8:                                 #   in Loop: Header=BB51_7 Depth=2
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB51_11
# BB#9:                                 #   in Loop: Header=BB51_7 Depth=2
	movl	$1, %r14d
	cmpl	$0, 132(%rbx)
	je	.LBB51_52
# BB#10:                                #   in Loop: Header=BB51_7 Depth=2
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB51_51
.LBB51_11:                              #   in Loop: Header=BB51_7 Depth=2
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	8(%rcx), %rdi
	callq	term_Copy
	movq	%rax, %r15
	movl	$term_Copy, %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	list_CopyWithElement
	movq	%rax, %rbp
	movl	fol_EXIST(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	fol_FreeVariables
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB51_15
# BB#12:                                #   in Loop: Header=BB51_7 Depth=2
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_CopyWithElement
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB51_13:                              # %.lr.ph.i.i156
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB51_13
# BB#14:                                # %list_Delete.exit.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movl	fol_ALL(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbp
.LBB51_15:                              # %cnf_QuantifyAndNegate.exit
                                        #   in Loop: Header=BB51_7 Depth=2
	movl	fol_NOT(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %r14
	movq	112(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movq	%rbx, %rdi
	callq	cnf_RemoveTrivialAtoms
	movq	%rax, %rdi
	callq	cnf_RemoveTrivialOperators
	movq	%rax, %rdi
	callq	cnf_SimplifyQuantors
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	224(%rsp), %rbx
	movq	%rbx, %rdx
	callq	ren_Rename
	movq	%rax, %rdi
	callq	cnf_RemoveEquivImplFromFormula
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rdi
	callq	cnf_AntiPrenex
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	cnf_SkolemFormula
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	cnf_ComputeLiteralLists
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB51_18
# BB#16:                                # %.lr.ph.i9.i.preheader
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB51_17:                              # %.lr.ph.i9.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	fol_OR(%rip), %edi
	movq	8(%rbx), %rsi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_17
.LBB51_18:                              # %cnf_DistributiveFormula.exit.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movl	fol_AND(%rip), %edi
	movq	%rbp, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	term_Delete
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	cnf_MakeClauseList
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	term_Delete
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	testq	%rbp, %rbp
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	240(%rsp), %rsi
	movq	56(%rsp), %r14          # 8-byte Reload
	je	.LBB51_30
# BB#19:                                # %.lr.ph17.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_20:                              #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_23 Depth 4
                                        #         Child Loop BB51_26 Depth 4
	movq	8(%rbp), %rax
	orl	$8, 48(%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	je	.LBB51_29
# BB#21:                                #   in Loop: Header=BB51_20 Depth=3
	movq	8(%rbp), %rbx
	movq	%rbx, %rax
	movabsq	$1908283869694091547, %rcx # imm = 0x1A7B9611A7B9611B
	mulq	%rcx
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rcx
	movq	%rbx, %rax
	subq	%rcx, %rax
	movq	(%rsi,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB51_27
	.p2align	4, 0x90
.LBB51_23:                              # %.lr.ph.i7.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        #       Parent Loop BB51_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rcx), %r15
	cmpq	%rbx, 8(%r15)
	je	.LBB51_24
# BB#22:                                #   in Loop: Header=BB51_23 Depth=4
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB51_23
.LBB51_27:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB51_20 Depth=3
	leaq	(%rsi,%rax,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r14, 8(%r12)
	movq	$0, (%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	%r12, (%r14)
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB51_28
.LBB51_24:                              #   in Loop: Header=BB51_20 Depth=3
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB51_28
	.p2align	4, 0x90
.LBB51_26:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        #       Parent Loop BB51_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%r14, 8(%rax)
	je	.LBB51_29
# BB#25:                                #   in Loop: Header=BB51_26 Depth=4
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB51_26
.LBB51_28:                              # %list_PointerMember.exit.sink.split.i.i
                                        #   in Loop: Header=BB51_20 Depth=3
	movq	(%r15), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r15)
	movq	240(%rsp), %rsi
	movq	56(%rsp), %r14          # 8-byte Reload
.LBB51_29:                              # %hsh_Put.exit.i
                                        #   in Loop: Header=BB51_20 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB51_20
.LBB51_30:                              # %._crit_edge18.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	cnf_SatUnit
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB51_37
# BB#31:                                #   in Loop: Header=BB51_7 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	movl	232(%rsp), %r15d
	je	.LBB51_40
# BB#32:                                #   in Loop: Header=BB51_7 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rbp, %rsi
	movq	(%rax), %rbp
	movq	8(%rbx), %rdi
	movq	240(%rsp), %rdx
	callq	cnf_GetUsedTerms
	testq	%rbp, %rbp
	je	.LBB51_38
# BB#33:                                #   in Loop: Header=BB51_7 Depth=2
	testq	%rax, %rax
	je	.LBB51_39
# BB#34:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB51_35:                              # %.preheader.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB51_35
# BB#36:                                #   in Loop: Header=BB51_7 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB51_39
.LBB51_37:                              #   in Loop: Header=BB51_7 Depth=2
	xorl	%r14d, %r14d
	movl	232(%rsp), %r15d
	jmp	.LBB51_41
.LBB51_38:                              #   in Loop: Header=BB51_7 Depth=2
	movq	%rax, %rbp
.LBB51_39:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rbp, (%rax)
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB51_40:                              #   in Loop: Header=BB51_7 Depth=2
	movq	%rbx, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rdi
	callq	clause_DeleteClauseList
	movl	$1, %r14d
.LBB51_41:                              #   in Loop: Header=BB51_7 Depth=2
	movq	56(%rbp), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB51_50
# BB#42:                                # %.lr.ph.i
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB51_43:                              #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	movq	8(%rbx), %rsi
	je	.LBB51_45
# BB#44:                                #   in Loop: Header=BB51_43 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	prfs_MoveUsableDocProof
	jmp	.LBB51_46
	.p2align	4, 0x90
.LBB51_45:                              #   in Loop: Header=BB51_43 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	prfs_DeleteUsable
.LBB51_46:                              #   in Loop: Header=BB51_43 Depth=3
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_43
# BB#47:                                # %.lr.ph.i.i161.preheader
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	88(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_48:                              # %.lr.ph.i.i161
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB51_48
# BB#49:                                #   in Loop: Header=BB51_7 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB51_51
.LBB51_50:                              #   in Loop: Header=BB51_7 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_51:                              # %cnf_HaveProofOptSkolem.exit
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB51_52:                              # %cnf_HaveProofOptSkolem.exit
                                        #   in Loop: Header=BB51_7 Depth=2
	movl	%r14d, %eax
	orl	%r15d, %eax
	jne	.LBB51_90
# BB#53:                                # %.outer
                                        #   in Loop: Header=BB51_7 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB51_7
.LBB51_54:                              # %.critedge.thread
                                        #   in Loop: Header=BB51_240 Depth=1
	cmpl	$0, 216(%rbx)
	je	.LBB51_62
# BB#55:                                #   in Loop: Header=BB51_240 Depth=1
	movq	104(%rbp), %r15
	movq	112(%rbp), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	cmpl	$0, 216(%rax)
	je	.LBB51_57
# BB#56:                                #   in Loop: Header=BB51_240 Depth=1
	movq	%r12, %rdi
	callq	term_MaxVar
	movl	%eax, symbol_STANDARDVARCOUNTER(%rip)
.LBB51_57:                              #   in Loop: Header=BB51_240 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB51_133
# BB#58:                                # %.lr.ph.i.i206.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB51_59:                              # %.lr.ph.i.i206
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movslq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %rbp
	jne	.LBB51_59
# BB#60:                                # %cnf_GetSymbolList.exit.i208.loopexit
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	jmp	.LBB51_134
.LBB51_61:                              #   in Loop: Header=BB51_240 Depth=1
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB51_63
.LBB51_62:                              # %.critedge.thread.cnf_StrongSkolemization.exit_crit_edge
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	16(%r13), %rax
	movq	56(%rsp), %r14          # 8-byte Reload
.LBB51_63:                              # %cnf_StrongSkolemization.exit
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB51_67
# BB#64:                                # %.lr.ph.i34.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB51_65:                              # %.lr.ph.i34.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movslq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r14, 8(%r12)
	movq	%rbp, (%r12)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%r12, %rbp
	jne	.LBB51_65
# BB#66:                                # %cnf_GetSymbolList.exit.loopexit.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	56(%rsp), %r14          # 8-byte Reload
	jmp	.LBB51_68
.LBB51_67:                              #   in Loop: Header=BB51_240 Depth=1
	xorl	%r12d, %r12d
.LBB51_68:                              # %cnf_GetSymbolList.exit.i
                                        #   in Loop: Header=BB51_240 Depth=1
	callq	term_Delete
	movq	16(%r13), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB51_71
# BB#69:                                # %.lr.ph.i39.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB51_71
	.p2align	4, 0x90
.LBB51_70:                              # %.lr.ph.i39..lr.ph.i39_crit_edge.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB51_70
.LBB51_71:                              # %list_Delete.exit41.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, (%r13)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r13)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movl	vec_MAX(%rip), %ebx
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	list_Length
	movl	%eax, 64(%rsp)          # 4-byte Spill
	testq	%r12, %r12
	je	.LBB51_88
# BB#72:                                # %.lr.ph49.preheader.i.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	vec_MAX(%rip), %eax
	movq	%r12, %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB51_73:                              # %.lr.ph49.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_74 Depth 3
                                        #         Child Loop BB51_76 Depth 4
                                        #         Child Loop BB51_80 Depth 4
	leal	1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	%r13, vec_VECTOR(,%rax,8)
	movl	64(%rsp), %edi          # 4-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	symbol_CreateSkolemFunction
	movl	%eax, %r14d
	movslq	%r14d, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, (%rax)
	movl	vec_MAX(%rip), %eax
	cmpl	%eax, %ebx
	movq	112(%rsp), %r13         # 8-byte Reload
	je	.LBB51_82
	.p2align	4, 0x90
.LBB51_74:                              #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_73 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_76 Depth 4
                                        #         Child Loop BB51_80 Depth 4
	leal	-1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	vec_VECTOR-8(,%rax,8), %rbp
	movl	(%rbp), %eax
	cmpl	8(%r15), %eax
	jne	.LBB51_78
# BB#75:                                #   in Loop: Header=BB51_74 Depth=3
	movl	%r14d, (%rbp)
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB51_77
	.p2align	4, 0x90
.LBB51_76:                              # %.lr.ph.i.i.i184
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_73 Depth=2
                                        #       Parent Loop BB51_74 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB51_76
.LBB51_77:                              # %list_Delete.exit.i.i185
                                        #   in Loop: Header=BB51_74 Depth=3
	movl	$term_Copy, %esi
	movq	%r13, %rdi
	callq	list_CopyWithElement
	movq	%rax, 16(%rbp)
	movl	vec_MAX(%rip), %ecx
	testq	%rax, %rax
	jne	.LBB51_79
	jmp	.LBB51_81
	.p2align	4, 0x90
.LBB51_78:                              # %._crit_edge51.i.i
                                        #   in Loop: Header=BB51_74 Depth=3
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB51_81
.LBB51_79:                              # %.lr.ph.i31.preheader.i
                                        #   in Loop: Header=BB51_74 Depth=3
	movslq	%ecx, %rdx
	leaq	vec_VECTOR(,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB51_80:                              # %.lr.ph.i31.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_73 Depth=2
                                        #       Parent Loop BB51_74 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rax), %rsi
	incl	%ecx
	movl	%ecx, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rax), %rax
	addq	$8, %rdx
	testq	%rax, %rax
	jne	.LBB51_80
.LBB51_81:                              # %.backedge.i.i186
                                        #   in Loop: Header=BB51_74 Depth=3
	cmpl	%ecx, %ebx
	movl	%ecx, %eax
	jne	.LBB51_74
.LBB51_82:                              # %._crit_edge.i.i190
                                        #   in Loop: Header=BB51_73 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	movl	%ebx, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jne	.LBB51_73
# BB#83:                                # %.lr.ph.i29.preheader.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	%ebx, vec_MAX(%rip)
	.p2align	4, 0x90
.LBB51_84:                              # %.lr.ph.i29.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB51_84
# BB#85:                                # %list_Delete.exit30.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	224(%rsp), %r15
	je	.LBB51_89
# BB#86:                                # %.lr.ph.i.i194.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_87:                              # %.lr.ph.i.i194
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	%rcx, %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB51_87
	jmp	.LBB51_237
.LBB51_88:                              # %list_Delete.exit30.thread.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	%ebx, vec_MAX(%rip)
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	224(%rsp), %r15
	jmp	.LBB51_237
.LBB51_89:                              #   in Loop: Header=BB51_240 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB51_237
.LBB51_90:                              #   in Loop: Header=BB51_240 Depth=1
	cmpl	$0, 132(%rbx)
	je	.LBB51_92
# BB#91:                                #   in Loop: Header=BB51_240 Depth=1
	movq	%rcx, %rbx
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	term_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rdi
	callq	term_Print
	movl	$.L.str.27, %edi
	callq	puts
	movq	%rbx, %rcx
.LBB51_92:                              #   in Loop: Header=BB51_240 Depth=1
	movq	8(%rcx), %rsi
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rbx), %rdi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	list_PointerDeleteElement
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	callq	list_Length
	cmpl	$1, %eax
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	ja	.LBB51_96
# BB#93:                                #   in Loop: Header=BB51_240 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rcx
	movq	8(%rcx), %rax
	testq	%rcx, %rcx
	je	.LBB51_95
	.p2align	4, 0x90
.LBB51_94:                              # %.lr.ph.i67.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB51_94
.LBB51_95:                              # %list_Delete.exit68.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	(%rax), %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
.LBB51_96:                              # %.preheader.i
                                        #   in Loop: Header=BB51_240 Depth=1
	testq	%rbp, %rbp
	je	.LBB51_114
	.p2align	4, 0x90
.LBB51_97:                              # %.lr.ph.i163
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_98 Depth 3
                                        #         Child Loop BB51_102 Depth 4
                                        #       Child Loop BB51_106 Depth 3
                                        #         Child Loop BB51_110 Depth 4
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rax
	movl	(%rax), %r15d
	movq	%rbx, %rdi
	callq	list_Length
	movl	%eax, %edi
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	symbol_CreateSkolemFunction
	movl	%eax, %ebp
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_CopyWithElement
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r12
	movl	vec_MAX(%rip), %r13d
	leal	1(%r13), %ebp
	movl	%ebp, vec_MAX(%rip)
	movslq	%r13d, %rbx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, vec_VECTOR(,%rbx,8)
	movl	%ebp, %eax
	.p2align	4, 0x90
.LBB51_98:                              # %.backedge._crit_edge.i76.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_97 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_102 Depth 4
	movslq	%eax, %rsi
	leal	-1(%rsi), %edx
	movl	%edx, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rsi,8), %r14
	cmpl	%r15d, (%r14)
	jne	.LBB51_100
# BB#99:                                #   in Loop: Header=BB51_98 Depth=3
	movl	(%r12), %eax
	movl	%eax, (%r14)
	movq	16(%r12), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%r14)
	movl	vec_MAX(%rip), %edx
	jmp	.LBB51_104
	.p2align	4, 0x90
.LBB51_100:                             #   in Loop: Header=BB51_98 Depth=3
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB51_104
# BB#101:                               # %.lr.ph.i88.i.preheader
                                        #   in Loop: Header=BB51_98 Depth=3
	decq	%rsi
	leaq	vec_VECTOR(,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB51_102:                             # %.lr.ph.i88.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_97 Depth=2
                                        #       Parent Loop BB51_98 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rcx), %rsi
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	addq	$8, %rdx
	incl	%eax
	testq	%rcx, %rcx
	jne	.LBB51_102
# BB#103:                               # %.backedge.i80.i.loopexit
                                        #   in Loop: Header=BB51_98 Depth=3
	decl	%eax
	movl	%eax, %edx
.LBB51_104:                             # %.backedge.i80.i
                                        #   in Loop: Header=BB51_98 Depth=3
	cmpl	%edx, %r13d
	movl	%edx, %eax
	jne	.LBB51_98
# BB#105:                               # %cnf_RplacVarsymbFunction.exit89.i
                                        #   in Loop: Header=BB51_97 Depth=2
	movl	%ebp, vec_MAX(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, vec_VECTOR(,%rbx,8)
	.p2align	4, 0x90
.LBB51_106:                             # %.backedge._crit_edge.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_97 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_110 Depth 4
	movslq	%ebp, %rdx
	leal	-1(%rdx), %ecx
	movl	%ecx, vec_MAX(%rip)
	movq	vec_VECTOR-8(,%rdx,8), %rbx
	cmpl	%r15d, (%rbx)
	jne	.LBB51_108
# BB#107:                               #   in Loop: Header=BB51_106 Depth=3
	movl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	16(%r12), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rbx)
	movl	vec_MAX(%rip), %ecx
	jmp	.LBB51_112
	.p2align	4, 0x90
.LBB51_108:                             #   in Loop: Header=BB51_106 Depth=3
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB51_112
# BB#109:                               # %.lr.ph.i71.i.preheader
                                        #   in Loop: Header=BB51_106 Depth=3
	decq	%rdx
	leaq	vec_VECTOR(,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB51_110:                             # %.lr.ph.i71.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_97 Depth=2
                                        #       Parent Loop BB51_106 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rax), %rdx
	movl	%ebp, vec_MAX(%rip)
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	addq	$8, %rcx
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB51_110
# BB#111:                               # %.backedge.i69.i.loopexit
                                        #   in Loop: Header=BB51_106 Depth=3
	decl	%ebp
	movl	%ebp, %ecx
.LBB51_112:                             # %.backedge.i69.i
                                        #   in Loop: Header=BB51_106 Depth=3
	cmpl	%ecx, %r13d
	movl	%ecx, %ebp
	jne	.LBB51_106
# BB#113:                               # %cnf_RplacVarsymbFunction.exit.i
                                        #   in Loop: Header=BB51_97 Depth=2
	movl	%r13d, vec_MAX(%rip)
	movq	%r12, %rdi
	callq	term_Delete
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	112(%rsp), %rbx         # 8-byte Reload
	jne	.LBB51_97
.LBB51_114:                             # %._crit_edge.i
                                        #   in Loop: Header=BB51_240 Depth=1
	testq	%rbx, %rbx
	je	.LBB51_230
# BB#115:                               #   in Loop: Header=BB51_240 Depth=1
	movl	fol_ALL(%rip), %r14d
	movl	$term_Copy, %esi
	movq	%rbx, %rdi
	callq	list_CopyWithElement
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	list_Length
	cmpl	$2, %eax
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	224(%rsp), %r15
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jb	.LBB51_231
# BB#116:                               #   in Loop: Header=BB51_240 Depth=1
	movslq	vec_MAX(%rip), %rcx
	leal	1(%rcx), %eax
	movl	%eax, vec_MAX(%rip)
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rbp, vec_VECTOR(,%rcx,8)
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_117:                             # %.lr.ph67.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_122 Depth 3
                                        #       Child Loop BB51_124 Depth 3
                                        #       Child Loop BB51_126 Depth 3
                                        #       Child Loop BB51_129 Depth 3
	movq	%r14, %rbx
	leal	-1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	vec_VECTOR-8(,%rax,8), %r12
	movl	(%r12), %r14d
	cmpl	%r14d, fol_ALL(%rip)
	je	.LBB51_119
# BB#118:                               # %.lr.ph67.i.i
                                        #   in Loop: Header=BB51_117 Depth=2
	cmpl	%r14d, fol_EXIST(%rip)
	jne	.LBB51_127
.LBB51_119:                             #   in Loop: Header=BB51_117 Depth=2
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Length
	cmpl	$2, %eax
	jb	.LBB51_127
# BB#120:                               #   in Loop: Header=BB51_117 Depth=2
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	callq	list_Copy
	movl	%r14d, %ecx
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB51_127
# BB#121:                               # %.lr.ph.i63.i.preheader
                                        #   in Loop: Header=BB51_117 Depth=2
	movq	%r14, %rbx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB51_122:                             # %.lr.ph.i63.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	movq	8(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_122
# BB#123:                               # %.lr.ph63.i.i.preheader
                                        #   in Loop: Header=BB51_117 Depth=2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB51_124:                             # %.lr.ph63.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %rax
	movq	8(%rax), %rbp
	movq	16(%rbp), %rdi
	movq	8(%rbx), %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_124
# BB#125:                               # %.lr.ph.i.i.i166.preheader
                                        #   in Loop: Header=BB51_117 Depth=2
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	224(%rsp), %r15
	movq	72(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_126:                             # %.lr.ph.i.i.i166
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB51_126
	.p2align	4, 0x90
.LBB51_127:                             # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB51_117 Depth=2
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	movl	vec_MAX(%rip), %eax
	je	.LBB51_130
# BB#128:                               # %.lr.ph65.i.preheader.i
                                        #   in Loop: Header=BB51_117 Depth=2
	movslq	%eax, %rdx
	leaq	vec_VECTOR(,%rdx,8), %rdx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB51_129:                             # %.lr.ph65.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_117 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rsi
	incl	%eax
	movl	%eax, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rcx), %rcx
	addq	$8, %rdx
	testq	%rcx, %rcx
	jne	.LBB51_129
	jmp	.LBB51_131
.LBB51_130:                             #   in Loop: Header=BB51_117 Depth=2
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%rbx, %r14
.LBB51_131:                             # %.backedge.i.i
                                        #   in Loop: Header=BB51_117 Depth=2
	cmpl	%eax, 80(%rsp)          # 4-byte Folded Reload
	jne	.LBB51_117
# BB#132:                               # %cnf_QuantMakeOneVar.exit.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, vec_MAX(%rip)
	jmp	.LBB51_232
.LBB51_133:                             #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB51_134:                             # %cnf_GetSymbolList.exit.i208
                                        #   in Loop: Header=BB51_240 Depth=1
	callq	term_Delete
	movq	16(%r13), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB51_137
# BB#135:                               # %.lr.ph.i189.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB51_137
	.p2align	4, 0x90
.LBB51_136:                             # %.lr.ph.i189.i..lr.ph.i189.i_crit_edge
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB51_136
.LBB51_137:                             # %list_Delete.exit.i214
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, (%r13)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r13)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r13), %r12
	testq	%r12, %r12
	movq	%r15, 64(%rsp)          # 8-byte Spill
	je	.LBB51_167
# BB#138:                               # %.lr.ph341
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_139:                             #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_141 Depth 3
                                        #         Child Loop BB51_142 Depth 4
                                        #       Child Loop BB51_147 Depth 3
                                        #       Child Loop BB51_150 Depth 3
                                        #         Child Loop BB51_154 Depth 4
                                        #         Child Loop BB51_157 Depth 4
	movq	8(%r12), %r15
	movq	%r15, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB51_148
# BB#140:                               # %.lr.ph.i190.i
                                        #   in Loop: Header=BB51_139 Depth=2
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %r13
	je	.LBB51_146
	.p2align	4, 0x90
.LBB51_141:                             # %.lr.ph.split.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_142 Depth 4
	movq	8(%r13), %rax
	movslq	(%rax), %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB51_142:                             # %.lr.ph.i.i.i217
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        #       Parent Loop BB51_141 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	symbol_Equal
	testl	%eax, %eax
	jne	.LBB51_144
# BB#143:                               #   in Loop: Header=BB51_142 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB51_142
	jmp	.LBB51_145
	.p2align	4, 0x90
.LBB51_144:                             # %list_Member.exit.i.i
                                        #   in Loop: Header=BB51_141 Depth=3
	movq	$0, 8(%r13)
.LBB51_145:                             # %list_Member.exit.thread.i.i
                                        #   in Loop: Header=BB51_141 Depth=3
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB51_141
	jmp	.LBB51_148
	.p2align	4, 0x90
.LBB51_146:                             # %.lr.ph.split.us.i.i.preheader
                                        #   in Loop: Header=BB51_139 Depth=2
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB51_147:                             # %.lr.ph.split.us.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB51_147
.LBB51_148:                             # %cnf_FreeVariablesBut.exit.i
                                        #   in Loop: Header=BB51_139 Depth=2
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r15, 8(%r14)
	movq	%rbx, (%r14)
	movq	96(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB51_161
# BB#149:                               # %.lr.ph334
                                        #   in Loop: Header=BB51_139 Depth=2
	movq	%rbx, %rdi
	callq	list_Length
	movl	%eax, %r15d
	movq	%rbp, %rax
	xorl	%ebp, %ebp
	movq	72(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_150:                             #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_154 Depth 4
                                        #         Child Loop BB51_157 Depth 4
	movq	%rbp, %rbx
	movq	%rax, %rbp
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	callq	list_Length
	cmpl	%r15d, %eax
	jl	.LBB51_159
# BB#151:                               #   in Loop: Header=BB51_150 Depth=3
	jne	.LBB51_162
# BB#152:                               #   in Loop: Header=BB51_150 Depth=3
	movq	8(%rbp), %rax
	movq	(%rax), %rsi
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.LBB51_155
# BB#153:                               # %.lr.ph37.i.i
                                        #   in Loop: Header=BB51_150 Depth=3
	movq	cnf_VARIABLEDEPTHARRAY(%rip), %rdx
	.p2align	4, 0x90
.LBB51_154:                             #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        #       Parent Loop BB51_150 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	movslq	(%rdi), %rdi
	movl	(%rdx,%rdi,4), %edi
	cmpl	%ecx, %edi
	cmovgel	%edi, %ecx
	testq	%rsi, %rsi
	jne	.LBB51_154
.LBB51_155:                             # %.preheader.i.i218
                                        #   in Loop: Header=BB51_150 Depth=3
	testq	%rax, %rax
	je	.LBB51_159
# BB#156:                               # %.lr.ph.i193.i
                                        #   in Loop: Header=BB51_150 Depth=3
	movq	cnf_VARIABLEDEPTHARRAY(%rip), %rdx
	.p2align	4, 0x90
.LBB51_157:                             #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_139 Depth=2
                                        #       Parent Loop BB51_150 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rax), %rsi
	movslq	(%rsi), %rsi
	cmpl	%ecx, (%rdx,%rsi,4)
	jge	.LBB51_163
# BB#158:                               #   in Loop: Header=BB51_157 Depth=4
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB51_157
	.p2align	4, 0x90
.LBB51_159:                             # %.backedge
                                        #   in Loop: Header=BB51_150 Depth=3
	movq	(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB51_150
# BB#160:                               # %cnf_HasDeeperVariable.exit.i.thread232
                                        #   in Loop: Header=BB51_139 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rbp)
	jmp	.LBB51_166
	.p2align	4, 0x90
.LBB51_161:                             #   in Loop: Header=BB51_139 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB51_166
.LBB51_162:                             # %cnf_HasDeeperVariable.exit.i.thread
                                        #   in Loop: Header=BB51_139 Depth=2
	cmpl	%r15d, %eax
	jl	.LBB51_166
	.p2align	4, 0x90
.LBB51_163:                             # %cnf_HasDeeperVariable.exit.i.thread.thread
                                        #   in Loop: Header=BB51_139 Depth=2
	testq	%rbx, %rbx
	je	.LBB51_165
# BB#164:                               #   in Loop: Header=BB51_139 Depth=2
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	list_InsertNext
	jmp	.LBB51_166
.LBB51_165:                             #   in Loop: Header=BB51_139 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_166:                             #   in Loop: Header=BB51_139 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB51_139
	jmp	.LBB51_168
.LBB51_167:                             #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB51_168:                             # %._crit_edge342
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	%r13, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB51_177
# BB#169:                               # %.lr.ph.i196.i
                                        #   in Loop: Header=BB51_240 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %r15
	je	.LBB51_175
	.p2align	4, 0x90
.LBB51_170:                             # %.lr.ph.split.i206.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_171 Depth 3
	movq	8(%r15), %rax
	movslq	(%rax), %rbp
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB51_171:                             # %.lr.ph.i.i210.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_170 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rsi
	movq	%rbp, %rdi
	callq	symbol_Equal
	testl	%eax, %eax
	jne	.LBB51_173
# BB#172:                               #   in Loop: Header=BB51_171 Depth=3
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_171
	jmp	.LBB51_174
	.p2align	4, 0x90
.LBB51_173:                             # %list_Member.exit.i213.i
                                        #   in Loop: Header=BB51_170 Depth=2
	movq	$0, 8(%r15)
.LBB51_174:                             # %list_Member.exit.thread.i216.i
                                        #   in Loop: Header=BB51_170 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB51_170
	jmp	.LBB51_177
.LBB51_175:                             # %.lr.ph.split.us.i200.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB51_176:                             # %.lr.ph.split.us.i200.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB51_176
.LBB51_177:                             # %cnf_FreeVariablesBut.exit217.i
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	list_Length
	movl	%eax, %ebx
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	$0, %ecx
	movq	224(%rsp), %r14
	movq	64(%rsp), %rsi          # 8-byte Reload
	je	.LBB51_180
# BB#178:                               # %.lr.ph346.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB51_179:                             # %.lr.ph346
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	24(%rsp), %edi          # 4-byte Reload
	callq	symbol_CreateSkolemFunction
	movslq	%eax, %r13
	movq	(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r14)
	movq	8(%rbp), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r15, 8(%r12)
	movq	%r13, (%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movl	24(%rsp), %ebx          # 4-byte Reload
	movq	%rax, %rcx
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r12, 8(%rcx)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rcx, %rax
	jne	.LBB51_179
.LBB51_180:                             # %._crit_edge347
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	%r14, %r15
	je	.LBB51_182
	.p2align	4, 0x90
.LBB51_181:                             # %.lr.ph.i221.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB51_181
.LBB51_182:                             # %list_Delete.exit222.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	testl	%ebx, %ebx
	jle	.LBB51_185
# BB#183:                               # %list_Delete.exit222.i.preheader490
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB51_184:                             # %list_Delete.exit222.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	term_CreateStandardVariable
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbp, (%rax)
	decl	%ebx
	movq	%rax, %rbp
	jne	.LBB51_184
	jmp	.LBB51_186
.LBB51_185:                             #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
.LBB51_186:                             # %.preheader269
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB51_219
# BB#187:                               # %.lr.ph363
                                        #   in Loop: Header=BB51_240 Depth=1
	xorl	%ecx, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB51_188:                             #   Parent Loop BB51_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_191 Depth 3
                                        #       Child Loop BB51_197 Depth 3
                                        #       Child Loop BB51_201 Depth 3
                                        #       Child Loop BB51_206 Depth 3
                                        #         Child Loop BB51_207 Depth 4
                                        #           Child Loop BB51_209 Depth 5
                                        #           Child Loop BB51_213 Depth 5
                                        #       Child Loop BB51_217 Depth 3
	movq	8(%rcx), %rax
	movq	(%rax), %rax
	movq	136(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	je	.LBB51_193
# BB#189:                               #   in Loop: Header=BB51_188 Depth=2
	testq	%rax, %rax
	movq	128(%rsp), %r15         # 8-byte Reload
	je	.LBB51_194
# BB#190:                               # %.preheader.i223.i.preheader
                                        #   in Loop: Header=BB51_188 Depth=2
	movq	%rdi, %rdx
	movq	120(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB51_191:                             # %.preheader.i223.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB51_191
# BB#192:                               #   in Loop: Header=BB51_188 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB51_195
	.p2align	4, 0x90
.LBB51_193:                             #   in Loop: Header=BB51_188 Depth=2
	movq	%rax, %rdi
	movq	128(%rsp), %r15         # 8-byte Reload
.LBB51_194:                             #   in Loop: Header=BB51_188 Depth=2
	movq	120(%rsp), %r12         # 8-byte Reload
.LBB51_195:                             # %list_Nconc.exit.i223
                                        #   in Loop: Header=BB51_188 Depth=2
	movl	$term_Equal, %esi
	callq	list_DeleteDuplicates
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	list_Length
	cmpl	%r12d, %eax
	je	.LBB51_198
# BB#196:                               # %.lr.ph355.preheader
                                        #   in Loop: Header=BB51_188 Depth=2
	xorl	%ebx, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB51_197:                             # %.lr.ph355
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rdi
	callq	term_Delete
	movq	(%rbp), %r15
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	incl	%ebx
	movq	%r14, %rdi
	callq	list_Length
	subl	%r12d, %eax
	cmpl	%eax, %ebx
	movq	%r15, %rbp
	jb	.LBB51_197
.LBB51_198:                             # %._crit_edge356
                                        #   in Loop: Header=BB51_188 Depth=2
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ebx
	testq	%r15, %r15
	movl	$1, %eax
	movl	88(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movq	%r14, %rdi
	callq	list_Copy
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	list_Copy
	testq	%r13, %r13
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	je	.LBB51_203
# BB#199:                               #   in Loop: Header=BB51_188 Depth=2
	testq	%rax, %rax
	je	.LBB51_204
# BB#200:                               # %.preheader.i229.i.preheader
                                        #   in Loop: Header=BB51_188 Depth=2
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB51_201:                             # %.preheader.i229.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB51_201
# BB#202:                               #   in Loop: Header=BB51_188 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB51_204
	.p2align	4, 0x90
.LBB51_203:                             #   in Loop: Header=BB51_188 Depth=2
	movq	%rax, %r13
.LBB51_204:                             # %list_Nconc.exit231.i
                                        #   in Loop: Header=BB51_188 Depth=2
	cmpq	$0, 152(%rsp)           # 8-byte Folded Reload
	movl	vec_MAX(%rip), %ebx
	je	.LBB51_216
# BB#205:                               # %.lr.ph43.preheader.preheader.i.i
                                        #   in Loop: Header=BB51_188 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%ebx, %rcx
	movq	%rcx, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leal	1(%rcx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	152(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB51_206:                             # %.lr.ph43.preheader.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB51_207 Depth 4
                                        #           Child Loop BB51_209 Depth 5
                                        #           Child Loop BB51_213 Depth 5
	movl	64(%rsp), %edx          # 4-byte Reload
	movl	%edx, vec_MAX(%rip)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rax, vec_VECTOR(,%rcx,8)
	movq	8(%r14), %rax
	movl	(%rax), %r12d
	movl	8(%rax), %ebp
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB51_207:                             # %.lr.ph43.i.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        #       Parent Loop BB51_206 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB51_209 Depth 5
                                        #           Child Loop BB51_213 Depth 5
	leal	-1(%rax), %ecx
	movl	%ecx, vec_MAX(%rip)
	cltq
	movq	vec_VECTOR-8(,%rax,8), %r15
	cmpl	%ebp, (%r15)
	jne	.LBB51_211
# BB#208:                               #   in Loop: Header=BB51_207 Depth=4
	movl	%r12d, (%r15)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB51_210
	.p2align	4, 0x90
.LBB51_209:                             # %.lr.ph.i.i234.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        #       Parent Loop BB51_206 Depth=3
                                        #         Parent Loop BB51_207 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB51_209
.LBB51_210:                             # %list_Delete.exit.i.i227
                                        #   in Loop: Header=BB51_207 Depth=4
	movl	$term_Copy, %esi
	movq	%r13, %rdi
	callq	list_CopyWithElement
	movq	%rax, 16(%r15)
	movl	vec_MAX(%rip), %ecx
	testq	%rax, %rax
	jne	.LBB51_212
	jmp	.LBB51_214
	.p2align	4, 0x90
.LBB51_211:                             # %.lr.ph43._crit_edge.i.i
                                        #   in Loop: Header=BB51_207 Depth=4
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB51_214
.LBB51_212:                             # %.lr.ph.i236.i.preheader
                                        #   in Loop: Header=BB51_207 Depth=4
	movslq	%ecx, %rdx
	leaq	vec_VECTOR(,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB51_213:                             # %.lr.ph.i236.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        #       Parent Loop BB51_206 Depth=3
                                        #         Parent Loop BB51_207 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	8(%rax), %rsi
	incl	%ecx
	movl	%ecx, vec_MAX(%rip)
	movq	%rsi, (%rdx)
	movq	(%rax), %rax
	addq	$8, %rdx
	testq	%rax, %rax
	jne	.LBB51_213
.LBB51_214:                             # %.backedge.i.i228
                                        #   in Loop: Header=BB51_207 Depth=4
	cmpl	%ecx, %ebx
	movl	%ecx, %eax
	jne	.LBB51_207
# BB#215:                               # %._crit_edge.i.i229
                                        #   in Loop: Header=BB51_206 Depth=3
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB51_206
.LBB51_216:                             # %cnf_SkolemFunctionFormulaMapped.exit.i
                                        #   in Loop: Header=BB51_188 Depth=2
	movl	%ebx, vec_MAX(%rip)
	testq	%r13, %r13
	je	.LBB51_218
	.p2align	4, 0x90
.LBB51_217:                             # %.lr.ph.i240.i
                                        #   Parent Loop BB51_240 Depth=1
                                        #     Parent Loop BB51_188 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB51_217
.LBB51_218:                             # %list_Delete.exit242.i
                                        #   in Loop: Header=BB51_188 Depth=2
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rax
	movq	8(%rax), %r8
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movl	44(%rsp), %ecx          # 4-byte Reload
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	%r13, %r9
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi482:
	.cfi_adjust_cfa_offset 8
	pushq	248(%rsp)
.Lcfi483:
	.cfi_adjust_cfa_offset 8
	movl	248(%rsp), %eax
	pushq	%rax
.Lcfi484:
	.cfi_adjust_cfa_offset 8
	movq	248(%rsp), %r15
	pushq	%r15
.Lcfi485:
	.cfi_adjust_cfa_offset 8
	callq	cnf_OptimizedSkolemFormula
	movq	%rbp, %rcx
	addq	$32, %rsp
.Lcfi486:
	.cfi_adjust_cfa_offset -32
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB51_188
	jmp	.LBB51_220
.LBB51_219:                             #   in Loop: Header=BB51_240 Depth=1
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$0, 88(%rsp)            # 4-byte Folded Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB51_220:                             # %.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	128(%rsp), %r12         # 8-byte Reload
	testq	%r12, %r12
	je	.LBB51_222
	.p2align	4, 0x90
.LBB51_221:                             # %.lr.ph368
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12), %rdi
	callq	term_Delete
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB51_221
.LBB51_222:                             # %._crit_edge369
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	136(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	movq	%r14, %rbp
	je	.LBB51_224
	.p2align	4, 0x90
.LBB51_223:                             # %.lr.ph.i248.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB51_223
.LBB51_224:                             # %list_Delete.exit250.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	$list_PairFree, %esi
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	list_DeleteWithElement
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%rbx, %r14
	je	.LBB51_226
	.p2align	4, 0x90
.LBB51_225:                             # %.lr.ph.i254.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB51_225
.LBB51_226:                             # %list_Delete.exit256.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	$list_PairFree, %esi
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	list_DeleteWithElement
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	je	.LBB51_229
# BB#227:                               # %list_Delete.exit256.i
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	136(%rax), %eax
	testl	%eax, %eax
	movq	72(%rsp), %r13          # 8-byte Reload
	je	.LBB51_237
# BB#228:                               #   in Loop: Header=BB51_240 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB51_237
.LBB51_229:                             #   in Loop: Header=BB51_240 Depth=1
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB51_237
.LBB51_230:                             #   in Loop: Header=BB51_240 Depth=1
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	224(%rsp), %r15
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB51_232
.LBB51_231:                             #   in Loop: Header=BB51_240 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB51_232:                             #   in Loop: Header=BB51_240 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.LBB51_234
	.p2align	4, 0x90
.LBB51_233:                             # %.lr.ph.i.i171
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB51_233
.LBB51_234:                             # %cnf_MoveProvedTermToTopLevel.exit
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	movl	%eax, (%r13)
	movq	16(%rdx), %rax
	movq	%rax, 16(%r13)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdx, (%rax)
	movq	16(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 16(%r12)
	movq	88(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 132(%rax)
	je	.LBB51_236
# BB#235:                               #   in Loop: Header=BB51_240 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	term_Print
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB51_236:                             # %.critedge
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %r9
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi487:
	.cfi_adjust_cfa_offset 8
	pushq	248(%rsp)
.Lcfi488:
	.cfi_adjust_cfa_offset 8
	movl	248(%rsp), %eax
	pushq	%rax
.Lcfi489:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi490:
	.cfi_adjust_cfa_offset 8
	callq	cnf_OptimizedSkolemFormula
	addq	$32, %rsp
.Lcfi491:
	.cfi_adjust_cfa_offset -32
.LBB51_237:                             # %cnf_Skolemize.exit
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB51_239
	.p2align	4, 0x90
.LBB51_238:                             # %.lr.ph.i196
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB51_238
.LBB51_239:                             # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	(%r13), %eax
	cmpl	%eax, fol_ALL(%rip)
	sete	%cl
	movq	144(%rsp), %rsi         # 8-byte Reload
	jne	.LBB51_250
	.p2align	4, 0x90
.LBB51_240:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB51_7 Depth 2
                                        #       Child Loop BB51_13 Depth 3
                                        #       Child Loop BB51_17 Depth 3
                                        #       Child Loop BB51_20 Depth 3
                                        #         Child Loop BB51_23 Depth 4
                                        #         Child Loop BB51_26 Depth 4
                                        #       Child Loop BB51_35 Depth 3
                                        #       Child Loop BB51_43 Depth 3
                                        #       Child Loop BB51_48 Depth 3
                                        #     Child Loop BB51_94 Depth 2
                                        #     Child Loop BB51_97 Depth 2
                                        #       Child Loop BB51_98 Depth 3
                                        #         Child Loop BB51_102 Depth 4
                                        #       Child Loop BB51_106 Depth 3
                                        #         Child Loop BB51_110 Depth 4
                                        #     Child Loop BB51_117 Depth 2
                                        #       Child Loop BB51_122 Depth 3
                                        #       Child Loop BB51_124 Depth 3
                                        #       Child Loop BB51_126 Depth 3
                                        #       Child Loop BB51_129 Depth 3
                                        #     Child Loop BB51_233 Depth 2
                                        #     Child Loop BB51_59 Depth 2
                                        #     Child Loop BB51_136 Depth 2
                                        #     Child Loop BB51_139 Depth 2
                                        #       Child Loop BB51_141 Depth 3
                                        #         Child Loop BB51_142 Depth 4
                                        #       Child Loop BB51_147 Depth 3
                                        #       Child Loop BB51_150 Depth 3
                                        #         Child Loop BB51_154 Depth 4
                                        #         Child Loop BB51_157 Depth 4
                                        #     Child Loop BB51_170 Depth 2
                                        #       Child Loop BB51_171 Depth 3
                                        #     Child Loop BB51_176 Depth 2
                                        #     Child Loop BB51_179 Depth 2
                                        #     Child Loop BB51_181 Depth 2
                                        #     Child Loop BB51_184 Depth 2
                                        #     Child Loop BB51_188 Depth 2
                                        #       Child Loop BB51_191 Depth 3
                                        #       Child Loop BB51_197 Depth 3
                                        #       Child Loop BB51_201 Depth 3
                                        #       Child Loop BB51_206 Depth 3
                                        #         Child Loop BB51_207 Depth 4
                                        #           Child Loop BB51_209 Depth 5
                                        #           Child Loop BB51_213 Depth 5
                                        #       Child Loop BB51_217 Depth 3
                                        #     Child Loop BB51_221 Depth 2
                                        #     Child Loop BB51_223 Depth 2
                                        #     Child Loop BB51_225 Depth 2
                                        #     Child Loop BB51_65 Depth 2
                                        #     Child Loop BB51_70 Depth 2
                                        #     Child Loop BB51_73 Depth 2
                                        #       Child Loop BB51_74 Depth 3
                                        #         Child Loop BB51_76 Depth 4
                                        #         Child Loop BB51_80 Depth 4
                                        #     Child Loop BB51_84 Depth 2
                                        #     Child Loop BB51_87 Depth 2
                                        #     Child Loop BB51_238 Depth 2
                                        #     Child Loop BB51_244 Depth 2
                                        #     Child Loop BB51_248 Depth 2
	movq	112(%rbp), %rbx
	testb	$1, %cl
	je	.LBB51_3
# BB#241:                               #   in Loop: Header=BB51_240 Depth=1
	cmpl	$0, 216(%rbx)
	je	.LBB51_246
# BB#242:                               #   in Loop: Header=BB51_240 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB51_245
# BB#243:                               # %.lr.ph305.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	cnf_VARIABLEDEPTHARRAY(%rip), %rcx
	.p2align	4, 0x90
.LBB51_244:                             # %.lr.ph305
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rdx
	movslq	(%rdx), %rdx
	movl	%esi, (%rcx,%rdx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB51_244
.LBB51_245:                             # %._crit_edge
                                        #   in Loop: Header=BB51_240 Depth=1
	incl	%esi
.LBB51_246:                             #   in Loop: Header=BB51_240 Depth=1
	movq	%rsi, %rbx
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r13), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB51_249
# BB#247:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB51_240 Depth=1
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB51_249
	.p2align	4, 0x90
.LBB51_248:                             # %.lr.ph.i..lr.ph.i_crit_edge.i
                                        #   Parent Loop BB51_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB51_248
.LBB51_249:                             # %cnf_PopAllQuantifier.exit
                                        #   in Loop: Header=BB51_240 Depth=1
	movl	(%rax), %ecx
	movl	%ecx, (%r13)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r13)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movl	(%r13), %eax
	cmpl	%eax, fol_ALL(%rip)
	sete	%cl
	movq	%rbx, %rsi
	je	.LBB51_240
.LBB51_250:                             # %cnf_PopAllQuantifier.exit
                                        #   in Loop: Header=BB51_240 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB51_240
.LBB51_251:                             # %tailrecurse.outer._crit_edge
	cmpl	fol_AND(%rip), %eax
	movl	fol_OR(%rip), %ebx
	movq	%r14, %rdx
	movq	%rbp, %rdi
	movl	44(%rsp), %ecx          # 4-byte Reload
	movq	%rsi, %r10
	movq	%r12, %rsi
	movq	240(%rsp), %r12
	movq	%r12, %rbp
	movq	%r15, %r11
	movl	232(%rsp), %r13d
	movq	104(%rsp), %r12         # 8-byte Reload
	je	.LBB51_253
# BB#252:                               # %tailrecurse.outer._crit_edge
	cmpl	%ebx, %eax
	jne	.LBB51_256
.LBB51_253:
	xorl	%r8d, %r8d
	cmpl	%ebx, %eax
	cmovel	%r8d, %ecx
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB51_256
# BB#254:                               # %.lr.ph.preheader
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB51_255:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r8
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %r9
	pushq	%r10
.Lcfi492:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi493:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi494:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi495:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r15
	movq	%r11, %r14
	callq	cnf_OptimizedSkolemFormula
	movq	%r14, %r11
	movl	76(%rsp), %ecx          # 4-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %r10
	movq	88(%rsp), %rdx          # 8-byte Reload
	addq	$32, %rsp
.Lcfi496:
	.cfi_adjust_cfa_offset -32
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB51_255
.LBB51_256:                             # %.loopexit
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end51:
	.size	cnf_OptimizedSkolemFormula, .Lfunc_end51-cnf_OptimizedSkolemFormula
	.cfi_endproc

	.p2align	4, 0x90
	.type	cnf_GetUsedTerms,@function
cnf_GetUsedTerms:                       # @cnf_GetUsedTerms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi497:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi498:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi499:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi500:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi501:
	.cfi_def_cfa_offset 48
.Lcfi502:
	.cfi_offset %rbx, -48
.Lcfi503:
	.cfi_offset %r12, -40
.Lcfi504:
	.cfi_offset %r14, -32
.Lcfi505:
	.cfi_offset %r15, -24
.Lcfi506:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movabsq	$1908283869694091547, %rcx # imm = 0x1A7B9611A7B9611B
	movq	%rbx, %rax
	mulq	%rcx
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	imulq	$29, %rax, %rax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	(%r14,%rcx,8), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.LBB52_2
	jmp	.LBB52_4
	.p2align	4, 0x90
.LBB52_18:                              #   in Loop: Header=BB52_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB52_4
.LBB52_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	cmpq	%rbx, 8(%rcx)
	jne	.LBB52_18
# BB#3:
	movq	(%rcx), %rdi
.LBB52_4:                               # %hsh_Get.exit
	callq	list_Copy
	movl	$cnf_LabelEqual, %esi
	movq	%rax, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB52_17
# BB#5:
	movq	32(%rbx), %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	jne	.LBB52_7
	jmp	.LBB52_17
	.p2align	4, 0x90
.LBB52_16:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB52_7 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB52_17
.LBB52_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB52_14 Depth 2
	movl	8(%rbx), %ebp
	movq	40(%r15), %rsi
	movl	%ebp, %edi
	callq	clause_GetNumberedCl
	testq	%rax, %rax
	jne	.LBB52_10
# BB#8:                                 #   in Loop: Header=BB52_7 Depth=1
	movq	56(%r15), %rsi
	movl	%ebp, %edi
	callq	clause_GetNumberedCl
	testq	%rax, %rax
	jne	.LBB52_10
# BB#9:                                 #   in Loop: Header=BB52_7 Depth=1
	movq	96(%r15), %rsi
	movl	%ebp, %edi
	callq	clause_GetNumberedCl
	.p2align	4, 0x90
.LBB52_10:                              #   in Loop: Header=BB52_7 Depth=1
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	cnf_GetUsedTerms
	testq	%r12, %r12
	je	.LBB52_11
# BB#12:                                #   in Loop: Header=BB52_7 Depth=1
	testq	%rax, %rax
	je	.LBB52_16
# BB#13:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB52_7 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB52_14:                              # %.preheader.i
                                        #   Parent Loop BB52_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB52_14
# BB#15:                                #   in Loop: Header=BB52_7 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB52_16
	.p2align	4, 0x90
.LBB52_11:                              #   in Loop: Header=BB52_7 Depth=1
	movq	%rax, %r12
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB52_7
.LBB52_17:                              # %.loopexit
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end52:
	.size	cnf_GetUsedTerms, .Lfunc_end52-cnf_GetUsedTerms
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi507:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end53:
	.size	misc_Error, .Lfunc_end53-misc_Error
	.cfi_endproc

	.type	cnf_VARIABLEDEPTHARRAY,@object # @cnf_VARIABLEDEPTHARRAY
	.local	cnf_VARIABLEDEPTHARRAY
	.comm	cnf_VARIABLEDEPTHARRAY,8,8
	.type	cnf_SEARCHCOPY,@object  # @cnf_SEARCHCOPY
	.local	cnf_SEARCHCOPY
	.comm	cnf_SEARCHCOPY,8,8
	.type	cnf_HAVEPROOFPS,@object # @cnf_HAVEPROOFPS
	.local	cnf_HAVEPROOFPS
	.comm	cnf_HAVEPROOFPS,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nTarget after applying def:\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/cnf.c"
	.size	.L.str.3, 75

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n In cnf_ComputeLiteralLists: Unexpected junctor in input Formula!\n"
	.size	.L.str.4, 68

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.5, 133

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	".\n"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"."
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" | "
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"conjecture%d"
	.size	.L.str.9, 13

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nAdded label %s for conjecture"
	.size	.L.str.10, 31

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nTarget :"
	.size	.L.str.11, 10

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nConverted to :"
	.size	.L.str.12, 16

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\nToProve for this target :"
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\nPredicate : "
	.size	.L.str.14, 14

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\nAlways Applicable     : "
	.size	.L.str.15, 26

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\nClause before applying def :"
	.size	.L.str.16, 30

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\nPredicate :"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\nExpansion :"
	.size	.L.str.18, 13

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\nClauses derived by expanding definition :"
	.size	.L.str.19, 43

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\n\n"
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\n In cnf_GetFormulaPolarity: Wrong arguments !\n"
	.size	.L.str.21, 48

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\nTerm before skolemization : \n "
	.size	.L.str.22, 32

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\nTerm after skolemization : "
	.size	.L.str.23, 29

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\nHaveProof not necessary"
	.size	.L.str.24, 25

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\nIn term "
	.size	.L.str.25, 10

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\n subterm "
	.size	.L.str.26, 11

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" is moved to toplevel."
	.size	.L.str.27, 23

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Result : "
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\nStrong skolemization applied"
	.size	.L.str.29, 30

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.30, 50

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.31, 50

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\ncheck1"
	.size	.L.str.32, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
