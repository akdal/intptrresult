	.text
	.file	"timeit.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI0_1:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$208, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 256
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	(%r14), %rax
	movq	%rax, g_program_name(%rip)
	movl	$1, %ebp
	cmpl	$1, %r15d
	je	.LBB0_57
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_134
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_134
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_10
# BB#13:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_14
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_17
# BB#19:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_20
# BB#22:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_23
# BB#25:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_26
# BB#28:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_29
# BB#30:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_32
# BB#34:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.15, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB0_53
# BB#35:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#36:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movslq	%eax, %r12
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_37
# BB#38:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_39
# BB#40:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_41
# BB#42:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_43
# BB#44:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_45
# BB#46:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_47
# BB#48:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_49
# BB#50:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_52
# BB#51:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_subprocess_count_limit(%rip)
	jmp	.LBB0_55
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$1, g_posix_mode(%rip)
	jmp	.LBB0_55
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#54:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, g_timeout_in_seconds(%rip)
.LBB0_55:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jne	.LBB0_2
	jmp	.LBB0_56
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_summary_file(%rip)
	jmp	.LBB0_55
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_target_redirect_input(%rip)
	jmp	.LBB0_55
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_target_redirect_stdout(%rip)
	movq	%rax, g_target_redirect_stderr(%rip)
	jmp	.LBB0_55
.LBB0_23:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#24:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_target_redirect_stdout(%rip)
	jmp	.LBB0_55
.LBB0_26:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#27:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_target_redirect_stderr(%rip)
	jmp	.LBB0_55
.LBB0_32:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	je	.LBB0_11
# BB#33:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rax
	movq	%rax, g_target_exec_directory(%rip)
	jmp	.LBB0_55
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$1, g_append_exitstats(%rip)
	jmp	.LBB0_55
.LBB0_37:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_cpu_limit(%rip)
	jmp	.LBB0_55
.LBB0_39:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_stack_size_limit(%rip)
	jmp	.LBB0_55
.LBB0_41:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_data_size_limit(%rip)
	jmp	.LBB0_55
.LBB0_43:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_rss_size_limit(%rip)
	jmp	.LBB0_55
.LBB0_45:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_file_size_limit(%rip)
	jmp	.LBB0_55
.LBB0_47:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_core_limit(%rip)
	jmp	.LBB0_55
.LBB0_49:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r12, g_target_file_count_limit(%rip)
	jmp	.LBB0_55
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rax
	movq	(%r14,%rax,8), %rbx
	cmpb	$45, (%rbx)
	je	.LBB0_3
.LBB0_57:                               # %._crit_edge
	cmpl	%r15d, %ebp
	je	.LBB0_56
# BB#58:
	movslq	%ebp, %rbp
	movq	(%r14,%rbp,8), %rax
	movq	%rax, g_target_program(%rip)
	movl	$2, %edi
	movl	$terminate_handler, %esi
	callq	signal
	movl	$15, %edi
	movl	$terminate_handler, %esi
	callq	signal
	movl	$14, %edi
	movl	$timeout_handler, %esi
	callq	signal
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	cvtsi2sdq	64(%rsp), %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	72(%rsp), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	fork
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB0_59
# BB#60:
	je	.LBB0_61
# BB#108:
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	.LCPI0_0(%rip), %xmm1
	movl	%ebx, g_monitored_pid(%rip)
	cmpl	$0, g_timeout_in_seconds(%rip)
	je	.LBB0_110
# BB#109:
	leaq	64(%rsp), %rbp
	movq	%rbp, %rdi
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	sigemptyset
	movl	$14, %esi
	movq	%rbp, %rdi
	callq	sigaddset
	movl	g_timeout_in_seconds(%rip), %edi
	callq	alarm
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB0_110:                              # %.preheader.i.i.preheader
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movq	%rsp, %rbp
	.p2align	4, 0x90
.LBB0_111:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	waitpid
	testl	%eax, %eax
	jns	.LBB0_114
# BB#112:                               #   in Loop: Header=BB0_111 Depth=1
	callq	__errno_location
	cmpl	$4, (%rax)
	je	.LBB0_111
# BB#113:                               # %.critedge.i8.i
	movl	$.L.str.83, %edi
.LBB0_132:                              # %monitor_child_process.exit.i
	callq	perror
	movl	$66, %ebx
.LBB0_133:                              # %execute.exit
	movl	%ebx, %eax
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_114:                              # %.critedge51.i.i
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	xorps	%xmm0, %xmm0
	cvtsi2sdq	64(%rsp), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	72(%rsp), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	negl	%ebx
	movl	$9, %esi
	movl	%ebx, %edi
	callq	kill
	leaq	64(%rsp), %rsi
	movl	$-1, %edi
	callq	getrusage
	testl	%eax, %eax
	js	.LBB0_115
# BB#116:
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_0(%rip), %xmm0
	movsd	16(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	addsd	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	cvtsi2sdq	64(%rsp), %xmm1
	cvtsi2sdq	72(%rsp), %xmm3
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm3
	cvtsi2sdq	80(%rsp), %xmm2
	cvtsi2sdq	88(%rsp), %xmm4
	divsd	%xmm0, %xmm4
	movl	(%rsp), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, %edx
	shll	$24, %edx
	addl	$16777216, %edx         # imm = 0x1000000
	sarl	$25, %edx
	testl	%edx, %edx
	movapd	%xmm5, %xmm0
	jg	.LBB0_117
# BB#118:
	movl	$66, %ebx
	testl	%ecx, %ecx
	jne	.LBB0_120
# BB#119:
	movzbl	%ah, %ebx  # NOREX
.LBB0_120:
	subsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm3, %xmm1
	addsd	%xmm4, %xmm2
	movq	g_summary_file(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_121
# BB#125:
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movl	$.L.str.71, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_131
# BB#126:                               # %.critedge52.i.i
	movl	$.L.str.88, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movl	$.L.str.89, %esi
	movl	$.L.str.90, %edx
	movb	$1, %al
	movq	%rbp, %rdi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fprintf
	movl	$.L.str.89, %esi
	movl	$.L.str.91, %edx
	movb	$1, %al
	movq	%rbp, %rdi
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fprintf
	movl	$.L.str.89, %esi
	movl	$.L.str.92, %edx
	movb	$1, %al
	movq	%rbp, %rdi
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fprintf
	movq	%rbp, %rdi
	callq	fclose
	cmpb	$1, g_append_exitstats(%rip)
	jne	.LBB0_133
.LBB0_128:
	movq	g_target_program(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_133
# BB#129:
	movq	g_target_redirect_stdout(%rip), %rdi
	movl	$.L.str.93, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_131
# BB#130:                               # %.thread.i.i
	movl	$.L.str.88, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	%rbp, %rdi
	callq	fclose
	xorl	%ebx, %ebx
	jmp	.LBB0_133
.LBB0_59:
	movl	$.L.str.64, %edi
	jmp	.LBB0_132
.LBB0_61:
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	setpgid
	movq	g_target_redirect_input(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_66
# BB#62:
	movl	$.L.str.68, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_131
# BB#63:
	movq	%rbx, %rdi
	callq	fileno
	xorl	%esi, %esi
	movl	%eax, %edi
	callq	dup2
	testl	%eax, %eax
	js	.LBB0_64
# BB#65:
	movq	%rbx, %rdi
	callq	fclose
.LBB0_66:
	movq	g_target_redirect_stdout(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_68
# BB#67:
	xorl	%r12d, %r12d
.LBB0_70:
	movq	g_target_redirect_stderr(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_71
.LBB0_78:
	testq	%r12, %r12
	je	.LBB0_80
.LBB0_79:
	movq	%r12, %rdi
	callq	fclose
.LBB0_80:
	movq	g_target_cpu_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_83
# BB#81:
	leaq	64(%rsp), %rsi
	xorl	%edi, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	xorl	%edi, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_83
# BB#82:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.72, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_83:
	movq	g_target_stack_size_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_86
# BB#84:
	leaq	64(%rsp), %rsi
	movl	$3, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$3, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_86
# BB#85:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.73, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_86:
	movq	g_target_data_size_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_89
# BB#87:
	leaq	64(%rsp), %rsi
	movl	$2, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$2, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_89
# BB#88:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.74, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_89:
	movq	g_target_rss_size_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_92
# BB#90:
	leaq	64(%rsp), %rsi
	movl	$5, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$5, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_92
# BB#91:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.75, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_92:
	movq	g_target_file_size_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_95
# BB#93:
	leaq	64(%rsp), %rsi
	movl	$1, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$1, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_95
# BB#94:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.76, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_95:
	movq	g_target_core_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_98
# BB#96:
	leaq	64(%rsp), %rsi
	movl	$4, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$4, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_98
# BB#97:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.77, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_98:
	movq	g_target_file_count_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_101
# BB#99:
	leaq	64(%rsp), %rsi
	movl	$7, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$7, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_101
# BB#100:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.78, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_101:
	movq	g_target_subprocess_count_limit(%rip), %rbx
	cmpq	$-1, %rbx
	je	.LBB0_104
# BB#102:
	leaq	64(%rsp), %rsi
	movl	$6, %edi
	callq	getrlimit
	movq	72(%rsp), %rax
	cmpq	%rbx, %rax
	cmovaq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rsp, %rsi
	movl	$6, %edi
	callq	setrlimit
	testl	%eax, %eax
	jns	.LBB0_104
# BB#103:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	movl	$.L.str.82, %esi
	movl	$.L.str.79, %ecx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_104:
	movq	g_target_exec_directory(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_107
# BB#105:
	callq	chdir
	testl	%eax, %eax
	jns	.LBB0_107
# BB#106:
	movl	$.L.str.80, %edi
	jmp	.LBB0_132
.LBB0_115:
	movl	$.L.str.84, %edi
	jmp	.LBB0_132
.LBB0_117:
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	%xmm3, 56(%rsp)         # 8-byte Spill
	movsd	%xmm4, 48(%rsp)         # 8-byte Spill
	callq	fprintf
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	56(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	(%rsp), %ebx
	andl	$127, %ebx
	orl	$128, %ebx
	jmp	.LBB0_120
.LBB0_121:
	movq	stderr(%rip), %rdi
	cmpb	$1, g_posix_mode(%rip)
	jne	.LBB0_124
# BB#122:
	movl	$.L.str.86, %esi
	jmp	.LBB0_123
.LBB0_68:
	movl	$.L.str.71, %esi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_131
# BB#69:
	movq	%r12, %rdi
	callq	fileno
	movl	$1, %esi
	movl	%eax, %edi
	callq	dup2
	testl	%eax, %eax
	jns	.LBB0_70
	jmp	.LBB0_64
.LBB0_107:
	leaq	(%r14,%rbp,8), %rsi
	movq	(%rsi), %rdi
	callq	execvp
	movl	$.L.str.81, %edi
	callq	perror
	callq	__errno_location
	movl	(%rax), %eax
	cmpl	$13, %eax
	movl	$126, %ecx
	movl	$67, %edx
	cmovel	%ecx, %edx
	cmpl	$2, %eax
	movl	$127, %ebx
	cmovnel	%edx, %ebx
	jmp	.LBB0_133
.LBB0_71:
	movq	g_target_redirect_stdout(%rip), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_73
# BB#72:
	movq	%r12, %rdi
	callq	fileno
	xorl	%r15d, %r15d
	jmp	.LBB0_75
.LBB0_124:
	movl	$.L.str.87, %esi
.LBB0_123:
	movb	$3, %al
	callq	fprintf
	cmpb	$1, g_append_exitstats(%rip)
	je	.LBB0_128
	jmp	.LBB0_133
.LBB0_73:
	movl	$.L.str.71, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_74
.LBB0_131:
	movl	$.L.str.69, %edi
	jmp	.LBB0_132
.LBB0_74:
	movq	%r15, %rdi
	callq	fileno
.LBB0_75:
	movl	$2, %esi
	movl	%eax, %edi
	callq	dup2
	testl	%eax, %eax
	jns	.LBB0_76
.LBB0_64:
	movl	$.L.str.70, %edi
	jmp	.LBB0_132
.LBB0_76:
	testq	%r15, %r15
	je	.LBB0_78
# BB#77:
	movq	%r15, %rdi
	callq	fclose
	testq	%r12, %r12
	jne	.LBB0_79
	jmp	.LBB0_80
.LBB0_134:
	xorl	%edi, %edi
	callq	usage
.LBB0_56:                               # %._crit_edge.thread
	movq	stderr(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	usage
.LBB0_11:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
.LBB0_12:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	usage
.LBB0_53:
	movq	stderr(%rip), %rdi
	movl	$.L.str.25, %esi
	jmp	.LBB0_12
.LBB0_52:
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
	jmp	.LBB0_12
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.30, %edx
	movl	$.L.str.31, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.32, %edx
	movl	$.L.str.33, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.34, %edx
	movl	$.L.str.35, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.36, %edx
	movl	$.L.str.37, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.38, %edx
	movl	$.L.str.39, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.40, %edx
	movl	$.L.str.41, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.42, %edx
	movl	$.L.str.43, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.44, %edx
	movl	$.L.str.45, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.46, %edx
	movl	$.L.str.47, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.48, %edx
	movl	$.L.str.49, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.50, %edx
	movl	$.L.str.51, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.52, %edx
	movl	$.L.str.53, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.54, %edx
	movl	$.L.str.55, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.56, %edx
	movl	$.L.str.57, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.58, %edx
	movl	$.L.str.59, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.60, %edx
	movl	$.L.str.61, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.29, %esi
	movl	$.L.str.62, %edx
	movl	$.L.str.63, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	%ebx, %edi
	callq	_exit
.Lfunc_end1:
	.size	usage, .Lfunc_end1-usage
	.cfi_endproc

	.p2align	4, 0x90
	.type	terminate_handler,@function
terminate_handler:                      # @terminate_handler
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	cmpl	$0, g_monitored_pid(%rip)
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	je	.LBB2_2
# BB#1:
	movq	g_target_program(%rip), %r8
	xorl	%ebx, %ebx
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	callq	fprintf
	subl	g_monitored_pid(%rip), %ebx
	movl	$9, %esi
	movl	%ebx, %edi
	popq	%rbx
	jmp	kill                    # TAILCALL
.LBB2_2:
	movl	$.L.str.66, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$68, %edi
	callq	_exit
.Lfunc_end2:
	.size	terminate_handler, .Lfunc_end2-terminate_handler
	.cfi_endproc

	.p2align	4, 0x90
	.type	timeout_handler,@function
timeout_handler:                        # @timeout_handler
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	stderr(%rip), %rdi
	movq	g_program_name(%rip), %rdx
	movq	g_target_program(%rip), %rcx
	xorl	%ebx, %ebx
	movl	$.L.str.67, %esi
	xorl	%eax, %eax
	callq	fprintf
	subl	g_monitored_pid(%rip), %ebx
	movl	$9, %esi
	movl	%ebx, %edi
	popq	%rbx
	jmp	kill                    # TAILCALL
.Lfunc_end3:
	.size	timeout_handler, .Lfunc_end3-timeout_handler
	.cfi_endproc

	.type	g_program_name,@object  # @g_program_name
	.local	g_program_name
	.comm	g_program_name,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-h"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"--help"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"-p"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"--posix"
	.size	.L.str.3, 8

	.type	g_posix_mode,@object    # @g_posix_mode
	.local	g_posix_mode
	.comm	g_posix_mode,1,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-t"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"--timeout"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"error: %s argument requires an option\n"
	.size	.L.str.6, 39

	.type	g_timeout_in_seconds,@object # @g_timeout_in_seconds
	.local	g_timeout_in_seconds
	.comm	g_timeout_in_seconds,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"--summary"
	.size	.L.str.7, 10

	.type	g_summary_file,@object  # @g_summary_file
	.local	g_summary_file
	.comm	g_summary_file,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"--redirect-input"
	.size	.L.str.8, 17

	.type	g_target_redirect_input,@object # @g_target_redirect_input
	.local	g_target_redirect_input
	.comm	g_target_redirect_input,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"--redirect-output"
	.size	.L.str.9, 18

	.type	g_target_redirect_stdout,@object # @g_target_redirect_stdout
	.local	g_target_redirect_stdout
	.comm	g_target_redirect_stdout,8,8
	.type	g_target_redirect_stderr,@object # @g_target_redirect_stderr
	.local	g_target_redirect_stderr
	.comm	g_target_redirect_stderr,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"--redirect-stdout"
	.size	.L.str.10, 18

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"--redirect-stderr"
	.size	.L.str.11, 18

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"--append-exitstatus"
	.size	.L.str.12, 20

	.type	g_append_exitstats,@object # @g_append_exitstats
	.local	g_append_exitstats
	.comm	g_append_exitstats,1,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"-c"
	.size	.L.str.13, 3

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"--chdir"
	.size	.L.str.14, 8

	.type	g_target_exec_directory,@object # @g_target_exec_directory
	.local	g_target_exec_directory
	.comm	g_target_exec_directory,8,8
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"--limit-"
	.size	.L.str.15, 9

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"--limit-cpu"
	.size	.L.str.16, 12

	.type	g_target_cpu_limit,@object # @g_target_cpu_limit
	.data
	.p2align	3
g_target_cpu_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_cpu_limit, 8

	.type	.L.str.17,@object       # @.str.17
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.17:
	.asciz	"--limit-stack-size"
	.size	.L.str.17, 19

	.type	g_target_stack_size_limit,@object # @g_target_stack_size_limit
	.data
	.p2align	3
g_target_stack_size_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_stack_size_limit, 8

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"--limit-data-size"
	.size	.L.str.18, 18

	.type	g_target_data_size_limit,@object # @g_target_data_size_limit
	.data
	.p2align	3
g_target_data_size_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_data_size_limit, 8

	.type	.L.str.19,@object       # @.str.19
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.19:
	.asciz	"--limit-rss-size"
	.size	.L.str.19, 17

	.type	g_target_rss_size_limit,@object # @g_target_rss_size_limit
	.data
	.p2align	3
g_target_rss_size_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_rss_size_limit, 8

	.type	.L.str.20,@object       # @.str.20
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.20:
	.asciz	"--limit-file-size"
	.size	.L.str.20, 18

	.type	g_target_file_size_limit,@object # @g_target_file_size_limit
	.data
	.p2align	3
g_target_file_size_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_file_size_limit, 8

	.type	.L.str.21,@object       # @.str.21
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.21:
	.asciz	"--limit-core"
	.size	.L.str.21, 13

	.type	g_target_core_limit,@object # @g_target_core_limit
	.data
	.p2align	3
g_target_core_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_core_limit, 8

	.type	.L.str.22,@object       # @.str.22
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.22:
	.asciz	"--limit-file-count"
	.size	.L.str.22, 19

	.type	g_target_file_count_limit,@object # @g_target_file_count_limit
	.data
	.p2align	3
g_target_file_count_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_file_count_limit, 8

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.23:
	.asciz	"--limit-subprocess-count"
	.size	.L.str.23, 25

	.type	g_target_subprocess_count_limit,@object # @g_target_subprocess_count_limit
	.data
	.p2align	3
g_target_subprocess_count_limit:
	.quad	-1                      # 0xffffffffffffffff
	.size	g_target_subprocess_count_limit, 8

	.type	.L.str.24,@object       # @.str.24
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.24:
	.asciz	"error: invalid limit argument '%s'\n"
	.size	.L.str.24, 36

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"error: invalid argument '%s'\n"
	.size	.L.str.25, 30

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"error: no command (or arguments) was given\n"
	.size	.L.str.26, 44

	.type	g_target_program,@object # @g_target_program
	.local	g_target_program
	.comm	g_target_program,8,8
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"usage: %s [options] command ... arguments ...\n"
	.size	.L.str.27, 47

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Options:\n"
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"  %-20s %s"
	.size	.L.str.29, 11

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"-h, --help"
	.size	.L.str.30, 11

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Show this help text.\n"
	.size	.L.str.31, 22

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"-p, --posix"
	.size	.L.str.32, 12

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Report time in /usr/bin/time POSIX format.\n"
	.size	.L.str.33, 44

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"-t, --timeout <N>"
	.size	.L.str.34, 18

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Execute the subprocess with a timeout of N seconds.\n"
	.size	.L.str.35, 53

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"-c, --chdir <PATH>"
	.size	.L.str.36, 19

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Execute the subprocess in the given working directory.\n"
	.size	.L.str.37, 56

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"--summary <PATH>"
	.size	.L.str.38, 17

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Write monitored process summary (exit code and time) to PATH.\n"
	.size	.L.str.39, 63

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"--redirect-output <PATH>"
	.size	.L.str.40, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\n                       Redirect stdout and stderr for the target to PATH.\n"
	.size	.L.str.41, 76

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"--redirect-stdout <PATH>"
	.size	.L.str.42, 25

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"\n                       Redirect stdout for the target to PATH.\n"
	.size	.L.str.43, 65

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"--redirect-stderr <PATH>"
	.size	.L.str.44, 25

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"\n                       Redirect stderr for the target to PATH.\n"
	.size	.L.str.45, 65

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"--redirect-input <PATH>"
	.size	.L.str.46, 24

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"\n                       Redirect stdin for the target to PATH.\n"
	.size	.L.str.47, 64

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"--limit-cpu <N>"
	.size	.L.str.48, 16

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"\n                       Limit the target to N seconds of CPU time.\n"
	.size	.L.str.49, 68

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"--limit-stack-size <N>"
	.size	.L.str.50, 23

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"\n                       Limit the target to N bytes of stack space.\n"
	.size	.L.str.51, 69

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"--limit-data-size <N>"
	.size	.L.str.52, 22

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"\n                       Limit the target to N bytes of data.\n"
	.size	.L.str.53, 62

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"--limit-rss-size <N>"
	.size	.L.str.54, 21

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"\n                       Limit the target to N bytes of resident memory.\n"
	.size	.L.str.55, 73

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"--limit-file-size <N>"
	.size	.L.str.56, 22

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"\n                       Limit the target to creating files no more than N bytes.\n"
	.size	.L.str.57, 82

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"--limit-core <N>"
	.size	.L.str.58, 17

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"\n                       Limit the size for which core files will be generated.\n"
	.size	.L.str.59, 80

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"--limit-file-count <N>"
	.size	.L.str.60, 23

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"\n                       Limit the maximum number of open files the target can have.\n"
	.size	.L.str.61, 85

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"--limit-subprocess-count <N>"
	.size	.L.str.62, 29

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"\n                       Limit the maximum number of simultaneous processes the target can use.\n"
	.size	.L.str.63, 96

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"fork"
	.size	.L.str.64, 5

	.type	g_monitored_pid,@object # @g_monitored_pid
	.local	g_monitored_pid
	.comm	g_monitored_pid,4,4
	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%s: error: received signal %d. killing monitored process(es): %s\n"
	.size	.L.str.65, 66

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"%s: error: received signal %d. exiting.\n"
	.size	.L.str.66, 41

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"%s: TIMING OUT PROCESS: %s\n"
	.size	.L.str.67, 28

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"r"
	.size	.L.str.68, 2

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"fopen"
	.size	.L.str.69, 6

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"dup2"
	.size	.L.str.70, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"w"
	.size	.L.str.71, 2

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"RLIMIT_CPU"
	.size	.L.str.72, 11

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"RLIMIT_STACK"
	.size	.L.str.73, 13

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"RLIMIT_DATA"
	.size	.L.str.74, 12

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"RLIMIT_RSS"
	.size	.L.str.75, 11

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"RLIMIT_FSIZE"
	.size	.L.str.76, 13

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"RLIMIT_CORE"
	.size	.L.str.77, 12

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"RLIMIT_NOFILE"
	.size	.L.str.78, 14

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"RLIMIT_NPROC"
	.size	.L.str.79, 13

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"chdir"
	.size	.L.str.80, 6

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"execv"
	.size	.L.str.81, 6

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"%s: warning: unable to set limit for %s (to {%lu, %lu})\n"
	.size	.L.str.82, 57

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"waitpid"
	.size	.L.str.83, 8

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"getrusage"
	.size	.L.str.84, 10

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"%s: error: child terminated by signal %d\n"
	.size	.L.str.85, 42

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"real %12.4f\nuser %12.4f\nsys  %12.4f\n"
	.size	.L.str.86, 37

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"%12.4f real %12.4f user %12.4f sys\n"
	.size	.L.str.87, 36

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"exit %d\n"
	.size	.L.str.88, 9

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"%-10s %.4f\n"
	.size	.L.str.89, 12

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"real"
	.size	.L.str.90, 5

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"user"
	.size	.L.str.91, 5

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"sys"
	.size	.L.str.92, 4

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"a"
	.size	.L.str.93, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
