	.text
	.file	"except.bc"
	.globl	_Z6blowupm
	.p2align	4, 0x90
	.type	_Z6blowupm,@function
_Z6blowupm:                             # @_Z6blowupm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	__cxa_allocate_exception
	testb	$1, %bl
	movq	%rbx, (%rax)
	jne	.LBB0_1
# BB#3:
	movl	$_ZTI12Hi_exception, %esi
	jmp	.LBB0_2
.LBB0_1:
	movl	$_ZTI12Lo_exception, %esi
.LBB0_2:
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_Z6blowupm, .Lfunc_end0-_Z6blowupm
	.cfi_endproc

	.globl	_Z11lo_functionm
	.p2align	4, 0x90
	.type	_Z11lo_functionm,@function
_Z11lo_functionm:                       # @_Z11lo_functionm
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	__cxa_allocate_exception
	testb	$1, %bl
	movq	%rbx, (%rax)
	jne	.LBB1_1
# BB#3:
.Ltmp2:
	movl	$_ZTI12Hi_exception, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp3:
# BB#4:                                 # %.noexc5
.LBB1_1:
.Ltmp0:
	movl	$_ZTI12Lo_exception, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp1:
# BB#2:                                 # %.noexc
.LBB1_5:
.Ltmp4:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	incq	LO(%rip)
	popq	%rbx
	jmp	__cxa_end_catch         # TAILCALL
.Lfunc_end1:
	.size	_Z11lo_functionm, .Lfunc_end1-_Z11lo_functionm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp2-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp2           #   Call between .Ltmp2 and .Ltmp1
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	1                       #   On action: 1
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	_ZTI12Lo_exception      # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z11hi_functionm
	.p2align	4, 0x90
	.type	_Z11hi_functionm,@function
_Z11hi_functionm:                       # @_Z11hi_functionm
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	__cxa_allocate_exception
	testb	$1, %bl
	movq	%rbx, (%rax)
	jne	.LBB2_1
# BB#3:
.Ltmp7:
	movl	$_ZTI12Hi_exception, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp8:
# BB#4:                                 # %.noexc5.i
.LBB2_1:
.Ltmp5:
	movl	$_ZTI12Lo_exception, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp6:
# BB#2:                                 # %.noexc.i
.LBB2_5:
.Ltmp9:
	cmpl	$2, %edx
	jne	.LBB2_9
# BB#6:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	incq	LO(%rip)
.Ltmp10:
	callq	__cxa_end_catch
.Ltmp11:
# BB#7:                                 # %_Z11lo_functionm.exit
	popq	%rbx
	retq
.LBB2_8:
.Ltmp12:
.LBB2_9:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	incq	HI(%rip)
	popq	%rbx
	jmp	__cxa_end_catch         # TAILCALL
.Lfunc_end2:
	.size	_Z11hi_functionm, .Lfunc_end2-_Z11hi_functionm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\317\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp7-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp7           #   Call between .Ltmp7 and .Ltmp6
	.long	.Ltmp9-.Lfunc_begin1    #     jumps to .Ltmp9
	.byte	3                       #   On action: 2
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp6          #   Call between .Ltmp6 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI12Lo_exception      # TypeInfo 2
	.long	_ZTI12Hi_exception      # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z13some_functionm
	.p2align	4, 0x90
	.type	_Z13some_functionm,@function
_Z13some_functionm:                     # @_Z13some_functionm
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
.Ltmp13:
	callq	_Z11hi_functionm
.Ltmp14:
# BB#1:
	popq	%rbx
	retq
.LBB3_2:
.Ltmp15:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
.Ltmp16:
	movl	$_ZSt4cerr, %edi
	movl	$.L.str, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.Ltmp17:
# BB#3:
	movl	$1, %edi
	callq	exit
.LBB3_4:
.Ltmp18:
	movq	%rax, %rbx
.Ltmp19:
	callq	__cxa_end_catch
.Ltmp20:
# BB#5:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_6:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_Z13some_functionm, .Lfunc_end3-_Z13some_functionm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin2   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp14         #   Call between .Ltmp14 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	cmpl	$2, %edi
	jne	.LBB5_1
# BB#2:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	cltq
	movl	$1, %ebx
	cmovgq	%rax, %rbx
	jmp	.LBB5_3
.LBB5_1:
	movl	$100000, %ebx           # imm = 0x186A0
.LBB5_3:                                # %_Z13some_functionm.exit.preheader
	decq	%rbx
	.p2align	4, 0x90
.LBB5_4:                                # %_Z13some_functionm.exit
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$-1, %rbx
	je	.LBB5_11
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
.Ltmp22:
	movq	%rbx, %rdi
	decq	%rbx
	callq	_Z11hi_functionm
.Ltmp23:
	jmp	.LBB5_4
.LBB5_11:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1, %esi
	movl	$15, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	HI(%rip), %rsi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %rbx
	movl	$.L.str.2, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.3, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	LO(%rip), %rsi
	movq	%rbx, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB5_16
# BB#12:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB5_14
# BB#13:
	movb	67(%rbx), %al
	jmp	.LBB5_15
.LBB5_14:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB5_15:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_16:
	callq	_ZSt16__throw_bad_castv
.LBB5_6:
.Ltmp24:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
.Ltmp25:
	movl	$_ZSt4cerr, %edi
	movl	$.L.str, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.Ltmp26:
# BB#7:
	movl	$1, %edi
	callq	exit
.LBB5_8:
.Ltmp27:
	movq	%rax, %rbx
.Ltmp28:
	callq	__cxa_end_catch
.Ltmp29:
# BB#9:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_10:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp23         #   Call between .Ltmp23 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin3   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_except.ii,@function
_GLOBAL__sub_I_except.ii:               # @_GLOBAL__sub_I_except.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end6:
	.size	_GLOBAL__sub_I_except.ii, .Lfunc_end6-_GLOBAL__sub_I_except.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	HI,@object              # @HI
	.bss
	.globl	HI
	.p2align	3
HI:
	.quad	0                       # 0x0
	.size	HI, 8

	.type	LO,@object              # @LO
	.globl	LO
	.p2align	3
LO:
	.quad	0                       # 0x0
	.size	LO, 8

	.type	_ZTS12Lo_exception,@object # @_ZTS12Lo_exception
	.section	.rodata._ZTS12Lo_exception,"aG",@progbits,_ZTS12Lo_exception,comdat
	.weak	_ZTS12Lo_exception
_ZTS12Lo_exception:
	.asciz	"12Lo_exception"
	.size	_ZTS12Lo_exception, 15

	.type	_ZTI12Lo_exception,@object # @_ZTI12Lo_exception
	.section	.rodata._ZTI12Lo_exception,"aG",@progbits,_ZTI12Lo_exception,comdat
	.weak	_ZTI12Lo_exception
	.p2align	3
_ZTI12Lo_exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS12Lo_exception
	.size	_ZTI12Lo_exception, 16

	.type	_ZTS12Hi_exception,@object # @_ZTS12Hi_exception
	.section	.rodata._ZTS12Hi_exception,"aG",@progbits,_ZTS12Hi_exception,comdat
	.weak	_ZTS12Hi_exception
_ZTS12Hi_exception:
	.asciz	"12Hi_exception"
	.size	_ZTS12Hi_exception, 15

	.type	_ZTI12Hi_exception,@object # @_ZTI12Hi_exception
	.section	.rodata._ZTI12Hi_exception,"aG",@progbits,_ZTI12Hi_exception,comdat
	.weak	_ZTI12Hi_exception
	.p2align	3
_ZTI12Hi_exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS12Hi_exception
	.size	_ZTI12Hi_exception, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"We shouldn't get here\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Exceptions: HI="
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" / "
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LO="
	.size	.L.str.3, 4

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_except.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
