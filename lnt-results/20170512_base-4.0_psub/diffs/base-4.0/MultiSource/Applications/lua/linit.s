	.text
	.file	"linit.bc"
	.globl	luaL_openlibs
	.p2align	4, 0x90
	.type	luaL_openlibs,@function
luaL_openlibs:                          # @luaL_openlibs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$luaopen_base, %esi
	xorl	%edx, %edx
	callq	lua_pushcclosure
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_package, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_table, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_io, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_os, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_string, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_math, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$luaopen_debug, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	lua_call                # TAILCALL
.Lfunc_end0:
	.size	luaL_openlibs, .Lfunc_end0-luaL_openlibs
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.zero	1
	.size	.L.str, 1

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"package"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"table"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"io"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"os"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"string"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"math"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"debug"
	.size	.L.str.7, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
