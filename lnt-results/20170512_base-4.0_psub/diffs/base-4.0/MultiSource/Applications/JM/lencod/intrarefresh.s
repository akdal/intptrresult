	.text
	.file	"intrarefresh.bc"
	.globl	RandomIntraInit
	.p2align	4, 0x90
	.type	RandomIntraInit,@function
RandomIntraInit:                        # @RandomIntraInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	$1, %edi
	callq	srand
	imull	%ebx, %ebp
	movl	%ebp, NumberOfMBs(%rip)
	movl	%r14d, NumberIntraPerPicture(%rip)
	testl	%r14d, %r14d
	je	.LBB0_9
# BB#1:
	movslq	%ebp, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, RefreshPattern(%rip)
	testq	%rax, %rax
	jne	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_3:
	movslq	%r14d, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, IntraMBs(%rip)
	testq	%rax, %rax
	jne	.LBB0_5
# BB#4:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_5:                                # %.preheader17
	movl	NumberOfMBs(%rip), %eax
	testl	%eax, %eax
	jle	.LBB0_10
# BB#6:                                 # %.preheader.preheader
	movq	RefreshPattern(%rip), %rdi
	decl	%eax
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	callq	rand
	movl	NumberOfMBs(%rip), %ecx
	cltd
	idivl	%ecx
	movq	RefreshPattern(%rip), %rax
	movslq	%edx, %rdx
	cmpl	$-1, (%rax,%rdx,4)
	jne	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movl	%ebx, (%rax,%rdx,4)
	incl	%ebx
	cmpl	%ecx, %ebx
	jl	.LBB0_7
	jmp	.LBB0_10
.LBB0_9:
	movq	$0, RefreshPattern(%rip)
	movq	$0, IntraMBs(%rip)
.LBB0_10:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	RandomIntraInit, .Lfunc_end0-RandomIntraInit
	.cfi_endproc

	.globl	RandomIntra
	.p2align	4, 0x90
	.type	RandomIntra,@function
RandomIntra:                            # @RandomIntra
	.cfi_startproc
# BB#0:
	movslq	NumberIntraPerPicture(%rip), %rax
	testq	%rax, %rax
	jle	.LBB1_1
# BB#4:                                 # %.lr.ph
	movq	IntraMBs(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	cmpl	%edi, (%rcx,%rdx,4)
	je	.LBB1_6
# BB#2:                                 #   in Loop: Header=BB1_5 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_5
# BB#3:
	xorl	%eax, %eax
	retq
.LBB1_1:
	xorl	%eax, %eax
	retq
.LBB1_6:
	movl	$1, %eax
	retq
.Lfunc_end1:
	.size	RandomIntra, .Lfunc_end1-RandomIntra
	.cfi_endproc

	.globl	RandomIntraNewPicture
	.p2align	4, 0x90
	.type	RandomIntraNewPicture,@function
RandomIntraNewPicture:                  # @RandomIntraNewPicture
	.cfi_startproc
# BB#0:
	movslq	NumberIntraPerPicture(%rip), %r8
	movl	WalkAround(%rip), %ecx
	addl	%r8d, %ecx
	testq	%r8, %r8
	movl	%ecx, WalkAround(%rip)
	jle	.LBB2_6
# BB#1:                                 # %.lr.ph
	movq	RefreshPattern(%rip), %r9
	movl	NumberOfMBs(%rip), %r10d
	movq	IntraMBs(%rip), %rdi
	testb	$1, %r8b
	jne	.LBB2_3
# BB#2:
	xorl	%esi, %esi
	cmpl	$1, %r8d
	jne	.LBB2_5
	jmp	.LBB2_6
.LBB2_3:
	movl	%ecx, %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%r9,%rax,4), %eax
	movl	%eax, (%rdi)
	incl	%ecx
	movl	$1, %esi
	cmpl	$1, %r8d
	je	.LBB2_6
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%r9,%rax,4), %eax
	movl	%eax, (%rdi,%rsi,4)
	leal	1(%rcx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%r9,%rax,4), %eax
	movl	%eax, 4(%rdi,%rsi,4)
	addq	$2, %rsi
	addl	$2, %ecx
	cmpq	%r8, %rsi
	jl	.LBB2_5
.LBB2_6:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	RandomIntraNewPicture, .Lfunc_end2-RandomIntraNewPicture
	.cfi_endproc

	.globl	RandomIntraUninit
	.p2align	4, 0x90
	.type	RandomIntraUninit,@function
RandomIntraUninit:                      # @RandomIntraUninit
	.cfi_startproc
# BB#0:
	cmpl	$0, NumberIntraPerPicture(%rip)
	jle	.LBB3_1
# BB#2:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movq	RefreshPattern(%rip), %rdi
	callq	free
	movq	IntraMBs(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	RandomIntraUninit, .Lfunc_end3-RandomIntraUninit
	.cfi_endproc

	.type	NumberOfMBs,@object     # @NumberOfMBs
	.local	NumberOfMBs
	.comm	NumberOfMBs,4,4
	.type	NumberIntraPerPicture,@object # @NumberIntraPerPicture
	.local	NumberIntraPerPicture
	.comm	NumberIntraPerPicture,4,4
	.type	RefreshPattern,@object  # @RefreshPattern
	.local	RefreshPattern
	.comm	RefreshPattern,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"RandomIntraInit: RefreshPattern"
	.size	.L.str, 32

	.type	IntraMBs,@object        # @IntraMBs
	.local	IntraMBs
	.comm	IntraMBs,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"RandomIntraInit: IntraMBs"
	.size	.L.str.1, 26

	.type	WalkAround,@object      # @WalkAround
	.local	WalkAround
	.comm	WalkAround,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
