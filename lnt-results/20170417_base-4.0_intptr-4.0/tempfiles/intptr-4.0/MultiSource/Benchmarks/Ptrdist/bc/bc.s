	.text
	.file	"bc.bc"
	.globl	yyparse
	.p2align	4, 0x90
	.type	yyparse,@function
yyparse:                                # @yyparse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi6:
	.cfi_def_cfa_offset 384
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$-1, yychar(%rip)
	movl	$0, yynerrs(%rip)
	movw	$0, yyerrflag(%rip)
	movl	$yyv-24, %r12d
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB0_25
.LBB0_145:                              #   in Loop: Header=BB0_25 Depth=1
	movl	next_label(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	-12(%rbx), %edx
	movl	$genstr, %edi
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	sprintf
.LBB0_146:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	callq	generate
	movl	36(%rbx), %eax
	orl	-36(%rbx), %eax
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_147:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %eax
	movl	%eax, yyval+12(%rip)
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	warn
	movl	$.L.str.50, %edi
	jmp	.LBB0_196
.LBB0_148:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$3, yyval+12(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movsbl	(%rax), %ecx
	addl	$-33, %ecx
	cmpl	$29, %ecx
	ja	.LBB0_197
# BB#149:                               #   in Loop: Header=BB0_25 Depth=1
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_150:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.52, %edi
	jmp	.LBB0_196
.LBB0_151:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.57, %edi
	jmp	.LBB0_155
.LBB0_152:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.58, %edi
	jmp	.LBB0_155
.LBB0_153:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movb	8(%rbx), %al
	movb	%al, genstr(%rip)
	movb	$0, genstr+1(%rip)
	movl	$genstr, %edi
	callq	generate
	movl	36(%rbx), %eax
	orl	-12(%rbx), %eax
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_154:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.59, %edi
.LBB0_155:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	generate
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	36(%rcx), %eax
	orl	-12(%rcx), %eax
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_156:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.60, %edi
	callq	generate
.LBB0_157:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %eax
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_158:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$1, yyval+12(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %edx
	testl	%edx, %edx
	js	.LBB0_5
.LBB0_159:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_160:                              #   in Loop: Header=BB0_25 Depth=1
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	$1, yyval+12(%rip)
	cmpl	$1, %eax
	jne	.LBB0_187
# BB#161:                               #   in Loop: Header=BB0_25 Depth=1
	movb	(%rbx), %al
	cmpb	$49, %al
	je	.LBB0_22
# BB#162:                               #   in Loop: Header=BB0_25 Depth=1
	cmpb	$48, %al
	jne	.LBB0_187
# BB#163:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.35, %edi
	jmp	.LBB0_188
.LBB0_164:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	12(%rax), %eax
	orl	$1, %eax
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_165:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$1, yyval+12(%rip)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	16(%rbp), %rbx
	movq	-48(%rbp), %rdi
	movl	$2, %esi
	callq	lookup
	movl	%eax, %r15d
	cmpq	$0, %rbx
	je	.LBB0_6
# BB#166:                               #   in Loop: Header=BB0_25 Depth=1
	movq	16(%rbp), %rdi
	xorl	%esi, %esi
	callq	arg_str
	movq	%rax, %rcx
	movl	$genstr, %edi
	movl	$.L.str.64, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movq	16(%rbp), %rdi
	callq	free_args
	jmp	.LBB0_195
.LBB0_167:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$1, yyval+12(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %edx
	movb	8(%rax), %al
	testl	%edx, %edx
	js	.LBB0_7
# BB#168:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	cmpb	$43, %al
	jne	.LBB0_11
# BB#169:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.68, %esi
	jmp	.LBB0_19
.LBB0_170:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$1, yyval+12(%rip)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	12(%rbx), %edx
	testl	%edx, %edx
	js	.LBB0_9
# BB#171:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movl	12(%rbx), %edx
	cmpb	$43, 32(%rbx)
	jne	.LBB0_12
# BB#172:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_173:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.75, %edi
	jmp	.LBB0_177
.LBB0_174:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.76, %edi
	jmp	.LBB0_177
.LBB0_175:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.77, %edi
	jmp	.LBB0_177
.LBB0_176:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	callq	warn
	movl	$.L.str.79, %edi
.LBB0_177:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	generate
	movl	$1, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_178:                              #   in Loop: Header=BB0_25 Depth=1
	movq	(%r15), %rdi
	xorl	%esi, %esi
	jmp	.LBB0_182
.LBB0_179:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 12(%rax)
	jl	.LBB0_181
# BB#180:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	callq	warn
.LBB0_181:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-48(%rax), %rdi
	movl	$1, %esi
.LBB0_182:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	lookup
	movl	%eax, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_183:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_184:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$1, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_185:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$2, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_186:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$3, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_187:                              # %.critedge194
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.62, %edi
	callq	generate
	movq	(%r15), %rdi
	callq	generate
	movl	$.L.str.63, %edi
.LBB0_188:                              #   in Loop: Header=BB0_25 Depth=1
	callq	generate
	movq	(%r15), %rdi
	callq	free
	jmp	.LBB0_197
.LBB0_189:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.6, %edi
	jmp	.LBB0_196
.LBB0_190:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.12, %edi
.LBB0_191:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB0_197
.LBB0_1:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.22, %esi
.LBB0_2:                                #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB0_195
.LBB0_3:                                #   in Loop: Header=BB0_25 Depth=1
	negl	%edx
	movl	$genstr, %edi
	movl	$.L.str.41, %esi
.LBB0_4:                                #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movl	$0, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_5:                                #   in Loop: Header=BB0_25 Depth=1
	negl	%edx
	movl	$genstr, %edi
	movl	$.L.str.61, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_6:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.65, %esi
	jmp	.LBB0_21
.LBB0_7:                                #   in Loop: Header=BB0_25 Depth=1
	negl	%edx
	movl	$genstr, %edi
	cmpb	$43, %al
	jne	.LBB0_18
# BB#8:                                 #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.66, %esi
	jmp	.LBB0_19
.LBB0_9:                                #   in Loop: Header=BB0_25 Depth=1
	negl	%edx
	xorl	%r15d, %r15d
	movl	$genstr, %edi
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	subl	12(%rbx), %r15d
	cmpb	$43, 32(%rbx)
	jne	.LBB0_20
# BB#10:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.71, %esi
	jmp	.LBB0_21
.LBB0_11:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.69, %esi
	jmp	.LBB0_19
.LBB0_12:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_13:                               #   in Loop: Header=BB0_25 Depth=1
	cmpb	$61, 1(%rax)
	jne	.LBB0_23
# BB#14:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.53, %edi
	jmp	.LBB0_196
.LBB0_15:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.51, %edi
	jmp	.LBB0_196
.LBB0_16:                               #   in Loop: Header=BB0_25 Depth=1
	cmpb	$61, 1(%rax)
	jne	.LBB0_24
# BB#17:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.55, %edi
	jmp	.LBB0_196
.LBB0_18:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.67, %esi
.LBB0_19:                               #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	movl	%edx, %ecx
	callq	sprintf
	jmp	.LBB0_195
.LBB0_20:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.72, %esi
.LBB0_21:                               #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	movl	%r15d, %edx
	jmp	.LBB0_194
.LBB0_22:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.19, %edi
	jmp	.LBB0_188
.LBB0_23:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.54, %edi
	jmp	.LBB0_196
.LBB0_24:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.56, %edi
	jmp	.LBB0_196
	.p2align	4, 0x90
.LBB0_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_31 Depth 2
                                        #       Child Loop BB0_41 Depth 3
                                        #       Child Loop BB0_43 Depth 3
                                        #     Child Loop BB0_49 Depth 2
	movw	%bp, 16(%rsp,%r13,2)
	leaq	24(%r12), %r15
	movq	yyval+16(%rip), %rax
	movq	%rax, 40(%r12)
	movups	yyval(%rip), %xmm0
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movups	%xmm0, 24(%r12)
	movswq	%bp, %r12
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_31 Depth=2
	movw	-2(%rcx), %ax
	testw	%ax, %ax
	js	.LBB0_200
.LBB0_27:                               #   in Loop: Header=BB0_31 Depth=2
	testw	%ax, %ax
	jne	.LBB0_56
# BB#28:                                #   in Loop: Header=BB0_31 Depth=2
	movswl	yyerrflag(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB0_45
# BB#29:                                #   in Loop: Header=BB0_31 Depth=2
	cmpl	$0, yychar(%rip)
	je	.LBB0_199
# BB#30:                                #   in Loop: Header=BB0_31 Depth=2
	movl	$-1, yychar(%rip)
.LBB0_31:                               #   Parent Loop BB0_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_41 Depth 3
                                        #       Child Loop BB0_43 Depth 3
	movswl	yypact(%r12,%r12), %eax
	cmpl	$-999, %eax             # imm = 0xFC19
	jl	.LBB0_36
# BB#32:                                #   in Loop: Header=BB0_31 Depth=2
	movzwl	%ax, %ebx
	movl	yychar(%rip), %eax
	testl	%eax, %eax
	jns	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_31 Depth=2
	callq	yylex
	testl	%eax, %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	movl	%eax, yychar(%rip)
.LBB0_34:                               #   in Loop: Header=BB0_31 Depth=2
	addl	%eax, %ebx
	movzwl	%bx, %ecx
	cmpl	$705, %ecx              # imm = 0x2C1
	ja	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_31 Depth=2
	movswq	%bx, %rcx
	movswq	yyact(%rcx,%rcx), %r14
	movswl	yychk(%r14,%r14), %ecx
	cmpl	%eax, %ecx
	je	.LBB0_52
.LBB0_36:                               #   in Loop: Header=BB0_31 Depth=2
	movzwl	yydef(%r12,%r12), %eax
	cmpl	$65534, %eax            # imm = 0xFFFE
	jne	.LBB0_27
# BB#37:                                #   in Loop: Header=BB0_31 Depth=2
	movl	yychar(%rip), %eax
	testl	%eax, %eax
	jns	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_31 Depth=2
	callq	yylex
	testl	%eax, %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	movl	%eax, yychar(%rip)
.LBB0_39:                               # %.preheader195.preheader
                                        #   in Loop: Header=BB0_31 Depth=2
	movl	$yyexca+4, %ecx
	movl	$yyexca, %edx
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_40:                               # %.critedge
                                        #   in Loop: Header=BB0_41 Depth=3
	addq	$4, %rdx
	addq	$4, %rcx
.LBB0_41:                               # %.preheader195
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rdx), %esi
	cmpl	$65535, %esi            # imm = 0xFFFF
	jne	.LBB0_40
# BB#42:                                #   in Loop: Header=BB0_41 Depth=3
	cmpw	%bp, 2(%rdx)
	jne	.LBB0_40
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movswl	(%rcx), %edx
	addq	$4, %rcx
	testl	%edx, %edx
	js	.LBB0_26
# BB#44:                                # %.preheader
                                        #   in Loop: Header=BB0_43 Depth=3
	cmpl	%eax, %edx
	jne	.LBB0_43
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_45:                               #   in Loop: Header=BB0_25 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB0_48
# BB#46:                                #   in Loop: Header=BB0_25 Depth=1
	testl	%eax, %eax
	jne	.LBB0_55
# BB#47:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	yyerror
	incl	yynerrs(%rip)
.LBB0_48:                               # %.loopexit198
                                        #   in Loop: Header=BB0_25 Depth=1
	movw	$3, yyerrflag(%rip)
	movl	$1, %eax
	testq	%r13, %r13
	js	.LBB0_201
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	16(%rsp,%r13,2), %rcx
	movzwl	yypact(%rcx,%rcx), %ecx
	addl	$256, %ecx              # imm = 0x100
	movzwl	%cx, %edx
	cmpl	$705, %edx              # imm = 0x2C1
	ja	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_49 Depth=2
	movswq	%cx, %rcx
	movswq	yyact(%rcx,%rcx), %r14
	movzwl	yychk(%r14,%r14), %ecx
	cmpl	$256, %ecx              # imm = 0x100
	je	.LBB0_54
.LBB0_51:                               #   in Loop: Header=BB0_49 Depth=2
	addq	$-24, %r15
	testq	%r13, %r13
	leaq	-1(%r13), %r13
	jg	.LBB0_49
	jmp	.LBB0_201
.LBB0_52:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$-1, yychar(%rip)
	movq	yylval+16(%rip), %rax
	movq	%rax, yyval+16(%rip)
	movups	yylval(%rip), %xmm0
	movups	%xmm0, yyval(%rip)
	movw	yyerrflag(%rip), %ax
	testw	%ax, %ax
	jle	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_25 Depth=1
	decl	%eax
	movw	%ax, yyerrflag(%rip)
.LBB0_54:                               # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	movq	%r15, %r12
	jmp	.LBB0_197
.LBB0_55:                               #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_56:                               #   in Loop: Header=BB0_25 Depth=1
	cwtl
	movswq	%ax, %rdx
	movswq	yyr2(%rdx,%rdx), %rcx
	subq	%rcx, %r13
	negq	%rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	40(%r15,%rcx,8), %rsi
	movq	%rsi, yyval+16(%rip)
	movups	24(%r15,%rcx,8), %xmm0
	movups	%xmm0, yyval(%rip)
	movswq	yyr1(%rdx,%rdx), %rsi
	movswq	yypgo(%rsi,%rsi), %rdx
	movw	16(%rsp,%r13,2), %di
	addw	%dx, %di
	incl	%edi
	movswl	%di, %ebp
	cmpl	$705, %ebp              # imm = 0x2C1
	jg	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_25 Depth=1
	movswq	%di, %rdi
	movswq	yyact(%rdi,%rdi), %r14
	movswl	yychk(%r14,%r14), %edi
	addl	%esi, %edi
	je	.LBB0_59
.LBB0_58:                               #   in Loop: Header=BB0_25 Depth=1
	movw	yyact(%rdx,%rdx), %r14w
.LBB0_59:                               #   in Loop: Header=BB0_25 Depth=1
	leaq	(%r15,%rcx,8), %r12
	decl	%eax
	cmpl	$96, %eax
	ja	.LBB0_197
# BB#60:                                #   in Loop: Header=BB0_25 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_61:                               #   in Loop: Header=BB0_25 Depth=1
	callq	run_code
	jmp	.LBB0_197
.LBB0_62:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_63:                               #   in Loop: Header=BB0_25 Depth=1
	movq	$0, yyval+16(%rip)
	jmp	.LBB0_197
.LBB0_64:                               #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rax, yyval+16(%rip)
	jmp	.LBB0_197
.LBB0_65:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	cmpb	$0, interactive(%rip)
	je	.LBB0_197
# BB#66:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.3, %edi
	callq	puts
	callq	welcome
	jmp	.LBB0_197
.LBB0_67:                               #   in Loop: Header=BB0_25 Depth=1
	movw	$0, yyerrflag(%rip)
	callq	init_gen
	jmp	.LBB0_197
.LBB0_68:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.4, %edi
	callq	warranty
	jmp	.LBB0_197
.LBB0_69:                               #   in Loop: Header=BB0_25 Depth=1
	callq	limits
	jmp	.LBB0_197
.LBB0_70:                               #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	36(%rbx), %eax
	testb	$2, %al
	je	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	warn
	movl	36(%rbx), %eax
.LBB0_72:                               #   in Loop: Header=BB0_25 Depth=1
	testb	$1, %al
	jne	.LBB0_189
# BB#73:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.7, %edi
	jmp	.LBB0_196
.LBB0_74:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	movl	$.L.str.8, %edi
	jmp	.LBB0_105
.LBB0_75:                               #   in Loop: Header=BB0_25 Depth=1
	movl	break_label(%rip), %edx
	testl	%edx, %edx
	jne	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.9, %edi
	jmp	.LBB0_191
.LBB0_77:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	warn
	movl	continue_label(%rip), %edx
	testl	%edx, %edx
	je	.LBB0_190
.LBB0_78:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_79:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.13, %edi
	jmp	.LBB0_196
.LBB0_80:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.14, %edi
	jmp	.LBB0_196
.LBB0_81:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.15, %edi
	jmp	.LBB0_196
.LBB0_82:                               #   in Loop: Header=BB0_25 Depth=1
	movl	break_label(%rip), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 36(%rcx)
	movl	next_label(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, next_label(%rip)
	movl	%eax, break_label(%rip)
	jmp	.LBB0_197
.LBB0_83:                               #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 12(%rax)
	jl	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	warn
.LBB0_85:                               #   in Loop: Header=BB0_25 Depth=1
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%edx, 12(%rax)
	movl	$genstr, %edi
	testl	%edx, %edx
	js	.LBB0_192
# BB#86:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.18, %esi
	jmp	.LBB0_193
.LBB0_87:                               #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpl	$0, 12(%rbx)
	jns	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.19, %edi
	callq	generate
.LBB0_89:                               #   in Loop: Header=BB0_25 Depth=1
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movl	%edx, 12(%rbx)
	movl	break_label(%rip), %ecx
	movl	$genstr, %edi
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movl	continue_label(%rip), %eax
	movl	%eax, yyval+12(%rip)
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movl	%edx, continue_label(%rip)
	jmp	.LBB0_98
.LBB0_90:                               #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	12(%rbx), %eax
	cmpl	$2, %eax
	jl	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	warn
	movl	12(%rbx), %eax
.LBB0_92:                               #   in Loop: Header=BB0_25 Depth=1
	movl	-132(%rbx), %edx
	movl	-60(%rbx), %ecx
	movl	$genstr, %edi
	testl	%eax, %eax
	js	.LBB0_1
# BB#93:                                #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.23, %esi
	jmp	.LBB0_2
.LBB0_94:                               #   in Loop: Header=BB0_25 Depth=1
	movl	continue_label(%rip), %edx
	movl	break_label(%rip), %ecx
	movl	$genstr, %edi
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	-252(%rcx), %eax
	movl	%eax, break_label(%rip)
	movl	-60(%rcx), %eax
	movl	%eax, continue_label(%rip)
	jmp	.LBB0_197
.LBB0_95:                               #   in Loop: Header=BB0_25 Depth=1
	movl	if_label(%rip), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 12(%rcx)
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movl	%edx, if_label(%rip)
	jmp	.LBB0_100
.LBB0_96:                               #   in Loop: Header=BB0_25 Depth=1
	movl	if_label(%rip), %edx
	movl	$genstr, %edi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	-60(%rax), %eax
	movl	%eax, if_label(%rip)
	jmp	.LBB0_197
.LBB0_97:                               #   in Loop: Header=BB0_25 Depth=1
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%edx, 36(%rax)
.LBB0_98:                               # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
.LBB0_192:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.17, %esi
	jmp	.LBB0_193
.LBB0_99:                               #   in Loop: Header=BB0_25 Depth=1
	movl	break_label(%rip), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 36(%rcx)
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movl	%edx, break_label(%rip)
.LBB0_100:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.24, %esi
	jmp	.LBB0_193
.LBB0_101:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	-108(%rbx), %edx
	movl	break_label(%rip), %ecx
	movl	$genstr, %edi
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movl	-36(%rbx), %eax
	movl	%eax, break_label(%rip)
	jmp	.LBB0_197
.LBB0_102:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	jmp	.LBB0_197
.LBB0_103:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.25, %edi
	jmp	.LBB0_132
.LBB0_104:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.26, %edi
.LBB0_105:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	generate
	movq	(%r15), %rdi
	jmp	.LBB0_188
.LBB0_106:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.27, %edi
	jmp	.LBB0_196
.LBB0_107:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	warn
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%edx, 36(%rbx)
	movl	if_label(%rip), %ecx
	movl	$genstr, %edi
	movl	$.L.str.29, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movl	36(%rbx), %eax
	movl	%eax, if_label(%rip)
	jmp	.LBB0_197
.LBB0_108:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	-56(%rbx), %rdi
	movq	40(%rbx), %rsi
	callq	check_params
	movq	-120(%rbx), %rdi
	movl	$2, %esi
	callq	lookup
	movl	%eax, %ebp
	movq	-56(%rbx), %rdi
	movl	$1, %esi
	callq	arg_str
	movq	%rax, %r15
	movq	40(%rbx), %rdi
	movl	$1, %esi
	callq	arg_str
	movq	%rax, %r8
	movl	$genstr, %edi
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movq	%r15, %rcx
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movq	-56(%rbx), %rdi
	callq	free_args
	movq	40(%rbx), %rdi
	callq	free_args
	movl	next_label(%rip), %eax
	movl	%eax, -132(%rbx)
	movl	$0, next_label(%rip)
	jmp	.LBB0_197
.LBB0_109:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.31, %edi
	callq	generate
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	-228(%rax), %eax
	movl	%eax, next_label(%rip)
	jmp	.LBB0_197
.LBB0_110:                              #   in Loop: Header=BB0_25 Depth=1
	movq	(%r15), %rdi
	xorl	%esi, %esi
	jmp	.LBB0_112
.LBB0_111:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-24(%rax), %rdi
	movl	$1, %esi
.LBB0_112:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	lookup
	movsbl	%al, %esi
	xorl	%edi, %edi
	jmp	.LBB0_127
.LBB0_113:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-8(%rax), %r15
	movq	24(%rax), %rdi
	xorl	%esi, %esi
	jmp	.LBB0_115
.LBB0_114:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-56(%rax), %r15
	movq	-24(%rax), %rdi
	movl	$1, %esi
.LBB0_115:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	lookup
	movsbl	%al, %esi
	movq	%r15, %rdi
	jmp	.LBB0_127
.LBB0_116:                              #   in Loop: Header=BB0_25 Depth=1
	movq	$0, yyval+16(%rip)
	jmp	.LBB0_197
.LBB0_117:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 36(%rax)
	jl	.LBB0_119
# BB#118:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	warn
.LBB0_119:                              #   in Loop: Header=BB0_25 Depth=1
	xorl	%edi, %edi
	jmp	.LBB0_124
.LBB0_120:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-24(%rax), %rdi
	movl	$1, %esi
	callq	lookup
	movl	%eax, %ecx
	negl	%ecx
	movl	$genstr, %edi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	xorl	%edi, %edi
	jmp	.LBB0_126
.LBB0_121:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 36(%rax)
	jl	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	warn
.LBB0_123:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-8(%rax), %rdi
.LBB0_124:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	xorl	%esi, %esi
	jmp	.LBB0_127
.LBB0_125:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	-24(%rbx), %rdi
	movl	$1, %esi
	callq	lookup
	movl	%eax, %ecx
	negl	%ecx
	movl	$genstr, %edi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	movl	$genstr, %edi
	callq	generate
	movq	-56(%rbx), %rdi
.LBB0_126:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$1, %esi
.LBB0_127:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	nextarg
	movq	%rax, yyval+16(%rip)
	jmp	.LBB0_197
.LBB0_128:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$-1, yyval+12(%rip)
	movl	$.L.str.34, %edi
	jmp	.LBB0_132
.LBB0_129:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$0, yyval+12(%rip)
	movl	$.L.str.35, %edi
	jmp	.LBB0_196
.LBB0_130:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 36(%rax)
	jl	.LBB0_197
# BB#131:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.36, %edi
.LBB0_132:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
	callq	warn
	jmp	.LBB0_197
.LBB0_133:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$61, 32(%rax)
	je	.LBB0_197
# BB#134:                               #   in Loop: Header=BB0_25 Depth=1
	movl	12(%rax), %edx
	testl	%edx, %edx
	jns	.LBB0_159
# BB#135:                               #   in Loop: Header=BB0_25 Depth=1
	negl	%edx
	movl	$genstr, %edi
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	jmp	.LBB0_194
.LBB0_136:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$2, 36(%rax)
	jl	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	warn
.LBB0_138:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	-16(%rax), %al
	cmpb	$61, %al
	je	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_25 Depth=1
	movb	%al, genstr(%rip)
	movb	$0, genstr+1(%rip)
	movl	$genstr, %edi
	callq	generate
.LBB0_140:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	-36(%rax), %edx
	testl	%edx, %edx
	js	.LBB0_3
# BB#141:                               #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
	movl	$.L.str.42, %esi
	jmp	.LBB0_4
.LBB0_142:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	warn
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%edx, 36(%rax)
	movl	$genstr, %edi
	movl	$.L.str.44, %esi
	jmp	.LBB0_193
.LBB0_143:                              #   in Loop: Header=BB0_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	-12(%rbx), %edx
	movl	$genstr, %edi
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	movl	%edx, %ecx
	callq	sprintf
	jmp	.LBB0_146
.LBB0_144:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	warn
	movl	next_label(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, next_label(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%edx, 36(%rax)
	movl	$genstr, %edi
	movl	$.L.str.47, %esi
.LBB0_193:                              #   in Loop: Header=BB0_25 Depth=1
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
.LBB0_194:                              #   in Loop: Header=BB0_25 Depth=1
	callq	sprintf
.LBB0_195:                              #   in Loop: Header=BB0_25 Depth=1
	movl	$genstr, %edi
.LBB0_196:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	callq	generate
	.p2align	4, 0x90
.LBB0_197:                              # %.backedge
                                        #   in Loop: Header=BB0_25 Depth=1
	cmpq	$149, %r13
	leaq	1(%r13), %r13
	movw	%r14w, %bp
	jle	.LBB0_25
# BB#198:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
.LBB0_199:                              # %.loopexit
	movl	$1, %eax
	jmp	.LBB0_201
.LBB0_200:
	xorl	%eax, %eax
.LBB0_201:                              # %.loopexit
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_202:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	yyparse, .Lfunc_end0-yyparse
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_65
	.quad	.LBB0_197
	.quad	.LBB0_61
	.quad	.LBB0_61
	.quad	.LBB0_67
	.quad	.LBB0_62
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_62
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_157
	.quad	.LBB0_68
	.quad	.LBB0_69
	.quad	.LBB0_70
	.quad	.LBB0_74
	.quad	.LBB0_75
	.quad	.LBB0_77
	.quad	.LBB0_202
	.quad	.LBB0_79
	.quad	.LBB0_80
	.quad	.LBB0_81
	.quad	.LBB0_82
	.quad	.LBB0_83
	.quad	.LBB0_87
	.quad	.LBB0_90
	.quad	.LBB0_94
	.quad	.LBB0_95
	.quad	.LBB0_96
	.quad	.LBB0_97
	.quad	.LBB0_99
	.quad	.LBB0_101
	.quad	.LBB0_102
	.quad	.LBB0_103
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_104
	.quad	.LBB0_106
	.quad	.LBB0_197
	.quad	.LBB0_107
	.quad	.LBB0_197
	.quad	.LBB0_108
	.quad	.LBB0_109
	.quad	.LBB0_63
	.quad	.LBB0_197
	.quad	.LBB0_63
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_110
	.quad	.LBB0_111
	.quad	.LBB0_113
	.quad	.LBB0_114
	.quad	.LBB0_116
	.quad	.LBB0_197
	.quad	.LBB0_117
	.quad	.LBB0_120
	.quad	.LBB0_121
	.quad	.LBB0_125
	.quad	.LBB0_128
	.quad	.LBB0_197
	.quad	.LBB0_129
	.quad	.LBB0_130
	.quad	.LBB0_133
	.quad	.LBB0_136
	.quad	.LBB0_142
	.quad	.LBB0_143
	.quad	.LBB0_144
	.quad	.LBB0_145
	.quad	.LBB0_147
	.quad	.LBB0_148
	.quad	.LBB0_151
	.quad	.LBB0_152
	.quad	.LBB0_153
	.quad	.LBB0_154
	.quad	.LBB0_156
	.quad	.LBB0_158
	.quad	.LBB0_160
	.quad	.LBB0_164
	.quad	.LBB0_165
	.quad	.LBB0_167
	.quad	.LBB0_170
	.quad	.LBB0_173
	.quad	.LBB0_174
	.quad	.LBB0_175
	.quad	.LBB0_176
	.quad	.LBB0_178
	.quad	.LBB0_179
	.quad	.LBB0_183
	.quad	.LBB0_184
	.quad	.LBB0_185
	.quad	.LBB0_186
.LJTI0_1:
	.quad	.LBB0_150
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_197
	.quad	.LBB0_13
	.quad	.LBB0_15
	.quad	.LBB0_16

	.type	yyexca,@object          # @yyexca
	.data
	.globl	yyexca
	.p2align	4
yyexca:
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	257                     # 0x101
	.short	6                       # 0x6
	.short	59                      # 0x3b
	.short	6                       # 0x6
	.short	65534                   # 0xfffe
	.short	0                       # 0x0
	.size	yyexca, 20

	.type	yyact,@object           # @yyact
	.globl	yyact
	.p2align	4
yyact:
	.short	27                      # 0x1b
	.short	65                      # 0x41
	.short	149                     # 0x95
	.short	145                     # 0x91
	.short	48                      # 0x30
	.short	25                      # 0x19
	.short	57                      # 0x39
	.short	45                      # 0x2d
	.short	58                      # 0x3a
	.short	46                      # 0x2e
	.short	105                     # 0x69
	.short	135                     # 0x87
	.short	41                      # 0x29
	.short	125                     # 0x7d
	.short	86                      # 0x56
	.short	86                      # 0x56
	.short	38                      # 0x26
	.short	66                      # 0x42
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	141                     # 0x8d
	.short	109                     # 0x6d
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	36                      # 0x24
	.short	6                       # 0x6
	.short	134                     # 0x86
	.short	11                      # 0xb
	.short	159                     # 0x9f
	.short	88                      # 0x58
	.short	104                     # 0x68
	.short	48                      # 0x30
	.short	150                     # 0x96
	.short	27                      # 0x1b
	.short	136                     # 0x88
	.short	62                      # 0x3e
	.short	62                      # 0x3e
	.short	62                      # 0x3e
	.short	25                      # 0x19
	.short	142                     # 0x8e
	.short	126                     # 0x7e
	.short	63                      # 0x3f
	.short	151                     # 0x97
	.short	127                     # 0x7f
	.short	125                     # 0x7d
	.short	118                     # 0x76
	.short	115                     # 0x73
	.short	162                     # 0xa2
	.short	139                     # 0x8b
	.short	124                     # 0x7c
	.short	8                       # 0x8
	.short	117                     # 0x75
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	108                     # 0x6c
	.short	61                      # 0x3d
	.short	40                      # 0x28
	.short	102                     # 0x66
	.short	48                      # 0x30
	.short	84                      # 0x54
	.short	82                      # 0x52
	.short	72                      # 0x48
	.short	71                      # 0x47
	.short	70                      # 0x46
	.short	69                      # 0x45
	.short	68                      # 0x44
	.short	27                      # 0x1b
	.short	67                      # 0x43
	.short	51                      # 0x33
	.short	49                      # 0x31
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	75                      # 0x4b
	.short	76                      # 0x4c
	.short	77                      # 0x4d
	.short	78                      # 0x4e
	.short	79                      # 0x4f
	.short	81                      # 0x51
	.short	74                      # 0x4a
	.short	83                      # 0x53
	.short	87                      # 0x57
	.short	73                      # 0x49
	.short	92                      # 0x5c
	.short	21                      # 0x15
	.short	91                      # 0x5b
	.short	163                     # 0xa3
	.short	140                     # 0x8c
	.short	119                     # 0x77
	.short	63                      # 0x3f
	.short	153                     # 0x99
	.short	96                      # 0x60
	.short	98                      # 0x62
	.short	152                     # 0x98
	.short	89                      # 0x59
	.short	56                      # 0x38
	.short	99                      # 0x63
	.short	100                     # 0x64
	.short	101                     # 0x65
	.short	27                      # 0x1b
	.short	129                     # 0x81
	.short	64                      # 0x40
	.short	106                     # 0x6a
	.short	107                     # 0x6b
	.short	25                      # 0x19
	.short	52                      # 0x34
	.short	144                     # 0x90
	.short	40                      # 0x28
	.short	128                     # 0x80
	.short	164                     # 0xa4
	.short	113                     # 0x71
	.short	110                     # 0x6e
	.short	155                     # 0x9b
	.short	112                     # 0x70
	.short	137                     # 0x89
	.short	50                      # 0x32
	.short	4                       # 0x4
	.short	21                      # 0x15
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	95                      # 0x5f
	.short	116                     # 0x74
	.short	94                      # 0x5e
	.short	148                     # 0x94
	.short	103                     # 0x67
	.short	80                      # 0x50
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	114                     # 0x72
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	91                      # 0x5b
	.short	0                       # 0x0
	.short	130                     # 0x82
	.short	131                     # 0x83
	.short	98                      # 0x62
	.short	0                       # 0x0
	.short	21                      # 0x15
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	143                     # 0x8f
	.short	0                       # 0x0
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	110                     # 0x6e
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	98                      # 0x62
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	157                     # 0x9d
	.short	160                     # 0xa0
	.short	0                       # 0x0
	.short	138                     # 0x8a
	.short	154                     # 0x9a
	.short	21                      # 0x15
	.short	0                       # 0x0
	.short	110                     # 0x6e
	.short	0                       # 0x0
	.short	113                     # 0x71
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	146                     # 0x92
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	156                     # 0x9c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	147                     # 0x93
	.short	21                      # 0x15
	.short	0                       # 0x0
	.short	85                      # 0x55
	.short	161                     # 0xa1
	.short	37                      # 0x25
	.short	165                     # 0xa5
	.short	55                      # 0x37
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	133                     # 0x85
	.short	24                      # 0x18
	.short	12                      # 0xc
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	158                     # 0x9e
	.short	29                      # 0x1d
	.short	47                      # 0x2f
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	30                      # 0x1e
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	9                       # 0x9
	.short	16                      # 0x10
	.short	36                      # 0x24
	.short	14                      # 0xe
	.short	22                      # 0x16
	.short	10                      # 0xa
	.short	5                       # 0x5
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	24                      # 0x18
	.short	12                      # 0xc
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	7                       # 0x7
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	30                      # 0x1e
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	9                       # 0x9
	.short	16                      # 0x10
	.short	36                      # 0x24
	.short	14                      # 0xe
	.short	22                      # 0x16
	.short	10                      # 0xa
	.short	55                      # 0x37
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	27                      # 0x1b
	.short	24                      # 0x18
	.short	12                      # 0xc
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	25                      # 0x19
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	30                      # 0x1e
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	9                       # 0x9
	.short	16                      # 0x10
	.short	36                      # 0x24
	.short	14                      # 0xe
	.short	22                      # 0x16
	.short	10                      # 0xa
	.short	39                      # 0x27
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	12                      # 0xc
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	30                      # 0x1e
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	9                       # 0x9
	.short	16                      # 0x10
	.short	36                      # 0x24
	.short	14                      # 0xe
	.short	22                      # 0x16
	.short	10                      # 0xa
	.short	24                      # 0x18
	.short	12                      # 0xc
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	30                      # 0x1e
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	9                       # 0x9
	.short	16                      # 0x10
	.short	36                      # 0x24
	.short	14                      # 0xe
	.short	22                      # 0x16
	.short	10                      # 0xa
	.short	24                      # 0x18
	.short	0                       # 0x0
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	31                      # 0x1f
	.short	29                      # 0x1d
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	30                      # 0x1e
	.short	33                      # 0x21
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	36                      # 0x24
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	45                      # 0x2d
	.short	33                      # 0x21
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	36                      # 0x24
	.short	24                      # 0x18
	.short	90                      # 0x5a
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	123                     # 0x7b
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	29                      # 0x1d
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	28                      # 0x1c
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	31                      # 0x1f
	.short	29                      # 0x1d
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	30                      # 0x1e
	.short	33                      # 0x21
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	36                      # 0x24
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	0                       # 0x0
	.short	122                     # 0x7a
	.short	36                      # 0x24
	.short	45                      # 0x2d
	.short	121                     # 0x79
	.short	46                      # 0x2e
	.short	45                      # 0x2d
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	120                     # 0x78
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	111                     # 0x6f
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	0                       # 0x0
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	0                       # 0x0
	.short	46                      # 0x2e
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	0                       # 0x0
	.short	132                     # 0x84
	.short	26                      # 0x1a
	.short	93                      # 0x5d
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	29                      # 0x1d
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	45                      # 0x2d
	.short	30                      # 0x1e
	.short	46                      # 0x2e
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	36                      # 0x24
	.short	0                       # 0x0
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	48                      # 0x30
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	0                       # 0x0
	.short	97                      # 0x61
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	30                      # 0x1e
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	31                      # 0x1f
	.short	0                       # 0x0
	.short	32                      # 0x20
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	0                       # 0x0
	.short	33                      # 0x21
	.short	0                       # 0x0
	.short	48                      # 0x30
	.short	36                      # 0x24
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	47                      # 0x2f
	.short	47                      # 0x2f
	.short	44                      # 0x2c
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	44                      # 0x2c
	.size	yyact, 1412

	.type	yypact,@object          # @yypact
	.globl	yypact
	.p2align	4
yypact:
	.short	64536                   # 0xfc18
	.short	65529                   # 0xfff9
	.short	64536                   # 0xfc18
	.short	65493                   # 0xffd5
	.short	64536                   # 0xfc18
	.short	58                      # 0x3a
	.short	64536                   # 0xfc18
	.short	65286                   # 0xff06
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	423                     # 0x1a7
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	29                      # 0x1d
	.short	64536                   # 0xfc18
	.short	28                      # 0x1c
	.short	64536                   # 0xfc18
	.short	26                      # 0x1a
	.short	64536                   # 0xfc18
	.short	65277                   # 0xfefd
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	64536                   # 0xfc18
	.short	162                     # 0xa2
	.short	65533                   # 0xfffd
	.short	65275                   # 0xfefb
	.short	27                      # 0x1b
	.short	25                      # 0x19
	.short	24                      # 0x18
	.short	23                      # 0x17
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	26                      # 0x1a
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	21                      # 0x15
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	20                      # 0x14
	.short	162                     # 0xa2
	.short	19                      # 0x13
	.short	65491                   # 0xffd3
	.short	64536                   # 0xfc18
	.short	87                      # 0x57
	.short	152                     # 0x98
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	409                     # 0x199
	.short	64536                   # 0xfc18
	.short	435                     # 0x1b3
	.short	245                     # 0xf5
	.short	162                     # 0xa2
	.short	64536                   # 0xfc18
	.short	65486                   # 0xffce
	.short	64536                   # 0xfc18
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	16                      # 0x10
	.short	64536                   # 0xfc18
	.short	65284                   # 0xff04
	.short	162                     # 0xa2
	.short	162                     # 0xa2
	.short	65500                   # 0xffdc
	.short	65446                   # 0xffa6
	.short	65446                   # 0xffa6
	.short	65473                   # 0xffc1
	.short	65473                   # 0xffc1
	.short	13                      # 0xd
	.short	423                     # 0x1a7
	.short	162                     # 0xa2
	.short	419                     # 0x1a3
	.short	162                     # 0xa2
	.short	26                      # 0x1a
	.short	87                      # 0x57
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	2                       # 0x2
	.short	64536                   # 0xfc18
	.short	423                     # 0x1a7
	.short	162                     # 0xa2
	.short	64536                   # 0xfc18
	.short	10                      # 0xa
	.short	1                       # 0x1
	.short	423                     # 0x1a7
	.short	65532                   # 0xfffc
	.short	364                     # 0x16c
	.short	408                     # 0x198
	.short	405                     # 0x195
	.short	375                     # 0x177
	.short	64536                   # 0xfc18
	.short	8                       # 0x8
	.short	0                       # 0x0
	.short	65485                   # 0xffcd
	.short	409                     # 0x199
	.short	439                     # 0x1b7
	.short	64536                   # 0xfc18
	.short	65520                   # 0xfff0
	.short	423                     # 0x1a7
	.short	64536                   # 0xfc18
	.short	423                     # 0x1a7
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	152                     # 0x98
	.short	65500                   # 0xffdc
	.short	64536                   # 0xfc18
	.short	212                     # 0xd4
	.short	126                     # 0x7e
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	65439                   # 0xff9f
	.short	65285                   # 0xff05
	.short	65477                   # 0xffc5
	.short	64536                   # 0xfc18
	.short	87                      # 0x57
	.short	7                       # 0x7
	.short	64536                   # 0xfc18
	.short	423                     # 0x1a7
	.short	65531                   # 0xfffb
	.short	64536                   # 0xfc18
	.short	65299                   # 0xff13
	.short	65484                   # 0xffcc
	.short	64536                   # 0xfc18
	.short	162                     # 0xa2
	.short	65262                   # 0xfeee
	.short	87                      # 0x57
	.short	116                     # 0x74
	.short	65257                   # 0xfee9
	.short	65475                   # 0xffc3
	.short	65519                   # 0xffef
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	65284                   # 0xff04
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	87                      # 0x57
	.short	26                      # 0x1a
	.short	65505                   # 0xffe1
	.short	162                     # 0xa2
	.short	64536                   # 0xfc18
	.short	65492                   # 0xffd4
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	6                       # 0x6
	.short	65496                   # 0xffd8
	.short	64536                   # 0xfc18
	.short	64536                   # 0xfc18
	.short	87                      # 0x57
	.short	64536                   # 0xfc18
	.size	yypact, 332

	.type	yypgo,@object           # @yypgo
	.globl	yypgo
	.p2align	4
yypgo:
	.short	0                       # 0x0
	.short	125                     # 0x7d
	.short	27                      # 0x1b
	.short	124                     # 0x7c
	.short	70                      # 0x46
	.short	21                      # 0x15
	.short	123                     # 0x7b
	.short	122                     # 0x7a
	.short	30                      # 0x1e
	.short	121                     # 0x79
	.short	119                     # 0x77
	.short	118                     # 0x76
	.short	117                     # 0x75
	.short	22                      # 0x16
	.short	50                      # 0x32
	.short	115                     # 0x73
	.short	23                      # 0x17
	.short	114                     # 0x72
	.short	113                     # 0x71
	.short	111                     # 0x6f
	.short	108                     # 0x6c
	.short	107                     # 0x6b
	.short	105                     # 0x69
	.short	104                     # 0x68
	.short	99                      # 0x63
	.short	94                      # 0x5e
	.short	29                      # 0x1d
	.short	93                      # 0x5d
	.short	92                      # 0x5c
	.short	89                      # 0x59
	.short	82                      # 0x52
	.short	81                      # 0x51
	.short	78                      # 0x4e
	.size	yypgo, 66

	.type	yyr1,@object            # @yyr1
	.globl	yyr1
	.p2align	4
yyr1:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	16                      # 0x10
	.short	16                      # 0x10
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	14                      # 0xe
	.short	21                      # 0x15
	.short	14                      # 0xe
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	25                      # 0x19
	.short	14                      # 0xe
	.short	26                      # 0x1a
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	27                      # 0x1b
	.short	22                      # 0x16
	.short	28                      # 0x1c
	.short	22                      # 0x16
	.short	29                      # 0x1d
	.short	15                      # 0xf
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	30                      # 0x1e
	.short	2                       # 0x2
	.short	31                      # 0x1f
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.size	yyr1, 196

	.type	yyr2,@object            # @yyr2
	.globl	yyr2
	.p2align	4
yyr2:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	4                       # 0x4
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	13                      # 0xd
	.short	0                       # 0x0
	.short	7                       # 0x7
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	7                       # 0x7
	.short	3                       # 0x3
	.short	0                       # 0x0
	.short	3                       # 0x3
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	3                       # 0x3
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	5                       # 0x5
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	5                       # 0x5
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	3                       # 0x3
	.short	1                       # 0x1
	.short	4                       # 0x4
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.size	yyr2, 196

	.type	yychk,@object           # @yychk
	.globl	yychk
	.p2align	4
yychk:
	.short	64536                   # 0xfc18
	.short	65535                   # 0xffff
	.short	65525                   # 0xfff5
	.short	65524                   # 0xfff4
	.short	65521                   # 0xfff1
	.short	256                     # 0x100
	.short	65520                   # 0xfff0
	.short	268                     # 0x10c
	.short	65522                   # 0xfff2
	.short	283                     # 0x11b
	.short	288                     # 0x120
	.short	65534                   # 0xfffe
	.short	261                     # 0x105
	.short	269                     # 0x10d
	.short	286                     # 0x11e
	.short	270                     # 0x10e
	.short	284                     # 0x11c
	.short	272                     # 0x110
	.short	273                     # 0x111
	.short	274                     # 0x112
	.short	275                     # 0x113
	.short	123                     # 0x7b
	.short	287                     # 0x11f
	.short	65532                   # 0xfffc
	.short	260                     # 0x104
	.short	45                      # 0x2d
	.short	263                     # 0x107
	.short	40                      # 0x28
	.short	262                     # 0x106
	.short	267                     # 0x10b
	.short	271                     # 0x10f
	.short	276                     # 0x114
	.short	278                     # 0x116
	.short	282                     # 0x11a
	.short	279                     # 0x117
	.short	280                     # 0x118
	.short	285                     # 0x11d
	.short	257                     # 0x101
	.short	59                      # 0x3b
	.short	257                     # 0x101
	.short	65522                   # 0xfff2
	.short	262                     # 0x106
	.short	258                     # 0x102
	.short	259                     # 0x103
	.short	266                     # 0x10a
	.short	43                      # 0x2b
	.short	45                      # 0x2d
	.short	264                     # 0x108
	.short	94                      # 0x5e
	.short	40                      # 0x28
	.short	65519                   # 0xffef
	.short	40                      # 0x28
	.short	65513                   # 0xffe9
	.short	65523                   # 0xfff3
	.short	65520                   # 0xfff0
	.short	256                     # 0x100
	.short	65511                   # 0xffe7
	.short	265                     # 0x109
	.short	267                     # 0x10b
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	40                      # 0x28
	.short	91                      # 0x5b
	.short	65532                   # 0xfffc
	.short	262                     # 0x106
	.short	278                     # 0x116
	.short	40                      # 0x28
	.short	40                      # 0x28
	.short	40                      # 0x28
	.short	40                      # 0x28
	.short	65520                   # 0xfff0
	.short	40                      # 0x28
	.short	65505                   # 0xffe1
	.short	65504                   # 0xffe0
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65533                   # 0xfffd
	.short	65534                   # 0xfffe
	.short	40                      # 0x28
	.short	65534                   # 0xfffe
	.short	40                      # 0x28
	.short	257                     # 0x101
	.short	59                      # 0x3b
	.short	125                     # 0x7d
	.short	65510                   # 0xffe6
	.short	65509                   # 0xffe5
	.short	261                     # 0x105
	.short	65534                   # 0xfffe
	.short	65506                   # 0xffe2
	.short	41                      # 0x29
	.short	65527                   # 0xfff7
	.short	65526                   # 0xfff6
	.short	65534                   # 0xfffe
	.short	262                     # 0x106
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	41                      # 0x29
	.short	65530                   # 0xfffa
	.short	65528                   # 0xfff8
	.short	262                     # 0x106
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	41                      # 0x29
	.short	65531                   # 0xfffb
	.short	65534                   # 0xfffe
	.short	41                      # 0x29
	.short	65534                   # 0xfffe
	.short	65520                   # 0xfff0
	.short	65522                   # 0xfff2
	.short	44                      # 0x2c
	.short	65534                   # 0xfffe
	.short	41                      # 0x29
	.short	44                      # 0x2c
	.short	91                      # 0x5b
	.short	93                      # 0x5d
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	41                      # 0x29
	.short	44                      # 0x2c
	.short	91                      # 0x5b
	.short	59                      # 0x3b
	.short	65515                   # 0xffeb
	.short	65512                   # 0xffe8
	.short	65510                   # 0xffe6
	.short	65534                   # 0xfffe
	.short	262                     # 0x106
	.short	93                      # 0x5d
	.short	123                     # 0x7b
	.short	262                     # 0x106
	.short	93                      # 0x5d
	.short	65518                   # 0xffee
	.short	65522                   # 0xfff2
	.short	41                      # 0x29
	.short	91                      # 0x5b
	.short	257                     # 0x101
	.short	91                      # 0x5b
	.short	65531                   # 0xfffb
	.short	65514                   # 0xffea
	.short	277                     # 0x115
	.short	65522                   # 0xfff2
	.short	93                      # 0x5d
	.short	65529                   # 0xfff9
	.short	281                     # 0x119
	.short	93                      # 0x5d
	.short	59                      # 0x3b
	.short	65508                   # 0xffe4
	.short	65507                   # 0xffe3
	.short	65528                   # 0xfff8
	.short	65517                   # 0xffed
	.short	65522                   # 0xfff2
	.short	65523                   # 0xfff3
	.short	257                     # 0x101
	.short	59                      # 0x3b
	.short	65531                   # 0xfffb
	.short	257                     # 0x101
	.short	41                      # 0x29
	.short	125                     # 0x7d
	.short	65516                   # 0xffec
	.short	65522                   # 0xfff2
	.size	yychk, 332

	.type	yydef,@object           # @yydef
	.globl	yydef
	.p2align	4
yydef:
	.short	1                       # 0x1
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	0                       # 0x0
	.short	7                       # 0x7
	.short	0                       # 0x0
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	28                      # 0x1c
	.short	0                       # 0x0
	.short	35                      # 0x23
	.short	10                      # 0xa
	.short	39                      # 0x27
	.short	82                      # 0x52
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	83                      # 0x53
	.short	0                       # 0x0
	.short	92                      # 0x5c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	96                      # 0x60
	.short	0                       # 0x0
	.short	94                      # 0x5e
	.short	95                      # 0x5f
	.short	97                      # 0x61
	.short	3                       # 0x3
	.short	9                       # 0x9
	.short	5                       # 0x5
	.short	17                      # 0x11
	.short	0                       # 0x0
	.short	71                      # 0x47
	.short	73                      # 0x49
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	67                      # 0x43
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	11                      # 0xb
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	69                      # 0x45
	.short	87                      # 0x57
	.short	75                      # 0x4b
	.short	81                      # 0x51
	.short	0                       # 0x0
	.short	59                      # 0x3b
	.short	0                       # 0x0
	.short	86                      # 0x56
	.short	92                      # 0x5c
	.short	96                      # 0x60
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	8                       # 0x8
	.short	50                      # 0x32
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	76                      # 0x4c
	.short	77                      # 0x4d
	.short	78                      # 0x4e
	.short	79                      # 0x4f
	.short	80                      # 0x50
	.short	0                       # 0x0
	.short	68                      # 0x44
	.short	65                      # 0x41
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	38                      # 0x26
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	0                       # 0x0
	.short	84                      # 0x54
	.short	0                       # 0x0
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	92                      # 0x5c
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	91                      # 0x5b
	.short	0                       # 0x0
	.short	51                      # 0x33
	.short	55                      # 0x37
	.short	72                      # 0x48
	.short	74                      # 0x4a
	.short	27                      # 0x1b
	.short	0                       # 0x0
	.short	66                      # 0x42
	.short	33                      # 0x21
	.short	36                      # 0x24
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	0                       # 0x0
	.short	70                      # 0x46
	.short	85                      # 0x55
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	93                      # 0x5d
	.short	88                      # 0x58
	.short	89                      # 0x59
	.short	90                      # 0x5a
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	29                      # 0x1d
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	42                      # 0x2a
	.short	63                      # 0x3f
	.short	92                      # 0x5c
	.short	62                      # 0x3e
	.short	0                       # 0x0
	.short	57                      # 0x39
	.short	56                      # 0x38
	.short	65                      # 0x41
	.short	45                      # 0x2d
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	52                      # 0x34
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	34                      # 0x22
	.short	46                      # 0x2e
	.short	37                      # 0x25
	.short	64                      # 0x40
	.short	48                      # 0x30
	.short	0                       # 0x0
	.short	58                      # 0x3a
	.short	30                      # 0x1e
	.short	0                       # 0x0
	.short	10                      # 0xa
	.short	0                       # 0x0
	.short	65                      # 0x41
	.short	47                      # 0x2f
	.short	0                       # 0x0
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	31                      # 0x1f
	.short	49                      # 0x31
	.short	0                       # 0x0
	.short	32                      # 0x20
	.size	yydef, 332

	.type	yychar,@object          # @yychar
	.globl	yychar
	.p2align	2
yychar:
	.long	4294967295              # 0xffffffff
	.size	yychar, 4

	.type	yynerrs,@object         # @yynerrs
	.bss
	.globl	yynerrs
	.p2align	2
yynerrs:
	.long	0                       # 0x0
	.size	yynerrs, 4

	.type	yyerrflag,@object       # @yyerrflag
	.globl	yyerrflag
	.p2align	1
yyerrflag:
	.short	0                       # 0x0
	.size	yyerrflag, 2

	.type	yyv,@object             # @yyv
	.comm	yyv,3600,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"yacc stack overflow"
	.size	.L.str, 20

	.type	yyval,@object           # @yyval
	.comm	yyval,24,8
	.type	yylval,@object          # @yylval
	.comm	yylval,24,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"syntax error"
	.size	.L.str.1, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"bc 1.02 (Mar 3, 92) Copyright (C) 1991, 1992 Free Software Foundation, Inc."
	.size	.L.str.3, 76

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.zero	1
	.size	.L.str.4, 1

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"comparison in expression"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"W"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"p"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"w"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Break outside a for/while"
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"J%1d:"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Continue statement"
	.size	.L.str.11, 19

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Continue outside a for"
	.size	.L.str.12, 23

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"h"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"0R"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"R"
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Comparison in first for expression"
	.size	.L.str.16, 35

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"N%1d:"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"pN%1d:"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"1"
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"B%1d:J%1d:"
	.size	.L.str.20, 11

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Comparison in third for expression"
	.size	.L.str.21, 35

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"J%1d:N%1d:"
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"pJ%1d:N%1d:"
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Z%1d:"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"print statement"
	.size	.L.str.25, 16

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"O"
	.size	.L.str.26, 2

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"P"
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"else clause in if statement"
	.size	.L.str.28, 28

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"J%d:N%1d:"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"F%d,%s.%s["
	.size	.L.str.30, 11

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"0R]"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"comparison in argument"
	.size	.L.str.32, 23

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"K%d:"
	.size	.L.str.33, 5

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Missing expression in for statement"
	.size	.L.str.34, 36

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"0"
	.size	.L.str.35, 2

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"comparison in return expresion"
	.size	.L.str.36, 31

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"DL%d:"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"l%d:"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"comparison in assignment"
	.size	.L.str.39, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"S%d:"
	.size	.L.str.41, 5

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"s%d:"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"&& operator"
	.size	.L.str.43, 12

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"DZ%d:p"
	.size	.L.str.44, 7

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"DZ%d:p1N%d:"
	.size	.L.str.45, 12

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"|| operator"
	.size	.L.str.46, 12

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"B%d:"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"B%d:0J%d:N%d:1N%d:"
	.size	.L.str.48, 19

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"! operator"
	.size	.L.str.49, 11

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"!"
	.size	.L.str.50, 2

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"="
	.size	.L.str.51, 2

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"#"
	.size	.L.str.52, 2

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"{"
	.size	.L.str.53, 2

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"<"
	.size	.L.str.54, 2

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"}"
	.size	.L.str.55, 2

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	">"
	.size	.L.str.56, 2

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"+"
	.size	.L.str.57, 2

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"-"
	.size	.L.str.58, 2

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"^"
	.size	.L.str.59, 2

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"n"
	.size	.L.str.60, 2

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"L%d:"
	.size	.L.str.61, 5

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"K"
	.size	.L.str.62, 2

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	":"
	.size	.L.str.63, 2

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"C%d,%s:"
	.size	.L.str.64, 8

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"C%d:"
	.size	.L.str.65, 5

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"DA%d:L%d:"
	.size	.L.str.66, 10

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"DM%d:L%d:"
	.size	.L.str.67, 10

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"i%d:l%d:"
	.size	.L.str.68, 9

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"d%d:l%d:"
	.size	.L.str.69, 9

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"DL%d:x"
	.size	.L.str.70, 7

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"A%d:"
	.size	.L.str.71, 5

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"M%d:"
	.size	.L.str.72, 5

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"i%d:"
	.size	.L.str.73, 5

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"d%d:"
	.size	.L.str.74, 5

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"cL"
	.size	.L.str.75, 3

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"cR"
	.size	.L.str.76, 3

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"cS"
	.size	.L.str.77, 3

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"read function"
	.size	.L.str.78, 14

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"cI"
	.size	.L.str.79, 3

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"comparison in subscript"
	.size	.L.str.80, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
