	.text
	.file	"ag_dec.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	40                      # 0x28
	.long	14                      # 0xe
	.text
	.globl	set_standard_ag_params
	.p2align	4, 0x90
	.type	set_standard_ag_params,@function
set_standard_ag_params:                 # @set_standard_ag_params
	.cfi_startproc
# BB#0:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [10,10,40,14]
	movups	%xmm0, (%rdi)
	movabsq	$2027224580095, %rax    # imm = 0x1D800003FFF
	movq	%rax, 16(%rdi)
	movl	%esi, 24(%rdi)
	movl	%edx, 28(%rdi)
	movl	$255, 32(%rdi)
	retq
.Lfunc_end0:
	.size	set_standard_ag_params, .Lfunc_end0-set_standard_ag_params
	.cfi_endproc

	.globl	set_ag_params
	.p2align	4, 0x90
	.type	set_ag_params,@function
set_ag_params:                          # @set_ag_params
	.cfi_startproc
# BB#0:
	movl	8(%rsp), %eax
	movl	%esi, 4(%rdi)
	movl	%esi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	decl	%esi
	movl	%esi, 16(%rdi)
	movl	$512, %ecx              # imm = 0x200
	subl	%edx, %ecx
	movl	%ecx, 20(%rdi)
	movl	%r8d, 24(%rdi)
	movl	%r9d, 28(%rdi)
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end1:
	.size	set_ag_params, .Lfunc_end1-set_ag_params
	.cfi_endproc

	.globl	dyn_decomp
	.p2align	4, 0x90
	.type	dyn_decomp,@function
dyn_decomp:                             # @dyn_decomp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r12d
	movl	$-50, %eax
	testq	%rsi, %rsi
	je	.LBB2_69
# BB#1:
	testq	%rdx, %rdx
	je	.LBB2_69
# BB#2:
	testq	%r9, %r9
	je	.LBB2_69
# BB#3:
	movl	8(%rdi), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	12(%rdi), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	16(%rdi), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movl	$0, (%r9)
	movl	16(%rsi), %r14d
	testl	%r12d, %r12d
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	%r14d, 32(%rsp)         # 4-byte Spill
	je	.LBB2_65
# BB#4:                                 # %.lr.ph146
	movq	(%rsi), %r13
	movl	20(%rsi), %eax
	shll	$3, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	$2147483648, %eax       # imm = 0x80000000
	movl	4(%rdi), %r9d
	movl	$32, %ecx
	subl	%r8d, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	leal	9(%r8), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	leaq	2147483647(%rax), %rax
	movq	%rax, %rsi
	movl	%r8d, %ecx
	shlq	%cl, %rsi
	xorq	%rax, %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r14d, %r10d
	movq	%r8, 72(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_49 Depth 2
	cmpl	44(%rsp), %r10d         # 4-byte Folded Reload
	jae	.LBB2_66
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	%r9d, %eax
	shrl	$9, %eax
	addl	$3, %eax
	movl	$3, %esi
	movl	$2147483648, %ebp       # imm = 0x80000000
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_7:                                #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rbp
	jne	.LBB2_15
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=2
	movq	%rbp, %rdi
	shrq	%rdi
	testq	%rax, %rdi
	jne	.LBB2_12
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=2
	movq	%rbp, %rdi
	shrq	$2, %rdi
	testq	%rax, %rdi
	jne	.LBB2_13
# BB#10:                                #   in Loop: Header=BB2_7 Depth=2
	movq	%rbp, %rdi
	shrq	$3, %rdi
	testq	%rax, %rdi
	jne	.LBB2_14
# BB#11:                                #   in Loop: Header=BB2_7 Depth=2
	shrq	$4, %rbp
	addq	$4, %rcx
	leaq	4(%rsi), %rdi
	incq	%rsi
	cmpq	$32, %rsi
	movq	%rdi, %rsi
	jl	.LBB2_7
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$1, %rcx
	jmp	.LBB2_15
.LBB2_13:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$2, %rcx
	jmp	.LBB2_15
.LBB2_14:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB2_15:                               # %lg3a.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$31, %r11d
	subl	%ecx, %r11d
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %r11d
	cmovael	%eax, %r11d
	movl	$1, %esi
	movl	%r11d, %ecx
	shll	%cl, %esi
	decl	%esi
	movl	%r10d, %ecx
	shrl	$3, %ecx
	movzbl	(%r13,%rcx), %edx
	shll	$24, %edx
	movzbl	1(%r13,%rcx), %edi
	shll	$16, %edi
	orl	%edx, %edi
	movzbl	2(%r13,%rcx), %edx
	shll	$8, %edx
	orl	%edi, %edx
	movzbl	3(%r13,%rcx), %ebp
	orl	%edx, %ebp
	movl	%r10d, %ecx
	andb	$7, %cl
	shll	%cl, %ebp
	movl	%ebp, %ecx
	notl	%ecx
	movslq	%ecx, %rcx
	movl	$3, %edx
	movl	$2147483648, %eax       # imm = 0x80000000
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rax
	jne	.LBB2_24
# BB#17:                                #   in Loop: Header=BB2_16 Depth=2
	movq	%rax, %rdi
	shrq	%rdi
	testq	%rcx, %rdi
	jne	.LBB2_21
# BB#18:                                #   in Loop: Header=BB2_16 Depth=2
	movq	%rax, %rdi
	shrq	$2, %rdi
	testq	%rcx, %rdi
	jne	.LBB2_22
# BB#19:                                #   in Loop: Header=BB2_16 Depth=2
	movq	%rax, %rdi
	shrq	$3, %rdi
	testq	%rcx, %rdi
	jne	.LBB2_23
# BB#20:                                #   in Loop: Header=BB2_16 Depth=2
	shrq	$4, %rax
	addq	$4, %rbx
	leaq	4(%rdx), %rdi
	incq	%rdx
	cmpq	$32, %rdx
	movq	%rdi, %rdx
	jl	.LBB2_16
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$1, %rbx
	cmpl	$9, %ebx
	jae	.LBB2_25
	jmp	.LBB2_27
.LBB2_22:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$2, %rbx
	cmpl	$9, %ebx
	jae	.LBB2_25
	jmp	.LBB2_27
.LBB2_23:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB2_24:                               # %lead.exit.i126
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$9, %ebx
	jb	.LBB2_27
.LBB2_25:                               #   in Loop: Header=BB2_5 Depth=1
	leal	9(%r10), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	leal	9(%r10,%rax), %eax
	sarl	$3, %eax
	movzbl	(%r13,%rax), %edx
	shll	$24, %edx
	movzbl	1(%r13,%rax), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movzbl	2(%r13,%rax), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	3(%r13,%rax), %ebp
	orl	%edx, %ebp
	andl	$7, %ecx
	leal	(%rcx,%r8), %edx
	cmpl	$33, %edx
	jl	.LBB2_29
# BB#26:                                #   in Loop: Header=BB2_5 Depth=1
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebp
	addl	$4, %eax
	movzbl	(%r13,%rax), %eax
	movl	$40, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	20(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	orl	%eax, %ebp
	jmp	.LBB2_30
	.p2align	4, 0x90
.LBB2_27:                               #   in Loop: Header=BB2_5 Depth=1
	leal	1(%rbx,%r10), %r14d
	cmpl	$1, %r11d
	jne	.LBB2_31
# BB#28:                                #   in Loop: Header=BB2_5 Depth=1
	movl	%ebx, %esi
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_29:                               #   in Loop: Header=BB2_5 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %ebp
.LBB2_30:                               # %getstreambits.exit.i
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebp, %esi
	andl	80(%rsp), %esi          # 4-byte Folded Reload
	cmpl	$32, %r8d
	cmovel	%ebp, %esi
	addl	36(%rsp), %r10d         # 4-byte Folded Reload
	movl	%r10d, %r14d
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_31:                               #   in Loop: Header=BB2_5 Depth=1
	leal	1(%rbx), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movl	$32, %ecx
	subl	%r11d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	imull	%ebx, %esi
	cmpl	$2, %ebp
	jb	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_5 Depth=1
	addl	%r11d, %r14d
	leal	-1(%rsi,%rbp), %esi
	jmp	.LBB2_34
.LBB2_33:                               #   in Loop: Header=BB2_5 Depth=1
	addl	%r10d, %ebx
	addl	%r11d, %ebx
	movl	%ebx, %r14d
.LBB2_34:                               # %dyn_get_32bit.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB2_35:                               # %dyn_get_32bit.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	leal	(%rsi,%r15), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	negl	%ecx
	orl	$1, %ecx
	leal	1(%rsi,%r15), %edx
	shrl	%edx
	imull	%ecx, %edx
	leaq	4(%rbx), %rdi
	movl	%edx, (%rbx)
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %r10d
	movl	52(%rsp), %edx          # 4-byte Reload
	imull	%edx, %eax
	movl	%r9d, %ecx
	imull	%edx, %ecx
	shrl	$9, %ecx
	subl	%ecx, %r9d
	addl	%eax, %r9d
	cmpl	$65535, %esi            # imm = 0xFFFF
	movl	$65535, %eax            # imm = 0xFFFF
	cmoval	%eax, %r9d
	xorl	%ebp, %ebp
	cmpl	%r12d, %r10d
	jae	.LBB2_44
# BB#36:                                # %dyn_get_32bit.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	leal	(,%r9,4), %eax
	cmpl	$511, %eax              # imm = 0x1FF
	ja	.LBB2_44
# BB#37:                                #   in Loop: Header=BB2_5 Depth=1
	movslq	%r9d, %rcx
	movl	$3, %edx
	movl	$2147483648, %ebp       # imm = 0x80000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rbp
	jne	.LBB2_48
# BB#39:                                #   in Loop: Header=BB2_38 Depth=2
	movq	%rbp, %rsi
	shrq	%rsi
	testq	%rcx, %rsi
	jne	.LBB2_45
# BB#40:                                #   in Loop: Header=BB2_38 Depth=2
	movq	%rbp, %rsi
	shrq	$2, %rsi
	testq	%rcx, %rsi
	jne	.LBB2_46
# BB#41:                                #   in Loop: Header=BB2_38 Depth=2
	movq	%rbp, %rsi
	shrq	$3, %rsi
	testq	%rcx, %rsi
	jne	.LBB2_47
# BB#42:                                #   in Loop: Header=BB2_38 Depth=2
	shrq	$4, %rbp
	addq	$4, %rax
	leaq	4(%rdx), %rsi
	incq	%rdx
	cmpq	$32, %rdx
	movq	%rsi, %rdx
	jl	.LBB2_38
	jmp	.LBB2_48
	.p2align	4, 0x90
.LBB2_44:                               #   in Loop: Header=BB2_5 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB2_64
.LBB2_45:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$1, %rax
	jmp	.LBB2_48
.LBB2_46:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$2, %rax
	jmp	.LBB2_48
.LBB2_47:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB2_48:                               # %lead.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	addl	$16, %r9d
	shrl	$6, %r9d
	leal	-24(%r9,%rax), %r15d
	movl	$1, %r9d
	movl	%r15d, %ecx
	shll	%cl, %r9d
	decl	%r9d
	andl	40(%rsp), %r9d          # 4-byte Folded Reload
	movl	%r14d, %eax
	shrl	$3, %eax
	movzbl	(%r13,%rax), %ecx
	shll	$24, %ecx
	movzbl	1(%r13,%rax), %esi
	shll	$16, %esi
	orl	%ecx, %esi
	movzbl	2(%r13,%rax), %ecx
	shll	$8, %ecx
	orl	%esi, %ecx
	movzbl	3(%r13,%rax), %r11d
	orl	%ecx, %r11d
	movl	%r14d, %ecx
	andb	$7, %cl
	shll	%cl, %r11d
	movl	%r11d, %eax
	notl	%eax
	movslq	%eax, %rcx
	movl	$3, %eax
	movl	$2147483648, %esi       # imm = 0x80000000
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_49:                               #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rsi
	jne	.LBB2_57
# BB#50:                                #   in Loop: Header=BB2_49 Depth=2
	movq	%rsi, %rbp
	shrq	%rbp
	testq	%rcx, %rbp
	jne	.LBB2_54
# BB#51:                                #   in Loop: Header=BB2_49 Depth=2
	movq	%rsi, %rdx
	shrq	$2, %rdx
	testq	%rcx, %rdx
	jne	.LBB2_55
# BB#52:                                #   in Loop: Header=BB2_49 Depth=2
	movq	%rsi, %rdx
	shrq	$3, %rdx
	testq	%rcx, %rdx
	jne	.LBB2_56
# BB#53:                                #   in Loop: Header=BB2_49 Depth=2
	shrq	$4, %rsi
	addq	$4, %rbx
	leaq	4(%rax), %rdx
	incq	%rax
	cmpq	$32, %rax
	movq	%rdx, %rax
	jl	.LBB2_49
	jmp	.LBB2_57
.LBB2_54:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$1, %rbx
	jmp	.LBB2_57
.LBB2_55:                               #   in Loop: Header=BB2_5 Depth=1
	orq	$2, %rbx
	jmp	.LBB2_57
.LBB2_56:                               #   in Loop: Header=BB2_5 Depth=1
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB2_57:                               # %lead.exit.i
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	%r12d, %ebp
	cmpl	$9, %ebx
	jb	.LBB2_59
# BB#58:                                #   in Loop: Header=BB2_5 Depth=1
	shrl	$7, %r11d
	movzwl	%r11w, %r12d
	addl	$25, %r14d
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_59:                               #   in Loop: Header=BB2_5 Depth=1
	leal	1(%rbx), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r11d
	movl	$32, %ecx
	subl	%r15d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r11d
	imull	%ebx, %r9d
	leal	-1(%r9,%r11), %r12d
	cmpl	$2, %r11d
	cmovbl	%r9d, %r12d
	xorl	%eax, %eax
	cmpl	$1, %r11d
	seta	%al
	addl	%r15d, %r14d
	addl	%ebx, %r14d
	addl	%eax, %r14d
.LBB2_60:                               # %dyn_get.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	leal	(%r12,%r10), %eax
	cmpl	%ebp, %eax
	ja	.LBB2_67
# BB#61:                                # %.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	testl	%r12d, %r12d
	je	.LBB2_63
# BB#62:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	leal	-1(%r12), %ebx
	leaq	4(,%rbx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax,%rbx,4), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	1(%r12,%rax), %r10d
.LBB2_63:                               # %._crit_edge
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$65535, %r12d           # imm = 0xFFFF
	sbbl	%r15d, %r15d
	andl	$1, %r15d
	xorl	%r9d, %r9d
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
.LBB2_64:                               # %.backedge
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	%r12d, %r10d
	movq	%rdi, %rdx
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movl	%r14d, %r10d
	jb	.LBB2_5
	jmp	.LBB2_68
.LBB2_65:
	xorl	%ebp, %ebp
	jmp	.LBB2_68
.LBB2_66:
	movl	%r10d, %r14d
.LBB2_67:
	movl	$-50, %ebp
.LBB2_68:                               # %dyn_get.exit._crit_edge
	subl	32(%rsp), %r14d         # 4-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%r14d, (%rax)
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	BitBufferAdvance
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	movl	$-50, %eax
	cmovbel	%ebp, %eax
.LBB2_69:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	dyn_decomp, .Lfunc_end2-dyn_decomp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
