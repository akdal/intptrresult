	.text
	.file	"queens.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	101                     # 0x65
	.long	101                     # 0x65
	.long	101                     # 0x65
	.long	101                     # 0x65
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	(%r14), %rax
	movq	%rax, progname(%rip)
	movl	$0, printing(%rip)
	movl	$14, queens(%rip)
	movl	$1, findall(%rip)
	cmpl	$2, %edi
	jl	.LBB0_1
# BB#7:                                 # %.lr.ph36.preheader
	movslq	%edi, %r15
	movl	$14, %esi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
	movq	(%r14,%rbx,8), %r12
	cmpb	$45, (%r12)
	jne	.LBB0_16
# BB#9:                                 # %.preheader25
                                        #   in Loop: Header=BB0_8 Depth=1
	movb	1(%r12), %cl
	testb	%cl, %cl
	je	.LBB0_21
# BB#10:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	leaq	2(%r12), %rax
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	%cl, %ecx
	cmpl	$97, %ecx
	je	.LBB0_14
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB0_11 Depth=2
	cmpl	$99, %ecx
	jne	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_11 Depth=2
	movl	$0, printing(%rip)
.LBB0_14:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$1, findall(%rip)
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_11
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.2, %esi
	movl	$queens, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB0_27
# BB#17:                                #   in Loop: Header=BB0_8 Depth=1
	movl	queens(%rip), %esi
	testl	%esi, %esi
	jle	.LBB0_18
# BB#20:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	$101, %esi
	jge	.LBB0_28
.LBB0_21:                               # %.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB0_8
# BB#22:                                # %._crit_edge
	testl	%esi, %esi
	jne	.LBB0_2
# BB#23:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_24
.LBB0_15:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r12, %rcx
	callq	fprintf
.LBB0_24:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str, %esi
.LBB0_19:
	xorl	%eax, %eax
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.LBB0_27:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r12, %rcx
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.LBB0_18:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.4, %esi
	jmp	.LBB0_19
.LBB0_28:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.5, %esi
	movl	$100, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.LBB0_1:
	movl	$14, %esi
.LBB0_2:                                # %.preheader
	movl	%esi, files(%rip)
	movl	%esi, ranks(%rip)
	cmpl	$1, %esi
	movl	$.L.str.8, %eax
	movl	$.L.str.9, %edx
	cmovgq	%rax, %rdx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%esi, %ecx
	movl	%esi, %r8d
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	$0, solutions(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [101,101,101,101]
	movaps	%xmm0, file(%rip)
	movaps	%xmm0, file+16(%rip)
	movaps	%xmm0, file+32(%rip)
	movaps	%xmm0, file+48(%rip)
	movaps	%xmm0, file+64(%rip)
	movaps	%xmm0, file+80(%rip)
	movaps	%xmm0, file+96(%rip)
	movaps	%xmm0, file+112(%rip)
	movaps	%xmm0, file+128(%rip)
	movaps	%xmm0, file+144(%rip)
	movaps	%xmm0, file+160(%rip)
	movaps	%xmm0, file+176(%rip)
	movaps	%xmm0, file+192(%rip)
	movaps	%xmm0, file+208(%rip)
	movaps	%xmm0, file+224(%rip)
	movaps	%xmm0, file+240(%rip)
	movaps	%xmm0, file+256(%rip)
	movaps	%xmm0, file+272(%rip)
	movaps	%xmm0, file+288(%rip)
	movaps	%xmm0, file+304(%rip)
	movaps	%xmm0, file+320(%rip)
	movaps	%xmm0, file+336(%rip)
	movaps	%xmm0, file+352(%rip)
	movaps	%xmm0, file+368(%rip)
	movaps	%xmm0, file+384(%rip)
	movaps	%xmm0, bakdiag(%rip)
	movaps	%xmm0, bakdiag+16(%rip)
	movaps	%xmm0, fordiag(%rip)
	movaps	%xmm0, fordiag+16(%rip)
	movaps	%xmm0, bakdiag+32(%rip)
	movaps	%xmm0, bakdiag+48(%rip)
	movaps	%xmm0, fordiag+32(%rip)
	movaps	%xmm0, fordiag+48(%rip)
	movaps	%xmm0, bakdiag+64(%rip)
	movaps	%xmm0, bakdiag+80(%rip)
	movaps	%xmm0, fordiag+64(%rip)
	movaps	%xmm0, fordiag+80(%rip)
	movaps	%xmm0, bakdiag+96(%rip)
	movaps	%xmm0, bakdiag+112(%rip)
	movaps	%xmm0, fordiag+96(%rip)
	movaps	%xmm0, fordiag+112(%rip)
	movaps	%xmm0, bakdiag+128(%rip)
	movaps	%xmm0, bakdiag+144(%rip)
	movaps	%xmm0, fordiag+128(%rip)
	movaps	%xmm0, fordiag+144(%rip)
	movaps	%xmm0, bakdiag+160(%rip)
	movaps	%xmm0, bakdiag+176(%rip)
	movaps	%xmm0, fordiag+160(%rip)
	movaps	%xmm0, fordiag+176(%rip)
	movaps	%xmm0, bakdiag+192(%rip)
	movaps	%xmm0, bakdiag+208(%rip)
	movaps	%xmm0, fordiag+192(%rip)
	movaps	%xmm0, fordiag+208(%rip)
	movaps	%xmm0, bakdiag+224(%rip)
	movaps	%xmm0, bakdiag+240(%rip)
	movaps	%xmm0, fordiag+224(%rip)
	movaps	%xmm0, fordiag+240(%rip)
	movaps	%xmm0, bakdiag+256(%rip)
	movaps	%xmm0, bakdiag+272(%rip)
	movaps	%xmm0, fordiag+256(%rip)
	movaps	%xmm0, fordiag+272(%rip)
	movaps	%xmm0, bakdiag+288(%rip)
	movaps	%xmm0, bakdiag+304(%rip)
	movaps	%xmm0, fordiag+288(%rip)
	movaps	%xmm0, fordiag+304(%rip)
	movaps	%xmm0, bakdiag+320(%rip)
	movaps	%xmm0, bakdiag+336(%rip)
	movaps	%xmm0, fordiag+320(%rip)
	movaps	%xmm0, fordiag+336(%rip)
	movaps	%xmm0, bakdiag+352(%rip)
	movaps	%xmm0, bakdiag+368(%rip)
	movaps	%xmm0, fordiag+352(%rip)
	movaps	%xmm0, fordiag+368(%rip)
	movaps	%xmm0, bakdiag+384(%rip)
	movaps	%xmm0, bakdiag+400(%rip)
	movaps	%xmm0, fordiag+384(%rip)
	movaps	%xmm0, fordiag+400(%rip)
	movaps	%xmm0, bakdiag+416(%rip)
	movaps	%xmm0, bakdiag+432(%rip)
	movaps	%xmm0, fordiag+416(%rip)
	movaps	%xmm0, fordiag+432(%rip)
	movaps	%xmm0, bakdiag+448(%rip)
	movaps	%xmm0, bakdiag+464(%rip)
	movaps	%xmm0, fordiag+448(%rip)
	movaps	%xmm0, fordiag+464(%rip)
	movaps	%xmm0, bakdiag+480(%rip)
	movaps	%xmm0, bakdiag+496(%rip)
	movaps	%xmm0, fordiag+480(%rip)
	movaps	%xmm0, fordiag+496(%rip)
	movaps	%xmm0, bakdiag+512(%rip)
	movaps	%xmm0, bakdiag+528(%rip)
	movaps	%xmm0, fordiag+512(%rip)
	movaps	%xmm0, fordiag+528(%rip)
	movaps	%xmm0, bakdiag+544(%rip)
	movaps	%xmm0, bakdiag+560(%rip)
	movaps	%xmm0, fordiag+544(%rip)
	movaps	%xmm0, fordiag+560(%rip)
	movaps	%xmm0, bakdiag+576(%rip)
	movaps	%xmm0, bakdiag+592(%rip)
	movaps	%xmm0, fordiag+576(%rip)
	movaps	%xmm0, fordiag+592(%rip)
	movaps	%xmm0, bakdiag+608(%rip)
	movaps	%xmm0, bakdiag+624(%rip)
	movaps	%xmm0, fordiag+608(%rip)
	movaps	%xmm0, fordiag+624(%rip)
	movaps	%xmm0, bakdiag+640(%rip)
	movaps	%xmm0, bakdiag+656(%rip)
	movaps	%xmm0, fordiag+640(%rip)
	movaps	%xmm0, fordiag+656(%rip)
	movaps	%xmm0, bakdiag+672(%rip)
	movaps	%xmm0, bakdiag+688(%rip)
	movaps	%xmm0, fordiag+672(%rip)
	movaps	%xmm0, fordiag+688(%rip)
	movaps	%xmm0, bakdiag+704(%rip)
	movaps	%xmm0, bakdiag+720(%rip)
	movaps	%xmm0, fordiag+704(%rip)
	movaps	%xmm0, fordiag+720(%rip)
	movaps	%xmm0, bakdiag+736(%rip)
	movaps	%xmm0, bakdiag+752(%rip)
	movaps	%xmm0, fordiag+736(%rip)
	movaps	%xmm0, fordiag+752(%rip)
	movl	$101, bakdiag+768(%rip)
	movl	$101, fordiag+768(%rip)
	movl	$101, bakdiag+772(%rip)
	movl	$101, fordiag+772(%rip)
	movl	$101, bakdiag+776(%rip)
	movl	$101, fordiag+776(%rip)
	movl	$101, bakdiag+780(%rip)
	movl	$101, fordiag+780(%rip)
	movl	$101, bakdiag+784(%rip)
	movl	$101, fordiag+784(%rip)
	movl	$101, bakdiag+788(%rip)
	movl	$101, fordiag+788(%rip)
	movl	$101, bakdiag+792(%rip)
	movl	$101, fordiag+792(%rip)
	xorl	%edi, %edi
	callq	find
	movq	solutions(%rip), %rsi
	cmpl	$0, printing(%rip)
	je	.LBB0_5
# BB#3:                                 # %.preheader
	testq	%rsi, %rsi
	je	.LBB0_5
# BB#4:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	solutions(%rip), %rsi
.LBB0_5:
	cmpq	$1, %rsi
	jne	.LBB0_25
# BB#6:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.LBB0_25:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	find
	.p2align	4, 0x90
	.type	find,@function
find:                                   # @find
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edi, %r13d
	cmpl	%r13d, queens(%rip)
	jne	.LBB1_5
# BB#1:
	incq	solutions(%rip)
	cmpl	$0, printing(%rip)
	je	.LBB1_3
# BB#2:
	callq	pboard
.LBB1_3:
	cmpl	$0, findall(%rip)
	jne	.LBB1_12
# BB#4:
	xorl	%edi, %edi
	callq	exit
.LBB1_5:
	movl	files(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_12
# BB#6:                                 # %.lr.ph
	movslq	%r13d, %r15
	leal	-1(%r13,%rax), %ecx
	movslq	%ecx, %rcx
	leaq	bakdiag(,%rcx,4), %rbp
	leal	1(%r15), %r14d
	leaq	fordiag(,%r15,4), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	cmpl	%r13d, file(,%rbx,4)
	jl	.LBB1_11
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	cmpl	%r13d, (%r12,%rbx,4)
	jl	.LBB1_11
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=1
	cmpl	%r13d, (%rbp)
	jl	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_7 Depth=1
	movl	%ebx, queen(,%r15,4)
	movl	%r13d, (%rbp)
	movl	%r13d, (%r12,%rbx,4)
	movl	%r13d, file(,%rbx,4)
	movl	%r14d, %edi
	callq	find
	movl	$101, (%rbp)
	movl	$101, (%r12,%rbx,4)
	movl	$101, file(,%rbx,4)
	movl	files(%rip), %eax
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=1
	addq	$-4, %rbp
	incq	%rbx
	cmpl	%eax, %ebx
	jl	.LBB1_7
.LBB1_12:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	find, .Lfunc_end1-find
	.cfi_endproc

	.globl	pboard
	.p2align	4, 0x90
	.type	pboard,@function
pboard:                                 # @pboard
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	cmpl	$0, findall(%rip)
	je	.LBB2_2
# BB#1:
	movq	solutions(%rip), %rsi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_2:                                # %.preheader10
	cmpl	$0, ranks(%rip)
	jle	.LBB2_11
# BB#3:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
	movq	stdout(%rip), %rsi
	cmpl	$0, files(%rip)
	jle	.LBB2_10
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_4 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_6:                                #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$32, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	cmpl	queen(,%rbx,4), %ebp
	jne	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_6 Depth=2
	movl	$81, %edi
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_6 Depth=2
	movl	$45, %edi
.LBB2_9:                                #   in Loop: Header=BB2_6 Depth=2
	callq	_IO_putc
	incl	%ebp
	movq	stdout(%rip), %rsi
	cmpl	files(%rip), %ebp
	jl	.LBB2_6
.LBB2_10:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	$10, %edi
	callq	_IO_putc
	incq	%rbx
	movslq	ranks(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_4
.LBB2_11:                               # %._crit_edge13
	movq	stdout(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fflush                  # TAILCALL
.Lfunc_end2:
	.size	pboard, .Lfunc_end2-pboard
	.cfi_endproc

	.type	printing,@object        # @printing
	.data
	.globl	printing
	.p2align	2
printing:
	.long	1                       # 0x1
	.size	printing, 4

	.type	findall,@object         # @findall
	.bss
	.globl	findall
	.p2align	2
findall:
	.long	0                       # 0x0
	.size	findall, 4

	.type	solutions,@object       # @solutions
	.globl	solutions
	.p2align	3
solutions:
	.quad	0                       # 0x0
	.size	solutions, 8

	.type	progname,@object        # @progname
	.globl	progname
	.p2align	3
progname:
	.quad	0
	.size	progname, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Usage:  %s [-ac] n\n\tn\tNumber of queens (rows and columns). An integer from 1 to 100.\n\t-a\tFind and print all solutions.\n\t-c\tCount all solutions, but do not print them.\n"
	.size	.L.str, 168

	.type	queens,@object          # @queens
	.comm	queens,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s: Illegal option '%s'\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: Non-integer argument '%s'\n"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s: n must be positive integer\n"
	.size	.L.str.4, 32

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s: Can't have more than %d queens\n"
	.size	.L.str.5, 36

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s: Missing n argument\n"
	.size	.L.str.6, 24

	.type	files,@object           # @files
	.comm	files,4,4
	.type	ranks,@object           # @ranks
	.comm	ranks,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d queen%s on a %dx%d board...\n"
	.size	.L.str.7, 32

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"s"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.zero	1
	.size	.L.str.9, 1

	.type	file,@object            # @file
	.comm	file,400,16
	.type	bakdiag,@object         # @bakdiag
	.comm	bakdiag,796,16
	.type	fordiag,@object         # @fordiag
	.comm	fordiag,796,16
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"...there are %ld solutions\n"
	.size	.L.str.11, 28

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nSolution #%lu:\n"
	.size	.L.str.12, 17

	.type	queen,@object           # @queen
	.comm	queen,400,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"...there is 1 solution"
	.size	.Lstr, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
