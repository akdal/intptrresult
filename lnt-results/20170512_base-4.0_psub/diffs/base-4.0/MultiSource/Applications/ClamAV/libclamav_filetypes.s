	.text
	.file	"libclamav_filetypes.bc"
	.globl	cli_filetype
	.p2align	4, 0x90
	.type	cli_filetype,@function
cli_filetype:                           # @cli_filetype
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$.L.str.12, %esi
	movq	$-2520, %rbx            # imm = 0xF628
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	cli_magic+2520(%rbx), %rdi
	movq	cli_magic+2536(%rbx), %rdx
	leaq	(%rdx,%rdi), %rax
	cmpq	%r15, %rax
	ja	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	%r14, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_3
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movq	cli_magic+2568(%rbx), %rsi
	addq	$40, %rbx
	jne	.LBB0_1
# BB#5:
	movl	$500, %eax              # imm = 0x1F4
	jmp	.LBB0_6
.LBB0_3:
	movq	cli_magic+2544(%rbx), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	cli_magic+2552(%rbx), %eax
.LBB0_6:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	cli_filetype, .Lfunc_end0-cli_filetype
	.cfi_endproc

	.globl	cli_filetype2
	.p2align	4, 0x90
	.type	cli_filetype2,@function
cli_filetype2:                          # @cli_filetype2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$504, %rsp              # imm = 0x1F8
.Lcfi12:
	.cfi_def_cfa_offset 560
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	leaq	64(%rsp), %rbx
	xorl	%esi, %esi
	movl	$257, %edx              # imm = 0x101
	movq	%rbx, %rdi
	callq	memset
	movl	$256, %edx              # imm = 0x100
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	read
	movq	%rax, %r13
	testl	%r13d, %r13d
	jle	.LBB1_1
# BB#2:
	movl	$.L.str.12, %esi
	movslq	%r13d, %rbx
	movq	$-2520, %rbp            # imm = 0xF628
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	cli_magic+2520(%rbp), %rax
	movq	cli_magic+2536(%rbp), %rdx
	leaq	(%rdx,%rax), %rcx
	cmpq	%rbx, %rcx
	ja	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	leaq	64(%rsp,%rax), %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB1_5
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=1
	movq	cli_magic+2568(%rbp), %rsi
	addq	$40, %rbp
	jne	.LBB1_3
# BB#7:
	movl	$500, %ebp              # imm = 0x1F4
	testq	%r14, %r14
	jne	.LBB1_9
	jmp	.LBB1_28
.LBB1_1:
	movl	$501, %r12d             # imm = 0x1F5
	jmp	.LBB1_29
.LBB1_5:
	movq	cli_magic+2544(%rbp), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	cli_magic+2552(%rbp), %ebp
	testq	%r14, %r14
	je	.LBB1_28
.LBB1_9:                                # %cli_filetype.exit
	cmpl	$500, %ebp              # imm = 0x1F4
	jne	.LBB1_28
# BB#10:
	movq	16(%r14), %rax
	movq	(%rax), %rbp
	movl	$500, %r12d             # imm = 0x1F4
	testq	%rbp, %rbp
	je	.LBB1_43
# BB#11:
	movl	64(%rbp), %esi
	leaq	16(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	testl	%eax, %eax
	jne	.LBB1_43
# BB#12:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	16(%r14), %rax
	movq	(%rax), %rcx
	leaq	64(%rsp), %rdi
	leaq	16(%rsp), %r8
	movl	$0, %edx
	movl	$1, %r9d
	movl	%r13d, %esi
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebp
	leaq	16(%rsp), %rdi
	callq	cli_ac_freedata
	cmpl	$499, %ebp              # imm = 0x1F3
	jg	.LBB1_28
# BB#13:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	64(%rax), %esi
	leaq	16(%rsp), %rdi
	movl	$8, %edx
	callq	cli_ac_initdata
	testl	%eax, %eax
	jne	.LBB1_43
# BB#14:
	leaq	64(%rsp), %rdi
	movl	%r13d, %esi
	callq	cli_utf16toascii
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_15
# BB#16:
	movq	%rbp, %rdi
	callq	strlen
	movq	16(%r14), %rcx
	movq	(%rcx), %rcx
	leaq	16(%rsp), %r8
	movl	$0, %edx
	movl	$1, %r9d
	movq	%rbp, %rdi
	movl	%eax, %esi
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %r12d
	movq	%rbp, %rdi
	callq	free
	cmpl	$528, %r12d             # imm = 0x210
	movl	$526, %eax              # imm = 0x20E
	movl	$500, %r12d             # imm = 0x1F4
	cmovel	%eax, %r12d
	jmp	.LBB1_17
.LBB1_15:
	movl	$500, %r12d             # imm = 0x1F4
.LBB1_17:
	leaq	16(%rsp), %rdi
	callq	cli_ac_freedata
	cmpl	$526, %r12d             # imm = 0x20E
	je	.LBB1_27
# BB#18:
	movq	80(%r14), %rax
	movl	24(%rax), %eax
	andl	$2, %eax
	je	.LBB1_27
# BB#19:
	leal	(%r13,%r13), %eax
	cmpl	$255, %eax
	movl	$256, %edx              # imm = 0x100
	cmovgl	%eax, %edx
	leaq	328(%rsp), %rdi
	movl	$.L.str.1, %esi
	callq	init_entity_converter
	testl	%eax, %eax
	je	.LBB1_20
# BB#26:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB1_27
.LBB1_20:
	leaq	64(%rsp), %rax
	movq	%rax, 40(%rsp)
	movq	%rbx, 48(%rsp)
	movq	$0, 56(%rsp)
	leaq	16(%rsp), %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movl	64(%rax), %esi
	movl	$8, %edx
	movq	%rbp, %rdi
	callq	cli_ac_initdata
	testl	%eax, %eax
	jne	.LBB1_43
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	xorl	%esi, %esi
	leaq	328(%rsp), %rdi
	leaq	40(%rsp), %rdx
	movq	%rbx, %rcx
	callq	encoding_norm_readline
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_23
# BB#24:                                #   in Loop: Header=BB1_21 Depth=1
	movq	%r13, %rdi
	callq	strlen
	movq	16(%r14), %rcx
	movq	(%rcx), %rcx
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r13, %rdi
	movl	%eax, %esi
	movq	%rbp, %r8
	pushq	$0
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -32
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	%r13, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	cli_ac_freedata
	movl	36(%rsp), %eax          # 4-byte Reload
	cmpl	$528, %eax              # imm = 0x210
	cmovel	%eax, %r12d
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB1_21
	jmp	.LBB1_25
.LBB1_23:                               # %.thread104
	leaq	16(%rsp), %rdi
	callq	cli_ac_freedata
.LBB1_25:                               # %.thread
	leaq	328(%rsp), %rdi
	callq	entity_norm_done
.LBB1_27:
	movl	%r12d, %ebp
.LBB1_28:
	movl	%ebp, %eax
	orl	$1, %eax
	cmpl	$501, %eax              # imm = 0x1F5
	movl	%ebp, %r12d
	jne	.LBB1_43
.LBB1_29:                               # %.thread94
	movl	$37639, %edi            # imm = 0x9307
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_43
# BB#30:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	movl	$37638, %edx            # imm = 0x9306
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	read
	testl	%eax, %eax
	jle	.LBB1_35
# BB#31:
	movslq	%eax, %rcx
	movb	$0, (%rbx,%rcx)
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	is_tar
	cmpl	$2, %eax
	je	.LBB1_34
# BB#32:
	cmpl	$1, %eax
	jne	.LBB1_35
# BB#33:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$506, %r12d             # imm = 0x1FA
	jmp	.LBB1_42
.LBB1_35:
	movl	%r12d, %eax
	orl	$1, %eax
	cmpl	$501, %eax              # imm = 0x1F5
	jne	.LBB1_42
# BB#36:
	leaq	32769(%rbx), %rdi
	movl	$.L.str.5, %esi
	movl	$5, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB1_38
# BB#37:
	leaq	37633(%rbx), %rdi
	movl	$.L.str.5, %esi
	movl	$5, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB1_38
# BB#39:
	movq	%rbx, %rdi
	addq	$32776, %rdi            # imm = 0x8008
	movl	$.L.str.7, %esi
	movl	$5, %edx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB1_42
# BB#40:
	movl	$.L.str.8, %edi
	jmp	.LBB1_41
.LBB1_34:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$505, %r12d             # imm = 0x1F9
	jmp	.LBB1_42
.LBB1_38:
	movl	$.L.str.6, %edi
.LBB1_41:                               # %.thread97
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$504, %r12d             # imm = 0x1F8
.LBB1_42:                               # %.thread97
	movq	%rbx, %rdi
	callq	free
.LBB1_43:
	movl	%r12d, %eax
	addq	$504, %rsp              # imm = 0x1F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_filetype2, .Lfunc_end1-cli_filetype2
	.cfi_endproc

	.globl	cli_addtypesigs
	.p2align	4, 0x90
	.type	cli_addtypesigs,@function
cli_addtypesigs:                        # @cli_addtypesigs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 48
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	16(%rbp), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB2_1
.LBB2_7:
	movq	$-1032, %rbp            # imm = 0xFBF8
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movq	cli_smagic+1032(%rbp), %rdx
	movq	cli_smagic+1040(%rbp), %rbx
	movzwl	cli_smagic+1048(%rbp), %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	cli_parse_add
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB2_9
# BB#5:                                 #   in Loop: Header=BB2_8 Depth=1
	addq	$24, %rbp
	jne	.LBB2_8
# BB#6:
	xorl	%r15d, %r15d
	jmp	.LBB2_10
.LBB2_1:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %edi
	movl	$80, %esi
	callq	cli_calloc
	movq	%rax, %r14
	movq	16(%rbp), %rax
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.LBB2_2
# BB#3:
	movzbl	cli_ac_maxdepth(%rip), %edx
	movzbl	cli_ac_mindepth(%rip), %esi
	movq	%r14, %rdi
	callq	cli_ac_init
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB2_7
# BB#4:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB2_10
.LBB2_9:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB2_10
.LBB2_2:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %r15d
.LBB2_10:                               # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cli_addtypesigs, .Lfunc_end2-cli_addtypesigs
	.cfi_endproc

	.type	cli_magic,@object       # @cli_magic
	.section	.rodata,"a",@progbits
	.p2align	4
cli_magic:
	.quad	0                       # 0x0
	.quad	.L.str.12
	.quad	2                       # 0x2
	.quad	.L.str.13
	.long	502                     # 0x1f6
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.14
	.quad	4                       # 0x4
	.quad	.L.str.15
	.long	503                     # 0x1f7
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.16
	.quad	4                       # 0x4
	.quad	.L.str.17
	.long	510                     # 0x1fe
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.18
	.quad	4                       # 0x4
	.quad	.L.str.19
	.long	508                     # 0x1fc
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.20
	.quad	8                       # 0x8
	.quad	.L.str.19
	.long	508                     # 0x1fc
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.21
	.quad	2                       # 0x2
	.quad	.L.str.22
	.long	507                     # 0x1fb
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.23
	.quad	3                       # 0x3
	.quad	.L.str.24
	.long	509                     # 0x1fd
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.25
	.quad	2                       # 0x2
	.quad	.L.str.26
	.long	511                     # 0x1ff
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.27
	.quad	4                       # 0x4
	.quad	.L.str.28
	.long	512                     # 0x200
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.29
	.quad	4                       # 0x4
	.quad	.L.str.30
	.long	514                     # 0x202
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.31
	.quad	4                       # 0x4
	.quad	.L.str.32
	.long	515                     # 0x203
	.zero	4
	.quad	8                       # 0x8
	.quad	.L.str.33
	.quad	4                       # 0x4
	.quad	.L.str.34
	.long	516                     # 0x204
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.35
	.quad	4                       # 0x4
	.quad	.L.str.36
	.long	517                     # 0x205
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.37
	.quad	45                      # 0x2d
	.quad	.L.str.38
	.long	520                     # 0x208
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.39
	.quad	5                       # 0x5
	.quad	.L.str.40
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.41
	.quad	10                      # 0xa
	.quad	.L.str.42
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.43
	.quad	13                      # 0xd
	.quad	.L.str.44
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.45
	.quad	13                      # 0xd
	.quad	.L.str.44
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.46
	.quad	14                      # 0xe
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.48
	.quad	8                       # 0x8
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.49
	.quad	17                      # 0x11
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.50
	.quad	17                      # 0x11
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.51
	.quad	15                      # 0xf
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.52
	.quad	11                      # 0xb
	.quad	.L.str.53
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.54
	.quad	5                       # 0x5
	.quad	.L.str.55
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.56
	.quad	11                      # 0xb
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.57
	.quad	9                       # 0x9
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.58
	.quad	6                       # 0x6
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.59
	.quad	6                       # 0x6
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.60
	.quad	12                      # 0xc
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.61
	.quad	12                      # 0xc
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.62
	.quad	13                      # 0xd
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.63
	.quad	15                      # 0xf
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.64
	.quad	4                       # 0x4
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.65
	.quad	9                       # 0x9
	.quad	.L.str.47
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.66
	.quad	5                       # 0x5
	.quad	.L.str.67
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.68
	.quad	6                       # 0x6
	.quad	.L.str.69
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.70
	.quad	14                      # 0xe
	.quad	.L.str.71
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.72
	.quad	13                      # 0xd
	.quad	.L.str.73
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.74
	.quad	26                      # 0x1a
	.quad	.L.str.75
	.long	529                     # 0x211
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.76
	.quad	4                       # 0x4
	.quad	.L.str.77
	.long	521                     # 0x209
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.78
	.quad	6                       # 0x6
	.quad	.L.str.79
	.long	524                     # 0x20c
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.80
	.quad	4                       # 0x4
	.quad	.L.str.81
	.long	525                     # 0x20d
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.82
	.quad	3                       # 0x3
	.quad	.L.str.82
	.long	518                     # 0x206
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.83
	.quad	2                       # 0x2
	.quad	.L.str.84
	.long	518                     # 0x206
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.85
	.quad	3                       # 0x3
	.quad	.L.str.86
	.long	518                     # 0x206
	.zero	4
	.quad	6                       # 0x6
	.quad	.L.str.87
	.quad	4                       # 0x4
	.quad	.L.str.86
	.long	518                     # 0x206
	.zero	4
	.quad	6                       # 0x6
	.quad	.L.str.88
	.quad	4                       # 0x4
	.quad	.L.str.86
	.long	518                     # 0x206
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.89
	.quad	4                       # 0x4
	.quad	.L.str.90
	.long	518                     # 0x206
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.91
	.quad	4                       # 0x4
	.quad	.L.str.91
	.long	519                     # 0x207
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.92
	.quad	4                       # 0x4
	.quad	.L.str.92
	.long	519                     # 0x207
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.93
	.quad	8                       # 0x8
	.quad	.L.str.94
	.long	513                     # 0x201
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.95
	.quad	5                       # 0x5
	.quad	.L.str.96
	.long	523                     # 0x20b
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.97
	.quad	8                       # 0x8
	.quad	.L.str.98
	.long	522                     # 0x20a
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.99
	.quad	5                       # 0x5
	.quad	.L.str.100
	.long	527                     # 0x20f
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.101
	.quad	4                       # 0x4
	.quad	.L.str.102
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.103
	.quad	4                       # 0x4
	.quad	.L.str.104
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.105
	.quad	4                       # 0x4
	.quad	.L.str.106
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.107
	.quad	3                       # 0x3
	.quad	.L.str.108
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.109
	.quad	3                       # 0x3
	.quad	.L.str.108
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.110
	.quad	11                      # 0xb
	.quad	.L.str.111
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.112
	.quad	7                       # 0x7
	.quad	.L.str.113
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	.L.str.114
	.quad	4                       # 0x4
	.quad	.L.str.115
	.long	504                     # 0x1f8
	.zero	4
	.quad	0                       # 0x0
	.quad	0
	.quad	0                       # 0x0
	.quad	0
	.long	501                     # 0x1f5
	.zero	4
	.size	cli_magic, 2560

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Recognized %s file\n"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata,"a",@progbits
.L.str.1:
	.zero	2
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"cli_filetype2: Error initializing entity converter\n"
	.size	.L.str.2, 52

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Recognized old fashioned tar file\n"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Recognized POSIX tar file\n"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"CD001"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Recognized ISO 9660 CD-ROM data\n"
	.size	.L.str.6, 33

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"CDROM"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Recognized High Sierra CD-ROM data\n"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_addtypesigs: Need to allocate AC trie in engine->root[0]\n"
	.size	.L.str.9, 62

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_addtypesigs: Can't initialise AC pattern matcher\n"
	.size	.L.str.10, 54

	.type	cli_smagic,@object      # @cli_smagic
	.section	.rodata,"a",@progbits
	.p2align	4
cli_smagic:
	.quad	.L.str.116
	.quad	.L.str.117
	.long	529                     # 0x211
	.zero	4
	.quad	.L.str.118
	.quad	.L.str.117
	.long	529                     # 0x211
	.zero	4
	.quad	.L.str.119
	.quad	.L.str.117
	.long	529                     # 0x211
	.zero	4
	.quad	.L.str.120
	.quad	.L.str.117
	.long	529                     # 0x211
	.zero	4
	.quad	.L.str.121
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.123
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.124
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.125
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.126
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.127
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.128
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.129
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.130
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.131
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.132
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.133
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.134
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.135
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.136
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.137
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.138
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.139
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.140
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.141
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.142
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.143
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.144
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.145
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.146
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.147
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.148
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.149
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.150
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.151
	.quad	.L.str.122
	.long	528                     # 0x210
	.zero	4
	.quad	.L.str.152
	.quad	.L.str.153
	.long	532                     # 0x214
	.zero	4
	.quad	.L.str.154
	.quad	.L.str.155
	.long	531                     # 0x213
	.zero	4
	.quad	.L.str.156
	.quad	.L.str.157
	.long	533                     # 0x215
	.zero	4
	.quad	.L.str.158
	.quad	.L.str.159
	.long	534                     # 0x216
	.zero	4
	.quad	.L.str.160
	.quad	.L.str.159
	.long	534                     # 0x216
	.zero	4
	.quad	.L.str.161
	.quad	.L.str.159
	.long	534                     # 0x216
	.zero	4
	.quad	.L.str.162
	.quad	.L.str.163
	.long	535                     # 0x217
	.zero	4
	.quad	.L.str.164
	.quad	.L.str.165
	.long	536                     # 0x218
	.zero	4
	.quad	.L.str.166
	.quad	.L.str.167
	.long	502                     # 0x1f6
	.zero	4
	.quad	0
	.quad	0
	.long	501                     # 0x1f5
	.zero	4
	.size	cli_smagic, 1056

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"cli_addtypesigs: Problem adding signature for %s\n"
	.size	.L.str.11, 50

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"MZ"
	.size	.L.str.12, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DOS/W32 executable/library/driver"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\177ELF"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"ELF"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Rar!"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"RAR"
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"PK\003\004"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ZIP"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"PK00PK\003\004"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\037\213"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"GZip"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"BZh"
	.size	.L.str.23, 4

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"BZip"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"`\352"
	.size	.L.str.25, 3

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ARJ"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SZDD"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"compress.exe'd"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"MSCF"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"MS CAB"
	.size	.L.str.30, 7

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"ITSF"
	.size	.L.str.31, 5

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"MS CHM"
	.size	.L.str.32, 7

	.type	.L.str.33,@object       # @.str.33
	.section	.rodata,"a",@progbits
.L.str.33:
	.asciz	"\031\004\000\020"
	.size	.L.str.33, 5

	.type	.L.str.34,@object       # @.str.34
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.34:
	.asciz	"SIS"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"#@~^"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SCRENC"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"(This file must be converted with BinHex 4.0)"
	.size	.L.str.37, 46

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"BinHex"
	.size	.L.str.38, 7

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"From "
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"MBox"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Received: "
	.size	.L.str.41, 11

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Raw mail"
	.size	.L.str.42, 9

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Return-Path: "
	.size	.L.str.43, 14

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Maildir"
	.size	.L.str.44, 8

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Return-path: "
	.size	.L.str.45, 14

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Delivered-To: "
	.size	.L.str.46, 15

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Mail"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"X-UIDL: "
	.size	.L.str.48, 9

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"X-Apparently-To: "
	.size	.L.str.49, 18

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"X-Envelope-From: "
	.size	.L.str.50, 18

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"X-Original-To: "
	.size	.L.str.51, 16

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"X-Symantec-"
	.size	.L.str.52, 12

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Symantec"
	.size	.L.str.53, 9

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"X-EVS"
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"EVS mail"
	.size	.L.str.55, 9

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"X-Real-To: "
	.size	.L.str.56, 12

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"X-Sieve: "
	.size	.L.str.57, 10

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	">From "
	.size	.L.str.58, 7

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Date: "
	.size	.L.str.59, 7

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"Message-Id: "
	.size	.L.str.60, 13

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"Message-ID: "
	.size	.L.str.61, 13

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"Envelope-to: "
	.size	.L.str.62, 14

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Delivery-date: "
	.size	.L.str.63, 16

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"To: "
	.size	.L.str.64, 5

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Subject: "
	.size	.L.str.65, 10

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"For: "
	.size	.L.str.66, 6

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Eserv mail"
	.size	.L.str.67, 11

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"From: "
	.size	.L.str.68, 7

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"Exim mail"
	.size	.L.str.69, 10

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"v:\r\nReceived: "
	.size	.L.str.70, 15

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"VPOP3 Mail (DOS)"
	.size	.L.str.71, 17

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"v:\nReceived: "
	.size	.L.str.72, 14

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"VPOP3 Mail (UNIX)"
	.size	.L.str.73, 18

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"Hi. This is the qmail-send"
	.size	.L.str.74, 27

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"Qmail bounce"
	.size	.L.str.75, 13

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"x\237>\""
	.size	.L.str.76, 5

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"TNEF"
	.size	.L.str.77, 5

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"begin "
	.size	.L.str.78, 7

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"UUencoded"
	.size	.L.str.79, 10

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"!BDN"
	.size	.L.str.80, 5

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"PST"
	.size	.L.str.81, 4

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"GIF"
	.size	.L.str.82, 4

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"BM"
	.size	.L.str.83, 3

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"BMP"
	.size	.L.str.84, 4

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"\377\330\377"
	.size	.L.str.85, 4

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"JPEG"
	.size	.L.str.86, 5

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"JFIF"
	.size	.L.str.87, 5

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Exif"
	.size	.L.str.88, 5

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"\211PNG"
	.size	.L.str.89, 5

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"PNG"
	.size	.L.str.90, 4

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"RIFF"
	.size	.L.str.91, 5

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"RIFX"
	.size	.L.str.92, 5

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"\320\317\021\340\241\261\032\341"
	.size	.L.str.93, 9

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"OLE2 container"
	.size	.L.str.94, 15

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"%PDF-"
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"PDF document"
	.size	.L.str.96, 13

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"\266\271\254\256\376\377\377\377"
	.size	.L.str.97, 9

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"CryptFF"
	.size	.L.str.98, 8

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"{\\rtf"
	.size	.L.str.99, 6

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"RTF"
	.size	.L.str.100, 4

	.type	.L.str.101,@object      # @.str.101
	.section	.rodata,"a",@progbits
.L.str.101:
	.asciz	"\000\000\001\263"
	.size	.L.str.101, 5

	.type	.L.str.102,@object      # @.str.102
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.102:
	.asciz	"MPEG video stream"
	.size	.L.str.102, 18

	.type	.L.str.103,@object      # @.str.103
	.section	.rodata,"a",@progbits
.L.str.103:
	.asciz	"\000\000\001\272"
	.size	.L.str.103, 5

	.type	.L.str.104,@object      # @.str.104
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.104:
	.asciz	"MPEG sys stream"
	.size	.L.str.104, 16

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"OggS"
	.size	.L.str.105, 5

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"Ogg Stream"
	.size	.L.str.106, 11

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"ID3"
	.size	.L.str.107, 4

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"MP3"
	.size	.L.str.108, 4

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"\377\373\220"
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"%!PS-Adobe-"
	.size	.L.str.110, 12

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"PostScript"
	.size	.L.str.111, 11

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"0&\262u\216f\317"
	.size	.L.str.112, 8

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"WMA/WMV/ASF"
	.size	.L.str.113, 12

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	".RMF"
	.size	.L.str.114, 5

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"Real Media File"
	.size	.L.str.115, 16

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"0a46726f6d3a20{-2048}0a436f6e74656e742d547970653a20"
	.size	.L.str.116, 52

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"Mail file"
	.size	.L.str.117, 10

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"0a52656365697665643a20{-2048}0a436f6e74656e742d547970653a20"
	.size	.L.str.118, 60

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"0a52656365697665643a20{-2048}0a436f6e74656e742d747970653a20"
	.size	.L.str.119, 60

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"4d494d452d56657273696f6e3a20{-2048}0a436f6e74656e742d547970653a20"
	.size	.L.str.120, 66

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"3c62723e"
	.size	.L.str.121, 9

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"HTML data"
	.size	.L.str.122, 10

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"3c42723e"
	.size	.L.str.123, 9

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"3c42523e"
	.size	.L.str.124, 9

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"3c703e"
	.size	.L.str.125, 7

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"3c503e"
	.size	.L.str.126, 7

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"68726566"
	.size	.L.str.127, 9

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"48726566"
	.size	.L.str.128, 9

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"48524546"
	.size	.L.str.129, 9

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"3c68746d6c3e"
	.size	.L.str.130, 13

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"3c48544d4c3e"
	.size	.L.str.131, 13

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"3c48746d6c3e"
	.size	.L.str.132, 13

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"3c686561643e"
	.size	.L.str.133, 13

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"3c484541443e"
	.size	.L.str.134, 13

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"3c486561643e"
	.size	.L.str.135, 13

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"3c666f6e74"
	.size	.L.str.136, 11

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"3c466f6e74"
	.size	.L.str.137, 11

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"3c464f4e54"
	.size	.L.str.138, 11

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"3c696d67"
	.size	.L.str.139, 9

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"3c494d47"
	.size	.L.str.140, 9

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"3c496d67"
	.size	.L.str.141, 9

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"3c736372697074"
	.size	.L.str.142, 15

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"3c536372697074"
	.size	.L.str.143, 15

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"3c534352495054"
	.size	.L.str.144, 15

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"3c6f626a656374"
	.size	.L.str.145, 15

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"3c4f626a656374"
	.size	.L.str.146, 15

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"3c4f424a454354"
	.size	.L.str.147, 15

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"3c696672616d65"
	.size	.L.str.148, 15

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"3c494652414d45"
	.size	.L.str.149, 15

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"3c7461626c65"
	.size	.L.str.150, 13

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"3c5441424c45"
	.size	.L.str.151, 13

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"526172211a0700"
	.size	.L.str.152, 15

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"RAR-SFX"
	.size	.L.str.153, 8

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"504b0304"
	.size	.L.str.154, 9

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"ZIP-SFX"
	.size	.L.str.155, 8

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"4d534346"
	.size	.L.str.156, 9

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"CAB-SFX"
	.size	.L.str.157, 8

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"60ea{7}0002"
	.size	.L.str.158, 12

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"ARJ-SFX"
	.size	.L.str.159, 8

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"60ea{7}0102"
	.size	.L.str.160, 12

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"60ea{7}0202"
	.size	.L.str.161, 12

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"efbeadde4e756c6c736f6674496e7374"
	.size	.L.str.162, 33

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"NSIS"
	.size	.L.str.163, 5

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"a3484bbe986c4aa9994c530a86d6487d41553321454130(35|36)"
	.size	.L.str.164, 54

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"AUTOIT"
	.size	.L.str.165, 7

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"4d5a{60-300}50450000"
	.size	.L.str.166, 21

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"PE"
	.size	.L.str.167, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
