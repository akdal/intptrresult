	.text
	.file	"btSoftBodyHelpers.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	3184315597              # float -0.100000001
.LCPI0_1:
	.long	1036831949              # float 0.100000001
.LCPI0_4:
	.long	1056964608              # float 0.5
.LCPI0_5:
	.long	1065353216              # float 1
.LCPI0_7:
	.long	1077936128              # float 3
.LCPI0_8:
	.long	3196059648              # float -0.25
.LCPI0_9:
	.long	1048576000              # float 0.25
.LCPI0_12:
	.long	1051372203              # float 0.333333343
.LCPI0_14:
	.long	1061997773              # float 0.800000011
.LCPI0_15:
	.long	805306368               # float 4.65661287E-10
.LCPI0_17:
	.long	1061158912              # float 0.75
.LCPI0_19:
	.long	1092616192              # float 10
.LCPI0_21:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.long	0                       # float 0
	.long	1036831949              # float 0.100000001
	.zero	4
	.zero	4
.LCPI0_3:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI0_6:
	.long	1077936128              # float 3
	.long	1077936128              # float 3
	.zero	4
	.zero	4
.LCPI0_10:
	.long	0                       # float 0
	.long	1048576000              # float 0.25
	.zero	4
	.zero	4
.LCPI0_11:
	.long	1051372203              # float 0.333333343
	.long	1051372203              # float 0.333333343
	.zero	4
	.zero	4
.LCPI0_13:
	.long	1061997773              # float 0.800000011
	.long	1061997773              # float 0.800000011
	.zero	4
	.zero	4
.LCPI0_16:
	.long	1061158912              # float 0.75
	.long	1061158912              # float 0.75
	.zero	4
	.zero	4
.LCPI0_18:
	.long	1092616192              # float 10
	.long	1092616192              # float 10
	.zero	4
	.zero	4
.LCPI0_20:
	.long	1048576000              # float 0.25
	.long	1048576000              # float 0.25
	.zero	4
	.zero	4
.LCPI0_22:
	.zero	16
	.text
	.globl	_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi,@function
_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi: # @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi6:
	.cfi_def_cfa_offset 512
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 432(%rsp)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 352(%rsp)
	movq	$1065353216, 360(%rsp)  # imm = 0x3F800000
	movq	$1065353216, 400(%rsp)  # imm = 0x3F800000
	movq	$0, 408(%rsp)
	movl	%edx, 20(%rsp)          # 4-byte Spill
	testb	$1, %dl
	je	.LBB0_6
# BB#1:                                 # %.preheader2087
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	820(%rax), %eax
	testl	%eax, %eax
	jle	.LBB0_6
# BB#2:                                 # %.lr.ph2291
	xorl	%ebp, %ebp
	movl	$24, %ebx
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	832(%rcx), %r15
	movq	-16(%r15,%rbx), %rcx
	testb	$1, 20(%rcx)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movsd	-8(%r15,%rbx), %xmm0    # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	movss	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	(%r15,%rbx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movss	(%r15,%rbx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	addss	%xmm4, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movq	$0, 48(%rsp)
	movq	%r14, %rdi
	leaq	80(%rsp), %r12
	movq	%r12, %rsi
	leaq	160(%rsp), %r12
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	-8(%r15,%rbx), %xmm0    # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_0(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	(%r15,%rbx), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movss	(%r15,%rbx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	addps	.LCPI0_2(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movabsq	$4575657221408423936, %rcx # imm = 0x3F80000000000000
	movq	%rcx, 40(%rsp)
	movq	$0, 48(%rsp)
	movq	%r14, %rdi
	leaq	80(%rsp), %r13
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	-8(%r15,%rbx), %rcx
	movd	%rcx, %xmm0
	movss	(%r15,%rbx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_0(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	addps	.LCPI0_22, %xmm0
	movss	.LCPI0_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	$0, 40(%rsp)
	movq	$1065353216, 48(%rsp)   # imm = 0x3F800000
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	820(%rax), %eax
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	addq	$120, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB0_3
.LBB0_6:                                # %.loopexit2088
	testb	$2, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB0_12
# BB#7:                                 # %.preheader2085
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	852(%rax), %eax
	testl	%eax, %eax
	jle	.LBB0_12
# BB#8:                                 # %.lr.ph2289
	xorl	%ebx, %ebx
	movl	$24, %ebp
	leaq	432(%rsp), %r15
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	864(%rcx), %rcx
	movq	-16(%rcx,%rbp), %rdx
	testb	$1, 20(%rdx)
	je	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	-8(%rcx,%rbp), %rsi
	movq	(%rcx,%rbp), %rdx
	addq	$16, %rsi
	addq	$16, %rdx
	movq	%r15, %rcx
	callq	*40(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	852(%rax), %eax
.LBB0_11:                               #   in Loop: Header=BB0_9 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$72, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB0_9
.LBB0_12:                               # %.loopexit2086
	testb	$16, 20(%rsp)           # 1-byte Folded Reload
	je	.LBB0_18
# BB#13:                                # %.preheader2083
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	820(%rax), %eax
	testl	%eax, %eax
	jle	.LBB0_18
# BB#14:                                # %.lr.ph2287
	xorl	%ebx, %ebx
	movl	$88, %r15d
	leaq	80(%rsp), %r13
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	832(%rcx), %r12
	movq	-80(%r12,%r15), %rcx
	testb	$1, 20(%rcx)
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	movsd	-8(%r12,%r15), %xmm1    # xmm1 = mem[0],zero
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = <0.5,0.5,u,u>
	mulps	%xmm0, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movss	(%r12,%r15), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 240(%rsp)        # 4-byte Spill
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	leaq	-72(%r12,%r15), %r14
	movsd	-72(%r12,%r15), %xmm0   # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movss	-64(%r12,%r15), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	leaq	352(%rsp), %rcx
	callq	*%rax
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	movsd	-72(%r12,%r15), %xmm0   # xmm0 = mem[0],zero
	subps	224(%rsp), %xmm0        # 16-byte Folded Reload
	movss	-64(%r12,%r15), %xmm1   # xmm1 = mem[0],zero,zero,zero
	subss	240(%rsp), %xmm1        # 4-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movsd	352(%rsp), %xmm0        # xmm0 = mem[0],zero
	mulps	.LCPI0_3(%rip), %xmm0
	movss	360(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI0_4(%rip), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	leaq	160(%rsp), %rcx
	callq	*%rax
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	820(%rax), %eax
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$120, %r15
	cmpq	%rcx, %rbx
	jl	.LBB0_15
.LBB0_18:                               # %.loopexit2084
	testb	$32, 20(%rsp)           # 1-byte Folded Reload
	je	.LBB0_29
# BB#19:
	movb	_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis(%rip), %al
	testb	%al, %al
	jne	.LBB0_22
# BB#20:
	movl	$_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_22
# BB#21:
	movl	$1065353216, _ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+4(%rip)
	movl	$1065353216, _ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+24(%rip)
	movq	$1065353216, _ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, %edi
	callq	__cxa_guard_release
.LBB0_22:                               # %.preheader2081
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$0, 980(%rcx)
	jle	.LBB0_29
# BB#23:                                # %.lr.ph2285
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	movq	992(%rcx), %r12
	movq	32(%r12,%r14), %rax
	movsd	16(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	8(%r12,%r14), %xmm1     # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm0, %xmm3      # xmm3 = xmm0[1,1,2,3]
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%r12,%r14), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	24(%r12,%r14), %xmm5
	mulss	%xmm5, %xmm3
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	subps	%xmm5, %xmm0
	subss	%xmm3, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	8(%r12,%r14), %xmm6     # xmm6 = mem[0],zero,zero,zero
	movq	12(%r12,%r14), %xmm0    # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomiss	%xmm6, %xmm0
	setbe	%al
	leaq	(%r12,%rax,4), %rcx
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	ucomiss	8(%r14,%rcx), %xmm1
	movl	$2, %ecx
	cmovbeq	%rcx, %rax
	shlq	$4, %rax
	movss	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+4(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movss	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis+8(%rax), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	mulss	%xmm2, %xmm6
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm0, %xmm5
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm2, %xmm4
	subps	%xmm4, %xmm5
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_26
# BB#25:                                # %call.sqrt
                                        #   in Loop: Header=BB0_24 Depth=1
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB0_26:                               # %.split
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	%rbx, 240(%rsp)         # 8-byte Spill
	movss	.LCPI0_5(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	divss	%xmm1, %xmm7
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm6, %xmm7
	movss	8(%r12,%r14), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movss	16(%r12,%r14), %xmm2    # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm7, %xmm6
	shufps	$0, %xmm0, %xmm6        # xmm6 = xmm6[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm6      # xmm6 = xmm6[2,0],xmm0[2,3]
	mulps	%xmm2, %xmm6
	movaps	%xmm7, %xmm2
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	movsd	12(%r12,%r14), %xmm3    # xmm3 = mem[0],zero
	mulps	%xmm2, %xmm3
	subps	%xmm3, %xmm6
	movaps	%xmm6, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	12(%r12,%r14), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_28
# BB#27:                                # %call.sqrt2674
                                        #   in Loop: Header=BB0_24 Depth=1
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	movaps	%xmm7, 272(%rsp)        # 16-byte Spill
	movss	%xmm3, 320(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	320(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	272(%rsp), %xmm7        # 16-byte Reload
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	movss	.LCPI0_5(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB0_28:                               # %.split.split
                                        #   in Loop: Header=BB0_24 Depth=1
	divss	%xmm1, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm6
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm4
	movaps	%xmm4, 256(%rsp)        # 16-byte Spill
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = <0.5,0.5,u,u>
	mulps	%xmm0, %xmm5
	movss	.LCPI0_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	subps	%xmm5, %xmm1
	movss	88(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	subss	%xmm7, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1,2,3]
	movlps	%xmm1, 160(%rsp)
	movlps	%xmm4, 168(%rsp)
	addps	%xmm0, %xmm5
	addss	%xmm2, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm5, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movq	%rbp, %rdi
	leaq	160(%rsp), %r13
	movq	%r13, %rsi
	leaq	40(%rsp), %rbx
	movq	%rbx, %rdx
	leaq	400(%rsp), %r15
	movq	%r15, %rcx
	callq	*%rax
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	mulps	.LCPI0_3(%rip), %xmm2
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	mulss	.LCPI0_4(%rip), %xmm5
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	subps	%xmm2, %xmm1
	movaps	%xmm2, %xmm6
	movss	88(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	subss	%xmm5, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1,2,3]
	movlps	%xmm1, 160(%rsp)
	movlps	%xmm4, 168(%rsp)
	addps	%xmm0, %xmm6
	addss	%xmm2, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm6, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	*%rax
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	movsd	8(%r12,%r14), %xmm0     # xmm0 = mem[0],zero
	mulps	.LCPI0_3(%rip), %xmm0
	movss	16(%r12,%r14), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI0_4(%rip), %xmm1
	mulps	.LCPI0_6(%rip), %xmm0
	mulss	.LCPI0_7(%rip), %xmm1
	movsd	80(%rsp), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm0, %xmm2
	addss	88(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 160(%rsp)
	movlps	%xmm0, 168(%rsp)
	movabsq	$4575657222473777152, %rcx # imm = 0x3F8000003F800000
	movq	%rcx, 40(%rsp)
	movq	$0, 48(%rsp)
	movq	%rbp, %rdi
	leaq	80(%rsp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	*%rax
	movq	240(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	980(%rcx), %rax
	addq	$120, %r14
	cmpq	%rax, %rbx
	jl	.LBB0_24
.LBB0_29:                               # %.loopexit2082
	testb	$64, 20(%rsp)           # 1-byte Folded Reload
	je	.LBB0_39
# BB#30:                                # %.preheader2080
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$0, 948(%rcx)
	jle	.LBB0_33
# BB#31:                                # %.lr.ph2283
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movq	72(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	960(%rcx), %rcx
	movq	24(%rcx,%rbp), %rax
	movss	8(%rcx,%rbp), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movss	12(%rcx,%rbp), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	16(%rcx,%rbp), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	56(%rax), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	40(%rax), %xmm2
	mulss	44(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rax), %xmm0
	addss	%xmm1, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 40(%rsp)
	movlps	%xmm1, 48(%rsp)
	movq	(%rcx,%rbp), %r12
	movq	$1065353216, 24(%rsp)   # imm = 0x3F800000
	movq	$0, 32(%rsp)
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	movss	.LCPI0_8(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	addss	%xmm4, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r13, %rdi
	leaq	80(%rsp), %rcx
	movq	%rcx, %rsi
	leaq	160(%rsp), %r14
	movq	%r14, %rdx
	leaq	24(%rsp), %rbx
	movq	%rbx, %rcx
	callq	*%rax
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movq	16(%r12), %xmm0         # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_8(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	24(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movaps	.LCPI0_10(%rip), %xmm1  # xmm1 = <0,0.25,u,u>
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r13, %rdi
	leaq	80(%rsp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movq	%rbx, %r14
	callq	*%rax
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movq	16(%r12), %rcx
	movd	%rcx, %xmm0
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_8(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	xorps	%xmm2, %xmm2
	addps	%xmm2, %xmm0
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%r13, %rdi
	leaq	80(%rsp), %r15
	movq	%r15, %rsi
	leaq	160(%rsp), %r12
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*%rax
	movabsq	$4575657221408423936, %rax # imm = 0x3F80000000000000
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	addss	.LCPI0_8(%rip), %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movq	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_8(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	48(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	addps	.LCPI0_10(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movq	40(%rsp), %rcx
	movd	%rcx, %xmm0
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_8(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	addps	.LCPI0_22, %xmm0
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r13), %rax
	movq	40(%rax), %rax
	movq	240(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rbp), %rsi
	movabsq	$4575657222473777152, %rcx # imm = 0x3F8000003F800000
	addq	$16, %rsi
	movq	%rcx, 80(%rsp)
	movq	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movq	%r13, %rdi
	leaq	40(%rsp), %rdx
	movq	%r15, %rcx
	callq	*%rax
	movq	224(%rsp), %rdx         # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	incq	%rdx
	movslq	948(%rcx), %rax
	addq	$104, %rbp
	cmpq	%rax, %rdx
	jl	.LBB0_32
.LBB0_33:                               # %.preheader2078
	movl	820(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB0_39
# BB#34:                                # %.lr.ph2281
	xorl	%ebp, %ebp
	movl	$8, %ebx
	xorps	%xmm4, %xmm4
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	832(%rcx), %r15
	movq	(%r15,%rbx), %rcx
	testb	$1, 20(%rcx)
	je	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_35 Depth=1
	ucomiss	88(%r15,%rbx), %xmm4
	jb	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_35 Depth=1
	movq	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movq	$0, 48(%rsp)
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movsd	8(%r15,%rbx), %xmm0     # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	movss	.LCPI0_8(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	16(%r15,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	16(%r15,%rbx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	leaq	80(%rsp), %r12
	movq	%r12, %rsi
	leaq	160(%rsp), %r12
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	8(%r15,%rbx), %xmm0     # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_8(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	16(%r15,%rbx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	16(%r15,%rbx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	addps	.LCPI0_10(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	leaq	80(%rsp), %r13
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	8(%r15,%rbx), %rcx
	movd	%rcx, %xmm0
	movss	16(%r15,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_8(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	addps	.LCPI0_22, %xmm0
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	40(%rsp), %rcx
	callq	*%rax
	xorps	%xmm4, %xmm4
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	820(%rax), %eax
.LBB0_38:                               #   in Loop: Header=BB0_35 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	addq	$120, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB0_35
.LBB0_39:                               # %.loopexit2079
	testb	$4, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB0_45
# BB#40:
	movabsq	$4554039942338052096, %rax # imm = 0x3F33333300000000
	movq	%rax, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	884(%rax), %eax
	testl	%eax, %eax
	jle	.LBB0_45
# BB#41:                                # %.lr.ph2278
	xorl	%ebx, %ebx
	movl	$32, %r15d
	leaq	40(%rsp), %r12
	leaq	24(%rsp), %r13
	leaq	80(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_42:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	896(%rcx), %rcx
	movq	-24(%rcx,%r15), %rdx
	testb	$1, 20(%rdx)
	je	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_42 Depth=1
	movq	-16(%rcx,%r15), %rax
	movq	-8(%rcx,%r15), %rdx
	movsd	16(%rax), %xmm6         # xmm6 = mem[0],zero
	movss	24(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movsd	16(%rdx), %xmm4         # xmm4 = mem[0],zero
	movss	24(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movq	(%rcx,%r15), %rax
	movsd	16(%rax), %xmm8         # xmm8 = mem[0],zero
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	addps	%xmm4, %xmm2
	movaps	%xmm7, %xmm3
	addss	%xmm5, %xmm3
	addps	%xmm8, %xmm2
	addss	%xmm1, %xmm3
	mulps	.LCPI0_11(%rip), %xmm2
	mulss	.LCPI0_12(%rip), %xmm3
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	subps	%xmm2, %xmm6
	subss	%xmm3, %xmm7
	movaps	.LCPI0_13(%rip), %xmm0  # xmm0 = <0.800000011,0.800000011,u,u>
	movaps	%xmm0, %xmm9
	mulps	%xmm9, %xmm6
	movss	.LCPI0_14(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm10
	mulss	%xmm10, %xmm7
	addps	%xmm2, %xmm6
	addss	%xmm3, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm6, 160(%rsp)
	movlps	%xmm0, 168(%rsp)
	subps	%xmm2, %xmm4
	subss	%xmm3, %xmm5
	mulps	%xmm9, %xmm4
	mulss	%xmm10, %xmm5
	addps	%xmm2, %xmm4
	addss	%xmm3, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm4, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	subps	%xmm2, %xmm8
	subss	%xmm3, %xmm1
	mulps	%xmm9, %xmm8
	mulss	%xmm10, %xmm1
	addps	%xmm2, %xmm8
	addss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm8, 24(%rsp)
	movlps	%xmm0, 32(%rsp)
	leaq	160(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%rbp, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	884(%rax), %eax
.LBB0_44:                               #   in Loop: Header=BB0_42 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$72, %r15
	cmpq	%rcx, %rbx
	jl	.LBB0_42
.LBB0_45:
	movl	20(%rsp), %eax          # 4-byte Reload
	testb	$1, %ah
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB0_114
# BB#46:
	movl	$1806, %edi             # imm = 0x70E
	callq	srand
	cmpl	$0, 1340(%rbx)
	jle	.LBB0_114
# BB#47:                                # %.lr.ph2275
	xorl	%r12d, %r12d
	leaq	24(%rsp), %r15
	.p2align	4, 0x90
.LBB0_48:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_55 Depth 2
                                        #     Child Loop BB0_60 Depth 2
                                        #     Child Loop BB0_66 Depth 2
                                        #     Child Loop BB0_76 Depth 2
                                        #     Child Loop BB0_82 Depth 2
                                        #     Child Loop BB0_87 Depth 2
                                        #     Child Loop BB0_91 Depth 2
	movq	1352(%rbx), %rax
	movq	(%rax,%r12,8), %rbp
	cmpb	$0, 417(%rbp)
	je	.LBB0_113
# BB#49:                                #   in Loop: Header=BB0_48 Depth=1
	callq	rand
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	movss	.LCPI0_15(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 224(%rsp)        # 4-byte Spill
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_15(%rip), %xmm0
	movss	%xmm0, 240(%rsp)        # 4-byte Spill
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_15(%rip), %xmm0
	movss	224(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 24(%rsp)
	movss	240(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 28(%rsp)
	movss	%xmm0, 32(%rsp)
	movl	$0, 36(%rsp)
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_51
# BB#50:                                # %call.sqrt2676
                                        #   in Loop: Header=BB0_48 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_51:                               # %.split2675
                                        #   in Loop: Header=BB0_48 Depth=1
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movsd	24(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	32(%rsp), %xmm0
	mulps	.LCPI0_16(%rip), %xmm2
	mulss	.LCPI0_17(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 24(%rsp)
	movlps	%xmm1, 32(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	1352(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movslq	4(%rax), %rbp
	testq	%rbp, %rbp
	jle	.LBB0_56
# BB#52:                                #   in Loop: Header=BB0_48 Depth=1
	movq	%rbp, %rdi
	shlq	$4, %rdi
.Ltmp0:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp1:
# BB#53:                                # %.lr.ph.i943
                                        #   in Loop: Header=BB0_48 Depth=1
	leaq	-1(%rbp), %rdi
	movq	%rbp, %rdx
	andq	$7, %rdx
	je	.LBB0_57
# BB#54:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_55:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	80(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rcx
	addq	$16, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB0_55
	jmp	.LBB0_58
	.p2align	4, 0x90
.LBB0_56:                               # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit.thread
                                        #   in Loop: Header=BB0_48 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_67
.LBB0_57:                               #   in Loop: Header=BB0_48 Depth=1
	xorl	%ecx, %ecx
.LBB0_58:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
                                        #   in Loop: Header=BB0_48 Depth=1
	cmpq	$7, %rdi
	jb	.LBB0_61
# BB#59:                                # %.lr.ph.i943.new
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	shlq	$4, %rcx
	leaq	112(%rax,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_60:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	80(%rsp), %xmm0
	movups	%xmm0, -112(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -96(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -80(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -64(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -48(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -32(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, -16(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, (%rcx)
	subq	$-128, %rcx
	addq	$-8, %rdx
	jne	.LBB0_60
.LBB0_61:                               # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit
                                        #   in Loop: Header=BB0_48 Depth=1
	testl	%ebp, %ebp
	jle	.LBB0_67
# BB#62:                                # %.lr.ph2269.preheader
                                        #   in Loop: Header=BB0_48 Depth=1
	movl	%ebp, %ebx
	testb	$1, %bl
	jne	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_48 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %ebp
	jne	.LBB0_65
	jmp	.LBB0_67
.LBB0_64:                               # %.lr.ph2269.prol
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	1352(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movups	16(%rcx), %xmm0
	movups	%xmm0, (%rax)
	movl	$1, %ecx
	cmpl	$1, %ebp
	je	.LBB0_67
.LBB0_65:                               # %.lr.ph2269.preheader.new
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rcx, %rsi
	shlq	$4, %rsi
	movq	%rax, %rdx
	addq	%rsi, %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_66:                               # %.lr.ph2269
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1352(%rdi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	16(%rsi), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movups	16(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movq	1352(%rdi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	16(%rsi), %rsi
	movq	8(%rsi,%rcx,8), %rsi
	movups	16(%rsi), %xmm0
	movups	%xmm0, 16(%rdx)
	addq	$2, %rcx
	addq	$32, %rdx
	cmpq	%rcx, %rbx
	jne	.LBB0_66
	.p2align	4, 0x90
.LBB0_67:                               # %._crit_edge2270
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	$1, 40(%rsp)
	movl	%ebp, 44(%rsp)
	movq	%rax, 48(%rsp)
	movabsq	$4216233944099586064, %rax # imm = 0x3A83126F00000010
	movq	%rax, 56(%rsp)
	movb	$1, 112(%rsp)
	movq	$0, 104(%rsp)
	movl	$0, 92(%rsp)
	movl	$0, 96(%rsp)
	movb	$1, 152(%rsp)
	movq	$0, 144(%rsp)
	movl	$0, 132(%rsp)
	movl	$0, 136(%rsp)
	movb	$1, 80(%rsp)
	movl	$0, 84(%rsp)
	movl	$0, 120(%rsp)
	movl	$0, 124(%rsp)
	movb	$1, 184(%rsp)
	movq	$0, 176(%rsp)
	movl	$0, 164(%rsp)
	movl	$0, 168(%rsp)
	movb	$1, 216(%rsp)
	movq	$0, 208(%rsp)
	movl	$0, 196(%rsp)
	movl	$0, 200(%rsp)
	movl	%ebp, 64(%rsp)
.Ltmp3:
	leaq	160(%rsp), %rdi
	leaq	40(%rsp), %rsi
	leaq	80(%rsp), %rdx
	callq	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
.Ltmp4:
# BB#68:                                #   in Loop: Header=BB0_48 Depth=1
	movl	92(%rsp), %eax
	movq	104(%rsp), %r8
	testl	%eax, %eax
	jle	.LBB0_72
# BB#69:                                #   in Loop: Header=BB0_48 Depth=1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	movsd	(%r8), %xmm0            # xmm0 = mem[0],zero
	movsd	8(%r8), %xmm3           # xmm3 = mem[0],zero
	cmpl	$1, %eax
	je	.LBB0_77
# BB#70:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_48 Depth=1
	testb	$1, %al
	jne	.LBB0_74
# BB#71:                                # %.lr.ph.i.i.prol
                                        #   in Loop: Header=BB0_48 Depth=1
	movsd	16(%r8), %xmm1          # xmm1 = mem[0],zero
	addps	%xmm1, %xmm0
	addss	24(%r8), %xmm3
	movl	$2, %edx
	cmpl	$2, %eax
	jne	.LBB0_75
	jmp	.LBB0_77
	.p2align	4, 0x90
.LBB0_72:                               #   in Loop: Header=BB0_48 Depth=1
	testl	%eax, %eax
	movss	.LCPI0_5(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
                                        # implicit-def: %XMM3
                                        # implicit-def: %XMM0
	je	.LBB0_77
# BB#73:                                # %.thread4.i
                                        #   in Loop: Header=BB0_48 Depth=1
	movsd	(%r8), %xmm0            # xmm0 = mem[0],zero
	movsd	8(%r8), %xmm3           # xmm3 = mem[0],zero
	movss	.LCPI0_5(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	testl	%eax, %eax
	jg	.LBB0_78
	jmp	.LBB0_88
.LBB0_74:                               #   in Loop: Header=BB0_48 Depth=1
	movl	$1, %edx
	cmpl	$2, %eax
	je	.LBB0_77
.LBB0_75:                               # %.lr.ph.preheader.i.i.new
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rax, %rcx
	subq	%rdx, %rcx
	shlq	$4, %rdx
	leaq	24(%r8,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_76:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rdx), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	-16(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	addss	(%rdx), %xmm4
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	addq	$32, %rdx
	addq	$-2, %rcx
	jne	.LBB0_76
	.p2align	4, 0x90
.LBB0_77:                               # %.loopexit2076
                                        #   in Loop: Header=BB0_48 Depth=1
	testl	%eax, %eax
	jle	.LBB0_88
.LBB0_78:                               # %.lr.ph.i927
                                        #   in Loop: Header=BB0_48 Depth=1
	movss	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm1, %xmm0
	mulss	%xmm3, %xmm1
	leaq	-1(%rax), %rcx
	testb	$1, %al
	jne	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_48 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	jne	.LBB0_81
	jmp	.LBB0_83
.LBB0_80:                               #   in Loop: Header=BB0_48 Depth=1
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	%xmm3, (%r8)
	movss	4(%r8), %xmm3           # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, 4(%r8)
	movss	8(%r8), %xmm3           # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	%xmm3, 8(%r8)
	movl	$1, %esi
	testq	%rcx, %rcx
	je	.LBB0_83
.LBB0_81:                               # %.lr.ph.i927.new
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	%rax, %rdx
	subq	%rsi, %rdx
	shlq	$4, %rsi
	leaq	24(%r8,%rsi), %rsi
	.p2align	4, 0x90
.LBB0_82:                               #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-24(%rsi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	%xmm3, -24(%rsi)
	movss	-20(%rsi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, -20(%rsi)
	movss	-16(%rsi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	%xmm3, -16(%rsi)
	movss	-8(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	%xmm3, -8(%rsi)
	movss	-4(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, -4(%rsi)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	%xmm3, (%rsi)
	addq	$32, %rsi
	addq	$-2, %rdx
	jne	.LBB0_82
.LBB0_83:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_48 Depth=1
	testb	$1, %al
	jne	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_48 Depth=1
	xorl	%edx, %edx
	testq	%rcx, %rcx
	jne	.LBB0_86
	jmp	.LBB0_88
.LBB0_85:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB0_48 Depth=1
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, (%r8)
	movss	4(%r8), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	movss	%xmm3, 4(%r8)
	movss	8(%r8), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, 8(%r8)
	movl	$1, %edx
	testq	%rcx, %rcx
	je	.LBB0_88
.LBB0_86:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB0_48 Depth=1
	subq	%rdx, %rax
	shlq	$4, %rdx
	leaq	24(%r8,%rdx), %rcx
	.p2align	4, 0x90
.LBB0_87:                               # %.lr.ph.i
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-24(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, -24(%rcx)
	movss	-20(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	movss	%xmm3, -20(%rcx)
	movss	-16(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, -16(%rcx)
	movss	-8(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, -8(%rcx)
	movss	-4(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	movss	%xmm3, -4(%rcx)
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, (%rcx)
	addq	$32, %rcx
	addq	$-2, %rax
	jne	.LBB0_87
.LBB0_88:                               # %_ZL3addI9btVector3S0_EvR20btAlignedObjectArrayIT_ERKT0_.exit.preheader
                                        #   in Loop: Header=BB0_48 Depth=1
	cmpl	$0, 120(%rsp)
	jle	.LBB0_93
# BB#89:                                # %.lr.ph2272.preheader
                                        #   in Loop: Header=BB0_48 Depth=1
	movl	$1, %r13d
	movl	$8, %ebp
	jmp	.LBB0_91
	.p2align	4, 0x90
.LBB0_90:                               # %_ZL3addI9btVector3S0_EvR20btAlignedObjectArrayIT_ERKT0_.exit..lr.ph2272_crit_edge
                                        #   in Loop: Header=BB0_91 Depth=2
	movq	104(%rsp), %r8
	addq	$12, %rbp
	incq	%r13
.LBB0_91:                               # %.lr.ph2272
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%rsp), %rax
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movslq	-8(%rax,%rbp), %rsi
	shlq	$4, %rsi
	addq	%r8, %rsi
	movslq	-4(%rax,%rbp), %rdx
	shlq	$4, %rdx
	addq	%r8, %rdx
	movslq	(%rax,%rbp), %rcx
	shlq	$4, %rcx
	addq	%r8, %rcx
.Ltmp6:
	movq	%r15, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*56(%rbx)
.Ltmp7:
# BB#92:                                # %_ZL3addI9btVector3S0_EvR20btAlignedObjectArrayIT_ERKT0_.exit
                                        #   in Loop: Header=BB0_91 Depth=2
	movslq	120(%rsp), %rax
	cmpq	%rax, %r13
	jl	.LBB0_90
.LBB0_93:                               # %_ZL3addI9btVector3S0_EvR20btAlignedObjectArrayIT_ERKT0_.exit._crit_edge
                                        #   in Loop: Header=BB0_48 Depth=1
.Ltmp9:
	leaq	160(%rsp), %rdi
	leaq	80(%rsp), %rsi
	callq	_ZN11HullLibrary13ReleaseResultER10HullResult
.Ltmp10:
# BB#94:                                #   in Loop: Header=BB0_48 Depth=1
	movq	208(%rsp), %rdi
	testq	%rdi, %rdi
	movq	224(%rsp), %rbx         # 8-byte Reload
	je	.LBB0_98
# BB#95:                                #   in Loop: Header=BB0_48 Depth=1
	cmpb	$0, 216(%rsp)
	je	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_48 Depth=1
.Ltmp20:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp21:
.LBB0_97:                               # %.noexc.i901
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	$0, 208(%rsp)
.LBB0_98:                               #   in Loop: Header=BB0_48 Depth=1
	movb	$1, 216(%rsp)
	movq	$0, 208(%rsp)
	movq	$0, 196(%rsp)
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_102
# BB#99:                                #   in Loop: Header=BB0_48 Depth=1
	cmpb	$0, 184(%rsp)
	je	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_48 Depth=1
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
.LBB0_101:                              # %.noexc905
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	$0, 176(%rsp)
.LBB0_102:                              #   in Loop: Header=BB0_48 Depth=1
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_106
# BB#103:                               #   in Loop: Header=BB0_48 Depth=1
	cmpb	$0, 152(%rsp)
	je	.LBB0_105
# BB#104:                               #   in Loop: Header=BB0_48 Depth=1
.Ltmp37:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp38:
.LBB0_105:                              # %.noexc.i868
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	$0, 144(%rsp)
.LBB0_106:                              #   in Loop: Header=BB0_48 Depth=1
	movb	$1, 152(%rsp)
	movq	$0, 144(%rsp)
	movq	$0, 132(%rsp)
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_110
# BB#107:                               #   in Loop: Header=BB0_48 Depth=1
	cmpb	$0, 112(%rsp)
	je	.LBB0_109
# BB#108:                               #   in Loop: Header=BB0_48 Depth=1
.Ltmp43:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp44:
.LBB0_109:                              # %.noexc872
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	$0, 104(%rsp)
.LBB0_110:                              #   in Loop: Header=BB0_48 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_112
# BB#111:                               #   in Loop: Header=BB0_48 Depth=1
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB0_112:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit860
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	1352(%rbx), %rax
	movq	(%rax,%r12,8), %rbp
.LBB0_113:                              #   in Loop: Header=BB0_48 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	leaq	264(%rbp), %r14
	movss	112(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	96(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	100(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movaps	.LCPI0_18(%rip), %xmm0  # xmm0 = <10,10,u,u>
	mulps	%xmm0, %xmm1
	movss	116(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm2
	addps	%xmm1, %xmm2
	movss	120(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	104(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm3, %xmm1
	addps	%xmm2, %xmm1
	movsd	144(%rbp), %xmm0        # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movss	128(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_19(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	132(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	136(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	addss	152(%rbp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movq	$1065353216, 160(%rsp)  # imm = 0x3F800000
	movq	$0, 168(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	80(%rsp), %r15
	movq	%r15, %rdx
	leaq	160(%rsp), %r13
	movq	%r13, %rcx
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movss	112(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	96(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	100(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	116(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	.LCPI0_18(%rip), %xmm2
	addps	%xmm1, %xmm2
	movss	120(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	104(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm3, %xmm1
	addps	%xmm2, %xmm1
	movsd	144(%rbp), %xmm0        # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movss	128(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm1
	movss	132(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	.LCPI0_19(%rip), %xmm2
	addss	%xmm1, %xmm2
	movss	136(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	addss	152(%rbp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movabsq	$4575657221408423936, %rcx # imm = 0x3F80000000000000
	movq	%rcx, 160(%rsp)
	movq	$0, 168(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movss	112(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	96(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	100(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	116(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm3, %xmm2
	addps	%xmm1, %xmm2
	movss	120(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	104(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	.LCPI0_18(%rip), %xmm1
	addps	%xmm2, %xmm1
	movsd	144(%rbp), %xmm0        # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movss	128(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm1
	movss	132(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	136(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI0_19(%rip), %xmm1
	addss	%xmm2, %xmm1
	addss	152(%rbp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movq	$0, 160(%rsp)
	movq	$1065353216, 168(%rsp)  # imm = 0x3F800000
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%r15, %rdx
	leaq	24(%rsp), %r15
	movq	%r13, %rcx
	callq	*%rax
	incq	%r12
	movslq	1340(%rbx), %rax
	cmpq	%rax, %r12
	jl	.LBB0_48
.LBB0_114:                              # %.loopexit2077
	testb	$8, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB0_120
# BB#115:
	movabsq	$4554039943398372147, %rax # imm = 0x3F3333333F333333
	movq	%rax, 80(%rsp)
	movq	$1060320051, 88(%rsp)   # imm = 0x3F333333
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	916(%rax), %eax
	testl	%eax, %eax
	jle	.LBB0_120
# BB#116:                               # %.lr.ph2266
	xorl	%r14d, %r14d
	movl	$40, %r15d
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_117:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	928(%rcx), %rcx
	movq	-32(%rcx,%r15), %rdx
	testb	$1, 20(%rdx)
	je	.LBB0_119
# BB#118:                               #   in Loop: Header=BB0_117 Depth=1
	movq	-24(%rcx,%r15), %rax
	movq	-16(%rcx,%r15), %rdx
	movsd	16(%rax), %xmm4         # xmm4 = mem[0],zero
	movss	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movsd	16(%rdx), %xmm6         # xmm6 = mem[0],zero
	movss	24(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movq	-8(%rcx,%r15), %rax
	movsd	16(%rax), %xmm5         # xmm5 = mem[0],zero
	movss	24(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	(%rcx,%r15), %rax
	movsd	16(%rax), %xmm9         # xmm9 = mem[0],zero
	movaps	%xmm9, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	%xmm8, 304(%rsp)        # 4-byte Spill
	movaps	%xmm4, %xmm3
	addps	%xmm6, %xmm3
	movaps	%xmm2, %xmm7
	addss	%xmm1, %xmm7
	addps	%xmm5, %xmm3
	addss	%xmm0, %xmm7
	addps	%xmm9, %xmm3
	addss	%xmm8, %xmm7
	mulps	.LCPI0_20(%rip), %xmm3
	mulss	.LCPI0_9(%rip), %xmm7
	movq	(%rbp), %rax
	movq	56(%rax), %r9
	subps	%xmm3, %xmm4
	movaps	%xmm3, %xmm8
	subss	%xmm7, %xmm2
	movaps	.LCPI0_13(%rip), %xmm3  # xmm3 = <0.800000011,0.800000011,u,u>
	mulps	%xmm3, %xmm4
	movaps	%xmm3, %xmm9
	movss	.LCPI0_14(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movaps	%xmm3, %xmm10
	addps	%xmm8, %xmm4
	movaps	%xmm4, 240(%rsp)        # 16-byte Spill
	addss	%xmm7, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movaps	%xmm3, 256(%rsp)        # 16-byte Spill
	movlps	%xmm4, 160(%rsp)
	movlps	%xmm3, 168(%rsp)
	movaps	%xmm8, 416(%rsp)        # 16-byte Spill
	subps	%xmm8, %xmm6
	movss	%xmm7, 300(%rsp)        # 4-byte Spill
	subss	%xmm7, %xmm1
	mulps	%xmm9, %xmm6
	mulss	%xmm10, %xmm1
	addps	%xmm8, %xmm6
	movaps	%xmm6, 384(%rsp)        # 16-byte Spill
	addss	%xmm7, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, 368(%rsp)        # 16-byte Spill
	movlps	%xmm6, 40(%rsp)
	movlps	%xmm2, 48(%rsp)
	subps	%xmm8, %xmm5
	subss	%xmm7, %xmm0
	mulps	%xmm9, %xmm5
	mulss	%xmm10, %xmm0
	addps	%xmm8, %xmm5
	movaps	%xmm5, 272(%rsp)        # 16-byte Spill
	addss	%xmm7, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	%xmm1, 320(%rsp)        # 16-byte Spill
	movlps	%xmm5, 24(%rsp)
	movlps	%xmm1, 32(%rsp)
	movq	%rbp, %rdi
	leaq	160(%rsp), %r12
	movq	%r12, %rsi
	leaq	40(%rsp), %r12
	movq	%r12, %rdx
	leaq	24(%rsp), %rbx
	movq	%rbx, %rcx
	leaq	80(%rsp), %rax
	movq	%rax, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*%r9
	movq	(%rbp), %rax
	movq	56(%rax), %rax
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 160(%rsp)
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 168(%rsp)
	movaps	384(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 40(%rsp)
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 48(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movaps	416(%rsp), %xmm3        # 16-byte Reload
	subps	%xmm3, %xmm0
	movss	304(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	300(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	mulps	.LCPI0_13(%rip), %xmm0
	mulss	.LCPI0_14(%rip), %xmm1
	addps	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, 304(%rsp)        # 16-byte Spill
	movlps	%xmm0, 24(%rsp)
	movlps	%xmm2, 32(%rsp)
	movq	%rbp, %rdi
	leaq	160(%rsp), %r13
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	leaq	80(%rsp), %rbx
	movq	%rbx, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
	movq	(%rbp), %rax
	movq	56(%rax), %rax
	movaps	384(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 160(%rsp)
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 168(%rsp)
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 40(%rsp)
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 48(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	24(%rsp), %rcx
	movq	%rbx, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
	movq	(%rbp), %rax
	movq	56(%rax), %rax
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 160(%rsp)
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 168(%rsp)
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 40(%rsp)
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 48(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	24(%rsp), %rcx
	movq	%rbx, %r8
	movss	.LCPI0_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	916(%rax), %eax
.LBB0_119:                              #   in Loop: Header=BB0_117 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	addq	$136, %r15
	cmpq	%rcx, %r14
	jl	.LBB0_117
.LBB0_120:
	cmpb	$0, 20(%rsp)            # 1-byte Folded Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	jns	.LBB0_128
# BB#121:                               # %.preheader2074
	cmpl	$0, 788(%rcx)
	jle	.LBB0_128
# BB#122:                               # %.lr.ph2263
	xorl	%ebx, %ebx
	leaq	80(%rsp), %r15
	.p2align	4, 0x90
.LBB0_123:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_125 Depth 2
	movq	800(%rcx), %rax
	imulq	$88, %rbx, %rcx
	movups	16(%rax,%rcx), %xmm0
	movaps	%xmm0, 80(%rsp)
	movslq	32(%rax,%rcx), %rdx
	testq	%rdx, %rdx
	jle	.LBB0_127
# BB#124:                               # %.lr.ph2256
                                        #   in Loop: Header=BB0_123 Depth=1
	movss	80(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	(%rax,%rcx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_125:                              #   Parent Loop BB0_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rsi,%rdi,8), %rbp
	movss	72(%rsi,%rdi,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	20(%rbp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	24(%rbp), %xmm3
	addss	%xmm4, %xmm2
	addss	%xmm5, %xmm1
	addss	%xmm3, %xmm0
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB0_125
# BB#126:                               # %._crit_edge
                                        #   in Loop: Header=BB0_123 Depth=1
	movss	%xmm2, 80(%rsp)
	movss	%xmm1, 84(%rsp)
	movss	%xmm0, 88(%rsp)
.LBB0_127:                              #   in Loop: Header=BB0_123 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rax,%rcx), %rdx
	movq	%r15, %rsi
	callq	*80(%rbp)
	incq	%rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	788(%rcx), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_123
.LBB0_128:                              # %.loopexit2075
	movl	20(%rsp), %eax          # 4-byte Reload
	testb	$2, %ah
	je	.LBB0_130
# BB#129:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	1144(%rax), %rsi
	movq	$1065353216, 80(%rsp)   # imm = 0x3F800000
	movq	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 160(%rsp)
	movq	$1065353216, 168(%rsp)  # imm = 0x3F800000
	movl	$-1, (%rsp)
	leaq	80(%rsp), %rcx
	leaq	160(%rsp), %r8
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
.LBB0_130:
	movl	20(%rsp), %eax          # 4-byte Reload
	testb	$4, %ah
	je	.LBB0_132
# BB#131:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	1208(%rax), %rsi
	movabsq	$4575657221408423936, %rax # imm = 0x3F80000000000000
	movq	%rax, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	$1065353216, 160(%rsp)  # imm = 0x3F800000
	movq	$0, 168(%rsp)
	movl	$-1, (%rsp)
	leaq	80(%rsp), %rcx
	leaq	160(%rsp), %r8
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
.LBB0_132:
	movl	20(%rsp), %eax          # 4-byte Reload
	testb	$8, %ah
	je	.LBB0_134
# BB#133:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	1272(%rax), %rsi
	movabsq	$4575657221408423936, %rax # imm = 0x3F80000000000000
	movq	%rax, 80(%rsp)
	movq	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movq	$1065353216, 160(%rsp)  # imm = 0x3F800000
	movq	$0, 168(%rsp)
	movl	$-1, (%rsp)
	leaq	80(%rsp), %rcx
	leaq	160(%rsp), %r8
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
.LBB0_134:
	movl	20(%rsp), %eax          # 4-byte Reload
	testb	$16, %ah
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB0_143
# BB#135:                               # %.preheader
	cmpl	$0, 1044(%rcx)
	jle	.LBB0_143
# BB#136:                               # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_137:                              # =>This Inner Loop Header: Depth=1
	movq	1056(%rcx), %rax
	movq	(%rax,%rbx,8), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*40(%rax)
	cmpl	$1, %eax
	je	.LBB0_140
# BB#138:                               #   in Loop: Header=BB0_137 Depth=1
	testl	%eax, %eax
	jne	.LBB0_142
# BB#139:                               #   in Loop: Header=BB0_137 Depth=1
	leaq	8(%r12), %r15
	movq	%r15, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movss	56(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	60(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	48(%rax), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	32(%rax), %xmm2
	mulss	36(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	40(%rax), %xmm0
	addss	%xmm1, %xmm0
	addss	56(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 40(%rsp)
	movlps	%xmm1, 48(%rsp)
	leaq	32(%r12), %r13
	movq	%r13, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movss	72(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	76(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	80(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	48(%rax), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	32(%rax), %xmm2
	mulss	36(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	40(%rax), %xmm0
	addss	%xmm1, %xmm0
	addss	56(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 24(%rsp)
	movlps	%xmm1, 32(%rsp)
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
	movq	40(%rax), %rbp
	movq	%r15, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	leaq	48(%rax), %rsi
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	%r14, %rdi
	leaq	40(%rsp), %rdx
	leaq	80(%rsp), %r15
	movq	%r15, %rcx
	callq	*%rbp
	movq	(%r14), %rax
	movq	40(%rax), %rbp
	movq	%r13, %rdi
	movabsq	$4575657222473777152, %r13 # imm = 0x3F8000003F800000
	callq	_ZNK10btSoftBody4Body5xformEv
	leaq	48(%rax), %rsi
	movabsq	$4575657221408423936, %r12 # imm = 0x3F80000000000000
	movq	%r12, 80(%rsp)
	movq	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movq	%r14, %rdi
	leaq	24(%rsp), %rdx
	movq	%r15, %rcx
	callq	*%rbp
	movq	%r13, 336(%rsp)
	movq	$0, 344(%rsp)
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	movss	.LCPI0_8(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	addss	%xmm4, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	leaq	160(%rsp), %rbp
	movq	%rbp, %rdx
	leaq	336(%rsp), %r13
	movq	%r13, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_8(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	48(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movaps	.LCPI0_10(%rip), %xmm1  # xmm1 = <0,0.25,u,u>
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	40(%rsp), %rcx
	movd	%rcx, %xmm0
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_8(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	xorps	%xmm2, %xmm2
	addps	%xmm2, %xmm0
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*%rax
	movq	%r12, 336(%rsp)
	movq	$1065353216, 344(%rsp)  # imm = 0x3F800000
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movsd	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	addss	.LCPI0_8(%rip), %xmm1
	movaps	%xmm0, %xmm2
	shufps	$1, %xmm1, %xmm2        # xmm2 = xmm2[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm2      # xmm2 = xmm2[2,0],xmm1[2,3]
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm1, 88(%rsp)
	movss	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	addss	.LCPI0_8(%rip), %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movss	32(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	.LCPI0_21, %xmm3
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	addps	.LCPI0_10(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movq	24(%rsp), %rcx
	movd	%rcx, %xmm0
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	.LCPI0_8(%rip), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%rcx, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	addps	.LCPI0_22, %xmm0
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm2, 168(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	jmp	.LBB0_141
	.p2align	4, 0x90
.LBB0_140:                              #   in Loop: Header=BB0_137 Depth=1
	leaq	8(%r12), %rbp
	movq	%rbp, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movups	48(%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	leaq	32(%r12), %r15
	movq	%r15, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movups	48(%rax), %xmm0
	movaps	%xmm0, 160(%rsp)
	movq	%rbp, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movss	56(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	60(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	movss	20(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	addps	%xmm2, %xmm3
	movss	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm3, %xmm2
	movaps	%xmm2, 224(%rsp)        # 16-byte Spill
	mulss	32(%rax), %xmm1
	mulss	36(%rax), %xmm0
	addss	%xmm1, %xmm0
	mulss	40(%rax), %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm5, 240(%rsp)        # 16-byte Spill
	movq	%r15, %rdi
	callq	_ZNK10btSoftBody4Body5xformEv
	movss	72(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	76(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	80(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	movss	20(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	addps	%xmm2, %xmm3
	movss	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm3, %xmm2
	movaps	%xmm2, 256(%rsp)        # 16-byte Spill
	mulss	32(%rax), %xmm1
	mulss	36(%rax), %xmm0
	addss	%xmm1, %xmm0
	mulss	40(%rax), %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm5, 272(%rsp)        # 16-byte Spill
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movaps	.LCPI0_18(%rip), %xmm0  # xmm0 = <10,10,u,u>
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	mulps	%xmm0, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movss	.LCPI0_19(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm0, %xmm2
	movaps	%xmm2, 240(%rsp)        # 16-byte Spill
	movaps	80(%rsp), %xmm0
	addps	%xmm1, %xmm0
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 40(%rsp)
	movlps	%xmm2, 48(%rsp)
	movq	%rbx, %r13
	movabsq	$4575657222473777152, %rbx # imm = 0x3F8000003F800000
	movq	%rbx, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	%r14, %rdi
	leaq	80(%rsp), %rbp
	movq	%rbp, %rsi
	leaq	40(%rsp), %r15
	movq	%r15, %rdx
	leaq	24(%rsp), %r12
	movq	%r12, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	mulps	.LCPI0_18(%rip), %xmm1
	movaps	%xmm1, 256(%rsp)        # 16-byte Spill
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	mulss	.LCPI0_19(%rip), %xmm2
	movaps	%xmm2, 272(%rsp)        # 16-byte Spill
	movaps	80(%rsp), %xmm0
	addps	%xmm1, %xmm0
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 40(%rsp)
	movlps	%xmm2, 48(%rsp)
	movq	%rbx, 24(%rsp)
	movq	%r13, %rbx
	movq	$0, 32(%rsp)
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	addps	160(%rsp), %xmm1
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	addss	168(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movabsq	$4575657221408423936, %rcx # imm = 0x3F80000000000000
	movq	%rcx, %rbp
	movq	%rbp, 24(%rsp)
	movq	$1065353216, 32(%rsp)   # imm = 0x3F800000
	movq	%r14, %rdi
	leaq	160(%rsp), %r13
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	addps	160(%rsp), %xmm1
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	addss	168(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movq	%rbp, 24(%rsp)
	movq	$1065353216, 32(%rsp)   # imm = 0x3F800000
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
.LBB0_141:                              #   in Loop: Header=BB0_137 Depth=1
	callq	*%rax
.LBB0_142:                              #   in Loop: Header=BB0_137 Depth=1
	incq	%rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	1044(%rcx), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_137
.LBB0_143:                              # %.loopexit
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_144:
.Ltmp45:
	movq	%rax, %rbx
	jmp	.LBB0_145
.LBB0_146:
.Ltmp39:
	movq	%rax, %rbx
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_150
# BB#147:
	cmpb	$0, 112(%rsp)
	je	.LBB0_149
# BB#148:
.Ltmp40:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp41:
.LBB0_149:                              # %.noexc4.i871
	movq	$0, 104(%rsp)
.LBB0_150:
	movb	$1, 112(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 92(%rsp)
.LBB0_145:
	movq	224(%rsp), %rbp         # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB0_181
	jmp	.LBB0_182
.LBB0_151:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_152:
.Ltmp28:
	movq	%rax, %rbx
	movq	224(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB0_172
.LBB0_153:
.Ltmp22:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_157
# BB#154:
	cmpb	$0, 184(%rsp)
	je	.LBB0_156
# BB#155:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB0_156:                              # %.noexc4.i904
	movq	$0, 176(%rsp)
.LBB0_157:
	movb	$1, 184(%rsp)
	movq	$0, 176(%rsp)
	movq	$0, 164(%rsp)
	movq	224(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB0_172
.LBB0_158:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_159:                              # %.thread
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_160:
.Ltmp5:
	jmp	.LBB0_163
.LBB0_161:
.Ltmp11:
	jmp	.LBB0_163
.LBB0_162:
.Ltmp8:
.LBB0_163:
	movq	%rax, %rbx
	movq	208(%rsp), %rdi
	testq	%rdi, %rdi
	movq	224(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_167
# BB#164:
	cmpb	$0, 216(%rsp)
	je	.LBB0_166
# BB#165:
.Ltmp12:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp13:
.LBB0_166:                              # %.noexc.i822
	movq	$0, 208(%rsp)
.LBB0_167:
	movb	$1, 216(%rsp)
	movq	$0, 208(%rsp)
	movq	$0, 196(%rsp)
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_171
# BB#168:
	cmpb	$0, 184(%rsp)
	je	.LBB0_170
# BB#169:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB0_170:                              # %.noexc826
	movq	$0, 176(%rsp)
.LBB0_171:                              # %_ZN11HullLibraryD2Ev.exit
	movb	$1, 184(%rsp)
	movq	$0, 176(%rsp)
	movq	$0, 164(%rsp)
.LBB0_172:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_176
# BB#173:
	cmpb	$0, 152(%rsp)
	je	.LBB0_175
# BB#174:
.Ltmp29:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp30:
.LBB0_175:                              # %.noexc.i
	movq	$0, 144(%rsp)
.LBB0_176:
	movb	$1, 152(%rsp)
	movq	$0, 144(%rsp)
	movq	$0, 132(%rsp)
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_180
# BB#177:
	cmpb	$0, 112(%rsp)
	je	.LBB0_179
# BB#178:
.Ltmp35:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp36:
.LBB0_179:                              # %.noexc804
	movq	$0, 104(%rsp)
.LBB0_180:                              # %_ZN10HullResultD2Ev.exit
	movb	$1, 112(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 92(%rsp)
	testq	%rbp, %rbp
	je	.LBB0_182
.LBB0_181:
.Ltmp46:
	movq	%rbp, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp47:
.LBB0_182:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_183:
.Ltmp14:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_187
# BB#184:
	cmpb	$0, 184(%rsp)
	je	.LBB0_186
# BB#185:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB0_186:                              # %.noexc4.i825
	movq	$0, 176(%rsp)
.LBB0_187:
	movb	$1, 184(%rsp)
	movq	$0, 176(%rsp)
	movq	$0, 164(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_188:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_189:
.Ltmp31:
	movq	%rax, %rbx
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_193
# BB#190:
	cmpb	$0, 112(%rsp)
	je	.LBB0_192
# BB#191:
.Ltmp32:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp33:
.LBB0_192:                              # %.noexc4.i
	movq	$0, 104(%rsp)
.LBB0_193:
	movb	$1, 112(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 92(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_194:
.Ltmp34:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_195:
.Ltmp48:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi, .Lfunc_end0-_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin0   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp40-.Ltmp44         #   Call between .Ltmp44 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp12-.Ltmp24         #   Call between .Ltmp24 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp47-.Ltmp35         #   Call between .Ltmp35 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp15-.Ltmp47         #   Call between .Ltmp47 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii,@function
_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii: # @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
	movl	%edx, %r9d
	movq	1144(%rdi), %rax
	movq	$1065353216, 24(%rsp)   # imm = 0x3F800000
	movq	$1065353216, 32(%rsp)   # imm = 0x3F800000
	movabsq	$4575657222473777152, %rdx # imm = 0x3F8000003F800000
	movq	%rdx, 8(%rsp)
	movq	$1065353216, 16(%rsp)   # imm = 0x3F800000
	movl	%ecx, (%rsp)
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	addq	$40, %rsp
	retq
.Lfunc_end2:
	.size	_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii, .Lfunc_end2-_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_endproc

	.globl	_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii,@function
_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii: # @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 48
	movl	%edx, %r9d
	movq	1208(%rdi), %rax
	movabsq	$4575657221408423936, %rdx # imm = 0x3F80000000000000
	movq	%rdx, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	$0, 16(%rsp)
	movl	%ecx, (%rsp)
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	addq	$40, %rsp
	retq
.Lfunc_end3:
	.size	_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii, .Lfunc_end3-_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_endproc

	.globl	_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii,@function
_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii: # @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 48
	movl	%edx, %r9d
	movq	1272(%rdi), %rax
	movabsq	$4575657221408423936, %rdx # imm = 0x3F80000000000000
	movq	%rdx, 24(%rsp)
	movq	$1065353216, 32(%rsp)   # imm = 0x3F800000
	movq	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	$0, 16(%rsp)
	movl	%ecx, (%rsp)
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	addq	$40, %rsp
	retq
.Lfunc_end4:
	.size	_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii, .Lfunc_end4-_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii
	.cfi_endproc

	.section	.text._ZNK10btSoftBody4Body5xformEv,"axG",@progbits,_ZNK10btSoftBody4Body5xformEv,comdat
	.weak	_ZNK10btSoftBody4Body5xformEv
	.p2align	4, 0x90
	.type	_ZNK10btSoftBody4Body5xformEv,@function
_ZNK10btSoftBody4Body5xformEv:          # @_ZNK10btSoftBody4Body5xformEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	_ZGVZNK10btSoftBody4Body5xformEvE8identity(%rip), %al
	testb	%al, %al
	jne	.LBB5_9
# BB#1:
	movl	$_ZGVZNK10btSoftBody4Body5xformEvE8identity, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB5_9
# BB#2:
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB5_8
# BB#3:
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB5_8
# BB#4:
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB5_7
# BB#5:
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB5_7
# BB#6:
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB5_7:
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB5_8:
	movups	_ZZN11btTransform11getIdentityEvE17identityTransform(%rip), %xmm0
	movups	%xmm0, _ZZNK10btSoftBody4Body5xformEvE8identity(%rip)
	movups	_ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip), %xmm0
	movups	%xmm0, _ZZNK10btSoftBody4Body5xformEvE8identity+16(%rip)
	movups	_ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip), %xmm0
	movups	%xmm0, _ZZNK10btSoftBody4Body5xformEvE8identity+32(%rip)
	movups	_ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip), %xmm0
	movups	%xmm0, _ZZNK10btSoftBody4Body5xformEvE8identity+48(%rip)
	movl	$_ZGVZNK10btSoftBody4Body5xformEvE8identity, %edi
	callq	__cxa_guard_release
.LBB5_9:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_11
# BB#10:
	addq	$72, %rax
	popq	%rbx
	retq
.LBB5_11:
	movq	(%rbx), %rax
	testq	%rax, %rax
	leaq	96(%rax), %rcx
	movl	$_ZZNK10btSoftBody4Body5xformEvE8identity, %eax
	cmovneq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZNK10btSoftBody4Body5xformEv, .Lfunc_end5-_ZNK10btSoftBody4Body5xformEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb,@function
_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb: # @_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$3096, %rsp             # imm = 0xC18
.Lcfi24:
	.cfi_def_cfa_offset 3152
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r14
	movslq	820(%r14), %rax
	testq	%rax, %rax
	jle	.LBB6_17
# BB#1:                                 # %.lr.ph
	testb	%dl, %dl
	je	.LBB6_8
# BB#2:                                 # %.lr.ph.split.us.preheader
	xorl	%ebp, %ebp
	movl	$100, %r15d
	leaq	1040(%rsp), %r13
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	832(%r14), %rbx
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r13, %rdi
	callq	memset
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	-4(%rbx,%r15), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %esi
	movb	$1, %al
	movq	%r12, %rdi
	callq	sprintf
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	strcat
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movss	(%rbx,%r15), %xmm0      # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.1, %esi
	movb	$1, %al
	movq	%r12, %rdi
	callq	sprintf
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	strcat
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=1
	cmpb	$0, 1040(%rsp)
	je	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	leaq	-84(%rbx,%r15), %rsi
	movq	%r13, %rdx
	callq	*80(%rax)
.LBB6_7:                                #   in Loop: Header=BB6_3 Depth=1
	incq	%rbp
	movslq	820(%r14), %rax
	addq	$120, %r15
	cmpq	%rax, %rbp
	jl	.LBB6_3
	jmp	.LBB6_17
.LBB6_8:                                # %.lr.ph.split
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB6_13
# BB#9:                                 # %.lr.ph.split.split.us.preheader
	xorl	%ebp, %ebp
	movl	$100, %ebx
	leaq	1040(%rsp), %r12
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.split.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	832(%r14), %r13
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%r12, %rdi
	callq	memset
	movss	(%r13,%rbx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.1, %esi
	movb	$1, %al
	movq	%r15, %rdi
	callq	sprintf
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcat
	cmpb	$0, 1040(%rsp)
	je	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_10 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	leaq	-84(%r13,%rbx), %rsi
	movq	%r12, %rdx
	callq	*80(%rax)
.LBB6_12:                               #   in Loop: Header=BB6_10 Depth=1
	incq	%rbp
	movslq	820(%r14), %rax
	addq	$120, %rbx
	cmpq	%rax, %rbp
	jl	.LBB6_10
	jmp	.LBB6_17
.LBB6_13:                               # %.preheader
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB6_15
	.p2align	4, 0x90
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB6_14
.LBB6_15:                               # %.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB6_17
	.p2align	4, 0x90
.LBB6_16:                               # =>This Inner Loop Header: Depth=1
	addq	$8, %rcx
	cmpq	%rax, %rcx
	jl	.LBB6_16
.LBB6_17:                               # %._crit_edge
	addq	$3096, %rsp             # imm = 0xC18
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb, .Lfunc_end6-_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1056964608              # float 0.5
	.text
	.p2align	4, 0x90
	.type	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii,@function
_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii: # @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 208
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, %r15
	movq	%rcx, %r14
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbp, %rbp
	je	.LBB7_7
# BB#1:
	cmpq	$0, 48(%rbp)
	je	.LBB7_5
# BB#2:
	movl	208(%rsp), %eax
	cmpl	%eax, %r13d
	jl	.LBB7_4
# BB#3:
	testl	%eax, %eax
	jns	.LBB7_5
.LBB7_4:
	movq	40(%rbp), %rsi
	leal	1(%r13), %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, (%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movq	%r15, %r8
	movl	%r12d, %r9d
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	movq	48(%rbp), %rsi
	movl	208(%rsp), %eax
	movl	%eax, (%rsp)
	movq	%rbx, %rdi
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	%r14, %rcx
	movq	%r15, %r8
	movl	%r12d, %r9d
	callq	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
.LBB7_5:
	cmpl	%r12d, %r13d
	jl	.LBB7_7
# BB#6:
	movss	16(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm5
	addss	%xmm1, %xmm5
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
	addss	%xmm0, %xmm6
	movss	8(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	24(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	.LCPI7_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm3, %xmm4
	subss	%xmm9, %xmm1
	subss	%xmm8, %xmm0
	subss	%xmm7, %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm2
	movaps	%xmm5, %xmm8
	subss	%xmm1, %xmm8
	movaps	%xmm6, %xmm3
	subss	%xmm0, %xmm3
	movaps	%xmm4, %xmm7
	subss	%xmm2, %xmm7
	addss	%xmm5, %xmm1
	addss	%xmm6, %xmm0
	addss	%xmm4, %xmm2
	cmpq	$0, 48(%rbp)
	cmoveq	%r15, %r14
	movss	%xmm8, 16(%rsp)
	movss	%xmm3, 20(%rsp)
	movss	%xmm7, 24(%rsp)
	movl	$0, 28(%rsp)
	leaq	32(%rsp), %r13
	movss	%xmm1, 32(%rsp)
	movss	%xmm3, 36(%rsp)
	movss	%xmm7, 40(%rsp)
	movl	$0, 44(%rsp)
	leaq	48(%rsp), %r12
	movss	%xmm1, 48(%rsp)
	movss	%xmm0, 52(%rsp)
	movss	%xmm7, 56(%rsp)
	movl	$0, 60(%rsp)
	leaq	64(%rsp), %r15
	movss	%xmm8, 64(%rsp)
	movss	%xmm0, 68(%rsp)
	movss	%xmm7, 72(%rsp)
	movl	$0, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movss	%xmm3, 84(%rsp)
	movss	%xmm2, 88(%rsp)
	movl	$0, 92(%rsp)
	movss	%xmm1, 96(%rsp)
	movss	%xmm3, 100(%rsp)
	movss	%xmm2, 104(%rsp)
	movl	$0, 108(%rsp)
	movss	%xmm1, 112(%rsp)
	movss	%xmm0, 116(%rsp)
	movss	%xmm2, 120(%rsp)
	movl	$0, 124(%rsp)
	movss	%xmm8, 128(%rsp)
	movss	%xmm0, 132(%rsp)
	movss	%xmm2, 136(%rsp)
	movl	$0, 140(%rsp)
	movq	(%rbx), %rax
	leaq	16(%rsp), %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	80(%rsp), %r13
	movq	%r13, %rsi
	leaq	96(%rsp), %r12
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	leaq	112(%rsp), %rbp
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	128(%rsp), %r15
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	32(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	48(%rsp), %rsi
	leaq	112(%rsp), %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	64(%rsp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
.LBB7_7:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii, .Lfunc_end7-_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1065353216              # float 1
.LCPI8_2:
	.long	1092616192              # float 10
.LCPI8_3:
	.long	3184315597              # float -0.100000001
.LCPI8_4:
	.long	1036831949              # float 0.100000001
.LCPI8_5:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_1:
	.long	1092616192              # float 10
	.long	1092616192              # float 10
	.zero	4
	.zero	4
	.text
	.globl	_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw,@function
_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw: # @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi50:
	.cfi_def_cfa_offset 416
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	cmpb	$0, 537(%rbp)
	je	.LBB8_10
# BB#1:
	movups	608(%rbp), %xmm0
	movaps	%xmm0, 48(%rsp)
	movss	672(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 32(%rsp)         # 16-byte Spill
	movss	688(%rbp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	624(%rbp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	628(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	704(%rbp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, 144(%rsp)        # 16-byte Spill
	movss	632(%rbp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	676(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm9
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movss	692(%rbp), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm11, %xmm2
	addss	%xmm0, %xmm2
	movss	708(%rbp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm14
	mulss	%xmm13, %xmm14
	addss	%xmm2, %xmm14
	movss	640(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm7, %xmm6
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	mulps	%xmm0, %xmm6
	movss	644(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm1, %xmm4
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movss	648(%rbp), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm8, %xmm5
	unpcklps	%xmm15, %xmm5   # xmm5 = xmm5[0],xmm15[0],xmm5[1],xmm15[1]
	mulps	%xmm3, %xmm5
	movss	680(%rbp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	addps	%xmm4, %xmm5
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	movaps	%xmm6, %xmm3
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm7, %xmm3
	movss	696(%rbp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1]
	movaps	%xmm7, %xmm4
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movss	712(%rbp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm13, %xmm8   # xmm8 = xmm8[0],xmm13[0],xmm8[1],xmm13[1]
	movaps	%xmm9, %xmm12
	unpcklps	%xmm15, %xmm12  # xmm12 = xmm12[0],xmm15[0],xmm12[1],xmm15[1]
	mulps	%xmm8, %xmm12
	addps	%xmm4, %xmm12
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	mulss	%xmm6, %xmm2
	mulss	%xmm7, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm9, %xmm15
	movss	656(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm1, %xmm3
	movss	660(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm10
	addss	%xmm3, %xmm10
	movss	664(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	addss	%xmm10, %xmm8
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm11
	addss	%xmm4, %xmm11
	mulss	%xmm3, %xmm13
	addss	%xmm11, %xmm13
	mulss	%xmm1, %xmm6
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm3, %xmm9
	addss	%xmm7, %xmm9
	movaps	%xmm12, %xmm3
	shufps	$1, %xmm14, %xmm3       # xmm3 = xmm3[1,0],xmm14[0,0]
	movaps	%xmm14, 336(%rsp)       # 16-byte Spill
	shufps	$226, %xmm14, %xmm3     # xmm3 = xmm3[2,0],xmm14[2,3]
	xorps	%xmm1, %xmm1
	movaps	%xmm3, %xmm10
	mulps	%xmm1, %xmm10
	movaps	%xmm5, %xmm7
	addps	%xmm10, %xmm7
	movaps	%xmm5, 304(%rsp)        # 16-byte Spill
	mulps	%xmm1, %xmm5
	shufps	$0, %xmm5, %xmm0        # xmm0 = xmm0[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm0      # xmm0 = xmm0[2,0],xmm5[2,3]
	shufps	$0, %xmm10, %xmm15      # xmm15 = xmm15[0,0],xmm10[0,0]
	shufps	$226, %xmm10, %xmm15    # xmm15 = xmm15[2,0],xmm10[2,3]
	addps	%xmm0, %xmm15
	movaps	%xmm15, %xmm0
	shufps	$1, %xmm12, %xmm0       # xmm0 = xmm0[1,0],xmm12[0,0]
	shufps	$226, %xmm12, %xmm0     # xmm0 = xmm0[2,0],xmm12[2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	addps	%xmm0, %xmm7
	movaps	%xmm7, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm2, %xmm2
	movaps	%xmm13, %xmm11
	mulss	%xmm2, %xmm11
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm8, %xmm1
	addss	%xmm11, %xmm1
	movaps	%xmm9, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm6, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm13, 256(%rsp)       # 16-byte Spill
	movaps	%xmm15, 128(%rsp)       # 16-byte Spill
	movaps	%xmm9, 208(%rsp)        # 16-byte Spill
	movaps	%xmm12, 192(%rsp)       # 16-byte Spill
	jnp	.LBB8_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, 160(%rsp)       # 16-byte Spill
	movaps	%xmm11, 240(%rsp)       # 16-byte Spill
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	240(%rsp), %xmm11       # 16-byte Reload
	movaps	160(%rsp), %xmm10       # 16-byte Reload
	xorps	%xmm2, %xmm2
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movaps	192(%rsp), %xmm12       # 16-byte Reload
	movaps	208(%rsp), %xmm9        # 16-byte Reload
	movaps	128(%rsp), %xmm15       # 16-byte Reload
	movaps	256(%rsp), %xmm13       # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
.LBB8_3:                                # %.split
	movss	.LCPI8_0(%rip), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm4
	divss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	addps	%xmm5, %xmm3
	addps	96(%rsp), %xmm3         # 16-byte Folded Reload
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm8, %xmm2
	movaps	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm1, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_5
# BB#4:                                 # %call.sqrt273
	movaps	%xmm1, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movss	%xmm2, 96(%rsp)         # 4-byte Spill
	movaps	%xmm10, 160(%rsp)       # 16-byte Spill
	movaps	%xmm4, 112(%rsp)        # 16-byte Spill
	movaps	%xmm11, 240(%rsp)       # 16-byte Spill
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	240(%rsp), %xmm11       # 16-byte Reload
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	movaps	160(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI8_0(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm14
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movaps	192(%rsp), %xmm12       # 16-byte Reload
	movaps	208(%rsp), %xmm9        # 16-byte Reload
	movaps	128(%rsp), %xmm15       # 16-byte Reload
.LBB8_5:                                # %.split.split
	mulps	272(%rsp), %xmm7        # 16-byte Folded Reload
	mulss	176(%rsp), %xmm4        # 16-byte Folded Reload
	movaps	%xmm14, %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	mulss	%xmm6, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	addss	%xmm10, %xmm5
	shufps	$0, %xmm12, %xmm5       # xmm5 = xmm5[0,0],xmm12[0,0]
	shufps	$226, %xmm12, %xmm5     # xmm5 = xmm5[2,0],xmm12[2,3]
	addps	%xmm15, %xmm5
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm11, %xmm2
	addss	%xmm9, %xmm2
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_7
# BB#6:                                 # %call.sqrt274
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movss	%xmm2, 96(%rsp)         # 4-byte Spill
	movaps	%xmm4, 112(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	movss	96(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI8_0(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm14
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB8_7:                                # %.split.split.split
	divss	%xmm1, %xmm14
	movaps	%xmm14, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm5
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	mulss	%xmm2, %xmm14
	movaps	%xmm14, 64(%rsp)        # 16-byte Spill
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	mulps	.LCPI8_1(%rip), %xmm7
	mulss	.LCPI8_2(%rip), %xmm4
	addps	48(%rsp), %xmm7
	addss	56(%rsp), %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm7, (%rsp)
	movlps	%xmm0, 8(%rsp)
	movq	$1065353216, 16(%rsp)   # imm = 0x3F800000
	movq	$0, 24(%rsp)
	leaq	48(%rsp), %r15
	movq	%rsp, %rdx
	leaq	16(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	mulps	.LCPI8_1(%rip), %xmm2
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	mulss	.LCPI8_2(%rip), %xmm1
	addps	48(%rsp), %xmm2
	addss	56(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, (%rsp)
	movlps	%xmm0, 8(%rsp)
	movabsq	$4575657221408423936, %rcx # imm = 0x3F80000000000000
	movq	%rcx, 16(%rsp)
	movq	$0, 24(%rsp)
	movq	%rsp, %rdx
	leaq	16(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulps	.LCPI8_1(%rip), %xmm0
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	.LCPI8_2(%rip), %xmm2
	addps	48(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	addss	56(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, (%rsp)
	movlps	%xmm0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	$1065353216, 24(%rsp)   # imm = 0x3F800000
	movq	%rsp, %rdx
	leaq	16(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*%rax
	cmpl	$0, 548(%rbp)
	movq	%rbp, %rcx
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movaps	256(%rsp), %xmm9        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	304(%rsp), %xmm2        # 16-byte Reload
	movaps	208(%rsp), %xmm10       # 16-byte Reload
	movaps	192(%rsp), %xmm7        # 16-byte Reload
	jle	.LBB8_10
# BB#8:                                 # %.lr.ph
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	movl	$8, %r14d
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movq	560(%rcx), %rax
	movss	-8(%rax,%r14), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	-4(%rax,%r14), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movaps	336(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	(%rax,%r14), %xmm6      # xmm6 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm0, %xmm2
	movaps	%xmm6, %xmm4
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	mulps	%xmm7, %xmm4
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	addps	%xmm4, %xmm3
	movaps	%xmm5, %xmm2
	mulss	%xmm6, %xmm2
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm10, %xmm6
	addss	%xmm1, %xmm6
	movss	48(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	addps	%xmm3, %xmm4
	addss	56(%rsp), %xmm6
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	movss	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	addps	%xmm4, %xmm1
	movaps	%xmm1, %xmm0
	movq	$1065353216, 320(%rsp)  # imm = 0x3F800000
	movq	$1065353216, 328(%rsp)  # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movlps	%xmm1, (%rsp)
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, %xmm5
	movaps	%xmm5, 272(%rsp)        # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	shufps	$1, %xmm4, %xmm0        # xmm0 = xmm0[1,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	movss	.LCPI8_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	xorps	%xmm3, %xmm3
	addss	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movlps	%xmm0, 16(%rsp)
	xorps	%xmm0, %xmm0
	movss	%xmm6, %xmm0            # xmm0 = xmm6[0],xmm0[1,2,3]
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movlps	%xmm0, 8(%rsp)
	movlps	%xmm2, 24(%rsp)
	movq	%rbx, %rdi
	movq	%rsp, %r12
	movq	%r12, %rsi
	leaq	16(%rsp), %r12
	movq	%r12, %rdx
	leaq	320(%rsp), %r15
	movq	%r15, %rcx
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm0
	movss	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm0      # xmm0 = xmm0[2,0],xmm1[2,3]
	movlps	%xmm0, (%rsp)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	addss	.LCPI8_5, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	addss	.LCPI8_4(%rip), %xmm2
	movaps	%xmm1, %xmm0
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movlps	%xmm0, 16(%rsp)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movq	%rbx, %rdi
	movq	%rsp, %r13
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	*%rax
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm0
	movss	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, (%rsp)
	movlps	%xmm1, 8(%rsp)
	addss	.LCPI8_4(%rip), %xmm2
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	64(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	*%rax
	movq	296(%rsp), %rcx         # 8-byte Reload
	movaps	192(%rsp), %xmm7        # 16-byte Reload
	movaps	208(%rsp), %xmm10       # 16-byte Reload
	movaps	256(%rsp), %xmm9        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movaps	304(%rsp), %xmm2        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	incq	%rbp
	movslq	548(%rcx), %rax
	addq	$16, %r14
	cmpq	%rax, %rbp
	jl	.LBB8_9
.LBB8_10:
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw, .Lfunc_end8-_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw
	.cfi_endproc

	.globl	_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii,@function
_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii: # @_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:                                 # %.loopexit
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 80
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movl	%ecx, %r14d
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leal	2(%r14), %r12d
	movslq	%r12d, %rbx
	movl	$16, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rbp
	cmovoq	%rbp, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	cmovnoq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Znam
	movq	%rax, %rbp
	cmpl	$-1, %r14d
	jl	.LBB9_3
# BB#1:                                 # %.lr.ph52
	leal	1(%r14), %eax
	cvtsi2ssl	%eax, %xmm0
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movsd	(%r13), %xmm2           # xmm2 = mem[0],zero
	subps	%xmm1, %xmm2
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$8, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ecx, %xmm5
	divss	%xmm0, %xmm5
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm2, %xmm6
	addps	%xmm1, %xmm6
	mulss	%xmm4, %xmm5
	addss	%xmm3, %xmm5
	xorps	%xmm7, %xmm7
	movss	%xmm5, %xmm7            # xmm7 = xmm5[0],xmm7[1,2,3]
	movlps	%xmm6, -8(%rax)
	movlps	%xmm7, (%rax)
	movl	$1065353216, (%rbp,%rcx,4) # imm = 0x3F800000
	incq	%rcx
	addq	$16, %rax
	cmpq	%rbx, %rcx
	jl	.LBB9_2
.LBB9_3:                                # %._crit_edge53
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp49:
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r12d, %edx
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %rcx
	movq	%rbp, %r8
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp50:
# BB#4:
	movl	4(%rsp), %ebx           # 4-byte Reload
	testb	$1, %bl
	je	.LBB9_6
# BB#5:
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	movq	%r13, %rdi
	callq	_ZN10btSoftBody7setMassEif
.LBB9_6:
	testb	$2, %bl
	je	.LBB9_8
# BB#7:
	leal	1(%r14), %esi
	xorps	%xmm0, %xmm0
	movq	%r13, %rdi
	callq	_ZN10btSoftBody7setMassEif
.LBB9_8:
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_ZdaPv
	cmpl	$2, %r12d
	jl	.LBB9_11
# BB#9:                                 # %.lr.ph.preheader
	incl	%r14d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rsi), %ebx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebx, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	cmpl	%ebx, %r14d
	movl	%ebx, %esi
	jne	.LBB9_10
.LBB9_11:                               # %._crit_edge
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_12:
.Ltmp51:
	movq	%rax, %rbx
.Ltmp52:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp53:
# BB#13:                                # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_14:
.Ltmp54:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii, .Lfunc_end9-_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp49-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp52-.Ltmp50         #   Call between .Ltmp50 and .Ltmp52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	1                       #   On action: 1
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end9-.Ltmp53     #   Call between .Ltmp53 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib,@function
_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib: # @_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 112
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%rcx, %r13
	movq	%rsi, %r12
	xorl	%eax, %eax
	movq	%r9, 16(%rsp)           # 8-byte Spill
	cmpl	$2, %r9d
	jl	.LBB10_41
# BB#1:
	movl	112(%rsp), %r14d
	cmpl	$2, %r14d
	jl	.LBB10_41
# BB#2:                                 # %.loopexit198
	movq	%rdx, %r15
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	imull	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	%eax, %rbx
	movl	$16, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rbp
	cmovoq	%rbp, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	cmovnoq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Znam
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB10_8
# BB#3:                                 # %.lr.ph207
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB10_8
# BB#4:                                 # %.lr.ph207.split.us.preheader
	leal	-1(%r14), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movsd	(%r12), %xmm9           # xmm9 = mem[0],zero
	movsd	(%r13), %xmm11          # xmm11 = mem[0],zero
	subps	%xmm9, %xmm11
	movss	8(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm13         # xmm13 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm13
	movq	%r15, %rax
	movsd	(%rax), %xmm12          # xmm12 = mem[0],zero
	movq	48(%rsp), %r15          # 8-byte Reload
	movsd	(%r15), %xmm15          # xmm15 = mem[0],zero
	subps	%xmm12, %xmm15
	movss	8(%rax), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm14, %xmm0
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	cvtsi2ssl	%eax, %xmm1
	movslq	%ecx, %r9
	movl	%ecx, %ecx
	movl	%r14d, %r8d
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$8, %rax
	movq	%r9, %r10
	shlq	$4, %r10
	shlq	$2, %r9
	xorl	%ebp, %ebp
	movq	32(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph207.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_6 Depth 2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%ebp, %xmm6
	divss	(%rsp), %xmm6           # 4-byte Folded Reload
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm3, %xmm5
	mulps	%xmm11, %xmm5
	addps	%xmm9, %xmm5
	movaps	%xmm6, %xmm4
	mulss	%xmm13, %xmm4
	addss	%xmm10, %xmm4
	mulps	%xmm15, %xmm3
	addps	%xmm12, %xmm3
	mulss	%xmm0, %xmm6
	addss	%xmm14, %xmm6
	subps	%xmm5, %xmm3
	subss	%xmm4, %xmm6
	movq	%rdx, %rbx
	movq	%rax, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB10_6:                               #   Parent Loop BB10_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm7, %xmm7
	cvtsi2ssl	%edi, %xmm7
	divss	%xmm1, %xmm7
	movaps	%xmm6, %xmm2
	mulss	%xmm7, %xmm2
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm3, %xmm7
	addps	%xmm5, %xmm7
	addss	%xmm4, %xmm2
	xorps	%xmm8, %xmm8
	movss	%xmm2, %xmm8            # xmm8 = xmm2[0],xmm8[1,2,3]
	movlps	%xmm7, -8(%rsi)
	movlps	%xmm8, (%rsi)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	incq	%rdi
	addq	$16, %rsi
	addq	$4, %rbx
	cmpq	%rdi, %rcx
	jne	.LBB10_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB10_5 Depth=1
	incq	%rbp
	addq	%r10, %rax
	addq	%r9, %rdx
	cmpq	%r8, %rbp
	jne	.LBB10_5
.LBB10_8:                               # %._crit_edge208
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp55:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r8
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp56:
# BB#9:
	movl	120(%rsp), %ebx
	testb	$1, %bl
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB10_11
# BB#10:
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10btSoftBody7setMassEif
.LBB10_11:
	testb	$2, %bl
	je	.LBB10_13
# BB#12:
	leal	-1(%r13), %esi
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10btSoftBody7setMassEif
.LBB10_13:
	testb	$4, %bl
	je	.LBB10_15
# BB#14:
	leal	-1(%r14), %esi
	imull	%r13d, %esi
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10btSoftBody7setMassEif
.LBB10_15:
	testb	$8, %bl
	je	.LBB10_17
# BB#16:
	leal	-1(%r14), %eax
	imull	%r13d, %eax
	leal	-1(%r13,%rax), %esi
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10btSoftBody7setMassEif
.LBB10_17:
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_ZdaPv
	testl	%r14d, %r14d
	jle	.LBB10_40
# BB#18:                                # %.thread.preheader.lr.ph
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB10_19:                              # %.thread.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_25 Depth 2
                                        #     Child Loop BB10_29 Depth 2
	testl	%r13d, %r13d
	jle	.LBB10_20
# BB#23:                                # %.lr.ph
                                        #   in Loop: Header=BB10_19 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ebp
	imull	%r13d, %ebp
	leal	1(%rax), %r15d
	cmpl	%r14d, %r15d
	jge	.LBB10_24
# BB#28:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB10_19 Depth=1
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	movl	%r15d, %r12d
	imull	%r13d, %r12d
	xorl	%r15d, %r15d
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB10_29
	.p2align	4, 0x90
.LBB10_31:                              #   in Loop: Header=BB10_29 Depth=2
	leal	1(%r12,%rbx), %ebx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%ebx, %edx
	movl	%r13d, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	cmpb	$0, 128(%rsp)
	je	.LBB10_37
# BB#32:                                #   in Loop: Header=BB10_29 Depth=2
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%ebx, %edx
	jmp	.LBB10_36
	.p2align	4, 0x90
.LBB10_29:                              # %.lr.ph.split.us
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %ebx
	leal	1(%rbx), %r15d
	leal	(%rbp,%rbx), %r14d
	cmpl	%r13d, %r15d
	jge	.LBB10_35
# BB#30:                                #   in Loop: Header=BB10_29 Depth=2
	leal	1(%rbp,%rbx), %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%edx, 24(%rsp)          # 4-byte Spill
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	leal	(%r12,%rbx), %r13d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%rbx), %eax
	testb	$1, %al
	jne	.LBB10_31
# BB#33:                                #   in Loop: Header=BB10_29 Depth=2
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%r14d, %edx
	movl	24(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	leal	1(%r12,%rbx), %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%r14d, %edx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	cmpb	$0, 128(%rsp)
	je	.LBB10_37
# BB#34:                                #   in Loop: Header=BB10_29 Depth=2
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	jmp	.LBB10_36
	.p2align	4, 0x90
.LBB10_35:                              # %.thread.backedge.us.critedge
                                        #   in Loop: Header=BB10_29 Depth=2
	addl	%r12d, %ebx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r14d, %esi
	movl	%ebx, %edx
.LBB10_36:                              # %.thread.backedge.us
                                        #   in Loop: Header=BB10_29 Depth=2
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.LBB10_37:                              # %.thread.backedge.us
                                        #   in Loop: Header=BB10_29 Depth=2
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r15d, %r13d
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jne	.LBB10_29
# BB#38:                                #   in Loop: Header=BB10_19 Depth=1
	movl	112(%rsp), %r14d
	movl	48(%rsp), %r15d         # 4-byte Reload
	jmp	.LBB10_39
	.p2align	4, 0x90
.LBB10_20:                              # %.thread.preheader..thread._crit_edge_crit_edge
                                        #   in Loop: Header=BB10_19 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	incl	%eax
	movl	%eax, %r15d
	jmp	.LBB10_39
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB10_19 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_25:                              # %.lr.ph.split
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rax), %ebx
	cmpl	%r13d, %ebx
	jge	.LBB10_27
# BB#26:                                #   in Loop: Header=BB10_25 Depth=2
	leal	(%rbp,%rax), %esi
	leal	1(%rbp,%rax), %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.LBB10_27:                              # %.thread.backedge
                                        #   in Loop: Header=BB10_25 Depth=2
	cmpl	%ebx, %r13d
	movl	%ebx, %eax
	jne	.LBB10_25
.LBB10_39:                              # %.thread._crit_edge
                                        #   in Loop: Header=BB10_19 Depth=1
	cmpl	%r14d, %r15d
	movl	%r15d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB10_19
.LBB10_40:
	movq	(%rsp), %rax            # 8-byte Reload
.LBB10_41:                              # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_21:
.Ltmp57:
	movq	%rax, %rbx
.Ltmp58:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp59:
# BB#22:                                # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_42:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib, .Lfunc_end10-_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp55-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp56         #   Call between .Ltmp56 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end10-.Ltmp59    #   Call between .Ltmp59 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf,@function
_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf: # @_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 176
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%rcx, %rbp
	movq	%rsi, %r12
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpl	$2, %r13d
	jl	.LBB11_44
# BB#1:
	movl	176(%rsp), %r15d
	cmpl	$2, %r15d
	jl	.LBB11_44
# BB#2:                                 # %.loopexit267
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	imull	%r13d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movslq	%eax, %r14
	movl	$16, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rbx
	cmovoq	%rbx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	cmovnoq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %r14
	testl	%r15d, %r15d
	jle	.LBB11_8
# BB#3:                                 # %.lr.ph287
	testl	%r13d, %r13d
	jle	.LBB11_8
# BB#4:                                 # %.lr.ph287.split.us.preheader
	leal	-1(%r15), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movsd	(%r12), %xmm9           # xmm9 = mem[0],zero
	movsd	(%rbp), %xmm11          # xmm11 = mem[0],zero
	subps	%xmm9, %xmm11
	movss	8(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm13         # xmm13 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm13
	movq	40(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx), %xmm12          # xmm12 = mem[0],zero
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	(%rax), %xmm15          # xmm15 = mem[0],zero
	subps	%xmm12, %xmm15
	movss	8(%rcx), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm14, %xmm0
	leal	-1(%r13), %eax
	cvtsi2ssl	%eax, %xmm1
	movslq	%r13d, %r9
	movl	%r13d, %ecx
	movl	%r15d, %r8d
	movq	8(%rsp), %r11           # 8-byte Reload
	addq	$8, %r11
	movq	%r9, %r10
	shlq	$4, %r10
	shlq	$2, %r9
	xorl	%eax, %eax
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph287.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_6 Depth 2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	divss	4(%rsp), %xmm6          # 4-byte Folded Reload
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm3, %xmm5
	mulps	%xmm11, %xmm5
	addps	%xmm9, %xmm5
	movaps	%xmm6, %xmm4
	mulss	%xmm13, %xmm4
	addss	%xmm10, %xmm4
	mulps	%xmm15, %xmm3
	addps	%xmm12, %xmm3
	mulss	%xmm0, %xmm6
	addss	%xmm14, %xmm6
	subps	%xmm5, %xmm3
	subss	%xmm4, %xmm6
	movq	%rdx, %rbx
	movq	%r11, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_6:                               #   Parent Loop BB11_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm7, %xmm7
	cvtsi2ssl	%edi, %xmm7
	divss	%xmm1, %xmm7
	movaps	%xmm6, %xmm2
	mulss	%xmm7, %xmm2
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm3, %xmm7
	addps	%xmm5, %xmm7
	addss	%xmm4, %xmm2
	xorps	%xmm8, %xmm8
	movss	%xmm2, %xmm8            # xmm8 = xmm2[0],xmm8[1,2,3]
	movlps	%xmm7, -8(%rsi)
	movlps	%xmm8, (%rsi)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	incq	%rdi
	addq	$16, %rsi
	addq	$4, %rbx
	cmpq	%rdi, %rcx
	jne	.LBB11_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB11_5 Depth=1
	incq	%rax
	addq	%r10, %r11
	addq	%r9, %rdx
	cmpq	%r8, %rax
	jne	.LBB11_5
.LBB11_8:                               # %._crit_edge288
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp61:
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r14, %r8
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp62:
# BB#9:
	movl	184(%rsp), %ecx
	testb	$1, %cl
	movl	%ecx, %ebp
	je	.LBB11_11
# BB#10:
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_11:
	testb	$2, %cl
	je	.LBB11_13
# BB#12:
	leal	-1(%r13), %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_13:
	testb	$4, %cl
	je	.LBB11_15
# BB#14:
	leal	-1(%r15), %esi
	imull	%r13d, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_15:
	testb	$8, %cl
	je	.LBB11_17
# BB#16:
	leal	-1(%r15), %eax
	imull	%r13d, %eax
	leal	-1(%r13,%rax), %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_17:
	testb	$16, %cl
	je	.LBB11_19
# BB#18:
	leal	-1(%r13), %eax
	shrl	$31, %eax
	leal	-1(%r13,%rax), %esi
	sarl	%esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_19:
	testb	$32, %cl
	je	.LBB11_21
# BB#20:
	leal	-1(%r15), %eax
	shrl	$31, %eax
	leal	-1(%r15,%rax), %esi
	sarl	%esi
	imull	%r13d, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_21:
	testb	$64, %cl
	je	.LBB11_23
# BB#22:
	leal	-1(%r15), %eax
	shrl	$31, %eax
	leal	-1(%r15,%rax), %eax
	sarl	%eax
	imull	%r13d, %eax
	leal	-1(%r13,%rax), %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_23:
	testb	%cl, %cl
	jns	.LBB11_25
# BB#24:
	leal	-1(%r15), %eax
	imull	%r13d, %eax
	leal	-1(%r13), %ecx
	shrl	$31, %ecx
	leal	-1(%r13,%rcx), %esi
	sarl	%esi
	addl	%eax, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
	movl	%ebp, %ecx
.LBB11_25:
	testb	$1, %ch
	je	.LBB11_27
# BB#26:
	leal	-1(%r15), %eax
	shrl	$31, %eax
	leal	-1(%r15,%rax), %eax
	sarl	%eax
	imull	%r13d, %eax
	leal	-1(%r13), %ecx
	shrl	$31, %ecx
	leal	-1(%r13,%rcx), %esi
	sarl	%esi
	addl	%eax, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	_ZN10btSoftBody7setMassEif
.LBB11_27:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_ZdaPv
	testl	%r15d, %r15d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jle	.LBB11_44
# BB#28:                                # %.thread.preheader.lr.ph
	movq	200(%rsp), %rcx
	leal	-1(%r13), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm2
	movss	%xmm2, 88(%rsp)         # 4-byte Spill
	leal	-1(%r15), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm0, %xmm1
	movss	%xmm1, 84(%rsp)         # 4-byte Spill
	leal	-2(%r15), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	leaq	44(%rcx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	movq	%r13, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_29:                              # %.thread.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_39 Depth 2
                                        #     Child Loop BB11_32 Depth 2
	movl	%r12d, %eax
	leal	1(%rax), %r12d
	testl	%r13d, %r13d
	jle	.LBB11_43
# BB#30:                                # %.lr.ph.lr.ph
                                        #   in Loop: Header=BB11_29 Depth=1
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	cmpl	%r15d, %r12d
	jge	.LBB11_31
# BB#38:                                # %.lr.ph.split.us.lr.ph
                                        #   in Loop: Header=BB11_29 Depth=1
	movl	80(%rsp), %ecx          # 4-byte Reload
	subl	%eax, %ecx
	movl	76(%rsp), %edx          # 4-byte Reload
	subl	%eax, %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	movss	84(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	movl	36(%rsp), %edx          # 4-byte Reload
	movslq	%edx, %rax
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %r14
	xorl	%r13d, %r13d
	movl	%edx, %ebx
	movq	64(%rsp), %rbp          # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movl	%r12d, 92(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB11_39:                              # %.lr.ph.split.us
                                        #   Parent Loop BB11_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	leal	1(%r13), %eax
	movq	104(%rsp), %r15         # 8-byte Reload
	leal	(%r15,%rbp), %ebx
	movl	%eax, 56(%rsp)          # 4-byte Spill
	cmpl	%r15d, %eax
	jge	.LBB11_42
# BB#40:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB11_39 Depth=2
	leal	1(%rbp), %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%edx, 4(%rsp)           # 4-byte Spill
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	leal	1(%r15,%rbp), %r15d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	cmpq	$0, 200(%rsp)
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	je	.LBB11_45
# BB#41:                                #   in Loop: Header=BB11_39 Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r13d, %xmm1
	movss	88(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 100(%rsp)        # 4-byte Spill
	movss	%xmm1, -44(%r14)
	movss	48(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, -40(%r14)
	movss	%xmm1, -36(%r14)
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -32(%r14)
	movl	56(%rsp), %r13d         # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r13d, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 96(%rsp)         # 4-byte Spill
	movss	%xmm2, -28(%r14)
	movss	%xmm1, -24(%r14)
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%r15d, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%ebp, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -20(%r14)
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -16(%r14)
	movss	%xmm0, -12(%r14)
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -8(%r14)
	movss	100(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -4(%r14)
	movss	%xmm0, (%r14)
	movl	176(%rsp), %r15d
	movq	%r12, %rdi
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB11_46
	.p2align	4, 0x90
.LBB11_45:                              # %.critedge
                                        #   in Loop: Header=BB11_39 Depth=2
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%r15d, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%ebp, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
	movl	176(%rsp), %r15d
	movq	%r12, %rdi
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	56(%rsp), %r13d         # 4-byte Reload
.LBB11_46:                              #   in Loop: Header=BB11_39 Depth=2
	cmpb	$0, 192(%rsp)
	movl	92(%rsp), %r12d         # 4-byte Reload
	je	.LBB11_48
# BB#47:                                #   in Loop: Header=BB11_39 Depth=2
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	%ebp, %esi
	movl	20(%rsp), %edx          # 4-byte Reload
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.LBB11_48:                              # %.thread.outer
                                        #   in Loop: Header=BB11_39 Depth=2
	addl	$12, %ebx
	addq	$48, %r14
	cmpl	%r15d, %r12d
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebp
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
	jl	.LBB11_39
.LBB11_31:                              # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB11_29 Depth=1
	xorl	%eax, %eax
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB11_32:                              # %.lr.ph.split
                                        #   Parent Loop BB11_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rax), %ebx
	cmpl	%r13d, %ebx
	jge	.LBB11_34
# BB#33:                                #   in Loop: Header=BB11_32 Depth=2
	leal	(%rbp,%rax), %esi
	leal	1(%rbp,%rax), %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.LBB11_34:                              # %.thread.backedge
                                        #   in Loop: Header=BB11_32 Depth=2
	cmpl	%ebx, %r13d
	movl	%ebx, %eax
	jne	.LBB11_32
# BB#35:                                #   in Loop: Header=BB11_29 Depth=1
	movl	36(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB11_43
.LBB11_42:                              # %.thread.backedge.us.critedge
                                        #   in Loop: Header=BB11_29 Depth=1
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	movq	%r15, %r13
	movl	176(%rsp), %r15d
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	.p2align	4, 0x90
.LBB11_43:                              # %.thread.outer._crit_edge
                                        #   in Loop: Header=BB11_29 Depth=1
	addl	%r13d, %ebp
	cmpl	%r15d, %r12d
	jne	.LBB11_29
.LBB11_44:                              # %.loopexit
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_36:
.Ltmp63:
	movq	%rax, %rbp
.Ltmp64:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp65:
# BB#37:                                # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB11_49:
.Ltmp66:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf, .Lfunc_end11-_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp61-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin3   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp64-.Ltmp62         #   Call between .Ltmp62 and .Ltmp64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin3   #     jumps to .Ltmp66
	.byte	1                       #   On action: 1
	.long	.Ltmp65-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp65    #   Call between .Ltmp65 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btSoftBodyHelpers11CalculateUVEiiiii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers11CalculateUVEiiiii,@function
_ZN17btSoftBodyHelpers11CalculateUVEiiiii: # @_ZN17btSoftBodyHelpers11CalculateUVEiiiii
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$3, %r8d
	ja	.LBB12_1
# BB#2:
	movl	%r8d, %eax
	jmpq	*.LJTI12_0(,%rax,8)
.LBB12_3:
	decl	%edi
	cvtsi2ssl	%edi, %xmm0
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	jmp	.LBB12_8
.LBB12_1:
	xorps	%xmm0, %xmm0
	retq
.LBB12_4:
	decl	%esi
	cvtsi2ssl	%esi, %xmm0
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	jmp	.LBB12_5
.LBB12_6:
	leal	-1(%rsi), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	addl	$-2, %esi
.LBB12_5:
	subl	%ecx, %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	mulss	%xmm1, %xmm0
	retq
.LBB12_7:
	decl	%edi
	cvtsi2ssl	%edi, %xmm0
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	incl	%edx
.LBB12_8:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	mulss	%xmm1, %xmm0
	retq
.Lfunc_end12:
	.size	_ZN17btSoftBodyHelpers11CalculateUVEiiiii, .Lfunc_end12-_ZN17btSoftBodyHelpers11CalculateUVEiiiii
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_3
	.quad	.LBB12_4
	.quad	.LBB12_6
	.quad	.LBB12_7

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1056964608              # float 0.5
.LCPI13_1:
	.long	3212836864              # float -1
.LCPI13_2:
	.long	1078530011              # float 3.14159274
.LCPI13_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i,@function
_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i: # @_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 112
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r15
	leal	3(%rcx), %r12d
	cmpl	$-2, %ecx
	jl	.LBB13_19
# BB#1:
	testl	%r12d, %r12d
	js	.LBB13_19
# BB#2:
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%r12d, %rbx
	movq	%rbx, %rdi
	shlq	$4, %rdi
.Ltmp67:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp68:
# BB#3:                                 # %.lr.ph.i
	leaq	-1(%rbx), %rcx
	movq	%rbx, %rdx
	xorl	%edi, %edi
	andq	$7, %rdx
	je	.LBB13_6
# BB#4:                                 # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB13_5:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	12(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdi, %rdx
	jne	.LBB13_5
.LBB13_6:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	movq	%r15, 40(%rsp)          # 8-byte Spill
	cmpq	$7, %rcx
	jb	.LBB13_9
# BB#7:                                 # %.lr.ph.i.new
	subq	%rdi, %rbx
	shlq	$4, %rdi
	leaq	112(%rax,%rdi), %rcx
	.p2align	4, 0x90
.LBB13_8:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movups	12(%rsp), %xmm0
	movups	%xmm0, -112(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -96(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -80(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -64(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -48(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -32(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, -16(%rcx)
	movups	12(%rsp), %xmm0
	movups	%xmm0, (%rcx)
	subq	$-128, %rcx
	addq	$-8, %rbx
	jne	.LBB13_8
.LBB13_9:                               # %.preheader.lr.ph.i
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	%rax, %rbx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB13_10:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_16 Depth 2
	xorps	%xmm2, %xmm2
	testl	%r14d, %r14d
	je	.LBB13_11
# BB#15:                                # %.lr.ph.i23.preheader
                                        #   in Loop: Header=BB13_10 Depth=1
	movl	%r14d, %eax
	movaps	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB13_16:                              # %.lr.ph.i23
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$1, %al
	je	.LBB13_18
# BB#17:                                # %.lr.ph.i23
                                        #   in Loop: Header=BB13_16 Depth=2
	addss	%xmm0, %xmm2
.LBB13_18:                              # %.lr.ph.i23
                                        #   in Loop: Header=BB13_16 Depth=2
	mulss	%xmm1, %xmm0
	sarl	%eax
	jne	.LBB13_16
.LBB13_11:                              # %._crit_edge.i
                                        #   in Loop: Header=BB13_10 Depth=1
	addss	%xmm2, %xmm2
	addss	.LCPI13_1(%rip), %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	leal	(%r14,%r14), %eax
	cvtsi2ssl	%eax, %xmm3
	movss	.LCPI13_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm0, %xmm3
	divss	28(%rsp), %xmm3         # 4-byte Folded Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	movss	.LCPI13_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	jnp	.LBB13_13
# BB#12:                                # %call.sqrt
                                        #   in Loop: Header=BB13_10 Depth=1
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB13_13:                              # %._crit_edge.i.split
                                        #   in Loop: Header=BB13_10 Depth=1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movaps	%xmm3, %xmm0
	callq	cosf
	mulss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	mulss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, (%r15)
	movss	%xmm0, 4(%r15)
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%r15)
	movl	$0, 12(%r15)
	leaq	16(%r15), %r15
	incl	%r14d
	cmpl	%r12d, %r14d
	movss	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jne	.LBB13_10
# BB#14:
	movq	%rbx, %r14
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$-2, %ecx
	jge	.LBB13_21
	jmp	.LBB13_23
.LBB13_19:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit.thread
	xorl	%r14d, %r14d
	cmpl	$-2, %ecx
	jl	.LBB13_23
.LBB13_21:                              # %.lr.ph
	movslq	%r12d, %rax
	leaq	8(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_22:                              # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	8(%r13), %xmm0
	movsd	(%rbp), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	8(%rbp), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, -8(%rcx)
	movlps	%xmm1, (%rcx)
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rax, %rdx
	jl	.LBB13_22
.LBB13_23:                              # %._crit_edge
.Ltmp70:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%r12d, %edx
	callq	_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i
	movq	%rax, %rbx
.Ltmp71:
# BB#24:
	testq	%r14, %r14
	je	.LBB13_26
# BB#25:
	movq	%r14, %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB13_26:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit21
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_31:                              # %.thread
.Ltmp69:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_27:
.Ltmp72:
	movq	%rax, %rbx
	movq	%r14, %rdi
	testq	%r14, %r14
	je	.LBB13_29
# BB#28:
.Ltmp73:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp74:
.LBB13_29:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_30:
.Ltmp75:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i, .Lfunc_end13-_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp67-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin4   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin4   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp73-.Ltmp71         #   Call between .Ltmp71 and .Ltmp73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin4   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end13-.Ltmp74    #   Call between .Ltmp74 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i,@function
_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i: # @_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 240
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, 152(%rsp)
	movl	%edx, 156(%rsp)
	movq	%rsi, 160(%rsp)
	movabsq	$4216233944099586064, %rax # imm = 0x3A83126F00000010
	movq	%rax, 168(%rsp)
	movb	$1, 104(%rsp)
	movq	$0, 96(%rsp)
	movl	$0, 84(%rsp)
	movl	$0, 88(%rsp)
	movb	$1, 144(%rsp)
	movq	$0, 136(%rsp)
	movl	$0, 124(%rsp)
	movl	$0, 128(%rsp)
	movb	$1, 72(%rsp)
	movl	$0, 76(%rsp)
	movl	$0, 112(%rsp)
	movl	$0, 116(%rsp)
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 16(%rsp)
	movb	$1, 64(%rsp)
	movq	$0, 56(%rsp)
	movl	$0, 44(%rsp)
	movl	$0, 48(%rsp)
	movl	%edx, 176(%rsp)
.Ltmp76:
	leaq	8(%rsp), %rdi
	leaq	152(%rsp), %rsi
	leaq	72(%rsp), %rdx
	callq	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
.Ltmp77:
# BB#1:
.Ltmp78:
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp79:
# BB#2:                                 # %_ZN17btCollisionObjectnwEm.exit
	movl	76(%rsp), %edx
	movq	96(%rsp), %rcx
.Ltmp81:
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp82:
# BB#3:                                 # %.preheader
	cmpl	$0, 112(%rsp)
	jle	.LBB14_13
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	136(%rsp), %rax
	movl	-8(%rax,%rbp), %r14d
	movl	-4(%rax,%rbp), %r12d
	movl	(%rax,%rbp), %r13d
	cmpl	%r12d, %r14d
	jge	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_5 Depth=1
.Ltmp86:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp87:
.LBB14_7:                               #   in Loop: Header=BB14_5 Depth=1
	cmpl	%r13d, %r12d
	jge	.LBB14_9
# BB#8:                                 #   in Loop: Header=BB14_5 Depth=1
.Ltmp88:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp89:
.LBB14_9:                               #   in Loop: Header=BB14_5 Depth=1
	cmpl	%r14d, %r13d
	jge	.LBB14_11
# BB#10:                                #   in Loop: Header=BB14_5 Depth=1
.Ltmp90:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	%r13d, %esi
	movl	%r14d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp91:
.LBB14_11:                              #   in Loop: Header=BB14_5 Depth=1
.Ltmp92:
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	movl	%r13d, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
.Ltmp93:
# BB#12:                                #   in Loop: Header=BB14_5 Depth=1
	incq	%rbx
	movslq	112(%rsp), %rax
	addq	$12, %rbp
	cmpq	%rax, %rbx
	jl	.LBB14_5
.LBB14_13:                              # %._crit_edge
.Ltmp95:
	leaq	8(%rsp), %rdi
	leaq	72(%rsp), %rsi
	callq	_ZN11HullLibrary13ReleaseResultER10HullResult
.Ltmp96:
# BB#14:
.Ltmp97:
	movq	%r15, %rdi
	callq	_ZN10btSoftBody20randomizeConstraintsEv
.Ltmp98:
# BB#15:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_19
# BB#16:
	cmpb	$0, 64(%rsp)
	je	.LBB14_18
# BB#17:
.Ltmp108:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp109:
.LBB14_18:                              # %.noexc.i56
	movq	$0, 56(%rsp)
.LBB14_19:
	movb	$1, 64(%rsp)
	movq	$0, 56(%rsp)
	movq	$0, 44(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_23
# BB#20:
	cmpb	$0, 32(%rsp)
	je	.LBB14_22
# BB#21:
.Ltmp114:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp115:
.LBB14_22:                              # %.noexc60
	movq	$0, 24(%rsp)
.LBB14_23:
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_27
# BB#24:
	cmpb	$0, 144(%rsp)
	je	.LBB14_26
# BB#25:
.Ltmp126:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp127:
.LBB14_26:                              # %.noexc.i65
	movq	$0, 136(%rsp)
.LBB14_27:
	movb	$1, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 124(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_31
# BB#28:
	cmpb	$0, 104(%rsp)
	je	.LBB14_30
# BB#29:
	callq	_Z21btAlignedFreeInternalPv
.LBB14_30:
	movq	$0, 96(%rsp)
.LBB14_31:                              # %_ZN10HullResultD2Ev.exit69
	movq	%r15, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_39:
.Ltmp128:
	movq	%rax, %r14
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_43
# BB#40:
	cmpb	$0, 104(%rsp)
	je	.LBB14_42
# BB#41:
.Ltmp129:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp130:
	jmp	.LBB14_42
.LBB14_44:
.Ltmp131:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_45:
.Ltmp116:
	movq	%rax, %r14
	jmp	.LBB14_57
.LBB14_34:
.Ltmp110:
	movq	%rax, %r14
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_56
# BB#35:
	cmpb	$0, 32(%rsp)
	je	.LBB14_55
# BB#36:
.Ltmp111:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp112:
	jmp	.LBB14_55
.LBB14_37:
.Ltmp113:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_32:
.Ltmp83:
	movq	%rax, %r14
.Ltmp84:
	movq	%r15, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp85:
	jmp	.LBB14_48
.LBB14_38:
.Ltmp99:
	jmp	.LBB14_47
.LBB14_46:
.Ltmp80:
	jmp	.LBB14_47
.LBB14_33:
.Ltmp94:
.LBB14_47:                              # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rax, %r14
.LBB14_48:                              # %_ZN17btCollisionObjectdlEPv.exit
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_52
# BB#49:
	cmpb	$0, 64(%rsp)
	je	.LBB14_51
# BB#50:
.Ltmp100:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp101:
.LBB14_51:                              # %.noexc.i48
	movq	$0, 56(%rsp)
.LBB14_52:
	movb	$1, 64(%rsp)
	movq	$0, 56(%rsp)
	movq	$0, 44(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_56
# BB#53:
	cmpb	$0, 32(%rsp)
	je	.LBB14_55
# BB#54:
.Ltmp106:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp107:
.LBB14_55:                              # %.noexc52
	movq	$0, 24(%rsp)
.LBB14_56:                              # %_ZN11HullLibraryD2Ev.exit
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
.LBB14_57:
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_61
# BB#58:
	cmpb	$0, 144(%rsp)
	je	.LBB14_60
# BB#59:
.Ltmp117:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp118:
.LBB14_60:                              # %.noexc.i
	movq	$0, 136(%rsp)
.LBB14_61:
	movb	$1, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 124(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_43
# BB#62:
	cmpb	$0, 104(%rsp)
	je	.LBB14_42
# BB#63:
.Ltmp123:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp124:
.LBB14_42:                              # %.noexc4.i68
	movq	$0, 96(%rsp)
.LBB14_43:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_64:
.Ltmp102:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_68
# BB#65:
	cmpb	$0, 32(%rsp)
	je	.LBB14_67
# BB#66:
.Ltmp103:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp104:
.LBB14_67:                              # %.noexc4.i51
	movq	$0, 24(%rsp)
.LBB14_68:
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB14_69:
.Ltmp105:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_70:
.Ltmp119:
	movq	%rax, %rbx
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_74
# BB#71:
	cmpb	$0, 104(%rsp)
	je	.LBB14_73
# BB#72:
.Ltmp120:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp121:
.LBB14_73:                              # %.noexc4.i
	movq	$0, 96(%rsp)
.LBB14_74:
	movb	$1, 104(%rsp)
	movq	$0, 96(%rsp)
	movq	$0, 84(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB14_75:
.Ltmp122:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_76:
.Ltmp125:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i, .Lfunc_end14-_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp76-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp79-.Ltmp76         #   Call between .Ltmp76 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin5   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin5   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp93-.Ltmp86         #   Call between .Ltmp86 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin5   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp98-.Ltmp95         #   Call between .Ltmp95 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin5   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin5  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin5  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin5  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp129-.Ltmp127       #   Call between .Ltmp127 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin5  #     jumps to .Ltmp131
	.byte	1                       #   On action: 1
	.long	.Ltmp111-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin5  #     jumps to .Ltmp113
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin5   # >> Call Site 11 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin5  #     jumps to .Ltmp102
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	1                       #   On action: 1
	.long	.Ltmp117-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin5  #     jumps to .Ltmp119
	.byte	1                       #   On action: 1
	.long	.Ltmp123-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	1                       #   On action: 1
	.long	.Ltmp124-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp103-.Ltmp124       #   Call between .Ltmp124 and .Ltmp103
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin5  #     jumps to .Ltmp105
	.byte	1                       #   On action: 1
	.long	.Ltmp120-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin5  #     jumps to .Ltmp122
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii,@function
_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii: # @_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 144
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r13
	movq	%rsi, %r12
	leal	(%r14,%r14,2), %ebx
	testl	%r14d, %r14d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jle	.LBB15_7
# BB#1:                                 # %.lr.ph199.preheader
	movslq	%ebx, %rax
	testq	%rax, %rax
	movl	$1, %edx
	cmovgq	%rax, %rdx
	cmpq	$8, %rdx
	jb	.LBB15_5
# BB#2:                                 # %min.iters.checked
	movabsq	$9223372036854775800, %rcx # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdx, %rcx
	je	.LBB15_5
# BB#3:                                 # %vector.body.preheader
	leaq	-8(%rcx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB15_8
# BB#4:                                 # %vector.body.prol
	movdqu	(%r13), %xmm2
	movdqu	16(%r13), %xmm3
	pxor	%xmm4, %xmm4
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm4, %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm1
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB15_9
	jmp	.LBB15_11
.LBB15_5:
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph199
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rcx,4), %edx
	cmpl	%ebp, %edx
	cmovgel	%edx, %ebp
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB15_6
	jmp	.LBB15_12
.LBB15_7:
	xorl	%ebp, %ebp
	jmp	.LBB15_12
.LBB15_8:
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB15_11
.LBB15_9:                               # %vector.body.preheader.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	48(%r13,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB15_10:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rdi), %xmm2
	movdqu	-32(%rdi), %xmm3
	movdqu	-16(%rdi), %xmm4
	movdqu	(%rdi), %xmm5
	movdqa	%xmm2, %xmm6
	pcmpgtd	%xmm0, %xmm6
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm1, %xmm7
	pand	%xmm6, %xmm2
	pandn	%xmm0, %xmm6
	por	%xmm2, %xmm6
	pand	%xmm7, %xmm3
	pandn	%xmm1, %xmm7
	por	%xmm3, %xmm7
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm0, %xmm4
	pandn	%xmm6, %xmm0
	por	%xmm4, %xmm0
	pand	%xmm1, %xmm5
	pandn	%xmm7, %xmm1
	por	%xmm5, %xmm1
	addq	$64, %rdi
	addq	$-16, %rsi
	jne	.LBB15_10
.LBB15_11:                              # %middle.block
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm1, %esi
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm1, 64(%rsp)
	movd	%xmm0, %ebp
	testb	$1, 64(%rsp)
	cmovnel	%esi, %ebp
	cmpq	%rcx, %rdx
	jne	.LBB15_6
.LBB15_12:                              # %._crit_edge200
	leal	1(%rbp), %ecx
	movl	%ecx, %eax
	imull	%eax, %eax
	testl	%eax, %eax
	movq	%rcx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB15_15
# BB#13:
	movl	%eax, %r15d
.Ltmp132:
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp133:
# BB#14:                                # %.lr.ph.i
	xorl	%esi, %esi
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	(%rsp), %rcx            # 8-byte Reload
	testl	%ebp, %ebp
	jns	.LBB15_16
	jmp	.LBB15_27
.LBB15_15:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	js	.LBB15_27
.LBB15_16:
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movslq	%ecx, %rbx
	movq	%rbx, %rdi
	shlq	$4, %rdi
.Ltmp135:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp136:
# BB#17:                                # %.lr.ph.i85
	leaq	-1(%rbx), %rcx
	movq	%rbx, %rdx
	xorl	%edi, %edi
	andq	$7, %rdx
	je	.LBB15_20
# BB#18:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB15_19:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	32(%rsp), %xmm0
	movdqu	%xmm0, (%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdi, %rdx
	jne	.LBB15_19
.LBB15_20:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB15_23
# BB#21:                                # %.lr.ph.i85.new
	subq	%rdi, %rbx
	shlq	$4, %rdi
	leaq	112(%rax,%rdi), %rcx
	.p2align	4, 0x90
.LBB15_22:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movups	32(%rsp), %xmm0
	movups	%xmm0, -112(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -96(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -80(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -64(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -48(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -32(%rcx)
	movups	32(%rsp), %xmm0
	movups	%xmm0, -16(%rcx)
	movdqu	32(%rsp), %xmm0
	movdqu	%xmm0, (%rcx)
	subq	$-128, %rcx
	addq	$-8, %rbx
	jne	.LBB15_22
.LBB15_23:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit
	testl	%ebp, %ebp
	movq	%rax, 16(%rsp)          # 8-byte Spill
	js	.LBB15_28
# BB#24:                                # %.lr.ph194.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	(%rcx,%rcx,2), %ecx
	movslq	%ecx, %rbx
	movq	%rax, %rcx
	addq	$12, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_25:                              # %.lr.ph194
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rdx,4), %esi
	movl	4(%r12,%rdx,4), %edi
	movl	8(%r12,%rdx,4), %ebp
	movl	%esi, -12(%rcx)
	movl	%edi, -8(%rcx)
	movl	%ebp, -4(%rcx)
	movl	$0, (%rcx)
	addq	$3, %rdx
	addq	$16, %rcx
	cmpq	%rbx, %rdx
	jl	.LBB15_25
.LBB15_28:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB15_29
.LBB15_27:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit.thread
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB15_29:                              # %._crit_edge195
.Ltmp138:
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp139:
# BB#30:                                # %_ZN17btCollisionObjectnwEm.exit
.Ltmp141:
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp142:
# BB#31:                                # %.preheader
	testl	%r14d, %r14d
	movq	(%rsp), %rcx            # 8-byte Reload
	jle	.LBB15_41
# BB#32:                                # %.lr.ph
	movslq	%ebx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB15_33:                              # =>This Inner Loop Header: Depth=1
	movl	(%r13,%r12,4), %edx
	movl	4(%r13,%r12,4), %r14d
	movl	8(%r13,%r12,4), %r15d
	movl	%edx, %ebp
	imull	%ecx, %ebp
	leal	(%rbp,%r15), %eax
	cltq
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpb	$0, (%rbx,%rax)
	jne	.LBB15_35
# BB#34:                                #   in Loop: Header=BB15_33 Depth=1
	movb	$1, (%rbx,%rax)
	movl	%r15d, %eax
	imull	%ecx, %eax
	addl	%edx, %eax
	cltq
	movb	$1, (%rbx,%rax)
.Ltmp146:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r15d, %esi
	movq	%rdx, %r13
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
.Ltmp147:
.LBB15_35:                              #   in Loop: Header=BB15_33 Depth=1
	movl	%r14d, %r13d
	imull	%ecx, %r13d
	leal	(%r13,%rdx), %eax
	cltq
	cmpb	$0, (%rbx,%rax)
	jne	.LBB15_37
# BB#36:                                #   in Loop: Header=BB15_33 Depth=1
	movb	$1, (%rbx,%rax)
	addl	%r14d, %ebp
	movslq	%ebp, %rax
	movb	$1, (%rbx,%rax)
.Ltmp148:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edx, %esi
	movq	%rdx, %rbp
	movl	%r14d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	movq	%rbp, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
.Ltmp149:
.LBB15_37:                              #   in Loop: Header=BB15_33 Depth=1
	movl	%r15d, %eax
	imull	%ecx, %eax
	addl	%r14d, %eax
	cltq
	cmpb	$0, (%rbx,%rax)
	jne	.LBB15_39
# BB#38:                                #   in Loop: Header=BB15_33 Depth=1
	movb	$1, (%rbx,%rax)
	addl	%r15d, %r13d
	movslq	%r13d, %rax
	movb	$1, (%rbx,%rax)
.Ltmp150:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%rdx, %rbx
	movl	%r15d, %edx
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
	movq	%rbx, %rdx
.Ltmp151:
.LBB15_39:                              #   in Loop: Header=BB15_33 Depth=1
.Ltmp153:
	xorl	%r8d, %r8d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movl	%edx, %esi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE
.Ltmp154:
# BB#40:                                #   in Loop: Header=BB15_33 Depth=1
	addq	$3, %r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	jl	.LBB15_33
.LBB15_41:                              # %._crit_edge
.Ltmp156:
	movq	%rbp, %rdi
	callq	_ZN10btSoftBody20randomizeConstraintsEv
.Ltmp157:
# BB#42:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB15_44
# BB#43:
.Ltmp161:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp162:
.LBB15_44:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit96
	movq	24(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB15_46
# BB#45:
	callq	_Z21btAlignedFreeInternalPv
.LBB15_46:                              # %_ZN20btAlignedObjectArrayIbED2Ev.exit92
	movq	%rbp, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_47:
.Ltmp163:
	jmp	.LBB15_49
.LBB15_48:
.Ltmp137:
.LBB15_49:
	movq	%rax, %rbp
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB15_60
	jmp	.LBB15_61
.LBB15_50:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit.thread
.Ltmp134:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB15_51:
.Ltmp158:
	jmp	.LBB15_56
.LBB15_52:
.Ltmp143:
	movq	%rbp, %rdi
	movq	%rax, %rbp
.Ltmp144:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp145:
	jmp	.LBB15_57
.LBB15_53:
.Ltmp140:
	jmp	.LBB15_56
.LBB15_54:
.Ltmp155:
	jmp	.LBB15_56
.LBB15_55:
.Ltmp152:
.LBB15_56:                              # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rax, %rbp
.LBB15_57:                              # %_ZN17btCollisionObjectdlEPv.exit
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB15_59
# BB#58:
.Ltmp159:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp160:
.LBB15_59:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB15_61
.LBB15_60:
.Ltmp164:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp165:
.LBB15_61:                              # %_ZN20btAlignedObjectArrayIbED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB15_62:
.Ltmp166:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii, .Lfunc_end15-_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp132-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin6  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp135-.Ltmp133       #   Call between .Ltmp133 and .Ltmp135
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin6  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin6  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin6  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp151-.Ltmp146       #   Call between .Ltmp146 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin6  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin6  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin6  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin6  #     jumps to .Ltmp163
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp144-.Ltmp162       #   Call between .Ltmp162 and .Ltmp144
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp165-.Ltmp144       #   Call between .Ltmp144 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin6  #     jumps to .Ltmp166
	.byte	1                       #   On action: 1
	.long	.Ltmp165-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Lfunc_end15-.Ltmp165   #   Call between .Ltmp165 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb
	.p2align	4, 0x90
	.type	_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb,@function
_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb: # @_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi139:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 144
.Lcfi142:
	.cfi_offset %rbx, -56
.Lcfi143:
	.cfi_offset %r12, -48
.Lcfi144:
	.cfi_offset %r13, -40
.Lcfi145:
	.cfi_offset %r14, -32
.Lcfi146:
	.cfi_offset %r15, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	movq	%rcx, %rbp
	movq	%rsi, %r12
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	$0, 52(%rsp)
	movl	$0, 76(%rsp)
	movl	$0, 72(%rsp)
	movl	$0, 68(%rsp)
	leaq	52(%rsp), %r14
	leaq	76(%rsp), %r15
	leaq	72(%rsp), %rbx
	leaq	68(%rsp), %r13
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	%r13, %r9
	callq	sscanf
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	%r13, %r9
	callq	sscanf
	movl	$1, %ebx
	cmpb	$10, (%rbp)
	je	.LBB16_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$10, (%rbp,%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB16_2
.LBB16_3:                               # %_ZL8nextLinePKc.exit
	movslq	52(%rsp), %r15
	testq	%r15, %r15
	jle	.LBB16_4
# BB#20:
	movq	%r15, %rdi
	shlq	$4, %rdi
.Ltmp167:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp168:
# BB#21:                                # %.lr.ph.i52
	leaq	-1(%r15), %rax
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	andq	$7, %rdx
	je	.LBB16_24
# BB#22:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB16_23:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rcx
	addq	$16, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB16_23
.LBB16_24:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB16_27
# BB#25:                                # %.lr.ph.i52.new
	movq	%r15, %rax
	subq	%rcx, %rax
	shlq	$4, %rcx
	leaq	112(%r13,%rcx), %rcx
	.p2align	4, 0x90
.LBB16_26:                              # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movups	16(%rsp), %xmm0
	movups	%xmm0, -112(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -96(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -80(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -64(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -48(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -32(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, -16(%rcx)
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%rcx)
	subq	$-128, %rcx
	addq	$-8, %rax
	jne	.LBB16_26
.LBB16_27:                              # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit
	testl	%r15d, %r15d
	jle	.LBB16_5
# BB#28:                                # %.lr.ph111
	movslq	%ebx, %rax
	addq	%rax, %rbp
	leaq	44(%rsp), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_29:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_31 Depth 2
	movl	$0, 16(%rsp)
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	16(%rsp), %rdx
	leaq	36(%rsp), %rcx
	leaq	48(%rsp), %r8
	movq	%r14, %r9
	callq	sscanf
	movl	$1, %eax
	cmpb	$10, (%rbp)
	je	.LBB16_32
# BB#30:                                # %.lr.ph.i59.preheader
                                        #   in Loop: Header=BB16_29 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB16_31:                              # %.lr.ph.i59
                                        #   Parent Loop BB16_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%rbp,%rax)
	leaq	1(%rax), %rax
	jne	.LBB16_31
.LBB16_32:                              # %_ZL8nextLinePKc.exit61
                                        #   in Loop: Header=BB16_29 Depth=1
	cltq
	addq	%rax, %rbp
	movslq	16(%rsp), %rax
	shlq	$4, %rax
	movl	36(%rsp), %ecx
	movq	%r13, %rdx
	movl	%ecx, (%rdx,%rax)
	movl	48(%rsp), %ecx
	movl	%ecx, 4(%rdx,%rax)
	movl	44(%rsp), %ecx
	movl	%ecx, 8(%rdx,%rax)
	incl	%ebx
	cmpl	%r15d, %ebx
	jne	.LBB16_29
	jmp	.LBB16_5
.LBB16_4:                               # %_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_.exit.thread
	xorl	%r13d, %r13d
.LBB16_5:                               # %._crit_edge112
.Ltmp170:
	movl	$1496, %edi             # imm = 0x5D8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp171:
# BB#6:                                 # %_ZN17btCollisionObjectnwEm.exit
	movl	52(%rsp), %edx
.Ltmp173:
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rcx
	callq	_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf
.Ltmp174:
# BB#7:
	testq	%r12, %r12
	je	.LBB16_46
# BB#8:
	cmpb	$0, (%r12)
	je	.LBB16_46
# BB#9:
	movl	$0, 36(%rsp)
	movl	$0, 48(%rsp)
	movl	$0, 44(%rsp)
	leaq	36(%rsp), %rdx
	leaq	48(%rsp), %rcx
	leaq	44(%rsp), %r8
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sscanf
	cmpb	$10, (%r12)
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	$1, %eax
	je	.LBB16_12
	.p2align	4, 0x90
.LBB16_10:                              # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$10, (%r12,%rax)
	leaq	1(%rax), %rax
	jne	.LBB16_10
# BB#11:                                # %_ZL8nextLinePKc.exit74.loopexit
	cltq
.LBB16_12:                              # %_ZL8nextLinePKc.exit74
	cmpl	$0, 36(%rsp)
	jle	.LBB16_45
# BB#13:                                # %.lr.ph
	addq	%rax, %r12
	leaq	20(%rsp), %rbx
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB16_14
# BB#33:                                # %.lr.ph.split.us.preheader
	leaq	40(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_34:                              # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_36 Depth 2
	movl	$0, 40(%rsp)
	leaq	28(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rdx
	leaq	16(%rsp), %rcx
	movq	%rbx, %r8
	leaq	24(%rsp), %r9
	callq	sscanf
	cmpb	$10, (%r12)
	movl	$1, %r13d
	je	.LBB16_37
# BB#35:                                # %.lr.ph.i65.us.preheader
                                        #   in Loop: Header=BB16_34 Depth=1
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB16_36:                              # %.lr.ph.i65.us
                                        #   Parent Loop BB16_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%r12,%r13)
	leaq	1(%r13), %r13
	jne	.LBB16_36
.LBB16_37:                              # %_ZL8nextLinePKc.exit67.us
                                        #   in Loop: Header=BB16_34 Depth=1
	movl	16(%rsp), %esi
	movl	20(%rsp), %edx
	movl	24(%rsp), %ecx
	movl	28(%rsp), %r8d
.Ltmp181:
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE
.Ltmp182:
# BB#38:                                #   in Loop: Header=BB16_34 Depth=1
	movl	16(%rsp), %esi
	movl	20(%rsp), %edx
.Ltmp183:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp184:
# BB#39:                                #   in Loop: Header=BB16_34 Depth=1
	movl	20(%rsp), %esi
	movl	24(%rsp), %edx
.Ltmp185:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp186:
# BB#40:                                #   in Loop: Header=BB16_34 Depth=1
	movl	16(%rsp), %edx
	movl	24(%rsp), %esi
.Ltmp187:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp188:
# BB#41:                                #   in Loop: Header=BB16_34 Depth=1
	movl	16(%rsp), %esi
	movl	28(%rsp), %edx
.Ltmp189:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp190:
# BB#42:                                #   in Loop: Header=BB16_34 Depth=1
	movl	20(%rsp), %esi
	movl	28(%rsp), %edx
.Ltmp191:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp192:
# BB#43:                                #   in Loop: Header=BB16_34 Depth=1
	movl	24(%rsp), %esi
	movl	28(%rsp), %edx
.Ltmp193:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb
.Ltmp194:
# BB#44:                                #   in Loop: Header=BB16_34 Depth=1
	movslq	%r13d, %rax
	addq	%rax, %r12
	incl	%ebp
	cmpl	36(%rsp), %ebp
	jl	.LBB16_34
	jmp	.LBB16_45
.LBB16_14:                              # %.lr.ph.split.preheader
	leaq	16(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_15:                              # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_17 Depth 2
	movl	$0, 40(%rsp)
	leaq	28(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	40(%rsp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rbp
	movq	%rbx, %r8
	leaq	24(%rsp), %r9
	callq	sscanf
	movl	$1, %ebx
	cmpb	$10, (%r12)
	je	.LBB16_18
# BB#16:                                # %.lr.ph.i65.preheader
                                        #   in Loop: Header=BB16_15 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB16_17:                              # %.lr.ph.i65
                                        #   Parent Loop BB16_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%r12,%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB16_17
.LBB16_18:                              # %_ZL8nextLinePKc.exit67
                                        #   in Loop: Header=BB16_15 Depth=1
	movl	16(%rsp), %esi
	movl	20(%rsp), %edx
	movl	24(%rsp), %ecx
	movl	28(%rsp), %r8d
.Ltmp178:
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	callq	_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE
.Ltmp179:
# BB#19:                                #   in Loop: Header=BB16_15 Depth=1
	movslq	%ebx, %rax
	addq	%rax, %r12
	incl	%r14d
	cmpl	36(%rsp), %r14d
	movq	%rbp, %rbx
	jl	.LBB16_15
.LBB16_45:                              # %._crit_edge
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB16_46:
	movl	820(%r15), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	852(%r15), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	884(%r15), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	916(%r15), %esi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	testq	%r13, %r13
	je	.LBB16_48
# BB#47:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB16_48:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit51
	movq	%r15, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_57:
.Ltmp169:
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB16_54
	jmp	.LBB16_55
.LBB16_49:
.Ltmp175:
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%rax, %rbx
.Ltmp176:
	movq	%r15, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp177:
	jmp	.LBB16_53
.LBB16_51:
.Ltmp172:
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jmp	.LBB16_52
.LBB16_50:                              # %.us-lcssa
.Ltmp180:
	jmp	.LBB16_52
.LBB16_58:                              # %.us-lcssa.us
.Ltmp195:
.LBB16_52:                              # %_ZN17btCollisionObjectdlEPv.exit
	movq	%rax, %rbx
.LBB16_53:                              # %_ZN17btCollisionObjectdlEPv.exit
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB16_55
.LBB16_54:
.Ltmp196:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp197:
.LBB16_55:                              # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_56:
.Ltmp198:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb, .Lfunc_end16-_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp167-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp168-.Ltmp167       #   Call between .Ltmp167 and .Ltmp168
	.long	.Ltmp169-.Lfunc_begin7  #     jumps to .Ltmp169
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin7  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin7  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp194-.Ltmp181       #   Call between .Ltmp181 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin7  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin7  #     jumps to .Ltmp180
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp176-.Ltmp179       #   Call between .Ltmp179 and .Ltmp176
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp197-.Ltmp176       #   Call between .Ltmp176 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin7  #     jumps to .Ltmp198
	.byte	1                       #   On action: 1
	.long	.Ltmp197-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Lfunc_end16-.Ltmp197   #   Call between .Ltmp197 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis,@object # @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis
	.local	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis
	.comm	_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis,48,16
	.type	_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis,@object # @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis
	.local	_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis
	.comm	_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" M(%.2f)"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" A(%.2f)"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d %d %d %d"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d %f %f %f"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d %d %d"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d %d %d %d %d"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Nodes:  %u\r\n"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Links:  %u\r\n"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Faces:  %u\r\n"
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Tetras: %u\r\n"
	.size	.L.str.9, 13

	.type	_ZZNK10btSoftBody4Body5xformEvE8identity,@object # @_ZZNK10btSoftBody4Body5xformEvE8identity
	.section	.bss._ZZNK10btSoftBody4Body5xformEvE8identity,"aGw",@nobits,_ZZNK10btSoftBody4Body5xformEvE8identity,comdat
	.weak	_ZZNK10btSoftBody4Body5xformEvE8identity
	.p2align	2
_ZZNK10btSoftBody4Body5xformEvE8identity:
	.zero	64
	.size	_ZZNK10btSoftBody4Body5xformEvE8identity, 64

	.type	_ZGVZNK10btSoftBody4Body5xformEvE8identity,@object # @_ZGVZNK10btSoftBody4Body5xformEvE8identity
	.section	.bss._ZGVZNK10btSoftBody4Body5xformEvE8identity,"aGw",@nobits,_ZGVZNK10btSoftBody4Body5xformEvE8identity,comdat
	.weak	_ZGVZNK10btSoftBody4Body5xformEvE8identity
	.p2align	3
_ZGVZNK10btSoftBody4Body5xformEvE8identity:
	.quad	0                       # 0x0
	.size	_ZGVZNK10btSoftBody4Body5xformEvE8identity, 8

	.type	_ZZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZZN11btTransform11getIdentityEvE17identityTransform
	.p2align	2
_ZZN11btTransform11getIdentityEvE17identityTransform:
	.zero	64
	.size	_ZZN11btTransform11getIdentityEvE17identityTransform, 64

	.type	_ZGVZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZGVZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZGVZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.p2align	3
_ZGVZN11btTransform11getIdentityEvE17identityTransform:
	.quad	0                       # 0x0
	.size	_ZGVZN11btTransform11getIdentityEvE17identityTransform, 8

	.type	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	2
_ZZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.zero	48
	.size	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, 48

	.type	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	3
_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.quad	0                       # 0x0
	.size	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
