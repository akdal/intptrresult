	.text
	.file	"sysspec.bc"
	.globl	AllocateMemory
	.p2align	4, 0x90
	.type	AllocateMemory,@function
AllocateMemory:                         # @AllocateMemory
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movslq	global_align(%rip), %rax
	leaq	(%rdi,%rax,2), %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	sete	%al
	movl	%eax, (%rbx)
	movslq	global_align(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#1:
	cmpl	$1, %edi
	jne	.LBB0_2
# BB#8:
	movl	%ecx, %edx
	andl	$1, %edx
	movq	%rcx, %rdi
	incq	%rdi
	movq	%rcx, %rsi
	jmp	.LBB0_9
.LBB0_5:
	movslq	mem_array_ents(%rip), %rax
	cmpq	$19, %rax
	jle	.LBB0_6
# BB#7:                                 # %AddMemArray.exit
	movl	$2, (%rbx)
	jmp	.LBB0_13
.LBB0_2:                                # %.preheader
	movq	%rcx, %rsi
	decq	%rsi
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	incq	%rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	testq	%rdx, %rdx
	jne	.LBB0_3
# BB#4:
	leal	(%rdi,%rdi), %eax
	movslq	%eax, %r8
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	addq	%rsi, %rdi
.LBB0_9:
	testq	%rdx, %rdx
	cmoveq	%rdi, %rsi
	movslq	mem_array_ents(%rip), %rax
	cmpq	$19, %rax
	jle	.LBB0_10
# BB#11:                                # %AddMemArray.exit27
	movl	$2, (%rbx)
	jmp	.LBB0_12
.LBB0_10:                               # %AddMemArray.exit27.thread
	movq	%rcx, mem_array(,%rax,8)
	movq	%rsi, mem_array+160(,%rax,8)
	leal	1(%rax), %eax
	movl	%eax, mem_array_ents(%rip)
.LBB0_12:
	movq	%rsi, %rcx
	jmp	.LBB0_13
.LBB0_6:                                # %AddMemArray.exit.thread
	movq	%rcx, mem_array(,%rax,8)
	movq	%rcx, mem_array+160(,%rax,8)
	leal	1(%rax), %eax
	movl	%eax, mem_array_ents(%rip)
.LBB0_13:
	movq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	AllocateMemory, .Lfunc_end0-AllocateMemory
	.cfi_endproc

	.globl	AddMemArray
	.p2align	4, 0x90
	.type	AddMemArray,@function
AddMemArray:                            # @AddMemArray
	.cfi_startproc
# BB#0:
	movslq	mem_array_ents(%rip), %rcx
	cmpq	$19, %rcx
	movl	$-1, %eax
	jg	.LBB1_2
# BB#1:
	movq	%rdi, mem_array(,%rcx,8)
	movq	%rsi, mem_array+160(,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, mem_array_ents(%rip)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	AddMemArray, .Lfunc_end1-AddMemArray
	.cfi_endproc

	.globl	FreeMemory
	.p2align	4, 0x90
	.type	FreeMemory,@function
FreeMemory:                             # @FreeMemory
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movslq	mem_array_ents(%rip), %r9
	testq	%r9, %r9
	movl	$3, %ecx
	jle	.LBB2_12
# BB#1:                                 # %.lr.ph22.i
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, mem_array+160(,%rdx,8)
	je	.LBB2_3
# BB#11:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%rdx
	cmpq	%r9, %rdx
	jl	.LBB2_2
	jmp	.LBB2_12
.LBB2_3:
	movq	mem_array(,%rdx,8), %rdi
	leal	1(%rdx), %ecx
	cmpl	%r9d, %ecx
	jge	.LBB2_10
# BB#4:                                 # %.lr.ph.i
	leaq	1(%rdx), %rcx
	movl	%r9d, %esi
	subl	%edx, %esi
	decl	%esi
	leaq	-2(%r9), %rbx
	testb	$1, %sil
	jne	.LBB2_6
# BB#5:
	movq	%rcx, %rsi
	movq	%rdx, %rcx
	cmpq	%rdx, %rbx
	jne	.LBB2_8
	jmp	.LBB2_10
.LBB2_6:
	movq	mem_array+8(,%rdx,8), %rsi
	movq	%rsi, mem_array(,%rdx,8)
	movq	mem_array+168(,%rdx,8), %rsi
	movq	%rsi, mem_array+160(,%rdx,8)
	leaq	2(%rdx), %rsi
	cmpq	%rdx, %rbx
	je	.LBB2_10
.LBB2_8:                                # %.lr.ph.i.new
	movq	%rsi, %rax
	shlq	$32, %rax
	movabsq	$8589934592, %r8        # imm = 0x200000000
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movq	mem_array(,%rsi,8), %rbx
	movslq	%ecx, %rcx
	movq	%rbx, mem_array(,%rcx,8)
	movq	mem_array+160(,%rsi,8), %rbx
	movq	%rbx, mem_array+160(,%rcx,8)
	movq	mem_array+8(,%rsi,8), %rcx
	movq	%rax, %rdx
	sarq	$29, %rdx
	movq	%rcx, mem_array(%rdx)
	movq	mem_array+168(,%rsi,8), %rcx
	movq	%rcx, mem_array+160(%rdx)
	leaq	1(%rsi), %rcx
	addq	$2, %rsi
	addq	%r8, %rax
	cmpq	%r9, %rsi
	jl	.LBB2_9
.LBB2_10:                               # %.loopexit
	decl	%r9d
	movl	%r9d, mem_array_ents(%rip)
	callq	free
	xorl	%ecx, %ecx
.LBB2_12:                               # %RemoveMemArray.exit.thread
	movl	%ecx, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	FreeMemory, .Lfunc_end2-FreeMemory
	.cfi_endproc

	.globl	RemoveMemArray
	.p2align	4, 0x90
	.type	RemoveMemArray,@function
RemoveMemArray:                         # @RemoveMemArray
	.cfi_startproc
# BB#0:
	movslq	mem_array_ents(%rip), %r9
	testq	%r9, %r9
	movl	$-1, %eax
	jle	.LBB3_8
# BB#1:                                 # %.lr.ph22
	movabsq	$4294967296, %r8        # imm = 0x100000000
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, mem_array+160(,%rcx,8)
	je	.LBB3_3
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	incq	%rcx
	addq	%r8, %rdx
	cmpq	%r9, %rcx
	jl	.LBB3_2
.LBB3_8:                                # %.loopexit
	retq
.LBB3_3:
	movq	mem_array(,%rcx,8), %rax
	movq	%rax, (%rsi)
	leal	1(%rcx), %eax
	cmpl	%r9d, %eax
	jge	.LBB3_6
# BB#4:                                 # %.lr.ph
	movl	mem_array_ents(%rip), %r9d
	incq	%rcx
	movslq	%r9d, %rax
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	movq	mem_array(,%rcx,8), %rsi
	movq	%rdx, %rdi
	sarq	$29, %rdi
	movq	%rsi, mem_array(%rdi)
	movq	mem_array+160(,%rcx,8), %rsi
	movq	%rsi, mem_array+160(%rdi)
	incq	%rcx
	addq	%r8, %rdx
	cmpq	%rax, %rcx
	jl	.LBB3_5
.LBB3_6:                                # %._crit_edge
	decl	%r9d
	movl	%r9d, mem_array_ents(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	RemoveMemArray, .Lfunc_end3-RemoveMemArray
	.cfi_endproc

	.globl	MoveMemory
	.p2align	4, 0x90
	.type	MoveMemory,@function
MoveMemory:                             # @MoveMemory
	.cfi_startproc
# BB#0:
	jmp	memmove                 # TAILCALL
.Lfunc_end4:
	.size	MoveMemory, .Lfunc_end4-MoveMemory
	.cfi_endproc

	.globl	InitMemArray
	.p2align	4, 0x90
	.type	InitMemArray,@function
InitMemArray:                           # @InitMemArray
	.cfi_startproc
# BB#0:
	movl	$0, mem_array_ents(%rip)
	retq
.Lfunc_end5:
	.size	InitMemArray, .Lfunc_end5-InitMemArray
	.cfi_endproc

	.globl	CreateFile
	.p2align	4, 0x90
	.type	CreateFile,@function
CreateFile:                             # @CreateFile
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	CreateFile, .Lfunc_end6-CreateFile
	.cfi_endproc

	.globl	ReportError
	.p2align	4, 0x90
	.type	ReportError,@function
ReportError:                            # @ReportError
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movl	%esi, %ebx
	movq	%rdi, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end7:
	.size	ReportError, .Lfunc_end7-ReportError
	.cfi_endproc

	.globl	ErrorExit
	.p2align	4, 0x90
	.type	ErrorExit,@function
ErrorExit:                              # @ErrorExit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	exit
.Lfunc_end8:
	.size	ErrorExit, .Lfunc_end8-ErrorExit
	.cfi_endproc

	.globl	StartStopwatch
	.p2align	4, 0x90
	.type	StartStopwatch,@function
StartStopwatch:                         # @StartStopwatch
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	StartStopwatch, .Lfunc_end9-StartStopwatch
	.cfi_endproc

	.globl	StopStopwatch
	.p2align	4, 0x90
	.type	StopStopwatch,@function
StopStopwatch:                          # @StopStopwatch
	.cfi_startproc
# BB#0:
	movl	$1000, %eax             # imm = 0x3E8
	retq
.Lfunc_end10:
	.size	StopStopwatch, .Lfunc_end10-StopStopwatch
	.cfi_endproc

	.globl	TicksToSecs
	.p2align	4, 0x90
	.type	TicksToSecs,@function
TicksToSecs:                            # @TicksToSecs
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	TicksToSecs, .Lfunc_end11-TicksToSecs
	.cfi_endproc

	.globl	TicksToFracSecs
	.p2align	4, 0x90
	.type	TicksToFracSecs,@function
TicksToFracSecs:                        # @TicksToFracSecs
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end12:
	.size	TicksToFracSecs, .Lfunc_end12-TicksToFracSecs
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ERROR CONDITION\nContext: %s\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Code: %d"
	.size	.L.str.1, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
