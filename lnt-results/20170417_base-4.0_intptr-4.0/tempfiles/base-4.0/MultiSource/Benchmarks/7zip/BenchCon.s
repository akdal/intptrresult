	.text
	.file	"BenchCon.bc"
	.globl	_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob
	.p2align	4, 0x90
	.type	_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob,@function
_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob: # @_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	jne	.LBB0_3
# BB#1:
	xorb	$1, %bpl
	jne	.LBB0_3
# BB#2:
	movl	80(%rbx), %edi
	movq	(%r14), %rsi
	movq	8(%r14), %rdx
	movq	32(%r14), %rcx
	callq	_Z17GetCompressRatingjyyy
	movq	72(%rbx), %rdi
	addq	$8, %rbx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%rbx, %rcx
	callq	_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes
	xorl	%eax, %eax
.LBB0_3:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob, .Lfunc_end0-_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes,@function
_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes: # @_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 96
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	32(%r12), %rcx
	movq	(%r12), %rdx
	movq	8(%r12), %rsi
	cmpq	$1000001, %rsi          # imm = 0xF4241
	jb	.LBB1_1
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rax
	shrq	%rax
	shrq	%rdx
	cmpq	$2000001, %rsi          # imm = 0x1E8481
	movq	%rax, %rsi
	ja	.LBB1_2
	jmp	.LBB1_3
.LBB1_1:
	movq	%rsi, %rax
.LBB1_3:                                # %_ZL11MyMultDiv64yyy.exit
	testq	%rdx, %rdx
	movl	$1, %esi
	cmovneq	%rdx, %rsi
	imulq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rsi
	shrq	$10, %rax
	movq	%rsp, %r13
	movl	$10, %edx
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	%r13, %rdi
	callq	strlen
	cmpl	$6, %eax
	jg	.LBB1_6
# BB#4:                                 # %.lr.ph.i.preheader
	movl	$7, %ebp
	subl	%eax, %ebp
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	decl	%ebp
	jne	.LBB1_5
.LBB1_6:                                # %_ZL11PrintNumberP8_IO_FILEyi.exit
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	fputs
	movq	%r12, %rdi
	callq	_Z8GetUsageRK10CBenchInfo
	movq	%rax, %r13
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_Z17GetRatingPerUsageRK10CBenchInfoy
	movq	%rax, %r12
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	_ZL12PrintResultsP8_IO_FILEyyy
	movdqu	(%r15), %xmm0
	movd	%r14, %xmm1
	movl	$1, %eax
	movd	%rax, %xmm2
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, (%r15)
	movdqu	16(%r15), %xmm0
	movd	%r12, %xmm1
	movd	%r13, %xmm2
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, 16(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes, .Lfunc_end1-_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes
	.cfi_endproc

	.globl	_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob
	.p2align	4, 0x90
	.type	_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob,@function
_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob: # @_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 96
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	jne	.LBB2_3
# BB#1:
	xorb	$1, %bpl
	jne	.LBB2_3
# BB#2:
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movl	48(%rbx), %r8d
	callq	_Z19GetDecompressRatingyyyyj
	movq	%rax, %r15
	movq	72(%r14), %rcx
	movl	$.L.str.18, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	48(%rbx), %rax
	movq	%rax, 48(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movl	48(%rsp), %eax
	movq	32(%rsp), %rcx
	imulq	%rax, %rcx
	movq	%rcx, 32(%rsp)
	imulq	40(%rsp), %rax
	movq	%rax, 40(%rsp)
	movl	$1, 48(%rsp)
	movq	72(%r14), %rdi
	addq	$40, %r14
	movq	%rsp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZL12PrintResultsP8_IO_FILERK10CBenchInfoyR14CTotalBenchRes
	xorl	%eax, %eax
.LBB2_3:
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob, .Lfunc_end2-_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob
	.cfi_endproc

	.globl	_Z12LzmaBenchConP8_IO_FILEjjj
	.p2align	4, 0x90
	.type	_Z12LzmaBenchConP8_IO_FILEjjj,@function
_Z12LzmaBenchConP8_IO_FILEjjj:          # @_Z12LzmaBenchConP8_IO_FILEjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 176
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r12
	movl	$1, %r13d
	callq	_Z15CrcInternalTestv
	testb	%al, %al
	je	.LBB3_18
# BB#1:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	callq	_ZN8NWindows7NSystem10GetRamSizeEv
	movq	%rax, %rbp
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, %r13d
	movl	$.L.str, %esi
	movl	$.L.str.1, %ecx
	movq	%r12, %rdi
	movq	%rbp, %r14
	movq	%rbp, %rdx
	movl	%r13d, %r8d
	callq	_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j
	cmpl	$-1, %ebx
	cmovel	%r13d, %ebx
	movl	%ebx, %r13d
	andl	$-2, %r13d
	cmpl	$1, %ebx
	cmovbel	%ebx, %r13d
	cmpl	$-1, %r15d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jne	.LBB3_5
# BB#2:                                 # %.preheader104.preheader
	movl	$33554432, %esi         # imm = 0x2000000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$25, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#3:                                 # %.preheader104.1112
	movl	$16777216, %esi         # imm = 0x1000000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$24, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#19:                                # %.preheader104.2113
	movl	$8388608, %esi          # imm = 0x800000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$23, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#20:                                # %.preheader104.3114
	movl	$4194304, %esi          # imm = 0x400000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$22, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#21:                                # %.preheader104.4115
	movl	$2097152, %esi          # imm = 0x200000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$21, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#22:                                # %.preheader104.5116
	movl	$1048576, %esi          # imm = 0x100000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	movl	$20, %ecx
	cmpq	%r14, %rax
	jbe	.LBB3_4
# BB#23:                                # %.preheader104.6117
	movl	$524288, %esi           # imm = 0x80000
	movl	%r13d, %edi
	callq	_Z19GetBenchMemoryUsagejj
	addq	$8388608, %rax          # imm = 0x800000
	xorl	%ecx, %ecx
	cmpq	%r14, %rax
	setbe	%cl
	orl	$18, %ecx
.LBB3_4:
	movl	$1, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB3_5:
	movl	%r13d, %edi
	movl	%r15d, %esi
	callq	_Z19GetBenchMemoryUsagejj
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	%r13d, %r8d
	callq	_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j
	movq	$_ZTV14CBenchCallback+16, 32(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rsp)
	movups	%xmm0, 72(%rsp)
	movups	%xmm0, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	%r12, 104(%rsp)
	movl	$.L.str.4, %edi
	movl	$61, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.5, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.18, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.5, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.6, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	$.L.str.18, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	$.L.str.8, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	12(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB3_15
# BB#6:                                 # %.lr.ph107
	xorl	%eax, %eax
	cmpl	$4194303, %r15d         # imm = 0x3FFFFF
	seta	%al
	leal	18(,%rax,4), %ebx
	xorl	%edx, %edx
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB3_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_11 Depth 2
	movl	%edx, 28(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %r14d
	movl	%ebx, %ecx
	shll	%cl, %r14d
	decl	%ebx
	cmpl	%r15d, %r14d
	ja	.LBB3_8
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_7 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph
                                        #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movl	%r14d, 112(%rsp)
	movl	%r13d, %ebp
	movl	%r13d, %edi
	movl	%r14d, %esi
	leaq	32(%rsp), %rdx
	callq	_Z9LzmaBenchjjP14IBenchCallback
	movl	%eax, %r13d
	movl	$10, %edi
	movq	%r12, %rsi
	callq	fputc
	testl	%r13d, %r13d
	jne	.LBB3_18
# BB#10:                                #   in Loop: Header=BB3_11 Depth=2
	incl	%ebx
	movl	$1, %r14d
	movl	%ebx, %ecx
	shll	%cl, %r14d
	cmpl	%r15d, %r14d
	movl	%ebp, %r13d
	jbe	.LBB3_11
# BB#12:                                # %.thread
                                        #   in Loop: Header=BB3_7 Depth=1
	movl	28(%rsp), %edx          # 4-byte Reload
	incl	%edx
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %edx
	movl	24(%rsp), %ebx          # 4-byte Reload
	jb	.LBB3_7
# BB#13:                                # %._crit_edge
	movq	40(%rsp), %rcx
	testq	%rcx, %rcx
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB3_15
# BB#14:
	movq	48(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 48(%rsp)
	movq	56(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 56(%rsp)
	movq	64(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 64(%rsp)
	movq	$1, 40(%rsp)
.LBB3_15:                               # %_ZN14CTotalBenchRes9NormalizeEv.exit.i
	movq	72(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB3_17
# BB#16:
	movq	80(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 80(%rsp)
	movq	88(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 88(%rsp)
	movq	96(%rsp), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, 96(%rsp)
	movq	$1, 72(%rsp)
.LBB3_17:                               # %_ZN14CBenchCallback9NormalizeEv.exit
	movl	$.L.str.11, %edi
	movl	$69, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.21, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	48(%rsp), %rcx
	movq	56(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	%r12, %rdi
	callq	_ZL12PrintResultsP8_IO_FILEyyy
	movl	$.L.str.12, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$.L.str.21, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	80(%rsp), %rcx
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdx
	movq	%r12, %rdi
	callq	_ZL12PrintResultsP8_IO_FILEyyy
	movl	$.L.str.13, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	80(%rsp), %r14
	movq	88(%rsp), %rbp
	addq	48(%rsp), %r14
	shrq	%r14
	addq	56(%rsp), %rbp
	shrq	%rbp
	movq	96(%rsp), %rbx
	addq	64(%rsp), %rbx
	shrq	%rbx
	movl	$.L.str.21, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	_ZL12PrintResultsP8_IO_FILEyyy
	movl	$10, %edi
	movq	%r12, %rsi
	callq	fputc
	xorl	%r13d, %r13d
.LBB3_18:
	movl	%r13d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z12LzmaBenchConP8_IO_FILEjjj, .Lfunc_end3-_Z12LzmaBenchConP8_IO_FILEjjj
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j,@function
_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j: # @_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -48
.Lcfi48:
	.cfi_offset %r12, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %rcx
	movq	%rdi, %rbp
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	shrq	$20, %rbx
	movq	%rsp, %r12
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$32, %edi
	movq	%rbp, %rsi
	callq	fputc
	movq	%r12, %rdi
	callq	strlen
	cmpl	$4, %eax
	jg	.LBB4_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$5, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbp, %rsi
	callq	fputc
	decl	%ebx
	jne	.LBB4_2
.LBB4_3:                                # %_ZL11PrintNumberP8_IO_FILEyi.exit
	movq	%rsp, %rdi
	movq	%rbp, %rsi
	callq	fputs
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	fprintf
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j, .Lfunc_end4-_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j
	.cfi_endproc

	.globl	_Z11CrcBenchConP8_IO_FILEjjj
	.p2align	4, 0x90
	.type	_Z11CrcBenchConP8_IO_FILEjjj,@function
_Z11CrcBenchConP8_IO_FILEjjj:           # @_Z11CrcBenchConP8_IO_FILEjjj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 160
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%edx, %ebx
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	$1, %r12d
	callq	_Z15CrcInternalTestv
	testb	%al, %al
	je	.LBB5_39
# BB#1:
	callq	_ZN8NWindows7NSystem10GetRamSizeEv
	movq	%rax, %r15
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, %ebp
	movl	$.L.str, %esi
	movl	$.L.str.1, %ecx
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%rbx, %r15
	movl	%ebp, %r8d
	callq	_ZL17PrintRequirementsP8_IO_FILEPKcyS2_j
	cmpl	$-1, %r15d
	cmovel	%ebp, %r15d
	cmpl	$-1, %r13d
	movl	$16777216, %r12d        # imm = 0x1000000
	cmovnel	%r13d, %r12d
	leaq	(,%r15,8), %rdi
	callq	_Znam
	movq	%rax, %r13
	movl	$.L.str.14, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%r15d, %r15d
	je	.LBB5_4
# BB#2:                                 # %.lr.ph176.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph176
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rbp), %rbx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	$0, (%r13,%rbp,8)
	cmpq	%rbx, %r15
	movq	%rbx, %rbp
	jne	.LBB5_3
.LBB5_4:                                # %._crit_edge177
	movl	$.L.str.8, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB5_5
# BB#6:                                 # %.preheader.lr.ph
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movl	%r12d, 44(%rsp)         # 4-byte Spill
	jmp	.LBB5_7
.LBB5_5:
	xorl	%r12d, %r12d
	jmp	.LBB5_38
.LBB5_10:                               #   in Loop: Header=BB5_7 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB5_22
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_9 Depth 2
                                        #       Child Loop BB5_12 Depth 3
                                        #         Child Loop BB5_19 Depth 4
                                        #     Child Loop BB5_33 Depth 2
	movl	%eax, 52(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	je	.LBB5_32
# BB#8:                                 # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB5_7 Depth=1
	movl	$10, %ecx
.LBB5_9:                                # %.preheader.split.us
                                        #   Parent Loop BB5_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_12 Depth 3
                                        #         Child Loop BB5_19 Depth 4
	movl	$1, %r13d
	shll	%cl, %r13d
	cmpl	%r12d, %r13d
	ja	.LBB5_10
# BB#11:                                # %.lr.ph164.us
                                        #   in Loop: Header=BB5_9 Depth=2
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
	callq	fprintf
	xorl	%r15d, %r15d
	leaq	64(%rsp), %rbx
	.p2align	4, 0x90
.LBB5_12:                               #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_19 Depth 4
.Ltmp0:
	callq	_ZN13NConsoleClose15TestBreakSignalEv
.Ltmp1:
# BB#13:                                #   in Loop: Header=BB5_12 Depth=3
	testb	%al, %al
	jne	.LBB5_36
# BB#14:                                #   in Loop: Header=BB5_12 Depth=3
	leaq	1(%r15), %rbp
.Ltmp2:
	movl	%ebp, %edi
	movl	%r13d, %esi
	leaq	56(%rsp), %rdx
	callq	_Z8CrcBenchjjRy
	movl	%eax, %r12d
.Ltmp3:
# BB#15:                                #   in Loop: Header=BB5_12 Depth=3
	testl	%r12d, %r12d
	jne	.LBB5_37
# BB#16:                                #   in Loop: Header=BB5_12 Depth=3
	movq	56(%rsp), %rdi
	shrq	$20, %rdi
.Ltmp4:
	movl	$10, %edx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp5:
# BB#17:                                # %.noexc.us
                                        #   in Loop: Header=BB5_12 Depth=3
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	movq	%rbx, %rdi
	callq	strlen
	cmpl	$4, %eax
	jg	.LBB5_20
# BB#18:                                # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB5_12 Depth=3
	movl	$5, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph.i.us
                                        #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        #       Parent Loop BB5_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	decl	%ebx
	jne	.LBB5_19
.LBB5_20:                               # %.loopexit144.us
                                        #   in Loop: Header=BB5_12 Depth=3
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	fputs
	movq	56(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	%rax, (%rcx,%r15,8)
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rbp, %r15
	jb	.LBB5_12
# BB#21:                                # %..thread130_crit_edge.us
                                        #   in Loop: Header=BB5_9 Depth=2
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	movq	24(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movl	48(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	cmpl	$32, %ecx
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	44(%rsp), %r12d         # 4-byte Reload
	jl	.LBB5_9
	jmp	.LBB5_22
.LBB5_32:                               # %.preheader.split.preheader
                                        #   in Loop: Header=BB5_7 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_33:                               # %.preheader.split
                                        #   Parent Loop BB5_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	10(%rbp), %rcx
	movl	$1, %eax
	shll	%cl, %eax
	cmpl	%r12d, %eax
	ja	.LBB5_35
# BB#34:                                # %.thread130
                                        #   in Loop: Header=BB5_33 Depth=2
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ecx, %edx
	callq	fprintf
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	incq	%rbp
	leal	10(%rbp), %eax
	cmpl	$32, %eax
	jl	.LBB5_33
.LBB5_35:                               # %.preheader.split..thread128.loopexit_crit_edge
                                        #   in Loop: Header=BB5_7 Depth=1
	addq	%rbp, %rbx
.LBB5_22:                               # %.thread128
                                        #   in Loop: Header=BB5_7 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	20(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB5_7
# BB#23:                                # %._crit_edge172
	xorl	%r12d, %r12d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB5_38
# BB#24:
	movl	$.L.str.17, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%r15d, %r15d
	je	.LBB5_31
# BB#25:                                # %.lr.ph
	xorl	%ebp, %ebp
	leaq	64(%rsp), %r15
.LBB5_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_29 Depth 2
	movq	(%r13,%rbp,8), %rax
	xorl	%edx, %edx
	divq	24(%rsp)                # 8-byte Folded Reload
	shrq	$20, %rax
.Ltmp7:
	movl	$10, %edx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp8:
# BB#27:                                # %.noexc117
                                        #   in Loop: Header=BB5_26 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	movq	%r15, %rdi
	callq	strlen
	cmpl	$4, %eax
	jg	.LBB5_30
# BB#28:                                # %.lr.ph.i116.preheader
                                        #   in Loop: Header=BB5_26 Depth=1
	movl	$5, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph.i116
                                        #   Parent Loop BB5_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$32, %edi
	movq	%r14, %rsi
	callq	fputc
	decl	%ebx
	jne	.LBB5_29
.LBB5_30:                               # %.loopexit
                                        #   in Loop: Header=BB5_26 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	fputs
	incq	%rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jb	.LBB5_26
.LBB5_31:                               # %._crit_edge
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	jmp	.LBB5_38
.LBB5_36:                               # %.thread
	movl	$-2147467260, %r12d     # imm = 0x80004004
.LBB5_37:                               # %.us-lcssa.us
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB5_38:                               # %_ZN11CTempValuesD2Ev.exit
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB5_39:
	movl	%r12d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_42:
.Ltmp9:
	jmp	.LBB5_41
.LBB5_40:                               # %.us-lcssa168.us
.Ltmp6:
.LBB5_41:                               # %_ZN11CTempValuesD2Ev.exit119
	movq	%rax, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_Z11CrcBenchConP8_IO_FILEjjj, .Lfunc_end5-_Z11CrcBenchConP8_IO_FILEjjj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL12PrintResultsP8_IO_FILEyyy,@function
_ZL12PrintResultsP8_IO_FILEyyy:         # @_ZL12PrintResultsP8_IO_FILEyyy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 96
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	leaq	5000(%rsi), %rax
	movabsq	$3777893186295716171, %rcx # imm = 0x346DC5D63886594B
	mulq	%rcx
	movq	%rdx, %rax
	shrq	$11, %rax
	movq	%rsp, %r12
	movl	$10, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	%r12, %rdi
	callq	strlen
	cmpl	$4, %eax
	jg	.LBB6_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$5, %ebp
	subl	%eax, %ebp
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	decl	%ebp
	jne	.LBB6_2
.LBB6_3:                                # %_ZL11PrintNumberP8_IO_FILEyi.exit
	movq	%rsp, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	fputs
	movabsq	$4835703278458516699, %r13 # imm = 0x431BDE82D7B634DB
	movq	%r15, %rax
	mulq	%r13
	movq	%rdx, %rax
	shrq	$18, %rax
	movl	$10, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	%r12, %rdi
	callq	strlen
	cmpl	$5, %eax
	jg	.LBB6_6
# BB#4:                                 # %.lr.ph.i.i.preheader
	movl	$6, %ebp
	subl	%eax, %ebp
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	decl	%ebp
	jne	.LBB6_5
.LBB6_6:                                # %_ZL11PrintRatingP8_IO_FILEy.exit
	movq	%rsp, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	fputs
	movq	%r14, %rax
	mulq	%r13
	movq	%rdx, %rax
	shrq	$18, %rax
	movl	$10, %edx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	%r15, %rdi
	callq	strlen
	cmpl	$5, %eax
	jg	.LBB6_9
# BB#7:                                 # %.lr.ph.i.i9.preheader
	movl	$6, %ebp
	subl	%eax, %ebp
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph.i.i9
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	fputc
	decl	%ebp
	jne	.LBB6_8
.LBB6_9:                                # %_ZL11PrintRatingP8_IO_FILEy.exit10
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	fputs
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZL12PrintResultsP8_IO_FILEyyy, .Lfunc_end6-_ZL12PrintResultsP8_IO_FILEyyy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"size: "
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"CPU hardware threads:"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"usage:"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Benchmark threads:   "
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\nDict        Compressing          |        Decompressing\n   "
	.size	.L.str.4, 62

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"   Speed Usage    R/U Rating"
	.size	.L.str.5, 29

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n   "
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"    KB/s     %%   MIPS   MIPS"
	.size	.L.str.7, 30

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n\n"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%2d:"
	.size	.L.str.9, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"----------------------------------------------------------------\nAvr:"
	.size	.L.str.11, 70

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"     "
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\nTot:"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n\nSize"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" %5d"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%2d: "
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\nAvg:"
	.size	.L.str.17, 6

	.type	_ZTV14CBenchCallback,@object # @_ZTV14CBenchCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTV14CBenchCallback
	.p2align	3
_ZTV14CBenchCallback:
	.quad	0
	.quad	_ZTI14CBenchCallback
	.quad	_ZN14CBenchCallback15SetEncodeResultERK10CBenchInfob
	.quad	_ZN14CBenchCallback15SetDecodeResultERK10CBenchInfob
	.size	_ZTV14CBenchCallback, 32

	.type	_ZTS14CBenchCallback,@object # @_ZTS14CBenchCallback
	.globl	_ZTS14CBenchCallback
	.p2align	4
_ZTS14CBenchCallback:
	.asciz	"14CBenchCallback"
	.size	_ZTS14CBenchCallback, 17

	.type	_ZTS14IBenchCallback,@object # @_ZTS14IBenchCallback
	.section	.rodata._ZTS14IBenchCallback,"aG",@progbits,_ZTS14IBenchCallback,comdat
	.weak	_ZTS14IBenchCallback
	.p2align	4
_ZTS14IBenchCallback:
	.asciz	"14IBenchCallback"
	.size	_ZTS14IBenchCallback, 17

	.type	_ZTI14IBenchCallback,@object # @_ZTI14IBenchCallback
	.section	.rodata._ZTI14IBenchCallback,"aG",@progbits,_ZTI14IBenchCallback,comdat
	.weak	_ZTI14IBenchCallback
	.p2align	3
_ZTI14IBenchCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS14IBenchCallback
	.size	_ZTI14IBenchCallback, 16

	.type	_ZTI14CBenchCallback,@object # @_ZTI14CBenchCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTI14CBenchCallback
	.p2align	4
_ZTI14CBenchCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14CBenchCallback
	.quad	_ZTI14IBenchCallback
	.size	_ZTI14CBenchCallback, 24

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"  | "
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\nRAM %s "
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" MB,  # %s %3d"
	.size	.L.str.20, 15

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"       "
	.size	.L.str.21, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
