	.text
	.file	"bitap.bc"
	.globl	bitap
	.p2align	4, 0x90
	.type	bitap,@function
bitap:                                  # @bitap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$147512, %rsp           # imm = 0x24038
.Lcfi6:
	.cfi_def_cfa_offset 147568
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rdi, %r13
	callq	strlen
	movl	%eax, D_length(%rip)
	testl	%eax, %eax
	je	.LBB0_6
# BB#1:                                 # %.lr.ph179.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph179
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ecx
	movzbl	(%r13,%rcx), %edx
	cmpb	$94, %dl
	je	.LBB0_4
# BB#3:                                 # %.lr.ph179
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpb	$36, %dl
	jne	.LBB0_5
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$10, (%r13,%rcx)
	movl	D_length(%rip), %eax
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ecx
	cmpl	%eax, %ecx
	jb	.LBB0_2
	jmp	.LBB0_7
.LBB0_6:
	xorl	%eax, %eax
.LBB0_7:                                # %._crit_edge180
	cmpl	$0, REGEX(%rip)
	je	.LBB0_11
# BB#8:
	cmpl	$5, %ebp
	jge	.LBB0_76
# BB#9:
	cmpl	$15, %r15d
	jg	.LBB0_14
# BB#10:
	movl	16(%rsp), %edi          # 4-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	re
	jmp	.LBB0_75
.LBB0_11:
	testl	%ebp, %ebp
	jle	.LBB0_15
# BB#12:
	cmpl	$1, JUMP(%rip)
	jne	.LBB0_15
# BB#13:
	movq	%r13, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	callq	asearch1
	jmp	.LBB0_75
.LBB0_14:
	movl	16(%rsp), %edi          # 4-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	re1
	jmp	.LBB0_75
.LBB0_15:
	testl	%ebp, %ebp
	movl	16(%rsp), %ebx          # 4-byte Reload
	jle	.LBB0_17
# BB#16:
	movq	%r13, %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	asearch
	jmp	.LBB0_75
.LBB0_17:
	cmpl	$0, I(%rip)
	je	.LBB0_19
# BB#18:                                # %._crit_edge183
	movl	Init1(%rip), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	jmp	.LBB0_20
.LBB0_19:
	movl	$-1, Init1(%rip)
	movl	$-1, 12(%rsp)           # 4-byte Folded Spill
.LBB0_20:
	movl	Init(%rip), %r14d
	movl	D_endpos(%rip), %r8d
	movb	$10, 49199(%rsp)
	cmpl	$2, %eax
	movl	%r8d, %ebp
	jb	.LBB0_27
# BB#21:                                # %.lr.ph174.preheader
	leal	7(%rax), %esi
	leal	-2(%rax), %edx
	andl	$7, %esi
	je	.LBB0_25
# BB#22:                                # %.lr.ph174.prol.preheader
	xorl	%ecx, %ecx
	movl	%r8d, %ebp
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph174.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp), %edi
	orl	%edi, %ebp
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB0_23
# BB#24:                                # %.lr.ph174.prol.loopexit.unr-lcssa
	incl	%ecx
	cmpl	$7, %edx
	jae	.LBB0_26
	jmp	.LBB0_27
.LBB0_25:
	movl	$1, %ecx
	movl	%r8d, %ebp
	cmpl	$7, %edx
	jb	.LBB0_27
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph174
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp), %edx
	orl	%ebp, %edx
	leal	(%rdx,%rdx), %esi
	orl	%edx, %esi
	leal	(%rsi,%rsi), %edx
	orl	%esi, %edx
	leal	(%rdx,%rdx), %esi
	orl	%edx, %esi
	leal	(%rsi,%rsi), %edx
	orl	%esi, %edx
	leal	(%rdx,%rdx), %esi
	orl	%edx, %esi
	leal	(%rsi,%rsi), %edx
	orl	%esi, %edx
	leal	(%rdx,%rdx), %ebp
	orl	%edx, %ebp
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jb	.LBB0_26
.LBB0_27:                               # %._crit_edge175
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	notl	%ebp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$49152, %r12d           # imm = 0xC000
	movl	$1, %r15d
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_28:                               # %._crit_edge167
                                        #   in Loop: Header=BB0_31 Depth=1
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	44(%rsp), %eax          # 4-byte Reload
	subl	%r12d, %eax
	cmpl	$49153, %eax            # imm = 0xC001
	jl	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$1, TRUNCATE(%rip)
	movl	$49152, %eax            # imm = 0xC000
.LBB0_30:                               #   in Loop: Header=BB0_31 Depth=1
	movslq	%eax, %rbx
	leaq	49200(%rsp), %rdi
	subq	%rbx, %rdi
	movslq	%r12d, %rax
	leaq	48(%rsp,%rax), %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movl	$49152, %r12d           # imm = 0xC000
	subl	%ebx, %r12d
	movl	$1, %eax
	cmovsl	%eax, %r12d
	xorl	%r15d, %r15d
	movl	16(%rsp), %ebx          # 4-byte Reload
.LBB0_31:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_32 Depth 2
                                        #     Child Loop BB0_39 Depth 2
                                        #     Child Loop BB0_49 Depth 2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph.i
                                        #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	49200(%rsp), %rax
	leaq	(%rax,%rbp), %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebx, %edi
	callq	read
	addl	%eax, %ebp
	testl	%eax, %eax
	jle	.LBB0_34
# BB#33:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_32 Depth=2
	cmpl	$49152, %ebp            # imm = 0xC000
	jl	.LBB0_32
.LBB0_34:                               # %fill_buf.exit
                                        #   in Loop: Header=BB0_31 Depth=1
	testl	%ebp, %ebp
	jle	.LBB0_75
# BB#35:                                #   in Loop: Header=BB0_31 Depth=1
	testl	%r15d, %r15d
	je	.LBB0_41
# BB#36:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$49151, %ebx            # imm = 0xBFFF
	cmpl	$0, DELIMITER(%rip)
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	je	.LBB0_42
# BB#37:                                # %.preheader151
                                        #   in Loop: Header=BB0_31 Depth=1
	movslq	D_length(%rip), %rax
	testq	%rax, %rax
	movq	32(%rsp), %r8           # 8-byte Reload
	jle	.LBB0_43
# BB#38:                                # %.lr.ph
                                        #   in Loop: Header=BB0_31 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13,%rdi), %edx
	cmpb	49200(%rsp,%rdi), %dl
	jne	.LBB0_44
# BB#40:                                #   in Loop: Header=BB0_39 Depth=2
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB0_39
	jmp	.LBB0_44
.LBB0_41:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$49152, %ebx            # imm = 0xC000
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_45
.LBB0_42:                               #   in Loop: Header=BB0_31 Depth=1
	movq	32(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_45
.LBB0_43:                               #   in Loop: Header=BB0_31 Depth=1
	xorl	%edi, %edi
.LBB0_44:                               # %._crit_edge
                                        #   in Loop: Header=BB0_31 Depth=1
	xorl	%edx, %edx
	cmpl	%eax, %edi
	setge	%dl
	subl	%edx, %ecx
.LBB0_45:                               #   in Loop: Header=BB0_31 Depth=1
	leal	49152(%rbp), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	cmpl	$49151, %ebp            # imm = 0xBFFF
	movl	%eax, %edi
	jg	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_31 Depth=1
	movslq	%ebp, %rdi
	leaq	49200(%rsp), %rax
	addq	%rax, %rdi
	movslq	D_length(%rip), %rdx
	movq	%r13, %rsi
	movq	%rcx, %r15
	callq	strncpy
	movq	%r15, %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	D_length(%rip), %edi
	addl	44(%rsp), %edi          # 4-byte Folded Reload
	movb	$0, 48(%rsp,%rdi)
.LBB0_47:                               # %.preheader
                                        #   in Loop: Header=BB0_31 Depth=1
	cmpl	%edi, %ebx
	movl	20(%rsp), %edx          # 4-byte Reload
	jae	.LBB0_28
# BB#48:                                # %.lr.ph166
                                        #   in Loop: Header=BB0_31 Depth=1
	addl	$49151, %ebp            # imm = 0xBFFF
	incl	%ebx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_49:                               #   Parent Loop BB0_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rbx), %eax
	movsbq	48(%rsp,%rax), %rax
	movl	Mask(,%rax,4), %r15d
	movl	%r14d, %eax
	andl	%esi, %eax
	shrl	%r14d
	andl	%r15d, %r14d
	orl	%eax, %r14d
	testl	%edx, %r14d
	je	.LBB0_61
# BB#50:                                #   in Loop: Header=BB0_49 Depth=2
	incl	%ecx
	movl	AND(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_54
# BB#51:                                #   in Loop: Header=BB0_49 Depth=2
	cmpl	$1, %eax
	jne	.LBB0_55
# BB#52:                                #   in Loop: Header=BB0_49 Depth=2
	movl	endposition(%rip), %eax
	andl	%eax, %r14d
	cmpl	%eax, %r14d
	je	.LBB0_57
.LBB0_55:                               #   in Loop: Header=BB0_49 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_49 Depth=2
	testl	endposition(%rip), %r14d
	setne	%al
.LBB0_56:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB0_49 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_60
.LBB0_57:                               #   in Loop: Header=BB0_49 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_74
# BB#58:                                #   in Loop: Header=BB0_49 Depth=2
	cmpl	%ebp, %r12d
	jge	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_49 Depth=2
	movl	%ebx, %edx
	subl	D_length(%rip), %edx
	decl	%edx
	leaq	48(%rsp), %rdi
	movl	%r12d, %esi
	movq	%rcx, %r14
	callq	output
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB0_60:                               #   in Loop: Header=BB0_49 Depth=2
	movl	%ebx, %r12d
	subl	D_length(%rip), %r12d
	movl	$0, TRUNCATE(%rip)
	movl	Init(%rip), %r14d
	movl	%r14d, %eax
	andl	%esi, %eax
	shrl	%r14d
	andl	%r15d, %r14d
	orl	%eax, %r14d
	andl	%r8d, %r14d
.LBB0_61:                               #   in Loop: Header=BB0_49 Depth=2
	movl	%ebx, %eax
	movsbq	48(%rsp,%rax), %rax
	movl	Mask(,%rax,4), %r15d
	movl	%r14d, %eax
	andl	%esi, %eax
	shrl	%r14d
	andl	%r15d, %r14d
	orl	%eax, %r14d
	testl	%edx, %r14d
	je	.LBB0_73
# BB#62:                                #   in Loop: Header=BB0_49 Depth=2
	incl	%ecx
	movl	AND(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_66
# BB#63:                                #   in Loop: Header=BB0_49 Depth=2
	cmpl	$1, %eax
	jne	.LBB0_67
# BB#64:                                #   in Loop: Header=BB0_49 Depth=2
	movl	endposition(%rip), %eax
	andl	%eax, %r14d
	cmpl	%eax, %r14d
	je	.LBB0_69
.LBB0_67:                               #   in Loop: Header=BB0_49 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_66:                               #   in Loop: Header=BB0_49 Depth=2
	testl	endposition(%rip), %r14d
	setne	%al
.LBB0_68:                               # %thread-pre-split148.thread
                                        #   in Loop: Header=BB0_49 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_72
.LBB0_69:                               #   in Loop: Header=BB0_49 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_74
# BB#70:                                #   in Loop: Header=BB0_49 Depth=2
	cmpl	%ebp, %r12d
	jge	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_49 Depth=2
	movl	%ebx, %edx
	subl	D_length(%rip), %edx
	leaq	48(%rsp), %rdi
	movl	%r12d, %esi
	movq	%rcx, %r14
	callq	output
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
.LBB0_72:                               #   in Loop: Header=BB0_49 Depth=2
	movl	%ebx, %r12d
	subl	D_length(%rip), %r12d
	incl	%r12d
	movl	$0, TRUNCATE(%rip)
	movl	Init(%rip), %r14d
	movl	%r14d, %eax
	andl	%esi, %eax
	shrl	%r14d
	andl	%r15d, %r14d
	orl	%eax, %r14d
	andl	%r8d, %r14d
.LBB0_73:                               # %.backedge
                                        #   in Loop: Header=BB0_49 Depth=2
	leal	2(%rbx), %eax
	incl	%ebx
	cmpl	%edi, %ebx
	movl	%eax, %ebx
	jb	.LBB0_49
	jmp	.LBB0_28
.LBB0_74:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB0_75:                               # %.loopexit
	addq	$147512, %rsp           # imm = 0x24038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_76:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end0:
	.size	bitap, .Lfunc_end0-bitap
	.cfi_endproc

	.globl	fill_buf
	.p2align	4, 0x90
	.type	fill_buf,@function
fill_buf:                               # @fill_buf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movl	%edi, %ebp
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	jle	.LBB1_3
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	leaq	(%r14,%rbx), %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	callq	read
	addl	%eax, %ebx
	testl	%eax, %eax
	jle	.LBB1_3
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	%r15d, %ebx
	jl	.LBB1_1
.LBB1_3:                                # %._crit_edge
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fill_buf, .Lfunc_end1-fill_buf
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: the maximum number of erorrs allowed for full regular expression is 4\n"
	.size	.L.str, 75


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
