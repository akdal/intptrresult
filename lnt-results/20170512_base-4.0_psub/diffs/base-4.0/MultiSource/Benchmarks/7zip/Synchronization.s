	.text
	.file	"Synchronization.bc"
	.globl	sync_TestConstructor
	.p2align	4, 0x90
	.type	sync_TestConstructor,@function
sync_TestConstructor:                   # @sync_TestConstructor
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	cmpl	$305449726, _ZL15gbl_synchroTest(%rip) # imm = 0x1234CAFE
	jne	.LBB0_2
# BB#1:                                 # %_ZN12CSynchroTest15testConstructorEv.exit
	popq	%rax
	retq
.LBB0_2:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	sync_TestConstructor, .Lfunc_end0-sync_TestConstructor
	.cfi_endproc

	.globl	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	.p2align	4, 0x90
	.type	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij,@function
_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij: # @_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movl	%edi, %ebx
	testl	%edx, %edx
	jne	.LBB1_9
# BB#1:
	cmpl	$-1, %ecx
	jne	.LBB1_10
# BB#2:
	testl	%ebx, %ebx
	je	.LBB1_11
# BB#3:                                 # %.split.us.preheader
	movq	(%r12), %rax
	movq	8(%rax), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	leaq	40(%r14), %r15
	movl	%ebx, %r13d
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_7:                                # %._crit_edge.us
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	pthread_cond_wait
.LBB1_4:                                # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rbx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	testb	%al, %al
	jne	.LBB1_8
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	incq	%rbx
	cmpq	%r13, %rbx
	jb	.LBB1_5
	jmp	.LBB1_7
.LBB1_8:                                # %.us-lcssa.us
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_9:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	callq	printf
	callq	abort
.LBB1_10:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	callq	abort
.LBB1_11:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	printf
	callq	abort
.Lfunc_end1:
	.size	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij, .Lfunc_end1-_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	.cfi_endproc

	.type	_ZL15gbl_synchroTest,@object # @_ZL15gbl_synchroTest
	.data
	.p2align	2
_ZL15gbl_synchroTest:
	.long	305449726               # 0x1234cafe
	.size	_ZL15gbl_synchroTest, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\n INTERNAL ERROR - WaitForMultipleObjects(...) wait_all(%d) != FALSE\n\n"
	.size	.L.str, 72

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n\n INTERNAL ERROR - WaitForMultipleObjects(...) timeout(%u) != INFINITE\n\n"
	.size	.L.str.1, 74

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\n INTERNAL ERROR - WaitForMultipleObjects(...) count(%u) < 1\n\n"
	.size	.L.str.2, 64

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ERROR : no constructors called during loading of plugins (please look at LINK_SHARED in makefile.machine)"
	.size	.Lstr, 106


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
