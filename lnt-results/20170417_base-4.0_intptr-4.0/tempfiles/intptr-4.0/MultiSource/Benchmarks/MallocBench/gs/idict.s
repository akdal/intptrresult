	.text
	.file	"idict.bc"
	.globl	dict_create
	.p2align	4, 0x90
	.type	dict_create,@function
dict_create:                            # @dict_create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	1(%rdi), %eax
	testl	%edi, %edi
	movl	$2, %ebx
	cmovnel	%eax, %ebx
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %rbp
	movl	$-25, %r14d
	testq	%rbp, %rbp
	je	.LBB0_10
# BB#1:
	movl	$32, %esi
	movl	$.L.str.1, %edx
	movl	%ebx, %edi
	callq	alloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#3:
	movq	$0, (%rbp)
	movw	$20, 8(%rbp)
	movq	%rax, 16(%rbp)
	movw	$770, 24(%rbp)          # imm = 0x302
	leal	(%rbx,%rbx), %eax
	movw	%ax, 26(%rbp)
	movq	%rbp, (%r15)
	movw	$778, 8(%r15)           # imm = 0x30A
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	je	.LBB0_10
# BB#4:                                 # %.lr.ph.preheader
	movq	16(%rbp), %rax
	leal	-1(%rbx), %ecx
	movl	%ebx, %edx
	andl	$7, %edx
	je	.LBB0_7
# BB#5:                                 # %.lr.ph.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movw	$32, 24(%rax)
	movw	$32, 8(%rax)
	addq	$32, %rax
	incl	%edx
	jne	.LBB0_6
.LBB0_7:                                # %.lr.ph.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB0_10
# BB#8:                                 # %.lr.ph.preheader.new
	addq	$248, %rax
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	$32, -224(%rax)
	movw	$32, -240(%rax)
	movw	$32, -192(%rax)
	movw	$32, -208(%rax)
	movw	$32, -160(%rax)
	movw	$32, -176(%rax)
	movw	$32, -128(%rax)
	movw	$32, -144(%rax)
	movw	$32, -96(%rax)
	movw	$32, -112(%rax)
	movw	$32, -64(%rax)
	movw	$32, -80(%rax)
	movw	$32, -32(%rax)
	movw	$32, -48(%rax)
	addl	$-8, %ebx
	movw	$32, (%rax)
	movw	$32, -16(%rax)
	leaq	256(%rax), %rax
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_2:
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str, %ecx
	movq	%rbp, %rdi
	callq	alloc_free
.LBB0_10:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	dict_create, .Lfunc_end0-dict_create
	.cfi_endproc

	.globl	dict_access_ref
	.p2align	4, 0x90
	.type	dict_access_ref,@function
dict_access_ref:                        # @dict_access_ref
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	addq	$16, %rax
	retq
.Lfunc_end1:
	.size	dict_access_ref, .Lfunc_end1-dict_access_ref
	.cfi_endproc

	.globl	dict_lookup
	.p2align	4, 0x90
	.type	dict_lookup,@function
dict_lookup:                            # @dict_lookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 112
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	cmpb	$13, %cl
	je	.LBB2_3
# BB#1:
	cmpb	$7, %cl
	jne	.LBB2_6
# BB#2:
	movq	(%rbx), %r14
	jmp	.LBB2_5
.LBB2_3:
	movq	(%rbx), %rdi
	movzwl	10(%rbx), %esi
	leaq	40(%rsp), %rdx
	movl	$1, %ecx
	callq	name_ref
	testl	%eax, %eax
	js	.LBB2_32
# BB#4:
	movq	40(%rsp), %r14
.LBB2_5:
	movzwl	8(%r14), %eax
	imull	$40503, %eax, %r12d     # imm = 0x9E37
	movl	$7, %r13d
	jmp	.LBB2_7
.LBB2_6:
	movzwl	%ax, %eax
	imull	$99, %ecx, %ecx
	cmpl	$60, %eax
	movl	$891, %r12d             # imm = 0x37B
	cmovbel	%ecx, %r12d
	movl	$-1, %r13d
                                        # implicit-def: %R14
.LBB2_7:                                # %.preheader
	movl	$1, %eax
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB2_17
.LBB2_8:                                # %.outer.1104
                                        #   in Loop: Header=BB2_17 Depth=1
	shlq	$5, %rbp
	movl	12(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_9:                                # %.backedge.backedge.1
                                        #   in Loop: Header=BB2_10 Depth=2
	addq	$-32, %rbp
.LBB2_10:                               # %.backedge.1
                                        #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-24(%r15,%rbp), %eax
	andl	$252, %eax
	shrl	$2, %eax
	cmpl	%r13d, %eax
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=2
	cmpq	%r14, -32(%r15,%rbp)
	jne	.LBB2_9
	jmp	.LBB2_30
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=2
	cmpl	$8, %eax
	je	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_10 Depth=2
	leaq	-32(%r15,%rbp), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	obj_eq
	testl	%eax, %eax
	je	.LBB2_9
	jmp	.LBB2_30
.LBB2_14:                               #   in Loop: Header=BB2_17 Depth=1
	cmpq	$32, %rbp
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_17 Depth=1
	addq	%rbp, %r15
	jmp	.LBB2_25
.LBB2_16:                               #   in Loop: Header=BB2_17 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	movl	$-2, %ecx
	cmovgl	%ecx, %eax
	jmp	.LBB2_27
	.p2align	4, 0x90
.LBB2_17:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_19 Depth 2
                                        #     Child Loop BB2_10 Depth 2
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	(%rbp), %rax
	movzwl	26(%rax), %ebp
	shrl	%ebp
	movq	16(%rax), %r15
	movl	%ebp, %ecx
	decl	%ecx
	xorl	%edx, %edx
	movl	%r12d, %eax
	divl	%ecx
	movl	%edx, %r12d
	shlq	$5, %r12
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_18:                               # %.backedge.backedge
                                        #   in Loop: Header=BB2_19 Depth=2
	addq	$-32, %r12
.LBB2_19:                               # %.backedge
                                        #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	40(%r15,%r12), %eax
	andl	$252, %eax
	shrl	$2, %eax
	cmpl	%r13d, %eax
	jne	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_19 Depth=2
	cmpq	%r14, 32(%r15,%r12)
	jne	.LBB2_18
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_19 Depth=2
	cmpl	$8, %eax
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_19 Depth=2
	leaq	32(%r15,%r12), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	obj_eq
	testl	%eax, %eax
	je	.LBB2_18
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_23:                               #   in Loop: Header=BB2_17 Depth=1
	cmpq	$-32, %r12
	je	.LBB2_8
# BB#24:                                #   in Loop: Header=BB2_17 Depth=1
	leaq	64(%r15,%r12), %r15
	movl	12(%rsp), %r12d         # 4-byte Reload
.LBB2_25:                               #   in Loop: Header=BB2_17 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_17 Depth=1
	addq	$-16, %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r15, (%rax)
	xorl	%eax, %eax
.LBB2_27:                               #   in Loop: Header=BB2_17 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	addq	$-16, %rbp
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jae	.LBB2_17
	jmp	.LBB2_32
.LBB2_29:                               # %.loopexit105
	leaq	64(%r15,%r12), %r15
.LBB2_31:
	addq	$-16, %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r15, (%rax)
	movl	$1, %eax
.LBB2_32:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_30:                               # %.loopexit109
	addq	%rbp, %r15
	jmp	.LBB2_31
.Lfunc_end2:
	.size	dict_lookup, .Lfunc_end2-dict_lookup
	.cfi_endproc

	.globl	dict_put
	.p2align	4, 0x90
	.type	dict_put,@function
dict_put:                               # @dict_put
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	8(%rsp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB3_2
# BB#1:                                 # %._crit_edge
	movq	8(%rsp), %rax
.LBB3_11:
	movups	(%r14), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
.LBB3_12:                               # %.critedge
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	movq	(%rbx), %rcx
	movq	(%rcx), %rdx
	movzwl	26(%rcx), %eax
	shrl	%eax
	decl	%eax
	movslq	%eax, %rsi
	movl	$-2, %eax
	cmpq	%rsi, %rdx
	je	.LBB3_12
# BB#3:
	incq	%rdx
	movq	%rdx, (%rcx)
	movq	8(%rsp), %rax
	movups	(%r15), %xmm0
	movups	%xmm0, -16(%rax)
	movzwl	8(%r15), %edx
	andl	$252, %edx
	cmpl	$28, %edx
	jne	.LBB3_11
# BB#4:
	movq	(%r15), %rdx
	cmpq	$0, 24(%rdx)
	je	.LBB3_6
# BB#5:
	movl	$1, %ecx
	jmp	.LBB3_10
.LBB3_6:
	cmpq	dstack(%rip), %rcx
	je	.LBB3_9
# BB#7:
	cmpq	dstack+16(%rip), %rcx
	je	.LBB3_9
# BB#8:
	movl	$1, %ecx
	jmp	.LBB3_10
.LBB3_9:
	movq	%rax, %rcx
.LBB3_10:
	movq	%rcx, 24(%rdx)
	jmp	.LBB3_11
.Lfunc_end3:
	.size	dict_put, .Lfunc_end3-dict_put
	.cfi_endproc

	.globl	dict_length
	.p2align	4, 0x90
	.type	dict_length,@function
dict_length:                            # @dict_length
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	(%rax), %eax
	retq
.Lfunc_end4:
	.size	dict_length, .Lfunc_end4-dict_length
	.cfi_endproc

	.globl	dict_maxlength
	.p2align	4, 0x90
	.type	dict_maxlength,@function
dict_maxlength:                         # @dict_maxlength
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movzwl	26(%rax), %eax
	shrl	%eax
	decl	%eax
	retq
.Lfunc_end5:
	.size	dict_maxlength, .Lfunc_end5-dict_maxlength
	.cfi_endproc

	.globl	dict_copy
	.p2align	4, 0x90
	.type	dict_copy,@function
dict_copy:                              # @dict_copy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -48
.Lcfi36:
	.cfi_offset %r12, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	(%rdi), %rax
	movzwl	26(%rax), %ebx
	xorl	%r14d, %r14d
	shrl	%ebx
	je	.LBB6_16
# BB#1:                                 # %.lr.ph
	movq	16(%rax), %rbp
	negl	%ebx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movzwl	8(%rbp), %eax
	andl	$252, %eax
	cmpl	$32, %eax
	je	.LBB6_15
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB6_5
# BB#4:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rsp), %rax
	jmp	.LBB6_14
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%r12), %rcx
	movq	(%rcx), %rax
	movzwl	26(%rcx), %edx
	shrl	%edx
	decl	%edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rax
	je	.LBB6_17
# BB#6:                                 #   in Loop: Header=BB6_2 Depth=1
	incq	%rax
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movups	(%rbp), %xmm0
	movups	%xmm0, -16(%rax)
	movzwl	8(%rbp), %edx
	andl	$252, %edx
	cmpl	$28, %edx
	jne	.LBB6_14
# BB#7:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	(%rbp), %rdx
	cmpq	$0, 24(%rdx)
	je	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ecx
	jmp	.LBB6_13
.LBB6_9:                                #   in Loop: Header=BB6_2 Depth=1
	cmpq	dstack(%rip), %rcx
	je	.LBB6_12
# BB#10:                                #   in Loop: Header=BB6_2 Depth=1
	cmpq	dstack+16(%rip), %rcx
	je	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ecx
	jmp	.LBB6_13
.LBB6_12:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, %rcx
.LBB6_13:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, 24(%rdx)
	.p2align	4, 0x90
.LBB6_14:                               # %dict_put.exit.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movups	16(%rbp), %xmm0
	movups	%xmm0, (%rax)
.LBB6_15:                               #   in Loop: Header=BB6_2 Depth=1
	addq	$32, %rbp
	incl	%ebx
	jne	.LBB6_2
.LBB6_16:                               # %.loopexit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:                               # %dict_put.exit
	movl	$-2, %r14d
	jmp	.LBB6_16
.Lfunc_end6:
	.size	dict_copy, .Lfunc_end6-dict_copy
	.cfi_endproc

	.globl	dict_resize
	.p2align	4, 0x90
	.type	dict_resize,@function
dict_resize:                            # @dict_resize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	movq	(%r14), %r13
	leal	1(%rsi), %eax
	testl	%esi, %esi
	movl	$2, %ebx
	cmovnel	%eax, %ebx
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %r15
	movl	$-25, %ebp
	testq	%r15, %r15
	je	.LBB7_27
# BB#1:
	movl	$32, %esi
	movl	$.L.str.1, %edx
	movl	%ebx, %edi
	callq	alloc
	testq	%rax, %rax
	je	.LBB7_2
# BB#3:
	movq	$0, (%r15)
	movw	$20, 8(%r15)
	movq	%rax, 16(%r15)
	movw	$770, 24(%r15)          # imm = 0x302
	leal	(%rbx,%rbx), %ecx
	movw	%cx, 26(%r15)
	movq	%r15, 8(%rsp)
	movw	$778, 16(%rsp)          # imm = 0x30A
	testl	%ebx, %ebx
	je	.LBB7_10
# BB#4:                                 # %.lr.ph.preheader.i
	leal	-1(%rbx), %ecx
	movl	%ebx, %edx
	andl	$7, %edx
	je	.LBB7_7
# BB#5:                                 # %.lr.ph.i.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movw	$32, 24(%rax)
	movw	$32, 8(%rax)
	addq	$32, %rax
	incl	%edx
	jne	.LBB7_6
.LBB7_7:                                # %.lr.ph.i.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB7_10
# BB#8:                                 # %.lr.ph.preheader.i.new
	addq	$248, %rax
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movw	$32, -224(%rax)
	movw	$32, -240(%rax)
	movw	$32, -192(%rax)
	movw	$32, -208(%rax)
	movw	$32, -160(%rax)
	movw	$32, -176(%rax)
	movw	$32, -128(%rax)
	movw	$32, -144(%rax)
	movw	$32, -96(%rax)
	movw	$32, -112(%rax)
	movw	$32, -64(%rax)
	movw	$32, -80(%rax)
	movw	$32, -32(%rax)
	movw	$32, -48(%rax)
	addl	$-8, %ebx
	movw	$32, (%rax)
	movw	$32, -16(%rax)
	leaq	256(%rax), %rax
	jne	.LBB7_9
.LBB7_10:                               # %dict_create.exit
	movq	(%r14), %rax
	movzwl	26(%rax), %ebp
	shrl	%ebp
	je	.LBB7_26
# BB#11:                                # %.lr.ph.i10
	movq	16(%rax), %rbx
	negl	%ebp
	leaq	8(%rsp), %r15
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB7_12:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$32, %eax
	je	.LBB7_25
# BB#13:                                #   in Loop: Header=BB7_12 Depth=1
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB7_15
# BB#14:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB7_12 Depth=1
	movq	(%rsp), %rax
	jmp	.LBB7_24
.LBB7_15:                               #   in Loop: Header=BB7_12 Depth=1
	movq	8(%rsp), %rcx
	movq	(%rcx), %rax
	movzwl	26(%rcx), %edx
	shrl	%edx
	decl	%edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rax
	je	.LBB7_26
# BB#16:                                #   in Loop: Header=BB7_12 Depth=1
	incq	%rax
	movq	%rax, (%rcx)
	movq	(%rsp), %rax
	movups	(%rbx), %xmm0
	movups	%xmm0, -16(%rax)
	movzwl	8(%rbx), %edx
	andl	$252, %edx
	cmpl	$28, %edx
	jne	.LBB7_24
# BB#17:                                #   in Loop: Header=BB7_12 Depth=1
	movq	(%rbx), %rdx
	cmpq	$0, 24(%rdx)
	je	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_12 Depth=1
	movl	$1, %ecx
	jmp	.LBB7_23
.LBB7_19:                               #   in Loop: Header=BB7_12 Depth=1
	cmpq	dstack(%rip), %rcx
	je	.LBB7_22
# BB#20:                                #   in Loop: Header=BB7_12 Depth=1
	cmpq	dstack+16(%rip), %rcx
	je	.LBB7_22
# BB#21:                                #   in Loop: Header=BB7_12 Depth=1
	movl	$1, %ecx
	jmp	.LBB7_23
.LBB7_22:                               #   in Loop: Header=BB7_12 Depth=1
	movq	%rax, %rcx
.LBB7_23:                               #   in Loop: Header=BB7_12 Depth=1
	movq	%rcx, 24(%rdx)
	.p2align	4, 0x90
.LBB7_24:                               # %dict_put.exit.thread.i
                                        #   in Loop: Header=BB7_12 Depth=1
	movups	16(%rbx), %xmm0
	movups	%xmm0, (%rax)
.LBB7_25:                               #   in Loop: Header=BB7_12 Depth=1
	addq	$32, %rbx
	incl	%ebp
	jne	.LBB7_12
.LBB7_26:                               # %dict_copy.exit
	movq	16(%r13), %rdi
	movq	(%r14), %rax
	movzwl	26(%rax), %esi
	shrl	%esi
	decl	%esi
	movl	$32, %edx
	movl	$.L.str.2, %ecx
	callq	alloc_free
	movq	8(%rsp), %rdi
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%r13)
	movups	%xmm0, (%r13)
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str.3, %ecx
	callq	alloc_free
	xorl	%ebp, %ebp
	jmp	.LBB7_27
.LBB7_2:
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str, %ecx
	movq	%r15, %rdi
	callq	alloc_free
.LBB7_27:                               # %dict_create.exit.thread
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	dict_resize, .Lfunc_end7-dict_resize
	.cfi_endproc

	.globl	dict_first
	.p2align	4, 0x90
	.type	dict_first,@function
dict_first:                             # @dict_first
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movzwl	26(%rax), %eax
	shrl	%eax
	retq
.Lfunc_end8:
	.size	dict_first, .Lfunc_end8-dict_first
	.cfi_endproc

	.globl	dict_next
	.p2align	4, 0x90
	.type	dict_next,@function
dict_next:                              # @dict_next
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	movslq	%esi, %rax
	shlq	$5, %rax
	addq	16(%rcx), %rax
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	testl	%esi, %esi
	jle	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	decl	%esi
	movzwl	-24(%rax), %ecx
	addq	$-32, %rax
	andl	$252, %ecx
	cmpl	$32, %ecx
	je	.LBB9_1
# BB#4:
	movups	(%rax), %xmm0
	movups	%xmm0, (%rdx)
	movups	16(%rax), %xmm0
	movups	%xmm0, 16(%rdx)
	movl	%esi, %eax
	retq
.LBB9_2:
	movl	$-1, %esi
	movl	%esi, %eax
	retq
.Lfunc_end9:
	.size	dict_next, .Lfunc_end9-dict_next
	.cfi_endproc

	.type	dict_max_size,@object   # @dict_max_size
	.data
	.globl	dict_max_size
	.p2align	2
dict_max_size:
	.long	32766                   # 0x7ffe
	.size	dict_max_size, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"dict_create"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"dict_create(pairs)"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"dict_resize(old)"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"dict_resize(new)"
	.size	.L.str.3, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
