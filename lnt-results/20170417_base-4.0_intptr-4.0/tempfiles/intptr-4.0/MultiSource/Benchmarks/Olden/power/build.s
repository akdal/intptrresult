	.text
	.file	"build.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4594932627813569659     # double 0.16
	.text
	.globl	build_tree
	.p2align	4, 0x90
	.type	build_tree,@function
build_tree:                             # @build_tree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$152, %edi
	callq	malloc
	movq	%rax, %rbx
	xorl	%edi, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 64(%rbx)
	movl	$21, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 72(%rbx)
	movl	$42, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 80(%rbx)
	movl	$63, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 88(%rbx)
	movl	$84, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 96(%rbx)
	movl	$105, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 104(%rbx)
	movl	$126, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 112(%rbx)
	movl	$147, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 120(%rbx)
	movl	$168, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 128(%rbx)
	movl	$189, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 136(%rbx)
	movl	$210, %edi
	movl	$21, %esi
	callq	build_lateral
	movq	%rax, 144(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [8.000000e-01,1.600000e-01]
	movups	%xmm0, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	build_tree, .Lfunc_end0-build_tree
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	4524981037057390230     # double 3.3333333333333333E-6
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	build_lateral
	.p2align	4, 0x90
	.type	build_lateral,@function
build_lateral:                          # @build_lateral
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r12, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movl	%edi, %ebx
	testl	%r14d, %r14d
	je	.LBB1_2
# BB#1:
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r15
	leal	-1(%r14), %esi
	movl	%ebx, %edi
	callq	build_lateral
	movq	%rax, %r12
	addl	%ebx, %ebx
	leal	(%rbx,%rbx,2), %edi
	leal	-2(%r14,%r14), %eax
	leal	(%rax,%rax,2), %esi
	movl	$6, %edx
	callq	build_branch
	movq	%r12, 48(%r15)
	movq	%rax, 56(%r15)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [3.333333e-06,1.000000e-06]
	movups	%xmm0, 32(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r15)
	movq	%r15, %rax
	jmp	.LBB1_3
.LBB1_2:
	xorl	%eax, %eax
.LBB1_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	build_lateral, .Lfunc_end1-build_lateral
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI2_1:
	.quad	4547007122018943789     # double 1.0E-4
	.quad	4536524183238306033     # double 2.0000000000000002E-5
	.text
	.globl	build_branch
	.p2align	4, 0x90
	.type	build_branch,@function
build_branch:                           # @build_branch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	%edi, %r15d
	testl	%ebp, %ebp
	je	.LBB2_1
# BB#2:
	movl	$152, %edi
	callq	malloc
	movq	%rax, %rbx
	decl	%ebp
	movl	%r15d, %edi
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	build_branch
	movq	%rax, 48(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 56(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 64(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 80(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 88(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 96(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 104(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 112(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 120(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 128(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 136(%rbx)
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	movq	%rax, 144(%rbx)
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [1.000000e-04,2.000000e-05]
	movups	%xmm0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.LBB2_3
.LBB2_1:
	xorl	%ebx, %ebx
.LBB2_3:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	build_branch, .Lfunc_end2-build_branch
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	build_leaf
	.p2align	4, 0x90
	.type	build_leaf,@function
build_leaf:                             # @build_leaf
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movl	$32, %edi
	callq	malloc
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, (%rax)
	popq	%rcx
	retq
.Lfunc_end3:
	.size	build_leaf, .Lfunc_end3-build_leaf
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
