	.text
	.file	"deleteIndexEntry.bc"
	.globl	deleteIndexEntry
	.p2align	4, 0x90
	.type	deleteIndexEntry,@function
deleteIndexEntry:                       # @deleteIndexEntry
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	jle	.LBB0_2
# BB#1:
	movq	(%rbx), %rdi
	callq	deleteIndexNode
	jmp	.LBB0_5
.LBB0_2:
	je	.LBB0_3
# BB#4:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$deleteIndexEntry.name, %edi
	movl	$1, %esi
	callq	errorMessage
	jmp	.LBB0_5
.LBB0_3:
	movq	(%rbx), %rdi
	callq	deleteDataObject
.LBB0_5:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end0:
	.size	deleteIndexEntry, .Lfunc_end0-deleteIndexEntry
	.cfi_endproc

	.type	deleteIndexEntry.name,@object # @deleteIndexEntry.name
	.data
	.p2align	4
deleteIndexEntry.name:
	.asciz	"deleteIndexEntry"
	.size	deleteIndexEntry.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"invalid level"
	.size	.L.str, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
