	.text
	.file	"bmm.bc"
	.globl	my_rand_r
	.p2align	4, 0x90
	.type	my_rand_r,@function
my_rand_r:                              # @my_rand_r
	.cfi_startproc
# BB#0:
	imull	$1664525, (%rdi), %eax  # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	movl	%eax, (%rdi)
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	retq
.Lfunc_end0:
	.size	my_rand_r, .Lfunc_end0-my_rand_r
	.cfi_endproc

	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
	movq	%r8, %rdi
	movl	%r9d, %r11d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$1664525, %esi, %eax    # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	imull	$1664525, %eax, %esi    # imm = 0x19660D
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%r11d, %ecx
	shrl	%cl, %eax
	andl	$15, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, a(%rdi)
	addl	$1013904223, %esi       # imm = 0x3C6EF35F
	movl	%esi, %eax
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	leal	(%r10,%rdx), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	andl	$15, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, b(%rdi)
	incq	%rdx
	incl	%r11d
	addq	$4, %rdi
	cmpq	$1024, %rdx             # imm = 0x400
	jne	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	incq	%r10
	decl	%r9d
	addq	$4096, %r8              # imm = 0x1000
	cmpq	$1024, %r10             # imm = 0x400
	jne	.LBB1_1
# BB#4:
	retq
.Lfunc_end1:
	.size	init, .Lfunc_end1-init
	.cfi_endproc

	.globl	mm_inner
	.p2align	4, 0x90
	.type	mm_inner,@function
mm_inner:                               # @mm_inner
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	BLOCK(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_7
# BB#1:                                 # %.preheader25.us.preheader
	leal	(%rax,%rdi), %ebx
	leal	(%rax,%rdx), %ecx
	addl	%esi, %eax
	movslq	%edx, %r11
	movslq	%ecx, %rcx
	movslq	%esi, %r9
	movslq	%eax, %r14
	movslq	%edi, %r15
	movslq	%ebx, %r8
	movq	%r15, %rax
	shlq	$12, %rax
	leaq	a(%rax,%r11,4), %r12
	movq	%r11, %rax
	shlq	$12, %rax
	leaq	b(%rax,%r9,4), %r10
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader.us.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
                                        #       Child Loop BB2_4 Depth 3
	movq	%r10, %rdi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader.us.us
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_4 Depth 3
	movq	%r15, %rax
	shlq	$12, %rax
	leaq	c(%rax,%rbx,4), %r13
	movss	c(%rax,%rbx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%rdi, %rax
	movq	%r12, %rdx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm1
	addss	%xmm1, %xmm0
	incq	%rsi
	addq	$4, %rdx
	addq	$4096, %rax             # imm = 0x1000
	cmpq	%rcx, %rsi
	jl	.LBB2_4
# BB#5:                                 # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_3 Depth=2
	movss	%xmm0, (%r13)
	incq	%rbx
	addq	$4, %rdi
	cmpq	%r14, %rbx
	jl	.LBB2_3
# BB#6:                                 # %._crit_edge28.us
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r15
	addq	$4096, %r12             # imm = 0x1000
	cmpq	%r8, %r15
	jl	.LBB2_2
.LBB2_7:                                # %._crit_edge30
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	mm_inner, .Lfunc_end2-mm_inner
	.cfi_endproc

	.globl	matmult
	.p2align	4, 0x90
	.type	matmult,@function
matmult:                                # @matmult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	NUM(%rip), %eax
	movl	%eax, -44(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB3_14
# BB#1:                                 # %.preheader18.us.preheader
	xorl	%ecx, %ecx
	movl	BLOCK(%rip), %eax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader.us.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #       Child Loop BB3_4 Depth 3
                                        #         Child Loop BB3_6 Depth 4
                                        #           Child Loop BB3_7 Depth 5
                                        #             Child Loop BB3_8 Depth 6
	movslq	%ecx, %rcx
	movq	-40(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	movslq	%eax, %rdi
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	%rcx, %rax
	shlq	$12, %rax
	leaq	a(%rax), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader.us.us
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_4 Depth 3
                                        #         Child Loop BB3_6 Depth 4
                                        #           Child Loop BB3_7 Depth 5
                                        #             Child Loop BB3_8 Depth 6
	movslq	%eax, %r10
	movq	-40(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r10), %eax
	movl	%eax, -28(%rsp)         # 4-byte Spill
	movslq	%eax, %rdx
	leaq	b(,%r10,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_6 Depth 4
                                        #           Child Loop BB3_7 Depth 5
                                        #             Child Loop BB3_8 Depth 6
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leal	(%rax,%rcx), %r13d
	testl	%ecx, %ecx
	jle	.LBB3_11
# BB#5:                                 # %.preheader25.us.preheader.i.us.us
                                        #   in Loop: Header=BB3_4 Depth=3
	movslq	%eax, %r12
	movq	%r12, %rax
	shlq	$12, %rax
	addq	-8(%rsp), %rax          # 8-byte Folded Reload
	movq	-24(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12,4), %r14
	movslq	%r13d, %rbp
	movq	-16(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader.us.us.preheader.i.us.us
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        #       Parent Loop BB3_4 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_7 Depth 5
                                        #             Child Loop BB3_8 Depth 6
	movq	%rax, %r15
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader.us.us.i.us.us
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        #       Parent Loop BB3_4 Depth=3
                                        #         Parent Loop BB3_6 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB3_8 Depth 6
	movq	%r8, %rcx
	shlq	$12, %rcx
	leaq	c(%rcx,%rsi,4), %r11
	movss	c(%rcx,%rsi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%r14, %rcx
	movq	%r15, %r9
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        #       Parent Loop BB3_4 Depth=3
                                        #         Parent Loop BB3_6 Depth=4
                                        #           Parent Loop BB3_7 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm1
	addss	%xmm1, %xmm0
	incq	%rbx
	addq	$4096, %r9              # imm = 0x1000
	addq	$4, %rcx
	cmpq	%rbp, %rbx
	jl	.LBB3_8
# BB#9:                                 # %._crit_edge.us.us.i.us.us
                                        #   in Loop: Header=BB3_7 Depth=5
	movss	%xmm0, (%r11)
	incq	%rsi
	addq	$4, %r15
	cmpq	%rdx, %rsi
	jl	.LBB3_7
# BB#10:                                # %._crit_edge28.us.i.us.us
                                        #   in Loop: Header=BB3_6 Depth=4
	incq	%r8
	addq	$4096, %r14             # imm = 0x1000
	cmpq	%rdi, %r8
	jl	.LBB3_6
.LBB3_11:                               # %mm_inner.exit.us.us
                                        #   in Loop: Header=BB3_4 Depth=3
	cmpl	-44(%rsp), %r13d        # 4-byte Folded Reload
	movl	%r13d, %eax
	jl	.LBB3_4
# BB#12:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_3 Depth=2
	movl	-28(%rsp), %eax         # 4-byte Reload
	cmpl	-44(%rsp), %eax         # 4-byte Folded Reload
	jl	.LBB3_3
# BB#13:                                # %._crit_edge23.us
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	-32(%rsp), %ecx         # 4-byte Reload
	cmpl	-44(%rsp), %ecx         # 4-byte Folded Reload
	jl	.LBB3_2
.LBB3_14:                               # %._crit_edge27
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	matmult, .Lfunc_end3-matmult
	.cfi_endproc

	.globl	mm_sum
	.p2align	4, 0x90
	.type	mm_sum,@function
mm_sum:                                 # @mm_sum
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	BLOCK(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_1
# BB#2:                                 # %.preheader.us.preheader
	leal	(%rax,%rsi), %ecx
	addl	%edi, %eax
	movslq	%esi, %r8
	movslq	%ecx, %rcx
	movslq	%edi, %rdx
	movslq	%eax, %r9
	movq	%rdx, %rax
	shlq	$12, %rax
	leaq	c(%rax,%r8,4), %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movq	%rax, %rdi
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	(%rdi), %xmm0
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rcx, %rsi
	jl	.LBB4_4
# BB#5:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%rdx
	addq	$4096, %rax             # imm = 0x1000
	cmpq	%r9, %rdx
	jl	.LBB4_3
# BB#6:                                 # %._crit_edge21
	retq
.LBB4_1:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end4:
	.size	mm_sum, .Lfunc_end4-mm_sum
	.cfi_endproc

	.globl	sumup
	.p2align	4, 0x90
	.type	sumup,@function
sumup:                                  # @sumup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	NUM(%rip), %r11d
	xorps	%xmm0, %xmm0
	testl	%r11d, %r11d
	jle	.LBB5_11
# BB#1:                                 # %.preheader.us.preheader
	xorl	%r8d, %r8d
	movl	BLOCK(%rip), %r14d
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_3 Depth 2
                                        #       Child Loop BB5_6 Depth 3
                                        #         Child Loop BB5_7 Depth 4
	movslq	%r8d, %r10
	leal	(%r14,%r10), %r8d
	movslq	%r8d, %rdi
	movq	%r10, %rax
	shlq	$12, %rax
	leaq	c(%rax), %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_3:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_6 Depth 3
                                        #         Child Loop BB5_7 Depth 4
	leal	(%rax,%r14), %r15d
	testl	%r14d, %r14d
	jle	.LBB5_4
# BB#5:                                 # %.preheader.us.preheader.i.us
                                        #   in Loop: Header=BB5_3 Depth=2
	movslq	%eax, %rbp
	leaq	(%r9,%rbp,4), %rdx
	movslq	%r15d, %rbx
	xorps	%xmm1, %xmm1
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB5_6:                                # %.preheader.us.i.us
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_7 Depth 4
	movq	%rdx, %rax
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB5_7:                                #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_3 Depth=2
                                        #       Parent Loop BB5_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addss	(%rax), %xmm1
	incq	%rsi
	addq	$4, %rax
	cmpq	%rbx, %rsi
	jl	.LBB5_7
# BB#8:                                 # %._crit_edge.us.i.us
                                        #   in Loop: Header=BB5_6 Depth=3
	incq	%rcx
	addq	$4096, %rdx             # imm = 0x1000
	cmpq	%rdi, %rcx
	jl	.LBB5_6
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_3 Depth=2
	xorps	%xmm1, %xmm1
.LBB5_9:                                # %mm_sum.exit.us
                                        #   in Loop: Header=BB5_3 Depth=2
	addss	%xmm1, %xmm0
	cmpl	%r11d, %r15d
	movl	%r15d, %eax
	jl	.LBB5_3
# BB#10:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpl	%r11d, %r8d
	jl	.LBB5_2
.LBB5_11:                               # %._crit_edge21
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	sumup, .Lfunc_end5-sumup
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	cmpl	$3, %edi
	jne	.LBB6_1
# BB#3:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	movl	%r14d, NUM(%rip)
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, BLOCK(%rip)
	cmpl	$1024, %r14d            # imm = 0x400
	ja	.LBB6_5
# BB#4:
	cmpl	%r14d, %eax
	ja	.LBB6_5
# BB#6:
	callq	init
	callq	matmult
	callq	sumup
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB6_1:
	movl	$.Lstr.1, %edi
	jmp	.LBB6_2
.LBB6_5:
	movl	$.Lstr, %edi
.LBB6_2:
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	c,@object               # @c
	.bss
	.globl	c
	.p2align	4
c:
	.zero	4194304
	.size	c, 4194304

	.type	a,@object               # @a
	.comm	a,4194304,16
	.type	b,@object               # @b
	.comm	b,4194304,16
	.type	BLOCK,@object           # @BLOCK
	.comm	BLOCK,4,4
	.type	NUM,@object             # @NUM
	.comm	NUM,4,4
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"final sum = %f\n"
	.size	.L.str.2, 16

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"size must be in [0, 1024]; block must be <= than size"
	.size	.Lstr, 54

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Usage: bmm <size> <block>"
	.size	.Lstr.1, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
