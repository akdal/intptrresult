	.text
	.file	"btAlignedAllocator.bc"
	.globl	_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E
	.p2align	4, 0x90
	.type	_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E,@function
_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E: # @_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	movl	$_ZL21btAlignedAllocDefaultmi, %eax
	cmovneq	%rdi, %rax
	movq	%rax, _ZL17sAlignedAllocFunc(%rip)
	testq	%rsi, %rsi
	movl	$_ZL20btAlignedFreeDefaultPv, %eax
	cmovneq	%rsi, %rax
	movq	%rax, _ZL16sAlignedFreeFunc(%rip)
	retq
.Lfunc_end0:
	.size	_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E, .Lfunc_end0-_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL21btAlignedAllocDefaultmi,@function
_ZL21btAlignedAllocDefaultmi:           # @_ZL21btAlignedAllocDefaultmi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movslq	%esi, %rbx
	leaq	7(%rdi,%rbx), %rdi
	callq	*_ZL10sAllocFunc(%rip)
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	leaq	8(%rax), %rcx
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rbx
	andq	%rdx, %rbx
	leaq	8(%rax,%rbx), %rcx
	movq	%rax, (%rax,%rbx)
	jmp	.LBB1_3
.LBB1_1:
	xorl	%ecx, %ecx
.LBB1_3:
	movq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZL21btAlignedAllocDefaultmi, .Lfunc_end1-_ZL21btAlignedAllocDefaultmi
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL20btAlignedFreeDefaultPv,@function
_ZL20btAlignedFreeDefaultPv:            # @_ZL20btAlignedFreeDefaultPv
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	movq	-8(%rdi), %rdi
	jmpq	*_ZL9sFreeFunc(%rip)    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	_ZL20btAlignedFreeDefaultPv, .Lfunc_end2-_ZL20btAlignedFreeDefaultPv
	.cfi_endproc

	.globl	_Z23btAlignedAllocSetCustomPFPvmEPFvS_E
	.p2align	4, 0x90
	.type	_Z23btAlignedAllocSetCustomPFPvmEPFvS_E,@function
_Z23btAlignedAllocSetCustomPFPvmEPFvS_E: # @_Z23btAlignedAllocSetCustomPFPvmEPFvS_E
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	movl	$_ZL14btAllocDefaultm, %eax
	cmovneq	%rdi, %rax
	movq	%rax, _ZL10sAllocFunc(%rip)
	testq	%rsi, %rsi
	movl	$_ZL13btFreeDefaultPv, %eax
	cmovneq	%rsi, %rax
	movq	%rax, _ZL9sFreeFunc(%rip)
	retq
.Lfunc_end3:
	.size	_Z23btAlignedAllocSetCustomPFPvmEPFvS_E, .Lfunc_end3-_Z23btAlignedAllocSetCustomPFPvmEPFvS_E
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL14btAllocDefaultm,@function
_ZL14btAllocDefaultm:                   # @_ZL14btAllocDefaultm
	.cfi_startproc
# BB#0:
	jmp	malloc                  # TAILCALL
.Lfunc_end4:
	.size	_ZL14btAllocDefaultm, .Lfunc_end4-_ZL14btAllocDefaultm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL13btFreeDefaultPv,@function
_ZL13btFreeDefaultPv:                   # @_ZL13btFreeDefaultPv
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end5:
	.size	_ZL13btFreeDefaultPv, .Lfunc_end5-_ZL13btFreeDefaultPv
	.cfi_endproc

	.globl	_Z22btAlignedAllocInternalmi
	.p2align	4, 0x90
	.type	_Z22btAlignedAllocInternalmi,@function
_Z22btAlignedAllocInternalmi:           # @_Z22btAlignedAllocInternalmi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	incl	gNumAlignedAllocs(%rip)
	movslq	%esi, %rbx
	leaq	7(%rdi,%rbx), %rdi
	callq	*_ZL10sAllocFunc(%rip)
	testq	%rax, %rax
	je	.LBB6_1
# BB#2:
	leaq	8(%rax), %rcx
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rbx
	andq	%rdx, %rbx
	leaq	8(%rax,%rbx), %rcx
	movq	%rax, (%rax,%rbx)
	jmp	.LBB6_3
.LBB6_1:
	xorl	%ecx, %ecx
.LBB6_3:
	movq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_Z22btAlignedAllocInternalmi, .Lfunc_end6-_Z22btAlignedAllocInternalmi
	.cfi_endproc

	.globl	_Z21btAlignedFreeInternalPv
	.p2align	4, 0x90
	.type	_Z21btAlignedFreeInternalPv,@function
_Z21btAlignedFreeInternalPv:            # @_Z21btAlignedFreeInternalPv
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#2:
	incl	gNumAlignedFree(%rip)
	movq	-8(%rdi), %rdi
	jmpq	*_ZL9sFreeFunc(%rip)    # TAILCALL
.LBB7_1:
	retq
.Lfunc_end7:
	.size	_Z21btAlignedFreeInternalPv, .Lfunc_end7-_Z21btAlignedFreeInternalPv
	.cfi_endproc

	.type	gNumAlignedAllocs,@object # @gNumAlignedAllocs
	.bss
	.globl	gNumAlignedAllocs
	.p2align	2
gNumAlignedAllocs:
	.long	0                       # 0x0
	.size	gNumAlignedAllocs, 4

	.type	gNumAlignedFree,@object # @gNumAlignedFree
	.globl	gNumAlignedFree
	.p2align	2
gNumAlignedFree:
	.long	0                       # 0x0
	.size	gNumAlignedFree, 4

	.type	gTotalBytesAlignedAllocs,@object # @gTotalBytesAlignedAllocs
	.globl	gTotalBytesAlignedAllocs
	.p2align	2
gTotalBytesAlignedAllocs:
	.long	0                       # 0x0
	.size	gTotalBytesAlignedAllocs, 4

	.type	_ZL17sAlignedAllocFunc,@object # @_ZL17sAlignedAllocFunc
	.data
	.p2align	3
_ZL17sAlignedAllocFunc:
	.quad	_ZL21btAlignedAllocDefaultmi
	.size	_ZL17sAlignedAllocFunc, 8

	.type	_ZL16sAlignedFreeFunc,@object # @_ZL16sAlignedFreeFunc
	.p2align	3
_ZL16sAlignedFreeFunc:
	.quad	_ZL20btAlignedFreeDefaultPv
	.size	_ZL16sAlignedFreeFunc, 8

	.type	_ZL10sAllocFunc,@object # @_ZL10sAllocFunc
	.p2align	3
_ZL10sAllocFunc:
	.quad	_ZL14btAllocDefaultm
	.size	_ZL10sAllocFunc, 8

	.type	_ZL9sFreeFunc,@object   # @_ZL9sFreeFunc
	.p2align	3
_ZL9sFreeFunc:
	.quad	_ZL13btFreeDefaultPv
	.size	_ZL9sFreeFunc, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
