	.text
	.file	"em3d.bc"
	.globl	compute_nodes
	.p2align	4, 0x90
	.type	compute_nodes,@function
compute_nodes:                          # @compute_nodes
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	jne	.LBB0_2
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_10:                               # %._crit_edge.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	andl	$-2, %r9d
	cmpl	%r10d, %r9d
	je	.LBB0_12
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%r9d, %r9d
	cmpl	%r10d, %r9d
	jne	.LBB0_16
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	%r10d, %rcx
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movq	(%rax,%rcx,8), %rax
	movsd	(%rdx,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	testq	%rax, %rax
	je	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_2 Depth=1
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	jmp	.LBB0_15
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	xorpd	%xmm2, %xmm2
.LBB0_15:                               #   in Loop: Header=BB0_2 Depth=1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
.LBB0_16:                               #   in Loop: Header=BB0_2 Depth=1
	movsd	%xmm0, (%r8)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
.LBB0_2:                                # %.lr.ph60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
	movq	(%rdi), %r8
	movsd	(%r8), %xmm0            # xmm0 = mem[0],zero
	movslq	40(%rdi), %r9
	leaq	-1(%r9), %r10
	cmpq	$2, %r9
	jl	.LBB0_3
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rdx,8), %rcx
	movsd	(%rax,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm2, %xmm2
	testq	%rcx, %rcx
	xorpd	%xmm3, %xmm3
	je	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	movsd	(%rcx), %xmm3           # xmm3 = mem[0],zero
.LBB0_7:                                #   in Loop: Header=BB0_5 Depth=2
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	movq	8(%rsi,%rdx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_5 Depth=2
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
.LBB0_9:                                #   in Loop: Header=BB0_5 Depth=2
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	addq	$2, %rdx
	cmpq	%r10, %rdx
	jl	.LBB0_5
	jmp	.LBB0_10
.LBB0_17:                               # %._crit_edge61
	retq
.Lfunc_end0:
	.size	compute_nodes, .Lfunc_end0-compute_nodes
	.cfi_endproc

	.type	nonlocals,@object       # @nonlocals
	.bss
	.globl	nonlocals
	.p2align	2
nonlocals:
	.long	0                       # 0x0
	.size	nonlocals, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
