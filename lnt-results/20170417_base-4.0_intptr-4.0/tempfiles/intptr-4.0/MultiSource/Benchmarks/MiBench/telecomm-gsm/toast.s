	.text
	.file	"toast.bc"
	.p2align	4, 0x90
	.type	generic_init,@function
generic_init:                           # @generic_init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	generic_init, .Lfunc_end0-generic_init
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %r14d
	movq	(%rbx), %rax
	testq	%rax, %rax
	movl	$.L.str.14, %ebp
	cmovneq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_1
# BB#2:
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	je	.LBB1_4
# BB#3:
	movq	%rax, %rcx
	incq	%rcx
	cmpb	$0, 1(%rax)
	cmovneq	%rcx, %rbp
	jmp	.LBB1_4
.LBB1_1:
	xorl	%ebp, %ebp
.LBB1_4:                                # %endname.exit.i
	movq	%rbp, progname(%rip)
	movl	$.L.str.15, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_6
# BB#5:
	movl	$1, f_decode(%rip)
.LBB1_6:
	movq	%rbp, %rdi
	callq	strlen
	cmpl	$3, %eax
	jl	.LBB1_12
# BB#7:
	cltq
	cmpb	$99, -3(%rbp,%rax)
	jne	.LBB1_12
# BB#8:
	cmpb	$97, -2(%rbp,%rax)
	jne	.LBB1_12
# BB#9:
	cmpb	$116, -1(%rbp,%rax)
	jne	.LBB1_12
# BB#10:
	cmpb	$0, (%rbp,%rax)
	jne	.LBB1_12
# BB#11:
	movl	$1, f_decode(%rip)
	movl	$1, f_cat(%rip)
.LBB1_12:                               # %parse_argv0.exit.preheader
	movl	$f_alaw, %r15d
	movl	$f_linear, %r12d
	movl	$f_audio, %r13d
	movl	$f_ulaw, %ebp
	jmp	.LBB1_13
.LBB1_16:                               #   in Loop: Header=BB1_13 Depth=1
	movl	$1, f_force(%rip)
	.p2align	4, 0x90
.LBB1_13:                               # %parse_argv0.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.12, %edx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incl	%eax
	cmpl	$119, %eax
	ja	.LBB1_34
# BB#14:                                # %parse_argv0.exit
                                        #   in Loop: Header=BB1_13 Depth=1
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_19:                               #   in Loop: Header=BB1_13 Depth=1
	movl	$1, f_fast(%rip)
	jmp	.LBB1_13
.LBB1_29:                               #   in Loop: Header=BB1_13 Depth=1
	movq	f_format(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_13 Depth=1
	cmpq	%r13, %rax
	jne	.LBB1_39
.LBB1_31:                               # %set_format.exit14
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	$f_audio, f_format(%rip)
	jmp	.LBB1_13
.LBB1_26:                               #   in Loop: Header=BB1_13 Depth=1
	movq	f_format(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_13 Depth=1
	cmpq	%r15, %rax
	jne	.LBB1_39
.LBB1_28:                               # %set_format.exit12
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	$f_alaw, f_format(%rip)
	jmp	.LBB1_13
.LBB1_17:                               #   in Loop: Header=BB1_13 Depth=1
	movl	$1, f_cat(%rip)
	jmp	.LBB1_13
.LBB1_15:                               #   in Loop: Header=BB1_13 Depth=1
	movl	$1, f_decode(%rip)
	jmp	.LBB1_13
.LBB1_23:                               #   in Loop: Header=BB1_13 Depth=1
	movq	f_format(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_13 Depth=1
	cmpq	%r12, %rax
	jne	.LBB1_39
.LBB1_25:                               # %set_format.exit10
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	$f_linear, f_format(%rip)
	jmp	.LBB1_13
.LBB1_18:                               #   in Loop: Header=BB1_13 Depth=1
	movl	$1, f_precious(%rip)
	jmp	.LBB1_13
.LBB1_20:                               #   in Loop: Header=BB1_13 Depth=1
	movq	f_format(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_13 Depth=1
	cmpq	%rbp, %rax
	jne	.LBB1_39
.LBB1_22:                               # %set_format.exit
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	$f_ulaw, f_format(%rip)
	jmp	.LBB1_13
.LBB1_35:
	movl	f_cat(%rip), %eax
	orl	%eax, f_precious(%rip)
	movslq	optind(%rip), %rbp
	movl	$1, %edi
	movl	$onintr, %esi
	callq	signal
	movl	$2, %edi
	movl	$onintr, %esi
	callq	signal
	movl	$13, %edi
	movl	$onintr, %esi
	callq	signal
	movl	$15, %edi
	movl	$onintr, %esi
	callq	signal
	movl	$25, %edi
	movl	$onintr, %esi
	callq	signal
	cmpl	%ebp, %r14d
	jle	.LBB1_40
# BB#36:                                # %.lr.ph.preheader
	leaq	(%rbx,%rbp,8), %rbx
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	$8, %rbx
	callq	process
	incl	%ebp
	jne	.LBB1_37
# BB#38:                                # %.loopexit
	xorl	%edi, %edi
	callq	exit
.LBB1_39:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB1_34:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB1_33:
	movq	progname(%rip), %rsi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	xorl	%edi, %edi
	callq	exit
.LBB1_32:
	movq	progname(%rip), %rsi
	movl	$.L.str.18, %edi
	movl	$.L.str.19, %edx
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB1_40:
	xorl	%edi, %edi
	callq	process
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_35
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_19
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_26
	.quad	.LBB1_34
	.quad	.LBB1_17
	.quad	.LBB1_15
	.quad	.LBB1_34
	.quad	.LBB1_16
	.quad	.LBB1_34
	.quad	.LBB1_33
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_23
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_18
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_29
	.quad	.LBB1_34
	.quad	.LBB1_20
	.quad	.LBB1_32

	.text
	.p2align	4, 0x90
	.type	onintr,@function
onintr:                                 # @onintr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	outname(%rip), %rdi
	movq	$0, outname(%rip)
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	unlink
.LBB2_2:
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	onintr, .Lfunc_end2-onintr
	.cfi_endproc

	.p2align	4, 0x90
	.type	process,@function
process:                                # @process
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	$0, out(%rip)
	movq	$0, in(%rip)
	movq	$0, outname(%rip)
	movq	$0, inname(%rip)
	movq	f_format(%rip), %rbp
	movq	$0, instat+16(%rip)
	testq	%r14, %r14
	je	.LBB3_1
# BB#2:
	cmpl	$0, f_decode(%rip)
	je	.LBB3_4
# BB#3:
	movl	$.L.str.41, %esi
	movl	$.L.str.45, %edx
	movq	%r14, %rdi
	callq	normalname
	jmp	.LBB3_27
.LBB3_1:
	movq	$0, inname(%rip)
	movq	stdin(%rip), %rax
	movq	%rax, in(%rip)
	jmp	.LBB3_51
.LBB3_4:
	cmpl	$0, f_cat(%rip)
	jne	.LBB3_25
# BB#5:
	movq	%r14, %rdi
	callq	strlen
	cmpq	$5, %rax
	jb	.LBB3_25
# BB#6:
	leaq	-4(%r14,%rax), %rdx
	xorl	%ecx, %ecx
	cmpl	$1836279598, -4(%r14,%rax) # imm = 0x6D73672E
	setne	%al
	testq	%rdx, %rdx
	je	.LBB3_25
# BB#7:
	movb	%al, %cl
	testl	%ecx, %ecx
	je	.LBB3_8
.LBB3_25:                               # %suffix.exit.thread.i
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	incq	%rbx
	movq	%rbx, %rdi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_101
# BB#26:                                # %emalloc.exit.i
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
.LBB3_27:
	movq	%rax, %rdi
	movq	%rdi, inname(%rip)
	movl	$.L.str.43, %esi
	callq	fopen
	movq	%rax, in(%rip)
	movq	inname(%rip), %rbx
	testq	%rax, %rax
	je	.LBB3_28
# BB#29:
	movq	%rax, %rdi
	callq	fileno
	movl	$1, %edi
	movl	$instat, %edx
	movl	%eax, %esi
	callq	__fxstat
	testl	%eax, %eax
	js	.LBB3_30
# BB#31:
	movl	$61440, %eax            # imm = 0xF000
	andl	instat+24(%rip), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB3_32
# BB#34:
	movq	instat+16(%rip), %rax
	cmpq	$2, %rax
	jb	.LBB3_37
# BB#35:
	movl	f_precious(%rip), %ecx
	orl	f_cat(%rip), %ecx
	je	.LBB3_36
.LBB3_37:                               # %okay_as_input.exit.i
	testq	%rbp, %rbp
	jne	.LBB3_51
# BB#38:
	movq	inname(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_50
# BB#39:
	movl	$.L.str.45, %esi
	movl	$.L.str.41, %edx
	callq	normalname
	movq	%rax, %r15
	movq	alldescs(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_49
# BB#40:                                # %.lr.ph.i.i.preheader
	movl	$alldescs+8, %ebx
	.p2align	4, 0x90
.LBB3_41:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB3_48
# BB#42:                                #   in Loop: Header=BB3_41 Depth=1
	cmpb	$0, (%rbp)
	je	.LBB3_48
# BB#43:                                #   in Loop: Header=BB3_41 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB3_48
# BB#44:                                #   in Loop: Header=BB3_41 Depth=1
	subq	%rax, %r12
	jbe	.LBB3_48
# BB#45:                                #   in Loop: Header=BB3_41 Depth=1
	leaq	(%r15,%r12), %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	memcmp
	addq	%r15, %r12
	je	.LBB3_48
# BB#46:                                #   in Loop: Header=BB3_41 Depth=1
	testl	%eax, %eax
	je	.LBB3_47
	.p2align	4, 0x90
.LBB3_48:                               # %suffix.exit.thread.i.i
                                        #   in Loop: Header=BB3_41 Depth=1
	movq	(%rbx), %rax
	addq	$8, %rbx
	testq	%rax, %rax
	jne	.LBB3_41
.LBB3_49:                               # %._crit_edge.i.i
	movq	%r15, %rdi
	callq	free
.LBB3_50:
	xorl	%ebp, %ebp
.LBB3_51:
	testq	%rbp, %rbp
	movl	$f_ulaw, %eax
	cmovneq	%rbp, %rax
	testq	%r14, %r14
	movq	48(%rax), %rcx
	movq	%rcx, output(%rip)
	movq	40(%rax), %rcx
	movq	%rcx, input(%rip)
	movq	24(%rax), %rcx
	movq	%rcx, init_input(%rip)
	movq	32(%rax), %rax
	movq	%rax, init_output(%rip)
	je	.LBB3_53
# BB#52:
	movl	f_cat(%rip), %edx
	testl	%edx, %edx
	jne	.LBB3_53
# BB#54:
	cmpl	$0, f_decode(%rip)
	movl	$plainname, %eax
	movl	$codename, %ecx
	cmovneq	%rax, %rcx
	movq	%r14, %rdi
	callq	*%rcx
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_10
# BB#55:
	movl	$193, %esi
	movl	$438, %edx              # imm = 0x1B6
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB3_57
# BB#56:
	movl	$.L.str.51, %esi
	movl	%ebp, %edi
	callq	fdopen
	jmp	.LBB3_70
.LBB3_53:
	movq	stdout(%rip), %rdx
	movq	%rdx, out(%rip)
	xorl	%ebx, %ebx
	jmp	.LBB3_72
.LBB3_57:
	callq	__errno_location
	cmpl	$17, (%rax)
	jne	.LBB3_58
# BB#61:
	cmpl	$0, f_force(%rip)
	je	.LBB3_62
.LBB3_69:                               # %ok_to_replace.exit.i
	movl	$.L.str.51, %esi
	movq	%rbx, %rdi
	callq	fopen
.LBB3_70:
	movq	%rax, out(%rip)
	testq	%rax, %rax
	je	.LBB3_59
# BB#71:                                # %.open_output.exit_crit_edge
	movq	init_output(%rip), %rax
	movq	init_input(%rip), %rcx
.LBB3_72:                               # %open_output.exit
	movq	%rbx, outname(%rip)
	cmpl	$0, f_decode(%rip)
	cmovneq	%rax, %rcx
	callq	*%rcx
	testl	%eax, %eax
	jne	.LBB3_73
# BB#74:
	cmpl	$0, f_decode(%rip)
	movl	$process_decode, %eax
	movl	$process_encode, %ecx
	cmovneq	%rax, %rcx
	callq	*%rcx
	testl	%eax, %eax
	je	.LBB3_75
.LBB3_10:                               # %open_input.exit.thread
	movq	out(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_16
# BB#11:                                # %open_input.exit.thread
	cmpq	stdout(%rip), %rdi
	je	.LBB3_16
# BB#12:
	callq	fclose
	movq	$0, out(%rip)
	movq	outname(%rip), %rdi
	callq	unlink
	testl	%eax, %eax
	jns	.LBB3_16
# BB#13:
	callq	__errno_location
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.LBB3_16
# BB#14:
	cmpl	$4, %eax
	jne	.LBB3_15
.LBB3_16:
	movq	in(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_19
# BB#17:
	cmpq	stdin(%rip), %rdi
	je	.LBB3_19
# BB#18:
	callq	fclose
	movq	$0, in(%rip)
.LBB3_19:
	movq	inname(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_22
# BB#20:
	cmpq	%r14, %rdi
	je	.LBB3_22
# BB#21:
	callq	free
.LBB3_22:
	movq	outname(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_100
# BB#23:
	cmpq	%r14, %rdi
	je	.LBB3_100
# BB#24:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB3_75:
	movq	out(%rip), %rdi
	callq	fflush
	testl	%eax, %eax
	js	.LBB3_77
# BB#76:
	movq	out(%rip), %rbx
	movq	%rbx, %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB3_77
# BB#78:
	cmpq	stdout(%rip), %rbx
	je	.LBB3_93
# BB#79:
	movq	instat+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_82
# BB#80:
	movq	outname(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_82
# BB#81:
	movq	instat+72(%rip), %rax
	movq	%rax, (%rsp)
	movq	instat+88(%rip), %rax
	movq	%rax, 8(%rsp)
	movq	%rsp, %rsi
	callq	utime
	movq	instat+16(%rip), %rax
.LBB3_82:                               # %update_times.exit
	testq	%rax, %rax
	je	.LBB3_85
# BB#83:
	movq	out(%rip), %rdi
	callq	fileno
	movl	instat+24(%rip), %esi
	andl	$4095, %esi             # imm = 0xFFF
	movl	%eax, %edi
	callq	fchmod
	testl	%eax, %eax
	jne	.LBB3_84
.LBB3_85:                               # %update_mode.exit
	cmpq	$0, instat+16(%rip)
	je	.LBB3_87
.LBB3_86:
	movq	out(%rip), %rdi
	callq	fileno
	movl	instat+28(%rip), %esi
	movl	instat+32(%rip), %edx
	movl	%eax, %edi
	callq	fchown
.LBB3_87:                               # %update_own.exit
	movq	out(%rip), %rdi
	callq	fclose
	movq	outname(%rip), %rdi
	testl	%eax, %eax
	js	.LBB3_88
# BB#90:
	cmpq	%r14, %rdi
	je	.LBB3_92
# BB#91:
	callq	free
.LBB3_92:
	movq	$0, outname(%rip)
.LBB3_93:
	movq	$0, out(%rip)
	movq	in(%rip), %rdi
	cmpq	stdin(%rip), %rdi
	je	.LBB3_100
# BB#94:
	callq	fclose
	movq	$0, in(%rip)
	movl	f_precious(%rip), %eax
	movq	inname(%rip), %rdi
	orl	f_cat(%rip), %eax
	je	.LBB3_95
# BB#97:
	cmpq	%r14, %rdi
	je	.LBB3_99
# BB#98:
	callq	free
.LBB3_99:
	movq	$0, inname(%rip)
.LBB3_100:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_95:
	callq	unlink
	testl	%eax, %eax
	jns	.LBB3_10
# BB#96:
	movq	inname(%rip), %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rcx
	movl	$.L.str.39, %esi
	jmp	.LBB3_89
.LBB3_73:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	cmpl	$0, f_decode(%rip)
	movl	$.L.str.34, %eax
	movl	$.L.str.35, %ecx
	cmovneq	%rax, %rcx
	movl	$.L.str.36, %eax
	movl	$.L.str.37, %r8d
	cmovneq	%rax, %r8
	movl	$outname, %eax
	movl	$inname, %esi
	cmovneq	%rax, %rsi
	movq	(%rsi), %rax
	testq	%rax, %rax
	cmovneq	%rax, %r8
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
.LBB3_9:                                # %open_input.exit.thread
	callq	fprintf
	jmp	.LBB3_10
.LBB3_28:
	movq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rcx
	movl	$.L.str.44, %esi
.LBB3_89:                               # %open_input.exit.thread
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB3_10
.LBB3_62:
	movq	stderr(%rip), %rdi
	callq	fileno
	movl	%eax, %edi
	callq	isatty
	testl	%eax, %eax
	je	.LBB3_10
# BB#63:
	movq	stderr(%rip), %rdi
	movl	$.L.str.54, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%rbx, %rcx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	cmpl	$-1, %eax
	jne	.LBB3_65
	jmp	.LBB3_67
.LBB3_66:                               #   in Loop: Header=BB3_65 Depth=1
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_67
.LBB3_65:                               # =>This Inner Loop Header: Depth=1
	cmpl	$10, %eax
	jne	.LBB3_66
.LBB3_67:
	cmpl	$121, %r15d
	je	.LBB3_69
# BB#68:
	movq	stderr(%rip), %rcx
	movl	$.L.str.55, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB3_10
.LBB3_30:
	movq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.47, %esi
	jmp	.LBB3_33
.LBB3_32:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.48, %esi
	jmp	.LBB3_33
.LBB3_36:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	cmpq	$3, %rax
	leaq	-1(%rax), %r8
	movl	$.L.str.50, %r9d
	adcq	$0, %r9
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	jmp	.LBB3_10
.LBB3_77:
	movq	outname(%rip), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.36, %ebx
	cmoveq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rax
	testq	%rax, %rax
	cmovneq	%rax, %rbx
	movl	$.L.str.38, %esi
.LBB3_33:                               # %open_input.exit.thread
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	jmp	.LBB3_10
.LBB3_47:
	movq	%r15, %rdi
	callq	free
	movq	-8(%rbx), %rbp
	jmp	.LBB3_51
.LBB3_15:
	movq	outname(%rip), %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rcx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB3_16
.LBB3_88:
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rcx
	movl	$.L.str.38, %esi
	jmp	.LBB3_89
.LBB3_58:                               # %.thread.i
	movq	$0, out(%rip)
.LBB3_59:
	movq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.52, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	testl	%ebp, %ebp
	js	.LBB3_10
# BB#60:
	movl	%ebp, %edi
	callq	close
	jmp	.LBB3_10
.LBB3_84:
	movq	outname(%rip), %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rcx
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpq	$0, instat+16(%rip)
	jne	.LBB3_86
	jmp	.LBB3_87
.LBB3_8:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.42, %esi
	movl	$.L.str.41, %r8d
	xorl	%eax, %eax
	movq	%r14, %rcx
	jmp	.LBB3_9
.LBB3_101:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	callq	onintr
.Lfunc_end3:
	.size	process, .Lfunc_end3-process
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_decode,@function
process_decode:                         # @process_decode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
	subq	$368, %rsp              # imm = 0x170
.Lcfi28:
	.cfi_def_cfa_offset 400
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	callq	gsm_create
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_13
# BB#1:
	movl	$2, %esi
	movl	$f_fast, %edx
	movq	%r14, %rdi
	callq	gsm_option
	movl	$1, %esi
	movl	$f_verbose, %edx
	movq	%r14, %rdi
	callq	gsm_option
	movq	%rsp, %rbx
	leaq	48(%rsp), %r15
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	in(%rip), %rcx
	movl	$1, %esi
	movl	$33, %edx
	movq	%rbx, %rdi
	callq	fread
	testl	%eax, %eax
	jle	.LBB4_11
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	$33, %eax
	jne	.LBB4_8
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	gsm_decode
	testl	%eax, %eax
	jne	.LBB4_9
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%r15, %rdi
	callq	*output(%rip)
	testl	%eax, %eax
	jns	.LBB4_2
# BB#6:
	movq	outname(%rip), %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rcx
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
.LBB4_7:
	callq	fprintf
	movq	%r14, %rdi
	callq	gsm_destroy
	jmp	.LBB4_14
.LBB4_8:
	cltq
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$33, %ecx
	subq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	$1, %rcx
	sete	%al
	leaq	.L.str.50(%rax), %r8
	movq	inname(%rip), %rax
	testq	%rax, %rax
	movl	$.L.str.37, %r9d
	cmovneq	%rax, %r9
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB4_10
.LBB4_9:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rax
	testq	%rax, %rax
	movl	$.L.str.37, %ecx
	cmovneq	%rax, %rcx
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB4_10:
	movq	%r14, %rdi
	callq	gsm_destroy
	callq	__errno_location
	movl	$0, (%rax)
.LBB4_14:
	movl	$-1, %eax
.LBB4_15:
	addq	$368, %rsp              # imm = 0x170
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_11:
	js	.LBB4_16
# BB#12:
	movq	%r14, %rdi
	callq	gsm_destroy
	xorl	%eax, %eax
	jmp	.LBB4_15
.LBB4_13:
	movq	progname(%rip), %rdi
	callq	perror
	jmp	.LBB4_14
.LBB4_16:
	movq	inname(%rip), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.37, %ebx
	cmoveq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rax
	testq	%rax, %rax
	cmovneq	%rax, %rbx
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	jmp	.LBB4_7
.Lfunc_end4:
	.size	process_decode, .Lfunc_end4-process_decode
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_encode,@function
process_encode:                         # @process_encode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
	subq	$368, %rsp              # imm = 0x170
.Lcfi35:
	.cfi_def_cfa_offset 400
.Lcfi36:
	.cfi_offset %rbx, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	callq	gsm_create
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_1
# BB#2:
	movl	$2, %esi
	movl	$f_fast, %edx
	movq	%r14, %rdi
	callq	gsm_option
	movl	$1, %esi
	movl	$f_verbose, %edx
	movq	%r14, %rdi
	callq	gsm_option
	leaq	48(%rsp), %r15
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	*input(%rip)
	testl	%eax, %eax
	jle	.LBB5_9
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	cmpl	$159, %eax
	ja	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=1
	cltq
	leaq	48(%rsp,%rax,2), %rdi
	addq	%rax, %rax
	movl	$320, %edx              # imm = 0x140
	subq	%rax, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB5_6:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	gsm_encode
	movq	out(%rip), %rcx
	movl	$33, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	fwrite
	cmpq	$1, %rax
	je	.LBB5_3
# BB#7:
	movq	outname(%rip), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.36, %ebx
	cmoveq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outname(%rip), %rax
	testq	%rax, %rax
	cmovneq	%rax, %rbx
	movl	$.L.str.58, %esi
.LBB5_8:
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	movq	%r14, %rdi
	callq	gsm_destroy
	movl	$-1, %eax
	jmp	.LBB5_12
.LBB5_9:
	js	.LBB5_10
# BB#11:
	movq	%r14, %rdi
	callq	gsm_destroy
	xorl	%eax, %eax
.LBB5_12:
	addq	$368, %rsp              # imm = 0x170
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_1:
	movq	progname(%rip), %rdi
	callq	perror
	movl	$-1, %eax
	jmp	.LBB5_12
.LBB5_10:
	movq	inname(%rip), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.37, %ebx
	cmoveq	%rbx, %rdi
	callq	perror
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rax
	testq	%rax, %rax
	cmovneq	%rax, %rbx
	movl	$.L.str.59, %esi
	jmp	.LBB5_8
.Lfunc_end5:
	.size	process_encode, .Lfunc_end5-process_encode
	.cfi_endproc

	.p2align	4, 0x90
	.type	codename,@function
codename:                               # @codename
	.cfi_startproc
# BB#0:
	movl	$.L.str.41, %esi
	movl	$.L.str.45, %edx
	jmp	normalname              # TAILCALL
.Lfunc_end6:
	.size	codename, .Lfunc_end6-codename
	.cfi_endproc

	.p2align	4, 0x90
	.type	normalname,@function
normalname:                             # @normalname
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r13, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB7_1
# BB#2:
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	%r12, %rbx
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax,%rbx), %r12
	movq	%r12, %rdi
	callq	malloc
	testq	%rax, %rax
	je	.LBB7_15
# BB#3:                                 # %emalloc.exit
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB7_8
# BB#4:                                 # %emalloc.exit
	subq	%rax, %rbx
	jbe	.LBB7_8
# BB#5:
	leaq	(%r12,%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	memcmp
	addq	%r12, %rbx
	je	.LBB7_8
# BB#6:
	testl	%eax, %eax
	jne	.LBB7_8
# BB#7:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	jmp	.LBB7_14
.LBB7_8:                                # %suffix.exit.thread
	cmpb	$0, (%r14)
	je	.LBB7_14
# BB#9:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB7_13
# BB#10:
	subq	%rax, %rbx
	jbe	.LBB7_13
# BB#11:
	leaq	(%r12,%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	memcmp
	movq	%r12, %rcx
	addq	%rbx, %rcx
	je	.LBB7_13
# BB#12:
	testl	%eax, %eax
	je	.LBB7_14
.LBB7_13:                               # %suffix.exit24.thread
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	strcat
	jmp	.LBB7_14
.LBB7_1:
	xorl	%r12d, %r12d
.LBB7_14:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB7_15:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r12, %rcx
	callq	fprintf
	callq	onintr
.Lfunc_end7:
	.size	normalname, .Lfunc_end7-normalname
	.cfi_endproc

	.p2align	4, 0x90
	.type	plainname,@function
plainname:                              # @plainname
	.cfi_startproc
# BB#0:
	movl	$.L.str.45, %esi
	movl	$.L.str.41, %edx
	jmp	normalname              # TAILCALL
.Lfunc_end8:
	.size	plainname, .Lfunc_end8-plainname
	.cfi_endproc

	.type	f_decode,@object        # @f_decode
	.bss
	.globl	f_decode
	.p2align	2
f_decode:
	.long	0                       # 0x0
	.size	f_decode, 4

	.type	f_cat,@object           # @f_cat
	.globl	f_cat
	.p2align	2
f_cat:
	.long	0                       # 0x0
	.size	f_cat, 4

	.type	f_force,@object         # @f_force
	.globl	f_force
	.p2align	2
f_force:
	.long	0                       # 0x0
	.size	f_force, 4

	.type	f_precious,@object      # @f_precious
	.globl	f_precious
	.p2align	2
f_precious:
	.long	0                       # 0x0
	.size	f_precious, 4

	.type	f_fast,@object          # @f_fast
	.globl	f_fast
	.p2align	2
f_fast:
	.long	0                       # 0x0
	.size	f_fast, 4

	.type	f_verbose,@object       # @f_verbose
	.globl	f_verbose
	.p2align	2
f_verbose:
	.long	0                       # 0x0
	.size	f_verbose, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"audio"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"8 kHz, 8 bit u-law encoding with Sun audio header"
	.size	.L.str.1, 50

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	".au"
	.size	.L.str.2, 4

	.type	f_audio,@object         # @f_audio
	.data
	.globl	f_audio
	.p2align	3
f_audio:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	audio_init_input
	.quad	audio_init_output
	.quad	ulaw_input
	.quad	ulaw_output
	.size	f_audio, 56

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"u-law"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"plain 8 kHz, 8 bit u-law encoding"
	.size	.L.str.4, 34

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	".u"
	.size	.L.str.5, 3

	.type	f_ulaw,@object          # @f_ulaw
	.data
	.globl	f_ulaw
	.p2align	3
f_ulaw:
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	generic_init
	.quad	generic_init
	.quad	ulaw_input
	.quad	ulaw_output
	.size	f_ulaw, 56

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"A-law"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"8 kHz, 8 bit A-law encoding"
	.size	.L.str.7, 28

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	".A"
	.size	.L.str.8, 3

	.type	f_alaw,@object          # @f_alaw
	.data
	.globl	f_alaw
	.p2align	3
f_alaw:
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	generic_init
	.quad	generic_init
	.quad	alaw_input
	.quad	alaw_output
	.size	f_alaw, 56

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"linear"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"16 bit (13 significant) signed 8 kHz signal"
	.size	.L.str.10, 44

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	".l"
	.size	.L.str.11, 3

	.type	f_linear,@object        # @f_linear
	.data
	.globl	f_linear
	.p2align	3
f_linear:
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	generic_init
	.quad	generic_init
	.quad	linear_input
	.quad	linear_output
	.size	f_linear, 56

	.type	alldescs,@object        # @alldescs
	.globl	alldescs
	.p2align	4
alldescs:
	.quad	f_audio
	.quad	f_alaw
	.quad	f_ulaw
	.quad	f_linear
	.quad	0
	.size	alldescs, 40

	.type	f_format,@object        # @f_format
	.bss
	.globl	f_format
	.p2align	3
f_format:
	.quad	0
	.size	f_format, 8

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.12:
	.asciz	"fcdpvhuaslVF"
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Usage: %s [-fcpdhvuaslF] [files...] (-h for help)\n"
	.size	.L.str.13, 51

	.type	progname,@object        # @progname
	.comm	progname,8,8
	.type	instat,@object          # @instat
	.comm	instat,144,8
	.type	in,@object              # @in
	.comm	in,8,8
	.type	out,@object             # @out
	.comm	out,8,8
	.type	inname,@object          # @inname
	.comm	inname,8,8
	.type	outname,@object         # @outname
	.comm	outname,8,8
	.type	output,@object          # @output
	.comm	output,8,8
	.type	input,@object           # @input
	.comm	input,8,8
	.type	init_input,@object      # @init_input
	.comm	init_input,8,8
	.type	init_output,@object     # @init_output
	.comm	init_output,8,8
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"toast"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"un"
	.size	.L.str.15, 3

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"cat"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s: only one of -[uals] is possible (%s -h for help)\n"
	.size	.L.str.17, 54

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s 1.0, version %s\n"
	.size	.L.str.18, 20

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"$Id: toast.c 42694 2007-10-06 10:55:20Z asl $"
	.size	.L.str.19, 46

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Usage: %s [-fcpdhvaulsF] [files...]\n"
	.size	.L.str.20, 37

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s: error %s %s\n"
	.size	.L.str.33, 17

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"writing header to"
	.size	.L.str.34, 18

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"reading header from"
	.size	.L.str.35, 20

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"stdout"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"stdin"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s: error writing \"%s\"\n"
	.size	.L.str.38, 24

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%s: source \"%s\" not deleted.\n"
	.size	.L.str.39, 30

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%s: could not unlink \"%s\"\n"
	.size	.L.str.40, 27

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	".gsm"
	.size	.L.str.41, 5

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"%s: %s already has \"%s\" suffix -- unchanged.\n"
	.size	.L.str.42, 46

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"rb"
	.size	.L.str.43, 3

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"%s: cannot open \"%s\" for reading\n"
	.size	.L.str.44, 34

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.zero	1
	.size	.L.str.45, 1

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%s: failed to malloc %d bytes -- abort\n"
	.size	.L.str.46, 40

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"%s: cannot stat \"%s\"\n"
	.size	.L.str.47, 22

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"%s: \"%s\" is not a regular file -- unchanged.\n"
	.size	.L.str.48, 46

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"%s: \"%s\" has %s other link%s -- unchanged.\n"
	.size	.L.str.49, 44

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"s"
	.size	.L.str.50, 2

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"wb"
	.size	.L.str.51, 3

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%s: can't open \"%s\" for writing\n"
	.size	.L.str.52, 33

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"%s already exists; do you wish to overwrite %s (y or n)? "
	.size	.L.str.54, 58

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"\tnot overwritten\n"
	.size	.L.str.55, 18

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"%s: incomplete frame (%d byte%s missing) from %s\n"
	.size	.L.str.56, 50

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%s: bad frame in %s\n"
	.size	.L.str.57, 21

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%s: error writing to %s\n"
	.size	.L.str.58, 25

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%s: error reading from %s\n"
	.size	.L.str.59, 27

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%s: could not change file mode of \"%s\"\n"
	.size	.L.str.60, 40

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" -f  force     Replace existing files without asking"
	.size	.Lstr, 53

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	" -c  cat       Write to stdout, do not remove source files"
	.size	.Lstr.1, 59

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	" -d  decode    Decode data (default is encode)"
	.size	.Lstr.2, 47

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	" -p  precious  Do not delete the source"
	.size	.Lstr.3, 40

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	" -u  u-law     Force 8 kHz/8 bit u-law in/output format"
	.size	.Lstr.4, 56

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	" -s  sun .au   Force Sun .au u-law in/output format"
	.size	.Lstr.5, 52

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	" -a  A-law     Force 8 kHz/8 bit A-law in/output format"
	.size	.Lstr.6, 56

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	" -l  linear    Force 16 bit linear in/output format"
	.size	.Lstr.7, 52

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	" -F  fast      Sacrifice conformance to performance"
	.size	.Lstr.8, 52

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	" -v  version   Show version information"
	.size	.Lstr.9, 40

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	" -h  help      Print this text"
	.size	.Lstr.10, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
