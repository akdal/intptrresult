	.text
	.file	"img_chroma.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
.LCPI0_1:
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.text
	.globl	getSubImagesChroma
	.p2align	4, 0x90
	.type	getSubImagesChroma,@function
getSubImagesChroma:                     # @getSubImagesChroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movslq	6400(%rdi), %rcx
	movq	%rcx, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	decq	%rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movl	6404(%rdi), %ecx
	movq	img(%rip), %rax
	movl	15536(%rax), %edx
	movl	$1, %r8d
	movabsq	$8589934584, %rdi       # imm = 0x1FFFFFFF8
	cmpl	$1, %edx
	je	.LBB0_1
# BB#2:
	cmpl	$2, %edx
	jne	.LBB0_4
# BB#3:
	movl	$2, -96(%rsp)           # 4-byte Folded Spill
	movl	$4, %edx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	$8, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$1, -92(%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_5
.LBB0_1:
	movl	$8, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$8, %edx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	$1, -92(%rsp)           # 4-byte Folded Spill
	movl	$1, -96(%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_5
.LBB0_4:
	movl	$2, -92(%rsp)           # 4-byte Folded Spill
	movl	$4, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$4, %edx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	$2, -96(%rsp)           # 4-byte Folded Spill
.LBB0_5:
	leal	-1(%rcx), %edx
	movl	%edx, -28(%rsp)         # 4-byte Spill
	movl	img_pad_size_uv_x(%rip), %ebp
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbp,2), %r11d
	movl	img_pad_size_uv_y(%rip), %esi
	leal	(%rcx,%rsi,2), %ecx
	subl	%esi, %ecx
	movl	%ecx, -88(%rsp)         # 4-byte Spill
	negl	%esi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	subl	%ebp, %r11d
	movl	%r11d, -84(%rsp)        # 4-byte Spill
	testl	%edx, %edx
	cmovgl	%edx, %r8d
	movslq	%ebp, %rax
	leal	-1(%rbp,%r8), %ecx
	movslq	%ecx, %r9
	movq	-128(%rsp), %r10        # 8-byte Reload
	movl	%r10d, %ebx
	cmpl	%r11d, %edx
	movl	%r11d, %ecx
	cmovgel	%edx, %ecx
	subl	%edx, %ecx
	leaq	1(%r9,%rcx), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	leaq	(%r8,%rcx), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	incq	%rcx
	movl	%r10d, %edx
	andl	$7, %edx
	movq	%rbx, %rsi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	subq	%rdx, %rsi
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	andq	%rcx, %rdi
	movl	%eax, %ecx
	andl	$7, %ecx
	movq	%rbp, %rdx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	subq	%rcx, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	-1(%r8), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	leaq	-1(%r8,%rdi), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	pxor	%xmm14, %xmm14
	movdqa	.LCPI0_0(%rip), %xmm11  # xmm11 = [32,32,32,32]
	movdqa	.LCPI0_1(%rip), %xmm7   # xmm7 = [65535,0,65535,0,65535,0,65535,0]
	leaq	(%rax,%rbx), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	leaq	-1(%rbp), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%r9, -8(%rsp)           # 8-byte Spill
	leaq	(%r9,%rdi), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	leal	(%rdi,%r10), %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader170
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_11 Depth 4
                                        #           Child Loop BB0_58 Depth 5
                                        #           Child Loop BB0_15 Depth 5
                                        #           Child Loop BB0_68 Depth 5
                                        #           Child Loop BB0_63 Depth 5
                                        #         Child Loop BB0_17 Depth 4
                                        #           Child Loop BB0_28 Depth 5
                                        #           Child Loop BB0_23 Depth 5
                                        #           Child Loop BB0_43 Depth 5
                                        #           Child Loop BB0_35 Depth 5
                                        #           Child Loop BB0_52 Depth 5
                                        #           Child Loop BB0_47 Depth 5
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_11 Depth 4
                                        #           Child Loop BB0_58 Depth 5
                                        #           Child Loop BB0_15 Depth 5
                                        #           Child Loop BB0_68 Depth 5
                                        #           Child Loop BB0_63 Depth 5
                                        #         Child Loop BB0_17 Depth 4
                                        #           Child Loop BB0_28 Depth 5
                                        #           Child Loop BB0_23 Depth 5
                                        #           Child Loop BB0_43 Depth 5
                                        #           Child Loop BB0_35 Depth 5
                                        #           Child Loop BB0_52 Depth 5
                                        #           Child Loop BB0_47 Depth 5
	movl	$8, %r12d
	subl	%edx, %r12d
	leal	(,%r12,8), %r11d
	leal	(,%rdx,8), %r10d
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm10       # xmm10 = xmm0[0,0,0,0]
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm13       # xmm13 = xmm0[0,0,0,0]
	movd	%r11d, %xmm0
	pshufd	$0, %xmm0, %xmm5        # xmm5 = xmm0[0,0,0,0]
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm6        # xmm6 = xmm0[0,0,0,0]
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movdqa	%xmm5, 208(%rsp)        # 16-byte Spill
	movdqa	%xmm6, 192(%rsp)        # 16-byte Spill
	movq	%r12, -48(%rsp)         # 8-byte Spill
	movl	%r11d, 64(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_11 Depth 4
                                        #           Child Loop BB0_58 Depth 5
                                        #           Child Loop BB0_15 Depth 5
                                        #           Child Loop BB0_68 Depth 5
                                        #           Child Loop BB0_63 Depth 5
                                        #         Child Loop BB0_17 Depth 4
                                        #           Child Loop BB0_28 Depth 5
                                        #           Child Loop BB0_23 Depth 5
                                        #           Child Loop BB0_43 Depth 5
                                        #           Child Loop BB0_35 Depth 5
                                        #           Child Loop BB0_52 Depth 5
                                        #           Child Loop BB0_47 Depth 5
	movl	-88(%rsp), %eax         # 4-byte Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jle	.LBB0_71
# BB#9:                                 # %.lr.ph185
                                        #   in Loop: Header=BB0_8 Depth=3
	movl	$8, %r8d
	subl	%r14d, %r8d
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	6464(%rcx), %rax
	movq	6472(%rcx), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	(%rcx,%rdx,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movd	%r8d, %xmm0
	pshufd	$0, %xmm0, %xmm15       # xmm15 = xmm0[0,0,0,0]
	movd	%r14d, %xmm0
	jle	.LBB0_10
# BB#16:                                # %.lr.ph185.split.us.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	pshufd	$0, %xmm0, %xmm9        # xmm9 = xmm0[0,0,0,0]
	xorl	%esi, %esi
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edi
	movl	%r14d, -116(%rsp)       # 4-byte Spill
	movl	%r8d, -120(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph185.split.us
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_28 Depth 5
                                        #           Child Loop BB0_23 Depth 5
                                        #           Child Loop BB0_43 Depth 5
                                        #           Child Loop BB0_35 Depth 5
                                        #           Child Loop BB0_52 Depth 5
                                        #           Child Loop BB0_47 Depth 5
	testl	%edi, %edi
	movl	$0, %eax
	cmovnsl	%edi, %eax
	movl	-28(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	incl	%edi
	movl	$0, %ecx
	cmovnsl	%edi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rsi,8), %rbp
	cltq
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rax,8), %r13
	movslq	%ecx, %rax
	movq	(%rdx,%rax,8), %r15
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpl	$7, %eax
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	jbe	.LBB0_18
# BB#24:                                # %min.iters.checked300
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB0_18
# BB#25:                                # %vector.memcheck319
                                        #   in Loop: Header=BB0_17 Depth=4
	leaq	(%rbp,%rax,2), %rsi
	cmpq	%r13, %rbp
	sbbb	%cl, %cl
	cmpq	%rsi, %r13
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%r15, %rbp
	sbbb	%cl, %cl
	cmpq	%rsi, %r15
	sbbb	%sil, %sil
	testb	$1, %dl
	jne	.LBB0_18
# BB#26:                                # %vector.memcheck319
                                        #   in Loop: Header=BB0_17 Depth=4
	andb	%sil, %cl
	andb	$1, %cl
	movl	$0, %ecx
	jne	.LBB0_19
# BB#27:                                # %vector.ph320
                                        #   in Loop: Header=BB0_17 Depth=4
	movzwl	(%r13), %ecx
	movd	%ecx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	pand	%xmm7, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm1      # xmm1 = xmm3[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movzwl	(%r15), %ecx
	movd	%ecx, %xmm1
	pshuflw	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	pand	%xmm7, %xmm1
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm6, %xmm4      # xmm4 = xmm6[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm3      # xmm3 = xmm4[0,2,2,3]
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	paddd	%xmm11, %xmm0
	paddd	%xmm1, %xmm0
	psrld	$6, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm0, %xmm0
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_28:                               # %vector.body296
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	%xmm0, (%rdx)
	addq	$16, %rdx
	addq	$-8, %rcx
	jne	.LBB0_28
# BB#29:                                # %middle.block297
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jne	.LBB0_19
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_17 Depth=4
	xorl	%ecx, %ecx
.LBB0_19:                               # %scalar.ph298.preheader
                                        #   in Loop: Header=BB0_17 Depth=4
	movl	%eax, %edx
	subl	%ecx, %edx
	testb	$1, %dl
	movq	%rcx, %rdx
	je	.LBB0_21
# BB#20:                                # %scalar.ph298.prol
                                        #   in Loop: Header=BB0_17 Depth=4
	movzwl	(%r13), %edx
	imull	%r11d, %edx
	movzwl	(%r15), %esi
	imull	%r10d, %esi
	leal	32(%rdx,%rsi), %edx
	shrl	$6, %edx
	movw	%dx, (%rbp,%rcx,2)
	leaq	1(%rcx), %rdx
.LBB0_21:                               # %scalar.ph298.prol.loopexit
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpq	%rcx, 184(%rsp)         # 8-byte Folded Reload
	je	.LBB0_30
# BB#22:                                # %scalar.ph298.preheader.new
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	%rax, %rcx
	subq	%rdx, %rcx
	leaq	2(%rbp,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB0_23:                               # %scalar.ph298
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%r13), %eax
	imull	%r11d, %eax
	movzwl	(%r15), %esi
	imull	%r10d, %esi
	leal	32(%rax,%rsi), %eax
	shrl	$6, %eax
	movw	%ax, -2(%rdx)
	movzwl	(%r13), %eax
	imull	%r11d, %eax
	movzwl	(%r15), %esi
	imull	%r10d, %esi
	leal	32(%rax,%rsi), %eax
	shrl	$6, %eax
	movw	%ax, (%rdx)
	addq	$4, %rdx
	addq	$-2, %rcx
	jne	.LBB0_23
.LBB0_30:                               # %..preheader169_crit_edge.us
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	movl	%edi, -64(%rsp)         # 4-byte Spill
	cmpl	$2, -40(%rsp)           # 4-byte Folded Reload
	jl	.LBB0_36
# BB#31:                                # %.lr.ph175.us.preheader
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpl	$7, -128(%rsp)          # 4-byte Folded Reload
	ja	.LBB0_38
# BB#32:                                #   in Loop: Header=BB0_17 Depth=4
	xorl	%edx, %edx
	movq	88(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_38:                               # %min.iters.checked249
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpq	$0, -80(%rsp)           # 8-byte Folded Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	je	.LBB0_39
# BB#40:                                # %vector.memcheck270
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-112(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,2), %rcx
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rdx,%rax,2), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	leaq	(%r13,%rsi,2), %rdx
	leaq	(%r15,%rsi,2), %rsi
	cmpq	%rdx, %rcx
	sbbb	%dl, %dl
	cmpq	%rax, %r13
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rsi, %rcx
	sbbb	%dl, %dl
	cmpq	%rax, %r15
	sbbb	%sil, %sil
	testb	$1, %bl
	jne	.LBB0_39
# BB#41:                                # %vector.memcheck270
                                        #   in Loop: Header=BB0_17 Depth=4
	andb	%sil, %dl
	andb	$1, %dl
	movl	$0, %edx
	movq	%rdi, %rbp
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	-48(%rsp), %r12         # 8-byte Reload
	movl	-116(%rsp), %r14d       # 4-byte Reload
	movl	-120(%rsp), %r8d        # 4-byte Reload
	jne	.LBB0_34
# BB#42:                                # %vector.ph271
                                        #   in Loop: Header=BB0_17 Depth=4
	leaq	2(%r13), %rdi
	leaq	2(%r15), %rsi
	movq	-80(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_43:                               # %vector.body245
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	-2(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm14, %xmm3   # xmm3 = xmm3[0],xmm14[0],xmm3[1],xmm14[1],xmm3[2],xmm14[2],xmm3[3],xmm14[3]
	punpckhwd	%xmm14, %xmm0   # xmm0 = xmm0[4],xmm14[4],xmm0[5],xmm14[5],xmm0[6],xmm14[6],xmm0[7],xmm14[7]
	pshufd	$245, %xmm0, %xmm6      # xmm6 = xmm0[1,1,3,3]
	pmuludq	%xmm15, %xmm0
	movdqa	%xmm11, %xmm12
	pshufd	$232, %xmm0, %xmm11     # xmm11 = xmm0[0,2,2,3]
	pshufd	$245, %xmm15, %xmm4     # xmm4 = xmm15[1,1,3,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm0, %xmm11   # xmm11 = xmm11[0],xmm0[0],xmm11[1],xmm0[1]
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm15, %xmm3
	pshufd	$232, %xmm3, %xmm6      # xmm6 = xmm3[0,2,2,3]
	pmuludq	%xmm4, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	movdqa	%xmm1, %xmm7
	punpcklwd	%xmm14, %xmm7   # xmm7 = xmm7[0],xmm14[0],xmm7[1],xmm14[1],xmm7[2],xmm14[2],xmm7[3],xmm14[3]
	punpckhwd	%xmm14, %xmm1   # xmm1 = xmm1[4],xmm14[4],xmm1[5],xmm14[5],xmm1[6],xmm14[6],xmm1[7],xmm14[7]
	pshufd	$245, %xmm1, %xmm8      # xmm8 = xmm1[1,1,3,3]
	pmuludq	%xmm9, %xmm1
	pshufd	$232, %xmm1, %xmm3      # xmm3 = xmm1[0,2,2,3]
	pshufd	$245, %xmm9, %xmm0      # xmm0 = xmm9[1,1,3,3]
	pmuludq	%xmm0, %xmm8
	pshufd	$232, %xmm8, %xmm1      # xmm1 = xmm8[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	pshufd	$245, %xmm7, %xmm2      # xmm2 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm8      # xmm8 = xmm7[0,2,2,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm8    # xmm8 = xmm8[0],xmm2[0],xmm8[1],xmm2[1]
	movdqu	-2(%rsi), %xmm2
	movdqu	(%rsi), %xmm7
	movdqa	%xmm2, %xmm1
	punpcklwd	%xmm14, %xmm1   # xmm1 = xmm1[0],xmm14[0],xmm1[1],xmm14[1],xmm1[2],xmm14[2],xmm1[3],xmm14[3]
	punpckhwd	%xmm14, %xmm2   # xmm2 = xmm2[4],xmm14[4],xmm2[5],xmm14[5],xmm2[6],xmm14[6],xmm2[7],xmm14[7]
	movdqa	%xmm13, %xmm14
	movdqa	%xmm10, %xmm13
	pshufd	$245, %xmm2, %xmm10     # xmm10 = xmm2[1,1,3,3]
	pmuludq	%xmm15, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm4, %xmm10
	pshufd	$232, %xmm10, %xmm5     # xmm5 = xmm10[0,2,2,3]
	movdqa	%xmm13, %xmm10
	movdqa	%xmm14, %xmm13
	pxor	%xmm14, %xmm14
	punpckldq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm15, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	movdqa	%xmm7, %xmm4
	punpcklwd	%xmm14, %xmm4   # xmm4 = xmm4[0],xmm14[0],xmm4[1],xmm14[1],xmm4[2],xmm14[2],xmm4[3],xmm14[3]
	punpckhwd	%xmm14, %xmm7   # xmm7 = xmm7[4],xmm14[4],xmm7[5],xmm14[5],xmm7[6],xmm14[6],xmm7[7],xmm14[7]
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm0, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm0, %xmm5
	pshufd	$232, %xmm5, %xmm0      # xmm0 = xmm5[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddd	%xmm1, %xmm4
	paddd	%xmm2, %xmm7
	pshufd	$245, %xmm7, %xmm1      # xmm1 = xmm7[1,1,3,3]
	pmuludq	%xmm10, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	pshufd	$245, %xmm10, %xmm2     # xmm2 = xmm10[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pshufd	$245, %xmm4, %xmm1      # xmm1 = xmm4[1,1,3,3]
	pmuludq	%xmm10, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	paddd	%xmm6, %xmm8
	paddd	%xmm11, %xmm3
	movdqa	%xmm12, %xmm11
	pshufd	$245, %xmm3, %xmm1      # xmm1 = xmm3[1,1,3,3]
	pmuludq	%xmm13, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm13, %xmm2     # xmm2 = xmm13[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	pshufd	$245, %xmm8, %xmm1      # xmm1 = xmm8[1,1,3,3]
	pmuludq	%xmm13, %xmm8
	pshufd	$232, %xmm8, %xmm5      # xmm5 = xmm8[0,2,2,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	paddd	%xmm4, %xmm5
	paddd	%xmm0, %xmm3
	paddd	%xmm11, %xmm3
	paddd	%xmm11, %xmm5
	psrld	$6, %xmm5
	psrld	$6, %xmm3
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	packssdw	%xmm3, %xmm5
	movdqu	%xmm5, (%rcx)
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$16, %rcx
	addq	$-8, %r9
	jne	.LBB0_43
# BB#44:                                # %middle.block246
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movdqa	.LCPI0_1(%rip), %xmm12  # xmm12 = [65535,0,65535,0,65535,0,65535,0]
	movdqa	%xmm12, %xmm7
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	-48(%rsp), %r12         # 8-byte Reload
	movdqa	208(%rsp), %xmm5        # 16-byte Reload
	movdqa	192(%rsp), %xmm6        # 16-byte Reload
	movl	-116(%rsp), %r14d       # 4-byte Reload
	movl	-120(%rsp), %r8d        # 4-byte Reload
	jne	.LBB0_34
	jmp	.LBB0_36
.LBB0_39:                               #   in Loop: Header=BB0_17 Depth=4
	xorl	%edx, %edx
	movq	%rdi, %rbp
.LBB0_33:                               # %.lr.ph175.us.preheader425
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	-48(%rsp), %r12         # 8-byte Reload
	movl	-116(%rsp), %r14d       # 4-byte Reload
	movl	-120(%rsp), %r8d        # 4-byte Reload
.LBB0_34:                               # %.lr.ph175.us.preheader425
                                        #   in Loop: Header=BB0_17 Depth=4
	leaq	2(%r13,%rdx,2), %rcx
	leaq	2(%r15,%rdx,2), %rsi
	movq	56(%rsp), %r9           # 8-byte Reload
	subq	%rdx, %r9
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rbp,2), %rdx
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph175.us
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	-2(%rcx), %ebp
	imull	%r8d, %ebp
	movzwl	(%rcx), %ebx
	imull	%r14d, %ebx
	movzwl	-2(%rsi), %eax
	imull	%r8d, %eax
	movzwl	(%rsi), %edi
	imull	%r14d, %edi
	addl	%eax, %edi
	imull	%r11d, %edi
	addl	%ebp, %ebx
	imull	%r12d, %ebx
	leal	32(%rbx,%rdi), %eax
	shrl	$6, %eax
	movw	%ax, (%rdx)
	addq	$2, %rcx
	addq	$2, %rsi
	addq	$2, %rdx
	decq	%r9
	jne	.LBB0_35
.LBB0_36:                               # %.preheader.us
                                        #   in Loop: Header=BB0_17 Depth=4
	movl	-84(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, -40(%rsp)         # 4-byte Folded Reload
	movl	64(%rsp), %r11d         # 4-byte Reload
	jg	.LBB0_37
# BB#45:                                # %.lr.ph180.us
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-128(%rsp), %rdx        # 8-byte Reload
	leaq	(%r13,%rdx,2), %rcx
	leaq	(%r15,%rdx,2), %rax
	cmpq	$7, -16(%rsp)           # 8-byte Folded Reload
	movq	-8(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jbe	.LBB0_46
# BB#48:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_17 Depth=4
	cmpq	$0, -72(%rsp)           # 8-byte Folded Reload
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movq	-128(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	je	.LBB0_46
# BB#49:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-8(%rsp), %rbp          # 8-byte Reload
	movq	-112(%rsp), %rdi        # 8-byte Reload
	leaq	(%rdi,%rbp,2), %rsi
	movq	176(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdi,%rdx,2), %rdi
	cmpq	%rcx, %rsi
	sbbb	%bl, %bl
	cmpq	%rdi, %rcx
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rax, %rsi
	sbbb	%bl, %bl
	cmpq	%rdi, %rax
	sbbb	%r9b, %r9b
	testb	$1, %dl
	movq	%rbp, %rdi
	movq	-128(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jne	.LBB0_46
# BB#50:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_17 Depth=4
	andb	%r9b, %bl
	andb	$1, %bl
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movq	-128(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jne	.LBB0_46
# BB#51:                                # %vector.ph
                                        #   in Loop: Header=BB0_17 Depth=4
	movzwl	(%rcx), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	pand	%xmm7, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm0
	pshufd	$232, %xmm0, %xmm2      # xmm2 = xmm0[0,2,2,3]
	pshufd	$245, %xmm5, %xmm0      # xmm0 = xmm5[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movzwl	(%rax), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	pand	%xmm7, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm6, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm6, %xmm3      # xmm3 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm1      # xmm1 = xmm3[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	paddd	%xmm2, %xmm0
	paddd	%xmm11, %xmm0
	psrld	$6, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm0, %xmm0
	movq	-72(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_52:                               # %vector.body
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	%xmm0, (%rsi)
	addq	$16, %rsi
	addq	$-8, %rdx
	jne	.LBB0_52
# BB#53:                                # %middle.block
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-16(%rsp), %rdx         # 8-byte Reload
	cmpq	-72(%rsp), %rdx         # 8-byte Folded Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	-32(%rsp), %edx         # 4-byte Reload
	je	.LBB0_37
	.p2align	4, 0x90
.LBB0_46:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-112(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdi,2), %rsi
	.p2align	4, 0x90
.LBB0_47:                               # %scalar.ph
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rcx), %edi
	imull	%r11d, %edi
	movzwl	(%rax), %ebp
	imull	%r10d, %ebp
	leal	32(%rdi,%rbp), %edi
	shrl	$6, %edi
	movw	%di, (%rsi)
	incl	%edx
	addq	$2, %rsi
	cmpl	-84(%rsp), %edx         # 4-byte Folded Reload
	jl	.LBB0_47
.LBB0_37:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_17 Depth=4
	movq	-56(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movl	-64(%rsp), %edi         # 4-byte Reload
	cmpl	-88(%rsp), %edi         # 4-byte Folded Reload
	jl	.LBB0_17
	jmp	.LBB0_71
.LBB0_10:                               # %.lr.ph185.split.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	xorl	%r15d, %r15d
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph185.split
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_58 Depth 5
                                        #           Child Loop BB0_15 Depth 5
                                        #           Child Loop BB0_68 Depth 5
                                        #           Child Loop BB0_63 Depth 5
	testl	%esi, %esi
	movl	$0, %eax
	cmovnsl	%esi, %eax
	movl	-28(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	incl	%esi
	movl	$0, %ecx
	movl	%esi, 68(%rsp)          # 4-byte Spill
	cmovnsl	%esi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%r15,8), %rdx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	cltq
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movslq	%ecx, %rax
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	cmpl	$2, -40(%rsp)           # 4-byte Folded Reload
	movq	-104(%rsp), %r13        # 8-byte Reload
	jl	.LBB0_60
# BB#12:                                # %.lr.ph175.preheader
                                        #   in Loop: Header=BB0_11 Depth=4
	cmpl	$7, -128(%rsp)          # 4-byte Folded Reload
	jbe	.LBB0_13
# BB#54:                                # %min.iters.checked385
                                        #   in Loop: Header=BB0_11 Depth=4
	cmpq	$0, -80(%rsp)           # 8-byte Folded Reload
	je	.LBB0_13
# BB#55:                                # %vector.memcheck404
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	-112(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rax,2), %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	-56(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rdx,2), %rax
	movq	-64(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdx,2), %rdx
	cmpq	%rax, %rsi
	sbbb	%al, %al
	cmpq	%rcx, %rdi
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rdx, %rsi
	sbbb	%al, %al
	cmpq	%rcx, %rbp
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB0_13
# BB#56:                                # %vector.memcheck404
                                        #   in Loop: Header=BB0_11 Depth=4
	andb	%cl, %al
	andb	$1, %al
	movl	$0, %edx
	jne	.LBB0_14
# BB#57:                                # %vector.ph405
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	2(%rax), %rbx
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	2(%rax), %rsi
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	-112(%rsp), %r9         # 8-byte Reload
	movdqa	%xmm10, %xmm12
	.p2align	4, 0x90
.LBB0_58:                               # %vector.body381
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	-2(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm14, %xmm2   # xmm2 = xmm2[0],xmm14[0],xmm2[1],xmm14[1],xmm2[2],xmm14[2],xmm2[3],xmm14[3]
	punpckhwd	%xmm14, %xmm0   # xmm0 = xmm0[4],xmm14[4],xmm0[5],xmm14[5],xmm0[6],xmm14[6],xmm0[7],xmm14[7]
	pshufd	$245, %xmm0, %xmm3      # xmm3 = xmm0[1,1,3,3]
	pmuludq	%xmm15, %xmm0
	pshufd	$232, %xmm0, %xmm9      # xmm9 = xmm0[0,2,2,3]
	pshufd	$245, %xmm15, %xmm4     # xmm4 = xmm15[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm0, %xmm9    # xmm9 = xmm9[0],xmm0[0],xmm9[1],xmm0[1]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm15, %xmm2
	pshufd	$232, %xmm2, %xmm10     # xmm10 = xmm2[0,2,2,3]
	pmuludq	%xmm4, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm10   # xmm10 = xmm10[0],xmm0[0],xmm10[1],xmm0[1]
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm14, %xmm2   # xmm2 = xmm2[0],xmm14[0],xmm2[1],xmm14[1],xmm2[2],xmm14[2],xmm2[3],xmm14[3]
	punpckhwd	%xmm14, %xmm1   # xmm1 = xmm1[4],xmm14[4],xmm1[5],xmm14[5],xmm1[6],xmm14[6],xmm1[7],xmm14[7]
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	pmuludq	%xmm8, %xmm1
	pshufd	$232, %xmm1, %xmm3      # xmm3 = xmm1[0,2,2,3]
	pshufd	$245, %xmm8, %xmm0      # xmm0 = xmm8[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm1      # xmm1 = xmm7[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	pshufd	$245, %xmm2, %xmm7      # xmm7 = xmm2[1,1,3,3]
	pmuludq	%xmm8, %xmm2
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm2      # xmm2 = xmm7[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqu	-2(%rsi), %xmm2
	movdqu	(%rsi), %xmm7
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm14, %xmm5   # xmm5 = xmm5[0],xmm14[0],xmm5[1],xmm14[1],xmm5[2],xmm14[2],xmm5[3],xmm14[3]
	punpckhwd	%xmm14, %xmm2   # xmm2 = xmm2[4],xmm14[4],xmm2[5],xmm14[5],xmm2[6],xmm14[6],xmm2[7],xmm14[7]
	pshufd	$245, %xmm2, %xmm6      # xmm6 = xmm2[1,1,3,3]
	pmuludq	%xmm15, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	pshufd	$245, %xmm5, %xmm6      # xmm6 = xmm5[1,1,3,3]
	pmuludq	%xmm15, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movdqa	%xmm7, %xmm4
	punpcklwd	%xmm14, %xmm4   # xmm4 = xmm4[0],xmm14[0],xmm4[1],xmm14[1],xmm4[2],xmm14[2],xmm4[3],xmm14[3]
	punpckhwd	%xmm14, %xmm7   # xmm7 = xmm7[4],xmm14[4],xmm7[5],xmm14[5],xmm7[6],xmm14[6],xmm7[7],xmm14[7]
	pshufd	$245, %xmm7, %xmm6      # xmm6 = xmm7[1,1,3,3]
	pmuludq	%xmm8, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm0, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	pshufd	$245, %xmm4, %xmm6      # xmm6 = xmm4[1,1,3,3]
	pmuludq	%xmm8, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm0, %xmm6
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddd	%xmm5, %xmm4
	paddd	%xmm2, %xmm7
	pshufd	$245, %xmm7, %xmm2      # xmm2 = xmm7[1,1,3,3]
	pmuludq	%xmm12, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	pshufd	$245, %xmm12, %xmm5     # xmm5 = xmm12[1,1,3,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	pshufd	$245, %xmm4, %xmm2      # xmm2 = xmm4[1,1,3,3]
	pmuludq	%xmm12, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	paddd	%xmm10, %xmm1
	paddd	%xmm9, %xmm3
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm13, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	pshufd	$245, %xmm13, %xmm3     # xmm3 = xmm13[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm13, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	paddd	%xmm4, %xmm1
	paddd	%xmm0, %xmm2
	paddd	%xmm11, %xmm2
	paddd	%xmm11, %xmm1
	psrld	$6, %xmm1
	psrld	$6, %xmm2
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm2, %xmm1
	movdqu	%xmm1, (%r9)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$16, %r9
	addq	$-8, %rdi
	jne	.LBB0_58
# BB#59:                                # %middle.block382
                                        #   in Loop: Header=BB0_11 Depth=4
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movdqa	%xmm12, %xmm10
	movdqa	208(%rsp), %xmm5        # 16-byte Reload
	movdqa	192(%rsp), %xmm6        # 16-byte Reload
	movdqa	.LCPI0_1(%rip), %xmm7   # xmm7 = [65535,0,65535,0,65535,0,65535,0]
	jne	.LBB0_14
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_11 Depth=4
	xorl	%edx, %edx
.LBB0_14:                               # %.lr.ph175.preheader426
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rsi
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rdi
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rdx,2), %rbx
	movq	56(%rsp), %r9           # 8-byte Reload
	subq	%rdx, %r9
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph175
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	-2(%rsi), %eax
	imull	%r8d, %eax
	movzwl	(%rsi), %edx
	imull	%r14d, %edx
	movzwl	-2(%rdi), %ebp
	imull	%r8d, %ebp
	movzwl	(%rdi), %ecx
	imull	%r14d, %ecx
	addl	%ebp, %ecx
	imull	%r13d, %ecx
	addl	%eax, %edx
	imull	%r12d, %edx
	leal	32(%rdx,%rcx), %eax
	shrl	$6, %eax
	movw	%ax, (%rbx)
	addq	$2, %rsi
	addq	$2, %rdi
	addq	$2, %rbx
	decq	%r9
	jne	.LBB0_15
.LBB0_60:                               # %.preheader
                                        #   in Loop: Header=BB0_11 Depth=4
	movl	-84(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, -40(%rsp)         # 4-byte Folded Reload
	jg	.LBB0_70
# BB#61:                                # %.lr.ph180
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-128(%rsp), %rax        # 8-byte Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rsi
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rcx
	cmpq	$7, -16(%rsp)           # 8-byte Folded Reload
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movl	%eax, %ebx
	jbe	.LBB0_62
# BB#64:                                # %min.iters.checked337
                                        #   in Loop: Header=BB0_11 Depth=4
	cmpq	$0, -72(%rsp)           # 8-byte Folded Reload
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %ebx
	je	.LBB0_62
# BB#65:                                # %vector.memcheck359
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-24(%rsp), %r13         # 8-byte Reload
	movq	-112(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%r13,2), %rdi
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rdx,%rax,2), %rdx
	cmpq	%rsi, %rdi
	sbbb	%al, %al
	cmpq	%rdx, %rsi
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rcx, %rdi
	sbbb	%al, %al
	cmpq	%rdx, %rcx
	sbbb	%r9b, %r9b
	testb	$1, %bl
	movq	%r13, %rdx
	movq	-128(%rsp), %rbx        # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	jne	.LBB0_62
# BB#66:                                # %vector.memcheck359
                                        #   in Loop: Header=BB0_11 Depth=4
	andb	%r9b, %al
	andb	$1, %al
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %ebx
	jne	.LBB0_62
# BB#67:                                # %vector.ph360
                                        #   in Loop: Header=BB0_11 Depth=4
	movzwl	(%rsi), %eax
	movd	%eax, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	pand	%xmm7, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm5, %xmm2      # xmm2 = xmm5[1,1,3,3]
	pmuludq	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movzwl	(%rcx), %eax
	movd	%eax, %xmm1
	pshuflw	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	pand	%xmm7, %xmm1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm6, %xmm3      # xmm3 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm11, %xmm0
	paddd	%xmm1, %xmm0
	psrld	$6, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm0, %xmm0
	movq	-72(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_68:                               # %vector.body332
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movdqu	%xmm0, (%rdi)
	addq	$16, %rdi
	addq	$-8, %rdx
	jne	.LBB0_68
# BB#69:                                # %middle.block333
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-16(%rsp), %rax         # 8-byte Reload
	cmpq	-72(%rsp), %rax         # 8-byte Folded Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	movl	-32(%rsp), %ebx         # 4-byte Reload
	je	.LBB0_70
	.p2align	4, 0x90
.LBB0_62:                               # %scalar.ph334.preheader
                                        #   in Loop: Header=BB0_11 Depth=4
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rdx,2), %rdi
	.p2align	4, 0x90
.LBB0_63:                               # %scalar.ph334
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rsi), %eax
	imull	%r11d, %eax
	movzwl	(%rcx), %edx
	imull	%r10d, %edx
	leal	32(%rax,%rdx), %eax
	shrl	$6, %eax
	movw	%ax, (%rdi)
	incl	%ebx
	addq	$2, %rdi
	cmpl	%ebp, %ebx
	jl	.LBB0_63
.LBB0_70:                               # %._crit_edge
                                        #   in Loop: Header=BB0_11 Depth=4
	incq	%r15
	movl	68(%rsp), %esi          # 4-byte Reload
	cmpl	-88(%rsp), %esi         # 4-byte Folded Reload
	jl	.LBB0_11
	.p2align	4, 0x90
.LBB0_71:                               # %._crit_edge186
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addl	-92(%rsp), %r14d        # 4-byte Folded Reload
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB0_8
# BB#72:                                #   in Loop: Header=BB0_7 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addl	-96(%rsp), %edx         # 4-byte Folded Reload
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jl	.LBB0_7
# BB#73:                                #   in Loop: Header=BB0_6 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$2, %rcx
	jne	.LBB0_6
# BB#74:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	getSubImagesChroma, .Lfunc_end0-getSubImagesChroma
	.cfi_endproc

	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
