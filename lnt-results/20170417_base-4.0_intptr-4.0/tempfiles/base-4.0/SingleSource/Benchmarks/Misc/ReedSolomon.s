	.text
	.file	"ReedSolomon.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
.LCPI0_1:
	.long	16                      # 0x10
	.long	32                      # 0x20
	.long	64                      # 0x40
	.long	128                     # 0x80
.LCPI0_2:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
.LCPI0_3:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.text
	.globl	rsdec_204
	.p2align	4, 0x90
	.type	rsdec_204,@function
rsdec_204:                              # @rsdec_204
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1720, %rsp             # imm = 0x6B8
.Lcfi6:
	.cfi_def_cfa_offset 1776
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	inited(%rip), %al
	testb	%al, %al
	jne	.LBB0_9
# BB#1:
	movl	$0, index_of+4(%rip)
	movl	$1, index_of+8(%rip)
	movl	$2, index_of+16(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,2,4,8]
	movaps	%xmm0, alpha_to(%rip)
	movl	$3, index_of+32(%rip)
	movl	$4, index_of+64(%rip)
	movl	$29, alpha_to+32(%rip)
	movl	$5, index_of+128(%rip)
	movl	$6, index_of+256(%rip)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [16,32,64,128]
	movdqa	%xmm0, alpha_to+16(%rip)
	movl	$7, index_of+512(%rip)
	movl	$8, index_of+116(%rip)
	movl	$29, %eax
	movl	$9, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$128, %eax
	jge	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	addl	%eax, %eax
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	addl	%eax, %eax
	xorl	alpha_to+32(%rip), %eax
	xorl	$256, %eax              # imm = 0x100
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, alpha_to(,%rcx,4)
	movslq	%eax, %rdx
	movl	%ecx, index_of(,%rdx,4)
	addl	%eax, %eax
	cmpl	$128, %edx
	jl	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	alpha_to+32(%rip), %eax
	xorl	$256, %eax              # imm = 0x100
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, alpha_to+4(,%rcx,4)
	movslq	%eax, %rdx
	leal	1(%rcx), %esi
	movl	%esi, index_of(,%rdx,4)
	addq	$2, %rcx
	cmpq	$255, %rcx
	jne	.LBB0_2
# BB#8:                                 # %generate_gf.exit
	movl	$-1, index_of(%rip)
	callq	gen_poly
	movb	$1, inited(%rip)
.LBB0_9:                                # %.preheader34.preheader
	leaq	188(%rbx), %rax
	movd	188(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm0, %xmm0
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqa	%xmm1, recd(%rip)
	movd	192(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqa	%xmm1, recd+16(%rip)
	movd	196(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqa	%xmm1, recd+32(%rip)
	movd	200(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movdqa	%xmm1, recd+48(%rip)
	movdqu	%xmm0, recd+252(%rip)
	movdqa	%xmm0, recd+240(%rip)
	movdqa	%xmm0, recd+224(%rip)
	movdqa	%xmm0, recd+208(%rip)
	movdqa	%xmm0, recd+192(%rip)
	movdqa	%xmm0, recd+176(%rip)
	movdqa	%xmm0, recd+160(%rip)
	movdqa	%xmm0, recd+144(%rip)
	movdqa	%xmm0, recd+128(%rip)
	movdqa	%xmm0, recd+112(%rip)
	movdqa	%xmm0, recd+96(%rip)
	movdqa	%xmm0, recd+80(%rip)
	movdqa	%xmm0, recd+64(%rip)
	movl	$recd+268, %ecx
	cmpq	%rcx, %rax
	jbe	.LBB0_12
# BB#10:                                # %.preheader34.preheader
	movl	$recd+1020, %eax
	cmpq	%rax, %rbx
	jae	.LBB0_12
# BB#11:
	xorl	%eax, %eax
	jmp	.LBB0_14
.LBB0_12:                               # %vector.body.preheader
	xorl	%ecx, %ecx
	movl	$184, %eax
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_167:                              # %vector.body.1
                                        #   in Loop: Header=BB0_13 Depth=1
	movd	8(%rbx,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movd	12(%rbx,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	%xmm1, recd+300(,%rcx,4)
	movdqu	%xmm2, recd+316(,%rcx,4)
	addq	$16, %rcx
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movd	4(%rbx,%rcx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	%xmm1, recd+268(,%rcx,4)
	movdqu	%xmm2, recd+284(,%rcx,4)
	cmpq	$176, %rcx
	jne	.LBB0_167
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader32
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx,%rax), %ecx
	movl	%ecx, recd+268(,%rax,4)
	movzbl	1(%rbx,%rax), %ecx
	movl	%ecx, recd+272(,%rax,4)
	movzbl	2(%rbx,%rax), %ecx
	movl	%ecx, recd+276(,%rax,4)
	movzbl	3(%rbx,%rax), %ecx
	movl	%ecx, recd+280(,%rax,4)
	addq	$4, %rax
	cmpq	$188, %rax
	jne	.LBB0_14
# BB#15:                                # %.preheader.preheader
	movq	$-1020, %rax            # imm = 0xFC04
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movslq	recd+1020(%rax), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, recd+1020(%rax)
	movslq	recd+1024(%rax), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, recd+1024(%rax)
	movslq	recd+1028(%rax), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, recd+1028(%rax)
	movslq	recd+1032(%rax), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, recd+1032(%rax)
	movslq	recd+1036(%rax), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, recd+1036(%rax)
	addq	$20, %rax
	jne	.LBB0_16
# BB#17:
	xorl	%eax, %eax
	movl	$1, %ecx
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB0_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
	movl	$0, 224(%rsp,%rcx,4)
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_19:                               #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	recd(,%rdi,4), %ebp
	cmpl	$-1, %ebp
	je	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_19 Depth=2
	movl	%edi, %ebx
	imull	%ecx, %ebx
	addl	%ebp, %ebx
	movslq	%ebx, %rbp
	imulq	$-2139062143, %rbp, %rbx # imm = 0x80808081
	shrq	$32, %rbx
	addl	%ebp, %ebx
	movl	%ebx, %edx
	shrl	$31, %edx
	sarl	$7, %ebx
	addl	%edx, %ebx
	movl	%ebx, %edx
	shll	$8, %edx
	subl	%ebx, %edx
	subl	%edx, %ebp
	movslq	%ebp, %rdx
	xorl	alpha_to(,%rdx,4), %esi
	movl	%esi, 224(%rsp,%rcx,4)
.LBB0_21:                               #   in Loop: Header=BB0_19 Depth=2
	incq	%rdi
	cmpq	$255, %rdi
	jne	.LBB0_19
# BB#22:                                #   in Loop: Header=BB0_18 Depth=1
	testl	%esi, %esi
	cmovnel	%r8d, %eax
	movslq	%esi, %rdx
	movl	index_of(,%rdx,4), %edx
	movl	%edx, 224(%rsp,%rcx,4)
	incq	%rcx
	cmpq	$17, %rcx
	jne	.LBB0_18
# BB#23:
	testl	%eax, %eax
	je	.LBB0_24
# BB#31:
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movl	$0, 144(%rsp)
	leaq	228(%rsp), %r14
	movl	228(%rsp), %r12d
	movl	%r12d, 148(%rsp)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [0,4294967295,4294967295,4294967295]
	movaps	%xmm0, 560(%rsp)
	movl	$1, %eax
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	movd	%eax, %xmm0
	movdqa	%xmm0, 624(%rsp)
	pcmpeqd	%xmm1, %xmm1
	movdqa	%xmm1, 576(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 640(%rsp)
	movdqa	%xmm1, 592(%rsp)
	movdqa	%xmm0, 656(%rsp)
	movdqa	%xmm1, 608(%rsp)
	movdqa	%xmm0, 672(%rsp)
	movl	$0, 400(%rsp)
	movl	$0, 404(%rsp)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 480(%rsp)
	leaq	688(%rsp), %r9
	leaq	628(%rsp), %r15
	leaq	692(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	movabsq	$-4294967296, %r11      # imm = 0xFFFFFFFF00000000
	movq	%rcx, %rsi
	movl	%r12d, 60(%rsp)         # 4-byte Spill
	xorl	%r8d, %r8d
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_74:                               # %._crit_edge338.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%ebp, %rax
	movl	index_of(,%rax,4), %r12d
	movl	%r12d, 152(%rsp,%r8,4)
	cmpl	$9, %r10d
	movq	24(%rsp), %rsi          # 8-byte Reload
	jl	.LBB0_68
	jmp	.LBB0_76
	.p2align	4, 0x90
.LBB0_67:                               # %._crit_edge338.thread.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%ebp, %rax
	movl	index_of(,%rax,4), %r12d
	movl	%r12d, 152(%rsp,%r8,4)
.LBB0_68:                               # %._crit_edge338._crit_edge.i.backedge
                                        #   in Loop: Header=BB0_32 Depth=1
	addq	$64, %r9
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rsi
	incl	20(%rsp)                # 4-byte Folded Spill
	addq	$64, %r15
	addq	$64, %r13
	movq	%r13, 48(%rsp)          # 8-byte Spill
	addq	$4, %r14
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB0_32:                               # %._crit_edge338._crit_edge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_34 Depth 2
                                        #     Child Loop BB0_38 Depth 2
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_60 Depth 2
                                        #     Child Loop BB0_54 Depth 2
                                        #     Child Loop BB0_70 Depth 2
	leaq	2(%r8), %r13
	leaq	1(%r8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$-1, %r12d
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	je	.LBB0_48
# BB#33:                                #   in Loop: Header=BB0_32 Depth=1
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movslq	%r13d, %rax
	shlq	$6, %rax
	leaq	560(%rsp,%rax), %r14
	movl	20(%rsp), %eax          # 4-byte Reload
	movq	%rsi, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB0_34:                               # %.preheader292.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdi
	leaq	-1(%rdi), %rcx
	decl	%eax
	testq	%rdi, %rdi
	jle	.LBB0_36
# BB#35:                                # %.preheader292.i
                                        #   in Loop: Header=BB0_34 Depth=2
	addq	%r11, %rdx
	cmpl	$-1, 144(%rsp,%rdi,4)
	je	.LBB0_34
.LBB0_36:                               #   in Loop: Header=BB0_32 Depth=1
	leaq	1(%rcx), %rdx
	testq	%rdi, %rdi
	movq	%r10, 72(%rsp)          # 8-byte Spill
	movq	%r9, 40(%rsp)           # 8-byte Spill
	jle	.LBB0_41
# BB#37:                                # %.preheader290.preheader.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%eax, %rdi
	leaq	476(%rsp), %rax
	leaq	(%rax,%rdi,4), %r9
	leaq	140(%rsp), %rax
	leaq	(%rax,%rdi,4), %rax
	leaq	1(%rdi), %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_38:                               # %.preheader290.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, (%rax,%rdi,4)
	je	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_38 Depth=2
	movslq	%edx, %rdx
	movl	480(%rsp,%rdx,4), %esi
	leal	(%rcx,%rdi), %ebp
	cmpl	(%r9,%rdi,4), %esi
	cmovll	%ebp, %edx
.LBB0_40:                               #   in Loop: Header=BB0_38 Depth=2
	leaq	-1(%rbx,%rdi), %rsi
	decq	%rdi
	cmpq	$1, %rsi
	jg	.LBB0_38
.LBB0_41:                               # %.loopexit291.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%edx, %r10
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %r9d
	subl	%r10d, %r9d
	addl	400(%rsp,%r10,4), %r9d
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpl	%r9d, %eax
	movq	%rax, %rbp
	cmovgel	%eax, %r9d
	movl	%r9d, 408(%rsp,%r8,4)
	movdqa	%xmm0, 48(%r14)
	movdqa	%xmm0, 32(%r14)
	movdqa	%xmm0, 16(%r14)
	movdqa	%xmm0, (%r14)
	movslq	400(%rsp,%r10,4), %rbx
	testq	%rbx, %rbx
	movq	64(%rsp), %r14          # 8-byte Reload
	js	.LBB0_46
# BB#42:                                # %.lr.ph329.i
                                        #   in Loop: Header=BB0_32 Depth=1
	addl	$255, %r12d
	incq	%rbx
	movl	20(%rsp), %ecx          # 4-byte Reload
	subl	%r10d, %ecx
	movq	%r10, %rax
	shlq	$6, %rax
	leaq	560(%rsp,%rax), %rdx
	.p2align	4, 0x90
.LBB0_43:                               #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %eax
	cmpl	$-1, %eax
	je	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_43 Depth=2
	addl	%r12d, %eax
	subl	144(%rsp,%r10,4), %eax
	cltq
	imulq	$-2139062143, %rax, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%eax, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %eax
	cltq
	movl	alpha_to(,%rax,4), %eax
	movslq	%ecx, %rsi
	movq	%r13, %rdi
	shlq	$6, %rdi
	leaq	560(%rsp,%rdi), %rdi
	movl	%eax, (%rdi,%rsi,4)
.LBB0_45:                               #   in Loop: Header=BB0_43 Depth=2
	incl	%ecx
	addq	$4, %rdx
	decq	%rbx
	jne	.LBB0_43
.LBB0_46:                               # %.preheader287.i
                                        #   in Loop: Header=BB0_32 Depth=1
	testl	%ebp, %ebp
	js	.LBB0_47
# BB#55:                                # %.lr.ph331.i
                                        #   in Loop: Header=BB0_32 Depth=1
	testb	$1, %bpl
	jne	.LBB0_56
# BB#57:                                #   in Loop: Header=BB0_32 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$6, %rax
	movslq	560(%rsp,%rax), %rcx
	movq	%r13, %rdx
	shlq	$6, %rdx
	xorl	%ecx, 560(%rsp,%rdx)
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 560(%rsp,%rax)
	movl	$1, %ecx
	jmp	.LBB0_58
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_32 Depth=1
	movl	%r10d, 408(%rsp,%r8,4)
	testl	%r10d, %r10d
	js	.LBB0_62
# BB#49:                                # %.lr.ph334.i
                                        #   in Loop: Header=BB0_32 Depth=1
	testb	$1, %r10b
	jne	.LBB0_50
# BB#51:                                #   in Loop: Header=BB0_32 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$6, %rax
	movslq	560(%rsp,%rax), %rcx
	movq	%r13, %rdx
	shlq	$6, %rdx
	movl	%ecx, 560(%rsp,%rdx)
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 560(%rsp,%rax)
	movl	$1, %eax
	testl	%r10d, %r10d
	jne	.LBB0_53
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_47:                               #   in Loop: Header=BB0_32 Depth=1
	movl	%r9d, %r10d
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_62
.LBB0_56:                               #   in Loop: Header=BB0_32 Depth=1
	xorl	%ecx, %ecx
.LBB0_58:                               # %.prol.loopexit158
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB0_61
# BB#59:                                # %.lr.ph331.i.new
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%ebp, %rax
	incq	%rax
	subq	%rcx, %rax
	leaq	(%r15,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_60:                               #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-4(%rcx), %rdx
	xorl	%edx, 60(%rcx)
	movl	index_of(,%rdx,4), %edx
	movl	%edx, -4(%rcx)
	movslq	(%rcx), %rdx
	xorl	%edx, 64(%rcx)
	movl	index_of(,%rdx,4), %edx
	movl	%edx, (%rcx)
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB0_60
.LBB0_61:                               #   in Loop: Header=BB0_32 Depth=1
	movl	%r9d, %r10d
	movq	40(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_62
.LBB0_50:                               #   in Loop: Header=BB0_32 Depth=1
	xorl	%eax, %eax
	testl	%r10d, %r10d
	je	.LBB0_62
.LBB0_53:                               # %.lr.ph334.i.new
                                        #   in Loop: Header=BB0_32 Depth=1
	movslq	%r10d, %rcx
	decq	%rax
	.p2align	4, 0x90
.LBB0_54:                               #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r15,%rax,4), %rdx
	movl	%edx, 64(%r15,%rax,4)
	movl	index_of(,%rdx,4), %edx
	movl	%edx, (%r15,%rax,4)
	movslq	4(%r15,%rax,4), %rdx
	movl	%edx, 68(%r15,%rax,4)
	movl	index_of(,%rdx,4), %edx
	movl	%edx, 4(%r15,%rax,4)
	addq	$2, %rax
	cmpq	%rax, %rcx
	jne	.LBB0_54
	.p2align	4, 0x90
.LBB0_62:                               # %.loopexit286.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	subl	%r10d, %eax
	movl	%eax, 488(%rsp,%r8,4)
	cmpq	$15, %rcx
	jg	.LBB0_75
# BB#63:                                #   in Loop: Header=BB0_32 Depth=1
	movslq	232(%rsp,%r8,4), %rax
	cmpq	$-1, %rax
	je	.LBB0_64
# BB#65:                                #   in Loop: Header=BB0_32 Depth=1
	movl	alpha_to(,%rax,4), %ebp
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_64:                               #   in Loop: Header=BB0_32 Depth=1
	xorl	%ebp, %ebp
.LBB0_66:                               #   in Loop: Header=BB0_32 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	%ebp, 152(%rsp,%r8,4)
	testl	%r10d, %r10d
	jle	.LBB0_67
# BB#69:                                # %.lr.ph337.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	%r14, %rax
	movslq	%r10d, %rbx
	movq	%rax, %rcx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB0_70:                               #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB0_73
# BB#71:                                #   in Loop: Header=BB0_70 Depth=2
	movslq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_70 Depth=2
	addl	index_of(,%rdi,4), %eax
	cltq
	imulq	$-2139062143, %rax, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%eax, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %eax
	cltq
	xorl	alpha_to(,%rax,4), %ebp
	movl	%ebp, 152(%rsp,%r8,4)
.LBB0_73:                               #   in Loop: Header=BB0_70 Depth=2
	addq	$4, %rdx
	addq	$-4, %rcx
	decq	%rbx
	jne	.LBB0_70
	jmp	.LBB0_74
.LBB0_75:                               # %.critedge.i
	cmpl	$9, %r10d
	jge	.LBB0_76
# BB#83:                                # %.preheader285.i
	movb	$1, %sil
	testl	%r10d, %r10d
	js	.LBB0_84
# BB#85:                                # %.lr.ph326.i
	leal	1(%r10), %ecx
	xorl	%eax, %eax
	andq	$3, %rcx
	je	.LBB0_87
	.p2align	4, 0x90
.LBB0_86:                               # =>This Inner Loop Header: Depth=1
	movslq	(%r9,%rax,4), %rdx
	movl	index_of(,%rdx,4), %edx
	movl	%edx, (%r9,%rax,4)
	incq	%rax
	cmpq	%rax, %rcx
	jne	.LBB0_86
.LBB0_87:                               # %.prol.loopexit
	movslq	%r10d, %rbx
	cmpl	$3, %r10d
	jb	.LBB0_90
# BB#88:                                # %.lr.ph326.i.new
	decq	%rax
	.p2align	4, 0x90
.LBB0_89:                               # =>This Inner Loop Header: Depth=1
	movslq	4(%r9,%rax,4), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 4(%r9,%rax,4)
	movslq	8(%r9,%rax,4), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 8(%r9,%rax,4)
	movslq	12(%r9,%rax,4), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 12(%r9,%rax,4)
	movslq	16(%r9,%rax,4), %rcx
	movl	index_of(,%rcx,4), %ecx
	movl	%ecx, 16(%r9,%rax,4)
	addq	$4, %rax
	cmpq	%rax, %rbx
	jne	.LBB0_89
.LBB0_90:                               # %.preheader284.i
	testl	%r10d, %r10d
	jle	.LBB0_84
# BB#91:                                # %.preheader282.preheader.i
	movq	%r9, %r12
	leaq	356(%rsp), %rdi
	movq	%r13, %rax
	shlq	$6, %rax
	leaq	564(%rsp,%rax), %rsi
	movq	%r10, %r14
	leal	-1(%r10), %eax
	leaq	4(,%rax,4), %rdx
	callq	memcpy
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB0_92:                               # %.preheader282.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_93 Depth 2
	xorl	%edi, %edi
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_93:                               #   Parent Loop BB0_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	356(%rsp,%rdi,4), %ebp
	cmpq	%rdx, %rbp
	je	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_93 Depth=2
	leaq	1(%rdi,%rbp), %rbp
	movslq	%ebp, %rbp
	imulq	$-2139062143, %rbp, %rax # imm = 0x80808081
	shrq	$32, %rax
	addl	%ebp, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$7, %eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	shll	$8, %ecx
	subl	%eax, %ecx
	subl	%ecx, %ebp
	movl	%ebp, 356(%rsp,%rdi,4)
	movslq	%ebp, %rax
	xorl	alpha_to(,%rax,4), %esi
.LBB0_95:                               #   in Loop: Header=BB0_93 Depth=2
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB0_93
# BB#96:                                # %._crit_edge319.i
                                        #   in Loop: Header=BB0_92 Depth=1
	testl	%esi, %esi
	jne	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_92 Depth=1
	movslq	%r9d, %r9
	movl	%r8d, 112(%rsp,%r9,4)
	movl	$255, %eax
	subl	%r8d, %eax
	movl	%eax, 80(%rsp,%r9,4)
	incl	%r9d
.LBB0_98:                               #   in Loop: Header=BB0_92 Depth=1
	incl	%r8d
	cmpl	$256, %r8d              # imm = 0x100
	jne	.LBB0_92
# BB#99:
	xorl	%esi, %esi
	movq	%r14, %r10
	cmpl	%r10d, %r9d
	je	.LBB0_108
	jmp	.LBB0_101
.LBB0_76:                               # %.critedge.thread.preheader.i.preheader
	movq	$-1020, %rax            # imm = 0xFC04
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_77:                               # %.critedge.thread.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	recd+1020(%rax), %rdx
	xorl	%ecx, %ecx
	cmpq	$-1, %rdx
	movl	$0, %esi
	je	.LBB0_79
# BB#78:                                #   in Loop: Header=BB0_77 Depth=1
	movl	alpha_to(,%rdx,4), %esi
.LBB0_79:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB0_77 Depth=1
	movl	%esi, recd+1020(%rax)
	movslq	recd+1024(%rax), %rdx
	cmpq	$-1, %rdx
	je	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_77 Depth=1
	movl	alpha_to(,%rdx,4), %ecx
.LBB0_81:                               # %.critedge.thread.i.1
                                        #   in Loop: Header=BB0_77 Depth=1
	movl	%ecx, recd+1024(%rax)
	movslq	recd+1028(%rax), %rcx
	cmpq	$-1, %rcx
	je	.LBB0_82
# BB#161:                               #   in Loop: Header=BB0_77 Depth=1
	movl	alpha_to(,%rcx,4), %ecx
	jmp	.LBB0_162
	.p2align	4, 0x90
.LBB0_82:                               #   in Loop: Header=BB0_77 Depth=1
	xorl	%ecx, %ecx
.LBB0_162:                              # %.critedge.thread.i.2
                                        #   in Loop: Header=BB0_77 Depth=1
	movl	%ecx, recd+1028(%rax)
	addq	$12, %rax
	jne	.LBB0_77
	jmp	.LBB0_145
.LBB0_24:                               # %.preheader.i.preheader
	movq	$-1020, %rax            # imm = 0xFC04
	.p2align	4, 0x90
.LBB0_25:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	recd+1020(%rax), %rdx
	xorl	%ecx, %ecx
	cmpq	$-1, %rdx
	movl	$0, %esi
	je	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_25 Depth=1
	movl	alpha_to(,%rdx,4), %esi
.LBB0_27:                               # %.preheader.i.1148
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	%esi, recd+1020(%rax)
	movslq	recd+1024(%rax), %rdx
	cmpq	$-1, %rdx
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_25 Depth=1
	movl	alpha_to(,%rdx,4), %ecx
.LBB0_29:                               # %.preheader.i.2149
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	%ecx, recd+1024(%rax)
	movslq	recd+1028(%rax), %rcx
	cmpq	$-1, %rcx
	je	.LBB0_30
# BB#159:                               #   in Loop: Header=BB0_25 Depth=1
	movl	alpha_to(,%rcx,4), %ecx
	jmp	.LBB0_160
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_25 Depth=1
	xorl	%ecx, %ecx
.LBB0_160:                              #   in Loop: Header=BB0_25 Depth=1
	movl	%ecx, recd+1028(%rax)
	addq	$12, %rax
	jne	.LBB0_25
	jmp	.LBB0_145
.LBB0_84:
	movq	%r9, %r12
	xorl	%r9d, %r9d
	cmpl	%r10d, %r9d
	jne	.LBB0_101
.LBB0_108:                              # %.preheader279.i
	movq	%r10, %r8
	movl	%esi, 8(%rsp)           # 4-byte Spill
	testb	%sil, %sil
	jne	.LBB0_109
# BB#116:                               # %.lr.ph314.i
	movslq	%r8d, %r10
	leaq	228(%rsp), %r9
	movl	$1, %r15d
	xorl	%r14d, %r14d
	shlq	$6, %r13
	leaq	560(%rsp,%r13), %r11
	movl	60(%rsp), %eax          # 4-byte Reload
	jmp	.LBB0_117
	.p2align	4, 0x90
.LBB0_129:                              # %._crit_edge311.i
                                        #   in Loop: Header=BB0_117 Depth=1
	movslq	%ebx, %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, 304(%rsp,%r15,4)
	cmpq	%r10, %r15
	jge	.LBB0_109
# BB#130:                               # %._crit_edge311._crit_edge.i
                                        #   in Loop: Header=BB0_117 Depth=1
	movl	228(%rsp,%r15,4), %eax
	incq	%r15
	incq	%r14
	addq	$4, %r12
.LBB0_117:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_125 Depth 2
	cmpl	$-1, %eax
	movslq	(%r11,%r15,4), %rcx
	je	.LBB0_120
# BB#118:                               #   in Loop: Header=BB0_117 Depth=1
	cmpl	$-1, %ecx
	cltq
	movl	alpha_to(,%rax,4), %ebx
	je	.LBB0_123
# BB#119:                               #   in Loop: Header=BB0_117 Depth=1
	xorl	alpha_to(,%rcx,4), %ebx
	jmp	.LBB0_123
	.p2align	4, 0x90
.LBB0_120:                              # %.thread273.i
                                        #   in Loop: Header=BB0_117 Depth=1
	cmpl	$-1, %ecx
	je	.LBB0_121
# BB#122:                               #   in Loop: Header=BB0_117 Depth=1
	movl	alpha_to(,%rcx,4), %ebx
	jmp	.LBB0_123
.LBB0_121:                              #   in Loop: Header=BB0_117 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_123:                              #   in Loop: Header=BB0_117 Depth=1
	movl	%ebx, 304(%rsp,%r15,4)
	cmpq	$2, %r15
	jl	.LBB0_129
# BB#124:                               # %.lr.ph310.i.preheader
                                        #   in Loop: Header=BB0_117 Depth=1
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB0_125:                              # %.lr.ph310.i
                                        #   Parent Loop BB0_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ebp
	cmpl	$-1, %ebp
	je	.LBB0_128
# BB#126:                               #   in Loop: Header=BB0_125 Depth=2
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_125 Depth=2
	addl	%ebp, %eax
	cltq
	imulq	$-2139062143, %rax, %rbp # imm = 0x80808081
	shrq	$32, %rbp
	addl	%eax, %ebp
	movl	%ebp, %esi
	shrl	$31, %esi
	sarl	$7, %ebp
	addl	%esi, %ebp
	movl	%ebp, %esi
	shll	$8, %esi
	subl	%ebp, %esi
	subl	%esi, %eax
	cltq
	xorl	alpha_to(,%rax,4), %ebx
	movl	%ebx, 304(%rsp,%r15,4)
.LBB0_128:                              #   in Loop: Header=BB0_125 Depth=2
	addq	$-4, %rcx
	addq	$4, %rdx
	decq	%rdi
	jne	.LBB0_125
	jmp	.LBB0_129
.LBB0_101:                              # %.preheader280.i.preheader
	movq	$-1020, %rax            # imm = 0xFC04
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_102:                              # %.preheader280.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	recd+1020(%rax), %rdx
	xorl	%ecx, %ecx
	cmpq	$-1, %rdx
	movl	$0, %esi
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_102 Depth=1
	movl	alpha_to(,%rdx,4), %esi
.LBB0_104:                              # %.preheader280.i.1152
                                        #   in Loop: Header=BB0_102 Depth=1
	movl	%esi, recd+1020(%rax)
	movslq	recd+1024(%rax), %rdx
	cmpq	$-1, %rdx
	je	.LBB0_106
# BB#105:                               #   in Loop: Header=BB0_102 Depth=1
	movl	alpha_to(,%rdx,4), %ecx
.LBB0_106:                              # %.preheader280.i.2153
                                        #   in Loop: Header=BB0_102 Depth=1
	movl	%ecx, recd+1024(%rax)
	movslq	recd+1028(%rax), %rcx
	cmpq	$-1, %rcx
	je	.LBB0_107
# BB#165:                               #   in Loop: Header=BB0_102 Depth=1
	movl	alpha_to(,%rcx,4), %ecx
	jmp	.LBB0_166
	.p2align	4, 0x90
.LBB0_107:                              #   in Loop: Header=BB0_102 Depth=1
	xorl	%ecx, %ecx
.LBB0_166:                              #   in Loop: Header=BB0_102 Depth=1
	movl	%ecx, recd+1028(%rax)
	addq	$12, %rax
	jne	.LBB0_102
	jmp	.LBB0_145
.LBB0_109:                              # %.preheader278.i.preheader
	movq	$-1020, %rax            # imm = 0xFC04
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_110:                              # %.preheader278.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	recd+1020(%rax), %rdx
	xorl	%ecx, %ecx
	cmpq	$-1, %rdx
	movl	$0, %esi
	je	.LBB0_112
# BB#111:                               #   in Loop: Header=BB0_110 Depth=1
	movl	alpha_to(,%rdx,4), %esi
.LBB0_112:                              # %.preheader278.i.1150
                                        #   in Loop: Header=BB0_110 Depth=1
	movl	%esi, recd+1020(%rax)
	movslq	recd+1024(%rax), %rdx
	cmpq	$-1, %rdx
	je	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_110 Depth=1
	movl	alpha_to(,%rdx,4), %ecx
.LBB0_114:                              # %.preheader278.i.2151
                                        #   in Loop: Header=BB0_110 Depth=1
	movl	%ecx, recd+1024(%rax)
	movslq	recd+1028(%rax), %rcx
	cmpq	$-1, %rcx
	je	.LBB0_115
# BB#163:                               #   in Loop: Header=BB0_110 Depth=1
	movl	alpha_to(,%rcx,4), %ecx
	jmp	.LBB0_164
	.p2align	4, 0x90
.LBB0_115:                              #   in Loop: Header=BB0_110 Depth=1
	xorl	%ecx, %ecx
.LBB0_164:                              #   in Loop: Header=BB0_110 Depth=1
	movl	%ecx, recd+1028(%rax)
	addq	$12, %rax
	jne	.LBB0_110
# BB#131:                               # %.preheader276.i
	testl	%r8d, %r8d
	movl	8(%rsp), %ecx           # 4-byte Reload
	jle	.LBB0_145
# BB#132:                               # %.lr.ph306.i
	movslq	%r8d, %rax
	testb	%cl, %cl
	je	.LBB0_133
# BB#148:                               # %.lr.ph306.split.us.preheader.i
	movl	$255, %r8d
	addl	index_of+4(%rip), %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_153:                              # %.lr.ph303.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_149 Depth 2
	movslq	80(%rsp,%rdx,4), %r9
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_149:                              #   Parent Loop BB0_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %rdx
	je	.LBB0_151
# BB#150:                               #   in Loop: Header=BB0_149 Depth=2
	movl	112(%rsp,%rdx,4), %ebx
	addl	80(%rsp,%rbp,4), %ebx
	movslq	%ebx, %rbx
	imulq	$-2139062143, %rbx, %rcx # imm = 0x80808081
	shrq	$32, %rcx
	addl	%ebx, %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	sarl	$7, %ecx
	addl	%esi, %ecx
	movl	%ecx, %esi
	shll	$8, %esi
	subl	%ecx, %esi
	subl	%esi, %ebx
	movslq	%ebx, %rcx
	movslq	alpha_to(,%rcx,4), %rcx
	xorq	$1, %rcx
	addl	index_of(,%rcx,4), %edi
.LBB0_151:                              #   in Loop: Header=BB0_149 Depth=2
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB0_149
# BB#152:                               #   in Loop: Header=BB0_153 Depth=1
	movslq	%edi, %rcx
	imulq	$-2139062143, %rcx, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%ecx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %ecx
	movl	%r8d, %esi
	subl	%ecx, %esi
	movslq	%esi, %rcx
	imulq	$-2139062143, %rcx, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%ecx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %ecx
	movslq	%ecx, %rcx
	movl	alpha_to(,%rcx,4), %ecx
	xorl	%ecx, recd(,%r9,4)
	incq	%rdx
	cmpq	%rax, %rdx
	jne	.LBB0_153
	jmp	.LBB0_145
.LBB0_133:                              # %.lr.ph306.split.preheader.i
	movl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_134:                              # %.lr.ph306.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_135 Depth 2
                                        #     Child Loop BB0_140 Depth 2
	movslq	80(%rsp,%rdx,4), %r9
	movl	$1, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_135:                              #   Parent Loop BB0_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	308(%rsp,%rbp,4), %ebx
	cmpl	$-1, %ebx
	je	.LBB0_137
# BB#136:                               #   in Loop: Header=BB0_135 Depth=2
	leal	1(%rbp), %ecx
	imull	112(%rsp,%rdx,4), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	imulq	$-2139062143, %rcx, %rbx # imm = 0x80808081
	shrq	$32, %rbx
	addl	%ecx, %ebx
	movl	%ebx, %esi
	shrl	$31, %esi
	sarl	$7, %ebx
	addl	%esi, %ebx
	movl	%ebx, %esi
	shll	$8, %esi
	subl	%ebx, %esi
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	xorl	alpha_to(,%rcx,4), %edi
.LBB0_137:                              #   in Loop: Header=BB0_135 Depth=2
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB0_135
# BB#138:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_134 Depth=1
	testl	%edi, %edi
	je	.LBB0_144
# BB#139:                               # %.lr.ph303.i
                                        #   in Loop: Header=BB0_134 Depth=1
	movslq	%edi, %rcx
	movl	index_of(,%rcx,4), %r10d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_140:                              #   Parent Loop BB0_134 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %rdx
	je	.LBB0_142
# BB#141:                               #   in Loop: Header=BB0_140 Depth=2
	movl	112(%rsp,%rdx,4), %ecx
	addl	80(%rsp,%rbp,4), %ecx
	movslq	%ecx, %rcx
	imulq	$-2139062143, %rcx, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%ecx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %ecx
	movslq	%ecx, %rcx
	movslq	alpha_to(,%rcx,4), %rcx
	xorq	$1, %rcx
	addl	index_of(,%rcx,4), %ebx
.LBB0_142:                              #   in Loop: Header=BB0_140 Depth=2
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB0_140
# BB#143:                               # %._crit_edge304.i
                                        #   in Loop: Header=BB0_134 Depth=1
	movslq	%ebx, %rcx
	imulq	$-2139062143, %rcx, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%ecx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %ecx
	addl	$255, %r10d
	subl	%ecx, %r10d
	movslq	%r10d, %rcx
	imulq	$-2139062143, %rcx, %rsi # imm = 0x80808081
	shrq	$32, %rsi
	addl	%ecx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	sarl	$7, %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shll	$8, %edi
	subl	%esi, %edi
	subl	%edi, %ecx
	movslq	%ecx, %rcx
	movl	alpha_to(,%rcx,4), %ecx
	xorl	%ecx, recd(,%r9,4)
.LBB0_144:                              #   in Loop: Header=BB0_134 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jne	.LBB0_134
.LBB0_145:                              # %decode_rs.exit
	movl	$recd+1020, %eax
	cmpq	%rax, %r14
	jae	.LBB0_154
# BB#146:                               # %decode_rs.exit
	leaq	188(%r14), %rax
	movl	$recd+268, %ecx
	cmpq	%rcx, %rax
	jbe	.LBB0_154
# BB#147:
	xorl	%eax, %eax
	jmp	.LBB0_156
.LBB0_154:                              # %vector.body113.preheader
	xorl	%ecx, %ecx
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [255,255,255,255]
	movl	$184, %eax
	jmp	.LBB0_155
	.p2align	4, 0x90
.LBB0_158:                              # %vector.body113.1
                                        #   in Loop: Header=BB0_155 Depth=1
	movdqu	recd+300(,%rcx,4), %xmm1
	movdqu	recd+316(,%rcx,4), %xmm2
	pand	%xmm0, %xmm1
	packuswb	%xmm1, %xmm1
	packuswb	%xmm1, %xmm1
	movd	%xmm1, 8(%r14,%rcx)
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm2
	packuswb	%xmm2, %xmm2
	movd	%xmm2, 12(%r14,%rcx)
	addq	$16, %rcx
.LBB0_155:                              # %vector.body113
                                        # =>This Inner Loop Header: Depth=1
	movdqu	recd+268(,%rcx,4), %xmm1
	movdqu	recd+284(,%rcx,4), %xmm2
	pand	%xmm0, %xmm1
	packuswb	%xmm1, %xmm1
	packuswb	%xmm1, %xmm1
	movd	%xmm1, (%r14,%rcx)
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm2
	packuswb	%xmm2, %xmm2
	movd	%xmm2, 4(%r14,%rcx)
	cmpq	$176, %rcx
	jne	.LBB0_158
	.p2align	4, 0x90
.LBB0_156:                              # %scalar.ph115
                                        # =>This Inner Loop Header: Depth=1
	movzbl	recd+268(,%rax,4), %ecx
	movb	%cl, (%r14,%rax)
	movzbl	recd+272(,%rax,4), %ecx
	movb	%cl, 1(%r14,%rax)
	movzbl	recd+276(,%rax,4), %ecx
	movb	%cl, 2(%r14,%rax)
	movzbl	recd+280(,%rax,4), %ecx
	movb	%cl, 3(%r14,%rax)
	addq	$4, %rax
	cmpq	$188, %rax
	jne	.LBB0_156
# BB#157:                               # %.loopexit
	addq	$1720, %rsp             # imm = 0x6B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	rsdec_204, .Lfunc_end0-rsdec_204
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
.LCPI1_1:
	.long	16                      # 0x10
	.long	32                      # 0x20
	.long	64                      # 0x40
	.long	128                     # 0x80
	.text
	.globl	rsenc_204
	.p2align	4, 0x90
	.type	rsenc_204,@function
rsenc_204:                              # @rsenc_204
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	inited(%rip), %al
	testb	%al, %al
	jne	.LBB1_9
# BB#1:
	movl	$0, index_of+4(%rip)
	movl	$1, index_of+8(%rip)
	movl	$2, index_of+16(%rip)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1,2,4,8]
	movaps	%xmm0, alpha_to(%rip)
	movl	$3, index_of+32(%rip)
	movl	$4, index_of+64(%rip)
	movl	$29, alpha_to+32(%rip)
	movl	$5, index_of+128(%rip)
	movl	$6, index_of+256(%rip)
	movdqa	.LCPI1_1(%rip), %xmm0   # xmm0 = [16,32,64,128]
	movdqa	%xmm0, alpha_to+16(%rip)
	movl	$7, index_of+512(%rip)
	movl	$8, index_of+116(%rip)
	movl	$29, %eax
	movl	$9, %ecx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$128, %eax
	jge	.LBB1_3
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	addl	%eax, %eax
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_2 Depth=1
	addl	%eax, %eax
	xorl	alpha_to+32(%rip), %eax
	xorl	$256, %eax              # imm = 0x100
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%eax, alpha_to(,%rcx,4)
	movslq	%eax, %rdx
	movl	%ecx, index_of(,%rdx,4)
	addl	%eax, %eax
	cmpl	$128, %edx
	jl	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	alpha_to+32(%rip), %eax
	xorl	$256, %eax              # imm = 0x100
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%eax, alpha_to+4(,%rcx,4)
	movslq	%eax, %rdx
	leal	1(%rcx), %esi
	movl	%esi, index_of(,%rdx,4)
	addq	$2, %rcx
	cmpq	$255, %rcx
	jne	.LBB1_2
# BB#8:                                 # %generate_gf.exit
	movl	$-1, index_of(%rip)
	callq	gen_poly
	movb	$1, inited(%rip)
.LBB1_9:                                # %.preheader23.preheader
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, data+188(%rip)
	movdqa	%xmm0, data+176(%rip)
	movdqa	%xmm0, data+160(%rip)
	movdqa	%xmm0, data+144(%rip)
	movdqa	%xmm0, data+128(%rip)
	movdqa	%xmm0, data+112(%rip)
	movdqa	%xmm0, data+96(%rip)
	movdqa	%xmm0, data+80(%rip)
	movdqa	%xmm0, data+64(%rip)
	movdqa	%xmm0, data+48(%rip)
	movdqa	%xmm0, data+32(%rip)
	movdqa	%xmm0, data+16(%rip)
	movdqa	%xmm0, data(%rip)
	leaq	188(%r15), %r8
	movl	$data+204, %eax
	cmpq	%rax, %r8
	jbe	.LBB1_12
# BB#10:                                # %.preheader23.preheader
	movl	$data+956, %eax
	cmpq	%rax, %r15
	jae	.LBB1_12
# BB#11:
	xorl	%ecx, %ecx
	jmp	.LBB1_14
.LBB1_12:                               # %vector.body.preheader
	xorl	%edx, %edx
	movl	$184, %ecx
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_33:                               # %vector.body.1
                                        #   in Loop: Header=BB1_13 Depth=1
	movd	8(%r15,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movd	12(%r15,%rdx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	%xmm1, data+236(,%rdx,4)
	movdqu	%xmm2, data+252(,%rdx,4)
	addq	$16, %rdx
.LBB1_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	(%r15,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movd	4(%r15,%rdx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	%xmm1, data+204(,%rdx,4)
	movdqu	%xmm2, data+220(,%rdx,4)
	cmpq	$176, %rdx
	jne	.LBB1_33
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader22
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rcx), %eax
	movl	%eax, data+204(,%rcx,4)
	movzbl	1(%r15,%rcx), %eax
	movl	%eax, data+208(,%rcx,4)
	movzbl	2(%r15,%rcx), %eax
	movl	%eax, data+212(,%rcx,4)
	movzbl	3(%r15,%rcx), %eax
	movl	%eax, data+216(,%rcx,4)
	addq	$4, %rcx
	cmpq	$188, %rcx
	jne	.LBB1_14
# BB#15:                                # %.loopexit
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, bb+48(%rip)
	movdqa	%xmm0, bb+32(%rip)
	movdqa	%xmm0, bb+16(%rip)
	movdqa	%xmm0, bb(%rip)
	xorl	%ecx, %ecx
	movl	$238, %r10d
	movl	gg(%rip), %r9d
	xorl	%eax, %eax
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=1
	addl	%r9d, %edi
	movslq	%edi, %rax
	imulq	$-2139062143, %rax, %rcx # imm = 0x80808081
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$7, %ecx
	addl	%edx, %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	subl	%ecx, %edx
	subl	%edx, %eax
	cltq
	movl	alpha_to(,%rax,4), %ecx
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader.preheader.i
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	bb+56(%rip), %eax
	movl	%eax, bb+60(%rip)
	movl	bb+52(%rip), %eax
	movl	%eax, bb+56(%rip)
	movups	bb+36(%rip), %xmm0
	movups	%xmm0, bb+40(%rip)
	movups	bb+20(%rip), %xmm0
	movups	%xmm0, bb+24(%rip)
	movdqu	bb+4(%rip), %xmm0
	movdqu	%xmm0, bb+8(%rip)
	movl	%ecx, bb+4(%rip)
	xorl	%ecx, %ecx
.LBB1_22:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	%ecx, bb(%rip)
	testq	%r10, %r10
	jle	.LBB1_23
# BB#32:                                # %.loopexit._crit_edge.i
                                        #   in Loop: Header=BB1_16 Depth=1
	decq	%r10
	movl	bb+60(%rip), %eax
.LBB1_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_18 Depth 2
	xorl	data(,%r10,4), %eax
	cltq
	movl	index_of(,%rax,4), %edi
	cmpl	$-1, %edi
	je	.LBB1_34
# BB#17:                                # %.preheader29.i.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	$16, %ecx
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader29.i
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	gg-4(,%rcx,4), %esi
	cmpl	$-1, %esi
	movl	bb-8(,%rcx,4), %eax
	je	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_18 Depth=2
	addl	%edi, %esi
	movslq	%esi, %rsi
	imulq	$-2139062143, %rsi, %rdx # imm = 0x80808081
	shrq	$32, %rdx
	addl	%esi, %edx
	movl	%edx, %ebx
	shrl	$31, %ebx
	sarl	$7, %edx
	addl	%ebx, %edx
	movl	%edx, %ebx
	shll	$8, %ebx
	subl	%edx, %ebx
	subl	%ebx, %esi
	movslq	%esi, %rdx
	xorl	alpha_to(,%rdx,4), %eax
.LBB1_20:                               #   in Loop: Header=BB1_18 Depth=2
	movl	%eax, bb-4(,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB1_18
	jmp	.LBB1_21
.LBB1_23:                               # %vector.memcheck47
	cmpq	%r14, %r8
	jbe	.LBB1_31
# BB#24:                                # %vector.memcheck47
	leaq	188(%r14), %rax
	cmpq	%r15, %rax
	jbe	.LBB1_31
# BB#25:
	xorl	%eax, %eax
	jmp	.LBB1_26
.LBB1_31:                               # %vector.body36
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	%xmm0, (%r14)
	movups	%xmm1, 16(%r14)
	movups	32(%r15), %xmm0
	movups	48(%r15), %xmm1
	movups	%xmm0, 32(%r14)
	movups	%xmm1, 48(%r14)
	movups	64(%r15), %xmm0
	movups	80(%r15), %xmm1
	movups	%xmm0, 64(%r14)
	movups	%xmm1, 80(%r14)
	movups	96(%r15), %xmm0
	movups	112(%r15), %xmm1
	movups	%xmm0, 96(%r14)
	movups	%xmm1, 112(%r14)
	movdqu	128(%r15), %xmm0
	movdqu	144(%r15), %xmm1
	movdqu	%xmm0, 128(%r14)
	movdqu	%xmm1, 144(%r14)
	movl	$160, %eax
.LBB1_26:                               # %encode_rs.exit.prol.preheader
	leaq	(%r14,%rax), %rsi
	leaq	(%r15,%rax), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_27:                               # %encode_rs.exit.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rdx), %ebx
	movb	%bl, (%rsi,%rdx)
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB1_27
# BB#28:                                # %encode_rs.exit.preheader.new
	addq	%rdx, %rax
	.p2align	4, 0x90
.LBB1_29:                               # %encode_rs.exit
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rax), %edx
	movb	%dl, (%r14,%rax)
	movzbl	1(%r15,%rax), %edx
	movb	%dl, 1(%r14,%rax)
	movzbl	2(%r15,%rax), %edx
	movb	%dl, 2(%r14,%rax)
	movzbl	3(%r15,%rax), %edx
	movb	%dl, 3(%r14,%rax)
	movzbl	4(%r15,%rax), %edx
	movb	%dl, 4(%r14,%rax)
	movzbl	5(%r15,%rax), %edx
	movb	%dl, 5(%r14,%rax)
	movzbl	6(%r15,%rax), %edx
	movb	%dl, 6(%r14,%rax)
	movzbl	7(%r15,%rax), %edx
	movb	%dl, 7(%r14,%rax)
	addq	$8, %rax
	cmpq	$188, %rax
	jne	.LBB1_29
# BB#30:                                # %.preheader.preheader
	movb	%cl, 188(%r14)
	movb	bb+4(%rip), %al
	movb	%al, 189(%r14)
	movb	bb+8(%rip), %al
	movb	%al, 190(%r14)
	movb	bb+12(%rip), %al
	movb	%al, 191(%r14)
	movb	bb+16(%rip), %al
	movb	%al, 192(%r14)
	movb	bb+20(%rip), %al
	movb	%al, 193(%r14)
	movb	bb+24(%rip), %al
	movb	%al, 194(%r14)
	movb	bb+28(%rip), %al
	movb	%al, 195(%r14)
	movb	bb+32(%rip), %al
	movb	%al, 196(%r14)
	movb	bb+36(%rip), %al
	movb	%al, 197(%r14)
	movb	bb+40(%rip), %al
	movb	%al, 198(%r14)
	movb	bb+44(%rip), %al
	movb	%al, 199(%r14)
	movb	bb+48(%rip), %al
	movb	%al, 200(%r14)
	movb	bb+52(%rip), %al
	movb	%al, 201(%r14)
	movb	bb+56(%rip), %al
	movb	%al, 202(%r14)
	movb	bb+60(%rip), %al
	movb	%al, 203(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	rsenc_204, .Lfunc_end1-rsenc_204
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi25:
	.cfi_def_cfa_offset 480
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	xorl	%r13d, %r13d
	movq	%rsp, %r14
	leaq	208(%rsp), %r15
	movabsq	$-6872316419617283935, %rbp # imm = 0xA0A0A0A0A0A0A0A1
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #     Child Loop BB2_4 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	random
	movb	%al, 208(%rsp,%rbx)
	incq	%rbx
	cmpq	$188, %rbx
	jne	.LBB2_2
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	rsenc_204
	callq	random
	movq	%rax, %rbx
	andl	$127, %ebx
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	random
	movq	%rax, %r12
	callq	random
	movq	%rax, %rcx
	imulq	%rbp
	addq	%rcx, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$7, %rdx
	addq	%rax, %rdx
	imulq	$204, %rdx, %rax
	subq	%rax, %rcx
	movb	%r12b, (%rsp,%rcx)
	decl	%ebx
	jne	.LBB2_4
.LBB2_5:                                # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	rsdec_204
	incl	%r13d
	cmpl	$150000, %r13d          # imm = 0x249F0
	jne	.LBB2_1
# BB#6:
	xorl	%eax, %eax
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	gen_poly,@function
gen_poly:                               # @gen_poly
	.cfi_startproc
# BB#0:
	movabsq	$4294967298, %rax       # imm = 0x100000002
	movq	%rax, gg(%rip)
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
	movl	$1, gg(,%r8,4)
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %r9
	movslq	gg-4(,%rcx,4), %rdi
	testq	%rdi, %rdi
	movl	gg-8(,%rcx,4), %esi
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=2
	movl	index_of(,%rdi,4), %edi
	addl	%r8d, %edi
	movslq	%edi, %rdi
	imulq	$-2139062143, %rdi, %rax # imm = 0x80808081
	shrq	$32, %rax
	addl	%edi, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$7, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	shll	$8, %edx
	subl	%eax, %edx
	subl	%edx, %edi
	movslq	%edi, %rax
	xorl	alpha_to(,%rax,4), %esi
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=2
	movl	%esi, gg-4(,%rcx,4)
	cmpq	$1, %r9
	movq	%r9, %rcx
	jg	.LBB3_2
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	movslq	gg(%rip), %rax
	movl	index_of(,%rax,4), %eax
	addl	%r8d, %eax
	cltq
	imulq	$-2139062143, %rax, %rcx # imm = 0x80808081
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$7, %ecx
	addl	%edx, %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	subl	%ecx, %edx
	subl	%edx, %eax
	cltq
	movslq	alpha_to(,%rax,4), %rcx
	movl	%ecx, gg(%rip)
	incq	%r8
	cmpq	$17, %r8
	jne	.LBB3_1
# BB#6:                                 # %.preheader.preheader
	movl	index_of(,%rcx,4), %eax
	movl	%eax, gg(%rip)
	movslq	gg+4(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+4(%rip)
	movslq	gg+8(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+8(%rip)
	movslq	gg+12(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+12(%rip)
	movslq	gg+16(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+16(%rip)
	movslq	gg+20(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+20(%rip)
	movslq	gg+24(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+24(%rip)
	movslq	gg+28(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+28(%rip)
	movslq	gg+32(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+32(%rip)
	movslq	gg+36(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+36(%rip)
	movslq	gg+40(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+40(%rip)
	movslq	gg+44(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+44(%rip)
	movslq	gg+48(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+48(%rip)
	movslq	gg+52(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+52(%rip)
	movslq	gg+56(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+56(%rip)
	movslq	gg+60(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+60(%rip)
	movslq	gg+64(%rip), %rax
	movl	index_of(,%rax,4), %eax
	movl	%eax, gg+64(%rip)
	retq
.Lfunc_end3:
	.size	gen_poly, .Lfunc_end3-gen_poly
	.cfi_endproc

	.type	inited,@object          # @inited
	.local	inited
	.comm	inited,1,4
	.type	recd,@object            # @recd
	.local	recd
	.comm	recd,1020,16
	.type	index_of,@object        # @index_of
	.local	index_of
	.comm	index_of,1024,16
	.type	data,@object            # @data
	.local	data
	.comm	data,956,16
	.type	bb,@object              # @bb
	.local	bb
	.comm	bb,64,16
	.type	alpha_to,@object        # @alpha_to
	.local	alpha_to
	.comm	alpha_to,1024,16
	.type	gg,@object              # @gg
	.local	gg
	.comm	gg,68,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
