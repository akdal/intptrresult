	.text
	.file	"LzFind.bc"
	.globl	MatchFinder_GetPointerToCurrentPos
	.p2align	4, 0x90
	.type	MatchFinder_GetPointerToCurrentPos,@function
MatchFinder_GetPointerToCurrentPos:     # @MatchFinder_GetPointerToCurrentPos
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	retq
.Lfunc_end0:
	.size	MatchFinder_GetPointerToCurrentPos, .Lfunc_end0-MatchFinder_GetPointerToCurrentPos
	.cfi_endproc

	.globl	MatchFinder_GetIndexByte
	.p2align	4, 0x90
	.type	MatchFinder_GetIndexByte,@function
MatchFinder_GetIndexByte:               # @MatchFinder_GetIndexByte
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movslq	%esi, %rcx
	movb	(%rax,%rcx), %al
	retq
.Lfunc_end1:
	.size	MatchFinder_GetIndexByte, .Lfunc_end1-MatchFinder_GetIndexByte
	.cfi_endproc

	.globl	MatchFinder_GetNumAvailableBytes
	.p2align	4, 0x90
	.type	MatchFinder_GetNumAvailableBytes,@function
MatchFinder_GetNumAvailableBytes:       # @MatchFinder_GetNumAvailableBytes
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	subl	8(%rdi), %eax
	retq
.Lfunc_end2:
	.size	MatchFinder_GetNumAvailableBytes, .Lfunc_end2-MatchFinder_GetNumAvailableBytes
	.cfi_endproc

	.globl	MatchFinder_ReduceOffsets
	.p2align	4, 0x90
	.type	MatchFinder_ReduceOffsets,@function
MatchFinder_ReduceOffsets:              # @MatchFinder_ReduceOffsets
	.cfi_startproc
# BB#0:
	subl	%esi, 12(%rdi)
	subl	%esi, 8(%rdi)
	subl	%esi, 16(%rdi)
	retq
.Lfunc_end3:
	.size	MatchFinder_ReduceOffsets, .Lfunc_end3-MatchFinder_ReduceOffsets
	.cfi_endproc

	.globl	MatchFinder_MoveBlock
	.p2align	4, 0x90
	.type	MatchFinder_MoveBlock,@function
MatchFinder_MoveBlock:                  # @MatchFinder_MoveBlock
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	movq	(%rbx), %rsi
	movl	88(%rbx), %eax
	subq	%rax, %rsi
	movl	16(%rbx), %edx
	addl	%eax, %edx
	subl	8(%rbx), %edx
	callq	memmove
	movl	88(%rbx), %eax
	addq	64(%rbx), %rax
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	MatchFinder_MoveBlock, .Lfunc_end4-MatchFinder_MoveBlock
	.cfi_endproc

	.globl	MatchFinder_NeedMove
	.p2align	4, 0x90
	.type	MatchFinder_NeedMove,@function
MatchFinder_NeedMove:                   # @MatchFinder_NeedMove
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$0, 100(%rdi)
	jne	.LBB5_2
# BB#1:
	movl	84(%rdi), %ecx
	movl	92(%rdi), %edx
	addq	64(%rdi), %rcx
	subq	(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	setbe	%al
.LBB5_2:
	retq
.Lfunc_end5:
	.size	MatchFinder_NeedMove, .Lfunc_end5-MatchFinder_NeedMove
	.cfi_endproc

	.globl	MatchFinder_ReadIfRequired
	.p2align	4, 0x90
	.type	MatchFinder_ReadIfRequired,@function
MatchFinder_ReadIfRequired:             # @MatchFinder_ReadIfRequired
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 80(%rbx)
	jne	.LBB6_12
# BB#1:
	movl	8(%rbx), %eax
	movl	16(%rbx), %esi
	movl	%esi, %ecx
	subl	%eax, %ecx
	cmpl	%ecx, 92(%rbx)
	jb	.LBB6_12
# BB#2:
	cmpl	$0, 136(%rbx)
	je	.LBB6_3
.LBB6_12:                               # %MatchFinder_ReadBlock.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_3:
	cmpl	$0, 100(%rbx)
	je	.LBB6_4
# BB#9:
	movl	%esi, %eax
	notl	%eax
	movq	104(%rbx), %rcx
	cmpq	%rcx, %rax
	movl	%ecx, %edx
	cmovbel	%eax, %edx
	subq	%rdx, %rcx
	movq	%rcx, 104(%rbx)
	addl	%esi, %edx
	movl	%edx, 16(%rbx)
	testq	%rcx, %rcx
	jne	.LBB6_12
# BB#10:
	movl	$1, 80(%rbx)
	jmp	.LBB6_12
.LBB6_4:
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	subl	%eax, %esi
	addq	(%rbx), %rsi
	movl	84(%rbx), %eax
	addq	64(%rbx), %rax
	subq	%rsi, %rax
	movq	%rax, (%rsp)
	je	.LBB6_12
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	movq	72(%rbx), %rdi
	movq	%r14, %rdx
	callq	*(%rdi)
	movl	%eax, 136(%rbx)
	testl	%eax, %eax
	jne	.LBB6_12
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB6_8
# BB#11:                                #   in Loop: Header=BB6_5 Depth=1
	addl	16(%rbx), %esi
	movl	%esi, 16(%rbx)
	movl	8(%rbx), %eax
	movl	%esi, %ecx
	subl	%eax, %ecx
	cmpl	92(%rbx), %ecx
	jbe	.LBB6_5
	jmp	.LBB6_12
.LBB6_8:                                # %.critedge.i
	movl	$1, 80(%rbx)
	jmp	.LBB6_12
.Lfunc_end6:
	.size	MatchFinder_ReadIfRequired, .Lfunc_end6-MatchFinder_ReadIfRequired
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI7_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI7_2:
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
.LCPI7_3:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.text
	.globl	MatchFinder_Construct
	.p2align	4, 0x90
	.type	MatchFinder_Construct,@function
MatchFinder_Construct:                  # @MatchFinder_Construct
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$0, 64(%rdi)
	movl	$0, 100(%rdi)
	movq	$0, 40(%rdi)
	movl	$32, 60(%rdi)
	movl	$1, 112(%rdi)
	movl	$4, 96(%rdi)
	movl	$0, 116(%rdi)
	movdqa	.LCPI7_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	xorl	%eax, %eax
	movdqa	.LCPI7_1(%rip), %xmm1   # xmm1 = [1,1,1,1]
	movdqa	.LCPI7_2(%rip), %xmm2   # xmm2 = [3988292384,3988292384,3988292384,3988292384]
	movdqa	.LCPI7_3(%rip), %xmm3   # xmm3 = [4,4,4,4]
	.p2align	4, 0x90
.LBB7_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	psrld	$1, %xmm4
	movdqa	%xmm0, %xmm5
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrld	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubd	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrld	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrld	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubd	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrld	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	movdqu	%xmm5, 140(%rdi,%rax,4)
	addq	$4, %rax
	paddd	%xmm3, %xmm0
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB7_1
# BB#2:                                 # %middle.block
	retq
.Lfunc_end7:
	.size	MatchFinder_Construct, .Lfunc_end7-MatchFinder_Construct
	.cfi_endproc

	.globl	MatchFinder_Free
	.p2align	4, 0x90
	.type	MatchFinder_Free,@function
MatchFinder_Free:                       # @MatchFinder_Free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	40(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 40(%rbx)
	cmpl	$0, 100(%rbx)
	jne	.LBB8_2
# BB#1:
	movq	64(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 64(%rbx)
.LBB8_2:                                # %LzInWindow_Free.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	MatchFinder_Free, .Lfunc_end8-MatchFinder_Free
	.cfi_endproc

	.globl	MatchFinder_Create
	.p2align	4, 0x90
	.type	MatchFinder_Create,@function
MatchFinder_Create:                     # @MatchFinder_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%ecx, %r12d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r15d
	movq	%rdi, %rbx
	cmpl	$-1073741823, %r15d     # imm = 0xC0000001
	jb	.LBB9_3
# BB#1:
	movq	40(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 40(%rbx)
	xorl	%r15d, %r15d
	cmpl	$0, 100(%rbx)
	jne	.LBB9_23
# BB#2:
	movq	64(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	jmp	.LBB9_22
.LBB9_3:
	cmpl	$-2147483648, %r15d     # imm = 0x80000000
	seta	%cl
	incb	%cl
	movl	%r15d, %eax
	shrl	%cl, %eax
	leal	(%r12,%rdx), %ecx
	addl	%r8d, %ecx
	shrl	%ecx
	leal	1(%r15,%rdx), %edx
	movl	%edx, 88(%rbx)
	addl	%r12d, %r8d
	movl	%r8d, 92(%rbx)
	addl	%edx, %eax
	addl	%r8d, %eax
	leal	524288(%rcx,%rax), %ebp
	cmpl	$0, 100(%rbx)
	je	.LBB9_7
# BB#4:                                 # %LzInWindow_Create.exit.thread
	movl	%ebp, 84(%rbx)
	jmp	.LBB9_5
.LBB9_7:
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB9_9
# BB#8:
	cmpl	%ebp, 84(%rbx)
	je	.LBB9_5
.LBB9_9:                                # %LzInWindow_Create.exit
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 64(%rbx)
	movl	%ebp, 84(%rbx)
	movl	%ebp, %esi
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB9_10
.LBB9_5:                                # %LzInWindow_Create.exit.thread96
	leal	1(%r15), %ebp
	movl	%r12d, 32(%rbx)
	movl	$0, 124(%rbx)
	movl	96(%rbx), %ecx
	cmpl	$2, %ecx
	jne	.LBB9_11
# BB#6:                                 # %.thread107
	movl	$65535, 56(%rbx)        # imm = 0xFFFF
	xorl	%edx, %edx
	movl	$65536, %eax            # imm = 0x10000
	jmp	.LBB9_16
.LBB9_11:
	leal	-1(%r15), %eax
	movl	%eax, %edx
	shrl	%edx
	orl	%eax, %edx
	movl	%edx, %eax
	shrl	$2, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	shrl	$4, %edx
	orl	%eax, %edx
	movl	%edx, %eax
	shrl	$8, %eax
	orl	%edx, %eax
	shrl	%eax
	orl	$65535, %eax            # imm = 0xFFFF
	cmpl	$16777217, %eax         # imm = 0x1000001
	jb	.LBB9_13
# BB#12:
	shrl	%eax
	cmpl	$3, %ecx
	movl	$16777215, %edx         # imm = 0xFFFFFF
	cmovel	%edx, %eax
.LBB9_13:
	movl	%eax, 56(%rbx)
	incl	%eax
	xorl	%edx, %edx
	cmpl	$3, %ecx
	jb	.LBB9_16
# BB#14:
	movl	$1024, 124(%rbx)        # imm = 0x400
	movl	$1024, %edx             # imm = 0x400
	je	.LBB9_16
# BB#15:                                # %.thread
	cmpl	$4, %ecx
	movl	$1115136, %ecx          # imm = 0x110400
	movl	$66560, %edx            # imm = 0x10400
	cmoval	%ecx, %edx
	movl	%edx, 124(%rbx)
.LBB9_16:                               # %.thread98
	addl	%edx, %eax
	movl	132(%rbx), %edx
	addl	128(%rbx), %edx
	movl	%r15d, 120(%rbx)
	movl	%eax, 128(%rbx)
	movl	%ebp, 28(%rbx)
	cmpl	$0, 112(%rbx)
	setne	%cl
	shll	%cl, %ebp
	movl	%ebp, 132(%rbx)
	addl	%eax, %ebp
	movq	40(%rbx), %rsi
	movl	$1, %r15d
	testq	%rsi, %rsi
	je	.LBB9_18
# BB#17:                                # %.thread98
	cmpl	%ebp, %edx
	je	.LBB9_23
.LBB9_18:
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 40(%rbx)
	movl	%ebp, %esi
	shlq	$2, %rsi
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.LBB9_19
# BB#24:
	movl	128(%rbx), %ecx
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 48(%rbx)
	jmp	.LBB9_23
.LBB9_19:
	leaq	8(%r14), %rbp
	xorl	%esi, %esi
	jmp	.LBB9_20
.LBB9_10:                               # %LzInWindow_Create.exit._crit_edge
	leaq	8(%r14), %rbp
	movq	40(%rbx), %rsi
.LBB9_20:
	movq	%r14, %rdi
	callq	*(%rbp)
	movq	$0, 40(%rbx)
	xorl	%r15d, %r15d
	cmpl	$0, 100(%rbx)
	jne	.LBB9_23
# BB#21:
	movq	64(%rbx), %rsi
	movq	%r14, %rdi
	callq	*(%rbp)
.LBB9_22:                               # %MatchFinder_Free.exit
	movq	$0, 64(%rbx)
.LBB9_23:                               # %MatchFinder_Free.exit
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	MatchFinder_Create, .Lfunc_end9-MatchFinder_Create
	.cfi_endproc

	.globl	MatchFinder_Init
	.p2align	4, 0x90
	.type	MatchFinder_Init,@function
MatchFinder_Init:                       # @MatchFinder_Init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 128(%rbx)
	je	.LBB10_3
# BB#1:                                 # %.lr.ph
	movq	40(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movl	$0, (%rax,%rdx,4)
	incl	%ecx
	cmpl	128(%rbx), %ecx
	jb	.LBB10_2
.LBB10_3:                               # %._crit_edge
	movl	$0, 24(%rbx)
	movq	64(%rbx), %rsi
	movq	%rsi, (%rbx)
	movl	28(%rbx), %eax
	movl	%eax, 16(%rbx)
	movl	%eax, 8(%rbx)
	movl	$0, 136(%rbx)
	movl	$0, 80(%rbx)
	cmpl	$0, 100(%rbx)
	je	.LBB10_6
# BB#4:
	movl	%eax, %ecx
	notl	%ecx
	movq	104(%rbx), %rdx
	cmpq	%rdx, %rcx
	movl	%edx, %esi
	cmovbel	%ecx, %esi
	subq	%rsi, %rdx
	movq	%rdx, 104(%rbx)
	addl	%eax, %esi
	movl	%esi, 16(%rbx)
	testq	%rdx, %rdx
	jne	.LBB10_13
	jmp	.LBB10_12
.LBB10_6:                               # %.preheader.i
	movl	84(%rbx), %eax
	testq	%rax, %rax
	movq	%rax, (%rsp)
	je	.LBB10_13
# BB#7:                                 # %.lr.ph19.preheader
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph19
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%rbx), %rdi
	movq	%r14, %rdx
	callq	*(%rdi)
	movl	%eax, 136(%rbx)
	testl	%eax, %eax
	jne	.LBB10_13
# BB#9:                                 #   in Loop: Header=BB10_8 Depth=1
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB10_12
# BB#10:                                #   in Loop: Header=BB10_8 Depth=1
	addl	16(%rbx), %esi
	movl	%esi, 16(%rbx)
	subl	8(%rbx), %esi
	cmpl	92(%rbx), %esi
	ja	.LBB10_13
# BB#11:                                # %._crit_edge17
                                        #   in Loop: Header=BB10_8 Depth=1
	addq	(%rbx), %rsi
	movl	84(%rbx), %eax
	addq	64(%rbx), %rax
	subq	%rsi, %rax
	movq	%rax, (%rsp)
	jne	.LBB10_8
	jmp	.LBB10_13
.LBB10_12:                              # %.critedge.i
	movl	$1, 80(%rbx)
.LBB10_13:                              # %MatchFinder_ReadBlock.exit
	movl	8(%rbx), %eax
	movl	16(%rbx), %ecx
	movl	%eax, %edx
	notl	%edx
	movl	28(%rbx), %esi
	subl	24(%rbx), %esi
	cmpl	%edx, %esi
	cmovael	%edx, %esi
	xorl	%edx, %edx
	subl	%eax, %ecx
	setne	%dl
	movl	%ecx, %edi
	subl	92(%rbx), %edi
	cmoval	%edi, %edx
	cmpl	%esi, %edx
	cmovael	%esi, %edx
	movl	32(%rbx), %esi
	cmpl	%esi, %ecx
	cmoval	%esi, %ecx
	movl	%ecx, 20(%rbx)
	addl	%eax, %edx
	movl	%edx, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	MatchFinder_Init, .Lfunc_end10-MatchFinder_Init
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	MatchFinder_Normalize3
	.p2align	4, 0x90
	.type	MatchFinder_Normalize3,@function
MatchFinder_Normalize3:                 # @MatchFinder_Normalize3
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	je	.LBB11_10
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %eax
	xorl	%r9d, %r9d
	cmpl	$8, %edx
	jb	.LBB11_7
# BB#3:                                 # %min.iters.checked
	andl	$7, %edx
	movq	%rax, %r8
	subq	%rdx, %r8
	je	.LBB11_7
# BB#4:                                 # %vector.ph
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rsi), %rcx
	movdqa	.LCPI11_0(%rip), %xmm1  # xmm1 = [2147483648,2147483648,2147483648,2147483648]
	movdqa	%xmm0, %xmm2
	pxor	%xmm1, %xmm2
	movq	%r8, %r10
	.p2align	4, 0x90
.LBB11_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm3
	movdqu	(%rcx), %xmm4
	movdqa	%xmm3, %xmm5
	pxor	%xmm1, %xmm5
	pcmpgtd	%xmm2, %xmm5
	movdqa	%xmm4, %xmm6
	pxor	%xmm1, %xmm6
	pcmpgtd	%xmm2, %xmm6
	psubd	%xmm0, %xmm3
	psubd	%xmm0, %xmm4
	pand	%xmm5, %xmm3
	pand	%xmm6, %xmm4
	movdqu	%xmm3, -16(%rcx)
	movdqu	%xmm4, (%rcx)
	addq	$32, %rcx
	addq	$-8, %r10
	jne	.LBB11_5
# BB#6:                                 # %middle.block
	testl	%edx, %edx
	jne	.LBB11_8
	jmp	.LBB11_10
.LBB11_7:
	xorl	%r8d, %r8d
.LBB11_8:                               # %.lr.ph.preheader20
	leaq	(%rsi,%r8,4), %rcx
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	subl	%edi, %edx
	cmovbel	%r9d, %edx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	decq	%rax
	jne	.LBB11_9
.LBB11_10:                              # %._crit_edge
	retq
.Lfunc_end11:
	.size	MatchFinder_Normalize3, .Lfunc_end11-MatchFinder_Normalize3
	.cfi_endproc

	.globl	GetMatchesSpec1
	.p2align	4, 0x90
	.type	GetMatchesSpec1,@function
GetMatchesSpec1:                        # @GetMatchesSpec1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%rcx, %r12
	movl	%edi, %r14d
	movq	72(%rsp), %rax
	movl	64(%rsp), %r15d
	movq	%r9, -8(%rsp)           # 8-byte Spill
	leal	(%r9,%r9), %ecx
	leaq	(%r8,%rcx,4), %rdi
	movq	%r8, -16(%rsp)          # 8-byte Spill
	leaq	4(%r8,%rcx,4), %rcx
	testl	%r15d, %r15d
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	movq	%rax, -40(%rsp)         # 8-byte Spill
	je	.LBB12_20
# BB#1:
	movl	56(%rsp), %ebp
	movl	%edx, %r9d
	subl	%esi, %r9d
	cmpl	%ebp, %r9d
	jae	.LBB12_20
# BB#2:                                 # %.lr.ph.preheader
	movl	80(%rsp), %eax
	movl	%eax, -52(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movl	%edx, -20(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_7 Depth 2
	movq	-8(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r9d, %ecx
	movl	$0, %edi
	movl	%ebp, %r8d
	cmovbl	%ebp, %edi
	addl	%ecx, %edi
	addl	%edi, %edi
	movq	-16(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rbp
	movl	%r9d, %edi
	movq	%r12, %r10
	subq	%rdi, %r10
	cmpl	%eax, %r13d
	movl	%eax, %r11d
	cmovbl	%r13d, %r11d
	movb	(%r10,%r11), %bl
	cmpb	(%r12,%r11), %bl
	jne	.LBB12_15
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	incl	%r11d
	cmpl	%r14d, %r11d
	movl	%r14d, %edi
	je	.LBB12_11
# BB#5:                                 #   in Loop: Header=BB12_3 Depth=1
	movl	%r11d, %edi
	movb	(%r10,%rdi), %bl
	cmpb	(%r12,%rdi), %bl
	movl	%r11d, %edi
	jne	.LBB12_11
# BB#6:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	%r13d, %edi
	notl	%edi
	movl	%eax, %ebx
	notl	%ebx
	cmpl	%ebx, %edi
	cmoval	%edi, %ebx
	movl	$1, %edi
	subl	%ebx, %edi
	.p2align	4, 0x90
.LBB12_7:                               # %.preheader
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%edi, %r14d
	je	.LBB12_8
# BB#9:                                 #   in Loop: Header=BB12_7 Depth=2
	movl	%edi, %ebx
	movzbl	(%r10,%rbx), %ecx
	incl	%edi
	cmpb	(%r12,%rbx), %cl
	je	.LBB12_7
# BB#10:                                # %..loopexit.loopexit_crit_edge
                                        #   in Loop: Header=BB12_3 Depth=1
	decl	%edi
	cmpl	%edi, -52(%rsp)         # 4-byte Folded Reload
	jb	.LBB12_12
	jmp	.LBB12_14
.LBB12_8:                               #   in Loop: Header=BB12_3 Depth=1
	movl	%r14d, %edi
	.p2align	4, 0x90
.LBB12_11:                              # %.loopexit
                                        #   in Loop: Header=BB12_3 Depth=1
	cmpl	%edi, -52(%rsp)         # 4-byte Folded Reload
	jae	.LBB12_14
.LBB12_12:                              #   in Loop: Header=BB12_3 Depth=1
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movl	%edi, (%rcx)
	decl	%r9d
	movl	%r9d, 4(%rcx)
	addq	$8, %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	cmpl	%r14d, %edi
	je	.LBB12_21
# BB#13:                                #   in Loop: Header=BB12_3 Depth=1
	movl	%edi, -52(%rsp)         # 4-byte Spill
.LBB12_14:                              #   in Loop: Header=BB12_3 Depth=1
	movl	%edi, %r11d
.LBB12_15:                              #   in Loop: Header=BB12_3 Depth=1
	decl	%r15d
	movl	%r11d, %ecx
	movb	(%r10,%rcx), %dl
	cmpb	(%r12,%rcx), %dl
	jae	.LBB12_17
# BB#16:                                #   in Loop: Header=BB12_3 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	%esi, (%rcx)
	addq	$4, %rbp
	movq	%rbp, -48(%rsp)         # 8-byte Spill
	movl	%r11d, %eax
	testl	%r15d, %r15d
	jne	.LBB12_19
	jmp	.LBB12_20
	.p2align	4, 0x90
.LBB12_17:                              #   in Loop: Header=BB12_3 Depth=1
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	%esi, (%rcx)
	movq	%rbp, -32(%rsp)         # 8-byte Spill
	movl	%r11d, %r13d
	testl	%r15d, %r15d
	je	.LBB12_20
.LBB12_19:                              # %.thread119
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	(%rbp), %esi
	movl	-20(%rsp), %edx         # 4-byte Reload
	movl	%edx, %r9d
	subl	%esi, %r9d
	movl	%r8d, %ebp
	cmpl	%ebp, %r9d
	jb	.LBB12_3
.LBB12_20:                              # %.thread
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx)
	xorl	%ecx, %ecx
.LBB12_22:
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx)
	movq	-40(%rsp), %rax         # 8-byte Reload
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_21:
	movl	(%rbp), %ecx
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx)
	movl	4(%rbp), %ecx
	jmp	.LBB12_22
.Lfunc_end12:
	.size	GetMatchesSpec1, .Lfunc_end12-GetMatchesSpec1
	.cfi_endproc

	.globl	Bt3Zip_MatchFinder_GetMatches
	.p2align	4, 0x90
	.type	Bt3Zip_MatchFinder_GetMatches,@function
Bt3Zip_MatchFinder_GetMatches:          # @Bt3Zip_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	20(%rbx), %edi
	cmpl	$2, %edi
	ja	.LBB13_2
# BB#1:
	incl	24(%rbx)
	incq	(%rbx)
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	xorl	%r15d, %r15d
	cmpl	12(%rbx), %eax
	je	.LBB13_4
	jmp	.LBB13_5
.LBB13_2:
	movq	(%rbx), %rcx
	movzbl	2(%rcx), %eax
	movzbl	(%rcx), %edx
	shll	$8, %edx
	orl	%eax, %edx
	movzbl	1(%rcx), %eax
	movzwl	140(%rbx,%rax,4), %eax
	xorl	%edx, %eax
	movq	40(%rbx), %r8
	movl	(%r8,%rax,4), %esi
	movl	8(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	movq	48(%rbx), %r8
	movl	8(%rbx), %edx
	movl	24(%rbx), %r9d
	movl	28(%rbx), %r10d
	movl	60(%rbx), %eax
	pushq	$2
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	GetMatchesSpec1
	addq	$32, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	subq	%r14, %r15
	shrq	$2, %r15
	incl	24(%rbx)
	incq	(%rbx)
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	cmpl	12(%rbx), %eax
	jne	.LBB13_5
.LBB13_4:
	movq	%rbx, %rdi
	callq	MatchFinder_CheckLimits
.LBB13_5:                               # %MatchFinder_MovePos.exit
	movl	%r15d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	Bt3Zip_MatchFinder_GetMatches, .Lfunc_end13-Bt3Zip_MatchFinder_GetMatches
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.p2align	4, 0x90
	.type	MatchFinder_CheckLimits,@function
MatchFinder_CheckLimits:                # @MatchFinder_CheckLimits
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %ecx
	cmpl	$-1, %ecx
	jne	.LBB14_14
# BB#1:
	movl	$-2, %eax
	subl	120(%rbx), %eax
	andl	$-1024, %eax            # imm = 0xFC00
	movl	132(%rbx), %r10d
	addl	128(%rbx), %r10d
	je	.LBB14_4
# BB#2:                                 # %.lr.ph.preheader.i.i
	movq	40(%rbx), %r8
	movl	%r10d, %ecx
	xorl	%edx, %edx
	cmpl	$8, %r10d
	jb	.LBB14_9
# BB#5:                                 # %min.iters.checked
	andl	$7, %r10d
	movq	%rcx, %r9
	subq	%r10, %r9
	je	.LBB14_9
# BB#6:                                 # %vector.ph
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rsi
	movdqa	.LCPI14_0(%rip), %xmm1  # xmm1 = [2147483648,2147483648,2147483648,2147483648]
	movdqa	%xmm0, %xmm2
	pxor	%xmm1, %xmm2
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB14_7:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm3
	movdqu	(%rsi), %xmm4
	movdqa	%xmm3, %xmm5
	pxor	%xmm1, %xmm5
	pcmpgtd	%xmm2, %xmm5
	movdqa	%xmm4, %xmm6
	pxor	%xmm1, %xmm6
	pcmpgtd	%xmm2, %xmm6
	psubd	%xmm0, %xmm3
	psubd	%xmm0, %xmm4
	pand	%xmm5, %xmm3
	pand	%xmm6, %xmm4
	movdqu	%xmm3, -16(%rsi)
	movdqu	%xmm4, (%rsi)
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB14_7
# BB#8:                                 # %middle.block
	testl	%r10d, %r10d
	jne	.LBB14_10
	jmp	.LBB14_12
.LBB14_9:
	xorl	%r9d, %r9d
.LBB14_10:                              # %.lr.ph.i.i.preheader
	subq	%r9, %rcx
	leaq	(%r8,%r9,4), %rsi
	.p2align	4, 0x90
.LBB14_11:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edi
	subl	%eax, %edi
	cmovbel	%edx, %edi
	movl	%edi, (%rsi)
	addq	$4, %rsi
	decq	%rcx
	jne	.LBB14_11
.LBB14_12:                              # %MatchFinder_Normalize3.exit.loopexit.i
	movl	8(%rbx), %ecx
	jmp	.LBB14_13
.LBB14_4:
	movl	$-1, %ecx
.LBB14_13:                              # %MatchFinder_Normalize.exit
	subl	%eax, 12(%rbx)
	subl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	subl	%eax, 16(%rbx)
.LBB14_14:
	cmpl	$0, 80(%rbx)
	jne	.LBB14_20
# BB#15:
	movl	16(%rbx), %edx
	movl	92(%rbx), %eax
	subl	%ecx, %edx
	cmpl	%edx, %eax
	jne	.LBB14_20
# BB#16:
	cmpl	$0, 100(%rbx)
	jne	.LBB14_19
# BB#17:                                # %MatchFinder_NeedMove.exit.i
	movq	(%rbx), %rsi
	movq	64(%rbx), %rdi
	movl	84(%rbx), %ecx
	addq	%rdi, %rcx
	subq	%rsi, %rcx
	cmpq	%rax, %rcx
	ja	.LBB14_19
# BB#18:                                # %MatchFinder_NeedMove.exit.thread.i
	movl	88(%rbx), %ecx
	subq	%rcx, %rsi
	leal	(%rcx,%rax), %edx
	callq	memmove
	movl	88(%rbx), %eax
	addq	64(%rbx), %rax
	movq	%rax, (%rbx)
	cmpl	$0, 80(%rbx)
	jne	.LBB14_20
.LBB14_19:                              # %MatchFinder_NeedMove.exit.thread.i.thread
	cmpl	$0, 136(%rbx)
	je	.LBB14_23
.LBB14_20:                              # %MatchFinder_CheckAndMoveAndRead.exit
	movl	24(%rbx), %ecx
	movl	28(%rbx), %eax
	cmpl	%eax, %ecx
	jne	.LBB14_22
# BB#21:
	movl	$0, 24(%rbx)
	xorl	%ecx, %ecx
.LBB14_22:
	movl	8(%rbx), %edx
	movl	16(%rbx), %esi
	movl	%edx, %edi
	notl	%edi
	subl	%ecx, %eax
	cmpl	%edi, %eax
	cmovael	%edi, %eax
	xorl	%ecx, %ecx
	subl	%edx, %esi
	setne	%cl
	movl	%esi, %edi
	subl	92(%rbx), %edi
	cmoval	%edi, %ecx
	cmpl	%eax, %ecx
	cmovael	%eax, %ecx
	movl	32(%rbx), %eax
	cmpl	%eax, %esi
	cmoval	%eax, %esi
	movl	%esi, 20(%rbx)
	addl	%edx, %ecx
	movl	%ecx, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB14_23:
	cmpl	$0, 100(%rbx)
	je	.LBB14_26
# BB#24:
	movl	16(%rbx), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	xorq	%rax, %rdx
	movq	104(%rbx), %rsi
	cmpq	%rsi, %rdx
	movl	%esi, %edx
	cmovbel	%ecx, %edx
	subq	%rdx, %rsi
	movq	%rsi, 104(%rbx)
	addl	%eax, %edx
	movl	%edx, 16(%rbx)
	testq	%rsi, %rsi
	jne	.LBB14_20
# BB#25:
	movl	$1, 80(%rbx)
	jmp	.LBB14_20
.LBB14_26:                              # %.preheader.i.i
	movl	8(%rbx), %eax
	movl	16(%rbx), %esi
	movq	%rsp, %r14
.LBB14_27:                              # =>This Inner Loop Header: Depth=1
	subl	%eax, %esi
	addq	(%rbx), %rsi
	movl	84(%rbx), %eax
	addq	64(%rbx), %rax
	subq	%rsi, %rax
	movq	%rax, (%rsp)
	je	.LBB14_20
# BB#28:                                #   in Loop: Header=BB14_27 Depth=1
	movq	72(%rbx), %rdi
	movq	%r14, %rdx
	callq	*(%rdi)
	movl	%eax, 136(%rbx)
	testl	%eax, %eax
	jne	.LBB14_20
# BB#29:                                #   in Loop: Header=BB14_27 Depth=1
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB14_31
# BB#30:                                #   in Loop: Header=BB14_27 Depth=1
	addl	16(%rbx), %esi
	movl	%esi, 16(%rbx)
	movl	8(%rbx), %eax
	movl	%esi, %ecx
	subl	%eax, %ecx
	cmpl	92(%rbx), %ecx
	jbe	.LBB14_27
	jmp	.LBB14_20
.LBB14_31:                              # %.critedge.i.i
	movl	$1, 80(%rbx)
	jmp	.LBB14_20
.Lfunc_end14:
	.size	MatchFinder_CheckLimits, .Lfunc_end14-MatchFinder_CheckLimits
	.cfi_endproc

	.globl	Hc3Zip_MatchFinder_GetMatches
	.p2align	4, 0x90
	.type	Hc3Zip_MatchFinder_GetMatches,@function
Hc3Zip_MatchFinder_GetMatches:          # @Hc3Zip_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 96
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	20(%rdi), %r14d
	cmpl	$2, %r14d
	ja	.LBB15_2
# BB#1:
	incl	24(%rdi)
	incq	(%rdi)
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	xorl	%r13d, %r13d
	cmpl	12(%rdi), %eax
	je	.LBB15_20
	jmp	.LBB15_21
.LBB15_2:
	movq	(%rdi), %r15
	movzbl	2(%r15), %eax
	movzbl	(%r15), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	leaq	1(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	1(%r15), %eax
	movzwl	140(%rdi,%rax,4), %eax
	xorl	%ecx, %eax
	movq	40(%rdi), %rcx
	movl	(%rcx,%rax,4), %ebp
	movl	8(%rdi), %edx
	movl	%edx, (%rcx,%rax,4)
	movl	8(%rdi), %r9d
	movl	24(%rdi), %r10d
	movq	48(%rdi), %r11
	movl	28(%rdi), %r12d
	movl	60(%rdi), %eax
	movl	%ebp, (%r11,%r10,4)
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
	movq	%rsi, %r13
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
.LBB15_3:                               # %.thread69.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_6 Depth 2
                                        #       Child Loop BB15_9 Depth 3
	testl	%eax, %eax
	je	.LBB15_18
# BB#4:                                 # %.thread69.outer.i
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	%r9d, %ebx
	subl	%ebp, %ebx
	cmpl	%r12d, %ebx
	jae	.LBB15_18
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movl	4(%rsp), %r13d          # 4-byte Reload
	movb	(%r15,%r13), %r8b
	.p2align	4, 0x90
.LBB15_6:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_9 Depth 3
	decl	%eax
	movl	%ebx, %ecx
	movq	%r15, %rdx
	subq	%rcx, %rdx
	movl	%r10d, %ecx
	subl	%ebx, %ecx
	movl	$0, %ebp
	cmovbl	%r12d, %ebp
	addl	%ecx, %ebp
	movl	(%r11,%rbp,4), %ebp
	cmpb	%r8b, (%rdx,%r13)
	jne	.LBB15_11
# BB#7:                                 #   in Loop: Header=BB15_6 Depth=2
	movb	(%rdx), %cl
	cmpb	(%r15), %cl
	jne	.LBB15_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB15_6 Depth=2
	movq	%r11, %rdi
	movq	%r10, %r11
	movl	%r9d, %r10d
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB15_9:                               # %.preheader.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, %r14d
	je	.LBB15_10
# BB#14:                                #   in Loop: Header=BB15_9 Depth=3
	movl	%ecx, %esi
	movzbl	(%rdx,%rsi), %r9d
	incl	%ecx
	cmpb	(%r15,%rsi), %r9b
	je	.LBB15_9
# BB#15:                                # %._crit_edge
                                        #   in Loop: Header=BB15_6 Depth=2
	decl	%ecx
	xorl	%edx, %edx
	jmp	.LBB15_16
.LBB15_10:                              #   in Loop: Header=BB15_6 Depth=2
	movb	$1, %dl
	movl	%r14d, %ecx
.LBB15_16:                              #   in Loop: Header=BB15_6 Depth=2
	movl	%r10d, %r9d
	movq	%r11, %r10
	movq	%rdi, %r11
	cmpl	%ecx, 4(%rsp)           # 4-byte Folded Reload
	jb	.LBB15_17
.LBB15_11:                              # %.thread69.backedge.i
                                        #   in Loop: Header=BB15_6 Depth=2
	testl	%eax, %eax
	je	.LBB15_12
# BB#13:                                # %.thread69.backedge.i
                                        #   in Loop: Header=BB15_6 Depth=2
	movl	%r9d, %ebx
	subl	%ebp, %ebx
	cmpl	%r12d, %ebx
	jb	.LBB15_6
	jmp	.LBB15_12
.LBB15_17:                              #   in Loop: Header=BB15_3 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	%ecx, (%r13)
	decl	%ebx
	movl	%ebx, 4(%r13)
	addq	$8, %r13
	testb	%dl, %dl
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB15_3
	jmp	.LBB15_18
.LBB15_12:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB15_18:                              # %Hc_GetMatchesSpec.exit
	subq	%rsi, %r13
	shrq	$2, %r13
	incl	24(%rdi)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rdi)
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	cmpl	12(%rdi), %eax
	jne	.LBB15_21
.LBB15_20:
	callq	MatchFinder_CheckLimits
.LBB15_21:                              # %MatchFinder_MovePos.exit
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	Hc3Zip_MatchFinder_GetMatches, .Lfunc_end15-Hc3Zip_MatchFinder_GetMatches
	.cfi_endproc

	.globl	Bt3Zip_MatchFinder_Skip
	.p2align	4, 0x90
	.type	Bt3Zip_MatchFinder_Skip,@function
Bt3Zip_MatchFinder_Skip:                # @Bt3Zip_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 112
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	.p2align	4, 0x90
.LBB16_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_6 Depth 2
                                        #       Child Loop BB16_8 Depth 3
	movl	20(%r12), %ecx
	cmpl	$2, %ecx
	ja	.LBB16_3
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	incl	24(%r12)
	incq	(%r12)
	jmp	.LBB16_19
	.p2align	4, 0x90
.LBB16_3:                               #   in Loop: Header=BB16_1 Depth=1
	movq	(%r12), %rdx
	movzbl	2(%rdx), %eax
	movzbl	(%rdx), %esi
	shll	$8, %esi
	orl	%eax, %esi
	leaq	1(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	1(%rdx), %eax
	movzwl	140(%r12,%rax,4), %eax
	xorl	%esi, %eax
	movq	40(%r12), %rsi
	movl	(%rsi,%rax,4), %r13d
	movl	8(%r12), %ebp
	movl	%ebp, (%rsi,%rax,4)
	movl	8(%r12), %ebp
	movl	24(%r12), %eax
	movq	48(%r12), %rdi
	movl	28(%r12), %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%rdi,%rax,4), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	leaq	4(%rdi,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	%ebx, %eax
	jae	.LBB16_16
# BB#4:                                 #   in Loop: Header=BB16_1 Depth=1
	movl	60(%r12), %r10d
	testl	%r10d, %r10d
	je	.LBB16_16
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB16_1 Depth=1
	xorl	%edi, %edi
	xorl	%r11d, %r11d
	movl	%ebx, 24(%rsp)          # 4-byte Spill
.LBB16_6:                               # %.lr.ph.i
                                        #   Parent Loop BB16_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_8 Depth 3
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %ebp
	subl	%eax, %ebp
	movl	$0, %esi
	cmovbl	%ebx, %esi
	addl	%ebp, %esi
	addl	%esi, %esi
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rsi,4), %r9
	movl	%eax, %esi
	movq	%rdx, %rax
	subq	%rsi, %rax
	cmpl	%edi, %r11d
	movl	%edi, %ebx
	movl	%edi, %r8d
	movl	%r11d, %ebp
	cmovbl	%r11d, %r8d
	movb	(%rax,%r8), %r11b
	movb	(%rdx,%r8), %r14b
	cmpb	%r14b, %r11b
	jne	.LBB16_11
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB16_6 Depth=2
	incl	%r8d
	.p2align	4, 0x90
.LBB16_8:                               # %.preheader.i
                                        #   Parent Loop BB16_1 Depth=1
                                        #     Parent Loop BB16_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r8d, %ecx
	je	.LBB16_17
# BB#9:                                 #   in Loop: Header=BB16_8 Depth=3
	movl	%r8d, %esi
	movzbl	(%rax,%rsi), %r11d
	movzbl	(%rdx,%rsi), %r14d
	incl	%r8d
	cmpb	%r14b, %r11b
	je	.LBB16_8
# BB#10:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB16_6 Depth=2
	decl	%r8d
.LBB16_11:                              # %.thread90.i
                                        #   in Loop: Header=BB16_6 Depth=2
	decl	%r10d
	cmpb	%r14b, %r11b
	jae	.LBB16_13
# BB#12:                                #   in Loop: Header=BB16_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r13d, (%rax)
	addq	$4, %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %edi
	movl	%ebp, %r11d
	testl	%r10d, %r10d
	jne	.LBB16_15
	jmp	.LBB16_16
	.p2align	4, 0x90
.LBB16_13:                              #   in Loop: Header=BB16_6 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movl	%r8d, %r11d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%ebx, %edi
	testl	%r10d, %r10d
	je	.LBB16_16
.LBB16_15:                              # %.thread92.i
                                        #   in Loop: Header=BB16_6 Depth=2
	movl	(%r9), %r13d
	movl	28(%rsp), %eax          # 4-byte Reload
	subl	%r13d, %eax
	movl	24(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	jb	.LBB16_6
	.p2align	4, 0x90
.LBB16_16:                              # %.thread.i
                                        #   in Loop: Header=BB16_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB16_18
	.p2align	4, 0x90
.LBB16_17:                              #   in Loop: Header=BB16_1 Depth=1
	movl	(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%r9), %eax
.LBB16_18:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r12)
.LBB16_19:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	8(%r12), %eax
	incl	%eax
	movl	%eax, 8(%r12)
	cmpl	12(%r12), %eax
	jne	.LBB16_21
# BB#20:                                #   in Loop: Header=BB16_1 Depth=1
	movq	%r12, %rdi
	callq	MatchFinder_CheckLimits
.LBB16_21:                              # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	decl	%r15d
	jne	.LBB16_1
# BB#22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	Bt3Zip_MatchFinder_Skip, .Lfunc_end16-Bt3Zip_MatchFinder_Skip
	.cfi_endproc

	.globl	Hc3Zip_MatchFinder_Skip
	.p2align	4, 0x90
	.type	Hc3Zip_MatchFinder_Skip,@function
Hc3Zip_MatchFinder_Skip:                # @Hc3Zip_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB17_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$2, 20(%rbx)
	ja	.LBB17_3
# BB#2:                                 #   in Loop: Header=BB17_1 Depth=1
	incl	24(%rbx)
	incq	(%rbx)
	jmp	.LBB17_4
	.p2align	4, 0x90
.LBB17_3:                               #   in Loop: Header=BB17_1 Depth=1
	movq	(%rbx), %rax
	movzbl	2(%rax), %ecx
	movzbl	(%rax), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	1(%rax), %ecx
	incq	%rax
	movzwl	140(%rbx,%rcx,4), %ecx
	xorl	%edx, %ecx
	movq	40(%rbx), %rdx
	movl	(%rdx,%rcx,4), %esi
	movl	8(%rbx), %edi
	movl	%edi, (%rdx,%rcx,4)
	movq	48(%rbx), %rcx
	movl	24(%rbx), %edx
	movl	%esi, (%rcx,%rdx,4)
	incl	24(%rbx)
	movq	%rax, (%rbx)
.LBB17_4:                               #   in Loop: Header=BB17_1 Depth=1
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	cmpl	12(%rbx), %eax
	jne	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_1 Depth=1
	movq	%rbx, %rdi
	callq	MatchFinder_CheckLimits
.LBB17_6:                               # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB17_1 Depth=1
	decl	%ebp
	jne	.LBB17_1
# BB#7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end17:
	.size	Hc3Zip_MatchFinder_Skip, .Lfunc_end17-Hc3Zip_MatchFinder_Skip
	.cfi_endproc

	.globl	MatchFinder_CreateVTable
	.p2align	4, 0x90
	.type	MatchFinder_CreateVTable,@function
MatchFinder_CreateVTable:               # @MatchFinder_CreateVTable
	.cfi_startproc
# BB#0:
	movq	$MatchFinder_Init, (%rsi)
	movq	$MatchFinder_GetIndexByte, 8(%rsi)
	movq	$MatchFinder_GetNumAvailableBytes, 16(%rsi)
	movq	$MatchFinder_GetPointerToCurrentPos, 24(%rsi)
	cmpl	$0, 112(%rdi)
	je	.LBB18_1
# BB#2:
	movl	96(%rdi), %eax
	cmpl	$2, %eax
	jne	.LBB18_4
# BB#3:
	movq	$Bt2_MatchFinder_GetMatches, 32(%rsi)
	movl	$Bt2_MatchFinder_Skip, %eax
	movq	%rax, 40(%rsi)
	retq
.LBB18_1:
	movq	$Hc4_MatchFinder_GetMatches, 32(%rsi)
	movl	$Hc4_MatchFinder_Skip, %eax
	movq	%rax, 40(%rsi)
	retq
.LBB18_4:
	cmpl	$3, %eax
	jne	.LBB18_6
# BB#5:
	movq	$Bt3_MatchFinder_GetMatches, 32(%rsi)
	movl	$Bt3_MatchFinder_Skip, %eax
	movq	%rax, 40(%rsi)
	retq
.LBB18_6:
	movq	$Bt4_MatchFinder_GetMatches, 32(%rsi)
	movl	$Bt4_MatchFinder_Skip, %eax
	movq	%rax, 40(%rsi)
	retq
.Lfunc_end18:
	.size	MatchFinder_CreateVTable, .Lfunc_end18-MatchFinder_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	Hc4_MatchFinder_GetMatches,@function
Hc4_MatchFinder_GetMatches:             # @Hc4_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 96
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rdi, %r10
	movl	20(%r10), %r14d
	cmpl	$3, %r14d
	ja	.LBB19_2
# BB#1:
	incl	24(%r10)
	incq	(%r10)
	movl	8(%r10), %eax
	incl	%eax
	movl	%eax, 8(%r10)
	xorl	%r13d, %r13d
	cmpl	12(%r10), %eax
	je	.LBB19_37
	jmp	.LBB19_38
.LBB19_2:
	movq	(%r10), %r15
	movzbl	(%r15), %ecx
	movzbl	1(%r15), %eax
	xorl	140(%r10,%rcx,4), %eax
	movzbl	2(%r15), %ecx
	shll	$8, %ecx
	xorl	%eax, %ecx
	andl	$1023, %eax             # imm = 0x3FF
	movzwl	%cx, %r9d
	movzbl	3(%r15), %edx
	movl	140(%r10,%rdx,4), %ebp
	shll	$5, %ebp
	xorl	%ecx, %ebp
	andl	56(%r10), %ebp
	movl	8(%r10), %ecx
	movq	40(%r10), %rbx
	movl	%ecx, %edx
	subl	(%rbx,%rax,4), %edx
	movl	%ecx, %r8d
	subl	4096(%rbx,%r9,4), %r8d
	addl	$66560, %ebp            # imm = 0x10400
	movl	(%rbx,%rbp,4), %r12d
	movl	%ecx, (%rbx,%rbp,4)
	movl	%ecx, 4096(%rbx,%r9,4)
	movl	%ecx, (%rbx,%rax,4)
	movl	$1, %ebp
	xorl	%r13d, %r13d
	cmpl	28(%r10), %edx
	jae	.LBB19_5
# BB#3:
	movl	%edx, %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	movb	(%rcx), %al
	cmpb	(%r15), %al
	jne	.LBB19_5
# BB#4:
	movl	$2, (%rsi)
	leal	-1(%rdx), %eax
	movl	%eax, 4(%rsi)
	movl	$2, %r13d
	movl	$2, %ebp
.LBB19_5:
	leaq	1(%r15), %rbx
	cmpl	%r8d, %edx
	je	.LBB19_9
# BB#6:
	cmpl	28(%r10), %r8d
	jae	.LBB19_9
# BB#7:
	movl	%r8d, %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	movb	(%rcx), %al
	cmpb	(%r15), %al
	jne	.LBB19_9
# BB#8:                                 # %.thread
	leal	-1(%r8), %eax
	movl	%r13d, %ecx
	orl	$1, %ecx
	movl	%eax, (%rsi,%rcx,4)
	addl	$2, %r13d
	movl	$3, %ebp
	cmpl	%r14d, %ebp
	jne	.LBB19_12
	jmp	.LBB19_16
.LBB19_9:
	testl	%r13d, %r13d
	je	.LBB19_17
# BB#10:
	movl	%edx, %r8d
	cmpl	%r14d, %ebp
	je	.LBB19_16
.LBB19_12:                              # %.lr.ph
	movl	%r8d, %eax
	.p2align	4, 0x90
.LBB19_13:                              # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movzbl	(%r15,%rdx), %edx
	cmpb	(%r15,%rcx), %dl
	jne	.LBB19_14
# BB#15:                                #   in Loop: Header=BB19_13 Depth=1
	incl	%ebp
	cmpl	%ebp, %r14d
	jne	.LBB19_13
.LBB19_16:                              # %._crit_edge
	leal	-2(%r13), %eax
	movl	%r14d, (%rsi,%rax,4)
	movq	48(%r10), %rax
	movl	24(%r10), %ecx
	movl	%r12d, (%rax,%rcx,4)
	incl	24(%r10)
	movq	%rbx, (%r10)
	jmp	.LBB19_35
.LBB19_17:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB19_18
.LBB19_14:                              # %.thread120
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leal	-2(%r13), %eax
	movl	%ebp, (%rsi,%rax,4)
.LBB19_18:
	cmpl	$3, %ebp
	movl	$3, %eax
	cmoval	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	8(%r10), %r9d
	movl	24(%r10), %edi
	movq	48(%r10), %r11
	movl	28(%r10), %ebx
	movl	60(%r10), %ebp
	movl	%r13d, %eax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rax,4), %r13
	movl	%r12d, (%r11,%rdi,4)
	movq	%r10, 8(%rsp)           # 8-byte Spill
.LBB19_19:                              # %.thread69.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_22 Depth 2
                                        #       Child Loop BB19_25 Depth 3
	testl	%ebp, %ebp
	je	.LBB19_34
# BB#20:                                # %.thread69.outer.i
                                        #   in Loop: Header=BB19_19 Depth=1
	movl	%r9d, %edx
	subl	%r12d, %edx
	cmpl	%ebx, %edx
	jae	.LBB19_34
# BB#21:                                # %.lr.ph.i
                                        #   in Loop: Header=BB19_19 Depth=1
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	4(%rsp), %r13d          # 4-byte Reload
	movb	(%r15,%r13), %r8b
	.p2align	4, 0x90
.LBB19_22:                              #   Parent Loop BB19_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_25 Depth 3
	decl	%ebp
	movl	%edx, %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	movl	%edi, %eax
	subl	%edx, %eax
	movl	$0, %esi
	cmovbl	%ebx, %esi
	addl	%eax, %esi
	movl	(%r11,%rsi,4), %r12d
	cmpb	%r8b, (%rcx,%r13)
	jne	.LBB19_27
# BB#23:                                #   in Loop: Header=BB19_22 Depth=2
	movb	(%rcx), %al
	cmpb	(%r15), %al
	jne	.LBB19_27
# BB#24:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB19_22 Depth=2
	movl	%r9d, %r10d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB19_25:                              # %.preheader.i
                                        #   Parent Loop BB19_19 Depth=1
                                        #     Parent Loop BB19_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, %r14d
	je	.LBB19_26
# BB#30:                                #   in Loop: Header=BB19_25 Depth=3
	movl	%eax, %esi
	movzbl	(%rcx,%rsi), %r9d
	incl	%eax
	cmpb	(%r15,%rsi), %r9b
	je	.LBB19_25
# BB#31:                                # %._crit_edge170
                                        #   in Loop: Header=BB19_22 Depth=2
	decl	%eax
	xorl	%ecx, %ecx
	jmp	.LBB19_32
.LBB19_26:                              #   in Loop: Header=BB19_22 Depth=2
	movb	$1, %cl
	movl	%r14d, %eax
.LBB19_32:                              #   in Loop: Header=BB19_22 Depth=2
	movl	%r10d, %r9d
	cmpl	%eax, 4(%rsp)           # 4-byte Folded Reload
	jb	.LBB19_33
.LBB19_27:                              # %.thread69.backedge.i
                                        #   in Loop: Header=BB19_22 Depth=2
	testl	%ebp, %ebp
	je	.LBB19_28
# BB#29:                                # %.thread69.backedge.i
                                        #   in Loop: Header=BB19_22 Depth=2
	movl	%r9d, %edx
	subl	%r12d, %edx
	cmpl	%ebx, %edx
	jb	.LBB19_22
	jmp	.LBB19_28
.LBB19_33:                              #   in Loop: Header=BB19_19 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	%eax, (%r13)
	decl	%edx
	movl	%edx, 4(%r13)
	addq	$8, %r13
	testb	%cl, %cl
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	8(%rsp), %r10           # 8-byte Reload
	je	.LBB19_19
	jmp	.LBB19_34
.LBB19_28:
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB19_34:                              # %Hc_GetMatchesSpec.exit
	subq	32(%rsp), %r13          # 8-byte Folded Reload
	shrq	$2, %r13
	incl	24(%r10)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r10)
.LBB19_35:                              # %Hc_GetMatchesSpec.exit
	movl	8(%r10), %eax
	incl	%eax
	movl	%eax, 8(%r10)
	cmpl	12(%r10), %eax
	jne	.LBB19_38
.LBB19_37:
	movq	%r10, %rdi
	callq	MatchFinder_CheckLimits
.LBB19_38:                              # %MatchFinder_MovePos.exit
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	Hc4_MatchFinder_GetMatches, .Lfunc_end19-Hc4_MatchFinder_GetMatches
	.cfi_endproc

	.p2align	4, 0x90
	.type	Hc4_MatchFinder_Skip,@function
Hc4_MatchFinder_Skip:                   # @Hc4_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$3, 20(%rbx)
	ja	.LBB20_3
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	incl	24(%rbx)
	incq	(%rbx)
	jmp	.LBB20_4
	.p2align	4, 0x90
.LBB20_3:                               #   in Loop: Header=BB20_1 Depth=1
	movq	(%rbx), %rcx
	movzbl	(%rcx), %edx
	movzbl	1(%rcx), %eax
	xorl	140(%rbx,%rdx,4), %eax
	movzbl	2(%rcx), %edx
	shll	$8, %edx
	xorl	%eax, %edx
	andl	$1023, %eax             # imm = 0x3FF
	movzwl	%dx, %r9d
	movzbl	3(%rcx), %edi
	leaq	1(%rcx), %r8
	movl	140(%rbx,%rdi,4), %edi
	shll	$5, %edi
	xorl	%edx, %edi
	andl	56(%rbx), %edi
	movq	40(%rbx), %rdx
	addl	$66560, %edi            # imm = 0x10400
	movl	(%rdx,%rdi,4), %ecx
	movl	8(%rbx), %esi
	movl	%esi, (%rdx,%rdi,4)
	movl	%esi, 4096(%rdx,%r9,4)
	movl	%esi, (%rdx,%rax,4)
	movq	48(%rbx), %rax
	movl	24(%rbx), %edx
	movl	%ecx, (%rax,%rdx,4)
	incl	24(%rbx)
	movq	%r8, (%rbx)
.LBB20_4:                               #   in Loop: Header=BB20_1 Depth=1
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	cmpl	12(%rbx), %eax
	jne	.LBB20_6
# BB#5:                                 #   in Loop: Header=BB20_1 Depth=1
	movq	%rbx, %rdi
	callq	MatchFinder_CheckLimits
.LBB20_6:                               # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	decl	%ebp
	jne	.LBB20_1
# BB#7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end20:
	.size	Hc4_MatchFinder_Skip, .Lfunc_end20-Hc4_MatchFinder_Skip
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt2_MatchFinder_GetMatches,@function
Bt2_MatchFinder_GetMatches:             # @Bt2_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	20(%rbx), %edi
	cmpl	$1, %edi
	ja	.LBB21_2
# BB#1:
	incl	24(%rbx)
	incq	(%rbx)
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	xorl	%r15d, %r15d
	cmpl	12(%rbx), %eax
	je	.LBB21_4
	jmp	.LBB21_5
.LBB21_2:
	movq	(%rbx), %rcx
	movzbl	(%rcx), %eax
	movzbl	1(%rcx), %edx
	shlq	$8, %rdx
	orq	%rax, %rdx
	movq	40(%rbx), %r8
	movl	(%r8,%rdx,4), %esi
	movl	8(%rbx), %eax
	movl	%eax, (%r8,%rdx,4)
	movq	48(%rbx), %r8
	movl	8(%rbx), %edx
	movl	24(%rbx), %r9d
	movl	28(%rbx), %r10d
	movl	60(%rbx), %eax
	pushq	$1
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	callq	GetMatchesSpec1
	addq	$32, %rsp
.Lcfi114:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	subq	%r14, %r15
	shrq	$2, %r15
	incl	24(%rbx)
	incq	(%rbx)
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	cmpl	12(%rbx), %eax
	jne	.LBB21_5
.LBB21_4:
	movq	%rbx, %rdi
	callq	MatchFinder_CheckLimits
.LBB21_5:                               # %MatchFinder_MovePos.exit
	movl	%r15d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	Bt2_MatchFinder_GetMatches, .Lfunc_end21-Bt2_MatchFinder_GetMatches
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt2_MatchFinder_Skip,@function
Bt2_MatchFinder_Skip:                   # @Bt2_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 112
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	.p2align	4, 0x90
.LBB22_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_6 Depth 2
                                        #       Child Loop BB22_8 Depth 3
	movl	20(%r12), %ecx
	cmpl	$1, %ecx
	ja	.LBB22_3
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	incl	24(%r12)
	incq	(%r12)
	jmp	.LBB22_19
	.p2align	4, 0x90
.LBB22_3:                               #   in Loop: Header=BB22_1 Depth=1
	movq	(%r12), %rsi
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movzbl	1(%rsi), %edx
	shlq	$8, %rdx
	orq	%rax, %rdx
	movq	40(%r12), %rax
	movl	(%rax,%rdx,4), %r13d
	movl	8(%r12), %ebp
	movl	%ebp, (%rax,%rdx,4)
	movl	8(%r12), %ebp
	movl	24(%r12), %eax
	movq	48(%r12), %rdi
	movl	28(%r12), %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%rdi,%rax,4), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	leaq	4(%rdi,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	%ebx, %eax
	jae	.LBB22_16
# BB#4:                                 #   in Loop: Header=BB22_1 Depth=1
	movl	60(%r12), %r10d
	testl	%r10d, %r10d
	je	.LBB22_16
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_1 Depth=1
	xorl	%edi, %edi
	xorl	%r11d, %r11d
	movl	%ebx, 24(%rsp)          # 4-byte Spill
.LBB22_6:                               # %.lr.ph.i
                                        #   Parent Loop BB22_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_8 Depth 3
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %ebp
	subl	%eax, %ebp
	movl	$0, %edx
	cmovbl	%ebx, %edx
	addl	%ebp, %edx
	addl	%edx, %edx
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx,4), %r9
	movl	%eax, %edx
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpl	%edi, %r11d
	movl	%edi, %ebx
	movl	%edi, %r8d
	movl	%r11d, %ebp
	cmovbl	%r11d, %r8d
	movb	(%rax,%r8), %r11b
	movb	(%rsi,%r8), %r14b
	cmpb	%r14b, %r11b
	jne	.LBB22_11
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB22_6 Depth=2
	incl	%r8d
	.p2align	4, 0x90
.LBB22_8:                               # %.preheader.i
                                        #   Parent Loop BB22_1 Depth=1
                                        #     Parent Loop BB22_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r8d, %ecx
	je	.LBB22_17
# BB#9:                                 #   in Loop: Header=BB22_8 Depth=3
	movl	%r8d, %edx
	movzbl	(%rax,%rdx), %r11d
	movzbl	(%rsi,%rdx), %r14d
	incl	%r8d
	cmpb	%r14b, %r11b
	je	.LBB22_8
# BB#10:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB22_6 Depth=2
	decl	%r8d
.LBB22_11:                              # %.thread90.i
                                        #   in Loop: Header=BB22_6 Depth=2
	decl	%r10d
	cmpb	%r14b, %r11b
	jae	.LBB22_13
# BB#12:                                #   in Loop: Header=BB22_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r13d, (%rax)
	addq	$4, %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %edi
	movl	%ebp, %r11d
	testl	%r10d, %r10d
	jne	.LBB22_15
	jmp	.LBB22_16
	.p2align	4, 0x90
.LBB22_13:                              #   in Loop: Header=BB22_6 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movl	%r8d, %r11d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%ebx, %edi
	testl	%r10d, %r10d
	je	.LBB22_16
.LBB22_15:                              # %.thread92.i
                                        #   in Loop: Header=BB22_6 Depth=2
	movl	(%r9), %r13d
	movl	28(%rsp), %eax          # 4-byte Reload
	subl	%r13d, %eax
	movl	24(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	jb	.LBB22_6
	.p2align	4, 0x90
.LBB22_16:                              # %.thread.i
                                        #   in Loop: Header=BB22_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB22_18
	.p2align	4, 0x90
.LBB22_17:                              #   in Loop: Header=BB22_1 Depth=1
	movl	(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%r9), %eax
.LBB22_18:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB22_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r12)
.LBB22_19:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB22_1 Depth=1
	movl	8(%r12), %eax
	incl	%eax
	movl	%eax, 8(%r12)
	cmpl	12(%r12), %eax
	jne	.LBB22_21
# BB#20:                                #   in Loop: Header=BB22_1 Depth=1
	movq	%r12, %rdi
	callq	MatchFinder_CheckLimits
.LBB22_21:                              # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB22_1 Depth=1
	decl	%r15d
	jne	.LBB22_1
# BB#22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	Bt2_MatchFinder_Skip, .Lfunc_end22-Bt2_MatchFinder_Skip
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt3_MatchFinder_GetMatches,@function
Bt3_MatchFinder_GetMatches:             # @Bt3_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi134:
	.cfi_def_cfa_offset 96
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	20(%r15), %edi
	cmpl	$2, %edi
	ja	.LBB23_2
# BB#1:
	incl	24(%r15)
	incq	(%r15)
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%r15)
	xorl	%ebx, %ebx
	cmpl	12(%r15), %eax
	je	.LBB23_24
	jmp	.LBB23_25
.LBB23_2:
	movq	(%r15), %rcx
	movzbl	(%rcx), %eax
	movzbl	1(%rcx), %ebp
	xorl	140(%r15,%rax,4), %ebp
	movzbl	2(%rcx), %eax
	shll	$8, %eax
	xorl	%ebp, %eax
	andl	$1023, %ebp             # imm = 0x3FF
	andl	56(%r15), %eax
	movl	8(%r15), %edx
	movq	40(%r15), %rbx
	movl	%edx, %r8d
	subl	(%rbx,%rbp,4), %r8d
	addl	$1024, %eax             # imm = 0x400
	movl	(%rbx,%rax,4), %esi
	movl	%edx, (%rbx,%rax,4)
	movl	%edx, (%rbx,%rbp,4)
	movl	28(%r15), %r11d
	movl	$2, %eax
	cmpl	%r11d, %r8d
	jae	.LBB23_21
# BB#3:
	movl	%r8d, %ebx
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	movb	(%rdx), %dl
	cmpb	(%rcx), %dl
	jne	.LBB23_21
# BB#4:                                 # %.lr.ph.preheader
	leaq	1(%rcx), %r9
	movl	$2, %eax
	.p2align	4, 0x90
.LBB23_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%rbp, %rdx
	subq	%rbx, %rdx
	movzbl	(%rcx,%rdx), %edx
	cmpb	(%rcx,%rbp), %dl
	jne	.LBB23_22
# BB#6:                                 #   in Loop: Header=BB23_5 Depth=1
	incl	%eax
	cmpl	%eax, %edi
	jne	.LBB23_5
# BB#7:                                 # %._crit_edge
	movl	%edi, (%r14)
	decl	%r8d
	movl	%r8d, 4(%r14)
	movl	8(%r15), %ebp
	movl	24(%r15), %eax
	movq	48(%r15), %r12
	movl	28(%r15), %r13d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%r12,%rax,4), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	leaq	4(%r12,%rax,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	%ebp, %edx
	subl	%esi, %edx
	cmpl	%r13d, %edx
	movq	%r9, 24(%rsp)           # 8-byte Spill
	jae	.LBB23_19
# BB#8:                                 # %._crit_edge
	movl	60(%r15), %ebp
	testl	%ebp, %ebp
	je	.LBB23_19
# BB#9:                                 # %.lr.ph.i.preheader
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
.LBB23_10:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_12 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%edx, %eax
	movl	$0, %ebx
	cmovbl	%r13d, %ebx
	addl	%eax, %ebx
	addl	%ebx, %ebx
	leaq	(%r12,%rbx,4), %rax
	movl	%edx, %ebx
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	cmpl	%r9d, %r14d
	movl	%r9d, %r8d
	cmovbl	%r14d, %r8d
	movb	(%rdx,%r8), %r10b
	movb	(%rcx,%r8), %r11b
	cmpb	%r11b, %r10b
	jne	.LBB23_15
# BB#11:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB23_10 Depth=1
	incl	%r8d
	.p2align	4, 0x90
.LBB23_12:                              # %.preheader.i
                                        #   Parent Loop BB23_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r8d, %edi
	je	.LBB23_26
# BB#13:                                #   in Loop: Header=BB23_12 Depth=2
	movl	%r8d, %ebx
	movzbl	(%rdx,%rbx), %r10d
	movzbl	(%rcx,%rbx), %r11d
	incl	%r8d
	cmpb	%r11b, %r10b
	je	.LBB23_12
# BB#14:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB23_10 Depth=1
	decl	%r8d
.LBB23_15:                              # %.thread90.i
                                        #   in Loop: Header=BB23_10 Depth=1
	decl	%ebp
	cmpb	%r11b, %r10b
	jae	.LBB23_17
# BB#16:                                #   in Loop: Header=BB23_10 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%esi, (%rdx)
	addq	$4, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r8d, %r9d
	testl	%ebp, %ebp
	jne	.LBB23_18
	jmp	.LBB23_19
.LBB23_17:                              #   in Loop: Header=BB23_10 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%esi, (%rdx)
	movl	%r8d, %r14d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB23_19
.LBB23_18:                              # %.thread92.i
                                        #   in Loop: Header=BB23_10 Depth=1
	movl	(%rax), %esi
	movl	20(%rsp), %edx          # 4-byte Reload
	subl	%esi, %edx
	cmpl	%r13d, %edx
	jb	.LBB23_10
.LBB23_19:                              # %.thread.i
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB23_27
.LBB23_21:
	xorl	%r10d, %r10d
.LBB23_23:
	movq	48(%r15), %r8
	movl	8(%r15), %edx
	movl	24(%r15), %r9d
	movl	60(%r15), %ebx
	leaq	(%r14,%r10,4), %rbp
	pushq	%rax
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	callq	GetMatchesSpec1
	addq	$32, %rsp
.Lcfi145:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	subq	%r14, %rbx
	shrq	$2, %rbx
	incl	24(%r15)
	incq	(%r15)
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%r15)
	cmpl	12(%r15), %eax
	jne	.LBB23_25
.LBB23_24:
	movq	%r15, %rdi
	callq	MatchFinder_CheckLimits
.LBB23_25:                              # %MatchFinder_MovePos.exit
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_22:                              # %.thread90
	movl	%eax, (%r14)
	decl	%r8d
	movl	%r8d, 4(%r14)
	movl	28(%r15), %r11d
	movl	$2, %r10d
	jmp	.LBB23_23
.LBB23_26:
	movl	(%rax), %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%ecx, (%rdx)
	movl	4(%rax), %eax
.LBB23_27:                              # %SkipMatchesSpec.exit
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%r15)
	movl	$2, %ebx
	cmpl	12(%r15), %eax
	je	.LBB23_24
	jmp	.LBB23_25
.Lfunc_end23:
	.size	Bt3_MatchFinder_GetMatches, .Lfunc_end23-Bt3_MatchFinder_GetMatches
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt3_MatchFinder_Skip,@function
Bt3_MatchFinder_Skip:                   # @Bt3_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi152:
	.cfi_def_cfa_offset 112
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r13
	.p2align	4, 0x90
.LBB24_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_6 Depth 2
                                        #       Child Loop BB24_8 Depth 3
	movl	20(%r13), %ecx
	cmpl	$2, %ecx
	ja	.LBB24_3
# BB#2:                                 #   in Loop: Header=BB24_1 Depth=1
	incl	24(%r13)
	incq	(%r13)
	jmp	.LBB24_19
	.p2align	4, 0x90
.LBB24_3:                               #   in Loop: Header=BB24_1 Depth=1
	movq	(%r13), %rdx
	movzbl	(%rdx), %eax
	leaq	1(%rdx), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movzbl	1(%rdx), %esi
	xorl	140(%r13,%rax,4), %esi
	movzbl	2(%rdx), %eax
	shll	$8, %eax
	xorl	%esi, %eax
	andl	$1023, %esi             # imm = 0x3FF
	andl	56(%r13), %eax
	movq	40(%r13), %rbp
	addl	$1024, %eax             # imm = 0x400
	movl	(%rbp,%rax,4), %r12d
	movl	8(%r13), %edi
	movl	%edi, (%rbp,%rax,4)
	movl	%edi, (%rbp,%rsi,4)
	movl	8(%r13), %edi
	movl	24(%r13), %eax
	movq	48(%r13), %rbp
	movl	28(%r13), %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%rbp,%rax,4), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	4(%rbp,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	subl	%r12d, %eax
	cmpl	%ebx, %eax
	jae	.LBB24_16
# BB#4:                                 #   in Loop: Header=BB24_1 Depth=1
	movl	60(%r13), %r10d
	testl	%r10d, %r10d
	je	.LBB24_16
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB24_1 Depth=1
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	movl	%ebx, 24(%rsp)          # 4-byte Spill
.LBB24_6:                               # %.lr.ph.i
                                        #   Parent Loop BB24_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_8 Depth 3
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ebp
	subl	%eax, %ebp
	movl	$0, %edi
	cmovbl	%ebx, %edi
	addl	%ebp, %edi
	addl	%edi, %edi
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdi,4), %r9
	movl	%eax, %edi
	movq	%rdx, %rax
	subq	%rdi, %rax
	cmpl	%esi, %r11d
	movl	%esi, %ebx
	movl	%esi, %r8d
	movl	%r11d, %ebp
	cmovbl	%r11d, %r8d
	movb	(%rax,%r8), %r11b
	movb	(%rdx,%r8), %r14b
	cmpb	%r14b, %r11b
	jne	.LBB24_11
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB24_6 Depth=2
	incl	%r8d
	.p2align	4, 0x90
.LBB24_8:                               # %.preheader.i
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r8d, %ecx
	je	.LBB24_17
# BB#9:                                 #   in Loop: Header=BB24_8 Depth=3
	movl	%r8d, %edi
	movzbl	(%rax,%rdi), %r11d
	movzbl	(%rdx,%rdi), %r14d
	incl	%r8d
	cmpb	%r14b, %r11b
	je	.LBB24_8
# BB#10:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB24_6 Depth=2
	decl	%r8d
.LBB24_11:                              # %.thread90.i
                                        #   in Loop: Header=BB24_6 Depth=2
	decl	%r10d
	cmpb	%r14b, %r11b
	jae	.LBB24_13
# BB#12:                                #   in Loop: Header=BB24_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r12d, (%rax)
	addq	$4, %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %esi
	movl	%ebp, %r11d
	testl	%r10d, %r10d
	jne	.LBB24_15
	jmp	.LBB24_16
	.p2align	4, 0x90
.LBB24_13:                              #   in Loop: Header=BB24_6 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r12d, (%rax)
	movl	%r8d, %r11d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%ebx, %esi
	testl	%r10d, %r10d
	je	.LBB24_16
.LBB24_15:                              # %.thread92.i
                                        #   in Loop: Header=BB24_6 Depth=2
	movl	(%r9), %r12d
	movl	28(%rsp), %eax          # 4-byte Reload
	subl	%r12d, %eax
	movl	24(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	jb	.LBB24_6
	.p2align	4, 0x90
.LBB24_16:                              # %.thread.i
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB24_18
	.p2align	4, 0x90
.LBB24_17:                              #   in Loop: Header=BB24_1 Depth=1
	movl	(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%r9), %eax
.LBB24_18:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r13)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
.LBB24_19:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	movl	8(%r13), %eax
	incl	%eax
	movl	%eax, 8(%r13)
	cmpl	12(%r13), %eax
	jne	.LBB24_21
# BB#20:                                #   in Loop: Header=BB24_1 Depth=1
	movq	%r13, %rdi
	callq	MatchFinder_CheckLimits
.LBB24_21:                              # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	decl	%r15d
	jne	.LBB24_1
# BB#22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	Bt3_MatchFinder_Skip, .Lfunc_end24-Bt3_MatchFinder_Skip
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt4_MatchFinder_GetMatches,@function
Bt4_MatchFinder_GetMatches:             # @Bt4_MatchFinder_GetMatches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi165:
	.cfi_def_cfa_offset 112
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	20(%r12), %edi
	cmpl	$3, %edi
	ja	.LBB25_2
# BB#1:
	incl	24(%r12)
	incq	(%r12)
	movl	8(%r12), %eax
	incl	%eax
	movl	%eax, 8(%r12)
	xorl	%r14d, %r14d
	cmpl	12(%r12), %eax
	je	.LBB25_36
	jmp	.LBB25_37
.LBB25_2:
	movq	(%r12), %rcx
	movzbl	(%rcx), %edx
	movzbl	1(%rcx), %eax
	xorl	140(%r12,%rdx,4), %eax
	movzbl	2(%rcx), %edx
	shll	$8, %edx
	xorl	%eax, %edx
	andl	$1023, %eax             # imm = 0x3FF
	movzwl	%dx, %r9d
	movzbl	3(%rcx), %esi
	movl	140(%r12,%rsi,4), %ebp
	shll	$5, %ebp
	xorl	%edx, %ebp
	andl	56(%r12), %ebp
	movl	8(%r12), %ebx
	movq	40(%r12), %rdx
	movl	%ebx, %r10d
	subl	(%rdx,%rax,4), %r10d
	movl	%ebx, %r8d
	subl	4096(%rdx,%r9,4), %r8d
	addl	$66560, %ebp            # imm = 0x10400
	movl	(%rdx,%rbp,4), %esi
	movl	%ebx, (%rdx,%rbp,4)
	movl	%ebx, 4096(%rdx,%r9,4)
	movl	%ebx, (%rdx,%rax,4)
	movl	$1, %eax
	xorl	%r14d, %r14d
	cmpl	28(%r12), %r10d
	jae	.LBB25_5
# BB#3:
	movl	%r10d, %edx
	movq	%rcx, %rbp
	subq	%rdx, %rbp
	movb	(%rbp), %dl
	cmpb	(%rcx), %dl
	jne	.LBB25_5
# BB#4:
	movl	$2, (%r15)
	leal	-1(%r10), %eax
	movl	%eax, 4(%r15)
	movl	$2, %r14d
	movl	$2, %eax
.LBB25_5:
	cmpl	%r8d, %r10d
	je	.LBB25_9
# BB#6:
	cmpl	28(%r12), %r8d
	jae	.LBB25_9
# BB#7:
	movl	%r8d, %edx
	movq	%rcx, %rbp
	subq	%rdx, %rbp
	movb	(%rbp), %dl
	cmpb	(%rcx), %dl
	jne	.LBB25_9
# BB#8:                                 # %.thread
	leal	-1(%r8), %eax
	movl	%r14d, %edx
	orl	$1, %edx
	movl	%eax, (%r15,%rdx,4)
	addl	$2, %r14d
	movl	$3, %eax
	cmpl	%edi, %eax
	jne	.LBB25_12
	jmp	.LBB25_16
.LBB25_9:
	testl	%r14d, %r14d
	je	.LBB25_32
# BB#10:
	movl	%r10d, %r8d
	cmpl	%edi, %eax
	je	.LBB25_16
.LBB25_12:                              # %.lr.ph
	movl	%r8d, %r8d
	.p2align	4, 0x90
.LBB25_13:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	movq	%rbx, %rbp
	subq	%r8, %rbp
	movzbl	(%rcx,%rbp), %edx
	cmpb	(%rcx,%rbx), %dl
	jne	.LBB25_14
# BB#15:                                #   in Loop: Header=BB25_13 Depth=1
	incl	%eax
	cmpl	%eax, %edi
	jne	.LBB25_13
.LBB25_16:                              # %._crit_edge
	leaq	1(%rcx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	-2(%r14), %eax
	movl	%edi, (%r15,%rax,4)
	movl	8(%r12), %ebp
	movl	24(%r12), %eax
	movq	48(%r12), %rbx
	movl	28(%r12), %r8d
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%rbx,%rax,4), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movl	%ebp, %edx
	subl	%esi, %edx
	cmpl	%r8d, %edx
	jae	.LBB25_29
# BB#17:                                # %._crit_edge
	movl	60(%r12), %eax
	testl	%eax, %eax
	je	.LBB25_29
# BB#18:                                # %.lr.ph.i.preheader
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	movl	%r8d, 24(%rsp)          # 4-byte Spill
.LBB25_19:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_21 Depth 2
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %ebx
	subl	%edx, %ebx
	movl	$0, %ebp
	cmovbl	%r8d, %ebp
	addl	%ebx, %ebp
	addl	%ebp, %ebp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,4), %r9
	movl	%edx, %ebp
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	cmpl	%r10d, %r13d
	movl	%r10d, %r8d
	cmovbl	%r13d, %r8d
	movb	(%rdx,%r8), %r11b
	movb	(%rcx,%r8), %r15b
	cmpb	%r15b, %r11b
	jne	.LBB25_24
# BB#20:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB25_19 Depth=1
	incl	%r8d
	.p2align	4, 0x90
.LBB25_21:                              # %.preheader.i
                                        #   Parent Loop BB25_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r8d, %edi
	je	.LBB25_30
# BB#22:                                #   in Loop: Header=BB25_21 Depth=2
	movl	%r8d, %ebx
	movzbl	(%rdx,%rbx), %r11d
	movzbl	(%rcx,%rbx), %r15d
	incl	%r8d
	cmpb	%r15b, %r11b
	je	.LBB25_21
# BB#23:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB25_19 Depth=1
	decl	%r8d
.LBB25_24:                              # %.thread90.i
                                        #   in Loop: Header=BB25_19 Depth=1
	decl	%eax
	cmpb	%r15b, %r11b
	jae	.LBB25_26
# BB#25:                                #   in Loop: Header=BB25_19 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%esi, (%rdx)
	addq	$4, %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %r10d
	testl	%eax, %eax
	jne	.LBB25_28
	jmp	.LBB25_29
.LBB25_26:                              #   in Loop: Header=BB25_19 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%esi, (%rdx)
	movl	%r8d, %r13d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	je	.LBB25_29
.LBB25_28:                              # %.thread92.i
                                        #   in Loop: Header=BB25_19 Depth=1
	movl	(%r9), %esi
	movl	28(%rsp), %edx          # 4-byte Reload
	subl	%esi, %edx
	movl	24(%rsp), %r8d          # 4-byte Reload
	cmpl	%r8d, %edx
	jb	.LBB25_19
.LBB25_29:                              # %.thread.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB25_31
.LBB25_32:
	xorl	%r14d, %r14d
	jmp	.LBB25_33
.LBB25_30:
	movl	(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%r9), %eax
.LBB25_31:                              # %SkipMatchesSpec.exit
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r12)
	jmp	.LBB25_34
.LBB25_14:                              # %.thread125
	leal	-2(%r14), %edx
	movl	%eax, (%r15,%rdx,4)
.LBB25_33:
	cmpl	$3, %eax
	movl	$3, %ebp
	cmoval	%eax, %ebp
	movq	48(%r12), %r8
	movl	8(%r12), %edx
	movl	24(%r12), %r9d
	movl	28(%r12), %r10d
	movl	60(%r12), %ebx
	movl	%r14d, %eax
	leaq	(%r15,%rax,4), %rax
	pushq	%rbp
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	callq	GetMatchesSpec1
	addq	$32, %rsp
.Lcfi176:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	subq	%r15, %r14
	shrq	$2, %r14
	incl	24(%r12)
	incq	(%r12)
.LBB25_34:
	movl	8(%r12), %eax
	incl	%eax
	movl	%eax, 8(%r12)
	cmpl	12(%r12), %eax
	jne	.LBB25_37
.LBB25_36:
	movq	%r12, %rdi
	callq	MatchFinder_CheckLimits
.LBB25_37:                              # %MatchFinder_MovePos.exit
	movl	%r14d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	Bt4_MatchFinder_GetMatches, .Lfunc_end25-Bt4_MatchFinder_GetMatches
	.cfi_endproc

	.p2align	4, 0x90
	.type	Bt4_MatchFinder_Skip,@function
Bt4_MatchFinder_Skip:                   # @Bt4_MatchFinder_Skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi183:
	.cfi_def_cfa_offset 112
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r13
	.p2align	4, 0x90
.LBB26_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_6 Depth 2
                                        #       Child Loop BB26_8 Depth 3
	movl	20(%r13), %ecx
	cmpl	$3, %ecx
	ja	.LBB26_3
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	incl	24(%r13)
	incq	(%r13)
	jmp	.LBB26_19
	.p2align	4, 0x90
.LBB26_3:                               #   in Loop: Header=BB26_1 Depth=1
	movq	(%r13), %rdx
	movzbl	(%rdx), %eax
	leaq	1(%rdx), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movzbl	1(%rdx), %esi
	xorl	140(%r13,%rax,4), %esi
	movzbl	2(%rdx), %eax
	shll	$8, %eax
	xorl	%esi, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movzwl	%ax, %r8d
	movzbl	3(%rdx), %edi
	movl	140(%r13,%rdi,4), %ebp
	shll	$5, %ebp
	xorl	%eax, %ebp
	andl	56(%r13), %ebp
	movq	40(%r13), %rax
	addl	$66560, %ebp            # imm = 0x10400
	movl	(%rax,%rbp,4), %r12d
	movl	8(%r13), %edi
	movl	%edi, 4096(%rax,%r8,4)
	movl	%edi, (%rax,%rsi,4)
	movl	8(%r13), %esi
	movl	%esi, (%rax,%rbp,4)
	movl	8(%r13), %edi
	movl	24(%r13), %eax
	movq	48(%r13), %rbp
	movl	28(%r13), %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %eax
	leaq	(%rbp,%rax,4), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	4(%rbp,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	subl	%r12d, %eax
	cmpl	%ebx, %eax
	jae	.LBB26_16
# BB#4:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	60(%r13), %r10d
	testl	%r10d, %r10d
	je	.LBB26_16
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_1 Depth=1
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	movl	%ebx, 24(%rsp)          # 4-byte Spill
.LBB26_6:                               # %.lr.ph.i
                                        #   Parent Loop BB26_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_8 Depth 3
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ebp
	subl	%eax, %ebp
	movl	$0, %edi
	cmovbl	%ebx, %edi
	addl	%ebp, %edi
	addl	%edi, %edi
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdi,4), %r9
	movl	%eax, %edi
	movq	%rdx, %rax
	subq	%rdi, %rax
	cmpl	%esi, %r11d
	movl	%esi, %ebx
	movl	%esi, %r8d
	movl	%r11d, %ebp
	cmovbl	%r11d, %r8d
	movb	(%rax,%r8), %r11b
	movb	(%rdx,%r8), %r14b
	cmpb	%r14b, %r11b
	jne	.LBB26_11
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB26_6 Depth=2
	incl	%r8d
	.p2align	4, 0x90
.LBB26_8:                               # %.preheader.i
                                        #   Parent Loop BB26_1 Depth=1
                                        #     Parent Loop BB26_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r8d, %ecx
	je	.LBB26_17
# BB#9:                                 #   in Loop: Header=BB26_8 Depth=3
	movl	%r8d, %edi
	movzbl	(%rax,%rdi), %r11d
	movzbl	(%rdx,%rdi), %r14d
	incl	%r8d
	cmpb	%r14b, %r11b
	je	.LBB26_8
# BB#10:                                # %.thread90.i.loopexit
                                        #   in Loop: Header=BB26_6 Depth=2
	decl	%r8d
.LBB26_11:                              # %.thread90.i
                                        #   in Loop: Header=BB26_6 Depth=2
	decl	%r10d
	cmpb	%r14b, %r11b
	jae	.LBB26_13
# BB#12:                                #   in Loop: Header=BB26_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r12d, (%rax)
	addq	$4, %r9
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %esi
	movl	%ebp, %r11d
	testl	%r10d, %r10d
	jne	.LBB26_15
	jmp	.LBB26_16
	.p2align	4, 0x90
.LBB26_13:                              #   in Loop: Header=BB26_6 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r12d, (%rax)
	movl	%r8d, %r11d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%ebx, %esi
	testl	%r10d, %r10d
	je	.LBB26_16
.LBB26_15:                              # %.thread92.i
                                        #   in Loop: Header=BB26_6 Depth=2
	movl	(%r9), %r12d
	movl	28(%rsp), %eax          # 4-byte Reload
	subl	%r12d, %eax
	movl	24(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	jb	.LBB26_6
	.p2align	4, 0x90
.LBB26_16:                              # %.thread.i
                                        #   in Loop: Header=BB26_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB26_18
	.p2align	4, 0x90
.LBB26_17:                              #   in Loop: Header=BB26_1 Depth=1
	movl	(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, (%rcx)
	movl	4(%r9), %eax
.LBB26_18:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB26_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	incl	24(%r13)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
.LBB26_19:                              # %SkipMatchesSpec.exit
                                        #   in Loop: Header=BB26_1 Depth=1
	movl	8(%r13), %eax
	incl	%eax
	movl	%eax, 8(%r13)
	cmpl	12(%r13), %eax
	jne	.LBB26_21
# BB#20:                                #   in Loop: Header=BB26_1 Depth=1
	movq	%r13, %rdi
	callq	MatchFinder_CheckLimits
.LBB26_21:                              # %MatchFinder_MovePos.exit
                                        #   in Loop: Header=BB26_1 Depth=1
	decl	%r15d
	jne	.LBB26_1
# BB#22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	Bt4_MatchFinder_Skip, .Lfunc_end26-Bt4_MatchFinder_Skip
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
