	.text
	.file	"ackermann.bc"
	.globl	_Z3Ackii
	.p2align	4, 0x90
	.type	_Z3Ackii,@function
_Z3Ackii:                               # @_Z3Ackii
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rdi), %ebx
	testl	%esi, %esi
	je	.LBB0_2
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	decl	%esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	_Z3Ackii
	movl	%eax, %esi
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %esi
.LBB0_3:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	movl	%ebx, %edi
	jne	.LBB0_1
.LBB0_4:                                # %tailrecurse._crit_edge
	incl	%esi
	movl	%esi, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_Z3Ackii, .Lfunc_end0-_Z3Ackii
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movl	$12, %r14d
	cmpl	$2, %edi
	jne	.LBB1_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB1_2:
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$6, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	%r14d, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$3, %edi
	movl	%r14d, %esi
	callq	_Z3Ackii
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_ZNSolsEi
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_7
# BB#3:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB1_5
# BB#4:
	movb	67(%rbx), %al
	jmp	.LBB1_6
.LBB1_5:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB1_6:                                # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_7:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ackermann.ii,@function
_GLOBAL__sub_I_ackermann.ii:            # @_GLOBAL__sub_I_ackermann.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_ackermann.ii, .Lfunc_end2-_GLOBAL__sub_I_ackermann.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Ack(3,"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"): "
	.size	.L.str.1, 4

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ackermann.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
