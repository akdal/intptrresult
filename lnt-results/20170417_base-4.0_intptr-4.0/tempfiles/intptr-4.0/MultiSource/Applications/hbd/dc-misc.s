	.text
	.file	"dc-misc.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	39                      # 0x27
.LCPI0_1:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	8                       # 0x8
	.long	15                      # 0xf
	.text
	.globl	_Z11docheckcastP9Classfile
	.p2align	4, 0x90
	.type	_Z11docheckcastP9Classfile,@function
_Z11docheckcastP9Classfile:             # @_Z11docheckcastP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	currpc(%rip), %r13d
	leal	2(%r13), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shll	$8, %ecx
	movzbl	1(%rax), %ebp
	orl	%ecx, %ebp
	movq	stkptr(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	-8(%rax), %r12
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	40(%rbx), %rax
	movzwl	%bp, %ebp
	movq	%rbp, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %r15
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#1:                                 # %.noexc
	decl	%r13d
	movq	%r15, (%rbx)
	movl	$8, 8(%rbx)
	movl	$2, 12(%rbx)
	movl	%ebp, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%r13d, 16(%r14)
	movl	%r13d, 12(%r14)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
.Ltmp3:
# BB#2:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,8,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	cmpl	%r13d, %eax
	cmovael	%r13d, %eax
	movl	$1, 8(%rbx)
	movl	%r13d, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#3:
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,2,8,15]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, -8(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp7:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB0_6
.LBB0_4:
.Ltmp4:
	movq	%rax, %r15
	movq	%r14, %rdi
.LBB0_6:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z11docheckcastP9Classfile, .Lfunc_end0-_Z11docheckcastP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	39                      # 0x27
.LCPI1_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	37                      # 0x25
	.text
	.globl	_Z12doinstanceofP9Classfile
	.p2align	4, 0x90
	.type	_Z12doinstanceofP9Classfile,@function
_Z12doinstanceofP9Classfile:            # @_Z12doinstanceofP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	currpc(%rip), %r13d
	leal	2(%r13), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shll	$8, %ecx
	movzbl	1(%rax), %ebp
	orl	%ecx, %ebp
	movq	stkptr(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	-8(%rax), %r12
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	40(%rbx), %rax
	movzwl	%bp, %ebp
	movq	%rbp, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %r15
.Ltmp8:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp9:
# BB#1:                                 # %.noexc
	decl	%r13d
	movq	%r15, (%rbx)
	movl	$8, 8(%rbx)
	movl	$2, 12(%rbx)
	movl	%ebp, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%r13d, 16(%r14)
	movl	%r13d, 12(%r14)
.Ltmp10:
	movl	$24, %edi
	callq	_Znwm
.Ltmp11:
# BB#2:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,8,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	movl	$1, 8(%rbx)
	movl	%r13d, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp13:
	movl	$24, %edi
	callq	_Znwm
.Ltmp14:
# BB#3:
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [0,4,10,37]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, -8(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_5:
.Ltmp15:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB1_6
.LBB1_4:
.Ltmp12:
	movq	%rax, %r15
	movq	%r14, %rdi
.LBB1_6:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z12doinstanceofP9Classfile, .Lfunc_end1-_Z12doinstanceofP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp8-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
