	.text
	.file	"spatscal.bc"
	.globl	Spatial_Prediction
	.p2align	4, 0x90
	.type	Spatial_Prediction,@function
Spatial_Prediction:                     # @Spatial_Prediction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$0, Frame_Store_Flag(%rip)
	movl	lower_layer_prediction_horizontal_size(%rip), %esi
	movl	lower_layer_prediction_vertical_size(%rip), %edx
	je	.LBB0_2
# BB#1:
	xorl	%edi, %edi
	callq	Read_Lower_Layer_Component_Framewise
	movl	lower_layer_prediction_horizontal_size(%rip), %esi
	sarl	%esi
	movl	lower_layer_prediction_vertical_size(%rip), %edx
	sarl	%edx
	movl	$1, %edi
	callq	Read_Lower_Layer_Component_Framewise
	movl	lower_layer_prediction_horizontal_size(%rip), %esi
	sarl	%esi
	movl	lower_layer_prediction_vertical_size(%rip), %edx
	sarl	%edx
	movl	$2, %edi
	callq	Read_Lower_Layer_Component_Framewise
	jmp	.LBB0_3
.LBB0_2:
	xorl	%edi, %edi
	callq	Read_Lower_Layer_Component_Fieldwise
	movl	lower_layer_prediction_horizontal_size(%rip), %esi
	sarl	%esi
	movl	lower_layer_prediction_vertical_size(%rip), %edx
	sarl	%edx
	movl	$1, %edi
	callq	Read_Lower_Layer_Component_Fieldwise
	movl	lower_layer_prediction_horizontal_size(%rip), %esi
	sarl	%esi
	movl	lower_layer_prediction_vertical_size(%rip), %edx
	sarl	%edx
	movl	$2, %edi
	callq	Read_Lower_Layer_Component_Fieldwise
.LBB0_3:
	movl	progressive_frame(%rip), %edi
	movl	lower_layer_progressive_frame(%rip), %esi
	movq	llframe0(%rip), %rdx
	movq	llframe1(%rip), %rcx
	movq	lltmp(%rip), %r8
	movq	current_frame(%rip), %r9
	movl	lower_layer_horizontal_offset(%rip), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	lower_layer_vertical_offset(%rip), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	lower_layer_prediction_horizontal_size(%rip), %r14d
	movl	lower_layer_prediction_vertical_size(%rip), %r15d
	movl	horizontal_size(%rip), %r12d
	movl	vertical_size(%rip), %r13d
	movl	vertical_subsampling_factor_m(%rip), %r11d
	xorl	%ebx, %ebx
	cmpl	$3, picture_structure(%rip)
	movl	vertical_subsampling_factor_n(%rip), %ebp
	setne	%bl
	movl	horizontal_subsampling_factor_m(%rip), %r10d
	movl	horizontal_subsampling_factor_n(%rip), %eax
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	Make_Spatial_Prediction_Frame
	addq	$96, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -96
	movl	progressive_frame(%rip), %edi
	movl	lower_layer_progressive_frame(%rip), %esi
	movq	llframe0+8(%rip), %rdx
	movq	llframe1+8(%rip), %rcx
	movq	lltmp(%rip), %r8
	movq	current_frame+8(%rip), %r9
	movl	lower_layer_horizontal_offset(%rip), %eax
	movl	%eax, %r10d
	shrl	$31, %r10d
	addl	%eax, %r10d
	sarl	%r10d
	movl	lower_layer_vertical_offset(%rip), %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	lower_layer_prediction_horizontal_size(%rip), %r11d
	sarl	%r11d
	movl	lower_layer_prediction_vertical_size(%rip), %r14d
	sarl	%r14d
	movl	horizontal_size(%rip), %r12d
	sarl	%r12d
	movl	vertical_size(%rip), %ebp
	sarl	%ebp
	movl	vertical_subsampling_factor_m(%rip), %ebx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	vertical_subsampling_factor_n(%rip), %ebx
	movl	horizontal_subsampling_factor_m(%rip), %r13d
	movl	horizontal_subsampling_factor_n(%rip), %r15d
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	callq	Make_Spatial_Prediction_Frame
	addq	$96, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset -96
	movl	progressive_frame(%rip), %edi
	movl	lower_layer_progressive_frame(%rip), %esi
	movq	llframe0+16(%rip), %rdx
	movq	llframe1+16(%rip), %rcx
	movq	lltmp(%rip), %r8
	movq	current_frame+16(%rip), %r9
	movl	lower_layer_horizontal_offset(%rip), %eax
	movl	%eax, %r10d
	shrl	$31, %r10d
	addl	%eax, %r10d
	sarl	%r10d
	movl	lower_layer_vertical_offset(%rip), %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	lower_layer_prediction_horizontal_size(%rip), %r11d
	sarl	%r11d
	movl	lower_layer_prediction_vertical_size(%rip), %r14d
	sarl	%r14d
	movl	horizontal_size(%rip), %r12d
	sarl	%r12d
	movl	vertical_size(%rip), %ebp
	sarl	%ebp
	movl	vertical_subsampling_factor_m(%rip), %ebx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	vertical_subsampling_factor_n(%rip), %ebx
	movl	horizontal_subsampling_factor_m(%rip), %r13d
	movl	horizontal_subsampling_factor_n(%rip), %r15d
	subq	$8, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	callq	Make_Spatial_Prediction_Frame
	addq	$120, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset -96
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Spatial_Prediction, .Lfunc_end0-Spatial_Prediction
	.cfi_endproc

	.p2align	4, 0x90
	.type	Read_Lower_Layer_Component_Framewise,@function
Read_Lower_Layer_Component_Framewise:   # @Read_Lower_Layer_Component_Framewise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi58:
	.cfi_def_cfa_offset 368
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r14d
	movl	%edi, %ebp
	movb	.LRead_Lower_Layer_Component_Fieldwise.ext+8(%rip), %al
	movb	%al, 40(%rsp)
	movq	.LRead_Lower_Layer_Component_Fieldwise.ext(%rip), %rax
	movq	%rax, 32(%rsp)
	movq	Lower_Layer_Picture_Filename(%rip), %rsi
	movl	True_Framenum(%rip), %edx
	leaq	48(%rsp), %r15
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sprintf
	movslq	%ebp, %rbx
	leaq	(%rbx,%rbx,2), %rax
	leaq	32(%rsp,%rax), %rsi
	movq	%r15, %rdi
	callq	strcat
	movl	$.L.str, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_11
# BB#1:                                 # %.preheader29
	testl	%r13d, %r13d
	jle	.LBB1_14
# BB#2:                                 # %.preheader.lr.ph
	testl	%r14d, %r14d
	jle	.LBB1_12
# BB#3:                                 # %.preheader.us.preheader
	movl	%r14d, %r15d
	xorl	%eax, %eax
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_8 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r14d, %eax
	movslq	%eax, %r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_IO_getc
	movq	llframe0(,%rbx,8), %rcx
	addq	%r12, %rcx
	movb	%al, (%r14,%rcx)
	leaq	1(%r14), %rax
	cmpq	%rax, %r15
	jne	.LBB1_5
# BB#6:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	$0, lower_layer_progressive_frame(%rip)
	jne	.LBB1_10
# BB#7:                                 # %.lr.ph33.us
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	incl	%r13d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	$-1, %r13
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movq	llframe1(,%rbx,8), %rcx
	addq	%r12, %rcx
	movb	%al, 1(%r13,%rcx)
	incq	%r13
	cmpq	%r13, %r14
	jne	.LBB1_8
# BB#9:                                 #   in Loop: Header=BB1_4 Depth=1
	movl	28(%rsp), %r13d         # 4-byte Reload
.LBB1_10:                               # %.loopexit.us
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	%r13d, %eax
	movl	12(%rsp), %r14d         # 4-byte Reload
	jl	.LBB1_4
	jmp	.LBB1_14
.LBB1_12:                               # %.preheader.preheader
	xorl	%eax, %eax
	cmpl	$0, lower_layer_progressive_frame(%rip)
	sete	%al
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rax), %ecx
	cmpl	%r13d, %ecx
	jl	.LBB1_13
.LBB1_14:                               # %._crit_edge35
	movq	%rbp, %rdi
	callq	fclose
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_11:
	movl	$-1, %edi
	callq	exit
.Lfunc_end1:
	.size	Read_Lower_Layer_Component_Framewise, .Lfunc_end1-Read_Lower_Layer_Component_Framewise
	.cfi_endproc

	.p2align	4, 0x90
	.type	Read_Lower_Layer_Component_Fieldwise,@function
Read_Lower_Layer_Component_Fieldwise:   # @Read_Lower_Layer_Component_Fieldwise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi71:
	.cfi_def_cfa_offset 368
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r15d
	movl	%edi, %r14d
	movb	.LRead_Lower_Layer_Component_Fieldwise.ext+8(%rip), %al
	movb	%al, 40(%rsp)
	movq	.LRead_Lower_Layer_Component_Fieldwise.ext(%rip), %rax
	movq	%rax, 32(%rsp)
	movq	Lower_Layer_Picture_Filename(%rip), %rsi
	movl	True_Framenum(%rip), %edx
	xorl	%eax, %eax
	cmpl	$0, lower_layer_progressive_frame(%rip)
	setne	%al
	leal	97(%rax,%rax,4), %ecx
	leaq	48(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movslq	%r14d, %rbp
	leaq	(%rbp,%rbp,2), %rax
	leaq	32(%rsp,%rax), %rsi
	movq	%rbx, %rdi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	strcat
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_7
# BB#1:                                 # %.preheader36
	testl	%r13d, %r13d
	jle	.LBB2_10
# BB#2:                                 # %.preheader35.lr.ph
	testl	%r15d, %r15d
	jle	.LBB2_8
# BB#3:                                 # %.preheader35.us.preheader
	movl	%r15d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movl	%r13d, 24(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader35.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movl	%r14d, %eax
	movl	%r15d, %r12d
	imull	%r15d, %eax
	movslq	%eax, %r15
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_5:                                #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movq	llframe0(,%rbp,8), %rcx
	movb	%al, (%rcx,%r15)
	incq	%r15
	decq	%r13
	jne	.LBB2_5
# BB#6:                                 # %._crit_edge42.us
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpl	$1, lower_layer_progressive_frame(%rip)
	adcl	$0, %r14d
	incl	%r14d
	movl	24(%rsp), %r13d         # 4-byte Reload
	cmpl	%r13d, %r14d
	movl	%r12d, %r15d
	jl	.LBB2_4
	jmp	.LBB2_10
.LBB2_8:                                # %.preheader35.preheader
	cmpl	$1, lower_layer_progressive_frame(%rip)
	movl	$1, %eax
	adcl	$0, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader35
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %ecx
	cmpl	%r13d, %ecx
	jl	.LBB2_9
.LBB2_10:                               # %._crit_edge44
	movq	%rbx, %rdi
	callq	fclose
	cmpl	$0, lower_layer_progressive_frame(%rip)
	jne	.LBB2_19
# BB#11:
	movq	Lower_Layer_Picture_Filename(%rip), %rsi
	movl	True_Framenum(%rip), %edx
	leaq	48(%rsp), %rbx
	movl	$98, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	strcat
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_20
# BB#12:                                # %.preheader34
	cmpl	$2, %r13d
	jl	.LBB2_18
# BB#13:                                # %.preheader.lr.ph
	testl	%r15d, %r15d
	jle	.LBB2_18
# BB#14:                                # %.preheader.us.preheader
	movslq	%r13d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%r15d, %r14
	movl	%r15d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	leaq	(%r14,%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movq	llframe1(,%rbp,8), %rcx
	movb	%al, (%rcx,%r12)
	incq	%r12
	decq	%r15
	jne	.LBB2_16
# BB#17:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_15 Depth=1
	addq	$2, %r13
	addq	8(%rsp), %r14           # 8-byte Folded Reload
	cmpq	24(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB2_15
.LBB2_18:                               # %._crit_edge39
	movq	%rbx, %rdi
	callq	fclose
.LBB2_19:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_7:
	movl	$-1, %edi
	callq	exit
.LBB2_20:
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	Read_Lower_Layer_Component_Fieldwise, .Lfunc_end2-Read_Lower_Layer_Component_Fieldwise
	.cfi_endproc

	.p2align	4, 0x90
	.type	Make_Spatial_Prediction_Frame,@function
Make_Spatial_Prediction_Frame:          # @Make_Spatial_Prediction_Frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 192
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rdx, %r12
	movl	264(%rsp), %eax
	movl	248(%rsp), %r8d
	movl	216(%rsp), %ebx
	imull	208(%rsp), %eax
	cltd
	idivl	256(%rsp)
	movl	%eax, %ebp
	movl	%r8d, %eax
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	imull	%ebx, %eax
	cltd
	idivl	240(%rsp)
	testl	%esi, %esi
	movq	%r12, 80(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%ebp, 116(%rsp)         # 4-byte Spill
	je	.LBB3_20
# BB#1:
	testl	%eax, %eax
	jle	.LBB3_98
# BB#2:                                 # %.lr.ph53.i
	cmpl	$0, 208(%rsp)
	jle	.LBB3_98
# BB#3:                                 # %.lr.ph53.split.us.preheader.i
	decl	72(%rsp)                # 4-byte Folded Spill
	movl	208(%rsp), %eax
	movslq	%eax, %r13
	movl	248(%rsp), %ecx
	movl	%ecx, %r15d
	sarl	%r15d
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	leaq	(%r12,%rbx), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rbx, %rcx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%r13,%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	1(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	pxor	%xmm8, %xmm8
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	%r15, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph53.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
                                        #     Child Loop BB3_18 Depth 2
	movq	%r13, %rcx
	imulq	%r8, %rcx
	movl	%r8d, %eax
	imull	240(%rsp), %eax
	cltd
	movl	248(%rsp), %esi
	movl	%esi, %r10d
	idivl	%r10d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, %esi
	movl	208(%rsp), %edi
	movl	%edi, %ebp
	imull	%ebp, %esi
	movslq	%esi, %r9
	leaq	(%r12,%r9), %r14
	leaq	(%r14,%r13), %rdi
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	cmovgeq	%r14, %rdi
	shll	$4, %edx
	leal	(%rdx,%r15), %eax
	cltd
	idivl	%r10d
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$8, %ebp
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,2), %r10
	jae	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_6:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB3_7
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB3_4 Depth=1
	addq	%rbx, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,2), %rsi
	leaq	(%rdi,%rbx), %rcx
	movq	96(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r9), %rbp
	cmpq	%rcx, %r10
	sbbb	%r15b, %r15b
	cmpq	%rsi, %rdi
	sbbb	%cl, %cl
	andb	%r15b, %cl
	movq	%r10, 24(%rsp)          # 8-byte Spill
	cmpq	%rbp, %r10
	sbbb	%r10b, %r10b
	cmpq	%rsi, %r14
	sbbb	%sil, %sil
	testb	$1, %cl
	jne	.LBB3_9
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_4 Depth=1
	andb	%sil, %r10b
	andb	$1, %r10b
	movl	$0, %ecx
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	jne	.LBB3_14
# BB#11:                                # %vector.ph
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	%r13, %rsi
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	%r11, %r12
	movq	%rdi, %r15
	movq	%r14, %r10
	.p2align	4, 0x90
.LBB3_12:                               # %vector.body
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10), %xmm3           # xmm3 = mem[0],zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movq	(%r15), %xmm5           # xmm5 = mem[0],zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	punpckhwd	%xmm8, %xmm5    # xmm5 = xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm4, %xmm6
	paddd	%xmm3, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm5, %xmm6
	movdqu	%xmm6, (%r12)
	addq	$8, %r10
	addq	$8, %r15
	addq	$16, %r12
	addq	$-8, %r13
	jne	.LBB3_12
# BB#13:                                # %middle.block
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	%rsi, %r13
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	jne	.LBB3_14
	jmp	.LBB3_19
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB3_14
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=1
	xorl	%ecx, %ecx
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_14:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	%ebx, %esi
	subl	%ecx, %esi
	testb	$1, %sil
	movq	%rcx, %rsi
	je	.LBB3_16
# BB#15:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB3_4 Depth=1
	movzbl	(%r14,%rcx), %esi
	imull	%edx, %esi
	movzbl	(%rdi,%rcx), %ebp
	imull	%eax, %ebp
	addl	%esi, %ebp
	movw	%bp, (%r10,%rcx,2)
	leaq	1(%rcx), %rsi
.LBB3_16:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpq	%rcx, 64(%rsp)          # 8-byte Folded Reload
	je	.LBB3_19
# BB#17:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_4 Depth=1
	addq	16(%rsp), %r9           # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB3_18:                               # %scalar.ph
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r9,%rsi), %ecx
	imull	%edx, %ecx
	movzbl	(%rdi,%rsi), %ebp
	imull	%eax, %ebp
	addl	%ecx, %ebp
	movw	%bp, (%r11,%rsi,2)
	movzbl	(%r9,%rsi), %ecx
	imull	%edx, %ecx
	movzbl	1(%rdi,%rsi), %ebp
	imull	%eax, %ebp
	addl	%ecx, %ebp
	movw	%bp, 2(%r11,%rsi,2)
	addq	$2, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB3_18
.LBB3_19:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB3_4 Depth=1
	incq	%r8
	addq	88(%rsp), %r11          # 8-byte Folded Reload
	cmpq	40(%rsp), %r8           # 8-byte Folded Reload
	jne	.LBB3_4
.LBB3_98:                               # %Subsample_Vertical.exit
	movl	232(%rsp), %edx
	movl	224(%rsp), %eax
	movl	192(%rsp), %ecx
	movl	200(%rsp), %edi
	testl	%edi, %edi
	js	.LBB3_99
# BB#100:
	movl	%eax, %esi
	imull	%edi, %esi
	movslq	%esi, %rsi
	addq	%rsi, 128(%rsp)         # 8-byte Folded Spill
	subl	%edi, %edx
	movl	12(%rsp), %r11d         # 4-byte Reload
	cmpl	%r11d, %edx
	cmovlel	%edx, %r11d
	jmp	.LBB3_101
.LBB3_20:
	movl	272(%rsp), %r15d
	testl	%edi, %edi
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	je	.LBB3_60
# BB#21:
	cmpl	$0, lower_layer_deinterlaced_field_select(%rip)
	je	.LBB3_41
# BB#22:
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movl	208(%rsp), %ecx
	movl	216(%rsp), %r8d
	movl	%r15d, %r9d
	movl	%eax, %ebx
	callq	Deinterlace
	movq	64(%rsp), %r10          # 8-byte Reload
	testl	%ebx, %ebx
	jle	.LBB3_98
# BB#23:                                # %.lr.ph53.i149
	cmpl	$0, 208(%rsp)
	jle	.LBB3_98
# BB#24:                                # %.lr.ph53.split.us.preheader.i151
	decl	72(%rsp)                # 4-byte Folded Spill
	movl	208(%rsp), %eax
	movslq	%eax, %r13
	movl	248(%rsp), %ecx
	movl	%ecx, %r8d
	sarl	%r8d
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	leaq	(%r10,%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rbx, %rcx
	movq	%rax, 96(%rsp)          # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	(%r13,%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	1(%r10), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	pxor	%xmm8, %xmm8
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph53.split.us.i153
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_33 Depth 2
                                        #     Child Loop BB3_39 Depth 2
	movq	%r13, %rsi
	imulq	%r9, %rsi
	movl	%r9d, %eax
	imull	240(%rsp), %eax
	cltd
	movl	248(%rsp), %ecx
	movl	%ecx, %ebp
	idivl	%ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, %ecx
	movl	208(%rsp), %edi
	imull	%edi, %ecx
	movslq	%ecx, %r12
	leaq	(%r10,%r12), %r14
	leaq	(%r14,%r13), %r15
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	cmovgeq	%r14, %r15
	shll	$4, %edx
	leal	(%rdx,%r8), %eax
	cltd
	idivl	%ebp
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$8, %edi
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,2), %rbp
	jae	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_25 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_35
	.p2align	4, 0x90
.LBB3_27:                               # %min.iters.checked203
                                        #   in Loop: Header=BB3_25 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB3_28
# BB#29:                                # %vector.memcheck223
                                        #   in Loop: Header=BB3_25 Depth=1
	addq	%rbx, %rsi
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,2), %rsi
	leaq	(%r15,%rbx), %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r12), %rdi
	cmpq	%rcx, %rbp
	sbbb	%r8b, %r8b
	cmpq	%rsi, %r15
	sbbb	%cl, %cl
	andb	%r8b, %cl
	cmpq	%rdi, %rbp
	sbbb	%dil, %dil
	cmpq	%rsi, %r14
	sbbb	%sil, %sil
	testb	$1, %cl
	jne	.LBB3_30
# BB#31:                                # %vector.memcheck223
                                        #   in Loop: Header=BB3_25 Depth=1
	andb	%sil, %dil
	andb	$1, %dil
	movl	$0, %edi
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB3_35
# BB#32:                                # %vector.ph224
                                        #   in Loop: Header=BB3_25 Depth=1
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r11, %r8
	movq	%r15, %rdi
	movq	%r14, %r10
	.p2align	4, 0x90
.LBB3_33:                               # %vector.body199
                                        #   Parent Loop BB3_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10), %xmm3           # xmm3 = mem[0],zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movq	(%rdi), %xmm5           # xmm5 = mem[0],zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	punpckhwd	%xmm8, %xmm5    # xmm5 = xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm4, %xmm6
	paddd	%xmm3, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm5, %xmm6
	movdqu	%xmm6, (%r8)
	addq	$8, %r10
	addq	$8, %rdi
	addq	$16, %r8
	addq	$-8, %rsi
	jne	.LBB3_33
# BB#34:                                # %middle.block200
                                        #   in Loop: Header=BB3_25 Depth=1
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB3_35
	jmp	.LBB3_40
.LBB3_28:                               #   in Loop: Header=BB3_25 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_35
.LBB3_30:                               #   in Loop: Header=BB3_25 Depth=1
	xorl	%edi, %edi
	movq	16(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_35:                               # %scalar.ph201.preheader
                                        #   in Loop: Header=BB3_25 Depth=1
	movl	%ebx, %ecx
	subl	%edi, %ecx
	testb	$1, %cl
	movq	%rdi, %rsi
	je	.LBB3_37
# BB#36:                                # %scalar.ph201.prol
                                        #   in Loop: Header=BB3_25 Depth=1
	movzbl	(%r14,%rdi), %ecx
	imull	%edx, %ecx
	movzbl	(%r15,%rdi), %esi
	imull	%eax, %esi
	addl	%ecx, %esi
	movw	%si, (%rbp,%rdi,2)
	leaq	1(%rdi), %rsi
.LBB3_37:                               # %scalar.ph201.prol.loopexit
                                        #   in Loop: Header=BB3_25 Depth=1
	cmpq	%rdi, 48(%rsp)          # 8-byte Folded Reload
	je	.LBB3_40
# BB#38:                                # %scalar.ph201.preheader.new
                                        #   in Loop: Header=BB3_25 Depth=1
	addq	80(%rsp), %r12          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB3_39:                               # %scalar.ph201
                                        #   Parent Loop BB3_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r12,%rsi), %ecx
	imull	%edx, %ecx
	movzbl	(%r15,%rsi), %edi
	imull	%eax, %edi
	addl	%ecx, %edi
	movw	%di, (%r11,%rsi,2)
	movzbl	(%r12,%rsi), %ecx
	imull	%edx, %ecx
	movzbl	1(%r15,%rsi), %edi
	imull	%eax, %edi
	addl	%ecx, %edi
	movw	%di, 2(%r11,%rsi,2)
	addq	$2, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB3_39
.LBB3_40:                               # %._crit_edge.us.i158
                                        #   in Loop: Header=BB3_25 Depth=1
	incq	%r9
	addq	88(%rsp), %r11          # 8-byte Folded Reload
	cmpq	40(%rsp), %r9           # 8-byte Folded Reload
	jne	.LBB3_25
	jmp	.LBB3_98
.LBB3_99:
	movl	208(%rsp), %esi
	imull	%edi, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	subq	%rsi, 32(%rsp)          # 8-byte Folded Spill
	xorl	%esi, %esi
	movl	12(%rsp), %r11d         # 4-byte Reload
	addl	%edi, %r11d
	cmovsl	%esi, %r11d
	cmpl	%edx, %r11d
	cmovgl	%edx, %r11d
.LBB3_101:
	movl	116(%rsp), %ebp         # 4-byte Reload
	testl	%ecx, %ecx
	js	.LBB3_102
# BB#103:
	movslq	%ecx, %rdx
	addq	%rdx, 128(%rsp)         # 8-byte Folded Spill
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	%ebp, %edx
	cmovlel	%edx, %ebp
	xorl	%ecx, %ecx
	jmp	.LBB3_104
.LBB3_102:
	movl	%ecx, %edx
	negl	%edx
	xorl	%esi, %esi
	addl	%ecx, %ebp
	cmovsl	%esi, %ebp
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	movslq	%edx, %rcx
.LBB3_104:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB3_114
# BB#105:                               # %.lr.ph65.i
	testl	%r11d, %r11d
	jle	.LBB3_114
# BB#106:                               # %.lr.ph65.split.us.preheader.i
	movl	264(%rsp), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	sarl	%ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movslq	%eax, %rdi
	movl	208(%rsp), %eax
	movslq	%eax, %rcx
	decl	%eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%ebp, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	andl	$1, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	leaq	(%rcx,%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	(,%rcx,4), %r13
	xorl	%ecx, %ecx
	movl	%r11d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB3_107:                              # %.lr.ph65.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_112 Depth 2
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leal	(%rcx,%rax), %eax
	imull	256(%rsp), %eax
	cltd
	movl	264(%rsp), %ecx
	movl	%ecx, %esi
	idivl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %r15
	cmpl	88(%rsp), %eax          # 4-byte Folded Reload
	leaq	2(%rcx,%rax,2), %r12
	cmovgeq	%r15, %r12
	shll	$4, %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %eax
	cltd
	idivl	%esi
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_109
# BB#108:                               #   in Loop: Header=BB3_107 Depth=1
	xorl	%r10d, %r10d
	cmpl	$1, %r11d
	jne	.LBB3_111
	jmp	.LBB3_113
	.p2align	4, 0x90
.LBB3_109:                              #   in Loop: Header=BB3_107 Depth=1
	movswl	(%r15), %r8d
	imull	%edx, %r8d
	movswl	(%r12), %r9d
	imull	%eax, %r9d
	addl	%r8d, %r9d
	movl	%r9d, %ebx
	sarl	$31, %ebx
	leal	128(%rbx,%r9), %ebx
	movb	%bh, (%rbp)  # NOREX
	addq	%rdi, %rbp
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%r15,%rcx,2), %r15
	leaq	(%r12,%rcx,2), %r12
	movl	$1, %r10d
	cmpl	$1, %r11d
	je	.LBB3_113
.LBB3_111:                              # %.lr.ph65.split.us.i.new
                                        #   in Loop: Header=BB3_107 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%r12,%rcx), %rbx
	leaq	(%r15,%rcx), %r8
	movl	12(%rsp), %r9d          # 4-byte Reload
	subl	%r10d, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_112:                              #   Parent Loop BB3_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%r15,%r10), %r11d
	imull	%edx, %r11d
	movswl	(%r12,%r10), %r14d
	imull	%eax, %r14d
	addl	%r11d, %r14d
	movl	%r14d, %ecx
	sarl	$31, %ecx
	leal	128(%rcx,%r14), %ecx
	movb	%ch, (%rbp)  # NOREX
	movswl	(%r8,%r10), %ecx
	imull	%edx, %ecx
	movswl	(%rbx,%r10), %esi
	imull	%eax, %esi
	addl	%ecx, %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	leal	128(%rcx,%rsi), %ecx
	movb	%ch, (%rbp,%rdi)  # NOREX
	leaq	(%rbp,%rdi), %rbp
	addq	%rdi, %rbp
	addq	%r13, %r10
	addl	$-2, %r9d
	jne	.LBB3_112
.LBB3_113:                              # %._crit_edge.us.i126
                                        #   in Loop: Header=BB3_107 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	movl	12(%rsp), %r11d         # 4-byte Reload
	jne	.LBB3_107
.LBB3_114:                              # %Subsample_Horizontal.exit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_60:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rcx, %rsi
	movl	208(%rsp), %r14d
	movl	%r14d, %ecx
	movl	216(%rsp), %ebx
	movl	%ebx, %r8d
	movl	%r15d, %r9d
	movl	%eax, %ebp
	callq	Deinterlace
	xorl	%edx, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	movl	%r15d, %r9d
	callq	Deinterlace
	testl	%ebp, %ebp
	jle	.LBB3_98
# BB#61:                                # %.lr.ph53.i138
	decl	72(%rsp)                # 4-byte Folded Spill
	movl	208(%rsp), %eax
	movslq	%eax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	248(%rsp), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	sarl	%ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB3_79
# BB#62:                                # %.lr.ph53.split.us.preheader.i140
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	movl	208(%rsp), %eax
	movl	%eax, %r13d
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r13), %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	leaq	-1(%r13), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%r13, %rsi
	movq	%rax, 120(%rsp)         # 8-byte Spill
	subq	%rax, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(,%rdx,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	pxor	%xmm8, %xmm8
	movq	32(%rsp), %rsi          # 8-byte Reload
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_63:                               # %.lr.ph53.split.us.i142
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_71 Depth 2
                                        #     Child Loop BB3_77 Depth 2
	movl	%r10d, %eax
	imull	240(%rsp), %eax
	cltd
	movl	248(%rsp), %ecx
	movl	%ecx, %ebp
	idivl	%ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, %ecx
	movl	208(%rsp), %edi
	imull	%edi, %ecx
	movslq	%ecx, %rbx
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbx), %r15
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%r15,%rcx), %r11
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	cmovgeq	%r15, %r11
	shll	$4, %edx
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %eax
	cltd
	idivl	%ebp
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$8, %edi
	jae	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_63 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_73
	.p2align	4, 0x90
.LBB3_65:                               # %min.iters.checked281
                                        #   in Loop: Header=BB3_63 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB3_66
# BB#67:                                # %vector.memcheck301
                                        #   in Loop: Header=BB3_63 Depth=1
	movq	%r14, %r12
	movq	96(%rsp), %rcx          # 8-byte Reload
	imulq	%r8, %rcx
	leaq	(%r13,%rcx), %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rcx,2), %rcx
	leaq	(%rbp,%rdi,2), %rdi
	leaq	(%r11,%r13), %r9
	movq	104(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx), %r14
	cmpq	%r9, %rcx
	sbbb	%bpl, %bpl
	cmpq	%rdi, %r11
	sbbb	%r9b, %r9b
	andb	%bpl, %r9b
	cmpq	%r14, %rcx
	sbbb	%cl, %cl
	cmpq	%rdi, %r15
	sbbb	%dil, %dil
	testb	$1, %r9b
	jne	.LBB3_68
# BB#69:                                # %vector.memcheck301
                                        #   in Loop: Header=BB3_63 Depth=1
	andb	%dil, %cl
	andb	$1, %cl
	movl	$0, %edi
	movq	%r12, %r14
	jne	.LBB3_73
# BB#70:                                # %vector.ph302
                                        #   in Loop: Header=BB3_63 Depth=1
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rsi, %rbp
	movq	%r11, %rcx
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB3_71:                               # %vector.body277
                                        #   Parent Loop BB3_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %xmm3           # xmm3 = mem[0],zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movq	(%rcx), %xmm5           # xmm5 = mem[0],zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	punpckhwd	%xmm8, %xmm5    # xmm5 = xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm4, %xmm6
	paddd	%xmm3, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm5, %xmm6
	movdqu	%xmm6, (%rbp)
	addq	$8, %r14
	addq	$8, %rcx
	addq	$16, %rbp
	addq	$-8, %rdi
	jne	.LBB3_71
# BB#72:                                # %middle.block278
                                        #   in Loop: Header=BB3_63 Depth=1
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %r14
	jne	.LBB3_73
	jmp	.LBB3_78
.LBB3_66:                               #   in Loop: Header=BB3_63 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_73
.LBB3_68:                               #   in Loop: Header=BB3_63 Depth=1
	xorl	%edi, %edi
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB3_73:                               # %scalar.ph279.preheader
                                        #   in Loop: Header=BB3_63 Depth=1
	movl	%r13d, %ecx
	subl	%edi, %ecx
	testb	$1, %cl
	movq	%rdi, %rcx
	je	.LBB3_75
# BB#74:                                # %scalar.ph279.prol
                                        #   in Loop: Header=BB3_63 Depth=1
	movq	%r10, %rcx
	imulq	40(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rcx,2), %r9
	movzbl	(%r15,%rdi), %ebp
	imull	%edx, %ebp
	movzbl	(%r11,%rdi), %ecx
	imull	%eax, %ecx
	addl	%ebp, %ecx
	movw	%cx, (%r9,%rdi,2)
	leaq	1(%rdi), %rcx
.LBB3_75:                               # %scalar.ph279.prol.loopexit
                                        #   in Loop: Header=BB3_63 Depth=1
	cmpq	%rdi, 24(%rsp)          # 8-byte Folded Reload
	je	.LBB3_78
# BB#76:                                # %scalar.ph279.preheader.new
                                        #   in Loop: Header=BB3_63 Depth=1
	addq	56(%rsp), %rbx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB3_77:                               # %scalar.ph279
                                        #   Parent Loop BB3_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbx,%rcx), %edi
	imull	%edx, %edi
	movzbl	(%r11,%rcx), %ebp
	imull	%eax, %ebp
	addl	%edi, %ebp
	movw	%bp, (%rsi,%rcx,2)
	movzbl	(%rbx,%rcx), %edi
	imull	%edx, %edi
	movzbl	1(%r11,%rcx), %ebp
	imull	%eax, %ebp
	addl	%edi, %ebp
	movw	%bp, 2(%rsi,%rcx,2)
	addq	$2, %rcx
	cmpq	%rcx, %r13
	jne	.LBB3_77
.LBB3_78:                               # %._crit_edge.us.i147
                                        #   in Loop: Header=BB3_63 Depth=1
	addq	$2, %r10
	incq	%r8
	addq	48(%rsp), %rsi          # 8-byte Folded Reload
	cmpq	%r14, %r10
	jl	.LBB3_63
.LBB3_79:                               # %Subsample_Vertical.exit148
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jl	.LBB3_98
# BB#80:                                # %.lr.ph53.i127
	cmpl	$0, 208(%rsp)
	jle	.LBB3_98
# BB#81:                                # %.lr.ph53.split.us.preheader.i129
	movslq	12(%rsp), %r15          # 4-byte Folded Reload
	movl	208(%rsp), %eax
	movl	%eax, %edi
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rdi), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rdi), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rdi), %r12
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rdi, %rcx
	movq	%rax, 120(%rsp)         # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,2), %r11
	leaq	(,%rdx,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	xorl	%r14d, %r14d
	pxor	%xmm8, %xmm8
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_82:                               # %.lr.ph53.split.us.i131
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_90 Depth 2
                                        #     Child Loop BB3_96 Depth 2
	movl	%r13d, %eax
	imull	240(%rsp), %eax
	cltd
	movl	248(%rsp), %ecx
	movl	%ecx, %r9d
	idivl	%r9d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, %ecx
	movl	208(%rsp), %esi
	imull	%esi, %ecx
	movslq	%ecx, %rbp
	leaq	(%rbx,%rbp), %r8
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%r8,%rcx), %r10
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	cmovgeq	%r8, %r10
	shll	$4, %edx
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %eax
	cltd
	idivl	%r9d
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$8, %esi
	jae	.LBB3_84
# BB#83:                                #   in Loop: Header=BB3_82 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_84:                               # %min.iters.checked320
                                        #   in Loop: Header=BB3_82 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB3_85
# BB#86:                                # %vector.memcheck342
                                        #   in Loop: Header=BB3_82 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	imulq	%r14, %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	addq	96(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rsi,2), %rsi
	leaq	(%rbx,%rcx,2), %rbx
	leaq	(%r10,%rdi), %r9
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbp), %r15
	cmpq	%r9, %rsi
	sbbb	%cl, %cl
	cmpq	%rbx, %r10
	sbbb	%r9b, %r9b
	andb	%cl, %r9b
	cmpq	%r15, %rsi
	sbbb	%cl, %cl
	cmpq	%rbx, %r8
	sbbb	%bl, %bl
	testb	$1, %r9b
	jne	.LBB3_87
# BB#88:                                # %vector.memcheck342
                                        #   in Loop: Header=BB3_82 Depth=1
	andb	%bl, %cl
	andb	$1, %cl
	movl	$0, %esi
	movq	16(%rsp), %r15          # 8-byte Reload
	jne	.LBB3_92
# BB#89:                                # %vector.ph343
                                        #   in Loop: Header=BB3_82 Depth=1
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r11, %rbx
	movq	%r10, %rcx
	movq	%r8, %r15
	.p2align	4, 0x90
.LBB3_90:                               # %vector.body316
                                        #   Parent Loop BB3_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %xmm3           # xmm3 = mem[0],zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movq	(%rcx), %xmm5           # xmm5 = mem[0],zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	punpckhwd	%xmm8, %xmm5    # xmm5 = xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm4, %xmm6
	paddd	%xmm3, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm5, %xmm6
	movdqu	%xmm6, (%rbx)
	addq	$8, %r15
	addq	$8, %rcx
	addq	$16, %rbx
	addq	$-8, %rsi
	jne	.LBB3_90
# BB#91:                                # %middle.block317
                                        #   in Loop: Header=BB3_82 Depth=1
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	jne	.LBB3_92
	jmp	.LBB3_97
.LBB3_85:                               #   in Loop: Header=BB3_82 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_92
.LBB3_87:                               #   in Loop: Header=BB3_82 Depth=1
	xorl	%esi, %esi
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_92:                               # %scalar.ph318.preheader
                                        #   in Loop: Header=BB3_82 Depth=1
	movl	%edi, %ecx
	subl	%esi, %ecx
	testb	$1, %cl
	movq	%rsi, %rcx
	je	.LBB3_94
# BB#93:                                # %scalar.ph318.prol
                                        #   in Loop: Header=BB3_82 Depth=1
	movq	%r13, %rcx
	imulq	40(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rcx,2), %r9
	movzbl	(%r8,%rsi), %ebx
	imull	%edx, %ebx
	movzbl	(%r10,%rsi), %ecx
	imull	%eax, %ecx
	addl	%ebx, %ecx
	movw	%cx, (%r9,%rsi,2)
	leaq	1(%rsi), %rcx
.LBB3_94:                               # %scalar.ph318.prol.loopexit
                                        #   in Loop: Header=BB3_82 Depth=1
	cmpq	%rsi, %r12
	je	.LBB3_97
# BB#95:                                # %scalar.ph318.preheader.new
                                        #   in Loop: Header=BB3_82 Depth=1
	addq	80(%rsp), %rbp          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB3_96:                               # %scalar.ph318
                                        #   Parent Loop BB3_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbp,%rcx), %esi
	imull	%edx, %esi
	movzbl	(%r10,%rcx), %ebx
	imull	%eax, %ebx
	addl	%esi, %ebx
	movw	%bx, (%r11,%rcx,2)
	movzbl	(%rbp,%rcx), %esi
	imull	%edx, %esi
	movzbl	1(%r10,%rcx), %ebx
	imull	%eax, %ebx
	addl	%esi, %ebx
	movw	%bx, 2(%r11,%rcx,2)
	addq	$2, %rcx
	cmpq	%rcx, %rdi
	jne	.LBB3_96
.LBB3_97:                               # %._crit_edge.us.i136
                                        #   in Loop: Header=BB3_82 Depth=1
	addq	$2, %r13
	incq	%r14
	addq	48(%rsp), %r11          # 8-byte Folded Reload
	cmpq	%r15, %r13
	movq	64(%rsp), %rbx          # 8-byte Reload
	jl	.LBB3_82
	jmp	.LBB3_98
.LBB3_41:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rcx, %rsi
	movl	208(%rsp), %ecx
	movl	216(%rsp), %r8d
	movl	%r15d, %r9d
	movl	%eax, %ebx
	callq	Deinterlace
	testl	%ebx, %ebx
	jle	.LBB3_98
# BB#42:                                # %.lr.ph53.i160
	cmpl	$0, 208(%rsp)
	jle	.LBB3_98
# BB#43:                                # %.lr.ph53.split.us.preheader.i162
	decl	72(%rsp)                # 4-byte Folded Spill
	movl	208(%rsp), %eax
	movslq	%eax, %r13
	movl	248(%rsp), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	sarl	%ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	12(%rsp), %r10          # 4-byte Folded Reload
	movl	%eax, %ebx
	leaq	(%r12,%rbx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$7, %eax
	movq	%rbx, %rcx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%r13,%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	1(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	pxor	%xmm8, %xmm8
	movq	32(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_44:                               # %.lr.ph53.split.us.i164
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_52 Depth 2
                                        #     Child Loop BB3_58 Depth 2
	movq	%r13, %rcx
	imulq	%r8, %rcx
	movl	%r8d, %eax
	imull	240(%rsp), %eax
	cltd
	movl	248(%rsp), %esi
	movl	%esi, %ebp
	idivl	%ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%eax, %esi
	movl	208(%rsp), %edi
	imull	%edi, %esi
	movslq	%esi, %r9
	leaq	(%r12,%r9), %r15
	leaq	(%r15,%r13), %r12
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	cmovgeq	%r15, %r12
	shll	$4, %edx
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %eax
	cltd
	idivl	%ebp
	movl	$16, %edx
	subl	%eax, %edx
	cmpl	$8, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,2), %rsi
	jae	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_44 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_54
	.p2align	4, 0x90
.LBB3_46:                               # %min.iters.checked242
                                        #   in Loop: Header=BB3_44 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB3_47
# BB#48:                                # %vector.memcheck262
                                        #   in Loop: Header=BB3_44 Depth=1
	movq	%r10, %r14
	addq	%rbx, %rcx
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,2), %rdi
	leaq	(%r12,%rbx), %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r9), %rbp
	cmpq	%rcx, %rsi
	sbbb	%r10b, %r10b
	cmpq	%rdi, %r12
	sbbb	%cl, %cl
	andb	%r10b, %cl
	cmpq	%rbp, %rsi
	sbbb	%r10b, %r10b
	cmpq	%rdi, %r15
	sbbb	%dil, %dil
	testb	$1, %cl
	jne	.LBB3_49
# BB#50:                                # %vector.memcheck262
                                        #   in Loop: Header=BB3_44 Depth=1
	andb	%dil, %r10b
	andb	$1, %r10b
	movl	$0, %edi
	movq	%r14, %r10
	jne	.LBB3_54
# BB#51:                                # %vector.ph263
                                        #   in Loop: Header=BB3_44 Depth=1
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r11, %rdi
	movq	%r12, %rbp
	movq	%r15, %r10
	.p2align	4, 0x90
.LBB3_52:                               # %vector.body238
                                        #   Parent Loop BB3_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10), %xmm3           # xmm3 = mem[0],zero
	punpcklbw	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3],xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm6, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movq	(%rbp), %xmm5           # xmm5 = mem[0],zero
	punpcklbw	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3],xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1],xmm6[2],xmm8[2],xmm6[3],xmm8[3]
	punpckhwd	%xmm8, %xmm5    # xmm5 = xmm5[4],xmm8[4],xmm5[5],xmm8[5],xmm5[6],xmm8[6],xmm5[7],xmm8[7]
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm0      # xmm0 = xmm7[0,2,2,3]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	paddd	%xmm4, %xmm6
	paddd	%xmm3, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm5, %xmm6
	movdqu	%xmm6, (%rdi)
	addq	$8, %r10
	addq	$8, %rbp
	addq	$16, %rdi
	addq	$-8, %rcx
	jne	.LBB3_52
# BB#53:                                # %middle.block239
                                        #   in Loop: Header=BB3_44 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %r10
	jne	.LBB3_54
	jmp	.LBB3_59
.LBB3_47:                               #   in Loop: Header=BB3_44 Depth=1
	xorl	%edi, %edi
	jmp	.LBB3_54
.LBB3_49:                               #   in Loop: Header=BB3_44 Depth=1
	xorl	%edi, %edi
	movq	%r14, %r10
	.p2align	4, 0x90
.LBB3_54:                               # %scalar.ph240.preheader
                                        #   in Loop: Header=BB3_44 Depth=1
	movl	%ebx, %ecx
	subl	%edi, %ecx
	testb	$1, %cl
	movq	%rdi, %rcx
	je	.LBB3_56
# BB#55:                                # %scalar.ph240.prol
                                        #   in Loop: Header=BB3_44 Depth=1
	movzbl	(%r15,%rdi), %ecx
	imull	%edx, %ecx
	movzbl	(%r12,%rdi), %ebp
	imull	%eax, %ebp
	addl	%ecx, %ebp
	movw	%bp, (%rsi,%rdi,2)
	leaq	1(%rdi), %rcx
.LBB3_56:                               # %scalar.ph240.prol.loopexit
                                        #   in Loop: Header=BB3_44 Depth=1
	cmpq	%rdi, 64(%rsp)          # 8-byte Folded Reload
	je	.LBB3_59
# BB#57:                                # %scalar.ph240.preheader.new
                                        #   in Loop: Header=BB3_44 Depth=1
	addq	24(%rsp), %r9           # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB3_58:                               # %scalar.ph240
                                        #   Parent Loop BB3_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r9,%rcx), %esi
	imull	%edx, %esi
	movzbl	(%r12,%rcx), %edi
	imull	%eax, %edi
	addl	%esi, %edi
	movw	%di, (%r11,%rcx,2)
	movzbl	(%r9,%rcx), %esi
	imull	%edx, %esi
	movzbl	1(%r12,%rcx), %edi
	imull	%eax, %edi
	addl	%esi, %edi
	movw	%di, 2(%r11,%rcx,2)
	addq	$2, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB3_58
.LBB3_59:                               # %._crit_edge.us.i169
                                        #   in Loop: Header=BB3_44 Depth=1
	incq	%r8
	addq	88(%rsp), %r11          # 8-byte Folded Reload
	cmpq	%r10, %r8
	movq	80(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_44
	jmp	.LBB3_98
.Lfunc_end3:
	.size	Make_Spatial_Prediction_Frame, .Lfunc_end3-Make_Spatial_Prediction_Frame
	.cfi_endproc

	.p2align	4, 0x90
	.type	Deinterlace,@function
Deinterlace:                            # @Deinterlace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movl	%r8d, %eax
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movq	%rdi, -96(%rsp)         # 8-byte Spill
	cmpl	%eax, %edx
	jge	.LBB4_25
# BB#1:                                 # %.lr.ph76
	movslq	%ecx, %r8
	movq	%r8, %rsi
	negq	%rsi
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	leal	-1(%rax), %esi
	movl	%esi, -68(%rsp)         # 4-byte Spill
	testl	%r9d, %r9d
	movl	%ecx, -100(%rsp)        # 4-byte Spill
	movq	%r8, -32(%rsp)          # 8-byte Spill
	je	.LBB4_6
# BB#2:                                 # %.lr.ph76.split.preheader
	movslq	%edx, %r12
	movslq	%eax, %r11
	movl	%r8d, %edx
	movq	%r8, %rsi
	imulq	%r12, %rsi
	leaq	(%r8,%r8), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	-1(%rdx), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	andl	$15, %eax
	movq	%rdx, %rdi
	movq	%rax, -16(%rsp)         # 8-byte Spill
	subq	%rax, %rdi
	movq	%rdi, -88(%rsp)         # 8-byte Spill
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %r13
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movq	%rsi, -64(%rsp)         # 8-byte Spill
	leaq	1(%rax,%rsi), %r14
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph76.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
                                        #     Child Loop BB4_23 Depth 2
	testl	%r12d, %r12d
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %r10
	cmoveq	%r8, %r10
	cmpl	-68(%rsp), %r12d        # 4-byte Folded Reload
	movq	%r8, %rax
	cmoveq	%rdx, %rax
	testl	%r8d, %r8d
	jle	.LBB4_24
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$16, %ecx
	jae	.LBB4_11
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	xorl	%esi, %esi
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_11:                               # %min.iters.checked
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	$0, -88(%rsp)           # 8-byte Folded Reload
	je	.LBB4_12
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r11, %rbp
	movq	-80(%rsp), %rsi         # 8-byte Reload
	imulq	%r9, %rsi
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	movq	-96(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rcx
	addq	-8(%rsp), %rsi          # 8-byte Folded Reload
	addq	%rdx, %rsi
	leaq	(%rcx,%r10), %rdi
	leaq	(%rsi,%r10), %rbx
	leaq	(%rsi,%rax), %r8
	cmpq	%rbx, %rcx
	sbbb	%r11b, %r11b
	cmpq	%rsi, %rdi
	sbbb	%bl, %bl
	andb	%r11b, %bl
	cmpq	%r8, %rcx
	leaq	(%rcx,%rax), %rdi
	sbbb	%cl, %cl
	cmpq	%rsi, %rdi
	sbbb	%sil, %sil
	testb	$1, %bl
	jne	.LBB4_14
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_3 Depth=1
	andb	%sil, %cl
	andb	$1, %cl
	movl	$0, %esi
	movl	-100(%rsp), %ecx        # 4-byte Reload
	movq	-32(%rsp), %r8          # 8-byte Reload
	movq	%rbp, %r11
	jne	.LBB4_19
# BB#16:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB4_17:                               # %vector.body
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r10,%rdi), %xmm0
	movdqu	(%rax,%rdi), %xmm1
	pavgb	%xmm0, %xmm1
	movdqu	%xmm1, (%rdi)
	addq	$16, %rdi
	addq	$-16, %rsi
	jne	.LBB4_17
# BB#18:                                # %middle.block
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, -16(%rsp)           # 4-byte Folded Reload
	movq	-88(%rsp), %rsi         # 8-byte Reload
	jne	.LBB4_19
	jmp	.LBB4_24
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%esi, %esi
	jmp	.LBB4_19
.LBB4_14:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%esi, %esi
	movl	-100(%rsp), %ecx        # 4-byte Reload
	movq	-32(%rsp), %r8          # 8-byte Reload
	movq	%rbp, %r11
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph.preheader97
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	-40(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%esi, %edx
	testb	$1, %dl
	movq	%rsi, %rdi
	je	.LBB4_21
# BB#20:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdx
	imulq	%r8, %rdx
	addq	-96(%rsp), %rdx         # 8-byte Folded Reload
	leaq	(%rdx,%r10), %rdi
	leaq	(%rdx,%rax), %rbx
	movzbl	(%rdi,%rsi), %edi
	movzbl	(%rbx,%rsi), %ebx
	leal	1(%rdi,%rbx), %edi
	shrl	%edi
	movb	%dil, (%rdx,%rsi)
	leaq	1(%rsi), %rdi
.LBB4_21:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	%rsi, -48(%rsp)         # 8-byte Folded Reload
	je	.LBB4_24
# BB#22:                                # %.lr.ph.preheader97.new
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rsi
	addq	%r14, %r10
	addq	%r14, %rax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%r15), %rbp
	movzbl	-1(%r10,%rbp), %ebx
	movzbl	-1(%rax,%rbp), %edx
	leal	1(%rbx,%rdx), %edx
	shrl	%edx
	movb	%dl, (%r13,%rbp)
	movzbl	(%r10,%rbp), %edx
	movzbl	(%rax,%rbp), %ebx
	leal	1(%rdx,%rbx), %edx
	shrl	%edx
	movb	%dl, 1(%r13,%rbp)
	addq	$2, %r15
	movq	%rsi, %rdx
	addq	%r15, %rdx
	jne	.LBB4_23
.LBB4_24:                               # %.loopexit70
                                        #   in Loop: Header=BB4_3 Depth=1
	addq	$2, %r12
	incq	%r9
	movq	-80(%rsp), %rax         # 8-byte Reload
	addq	%rax, %r13
	addq	%rax, %r14
	cmpq	%r11, %r12
	jl	.LBB4_3
	jmp	.LBB4_25
.LBB4_6:                                # %.lr.ph76.split.us.preheader
	leal	(%r8,%r8), %esi
	movslq	%esi, %rdi
	movq	%rdi, %rsi
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	negq	%rdi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leal	-2(%rax), %esi
	movslq	%edx, %r15
	movslq	%esi, %rdx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	%ecx, %r11d
	movq	%r8, %rax
	imulq	%r15, %rax
	addq	%rax, -96(%rsp)         # 8-byte Folded Spill
	leaq	(%r8,%r8), %rdx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	addq	-80(%rsp), %rax         # 8-byte Folded Reload
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph76.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	movq	%r15, %rdi
	imulq	%r8, %rdi
	testl	%r15d, %r15d
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	%rax, %r12
	cmoveq	%r8, %r12
	cmpl	-68(%rsp), %r15d        # 4-byte Folded Reload
	movq	%r8, %r13
	cmoveq	%rax, %r13
	addq	-80(%rsp), %rdi         # 8-byte Folded Reload
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	(%rdi,%rax), %r10
	cmpq	$2, %r15
	cmovlq	%rdi, %r10
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	(%rdi,%rax), %rsi
	cmpq	-88(%rsp), %r15         # 8-byte Folded Reload
	cmovgeq	%rdi, %rsi
	testl	%ecx, %ecx
	movq	%rdx, %rcx
	jle	.LBB4_10
# BB#8:                                 # %.lr.ph73.us.preheader
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	-96(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r12
	addq	%rdi, %r13
	movq	%rcx, %rbp
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph73.us
                                        #   Parent Loop BB4_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%r9), %r14d
	movzbl	(%r13,%r9), %r8d
	addl	%r14d, %r8d
	movzbl	(%rbp), %edx
	addl	%edx, %edx
	leal	(%rdx,%r8,8), %eax
	movzbl	(%r10,%r9), %ebx
	subl	%ebx, %eax
	movzbl	(%rsi,%r9), %ebx
	subl	%ebx, %eax
	movq	Clip(%rip), %rbx
	movl	%eax, %edx
	sarl	$31, %edx
	leal	8(%rax,%rdx), %eax
	sarl	$4, %eax
	cltq
	movzbl	(%rbx,%rax), %eax
	movb	%al, (%rdi)
	incq	%r9
	incq	%rbp
	incq	%rdi
	cmpq	%r9, %r11
	jne	.LBB4_9
.LBB4_10:                               # %.loopexit.us
                                        #   in Loop: Header=BB4_7 Depth=1
	addq	$2, %r15
	movq	-64(%rsp), %rax         # 8-byte Reload
	addq	%rax, -96(%rsp)         # 8-byte Folded Spill
	addq	%rax, %rcx
	movq	%rcx, %rdx
	cmpq	-56(%rsp), %r15         # 8-byte Folded Reload
	movl	-100(%rsp), %ecx        # 4-byte Reload
	movq	-32(%rsp), %r8          # 8-byte Reload
	jl	.LBB4_7
.LBB4_25:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Deinterlace, .Lfunc_end4-Deinterlace
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rb"
	.size	.L.str, 3

	.type	.LRead_Lower_Layer_Component_Fieldwise.ext,@object # @Read_Lower_Layer_Component_Fieldwise.ext
	.section	.rodata,"a",@progbits
.LRead_Lower_Layer_Component_Fieldwise.ext:
	.asciz	".Y"
	.asciz	".U"
	.asciz	".V"
	.size	.LRead_Lower_Layer_Component_Fieldwise.ext, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
