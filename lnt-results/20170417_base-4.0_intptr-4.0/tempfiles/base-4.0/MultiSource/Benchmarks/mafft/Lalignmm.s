	.text
	.file	"Lalignmm.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4607182418800017408     # double 1
.LCPI0_3:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_4:
	.long	3407386240              # float -1.0E+7
	.text
	.globl	Lalignmm_hmout
	.p2align	4, 0x90
	.type	Lalignmm_hmout,@function
Lalignmm_hmout:                         # @Lalignmm_hmout
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi6:
	.cfi_def_cfa_offset 672
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, %r14d
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cvtsi2ssl	RNApenalty(%rip), %xmm0
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	movq	(%rbx), %rdi
	callq	seqlen
	movq	(%rbp), %rdi
	callq	seqlen
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	(%rbp), %rdi
	callq	strlen
	movq	%rax, %rbp
	leal	200(%r12,%rbp), %ebx
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movl	%r15d, 72(%rsp)         # 4-byte Spill
	movl	%r15d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, %r13
	movl	$4, %edi
	xorl	%esi, %esi
	callq	AllocateFloatMtx
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	%r12, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leal	102(%r12), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	leal	102(%rbp), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$27, %esi
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	$27, %esi
	movl	%r12d, %edi
	callq	AllocateFloatMtx
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB0_4
# BB#1:                                 # %.lr.ph254
	movslq	104(%rsp), %r14         # 4-byte Folded Reload
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB0_344
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_2
.LBB0_4:                                # %.preheader211
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	jle	.LBB0_8
# BB#5:                                 # %.lr.ph250
	movslq	88(%rsp), %r14          # 4-byte Folded Reload
	movslq	72(%rsp), %rbx          # 4-byte Folded Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB0_345
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_6
.LBB0_8:                                # %._crit_edge251
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	184(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdx
	movq	104(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %ecx
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %r8d
	callq	MScpmx_calc_new
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	88(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %ecx
	movl	72(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r8d
	callq	MScpmx_calc_new
	movq	680(%rsp), %r9
	testq	%r9, %r9
	je	.LBB0_10
# BB#9:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	104(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	new_OpeningGapCount
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	movq	688(%rsp), %r9
	callq	new_OpeningGapCount
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	104(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	704(%rsp), %rbx
	movq	%rbx, %r9
	callq	new_FinalGapCount
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	movq	%rbx, %r9
	movq	104(%rsp), %rbp         # 8-byte Reload
	callq	new_FinalGapCount
	jmp	.LBB0_11
.LBB0_10:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	st_OpeningGapCount
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%r12d, %r8d
	callq	st_OpeningGapCount
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	st_FinalGapCount
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%r12d, %r8d
	callq	st_FinalGapCount
.LBB0_11:                               # %.preheader210
	testl	%ebp, %ebp
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movss	56(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movq	8(%rsp), %rbx           # 8-byte Reload
	jle	.LBB0_20
# BB#12:                                # %.lr.ph248
	cvtss2sd	%xmm7, %xmm0
	movl	%ebp, %eax
	cmpq	$3, %rax
	jbe	.LBB0_17
# BB#13:                                # %min.iters.checked
	movl	%ebp, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_17
# BB#14:                                # %vector.memcheck
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB0_98
# BB#15:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB0_98
# BB#16:
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_18
.LBB0_17:
	xorl	%ecx, %ecx
.LBB0_18:                               # %scalar.ph.preheader
	leaq	(%rbx,%rcx,4), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rsi
	subq	%rcx, %rax
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_19:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_19
.LBB0_20:                               # %.preheader209
	testl	%r9d, %r9d
	jle	.LBB0_28
# BB#21:                                # %.lr.ph245
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movl	%r9d, %eax
	cmpq	$3, %rax
	jbe	.LBB0_25
# BB#22:                                # %min.iters.checked398
	movl	%r9d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_25
# BB#23:                                # %vector.memcheck411
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r8
	jae	.LBB0_101
# BB#24:                                # %vector.memcheck411
	leaq	(%r8,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB0_101
.LBB0_25:
	xorl	%ecx, %ecx
.LBB0_26:                               # %scalar.ph396.preheader
	leaq	(%r8,%rcx,4), %rdx
	leaq	(%rdi,%rcx,4), %rsi
	subq	%rcx, %rax
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_27:                               # %scalar.ph396
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_27
.LBB0_28:                               # %._crit_edge246
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rbx, (%rax)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rdi, 24(%rax)
	incl	reccycle(%rip)
	testl	%r9d, %r9d
	jle	.LBB0_63
# BB#29:
	xorl	%esi, %esi
	movl	28(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	callq	AllocateCharMtx
	movq	%rax, %r13
	xorl	%esi, %esi
	movl	%r15d, %edi
	callq	AllocateCharMtx
	movq	%rax, %r14
	testl	%ebx, %ebx
	movq	304(%rsp), %rbp         # 8-byte Reload
	jle	.LBB0_41
# BB#30:                                # %.lr.ph172.preheader.i
	movl	%ebx, %eax
	cmpl	$3, %ebx
	jbe	.LBB0_34
# BB#31:                                # %min.iters.checked428
	movl	%ebx, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_34
# BB#32:                                # %vector.memcheck441
	leaq	(%rbp,%rax,8), %rsi
	cmpq	%rsi, %r13
	jae	.LBB0_221
# BB#33:                                # %vector.memcheck441
	leaq	(%r13,%rax,8), %rsi
	cmpq	%rsi, %rbp
	jae	.LBB0_221
.LBB0_34:
	xorl	%ecx, %ecx
.LBB0_35:                               # %.lr.ph172.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_38
# BB#36:                                # %.lr.ph172.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph172.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rcx,8), %rdi
	movq	%rdi, (%r13,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_37
.LBB0_38:                               # %.lr.ph172.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_41
# BB#39:                                # %.lr.ph172.i.preheader.new
	subq	%rcx, %rax
	leaq	56(%r13,%rcx,8), %rdx
	leaq	56(%rbp,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph172.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB0_40
.LBB0_41:                               # %.preheader33.i
	testl	%r15d, %r15d
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	jle	.LBB0_53
# BB#42:                                # %.lr.ph168.preheader.i
	movl	%r15d, %eax
	cmpl	$3, %r15d
	jbe	.LBB0_46
# BB#43:                                # %min.iters.checked457
	movl	%r15d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_46
# BB#44:                                # %vector.memcheck470
	leaq	(%r12,%rax,8), %rsi
	cmpq	%rsi, %r14
	jae	.LBB0_224
# BB#45:                                # %vector.memcheck470
	leaq	(%r14,%rax,8), %rsi
	cmpq	%rsi, %r12
	jae	.LBB0_224
.LBB0_46:
	xorl	%ecx, %ecx
.LBB0_47:                               # %.lr.ph168.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_50
# BB#48:                                # %.lr.ph168.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph168.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rcx,8), %rdi
	movq	%rdi, (%r14,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_49
.LBB0_50:                               # %.lr.ph168.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_53
# BB#51:                                # %.lr.ph168.i.preheader.new
	subq	%rcx, %rax
	leaq	56(%r14,%rcx,8), %rdx
	leaq	56(%r12,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph168.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB0_52
.LBB0_53:                               # %._crit_edge169.i
	cmpl	$10, 104(%rsp)          # 4-byte Folded Reload
	jl	.LBB0_72
# BB#54:                                # %._crit_edge169.i
	cmpl	$9, %r8d
	jle	.LBB0_72
# BB#55:
	movq	104(%rsp), %r12         # 8-byte Reload
	leal	100(%r12), %r14d
	leal	100(%r8), %r15d
	movl	152(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movl	96(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, %r13
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%r14d, %ebx
	movl	%r15d, %ebp
	cmpl	%ebp, %ebx
	cmovgel	%ebx, %r15d
	addl	$2, %r15d
	movl	%r15d, %edi
	callq	AllocateCharVec
	movl	$26, %esi
	movl	%r15d, %edi
	callq	AllocateFloatMtx
	movq	%rax, %r14
	movl	$26, %esi
	movl	%r15d, %edi
	callq	AllocateIntMtx
	movq	%rax, %r15
	movl	%ebx, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	$0, %ecx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	184(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	movl	%r12d, %r8d
	movq	%r14, %r9
	pushq	$1
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movl	$0, %ecx
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	88(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r14, 256(%rsp)         # 8-byte Spill
	movq	%r14, %r9
	pushq	$1
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	movq	%r15, 208(%rsp)         # 8-byte Spill
	pushq	%r15
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	leal	1(%r12), %r15d
	testl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	jle	.LBB0_79
# BB#56:                                # %.lr.ph165.preheader.i
	movl	%r15d, %ecx
	leaq	-1(%rcx), %rdx
	cmpq	$7, %rdx
	jbe	.LBB0_73
# BB#57:                                # %min.iters.checked486
	movq	%rdx, %r8
	andq	$-8, %r8
	je	.LBB0_73
# BB#58:                                # %vector.memcheck505
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi), %rax
	leaq	(%rsi,%rcx,4), %rsi
	leaq	-4(%r9,%rcx,4), %r11
	cmpq	%rbp, %rax
	sbbb	%r10b, %r10b
	cmpq	%rsi, %rbp
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%r11, %rax
	sbbb	%al, %al
	cmpq	%rsi, %r9
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB0_230
# BB#59:                                # %vector.memcheck505
	andb	%dil, %al
	andb	$1, %al
	movq	48(%rsp), %r9           # 8-byte Reload
	jne	.LBB0_74
# BB#60:                                # %vector.body482.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r9), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	20(%rdi), %rbp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB0_61:                               # %vector.body482
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB0_61
# BB#62:                                # %middle.block483
	cmpq	%r8, %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB0_74
	jmp	.LBB0_79
.LBB0_63:                               # %.preheader21.i
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_66
# BB#64:                                # %.lr.ph61.i
	movslq	104(%rsp), %r14         # 4-byte Folded Reload
	movl	28(%rsp), %r15d         # 4-byte Reload
	movq	304(%rsp), %rbp         # 8-byte Reload
	movq	216(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	callq	strncpy
	movq	(%rbp), %rax
	movb	$0, (%rax,%r14)
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r15
	jne	.LBB0_65
.LBB0_66:                               # %.preheader.i
	movl	72(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	240(%rsp), %r12         # 8-byte Reload
	jle	.LBB0_323
# BB#67:                                # %.lr.ph59.i
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movl	%eax, %r14d
	jle	.LBB0_104
# BB#68:                                # %.lr.ph59.split.i.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_69:                               # %.lr.ph59.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_70 Depth 2
	movq	(%r12,%r15,8), %rax
	movb	$0, (%rax)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB0_70:                               #   Parent Loop BB0_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movw	$45, (%rbp,%rax)
	decl	%ebx
	jne	.LBB0_70
# BB#71:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_69 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jne	.LBB0_69
	jmp	.LBB0_323
.LBB0_72:
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_323
.LBB0_73:
	movl	$1, %esi
.LBB0_74:                               # %.lr.ph165.i.preheader
	movl	%ecx, %eax
	subl	%esi, %eax
	testb	$1, %al
	movq	%rsi, %rdi
	je	.LBB0_76
# BB#75:                                # %.lr.ph165.i.prol
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r9,%rsi,4), %xmm0
	movq	40(%rsp), %rax          # 8-byte Reload
	addss	(%rax,%rsi,4), %xmm0
	movss	%xmm0, (%rax,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB0_76:                               # %.lr.ph165.i.prol.loopexit
	cmpq	%rsi, %rdx
	je	.LBB0_79
# BB#77:                                # %.lr.ph165.i.preheader.new
	subq	%rdi, %rcx
	leaq	(%r9,%rdi,4), %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph165.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rax), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rax
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB0_78
.LBB0_79:                               # %.preheader32.i
	movq	88(%rsp), %r11          # 8-byte Reload
	leal	1(%r11), %eax
	movl	%eax, 168(%rsp)         # 4-byte Spill
	testl	%r11d, %r11d
	movq	224(%rsp), %r14         # 8-byte Reload
	movq	136(%rsp), %r13         # 8-byte Reload
	jle	.LBB0_93
# BB#80:                                # %.lr.ph160.preheader.i
	movl	168(%rsp), %ecx         # 4-byte Reload
	leaq	-1(%rcx), %rdx
	cmpq	$7, %rdx
	jbe	.LBB0_87
# BB#81:                                # %min.iters.checked523
	movq	%rdx, %r8
	andq	$-8, %r8
	je	.LBB0_87
# BB#82:                                # %vector.memcheck544
	leaq	4(%r14), %rax
	leaq	(%r14,%rcx,4), %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	-4(%rbp,%rcx,4), %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpq	%rbx, %rax
	sbbb	%r10b, %r10b
	cmpq	%rsi, %rbx
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%rdi, %rax
	sbbb	%al, %al
	cmpq	%rsi, %rbp
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB0_88
# BB#83:                                # %vector.memcheck544
	andb	%dil, %al
	andb	$1, %al
	movq	16(%rsp), %rax          # 8-byte Reload
	jne	.LBB0_88
# BB#84:                                # %vector.body519.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rax
	leaq	20(%r14), %rbp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB0_85:                               # %vector.body519
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB0_85
# BB#86:                                # %middle.block520
	cmpq	%r8, %rdx
	jne	.LBB0_88
	jmp	.LBB0_93
.LBB0_87:
	movl	$1, %esi
.LBB0_88:                               # %.lr.ph160.i.preheader
	movl	%ecx, %eax
	subl	%esi, %eax
	testb	$1, %al
	movq	%rsi, %rdi
	je	.LBB0_90
# BB#89:                                # %.lr.ph160.i.prol
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	32(%rsp), %rax          # 8-byte Reload
	addss	-4(%rax,%rsi,4), %xmm0
	addss	(%r14,%rsi,4), %xmm0
	movss	%xmm0, (%r14,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB0_90:                               # %.lr.ph160.i.prol.loopexit
	cmpq	%rsi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_93
# BB#91:                                # %.lr.ph160.i.preheader.new
	subq	%rdi, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rax
	leaq	4(%r14,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB0_92:                               # %.lr.ph160.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rax), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rax
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB0_92
.LBB0_93:                               # %._crit_edge161.i
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %r10
	movl	%eax, (%r10)
	testl	%r12d, %r12d
	jle	.LBB0_112
# BB#94:                                # %.lr.ph158.preheader.i
	movl	%r15d, %ecx
	leal	3(%r15), %edi
	leaq	-2(%rcx), %r8
	andq	$3, %rdi
	je	.LBB0_109
# BB#95:                                # %.lr.ph158.i.prol.preheader
	xorl	%edx, %edx
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_96:                               # %.lr.ph158.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rsi,%rdx,4), %ebp
	movq	8(%rax,%rdx,8), %rbx
	movl	%ebp, (%rbx)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB0_96
# BB#97:                                # %.lr.ph158.i.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB0_110
	jmp	.LBB0_112
.LBB0_98:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI0_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI0_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rsi
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_99:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB0_99
# BB#100:                               # %middle.block
	testq	%rdx, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jne	.LBB0_18
	jmp	.LBB0_20
.LBB0_101:                              # %vector.ph412
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI0_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI0_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rsi
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB0_102:                              # %vector.body394
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB0_102
# BB#103:                               # %middle.block395
	testq	%rdx, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jne	.LBB0_26
	jmp	.LBB0_28
.LBB0_104:                              # %.lr.ph59.split.us.i.preheader
	leaq	-1(%r14), %rax
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	andq	$7, %rdx
	je	.LBB0_106
	.p2align	4, 0x90
.LBB0_105:                              # %.lr.ph59.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rcx,8), %rsi
	movb	$0, (%rsi)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB0_105
.LBB0_106:                              # %.lr.ph59.split.us.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB0_323
# BB#107:                               # %.lr.ph59.split.us.i.preheader.new
	subq	%rcx, %r14
	leaq	56(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB0_108:                              # %.lr.ph59.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rcx
	movb	$0, (%rcx)
	movq	-48(%rax), %rcx
	movb	$0, (%rcx)
	movq	-40(%rax), %rcx
	movb	$0, (%rcx)
	movq	-32(%rax), %rcx
	movb	$0, (%rcx)
	movq	-24(%rax), %rcx
	movb	$0, (%rcx)
	movq	-16(%rax), %rcx
	movb	$0, (%rcx)
	movq	-8(%rax), %rcx
	movb	$0, (%rcx)
	movq	(%rax), %rcx
	movb	$0, (%rcx)
	addq	$64, %rax
	addq	$-8, %r14
	jne	.LBB0_108
	jmp	.LBB0_323
.LBB0_109:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB0_112
.LBB0_110:                              # %.lr.ph158.preheader.i.new
	subq	%rdx, %rcx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	24(%rax,%rdx,8), %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	12(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_111:                              # %.lr.ph158.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %edi
	movq	-24(%rax), %rbp
	movl	%edi, (%rbp)
	movl	-8(%rdx), %edi
	movq	-16(%rax), %rbp
	movl	%edi, (%rbp)
	movl	-4(%rdx), %edi
	movq	-8(%rax), %rbp
	movl	%edi, (%rbp)
	movl	(%rdx), %edi
	movq	(%rax), %rbp
	movl	%edi, (%rbp)
	addq	$32, %rax
	addq	$16, %rdx
	addq	$-4, %rcx
	jne	.LBB0_111
.LBB0_112:                              # %.preheader31.i
	movq	%r15, 152(%rsp)         # 8-byte Spill
	leal	-1(%r11), %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	testl	%r11d, %r11d
	movq	8(%rsp), %r15           # 8-byte Reload
	jle	.LBB0_137
# BB#113:                               # %.lr.ph155.preheader.i
	movl	168(%rsp), %ecx         # 4-byte Reload
	leaq	-1(%rcx), %r8
	cmpq	$7, %r8
	jbe	.LBB0_117
# BB#114:                               # %min.iters.checked564
	movq	%r8, %rdi
	andq	$-8, %rdi
	je	.LBB0_117
# BB#115:                               # %vector.memcheck581
	leaq	4(%r10), %rax
	leaq	(%r14,%rcx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB0_227
# BB#116:                               # %vector.memcheck581
	leaq	(%r10,%rcx,4), %rax
	leaq	4(%r14), %rdx
	cmpq	%rax, %rdx
	jae	.LBB0_227
.LBB0_117:
	movl	$1, %edi
.LBB0_118:                              # %.lr.ph155.i.preheader
	movl	%ecx, %edx
	subl	%edi, %edx
	movq	%r8, %rax
	subq	%rdi, %rax
	andq	$7, %rdx
	je	.LBB0_121
# BB#119:                               # %.lr.ph155.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB0_120:                              # %.lr.ph155.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rdi,4), %ebp
	movl	%ebp, (%r10,%rdi,4)
	incq	%rdi
	incq	%rdx
	jne	.LBB0_120
.LBB0_121:                              # %.lr.ph155.i.prol.loopexit
	cmpq	$7, %rax
	movq	8(%rsp), %r15           # 8-byte Reload
	jb	.LBB0_124
# BB#122:                               # %.lr.ph155.i.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	28(%r10,%rdi,4), %rsi
	leaq	28(%r14,%rdi,4), %rax
	.p2align	4, 0x90
.LBB0_123:                              # %.lr.ph155.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %edi
	movl	%edi, -28(%rsi)
	movl	-24(%rax), %edi
	movl	%edi, -24(%rsi)
	movl	-20(%rax), %edi
	movl	%edi, -20(%rsi)
	movl	-16(%rax), %edi
	movl	%edi, -16(%rsi)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rsi)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rsi)
	movl	-4(%rax), %edi
	movl	%edi, -4(%rsi)
	movl	(%rax), %edi
	movl	%edi, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-8, %rdx
	jne	.LBB0_123
.LBB0_124:                              # %.lr.ph152.i
	leaq	4(%r15), %rsi
	cmpq	$7, %r8
	jbe	.LBB0_131
# BB#125:                               # %min.iters.checked599
	movq	%r8, %r10
	andq	$-8, %r10
	je	.LBB0_131
# BB#126:                               # %vector.memcheck620
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax), %rbx
	leaq	(%rax,%rcx,4), %rdx
	leaq	-4(%r14,%rcx,4), %rdi
	cmpq	%rdi, %rbx
	sbbb	%dil, %dil
	cmpq	%rdx, %r14
	sbbb	%bl, %bl
	andb	%dil, %bl
	cmpq	%r15, %rax
	sbbb	%al, %al
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	movl	$1, %r11d
	testb	$1, %bl
	jne	.LBB0_132
# BB#127:                               # %vector.memcheck620
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB0_132
# BB#128:                               # %vector.body595.preheader
	movq	%r10, %r11
	orq	$1, %r11
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r14), %rbx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	20(%rax), %rax
	leaq	20(%r13), %rbp
	xorps	%xmm1, %xmm1
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB0_129:                              # %vector.body595
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdx
	jne	.LBB0_129
# BB#130:                               # %middle.block596
	cmpq	%r10, %r8
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB0_132
	jmp	.LBB0_137
.LBB0_131:
	movl	$1, %r11d
.LBB0_132:                              # %scalar.ph597.preheader
	movl	%ecx, %eax
	subl	%r11d, %eax
	testb	$1, %al
	movq	%r11, %r10
	je	.LBB0_134
# BB#133:                               # %scalar.ph597.prol
	movss	-4(%r14,%r11,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movq	56(%rsp), %rax          # 8-byte Reload
	movss	%xmm0, (%rax,%r11,4)
	movl	$0, (%r13,%r11,4)
	leaq	1(%r11), %r10
.LBB0_134:                              # %scalar.ph597.prol.loopexit
	cmpq	%r11, %r8
	je	.LBB0_137
# BB#135:                               # %scalar.ph597.preheader.new
	subq	%r10, %rcx
	leaq	(%r14,%r10,4), %rdx
	leaq	4(%r13,%r10,4), %rax
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%r10,4), %rdi
	.p2align	4, 0x90
.LBB0_136:                              # %scalar.ph597
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, -4(%rdi)
	movl	$0, -4(%rax)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rdi)
	movl	$0, (%rax)
	addq	$8, %rdx
	addq	$8, %rax
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB0_136
.LBB0_137:                              # %._crit_edge153.i
	leal	-1(%r12), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movslq	160(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	(%r14,%rax,4), %eax
	movq	312(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	mulsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	testl	%r12d, %r12d
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r14, %rbx
	movq	16(%rsp), %r8           # 8-byte Reload
	jle	.LBB0_167
# BB#138:                               # %.lr.ph147.i
	movslq	88(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	120(%rsp), %r10d        # 4-byte Reload
	movl	168(%rsp), %r14d        # 4-byte Reload
	movl	152(%rsp), %eax         # 4-byte Reload
	movq	%rax, 152(%rsp)         # 8-byte Spill
	addq	$-2, %r14
	movl	$1, %r12d
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_139:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_140 Depth 2
                                        #       Child Loop BB0_141 Depth 3
                                        #     Child Loop BB0_144 Depth 2
                                        #       Child Loop BB0_146 Depth 3
                                        #     Child Loop BB0_162 Depth 2
                                        #     Child Loop BB0_154 Depth 2
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	%rax, %rcx
	leaq	-1(%r12), %r11
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r12,4), %eax
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx)
	movl	RNAthr(%rip), %eax
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r12,8), %rbp
	addq	$4, %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_140:                              #   Parent Loop BB0_139 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_141 Depth 3
	movl	$0, 512(%rsp,%rdx,4)
	xorpd	%xmm0, %xmm0
	movq	$-2704, %rbx            # imm = 0xF570
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_141:                              #   Parent Loop BB0_139 Depth=1
                                        #     Parent Loop BB0_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	n_dis+2704(%rbx,%rdx,4), %esi
	subl	%eax, %esi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	mulss	-4(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movl	n_dis+2808(%rbx,%rdx,4), %esi
	subl	%eax, %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	mulss	(%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	$8, %rcx
	addq	$208, %rbx
	jne	.LBB0_141
# BB#142:                               #   in Loop: Header=BB0_140 Depth=2
	movss	%xmm0, 512(%rsp,%rdx,4)
	incq	%rdx
	cmpq	$26, %rdx
	jne	.LBB0_140
# BB#143:                               # %.preheader.i.i
                                        #   in Loop: Header=BB0_139 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%r9, %rbx
	je	.LBB0_148
	.p2align	4, 0x90
.LBB0_144:                              # %.lr.ph85.i.i
                                        #   Parent Loop BB0_139 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_146 Depth 3
	decl	%eax
	movl	$0, (%rbx)
	movq	(%rcx), %rbp
	movl	(%rbp), %edi
	testl	%edi, %edi
	js	.LBB0_147
# BB#145:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_144 Depth=2
	movq	(%rdx), %rsi
	addq	$4, %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_146:                              #   Parent Loop BB0_139 Depth=1
                                        #     Parent Loop BB0_144 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rdi
	movss	512(%rsp,%rdi,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	movl	(%rbp), %edi
	addq	$4, %rbp
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB0_146
.LBB0_147:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_144 Depth=2
	addq	$4, %rbx
	addq	$8, %rcx
	addq	$8, %rdx
	testl	%eax, %eax
	jne	.LBB0_144
.LBB0_148:                              # %match_calc.exit.i
                                        #   in Loop: Header=BB0_139 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r12,4), %eax
	movl	%eax, (%r9)
	movl	(%r15,%r12,4), %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	%eax, (%rsi)
	cmpq	%r10, %r12
	jne	.LBB0_150
# BB#149:                               #   in Loop: Header=BB0_139 Depth=1
	movq	192(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_150:                              #   in Loop: Header=BB0_139 Depth=1
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jle	.LBB0_159
# BB#151:                               # %.lr.ph141.i
                                        #   in Loop: Header=BB0_139 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	cmpq	%r10, %r12
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r12,8), %rcx
	jne	.LBB0_160
# BB#152:                               # %.lr.ph141.split.us.i.preheader
                                        #   in Loop: Header=BB0_139 Depth=1
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.LBB0_154
	.p2align	4, 0x90
.LBB0_153:                              # %..lr.ph141.split.us_crit_edge.i
                                        #   in Loop: Header=BB0_154 Depth=2
	movq	64(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rbx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movss	8(%rsi,%rbx,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	incq	%rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB0_154:                              # %.lr.ph141.split.us.i
                                        #   Parent Loop BB0_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	maxss	%xmm1, %xmm3
	addss	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	movaps	%xmm0, %xmm4
	cmpnless	%xmm2, %xmm4
	movaps	%xmm4, %xmm5
	andnps	%xmm2, %xmm5
	andps	%xmm0, %xmm4
	orps	%xmm5, %xmm4
	movaps	%xmm4, %xmm0
	cmovael	%ebx, %edx
	movss	4(%rsi,%rbx,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movq	%rsi, %r13
	movq	48(%rsp), %rsi          # 8-byte Reload
	movss	-4(%rsi,%r12,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	maxss	%xmm3, %xmm2
	addss	(%r15,%r12,4), %xmm1
	ucomiss	%xmm4, %xmm1
	jae	.LBB0_156
# BB#155:                               # %.lr.ph141.split.us._crit_edge.i
                                        #   in Loop: Header=BB0_154 Depth=2
	movq	136(%rsp), %rsi         # 8-byte Reload
	movl	4(%rsi,%rbx,4), %esi
	jmp	.LBB0_157
	.p2align	4, 0x90
.LBB0_156:                              #   in Loop: Header=BB0_154 Depth=2
	movss	%xmm1, 4(%r13,%rbx,4)
	movq	136(%rsp), %rsi         # 8-byte Reload
	movl	%r11d, 4(%rsi,%rbx,4)
	movl	%r11d, %esi
.LBB0_157:                              #   in Loop: Header=BB0_154 Depth=2
	movq	%r9, %r8
	addss	4(%r8,%rbx,4), %xmm2
	movss	%xmm2, 4(%r8,%rbx,4)
	movss	%xmm2, 4(%rax,%rbx,4)
	movl	4(%r13,%rbx,4), %edi
	movl	%edi, 4(%rcx,%rbx,4)
	movq	352(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbx,4)
	movq	344(%rsp), %rsi         # 8-byte Reload
	movl	%edx, 4(%rsi,%rbx,4)
	movl	4(%r8,%rbx,4), %esi
	movq	264(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbx,4)
	movl	4(%r13,%rbx,4), %esi
	movq	192(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbx,4)
	movq	272(%rsp), %rsi         # 8-byte Reload
	movss	%xmm0, 4(%rsi,%rbx,4)
	cmpq	%rbx, %r14
	jne	.LBB0_153
# BB#158:                               #   in Loop: Header=BB0_139 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	136(%rsp), %r13         # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_166
	.p2align	4, 0x90
.LBB0_159:                              # %.._crit_edge142_crit_edge.i
                                        #   in Loop: Header=BB0_139 Depth=1
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rcx
	jmp	.LBB0_166
	.p2align	4, 0x90
.LBB0_160:                              # %.lr.ph141.split.i.preheader
                                        #   in Loop: Header=BB0_139 Depth=1
	xorl	%edx, %edx
	jmp	.LBB0_162
	.p2align	4, 0x90
.LBB0_161:                              # %..lr.ph141.split_crit_edge.i
                                        #   in Loop: Header=BB0_162 Depth=2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	cmpless	%xmm2, %xmm3
	andps	%xmm3, %xmm2
	andnps	%xmm0, %xmm3
	orps	%xmm2, %xmm3
	movq	64(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	8(%r8,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	incq	%rdx
	movaps	%xmm3, %xmm0
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB0_162:                              # %.lr.ph141.split.i
                                        #   Parent Loop BB0_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rdx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	maxss	%xmm1, %xmm4
	movss	4(%rsi,%rdx,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rbp,%r12,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	maxss	%xmm4, %xmm3
	movss	(%r15,%r12,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm4
	ucomiss	%xmm5, %xmm4
	jb	.LBB0_164
# BB#163:                               #   in Loop: Header=BB0_162 Depth=2
	movss	%xmm4, 4(%rsi,%rdx,4)
	movl	%r11d, 4(%r13,%rdx,4)
.LBB0_164:                              #   in Loop: Header=BB0_162 Depth=2
	movq	%r9, %rbx
	addss	4(%rbx,%rdx,4), %xmm3
	movss	%xmm3, 4(%rbx,%rdx,4)
	movss	%xmm3, 4(%rax,%rdx,4)
	movl	4(%rsi,%rdx,4), %esi
	movl	%esi, 4(%rcx,%rdx,4)
	cmpq	%rdx, %r14
	jne	.LBB0_161
# BB#165:                               #   in Loop: Header=BB0_139 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB0_166:                              # %._crit_edge142.i
                                        #   in Loop: Header=BB0_139 Depth=1
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	%r9, %rbx
	movl	(%rbx,%rdi,4), %eax
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx,%r12,4)
	movl	(%rsi,%rdi,4), %eax
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	incq	%r12
	cmpq	152(%rsp), %r12         # 8-byte Folded Reload
	movq	%rbx, %rax
	jne	.LBB0_139
.LBB0_167:                              # %._crit_edge148.i
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	232(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	184(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdx
	movq	160(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	104(%rsp), %r13         # 8-byte Reload
	movl	%r13d, %r8d
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	%r12, %r9
	pushq	$1
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)               # 8-byte Folded Reload
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r12, %r9
	pushq	$1
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)               # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	cmpl	$2, %r13d
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	192(%rsp), %r9          # 8-byte Reload
	movq	208(%rsp), %r14         # 8-byte Reload
	jl	.LBB0_181
# BB#168:                               # %.lr.ph129.i
	movl	112(%rsp), %edx         # 4-byte Reload
	movslq	%edx, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB0_175
# BB#169:                               # %min.iters.checked638
	movl	%edx, %r10d
	andl	$7, %r10d
	movq	%rcx, %r8
	subq	%r10, %r8
	je	.LBB0_175
# BB#170:                               # %vector.memcheck659
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rsi
	leaq	4(%r15), %r11
	leaq	4(%r15,%rcx,4), %rbp
	cmpq	%rax, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %r11
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB0_231
# BB#171:                               # %vector.memcheck659
	andb	%sil, %bl
	andb	$1, %bl
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB0_176
# BB#172:                               # %vector.body634.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r15), %rdi
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB0_173:                              # %vector.body634
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_173
# BB#174:                               # %middle.block635
	testl	%r10d, %r10d
	movq	%r8, %rdi
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB0_176
	jmp	.LBB0_181
.LBB0_175:
	xorl	%edi, %edi
.LBB0_176:                              # %scalar.ph636.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %r8
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB0_178
# BB#177:                               # %scalar.ph636.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	addss	4(%r15,%rdi,4), %xmm0
	movq	40(%rsp), %rdx          # 8-byte Reload
	addss	(%rdx,%rdi,4), %xmm0
	movss	%xmm0, (%rdx,%rdi,4)
.LBB0_178:                              # %scalar.ph636.prol.loopexit
	cmpq	%rdi, %r8
	je	.LBB0_181
# BB#179:                               # %scalar.ph636.preheader.new
	subq	%rsi, %rcx
	leaq	8(%r15,%rsi,4), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_180:                              # %scalar.ph636
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB0_180
.LBB0_181:                              # %.preheader29.i
	movq	88(%rsp), %r11          # 8-byte Reload
	cmpl	$2, %r11d
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jl	.LBB0_195
# BB#182:                               # %.lr.ph126.i
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	movq	160(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB0_189
# BB#183:                               # %min.iters.checked677
	movl	%edx, %r10d
	andl	$7, %r10d
	movq	%rcx, %rdx
	subq	%r10, %rdx
	je	.LBB0_189
# BB#184:                               # %vector.memcheck699
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%r8, %rdx
	movq	80(%rsp), %r8           # 8-byte Reload
	leaq	(%r8,%rcx,4), %rsi
	leaq	4(%rdx), %rdi
	leaq	4(%rdx,%rcx,4), %rbp
	cmpq	%rax, %r8
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %r8
	sbbb	%bl, %bl
	cmpq	%rsi, %rdi
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB0_232
# BB#185:                               # %vector.memcheck699
	andb	%sil, %bl
	andb	$1, %bl
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB0_190
# BB#186:                               # %vector.body673.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r8), %rdi
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB0_187:                              # %vector.body673
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_187
# BB#188:                               # %middle.block674
	testl	%r10d, %r10d
	movq	%rdx, %rdi
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB0_190
	jmp	.LBB0_195
.LBB0_189:
	xorl	%edi, %edi
.LBB0_190:                              # %scalar.ph675.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %rdx
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB0_192
# BB#191:                               # %scalar.ph675.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	addss	4(%r8,%rdi,4), %xmm0
	movq	80(%rsp), %rbp          # 8-byte Reload
	addss	(%rbp,%rdi,4), %xmm0
	movss	%xmm0, (%rbp,%rdi,4)
.LBB0_192:                              # %scalar.ph675.prol.loopexit
	cmpq	%rdi, %rdx
	je	.LBB0_195
# BB#193:                               # %scalar.ph675.preheader.new
	subq	%rsi, %rcx
	leaq	8(%r8,%rsi,4), %rdx
	movq	80(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_194:                              # %scalar.ph675
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB0_194
.LBB0_195:                              # %.preheader28.i
	cmpl	$2, %r13d
	jl	.LBB0_201
# BB#196:                               # %.lr.ph124.i
	movl	112(%rsp), %ecx         # 4-byte Reload
	movslq	%ecx, %rax
	movl	%ecx, %ecx
	testb	$1, %cl
	jne	.LBB0_198
# BB#197:
	xorl	%esi, %esi
	cmpl	$1, 112(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_199
	jmp	.LBB0_201
.LBB0_198:
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%r15), %xmm0
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %rdx
	addss	(%rdx,%r14,4), %xmm0
	movss	%xmm0, (%rdx,%r14,4)
	movl	$1, %esi
	cmpl	$1, 112(%rsp)           # 4-byte Folded Reload
	je	.LBB0_201
.LBB0_199:                              # %.lr.ph124.i.new
	subq	%rsi, %rcx
	leaq	8(%r15,%rsi,4), %rdx
	movq	128(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_200:                              # =>This Inner Loop Header: Depth=1
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	movq	-8(%rsi), %rdi
	addss	(%rdi,%r14,4), %xmm0
	movss	%xmm0, (%rdi,%r14,4)
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movq	(%rsi), %rdi
	addss	(%rdi,%r14,4), %xmm0
	movss	%xmm0, (%rdi,%r14,4)
	addq	$8, %rdx
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB0_200
.LBB0_201:                              # %.preheader27.i
	cmpl	$2, %r11d
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	112(%rsp), %ebp         # 4-byte Reload
	jl	.LBB0_240
# BB#202:                               # %.lr.ph122.i
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	movslq	%ebp, %rcx
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %r10
	movq	160(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB0_209
# BB#203:                               # %min.iters.checked717
	movl	%edx, %esi
	andl	$7, %esi
	movq	%rcx, %r8
	subq	%rsi, %r8
	je	.LBB0_209
# BB#204:                               # %vector.memcheck738
	leaq	(%r10,%rcx,4), %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx), %r9
	leaq	4(%rdx,%rcx,4), %rbp
	cmpq	%rax, %r10
	sbbb	%bl, %bl
	cmpq	%rdi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %r10
	sbbb	%bl, %bl
	cmpq	%rdi, %r9
	sbbb	%dil, %dil
	xorl	%ebp, %ebp
	testb	$1, %dl
	jne	.LBB0_210
# BB#205:                               # %vector.memcheck738
	andb	%dil, %bl
	andb	$1, %bl
	jne	.LBB0_210
# BB#206:                               # %vector.body713.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	20(%rdx), %rbp
	leaq	16(%r10), %rbx
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB0_207:                              # %vector.body713
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbx), %xmm3
	movups	(%rbx), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbx)
	movups	%xmm4, (%rbx)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB0_207
# BB#208:                               # %middle.block714
	testl	%esi, %esi
	movq	%r8, %rbp
	jne	.LBB0_210
	jmp	.LBB0_215
.LBB0_209:
	xorl	%ebp, %ebp
.LBB0_210:                              # %scalar.ph715.preheader
	movl	%ecx, %edi
	subl	%ebp, %edi
	leaq	-1(%rcx), %rsi
	testb	$1, %dil
	movq	%rbp, %rdi
	je	.LBB0_212
# BB#211:                               # %scalar.ph715.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rbp), %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	addss	4(%rdx,%rbp,4), %xmm0
	addss	(%r10,%rbp,4), %xmm0
	movss	%xmm0, (%r10,%rbp,4)
.LBB0_212:                              # %scalar.ph715.prol.loopexit
	cmpq	%rbp, %rsi
	je	.LBB0_215
# BB#213:                               # %scalar.ph715.preheader.new
	subq	%rdi, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	8(%rdx,%rdi,4), %rsi
	leaq	4(%r10,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB0_214:                              # %scalar.ph715
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rsi
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB0_214
.LBB0_215:                              # %.lr.ph120.i
	movq	%r11, %rcx
	shlq	$32, %rcx
	movabsq	$-8589934592, %rax      # imm = 0xFFFFFFFE00000000
	addq	%rcx, %rax
	sarq	$30, %rax
	addq	32(%rsp), %rax          # 8-byte Folded Reload
	movq	%r14, %rdi
	notq	%rdi
	cmpq	$-3, %rdi
	movq	$-2, %rcx
	cmovgq	%rdi, %rcx
	leaq	2(%rcx,%r14), %r10
	cmpq	$3, %r10
	movq	%r14, %rsi
	movq	192(%rsp), %r9          # 8-byte Reload
	jbe	.LBB0_238
# BB#216:                               # %min.iters.checked756
	movq	%r10, %r8
	andq	$-4, %r8
	movq	%r14, %rsi
	je	.LBB0_238
# BB#217:                               # %vector.memcheck780
	cmpq	$-3, %rdi
	movq	$-2, %rdx
	cmovleq	%rdx, %rdi
	subq	%rdi, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rdx
	leaq	(%rcx,%r14,4), %rsi
	notq	%rdi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdi
	leaq	4(%rcx,%r14,4), %rbp
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	cmpq	%rsi, %rdi
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rax, %rdx
	sbbb	%dl, %dl
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	testb	$1, %cl
	movq	%r14, %rsi
	jne	.LBB0_238
# BB#218:                               # %vector.memcheck780
	andb	%bl, %dl
	andb	$1, %dl
	movq	%r14, %rsi
	jne	.LBB0_238
# BB#219:                               # %vector.ph781
	movd	112(%rsp), %xmm1        # 4-byte Folded Reload
                                        # xmm1 = mem[0],zero,zero,zero
	leaq	-4(%r8), %rcx
	movq	%rcx, %rdi
	shrq	$2, %rdi
	btl	$2, %ecx
	jb	.LBB0_233
# BB#220:                               # %vector.body752.prol
	movq	80(%rsp), %rcx          # 8-byte Reload
	movups	-12(%rcx,%r14,4), %xmm0
	shufps	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movq	56(%rsp), %rcx          # 8-byte Reload
	movups	%xmm2, -16(%rcx,%r14,4)
	pshufd	$0, %xmm1, %xmm0        # xmm0 = xmm1[0,0,0,0]
	movq	136(%rsp), %rcx         # 8-byte Reload
	movdqu	%xmm0, -12(%rcx,%r14,4)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB0_234
	jmp	.LBB0_236
.LBB0_221:                              # %vector.body424.preheader
	leaq	16(%rbp), %rsi
	leaq	16(%r13), %rdi
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_222:                              # %vector.body424
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbx
	jne	.LBB0_222
# BB#223:                               # %middle.block425
	testl	%edx, %edx
	jne	.LBB0_35
	jmp	.LBB0_41
.LBB0_224:                              # %vector.body453.preheader
	leaq	16(%r12), %rsi
	leaq	16(%r14), %rdi
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_225:                              # %vector.body453
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbx
	jne	.LBB0_225
# BB#226:                               # %middle.block454
	testl	%edx, %edx
	jne	.LBB0_47
	jmp	.LBB0_53
.LBB0_227:                              # %vector.body560.preheader
	leaq	-8(%rdi), %rbx
	movl	%ebx, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB0_339
# BB#228:                               # %vector.body560.prol.preheader
	negq	%rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_229:                              # %vector.body560.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	4(%r14,%rdx,4), %xmm0
	movups	20(%r14,%rdx,4), %xmm1
	movups	%xmm0, 4(%r10,%rdx,4)
	movups	%xmm1, 20(%r10,%rdx,4)
	addq	$8, %rdx
	incq	%rax
	jne	.LBB0_229
	jmp	.LBB0_340
.LBB0_230:
	movq	48(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_74
.LBB0_231:
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_176
.LBB0_232:
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_190
.LBB0_233:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB0_236
.LBB0_234:                              # %vector.ph781.new
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	-12(%rcx,%r14,4), %rbp
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	-16(%rcx,%r14,4), %rbx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	-12(%rcx,%r14,4), %rdx
	.p2align	4, 0x90
.LBB0_235:                              # %vector.body752
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, (%rbx,%rsi,4)
	movdqu	%xmm1, (%rdx,%rsi,4)
	movups	-16(%rbp,%rsi,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, -16(%rbx,%rsi,4)
	movdqu	%xmm1, -16(%rdx,%rsi,4)
	addq	$-8, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_235
.LBB0_236:                              # %middle.block753
	cmpq	%r8, %r10
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	112(%rsp), %ebp         # 4-byte Reload
	je	.LBB0_240
# BB#237:
	movq	%r14, %rsi
	subq	%r8, %rsi
.LBB0_238:                              # %scalar.ph754.preheader
	incq	%rsi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	112(%rsp), %ebp         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_239:                              # %scalar.ph754
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rbx,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, -8(%rcx,%rsi,4)
	movl	%ebp, -4(%rdx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB0_239
.LBB0_240:                              # %.preheader25.i
	xorpd	%xmm0, %xmm0
	cmpl	$2, %r13d
	movq	264(%rsp), %r8          # 8-byte Reload
	jl	.LBB0_304
# BB#241:                               # %.lr.ph103.lr.ph.i
	leal	-2(%r11), %eax
	movl	%eax, 364(%rsp)         # 4-byte Spill
	cltq
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movslq	%r11d, %rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	-8(%rax,%rdx,4), %r14
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movslq	168(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edi
	leal	2(%rax), %esi
	leaq	-3(%rdi), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	andl	$3, %esi
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	1(%rcx), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	(%r9,%rcx,4), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	-4(%r8,%rcx,4), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	%rsi, 456(%rsp)         # 8-byte Spill
	negq	%rsi
	movq	%rsi, 432(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movss	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	-4(%rax,%rcx,4), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rdx, 480(%rsp)         # 8-byte Spill
	leaq	-2(%rdx), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	leaq	(,%rcx,4), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rcx,4), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rcx,4), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movq	288(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rcx,4), %rax
	movq	%rax, 496(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	movl	%ebp, %eax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
	movq	%rdi, 400(%rsp)         # 8-byte Spill
.LBB0_242:                              # %.lr.ph103.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_243 Depth 2
                                        #       Child Loop BB0_244 Depth 3
                                        #         Child Loop BB0_245 Depth 4
                                        #       Child Loop BB0_248 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #       Child Loop BB0_256 Depth 3
                                        #       Child Loop BB0_264 Depth 3
                                        #       Child Loop BB0_281 Depth 3
                                        #       Child Loop BB0_284 Depth 3
                                        #       Child Loop BB0_287 Depth 3
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	movslq	%ebp, %rcx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_243:                              #   Parent Loop BB0_242 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_244 Depth 3
                                        #         Child Loop BB0_245 Depth 4
                                        #       Child Loop BB0_248 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #       Child Loop BB0_256 Depth 3
                                        #       Child Loop BB0_264 Depth 3
                                        #       Child Loop BB0_281 Depth 3
                                        #       Child Loop BB0_284 Depth 3
                                        #       Child Loop BB0_287 Depth 3
	movl	%edx, 180(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rax, %rdx
	movl	%ebp, %r12d
	movaps	%xmm1, %xmm2
	movq	%rcx, %rsi
	leaq	-1(%rsi), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%r12d, %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r13,4), %eax
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	%eax, (%rdx,%rcx,4)
	movl	RNAthr(%rip), %eax
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	movq	-8(%rcx,%rsi,8), %rbp
	addq	$4, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_244:                              #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_245 Depth 4
	movl	$0, 512(%rsp,%rsi,4)
	xorps	%xmm1, %xmm1
	movq	$-2704, %rdi            # imm = 0xF570
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_245:                              #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        #       Parent Loop BB0_244 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	n_dis+2704(%rdi,%rsi,4), %ecx
	subl	%eax, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	-4(%rdx), %xmm3
	addss	%xmm1, %xmm3
	movl	n_dis+2808(%rdi,%rsi,4), %ecx
	subl	%eax, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	mulss	(%rdx), %xmm1
	addss	%xmm3, %xmm1
	addq	$8, %rdx
	addq	$208, %rdi
	jne	.LBB0_245
# BB#246:                               #   in Loop: Header=BB0_244 Depth=3
	movss	%xmm1, 512(%rsp,%rsi,4)
	incq	%rsi
	cmpq	$26, %rsi
	jne	.LBB0_244
# BB#247:                               # %.preheader.i4.i
                                        #   in Loop: Header=BB0_243 Depth=2
	testl	%r11d, %r11d
	movl	%r11d, %eax
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	256(%rsp), %rsi         # 8-byte Reload
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	je	.LBB0_252
	.p2align	4, 0x90
.LBB0_248:                              # %.lr.ph85.i15.i
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_250 Depth 4
	decl	%eax
	movl	$0, (%rdi)
	movq	(%rdx), %rbp
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	js	.LBB0_251
# BB#249:                               # %.lr.ph.i16.i
                                        #   in Loop: Header=BB0_248 Depth=3
	movq	(%rsi), %rbx
	addq	$4, %rbp
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB0_250:                              #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        #       Parent Loop BB0_248 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%ecx, %rcx
	movss	512(%rsp,%rcx,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm3
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rdi)
	movl	(%rbp), %ecx
	addq	$4, %rbp
	addq	$4, %rbx
	testl	%ecx, %ecx
	jns	.LBB0_250
.LBB0_251:                              # %._crit_edge.i19.i
                                        #   in Loop: Header=BB0_248 Depth=3
	addq	$4, %rdi
	addq	$8, %rdx
	addq	$8, %rsi
	testl	%eax, %eax
	jne	.LBB0_248
.LBB0_252:                              # %match_calc.exit20.i
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpl	$1, %r11d
	movq	328(%rsp), %rbp         # 8-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%rbp,4), %eax
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	%eax, (%rbx,%rcx,4)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movss	-4(%rax,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	%rcx, %rax
	jle	.LBB0_262
# BB#253:                               # %.lr.ph75.i
                                        #   in Loop: Header=BB0_243 Depth=2
	leaq	(%rbx,%rax,4), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	472(%rsp), %rdx         # 8-byte Reload
	movss	(%rcx,%rdx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rdi
	cmpl	120(%rsp), %r12d        # 4-byte Folded Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rcx,%rbp,8), %rcx
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	-8(%rdx,%rbp,8), %rdx
	je	.LBB0_263
# BB#254:                               # %.lr.ph75.i
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpl	76(%rsp), %esi          # 4-byte Folded Reload
	je	.LBB0_263
# BB#255:                               # %.lr.ph75.i.split.preheader
                                        #   in Loop: Header=BB0_243 Depth=2
	movq	440(%rsp), %rsi         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rbx
	leaq	(%rdx,%rsi,4), %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_256:                              # %.lr.ph75.i.split
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbp,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm5
	maxss	%xmm1, %xmm5
	movq	168(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbp,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm6
	movaps	%xmm3, %xmm4
	cmpless	%xmm6, %xmm4
	andps	%xmm4, %xmm6
	andnps	%xmm3, %xmm4
	orps	%xmm6, %xmm4
	movss	(%r14,%rbp,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movq	8(%rsp), %rcx           # 8-byte Reload
	movss	(%rcx,%r13,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm3
	addss	(%rdi), %xmm1
	ucomiss	%xmm6, %xmm1
	jb	.LBB0_258
# BB#257:                               #   in Loop: Header=BB0_256 Depth=3
	movss	%xmm1, (%r14,%rbp,4)
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	%r12d, -8(%rcx,%rbp,4)
.LBB0_258:                              #   in Loop: Header=BB0_256 Depth=3
	maxss	%xmm5, %xmm3
	movq	96(%rsp), %rcx          # 8-byte Reload
	cmpl	120(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB0_260
# BB#259:                               #   in Loop: Header=BB0_256 Depth=3
	movq	384(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rcx,%rbp,4)
	movss	(%r14,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	392(%rsp), %rcx         # 8-byte Reload
	addss	(%rcx,%rbp,4), %xmm1
	movss	%xmm1, (%rcx,%rbp,4)
.LBB0_260:                              #   in Loop: Header=BB0_256 Depth=3
	movss	(%rbx,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rbx,%rbp,4)
	movss	-8(%rax,%rbp,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	(%rdx,%rbp,4), %xmm1
	movss	%xmm1, (%rdx,%rbp,4)
	addss	-8(%rax,%rbp,4), %xmm3
	movss	%xmm3, -8(%rax,%rbp,4)
	movss	(%rsi,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx,%rbp), %rcx
	decq	%rbp
	cmpq	$1, %rcx
	movaps	%xmm4, %xmm3
	jg	.LBB0_256
# BB#261:                               #   in Loop: Header=BB0_243 Depth=2
	movl	$-1, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebp
	jmp	.LBB0_274
	.p2align	4, 0x90
.LBB0_262:                              # %match_calc.exit20.._crit_edge76_crit_edge.i
                                        #   in Loop: Header=BB0_243 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rdi
	movl	%ecx, %ebp
	movl	364(%rsp), %eax         # 4-byte Reload
	jmp	.LBB0_275
	.p2align	4, 0x90
.LBB0_263:                              # %.lr.ph75.i.split.us.preheader
                                        #   in Loop: Header=BB0_243 Depth=2
	movq	424(%rsp), %rsi         # 8-byte Reload
	leaq	-4(%rdx,%rsi), %rbx
	leaq	-4(%rcx,%rsi), %r11
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	368(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,4), %r10
	xorl	%r8d, %r8d
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r9d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_264:                              # %.lr.ph75.i.split.us
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm3, %xmm6
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm4
	movq	400(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8), %rsi
	ucomiss	%xmm1, %xmm4
	maxss	%xmm1, %xmm4
	movl	%esi, %edx
	cmoval	%r9d, %edx
	movq	168(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm7     # xmm7 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm7
	ucomiss	%xmm6, %xmm7
	movaps	%xmm6, %xmm5
	cmpnless	%xmm7, %xmm5
	movaps	%xmm5, %xmm3
	andnps	%xmm7, %xmm3
	andps	%xmm6, %xmm5
	cmovael	%esi, %r9d
	movss	(%r14,%r8,4), %xmm6     # xmm6 = mem[0],zero,zero,zero
	movss	(%rbp,%r13,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm7
	ucomiss	%xmm4, %xmm7
	movl	%r12d, %ecx
	jbe	.LBB0_266
# BB#265:                               #   in Loop: Header=BB0_264 Depth=3
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	-8(%rcx,%r8,4), %ecx
	movaps	%xmm7, %xmm4
	movl	%esi, %edx
.LBB0_266:                              #   in Loop: Header=BB0_264 Depth=3
	orps	%xmm5, %xmm3
	addss	(%rdi), %xmm1
	ucomiss	%xmm6, %xmm1
	jb	.LBB0_268
# BB#267:                               #   in Loop: Header=BB0_264 Depth=3
	movss	%xmm1, (%r14,%r8,4)
	movq	336(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, -8(%rsi,%r8,4)
.LBB0_268:                              #   in Loop: Header=BB0_264 Depth=3
	movq	496(%rsp), %rsi         # 8-byte Reload
	movl	%ecx, (%rsi,%r8,4)
	movq	504(%rsp), %rcx         # 8-byte Reload
	movl	%edx, (%rcx,%r8,4)
	movq	96(%rsp), %rcx          # 8-byte Reload
	cmpl	120(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB0_270
# BB#269:                               #   in Loop: Header=BB0_264 Depth=3
	movq	384(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, (%rcx,%r8,4)
	movss	(%r14,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movq	392(%rsp), %rcx         # 8-byte Reload
	addss	(%rcx,%r8,4), %xmm1
	movss	%xmm1, (%rcx,%r8,4)
.LBB0_270:                              #   in Loop: Header=BB0_264 Depth=3
	cmpl	120(%rsp), %r12d        # 4-byte Folded Reload
	jne	.LBB0_272
# BB#271:                               #   in Loop: Header=BB0_264 Depth=3
	movq	488(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rcx,%r8,4)
.LBB0_272:                              #   in Loop: Header=BB0_264 Depth=3
	movss	(%r11,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, (%r11,%r8,4)
	movss	-8(%rax,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	(%rbx,%r8,4), %xmm1
	movss	%xmm1, (%rbx,%r8,4)
	addss	-8(%rax,%r8,4), %xmm4
	movss	%xmm4, -8(%rax,%r8,4)
	movss	(%r10,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx,%r8), %rcx
	decq	%r8
	cmpq	$1, %rcx
	jg	.LBB0_264
# BB#273:                               #   in Loop: Header=BB0_243 Depth=2
	movl	$-1, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebp
	movq	88(%rsp), %r11          # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	192(%rsp), %r9          # 8-byte Reload
.LBB0_274:                              # %._crit_edge76.i
                                        #   in Loop: Header=BB0_243 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB0_275:                              # %._crit_edge76.i
                                        #   in Loop: Header=BB0_243 Depth=2
	movl	180(%rsp), %edx         # 4-byte Reload
	addss	(%rdi), %xmm1
	ucomiss	%xmm2, %xmm1
	maxss	%xmm2, %xmm1
	movq	320(%rsp), %rcx         # 8-byte Reload
	cmoval	%r12d, %ecx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	cmpl	120(%rsp), %ebp         # 4-byte Folded Reload
	jne	.LBB0_277
# BB#276:                               #   in Loop: Header=BB0_243 Depth=2
	cltq
	movss	4(%r9,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movss	%xmm2, 4(%r9,%rax,4)
.LBB0_277:                              #   in Loop: Header=BB0_243 Depth=2
	cmpl	176(%rsp), %ebp         # 4-byte Folded Reload
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	160(%rsp), %r10         # 8-byte Reload
	movq	400(%rsp), %rdi         # 8-byte Reload
	jne	.LBB0_296
# BB#278:                               #   in Loop: Header=BB0_243 Depth=2
	movss	4(%r8), %xmm2           # xmm2 = mem[0],zero,zero,zero
	xorl	%r15d, %r15d
	cmpl	$3, %r10d
	jl	.LBB0_285
# BB#279:                               # %.lr.ph82.i.preheader
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpq	$0, 456(%rsp)           # 8-byte Folded Reload
	movq	432(%rsp), %rdx         # 8-byte Reload
	je	.LBB0_282
# BB#280:                               # %.lr.ph82.i.prol.preheader
                                        #   in Loop: Header=BB0_243 Depth=2
	xorl	%r15d, %r15d
	movl	$2, %eax
	movaps	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB0_281:                              # %.lr.ph82.i.prol
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmoval	%eax, %r15d
	leaq	1(%rdx,%rax), %rcx
	incq	%rax
	cmpq	$2, %rcx
	movaps	%xmm2, %xmm0
	jne	.LBB0_281
	jmp	.LBB0_283
.LBB0_282:                              #   in Loop: Header=BB0_243 Depth=2
	xorl	%r15d, %r15d
	movl	$2, %eax
.LBB0_283:                              # %.lr.ph82.i.prol.loopexit
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpq	$3, 448(%rsp)           # 8-byte Folded Reload
	jb	.LBB0_285
	.p2align	4, 0x90
.LBB0_284:                              # %.lr.ph82.i
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%eax, %r15d
	movss	4(%r8,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	leal	1(%rax), %ecx
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmovbel	%r15d, %ecx
	movss	8(%r8,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leal	2(%rax), %edx
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmovbel	%ecx, %edx
	movss	12(%r8,%rax,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	leal	3(%rax), %r15d
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmovbel	%edx, %r15d
	addq	$4, %rax
	cmpq	%rdi, %rax
	jne	.LBB0_284
.LBB0_285:                              # %.preheader24.i
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpl	$-1, %r10d
	jl	.LBB0_288
# BB#286:                               # %.lr.ph88.i.preheader
                                        #   in Loop: Header=BB0_243 Depth=2
	xorl	%eax, %eax
	movq	464(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_287:                              # %.lr.ph88.i
                                        #   Parent Loop BB0_242 Depth=1
                                        #     Parent Loop BB0_243 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r9,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%eax, %r15d
	incq	%rax
	cmpq	%rcx, %rax
	movaps	%xmm0, %xmm2
	jl	.LBB0_287
	jmp	.LBB0_289
	.p2align	4, 0x90
.LBB0_288:                              #   in Loop: Header=BB0_243 Depth=2
	movaps	%xmm2, %xmm0
.LBB0_289:                              # %._crit_edge89.i
                                        #   in Loop: Header=BB0_243 Depth=2
	movslq	%r15d, %rax
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	leal	-1(%rax), %edx
	testl	%eax, %eax
	jle	.LBB0_292
# BB#290:                               #   in Loop: Header=BB0_243 Depth=2
	movslq	%edx, %rcx
	movq	%r9, %rsi
	movq	%r11, %r13
	movl	%edx, %r9d
	movq	272(%rsp), %rdx         # 8-byte Reload
	movss	(%rdx,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movl	%r9d, %edx
	movq	%rsi, %r9
	ucomiss	%xmm2, %xmm3
	movl	%edx, %ecx
	jbe	.LBB0_293
# BB#291:                               #   in Loop: Header=BB0_243 Depth=2
	movq	344(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movaps	%xmm3, %xmm2
	jmp	.LBB0_293
.LBB0_292:                              #   in Loop: Header=BB0_243 Depth=2
	movl	%edx, %ecx
.LBB0_293:                              #   in Loop: Header=BB0_243 Depth=2
	movss	(%r9,%rax,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	jbe	.LBB0_295
# BB#294:                               #   in Loop: Header=BB0_243 Depth=2
	movq	352(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	cmpl	76(%rsp), %ebp          # 4-byte Folded Reload
	je	.LBB0_297
	jmp	.LBB0_299
	.p2align	4, 0x90
.LBB0_295:                              #   in Loop: Header=BB0_243 Depth=2
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
.LBB0_296:                              #   in Loop: Header=BB0_243 Depth=2
	cmpl	76(%rsp), %ebp          # 4-byte Folded Reload
	jne	.LBB0_299
.LBB0_297:                              #   in Loop: Header=BB0_243 Depth=2
	testl	%r15d, %r15d
	je	.LBB0_301
# BB#298:                               #   in Loop: Header=BB0_243 Depth=2
	cmpl	%r11d, %r15d
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
	movl	%r11d, %r15d
	jge	.LBB0_300
	jmp	.LBB0_302
	.p2align	4, 0x90
.LBB0_299:                              #   in Loop: Header=BB0_243 Depth=2
	movl	%edx, %eax
.LBB0_300:                              # %.backedge.i
                                        #   in Loop: Header=BB0_243 Depth=2
	cmpq	$1, 328(%rsp)           # 8-byte Folded Reload
	movl	%eax, %edx
	movq	%rbx, %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	jg	.LBB0_243
	jmp	.LBB0_304
.LBB0_301:                              #   in Loop: Header=BB0_242 Depth=1
	movq	320(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %ecx
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	movl	$1, %r15d
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmpl	$1, 328(%rsp)           # 4-byte Folded Reload
	jg	.LBB0_242
	jmp	.LBB0_304
.LBB0_302:                              #   in Loop: Header=BB0_242 Depth=1
	movslq	%edx, %rax
	movq	288(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %r15d
	movl	%ebp, 76(%rsp)          # 4-byte Spill
	cmpl	$1, 328(%rsp)           # 4-byte Folded Reload
	jg	.LBB0_242
.LBB0_304:                              # %.preheader23.i
	movq	104(%rsp), %rcx         # 8-byte Reload
	testl	%ecx, %ecx
	movq	128(%rsp), %r15         # 8-byte Reload
	jle	.LBB0_322
# BB#305:                               # %.preheader22.preheader.i
	movq	712(%rsp), %r9
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	movl	%ecx, %r14d
	leaq	-1(%rbp), %r11
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rbp, %r10
	subq	%r8, %r10
	movapd	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_306:                              # %.preheader22.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_314 Depth 2
                                        #     Child Loop BB0_320 Depth 2
	cmpq	$4, %rbp
	movq	(%r15,%rbx,8), %rdx
	movq	(%r9,%rbx,8), %rsi
	jae	.LBB0_308
# BB#307:                               #   in Loop: Header=BB0_306 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB0_316
	.p2align	4, 0x90
.LBB0_308:                              # %min.iters.checked800
                                        #   in Loop: Header=BB0_306 Depth=1
	testq	%r10, %r10
	je	.LBB0_312
# BB#309:                               # %vector.memcheck813
                                        #   in Loop: Header=BB0_306 Depth=1
	leaq	(%rdx,%rbp,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB0_313
# BB#310:                               # %vector.memcheck813
                                        #   in Loop: Header=BB0_306 Depth=1
	leaq	(%rsi,%rbp,4), %rax
	cmpq	%rax, %rdx
	jae	.LBB0_313
.LBB0_312:                              #   in Loop: Header=BB0_306 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB0_316
.LBB0_313:                              # %vector.ph814
                                        #   in Loop: Header=BB0_306 Depth=1
	movq	%r10, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB0_314:                              # %vector.body796
                                        #   Parent Loop BB0_306 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi), %xmm2
	divps	%xmm1, %xmm2
	movups	%xmm2, (%rax)
	addq	$16, %rdi
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB0_314
# BB#315:                               # %middle.block797
                                        #   in Loop: Header=BB0_306 Depth=1
	testq	%r8, %r8
	movq	%r10, %rcx
	je	.LBB0_321
	.p2align	4, 0x90
.LBB0_316:                              # %scalar.ph798.preheader
                                        #   in Loop: Header=BB0_306 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%ecx, %eax
	testb	$1, %al
	movq	%rcx, %rdi
	je	.LBB0_318
# BB#317:                               # %scalar.ph798.prol
                                        #   in Loop: Header=BB0_306 Depth=1
	movss	(%rdx,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, (%rsi,%rcx,4)
	leaq	1(%rcx), %rdi
.LBB0_318:                              # %scalar.ph798.prol.loopexit
                                        #   in Loop: Header=BB0_306 Depth=1
	cmpq	%rcx, %r11
	je	.LBB0_321
# BB#319:                               # %scalar.ph798.preheader.new
                                        #   in Loop: Header=BB0_306 Depth=1
	movq	%rbp, %rcx
	subq	%rdi, %rcx
	leaq	4(%rsi,%rdi,4), %rax
	leaq	4(%rdx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_320:                              # %scalar.ph798
                                        #   Parent Loop BB0_306 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-4(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, -4(%rax)
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, (%rax)
	addq	$8, %rax
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB0_320
.LBB0_321:                              # %._crit_edge64.i
                                        #   in Loop: Header=BB0_306 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB0_306
.LBB0_322:                              # %._crit_edge66.i
	movq	224(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	296(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	312(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	192(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	344(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	352(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	288(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	416(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	408(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	256(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	200(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movq	%r15, %rdi
	callq	FreeFloatMtx
	movq	248(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
.LBB0_323:                              # %MSalignmm_rec.exit
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	FreeFloatVec
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	184(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	232(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	376(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	304(%rsp), %rdi         # 8-byte Reload
	callq	FreeCharMtx
	movq	%r12, %rdi
	callq	FreeCharMtx
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_330
# BB#324:                               # %.lr.ph243
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movslq	%eax, %r12
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%r12, %rax
	jne	.LBB0_348
# BB#325:                               # %.lr.ph385.preheader
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_326:                              # %.lr.ph385
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rbp
	jge	.LBB0_330
# BB#327:                               # %._crit_edge306
                                        #   in Loop: Header=BB0_326 Depth=1
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	callq	strlen
	incq	%rbp
	cmpq	%r12, %rax
	je	.LBB0_326
# BB#328:                               # %._crit_edge386
	decl	%ebp
.LBB0_329:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB0_337
.LBB0_330:                              # %.preheader
	movl	72(%rsp), %ebx          # 4-byte Reload
	testl	%ebx, %ebx
	jle	.LBB0_338
# BB#331:                               # %.lr.ph
	movslq	%r14d, %r14
	movq	%r15, %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB0_349
# BB#332:                               # %.lr.ph382.preheader
	movslq	%ebx, %rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_333:                              # %.lr.ph382
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB0_338
# BB#334:                               # %._crit_edge309
                                        #   in Loop: Header=BB0_333 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	strlen
	incq	%rbx
	cmpq	%r14, %rax
	je	.LBB0_333
# BB#335:                               # %._crit_edge383
	decl	%ebx
.LBB0_336:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	72(%rsp), %ecx          # 4-byte Reload
.LBB0_337:
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$42, %esi
	jmp	.LBB0_347
.LBB0_338:                              # %._crit_edge
	xorps	%xmm0, %xmm0
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_339:
	xorl	%edx, %edx
.LBB0_340:                              # %vector.body560.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB0_342
	.p2align	4, 0x90
.LBB0_341:                              # %vector.body560
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rdx,4), %rax
	orq	$4, %rax
	movups	(%r14,%rax), %xmm0
	movups	16(%r14,%rax), %xmm1
	movups	%xmm0, (%r10,%rax)
	movups	%xmm1, 16(%r10,%rax)
	leaq	8(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	leaq	16(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	leaq	24(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	addq	$32, %rdx
	cmpq	%rdi, %rdx
	jne	.LBB0_341
.LBB0_342:                              # %middle.block561
	cmpq	%rdi, %r8
	je	.LBB0_124
# BB#343:
	orq	$1, %rdi
	jmp	.LBB0_118
.LBB0_344:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB0_346
.LBB0_345:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	72(%rsp), %ecx          # 4-byte Reload
.LBB0_346:
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$27, %esi
.LBB0_347:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_348:
	xorl	%ebp, %ebp
	jmp	.LBB0_329
.LBB0_349:
	xorl	%ebx, %ebx
	jmp	.LBB0_336
.Lfunc_end0:
	.size	Lalignmm_hmout, .Lfunc_end0-Lalignmm_hmout
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_2:
	.quad	4607182418800017408     # double 1
.LCPI1_3:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_4:
	.long	3407386240              # float -1.0E+7
	.text
	.globl	Lalign2m2m_hmout
	.p2align	4, 0x90
	.type	Lalign2m2m_hmout,@function
Lalign2m2m_hmout:                       # @Lalign2m2m_hmout
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$648, %rsp              # imm = 0x288
.Lcfi31:
	.cfi_def_cfa_offset 704
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	728(%rsp), %r13d
	movl	720(%rsp), %r14d
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 72(%rsp)         # 4-byte Spill
	movq	(%rbx), %rdi
	callq	seqlen
	movq	(%r12), %rdi
	callq	seqlen
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %r15
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rbp
	leal	200(%r15,%rbp), %ebx
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	$4, %edi
	xorl	%esi, %esi
	callq	AllocateFloatMtx
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	%r15, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leal	102(%rbp), %r15d
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$39, %esi
	movl	%ebx, 160(%rsp)         # 4-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$39, %esi
	movl	%r15d, %edi
	callq	AllocateFloatMtx
	movq	%rax, 176(%rsp)         # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB1_4
# BB#1:                                 # %.lr.ph258
	movslq	80(%rsp), %r14          # 4-byte Folded Reload
	movl	720(%rsp), %eax
	movslq	%eax, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB1_346
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB1_2
.LBB1_4:                                # %.preheader215
	movl	728(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB1_8
# BB#5:                                 # %.lr.ph254
	movslq	56(%rsp), %r14          # 4-byte Folded Reload
	movl	728(%rsp), %eax
	movslq	%eax, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB1_347
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB1_6
.LBB1_8:                                # %._crit_edge255
	movl	%r15d, 64(%rsp)         # 4-byte Spill
	movq	%r12, %rbp
	movq	712(%rsp), %rbx
	movq	704(%rsp), %r8
	subq	$8, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %r9d
	movl	728(%rsp), %r14d
	pushq	%r14
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	cpmx_ribosum
	addq	$16, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -16
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	%rbx, %r8
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %r9d
	movl	736(%rsp), %ebx
	pushq	%rbx
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	cpmx_ribosum
	movq	760(%rsp), %r9
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	testq	%r9, %r9
	movq	%rbp, 224(%rsp)         # 8-byte Spill
	je	.LBB1_10
# BB#9:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	720(%rsp), %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r15, %rdx
	movq	704(%rsp), %rax
	movq	%rax, %rcx
	movl	%r12d, %r8d
	callq	new_OpeningGapCount
	movq	120(%rsp), %rdi         # 8-byte Reload
	movl	%ebx, %esi
	movq	%rbp, %rdx
	movq	712(%rsp), %r14
	movq	%r14, %rcx
	movl	%r13d, %r8d
	movq	752(%rsp), %r9
	callq	new_OpeningGapCount
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	720(%rsp), %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r15, %rdx
	movq	704(%rsp), %rcx
	movl	%r12d, %r8d
	movq	768(%rsp), %rbx
	movq	%rbx, %r9
	callq	new_FinalGapCount
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	728(%rsp), %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movl	%r13d, %r8d
	movq	%rbx, %r9
	movl	720(%rsp), %r13d
	callq	new_FinalGapCount
	jmp	.LBB1_11
.LBB1_10:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	704(%rsp), %rax
	movq	%rax, %rcx
	movl	%r12d, %r8d
	callq	st_OpeningGapCount
	movq	120(%rsp), %rdi         # 8-byte Reload
	movl	%ebx, %esi
	movq	%rbp, %rdx
	movq	712(%rsp), %rax
	movq	%rax, %rcx
	movl	%r13d, %r8d
	callq	st_OpeningGapCount
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	704(%rsp), %rcx
	movl	%r12d, %r8d
	callq	st_FinalGapCount
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%rbp, %rdx
	movq	712(%rsp), %rcx
	movl	%r13d, %r8d
	callq	st_FinalGapCount
	movq	%r14, %r13
.LBB1_11:                               # %.preheader214
	testl	%r12d, %r12d
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movss	72(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	jle	.LBB1_19
# BB#12:                                # %.lr.ph252
	cvtss2sd	%xmm7, %xmm0
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %eax
	cmpq	$3, %rax
	jbe	.LBB1_16
# BB#13:                                # %min.iters.checked
	movl	%ecx, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB1_16
# BB#14:                                # %vector.memcheck
	leaq	(%rbp,%rax,4), %rsi
	cmpq	%rsi, %r8
	jae	.LBB1_98
# BB#15:                                # %vector.memcheck
	leaq	(%r8,%rax,4), %rsi
	cmpq	%rsi, %rbp
	jae	.LBB1_98
.LBB1_16:
	xorl	%ecx, %ecx
.LBB1_17:                               # %scalar.ph.preheader
	leaq	(%r8,%rcx,4), %rdx
	leaq	(%rbp,%rcx,4), %rsi
	subq	%rcx, %rax
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB1_18:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB1_18
.LBB1_19:                               # %.preheader213
	testl	%ebx, %ebx
	jle	.LBB1_28
# BB#20:                                # %.lr.ph249
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movl	%ebx, %eax
	cmpq	$3, %rax
	jbe	.LBB1_25
# BB#21:                                # %min.iters.checked402
	movl	%ebx, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB1_25
# BB#22:                                # %vector.memcheck415
	leaq	(%rdi,%rax,4), %rsi
	movq	120(%rsp), %rbp         # 8-byte Reload
	cmpq	%rsi, %rbp
	jae	.LBB1_101
# BB#23:                                # %vector.memcheck415
	leaq	(%rbp,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB1_101
# BB#24:
	xorl	%ecx, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB1_26
.LBB1_25:
	xorl	%ecx, %ecx
.LBB1_26:                               # %scalar.ph400.preheader
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	leaq	(%rdi,%rcx,4), %rsi
	subq	%rcx, %rax
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB1_27:                               # %scalar.ph400
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rsi)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB1_27
.LBB1_28:                               # %._crit_edge250
	movq	368(%rsp), %rax         # 8-byte Reload
	movq	%r8, (%rax)
	movq	%rbp, 8(%rax)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 16(%rax)
	movq	%rdi, 24(%rax)
	incl	reccycle(%rip)
	testl	%ebx, %ebx
	jle	.LBB1_63
# BB#29:
	xorl	%esi, %esi
	movl	%r13d, %edi
	callq	AllocateCharMtx
	movq	%rax, %r15
	xorl	%esi, %esi
	movl	728(%rsp), %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	AllocateCharMtx
	movq	%rax, %r14
	testl	%r13d, %r13d
	movq	288(%rsp), %rbx         # 8-byte Reload
	movq	208(%rsp), %r8          # 8-byte Reload
	jle	.LBB1_41
# BB#30:                                # %.lr.ph172.preheader.i
	movl	%r13d, %eax
	cmpl	$3, %r13d
	jbe	.LBB1_34
# BB#31:                                # %min.iters.checked432
	movl	%r13d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB1_34
# BB#32:                                # %vector.memcheck445
	leaq	(%rbx,%rax,8), %rsi
	cmpq	%rsi, %r15
	jae	.LBB1_221
# BB#33:                                # %vector.memcheck445
	leaq	(%r15,%rax,8), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB1_221
.LBB1_34:
	xorl	%ecx, %ecx
.LBB1_35:                               # %.lr.ph172.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_38
# BB#36:                                # %.lr.ph172.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph172.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%r15,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_37
.LBB1_38:                               # %.lr.ph172.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_41
# BB#39:                                # %.lr.ph172.i.preheader.new
	subq	%rcx, %rax
	leaq	56(%r15,%rcx,8), %rdx
	leaq	56(%rbx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph172.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB1_40
.LBB1_41:                               # %.preheader33.i
	movl	728(%rsp), %ecx
	testl	%ecx, %ecx
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	80(%rsp), %r13          # 8-byte Reload
	jle	.LBB1_53
# BB#42:                                # %.lr.ph168.preheader.i
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.LBB1_46
# BB#43:                                # %min.iters.checked461
	movl	%ecx, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB1_46
# BB#44:                                # %vector.memcheck474
	leaq	(%r8,%rax,8), %rsi
	cmpq	%rsi, %r14
	jae	.LBB1_224
# BB#45:                                # %vector.memcheck474
	leaq	(%r14,%rax,8), %rsi
	cmpq	%rsi, %r8
	jae	.LBB1_224
.LBB1_46:
	xorl	%ecx, %ecx
.LBB1_47:                               # %.lr.ph168.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_50
# BB#48:                                # %.lr.ph168.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph168.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rdi
	movq	%rdi, (%r14,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_49
.LBB1_50:                               # %.lr.ph168.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_53
# BB#51:                                # %.lr.ph168.i.preheader.new
	subq	%rcx, %rax
	leaq	56(%r14,%rcx,8), %rdx
	leaq	56(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_52:                               # %.lr.ph168.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB1_52
.LBB1_53:                               # %._crit_edge169.i
	cmpl	$10, %r13d
	jl	.LBB1_72
# BB#54:                                # %._crit_edge169.i
	cmpl	$9, %r9d
	jle	.LBB1_72
# BB#55:
	leal	100(%r13), %r15d
	leal	100(%r9), %r14d
	movl	64(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movl	160(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	cmpl	%r14d, %ebp
	movq	%r13, %rbx
	movl	%r14d, %r13d
	cmovgel	%ebp, %r13d
	addl	$2, %r13d
	movl	%r13d, %edi
	callq	AllocateCharVec
	movl	$26, %esi
	movl	%r13d, %edi
	callq	AllocateFloatMtx
	movq	%rax, %r15
	movl	$26, %esi
	movl	%r13d, %edi
	callq	AllocateIntMtx
	movq	%rax, %r13
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movl	$0, %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	176(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rsi
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdx
	movl	%ebx, %r8d
	movq	%r15, %r9
	pushq	$1
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	match_ribosum
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	movl	$0, %ecx
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rdx
	movq	56(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r15, 248(%rsp)         # 8-byte Spill
	movq	%r15, %r9
	pushq	$1
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	%r13, 248(%rsp)         # 8-byte Spill
	pushq	%r13
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	match_ribosum
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	leal	1(%rbx), %r9d
	testl	%ebx, %ebx
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jle	.LBB1_79
# BB#56:                                # %.lr.ph165.preheader.i
	movl	%r9d, %ecx
	leaq	-1(%rcx), %rdx
	cmpq	$7, %rdx
	jbe	.LBB1_73
# BB#57:                                # %min.iters.checked490
	movq	%rdx, %r8
	andq	$-8, %r8
	je	.LBB1_73
# BB#58:                                # %vector.memcheck509
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi), %rax
	leaq	(%rsi,%rcx,4), %rsi
	leaq	-4(%rbp,%rcx,4), %rdi
	cmpq	%r12, %rax
	sbbb	%r10b, %r10b
	cmpq	%rsi, %r12
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%rdi, %rax
	sbbb	%al, %al
	cmpq	%rsi, %rbp
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB1_230
# BB#59:                                # %vector.memcheck509
	andb	%dil, %al
	andb	$1, %al
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_74
# BB#60:                                # %vector.body486.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbp), %rax
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	20(%rdi), %rbp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_61:                               # %vector.body486
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_61
# BB#62:                                # %middle.block487
	cmpq	%r8, %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB1_74
	jmp	.LBB1_79
.LBB1_63:                               # %.preheader21.i
	testl	%r13d, %r13d
	movq	80(%rsp), %r13          # 8-byte Reload
	jle	.LBB1_66
# BB#64:                                # %.lr.ph61.i
	movslq	%r13d, %r14
	movl	720(%rsp), %eax
	movl	%eax, %r15d
	movq	288(%rsp), %rbx         # 8-byte Reload
	movq	192(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r14, %rdx
	callq	strncpy
	movq	(%rbx), %rax
	movb	$0, (%rax,%r14)
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB1_65
.LBB1_66:                               # %.preheader.i
	movl	728(%rsp), %eax
	testl	%eax, %eax
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	208(%rsp), %rdx         # 8-byte Reload
	jle	.LBB1_325
# BB#67:                                # %.lr.ph59.i
	testl	%r13d, %r13d
	movl	728(%rsp), %eax
	movl	%eax, %r14d
	jle	.LBB1_104
# BB#68:                                # %.lr.ph59.split.i.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_69:                               # %.lr.ph59.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_70 Depth 2
	movq	(%rdx,%r15,8), %rax
	movb	$0, (%rax)
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB1_70:                               #   Parent Loop BB1_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%r15,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	208(%rsp), %rdx         # 8-byte Reload
	movw	$45, (%rbx,%rax)
	decl	%ebp
	jne	.LBB1_70
# BB#71:                                # %._crit_edge.i
                                        #   in Loop: Header=BB1_69 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jne	.LBB1_69
	jmp	.LBB1_325
.LBB1_72:
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	208(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB1_325
.LBB1_73:
	movl	$1, %esi
.LBB1_74:                               # %.lr.ph165.i.preheader
	movl	%ecx, %eax
	subl	%esi, %eax
	testb	$1, %al
	movq	%rsi, %rdi
	je	.LBB1_76
# BB#75:                                # %.lr.ph165.i.prol
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rbp,%rsi,4), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	addss	(%rax,%rsi,4), %xmm0
	movss	%xmm0, (%rax,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB1_76:                               # %.lr.ph165.i.prol.loopexit
	cmpq	%rsi, %rdx
	je	.LBB1_79
# BB#77:                                # %.lr.ph165.i.preheader.new
	subq	%rdi, %rcx
	leaq	(%rbp,%rdi,4), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB1_78:                               # %.lr.ph165.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rax), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rax
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB1_78
.LBB1_79:                               # %.preheader32.i
	movq	56(%rsp), %r11          # 8-byte Reload
	leal	1(%r11), %r15d
	testl	%r11d, %r11d
	movq	216(%rsp), %r14         # 8-byte Reload
	jle	.LBB1_93
# BB#80:                                # %.lr.ph160.preheader.i
	movl	%r15d, %ecx
	leaq	-1(%rcx), %rdx
	cmpq	$7, %rdx
	jbe	.LBB1_87
# BB#81:                                # %min.iters.checked527
	movq	%rdx, %r8
	andq	$-8, %r8
	je	.LBB1_87
# BB#82:                                # %vector.memcheck548
	leaq	4(%r14), %rax
	leaq	(%r14,%rcx,4), %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	-4(%rbp,%rcx,4), %rdi
	cmpq	%r13, %rax
	sbbb	%r10b, %r10b
	cmpq	%rsi, %r13
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%rdi, %rax
	sbbb	%al, %al
	cmpq	%rsi, %rbp
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB1_231
# BB#83:                                # %vector.memcheck548
	andb	%dil, %al
	andb	$1, %al
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_88
# BB#84:                                # %vector.body523.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rax
	leaq	20(%r14), %rbp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_85:                               # %vector.body523
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_85
# BB#86:                                # %middle.block524
	cmpq	%r8, %rdx
	jne	.LBB1_88
	jmp	.LBB1_93
.LBB1_87:
	movl	$1, %esi
.LBB1_88:                               # %.lr.ph160.i.preheader
	movl	%ecx, %eax
	subl	%esi, %eax
	testb	$1, %al
	movq	%rsi, %rdi
	je	.LBB1_90
# BB#89:                                # %.lr.ph160.i.prol
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rax          # 8-byte Reload
	addss	-4(%rax,%rsi,4), %xmm0
	addss	(%r14,%rsi,4), %xmm0
	movss	%xmm0, (%r14,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB1_90:                               # %.lr.ph160.i.prol.loopexit
	cmpq	%rsi, %rdx
	je	.LBB1_93
# BB#91:                                # %.lr.ph160.i.preheader.new
	subq	%rdi, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rax
	leaq	4(%r14,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB1_92:                               # %.lr.ph160.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rax), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rax
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB1_92
.LBB1_93:                               # %._crit_edge161.i
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %r10
	movl	%eax, (%r10)
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_112
# BB#94:                                # %.lr.ph158.preheader.i
	movl	%r9d, %ecx
	leal	3(%r9), %edi
	leaq	-2(%rcx), %r8
	andq	$3, %rdi
	je	.LBB1_109
# BB#95:                                # %.lr.ph158.i.prol.preheader
	xorl	%edx, %edx
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_96:                               # %.lr.ph158.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rsi,%rdx,4), %ebp
	movq	8(%rax,%rdx,8), %rbx
	movl	%ebp, (%rbx)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB1_96
# BB#97:                                # %.lr.ph158.i.prol.loopexit.unr-lcssa
	incq	%rdx
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpq	$3, %r8
	jae	.LBB1_110
	jmp	.LBB1_112
.LBB1_98:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI1_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rsi
	movq	%rbp, %rdi
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB1_99:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB1_99
# BB#100:                               # %middle.block
	testq	%rdx, %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_17
	jmp	.LBB1_19
.LBB1_101:                              # %vector.ph416
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI1_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB1_102:                              # %vector.body398
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB1_102
# BB#103:                               # %middle.block399
	testq	%rdx, %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_26
	jmp	.LBB1_28
.LBB1_104:                              # %.lr.ph59.split.us.i.preheader
	leaq	-1(%r14), %rax
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	andq	$7, %rdi
	je	.LBB1_106
	.p2align	4, 0x90
.LBB1_105:                              # %.lr.ph59.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rcx,8), %rsi
	movb	$0, (%rsi)
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB1_105
.LBB1_106:                              # %.lr.ph59.split.us.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB1_325
# BB#107:                               # %.lr.ph59.split.us.i.preheader.new
	subq	%rcx, %r14
	leaq	56(%rdx,%rcx,8), %rax
	.p2align	4, 0x90
.LBB1_108:                              # %.lr.ph59.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rcx
	movb	$0, (%rcx)
	movq	-48(%rax), %rcx
	movb	$0, (%rcx)
	movq	-40(%rax), %rcx
	movb	$0, (%rcx)
	movq	-32(%rax), %rcx
	movb	$0, (%rcx)
	movq	-24(%rax), %rcx
	movb	$0, (%rcx)
	movq	-16(%rax), %rcx
	movb	$0, (%rcx)
	movq	-8(%rax), %rcx
	movb	$0, (%rcx)
	movq	(%rax), %rcx
	movb	$0, (%rcx)
	addq	$64, %rax
	addq	$-8, %r14
	jne	.LBB1_108
	jmp	.LBB1_325
.LBB1_109:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB1_112
.LBB1_110:                              # %.lr.ph158.preheader.i.new
	subq	%rdx, %rcx
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	24(%rax,%rdx,8), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	12(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_111:                              # %.lr.ph158.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %edi
	movq	-24(%rax), %rbp
	movl	%edi, (%rbp)
	movl	-8(%rdx), %edi
	movq	-16(%rax), %rbp
	movl	%edi, (%rbp)
	movl	-4(%rdx), %edi
	movq	-8(%rax), %rbp
	movl	%edi, (%rbp)
	movl	(%rdx), %edi
	movq	(%rax), %rbp
	movl	%edi, (%rbp)
	addq	$32, %rax
	addq	$16, %rdx
	addq	$-4, %rcx
	jne	.LBB1_111
.LBB1_112:                              # %.preheader31.i
	leal	-1(%r11), %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	testl	%r11d, %r11d
	movq	(%rsp), %rbp            # 8-byte Reload
	jle	.LBB1_137
# BB#113:                               # %.lr.ph155.preheader.i
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r8
	cmpq	$7, %r8
	jbe	.LBB1_117
# BB#114:                               # %min.iters.checked568
	movq	%r8, %rdi
	andq	$-8, %rdi
	je	.LBB1_117
# BB#115:                               # %vector.memcheck585
	leaq	4(%r10), %rax
	leaq	(%r14,%rcx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB1_227
# BB#116:                               # %vector.memcheck585
	leaq	(%r10,%rcx,4), %rax
	leaq	4(%r14), %rdx
	cmpq	%rax, %rdx
	jae	.LBB1_227
.LBB1_117:
	movl	$1, %edi
.LBB1_118:                              # %.lr.ph155.i.preheader
	movl	%ecx, %edx
	subl	%edi, %edx
	movq	%r8, %rax
	subq	%rdi, %rax
	andq	$7, %rdx
	je	.LBB1_121
# BB#119:                               # %.lr.ph155.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB1_120:                              # %.lr.ph155.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rdi,4), %ebp
	movl	%ebp, (%r10,%rdi,4)
	incq	%rdi
	incq	%rdx
	jne	.LBB1_120
.LBB1_121:                              # %.lr.ph155.i.prol.loopexit
	cmpq	$7, %rax
	movq	(%rsp), %rbp            # 8-byte Reload
	jb	.LBB1_124
# BB#122:                               # %.lr.ph155.i.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	28(%r10,%rdi,4), %rsi
	leaq	28(%r14,%rdi,4), %rax
	.p2align	4, 0x90
.LBB1_123:                              # %.lr.ph155.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %edi
	movl	%edi, -28(%rsi)
	movl	-24(%rax), %edi
	movl	%edi, -24(%rsi)
	movl	-20(%rax), %edi
	movl	%edi, -20(%rsi)
	movl	-16(%rax), %edi
	movl	%edi, -16(%rsi)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rsi)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rsi)
	movl	-4(%rax), %edi
	movl	%edi, -4(%rsi)
	movl	(%rax), %edi
	movl	%edi, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-8, %rdx
	jne	.LBB1_123
.LBB1_124:                              # %.lr.ph152.i
	leaq	4(%r12), %rsi
	cmpq	$7, %r8
	jbe	.LBB1_131
# BB#125:                               # %min.iters.checked603
	movq	%r8, %r10
	andq	$-8, %r10
	movq	32(%rsp), %rdi          # 8-byte Reload
	je	.LBB1_131
# BB#126:                               # %vector.memcheck624
	leaq	4(%rdi), %rax
	leaq	(%rdi,%rcx,4), %rdx
	movq	%rdi, %r11
	leaq	-4(%r14,%rcx,4), %rdi
	cmpq	%rdi, %rax
	sbbb	%al, %al
	cmpq	%rdx, %r14
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r12, %r11
	sbbb	%al, %al
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	movl	$1, %r11d
	testb	$1, %bl
	jne	.LBB1_232
# BB#127:                               # %vector.memcheck624
	andb	%dl, %al
	andb	$1, %al
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_132
# BB#128:                               # %vector.body599.preheader
	movq	%r10, %r11
	orq	$1, %r11
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r14), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	20(%rax), %rax
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	20(%rdx), %rbp
	xorps	%xmm1, %xmm1
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB1_129:                              # %vector.body599
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rbx
	addq	$32, %rax
	addq	$32, %rbp
	addq	$-8, %rdx
	jne	.LBB1_129
# BB#130:                               # %middle.block600
	cmpq	%r10, %r8
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB1_132
	jmp	.LBB1_137
.LBB1_131:
	movl	$1, %r11d
.LBB1_132:                              # %scalar.ph601.preheader
	movl	%ecx, %eax
	subl	%r11d, %eax
	testb	$1, %al
	movq	%r11, %r10
	je	.LBB1_134
# BB#133:                               # %scalar.ph601.prol
	movss	-4(%r14,%r11,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	%xmm0, (%rax,%r11,4)
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax,%r11,4)
	leaq	1(%r11), %r10
.LBB1_134:                              # %scalar.ph601.prol.loopexit
	cmpq	%r11, %r8
	je	.LBB1_137
# BB#135:                               # %scalar.ph601.preheader.new
	subq	%r10, %rcx
	leaq	(%r14,%r10,4), %rdx
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax,%r10,4), %rax
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%r10,4), %rdi
	.p2align	4, 0x90
.LBB1_136:                              # %scalar.ph601
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, -4(%rdi)
	movl	$0, -4(%rax)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rdi)
	movl	$0, (%rax)
	addq	$8, %rdx
	addq	$8, %rax
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB1_136
.LBB1_137:                              # %._crit_edge153.i
	movl	%r15d, 200(%rsp)        # 4-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movslq	128(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	(%r14,%rax,4), %eax
	movq	296(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	.LCPI1_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r14, %r10
	movq	96(%rsp), %r8           # 8-byte Reload
	jle	.LBB1_167
# BB#138:                               # %.lr.ph147.i
	movslq	56(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	104(%rsp), %eax         # 4-byte Reload
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	200(%rsp), %r14d        # 4-byte Reload
	movl	%r9d, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	addq	$-2, %r14
	movl	$1, %r15d
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_139:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_140 Depth 2
                                        #       Child Loop BB1_142 Depth 3
                                        #     Child Loop BB1_145 Depth 2
                                        #       Child Loop BB1_147 Depth 3
                                        #     Child Loop BB1_163 Depth 2
                                        #     Child Loop BB1_155 Depth 2
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rax, %rcx
	leaq	-1(%r15), %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r15,4), %eax
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx)
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	addq	$4, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_140:                              #   Parent Loop BB1_139 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_142 Depth 3
	movl	$0, 496(%rsp,%rcx,4)
	xorps	%xmm1, %xmm1
	movq	$-5328, %rdx            # imm = 0xEB30
	movq	%rax, %rsi
	jmp	.LBB1_142
	.p2align	4, 0x90
.LBB1_141:                              #   in Loop: Header=BB1_142 Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	ribosumdis+5476(%rdx,%rcx,4), %xmm1
	mulss	(%rsi), %xmm1
	addss	%xmm1, %xmm0
	addq	$296, %rdx              # imm = 0x128
	addq	$8, %rsi
	movaps	%xmm0, %xmm1
.LBB1_142:                              #   Parent Loop BB1_139 Depth=1
                                        #     Parent Loop BB1_140 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm0, %xmm0
	cvtsi2ssl	ribosumdis+5328(%rdx,%rcx,4), %xmm0
	mulss	-4(%rsi), %xmm0
	addss	%xmm1, %xmm0
	testq	%rdx, %rdx
	jne	.LBB1_141
# BB#143:                               #   in Loop: Header=BB1_140 Depth=2
	movss	%xmm0, 496(%rsp,%rcx,4)
	incq	%rcx
	cmpq	$37, %rcx
	jne	.LBB1_140
# BB#144:                               # %.preheader.i.i
                                        #   in Loop: Header=BB1_139 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	240(%rsp), %rcx         # 8-byte Reload
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	%rbp, %r10
	je	.LBB1_149
	.p2align	4, 0x90
.LBB1_145:                              # %.lr.ph85.i.i
                                        #   Parent Loop BB1_139 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_147 Depth 3
	decl	%eax
	movl	$0, (%rbp)
	movq	(%rcx), %rbx
	movl	(%rbx), %edi
	testl	%edi, %edi
	js	.LBB1_148
# BB#146:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB1_145 Depth=2
	movq	(%rdx), %rsi
	addq	$4, %rbx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_147:                              #   Parent Loop BB1_139 Depth=1
                                        #     Parent Loop BB1_145 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rdi
	movss	496(%rsp,%rdi,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbp)
	movl	(%rbx), %edi
	addq	$4, %rbx
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB1_147
.LBB1_148:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_145 Depth=2
	addq	$4, %rbp
	addq	$8, %rcx
	addq	$8, %rdx
	testl	%eax, %eax
	jne	.LBB1_145
.LBB1_149:                              # %match_ribosum.exit.i
                                        #   in Loop: Header=BB1_139 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	movl	%eax, (%r10)
	movl	(%r12,%r15,4), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	cmpq	88(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB1_151
# BB#150:                               #   in Loop: Header=BB1_139 Depth=1
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
.LBB1_151:                              #   in Loop: Header=BB1_139 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	312(%rsp), %rbx         # 8-byte Reload
	jle	.LBB1_160
# BB#152:                               # %.lr.ph141.i
                                        #   in Loop: Header=BB1_139 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	cmpq	88(%rsp), %r15          # 8-byte Folded Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	232(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	jne	.LBB1_161
# BB#153:                               # %.lr.ph141.split.us.i.preheader
                                        #   in Loop: Header=BB1_139 Depth=1
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	jmp	.LBB1_155
	.p2align	4, 0x90
.LBB1_154:                              # %..lr.ph141.split.us_crit_edge.i
                                        #   in Loop: Header=BB1_155 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rbp,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13,%rbp,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	incq	%rbp
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
.LBB1_155:                              # %.lr.ph141.split.us.i
                                        #   Parent Loop BB1_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rbp,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	maxss	%xmm1, %xmm3
	addss	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	movaps	%xmm0, %xmm4
	cmpnless	%xmm2, %xmm4
	movaps	%xmm4, %xmm5
	andnps	%xmm2, %xmm5
	andps	%xmm0, %xmm4
	orps	%xmm5, %xmm4
	movaps	%xmm4, %xmm0
	cmovael	%ebp, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movss	4(%rdi,%rbp,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movq	(%rsp), %rsi            # 8-byte Reload
	movss	-4(%rsi,%r15,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	maxss	%xmm3, %xmm2
	addss	(%r12,%r15,4), %xmm1
	ucomiss	%xmm4, %xmm1
	jae	.LBB1_157
# BB#156:                               # %.lr.ph141.split.us._crit_edge.i
                                        #   in Loop: Header=BB1_155 Depth=2
	movl	4(%r8,%rbp,4), %esi
	jmp	.LBB1_158
	.p2align	4, 0x90
.LBB1_157:                              #   in Loop: Header=BB1_155 Depth=2
	movss	%xmm1, 4(%rdi,%rbp,4)
	movl	%r11d, 4(%r8,%rbp,4)
	movl	%r11d, %esi
.LBB1_158:                              #   in Loop: Header=BB1_155 Depth=2
	movq	%r10, %r9
	addss	4(%r9,%rbp,4), %xmm2
	movss	%xmm2, 4(%r9,%rbp,4)
	movss	%xmm2, 4(%rax,%rbp,4)
	movq	%rdi, %r8
	movl	4(%r8,%rbp,4), %edi
	movl	%edi, 4(%rcx,%rbp,4)
	movq	344(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbp,4)
	movq	336(%rsp), %rsi         # 8-byte Reload
	movl	%edx, 4(%rsi,%rbp,4)
	movl	4(%r9,%rbp,4), %esi
	movq	256(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbp,4)
	movl	4(%r8,%rbp,4), %esi
	movq	152(%rsp), %rdi         # 8-byte Reload
	movl	%esi, 4(%rdi,%rbp,4)
	movss	%xmm0, 4(%rbx,%rbp,4)
	cmpq	%rbp, %r14
	jne	.LBB1_154
# BB#159:                               #   in Loop: Header=BB1_139 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	jmp	.LBB1_166
	.p2align	4, 0x90
.LBB1_160:                              # %.._crit_edge142_crit_edge.i
                                        #   in Loop: Header=BB1_139 Depth=1
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rcx
	jmp	.LBB1_166
	.p2align	4, 0x90
.LBB1_161:                              # %.lr.ph141.split.i.preheader
                                        #   in Loop: Header=BB1_139 Depth=1
	xorl	%edx, %edx
	jmp	.LBB1_163
	.p2align	4, 0x90
.LBB1_162:                              # %..lr.ph141.split_crit_edge.i
                                        #   in Loop: Header=BB1_163 Depth=2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	cmpless	%xmm2, %xmm3
	andps	%xmm3, %xmm2
	andnps	%xmm0, %xmm3
	orps	%xmm2, %xmm3
	movq	72(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13,%rdx,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	incq	%rdx
	movaps	%xmm3, %xmm0
.LBB1_163:                              # %.lr.ph141.split.i
                                        #   Parent Loop BB1_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rdx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	maxss	%xmm1, %xmm4
	movq	32(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rdx,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rbp,%r15,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	maxss	%xmm4, %xmm3
	movss	(%r12,%r15,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm4
	ucomiss	%xmm5, %xmm4
	jb	.LBB1_165
# BB#164:                               #   in Loop: Header=BB1_163 Depth=2
	movss	%xmm4, 4(%rsi,%rdx,4)
	movl	%r11d, 4(%r8,%rdx,4)
.LBB1_165:                              #   in Loop: Header=BB1_163 Depth=2
	movq	%r10, %rbx
	addss	4(%rbx,%rdx,4), %xmm3
	movss	%xmm3, 4(%rbx,%rdx,4)
	movss	%xmm3, 4(%rax,%rdx,4)
	movl	4(%rsi,%rdx,4), %esi
	movl	%esi, 4(%rcx,%rdx,4)
	cmpq	%rdx, %r14
	jne	.LBB1_162
.LBB1_166:                              # %._crit_edge142.i
                                        #   in Loop: Header=BB1_139 Depth=1
	movq	184(%rsp), %rsi         # 8-byte Reload
	movl	(%r10,%rsi,4), %eax
	movq	296(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx,%r15,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	168(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	incq	%r15
	cmpq	160(%rsp), %r15         # 8-byte Folded Reload
	movq	%r10, %rax
	jne	.LBB1_139
.LBB1_167:                              # %._crit_edge148.i
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	144(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdx
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	80(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	248(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %r9
	pushq	$1
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	movq	248(%rsp), %r15         # 8-byte Reload
	pushq	%r15
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	match_ribosum
	addq	$16, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -16
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	176(%rsp), %rdx         # 8-byte Reload
	movl	64(%rsp), %ecx          # 4-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rbx, %r9
	movl	%ecx, %ebx
	pushq	$1
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	match_ribosum
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	cmpl	$2, 80(%rsp)            # 4-byte Folded Reload
	movq	152(%rsp), %r10         # 8-byte Reload
	movq	184(%rsp), %r14         # 8-byte Reload
	jl	.LBB1_181
# BB#168:                               # %.lr.ph129.i
	movslq	%ebx, %rax
	leaq	(%rbp,%rax,4), %rax
	movl	%ebx, %ecx
	cmpl	$7, %ebx
	jbe	.LBB1_175
# BB#169:                               # %min.iters.checked642
	movl	%ebx, %r9d
	andl	$7, %r9d
	movq	%rcx, %r8
	subq	%r9, %r8
	je	.LBB1_175
# BB#170:                               # %vector.memcheck663
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rsi
	leaq	4(%r12), %r11
	leaq	4(%r12,%rcx,4), %rbp
	cmpq	%rax, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %r11
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB1_233
# BB#171:                               # %vector.memcheck663
	andb	%sil, %bl
	andb	$1, %bl
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	jne	.LBB1_176
# BB#172:                               # %vector.body638.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r12), %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_173:                              # %vector.body638
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB1_173
# BB#174:                               # %middle.block639
	testl	%r9d, %r9d
	movq	%r8, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB1_176
	jmp	.LBB1_181
.LBB1_175:
	xorl	%edi, %edi
.LBB1_176:                              # %scalar.ph640.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %r8
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB1_178
# BB#177:                               # %scalar.ph640.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	addss	4(%r12,%rdi,4), %xmm0
	movq	16(%rsp), %rdx          # 8-byte Reload
	addss	(%rdx,%rdi,4), %xmm0
	movss	%xmm0, (%rdx,%rdi,4)
.LBB1_178:                              # %scalar.ph640.prol.loopexit
	cmpq	%rdi, %r8
	je	.LBB1_181
# BB#179:                               # %scalar.ph640.preheader.new
	subq	%rsi, %rcx
	leaq	8(%r12,%rsi,4), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_180:                              # %scalar.ph640
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB1_180
.LBB1_181:                              # %.preheader29.i
	movq	56(%rsp), %r11          # 8-byte Reload
	cmpl	$2, %r11d
	movq	312(%rsp), %r15         # 8-byte Reload
	jl	.LBB1_195
# BB#182:                               # %.lr.ph126.i
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB1_189
# BB#183:                               # %min.iters.checked681
	movl	%edx, %r9d
	andl	$7, %r9d
	movq	%rcx, %r8
	subq	%r9, %r8
	je	.LBB1_189
# BB#184:                               # %vector.memcheck703
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rsi
	leaq	4(%r13), %r12
	leaq	4(%r13,%rcx,4), %rbp
	cmpq	%rax, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %r12
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB1_234
# BB#185:                               # %vector.memcheck703
	andb	%sil, %bl
	andb	$1, %bl
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	jne	.LBB1_190
# BB#186:                               # %vector.body677.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r13), %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_187:                              # %vector.body677
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB1_187
# BB#188:                               # %middle.block678
	testl	%r9d, %r9d
	movq	%r8, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB1_190
	jmp	.LBB1_195
.LBB1_189:
	xorl	%edi, %edi
.LBB1_190:                              # %scalar.ph679.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %rdx
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB1_192
# BB#191:                               # %scalar.ph679.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	addss	4(%r13,%rdi,4), %xmm0
	movq	48(%rsp), %rbx          # 8-byte Reload
	addss	(%rbx,%rdi,4), %xmm0
	movss	%xmm0, (%rbx,%rdi,4)
	movl	64(%rsp), %ebx          # 4-byte Reload
.LBB1_192:                              # %scalar.ph679.prol.loopexit
	cmpq	%rdi, %rdx
	je	.LBB1_195
# BB#193:                               # %scalar.ph679.preheader.new
	subq	%rsi, %rcx
	leaq	8(%r13,%rsi,4), %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_194:                              # %scalar.ph679
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB1_194
.LBB1_195:                              # %.preheader28.i
	cmpl	$2, 80(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	jl	.LBB1_201
# BB#196:                               # %.lr.ph124.i
	movslq	%ebx, %rax
	movl	%ebx, %ecx
	testb	$1, %cl
	jne	.LBB1_198
# BB#197:
	xorl	%esi, %esi
	cmpl	$1, %ebx
	jne	.LBB1_199
	jmp	.LBB1_201
.LBB1_198:
	movss	(%rbp,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%r12), %xmm0
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %rdx
	addss	(%rdx,%r14,4), %xmm0
	movss	%xmm0, (%rdx,%r14,4)
	movl	$1, %esi
	cmpl	$1, %ebx
	je	.LBB1_201
.LBB1_199:                              # %.lr.ph124.i.new
	subq	%rsi, %rcx
	leaq	8(%r12,%rsi,4), %rdx
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB1_200:                              # =>This Inner Loop Header: Depth=1
	movss	(%rbp,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	movq	-8(%rsi), %rdi
	addss	(%rdi,%r14,4), %xmm0
	movss	%xmm0, (%rdi,%r14,4)
	movss	(%rbp,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movq	(%rsi), %rdi
	addss	(%rdi,%r14,4), %xmm0
	movss	%xmm0, (%rdi,%r14,4)
	addq	$8, %rdx
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB1_200
.LBB1_201:                              # %.preheader27.i
	cmpl	$2, %r11d
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	jl	.LBB1_242
# BB#202:                               # %.lr.ph122.i
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	movslq	%ebx, %rcx
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %r10
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB1_209
# BB#203:                               # %min.iters.checked721
	movl	%edx, %esi
	andl	$7, %esi
	movq	%rcx, %r8
	subq	%rsi, %r8
	je	.LBB1_209
# BB#204:                               # %vector.memcheck742
	leaq	(%r10,%rcx,4), %rdi
	leaq	4(%r13), %r9
	leaq	4(%r13,%rcx,4), %rbp
	cmpq	%rax, %r10
	sbbb	%bl, %bl
	cmpq	%rdi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %r10
	sbbb	%bl, %bl
	cmpq	%rdi, %r9
	sbbb	%dil, %dil
	xorl	%ebp, %ebp
	testb	$1, %dl
	jne	.LBB1_210
# BB#205:                               # %vector.memcheck742
	andb	%dil, %bl
	andb	$1, %bl
	jne	.LBB1_210
# BB#206:                               # %vector.body717.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r13), %rbp
	leaq	16(%r10), %rbx
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_207:                              # %vector.body717
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbx), %xmm3
	movups	(%rbx), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbx)
	movups	%xmm4, (%rbx)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB1_207
# BB#208:                               # %middle.block718
	testl	%esi, %esi
	movq	%r8, %rbp
	jne	.LBB1_210
	jmp	.LBB1_215
.LBB1_209:
	xorl	%ebp, %ebp
.LBB1_210:                              # %scalar.ph719.preheader
	movl	%ecx, %edi
	subl	%ebp, %edi
	leaq	-1(%rcx), %rsi
	testb	$1, %dil
	movq	%rbp, %rdi
	je	.LBB1_212
# BB#211:                               # %scalar.ph719.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rbp), %rdi
	addss	4(%r13,%rbp,4), %xmm0
	addss	(%r10,%rbp,4), %xmm0
	movss	%xmm0, (%r10,%rbp,4)
.LBB1_212:                              # %scalar.ph719.prol.loopexit
	cmpq	%rbp, %rsi
	je	.LBB1_215
# BB#213:                               # %scalar.ph719.preheader.new
	subq	%rdi, %rcx
	leaq	8(%r13,%rdi,4), %rsi
	leaq	4(%r10,%rdi,4), %rdx
	.p2align	4, 0x90
.LBB1_214:                              # %scalar.ph719
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rsi
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB1_214
.LBB1_215:                              # %.lr.ph120.i
	movq	%r11, %rcx
	shlq	$32, %rcx
	movabsq	$-8589934592, %rax      # imm = 0xFFFFFFFE00000000
	addq	%rcx, %rax
	sarq	$30, %rax
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	movq	%r14, %rdi
	notq	%rdi
	cmpq	$-3, %rdi
	movq	$-2, %rcx
	cmovgq	%rdi, %rcx
	leaq	2(%rcx,%r14), %r9
	cmpq	$3, %r9
	movq	%r14, %rsi
	movq	152(%rsp), %r10         # 8-byte Reload
	jbe	.LBB1_240
# BB#216:                               # %min.iters.checked760
	movq	%r9, %r8
	andq	$-4, %r8
	movq	%r14, %rsi
	je	.LBB1_240
# BB#217:                               # %vector.memcheck784
	cmpq	$-3, %rdi
	movq	$-2, %rdx
	cmovleq	%rdx, %rdi
	subq	%rdi, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rdx
	leaq	(%rcx,%r14,4), %rsi
	notq	%rdi
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdi
	leaq	4(%rcx,%r14,4), %rbp
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	cmpq	%rsi, %rdi
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rax, %rdx
	sbbb	%dl, %dl
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	testb	$1, %cl
	movq	%r14, %rsi
	jne	.LBB1_240
# BB#218:                               # %vector.memcheck784
	andb	%bl, %dl
	andb	$1, %dl
	movq	%r14, %rsi
	jne	.LBB1_240
# BB#219:                               # %vector.ph785
	movd	64(%rsp), %xmm1         # 4-byte Folded Reload
                                        # xmm1 = mem[0],zero,zero,zero
	leaq	-4(%r8), %rcx
	movq	%rcx, %rdi
	shrq	$2, %rdi
	btl	$2, %ecx
	jb	.LBB1_235
# BB#220:                               # %vector.body756.prol
	movq	48(%rsp), %rcx          # 8-byte Reload
	movups	-12(%rcx,%r14,4), %xmm0
	shufps	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movq	32(%rsp), %rcx          # 8-byte Reload
	movups	%xmm2, -16(%rcx,%r14,4)
	pshufd	$0, %xmm1, %xmm0        # xmm0 = xmm1[0,0,0,0]
	movq	96(%rsp), %rcx          # 8-byte Reload
	movdqu	%xmm0, -12(%rcx,%r14,4)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB1_236
	jmp	.LBB1_238
.LBB1_221:                              # %vector.body428.preheader
	leaq	16(%rbx), %rsi
	leaq	16(%r15), %rdi
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB1_222:                              # %vector.body428
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB1_222
# BB#223:                               # %middle.block429
	testl	%edx, %edx
	jne	.LBB1_35
	jmp	.LBB1_41
.LBB1_224:                              # %vector.body457.preheader
	leaq	16(%r8), %rsi
	leaq	16(%r14), %rdi
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB1_225:                              # %vector.body457
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB1_225
# BB#226:                               # %middle.block458
	testl	%edx, %edx
	jne	.LBB1_47
	jmp	.LBB1_53
.LBB1_227:                              # %vector.body564.preheader
	leaq	-8(%rdi), %rbx
	movl	%ebx, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB1_341
# BB#228:                               # %vector.body564.prol.preheader
	negq	%rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_229:                              # %vector.body564.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	4(%r14,%rdx,4), %xmm0
	movups	20(%r14,%rdx,4), %xmm1
	movups	%xmm0, 4(%r10,%rdx,4)
	movups	%xmm1, 20(%r10,%rdx,4)
	addq	$8, %rdx
	incq	%rax
	jne	.LBB1_229
	jmp	.LBB1_342
.LBB1_230:
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB1_74
.LBB1_231:
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB1_88
.LBB1_232:
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB1_132
.LBB1_233:
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB1_176
.LBB1_234:
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB1_190
.LBB1_235:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB1_238
.LBB1_236:                              # %vector.ph785.new
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-12(%rcx,%r14,4), %rbp
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	-16(%rcx,%r14,4), %rbx
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	-12(%rcx,%r14,4), %rdx
	.p2align	4, 0x90
.LBB1_237:                              # %vector.body756
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, (%rbx,%rsi,4)
	movdqu	%xmm1, (%rdx,%rsi,4)
	movups	-16(%rbp,%rsi,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, -16(%rbx,%rsi,4)
	movdqu	%xmm1, -16(%rdx,%rsi,4)
	addq	$-8, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB1_237
.LBB1_238:                              # %middle.block757
	cmpq	%r8, %r9
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	je	.LBB1_242
# BB#239:
	movq	%r14, %rsi
	subq	%r8, %rsi
.LBB1_240:                              # %scalar.ph758.preheader
	incq	%rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_241:                              # %scalar.ph758
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdi,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, -8(%rdx,%rsi,4)
	movl	%ebx, -4(%rcx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB1_241
.LBB1_242:                              # %.preheader25.i
	xorpd	%xmm0, %xmm0
	cmpl	$2, 80(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	jl	.LBB1_306
# BB#243:                               # %.lr.ph103.lr.ph.i
	leal	-2(%r11), %eax
	movl	%eax, 356(%rsp)         # 4-byte Spill
	cltq
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movslq	%r11d, %rbp
	leaq	-8(%rdx,%rbp,4), %r14
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,4), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movslq	200(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movl	%esi, %ecx
	leal	2(%rsi), %esi
	leaq	-3(%rcx), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	andl	$3, %esi
	movq	184(%rsp), %rdx         # 8-byte Reload
	leaq	1(%rdx), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	(%r10,%rdx,4), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	-1(%rdx), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	-4(%r8,%rdx,4), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	%rsi, 440(%rsp)         # 8-byte Spill
	negq	%rsi
	movq	%rsi, 416(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movss	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	leaq	-4(%r13,%rdx,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 464(%rsp)         # 8-byte Spill
	leaq	-2(%rbp), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	%rcx, %rbp
	leaq	(,%rdx,4), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	-4(%r15,%rdx,4), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movq	264(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rdx,4), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rdx,4), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	%ebx, %eax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
.LBB1_244:                              # %.lr.ph103.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_245 Depth 2
                                        #       Child Loop BB1_246 Depth 3
                                        #         Child Loop BB1_248 Depth 4
                                        #       Child Loop BB1_251 Depth 3
                                        #         Child Loop BB1_253 Depth 4
                                        #       Child Loop BB1_259 Depth 3
                                        #       Child Loop BB1_267 Depth 3
                                        #       Child Loop BB1_283 Depth 3
                                        #       Child Loop BB1_286 Depth 3
                                        #       Child Loop BB1_289 Depth 3
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movslq	%ebx, %rcx
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB1_245:                              #   Parent Loop BB1_244 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_246 Depth 3
                                        #         Child Loop BB1_248 Depth 4
                                        #       Child Loop BB1_251 Depth 3
                                        #         Child Loop BB1_253 Depth 4
                                        #       Child Loop BB1_259 Depth 3
                                        #       Child Loop BB1_267 Depth 3
                                        #       Child Loop BB1_283 Depth 3
                                        #       Child Loop BB1_286 Depth 3
                                        #       Child Loop BB1_289 Depth 3
	movl	%edx, 140(%rsp)         # 4-byte Spill
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rdx
	movl	%ebx, %r15d
	movaps	%xmm1, %xmm2
	movq	%rcx, %rbx
	leaq	-1(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movslq	%r15d, %r12
	movl	(%r9,%r12,4), %eax
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%eax, (%rdx,%rcx,4)
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	-8(%rax,%rbx,8), %rax
	addq	$4, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_246:                              #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_248 Depth 4
	movl	$0, 496(%rsp,%rdx,4)
	xorpd	%xmm3, %xmm3
	movq	$-5328, %rsi            # imm = 0xEB30
	movq	%rax, %rcx
	jmp	.LBB1_248
	.p2align	4, 0x90
.LBB1_247:                              #   in Loop: Header=BB1_248 Depth=4
	xorps	%xmm3, %xmm3
	cvtsi2ssl	ribosumdis+5476(%rsi,%rdx,4), %xmm3
	mulss	(%rcx), %xmm3
	addss	%xmm3, %xmm1
	addq	$296, %rsi              # imm = 0x128
	addq	$8, %rcx
	movaps	%xmm1, %xmm3
.LBB1_248:                              #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        #       Parent Loop BB1_246 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorps	%xmm1, %xmm1
	cvtsi2ssl	ribosumdis+5328(%rsi,%rdx,4), %xmm1
	mulss	-4(%rcx), %xmm1
	addss	%xmm3, %xmm1
	testq	%rsi, %rsi
	jne	.LBB1_247
# BB#249:                               #   in Loop: Header=BB1_246 Depth=3
	movss	%xmm1, 496(%rsp,%rdx,4)
	incq	%rdx
	cmpq	$37, %rdx
	jne	.LBB1_246
# BB#250:                               # %.preheader.i4.i
                                        #   in Loop: Header=BB1_245 Depth=2
	movq	%rbx, 320(%rsp)         # 8-byte Spill
	testl	%r11d, %r11d
	movl	%r11d, %eax
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rsi         # 8-byte Reload
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	je	.LBB1_255
	.p2align	4, 0x90
.LBB1_251:                              # %.lr.ph85.i15.i
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_253 Depth 4
	decl	%eax
	movl	$0, (%rdi)
	movq	(%rdx), %rbp
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	js	.LBB1_254
# BB#252:                               # %.lr.ph.i16.i
                                        #   in Loop: Header=BB1_251 Depth=3
	movq	(%rsi), %rbx
	addq	$4, %rbp
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_253:                              #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        #       Parent Loop BB1_251 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%ecx, %rcx
	movss	496(%rsp,%rcx,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm3
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rdi)
	movl	(%rbp), %ecx
	addq	$4, %rbp
	addq	$4, %rbx
	testl	%ecx, %ecx
	jns	.LBB1_253
.LBB1_254:                              # %._crit_edge.i19.i
                                        #   in Loop: Header=BB1_251 Depth=3
	addq	$4, %rdi
	addq	$8, %rdx
	addq	$8, %rsi
	testl	%eax, %eax
	jne	.LBB1_251
.LBB1_255:                              # %match_ribosum.exit20.i
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpl	$1, %r11d
	movq	320(%rsp), %rbx         # 8-byte Reload
	movl	-4(%r9,%rbx,4), %eax
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rdx,%rcx,4)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	464(%rsp), %rcx         # 8-byte Reload
	movss	-4(%rax,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	%rcx, %rax
	jle	.LBB1_265
# BB#256:                               # %.lr.ph75.i
                                        #   in Loop: Header=BB1_245 Depth=2
	leaq	(%rdx,%rax,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	456(%rsp), %rdx         # 8-byte Reload
	movss	(%rcx,%rdx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rdi
	cmpl	104(%rsp), %r15d        # 4-byte Folded Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rcx,%rbx,8), %rcx
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	-8(%rdx,%rbx,8), %rdx
	je	.LBB1_266
# BB#257:                               # %.lr.ph75.i
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpl	44(%rsp), %esi          # 4-byte Folded Reload
	je	.LBB1_266
# BB#258:                               # %.lr.ph75.i.split.preheader
                                        #   in Loop: Header=BB1_245 Depth=2
	movq	424(%rsp), %rsi         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rbp
	leaq	(%rdx,%rsi,4), %rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	360(%rsp), %rsi         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_259:                              # %.lr.ph75.i.split
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	160(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm5
	maxss	%xmm1, %xmm5
	movq	64(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx,%rbx,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm6
	movaps	%xmm3, %xmm4
	cmpless	%xmm6, %xmm4
	andps	%xmm4, %xmm6
	andnps	%xmm3, %xmm4
	orps	%xmm6, %xmm4
	movss	(%r14,%rbx,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movq	8(%rsp), %rcx           # 8-byte Reload
	movss	(%rcx,%r12,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm3
	addss	(%rdi), %xmm1
	ucomiss	%xmm6, %xmm1
	jb	.LBB1_261
# BB#260:                               #   in Loop: Header=BB1_259 Depth=3
	movss	%xmm1, (%r14,%rbx,4)
	movq	328(%rsp), %rcx         # 8-byte Reload
	movl	%r15d, -8(%rcx,%rbx,4)
.LBB1_261:                              #   in Loop: Header=BB1_259 Depth=3
	maxss	%xmm5, %xmm3
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	104(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB1_263
# BB#262:                               #   in Loop: Header=BB1_259 Depth=3
	movq	376(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rcx,%rbx,4)
	movss	(%r14,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	384(%rsp), %rcx         # 8-byte Reload
	addss	(%rcx,%rbx,4), %xmm1
	movss	%xmm1, (%rcx,%rbx,4)
.LBB1_263:                              #   in Loop: Header=BB1_259 Depth=3
	movss	(%rbp,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rbp,%rbx,4)
	movss	-8(%rax,%rbx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	(%rdx,%rbx,4), %xmm1
	movss	%xmm1, (%rdx,%rbx,4)
	addss	-8(%rax,%rbx,4), %xmm3
	movss	%xmm3, -8(%rax,%rbx,4)
	movss	(%rsi,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx,%rbx), %rcx
	decq	%rbx
	cmpq	$1, %rcx
	movaps	%xmm4, %xmm3
	jg	.LBB1_259
# BB#264:                               #   in Loop: Header=BB1_245 Depth=2
	movl	$-1, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebx
	jmp	.LBB1_277
	.p2align	4, 0x90
.LBB1_265:                              # %match_ribosum.exit20.._crit_edge76_crit_edge.i
                                        #   in Loop: Header=BB1_245 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rdi
	movl	%ecx, %ebx
	movl	356(%rsp), %eax         # 4-byte Reload
	jmp	.LBB1_277
	.p2align	4, 0x90
.LBB1_266:                              # %.lr.ph75.i.split.us.preheader
                                        #   in Loop: Header=BB1_245 Depth=2
	movq	408(%rsp), %rsi         # 8-byte Reload
	leaq	-4(%rdx,%rsi), %rbp
	leaq	-4(%rcx,%rsi), %r11
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	360(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,4), %r10
	xorl	%r8d, %r8d
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r9d
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_267:                              # %.lr.ph75.i.split.us
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm3, %xmm6
	movq	160(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm4
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8), %rsi
	ucomiss	%xmm1, %xmm4
	maxss	%xmm1, %xmm4
	movl	%esi, %edx
	cmoval	%r9d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm7     # xmm7 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm7
	ucomiss	%xmm6, %xmm7
	movaps	%xmm6, %xmm5
	cmpnless	%xmm7, %xmm5
	movaps	%xmm5, %xmm3
	andnps	%xmm7, %xmm3
	andps	%xmm6, %xmm5
	cmovael	%esi, %r9d
	movss	(%r14,%r8,4), %xmm6     # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx,%r12,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm7
	ucomiss	%xmm4, %xmm7
	movl	%r15d, %ecx
	jbe	.LBB1_269
# BB#268:                               #   in Loop: Header=BB1_267 Depth=3
	movq	328(%rsp), %rcx         # 8-byte Reload
	movl	-8(%rcx,%r8,4), %ecx
	movaps	%xmm7, %xmm4
	movl	%esi, %edx
.LBB1_269:                              #   in Loop: Header=BB1_267 Depth=3
	orps	%xmm5, %xmm3
	addss	(%rdi), %xmm1
	ucomiss	%xmm6, %xmm1
	jb	.LBB1_271
# BB#270:                               #   in Loop: Header=BB1_267 Depth=3
	movss	%xmm1, (%r14,%r8,4)
	movq	328(%rsp), %rsi         # 8-byte Reload
	movl	%r15d, -8(%rsi,%r8,4)
.LBB1_271:                              #   in Loop: Header=BB1_267 Depth=3
	movq	480(%rsp), %rsi         # 8-byte Reload
	movl	%ecx, (%rsi,%r8,4)
	movq	488(%rsp), %rcx         # 8-byte Reload
	movl	%edx, (%rcx,%r8,4)
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	104(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB1_273
# BB#272:                               #   in Loop: Header=BB1_267 Depth=3
	movq	376(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, (%rcx,%r8,4)
	movss	(%r14,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movq	384(%rsp), %rcx         # 8-byte Reload
	addss	(%rcx,%r8,4), %xmm1
	movss	%xmm1, (%rcx,%r8,4)
.LBB1_273:                              #   in Loop: Header=BB1_267 Depth=3
	cmpl	104(%rsp), %r15d        # 4-byte Folded Reload
	jne	.LBB1_275
# BB#274:                               #   in Loop: Header=BB1_267 Depth=3
	movq	472(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rcx,%r8,4)
.LBB1_275:                              #   in Loop: Header=BB1_267 Depth=3
	movss	(%r11,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, (%r11,%r8,4)
	movss	-8(%rax,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r8,4), %xmm1
	movss	%xmm1, (%rbp,%r8,4)
	addss	-8(%rax,%r8,4), %xmm4
	movss	%xmm4, -8(%rax,%r8,4)
	movss	(%r10,%r8,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rcx,%r8), %rcx
	decq	%r8
	cmpq	$1, %rcx
	jg	.LBB1_267
# BB#276:                               #   in Loop: Header=BB1_245 Depth=2
	movl	$-1, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebx
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movq	152(%rsp), %r10         # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
.LBB1_277:                              # %._crit_edge76.i
                                        #   in Loop: Header=BB1_245 Depth=2
	movl	140(%rsp), %edx         # 4-byte Reload
	addss	(%rdi), %xmm1
	ucomiss	%xmm2, %xmm1
	maxss	%xmm2, %xmm1
	movq	304(%rsp), %rcx         # 8-byte Reload
	cmoval	%r15d, %ecx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	cmpl	104(%rsp), %ebx         # 4-byte Folded Reload
	movq	312(%rsp), %r15         # 8-byte Reload
	movq	200(%rsp), %rbp         # 8-byte Reload
	jne	.LBB1_279
# BB#278:                               #   in Loop: Header=BB1_245 Depth=2
	cltq
	movss	4(%r10,%rax,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movss	%xmm2, 4(%r10,%rax,4)
.LBB1_279:                              #   in Loop: Header=BB1_245 Depth=2
	cmpl	136(%rsp), %ebx         # 4-byte Folded Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_298
# BB#280:                               #   in Loop: Header=BB1_245 Depth=2
	movss	4(%r8), %xmm2           # xmm2 = mem[0],zero,zero,zero
	xorl	%r13d, %r13d
	cmpl	$3, %esi
	jl	.LBB1_287
# BB#281:                               # %.lr.ph82.i.preheader
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpq	$0, 440(%rsp)           # 8-byte Folded Reload
	movq	416(%rsp), %rdx         # 8-byte Reload
	je	.LBB1_284
# BB#282:                               # %.lr.ph82.i.prol.preheader
                                        #   in Loop: Header=BB1_245 Depth=2
	xorl	%r13d, %r13d
	movl	$2, %eax
	movaps	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB1_283:                              # %.lr.ph82.i.prol
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmoval	%eax, %r13d
	leaq	1(%rdx,%rax), %rcx
	incq	%rax
	cmpq	$2, %rcx
	movaps	%xmm2, %xmm0
	jne	.LBB1_283
	jmp	.LBB1_285
.LBB1_284:                              #   in Loop: Header=BB1_245 Depth=2
	xorl	%r13d, %r13d
	movl	$2, %eax
.LBB1_285:                              # %.lr.ph82.i.prol.loopexit
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpq	$3, 432(%rsp)           # 8-byte Folded Reload
	jb	.LBB1_287
	.p2align	4, 0x90
.LBB1_286:                              # %.lr.ph82.i
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r8,%rax,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%eax, %r13d
	movss	4(%r8,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	leal	1(%rax), %ecx
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmovbel	%r13d, %ecx
	movss	8(%r8,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leal	2(%rax), %edx
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmovbel	%ecx, %edx
	movss	12(%r8,%rax,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	leal	3(%rax), %r13d
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmovbel	%edx, %r13d
	addq	$4, %rax
	cmpq	%rbp, %rax
	jne	.LBB1_286
.LBB1_287:                              # %.preheader24.i
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpl	$-1, %esi
	jl	.LBB1_290
# BB#288:                               # %.lr.ph88.i.preheader
                                        #   in Loop: Header=BB1_245 Depth=2
	xorl	%eax, %eax
	movq	448(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_289:                              # %.lr.ph88.i
                                        #   Parent Loop BB1_244 Depth=1
                                        #     Parent Loop BB1_245 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r10,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%eax, %r13d
	incq	%rax
	cmpq	%rcx, %rax
	movaps	%xmm0, %xmm2
	jl	.LBB1_289
	jmp	.LBB1_291
	.p2align	4, 0x90
.LBB1_290:                              #   in Loop: Header=BB1_245 Depth=2
	movaps	%xmm2, %xmm0
.LBB1_291:                              # %._crit_edge89.i
                                        #   in Loop: Header=BB1_245 Depth=2
	movslq	%r13d, %rax
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	leal	-1(%rax), %edx
	testl	%eax, %eax
	jle	.LBB1_294
# BB#292:                               #   in Loop: Header=BB1_245 Depth=2
	movslq	%edx, %rcx
	movss	(%r15,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movl	%edx, %ecx
	jbe	.LBB1_295
# BB#293:                               #   in Loop: Header=BB1_245 Depth=2
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movaps	%xmm3, %xmm2
	jmp	.LBB1_295
.LBB1_294:                              #   in Loop: Header=BB1_245 Depth=2
	movl	%edx, %ecx
.LBB1_295:                              #   in Loop: Header=BB1_245 Depth=2
	movss	(%r10,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	jbe	.LBB1_297
# BB#296:                               #   in Loop: Header=BB1_245 Depth=2
	movq	344(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	je	.LBB1_299
	jmp	.LBB1_301
	.p2align	4, 0x90
.LBB1_297:                              #   in Loop: Header=BB1_245 Depth=2
	movl	136(%rsp), %eax         # 4-byte Reload
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
.LBB1_298:                              #   in Loop: Header=BB1_245 Depth=2
	cmpl	44(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB1_301
.LBB1_299:                              #   in Loop: Header=BB1_245 Depth=2
	testl	%r13d, %r13d
	je	.LBB1_303
# BB#300:                               #   in Loop: Header=BB1_245 Depth=2
	cmpl	%r11d, %r13d
	movl	136(%rsp), %eax         # 4-byte Reload
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	movl	%r11d, %r13d
	jge	.LBB1_302
	jmp	.LBB1_304
	.p2align	4, 0x90
.LBB1_301:                              #   in Loop: Header=BB1_245 Depth=2
	movl	%edx, %eax
.LBB1_302:                              # %.backedge.i
                                        #   in Loop: Header=BB1_245 Depth=2
	cmpq	$1, 320(%rsp)           # 8-byte Folded Reload
	movl	%eax, %edx
	movq	%rdi, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	jg	.LBB1_245
	jmp	.LBB1_306
.LBB1_303:                              #   in Loop: Header=BB1_244 Depth=1
	movq	304(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %ecx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movl	$1, %r13d
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	$1, 320(%rsp)           # 4-byte Folded Reload
	jg	.LBB1_244
	jmp	.LBB1_306
.LBB1_304:                              #   in Loop: Header=BB1_244 Depth=1
	movslq	%edx, %rax
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	264(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %r13d
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	cmpl	$1, 320(%rsp)           # 4-byte Folded Reload
	jg	.LBB1_244
.LBB1_306:                              # %.preheader23.i
	movq	80(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	112(%rsp), %r12         # 8-byte Reload
	jle	.LBB1_324
# BB#307:                               # %.preheader22.preheader.i
	movq	776(%rsp), %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	movl	%ecx, %r14d
	leaq	-1(%rbp), %r11
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rbp, %r10
	subq	%r8, %r10
	movapd	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_308:                              # %.preheader22.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_316 Depth 2
                                        #     Child Loop BB1_322 Depth 2
	cmpq	$4, %rbp
	movq	(%r12,%rbx,8), %rdx
	movq	(%r9,%rbx,8), %rsi
	jae	.LBB1_310
# BB#309:                               #   in Loop: Header=BB1_308 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB1_318
	.p2align	4, 0x90
.LBB1_310:                              # %min.iters.checked804
                                        #   in Loop: Header=BB1_308 Depth=1
	testq	%r10, %r10
	je	.LBB1_314
# BB#311:                               # %vector.memcheck817
                                        #   in Loop: Header=BB1_308 Depth=1
	leaq	(%rdx,%rbp,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB1_315
# BB#312:                               # %vector.memcheck817
                                        #   in Loop: Header=BB1_308 Depth=1
	leaq	(%rsi,%rbp,4), %rax
	cmpq	%rax, %rdx
	jae	.LBB1_315
.LBB1_314:                              #   in Loop: Header=BB1_308 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB1_318
.LBB1_315:                              # %vector.ph818
                                        #   in Loop: Header=BB1_308 Depth=1
	movq	%r10, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB1_316:                              # %vector.body800
                                        #   Parent Loop BB1_308 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi), %xmm2
	divps	%xmm1, %xmm2
	movups	%xmm2, (%rax)
	addq	$16, %rdi
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB1_316
# BB#317:                               # %middle.block801
                                        #   in Loop: Header=BB1_308 Depth=1
	testq	%r8, %r8
	movq	%r10, %rcx
	je	.LBB1_323
	.p2align	4, 0x90
.LBB1_318:                              # %scalar.ph802.preheader
                                        #   in Loop: Header=BB1_308 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%ecx, %eax
	testb	$1, %al
	movq	%rcx, %rdi
	je	.LBB1_320
# BB#319:                               # %scalar.ph802.prol
                                        #   in Loop: Header=BB1_308 Depth=1
	movss	(%rdx,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, (%rsi,%rcx,4)
	leaq	1(%rcx), %rdi
.LBB1_320:                              # %scalar.ph802.prol.loopexit
                                        #   in Loop: Header=BB1_308 Depth=1
	cmpq	%rcx, %r11
	je	.LBB1_323
# BB#321:                               # %scalar.ph802.preheader.new
                                        #   in Loop: Header=BB1_308 Depth=1
	movq	%rbp, %rcx
	subq	%rdi, %rcx
	leaq	4(%rsi,%rdi,4), %rax
	leaq	4(%rdx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_322:                              # %scalar.ph802
                                        #   Parent Loop BB1_308 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-4(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, -4(%rax)
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, (%rax)
	addq	$8, %rax
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB1_322
.LBB1_323:                              # %._crit_edge64.i
                                        #   in Loop: Header=BB1_308 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB1_308
.LBB1_324:                              # %._crit_edge66.i
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	296(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	256(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	%r15, %rdi
	callq	FreeFloatVec
	movq	336(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	344(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	264(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	400(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	392(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	FreeIntVec
	movq	248(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	240(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movq	%r12, %rdi
	callq	FreeFloatMtx
	movq	232(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	208(%rsp), %rdx         # 8-byte Reload
	movq	224(%rsp), %r12         # 8-byte Reload
.LBB1_325:                              # %MSalign2m2m_rec.exit
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdx, %rbx
	callq	FreeFloatVec
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	FreeFloatVec
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	368(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	288(%rsp), %rdi         # 8-byte Reload
	callq	FreeCharMtx
	movq	%rbx, %rdi
	callq	FreeCharMtx
	movq	(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	movl	720(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB1_332
# BB#326:                               # %.lr.ph247
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movslq	%eax, %r12
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%r12, %rax
	jne	.LBB1_350
# BB#327:                               # %.lr.ph389.preheader
	movl	720(%rsp), %eax
	movslq	%eax, %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_328:                              # %.lr.ph389
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rbp
	jge	.LBB1_332
# BB#329:                               # %._crit_edge310
                                        #   in Loop: Header=BB1_328 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	callq	strlen
	incq	%rbp
	cmpq	%r12, %rax
	je	.LBB1_328
# BB#330:                               # %._crit_edge390
	decl	%ebp
.LBB1_331:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	720(%rsp), %ecx
	jmp	.LBB1_339
.LBB1_332:                              # %.preheader
	movl	728(%rsp), %eax
	testl	%eax, %eax
	movq	224(%rsp), %r12         # 8-byte Reload
	jle	.LBB1_340
# BB#333:                               # %.lr.ph
	movslq	%r14d, %r14
	movq	%r15, %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB1_351
# BB#334:                               # %.lr.ph386.preheader
	movl	728(%rsp), %eax
	movslq	%eax, %rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_335:                              # %.lr.ph386
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB1_340
# BB#336:                               # %._crit_edge313
                                        #   in Loop: Header=BB1_335 Depth=1
	movq	(%r12,%rbx,8), %rdi
	callq	strlen
	incq	%rbx
	cmpq	%r14, %rax
	je	.LBB1_335
# BB#337:                               # %._crit_edge387
	decl	%ebx
.LBB1_338:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	728(%rsp), %ecx
.LBB1_339:
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$42, %esi
	jmp	.LBB1_349
.LBB1_340:                              # %._crit_edge
	xorps	%xmm0, %xmm0
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_341:
	xorl	%edx, %edx
.LBB1_342:                              # %vector.body564.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB1_344
	.p2align	4, 0x90
.LBB1_343:                              # %vector.body564
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rdx,4), %rax
	orq	$4, %rax
	movups	(%r14,%rax), %xmm0
	movups	16(%r14,%rax), %xmm1
	movups	%xmm0, (%r10,%rax)
	movups	%xmm1, 16(%r10,%rax)
	leaq	8(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	leaq	16(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	leaq	24(%rdx), %rax
	orq	$1, %rax
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	%xmm0, (%r10,%rax,4)
	movups	%xmm1, 16(%r10,%rax,4)
	addq	$32, %rdx
	cmpq	%rdi, %rdx
	jne	.LBB1_343
.LBB1_344:                              # %middle.block565
	cmpq	%rdi, %r8
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB1_124
# BB#345:
	orq	$1, %rdi
	jmp	.LBB1_118
.LBB1_346:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	720(%rsp), %ecx
	jmp	.LBB1_348
.LBB1_347:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	728(%rsp), %ecx
.LBB1_348:
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$27, %esi
.LBB1_349:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB1_350:
	xorl	%ebp, %ebp
	jmp	.LBB1_331
.LBB1_351:
	xorl	%ebx, %ebx
	jmp	.LBB1_338
.Lfunc_end1:
	.size	Lalign2m2m_hmout, .Lfunc_end1-Lalign2m2m_hmout
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	56(%rsp), %r11
	cmpl	$0, 64(%rsp)
	je	.LBB2_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB2_10
# BB#2:                                 # %.preheader79.preheader
	movl	%r8d, %r10d
	xorl	%r15d, %r15d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader79
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	(%rdx,%r15,8), %r14
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB2_5
	jnp	.LBB2_6
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=2
	movq	(%r9,%r15,8), %r12
	movslq	%r13d, %r13
	movss	%xmm1, (%r12,%r13,4)
	movq	(%r11,%r15,8), %rbx
	movl	%eax, (%rbx,%r13,4)
	incl	%r13d
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=2
	movss	4(%r14,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB2_7
	jnp	.LBB2_8
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=2
	movq	(%r9,%r15,8), %rbx
	movslq	%r13d, %r13
	movss	%xmm1, (%rbx,%r13,4)
	movq	(%r11,%r15,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r13,4)
	incl	%r13d
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB2_4
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	(%r11,%r15,8), %rax
	movslq	%r13d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB2_3
.LBB2_10:                               # %.preheader78
	movl	RNAthr(%rip), %eax
	movslq	%ecx, %rcx
	movq	(%rsi,%rcx,8), %r10
	addq	$4, %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movl	$0, -104(%rsp,%rdx,4)
	xorps	%xmm0, %xmm0
	movq	$-2704, %rsi            # imm = 0xF570
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	n_dis+2704(%rsi,%rdx,4), %ebx
	subl	%eax, %ebx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebx, %xmm1
	mulss	-4(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movl	n_dis+2808(%rsi,%rdx,4), %ebx
	subl	%eax, %ebx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebx, %xmm0
	mulss	(%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	$8, %rcx
	addq	$208, %rsi
	jne	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_11 Depth=1
	movss	%xmm0, -104(%rsp,%rdx,4)
	incq	%rdx
	cmpq	$26, %rdx
	jne	.LBB2_11
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_18:                               # %._crit_edge
                                        #   in Loop: Header=BB2_14 Depth=1
	addq	$4, %rdi
	addq	$8, %r11
	addq	$8, %r9
.LBB2_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB2_19
# BB#15:                                # %.lr.ph85
                                        #   in Loop: Header=BB2_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB2_18
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rcx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB2_17
	jmp	.LBB2_18
.LBB2_19:                               # %._crit_edge86
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	match_calc, .Lfunc_end2-match_calc
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_ribosum,@function
match_ribosum:                          # @match_ribosum
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 80
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	80(%rsp), %r11
	cmpl	$0, 88(%rsp)
	je	.LBB3_8
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB3_8
# BB#2:                                 # %.preheader79.preheader
	movl	%r8d, %r10d
	xorl	%r15d, %r15d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader79
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	(%rdx,%r15,8), %r14
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_21:                               #   in Loop: Header=BB3_4 Depth=2
	addq	$2, %r13
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r14,%r13,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_5
	jnp	.LBB3_6
.LBB3_5:                                #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r15,8), %r12
	movslq	%ebx, %rbx
	movss	%xmm1, (%r12,%rbx,4)
	movq	(%r11,%r15,8), %rax
	movl	%r13d, (%rax,%rbx,4)
	incl	%ebx
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	cmpq	$36, %r13
	je	.LBB3_7
# BB#19:                                #   in Loop: Header=BB3_4 Depth=2
	movss	4(%r14,%r13,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_20
	jnp	.LBB3_21
.LBB3_20:                               #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r15,8), %rax
	movslq	%ebx, %rbx
	movss	%xmm1, (%rax,%rbx,4)
	movq	(%r11,%r15,8), %rax
	leal	1(%r13), %ebp
	movl	%ebp, (%rax,%rbx,4)
	incl	%ebx
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_3 Depth=1
	movq	(%r11,%r15,8), %rax
	movslq	%ebx, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB3_3
.LBB3_8:                                # %.preheader78
	movslq	%ecx, %rax
	movq	(%rsi,%rax,8), %rax
	addq	$4, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_10 Depth 2
	movl	$0, -128(%rsp,%rcx,4)
	xorps	%xmm1, %xmm1
	movq	$-5328, %rdx            # imm = 0xEB30
	movq	%rax, %rsi
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_18:                               #   in Loop: Header=BB3_10 Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	ribosumdis+5476(%rdx,%rcx,4), %xmm1
	mulss	(%rsi), %xmm1
	addss	%xmm1, %xmm0
	addq	$296, %rdx              # imm = 0x128
	addq	$8, %rsi
	movaps	%xmm0, %xmm1
.LBB3_10:                               #   Parent Loop BB3_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	ribosumdis+5328(%rdx,%rcx,4), %xmm0
	mulss	-4(%rsi), %xmm0
	addss	%xmm1, %xmm0
	testq	%rdx, %rdx
	jne	.LBB3_18
# BB#11:                                #   in Loop: Header=BB3_9 Depth=1
	movss	%xmm0, -128(%rsp,%rcx,4)
	incq	%rcx
	cmpq	$37, %rcx
	jne	.LBB3_9
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_16:                               # %._crit_edge
                                        #   in Loop: Header=BB3_12 Depth=1
	addq	$4, %rdi
	addq	$8, %r11
	addq	$8, %r9
.LBB3_12:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
	testl	%r8d, %r8d
	je	.LBB3_17
# BB#13:                                # %.lr.ph85
                                        #   in Loop: Header=BB3_12 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB3_16
# BB#14:                                # %.lr.ph
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-128(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rcx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB3_15
	jmp	.LBB3_16
.LBB3_17:                               # %._crit_edge86
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	match_ribosum, .Lfunc_end3-match_ribosum
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"i = %d / %d\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"bug! hairetsu ga kowareta!\n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"j = %d / %d\n"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"hairetsu ga kowareta (end of MSalignmm) !\n"
	.size	.L.str.3, 43

	.type	reccycle,@object        # @reccycle
	.local	reccycle
	.comm	reccycle,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
