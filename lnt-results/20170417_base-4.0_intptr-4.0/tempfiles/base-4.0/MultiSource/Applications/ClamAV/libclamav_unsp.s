	.text
	.file	"libclamav_unsp.bc"
	.globl	unspack
	.p2align	4, 0x90
	.type	unspack,@function
unspack:                                # @unspack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r10d
	movq	%rdi, %rbp
	movb	(%rbp), %bl
	movl	$1, %edi
	cmpb	$-32, %bl
	ja	.LBB0_15
# BB#1:
	xorl	%r14d, %r14d
	cmpb	$45, %bl
	jb	.LBB0_3
# BB#2:                                 # %.loopexit70.loopexit
	movzbl	%bl, %eax
	imull	$109, %eax, %eax
	shrl	$8, %eax
	movl	%ebx, %ecx
	subb	%al, %cl
	shrb	%cl
	addb	%al, %cl
	shrb	$5, %cl
	movzbl	%cl, %r11d
	movb	$-45, %cl
	movl	%r11d, %eax
	mulb	%cl
	addb	%al, %bl
	cmpb	$9, %bl
	jae	.LBB0_4
	jmp	.LBB0_5
.LBB0_3:
	xorl	%r11d, %r11d
	cmpb	$9, %bl
	jb	.LBB0_5
.LBB0_4:                                # %.loopexit.loopexit
	movzbl	%bl, %eax
	imull	$57, %eax, %r14d
	andl	$15872, %r14d           # imm = 0x3E00
	shrl	$9, %r14d
	movb	$-9, %cl
	movl	%r14d, %eax
	mulb	%cl
	addb	%al, %bl
.LBB0_5:                                # %.loopexit
	movzbl	%bl, %r12d
	leal	(%r12,%r14), %ecx
	movl	$1536, %r13d            # imm = 0x600
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	addl	$3692, %r13d            # imm = 0xE6C
	movq	32(%rdx), %rax
	testq	%rax, %rax
	je	.LBB0_8
# BB#6:
	movq	24(%rax), %rax
	movl	%r13d, %ebx
	testq	%rax, %rax
	je	.LBB0_9
# BB#7:
	cmpq	%rax, %rbx
	ja	.LBB0_15
	jmp	.LBB0_9
.LBB0_8:                                # %.loopexit._crit_edge
	movl	%r13d, %ebx
.LBB0_9:
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movl	%r10d, 8(%rsp)          # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_12
# BB#10:
	movl	5(%rbp), %eax
	cmpl	$13, %eax
	ja	.LBB0_13
# BB#11:
	movq	%r15, %rdi
	callq	free
.LBB0_12:
	movl	$1, %edi
	jmp	.LBB0_15
.LBB0_13:
	movl	9(%rbp), %ebx
	addq	$13, %rbp
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movl	%r13d, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	movq	%rbx, %r14
	movl	28(%rsp), %r8d          # 4-byte Reload
	movq	%rbp, %r9
	pushq	%r14
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rbp          # 8-byte Reload
	pushq	%rbp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	very_real_unpack
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	free
	testl	%ebx, %ebx
	movl	$1, %edi
	jne	.LBB0_15
# BB#14:
	movl	128(%rsp), %eax
	movl	$0, 40(%rsp)
	movl	%r14d, 44(%rsp)
	movl	%r14d, 36(%rsp)
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 32(%rsp)
	leaq	32(%rsp), %rsi
	movl	$1, %edx
	movl	$0, %r9d
	movq	%rbp, %rdi
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	16(%rsp), %r8d          # 4-byte Reload
	pushq	%rax
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
	xorl	%edi, %edi
	testl	%eax, %eax
	sete	%dil
.LBB0_15:
	movl	%edi, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	unspack, .Lfunc_end0-unspack
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.text
	.globl	very_real_unpack
	.p2align	4, 0x90
	.type	very_real_unpack,@function
very_real_unpack:                       # @very_real_unpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 224
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%ecx, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r15
	leal	(%rax,%rdx), %ecx
	movl	$768, %edi              # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	leal	1846(%rdi), %r12d
	movl	$1, %r11d
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	movl	%r8d, %ecx
	shll	%cl, %r11d
	movl	%esi, %eax
	leaq	(%r12,%r12), %rcx
	movl	$2, %r13d
	cmpq	%rcx, %rax
	jb	.LBB1_288
# BB#1:                                 # %.preheader411
	movl	224(%rsp), %r8d
	testl	%r12d, %r12d
	je	.LBB1_15
# BB#2:                                 # %.lr.ph452.preheader
	addl	$1845, %edi             # imm = 0x735
	incq	%rdi
	cmpq	$16, %rdi
	jb	.LBB1_13
# BB#3:                                 # %min.iters.checked
	movabsq	$8589934576, %r14       # imm = 0x1FFFFFFF0
	movq	%rdi, %r10
	andq	%r14, %r10
	andq	%rdi, %r14
	je	.LBB1_13
# BB#4:                                 # %vector.body.preheader
	movl	%ebx, 104(%rsp)         # 4-byte Spill
	movl	%r11d, 108(%rsp)        # 4-byte Spill
	movq	%rdx, %r13
	movl	$4294967279, %eax       # imm = 0xFFFFFFEF
	leaq	-16(%r14), %r11
	movl	%r11d, %ebp
	shrl	$4, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB1_8
# BB#5:                                 # %vector.body.prol.preheader
	leaq	16(%r12,%rax), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	.p2align	4, 0x90
.LBB1_6:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rcx), %edx
	movups	%xmm0, -14(%r15,%rdx,2)
	movups	%xmm0, -30(%r15,%rdx,2)
	addq	$-16, %rcx
	incq	%rbp
	jne	.LBB1_6
# BB#7:                                 # %vector.body.prol.loopexit.unr-lcssa
	negq	%rcx
	cmpq	$48, %r11
	jae	.LBB1_9
	jmp	.LBB1_11
.LBB1_8:
	xorl	%ecx, %ecx
	cmpq	$48, %r11
	jb	.LBB1_11
.LBB1_9:                                # %vector.body.preheader.new
	movq	%rdi, %rbp
	andq	$-16, %rbp
	negq	%rbp
	negq	%rcx
	addq	%r12, %rax
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	.p2align	4, 0x90
.LBB1_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leal	16(%rax,%rcx), %edx
	movups	%xmm0, -14(%r15,%rdx,2)
	movups	%xmm0, -30(%r15,%rdx,2)
	leal	(%rax,%rcx), %edx
	movups	%xmm0, -14(%r15,%rdx,2)
	movups	%xmm0, -30(%r15,%rdx,2)
	leal	-16(%rax,%rcx), %edx
	movups	%xmm0, -14(%r15,%rdx,2)
	movups	%xmm0, -30(%r15,%rdx,2)
	leal	-32(%rax,%rcx), %edx
	movups	%xmm0, -14(%r15,%rdx,2)
	movups	%xmm0, -30(%r15,%rdx,2)
	addq	$-64, %rcx
	cmpq	%rcx, %rbp
	jne	.LBB1_10
.LBB1_11:                               # %middle.block
	cmpq	%r14, %rdi
	movq	%r13, %rdx
	movl	108(%rsp), %r11d        # 4-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	je	.LBB1_15
# BB#12:
	subq	%r10, %r12
.LBB1_13:                               # %.lr.ph452.preheader546
	movl	%r12d, %eax
	negl	%eax
	decl	%r12d
	leaq	(%r15,%r12,2), %rcx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph452
                                        # =>This Inner Loop Header: Depth=1
	movw	$1024, (%rcx)           # imm = 0x400
	addq	$-2, %rcx
	incl	%eax
	jne	.LBB1_14
.LBB1_15:                               # %._crit_edge
	movl	$0, 32(%rsp)
	movl	$0, 28(%rsp)
	movq	%r9, 8(%rsp)
	movl	$-1, 24(%rsp)
	movl	%r8d, %eax
	leaq	-13(%r9,%rax), %rax
	movq	%rax, 16(%rsp)
	movq	%r15, 40(%rsp)
	movl	%esi, 48(%rsp)
	cmpq	%r9, %rax
	jbe	.LBB1_17
# BB#16:
	movzbl	(%r9), %edi
	incq	%r9
	movq	%r9, 8(%rsp)
	xorl	%ecx, %ecx
	jmp	.LBB1_18
.LBB1_17:
	movl	$1, 32(%rsp)
	movl	$255, %edi
	movl	$1, %ecx
.LBB1_18:                               # %get_byte.exit
	movl	%edi, 28(%rsp)
	shll	$8, %edi
	cmpq	%rax, %r9
	jae	.LBB1_20
# BB#19:
	movzbl	(%r9), %ebp
	incq	%r9
	movq	%r9, 8(%rsp)
	jmp	.LBB1_21
.LBB1_20:
	movl	$1, 32(%rsp)
	movl	$255, %ebp
	movl	$1, %ecx
.LBB1_21:                               # %get_byte.exit.1
	orl	%edi, %ebp
	movl	%ebp, 28(%rsp)
	shll	$8, %ebp
	cmpq	%rax, %r9
	jae	.LBB1_23
# BB#22:
	movzbl	(%r9), %edi
	incq	%r9
	movq	%r9, 8(%rsp)
	jmp	.LBB1_24
.LBB1_23:
	movl	$1, 32(%rsp)
	movl	$255, %edi
	movl	$1, %ecx
.LBB1_24:                               # %get_byte.exit.2
	orl	%ebp, %edi
	movl	%edi, 28(%rsp)
	shll	$8, %edi
	cmpq	%rax, %r9
	jae	.LBB1_26
# BB#25:
	movzbl	(%r9), %ebp
	incq	%r9
	movq	%r9, 8(%rsp)
	jmp	.LBB1_27
.LBB1_26:
	movl	$1, 32(%rsp)
	movl	$255, %ebp
	movl	$1, %ecx
.LBB1_27:                               # %get_byte.exit.3
	orl	%edi, %ebp
	movl	%ebp, 28(%rsp)
	shll	$8, %ebp
	cmpq	%rax, %r9
	jae	.LBB1_286
# BB#28:                                # %get_byte.exit.4
	movzbl	(%r9), %eax
	incq	%r9
	movq	%r9, 8(%rsp)
	orl	%ebp, %eax
	movl	%eax, 28(%rsp)
	movl	$1, %r13d
	testl	%ecx, %ecx
	jne	.LBB1_288
# BB#29:                                # %.lr.ph
	movq	232(%rsp), %r12
	decl	%ebx
	movl	%ebx, 104(%rsp)         # 4-byte Spill
	decl	%r11d
	movl	%r11d, 108(%rsp)        # 4-byte Spill
	leaq	2664(%r15), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	240(%rsp), %eax
	addq	%r12, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	1636(%r15), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	$8, %eax
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	subl	%edx, %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movl	$1, %r11d
	xorl	%edx, %edx
	movl	$0, %ebp
	movl	$0, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	movl	$1, %r13d
	movl	$1, 60(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	xorl	%edi, %edi
	testl	%esi, %esi
	jne	.LBB1_122
	jmp	.LBB1_133
	.p2align	4, 0x90
.LBB1_275:
	testl	%edx, %edx
	sete	%bl
	movl	%eax, %r8d
	addl	$2, %r8d
	sete	%sil
	movl	%r14d, %ecx
	addq	%r12, %rcx
	cmpl	%edx, %r8d
	ja	.LBB1_289
# BB#276:
	orb	%sil, %bl
	jne	.LBB1_289
# BB#277:
	movl	%r8d, %esi
	leaq	(%rcx,%rsi), %rbp
	cmpq	112(%rsp), %rbp         # 8-byte Folded Reload
	ja	.LBB1_289
# BB#278:
	testl	%r8d, %r8d
	je	.LBB1_289
# BB#279:
	testl	%edx, %edx
	je	.LBB1_289
# BB#280:
	cmpq	%r12, %rbp
	jbe	.LBB1_289
# BB#281:
	addq	%r12, %rdi
	addq	%rsi, %rdi
	cmpq	112(%rsp), %rdi         # 8-byte Folded Reload
	ja	.LBB1_289
# BB#282:
	cmpq	%r12, %rdi
	jbe	.LBB1_289
# BB#283:                               # %.preheader.preheader
	movl	%r11d, %ecx
	negl	%ecx
	notl	%eax
	.p2align	4, 0x90
.LBB1_284:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%r14), %esi
	movzbl	(%r12,%rsi), %ebp
	movl	%r14d, %esi
	movb	%bpl, (%r12,%rsi)
	incl	%r14d
	cmpl	%edx, %r14d
	jae	.LBB1_30
# BB#285:                               # %.preheader
                                        #   in Loop: Header=BB1_284 Depth=1
	testl	%eax, %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jne	.LBB1_284
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_122:
	movl	%edi, %r10d
	movl	%ebx, 80(%rsp)          # 4-byte Spill
	movl	%r13d, 84(%rsp)         # 4-byte Spill
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	shll	$4, %r8d
	movl	%esi, %r13d
	cmpl	$1, %esi
	je	.LBB1_124
# BB#123:
	leal	(%r8,%rdx), %eax
	leaq	(%r15,%rax,2), %rax
	movq	40(%rsp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_134
.LBB1_124:                              # %.thread481
	movl	$1, 32(%rsp)
.LBB1_125:                              # %.thread479
	cmpl	$1, %esi
	je	.LBB1_154
.LBB1_126:
	leal	192(%rbx), %eax
	leaq	(%r15,%rax,2), %rdi
	movq	40(%rsp), %rax
	cmpq	%rax, %rdi
	jb	.LBB1_154
# BB#127:
	leaq	2(%rdi), %rbp
	cmpq	%rax, %rbp
	jbe	.LBB1_154
# BB#128:
	addq	%r13, %rax
	cmpq	%rax, %rbp
	ja	.LBB1_154
# BB#129:
	movq	%r13, %rcx
	movzwl	(%rdi), %ebp
	movl	24(%rsp), %r10d
	movl	28(%rsp), %r13d
	movl	%r10d, %eax
	shrl	$11, %eax
	imull	%ebp, %eax
	movl	%r13d, %r9d
	subl	%eax, %r9d
	jae	.LBB1_143
# BB#130:
	movl	%eax, 24(%rsp)
	movl	$2048, %ecx             # imm = 0x800
	subl	%ebp, %ecx
	shrl	$5, %ecx
	addl	%ebp, %ecx
	movw	%cx, (%rdi)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB1_237
# BB#131:
	shll	$8, %r13d
	movq	8(%rsp), %rsi
	cmpq	16(%rsp), %rsi
	jae	.LBB1_235
# BB#132:
	movzbl	(%rsi), %ecx
	incq	%rsi
	movq	%rsi, 8(%rsp)
	jmp	.LBB1_236
.LBB1_134:
	leaq	2(%rax), %rdi
	cmpq	%rcx, %rdi
	jbe	.LBB1_124
# BB#135:
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	leaq	(%rcx,%r13), %rbx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	cmpq	%rbx, %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
	ja	.LBB1_124
# BB#136:
	movq	%r13, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movzwl	(%rax), %r9d
	movl	24(%rsp), %ecx
	movl	28(%rsp), %edi
	movl	%ecx, %r13d
	shrl	$11, %r13d
	imull	%r9d, %r13d
	movl	%edi, %ebx
	subl	%r13d, %edi
	jae	.LBB1_140
# BB#137:
	movl	%r13d, 24(%rsp)
	movl	$2048, %ecx             # imm = 0x800
	subl	%r9d, %ecx
	shrl	$5, %ecx
	addl	%r9d, %ecx
	movw	%cx, (%rax)
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	movl	%r13d, %r9d
	movl	%ebx, %r8d
	ja	.LBB1_146
# BB#138:
	shll	$8, %r8d
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_147
# BB#139:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_148
.LBB1_140:
	subl	%r13d, %ecx
	movl	%ecx, 24(%rsp)
	movl	%edi, 28(%rsp)
	movl	%r9d, %ebp
	shrl	$5, %ebp
	subl	%ebp, %r9d
	movw	%r9w, (%rax)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
	ja	.LBB1_125
# BB#141:
	shll	$8, %edi
	movq	8(%rsp), %rbp
	cmpq	16(%rsp), %rbp
	jae	.LBB1_152
# BB#142:
	movzbl	(%rbp), %eax
	incq	%rbp
	movq	%rbp, 8(%rsp)
	jmp	.LBB1_153
.LBB1_143:
	subl	%eax, %r10d
	movl	%r10d, 24(%rsp)
	movl	%r9d, 28(%rsp)
	movl	%ebp, %eax
	shrl	$5, %eax
	subl	%eax, %ebp
	movw	%bp, (%rdi)
	cmpl	$16777215, %r10d        # imm = 0xFFFFFF
	movq	%rcx, %r13
	ja	.LBB1_155
# BB#144:
	shll	$8, %r9d
	movq	8(%rsp), %rdi
	cmpq	16(%rsp), %rdi
	jae	.LBB1_261
# BB#145:
	movzbl	(%rdi), %eax
	incq	%rdi
	movq	%rdi, 8(%rsp)
	jmp	.LBB1_262
.LBB1_146:
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_149
.LBB1_147:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_148:                              # %get_byte.exit.i340
	movq	64(%rsp), %rbx          # 8-byte Reload
	orl	%eax, %r8d
	movl	%r8d, 28(%rsp)
	shll	$8, %r9d
	movl	%r9d, 24(%rsp)
.LBB1_149:                              # %getbit_from_table.exit344
	movl	132(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movl	%r14d, %eax
	andl	104(%rsp), %eax         # 4-byte Folded Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	addl	%ebp, %eax
	shll	$8, %eax
	xorl	%edx, %edx
	movq	72(%rsp), %rcx          # 8-byte Reload
	cmpl	$4, %ecx
	jl	.LBB1_169
# BB#150:
	cmpl	$10, %ecx
	jl	.LBB1_167
# BB#151:
	addl	$-6, %ecx
	jmp	.LBB1_168
.LBB1_152:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_153:                              # %get_byte.exit46.i342
	orl	%edi, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %ecx
	movl	%ecx, 24(%rsp)
	cmpl	$1, %esi
	jne	.LBB1_126
	.p2align	4, 0x90
.LBB1_154:                              # %.thread485
	movl	$1, 32(%rsp)
.LBB1_155:                              # %.thread483
	cmpl	$1, %esi
	je	.LBB1_263
.LBB1_156:
	leal	204(%rbx), %eax
	leaq	(%r15,%rax,2), %rdi
	movq	40(%rsp), %rbp
	cmpq	%rbp, %rdi
	jb	.LBB1_263
# BB#157:
	leaq	2(%rdi), %rax
	cmpq	%rbp, %rax
	jbe	.LBB1_263
# BB#158:
	leaq	(%rbp,%r13), %rcx
	cmpq	%rcx, %rax
	ja	.LBB1_263
# BB#159:
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r13, %r10
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	movzwl	(%rdi), %eax
	movl	24(%rsp), %r13d
	movl	28(%rsp), %r9d
	movl	%r13d, %r11d
	shrl	$11, %r11d
	imull	%eax, %r11d
	movl	%r9d, %ecx
	subl	%r11d, %r9d
	jae	.LBB1_163
# BB#160:
	movl	%r11d, 24(%rsp)
	movl	$2048, %esi             # imm = 0x800
	subl	%eax, %esi
	shrl	$5, %esi
	addl	%eax, %esi
	movw	%si, (%rdi)
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB1_166
# BB#161:
	shll	$8, %ecx
	movq	8(%rsp), %rsi
	cmpq	16(%rsp), %rsi
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	80(%rsp), %r10d         # 4-byte Reload
	jae	.LBB1_62
# BB#162:
	movzbl	(%rsi), %eax
	incq	%rsi
	movq	%rsi, 8(%rsp)
	jmp	.LBB1_63
.LBB1_163:
	subl	%r11d, %r13d
	movl	%r13d, 24(%rsp)
	movl	%r9d, 28(%rsp)
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movw	%ax, (%rdi)
	cmpl	$16777215, %r13d        # imm = 0xFFFFFF
	movl	88(%rsp), %r11d         # 4-byte Reload
	ja	.LBB1_264
# BB#164:
	shll	$8, %r9d
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_73
# BB#165:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_74
.LBB1_166:
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	80(%rsp), %r10d         # 4-byte Reload
	jmp	.LBB1_64
.LBB1_167:
	addl	$-3, %ecx
.LBB1_168:
	movl	%ecx, %edx
.LBB1_169:
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	leal	(%rax,%rax,2), %ecx
	testl	%r10d, %r10d
	je	.LBB1_212
# BB#170:
	movl	%r8d, %ebx
	movl	$1, %r13d
	movl	240(%rsp), %r11d
	testl	%r11d, %r11d
	movq	96(%rsp), %r8           # 8-byte Reload
	je	.LBB1_288
# BB#171:
	movl	%r14d, %eax
	subl	60(%rsp), %eax          # 4-byte Folded Reload
	leaq	1(%r12,%rax), %rdx
	cmpq	112(%rsp), %rdx         # 8-byte Folded Reload
	ja	.LBB1_288
# BB#172:
	cmpq	%r12, %rdx
	jbe	.LBB1_288
# BB#173:
	addq	%r12, %rax
	movzbl	(%rax), %eax
	addl	$1846, %ecx             # imm = 0x736
	leaq	(%r15,%rcx,2), %r10
	movl	$1, %ecx
	movl	%ebx, %r13d
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_183
.LBB1_174:                              #   in Loop: Header=BB1_183 Depth=1
	subl	%r11d, %r9d
	movl	%r9d, 24(%rsp)
	movl	%edi, 28(%rsp)
	movl	%r13d, %ebx
	shrl	$5, %ebx
	subl	%ebx, %r13d
	movw	%r13w, (%rdx)
	movl	$1, %edx
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB1_178
# BB#175:                               #   in Loop: Header=BB1_183 Depth=1
	shll	$8, %edi
	movq	8(%rsp), %rbx
	cmpq	16(%rsp), %rbx
	movl	240(%rsp), %r11d
	jae	.LBB1_181
# BB#176:                               #   in Loop: Header=BB1_183 Depth=1
	movzbl	(%rbx), %r13d
	incq	%rbx
	movq	%rbx, 8(%rsp)
	jmp	.LBB1_182
.LBB1_177:                              #   in Loop: Header=BB1_183 Depth=1
	movl	%r11d, %r9d
	movl	240(%rsp), %r11d
	movl	%ebx, %r13d
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_191
.LBB1_178:                              #   in Loop: Header=BB1_183 Depth=1
	movl	%edi, %r13d
	movl	240(%rsp), %r11d
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_191
.LBB1_179:                              #   in Loop: Header=BB1_183 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %edi
.LBB1_180:                              # %get_byte.exit.i358
                                        #   in Loop: Header=BB1_183 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	orl	%edi, %r13d
	movl	%r13d, 28(%rsp)
	shll	$8, %r11d
	movl	%r11d, 24(%rsp)
	movl	%r11d, %r9d
	movl	240(%rsp), %r11d
	jmp	.LBB1_191
.LBB1_181:                              #   in Loop: Header=BB1_183 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r13d
.LBB1_182:                              # %get_byte.exit46.i360
                                        #   in Loop: Header=BB1_183 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	orl	%edi, %r13d
	movl	%r13d, 28(%rsp)
	movl	%r9d, %edi
	shll	$8, %edi
	movl	%edi, %r9d
	movl	%edi, 24(%rsp)
	jmp	.LBB1_191
.LBB1_183:                              # =>This Inner Loop Header: Depth=1
	movzbl	%al, %esi
	movl	%esi, %ebp
	shrl	$7, %ebp
	movl	%ebp, %edx
	shll	$8, %edx
	leal	256(%rcx,%rdx), %edx
	leaq	(%r10,%rdx,2), %rdx
	cmpq	%rbx, %rdx
	jb	.LBB1_190
# BB#184:                               #   in Loop: Header=BB1_183 Depth=1
	leaq	2(%rdx), %rdi
	cmpq	%rbx, %rdi
	jbe	.LBB1_190
# BB#185:                               #   in Loop: Header=BB1_183 Depth=1
	cmpq	%r8, %rdi
	ja	.LBB1_190
# BB#186:                               #   in Loop: Header=BB1_183 Depth=1
	movl	%r13d, %edi
	movzwl	(%rdx), %r13d
	movl	%r9d, %r11d
	shrl	$11, %r11d
	imull	%r13d, %r11d
	movl	%edi, %ebx
	subl	%r11d, %edi
	jae	.LBB1_174
# BB#187:                               #   in Loop: Header=BB1_183 Depth=1
	movl	%r11d, 24(%rsp)
	movl	$2048, %edi             # imm = 0x800
	subl	%r13d, %edi
	shrl	$5, %edi
	addl	%r13d, %edi
	movw	%di, (%rdx)
	xorl	%edx, %edx
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB1_177
# BB#188:                               #   in Loop: Header=BB1_183 Depth=1
	movl	%ebx, %r13d
	shll	$8, %r13d
	movq	8(%rsp), %rbx
	cmpq	16(%rsp), %rbx
	jae	.LBB1_179
# BB#189:                               #   in Loop: Header=BB1_183 Depth=1
	movzbl	(%rbx), %edi
	incq	%rbx
	movq	%rbx, 8(%rsp)
	jmp	.LBB1_180
.LBB1_190:                              #   in Loop: Header=BB1_183 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %edx
.LBB1_191:                              # %getbit_from_table.exit362
                                        #   in Loop: Header=BB1_183 Depth=1
	addl	%ecx, %ecx
	orl	%edx, %ecx
	cmpl	%edx, %ebp
	je	.LBB1_193
# BB#192:                               # %getbit_from_table.exit362
                                        #   in Loop: Header=BB1_183 Depth=1
	cmpl	$255, %ecx
	jbe	.LBB1_203
.LBB1_193:                              # %.backedge.i346
                                        #   in Loop: Header=BB1_183 Depth=1
	andl	$127, %esi
	andl	$-256, %eax
	cmpl	$256, %ecx              # imm = 0x100
	leal	(%rax,%rsi,2), %eax
	jb	.LBB1_183
	jmp	.LBB1_231
.LBB1_194:                              #   in Loop: Header=BB1_203 Depth=1
	subl	%edx, %r9d
	movl	%r9d, 24(%rsp)
	movl	%esi, 28(%rsp)
	movl	%edi, %edx
	shrl	$5, %edx
	subl	%edx, %edi
	movw	%di, (%rcx)
	movl	$1, %ecx
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB1_198
# BB#195:                               #   in Loop: Header=BB1_203 Depth=1
	shll	$8, %esi
	movq	8(%rsp), %rdx
	cmpq	16(%rsp), %rdx
	jae	.LBB1_201
# BB#196:                               #   in Loop: Header=BB1_203 Depth=1
	movzbl	(%rdx), %r13d
	incq	%rdx
	movq	%rdx, 8(%rsp)
	jmp	.LBB1_202
.LBB1_197:                              #   in Loop: Header=BB1_203 Depth=1
	movl	%edx, %r9d
	jmp	.LBB1_211
.LBB1_198:                              #   in Loop: Header=BB1_203 Depth=1
	movl	%esi, %r13d
	jmp	.LBB1_211
.LBB1_199:                              #   in Loop: Header=BB1_203 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %esi
.LBB1_200:                              # %get_byte.exit.i351
                                        #   in Loop: Header=BB1_203 Depth=1
	orl	%esi, %r13d
	movl	%r13d, 28(%rsp)
	shll	$8, %edx
	movl	%edx, 24(%rsp)
	movl	%edx, %r9d
	jmp	.LBB1_211
.LBB1_201:                              #   in Loop: Header=BB1_203 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r13d
.LBB1_202:                              # %get_byte.exit46.i353
                                        #   in Loop: Header=BB1_203 Depth=1
	orl	%esi, %r13d
	movl	%r13d, 28(%rsp)
	movl	%r9d, %edi
	shll	$8, %edi
	movl	%edi, %r9d
	movl	%edi, 24(%rsp)
	jmp	.LBB1_211
.LBB1_203:                              # %.preheader494
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movl	%ecx, %eax
	addl	%eax, %eax
	leaq	(%r10,%rdx,2), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB1_210
# BB#204:                               #   in Loop: Header=BB1_203 Depth=1
	leaq	2(%rcx), %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB1_210
# BB#205:                               #   in Loop: Header=BB1_203 Depth=1
	cmpq	%r8, %rdx
	ja	.LBB1_210
# BB#206:                               #   in Loop: Header=BB1_203 Depth=1
	movzwl	(%rcx), %edi
	movl	%r9d, %edx
	shrl	$11, %edx
	imull	%edi, %edx
	movl	%r13d, %esi
	subl	%edx, %esi
	jae	.LBB1_194
# BB#207:                               #   in Loop: Header=BB1_203 Depth=1
	movl	%edx, 24(%rsp)
	movl	$2048, %esi             # imm = 0x800
	subl	%edi, %esi
	shrl	$5, %esi
	addl	%edi, %esi
	movw	%si, (%rcx)
	xorl	%ecx, %ecx
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB1_197
# BB#208:                               #   in Loop: Header=BB1_203 Depth=1
	shll	$8, %r13d
	movq	8(%rsp), %rdi
	cmpq	16(%rsp), %rdi
	jae	.LBB1_199
# BB#209:                               #   in Loop: Header=BB1_203 Depth=1
	movzbl	(%rdi), %esi
	incq	%rdi
	movq	%rdi, 8(%rsp)
	jmp	.LBB1_200
.LBB1_210:                              #   in Loop: Header=BB1_203 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_211:                              # %getbit_from_table.exit355
                                        #   in Loop: Header=BB1_203 Depth=1
	orl	%eax, %ecx
	cmpl	$256, %ecx              # imm = 0x100
	jb	.LBB1_203
	jmp	.LBB1_231
.LBB1_212:
	addl	$1846, %ecx             # imm = 0x736
	leaq	(%r15,%rcx,2), %rax
	movl	$1, %ecx
	movl	240(%rsp), %r11d
	movq	96(%rsp), %r10          # 8-byte Reload
	jmp	.LBB1_228
.LBB1_213:                              #   in Loop: Header=BB1_228 Depth=1
	leaq	2(%rcx), %rsi
	cmpq	%rbx, %rsi
	jbe	.LBB1_229
# BB#214:                               #   in Loop: Header=BB1_228 Depth=1
	cmpq	%r10, %rsi
	ja	.LBB1_229
# BB#215:                               #   in Loop: Header=BB1_228 Depth=1
	movzwl	(%rcx), %ebp
	movl	%r9d, %ebx
	shrl	$11, %r9d
	imull	%ebp, %r9d
	movl	%r8d, %edi
	subl	%r9d, %edi
	jae	.LBB1_219
# BB#216:                               #   in Loop: Header=BB1_228 Depth=1
	movl	%r9d, 24(%rsp)
	movl	$2048, %edi             # imm = 0x800
	subl	%ebp, %edi
	shrl	$5, %edi
	addl	%ebp, %edi
	movw	%di, (%rcx)
	xorl	%ecx, %ecx
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB1_227
# BB#217:                               #   in Loop: Header=BB1_228 Depth=1
	shll	$8, %r8d
	movq	8(%rsp), %rbp
	cmpq	16(%rsp), %rbp
	movq	64(%rsp), %rbx          # 8-byte Reload
	jae	.LBB1_223
# BB#218:                               #   in Loop: Header=BB1_228 Depth=1
	movzbl	(%rbp), %edi
	incq	%rbp
	movq	%rbp, 8(%rsp)
	jmp	.LBB1_224
.LBB1_219:                              #   in Loop: Header=BB1_228 Depth=1
	subl	%r9d, %ebx
	movl	%ebx, 24(%rsp)
	movl	%edi, 28(%rsp)
	movl	%ebp, %esi
	shrl	$5, %esi
	subl	%esi, %ebp
	movw	%bp, (%rcx)
	movl	$1, %ecx
	cmpl	$16777215, %ebx         # imm = 0xFFFFFF
	ja	.LBB1_222
# BB#220:                               #   in Loop: Header=BB1_228 Depth=1
	shll	$8, %edi
	movq	8(%rsp), %rsi
	cmpq	16(%rsp), %rsi
	jae	.LBB1_225
# BB#221:                               #   in Loop: Header=BB1_228 Depth=1
	movzbl	(%rsi), %r8d
	incq	%rsi
	movq	%rsi, 8(%rsp)
	jmp	.LBB1_226
.LBB1_222:                              #   in Loop: Header=BB1_228 Depth=1
	movl	%ebx, %r9d
	movl	%edi, %r8d
	jmp	.LBB1_227
.LBB1_223:                              #   in Loop: Header=BB1_228 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %edi
.LBB1_224:                              # %get_byte.exit.i379
                                        #   in Loop: Header=BB1_228 Depth=1
	orl	%edi, %r8d
	movl	%r8d, 28(%rsp)
	shll	$8, %r9d
	movl	%r9d, 24(%rsp)
	jmp	.LBB1_230
.LBB1_225:                              #   in Loop: Header=BB1_228 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r8d
.LBB1_226:                              # %get_byte.exit46.i381
                                        #   in Loop: Header=BB1_228 Depth=1
	orl	%edi, %r8d
	movl	%r8d, 28(%rsp)
	shll	$8, %ebx
	movl	%ebx, %r9d
	movl	%ebx, 24(%rsp)
.LBB1_227:                              # %getbit_from_table.exit383
                                        #   in Loop: Header=BB1_228 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_230
.LBB1_228:                              # =>This Inner Loop Header: Depth=1
	movl	%ecx, %esi
	movl	%ecx, %edx
	addl	%edx, %edx
	leaq	(%rax,%rsi,2), %rcx
	cmpq	%rbx, %rcx
	jae	.LBB1_213
.LBB1_229:                              #   in Loop: Header=BB1_228 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_230:                              # %getbit_from_table.exit383
                                        #   in Loop: Header=BB1_228 Depth=1
	orl	%edx, %ecx
	cmpl	$256, %ecx              # imm = 0x100
	jb	.LBB1_228
.LBB1_231:                              # %get_100_bits_from_table.exit
	testl	%r11d, %r11d
	movl	$1, %r13d
	je	.LBB1_288
# BB#232:
	movl	%r14d, %eax
	leaq	1(%r12,%rax), %rdx
	cmpq	112(%rsp), %rdx         # 8-byte Folded Reload
	ja	.LBB1_288
# BB#233:
	cmpq	%r12, %rdx
	jbe	.LBB1_288
# BB#234:
	movzbl	%cl, %ebp
	addq	%r12, %rax
	movb	%cl, (%rax)
	incl	%r14d
	cmpl	%r11d, %r14d
	movl	$1, %ecx
	movl	$7, %eax
	cmovbl	%eax, %ecx
	xorl	%edi, %edi
	movl	60(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r11d
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	72(%rsp), %r9d          # 4-byte Reload
	jmp	.LBB1_31
.LBB1_235:
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_236:                              # %get_byte.exit.i400
	orl	%r13d, %ecx
	movl	%ecx, 28(%rsp)
	shll	$8, %eax
	movl	%eax, 24(%rsp)
.LBB1_237:                              # %getbit_from_table.exit404
	movq	152(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	get_n_bits_from_tablesize
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$4, %eax
	movl	%eax, %ecx
	movl	$3, %edx
	cmovgel	%edx, %ecx
	shll	$6, %ecx
	addl	$432, %ecx              # imm = 0x1B0
	leaq	(%r15,%rcx,2), %r8
	movl	48(%rsp), %r9d
	movl	$1, %ebp
	movl	$-6, %esi
	movl	240(%rsp), %edx
	movq	%r9, 88(%rsp)           # 8-byte Spill
	jmp	.LBB1_247
.LBB1_238:                              #   in Loop: Header=BB1_247 Depth=1
	subl	%edi, %r10d
	movl	%r10d, 24(%rsp)
	movl	%r11d, 28(%rsp)
	movl	%ecx, %edi
	shrl	$5, %edi
	subl	%edi, %ecx
	movw	%cx, (%rbp)
	movl	$1, %ebp
	cmpl	$16777215, %r10d        # imm = 0xFFFFFF
	ja	.LBB1_242
# BB#239:                               #   in Loop: Header=BB1_247 Depth=1
	shll	$8, %r11d
	movq	8(%rsp), %rdi
	cmpq	16(%rsp), %rdi
	movq	88(%rsp), %r9           # 8-byte Reload
	jae	.LBB1_245
# BB#240:                               #   in Loop: Header=BB1_247 Depth=1
	movzbl	(%rdi), %ecx
	incq	%rdi
	movq	%rdi, 8(%rsp)
	jmp	.LBB1_246
.LBB1_241:                              #   in Loop: Header=BB1_247 Depth=1
	movl	240(%rsp), %edx
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_256
.LBB1_242:                              #   in Loop: Header=BB1_247 Depth=1
	movl	240(%rsp), %edx
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_256
.LBB1_243:                              #   in Loop: Header=BB1_247 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_244:                              # %get_byte.exit.i333
                                        #   in Loop: Header=BB1_247 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	orl	%r9d, %ecx
	movl	%ecx, 28(%rsp)
	shll	$8, %edi
	movl	%edi, 24(%rsp)
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_256
.LBB1_245:                              #   in Loop: Header=BB1_247 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_246:                              # %get_byte.exit46.i335
                                        #   in Loop: Header=BB1_247 Depth=1
	orl	%r11d, %ecx
	movl	%ecx, 28(%rsp)
	shll	$8, %r10d
	movl	%r10d, 24(%rsp)
	movl	240(%rsp), %edx
	jmp	.LBB1_256
	.p2align	4, 0x90
.LBB1_247:                              # %.lr.ph.i330
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp), %r13d
	cmpl	$2, %r9d
	jb	.LBB1_255
# BB#248:                               #   in Loop: Header=BB1_247 Depth=1
	movl	%ebp, %ecx
	leaq	(%r8,%rcx,2), %rbp
	movq	40(%rsp), %rcx
	cmpq	%rcx, %rbp
	jb	.LBB1_255
# BB#249:                               #   in Loop: Header=BB1_247 Depth=1
	leaq	2(%rbp), %rdi
	cmpq	%rcx, %rdi
	jbe	.LBB1_255
# BB#250:                               #   in Loop: Header=BB1_247 Depth=1
	addq	%r9, %rcx
	cmpq	%rcx, %rdi
	ja	.LBB1_255
# BB#251:                               #   in Loop: Header=BB1_247 Depth=1
	movzwl	(%rbp), %ecx
	movl	24(%rsp), %r10d
	movl	28(%rsp), %r9d
	movl	%r10d, %edi
	shrl	$11, %edi
	imull	%ecx, %edi
	movl	%r9d, %r11d
	subl	%edi, %r11d
	jae	.LBB1_238
# BB#252:                               #   in Loop: Header=BB1_247 Depth=1
	movl	%edi, 24(%rsp)
	movl	$2048, %ebx             # imm = 0x800
	subl	%ecx, %ebx
	shrl	$5, %ebx
	addl	%ecx, %ebx
	movw	%bx, (%rbp)
	xorl	%ebp, %ebp
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB1_241
# BB#253:                               #   in Loop: Header=BB1_247 Depth=1
	shll	$8, %r9d
	movq	8(%rsp), %rbx
	cmpq	16(%rsp), %rbx
	movl	240(%rsp), %edx
	jae	.LBB1_243
# BB#254:                               #   in Loop: Header=BB1_247 Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movq	%rbx, 8(%rsp)
	jmp	.LBB1_244
	.p2align	4, 0x90
.LBB1_255:                              #   in Loop: Header=BB1_247 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ebp
.LBB1_256:                              # %getbit_from_table.exit337
                                        #   in Loop: Header=BB1_247 Depth=1
	addl	%r13d, %ebp
	incl	%esi
	jne	.LBB1_247
# BB#257:                               # %get_n_bits_from_table.exit
	xorl	%r13d, %r13d
	cmpl	$6, %ebx
	setg	%r13b
	leal	-64(%rbp), %r11d
	cmpl	$4, %r11d
	jb	.LBB1_260
# BB#258:
	movl	%r11d, %esi
	shrl	%esi
	leal	-1(%rsi), %r10d
	movl	%r11d, %edi
	andl	$1, %edi
	orl	$2, %edi
	movl	%r10d, %ecx
	shll	%cl, %edi
	cmpl	$13, %r11d
	movq	%r13, 120(%rsp)         # 8-byte Spill
	jg	.LBB1_52
# BB#259:
	movl	$64, %ecx
	subl	%ebp, %ecx
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	leal	687(%rcx,%rdi), %ecx
	jmp	.LBB1_88
.LBB1_260:
	movl	84(%rsp), %r10d         # 4-byte Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB1_111
.LBB1_261:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_262:                              # %get_byte.exit46.i402
	orl	%r9d, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %r10d
	movl	%r10d, 24(%rsp)
	cmpl	$1, %esi
	jne	.LBB1_156
	.p2align	4, 0x90
.LBB1_263:                              # %.thread489
	movq	%r13, %r10
	movl	$1, 32(%rsp)
.LBB1_264:                              # %.thread487
	cmpl	$1, %esi
	je	.LBB1_266
.LBB1_265:
	leal	216(%rbx), %eax
	leaq	(%r15,%rax,2), %rcx
	movq	40(%rsp), %rax
	cmpq	%rax, %rcx
	jae	.LBB1_34
.LBB1_266:                              # %.thread493
	movl	$1, 32(%rsp)
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	80(%rsp), %ebx          # 4-byte Reload
.LBB1_267:                              # %.thread491
	cmpl	$1, %esi
	je	.LBB1_269
.LBB1_268:
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	228(%rax), %eax
	leaq	(%r15,%rax,2), %rax
	movq	40(%rsp), %rcx
	cmpq	%rcx, %rax
	jb	.LBB1_269
# BB#40:
	leaq	2(%rax), %rsi
	cmpq	%rcx, %rsi
	jbe	.LBB1_269
# BB#41:
	addq	%r10, %rcx
	cmpq	%rcx, %rsi
	ja	.LBB1_269
# BB#42:
	movzwl	(%rax), %ebp
	movl	24(%rsp), %esi
	movl	28(%rsp), %r8d
	movl	%esi, %ecx
	shrl	$11, %ecx
	imull	%ebp, %ecx
	movl	%r8d, %edi
	subl	%ecx, %edi
	jae	.LBB1_49
# BB#43:
	movl	%ecx, 24(%rsp)
	movl	$2048, %esi             # imm = 0x800
	subl	%ebp, %esi
	shrl	$5, %esi
	addl	%ebp, %esi
	movw	%si, (%rax)
	movb	$1, %al
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB1_271
# BB#44:
	shll	$8, %r8d
	movq	8(%rsp), %rdi
	cmpq	16(%rsp), %rdi
	jae	.LBB1_79
# BB#45:
	movzbl	(%rdi), %esi
	incq	%rdi
	movq	%rdi, 8(%rsp)
	jmp	.LBB1_80
.LBB1_34:
	leaq	2(%rcx), %rdi
	cmpq	%rax, %rdi
	jbe	.LBB1_266
# BB#35:
	addq	%r10, %rax
	cmpq	%rax, %rdi
	ja	.LBB1_266
# BB#36:
	movzwl	(%rcx), %eax
	movl	24(%rsp), %r9d
	movl	28(%rsp), %r8d
	movl	%r9d, %edi
	shrl	$11, %edi
	imull	%eax, %edi
	movl	%r8d, %ebp
	subl	%edi, %ebp
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	80(%rsp), %ebx          # 4-byte Reload
	jae	.LBB1_46
# BB#37:
	movl	%edi, 24(%rsp)
	movl	$2048, %esi             # imm = 0x800
	subl	%eax, %esi
	shrl	$5, %esi
	addl	%eax, %esi
	movw	%si, (%rcx)
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	ja	.LBB1_272
# BB#38:
	shll	$8, %r8d
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_75
# BB#39:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_76
.LBB1_46:
	subl	%edi, %r9d
	movl	%r9d, 24(%rsp)
	movl	%ebp, 28(%rsp)
	movl	%eax, %edi
	shrl	$5, %edi
	subl	%edi, %eax
	movw	%ax, (%rcx)
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB1_267
# BB#47:
	shll	$8, %ebp
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_77
# BB#48:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_78
.LBB1_49:
	subl	%ecx, %esi
	movl	%esi, 24(%rsp)
	movl	%edi, 28(%rsp)
	movl	%ebp, %ecx
	shrl	$5, %ecx
	subl	%ecx, %ebp
	movw	%bp, (%rax)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB1_270
# BB#50:
	shll	$8, %edi
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_81
# BB#51:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_82
.LBB1_52:
	movl	%esi, %ecx
	addl	$-5, %ecx
	jle	.LBB1_86
# BB#53:                                # %.preheader.i
	movl	24(%rsp), %ebp
	movl	28(%rsp), %ecx
	movl	$5, %ebx
	subl	%esi, %ebx
	xorl	%r8d, %r8d
	movq	%rdi, %r10
.LBB1_54:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %esi
	shrl	%esi
	movl	%esi, 24(%rsp)
	addl	%r8d, %r8d
	movl	%ecx, %edi
	subl	%esi, %edi
	jae	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_54 Depth=1
	movl	%ecx, %edi
	cmpl	$33554431, %ebp         # imm = 0x1FFFFFF
	jbe	.LBB1_57
	jmp	.LBB1_61
.LBB1_56:                               #   in Loop: Header=BB1_54 Depth=1
	movl	%edi, 28(%rsp)
	orl	$1, %r8d
	cmpl	$33554431, %ebp         # imm = 0x1FFFFFF
	ja	.LBB1_61
.LBB1_57:                               #   in Loop: Header=BB1_54 Depth=1
	shll	$8, %esi
	movl	%esi, 24(%rsp)
	shll	$8, %edi
	movq	8(%rsp), %rbp
	cmpq	16(%rsp), %rbp
	jae	.LBB1_59
# BB#58:                                #   in Loop: Header=BB1_54 Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movq	%rbp, 8(%rsp)
	jmp	.LBB1_60
.LBB1_59:                               #   in Loop: Header=BB1_54 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %ecx
.LBB1_60:                               # %get_byte.exit.i329
                                        #   in Loop: Header=BB1_54 Depth=1
	orl	%ecx, %edi
	movl	%edi, 28(%rsp)
.LBB1_61:                               # %.backedge.i
                                        #   in Loop: Header=BB1_54 Depth=1
	incl	%ebx
	movl	%edi, %ecx
	movl	%esi, %ebp
	jne	.LBB1_54
	jmp	.LBB1_87
.LBB1_62:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_63:                               # %get_byte.exit.i393
	orl	%eax, %ecx
	movl	%ecx, 28(%rsp)
	shll	$8, %r11d
	movl	%r11d, 24(%rsp)
.LBB1_64:
	leal	240(%rdx,%r8), %eax
	leaq	(%r15,%rax,2), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB1_71
# BB#65:
	leaq	2(%rbx), %rax
	cmpq	%rbp, %rax
	jbe	.LBB1_71
# BB#66:
	cmpq	64(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB1_71
# BB#67:
	movzwl	(%rbx), %edi
	movl	%r11d, %eax
	shrl	$11, %eax
	imull	%edi, %eax
	movl	%ecx, %esi
	subl	%eax, %esi
	jae	.LBB1_83
# BB#68:
	movl	%eax, 24(%rsp)
	movl	$2048, %edx             # imm = 0x800
	subl	%edi, %edx
	shrl	$5, %edx
	addl	%edi, %edx
	movw	%dx, (%rbx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	movl	240(%rsp), %edi
	movq	72(%rsp), %rsi          # 8-byte Reload
	ja	.LBB1_114
# BB#69:
	shll	$8, %ecx
	movq	8(%rsp), %rdx
	cmpq	16(%rsp), %rdx
	jae	.LBB1_112
# BB#70:
	movzbl	(%rdx), %ebp
	incq	%rdx
	movq	%rdx, 8(%rsp)
	jmp	.LBB1_113
.LBB1_71:
	movl	%r10d, %ebp
	movl	$1, 32(%rsp)
.LBB1_72:
	movq	144(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	get_n_bits_from_tablesize
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	xorl	%ecx, %ecx
	cmpl	$6, 72(%rsp)            # 4-byte Folded Reload
	setg	%cl
	leal	8(%rcx,%rcx,2), %r9d
	movl	60(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r11d
	movl	%r13d, 60(%rsp)         # 4-byte Spill
	movl	%ebp, %r10d
	jmp	.LBB1_273
.LBB1_73:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_74:                               # %get_byte.exit46.i395
	orl	%r9d, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %r13d
	movl	%r13d, 24(%rsp)
	cmpl	$1, %esi
	jne	.LBB1_265
	jmp	.LBB1_266
.LBB1_75:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_76:                               # %get_byte.exit.i372
	orl	%r8d, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %edi
	movl	%edi, 24(%rsp)
	jmp	.LBB1_272
.LBB1_77:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_78:                               # %get_byte.exit46.i374
	orl	%ebp, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %r9d
	movl	%r9d, 24(%rsp)
	cmpl	$1, %esi
	jne	.LBB1_268
	jmp	.LBB1_269
.LBB1_79:
	movl	$1, 32(%rsp)
	movl	$255, %esi
.LBB1_80:                               # %get_byte.exit.i365
	orl	%r8d, %esi
	movl	%esi, 28(%rsp)
	shll	$8, %ecx
	movl	%ecx, 24(%rsp)
	jmp	.LBB1_271
.LBB1_81:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_82:                               # %get_byte.exit46.i367
	orl	%edi, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %esi
	movl	%esi, 24(%rsp)
	jmp	.LBB1_270
.LBB1_83:
	movl	%r10d, %ebp
	subl	%eax, %r11d
	movl	%r11d, 24(%rsp)
	movl	%esi, 28(%rsp)
	movl	%edi, %eax
	shrl	$5, %eax
	subl	%eax, %edi
	movw	%di, (%rbx)
	cmpl	$16777215, %r11d        # imm = 0xFFFFFF
	ja	.LBB1_72
# BB#84:
	shll	$8, %esi
	movq	8(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	jae	.LBB1_119
# BB#85:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 8(%rsp)
	jmp	.LBB1_120
.LBB1_86:
	xorl	%r8d, %r8d
	movq	%rdi, %r10
.LBB1_87:                               # %get_bitmap.exit
	shll	$4, %r8d
	addl	%r10d, %r8d
	movl	$4, %r10d
	movl	$802, %ecx              # imm = 0x322
	movl	%r8d, %esi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
.LBB1_88:
	leaq	(%r15,%rcx,2), %r8
	movl	%r9d, %ecx
	orl	$1, %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movl	$1, %r13d
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	movl	%r10d, 64(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	jmp	.LBB1_100
.LBB1_89:                               #   in Loop: Header=BB1_100 Depth=1
	subl	%r8d, %r9d
	movl	%r9d, 24(%rsp)
	movl	%r10d, 28(%rsp)
	movl	%r11d, %ebp
	shrl	$5, %ebp
	subl	%ebp, %r11d
	movw	%r11w, (%rdi)
	movl	$1, %r11d
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB1_93
# BB#90:                                #   in Loop: Header=BB1_100 Depth=1
	shll	$8, %r10d
	movq	8(%rsp), %rbx
	cmpq	16(%rsp), %rbx
	movl	240(%rsp), %edx
	jae	.LBB1_96
# BB#91:                                #   in Loop: Header=BB1_100 Depth=1
	movzbl	(%rbx), %r8d
	incq	%rbx
	movq	%rbx, 8(%rsp)
	jmp	.LBB1_97
.LBB1_92:                               #   in Loop: Header=BB1_100 Depth=1
	movl	240(%rsp), %edx
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_99
.LBB1_93:                               #   in Loop: Header=BB1_100 Depth=1
	movl	240(%rsp), %edx
	jmp	.LBB1_98
.LBB1_94:                               #   in Loop: Header=BB1_100 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r9d
.LBB1_95:                               # %get_byte.exit.i
                                        #   in Loop: Header=BB1_100 Depth=1
	orl	%edi, %r9d
	movl	%r9d, 28(%rsp)
	shll	$8, %r8d
	movl	%r8d, 24(%rsp)
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_99
.LBB1_96:                               #   in Loop: Header=BB1_100 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r8d
.LBB1_97:                               # %get_byte.exit46.i
                                        #   in Loop: Header=BB1_100 Depth=1
	orl	%r10d, %r8d
	movl	%r8d, 28(%rsp)
	shll	$8, %r9d
	movl	%r9d, 24(%rsp)
.LBB1_98:                               # %getbit_from_table.exit
                                        #   in Loop: Header=BB1_100 Depth=1
	movq	88(%rsp), %r9           # 8-byte Reload
	movl	64(%rsp), %r10d         # 4-byte Reload
.LBB1_99:                               # %getbit_from_table.exit
                                        #   in Loop: Header=BB1_100 Depth=1
	movq	96(%rsp), %r8           # 8-byte Reload
	jmp	.LBB1_109
	.p2align	4, 0x90
.LBB1_100:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r11d, %esi
	cmpl	$1, 72(%rsp)            # 4-byte Folded Reload
	je	.LBB1_108
# BB#101:                               #   in Loop: Header=BB1_100 Depth=1
	movl	%r13d, %edi
	leaq	(%r8,%rdi,2), %rdi
	movq	40(%rsp), %rbx
	cmpq	%rbx, %rdi
	jb	.LBB1_108
# BB#102:                               #   in Loop: Header=BB1_100 Depth=1
	leaq	2(%rdi), %rbp
	cmpq	%rbx, %rbp
	jbe	.LBB1_108
# BB#103:                               #   in Loop: Header=BB1_100 Depth=1
	addq	%r9, %rbx
	cmpq	%rbx, %rbp
	ja	.LBB1_108
# BB#104:                               #   in Loop: Header=BB1_100 Depth=1
	movzwl	(%rdi), %r11d
	movl	24(%rsp), %r9d
	movl	28(%rsp), %edx
	movl	%r9d, %r8d
	shrl	$11, %r8d
	imull	%r11d, %r8d
	movl	%edx, %r10d
	subl	%r8d, %r10d
	jae	.LBB1_89
# BB#105:                               #   in Loop: Header=BB1_100 Depth=1
	movl	%r8d, 24(%rsp)
	movl	$2048, %ebp             # imm = 0x800
	subl	%r11d, %ebp
	shrl	$5, %ebp
	addl	%r11d, %ebp
	movw	%bp, (%rdi)
	xorl	%r11d, %r11d
	cmpl	$16777215, %r8d         # imm = 0xFFFFFF
	movl	64(%rsp), %r10d         # 4-byte Reload
	ja	.LBB1_92
# BB#106:                               #   in Loop: Header=BB1_100 Depth=1
	shll	$8, %edx
	movl	%edx, %edi
	movq	8(%rsp), %rbx
	cmpq	16(%rsp), %rbx
	movl	240(%rsp), %edx
	jae	.LBB1_94
# BB#107:                               #   in Loop: Header=BB1_100 Depth=1
	movzbl	(%rbx), %r9d
	incq	%rbx
	movq	%rbx, 8(%rsp)
	jmp	.LBB1_95
	.p2align	4, 0x90
.LBB1_108:                              #   in Loop: Header=BB1_100 Depth=1
	movl	$1, 32(%rsp)
	movl	$255, %r11d
.LBB1_109:                              # %getbit_from_table.exit
                                        #   in Loop: Header=BB1_100 Depth=1
	leal	(%r11,%r13,2), %r13d
	shll	%cl, %r11d
	orl	%esi, %r11d
	incl	%ecx
	cmpl	%ecx, %r10d
	jne	.LBB1_100
# BB#110:                               # %get_bb.exit
	addl	136(%rsp), %r11d        # 4-byte Folded Reload
	movl	84(%rsp), %r10d         # 4-byte Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
.LBB1_111:
	leal	7(%r13,%r13,2), %r9d
	incl	%r11d
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	testl	%r11d, %r11d
	jne	.LBB1_274
	jmp	.LBB1_291
.LBB1_112:
	movl	$1, 32(%rsp)
	movl	$255, %ebp
.LBB1_113:                              # %get_byte.exit.i386
	orl	%ecx, %ebp
	movl	%ebp, 28(%rsp)
	shll	$8, %eax
	movl	%eax, 24(%rsp)
.LBB1_114:                              # %getbit_from_table.exit390
	movl	$1, %r13d
	testl	%r14d, %r14d
	je	.LBB1_288
# BB#115:
	xorl	%eax, %eax
	cmpl	$6, %esi
	setg	%dl
	testl	%edi, %edi
	je	.LBB1_288
# BB#116:
	movl	%r14d, %ecx
	subl	60(%rsp), %ecx          # 4-byte Folded Reload
	leaq	1(%r12,%rcx), %rsi
	cmpq	112(%rsp), %rsi         # 8-byte Folded Reload
	ja	.LBB1_288
# BB#117:
	cmpq	%r12, %rsi
	jbe	.LBB1_288
# BB#118:
	movb	%dl, %al
	leal	9(%rax,%rax), %r9d
	addq	%r12, %rcx
	movzbl	(%rcx), %ebp
	movl	%r14d, %eax
	movb	%bpl, (%r12,%rax)
	incl	%r14d
	cmpl	%edi, %r14d
	movl	$1, %edi
	movl	$1, %ecx
	movl	$7, %eax
	cmovbl	%eax, %ecx
	movl	60(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r11d
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	80(%rsp), %eax          # 4-byte Reload
	jmp	.LBB1_31
.LBB1_119:
	movl	$1, 32(%rsp)
	movl	$255, %eax
.LBB1_120:                              # %get_byte.exit46.i388
	orl	%esi, %eax
	movl	%eax, 28(%rsp)
	shll	$8, %r11d
	movl	%r11d, 24(%rsp)
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_30:                               # %.critedge
	xorl	%ecx, %ecx
	cmpl	%edx, %r14d
	setae	%cl
	movl	$1, %edi
	movl	%r10d, %eax
.LBB1_31:
	andb	$7, %cl
	cmpb	$7, %cl
	je	.LBB1_33
# BB#32:
	testb	%cl, %cl
	jne	.LBB1_291
.LBB1_33:                               # %.backedge
	cmpl	$0, 32(%rsp)
	jne	.LBB1_287
# BB#121:                               # %.backedge._crit_edge
	movl	%r14d, %edx
	andl	108(%rsp), %edx         # 4-byte Folded Reload
	movl	48(%rsp), %esi
	movl	%r9d, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	movl	60(%rsp), %r13d         # 4-byte Reload
	movl	%r11d, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	88(%rsp), %r11d         # 4-byte Reload
	testl	%esi, %esi
	jne	.LBB1_122
.LBB1_133:
	movl	$1, 32(%rsp)
.LBB1_269:
	movl	$1, 32(%rsp)
.LBB1_270:                              # %getbit_from_table.exit369
	xorl	%eax, %eax
.LBB1_271:                              # %getbit_from_table.exit369
	testb	%al, %al
	movl	%r11d, %eax
	cmovnel	%ebx, %eax
	movl	%ebx, %ecx
	cmovnel	%r11d, %ecx
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	%r13d, %ebx
	movl	%eax, %r13d
.LBB1_272:                              # %getbit_from_table.exit376
	movq	144(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	get_n_bits_from_tablesize
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	xorl	%ecx, %ecx
	cmpl	$6, 72(%rsp)            # 4-byte Folded Reload
	setg	%cl
	leal	8(%rcx,%rcx,2), %r9d
	movl	%r13d, %r11d
	movl	%ebx, %r10d
.LBB1_273:                              # %getbit_from_table.exit376
	movl	240(%rsp), %edx
	testl	%r11d, %r11d
	je	.LBB1_291
.LBB1_274:
	movl	$1, %r13d
	movl	%r14d, %edi
	subl	%r11d, %edi
	jae	.LBB1_275
	jmp	.LBB1_288
.LBB1_286:                              # %get_byte.exit.4.thread
	movl	$1, 32(%rsp)
	orl	$255, %ebp
	movl	%ebp, 28(%rsp)
.LBB1_287:                              # %.thread
	movl	$1, %r13d
.LBB1_288:                              # %.thread
	movl	%r13d, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_289:                              # %.loopexit
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_288
.LBB1_291:
	xorl	%r13d, %r13d
	jmp	.LBB1_288
.Lfunc_end1:
	.size	very_real_unpack, .Lfunc_end1-very_real_unpack
	.cfi_endproc

	.globl	get_byte
	.p2align	4, 0x90
	.type	get_byte,@function
get_byte:                               # @get_byte
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	cmpq	8(%rdi), %rcx
	jae	.LBB2_1
# BB#2:
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, (%rdi)
	retq
.LBB2_1:
	movl	$1, 24(%rdi)
	movl	$255, %eax
	retq
.Lfunc_end2:
	.size	get_byte, .Lfunc_end2-get_byte
	.cfi_endproc

	.globl	getbit_from_table
	.p2align	4, 0x90
	.type	getbit_from_table,@function
getbit_from_table:                      # @getbit_from_table
	.cfi_startproc
# BB#0:
	movl	40(%rsi), %eax
	testq	%rax, %rax
	je	.LBB3_5
# BB#1:
	cmpl	$1, %eax
	je	.LBB3_5
# BB#2:
	movq	32(%rsi), %rcx
	cmpq	%rcx, %rdi
	jb	.LBB3_5
# BB#3:
	leaq	2(%rdi), %rdx
	cmpq	%rcx, %rdx
	jbe	.LBB3_5
# BB#4:
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	ja	.LBB3_5
# BB#6:
	movzwl	(%rdi), %eax
	movl	16(%rsi), %r9d
	movl	20(%rsi), %r8d
	movl	%r9d, %ecx
	shrl	$11, %ecx
	imull	%eax, %ecx
	movl	%r8d, %edx
	subl	%ecx, %edx
	jae	.LBB3_12
# BB#7:
	movl	%ecx, 16(%rsi)
	movl	$2048, %edx             # imm = 0x800
	subl	%eax, %edx
	shrl	$5, %edx
	addl	%eax, %edx
	movw	%dx, (%rdi)
	xorl	%eax, %eax
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB3_17
# BB#8:
	shll	$8, %r8d
	movq	(%rsi), %rdi
	cmpq	8(%rsi), %rdi
	jae	.LBB3_9
# BB#10:
	movzbl	(%rdi), %edx
	incq	%rdi
	movq	%rdi, (%rsi)
	jmp	.LBB3_11
.LBB3_5:
	movl	$1, 24(%rsi)
	movl	$255, %eax
	retq
.LBB3_12:
	subl	%ecx, %r9d
	movl	%r9d, 16(%rsi)
	movl	%edx, 20(%rsi)
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movw	%ax, (%rdi)
	movl	$1, %eax
	cmpl	$16777215, %r9d         # imm = 0xFFFFFF
	ja	.LBB3_17
# BB#13:
	shll	$8, %edx
	movq	(%rsi), %rdi
	cmpq	8(%rsi), %rdi
	jae	.LBB3_14
# BB#15:
	movzbl	(%rdi), %ecx
	incq	%rdi
	movq	%rdi, (%rsi)
	jmp	.LBB3_16
.LBB3_9:
	movl	$1, 24(%rsi)
	movl	$255, %edx
.LBB3_11:                               # %get_byte.exit
	orl	%r8d, %edx
	movl	%edx, 20(%rsi)
	shll	$8, %ecx
	movl	%ecx, 16(%rsi)
	retq
.LBB3_14:
	movl	$1, 24(%rsi)
	movl	$255, %ecx
.LBB3_16:                               # %get_byte.exit46
	orl	%edx, %ecx
	movl	%ecx, 20(%rsi)
	shll	$8, %r9d
	movl	%r9d, 16(%rsi)
.LBB3_17:
	retq
.Lfunc_end3:
	.size	getbit_from_table, .Lfunc_end3-getbit_from_table
	.cfi_endproc

	.globl	get_100_bits_from_tablesize
	.p2align	4, 0x90
	.type	get_100_bits_from_tablesize,@function
get_100_bits_from_tablesize:            # @get_100_bits_from_tablesize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 64
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	%r12b, %r13d
	movl	%r13d, %ebp
	shrl	$7, %ebp
	movl	%ebp, %eax
	shll	$8, %eax
	leal	256(%rbx,%rax), %eax
	leaq	(%r15,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	addl	%ebx, %ebx
	orl	%eax, %ebx
	cmpl	%eax, %ebp
	je	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	cmpl	$255, %ebx
	jbe	.LBB4_4
.LBB4_3:                                # %.backedge
                                        #   in Loop: Header=BB4_1 Depth=1
	andl	$127, %r13d
	andl	$-256, %r12d
	leal	(%r12,%r13,2), %r12d
	cmpl	$256, %ebx              # imm = 0x100
	jb	.LBB4_1
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	addl	%ebx, %ebx
	leaq	(%r15,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	orl	%eax, %ebx
	cmpl	$256, %ebx              # imm = 0x100
	jb	.LBB4_4
.LBB4_5:                                # %.backedge.thread
	movzbl	%bl, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_100_bits_from_tablesize, .Lfunc_end4-get_100_bits_from_tablesize
	.cfi_endproc

	.globl	get_100_bits_from_table
	.p2align	4, 0x90
	.type	get_100_bits_from_table,@function
get_100_bits_from_table:                # @get_100_bits_from_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movl	%eax, %ebp
	addl	%ebp, %ebp
	leaq	(%rbx,%rcx,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	orl	%ebp, %eax
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB5_1
# BB#2:
	movzbl	%al, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	get_100_bits_from_table, .Lfunc_end5-get_100_bits_from_table
	.cfi_endproc

	.globl	get_n_bits_from_tablesize
	.p2align	4, 0x90
	.type	get_n_bits_from_tablesize,@function
get_n_bits_from_tablesize:              # @get_n_bits_from_tablesize
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	getbit_from_table
	testl	%eax, %eax
	je	.LBB6_1
# BB#2:
	leaq	2(%rbx), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	testl	%eax, %eax
	je	.LBB6_3
# BB#4:                                 # %.lr.ph.i16
	leaq	518(%rbx), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	movl	%eax, %r15d
	leal	2(%r15), %eax
	leaq	516(%rbx,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	4(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r15,2), %r15d
	leaq	516(%rbx,%r15,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-240(%rax,%r15,2), %eax
	jmp	.LBB6_5
.LBB6_1:                                # %.lr.ph.i
	leal	2(,%r15,8), %eax
	leaq	(%rbx,%rax,2), %r15
	leaq	2(%rbx,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	movl	%eax, %ebx
	leal	2(%rbx), %eax
	leaq	(%r15,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	4(%rax,%rbx,2), %ebx
	leaq	(%r15,%rbx,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-8(%rax,%rbx,2), %eax
	jmp	.LBB6_5
.LBB6_3:                                # %.lr.ph.i12
	leal	130(,%r15,8), %eax
	leaq	(%rbx,%rax,2), %r15
	leaq	2(%rbx,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
	movl	%eax, %ebx
	leal	2(%rbx), %eax
	leaq	(%r15,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	4(%rax,%rbx,2), %ebx
	leaq	(%r15,%rbx,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rbx,2), %eax
.LBB6_5:                                # %get_n_bits_from_table.exit13
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	get_n_bits_from_tablesize, .Lfunc_end6-get_n_bits_from_tablesize
	.cfi_endproc

	.globl	get_n_bits_from_table
	.p2align	4, 0x90
	.type	get_n_bits_from_table,@function
get_n_bits_from_table:                  # @get_n_bits_from_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 64
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	$1, %r12d
	testl	%r14d, %r14d
	movl	$1, %ebx
	je	.LBB7_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	leaq	(%r13,%rax,2), %rdi
	movq	%r15, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	decl	%ebp
	leal	(%rax,%rbx,2), %ebx
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	movl	%r14d, %ecx
	shll	%cl, %r12d
	subl	%r12d, %ebx
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	get_n_bits_from_table, .Lfunc_end7-get_n_bits_from_table
	.cfi_endproc

	.globl	get_bb
	.p2align	4, 0x90
	.type	get_bb,@function
get_bb:                                 # @get_bb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r12d
	movq	%rdi, %r15
	testl	%r12d, %r12d
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	leaq	(%r15,%rax,2), %rdi
	movq	%r14, %rsi
	callq	getbit_from_table
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rbx,2), %ebx
	movl	%ebp, %ecx
	shll	%cl, %eax
	orl	%eax, %r13d
	incl	%ebp
	cmpl	%ebp, %r12d
	jne	.LBB8_3
	jmp	.LBB8_4
.LBB8_1:
	xorl	%r13d, %r13d
.LBB8_4:                                # %.loopexit
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	get_bb, .Lfunc_end8-get_bb
	.cfi_endproc

	.globl	get_bitmap
	.p2align	4, 0x90
	.type	get_bitmap,@function
get_bitmap:                             # @get_bitmap
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB9_1
# BB#2:                                 # %.preheader
	movl	16(%rdi), %r8d
	movl	20(%rdi), %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movl	%r8d, %r9d
	shrl	%r9d
	movl	%r9d, 16(%rdi)
	addl	%eax, %eax
	movl	%ecx, %edx
	subl	%r9d, %edx
	jae	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movl	%ecx, %edx
	jmp	.LBB9_6
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	movl	%edx, 20(%rdi)
	orl	$1, %eax
.LBB9_6:                                #   in Loop: Header=BB9_3 Depth=1
	decl	%esi
	cmpl	$33554431, %r8d         # imm = 0x1FFFFFF
	ja	.LBB9_11
# BB#7:                                 #   in Loop: Header=BB9_3 Depth=1
	shll	$8, %r9d
	movl	%r9d, 16(%rdi)
	shll	$8, %edx
	movq	(%rdi), %rcx
	cmpq	8(%rdi), %rcx
	jae	.LBB9_8
# BB#9:                                 #   in Loop: Header=BB9_3 Depth=1
	movzbl	(%rcx), %r8d
	incq	%rcx
	movq	%rcx, (%rdi)
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_3 Depth=1
	movl	$1, 24(%rdi)
	movl	$255, %r8d
.LBB9_10:                               # %get_byte.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	orl	%r8d, %edx
	movl	%edx, 20(%rdi)
.LBB9_11:                               # %.backedge
                                        #   in Loop: Header=BB9_3 Depth=1
	testl	%esi, %esi
	movl	%edx, %ecx
	movl	%r9d, %r8d
	jne	.LBB9_3
# BB#12:                                # %.loopexit
	retq
.LBB9_1:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	get_bitmap, .Lfunc_end9-get_bitmap
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unsp: table size = %d\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%x %x %x %x\n"
	.size	.L.str.1, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
