	.text
	.file	"mpistubs.bc"
	.globl	hypre_MPI_Init
	.p2align	4, 0x90
	.type	hypre_MPI_Init,@function
hypre_MPI_Init:                         # @hypre_MPI_Init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	hypre_MPI_Init, .Lfunc_end0-hypre_MPI_Init
	.cfi_endproc

	.globl	hypre_MPI_Finalize
	.p2align	4, 0x90
	.type	hypre_MPI_Finalize,@function
hypre_MPI_Finalize:                     # @hypre_MPI_Finalize
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	hypre_MPI_Finalize, .Lfunc_end1-hypre_MPI_Finalize
	.cfi_endproc

	.globl	hypre_MPI_Abort
	.p2align	4, 0x90
	.type	hypre_MPI_Abort,@function
hypre_MPI_Abort:                        # @hypre_MPI_Abort
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	hypre_MPI_Abort, .Lfunc_end2-hypre_MPI_Abort
	.cfi_endproc

	.globl	hypre_MPI_Wtime
	.p2align	4, 0x90
	.type	hypre_MPI_Wtime,@function
hypre_MPI_Wtime:                        # @hypre_MPI_Wtime
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end3:
	.size	hypre_MPI_Wtime, .Lfunc_end3-hypre_MPI_Wtime
	.cfi_endproc

	.globl	hypre_MPI_Wtick
	.p2align	4, 0x90
	.type	hypre_MPI_Wtick,@function
hypre_MPI_Wtick:                        # @hypre_MPI_Wtick
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end4:
	.size	hypre_MPI_Wtick, .Lfunc_end4-hypre_MPI_Wtick
	.cfi_endproc

	.globl	hypre_MPI_Barrier
	.p2align	4, 0x90
	.type	hypre_MPI_Barrier,@function
hypre_MPI_Barrier:                      # @hypre_MPI_Barrier
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	hypre_MPI_Barrier, .Lfunc_end5-hypre_MPI_Barrier
	.cfi_endproc

	.globl	hypre_MPI_Comm_create
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_create,@function
hypre_MPI_Comm_create:                  # @hypre_MPI_Comm_create
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	hypre_MPI_Comm_create, .Lfunc_end6-hypre_MPI_Comm_create
	.cfi_endproc

	.globl	hypre_MPI_Comm_dup
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_dup,@function
hypre_MPI_Comm_dup:                     # @hypre_MPI_Comm_dup
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	hypre_MPI_Comm_dup, .Lfunc_end7-hypre_MPI_Comm_dup
	.cfi_endproc

	.globl	hypre_MPI_Comm_size
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_size,@function
hypre_MPI_Comm_size:                    # @hypre_MPI_Comm_size
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	hypre_MPI_Comm_size, .Lfunc_end8-hypre_MPI_Comm_size
	.cfi_endproc

	.globl	hypre_MPI_Comm_rank
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_rank,@function
hypre_MPI_Comm_rank:                    # @hypre_MPI_Comm_rank
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	hypre_MPI_Comm_rank, .Lfunc_end9-hypre_MPI_Comm_rank
	.cfi_endproc

	.globl	hypre_MPI_Comm_free
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_free,@function
hypre_MPI_Comm_free:                    # @hypre_MPI_Comm_free
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_MPI_Comm_free, .Lfunc_end10-hypre_MPI_Comm_free
	.cfi_endproc

	.globl	hypre_MPI_Comm_group
	.p2align	4, 0x90
	.type	hypre_MPI_Comm_group,@function
hypre_MPI_Comm_group:                   # @hypre_MPI_Comm_group
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	hypre_MPI_Comm_group, .Lfunc_end11-hypre_MPI_Comm_group
	.cfi_endproc

	.globl	hypre_MPI_Group_incl
	.p2align	4, 0x90
	.type	hypre_MPI_Group_incl,@function
hypre_MPI_Group_incl:                   # @hypre_MPI_Group_incl
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	hypre_MPI_Group_incl, .Lfunc_end12-hypre_MPI_Group_incl
	.cfi_endproc

	.globl	hypre_MPI_Group_free
	.p2align	4, 0x90
	.type	hypre_MPI_Group_free,@function
hypre_MPI_Group_free:                   # @hypre_MPI_Group_free
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	hypre_MPI_Group_free, .Lfunc_end13-hypre_MPI_Group_free
	.cfi_endproc

	.globl	hypre_MPI_Address
	.p2align	4, 0x90
	.type	hypre_MPI_Address,@function
hypre_MPI_Address:                      # @hypre_MPI_Address
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	hypre_MPI_Address, .Lfunc_end14-hypre_MPI_Address
	.cfi_endproc

	.globl	hypre_MPI_Get_count
	.p2align	4, 0x90
	.type	hypre_MPI_Get_count,@function
hypre_MPI_Get_count:                    # @hypre_MPI_Get_count
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	hypre_MPI_Get_count, .Lfunc_end15-hypre_MPI_Get_count
	.cfi_endproc

	.globl	hypre_MPI_Alltoall
	.p2align	4, 0x90
	.type	hypre_MPI_Alltoall,@function
hypre_MPI_Alltoall:                     # @hypre_MPI_Alltoall
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	hypre_MPI_Alltoall, .Lfunc_end16-hypre_MPI_Alltoall
	.cfi_endproc

	.globl	hypre_MPI_Allgather
	.p2align	4, 0x90
	.type	hypre_MPI_Allgather,@function
hypre_MPI_Allgather:                    # @hypre_MPI_Allgather
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edx, %edx
	je	.LBB17_35
# BB#1:
	cmpl	$1, %edx
	je	.LBB17_18
# BB#2:
	cmpl	$2, %edx
	jne	.LBB17_52
# BB#3:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB17_52
# BB#4:                                 # %.lr.ph46.preheader
	movl	%esi, %r8d
	cmpl	$31, %esi
	jbe	.LBB17_5
# BB#12:                                # %min.iters.checked
	andl	$31, %esi
	movq	%r8, %r11
	subq	%rsi, %r11
	je	.LBB17_5
# BB#13:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB17_15
# BB#14:                                # %vector.memcheck
	leaq	(%rcx,%r8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB17_15
.LBB17_5:
	xorl	%r11d, %r11d
.LBB17_6:                               # %.lr.ph46.preheader121
	movl	%r8d, %eax
	subl	%r11d, %eax
	leaq	-1(%r8), %rsi
	subq	%r11, %rsi
	andq	$7, %rax
	je	.LBB17_9
# BB#7:                                 # %.lr.ph46.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB17_8:                               # %.lr.ph46.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%r11), %edx
	movb	%dl, (%rcx,%r11)
	incq	%r11
	incq	%rax
	jne	.LBB17_8
.LBB17_9:                               # %.lr.ph46.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB17_52
# BB#10:                                # %.lr.ph46.preheader121.new
	subq	%r11, %r8
	leaq	7(%rcx,%r11), %rcx
	leaq	7(%rdi,%r11), %rdx
	.p2align	4, 0x90
.LBB17_11:                              # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rdx), %eax
	movb	%al, -7(%rcx)
	movzbl	-6(%rdx), %eax
	movb	%al, -6(%rcx)
	movzbl	-5(%rdx), %eax
	movb	%al, -5(%rcx)
	movzbl	-4(%rdx), %eax
	movb	%al, -4(%rcx)
	movzbl	-3(%rdx), %eax
	movb	%al, -3(%rcx)
	movzbl	-2(%rdx), %eax
	movb	%al, -2(%rcx)
	movzbl	-1(%rdx), %eax
	movb	%al, -1(%rcx)
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-8, %r8
	jne	.LBB17_11
	jmp	.LBB17_52
.LBB17_35:
	testl	%esi, %esi
	jle	.LBB17_52
# BB#36:                                # %.lr.ph44.preheader
	movl	%esi, %r8d
	cmpl	$4, %esi
	jae	.LBB17_38
# BB#37:
	xorl	%edx, %edx
	jmp	.LBB17_46
.LBB17_18:
	testl	%esi, %esi
	jle	.LBB17_52
# BB#19:                                # %.lr.ph.preheader
	movl	%esi, %r8d
	cmpl	$8, %esi
	jae	.LBB17_21
# BB#20:
	xorl	%edx, %edx
	jmp	.LBB17_29
.LBB17_38:                              # %min.iters.checked68
	andl	$3, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB17_39
# BB#40:                                # %vector.memcheck79
	leaq	(%rdi,%r8,8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB17_43
# BB#41:                                # %vector.memcheck79
	leaq	(%rcx,%r8,8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB17_43
# BB#42:
	xorl	%edx, %edx
	jmp	.LBB17_46
.LBB17_21:                              # %min.iters.checked95
	andl	$7, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB17_22
# BB#23:                                # %vector.memcheck106
	leaq	(%rdi,%r8,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB17_26
# BB#24:                                # %vector.memcheck106
	leaq	(%rcx,%r8,4), %rax
	cmpq	%rdi, %rax
	jbe	.LBB17_26
# BB#25:
	xorl	%edx, %edx
	jmp	.LBB17_29
.LBB17_39:
	xorl	%edx, %edx
	jmp	.LBB17_46
.LBB17_22:
	xorl	%edx, %edx
	jmp	.LBB17_29
.LBB17_15:                              # %vector.body.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%r11, %r10
	.p2align	4, 0x90
.LBB17_16:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-32, %r10
	jne	.LBB17_16
# BB#17:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB17_6
	jmp	.LBB17_52
.LBB17_43:                              # %vector.body64.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB17_44:                              # %vector.body64
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-4, %r10
	jne	.LBB17_44
# BB#45:                                # %middle.block65
	testl	%esi, %esi
	je	.LBB17_52
.LBB17_46:                              # %.lr.ph44.preheader119
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB17_49
# BB#47:                                # %.lr.ph44.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB17_48:                              # %.lr.ph44.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	incq	%rax
	jne	.LBB17_48
.LBB17_49:                              # %.lr.ph44.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB17_52
# BB#50:                                # %.lr.ph44.preheader119.new
	subq	%rdx, %r8
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	56(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB17_51:                              # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rax
	movq	%rax, -56(%rcx)
	movq	-48(%rdx), %rax
	movq	%rax, -48(%rcx)
	movq	-40(%rdx), %rax
	movq	%rax, -40(%rcx)
	movq	-32(%rdx), %rax
	movq	%rax, -32(%rcx)
	movq	-24(%rdx), %rax
	movq	%rax, -24(%rcx)
	movq	-16(%rdx), %rax
	movq	%rax, -16(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$-8, %r8
	jne	.LBB17_51
	jmp	.LBB17_52
.LBB17_26:                              # %vector.body91.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB17_27:                              # %vector.body91
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-8, %r10
	jne	.LBB17_27
# BB#28:                                # %middle.block92
	testl	%esi, %esi
	je	.LBB17_52
.LBB17_29:                              # %.lr.ph.preheader118
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB17_32
# BB#30:                                # %.lr.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB17_31:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB17_31
.LBB17_32:                              # %.lr.ph.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB17_52
# BB#33:                                # %.lr.ph.preheader118.new
	subq	%rdx, %r8
	leaq	28(%rcx,%rdx,4), %rcx
	leaq	28(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB17_34:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %eax
	movl	%eax, -28(%rcx)
	movl	-24(%rdx), %eax
	movl	%eax, -24(%rcx)
	movl	-20(%rdx), %eax
	movl	%eax, -20(%rcx)
	movl	-16(%rdx), %eax
	movl	%eax, -16(%rcx)
	movl	-12(%rdx), %eax
	movl	%eax, -12(%rcx)
	movl	-8(%rdx), %eax
	movl	%eax, -8(%rcx)
	movl	-4(%rdx), %eax
	movl	%eax, -4(%rcx)
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r8
	jne	.LBB17_34
.LBB17_52:                              # %.loopexit
	xorl	%eax, %eax
	retq
.Lfunc_end17:
	.size	hypre_MPI_Allgather, .Lfunc_end17-hypre_MPI_Allgather
	.cfi_endproc

	.globl	hypre_MPI_Allgatherv
	.p2align	4, 0x90
	.type	hypre_MPI_Allgatherv,@function
hypre_MPI_Allgatherv:                   # @hypre_MPI_Allgatherv
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edx, %edx
	je	.LBB18_35
# BB#1:
	cmpl	$1, %edx
	je	.LBB18_18
# BB#2:
	cmpl	$2, %edx
	jne	.LBB18_52
# BB#3:                                 # %.preheader.i
	testl	%esi, %esi
	jle	.LBB18_52
# BB#4:                                 # %.lr.ph46.preheader.i
	movl	%esi, %r8d
	cmpl	$31, %esi
	jbe	.LBB18_5
# BB#12:                                # %min.iters.checked
	andl	$31, %esi
	movq	%r8, %r11
	subq	%rsi, %r11
	je	.LBB18_5
# BB#13:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB18_15
# BB#14:                                # %vector.memcheck
	leaq	(%rcx,%r8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB18_15
.LBB18_5:
	xorl	%r11d, %r11d
.LBB18_6:                               # %.lr.ph46.i.preheader
	movl	%r8d, %eax
	subl	%r11d, %eax
	leaq	-1(%r8), %rsi
	subq	%r11, %rsi
	andq	$7, %rax
	je	.LBB18_9
# BB#7:                                 # %.lr.ph46.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB18_8:                               # %.lr.ph46.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%r11), %edx
	movb	%dl, (%rcx,%r11)
	incq	%r11
	incq	%rax
	jne	.LBB18_8
.LBB18_9:                               # %.lr.ph46.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB18_52
# BB#10:                                # %.lr.ph46.i.preheader.new
	subq	%r11, %r8
	leaq	7(%rcx,%r11), %rcx
	leaq	7(%rdi,%r11), %rdx
	.p2align	4, 0x90
.LBB18_11:                              # %.lr.ph46.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rdx), %eax
	movb	%al, -7(%rcx)
	movzbl	-6(%rdx), %eax
	movb	%al, -6(%rcx)
	movzbl	-5(%rdx), %eax
	movb	%al, -5(%rcx)
	movzbl	-4(%rdx), %eax
	movb	%al, -4(%rcx)
	movzbl	-3(%rdx), %eax
	movb	%al, -3(%rcx)
	movzbl	-2(%rdx), %eax
	movb	%al, -2(%rcx)
	movzbl	-1(%rdx), %eax
	movb	%al, -1(%rcx)
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-8, %r8
	jne	.LBB18_11
	jmp	.LBB18_52
.LBB18_35:
	testl	%esi, %esi
	jle	.LBB18_52
# BB#36:                                # %.lr.ph44.preheader.i
	movl	%esi, %r8d
	cmpl	$4, %esi
	jae	.LBB18_38
# BB#37:
	xorl	%edx, %edx
	jmp	.LBB18_46
.LBB18_18:
	testl	%esi, %esi
	jle	.LBB18_52
# BB#19:                                # %.lr.ph.preheader.i
	movl	%esi, %r8d
	cmpl	$8, %esi
	jae	.LBB18_21
# BB#20:
	xorl	%edx, %edx
	jmp	.LBB18_29
.LBB18_38:                              # %min.iters.checked21
	andl	$3, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB18_39
# BB#40:                                # %vector.memcheck32
	leaq	(%rdi,%r8,8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB18_43
# BB#41:                                # %vector.memcheck32
	leaq	(%rcx,%r8,8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB18_43
# BB#42:
	xorl	%edx, %edx
	jmp	.LBB18_46
.LBB18_21:                              # %min.iters.checked48
	andl	$7, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB18_22
# BB#23:                                # %vector.memcheck59
	leaq	(%rdi,%r8,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB18_26
# BB#24:                                # %vector.memcheck59
	leaq	(%rcx,%r8,4), %rax
	cmpq	%rdi, %rax
	jbe	.LBB18_26
# BB#25:
	xorl	%edx, %edx
	jmp	.LBB18_29
.LBB18_39:
	xorl	%edx, %edx
	jmp	.LBB18_46
.LBB18_22:
	xorl	%edx, %edx
	jmp	.LBB18_29
.LBB18_15:                              # %vector.body.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%r11, %r10
	.p2align	4, 0x90
.LBB18_16:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-32, %r10
	jne	.LBB18_16
# BB#17:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB18_6
	jmp	.LBB18_52
.LBB18_43:                              # %vector.body17.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB18_44:                              # %vector.body17
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-4, %r10
	jne	.LBB18_44
# BB#45:                                # %middle.block18
	testl	%esi, %esi
	je	.LBB18_52
.LBB18_46:                              # %.lr.ph44.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB18_49
# BB#47:                                # %.lr.ph44.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB18_48:                              # %.lr.ph44.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	incq	%rax
	jne	.LBB18_48
.LBB18_49:                              # %.lr.ph44.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB18_52
# BB#50:                                # %.lr.ph44.i.preheader.new
	subq	%rdx, %r8
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	56(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB18_51:                              # %.lr.ph44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rax
	movq	%rax, -56(%rcx)
	movq	-48(%rdx), %rax
	movq	%rax, -48(%rcx)
	movq	-40(%rdx), %rax
	movq	%rax, -40(%rcx)
	movq	-32(%rdx), %rax
	movq	%rax, -32(%rcx)
	movq	-24(%rdx), %rax
	movq	%rax, -24(%rcx)
	movq	-16(%rdx), %rax
	movq	%rax, -16(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$-8, %r8
	jne	.LBB18_51
	jmp	.LBB18_52
.LBB18_26:                              # %vector.body44.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB18_27:                              # %vector.body44
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-8, %r10
	jne	.LBB18_27
# BB#28:                                # %middle.block45
	testl	%esi, %esi
	je	.LBB18_52
.LBB18_29:                              # %.lr.ph.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB18_32
# BB#30:                                # %.lr.ph.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB18_31:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB18_31
.LBB18_32:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB18_52
# BB#33:                                # %.lr.ph.i.preheader.new
	subq	%rdx, %r8
	leaq	28(%rcx,%rdx,4), %rcx
	leaq	28(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB18_34:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %eax
	movl	%eax, -28(%rcx)
	movl	-24(%rdx), %eax
	movl	%eax, -24(%rcx)
	movl	-20(%rdx), %eax
	movl	%eax, -20(%rcx)
	movl	-16(%rdx), %eax
	movl	%eax, -16(%rcx)
	movl	-12(%rdx), %eax
	movl	%eax, -12(%rcx)
	movl	-8(%rdx), %eax
	movl	%eax, -8(%rcx)
	movl	-4(%rdx), %eax
	movl	%eax, -4(%rcx)
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r8
	jne	.LBB18_34
.LBB18_52:                              # %hypre_MPI_Allgather.exit
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	hypre_MPI_Allgatherv, .Lfunc_end18-hypre_MPI_Allgatherv
	.cfi_endproc

	.globl	hypre_MPI_Gather
	.p2align	4, 0x90
	.type	hypre_MPI_Gather,@function
hypre_MPI_Gather:                       # @hypre_MPI_Gather
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edx, %edx
	je	.LBB19_35
# BB#1:
	cmpl	$1, %edx
	je	.LBB19_18
# BB#2:
	cmpl	$2, %edx
	jne	.LBB19_52
# BB#3:                                 # %.preheader.i
	testl	%esi, %esi
	jle	.LBB19_52
# BB#4:                                 # %.lr.ph46.preheader.i
	movl	%esi, %r8d
	cmpl	$31, %esi
	jbe	.LBB19_5
# BB#12:                                # %min.iters.checked
	andl	$31, %esi
	movq	%r8, %r11
	subq	%rsi, %r11
	je	.LBB19_5
# BB#13:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB19_15
# BB#14:                                # %vector.memcheck
	leaq	(%rcx,%r8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB19_15
.LBB19_5:
	xorl	%r11d, %r11d
.LBB19_6:                               # %.lr.ph46.i.preheader
	movl	%r8d, %eax
	subl	%r11d, %eax
	leaq	-1(%r8), %rsi
	subq	%r11, %rsi
	andq	$7, %rax
	je	.LBB19_9
# BB#7:                                 # %.lr.ph46.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB19_8:                               # %.lr.ph46.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%r11), %edx
	movb	%dl, (%rcx,%r11)
	incq	%r11
	incq	%rax
	jne	.LBB19_8
.LBB19_9:                               # %.lr.ph46.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB19_52
# BB#10:                                # %.lr.ph46.i.preheader.new
	subq	%r11, %r8
	leaq	7(%rcx,%r11), %rcx
	leaq	7(%rdi,%r11), %rdx
	.p2align	4, 0x90
.LBB19_11:                              # %.lr.ph46.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rdx), %eax
	movb	%al, -7(%rcx)
	movzbl	-6(%rdx), %eax
	movb	%al, -6(%rcx)
	movzbl	-5(%rdx), %eax
	movb	%al, -5(%rcx)
	movzbl	-4(%rdx), %eax
	movb	%al, -4(%rcx)
	movzbl	-3(%rdx), %eax
	movb	%al, -3(%rcx)
	movzbl	-2(%rdx), %eax
	movb	%al, -2(%rcx)
	movzbl	-1(%rdx), %eax
	movb	%al, -1(%rcx)
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-8, %r8
	jne	.LBB19_11
	jmp	.LBB19_52
.LBB19_35:
	testl	%esi, %esi
	jle	.LBB19_52
# BB#36:                                # %.lr.ph44.preheader.i
	movl	%esi, %r8d
	cmpl	$4, %esi
	jae	.LBB19_38
# BB#37:
	xorl	%edx, %edx
	jmp	.LBB19_46
.LBB19_18:
	testl	%esi, %esi
	jle	.LBB19_52
# BB#19:                                # %.lr.ph.preheader.i
	movl	%esi, %r8d
	cmpl	$8, %esi
	jae	.LBB19_21
# BB#20:
	xorl	%edx, %edx
	jmp	.LBB19_29
.LBB19_38:                              # %min.iters.checked21
	andl	$3, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB19_39
# BB#40:                                # %vector.memcheck32
	leaq	(%rdi,%r8,8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB19_43
# BB#41:                                # %vector.memcheck32
	leaq	(%rcx,%r8,8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB19_43
# BB#42:
	xorl	%edx, %edx
	jmp	.LBB19_46
.LBB19_21:                              # %min.iters.checked48
	andl	$7, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB19_22
# BB#23:                                # %vector.memcheck59
	leaq	(%rdi,%r8,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB19_26
# BB#24:                                # %vector.memcheck59
	leaq	(%rcx,%r8,4), %rax
	cmpq	%rdi, %rax
	jbe	.LBB19_26
# BB#25:
	xorl	%edx, %edx
	jmp	.LBB19_29
.LBB19_39:
	xorl	%edx, %edx
	jmp	.LBB19_46
.LBB19_22:
	xorl	%edx, %edx
	jmp	.LBB19_29
.LBB19_15:                              # %vector.body.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%r11, %r10
	.p2align	4, 0x90
.LBB19_16:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-32, %r10
	jne	.LBB19_16
# BB#17:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB19_6
	jmp	.LBB19_52
.LBB19_43:                              # %vector.body17.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB19_44:                              # %vector.body17
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-4, %r10
	jne	.LBB19_44
# BB#45:                                # %middle.block18
	testl	%esi, %esi
	je	.LBB19_52
.LBB19_46:                              # %.lr.ph44.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB19_49
# BB#47:                                # %.lr.ph44.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB19_48:                              # %.lr.ph44.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	incq	%rax
	jne	.LBB19_48
.LBB19_49:                              # %.lr.ph44.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB19_52
# BB#50:                                # %.lr.ph44.i.preheader.new
	subq	%rdx, %r8
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	56(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB19_51:                              # %.lr.ph44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rax
	movq	%rax, -56(%rcx)
	movq	-48(%rdx), %rax
	movq	%rax, -48(%rcx)
	movq	-40(%rdx), %rax
	movq	%rax, -40(%rcx)
	movq	-32(%rdx), %rax
	movq	%rax, -32(%rcx)
	movq	-24(%rdx), %rax
	movq	%rax, -24(%rcx)
	movq	-16(%rdx), %rax
	movq	%rax, -16(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$-8, %r8
	jne	.LBB19_51
	jmp	.LBB19_52
.LBB19_26:                              # %vector.body44.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB19_27:                              # %vector.body44
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-8, %r10
	jne	.LBB19_27
# BB#28:                                # %middle.block45
	testl	%esi, %esi
	je	.LBB19_52
.LBB19_29:                              # %.lr.ph.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB19_32
# BB#30:                                # %.lr.ph.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB19_31:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB19_31
.LBB19_32:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB19_52
# BB#33:                                # %.lr.ph.i.preheader.new
	subq	%rdx, %r8
	leaq	28(%rcx,%rdx,4), %rcx
	leaq	28(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB19_34:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %eax
	movl	%eax, -28(%rcx)
	movl	-24(%rdx), %eax
	movl	%eax, -24(%rcx)
	movl	-20(%rdx), %eax
	movl	%eax, -20(%rcx)
	movl	-16(%rdx), %eax
	movl	%eax, -16(%rcx)
	movl	-12(%rdx), %eax
	movl	%eax, -12(%rcx)
	movl	-8(%rdx), %eax
	movl	%eax, -8(%rcx)
	movl	-4(%rdx), %eax
	movl	%eax, -4(%rcx)
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r8
	jne	.LBB19_34
.LBB19_52:                              # %hypre_MPI_Allgather.exit
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	hypre_MPI_Gather, .Lfunc_end19-hypre_MPI_Gather
	.cfi_endproc

	.globl	hypre_MPI_Scatter
	.p2align	4, 0x90
	.type	hypre_MPI_Scatter,@function
hypre_MPI_Scatter:                      # @hypre_MPI_Scatter
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edx, %edx
	je	.LBB20_35
# BB#1:
	cmpl	$1, %edx
	je	.LBB20_18
# BB#2:
	cmpl	$2, %edx
	jne	.LBB20_52
# BB#3:                                 # %.preheader.i
	testl	%esi, %esi
	jle	.LBB20_52
# BB#4:                                 # %.lr.ph46.preheader.i
	movl	%esi, %r8d
	cmpl	$31, %esi
	jbe	.LBB20_5
# BB#12:                                # %min.iters.checked
	andl	$31, %esi
	movq	%r8, %r11
	subq	%rsi, %r11
	je	.LBB20_5
# BB#13:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB20_15
# BB#14:                                # %vector.memcheck
	leaq	(%rcx,%r8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB20_15
.LBB20_5:
	xorl	%r11d, %r11d
.LBB20_6:                               # %.lr.ph46.i.preheader
	movl	%r8d, %eax
	subl	%r11d, %eax
	leaq	-1(%r8), %rsi
	subq	%r11, %rsi
	andq	$7, %rax
	je	.LBB20_9
# BB#7:                                 # %.lr.ph46.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB20_8:                               # %.lr.ph46.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%r11), %edx
	movb	%dl, (%rcx,%r11)
	incq	%r11
	incq	%rax
	jne	.LBB20_8
.LBB20_9:                               # %.lr.ph46.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB20_52
# BB#10:                                # %.lr.ph46.i.preheader.new
	subq	%r11, %r8
	leaq	7(%rcx,%r11), %rcx
	leaq	7(%rdi,%r11), %rdx
	.p2align	4, 0x90
.LBB20_11:                              # %.lr.ph46.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rdx), %eax
	movb	%al, -7(%rcx)
	movzbl	-6(%rdx), %eax
	movb	%al, -6(%rcx)
	movzbl	-5(%rdx), %eax
	movb	%al, -5(%rcx)
	movzbl	-4(%rdx), %eax
	movb	%al, -4(%rcx)
	movzbl	-3(%rdx), %eax
	movb	%al, -3(%rcx)
	movzbl	-2(%rdx), %eax
	movb	%al, -2(%rcx)
	movzbl	-1(%rdx), %eax
	movb	%al, -1(%rcx)
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-8, %r8
	jne	.LBB20_11
	jmp	.LBB20_52
.LBB20_35:
	testl	%esi, %esi
	jle	.LBB20_52
# BB#36:                                # %.lr.ph44.preheader.i
	movl	%esi, %r8d
	cmpl	$4, %esi
	jae	.LBB20_38
# BB#37:
	xorl	%edx, %edx
	jmp	.LBB20_46
.LBB20_18:
	testl	%esi, %esi
	jle	.LBB20_52
# BB#19:                                # %.lr.ph.preheader.i
	movl	%esi, %r8d
	cmpl	$8, %esi
	jae	.LBB20_21
# BB#20:
	xorl	%edx, %edx
	jmp	.LBB20_29
.LBB20_38:                              # %min.iters.checked21
	andl	$3, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB20_39
# BB#40:                                # %vector.memcheck32
	leaq	(%rdi,%r8,8), %rax
	cmpq	%rcx, %rax
	jbe	.LBB20_43
# BB#41:                                # %vector.memcheck32
	leaq	(%rcx,%r8,8), %rax
	cmpq	%rdi, %rax
	jbe	.LBB20_43
# BB#42:
	xorl	%edx, %edx
	jmp	.LBB20_46
.LBB20_21:                              # %min.iters.checked48
	andl	$7, %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	je	.LBB20_22
# BB#23:                                # %vector.memcheck59
	leaq	(%rdi,%r8,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB20_26
# BB#24:                                # %vector.memcheck59
	leaq	(%rcx,%r8,4), %rax
	cmpq	%rdi, %rax
	jbe	.LBB20_26
# BB#25:
	xorl	%edx, %edx
	jmp	.LBB20_29
.LBB20_39:
	xorl	%edx, %edx
	jmp	.LBB20_46
.LBB20_22:
	xorl	%edx, %edx
	jmp	.LBB20_29
.LBB20_15:                              # %vector.body.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%r11, %r10
	.p2align	4, 0x90
.LBB20_16:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-32, %r10
	jne	.LBB20_16
# BB#17:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB20_6
	jmp	.LBB20_52
.LBB20_43:                              # %vector.body17.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB20_44:                              # %vector.body17
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-4, %r10
	jne	.LBB20_44
# BB#45:                                # %middle.block18
	testl	%esi, %esi
	je	.LBB20_52
.LBB20_46:                              # %.lr.ph44.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB20_49
# BB#47:                                # %.lr.ph44.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB20_48:                              # %.lr.ph44.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	incq	%rax
	jne	.LBB20_48
.LBB20_49:                              # %.lr.ph44.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB20_52
# BB#50:                                # %.lr.ph44.i.preheader.new
	subq	%rdx, %r8
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	56(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB20_51:                              # %.lr.ph44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rax
	movq	%rax, -56(%rcx)
	movq	-48(%rdx), %rax
	movq	%rax, -48(%rcx)
	movq	-40(%rdx), %rax
	movq	%rax, -40(%rcx)
	movq	-32(%rdx), %rax
	movq	%rax, -32(%rcx)
	movq	-24(%rdx), %rax
	movq	%rax, -24(%rcx)
	movq	-16(%rdx), %rax
	movq	%rax, -16(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$-8, %r8
	jne	.LBB20_51
	jmp	.LBB20_52
.LBB20_26:                              # %vector.body44.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rcx), %rax
	movq	%rdx, %r10
	.p2align	4, 0x90
.LBB20_27:                              # %vector.body44
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-8, %r10
	jne	.LBB20_27
# BB#28:                                # %middle.block45
	testl	%esi, %esi
	je	.LBB20_52
.LBB20_29:                              # %.lr.ph.i.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%r8), %r9
	subq	%rdx, %r9
	andq	$7, %rax
	je	.LBB20_32
# BB#30:                                # %.lr.ph.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB20_31:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB20_31
.LBB20_32:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB20_52
# BB#33:                                # %.lr.ph.i.preheader.new
	subq	%rdx, %r8
	leaq	28(%rcx,%rdx,4), %rcx
	leaq	28(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB20_34:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %eax
	movl	%eax, -28(%rcx)
	movl	-24(%rdx), %eax
	movl	%eax, -24(%rcx)
	movl	-20(%rdx), %eax
	movl	%eax, -20(%rcx)
	movl	-16(%rdx), %eax
	movl	%eax, -16(%rcx)
	movl	-12(%rdx), %eax
	movl	%eax, -12(%rcx)
	movl	-8(%rdx), %eax
	movl	%eax, -8(%rcx)
	movl	-4(%rdx), %eax
	movl	%eax, -4(%rcx)
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r8
	jne	.LBB20_34
.LBB20_52:                              # %hypre_MPI_Allgather.exit
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	hypre_MPI_Scatter, .Lfunc_end20-hypre_MPI_Scatter
	.cfi_endproc

	.globl	hypre_MPI_Bcast
	.p2align	4, 0x90
	.type	hypre_MPI_Bcast,@function
hypre_MPI_Bcast:                        # @hypre_MPI_Bcast
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end21:
	.size	hypre_MPI_Bcast, .Lfunc_end21-hypre_MPI_Bcast
	.cfi_endproc

	.globl	hypre_MPI_Send
	.p2align	4, 0x90
	.type	hypre_MPI_Send,@function
hypre_MPI_Send:                         # @hypre_MPI_Send
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	hypre_MPI_Send, .Lfunc_end22-hypre_MPI_Send
	.cfi_endproc

	.globl	hypre_MPI_Recv
	.p2align	4, 0x90
	.type	hypre_MPI_Recv,@function
hypre_MPI_Recv:                         # @hypre_MPI_Recv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	hypre_MPI_Recv, .Lfunc_end23-hypre_MPI_Recv
	.cfi_endproc

	.globl	hypre_MPI_Isend
	.p2align	4, 0x90
	.type	hypre_MPI_Isend,@function
hypre_MPI_Isend:                        # @hypre_MPI_Isend
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end24:
	.size	hypre_MPI_Isend, .Lfunc_end24-hypre_MPI_Isend
	.cfi_endproc

	.globl	hypre_MPI_Irecv
	.p2align	4, 0x90
	.type	hypre_MPI_Irecv,@function
hypre_MPI_Irecv:                        # @hypre_MPI_Irecv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end25:
	.size	hypre_MPI_Irecv, .Lfunc_end25-hypre_MPI_Irecv
	.cfi_endproc

	.globl	hypre_MPI_Send_init
	.p2align	4, 0x90
	.type	hypre_MPI_Send_init,@function
hypre_MPI_Send_init:                    # @hypre_MPI_Send_init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end26:
	.size	hypre_MPI_Send_init, .Lfunc_end26-hypre_MPI_Send_init
	.cfi_endproc

	.globl	hypre_MPI_Recv_init
	.p2align	4, 0x90
	.type	hypre_MPI_Recv_init,@function
hypre_MPI_Recv_init:                    # @hypre_MPI_Recv_init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end27:
	.size	hypre_MPI_Recv_init, .Lfunc_end27-hypre_MPI_Recv_init
	.cfi_endproc

	.globl	hypre_MPI_Irsend
	.p2align	4, 0x90
	.type	hypre_MPI_Irsend,@function
hypre_MPI_Irsend:                       # @hypre_MPI_Irsend
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end28:
	.size	hypre_MPI_Irsend, .Lfunc_end28-hypre_MPI_Irsend
	.cfi_endproc

	.globl	hypre_MPI_Startall
	.p2align	4, 0x90
	.type	hypre_MPI_Startall,@function
hypre_MPI_Startall:                     # @hypre_MPI_Startall
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end29:
	.size	hypre_MPI_Startall, .Lfunc_end29-hypre_MPI_Startall
	.cfi_endproc

	.globl	hypre_MPI_Probe
	.p2align	4, 0x90
	.type	hypre_MPI_Probe,@function
hypre_MPI_Probe:                        # @hypre_MPI_Probe
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end30:
	.size	hypre_MPI_Probe, .Lfunc_end30-hypre_MPI_Probe
	.cfi_endproc

	.globl	hypre_MPI_Iprobe
	.p2align	4, 0x90
	.type	hypre_MPI_Iprobe,@function
hypre_MPI_Iprobe:                       # @hypre_MPI_Iprobe
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end31:
	.size	hypre_MPI_Iprobe, .Lfunc_end31-hypre_MPI_Iprobe
	.cfi_endproc

	.globl	hypre_MPI_Test
	.p2align	4, 0x90
	.type	hypre_MPI_Test,@function
hypre_MPI_Test:                         # @hypre_MPI_Test
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end32:
	.size	hypre_MPI_Test, .Lfunc_end32-hypre_MPI_Test
	.cfi_endproc

	.globl	hypre_MPI_Testall
	.p2align	4, 0x90
	.type	hypre_MPI_Testall,@function
hypre_MPI_Testall:                      # @hypre_MPI_Testall
	.cfi_startproc
# BB#0:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end33:
	.size	hypre_MPI_Testall, .Lfunc_end33-hypre_MPI_Testall
	.cfi_endproc

	.globl	hypre_MPI_Wait
	.p2align	4, 0x90
	.type	hypre_MPI_Wait,@function
hypre_MPI_Wait:                         # @hypre_MPI_Wait
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end34:
	.size	hypre_MPI_Wait, .Lfunc_end34-hypre_MPI_Wait
	.cfi_endproc

	.globl	hypre_MPI_Waitall
	.p2align	4, 0x90
	.type	hypre_MPI_Waitall,@function
hypre_MPI_Waitall:                      # @hypre_MPI_Waitall
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end35:
	.size	hypre_MPI_Waitall, .Lfunc_end35-hypre_MPI_Waitall
	.cfi_endproc

	.globl	hypre_MPI_Waitany
	.p2align	4, 0x90
	.type	hypre_MPI_Waitany,@function
hypre_MPI_Waitany:                      # @hypre_MPI_Waitany
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end36:
	.size	hypre_MPI_Waitany, .Lfunc_end36-hypre_MPI_Waitany
	.cfi_endproc

	.globl	hypre_MPI_Allreduce
	.p2align	4, 0x90
	.type	hypre_MPI_Allreduce,@function
hypre_MPI_Allreduce:                    # @hypre_MPI_Allreduce
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB37_4
# BB#1:
	cmpl	$2, %ecx
	je	.LBB37_5
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB37_6
# BB#3:
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.LBB37_4:
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.LBB37_5:
	movb	(%rdi), %al
	movb	%al, (%rsi)
.LBB37_6:
	xorl	%eax, %eax
	retq
.Lfunc_end37:
	.size	hypre_MPI_Allreduce, .Lfunc_end37-hypre_MPI_Allreduce
	.cfi_endproc

	.globl	hypre_MPI_Request_free
	.p2align	4, 0x90
	.type	hypre_MPI_Request_free,@function
hypre_MPI_Request_free:                 # @hypre_MPI_Request_free
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end38:
	.size	hypre_MPI_Request_free, .Lfunc_end38-hypre_MPI_Request_free
	.cfi_endproc

	.globl	hypre_MPI_Type_contiguous
	.p2align	4, 0x90
	.type	hypre_MPI_Type_contiguous,@function
hypre_MPI_Type_contiguous:              # @hypre_MPI_Type_contiguous
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end39:
	.size	hypre_MPI_Type_contiguous, .Lfunc_end39-hypre_MPI_Type_contiguous
	.cfi_endproc

	.globl	hypre_MPI_Type_vector
	.p2align	4, 0x90
	.type	hypre_MPI_Type_vector,@function
hypre_MPI_Type_vector:                  # @hypre_MPI_Type_vector
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end40:
	.size	hypre_MPI_Type_vector, .Lfunc_end40-hypre_MPI_Type_vector
	.cfi_endproc

	.globl	hypre_MPI_Type_hvector
	.p2align	4, 0x90
	.type	hypre_MPI_Type_hvector,@function
hypre_MPI_Type_hvector:                 # @hypre_MPI_Type_hvector
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end41:
	.size	hypre_MPI_Type_hvector, .Lfunc_end41-hypre_MPI_Type_hvector
	.cfi_endproc

	.globl	hypre_MPI_Type_struct
	.p2align	4, 0x90
	.type	hypre_MPI_Type_struct,@function
hypre_MPI_Type_struct:                  # @hypre_MPI_Type_struct
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end42:
	.size	hypre_MPI_Type_struct, .Lfunc_end42-hypre_MPI_Type_struct
	.cfi_endproc

	.globl	hypre_MPI_Type_commit
	.p2align	4, 0x90
	.type	hypre_MPI_Type_commit,@function
hypre_MPI_Type_commit:                  # @hypre_MPI_Type_commit
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end43:
	.size	hypre_MPI_Type_commit, .Lfunc_end43-hypre_MPI_Type_commit
	.cfi_endproc

	.globl	hypre_MPI_Type_free
	.p2align	4, 0x90
	.type	hypre_MPI_Type_free,@function
hypre_MPI_Type_free:                    # @hypre_MPI_Type_free
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end44:
	.size	hypre_MPI_Type_free, .Lfunc_end44-hypre_MPI_Type_free
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
