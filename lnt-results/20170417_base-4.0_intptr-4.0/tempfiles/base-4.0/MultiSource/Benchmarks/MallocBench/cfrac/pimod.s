	.text
	.file	"pimod.bc"
	.globl	pimod
	.p2align	4, 0x90
	.type	pimod,@function
pimod:                                  # @pimod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	andl	$65535, %ecx            # imm = 0xFFFF
	je	.LBB0_3
# BB#4:
	leaq	8(%rbx), %rsi
	movzwl	4(%rbx), %eax
	leaq	8(%rbx,%rax,2), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	shll	$16, %edx
	movzwl	-2(%rdi), %eax
	addq	$-2, %rdi
	orl	%edx, %eax
	xorl	%edx, %edx
	divl	%ecx
	cmpq	%rsi, %rdi
	ja	.LBB0_5
# BB#6:
	movl	%edx, %ebp
	negl	%ebp
	cmpb	$0, 6(%rbx)
	cmovel	%edx, %ebp
	testq	%rbx, %rbx
	jne	.LBB0_8
	jmp	.LBB0_10
.LBB0_3:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$.L.str.2, %edx
	callq	errorp
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB0_10
.LBB0_8:
	decw	(%rbx)
	jne	.LBB0_10
# BB#9:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_10:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pimod, .Lfunc_end0-pimod
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"pimod"
	.size	.L.str, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"divide by zero"
	.size	.L.str.2, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
