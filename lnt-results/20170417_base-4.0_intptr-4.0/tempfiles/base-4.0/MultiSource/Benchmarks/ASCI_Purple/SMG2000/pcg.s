	.text
	.file	"pcg.bc"
	.globl	hypre_PCGFunctionsCreate
	.p2align	4, 0x90
	.type	hypre_PCGFunctionsCreate,@function
hypre_PCGFunctionsCreate:               # @hypre_PCGFunctionsCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	$1, %edi
	movl	$112, %esi
	callq	*%rbp
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%r12, 24(%rax)
	movq	%r15, 32(%rax)
	movq	%r14, 40(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 48(%rax)
	movq	72(%rsp), %rcx
	movq	%rcx, 56(%rax)
	movq	80(%rsp), %rcx
	movq	%rcx, 64(%rax)
	movq	88(%rsp), %rcx
	movq	%rcx, 72(%rax)
	movq	96(%rsp), %rcx
	movq	%rcx, 80(%rax)
	movq	104(%rsp), %rcx
	movq	%rcx, 88(%rax)
	movq	112(%rsp), %rcx
	movq	%rcx, 104(%rax)
	movq	120(%rsp), %rcx
	movq	%rcx, 96(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_PCGFunctionsCreate, .Lfunc_end0-hypre_PCGFunctionsCreate
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	hypre_PCGCreate
	.p2align	4, 0x90
	.type	hypre_PCGCreate,@function
hypre_PCGCreate:                        # @hypre_PCGCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$112, %esi
	callq	*(%rbx)
	movq	%rbx, 80(%rax)
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movups	%xmm0, (%rax)
	movl	$1000, %ecx             # imm = 0x3E8
	movd	%ecx, %xmm0
	movdqu	%xmm0, 16(%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 92(%rax)
	movl	$0, 108(%rax)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	hypre_PCGCreate, .Lfunc_end1-hypre_PCGCreate
	.cfi_endproc

	.globl	hypre_PCGDestroy
	.p2align	4, 0x90
	.type	hypre_PCGDestroy,@function
hypre_PCGDestroy:                       # @hypre_PCGDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_4
# BB#1:
	movq	80(%rbx), %r14
	cmpl	$0, 92(%rbx)
	jle	.LBB2_3
# BB#2:
	movq	96(%rbx), %rdi
	callq	*8(%r14)
	movq	$0, 96(%rbx)
	movq	104(%rbx), %rdi
	callq	*8(%r14)
	movq	$0, 104(%rbx)
.LBB2_3:                                # %._crit_edge
	movq	64(%rbx), %rdi
	callq	*48(%r14)
	movq	40(%rbx), %rdi
	callq	*24(%r14)
	movq	48(%rbx), %rdi
	callq	*24(%r14)
	movq	56(%rbx), %rdi
	callq	*24(%r14)
	movq	%rbx, %rdi
	callq	*8(%r14)
	movq	%r14, %rdi
	callq	*8(%r14)
.LBB2_4:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	hypre_PCGDestroy, .Lfunc_end2-hypre_PCGDestroy
	.cfi_endproc

	.globl	hypre_PCGSetup
	.p2align	4, 0x90
	.type	hypre_PCGSetup,@function
hypre_PCGSetup:                         # @hypre_PCGSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	80(%rbx), %r13
	movl	16(%rbx), %r14d
	movq	104(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	72(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r12, 32(%rbx)
	movq	%rbp, %rdi
	callq	*16(%r13)
	movq	%rax, 40(%rbx)
	movq	%rbp, %rdi
	callq	*16(%r13)
	movq	%rax, 48(%rbx)
	movq	%r15, %rdi
	callq	*16(%r13)
	movq	%rax, 56(%rbx)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*32(%r13)
	movq	%rax, 64(%rbx)
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	*16(%rsp)               # 8-byte Folded Reload
	cmpl	$0, 92(%rbx)
	jle	.LBB3_2
# BB#1:
	incl	%r14d
	movl	$8, %esi
	movl	%r14d, %edi
	callq	*(%r13)
	movq	%rax, 96(%rbx)
	movl	$8, %esi
	movl	%r14d, %edi
	callq	*(%r13)
	movq	%rax, 104(%rbx)
.LBB3_2:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_PCGSetup, .Lfunc_end3-hypre_PCGSetup
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	-4616189618054758400    # double -1
.LCPI4_1:
	.quad	4607182418800017408     # double 1
.LCPI4_4:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI4_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	hypre_PCGSolve
	.p2align	4, 0x90
	.type	hypre_PCGSolve,@function
hypre_PCGSolve:                         # @hypre_PCGSolve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 256
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	80(%rdi), %r15
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	movslq	16(%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	20(%rdi), %eax
	movl	24(%rdi), %ecx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movl	28(%rdi), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	40(%rdi), %r13
	movq	48(%rdi), %rbx
	movq	56(%rdi), %r14
	movq	64(%rdi), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	96(%r15), %rcx
	movq	72(%rdi), %rbp
	movl	%eax, 108(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	movl	92(%rdi), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	96(%rdi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movq	104(%rdi), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	je	.LBB4_2
# BB#1:
	movq	%r12, %rdi
	jmp	.LBB4_3
.LBB4_2:
	movq	%r13, %rdi
	callq	*72(%r15)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	*80(%rsp)               # 8-byte Folded Reload
	movq	%r13, %rdi
.LBB4_3:
	movq	%r12, %rsi
	callq	*56(%r15)
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB4_33
# BB#4:
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	je	.LBB4_6
# BB#5:
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
.LBB4_6:
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_8
# BB#7:
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
.LBB4_8:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	*64(%r15)
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	callq	*40(%r15)
	xorpd	%xmm0, %xmm0
	movsd	96(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	80(%rsp), %r12          # 8-byte Reload
	ja	.LBB4_10
# BB#9:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
                                        # implicit-def: %XMM1
	jle	.LBB4_14
.LBB4_10:
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	*56(%r15)
	movapd	%xmm0, %xmm1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_14
# BB#11:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_13
# BB#12:                                # %call.sqrt
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB4_13:                               # %.split
	movq	72(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax)
.LBB4_14:                               # %._crit_edge245
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	callq	*72(%r15)
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	*%r12
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	*56(%r15)
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_32
# BB#15:                                # %.lr.ph
	movl	$1, %eax
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	xorpd	%xmm1, %xmm1
	movq	144(%rsp), %rdi         # 8-byte Reload
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	*40(%r15)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*56(%r15)
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 176(%rsp)        # 16-byte Spill
	movapd	%xmm1, %xmm0
	movq	%r13, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	*88(%r15)
	movapd	176(%rsp), %xmm0        # 16-byte Reload
	xorpd	.LCPI4_2(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*88(%r15)
	movq	%rbx, %rdi
	callq	*72(%r15)
	xorl	%eax, %eax
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	*80(%rsp)               # 8-byte Folded Reload
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*56(%r15)
	movapd	%xmm0, %xmm3
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	movsd	%xmm3, 152(%rsp)        # 8-byte Spill
	je	.LBB4_18
# BB#17:                                #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	*56(%r15)
	movapd	%xmm0, %xmm3
.LBB4_18:                               #   in Loop: Header=BB4_16 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	jle	.LBB4_25
# BB#19:                                #   in Loop: Header=BB4_16 Depth=1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_21
# BB#20:                                # %call.sqrt249
                                        #   in Loop: Header=BB4_16 Depth=1
	movapd	%xmm3, %xmm0
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB4_21:                               # %.split248
                                        #   in Loop: Header=BB4_16 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax,%rbp,8)
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB4_22
	jnp	.LBB4_24
.LBB4_22:                               #   in Loop: Header=BB4_16 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_24
# BB#23:                                # %call.sqrt251
                                        #   in Loop: Header=BB4_16 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB4_24:                               #   in Loop: Header=BB4_16 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movsd	%xmm0, (%rax,%rbp,8)
.LBB4_25:                               #   in Loop: Header=BB4_16 Depth=1
	movapd	%xmm3, %xmm0
	divsd	%xmm2, %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB4_29
# BB#26:                                #   in Loop: Header=BB4_16 Depth=1
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB4_32
# BB#27:                                #   in Loop: Header=BB4_16 Depth=1
	ucomisd	.LCPI4_4, %xmm3
	jbe	.LBB4_32
# BB#28:                                #   in Loop: Header=BB4_16 Depth=1
	movq	%r13, %rdi
	movq	%r13, %rsi
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	callq	*56(%r15)
	movsd	%xmm0, 168(%rsp)        # 8-byte Spill
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rsi
	callq	*56(%r15)
	movsd	32(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	176(%rsp), %xmm1        # 16-byte Reload
	mulsd	%xmm1, %xmm1
	mulsd	168(%rsp), %xmm1        # 8-byte Folded Reload
	divsd	%xmm0, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	ja	.LBB4_32
.LBB4_29:                               #   in Loop: Header=BB4_16 Depth=1
	movsd	96(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	.LCPI4_4, %xmm0
	jbe	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_16 Depth=1
	divsd	64(%rsp), %xmm3         # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	addsd	%xmm0, %xmm0
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm3, %xmm0
	callq	pow
	movapd	%xmm0, %xmm1
	movsd	120(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	andpd	.LCPI4_3(%rip), %xmm1
	maxsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm1
	movsd	.LCPI4_1(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	ucomisd	96(%rsp), %xmm2         # 8-byte Folded Reload
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	ja	.LBB4_32
.LBB4_31:                               #   in Loop: Header=BB4_16 Depth=1
	movsd	152(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	movq	%r13, %rdi
	callq	*80(%r15)
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*88(%r15)
	movq	24(%rsp), %r12          # 8-byte Reload
	leaq	1(%rbp), %rax
	cmpq	128(%rsp), %rbp         # 8-byte Folded Reload
	movsd	152(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	jl	.LBB4_16
.LBB4_32:                               # %._crit_edge
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	%ebp, 88(%rax)
	jmp	.LBB4_35
.LBB4_33:
	movq	%r12, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	*64(%r15)
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_35
# BB#34:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
.LBB4_35:
	xorl	%eax, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_PCGSolve, .Lfunc_end4-hypre_PCGSolve
	.cfi_endproc

	.globl	hypre_PCGSetTol
	.p2align	4, 0x90
	.type	hypre_PCGSetTol,@function
hypre_PCGSetTol:                        # @hypre_PCGSetTol
	.cfi_startproc
# BB#0:
	movsd	%xmm0, (%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	hypre_PCGSetTol, .Lfunc_end5-hypre_PCGSetTol
	.cfi_endproc

	.globl	hypre_PCGSetConvergenceFactorTol
	.p2align	4, 0x90
	.type	hypre_PCGSetConvergenceFactorTol,@function
hypre_PCGSetConvergenceFactorTol:       # @hypre_PCGSetConvergenceFactorTol
	.cfi_startproc
# BB#0:
	movsd	%xmm0, 8(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	hypre_PCGSetConvergenceFactorTol, .Lfunc_end6-hypre_PCGSetConvergenceFactorTol
	.cfi_endproc

	.globl	hypre_PCGSetMaxIter
	.p2align	4, 0x90
	.type	hypre_PCGSetMaxIter,@function
hypre_PCGSetMaxIter:                    # @hypre_PCGSetMaxIter
	.cfi_startproc
# BB#0:
	movl	%esi, 16(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	hypre_PCGSetMaxIter, .Lfunc_end7-hypre_PCGSetMaxIter
	.cfi_endproc

	.globl	hypre_PCGSetTwoNorm
	.p2align	4, 0x90
	.type	hypre_PCGSetTwoNorm,@function
hypre_PCGSetTwoNorm:                    # @hypre_PCGSetTwoNorm
	.cfi_startproc
# BB#0:
	movl	%esi, 20(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	hypre_PCGSetTwoNorm, .Lfunc_end8-hypre_PCGSetTwoNorm
	.cfi_endproc

	.globl	hypre_PCGSetRelChange
	.p2align	4, 0x90
	.type	hypre_PCGSetRelChange,@function
hypre_PCGSetRelChange:                  # @hypre_PCGSetRelChange
	.cfi_startproc
# BB#0:
	movl	%esi, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	hypre_PCGSetRelChange, .Lfunc_end9-hypre_PCGSetRelChange
	.cfi_endproc

	.globl	hypre_PCGSetStopCrit
	.p2align	4, 0x90
	.type	hypre_PCGSetStopCrit,@function
hypre_PCGSetStopCrit:                   # @hypre_PCGSetStopCrit
	.cfi_startproc
# BB#0:
	movl	%esi, 28(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_PCGSetStopCrit, .Lfunc_end10-hypre_PCGSetStopCrit
	.cfi_endproc

	.globl	hypre_PCGGetPrecond
	.p2align	4, 0x90
	.type	hypre_PCGGetPrecond,@function
hypre_PCGGetPrecond:                    # @hypre_PCGGetPrecond
	.cfi_startproc
# BB#0:
	movq	72(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	hypre_PCGGetPrecond, .Lfunc_end11-hypre_PCGGetPrecond
	.cfi_endproc

	.globl	hypre_PCGSetPrecond
	.p2align	4, 0x90
	.type	hypre_PCGSetPrecond,@function
hypre_PCGSetPrecond:                    # @hypre_PCGSetPrecond
	.cfi_startproc
# BB#0:
	movq	80(%rdi), %rax
	movq	%rsi, 96(%rax)
	movq	%rdx, 104(%rax)
	movq	%rcx, 72(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	hypre_PCGSetPrecond, .Lfunc_end12-hypre_PCGSetPrecond
	.cfi_endproc

	.globl	hypre_PCGSetLogging
	.p2align	4, 0x90
	.type	hypre_PCGSetLogging,@function
hypre_PCGSetLogging:                    # @hypre_PCGSetLogging
	.cfi_startproc
# BB#0:
	movl	%esi, 92(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	hypre_PCGSetLogging, .Lfunc_end13-hypre_PCGSetLogging
	.cfi_endproc

	.globl	hypre_PCGGetNumIterations
	.p2align	4, 0x90
	.type	hypre_PCGGetNumIterations,@function
hypre_PCGGetNumIterations:              # @hypre_PCGGetNumIterations
	.cfi_startproc
# BB#0:
	movl	88(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	hypre_PCGGetNumIterations, .Lfunc_end14-hypre_PCGGetNumIterations
	.cfi_endproc

	.globl	hypre_PCGPrintLogging
	.p2align	4, 0x90
	.type	hypre_PCGPrintLogging,@function
hypre_PCGPrintLogging:                  # @hypre_PCGPrintLogging
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -40
.Lcfi52:
	.cfi_offset %r12, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	testl	%esi, %esi
	jne	.LBB15_5
# BB#1:
	movl	92(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB15_5
# BB#2:
	movl	88(%rdi), %r14d
	testl	%r14d, %r14d
	jle	.LBB15_5
# BB#3:                                 # %.lr.ph.preheader
	movq	96(%rdi), %r15
	movq	104(%rdi), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	movl	%ebx, %esi
	callq	printf
	movsd	(%r12,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$1, %al
	movl	%ebx, %esi
	callq	printf
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB15_4
.LBB15_5:                               # %.loopexit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	hypre_PCGPrintLogging, .Lfunc_end15-hypre_PCGPrintLogging
	.cfi_endproc

	.globl	hypre_PCGGetFinalRelativeResidualNorm
	.p2align	4, 0x90
	.type	hypre_PCGGetFinalRelativeResidualNorm,@function
hypre_PCGGetFinalRelativeResidualNorm:  # @hypre_PCGGetFinalRelativeResidualNorm
	.cfi_startproc
# BB#0:
	cmpl	$0, 92(%rdi)
	jle	.LBB16_1
# BB#2:
	movq	104(%rdi), %rax
	movslq	88(%rdi), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.LBB16_1:
	movl	$-1, %eax
	retq
.Lfunc_end16:
	.size	hypre_PCGGetFinalRelativeResidualNorm, .Lfunc_end16-hypre_PCGGetFinalRelativeResidualNorm
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Residual norm[%d] = %e   "
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Relative residual norm[%d] = %e\n"
	.size	.L.str.1, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
