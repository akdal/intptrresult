	.text
	.file	"OffsetStream.bc"
	.globl	_ZN16COffsetOutStream4InitEP10IOutStreamy
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream4InitEP10IOutStreamy,@function
_ZN16COffsetOutStream4InitEP10IOutStreamy: # @_ZN16COffsetOutStream4InitEP10IOutStreamy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%r14, 16(%r15)
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB0_2:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_4:                                # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit
	movq	%rbx, 24(%r15)
	movq	(%rbx), %rax
	movq	48(%rax), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end0:
	.size	_ZN16COffsetOutStream4InitEP10IOutStreamy, .Lfunc_end0-_ZN16COffsetOutStream4InitEP10IOutStreamy
	.cfi_endproc

	.globl	_ZN16COffsetOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream5WriteEPKvjPj,@function
_ZN16COffsetOutStream5WriteEPKvjPj:     # @_ZN16COffsetOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end1:
	.size	_ZN16COffsetOutStream5WriteEPKvjPj, .Lfunc_end1-_ZN16COffsetOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN16COffsetOutStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream4SeekExjPy,@function
_ZN16COffsetOutStream4SeekExjPy:        # @_ZN16COffsetOutStream4SeekExjPy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	testl	%edx, %edx
	jne	.LBB2_2
# BB#1:
	addq	16(%rbx), %rsi
.LBB2_2:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rcx
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB2_4
# BB#3:
	movq	(%rsp), %rcx
	subq	16(%rbx), %rcx
	movq	%rcx, (%r14)
.LBB2_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN16COffsetOutStream4SeekExjPy, .Lfunc_end2-_ZN16COffsetOutStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN16COffsetOutStream7SetSizeEy
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream7SetSizeEy,@function
_ZN16COffsetOutStream7SetSizeEy:        # @_ZN16COffsetOutStream7SetSizeEy
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
	addq	16(%rdi), %rsi
	movq	%rax, %rdi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end3:
	.size	_ZN16COffsetOutStream7SetSizeEy, .Lfunc_end3-_ZN16COffsetOutStream7SetSizeEy
	.cfi_endproc

	.section	.text._ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB4_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB4_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB4_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB4_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB4_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB4_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB4_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB4_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB4_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB4_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB4_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB4_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB4_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB4_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB4_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB4_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB4_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end4-_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16COffsetOutStream6AddRefEv,"axG",@progbits,_ZN16COffsetOutStream6AddRefEv,comdat
	.weak	_ZN16COffsetOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream6AddRefEv,@function
_ZN16COffsetOutStream6AddRefEv:         # @_ZN16COffsetOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN16COffsetOutStream6AddRefEv, .Lfunc_end5-_ZN16COffsetOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN16COffsetOutStream7ReleaseEv,"axG",@progbits,_ZN16COffsetOutStream7ReleaseEv,comdat
	.weak	_ZN16COffsetOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStream7ReleaseEv,@function
_ZN16COffsetOutStream7ReleaseEv:        # @_ZN16COffsetOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB6_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB6_2:
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN16COffsetOutStream7ReleaseEv, .Lfunc_end6-_ZN16COffsetOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16COffsetOutStreamD2Ev,"axG",@progbits,_ZN16COffsetOutStreamD2Ev,comdat
	.weak	_ZN16COffsetOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStreamD2Ev,@function
_ZN16COffsetOutStreamD2Ev:              # @_ZN16COffsetOutStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV16COffsetOutStream+16, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB7_1:                                # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	retq
.Lfunc_end7:
	.size	_ZN16COffsetOutStreamD2Ev, .Lfunc_end7-_ZN16COffsetOutStreamD2Ev
	.cfi_endproc

	.section	.text._ZN16COffsetOutStreamD0Ev,"axG",@progbits,_ZN16COffsetOutStreamD0Ev,comdat
	.weak	_ZN16COffsetOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN16COffsetOutStreamD0Ev,@function
_ZN16COffsetOutStreamD0Ev:              # @_ZN16COffsetOutStreamD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16COffsetOutStream+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB8_2:                                # %_ZN16COffsetOutStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN16COffsetOutStreamD0Ev, .Lfunc_end8-_ZN16COffsetOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV16COffsetOutStream,@object # @_ZTV16COffsetOutStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV16COffsetOutStream
	.p2align	3
_ZTV16COffsetOutStream:
	.quad	0
	.quad	_ZTI16COffsetOutStream
	.quad	_ZN16COffsetOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16COffsetOutStream6AddRefEv
	.quad	_ZN16COffsetOutStream7ReleaseEv
	.quad	_ZN16COffsetOutStreamD2Ev
	.quad	_ZN16COffsetOutStreamD0Ev
	.quad	_ZN16COffsetOutStream5WriteEPKvjPj
	.quad	_ZN16COffsetOutStream4SeekExjPy
	.quad	_ZN16COffsetOutStream7SetSizeEy
	.size	_ZTV16COffsetOutStream, 80

	.type	_ZTS16COffsetOutStream,@object # @_ZTS16COffsetOutStream
	.globl	_ZTS16COffsetOutStream
	.p2align	4
_ZTS16COffsetOutStream:
	.asciz	"16COffsetOutStream"
	.size	_ZTS16COffsetOutStream, 19

	.type	_ZTS10IOutStream,@object # @_ZTS10IOutStream
	.section	.rodata._ZTS10IOutStream,"aG",@progbits,_ZTS10IOutStream,comdat
	.weak	_ZTS10IOutStream
_ZTS10IOutStream:
	.asciz	"10IOutStream"
	.size	_ZTS10IOutStream, 13

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI10IOutStream,@object # @_ZTI10IOutStream
	.section	.rodata._ZTI10IOutStream,"aG",@progbits,_ZTI10IOutStream,comdat
	.weak	_ZTI10IOutStream
	.p2align	4
_ZTI10IOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IOutStream
	.quad	_ZTI20ISequentialOutStream
	.size	_ZTI10IOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI16COffsetOutStream,@object # @_ZTI16COffsetOutStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI16COffsetOutStream
	.p2align	4
_ZTI16COffsetOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16COffsetOutStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16COffsetOutStream, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
