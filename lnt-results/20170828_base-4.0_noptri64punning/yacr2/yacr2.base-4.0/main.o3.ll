; ModuleID = 'yacr2.base-4.0/main.bc'
source_filename = "main.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@channelTracks = external local_unnamed_addr global i64, align 8
@channelTracksCopy = external local_unnamed_addr global i64, align 8
@channelNets = external local_unnamed_addr global i64, align 8
@netsAssign = external local_unnamed_addr global i64*, align 8
@netsAssignCopy = external local_unnamed_addr global i64*, align 8
@.str = private unnamed_addr constant [56 x i8] c"Assignment could not route %d columns, trying maze1...\0A\00", align 1
@.str.1 = private unnamed_addr constant [51 x i8] c"Maze1 could not route %d columns, trying maze2...\0A\00", align 1
@.str.2 = private unnamed_addr constant [51 x i8] c"Maze2 could not route %d columns, trying maze3...\0A\00", align 1
@.str.3 = private unnamed_addr constant [53 x i8] c"Maze3 could not route %d columns, adding a track...\0A\00", align 1

; Function Attrs: noreturn nounwind uwtable
define i32 @main(i32 %argc, i8** %argv) local_unnamed_addr #0 {
entry:
  br label %for.body

for.body:                                         ; preds = %do.end58, %entry
  %TIMELOOP.036 = phi i32 [ 0, %entry ], [ %inc61, %do.end58 ]
  tail call void @Option(i32 %argc, i8** %argv) #4
  tail call void @BuildChannel() #4
  tail call void @BuildVCG() #4
  tail call void @AcyclicVCG() #4
  tail call void @BuildHCG() #4
  br label %do.body

do.body:                                          ; preds = %if.then54, %for.body
  tail call void @AllocAssign() #4
  tail call void @NetsAssign() #4
  tail call void @InitAllocMaps() #4
  %0 = load i64, i64* @channelTracks, align 8, !tbaa !1
  store i64 %0, i64* @channelTracksCopy, align 8, !tbaa !1
  %1 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp230 = icmp eq i64 %1, 0
  br i1 %cmp230, label %do.body5.preheader, label %for.body3.lr.ph

for.body3.lr.ph:                                  ; preds = %do.body
  %2 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %3 = load i64*, i64** @netsAssignCopy, align 8, !tbaa !5
  br label %for.body3

for.body3:                                        ; preds = %for.body3.lr.ph, %for.body3
  %net.031 = phi i64 [ 1, %for.body3.lr.ph ], [ %inc, %for.body3 ]
  %arrayidx = getelementptr inbounds i64, i64* %2, i64 %net.031
  %4 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %arrayidx4 = getelementptr inbounds i64, i64* %3, i64 %net.031
  store i64 %4, i64* %arrayidx4, align 8, !tbaa !1
  %inc = add i64 %net.031, 1
  %5 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp2 = icmp ugt i64 %inc, %5
  br i1 %cmp2, label %do.body5.preheader.loopexit, label %for.body3

do.body5.preheader.loopexit:                      ; preds = %for.body3
  br label %do.body5.preheader

do.body5.preheader:                               ; preds = %do.body5.preheader.loopexit, %do.body
  br label %do.body5

do.body5:                                         ; preds = %do.body5.preheader, %land.rhs
  %fail.0 = phi i64 [ %inc22, %land.rhs ], [ 0, %do.body5.preheader ]
  %call = tail call i32 @DrawNets() #4
  %cmp6 = icmp eq i32 %call, 0
  br i1 %cmp6, label %do.end58, label %if.then

if.then:                                          ; preds = %do.body5
  %call7 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str, i64 0, i64 0), i32 %call)
  %call8 = tail call i32 @Maze1() #4
  %cmp9 = icmp eq i32 %call8, 0
  br i1 %cmp9, label %do.end58, label %if.then10

if.then10:                                        ; preds = %if.then
  %call11 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.1, i64 0, i64 0), i32 %call8)
  %call12 = tail call i32 @Maze2() #4
  %cmp13 = icmp eq i32 %call12, 0
  br i1 %cmp13, label %do.end58, label %if.then14

if.then14:                                        ; preds = %if.then10
  %call15 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i64 0, i64 0), i32 %call12)
  %call16 = tail call i32 @Maze3() #4
  %cmp17 = icmp eq i32 %call16, 0
  br i1 %cmp17, label %do.end58, label %if.then18

if.then18:                                        ; preds = %if.then14
  %call19 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.3, i64 0, i64 0), i32 %call16)
  %tobool = icmp eq i64 %fail.0, 0
  br i1 %tobool, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then18
  %6 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %inc21 = add i64 %6, 1
  store i64 %inc21, i64* @channelTracks, align 8, !tbaa !1
  br label %if.end

if.end:                                           ; preds = %if.then18, %if.then20
  %inc22 = add i64 %fail.0, 1
  %7 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp2432 = icmp eq i64 %7, 0
  br i1 %cmp2432, label %land.rhs, label %for.body25.lr.ph

for.body25.lr.ph:                                 ; preds = %if.end
  %8 = load i64*, i64** @netsAssignCopy, align 8, !tbaa !5
  %9 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  br label %for.body25

for.body25:                                       ; preds = %for.body25.lr.ph, %for.body25
  %net.133 = phi i64 [ 1, %for.body25.lr.ph ], [ %inc29, %for.body25 ]
  %arrayidx26 = getelementptr inbounds i64, i64* %8, i64 %net.133
  %10 = load i64, i64* %arrayidx26, align 8, !tbaa !1
  %arrayidx27 = getelementptr inbounds i64, i64* %9, i64 %net.133
  store i64 %10, i64* %arrayidx27, align 8, !tbaa !1
  %inc29 = add i64 %net.133, 1
  %11 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp24 = icmp ugt i64 %inc29, %11
  br i1 %cmp24, label %if.end34, label %for.body25

if.end34:                                         ; preds = %for.body25
  %tobool36 = icmp eq i64 %inc22, 0
  %cmp3934 = icmp eq i64 %11, 0
  %or.cond = or i1 %tobool36, %cmp3934
  br i1 %or.cond, label %land.rhs, label %for.body40.lr.ph

for.body40.lr.ph:                                 ; preds = %if.end34
  %12 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  br label %for.body40

for.body40:                                       ; preds = %for.body40.lr.ph, %for.inc47
  %13 = phi i64 [ %11, %for.body40.lr.ph ], [ %15, %for.inc47 ]
  %insert.035 = phi i64 [ 1, %for.body40.lr.ph ], [ %inc48, %for.inc47 ]
  %arrayidx41 = getelementptr inbounds i64, i64* %12, i64 %insert.035
  %14 = load i64, i64* %arrayidx41, align 8, !tbaa !1
  %cmp42 = icmp ult i64 %14, %inc22
  br i1 %cmp42, label %for.inc47, label %if.then43

if.then43:                                        ; preds = %for.body40
  %inc45 = add i64 %14, 1
  store i64 %inc45, i64* %arrayidx41, align 8, !tbaa !1
  %.pre = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc47

for.inc47:                                        ; preds = %for.body40, %if.then43
  %15 = phi i64 [ %13, %for.body40 ], [ %.pre, %if.then43 ]
  %inc48 = add i64 %insert.035, 1
  %cmp39 = icmp ugt i64 %inc48, %15
  br i1 %cmp39, label %land.rhs.loopexit, label %for.body40

land.rhs.loopexit:                                ; preds = %for.inc47
  br label %land.rhs

land.rhs:                                         ; preds = %land.rhs.loopexit, %if.end, %if.end34
  %16 = load i64, i64* @channelTracksCopy, align 8, !tbaa !1
  %add = add i64 %16, 1
  %cmp52 = icmp ugt i64 %inc22, %add
  br i1 %cmp52, label %if.then54, label %do.body5

if.then54:                                        ; preds = %land.rhs
  tail call void @FreeAllocMaps() #4
  tail call void @FreeAssign() #4
  br label %do.body

do.end58:                                         ; preds = %do.body5, %if.then, %if.then10, %if.then14
  %putchar = tail call i32 @putchar(i32 10) #4
  tail call void @PrintChannel() #4
  %inc61 = add nuw nsw i32 %TIMELOOP.036, 1
  %exitcond = icmp eq i32 %inc61, 20
  br i1 %exitcond, label %for.end62, label %for.body

for.end62:                                        ; preds = %do.end58
  tail call void @exit(i32 0) #5
  unreachable
}

declare void @Option(i32, i8**) local_unnamed_addr #1

declare void @BuildChannel() local_unnamed_addr #1

declare void @BuildVCG() local_unnamed_addr #1

declare void @AcyclicVCG() local_unnamed_addr #1

declare void @BuildHCG() local_unnamed_addr #1

declare void @AllocAssign() local_unnamed_addr #1

declare void @NetsAssign() local_unnamed_addr #1

declare void @InitAllocMaps() local_unnamed_addr #1

declare i32 @DrawNets() local_unnamed_addr #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #2

declare i32 @Maze1() local_unnamed_addr #1

declare i32 @Maze2() local_unnamed_addr #1

declare i32 @Maze3() local_unnamed_addr #1

declare void @FreeAllocMaps() local_unnamed_addr #1

declare void @FreeAssign() local_unnamed_addr #1

declare void @PrintChannel() local_unnamed_addr #1

; Function Attrs: noreturn nounwind
declare void @exit(i32) local_unnamed_addr #3

declare i32 @putchar(i32)

attributes #0 = { noreturn nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
