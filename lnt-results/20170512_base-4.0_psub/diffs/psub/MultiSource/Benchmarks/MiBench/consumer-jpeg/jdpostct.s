	.text
	.file	"jdpostct.bc"
	.globl	jinit_d_post_controller
	.p2align	4, 0x90
	.type	jinit_d_post_controller,@function
jinit_d_post_controller:                # @jinit_d_post_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 552(%rbx)
	movq	$start_pass_dpost, (%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	cmpl	$0, 100(%rbx)
	je	.LBB0_4
# BB#1:
	movl	392(%rbx), %eax
	movl	%eax, 32(%r14)
	testl	%ebp, %ebp
	movq	8(%rbx), %r8
	movl	128(%rbx), %ecx
	movl	136(%rbx), %ebp
	je	.LBB0_3
# BB#2:
	movq	32(%r8), %r15
	imull	%ecx, %ebp
	movl	132(%rbx), %edi
	movq	%rax, %rsi
	callq	jround_up
	movl	32(%r14), %r9d
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	movl	%eax, %r8d
	callq	*%r15
	movq	%rax, 16(%r14)
	jmp	.LBB0_4
.LBB0_3:
	imull	%ecx, %ebp
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movl	%eax, %ecx
	callq	*16(%r8)
	movq	%rax, 24(%r14)
.LBB0_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_d_post_controller, .Lfunc_end0-jinit_d_post_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_dpost,@function
start_pass_dpost:                       # @start_pass_dpost
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	552(%rdi), %rbx
	cmpl	$3, %esi
	je	.LBB1_7
# BB#1:
	cmpl	$2, %esi
	je	.LBB1_10
# BB#2:
	testl	%esi, %esi
	jne	.LBB1_13
# BB#3:
	cmpl	$0, 100(%rdi)
	je	.LBB1_6
# BB#4:
	movq	$post_process_1pass, 8(%rbx)
	cmpq	$0, 24(%rbx)
	jne	.LBB1_14
# BB#5:
	movq	8(%rdi), %rax
	movq	16(%rbx), %rsi
	movl	32(%rbx), %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	callq	*56(%rax)
	movq	%rax, 24(%rbx)
	jmp	.LBB1_14
.LBB1_7:
	cmpq	$0, 16(%rbx)
	jne	.LBB1_9
# BB#8:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
.LBB1_9:
	movq	$post_process_prepass, 8(%rbx)
	jmp	.LBB1_14
.LBB1_10:
	cmpq	$0, 16(%rbx)
	jne	.LBB1_12
# BB#11:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
.LBB1_12:
	movq	$post_process_2pass, 8(%rbx)
	jmp	.LBB1_14
.LBB1_13:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	callq	*(%rax)
	jmp	.LBB1_14
.LBB1_6:
	movq	592(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%rbx)
.LBB1_14:
	movq	$0, 36(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	start_pass_dpost, .Lfunc_end1-start_pass_dpost
	.cfi_endproc

	.p2align	4, 0x90
	.type	post_process_1pass,@function
post_process_1pass:                     # @post_process_1pass
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%r9, %r15
	movq	%r8, %r14
	movq	%rdi, %rbx
	movl	64(%rsp), %eax
	movq	552(%rbx), %r12
	subl	(%r15), %eax
	movl	32(%r12), %edi
	cmpl	%edi, %eax
	cmoval	%edi, %eax
	movl	$0, 20(%rsp)
	movq	592(%rbx), %r10
	movq	24(%r12), %r8
	movl	%eax, (%rsp)
	leaq	20(%rsp), %r9
	movq	%rbx, %rdi
	callq	*8(%r10)
	movq	608(%rbx), %rax
	movq	24(%r12), %rsi
	movl	(%r15), %ecx
	leaq	(%r14,%rcx,8), %rdx
	movl	20(%rsp), %ecx
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	20(%rsp), %eax
	addl	%eax, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	post_process_1pass, .Lfunc_end2-post_process_1pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	post_process_prepass,@function
post_process_prepass:                   # @post_process_prepass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 96
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movq	%rdi, %rbx
	movq	552(%rbx), %r14
	leaq	40(%r14), %r15
	movl	40(%r14), %r13d
	testl	%r13d, %r13d
	je	.LBB3_2
# BB#1:                                 # %._crit_edge
	leaq	24(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%r14), %r8
	leaq	32(%r14), %r12
	jmp	.LBB3_3
.LBB3_2:
	movq	8(%rbx), %rax
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	16(%r14), %rsi
	movq	%rdx, %r13
	movl	36(%r14), %edx
	leaq	32(%r14), %r12
	movl	32(%r14), %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	movq	%rax, %r8
	leaq	24(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r8, 24(%r14)
	movl	40(%r14), %r13d
.LBB3_3:
	movq	592(%rbx), %rax
	movl	(%r12), %ecx
	movl	%ecx, (%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	movq	%r15, %r9
	callq	*8(%rax)
	movl	(%r15), %eax
	movl	%eax, %ebp
	subl	%r13d, %ebp
	jbe	.LBB3_5
# BB#4:
	movq	608(%rbx), %rax
	movl	%r13d, %esi
	shlq	$3, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	addq	(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	*8(%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	addl	%ebp, (%rax)
	movl	(%r15), %eax
.LBB3_5:
	movl	(%r12), %ecx
	cmpl	%ecx, %eax
	jb	.LBB3_7
# BB#6:
	addl	%ecx, 36(%r14)
	movl	$0, 40(%r14)
.LBB3_7:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	post_process_prepass, .Lfunc_end3-post_process_prepass
	.cfi_endproc

	.p2align	4, 0x90
	.type	post_process_2pass,@function
post_process_2pass:                     # @post_process_2pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdi, %r12
	movl	64(%rsp), %r13d
	movq	552(%r12), %rbx
	movl	40(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB4_2
# BB#1:                                 # %._crit_edge
	movq	24(%rbx), %rax
	jmp	.LBB4_3
.LBB4_2:
	movq	8(%r12), %rax
	movq	16(%rbx), %rsi
	movl	36(%rbx), %edx
	movl	32(%rbx), %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	*56(%rax)
	movq	%rax, 24(%rbx)
	movl	40(%rbx), %ecx
.LBB4_3:
	movl	32(%rbx), %edx
	subl	%ecx, %edx
	movl	(%r14), %edi
	subl	%edi, %r13d
	cmpl	%r13d, %edx
	cmovbel	%edx, %r13d
	movl	132(%r12), %ebp
	subl	36(%rbx), %ebp
	cmpl	%ebp, %r13d
	cmovbel	%r13d, %ebp
	movq	608(%r12), %r8
	movl	%ecx, %ecx
	leaq	(%rax,%rcx,8), %rsi
	leaq	(%r15,%rdi,8), %rdx
	movq	%r12, %rdi
	movl	%ebp, %ecx
	callq	*8(%r8)
	addl	%ebp, (%r14)
	addl	40(%rbx), %ebp
	movl	%ebp, 40(%rbx)
	movl	32(%rbx), %eax
	cmpl	%eax, %ebp
	jb	.LBB4_5
# BB#4:
	addl	%eax, 36(%rbx)
	movl	$0, 40(%rbx)
.LBB4_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	post_process_2pass, .Lfunc_end4-post_process_2pass
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
