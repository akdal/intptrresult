	.text
	.file	"btPolyhedralConvexShape.bc"
	.globl	_ZN23btPolyhedralConvexShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN23btPolyhedralConvexShapeC2Ev,@function
_ZN23btPolyhedralConvexShapeC2Ev:       # @_ZN23btPolyhedralConvexShapeC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV23btPolyhedralConvexShape+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN23btPolyhedralConvexShapeC2Ev, .Lfunc_end0-_ZN23btPolyhedralConvexShapeC2Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
.LCPI1_1:
	.long	953267991               # float 9.99999974E-5
.LCPI1_2:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	mulss	%xmm0, %xmm0
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB1_2
# BB#1:
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	jmp	.LBB1_5
.LBB1_2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_4
# BB#3:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_4:                                # %.split
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp)           # 4-byte Spill
.LBB1_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*144(%rax)
	xorps	%xmm0, %xmm0
	testl	%eax, %eax
	jle	.LBB1_6
# BB#7:                                 # %.lr.ph
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorl	%ebp, %ebp
	leaq	16(%rsp), %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	*168(%rax)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	4(%rsp), %xmm1          # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	(%rsp), %xmm2           # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=1
	maxss	%xmm1, %xmm2
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	incl	%ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*144(%rax)
	cmpl	%eax, %ebp
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	jl	.LBB1_8
	jmp	.LBB1_11
.LBB1_6:
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
.LBB1_11:                               # %._crit_edge
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end1-_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 112
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	jle	.LBB2_16
# BB#1:                                 # %.lr.ph36.preheader
	movl	%ecx, %esi
	leaq	-1(%rsi), %rdx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	andq	$7, %rsi
	je	.LBB2_2
# BB#3:                                 # %.lr.ph36.prol.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	12(%rax), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph36.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, (%rdi)     # imm = 0xDD5E0B6B
	incq	%rax
	addq	$16, %rdi
	cmpq	%rax, %rsi
	jne	.LBB2_4
	jmp	.LBB2_5
.LBB2_2:
	xorl	%eax, %eax
.LBB2_5:                                # %.lr.ph36.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB2_8
# BB#6:                                 # %.lr.ph36.preheader.new
	movq	24(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	shlq	$4, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	124(%rsi,%rax), %rax
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph36
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, -112(%rax) # imm = 0xDD5E0B6B
	movl	$-581039253, -96(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -80(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -64(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -48(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -32(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -16(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, (%rax)     # imm = 0xDD5E0B6B
	subq	$-128, %rax
	addq	$-8, %rdx
	jne	.LBB2_7
.LBB2_8:                                # %.preheader
	testl	%ecx, %ecx
	jle	.LBB2_16
# BB#9:                                 # %.lr.ph32
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*144(%rax)
	testl	%eax, %eax
	jle	.LBB2_15
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB2_10 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	shlq	$4, %r13
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13), %r15
	leaq	4(%rax,%r13), %r14
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	12(%rax,%r13), %r12
	addq	%rax, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	leaq	32(%rsp), %rdx
	callq	*168(%rax)
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rsp), %xmm0
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	36(%rsp), %xmm1
	addss	%xmm0, %xmm1
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	40(%rsp), %xmm0
	addss	%xmm1, %xmm0
	ucomiss	(%r12), %xmm0
	jbe	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_12 Depth=2
	movups	32(%rsp), %xmm1
	movups	%xmm1, (%r13)
	movss	%xmm0, (%r12)
.LBB2_14:                               #   in Loop: Header=BB2_12 Depth=2
	incl	%ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*144(%rax)
	cmpl	%eax, %ebp
	jl	.LBB2_12
.LBB2_15:                               # %._crit_edge
                                        #   in Loop: Header=BB2_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB2_10
.LBB2_16:                               # %._crit_edge33
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end2-_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	1056964608              # float 0.5
.LCPI3_2:
	.long	1034594986              # float 0.0833333284
	.text
	.globl	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3: # @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 160
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movl	$1065353216, 72(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 76(%rsp)
	movl	$1065353216, 92(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 96(%rsp)
	movl	$1065353216, 112(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 116(%rsp)
	movl	$0, 132(%rsp)
	movq	(%rbx), %rax
	leaq	72(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rbx, %rdi
	callq	*16(%rax)
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	subps	%xmm2, %xmm1
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	24(%rsp), %xmm0
	mulps	.LCPI3_0(%rip), %xmm1
	mulss	.LCPI3_1(%rip), %xmm0
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	addps	%xmm1, %xmm2
	addps	%xmm2, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm0, %xmm0
	mulps	%xmm2, %xmm2
	mulss	%xmm0, %xmm0
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	mulss	.LCPI3_2(%rip), %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	addps	%xmm2, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm2, %xmm1
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, (%r14)
	movlps	%xmm0, 8(%r14)
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end3-_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3,@function
_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3: # @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end4:
	.size	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3, .Lfunc_end4-_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
	.p2align	4, 0x90
	.type	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv,@function
_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv: # @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$96, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 112
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$1, 96(%rbx)
	movb	_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions(%rip), %al
	testb	%al, %al
	jne	.LBB5_3
# BB#1:
	movl	$_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB5_3
# BB#2:
	movl	$1065353216, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+4(%rip)
	movl	$1065353216, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+24(%rip)
	movl	$1065353216, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+40(%rip) # imm = 0x3F800000
	movl	$0, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+44(%rip)
	movl	$-1082130432, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+48(%rip) # imm = 0xBF800000
	movups	%xmm0, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+52(%rip)
	movl	$-1082130432, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+68(%rip) # imm = 0xBF800000
	movups	%xmm0, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+72(%rip)
	movl	$3212836864, %eax       # imm = 0xBF800000
	movq	%rax, _ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions+88(%rip)
	movl	$_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, %edi
	callq	__cxa_guard_release
.LBB5_3:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	(%rbx), %rax
	movq	%rsp, %rdx
	movl	$_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, %esi
	movl	$6, %ecx
	movq	%rbx, %rdi
	callq	*112(%rax)
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 80(%rbx)
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 64(%rbx)
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 84(%rbx)
	movss	68(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 68(%rbx)
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 88(%rbx)
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 72(%rbx)
	addq	$96, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv, .Lfunc_end5-_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
	.cfi_endproc

	.globl	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev,@function
_ZN34btPolyhedralConvexAabbCachingShapeC2Ev: # @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV34btPolyhedralConvexAabbCachingShape+16, (%rbx)
	movl	$1065353216, 64(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 68(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 72(%rbx)   # imm = 0x3F800000
	movl	$0, 76(%rbx)
	movl	$-1082130432, 80(%rbx)  # imm = 0xBF800000
	movl	$-1082130432, 84(%rbx)  # imm = 0xBF800000
	movl	$-1082130432, 88(%rbx)  # imm = 0xBF800000
	movl	$0, 92(%rbx)
	movb	$0, 96(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev, .Lfunc_end6-_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 48
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r12, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movss	80(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	subss	%xmm2, %xmm11
	movss	84(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm12
	subss	%xmm8, %xmm12
	movss	88(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm13
	subss	%xmm7, %xmm13
	movss	.LCPI8_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	mulss	%xmm3, %xmm12
	mulss	%xmm3, %xmm13
	addss	%xmm0, %xmm11
	addss	%xmm0, %xmm12
	addss	%xmm0, %xmm13
	addss	%xmm2, %xmm5
	addss	%xmm8, %xmm6
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm3, %xmm4
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movaps	.LCPI8_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm8, %xmm2
	andps	%xmm0, %xmm8
	movss	20(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	andps	%xmm0, %xmm9
	movss	24(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm10   # xmm10 = xmm10[0],xmm7[0],xmm10[1],xmm7[1]
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm1
	andps	%xmm0, %xmm10
	addps	%xmm2, %xmm3
	movss	32(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	andps	%xmm0, %xmm2
	addps	%xmm3, %xmm1
	movsd	48(%r12), %xmm7         # xmm7 = mem[0],zero
	addps	%xmm1, %xmm7
	movss	36(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	andps	%xmm0, %xmm3
	addss	%xmm5, %xmm6
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm11, %xmm2
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm8, %xmm11
	mulss	%xmm12, %xmm3
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm9, %xmm12
	addps	%xmm11, %xmm12
	mulss	%xmm13, %xmm0
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm13
	addps	%xmm12, %xmm13
	addss	56(%r12), %xmm4
	addss	%xmm2, %xmm3
	addss	%xmm3, %xmm0
	movaps	%xmm4, %xmm1
	subss	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm7, %xmm1
	subps	%xmm13, %xmm1
	movlps	%xmm1, (%r15)
	movlps	%xmm2, 8(%r15)
	addps	%xmm7, %xmm13
	addss	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm13, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end8-_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN23btPolyhedralConvexShapeD0Ev,"axG",@progbits,_ZN23btPolyhedralConvexShapeD0Ev,comdat
	.weak	_ZN23btPolyhedralConvexShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN23btPolyhedralConvexShapeD0Ev,@function
_ZN23btPolyhedralConvexShapeD0Ev:       # @_ZN23btPolyhedralConvexShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB9_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN23btPolyhedralConvexShapeD0Ev, .Lfunc_end9-_ZN23btPolyhedralConvexShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end10-_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end11-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end12-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end13:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end13-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end14-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end15-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN34btPolyhedralConvexAabbCachingShapeD0Ev,"axG",@progbits,_ZN34btPolyhedralConvexAabbCachingShapeD0Ev,comdat
	.weak	_ZN34btPolyhedralConvexAabbCachingShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN34btPolyhedralConvexAabbCachingShapeD0Ev,@function
_ZN34btPolyhedralConvexAabbCachingShapeD0Ev: # @_ZN34btPolyhedralConvexAabbCachingShapeD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB16_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN34btPolyhedralConvexAabbCachingShapeD0Ev, .Lfunc_end16-_ZN34btPolyhedralConvexAabbCachingShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTV23btPolyhedralConvexShape,@object # @_ZTV23btPolyhedralConvexShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btPolyhedralConvexShape
	.p2align	3
_ZTV23btPolyhedralConvexShape:
	.quad	0
	.quad	_ZTI23btPolyhedralConvexShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN23btPolyhedralConvexShapeD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	__cxa_pure_virtual
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV23btPolyhedralConvexShape, 216

	.type	_ZTV34btPolyhedralConvexAabbCachingShape,@object # @_ZTV34btPolyhedralConvexAabbCachingShape
	.globl	_ZTV34btPolyhedralConvexAabbCachingShape
	.p2align	3
_ZTV34btPolyhedralConvexAabbCachingShape:
	.quad	0
	.quad	_ZTI34btPolyhedralConvexAabbCachingShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN34btPolyhedralConvexAabbCachingShapeD0Ev
	.quad	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	__cxa_pure_virtual
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV34btPolyhedralConvexAabbCachingShape, 216

	.type	_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions,@object # @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions
	.local	_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions
	.comm	_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions,96,16
	.type	_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions,@object # @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions
	.local	_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions
	.comm	_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions,8,8
	.type	_ZTS23btPolyhedralConvexShape,@object # @_ZTS23btPolyhedralConvexShape
	.globl	_ZTS23btPolyhedralConvexShape
	.p2align	4
_ZTS23btPolyhedralConvexShape:
	.asciz	"23btPolyhedralConvexShape"
	.size	_ZTS23btPolyhedralConvexShape, 26

	.type	_ZTI23btPolyhedralConvexShape,@object # @_ZTI23btPolyhedralConvexShape
	.globl	_ZTI23btPolyhedralConvexShape
	.p2align	4
_ZTI23btPolyhedralConvexShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btPolyhedralConvexShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI23btPolyhedralConvexShape, 24

	.type	_ZTS34btPolyhedralConvexAabbCachingShape,@object # @_ZTS34btPolyhedralConvexAabbCachingShape
	.globl	_ZTS34btPolyhedralConvexAabbCachingShape
	.p2align	4
_ZTS34btPolyhedralConvexAabbCachingShape:
	.asciz	"34btPolyhedralConvexAabbCachingShape"
	.size	_ZTS34btPolyhedralConvexAabbCachingShape, 37

	.type	_ZTI34btPolyhedralConvexAabbCachingShape,@object # @_ZTI34btPolyhedralConvexAabbCachingShape
	.globl	_ZTI34btPolyhedralConvexAabbCachingShape
	.p2align	4
_ZTI34btPolyhedralConvexAabbCachingShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS34btPolyhedralConvexAabbCachingShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI34btPolyhedralConvexAabbCachingShape, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
