	.text
	.file	"hash.bc"
	.globl	hashstore
	.p2align	4, 0x90
	.type	hashstore,@function
hashstore:                              # @hashstore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	leaq	56(%rsp), %r15
	movq	g_trans_table(%rip), %rbx
	movslq	g_norm_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	g_norm_hashkey(%rip), %r10d
	cmpl	%r10d, (%rbx,%rax,8)
	movl	%esi, -4(%rsp)          # 4-byte Spill
	jne	.LBB0_5
# BB#1:
	movl	4(%rbx,%rax,8), %ebp
	cmpl	g_norm_hashkey+4(%rip), %ebp
	jne	.LBB0_5
# BB#2:
	movl	8(%rbx,%rax,8), %ebp
	cmpl	g_norm_hashkey+8(%rip), %ebp
	jne	.LBB0_5
# BB#3:
	movl	12(%rbx,%rax,8), %ebp
	cmpl	g_norm_hashkey+12(%rip), %ebp
	jne	.LBB0_5
# BB#4:                                 # %._crit_edge114
	movl	%r8d, %edi
	movq	%r15, %r8
	movl	%r12d, -8(%rsp)         # 4-byte Spill
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
	leaq	16(%rbx,%rax,8), %r14
	jmp	.LBB0_7
.LBB0_5:
	cmpl	%ecx, 16(%rbx,%rax,8)
	jbe	.LBB0_6
# BB#11:
	movslq	g_flipV_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	g_flipV_hashkey(%rip), %r14d
	cmpl	%r14d, (%rbx,%rax,8)
	jne	.LBB0_16
# BB#12:
	movl	4(%rbx,%rax,8), %ebp
	cmpl	g_flipV_hashkey+4(%rip), %ebp
	jne	.LBB0_16
# BB#13:
	movl	8(%rbx,%rax,8), %ebp
	cmpl	g_flipV_hashkey+8(%rip), %ebp
	jne	.LBB0_16
# BB#14:
	movl	12(%rbx,%rax,8), %ebp
	cmpl	g_flipV_hashkey+12(%rip), %ebp
	jne	.LBB0_16
# BB#15:                                # %._crit_edge112
	movq	%r15, %rbp
	movl	%r12d, %r10d
	movl	%edx, %edi
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
	leaq	16(%rbx,%rax,8), %rdx
	jmp	.LBB0_18
.LBB0_6:                                # %._crit_edge115
	movl	%r8d, %edi
	movq	%r15, %r8
	movl	%r12d, -8(%rsp)         # 4-byte Spill
	leaq	16(%rbx,%rax,8), %r14
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
.LBB0_7:
	leaq	(%rbx,%rax,8), %r11
	movl	%r10d, (%r11)
	movl	g_norm_hashkey+4(%rip), %ebp
	movl	%ebp, (%r13)
	movl	g_norm_hashkey+8(%rip), %ebp
	movl	%ebp, (%r12)
	movl	g_norm_hashkey+12(%rip), %ebp
	movl	%ebp, (%r15)
	movl	%ecx, (%r14)
	movl	(%r8), %ecx
	movl	4(%r8), %esi
	decl	%esi
	movslq	%r9d, %rbp
	imull	g_board_size(,%rbp,4), %esi
	leal	255(%rcx,%rsi), %ecx
	movb	%cl, 20(%rbx,%rax,8)
	movzbl	23(%rbx,%rax,8), %ecx
	andl	$127, %edi
	andl	$-64, %ecx
	shll	$16, %ecx
	orl	%edi, %ecx
	andl	$1, %r9d
	shll	$7, %r9d
	orl	%ecx, %r9d
	movl	-8(%rsp), %edi          # 4-byte Reload
	movl	%edi, %ecx
	shll	$8, %ecx
	andl	$4194048, %ecx          # imm = 0x3FFF00
	orl	%r9d, %ecx
	movw	%cx, 21(%rbx,%rax,8)
	movl	%ecx, %esi
	shrl	$16, %esi
	movb	%sil, 23(%rbx,%rax,8)
	cmpl	%edx, %edi
	jge	.LBB0_8
# BB#9:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	cmpl	-4(%rsp), %edi          # 4-byte Folded Reload
	jmp	.LBB0_10
.LBB0_16:
	cmpl	%ecx, 16(%rbx,%rax,8)
	jbe	.LBB0_17
# BB#21:
	movslq	g_flipH_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	g_flipH_hashkey(%rip), %r14d
	cmpl	%r14d, (%rbx,%rax,8)
	jne	.LBB0_26
# BB#22:
	movl	4(%rbx,%rax,8), %ebp
	cmpl	g_flipH_hashkey+4(%rip), %ebp
	jne	.LBB0_26
# BB#23:
	movl	8(%rbx,%rax,8), %ebp
	cmpl	g_flipH_hashkey+8(%rip), %ebp
	jne	.LBB0_26
# BB#24:
	movl	12(%rbx,%rax,8), %ebp
	cmpl	g_flipH_hashkey+12(%rip), %ebp
	jne	.LBB0_26
# BB#25:                                # %._crit_edge110
	movq	%r15, %rbp
	movl	%r12d, %r10d
	movl	%edx, %edi
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
	leaq	16(%rbx,%rax,8), %rdx
	jmp	.LBB0_28
.LBB0_17:                               # %._crit_edge113
	movq	%r15, %rbp
	movl	%r12d, %r10d
	movl	%edx, %edi
	leaq	16(%rbx,%rax,8), %rdx
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
.LBB0_18:
	leaq	(%rbx,%rax,8), %r11
	movl	%r14d, (%r11)
	movl	g_flipV_hashkey+4(%rip), %esi
	movl	%esi, (%r13)
	movl	g_flipV_hashkey+8(%rip), %esi
	movl	%esi, (%r12)
	movl	g_flipV_hashkey+12(%rip), %esi
	jmp	.LBB0_19
.LBB0_26:
	cmpl	%ecx, 16(%rbx,%rax,8)
	jbe	.LBB0_27
# BB#29:
	movslq	g_flipVH_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %r11
	movl	g_flipVH_hashkey(%rip), %ebp
	cmpl	%ebp, (%rbx,%r11,8)
	jne	.LBB0_34
# BB#30:
	movl	4(%rbx,%r11,8), %eax
	cmpl	g_flipVH_hashkey+4(%rip), %eax
	jne	.LBB0_34
# BB#31:
	movl	8(%rbx,%r11,8), %eax
	cmpl	g_flipVH_hashkey+8(%rip), %eax
	jne	.LBB0_34
# BB#32:
	movl	12(%rbx,%r11,8), %eax
	cmpl	g_flipVH_hashkey+12(%rip), %eax
	jne	.LBB0_34
# BB#33:                                # %._crit_edge
	movl	%r8d, %r10d
	movq	%r15, %r8
	movl	%r12d, %edi
	movl	%edx, -8(%rsp)          # 4-byte Spill
	leaq	4(%rbx,%r11,8), %r13
	leaq	8(%rbx,%r11,8), %r12
	leaq	12(%rbx,%r11,8), %rax
	leaq	16(%rbx,%r11,8), %r14
	jmp	.LBB0_36
.LBB0_27:                               # %._crit_edge111
	movq	%r15, %rbp
	movl	%r12d, %r10d
	movl	%edx, %edi
	leaq	16(%rbx,%rax,8), %rdx
	leaq	4(%rbx,%rax,8), %r13
	leaq	8(%rbx,%rax,8), %r12
	leaq	12(%rbx,%rax,8), %r15
.LBB0_28:
	leaq	(%rbx,%rax,8), %r11
	movl	%r14d, (%r11)
	movl	g_flipH_hashkey+4(%rip), %esi
	movl	%esi, (%r13)
	movl	g_flipH_hashkey+8(%rip), %esi
	movl	%esi, (%r12)
	movl	g_flipH_hashkey+12(%rip), %esi
.LBB0_19:
	movl	%esi, (%r15)
	movl	%ecx, (%rdx)
	movl	(%rbp), %ecx
	movl	4(%rbp), %edx
	decl	%edx
	movslq	%r9d, %rsi
	imull	g_board_size(,%rsi,4), %edx
	leal	255(%rcx,%rdx), %ecx
	movb	%cl, 20(%rbx,%rax,8)
	movzbl	23(%rbx,%rax,8), %ecx
	andl	$127, %r8d
	andl	$-64, %ecx
	shll	$16, %ecx
	orl	%r8d, %ecx
	andl	$1, %r9d
	shll	$7, %r9d
	orl	%ecx, %r9d
	movl	%r10d, %ecx
	shll	$8, %ecx
	andl	$4194048, %ecx          # imm = 0x3FFF00
	orl	%r9d, %ecx
	movw	%cx, 21(%rbx,%rax,8)
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 23(%rbx,%rax,8)
	cmpl	%edi, %r10d
	jge	.LBB0_8
# BB#20:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	cmpl	-4(%rsp), %r10d         # 4-byte Folded Reload
.LBB0_10:
	movl	$4194304, %eax          # imm = 0x400000
	movl	$-8388608, %edx         # imm = 0xFF800000
	cmovgl	%eax, %edx
	orl	%ecx, %edx
	movw	%dx, 21(%r11)
	shrl	$16, %edx
	movb	%dl, 23(%r11)
	jmp	.LBB0_39
.LBB0_8:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	movw	%cx, 21(%r11)
	shrl	$16, %ecx
	movb	%cl, 23(%r11)
.LBB0_39:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_34:
	cmpl	%ecx, 16(%rbx,%r11,8)
	ja	.LBB0_39
# BB#35:                                # %._crit_edge109
	movl	%r8d, %r10d
	movq	%r15, %r8
	movl	%r12d, %edi
	movl	%edx, -8(%rsp)          # 4-byte Spill
	leaq	16(%rbx,%r11,8), %r14
	leaq	4(%rbx,%r11,8), %r13
	leaq	8(%rbx,%r11,8), %r12
	leaq	12(%rbx,%r11,8), %rax
.LBB0_36:
	leaq	(%rbx,%r11,8), %r15
	movl	%ebp, (%r15)
	movl	g_flipVH_hashkey+4(%rip), %esi
	movl	%esi, (%r13)
	movl	g_flipVH_hashkey+8(%rip), %esi
	movl	%esi, (%r12)
	movl	g_flipVH_hashkey+12(%rip), %esi
	movl	%esi, (%rax)
	movl	%ecx, (%r14)
	movl	(%r8), %eax
	movl	4(%r8), %ecx
	decl	%ecx
	movslq	%r9d, %rdx
	imull	g_board_size(,%rdx,4), %ecx
	leal	255(%rax,%rcx), %eax
	movb	%al, 20(%rbx,%r11,8)
	movzbl	23(%rbx,%r11,8), %eax
	andl	$127, %r10d
	andl	$-64, %eax
	shll	$16, %eax
	orl	%r10d, %eax
	andl	$1, %edx
	shll	$7, %edx
	orl	%eax, %edx
	movl	%edi, %ecx
	shll	$8, %ecx
	andl	$4194048, %ecx          # imm = 0x3FFF00
	orl	%edx, %ecx
	movw	%cx, 21(%rbx,%r11,8)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 23(%rbx,%r11,8)
	cmpl	-8(%rsp), %edi          # 4-byte Folded Reload
	jge	.LBB0_37
# BB#38:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	cmpl	-4(%rsp), %edi          # 4-byte Folded Reload
	movl	$4194304, %eax          # imm = 0x400000
	movl	$-8388608, %edx         # imm = 0xFF800000
	cmovgl	%eax, %edx
	orl	%ecx, %edx
	movw	%dx, 21(%r15)
	shrl	$16, %edx
	movb	%dl, 23(%r15)
	jmp	.LBB0_39
.LBB0_37:
	andl	$4194303, %ecx          # imm = 0x3FFFFF
	movw	%cx, 21(%r15)
	shrl	$16, %ecx
	movb	%cl, 23(%r15)
	jmp	.LBB0_39
.Lfunc_end0:
	.size	hashstore, .Lfunc_end0-hashstore
	.cfi_endproc

	.globl	hashlookup
	.p2align	4, 0x90
	.type	hashlookup,@function
hashlookup:                             # @hashlookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdx, %r10
	movq	g_trans_table(%rip), %rbx
	movslq	g_norm_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	(%rbx,%rax,8), %edx
	cmpl	g_norm_hashkey(%rip), %edx
	jne	.LBB1_16
# BB#1:
	movl	4(%rbx,%rax,8), %edx
	cmpl	g_norm_hashkey+4(%rip), %edx
	jne	.LBB1_16
# BB#2:
	movl	8(%rbx,%rax,8), %edx
	cmpl	g_norm_hashkey+8(%rip), %edx
	jne	.LBB1_16
# BB#3:
	movl	12(%rbx,%rax,8), %edx
	cmpl	g_norm_hashkey+12(%rip), %edx
	jne	.LBB1_16
# BB#4:
	movzwl	21(%rbx,%rax,8), %edx
	shrl	$7, %edx
	andl	$1, %edx
	cmpl	%r9d, %edx
	jne	.LBB1_16
# BB#5:
	leaq	21(%rbx,%rax,8), %r14
	movzbl	20(%rbx,%rax,8), %r11d
	movslq	%r9d, %r15
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%eax
	movl	%eax, 4(%r8)
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%edx
	movl	%edx, (%r8)
	movzwl	(%r14), %edx
	movzbl	2(%r14), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	andl	$127, %edx
	cmpl	%ecx, %edx
	jl	.LBB1_16
# BB#6:
	movl	%eax, %edx
	andl	$12582912, %edx         # imm = 0xC00000
	shrl	$22, %edx
	testb	%dl, %dl
	je	.LBB1_10
# BB#7:
	cmpb	$2, %dl
	je	.LBB1_14
# BB#8:
	cmpb	$1, %dl
	je	.LBB1_9
.LBB1_16:
	movslq	g_flipV_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	(%rbx,%rax,8), %edx
	cmpl	g_flipV_hashkey(%rip), %edx
	jne	.LBB1_25
# BB#17:
	movl	4(%rbx,%rax,8), %edx
	cmpl	g_flipV_hashkey+4(%rip), %edx
	jne	.LBB1_25
# BB#18:
	movl	8(%rbx,%rax,8), %edx
	cmpl	g_flipV_hashkey+8(%rip), %edx
	jne	.LBB1_25
# BB#19:
	movl	12(%rbx,%rax,8), %edx
	cmpl	g_flipV_hashkey+12(%rip), %edx
	jne	.LBB1_25
# BB#20:
	movzwl	21(%rbx,%rax,8), %edx
	shrl	$7, %edx
	andl	$1, %edx
	cmpl	%r9d, %edx
	jne	.LBB1_25
# BB#21:
	leaq	21(%rbx,%rax,8), %r14
	movzbl	20(%rbx,%rax,8), %r11d
	movslq	%r9d, %r15
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%eax
	movl	%eax, 4(%r8)
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%edx
	movl	%edx, (%r8)
	movzwl	(%r14), %edx
	movzbl	2(%r14), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	andl	$127, %edx
	cmpl	%ecx, %edx
	jge	.LBB1_22
.LBB1_25:
	movslq	g_flipH_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	(%rbx,%rax,8), %edx
	cmpl	g_flipH_hashkey(%rip), %edx
	jne	.LBB1_34
# BB#26:
	movl	4(%rbx,%rax,8), %edx
	cmpl	g_flipH_hashkey+4(%rip), %edx
	jne	.LBB1_34
# BB#27:
	movl	8(%rbx,%rax,8), %edx
	cmpl	g_flipH_hashkey+8(%rip), %edx
	jne	.LBB1_34
# BB#28:
	movl	12(%rbx,%rax,8), %edx
	cmpl	g_flipH_hashkey+12(%rip), %edx
	jne	.LBB1_34
# BB#29:
	movzwl	21(%rbx,%rax,8), %edx
	shrl	$7, %edx
	andl	$1, %edx
	cmpl	%r9d, %edx
	jne	.LBB1_34
# BB#30:
	leaq	21(%rbx,%rax,8), %r14
	movzbl	20(%rbx,%rax,8), %r11d
	movslq	%r9d, %r15
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%eax
	movl	%eax, 4(%r8)
	xorl	%edx, %edx
	movl	%r11d, %eax
	idivl	g_board_size(,%r15,4)
	incl	%edx
	movl	%edx, (%r8)
	movzwl	(%r14), %edx
	movzbl	2(%r14), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	andl	$127, %edx
	cmpl	%ecx, %edx
	jl	.LBB1_34
# BB#31:
	movl	%eax, %edx
	andl	$12582912, %edx         # imm = 0xC00000
	shrl	$22, %edx
	testb	%dl, %dl
	je	.LBB1_46
# BB#32:
	cmpb	$2, %dl
	je	.LBB1_14
# BB#33:
	cmpb	$1, %dl
	je	.LBB1_9
.LBB1_34:
	movslq	g_flipVH_hashkey+16(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	movl	(%rbx,%rax,8), %edx
	xorl	%r11d, %r11d
	cmpl	g_flipVH_hashkey(%rip), %edx
	jne	.LBB1_55
# BB#35:
	movl	4(%rbx,%rax,8), %edx
	cmpl	g_flipVH_hashkey+4(%rip), %edx
	jne	.LBB1_55
# BB#36:
	movl	8(%rbx,%rax,8), %edx
	cmpl	g_flipVH_hashkey+8(%rip), %edx
	jne	.LBB1_55
# BB#37:
	movl	12(%rbx,%rax,8), %edx
	cmpl	g_flipVH_hashkey+12(%rip), %edx
	jne	.LBB1_55
# BB#38:
	movzwl	21(%rbx,%rax,8), %edx
	shrl	$7, %edx
	andl	$1, %edx
	cmpl	%r9d, %edx
	jne	.LBB1_55
# BB#39:
	leaq	21(%rbx,%rax,8), %r14
	movzbl	20(%rbx,%rax,8), %ebp
	movslq	%r9d, %rbx
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movl	%ebp, %eax
	idivl	g_board_size(,%rbx,4)
	incl	%eax
	movl	%eax, 4(%r8)
	xorl	%edx, %edx
	movl	%ebp, %eax
	idivl	g_board_size(,%rbx,4)
	incl	%edx
	movl	%edx, (%r8)
	movzwl	(%r14), %edx
	movzbl	2(%r14), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	andl	$127, %edx
	cmpl	%ecx, %edx
	jl	.LBB1_55
# BB#40:
	movl	%eax, %ecx
	andl	$12582912, %ecx         # imm = 0xC00000
	shrl	$22, %ecx
	testb	%cl, %cl
	je	.LBB1_49
# BB#41:
	cmpb	$2, %cl
	je	.LBB1_52
# BB#42:
	cmpb	$1, %cl
	je	.LBB1_9
	jmp	.LBB1_55
.LBB1_22:
	movl	%eax, %edx
	andl	$12582912, %edx         # imm = 0xC00000
	shrl	$22, %edx
	testb	%dl, %dl
	je	.LBB1_43
# BB#23:
	cmpb	$2, %dl
	je	.LBB1_14
# BB#24:
	cmpb	$1, %dl
	jne	.LBB1_25
.LBB1_9:
	shll	$10, %eax
	sarl	$18, %eax
	jmp	.LBB1_11
.LBB1_14:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%rsi), %eax
	jle	.LBB1_11
# BB#15:
	xorl	%r11d, %r11d
	cmpl	(%r10), %eax
	jl	.LBB1_54
	jmp	.LBB1_55
.LBB1_10:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%r10), %eax
	jge	.LBB1_11
# BB#12:
	xorl	%r11d, %r11d
	cmpl	(%rsi), %eax
	jle	.LBB1_55
# BB#13:
	movl	%eax, (%rsi)
	jmp	.LBB1_55
.LBB1_46:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%r10), %eax
	jge	.LBB1_11
# BB#47:
	xorl	%r11d, %r11d
	cmpl	(%rsi), %eax
	jle	.LBB1_55
# BB#48:
	movl	%eax, (%rsi)
	jmp	.LBB1_55
.LBB1_52:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%rsi), %eax
	jle	.LBB1_11
# BB#53:
	cmpl	(%r10), %eax
	jge	.LBB1_55
.LBB1_54:
	movl	%eax, (%r10)
	jmp	.LBB1_55
.LBB1_43:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%r10), %eax
	jge	.LBB1_11
# BB#44:
	xorl	%r11d, %r11d
	cmpl	(%rsi), %eax
	jle	.LBB1_55
# BB#45:
	movl	%eax, (%rsi)
	jmp	.LBB1_55
.LBB1_49:
	shll	$10, %eax
	sarl	$18, %eax
	cmpl	(%r10), %eax
	jge	.LBB1_11
# BB#50:
	cmpl	(%rsi), %eax
	jle	.LBB1_55
# BB#51:
	movl	%eax, (%rsi)
	jmp	.LBB1_55
.LBB1_11:
	movl	%eax, (%rdi)
	movl	$1, %r11d
.LBB1_55:
	movl	%r11d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hashlookup, .Lfunc_end1-hashlookup
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
