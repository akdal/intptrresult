	.text
	.file	"scimark2.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4611686018427387904     # double 2
.LCPI0_1:
	.quad	4617315517961601024     # double 5
.LCPI0_2:
	.quad	4711630319722168320     # double 1.0E+7
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %r13d
	movl	$101010, %edi           # imm = 0x18A92
	callq	new_Random_seed
	movq	%rax, %rbx
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	cmpl	$2, %r13d
	jl	.LBB0_1
# BB#2:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rbp
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_9
# BB#3:
	cmpb	$45, (%rbp)
	jne	.LBB0_6
# BB#4:
	cmpb	$104, 1(%rbp)
	jne	.LBB0_6
# BB#5:
	cmpb	$0, 2(%rbp)
	je	.LBB0_9
.LBB0_6:                                # %.thread
	movl	$.L.str.3, %esi
	movq	%rbp, %rdi
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movl	$1000, %eax             # imm = 0x3E8
	movl	$100, %ebp
	cmovel	%eax, %ebp
	movl	$1000000, %edx          # imm = 0xF4240
	movl	$5000, %r14d            # imm = 0x1388
	cmovel	%edx, %r14d
	movl	$100000, %r15d          # imm = 0x186A0
	cmovnel	%eax, %r15d
	movl	$1048576, %eax          # imm = 0x100000
	movl	$1024, %r12d            # imm = 0x400
	cmovel	%eax, %r12d
	incl	%ecx
	cmpl	%r13d, %ecx
	jge	.LBB0_8
# BB#7:
	movl	%ecx, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rdi
	xorl	%esi, %esi
	callq	strtod
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	jmp	.LBB0_8
.LBB0_1:
	movl	$1024, %r12d            # imm = 0x400
	movl	$1000, %r15d            # imm = 0x3E8
	movl	$5000, %r14d            # imm = 0x1388
	movl	$100, %ebp
.LBB0_8:
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.L.str.4, %edi
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	movl	%r12d, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbx, %rsi
	callq	kernel_measureFFT
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbx, %rsi
	callq	kernel_measureSOR
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	kernel_measureMonteCarlo
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movl	%r15d, %edi
	movl	%r14d, %esi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbx, %rdx
	callq	kernel_measureSparseMatMult
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rbx, %rsi
	callq	kernel_measureLU
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	32(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	40(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	divsd	.LCPI0_1(%rip), %xmm1
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	movl	$.Lstr, %edi
	callq	puts
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.7, %edi
	movb	$1, %al
	movl	%r12d, %esi
	callq	printf
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.8, %edi
	movb	$1, %al
	movl	%ebp, %esi
	movl	%ebp, %edx
	callq	printf
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.9, %edi
	movb	$1, %al
	callq	printf
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.10, %edi
	movb	$1, %al
	movl	%r15d, %esi
	movl	%r14d, %edx
	callq	printf
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.11, %edi
	movb	$1, %al
	movl	%ebp, %esi
	movl	%ebp, %edx
	callq	printf
	movq	%rbx, %rdi
	callq	Random_delete
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	print_banner
	.p2align	4, 0x90
	.type	print_banner,@function
print_banner:                           # @print_banner
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end1:
	.size	print_banner, .Lfunc_end1-print_banner
	.cfi_endproc

	.type	RESOLUTION_DEFAULT,@object # @RESOLUTION_DEFAULT
	.section	.rodata,"a",@progbits
	.globl	RESOLUTION_DEFAULT
	.p2align	3
RESOLUTION_DEFAULT:
	.quad	4611686018427387904     # double 2
	.size	RESOLUTION_DEFAULT, 8

	.type	RANDOM_SEED,@object     # @RANDOM_SEED
	.globl	RANDOM_SEED
	.p2align	2
RANDOM_SEED:
	.long	101010                  # 0x18a92
	.size	RANDOM_SEED, 4

	.type	FFT_SIZE,@object        # @FFT_SIZE
	.globl	FFT_SIZE
	.p2align	2
FFT_SIZE:
	.long	1024                    # 0x400
	.size	FFT_SIZE, 4

	.type	SOR_SIZE,@object        # @SOR_SIZE
	.globl	SOR_SIZE
	.p2align	2
SOR_SIZE:
	.long	100                     # 0x64
	.size	SOR_SIZE, 4

	.type	SPARSE_SIZE_M,@object   # @SPARSE_SIZE_M
	.globl	SPARSE_SIZE_M
	.p2align	2
SPARSE_SIZE_M:
	.long	1000                    # 0x3e8
	.size	SPARSE_SIZE_M, 4

	.type	SPARSE_SIZE_nz,@object  # @SPARSE_SIZE_nz
	.globl	SPARSE_SIZE_nz
	.p2align	2
SPARSE_SIZE_nz:
	.long	5000                    # 0x1388
	.size	SPARSE_SIZE_nz, 4

	.type	LU_SIZE,@object         # @LU_SIZE
	.globl	LU_SIZE
	.p2align	2
LU_SIZE:
	.long	100                     # 0x64
	.size	LU_SIZE, 4

	.type	LG_FFT_SIZE,@object     # @LG_FFT_SIZE
	.globl	LG_FFT_SIZE
	.p2align	2
LG_FFT_SIZE:
	.long	1048576                 # 0x100000
	.size	LG_FFT_SIZE, 4

	.type	LG_SOR_SIZE,@object     # @LG_SOR_SIZE
	.globl	LG_SOR_SIZE
	.p2align	2
LG_SOR_SIZE:
	.long	1000                    # 0x3e8
	.size	LG_SOR_SIZE, 4

	.type	LG_SPARSE_SIZE_M,@object # @LG_SPARSE_SIZE_M
	.globl	LG_SPARSE_SIZE_M
	.p2align	2
LG_SPARSE_SIZE_M:
	.long	100000                  # 0x186a0
	.size	LG_SPARSE_SIZE_M, 4

	.type	LG_SPARSE_SIZE_nz,@object # @LG_SPARSE_SIZE_nz
	.globl	LG_SPARSE_SIZE_nz
	.p2align	2
LG_SPARSE_SIZE_nz:
	.long	1000000                 # 0xf4240
	.size	LG_SPARSE_SIZE_nz, 4

	.type	LG_LU_SIZE,@object      # @LG_LU_SIZE
	.globl	LG_LU_SIZE
	.p2align	2
LG_LU_SIZE:
	.long	1000                    # 0x3e8
	.size	LG_LU_SIZE, 4

	.type	TINY_FFT_SIZE,@object   # @TINY_FFT_SIZE
	.globl	TINY_FFT_SIZE
	.p2align	2
TINY_FFT_SIZE:
	.long	16                      # 0x10
	.size	TINY_FFT_SIZE, 4

	.type	TINY_SOR_SIZE,@object   # @TINY_SOR_SIZE
	.globl	TINY_SOR_SIZE
	.p2align	2
TINY_SOR_SIZE:
	.long	10                      # 0xa
	.size	TINY_SOR_SIZE, 4

	.type	TINY_SPARSE_SIZE_M,@object # @TINY_SPARSE_SIZE_M
	.globl	TINY_SPARSE_SIZE_M
	.p2align	2
TINY_SPARSE_SIZE_M:
	.long	10                      # 0xa
	.size	TINY_SPARSE_SIZE_M, 4

	.type	TINY_SPARSE_SIZE_N,@object # @TINY_SPARSE_SIZE_N
	.globl	TINY_SPARSE_SIZE_N
	.p2align	2
TINY_SPARSE_SIZE_N:
	.long	10                      # 0xa
	.size	TINY_SPARSE_SIZE_N, 4

	.type	TINY_SPARSE_SIZE_nz,@object # @TINY_SPARSE_SIZE_nz
	.globl	TINY_SPARSE_SIZE_nz
	.p2align	2
TINY_SPARSE_SIZE_nz:
	.long	50                      # 0x32
	.size	TINY_SPARSE_SIZE_nz, 4

	.type	TINY_LU_SIZE,@object    # @TINY_LU_SIZE
	.globl	TINY_LU_SIZE
	.p2align	2
TINY_LU_SIZE:
	.long	10                      # 0xa
	.size	TINY_LU_SIZE, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-help"
	.size	.L.str, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Usage: [-large] [minimum_time]\n"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"-large"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Using %10.2f seconds min time per kenel.\n"
	.size	.L.str.4, 42

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Composite Score:        %8.2f\n"
	.size	.L.str.6, 31

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FFT             Mflops: %8.2f    (N=%d)\n"
	.size	.L.str.7, 41

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SOR             Mflops: %8.2f    (%d x %d)\n"
	.size	.L.str.8, 44

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"MonteCarlo:     Mflops: %8.2f\n"
	.size	.L.str.9, 31

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Sparse matmult  Mflops: %8.2f    (N=%d, nz=%d)\n"
	.size	.L.str.10, 48

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"LU              Mflops: %8.2f    (M=%d, N=%d)\n"
	.size	.L.str.11, 47

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"NOTE!!! All Mflops disabled to prevent diffs from failing!"
	.size	.Lstr, 59

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"** SciMark2 Numeric Benchmark, see http://math.nist.gov/scimark **"
	.size	.Lstr.2, 67

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"** for details. (Results can be submitted to pozo@nist.gov)     **"
	.size	.Lstr.3, 67

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"**                                                              **"
	.size	.Lstr.4, 67


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
