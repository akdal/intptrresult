	.text
	.file	"OpenArchive.bc"
	.globl	_Z22GetArchiveItemBoolPropP10IInArchivejjRb
	.p2align	4, 0x90
	.type	_Z22GetArchiveItemBoolPropP10IInArchivejjRb,@function
_Z22GetArchiveItemBoolPropP10IInArchivejjRb: # @_Z22GetArchiveItemBoolPropP10IInArchivejjRb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movw	$0, 8(%rsp)
	movw	$0, 10(%rsp)
	movb	$0, (%rbx)
	movq	(%rdi), %rax
.Ltmp0:
	leaq	8(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp1:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB0_6
# BB#2:
	movzwl	8(%rsp), %eax
	testw	%ax, %ax
	je	.LBB0_5
# BB#3:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB0_6
# BB#4:
	cmpw	$0, 16(%rsp)
	setne	(%rbx)
.LBB0_5:
	xorl	%ebp, %ebp
.LBB0_6:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB0_7:
.Ltmp2:
	movq	%rax, %rbx
.Ltmp3:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp4:
# BB#8:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_9:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z22GetArchiveItemBoolPropP10IInArchivejjRb, .Lfunc_end0-_Z22GetArchiveItemBoolPropP10IInArchivejjRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_Z19IsArchiveItemFolderP10IInArchivejRb
	.p2align	4, 0x90
	.type	_Z19IsArchiveItemFolderP10IInArchivejRb,@function
_Z19IsArchiveItemFolderP10IInArchivejRb: # @_Z19IsArchiveItemFolderP10IInArchivejRb
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movw	$0, 8(%rsp)
	movw	$0, 10(%rsp)
	movb	$0, (%rbx)
	movq	(%rdi), %rax
.Ltmp6:
	leaq	8(%rsp), %rcx
	movl	$6, %edx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp7:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB2_6
# BB#2:
	movzwl	8(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_5
# BB#3:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB2_6
# BB#4:
	cmpw	$0, 16(%rsp)
	setne	(%rbx)
.LBB2_5:
	xorl	%ebp, %ebp
.LBB2_6:                                # %_Z22GetArchiveItemBoolPropP10IInArchivejjRb.exit
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB2_7:
.Ltmp8:
	movq	%rax, %rbx
.Ltmp9:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp10:
# BB#8:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.i
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_9:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_Z19IsArchiveItemFolderP10IInArchivejRb, .Lfunc_end2-_Z19IsArchiveItemFolderP10IInArchivejRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE,@function
_ZNK4CArc11GetItemPathEjR11CStringBaseIwE: # @_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 112
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %r12
	movl	$0, 40(%rsp)
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp12:
	leaq	40(%rsp), %rcx
	movl	$3, %edx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp13:
# BB#1:
	testl	%r13d, %r13d
	je	.LBB3_2
.LBB3_42:
	leaq	40(%rsp), %rdi
.LBB3_43:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.LBB3_44:
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:
	movzwl	40(%rsp), %eax
	testw	%ax, %ax
	je	.LBB3_15
# BB#3:
	movl	$-2147467259, %r13d     # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB3_42
# BB#4:
	movq	48(%rsp), %rbp
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	xorl	%r13d, %r13d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_5
# BB#6:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	12(%r14), %eax
	cmpl	%r13d, %eax
	je	.LBB3_12
# BB#7:
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp14:
	callq	_Znam
.Ltmp15:
# BB#8:                                 # %.noexc
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB3_11
# BB#9:                                 # %.noexc
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB3_11
# BB#10:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	8(%r14), %rcx
.LBB3_11:                               # %._crit_edge16.i.i
	movq	%rax, (%r14)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 12(%r14)
	movq	%rax, %rbx
.LBB3_12:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%r13d
	.p2align	4, 0x90
.LBB3_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	addq	$4, %rbp
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB3_13
# BB#14:                                # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%r13d, 8(%r14)
	jmp	.LBB3_16
.LBB3_15:
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movl	$0, (%rax)
.LBB3_16:
	leaq	40(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%r13d, %r13d
	cmpl	$0, 8(%r14)
	jne	.LBB3_44
# BB#17:
	leaq	24(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB3_26
# BB#18:
	movl	$0, 8(%r14)
	movq	(%r14), %rbp
	movl	$0, (%rbp)
	movslq	32(%r12), %rbx
	incq	%rbx
	movl	12(%r14), %ecx
	cmpl	%ecx, %ebx
	je	.LBB3_23
# BB#19:
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB3_22
# BB#20:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_22
# BB#21:                                # %._crit_edge.thread.i.i38
	movq	%rbp, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movslq	8(%r14), %rcx
.LBB3_22:                               # %._crit_edge16.i.i39
	movq	%rax, (%r14)
	movl	$0, (%rax,%rcx,4)
	movl	%ebx, 12(%r14)
	movq	%rax, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB3_23:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i40
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB3_24
# BB#25:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	32(%r12), %eax
	movl	%eax, 8(%r14)
.LBB3_26:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movl	$0, 24(%rsp)
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp19:
	leaq	24(%rsp), %rcx
	movl	$5, %edx
	movl	%r15d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp20:
# BB#27:
	testl	%ebp, %ebp
	je	.LBB3_28
.LBB3_40:                               # %.thread
	leaq	24(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %r13d
	jmp	.LBB3_44
.LBB3_28:
	movzwl	24(%rsp), %eax
	testw	%ax, %ax
	je	.LBB3_37
# BB#29:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB3_40
# BB#30:
.Ltmp21:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp22:
# BB#31:
	movq	(%r14), %rax
	movslq	8(%r14), %rcx
	movl	$46, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r14)
	movl	$0, 4(%rax,%rcx,4)
	movl	$-1, %ebp
	movq	32(%rsp), %rbx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB3_32:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_32
# BB#33:                                # %_Z11MyStringLenIwEiPKT_.exit.i31
.Ltmp23:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp24:
# BB#34:                                # %.noexc34
	movslq	8(%r14), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r14), %rcx
	.p2align	4, 0x90
.LBB3_35:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB3_35
# BB#36:                                # %_ZN11CStringBaseIwEpLEPKw.exit
	addl	%eax, %ebp
	movl	%ebp, 8(%r14)
.LBB3_37:
	leaq	24(%rsp), %rdi
	jmp	.LBB3_43
.LBB3_41:
.Ltmp25:
	movq	%rax, %rbx
.Ltmp26:
	leaq	24(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp27:
	jmp	.LBB3_39
.LBB3_38:
.Ltmp16:
	movq	%rax, %rbx
.Ltmp17:
	leaq	40(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp18:
.LBB3_39:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_45:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE, .Lfunc_end3-_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp15         #   Call between .Ltmp15 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp24-.Ltmp21         #   Call between .Ltmp21 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp18-.Ltmp26         #   Call between .Ltmp26 and .Ltmp18
	.long	.Ltmp28-.Lfunc_begin2   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Lfunc_end3-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb
	.p2align	4, 0x90
	.type	_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb,@function
_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb: # @_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movw	$0, 8(%rsp)
	movw	$0, 10(%rsp)
	movb	$0, (%r14)
	movq	$0, (%r15)
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp29:
	leaq	8(%rsp), %rcx
	movl	$12, %edx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp30:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB4_9
# BB#2:
	movzwl	8(%rsp), %eax
	testw	%ax, %ax
	je	.LBB4_5
# BB#3:
	movl	$-2147467259, %ebp      # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB4_9
# BB#4:
	movq	16(%rsp), %rax
	jmp	.LBB4_7
.LBB4_5:
	cmpb	$0, 56(%rbx)
	je	.LBB4_8
# BB#6:
	movq	48(%rbx), %rax
.LBB4_7:                                # %.sink.split
	movq	%rax, (%r15)
	movb	$1, (%r14)
.LBB4_8:
	xorl	%ebp, %ebp
.LBB4_9:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_10:
.Ltmp31:
	movq	%rax, %rbx
.Ltmp32:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp33:
# BB#11:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_12:
.Ltmp34:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb, .Lfunc_end4-_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp30         #   Call between .Ltmp30 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp33     #   Call between .Ltmp33 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	105                     # 0x69
	.long	115                     # 0x73
	.long	111                     # 0x6f
	.long	0                       # 0x0
.LCPI5_1:
	.long	117                     # 0x75
	.long	100                     # 0x64
	.long	102                     # 0x66
	.long	0                       # 0x0
.LCPI5_2:
	.zero	16
	.text
	.globl	_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback,@function
_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback: # @_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi38:
	.cfi_def_cfa_offset 496
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movq	%rcx, %r12
	movl	%edx, %ebp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%r14)
.LBB5_2:                                # %_ZN9CMyComPtrI10IInArchiveE7ReleaseEv.exit
	movl	$0, 72(%r14)
	movq	64(%r14), %rax
	movl	$0, (%rax)
	leaq	8(%r14), %rsi
	leaq	144(%rsp), %rdi
	callq	_Z23ExtractFileNameFromPathRK11CStringBaseIwE
.Ltmp35:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp36:
# BB#3:
	movl	$0, (%r13)
	movslq	152(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB5_16
# BB#4:
	movq	144(%rsp), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	addq	$-4, %rdx
	jne	.LBB5_5
	jmp	.LBB5_16
.LBB5_7:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rax,%rdx), %rdx
	subq	%rax, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	js	.LBB5_16
# BB#8:
	incl	%edx
	subl	%edx, %ecx
.Ltmp38:
	leaq	176(%rsp), %rdi
	leaq	144(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp39:
# BB#9:                                 # %_ZNK11CStringBaseIwE3MidEi.exit
	movl	$0, (%r13)
	movslq	184(%rsp), %rax
	incq	%rax
	cmpl	$4, %eax
	je	.LBB5_12
# BB#10:
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp41:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp42:
# BB#11:                                # %._crit_edge16.i.i
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	movq	%rbx, %r13
.LBB5_12:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	176(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_13
# BB#14:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
	testq	%rdi, %rdi
	je	.LBB5_16
# BB#15:
	callq	_ZdaPv
.LBB5_16:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movq	$4, 72(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 48(%rsp)
	testl	%ebp, %ebp
	movq	%r12, 104(%rsp)         # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	js	.LBB5_103
# BB#17:
.Ltmp121:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp122:
# BB#18:                                # %_ZN13CRecordVectorIiE3AddEi.exit
	movq	64(%rsp), %rax
	leaq	60(%rsp), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	60(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	movl	60(%rsp), %ecx
	incl	%ecx
	movl	%ecx, 60(%rsp)
                                        # implicit-def: %EAX
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB5_19:                               # %.thread596.preheader
	movq	%r13, (%rsp)            # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB5_102
# BB#20:                                # %.lr.ph
	leaq	24(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_40 Depth 2
                                        #     Child Loop BB5_50 Depth 2
                                        #     Child Loop BB5_59 Depth 2
                                        #     Child Loop BB5_86 Depth 2
                                        #     Child Loop BB5_91 Depth 2
	testq	%r12, %r12
	je	.LBB5_24
# BB#22:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%r12), %rax
.Ltmp124:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp125:
# BB#23:                                #   in Loop: Header=BB5_21 Depth=1
	testl	%eax, %eax
	jne	.LBB5_133
.LBB5_24:                               #   in Loop: Header=BB5_21 Depth=1
	movq	64(%rsp), %rax
	movslq	(%rax,%r13,4), %rax
	movl	%eax, 40(%r14)
	movq	32(%rbp), %rcx
	movq	(%rcx,%rax,8), %rax
.Ltmp127:
	callq	*8(%rax)
	movq	%rax, %r15
.Ltmp128:
# BB#25:                                # %.noexc515
                                        #   in Loop: Header=BB5_21 Depth=1
	testq	%r15, %r15
	je	.LBB5_101
# BB#26:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%r15), %rax
.Ltmp129:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp130:
# BB#27:                                # %_ZNK7CCodecs15CreateInArchiveEiR9CMyComPtrI10IInArchiveE.exit
                                        #   in Loop: Header=BB5_21 Depth=1
	testq	%r12, %r12
	je	.LBB5_32
# BB#28:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%r15), %rax
.Ltmp142:
	movl	$_ZL22kMaxCheckStartPosition, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp143:
.LBB5_29:                               #   in Loop: Header=BB5_21 Depth=1
	testl	%ebp, %ebp
	je	.LBB5_37
# BB#30:                                #   in Loop: Header=BB5_21 Depth=1
	movl	$27, %ebx
	cmpl	$1, %ebp
	je	.LBB5_99
# BB#31:                                #   in Loop: Header=BB5_21 Depth=1
	movl	$1, %ebx
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	jmp	.LBB5_99
	.p2align	4, 0x90
.LBB5_32:                               #   in Loop: Header=BB5_21 Depth=1
	movq	$0, 80(%rsp)
	movq	(%r15), %rax
.Ltmp132:
	movl	$IID_IArchiveOpenSeq, %esi
	movq	%r15, %rdi
	leaq	80(%rsp), %rdx
	callq	*(%rax)
.Ltmp133:
# BB#33:                                # %_ZNK9CMyComPtrI10IInArchiveE14QueryInterfaceIvEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_46
# BB#34:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%rdi), %rax
.Ltmp134:
	movq	160(%rsp), %rsi         # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp135:
# BB#35:                                #   in Loop: Header=BB5_21 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_29
# BB#36:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%rdi), %rax
.Ltmp139:
	callq	*16(%rax)
.Ltmp140:
	jmp	.LBB5_29
.LBB5_37:                               #   in Loop: Header=BB5_21 Depth=1
	movl	$0, 176(%rsp)
	movq	(%r15), %rax
.Ltmp144:
	movl	$55, %esi
	movq	%r15, %rdi
	leaq	176(%rsp), %rdx
	callq	*80(%rax)
.Ltmp145:
# BB#38:                                #   in Loop: Header=BB5_21 Depth=1
	movzwl	176(%rsp), %eax
	testw	%ax, %ax
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB5_52
# BB#39:                                #   in Loop: Header=BB5_21 Depth=1
	movzwl	%ax, %eax
	cmpl	$8, %eax
	movq	184(%rsp), %rbx
	movl	$.L.str.6, %eax
	cmovneq	%rax, %rbx
	movl	$0, 72(%r14)
	movq	64(%r14), %rbp
	movl	$0, (%rbp)
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB5_40:                               #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r14d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB5_40
# BB#41:                                # %_Z11MyStringLenIwEiPKT_.exit.i472
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	76(%rax), %eax
	cmpl	%r14d, %eax
	je	.LBB5_49
# BB#42:                                #   in Loop: Header=BB5_21 Depth=1
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp146:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp147:
# BB#43:                                # %.noexc482
                                        #   in Loop: Header=BB5_21 Depth=1
	testq	%rbp, %rbp
	movl	8(%rsp), %eax           # 4-byte Reload
	je	.LBB5_47
# BB#44:                                # %.noexc482
                                        #   in Loop: Header=BB5_21 Depth=1
	testl	%eax, %eax
	movl	$0, %eax
	jle	.LBB5_48
# BB#45:                                # %._crit_edge.thread.i.i476
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	72(%rax), %rax
	jmp	.LBB5_48
.LBB5_46:                               # %_ZN9CMyComPtrI15IArchiveOpenSeqED2Ev.exit
                                        #   in Loop: Header=BB5_21 Depth=1
	movl	$1, %ebx
	movl	$-2147467263, 8(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x80004001
	jmp	.LBB5_99
.LBB5_47:                               #   in Loop: Header=BB5_21 Depth=1
	xorl	%eax, %eax
.LBB5_48:                               # %._crit_edge16.i.i477
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, 64(%rcx)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 76(%rcx)
	movq	%r12, %rbp
	movq	104(%rsp), %r12         # 8-byte Reload
.LBB5_49:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i480.preheader
                                        #   in Loop: Header=BB5_21 Depth=1
	decl	%r14d
	.p2align	4, 0x90
.LBB5_50:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i480
                                        #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB5_50
# BB#51:                                # %_ZN11CStringBaseIwEaSEPKw.exit
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r14d, 72(%rax)
	movq	%rax, %r14
.LBB5_52:                               #   in Loop: Header=BB5_21 Depth=1
.Ltmp151:
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp152:
# BB#53:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%r15), %rax
.Ltmp154:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp155:
# BB#54:                                # %.noexc447
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_56
# BB#55:                                #   in Loop: Header=BB5_21 Depth=1
	movq	(%rdi), %rax
.Ltmp156:
	callq	*16(%rax)
.Ltmp157:
.LBB5_56:                               #   in Loop: Header=BB5_21 Depth=1
	movq	%r15, (%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	movslq	40(%r14), %rcx
	movq	(%rax,%rcx,8), %rbx
	movl	52(%rbx), %eax
	testl	%eax, %eax
	je	.LBB5_62
# BB#57:                                #   in Loop: Header=BB5_21 Depth=1
	movl	$-1, %r14d
	jle	.LBB5_69
# BB#58:                                # %.lr.ph.i410.preheader
                                        #   in Loop: Header=BB5_21 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_59:                               # %.lr.ph.i410
                                        #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %rsi
.Ltmp159:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp160:
# BB#60:                                # %.noexc413
                                        #   in Loop: Header=BB5_59 Depth=2
	testl	%eax, %eax
	je	.LBB5_68
# BB#61:                                #   in Loop: Header=BB5_59 Depth=2
	incq	%rbp
	movslq	52(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB5_59
	jmp	.LBB5_69
.LBB5_62:                               # %_Z11MyStringLenIwEiPKT_.exit.i439
                                        #   in Loop: Header=BB5_21 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 112(%rsp)
.Ltmp168:
	movl	$4, %edi
	callq	_Znam
.Ltmp169:
	leaq	176(%rsp), %rbx
# BB#63:                                # %.noexc444
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rax, 112(%rsp)
	movl	$1, 124(%rsp)
	movl	$0, (%rax)
	movl	$0, 120(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
.Ltmp171:
	movl	$4, %edi
	callq	_Znam
.Ltmp172:
# BB#64:                                # %.noexc436
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rax, 80(%rsp)
	movl	$1, 92(%rsp)
	movl	$0, (%rax)
	movl	$0, 88(%rsp)
.Ltmp174:
	movq	%rbx, %rdi
	leaq	144(%rsp), %rsi
	leaq	112(%rsp), %rdx
	leaq	80(%rsp), %rcx
	callq	_Z15GetDefaultName2RK11CStringBaseIwES2_S2_
.Ltmp175:
# BB#65:                                #   in Loop: Header=BB5_21 Depth=1
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB5_78
# BB#66:                                #   in Loop: Header=BB5_21 Depth=1
	movl	$0, 32(%r14)
	movq	24(%r14), %rbp
	movl	$0, (%rbp)
	movslq	184(%rsp), %r12
	incq	%r12
	movl	36(%r14), %r14d
	cmpl	%r14d, %r12d
	jne	.LBB5_79
# BB#67:                                #   in Loop: Header=BB5_21 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_90
.LBB5_68:                               # %.noexc413._ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit414.loopexit_crit_edge
                                        #   in Loop: Header=BB5_21 Depth=1
	movl	%ebp, %r14d
.LBB5_69:                               # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit414
                                        #   in Loop: Header=BB5_21 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	cmovsl	%eax, %r14d
	movq	56(%rbx), %rax
	movq	(%rax,%r14,8), %rdx
.Ltmp162:
	leaq	16(%rdx), %rcx
	leaq	176(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	144(%rsp), %rsi
	callq	_Z15GetDefaultName2RK11CStringBaseIwES2_S2_
.Ltmp163:
# BB#70:                                #   in Loop: Header=BB5_21 Depth=1
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB5_73
# BB#71:                                #   in Loop: Header=BB5_21 Depth=1
	movl	$0, 32(%r14)
	movq	24(%r14), %rbp
	movl	$0, (%rbp)
	movslq	184(%rsp), %r12
	incq	%r12
	movl	36(%r14), %r14d
	cmpl	%r14d, %r12d
	jne	.LBB5_74
# BB#72:                                #   in Loop: Header=BB5_21 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_85
.LBB5_73:                               # %._ZN11CStringBaseIwEaSERKS0_.exit409_crit_edge
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_97
	jmp	.LBB5_98
.LBB5_74:                               #   in Loop: Header=BB5_21 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp165:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp166:
# BB#75:                                # %.noexc408
                                        #   in Loop: Header=BB5_21 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_83
# BB#76:                                # %.noexc408
                                        #   in Loop: Header=BB5_21 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	jle	.LBB5_84
# BB#77:                                # %._crit_edge.thread.i.i403
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	32(%r14), %rax
	jmp	.LBB5_84
.LBB5_78:                               # %._ZN11CStringBaseIwEaSERKS0_.exit431_crit_edge
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_93
	jmp	.LBB5_94
.LBB5_79:                               #   in Loop: Header=BB5_21 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp177:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp178:
# BB#80:                                # %.noexc430
                                        #   in Loop: Header=BB5_21 Depth=1
	testq	%rbp, %rbp
	je	.LBB5_88
# BB#81:                                # %.noexc430
                                        #   in Loop: Header=BB5_21 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	jle	.LBB5_89
# BB#82:                                # %._crit_edge.thread.i.i424
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	32(%r14), %rax
	jmp	.LBB5_89
.LBB5_83:                               #   in Loop: Header=BB5_21 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB5_84:                               # %._crit_edge16.i.i404
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rbx, 24(%r14)
	movl	$0, (%rbx,%rax,4)
	movl	%r12d, 36(%r14)
	movq	%rbx, %rbp
.LBB5_85:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i405
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	176(%rsp), %rdi
	xorl	%eax, %eax
	movq	104(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_86:                               #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_86
# BB#87:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB5_21 Depth=1
	movl	184(%rsp), %eax
	movl	%eax, 32(%r14)
	testq	%rdi, %rdi
	jne	.LBB5_97
	jmp	.LBB5_98
.LBB5_88:                               #   in Loop: Header=BB5_21 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB5_89:                               # %._crit_edge16.i.i425
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	%rbx, 24(%r14)
	movl	$0, (%rbx,%rax,4)
	movl	%r12d, 36(%r14)
	movq	%rbx, %rbp
.LBB5_90:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i426
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	176(%rsp), %rdi
	xorl	%eax, %eax
	movq	104(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_91:                               #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_91
# BB#92:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i429
                                        #   in Loop: Header=BB5_21 Depth=1
	movl	184(%rsp), %eax
	movl	%eax, 32(%r14)
	testq	%rdi, %rdi
	je	.LBB5_94
.LBB5_93:                               #   in Loop: Header=BB5_21 Depth=1
	callq	_ZdaPv
.LBB5_94:                               # %_ZN11CStringBaseIwED2Ev.exit420
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_96
# BB#95:                                #   in Loop: Header=BB5_21 Depth=1
	callq	_ZdaPv
.LBB5_96:                               # %_ZN11CStringBaseIwED2Ev.exit419
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_98
.LBB5_97:                               #   in Loop: Header=BB5_21 Depth=1
	callq	_ZdaPv
.LBB5_98:                               # %_ZN11CStringBaseIwED2Ev.exit418
                                        #   in Loop: Header=BB5_21 Depth=1
	movl	$1, %ebx
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB5_99:                               # %.thread606
                                        #   in Loop: Header=BB5_21 Depth=1
	movq	(%r15), %rax
.Ltmp182:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp183:
# BB#100:                               # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit394
                                        #   in Loop: Header=BB5_21 Depth=1
	cmpl	$27, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB5_134
.LBB5_101:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit394.thread
                                        #   in Loop: Header=BB5_21 Depth=1
	incq	%r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rax
	cmpq	%rax, %r13
	jl	.LBB5_21
.LBB5_102:
	movl	$25, %ebx
	jmp	.LBB5_134
.LBB5_103:                              # %.preheader649
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpl	$0, 28(%rbp)
	jle	.LBB5_116
# BB#104:                               # %.lr.ph734
	xorl	%r12d, %r12d
	leaq	48(%rsp), %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_105:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_107 Depth 2
	movq	32(%rbp), %rax
	movq	(%rax,%r12,8), %rbp
	cmpl	$0, 52(%rbp)
	jle	.LBB5_113
# BB#106:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_105 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_107:                              # %.lr.ph.i
                                        #   Parent Loop BB5_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
.Ltmp44:
	movq	%r13, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp45:
# BB#108:                               # %.noexc386
                                        #   in Loop: Header=BB5_107 Depth=2
	testl	%eax, %eax
	je	.LBB5_110
# BB#109:                               #   in Loop: Header=BB5_107 Depth=2
	incq	%rbx
	movslq	52(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_107
	jmp	.LBB5_113
	.p2align	4, 0x90
.LBB5_110:                              # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB5_105 Depth=1
	testl	%ebx, %ebx
	js	.LBB5_113
# BB#111:                               #   in Loop: Header=BB5_105 Depth=1
.Ltmp49:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp50:
# BB#112:                               # %_ZN13CRecordVectorIiE6InsertEii.exit
                                        #   in Loop: Header=BB5_105 Depth=1
	movq	64(%rsp), %rax
	movslq	%r14d, %rcx
	incl	%r14d
	movl	%r12d, (%rax,%rcx,4)
	jmp	.LBB5_115
	.p2align	4, 0x90
.LBB5_113:                              # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit.thread
                                        #   in Loop: Header=BB5_105 Depth=1
.Ltmp47:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp48:
# BB#114:                               # %_ZN13CRecordVectorIiE3AddEi.exit389
                                        #   in Loop: Header=BB5_105 Depth=1
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	movl	%r12d, (%rax,%rcx,4)
	incl	60(%rsp)
.LBB5_115:                              #   in Loop: Header=BB5_105 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	incq	%r12
	movslq	28(%rbp), %rax
	cmpq	%rax, %r12
	jl	.LBB5_105
	jmp	.LBB5_117
.LBB5_116:
	xorl	%r14d, %r14d
.LBB5_117:                              # %._crit_edge735
	movq	104(%rsp), %r12         # 8-byte Reload
	testq	%r12, %r12
	je	.LBB5_119
# BB#118:                               # %._crit_edge735._crit_edge
	leaq	48(%rsp), %r15
	cmpl	$2, 60(%rsp)
	jge	.LBB5_122
	jmp	.LBB5_125
.LBB5_119:
	movl	$-2147467263, 8(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x80004001
	cmpl	$1, %r14d
	jne	.LBB5_135
# BB#120:
	leaq	48(%rsp), %r15
.Ltmp52:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector10DeleteFromEi
.Ltmp53:
# BB#121:
	cmpl	$2, 60(%rsp)
	jl	.LBB5_125
.LBB5_122:
	testl	%r14d, %r14d
	je	.LBB5_139
# BB#123:
.Ltmp54:
	movl	$.L.str, %esi
	movq	%r13, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp55:
# BB#124:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
	testl	%eax, %eax
	je	.LBB5_139
.LBB5_125:
.Ltmp56:
	movl	$.L.str.1, %esi
	movq	%r13, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp57:
# BB#126:
	testl	%eax, %eax
	je	.LBB5_129
# BB#127:
.Ltmp58:
	movl	$.L.str.2, %esi
	movq	%r13, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp59:
# BB#128:
	testl	%eax, %eax
                                        # implicit-def: %EAX
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jne	.LBB5_209
.LBB5_129:
.Ltmp61:
	movq	%r15, %r14
	movl	$1024, %edi             # imm = 0x400
	callq	_Znam
	movq	%rax, %r15
.Ltmp62:
# BB#130:                               # %_ZN7CBufferIhE11SetCapacityEm.exit468
	movq	(%r12), %rax
.Ltmp64:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp65:
# BB#131:
	testl	%eax, %eax
	je	.LBB5_147
# BB#132:                               # %_ZN7CBufferIhED2Ev.exit487.thread
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jmp	.LBB5_149
.LBB5_133:
	movl	$1, %ebx
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB5_134:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit394.thread617
	cmpl	$25, %ebx
	movl	$1, %eax
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmovel	%eax, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	(%rsp), %r13            # 8-byte Reload
.LBB5_135:
.Ltmp188:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp189:
# BB#136:                               # %_ZN11CStringBaseIwED2Ev.exit390
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	movl	8(%rsp), %ebx           # 4-byte Reload
	je	.LBB5_138
# BB#137:
	callq	_ZdaPv
.LBB5_138:                              # %_ZN11CStringBaseIwED2Ev.exit385
	movl	%ebx, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_139:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 120(%rsp)
	movq	$4, 136(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 112(%rsp)
.Ltmp78:
	movl	$2097152, %edi          # imm = 0x200000
	callq	_Znam
	movq	%rax, %r14
.Ltmp79:
# BB#140:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	(%r12), %rax
.Ltmp81:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp82:
# BB#141:
	testl	%eax, %eax
	jne	.LBB5_144
# BB#142:
	movq	$2097152, 80(%rsp)      # imm = 0x200000
.Ltmp84:
	leaq	80(%rsp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
.Ltmp85:
# BB#143:
	testl	%eax, %eax
	je	.LBB5_165
.LBB5_144:
	movl	$1, %ebx
.LBB5_145:                              # %_ZN7CBufferIhED2Ev.exit461
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%r14, %rdi
	callq	_ZdaPv
.Ltmp106:
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp107:
# BB#146:
	testl	%ebx, %ebx
	jne	.LBB5_135
	jmp	.LBB5_209
.LBB5_147:
	movq	$1024, 176(%rsp)        # imm = 0x400
.Ltmp67:
	leaq	176(%rsp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	movl	%eax, %ebx
.Ltmp68:
# BB#148:
	testl	%ebx, %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	je	.LBB5_150
.LBB5_149:                              # %.critedge859
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB5_135
.LBB5_150:
	cmpq	$16, 176(%rsp)
	jb	.LBB5_208
# BB#151:                               # %.lr.ph.i470.preheader
	cmpb	$82, (%r15)
	jne	.LBB5_208
# BB#152:                               # %.lr.ph.i470.1770
	cmpb	$97, 1(%r15)
	jne	.LBB5_208
# BB#153:                               # %.lr.ph.i470.2771
	cmpb	$114, 2(%r15)
	jne	.LBB5_208
# BB#154:                               # %.lr.ph.i470.3772
	cmpb	$33, 3(%r15)
	jne	.LBB5_208
# BB#155:                               # %.lr.ph.i470.4773
	cmpb	$26, 4(%r15)
	jne	.LBB5_208
# BB#156:                               # %.lr.ph.i470.5774
	cmpb	$7, 5(%r15)
	jne	.LBB5_208
# BB#157:                               # %.lr.ph.i470.6775
	cmpb	$0, 6(%r15)
	jne	.LBB5_208
# BB#158:
	cmpb	$115, 9(%r15)
	jne	.LBB5_208
# BB#159:
	testb	$1, 10(%r15)
	je	.LBB5_208
# BB#160:
	movl	60(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB5_208
# BB#161:                               # %.lr.ph729
	xorl	%ebp, %ebp
.LBB5_162:                              # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rax
	movslq	(%rax,%rbp,4), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
.Ltmp70:
	movl	$.L.str.3, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp71:
# BB#163:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit484
                                        #   in Loop: Header=BB5_162 Depth=1
	testl	%eax, %eax
	je	.LBB5_205
# BB#164:                               #   in Loop: Header=BB5_162 Depth=1
	incq	%rbp
	movslq	60(%rsp), %rax
	cmpq	%rax, %rbp
	jl	.LBB5_162
	jmp	.LBB5_208
.LBB5_165:
	movq	80(%rsp), %r12
	testq	%r12, %r12
	je	.LBB5_204
# BB#166:
.Ltmp87:
	movl	$65536, %edi            # imm = 0x10000
	callq	_Znam
.Ltmp88:
# BB#167:                               # %_ZN7CBufferIhE11SetCapacityEm.exit399
	movl	$255, %esi
	movl	$65536, %edx            # imm = 0x10000
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	memset
	movslq	60(%rsp), %rcx
	cmpq	$255, %rcx
	movl	$1, %ebx
	jg	.LBB5_203
# BB#168:                               # %.preheader636
	testl	%ecx, %ecx
	jle	.LBB5_173
# BB#169:                               # %.lr.ph726
	movq	64(%rsp), %rax
	movq	32(%rbp), %rdx
	xorl	%esi, %esi
.LBB5_170:                              # =>This Inner Loop Header: Depth=1
	movslq	(%rax,%rsi,4), %rdi
	movq	(%rdx,%rdi,8), %rdi
	cmpq	$2, 80(%rdi)
	jb	.LBB5_172
# BB#171:                               #   in Loop: Header=BB5_170 Depth=1
	movq	88(%rdi), %rdi
	movzbl	(%rdi), %ebp
	movzbl	1(%rdi), %edi
	shlq	$8, %rdi
	orq	%rbp, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movzbl	(%rbp,%rdi), %ebx
	movb	%bl, 176(%rsp,%rsi)
	movb	%sil, (%rbp,%rdi)
.LBB5_172:                              #   in Loop: Header=BB5_170 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB5_170
.LBB5_173:                              # %._crit_edge727
	decq	%r12
	movq	%r12, 80(%rsp)
	movq	%r14, 32(%rsp)          # 8-byte Spill
	je	.LBB5_190
# BB#174:                               # %.preheader635.lr.ph
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r13, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	movq	40(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_175:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_179 Depth 2
                                        #       Child Loop BB5_182 Depth 3
	movl	%r13d, %r15d
	cmpq	%r12, %r15
	jae	.LBB5_177
# BB#176:                               #   in Loop: Header=BB5_175 Depth=1
	movzbl	(%r14,%r15), %eax
	leal	1(%r15), %r13d
	movzbl	(%r14,%r13), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	cmpb	$-1, (%rdx,%rcx)
	je	.LBB5_175
.LBB5_177:                              # %.critedge
                                        #   in Loop: Header=BB5_175 Depth=1
	cmpq	%r12, %r15
	je	.LBB5_189
# BB#178:                               #   in Loop: Header=BB5_175 Depth=1
	movq	%r14, %rcx
	leaq	(%rcx,%r15), %r14
	movzbl	(%rcx,%r15), %eax
	leal	1(%r15), %r13d
	movzbl	(%rcx,%r13), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	leaq	(%rdx,%rcx), %rdi
	movb	(%rdx,%rcx), %cl
	.p2align	4, 0x90
.LBB5_179:                              #   Parent Loop BB5_175 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_182 Depth 3
	movq	64(%rsp), %rax
	movzbl	%cl, %ebp
	movslq	(%rax,%rbp,4), %r12
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	movq	(%rax,%r12,8), %rcx
	movq	80(%rcx), %rax
	testq	%rax, %rax
	je	.LBB5_186
# BB#180:                               #   in Loop: Header=BB5_179 Depth=2
	leaq	(%rax,%r15), %rdx
	movq	80(%rsp), %rsi
	incq	%rsi
	cmpq	%rsi, %rdx
	ja	.LBB5_186
# BB#181:                               #   in Loop: Header=BB5_179 Depth=2
	movq	88(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_182:                              # %.lr.ph.i446
                                        #   Parent Loop BB5_175 Depth=1
                                        #     Parent Loop BB5_179 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r14,%rdx), %ebx
	cmpb	(%rcx,%rdx), %bl
	jne	.LBB5_186
# BB#183:                               #   in Loop: Header=BB5_182 Depth=3
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB5_182
# BB#184:                               #   in Loop: Header=BB5_179 Depth=2
.Ltmp90:
	movq	%rdi, %rbx
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp91:
# BB#185:                               #   in Loop: Header=BB5_179 Depth=2
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movl	%r12d, (%rax,%rcx,4)
	incl	124(%rsp)
	movq	64(%rsp), %rax
	movl	$255, (%rax,%rbp,4)
	movb	176(%rsp,%rbp), %cl
	movq	%rbx, %rdi
	movb	%cl, (%rdi)
	cmpb	$-1, %cl
	jne	.LBB5_179
	jmp	.LBB5_188
	.p2align	4, 0x90
.LBB5_186:                              # %_ZL13TestSignaturePKhS0_m.exit
                                        #   in Loop: Header=BB5_179 Depth=2
	leaq	176(%rsp,%rbp), %rdi
	movb	176(%rsp,%rbp), %cl
	cmpb	$-1, %cl
	jne	.LBB5_179
.LBB5_188:                              # %.loopexit634
                                        #   in Loop: Header=BB5_175 Depth=1
	movq	80(%rsp), %r12
	cmpq	%r12, %r13
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jb	.LBB5_175
.LBB5_189:                              # %.preheader.loopexit
	movl	60(%rsp), %ecx
	movq	(%rsp), %r13            # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB5_190:                              # %.preheader
	testl	%ecx, %ecx
	jle	.LBB5_196
# BB#191:                               # %.lr.ph722
	xorl	%ebx, %ebx
	leaq	112(%rsp), %r12
.LBB5_192:                              # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rax
	movl	(%rax,%rbx,4), %ebp
	cmpl	$255, %ebp
	je	.LBB5_195
# BB#193:                               #   in Loop: Header=BB5_192 Depth=1
.Ltmp93:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp94:
# BB#194:                               # %_ZN13CRecordVectorIiE3AddEi.exit455
                                        #   in Loop: Header=BB5_192 Depth=1
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	124(%rsp)
	movl	60(%rsp), %ecx
.LBB5_195:                              #   in Loop: Header=BB5_192 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB5_192
.LBB5_196:                              # %._crit_edge723
.Ltmp96:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp97:
# BB#197:                               # %.noexc458
	movl	124(%rsp), %r14d
	movl	60(%rsp), %esi
	addl	%r14d, %esi
.Ltmp98:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp99:
# BB#198:                               # %.noexc459
	testl	%r14d, %r14d
	jle	.LBB5_202
# BB#199:                               # %.lr.ph.i.i
	xorl	%ebp, %ebp
.LBB5_200:                              # =>This Inner Loop Header: Depth=1
	movq	128(%rsp), %rax
	movl	(%rax,%rbp,4), %ebx
.Ltmp101:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp102:
# BB#201:                               # %.noexc460
                                        #   in Loop: Header=BB5_200 Depth=1
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	60(%rsp)
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB5_200
.LBB5_202:
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB5_203:                              # %_ZN7CBufferIhED2Ev.exit
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movl	%ebx, %eax
	movq	104(%rsp), %r12         # 8-byte Reload
	jmp	.LBB5_145
.LBB5_204:
	movl	$1, %ebx
	movl	$1, %eax
	movq	104(%rsp), %r12         # 8-byte Reload
	jmp	.LBB5_145
.LBB5_205:
.Ltmp73:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$1, %edx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp74:
# BB#206:
.Ltmp75:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp76:
# BB#207:
	movq	64(%rsp), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, (%rax)
	movq	(%rsp), %r13            # 8-byte Reload
	movq	104(%rsp), %r12         # 8-byte Reload
.LBB5_208:                              # %_ZN7CBufferIhED2Ev.exit487
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB5_209:
	leaq	60(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	60(%rsp), %ecx
	cmpl	$2, %ecx
	jl	.LBB5_229
# BB#210:                               # %_Z11MyStringLenIwEiPKT_.exit.i492
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
.Ltmp109:
	movl	$16, %edi
	callq	_Znam
.Ltmp110:
# BB#211:                               # %.noexc497
	movq	%rax, 176(%rsp)
	movl	$4, 188(%rsp)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [105,115,111,0]
	movups	%xmm0, (%rax)
	movl	$3, 184(%rsp)
.Ltmp112:
	leaq	176(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	movl	%eax, %r15d
.Ltmp113:
# BB#212:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_214
# BB#213:
	callq	_ZdaPv
.LBB5_214:                              # %_ZN11CStringBaseIwED2Ev.exit499
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
.Ltmp115:
	movl	$16, %edi
	callq	_Znam
.Ltmp116:
# BB#215:                               # %.noexc507
	movq	%rax, 176(%rsp)
	movl	$4, 188(%rsp)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [117,100,102,0]
	movups	%xmm0, (%rax)
	movl	$3, 184(%rsp)
.Ltmp118:
	leaq	176(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	movl	%eax, %r14d
.Ltmp119:
# BB#216:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_218
# BB#217:
	callq	_ZdaPv
.LBB5_218:                              # %_ZN11CStringBaseIwED2Ev.exit509
	movslq	60(%rsp), %rcx
	testq	%rcx, %rcx
	jle	.LBB5_222
# BB#219:                               # %.lr.ph718
	movq	64(%rsp), %rdx
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	andq	$3, %r9
	je	.LBB5_223
# BB#220:                               # %.prol.preheader
	movl	$-1, %eax
	xorl	%edi, %edi
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB5_221:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rdi,4), %ebx
	cmpl	%r15d, %ebx
	cmovel	%edi, %esi
	cmpl	%r14d, %ebx
	cmovel	%edi, %eax
	incq	%rdi
	cmpq	%rdi, %r9
	jne	.LBB5_221
	jmp	.LBB5_224
.LBB5_222:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$25, %ebx
	jmp	.LBB5_134
.LBB5_223:
	xorl	%edi, %edi
	movl	$-1, %eax
	movl	$-1, %esi
.LBB5_224:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_226
	.p2align	4, 0x90
.LBB5_225:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rdi,4), %ebx
	cmpl	%r15d, %ebx
	cmovel	%edi, %esi
	cmpl	%r14d, %ebx
	cmovel	%edi, %eax
	movl	4(%rdx,%rdi,4), %ebx
	leal	1(%rdi), %ebp
	cmpl	%r15d, %ebx
	cmovel	%ebp, %esi
	cmpl	%r14d, %ebx
	cmovel	%ebp, %eax
	movl	8(%rdx,%rdi,4), %ebp
	leal	2(%rdi), %ebx
	cmpl	%r15d, %ebp
	cmovel	%ebx, %esi
	cmpl	%r14d, %ebp
	cmovel	%ebx, %eax
	movl	12(%rdx,%rdi,4), %ebp
	leal	3(%rdi), %ebx
	cmpl	%r15d, %ebp
	cmovel	%ebx, %esi
	cmpl	%r14d, %ebp
	cmovel	%ebx, %eax
	addq	$4, %rdi
	cmpq	%rcx, %rdi
	jl	.LBB5_225
.LBB5_226:                              # %._crit_edge
	testl	%esi, %esi
	js	.LBB5_230
# BB#227:                               # %._crit_edge
	cmpl	%esi, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB5_229
# BB#228:
	movq	64(%rsp), %rcx
	cltq
	movl	%r15d, (%rcx,%rax,4)
	movslq	%esi, %rax
	movl	%r14d, (%rcx,%rax,4)
	movl	60(%rsp), %ecx
.LBB5_229:                              # %.thread596.preheader
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_19
.LBB5_230:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_19
.LBB5_231:                              # %.loopexit.split-lp639
.Ltmp77:
	jmp	.LBB5_253
.LBB5_232:                              # %.loopexit638
.Ltmp72:
	jmp	.LBB5_252
.LBB5_233:                              # %.loopexit.split-lp
.Ltmp100:
	movq	%r13, (%rsp)            # 8-byte Spill
	jmp	.LBB5_258
.LBB5_234:                              # %.thread
.Ltmp89:
	jmp	.LBB5_247
.LBB5_235:
.Ltmp86:
	jmp	.LBB5_247
.LBB5_236:
.Ltmp95:
	movq	%r13, (%rsp)            # 8-byte Spill
	jmp	.LBB5_258
.LBB5_237:
.Ltmp69:
	jmp	.LBB5_252
.LBB5_238:
.Ltmp120:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_293
# BB#239:
	callq	_ZdaPv
	jmp	.LBB5_293
.LBB5_240:
.Ltmp117:
	jmp	.LBB5_291
.LBB5_241:
.Ltmp114:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_293
# BB#242:
	callq	_ZdaPv
	jmp	.LBB5_293
.LBB5_243:
.Ltmp111:
	jmp	.LBB5_291
.LBB5_244:                              # %.loopexit
.Ltmp103:
	movq	%r13, (%rsp)            # 8-byte Spill
	jmp	.LBB5_258
.LBB5_245:
.Ltmp108:
	jmp	.LBB5_291
.LBB5_246:
.Ltmp83:
.LBB5_247:
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	jmp	.LBB5_259
.LBB5_248:                              # %.thread587
.Ltmp80:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	jmp	.LBB5_260
.LBB5_249:
.Ltmp43:
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_294
# BB#250:
	callq	_ZdaPv
	jmp	.LBB5_294
.LBB5_251:
.Ltmp66:
.LBB5_252:
	movq	%r13, (%rsp)            # 8-byte Spill
.LBB5_253:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB5_293
.LBB5_254:
.Ltmp63:
	jmp	.LBB5_291
.LBB5_255:
.Ltmp179:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_265
# BB#256:
	callq	_ZdaPv
	jmp	.LBB5_265
.LBB5_257:
.Ltmp92:
.LBB5_258:
	movq	%rax, %rbx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB5_259:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB5_260:                              # %_ZN7CBufferIhED2Ev.exit463
.Ltmp104:
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp105:
	jmp	.LBB5_293
.LBB5_261:
.Ltmp40:
	jmp	.LBB5_276
.LBB5_262:
.Ltmp167:
	movq	%rax, %rbx
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_287
# BB#263:
	callq	_ZdaPv
	jmp	.LBB5_287
.LBB5_264:
.Ltmp176:
	movq	%rax, %rbx
.LBB5_265:                              # %_ZN11CStringBaseIwED2Ev.exit417
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_268
# BB#266:
	callq	_ZdaPv
	jmp	.LBB5_268
.LBB5_267:
.Ltmp173:
	movq	%rax, %rbx
.LBB5_268:                              # %_ZN11CStringBaseIwED2Ev.exit416
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_287
# BB#269:
	callq	_ZdaPv
	jmp	.LBB5_287
.LBB5_270:
.Ltmp170:
	jmp	.LBB5_286
.LBB5_271:                              # %.loopexit.split-lp645.loopexit.split-lp
.Ltmp60:
	jmp	.LBB5_291
.LBB5_272:
.Ltmp141:
	jmp	.LBB5_286
.LBB5_273:
.Ltmp123:
	jmp	.LBB5_291
.LBB5_274:
.Ltmp164:
	jmp	.LBB5_286
.LBB5_275:
.Ltmp190:
.LBB5_276:                              # %_ZN11CStringBaseIwED2Ev.exit384
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	jmp	.LBB5_294
.LBB5_277:
.Ltmp37:
	movq	%rax, %rbx
	jmp	.LBB5_295
.LBB5_278:
.Ltmp153:
	jmp	.LBB5_286
.LBB5_279:
.Ltmp148:
	movq	%rax, %rbx
.Ltmp149:
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp150:
	jmp	.LBB5_287
.LBB5_280:
.Ltmp136:
	movq	%rax, %rbx
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_287
# BB#281:
	movq	(%rdi), %rax
.Ltmp137:
	callq	*16(%rax)
.Ltmp138:
	jmp	.LBB5_287
.LBB5_282:
.Ltmp184:
	jmp	.LBB5_292
.LBB5_283:
.Ltmp126:
	jmp	.LBB5_292
.LBB5_284:                              # %.thread622
.Ltmp158:
	jmp	.LBB5_286
.LBB5_285:
.Ltmp161:
.LBB5_286:
	movq	%rax, %rbx
.LBB5_287:
	movq	(%r15), %rax
.Ltmp180:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp181:
	jmp	.LBB5_293
.LBB5_288:                              # %.loopexit.split-lp645.loopexit
.Ltmp51:
	jmp	.LBB5_291
.LBB5_289:                              # %.thread619
.Ltmp131:
	jmp	.LBB5_292
.LBB5_290:                              # %.loopexit644
.Ltmp46:
.LBB5_291:                              # %_ZN7CBufferIhED2Ev.exit488
	movq	%r13, (%rsp)            # 8-byte Spill
.LBB5_292:                              # %_ZN7CBufferIhED2Ev.exit488
	movq	%rax, %rbx
.LBB5_293:                              # %_ZN7CBufferIhED2Ev.exit488
.Ltmp185:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp186:
.LBB5_294:                              # %_ZN11CStringBaseIwED2Ev.exit384
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
.LBB5_295:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_297
# BB#296:
	callq	_ZdaPv
.LBB5_297:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_298:
.Ltmp187:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback, .Lfunc_end5-_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\337\204"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\326\004"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp35-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin4   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin4   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin4  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin4  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp130-.Ltmp127       #   Call between .Ltmp127 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin4  #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp158-.Lfunc_begin4  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp135-.Ltmp132       #   Call between .Ltmp132 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin4  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin4  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp147-.Ltmp144       #   Call between .Ltmp144 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin4  #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin4  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp157-.Ltmp154       #   Call between .Ltmp154 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin4  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin4  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin4  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin4  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin4  #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin4  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin4  # >> Call Site 19 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin4  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin4  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin4  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin4   # >> Call Site 22 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin4   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin4   # >> Call Site 23 <<
	.long	.Ltmp48-.Ltmp49         #   Call between .Ltmp49 and .Ltmp48
	.long	.Ltmp51-.Lfunc_begin4   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 24 <<
	.long	.Ltmp59-.Ltmp52         #   Call between .Ltmp52 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin4   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin4   # >> Call Site 25 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin4   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin4   # >> Call Site 26 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin4  # >> Call Site 27 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin4  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin4   # >> Call Site 28 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin4   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin4   # >> Call Site 29 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin4   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin4   # >> Call Site 30 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin4   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin4  # >> Call Site 31 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin4  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin4   # >> Call Site 32 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin4   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin4   # >> Call Site 33 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin4   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin4   # >> Call Site 34 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin4   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin4   # >> Call Site 35 <<
	.long	.Ltmp90-.Ltmp88         #   Call between .Ltmp88 and .Ltmp90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin4   # >> Call Site 36 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin4   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin4   # >> Call Site 37 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin4   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin4   # >> Call Site 38 <<
	.long	.Ltmp99-.Ltmp96         #   Call between .Ltmp96 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin4  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin4  # >> Call Site 39 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin4  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin4   # >> Call Site 40 <<
	.long	.Ltmp76-.Ltmp73         #   Call between .Ltmp73 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin4   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin4  # >> Call Site 41 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin4  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin4  # >> Call Site 42 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin4  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin4  # >> Call Site 43 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin4  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin4  # >> Call Site 44 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin4  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin4  # >> Call Site 45 <<
	.long	.Ltmp186-.Ltmp104       #   Call between .Ltmp104 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin4  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp186-.Lfunc_begin4  # >> Call Site 46 <<
	.long	.Lfunc_end5-.Ltmp186    #   Call between .Ltmp186 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback,@function
_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback: # @_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movl	%edx, %eax
	movq	%rdi, %rbp
	testb	%cl, %cl
	je	.LBB6_4
# BB#1:
.Ltmp201:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rsi, %r13
	movl	%eax, %r12d
	movq	%r9, %rbp
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp202:
# BB#2:
	movl	$0, 8(%r14)
	movq	$_ZTV16CStdInFileStream+16, (%r14)
.Ltmp203:
	movq	%r14, %rdi
	callq	*_ZTV16CStdInFileStream+24(%rip)
.Ltmp204:
# BB#3:
	xorl	%r15d, %r15d
	movq	%rbp, %r9
	movl	%r12d, %eax
	movq	%r13, %rsi
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB6_11
.LBB6_4:
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	movl	$0, %r14d
	jne	.LBB6_11
# BB#5:
.Ltmp191:
	movq	%rsi, %r12
	movl	%eax, %r13d
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp192:
# BB#6:
	movl	$0, 16(%rbx)
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%rbx)
	movl	$-1, 32(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
.Ltmp193:
	movl	$4, %edi
	callq	_Znam
.Ltmp194:
# BB#7:
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	$4, 52(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 24(%rbx)
	movb	$1, 20(%rbx)
	xorl	%r15d, %r15d
.Ltmp196:
	movq	%rbx, %rdi
	callq	*_ZTV13CInFileStream+24(%rip)
.Ltmp197:
# BB#8:                                 # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%rbx, %r15
	movq	8(%rbp), %rsi
.Ltmp198:
	movq	%rbx, %rdi
	callq	_ZN13CInFileStream4OpenEPKw
.Ltmp199:
# BB#9:
	testb	%al, %al
	je	.LBB6_17
# BB#10:
	xorl	%r14d, %r14d
	movq	%rbx, %r15
	movq	(%rsp), %r9             # 8-byte Reload
	movl	%r13d, %eax
	movq	%r12, %rsi
.LBB6_11:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
.Ltmp206:
	movq	%rbp, %rdi
	movl	%eax, %edx
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback
	movl	%eax, %ebp
.Ltmp207:
# BB#12:
	testq	%r14, %r14
	je	.LBB6_14
# BB#13:
	movq	(%r14), %rax
.Ltmp211:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp212:
.LBB6_14:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit29
	testq	%r15, %r15
	je	.LBB6_16
.LBB6_15:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*16(%rax)
.LBB6_16:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit27
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit29.thread
	callq	__errno_location
	movl	(%rax), %ebp
	movq	%rbx, %r15
	jmp	.LBB6_15
.LBB6_18:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.thread55
.Ltmp195:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB6_19:
.Ltmp200:
	jmp	.LBB6_20
.LBB6_21:
.Ltmp213:
.LBB6_20:
	movq	%rax, %rbp
	testq	%r15, %r15
	jne	.LBB6_25
	jmp	.LBB6_26
.LBB6_22:
.Ltmp208:
	movq	%rax, %rbp
	testq	%r14, %r14
	je	.LBB6_24
# BB#23:
	movq	(%r14), %rax
.Ltmp209:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp210:
.LBB6_24:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	testq	%r15, %r15
	je	.LBB6_26
.LBB6_25:
	movq	(%r15), %rax
.Ltmp214:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp215:
.LBB6_26:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB6_27:
.Ltmp216:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_28:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.thread
.Ltmp205:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback, .Lfunc_end6-_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp201-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp192-.Ltmp201       #   Call between .Ltmp201 and .Ltmp192
	.long	.Ltmp205-.Lfunc_begin5  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin5  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp199-.Ltmp196       #   Call between .Ltmp196 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin5  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin5  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin5  #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp209-.Ltmp212       #   Call between .Ltmp212 and .Ltmp209
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp215-.Ltmp209       #   Call between .Ltmp209 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin5  #     jumps to .Ltmp216
	.byte	1                       #   On action: 1
	.long	.Ltmp215-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Lfunc_end6-.Ltmp215    #   Call between .Ltmp215 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN12CArchiveLink5CloseEv
	.p2align	4, 0x90
	.type	_ZN12CArchiveLink5CloseEv,@function
_ZN12CArchiveLink5CloseEv:              # @_ZN12CArchiveLink5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	12(%r14), %rbx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jle	.LBB7_3
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	16(%r14), %rax
	movq	-8(%rax,%rbx,8), %rax
	decq	%rbx
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	testl	%eax, %eax
	je	.LBB7_1
	jmp	.LBB7_4
.LBB7_3:
	movb	$0, 72(%r14)
	xorl	%eax, %eax
.LBB7_4:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN12CArchiveLink5CloseEv, .Lfunc_end7-_ZN12CArchiveLink5CloseEv
	.cfi_endproc

	.globl	_ZN12CArchiveLink7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN12CArchiveLink7ReleaseEv,@function
_ZN12CArchiveLink7ReleaseEv:            # @_ZN12CArchiveLink7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 16
.Lcfi64:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 12(%rbx)
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	cmpl	$0, 12(%rbx)
	jne	.LBB8_1
.LBB8_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN12CArchiveLink7ReleaseEv, .Lfunc_end8-_ZN12CArchiveLink7ReleaseEv
	.cfi_endproc

	.globl	_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback,@function
_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback: # @_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi71:
	.cfi_def_cfa_offset 336
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r8, 192(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	movq	%rdx, %rbp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	cmpl	$0, 12(%r12)
	je	.LBB9_2
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	cmpl	$0, 12(%r12)
	jne	.LBB9_1
.LBB9_2:                                # %_ZN12CArchiveLink7ReleaseEv.exit
	movl	12(%rbp), %ecx
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmpl	$31, %ecx
	jg	.LBB9_107
# BB#3:                                 # %.preheader
	xorl	%eax, %eax
	movzbl	%bl, %edx
	movl	%edx, 60(%rsp)          # 4-byte Spill
                                        # implicit-def: %EDX
	movl	%edx, 4(%rsp)           # 4-byte Spill
                                        # implicit-def: %R14D
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jg	.LBB9_5
	.p2align	4, 0x90
.LBB9_7:
	movl	$-1, %ebx
	cmpl	$31, %eax
	jg	.LBB9_105
# BB#8:
	testl	%eax, %eax
	jne	.LBB9_9
.LBB9_12:
	leaq	112(%rsp), %rdi
	callq	_ZN4CArcC2Ev
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	120(%rsp), %rax
	cmpq	%rdx, %rax
	je	.LBB9_39
# BB#13:
	movl	$0, 128(%rsp)
	movq	120(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	8(%rdx), %r12
	incq	%r12
	movl	132(%rsp), %r13d
	cmpl	%r13d, %r12d
	je	.LBB9_36
# BB#14:
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp284:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp285:
# BB#15:                                # %.noexc138
	testq	%rbp, %rbp
	je	.LBB9_34
# BB#16:                                # %.noexc138
	testl	%r13d, %r13d
	movl	$0, %eax
	jle	.LBB9_35
# BB#17:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	128(%rsp), %rax
	jmp	.LBB9_35
.LBB9_34:
	xorl	%eax, %eax
.LBB9_35:                               # %._crit_edge16.i.i
	movq	%r15, 120(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r12d, 132(%rsp)
	movq	%r15, %rbp
	movq	72(%rsp), %rdx          # 8-byte Reload
.LBB9_36:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rdx), %rax
	.p2align	4, 0x90
.LBB9_37:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB9_37
# BB#38:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rdx), %eax
	movl	%eax, 128(%rsp)
.LBB9_39:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movl	$-1, 156(%rsp)
.Ltmp286:
	leaq	112(%rsp), %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, %edx
	movl	60(%rsp), %ecx          # 4-byte Reload
	movq	192(%rsp), %r8          # 8-byte Reload
	movq	336(%rsp), %r9
	callq	_ZN4CArc16OpenStreamOrFileEP7CCodecsibP9IInStreamP20IArchiveOpenCallback
.Ltmp287:
# BB#40:
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	movl	$1, %ebx
	jne	.LBB9_45
# BB#41:
.Ltmp288:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp289:
# BB#42:                                # %.noexc144
.Ltmp290:
	movq	%rbp, %rdi
	leaq	112(%rsp), %rsi
	callq	_ZN4CArcC2ERKS_
.Ltmp291:
# BB#43:
.Ltmp293:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp294:
# BB#44:                                # %_ZN13CObjectVectorI4CArcE3AddERKS0_.exit
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	$3, %ebx
.LBB9_45:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB9_47
# BB#46:
	callq	_ZdaPv
.LBB9_47:                               # %_ZN11CStringBaseIwED2Ev.exit.i146
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB9_49
# BB#48:
	callq	_ZdaPv
.LBB9_49:                               # %_ZN11CStringBaseIwED2Ev.exit1.i147
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_51
# BB#50:
	callq	_ZdaPv
.LBB9_51:                               # %_ZN11CStringBaseIwED2Ev.exit2.i148
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_52
	jmp	.LBB9_53
	.p2align	4, 0x90
.LBB9_4:                                # %thread-pre-split
	movl	12(%rbp), %ecx
	movl	12(%r12), %eax
	testl	%ecx, %ecx
	jle	.LBB9_7
.LBB9_5:
	cmpl	%eax, %ecx
	jle	.LBB9_105
# BB#6:
	decl	%ecx
	subl	%eax, %ecx
	movq	16(%rbp), %rdx
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %ebx
	testl	%eax, %eax
	je	.LBB9_12
.LBB9_9:
	movl	%ebx, %r13d
	movq	16(%r12), %rcx
	cltq
	movq	-8(%rcx,%rax,8), %r15
	movl	$0, 80(%rsp)
	movq	(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp217:
	movl	$1, %esi
	leaq	80(%rsp), %rdx
	callq	*80(%rax)
.Ltmp218:
# BB#10:
	testl	%eax, %eax
	movl	%eax, %ebp
	cmovel	%r14d, %ebp
	je	.LBB9_18
# BB#11:
	movl	$1, %ebx
	movl	%eax, %r14d
	jmp	.LBB9_23
	.p2align	4, 0x90
.LBB9_18:
	movl	$2, %ebx
	movzwl	80(%rsp), %eax
	cmpl	$19, %eax
	jne	.LBB9_23
# BB#19:
	movl	88(%rsp), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp220:
	leaq	112(%rsp), %rsi
	callq	*56(%rax)
.Ltmp221:
# BB#20:
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movl	$1, %ebx
	jne	.LBB9_22
# BB#21:
	xorl	%ebx, %ebx
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	112(%rsp), %eax
	setae	%bl
	addl	%ebx, %ebx
.LBB9_22:
	movl	%ebp, %r14d
	.p2align	4, 0x90
.LBB9_23:
	leaq	80(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	testl	%ebx, %ebx
	je	.LBB9_25
.LBB9_24:
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%ebx, %ebx
	jne	.LBB9_54
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_25:
	movq	$0, 40(%rsp)
	movq	(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp225:
	movl	$IID_IInArchiveGetStream, %esi
	leaq	40(%rsp), %rdx
	callq	*(%rax)
.Ltmp226:
# BB#26:
	movl	$2, %ebx
	testl	%eax, %eax
	je	.LBB9_29
.LBB9_27:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB9_28:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_53
.LBB9_52:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB9_53:
	testl	%ebx, %ebx
	je	.LBB9_4
.LBB9_54:
	cmpl	$3, %ebx
	je	.LBB9_4
	jmp	.LBB9_103
	.p2align	4, 0x90
.LBB9_29:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_33
# BB#30:
	movq	$0, 32(%rsp)
	movq	(%rdi), %rax
.Ltmp228:
	movl	4(%rsp), %esi           # 4-byte Reload
	leaq	32(%rsp), %rdx
	callq	*40(%rax)
.Ltmp229:
# BB#31:
	movl	$2, %ebx
	testl	%eax, %eax
	je	.LBB9_55
# BB#32:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB9_69
.LBB9_33:
	movl	$2, %ebx
	jmp	.LBB9_24
.LBB9_55:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_61
# BB#56:
	movq	$0, 16(%rsp)
	movq	(%rdi), %rax
.Ltmp231:
	movl	$IID_IInStream, %esi
	leaq	16(%rsp), %rdx
	callq	*(%rax)
.Ltmp232:
# BB#57:                                # %_ZNK9CMyComPtrI19ISequentialInStreamE14QueryInterfaceI9IInStreamEEiRK4GUIDPPT_.exit
	movl	$2, %ebx
	testl	%eax, %eax
	je	.LBB9_62
# BB#58:
	movl	%r14d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB9_59:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_68
# BB#60:
	movq	(%rdi), %rax
.Ltmp274:
	callq	*16(%rax)
.Ltmp275:
	jmp	.LBB9_68
.LBB9_61:
	movl	$2, %ebx
	jmp	.LBB9_27
.LBB9_62:
	cmpq	$0, 16(%rsp)
	je	.LBB9_67
# BB#63:
.Ltmp233:
	leaq	200(%rsp), %rdi
	callq	_ZN4CArcC2Ev
.Ltmp234:
# BB#64:
.Ltmp236:
	movq	%r15, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	leaq	208(%rsp), %rdx
	callq	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
.Ltmp237:
# BB#65:
	testl	%eax, %eax
	movl	%eax, %ecx
	cmovnel	%eax, %r14d
	je	.LBB9_71
# BB#66:
	movl	$1, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ecx, %r13d
	jmp	.LBB9_95
.LBB9_67:
	movl	$2, %ebx
	movl	%r14d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB9_68:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit156
	movl	%r13d, %r14d
.LBB9_69:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_28
# BB#70:
	movq	(%rdi), %rax
.Ltmp279:
	callq	*16(%rax)
.Ltmp280:
	jmp	.LBB9_28
.LBB9_71:
	movq	$0, 24(%rsp)
	movq	336(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp239:
	movl	$IID_IArchiveOpenSetSubArchiveName, %esi
	leaq	24(%rsp), %rdx
	callq	*(%rax)
.Ltmp240:
# BB#72:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_74
# BB#73:
	movq	(%rdi), %rax
	movq	208(%rsp), %rsi
.Ltmp241:
	callq	*40(%rax)
.Ltmp242:
.LBB9_74:
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 244(%rsp)
	movq	16(%rsp), %rcx
.Ltmp243:
	xorl	%r8d, %r8d
	leaq	200(%rsp), %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
	movq	336(%rsp), %r9
	callq	_ZN4CArc10OpenStreamEP7CCodecsiP9IInStreamP19ISequentialInStreamP20IArchiveOpenCallback
.Ltmp244:
# BB#75:
	testl	%eax, %eax
	je	.LBB9_78
# BB#76:
	movl	%r14d, %r13d
	movl	$2, %ebx
	cmpl	$1, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB9_93
# BB#77:
	movl	$1, %ebx
	movl	%eax, %r13d
	jmp	.LBB9_93
.LBB9_78:
	movl	$0, 96(%rsp)
	leaq	208(%rsp), %rax
	movb	$0, 48(%rax)
	movq	$0, 40(%rax)
	movq	(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp246:
	movl	$12, %edx
	movl	4(%rsp), %esi           # 4-byte Reload
	leaq	96(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp247:
# BB#79:
	testl	%r13d, %r13d
	jne	.LBB9_87
# BB#80:
	movzwl	96(%rsp), %eax
	testw	%ax, %ax
	je	.LBB9_83
# BB#81:
	movl	$-2147467259, %r13d     # imm = 0x80004005
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB9_87
# BB#82:
	movq	104(%rsp), %rax
	jmp	.LBB9_85
.LBB9_83:
	cmpb	$0, 56(%r15)
	je	.LBB9_86
# BB#84:
	movq	48(%r15), %rax
.LBB9_85:                               # %.sink.split.i
	movq	%rax, 248(%rsp)
	movb	$1, 256(%rsp)
.LBB9_86:
	xorl	%r13d, %r13d
.LBB9_87:
.Ltmp252:
	leaq	96(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp253:
	movq	8(%rsp), %rbp           # 8-byte Reload
# BB#88:
	movl	$1, %ebx
	testl	%r13d, %r13d
	jne	.LBB9_93
# BB#89:
.Ltmp254:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp255:
# BB#90:                                # %.noexc165
.Ltmp256:
	movq	%rbx, %rdi
	leaq	200(%rsp), %rsi
	callq	_ZN4CArcC2ERKS_
.Ltmp257:
# BB#91:
.Ltmp259:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp260:
# BB#92:                                # %_ZN13CObjectVectorI4CArcE3AddERKS0_.exit169
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	xorl	%ebx, %ebx
	movl	%r14d, %r13d
.LBB9_93:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_95
# BB#94:
	movq	(%rdi), %rax
.Ltmp264:
	callq	*16(%rax)
.Ltmp265:
.LBB9_95:
	movq	264(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_97
# BB#96:
	callq	_ZdaPv
.LBB9_97:                               # %_ZN11CStringBaseIwED2Ev.exit.i157
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_99
# BB#98:
	callq	_ZdaPv
.LBB9_99:                               # %_ZN11CStringBaseIwED2Ev.exit1.i158
	movq	208(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_101
# BB#100:
	callq	_ZdaPv
.LBB9_101:                              # %_ZN11CStringBaseIwED2Ev.exit2.i159
	movq	200(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_59
# BB#102:
	movq	(%rdi), %rax
.Ltmp269:
	callq	*16(%rax)
.Ltmp270:
	jmp	.LBB9_59
.LBB9_103:
	cmpl	$2, %ebx
	jne	.LBB9_106
# BB#104:                               # %..thread189_crit_edge
	movl	12(%r12), %eax
.LBB9_105:                              # %.thread189
	testl	%eax, %eax
	setne	72(%r12)
	xorl	%eax, %eax
	jmp	.LBB9_107
.LBB9_106:                              # %.loopexit.loopexit
	movl	%r14d, %eax
.LBB9_107:                              # %.loopexit
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_108:
.Ltmp258:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB9_116
.LBB9_109:
.Ltmp248:
	movq	%rax, %r14
.Ltmp249:
	leaq	96(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp250:
	jmp	.LBB9_116
.LBB9_110:
.Ltmp251:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_111:
.Ltmp261:
	jmp	.LBB9_115
.LBB9_112:
.Ltmp266:
	jmp	.LBB9_119
.LBB9_113:
.Ltmp271:
	jmp	.LBB9_130
.LBB9_114:
.Ltmp245:
.LBB9_115:                              # %.body167
	movq	%rax, %r14
.LBB9_116:                              # %.body167
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_120
# BB#117:
	movq	(%rdi), %rax
.Ltmp262:
	callq	*16(%rax)
.Ltmp263:
	jmp	.LBB9_120
.LBB9_118:
.Ltmp238:
.LBB9_119:
	movq	%rax, %r14
.LBB9_120:
	movq	264(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_122
# BB#121:
	callq	_ZdaPv
.LBB9_122:                              # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_124
# BB#123:
	callq	_ZdaPv
.LBB9_124:                              # %_ZN11CStringBaseIwED2Ev.exit1.i
	movq	208(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_126
# BB#125:
	callq	_ZdaPv
.LBB9_126:                              # %_ZN11CStringBaseIwED2Ev.exit2.i
	movq	200(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_131
# BB#127:
	movq	(%rdi), %rax
.Ltmp267:
	callq	*16(%rax)
.Ltmp268:
	jmp	.LBB9_131
.LBB9_128:
.Ltmp276:
	jmp	.LBB9_135
.LBB9_129:
.Ltmp235:
.LBB9_130:
	movq	%rax, %r14
.LBB9_131:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_136
# BB#132:
	movq	(%rdi), %rax
.Ltmp272:
	callq	*16(%rax)
.Ltmp273:
	jmp	.LBB9_136
.LBB9_133:
.Ltmp281:
	jmp	.LBB9_141
.LBB9_134:
.Ltmp230:
.LBB9_135:
	movq	%rax, %r14
.LBB9_136:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_142
# BB#137:
	movq	(%rdi), %rax
.Ltmp277:
	callq	*16(%rax)
.Ltmp278:
	jmp	.LBB9_142
.LBB9_138:
.Ltmp222:
	jmp	.LBB9_145
.LBB9_139:
.Ltmp292:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB9_147
.LBB9_140:
.Ltmp227:
.LBB9_141:
	movq	%rax, %r14
.LBB9_142:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_155
# BB#143:
	movq	(%rdi), %rax
.Ltmp282:
	callq	*16(%rax)
.Ltmp283:
	jmp	.LBB9_155
.LBB9_144:
.Ltmp219:
.LBB9_145:
	movq	%rax, %r14
.Ltmp223:
	leaq	80(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp224:
	jmp	.LBB9_155
.LBB9_146:
.Ltmp295:
	movq	%rax, %r14
.LBB9_147:                              # %.body
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_149
# BB#148:
	callq	_ZdaPv
.LBB9_149:                              # %_ZN11CStringBaseIwED2Ev.exit.i139
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_151
# BB#150:
	callq	_ZdaPv
.LBB9_151:                              # %_ZN11CStringBaseIwED2Ev.exit1.i140
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_153
# BB#152:
	callq	_ZdaPv
.LBB9_153:                              # %_ZN11CStringBaseIwED2Ev.exit2.i141
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_155
# BB#154:
	movq	(%rdi), %rax
.Ltmp296:
	callq	*16(%rax)
.Ltmp297:
.LBB9_155:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_156:
.Ltmp298:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback, .Lfunc_end9-_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\316\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\305\002"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp284-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp284
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp289-.Ltmp284       #   Call between .Ltmp284 and .Ltmp289
	.long	.Ltmp295-.Lfunc_begin6  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin6  #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin6  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin6  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin6  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp225-.Ltmp221       #   Call between .Ltmp221 and .Ltmp225
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp226-.Ltmp225       #   Call between .Ltmp225 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin6  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp228-.Ltmp226       #   Call between .Ltmp226 and .Ltmp228
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin6  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp235-.Lfunc_begin6  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp274-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin6  #     jumps to .Ltmp276
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin6  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin6  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin6  #     jumps to .Ltmp281
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp244-.Ltmp239       #   Call between .Ltmp239 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin6  #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp246-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin6  #     jumps to .Ltmp248
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp255-.Ltmp252       #   Call between .Ltmp252 and .Ltmp255
	.long	.Ltmp261-.Lfunc_begin6  #     jumps to .Ltmp261
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin6  #     jumps to .Ltmp258
	.byte	0                       #   On action: cleanup
	.long	.Ltmp259-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin6  #     jumps to .Ltmp261
	.byte	0                       #   On action: cleanup
	.long	.Ltmp264-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp265-.Ltmp264       #   Call between .Ltmp264 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin6  #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin6  #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp249-.Lfunc_begin6  # >> Call Site 23 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin6  #     jumps to .Ltmp251
	.byte	1                       #   On action: 1
	.long	.Ltmp262-.Lfunc_begin6  # >> Call Site 24 <<
	.long	.Ltmp297-.Ltmp262       #   Call between .Ltmp262 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin6  #     jumps to .Ltmp298
	.byte	1                       #   On action: 1
	.long	.Ltmp297-.Lfunc_begin6  # >> Call Site 25 <<
	.long	.Lfunc_end9-.Ltmp297    #   Call between .Ltmp297 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.zero	16
	.section	.text._ZN4CArcC2Ev,"axG",@progbits,_ZN4CArcC2Ev,comdat
	.weak	_ZN4CArcC2Ev
	.p2align	4, 0x90
	.type	_ZN4CArcC2Ev,@function
_ZN4CArcC2Ev:                           # @_ZN4CArcC2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -40
.Lcfi84:
	.cfi_offset %r12, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp299:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp300:
# BB#1:
	movq	%r15, 8(%rbx)
	movl	$0, (%r15)
	movl	$4, 20(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.Ltmp302:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp303:
# BB#2:
	movq	%r12, 24(%rbx)
	movl	$0, (%r12)
	movl	$4, 36(%rbx)
	movb	$0, 56(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp305:
	movl	$16, %edi
	callq	_Znam
.Ltmp306:
# BB#3:
	movq	%rax, 64(%rbx)
	movl	$0, (%rax)
	movl	$4, 76(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB10_6:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp307:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	8(%rbx), %r15
	testq	%r15, %r15
	jne	.LBB10_7
	jmp	.LBB10_8
.LBB10_5:                               # %_ZN11CStringBaseIwED2Ev.exit.thread
.Ltmp304:
	movq	%rax, %r14
.LBB10_7:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB10_8
.LBB10_4:
.Ltmp301:
	movq	%rax, %r14
.LBB10_8:                               # %_ZN11CStringBaseIwED2Ev.exit6
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp308:
	callq	*16(%rax)
.Ltmp309:
.LBB10_10:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_11:
.Ltmp310:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN4CArcC2Ev, .Lfunc_end10-_ZN4CArcC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp299-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin7  #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp302-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin7  #     jumps to .Ltmp304
	.byte	0                       #   On action: cleanup
	.long	.Ltmp305-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp306-.Ltmp305       #   Call between .Ltmp305 and .Ltmp306
	.long	.Ltmp307-.Lfunc_begin7  #     jumps to .Ltmp307
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin7  #     jumps to .Ltmp310
	.byte	1                       #   On action: 1
	.long	.Ltmp309-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Lfunc_end10-.Ltmp309   #   Call between .Ltmp309 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.zero	16
	.text
	.globl	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
	.p2align	4, 0x90
	.type	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI,@function
_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI: # @_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi93:
	.cfi_def_cfa_offset 192
.Lcfi94:
	.cfi_offset %rbx, -56
.Lcfi95:
	.cfi_offset %r12, -48
.Lcfi96:
	.cfi_offset %r13, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movl	%ecx, %r12d
	movq	%rdx, %r13
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	$0, 64(%rdi)
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp311:
	movq	%rbx, %rdi
	callq	_ZN16COpenCallbackImpC2Ev
.Ltmp312:
# BB#1:
	movq	192(%rsp), %rbp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	%rbp, 168(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
.Ltmp314:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp315:
# BB#2:
	movq	%rbp, 96(%rsp)
	movl	$0, (%rbp)
	movl	$4, 108(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp317:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp318:
# BB#3:
	movq	%r14, 32(%rsp)
	movl	$0, (%r14)
	movl	$4, 44(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
.Ltmp320:
	movl	$16, %edi
	callq	_Znam
.Ltmp321:
# BB#4:
	movq	%rax, 48(%rsp)
	movl	$0, (%rax)
	movl	$4, 60(%rsp)
	movq	24(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movl	%r12d, %r14d
	jne	.LBB11_49
# BB#5:
	testb	%r14b, %r14b
	jne	.LBB11_49
# BB#6:
	movq	(%r15), %rdi
.Ltmp323:
	leaq	96(%rsp), %rsi
	leaq	76(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
.Ltmp324:
# BB#7:
	testb	%al, %al
	je	.LBB11_131
# BB#8:
	movq	%r13, 128(%rsp)         # 8-byte Spill
	movl	76(%rsp), %ecx
.Ltmp325:
	leaq	80(%rsp), %rdi
	leaq	96(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp326:
# BB#9:                                 # %_ZNK11CStringBaseIwE4LeftEi.exit
	movl	$0, 40(%rsp)
	movq	32(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	88(%rsp), %r13
	incq	%r13
	movl	44(%rsp), %r14d
	cmpl	%r14d, %r13d
	movq	%r15, 120(%rsp)         # 8-byte Spill
	je	.LBB11_15
# BB#10:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp328:
	callq	_Znam
	movq	%rax, %r15
.Ltmp329:
# BB#11:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB11_14
# BB#12:                                # %.noexc
	testl	%r14d, %r14d
	jle	.LBB11_14
# BB#13:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
.LBB11_14:                              # %._crit_edge16.i.i
	movq	%r15, 32(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 44(%rsp)
	movq	%r15, %rbp
	movq	120(%rsp), %r15         # 8-byte Reload
.LBB11_15:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	80(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_16:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_16
# BB#17:
	movl	88(%rsp), %eax
	movl	%eax, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB11_19
# BB#18:
	callq	_ZdaPv
.LBB11_19:                              # %_ZN11CStringBaseIwED2Ev.exit
	movl	76(%rsp), %edx
	movl	104(%rsp), %ecx
	subl	%edx, %ecx
.Ltmp331:
	leaq	80(%rsp), %rdi
	leaq	96(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp332:
# BB#20:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	movl	$0, 56(%rsp)
	movq	48(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	88(%rsp), %r13
	incq	%r13
	movl	60(%rsp), %r14d
	cmpl	%r14d, %r13d
	jne	.LBB11_32
# BB#21:
	movl	%r12d, %r14d
	jmp	.LBB11_38
.LBB11_49:
	movq	(%rbx), %rax
	movq	(%r15), %rsi
.Ltmp340:
	movq	%rbx, %rdi
	callq	*80(%rax)
.Ltmp341:
	jmp	.LBB11_50
.LBB11_131:
	callq	__errno_location
	movl	(%rax), %ebp
	jmp	.LBB11_132
.LBB11_32:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp334:
	callq	_Znam
	movq	%rax, %r15
.Ltmp335:
# BB#33:                                # %.noexc63
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB11_34
# BB#35:                                # %.noexc63
	testl	%r14d, %r14d
	movl	%r12d, %r14d
	jle	.LBB11_37
# BB#36:                                # %._crit_edge.thread.i.i58
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	56(%rsp), %rax
	jmp	.LBB11_37
.LBB11_34:
	movl	%r12d, %r14d
.LBB11_37:                              # %._crit_edge16.i.i59
	movq	%r15, 48(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 60(%rsp)
	movq	%r15, %rbp
	movq	120(%rsp), %r15         # 8-byte Reload
.LBB11_38:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i60
	movq	80(%rsp), %rdi
	xorl	%eax, %eax
	movq	128(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB11_39:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_39
# BB#40:
	movl	88(%rsp), %eax
	movl	%eax, 56(%rsp)
	testq	%rdi, %rdi
	je	.LBB11_42
# BB#41:
	callq	_ZdaPv
.LBB11_42:                              # %_ZN11CStringBaseIwED2Ev.exit65
.Ltmp337:
	leaq	32(%rsp), %rsi
	leaq	48(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_
.Ltmp338:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB11_50:
.Ltmp342:
	movq	%rbx, (%rsp)
	movzbl	%r14b, %ecx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rbp, %r8
	movq	%r15, %r9
	callq	_ZN12CArchiveLink4OpenEP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP20IArchiveOpenCallback
	movl	%eax, %ebp
.Ltmp343:
# BB#51:
	testl	%ebp, %ebp
	jne	.LBB11_132
# BB#52:
	movslq	40(%rsp), %r12
	leaq	1(%r12), %r13
	testl	%r13d, %r13d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB11_53
# BB#54:                                # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp345:
	callq	_Znam
	movq	%rax, %r14
.Ltmp346:
# BB#55:                                # %.noexc68
	movl	$0, (%r14)
	movq	%r14, %rbp
	jmp	.LBB11_56
.LBB11_53:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
.LBB11_56:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	32(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_57:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbp,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_57
# BB#58:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	56(%rsp), %eax
	movl	%r13d, %ecx
	subl	%r12d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB11_60
# BB#59:
	movq	%r14, %r15
	jmp	.LBB11_71
.LBB11_60:
	decl	%ecx
	cmpl	$8, %r13d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB11_62
# BB#61:                                # %select.true.sink
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB11_62:                              # %select.end
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%r13,%rdi), %eax
	cmpl	%r13d, %eax
	jne	.LBB11_64
# BB#63:
	movq	%r14, %r15
	jmp	.LBB11_71
.LBB11_64:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp348:
	callq	_Znam
	movq	%rax, %r15
.Ltmp349:
# BB#65:                                # %.noexc117
	testl	%r13d, %r13d
	jle	.LBB11_70
# BB#66:                                # %.preheader.i.i
	testl	%r12d, %r12d
	jle	.LBB11_68
# BB#67:                                # %.lr.ph.i.i
	leaq	(,%r12,4), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	memcpy
	jmp	.LBB11_69
.LBB11_68:                              # %._crit_edge.i.i
	testq	%rbp, %rbp
	je	.LBB11_70
.LBB11_69:                              # %._crit_edge.thread.i.i114
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB11_70:                              # %._crit_edge16.i.i115
	movl	$0, (%r15,%r12,4)
	movq	%r15, %rbp
.LBB11_71:                              # %.noexc.i
	leaq	(%rbp,%r12,4), %rcx
	movq	48(%rsp), %rsi
	.p2align	4, 0x90
.LBB11_72:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB11_72
# BB#73:
	movslq	56(%rsp), %r13
.Ltmp351:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp352:
# BB#74:                                # %.noexc74
	addq	%r12, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	leaq	1(%r13), %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp353:
	callq	_Znam
.Ltmp354:
# BB#75:                                # %.noexc.i70
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r14d, 12(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_76:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i71
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_76
# BB#77:
	movl	%r13d, 8(%rbx)
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %rdi
.Ltmp356:
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp357:
# BB#78:
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	48(%rdx), %rax
	movslq	44(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 44(%rdx)
	testq	%rbp, %rbp
	je	.LBB11_80
# BB#79:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB11_80:                              # %_ZN11CStringBaseIwED2Ev.exit78.preheader
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 148(%rbx)
	jle	.LBB11_123
# BB#81:                                # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_82:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_99 Depth 2
                                        #     Child Loop BB11_114 Depth 2
                                        #     Child Loop BB11_118 Depth 2
	movq	152(%rbx), %rax
	movq	(%rax,%r14,8), %r15
	movslq	40(%rsp), %r13
	leaq	1(%r13), %rbp
	testl	%ebp, %ebp
	je	.LBB11_83
# BB#96:                                # %._crit_edge16.i.i.i80
                                        #   in Loop: Header=BB11_82 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp359:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp360:
# BB#97:                                # %.noexc89
                                        #   in Loop: Header=BB11_82 Depth=1
	movl	$0, (%r12)
	movq	%r12, %rbx
	jmp	.LBB11_98
	.p2align	4, 0x90
.LBB11_83:                              #   in Loop: Header=BB11_82 Depth=1
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
.LBB11_98:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i81
                                        #   in Loop: Header=BB11_82 Depth=1
	movq	32(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_99:                              #   Parent Loop BB11_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_99
# BB#100:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i84
                                        #   in Loop: Header=BB11_82 Depth=1
	movl	8(%r15), %eax
	movl	%ebp, %ecx
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB11_102
# BB#101:                               #   in Loop: Header=BB11_82 Depth=1
	movq	%r12, 24(%rsp)          # 8-byte Spill
	jmp	.LBB11_113
	.p2align	4, 0x90
.LBB11_102:                             #   in Loop: Header=BB11_82 Depth=1
	decl	%ecx
	cmpl	$8, %ebp
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB11_104
# BB#103:                               # %select.true.sink358
                                        #   in Loop: Header=BB11_82 Depth=1
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB11_104:                             # %select.end357
                                        #   in Loop: Header=BB11_82 Depth=1
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%rbp,%rdi), %eax
	cmpl	%ebp, %eax
	jne	.LBB11_106
# BB#105:                               #   in Loop: Header=BB11_82 Depth=1
	movq	%r12, 24(%rsp)          # 8-byte Spill
	jmp	.LBB11_113
	.p2align	4, 0x90
.LBB11_106:                             #   in Loop: Header=BB11_82 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp362:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Ltmp363:
# BB#107:                               # %.noexc131
                                        #   in Loop: Header=BB11_82 Depth=1
	testl	%ebp, %ebp
	jle	.LBB11_112
# BB#108:                               # %.preheader.i.i121
                                        #   in Loop: Header=BB11_82 Depth=1
	testl	%r13d, %r13d
	jle	.LBB11_110
# BB#109:                               # %.lr.ph.i.i122
                                        #   in Loop: Header=BB11_82 Depth=1
	leaq	(,%r13,4), %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	memcpy
	jmp	.LBB11_111
.LBB11_110:                             # %._crit_edge.i.i123
                                        #   in Loop: Header=BB11_82 Depth=1
	testq	%rbx, %rbx
	je	.LBB11_112
.LBB11_111:                             # %._crit_edge.thread.i.i128
                                        #   in Loop: Header=BB11_82 Depth=1
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB11_112:                             # %._crit_edge16.i.i129
                                        #   in Loop: Header=BB11_82 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	$0, (%rbx,%r13,4)
.LBB11_113:                             # %.noexc.i85
                                        #   in Loop: Header=BB11_82 Depth=1
	leaq	(%rbx,%r13,4), %rcx
	movq	(%r15), %rsi
	.p2align	4, 0x90
.LBB11_114:                             #   Parent Loop BB11_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB11_114
# BB#115:                               #   in Loop: Header=BB11_82 Depth=1
	movslq	8(%r15), %rbp
.Ltmp365:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp366:
# BB#116:                               # %.noexc98
                                        #   in Loop: Header=BB11_82 Depth=1
	addq	%r13, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	leaq	1(%rbp), %r15
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp367:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp368:
# BB#117:                               # %.noexc.i94
                                        #   in Loop: Header=BB11_82 Depth=1
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	%r15d, 12(%r12)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_118:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i95
                                        #   Parent Loop BB11_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_118
# BB#119:                               #   in Loop: Header=BB11_82 Depth=1
	movl	%ebp, 8(%r12)
.Ltmp370:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp371:
# BB#120:                               #   in Loop: Header=BB11_82 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	48(%rdx), %rax
	movslq	44(%rdx), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 44(%rdx)
	testq	%rbx, %rbx
	je	.LBB11_122
# BB#121:                               #   in Loop: Header=BB11_82 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB11_122:                             # %_ZN11CStringBaseIwED2Ev.exit103
                                        #   in Loop: Header=BB11_82 Depth=1
	incq	%r14
	movq	16(%rsp), %rbx          # 8-byte Reload
	movslq	148(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB11_82
.LBB11_123:                             # %_ZN11CStringBaseIwED2Ev.exit78._crit_edge
	movq	184(%rbx), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 64(%rcx)
	xorl	%ebp, %ebp
.LBB11_132:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_134
# BB#133:
	callq	_ZdaPv
.LBB11_134:                             # %_ZN11CStringBaseIwED2Ev.exit105
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_136
# BB#135:
	callq	_ZdaPv
.LBB11_136:                             # %_ZN11CStringBaseIwED2Ev.exit106
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_138
# BB#137:
	callq	_ZdaPv
.LBB11_138:                             # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movl	%ebp, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_84:
.Ltmp350:
	movq	%rax, %r13
	testq	%rbp, %rbp
	je	.LBB11_92
# BB#85:
	movq	%r14, %rdi
	jmp	.LBB11_91
.LBB11_47:
.Ltmp336:
	jmp	.LBB11_45
.LBB11_44:
.Ltmp330:
.LBB11_45:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB11_91
	jmp	.LBB11_92
.LBB11_87:
.Ltmp347:
	movq	%rax, %r13
	jmp	.LBB11_92
.LBB11_46:
.Ltmp333:
	jmp	.LBB11_31
.LBB11_43:
.Ltmp327:
	jmp	.LBB11_31
.LBB11_86:
.Ltmp355:
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	jne	.LBB11_90
	jmp	.LBB11_92
.LBB11_88:
.Ltmp358:
	movq	%rax, %r13
	testq	%rbp, %rbp
	je	.LBB11_92
.LBB11_90:
	movq	%r15, %rdi
	jmp	.LBB11_91
.LBB11_30:
.Ltmp339:
	jmp	.LBB11_31
.LBB11_28:                              # %.thread198
.Ltmp322:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB11_29
.LBB11_26:                              # %.thread203
.Ltmp319:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB11_27
.LBB11_24:
.Ltmp316:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB11_25
.LBB11_22:
.Ltmp313:
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r13, %rdi
	callq	_Unwind_Resume
.LBB11_48:
.Ltmp344:
.LBB11_31:                              # %_ZN11CStringBaseIwED2Ev.exit79
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB11_92
.LBB11_124:
.Ltmp364:
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB11_92
# BB#125:
	movq	%r12, %rdi
	jmp	.LBB11_91
.LBB11_127:
.Ltmp361:
	movq	%rax, %r13
	jmp	.LBB11_92
.LBB11_126:
.Ltmp369:
	movq	%rax, %r13
	movq	%r12, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	jne	.LBB11_130
	jmp	.LBB11_92
.LBB11_128:
.Ltmp372:
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB11_92
.LBB11_130:
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB11_91:                              # %_ZN11CStringBaseIwED2Ev.exit79
	callq	_ZdaPv
.LBB11_92:                              # %_ZN11CStringBaseIwED2Ev.exit79
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_94
# BB#93:
	callq	_ZdaPv
.LBB11_94:
	movq	32(%rsp), %r14
	testq	%r14, %r14
	je	.LBB11_95
.LBB11_29:
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB11_95:
	movq	96(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB11_25
.LBB11_27:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB11_25:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp373:
	callq	*16(%rax)
.Ltmp374:
# BB#23:                                # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit112
	movq	%r13, %rdi
	callq	_Unwind_Resume
.LBB11_139:
.Ltmp375:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI, .Lfunc_end11-_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\202\203\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\371\002"              # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp311-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp311
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin8  #     jumps to .Ltmp313
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp314-.Ltmp312       #   Call between .Ltmp312 and .Ltmp314
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp314-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin8  #     jumps to .Ltmp316
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin8  #     jumps to .Ltmp319
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin8  #     jumps to .Ltmp322
	.byte	0                       #   On action: cleanup
	.long	.Ltmp323-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp339-.Lfunc_begin8  #     jumps to .Ltmp339
	.byte	0                       #   On action: cleanup
	.long	.Ltmp325-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp326-.Ltmp325       #   Call between .Ltmp325 and .Ltmp326
	.long	.Ltmp327-.Lfunc_begin8  #     jumps to .Ltmp327
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin8  #     jumps to .Ltmp330
	.byte	0                       #   On action: cleanup
	.long	.Ltmp331-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp332-.Ltmp331       #   Call between .Ltmp331 and .Ltmp332
	.long	.Ltmp333-.Lfunc_begin8  #     jumps to .Ltmp333
	.byte	0                       #   On action: cleanup
	.long	.Ltmp340-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp341-.Ltmp340       #   Call between .Ltmp340 and .Ltmp341
	.long	.Ltmp344-.Lfunc_begin8  #     jumps to .Ltmp344
	.byte	0                       #   On action: cleanup
	.long	.Ltmp334-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin8  #     jumps to .Ltmp336
	.byte	0                       #   On action: cleanup
	.long	.Ltmp337-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin8  #     jumps to .Ltmp339
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin8  #     jumps to .Ltmp344
	.byte	0                       #   On action: cleanup
	.long	.Ltmp345-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin8  #     jumps to .Ltmp347
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin8  # >> Call Site 16 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin8  #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin8  # >> Call Site 17 <<
	.long	.Ltmp351-.Ltmp349       #   Call between .Ltmp349 and .Ltmp351
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin8  # >> Call Site 18 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp358-.Lfunc_begin8  #     jumps to .Ltmp358
	.byte	0                       #   On action: cleanup
	.long	.Ltmp353-.Lfunc_begin8  # >> Call Site 19 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin8  #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin8  # >> Call Site 20 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin8  #     jumps to .Ltmp358
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin8  # >> Call Site 21 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin8  #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin8  # >> Call Site 22 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin8  #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin8  # >> Call Site 23 <<
	.long	.Ltmp365-.Ltmp363       #   Call between .Ltmp363 and .Ltmp365
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin8  # >> Call Site 24 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp372-.Lfunc_begin8  #     jumps to .Ltmp372
	.byte	0                       #   On action: cleanup
	.long	.Ltmp367-.Lfunc_begin8  # >> Call Site 25 <<
	.long	.Ltmp368-.Ltmp367       #   Call between .Ltmp367 and .Ltmp368
	.long	.Ltmp369-.Lfunc_begin8  #     jumps to .Ltmp369
	.byte	0                       #   On action: cleanup
	.long	.Ltmp370-.Lfunc_begin8  # >> Call Site 26 <<
	.long	.Ltmp371-.Ltmp370       #   Call between .Ltmp370 and .Ltmp371
	.long	.Ltmp372-.Lfunc_begin8  #     jumps to .Ltmp372
	.byte	0                       #   On action: cleanup
	.long	.Ltmp371-.Lfunc_begin8  # >> Call Site 27 <<
	.long	.Ltmp373-.Ltmp371       #   Call between .Ltmp371 and .Ltmp373
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin8  # >> Call Site 28 <<
	.long	.Ltmp374-.Ltmp373       #   Call between .Ltmp373 and .Ltmp374
	.long	.Ltmp375-.Lfunc_begin8  #     jumps to .Ltmp375
	.byte	1                       #   On action: 1
	.long	.Ltmp374-.Lfunc_begin8  # >> Call Site 29 <<
	.long	.Lfunc_end11-.Ltmp374   #   Call between .Ltmp374 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.zero	16
	.section	.text._ZN16COpenCallbackImpC2Ev,"axG",@progbits,_ZN16COpenCallbackImpC2Ev,comdat
	.weak	_ZN16COpenCallbackImpC2Ev
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImpC2Ev,@function
_ZN16COpenCallbackImpC2Ev:              # @_ZN16COpenCallbackImpC2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 32(%rbx)
	movl	$_ZTV16COpenCallbackImp+120, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16COpenCallbackImp+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV16COpenCallbackImp+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTV16COpenCallbackImp+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
.Ltmp376:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp377:
# BB#1:
	movq	%r14, 40(%rbx)
	movl	$0, (%r14)
	movl	$4, 52(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 96(%rbx)
.Ltmp379:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp380:
# BB#2:
	leaq	40(%rbx), %r14
	movq	%r15, 96(%rbx)
	movl	$0, (%r15)
	movl	$4, 108(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%rbx)
.Ltmp382:
	movl	$16, %edi
	callq	_Znam
.Ltmp383:
# BB#3:
	movq	%rax, 120(%rbx)
	movl	$0, (%rax)
	movl	$4, 132(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 144(%rbx)
	movq	$8, 160(%rbx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 136(%rbx)
	movdqu	%xmm0, 168(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_6:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
.Ltmp384:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB12_7
	jmp	.LBB12_8
.LBB12_5:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit.thread
.Ltmp381:
	movq	%rax, %rbx
.LBB12_7:
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB12_8:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp378:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN16COpenCallbackImpC2Ev, .Lfunc_end12-_ZN16COpenCallbackImpC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp376-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp377-.Ltmp376       #   Call between .Ltmp376 and .Ltmp377
	.long	.Ltmp378-.Lfunc_begin9  #     jumps to .Ltmp378
	.byte	0                       #   On action: cleanup
	.long	.Ltmp379-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp380-.Ltmp379       #   Call between .Ltmp379 and .Ltmp380
	.long	.Ltmp381-.Lfunc_begin9  #     jumps to .Ltmp381
	.byte	0                       #   On action: cleanup
	.long	.Ltmp382-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp383-.Ltmp382       #   Call between .Ltmp382 and .Ltmp383
	.long	.Ltmp384-.Lfunc_begin9  #     jumps to .Ltmp384
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp383   #   Call between .Ltmp383 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_,"axG",@progbits,_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_,comdat
	.weak	_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_
	.p2align	4, 0x90
	.type	_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_,@function
_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_: # @_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 80
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	leaq	40(%r13), %r15
	cmpq	%r12, %r15
	je	.LBB13_9
# BB#1:
	movl	$0, 48(%r13)
	movq	40(%r13), %rbx
	movl	$0, (%rbx)
	movslq	8(%r12), %rbp
	incq	%rbp
	movl	52(%r13), %r14d
	cmpl	%r14d, %ebp
	je	.LBB13_6
# BB#2:
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#3:
	testl	%r14d, %r14d
	jle	.LBB13_5
# BB#4:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	48(%r13), %rcx
.LBB13_5:                               # %._crit_edge16.i.i
	movq	%rax, 40(%r13)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 52(%r13)
	movq	%rax, %rbx
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB13_6:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB13_7
# BB#8:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r12), %eax
	movl	%eax, 48(%r13)
.LBB13_9:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	leaq	56(%r13), %rbx
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
	movq	8(%rsp), %rsi
.Ltmp385:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
	movl	%eax, %ebx
.Ltmp386:
# BB#10:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_12
# BB#11:
	callq	_ZdaPv
.LBB13_12:                              # %_ZN11CStringBaseIwED2Ev.exit
	testb	%bl, %bl
	je	.LBB13_13
# BB#17:
	leaq	136(%r13), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movb	$0, 112(%r13)
	movq	$0, 184(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_13:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB13_14:
.Ltmp387:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_16
# BB#15:
	callq	_ZdaPv
.LBB13_16:                              # %_ZN11CStringBaseIwED2Ev.exit4
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_, .Lfunc_end13-_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp385-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp385
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp385-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin10 #     jumps to .Ltmp387
	.byte	0                       #   On action: cleanup
	.long	.Ltmp386-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Lfunc_end13-.Ltmp386   #   Call between .Ltmp386 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 48
.Lcfi124:
	.cfi_offset %rbx, -48
.Lcfi125:
	.cfi_offset %r12, -40
.Lcfi126:
	.cfi_offset %r13, -32
.Lcfi127:
	.cfi_offset %r14, -24
.Lcfi128:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB14_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB14_3
.LBB14_1:
	xorl	%eax, %eax
.LBB14_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB14_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp388:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp389:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB14_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB14_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB14_9:
.Ltmp390:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_11
# BB#10:
	callq	_ZdaPv
.LBB14_11:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end14-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp388-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp388
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin11 #     jumps to .Ltmp390
	.byte	0                       #   On action: cleanup
	.long	.Ltmp389-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Lfunc_end14-.Ltmp389   #   Call between .Ltmp389 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback,@function
_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback: # @_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi135:
	.cfi_def_cfa_offset 144
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r14
	movl	12(%r14), %eax
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmpl	$1, %eax
	jg	.LBB15_32
# BB#1:
	testl	%eax, %eax
	je	.LBB15_27
# BB#2:
	xorl	%ebp, %ebp
.Ltmp391:
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp392:
# BB#3:                                 # %.noexc25
.Ltmp393:
	movq	%r13, %rdi
	callq	_ZN16COpenCallbackImpC2Ev
.Ltmp394:
# BB#4:
	movq	(%r13), %rax
	xorl	%ebp, %ebp
.Ltmp396:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp397:
# BB#5:                                 # %_ZN9CMyComPtrI20IArchiveOpenCallbackEaSEPS0_.exit.i
	movq	$0, 168(%r13)
	testq	%r15, %r15
	je	.LBB15_7
# BB#6:
	movq	%r13, %rbp
	movq	(%r15), %rax
.Ltmp398:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp399:
.LBB15_7:                               # %.noexc28
	movq	176(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB15_9
# BB#8:
	movq	%r13, %rbp
	movq	(%rdi), %rax
.Ltmp400:
	callq	*16(%rax)
.Ltmp401:
.LBB15_9:                               # %_ZN9CMyComPtrI20IArchiveOpenCallbackEaSEPS0_.exit1.i
	movq	%r13, %rbp
	movq	%r15, 176(%r13)
	movq	$0, 32(%rsp)
.Ltmp402:
	movl	$16, %edi
	callq	_Znam
.Ltmp403:
# BB#10:                                # %.noexc30
	movq	%rax, 24(%rsp)
	movl	$0, (%rax)
	movl	$4, 36(%rsp)
	movq	(%r12), %rdi
.Ltmp404:
	leaq	24(%rsp), %rsi
	leaq	20(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
.Ltmp405:
# BB#11:
	movl	20(%rsp), %ecx
.Ltmp407:
	leaq	40(%rsp), %rdi
	leaq	24(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp408:
# BB#12:                                # %_ZNK11CStringBaseIwE4LeftEi.exit.i
	movl	20(%rsp), %edx
	movl	32(%rsp), %ecx
	subl	%edx, %ecx
.Ltmp410:
	leaq	72(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp411:
# BB#13:                                # %_ZNK11CStringBaseIwE3MidEi.exit.i
.Ltmp413:
	leaq	40(%rsp), %rsi
	leaq	72(%rsp), %rdx
	movq	%r13, %rdi
	callq	_ZN16COpenCallbackImp4InitERK11CStringBaseIwES3_
.Ltmp414:
# BB#14:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_16
# BB#15:
	callq	_ZdaPv
.LBB15_16:                              # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_18
# BB#17:
	callq	_ZdaPv
.LBB15_18:                              # %_ZN11CStringBaseIwED2Ev.exit2.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_20
# BB#19:
	callq	_ZdaPv
.LBB15_20:
.Ltmp416:
	movq	%r13, %rbp
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp417:
# BB#21:
	movl	$0, 16(%rbx)
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%rbx)
	movl	$-1, 32(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
.Ltmp419:
	movl	$4, %edi
	callq	_Znam
.Ltmp420:
# BB#22:
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	$4, 52(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 24(%rbx)
	movb	$1, 20(%rbx)
.Ltmp422:
	movq	%rbx, %rdi
	callq	*_ZTV13CInFileStream+24(%rip)
.Ltmp423:
# BB#23:                                # %_ZN9CMyComPtrI9IInStreamEC2EPS0_.exit
	movq	(%r12), %rsi
.Ltmp425:
	movq	%rbx, %rdi
	callq	_ZN13CInFileStream4OpenEPKw
.Ltmp426:
# BB#24:
	testb	%al, %al
	je	.LBB15_29
# BB#25:
	movslq	12(%r14), %rax
	movq	16(%r14), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp427:
	movl	$_ZL22kMaxCheckStartPosition, %edx
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp428:
# BB#26:
	testl	%ebp, %ebp
	sete	72(%r14)
	jmp	.LBB15_30
.LBB15_27:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rsp)
	movq	$4, 64(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 40(%rsp)
.Ltmp437:
	movq	$0, (%rsp)
	leaq	40(%rsp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r12, %r9
	callq	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
	movl	%eax, %ebp
.Ltmp438:
# BB#28:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	jmp	.LBB15_32
.LBB15_29:
	callq	__errno_location
	movl	(%rax), %ebp
.LBB15_30:
	movq	(%rbx), %rax
.Ltmp432:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp433:
# BB#31:                                # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit23
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*16(%rax)
.LBB15_32:
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_33:
.Ltmp439:
	movq	%rax, %r14
.Ltmp440:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp441:
	jmp	.LBB15_54
.LBB15_34:
.Ltmp434:
	jmp	.LBB15_36
.LBB15_35:
.Ltmp424:
.LBB15_36:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit34.thread
	movq	%rax, %r14
	movq	%r13, %rbp
	jmp	.LBB15_53
.LBB15_37:
.Ltmp421:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r13, %rbp
	jmp	.LBB15_53
.LBB15_38:
.Ltmp415:
	movq	%rax, %r14
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_41
# BB#39:
	callq	_ZdaPv
	jmp	.LBB15_41
.LBB15_40:
.Ltmp412:
	movq	%rax, %r14
.LBB15_41:                              # %_ZN11CStringBaseIwED2Ev.exit4.i
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_46
# BB#42:
	callq	_ZdaPv
	jmp	.LBB15_46
.LBB15_43:
.Ltmp409:
	jmp	.LBB15_45
.LBB15_44:
.Ltmp406:
.LBB15_45:
	movq	%rax, %r14
.LBB15_46:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_48
# BB#47:
	callq	_ZdaPv
	jmp	.LBB15_48
.LBB15_49:
.Ltmp395:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	xorl	%r13d, %r13d
.LBB15_48:
	movq	%r13, %rbp
	testq	%rbp, %rbp
	jne	.LBB15_53
	jmp	.LBB15_54
.LBB15_50:
.Ltmp429:
	movq	%rax, %r14
	movq	(%rbx), %rax
.Ltmp430:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp431:
# BB#51:
	movq	%r13, %rbp
	jmp	.LBB15_53
.LBB15_52:
.Ltmp418:
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB15_54
.LBB15_53:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit34.thread
	movq	(%rbp), %rax
.Ltmp435:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp436:
.LBB15_54:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_55:
.Ltmp442:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback, .Lfunc_end15-_ZN12CArchiveLink6ReOpenEP7CCodecsRK11CStringBaseIwEP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\346\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Ltmp391-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp392-.Ltmp391       #   Call between .Ltmp391 and .Ltmp392
	.long	.Ltmp418-.Lfunc_begin12 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin12 #     jumps to .Ltmp395
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp403-.Ltmp396       #   Call between .Ltmp396 and .Ltmp403
	.long	.Ltmp418-.Lfunc_begin12 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp404-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin12 #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin12 #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin12 #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin12 #     jumps to .Ltmp415
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin12 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp419-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin12 #     jumps to .Ltmp421
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin12 #     jumps to .Ltmp424
	.byte	0                       #   On action: cleanup
	.long	.Ltmp425-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp428-.Ltmp425       #   Call between .Ltmp425 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin12 #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp437-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin12 #     jumps to .Ltmp439
	.byte	0                       #   On action: cleanup
	.long	.Ltmp438-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp432-.Ltmp438       #   Call between .Ltmp438 and .Ltmp432
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp432-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp434-.Lfunc_begin12 #     jumps to .Ltmp434
	.byte	0                       #   On action: cleanup
	.long	.Ltmp433-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Ltmp440-.Ltmp433       #   Call between .Ltmp433 and .Ltmp440
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin12 # >> Call Site 16 <<
	.long	.Ltmp436-.Ltmp440       #   Call between .Ltmp440 and .Ltmp436
	.long	.Ltmp442-.Lfunc_begin12 #     jumps to .Ltmp442
	.byte	1                       #   On action: 1
	.long	.Ltmp436-.Lfunc_begin12 # >> Call Site 17 <<
	.long	.Lfunc_end15-.Ltmp436   #   Call between .Ltmp436 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -24
.Lcfi146:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp443:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp444:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp445:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end16-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp443-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp444-.Ltmp443       #   Call between .Ltmp443 and .Ltmp444
	.long	.Ltmp445-.Lfunc_begin13 #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp444-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp444   #   Call between .Ltmp444 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 32
.Lcfi150:
	.cfi_offset %rbx, -24
.Lcfi151:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp446:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp447:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB17_2:
.Ltmp448:
	movq	%rax, %r14
.Ltmp449:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp450:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp451:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end17-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp446-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp447-.Ltmp446       #   Call between .Ltmp446 and .Ltmp447
	.long	.Ltmp448-.Lfunc_begin14 #     jumps to .Ltmp448
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp449-.Ltmp447       #   Call between .Ltmp447 and .Ltmp449
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp449-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp450-.Ltmp449       #   Call between .Ltmp449 and .Ltmp450
	.long	.Ltmp451-.Lfunc_begin14 #     jumps to .Ltmp451
	.byte	1                       #   On action: 1
	.long	.Ltmp450-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp450   #   Call between .Ltmp450 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 32
.Lcfi155:
	.cfi_offset %rbx, -24
.Lcfi156:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp452:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp453:
# BB#1:
.Ltmp458:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp459:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp460:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp454:
	movq	%rax, %r14
.Ltmp455:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp456:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp457:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end18-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp452-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp453-.Ltmp452       #   Call between .Ltmp452 and .Ltmp453
	.long	.Ltmp454-.Lfunc_begin15 #     jumps to .Ltmp454
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin15 #     jumps to .Ltmp460
	.byte	0                       #   On action: cleanup
	.long	.Ltmp455-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin15 #     jumps to .Ltmp457
	.byte	1                       #   On action: 1
	.long	.Ltmp456-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp456   #   Call between .Ltmp456 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 64
.Lcfi164:
	.cfi_offset %rbx, -56
.Lcfi165:
	.cfi_offset %r12, -48
.Lcfi166:
	.cfi_offset %r13, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB19_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_6
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	callq	_ZdaPv
.LBB19_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB19_2
.LBB19_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end19:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 48
.Lcfi175:
	.cfi_offset %rbx, -48
.Lcfi176:
	.cfi_offset %r12, -40
.Lcfi177:
	.cfi_offset %r14, -32
.Lcfi178:
	.cfi_offset %r15, -24
.Lcfi179:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB20_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB20_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB20_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB20_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB20_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB20_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB20_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB20_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB20_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB20_17
.LBB20_7:
	xorl	%ecx, %ecx
.LBB20_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB20_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB20_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB20_10
.LBB20_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB20_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB20_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB20_13
	jmp	.LBB20_26
.LBB20_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB20_27
.LBB20_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB20_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB20_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB20_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB20_20
	jmp	.LBB20_21
.LBB20_18:
	xorl	%ebx, %ebx
.LBB20_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB20_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB20_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB20_23
.LBB20_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB20_8
	jmp	.LBB20_26
.Lfunc_end20:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end20-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi183:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi184:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi186:
	.cfi_def_cfa_offset 64
.Lcfi187:
	.cfi_offset %rbx, -56
.Lcfi188:
	.cfi_offset %r12, -48
.Lcfi189:
	.cfi_offset %r13, -40
.Lcfi190:
	.cfi_offset %r14, -32
.Lcfi191:
	.cfi_offset %r15, -24
.Lcfi192:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB21_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB21_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB21_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB21_5
.LBB21_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB21_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp461:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp462:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB21_35
# BB#12:
	movq	%rbx, %r13
.LBB21_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB21_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB21_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB21_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB21_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB21_15
.LBB21_14:
	xorl	%esi, %esi
.LBB21_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB21_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB21_16
.LBB21_29:
	movq	%r13, %rbx
.LBB21_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp463:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp464:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB21_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB21_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB21_8
.LBB21_3:
	xorl	%eax, %eax
.LBB21_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB21_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB21_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB21_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB21_30
.LBB21_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB21_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB21_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB21_24
	jmp	.LBB21_25
.LBB21_22:
	xorl	%ecx, %ecx
.LBB21_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB21_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB21_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB21_27
.LBB21_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB21_15
	jmp	.LBB21_29
.LBB21_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp465:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end21-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp461-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp461
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp464-.Ltmp461       #   Call between .Ltmp461 and .Ltmp464
	.long	.Ltmp465-.Lfunc_begin16 #     jumps to .Ltmp465
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end21-.Ltmp464   #   Call between .Ltmp464 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 32
.Lcfi196:
	.cfi_offset %rbx, -24
.Lcfi197:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp466:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp467:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp468:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end22-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp466-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp467-.Ltmp466       #   Call between .Ltmp466 and .Ltmp467
	.long	.Ltmp468-.Lfunc_begin17 #     jumps to .Ltmp468
	.byte	0                       #   On action: cleanup
	.long	.Ltmp467-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp467   #   Call between .Ltmp467 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN4CArcC2ERKS_,"axG",@progbits,_ZN4CArcC2ERKS_,comdat
	.weak	_ZN4CArcC2ERKS_
	.p2align	4, 0x90
	.type	_ZN4CArcC2ERKS_,@function
_ZN4CArcC2ERKS_:                        # @_ZN4CArcC2ERKS_
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%rbp
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi200:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi201:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi202:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 64
.Lcfi205:
	.cfi_offset %rbx, -56
.Lcfi206:
	.cfi_offset %r12, -48
.Lcfi207:
	.cfi_offset %r13, -40
.Lcfi208:
	.cfi_offset %r14, -32
.Lcfi209:
	.cfi_offset %r15, -24
.Lcfi210:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	(%r14), %rdi
	movq	%rdi, (%r13)
	testq	%rdi, %rdi
	je	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
	callq	*8(%rax)
.LBB23_2:                               # %_ZN9CMyComPtrI10IInArchiveEC2ERKS1_.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r13)
	movslq	16(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB23_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp469:
	callq	_Znam
	movq	%rax, %r15
.Ltmp470:
# BB#5:                                 # %.noexc
	movq	%r15, 8(%r13)
	movl	$0, (%r15)
	movl	%ebx, 20(%r13)
	jmp	.LBB23_6
.LBB23_3:
	xorl	%r15d, %r15d
.LBB23_6:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	leaq	8(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%r14), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB23_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB23_7
# BB#8:
	movl	%ebp, 16(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r13)
	movslq	32(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB23_9
# BB#10:                                # %._crit_edge16.i.i9
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp472:
	callq	_Znam
	movq	%rax, %r12
.Ltmp473:
# BB#11:                                # %.noexc13
	movq	%r12, 24(%r13)
	movl	$0, (%r12)
	movl	%ebx, 36(%r13)
	jmp	.LBB23_12
.LBB23_9:
	xorl	%r12d, %r12d
.LBB23_12:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i10
	movq	24(%r14), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB23_13:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB23_13
# BB#14:
	movl	%ebp, 32(%r13)
	movb	56(%r14), %al
	movb	%al, 56(%r13)
	movups	40(%r14), %xmm0
	movups	%xmm0, 40(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r13)
	movslq	72(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB23_15
# BB#16:                                # %._crit_edge16.i.i15
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp475:
	callq	_Znam
.Ltmp476:
# BB#17:                                # %.noexc19
	movq	%rax, 64(%r13)
	movl	$0, (%rax)
	movl	%ebx, 76(%r13)
	jmp	.LBB23_18
.LBB23_15:
	xorl	%eax, %eax
.LBB23_18:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i16
	movq	64(%r14), %rcx
	.p2align	4, 0x90
.LBB23_19:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB23_19
# BB#20:
	movl	%ebp, 72(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_23:
.Ltmp477:
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB23_25
# BB#24:
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.LBB23_26
	jmp	.LBB23_27
.LBB23_22:
.Ltmp474:
	movq	%rax, %r14
.LBB23_25:                              # %_ZN11CStringBaseIwED2Ev.exit
	testq	%r15, %r15
	je	.LBB23_27
.LBB23_26:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB23_27
.LBB23_21:
.Ltmp471:
	movq	%rax, %r14
.LBB23_27:                              # %_ZN11CStringBaseIwED2Ev.exit21
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB23_29
# BB#28:
	movq	(%rdi), %rax
.Ltmp478:
	callq	*16(%rax)
.Ltmp479:
.LBB23_29:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_30:
.Ltmp480:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN4CArcC2ERKS_, .Lfunc_end23-_ZN4CArcC2ERKS_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin18-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp469-.Lfunc_begin18 #   Call between .Lfunc_begin18 and .Ltmp469
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin18 #     jumps to .Ltmp471
	.byte	0                       #   On action: cleanup
	.long	.Ltmp472-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp473-.Ltmp472       #   Call between .Ltmp472 and .Ltmp473
	.long	.Ltmp474-.Lfunc_begin18 #     jumps to .Ltmp474
	.byte	0                       #   On action: cleanup
	.long	.Ltmp475-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin18 #     jumps to .Ltmp477
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin18 # >> Call Site 5 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin18 #     jumps to .Ltmp480
	.byte	1                       #   On action: 1
	.long	.Ltmp479-.Lfunc_begin18 # >> Call Site 6 <<
	.long	.Lfunc_end23-.Ltmp479   #   Call between .Ltmp479 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	101                     # 0x65
	.long	120                     # 0x78
	.long	101                     # 0x65
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	0                       # 0x0
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	0                       # 0x0
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	114                     # 0x72
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.3, 16

	.type	_ZL22kMaxCheckStartPosition,@object # @_ZL22kMaxCheckStartPosition
	.section	.rodata,"a",@progbits
	.p2align	3
_ZL22kMaxCheckStartPosition:
	.quad	4194304                 # 0x400000
	.size	_ZL22kMaxCheckStartPosition, 8

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.6:
	.long	85                      # 0x55
	.long	110                     # 0x6e
	.long	107                     # 0x6b
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	119                     # 0x77
	.long	110                     # 0x6e
	.long	32                      # 0x20
	.long	101                     # 0x65
	.long	114                     # 0x72
	.long	114                     # 0x72
	.long	111                     # 0x6f
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.6, 56

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
