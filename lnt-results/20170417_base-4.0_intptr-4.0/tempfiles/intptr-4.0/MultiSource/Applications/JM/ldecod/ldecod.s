	.text
	.file	"ldecod.bc"
	.globl	JMDecHelpExit
	.p2align	4, 0x90
	.type	JMDecHelpExit,@function
JMDecHelpExit:                          # @JMDecHelpExit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$782, %esi              # imm = 0x30E
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	JMDecHelpExit, .Lfunc_end0-JMDecHelpExit
	.cfi_endproc

	.globl	Configure
	.p2align	4, 0x90
	.type	Configure,@function
Configure:                              # @Configure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 64
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r15d
	movq	input(%rip), %rax
	movabsq	$3762249713818232180, %rcx # imm = 0x3436322E74736574
	movq	%rcx, (%rax)
	movb	$0, 8(%rax)
	movq	input(%rip), %rbp
	movabsq	$33343210581615972, %rax # imm = 0x7675792E636564
	movq	%rax, 1005(%rbp)
	movabsq	$7162241143541032308, %rax # imm = 0x6365645F74736574
	movq	%rax, 1000(%rbp)
	movabsq	$33343210581615986, %rax # imm = 0x7675792E636572
	movq	%rax, 2005(%rbp)
	movabsq	$7162256536703821172, %rax # imm = 0x6365725F74736574
	movq	%rax, 2000(%rbp)
	movq	$0, 3000(%rbp)
	movl	$2, 3008(%rbp)
	movl	$0, 3016(%rbp)
	cmpl	$2, %r15d
	jne	.LBB1_5
# BB#1:
	movq	8(%r12), %r14
	movl	$.L.str.4, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_37
# BB#2:
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_3
# BB#4:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	init_conf
	movl	$2, %ebx
	cmpl	%r15d, %ebx
	jl	.LBB1_14
	jmp	.LBB1_24
.LBB1_5:
	cmpl	$3, %r15d
	jl	.LBB1_6
# BB#7:
	movq	8(%r12), %r14
	movl	$.L.str.6, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	movl	$1, %ebx
	testl	%eax, %eax
	jne	.LBB1_9
# BB#8:
	movq	16(%r12), %rsi
	movq	%rbp, %rdi
	callq	strcpy
	movq	8(%r12), %r14
	movl	$3, %ebx
.LBB1_9:
	movl	$.L.str.4, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_37
# BB#10:
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_12
# BB#11:
	movq	input(%rip), %rax
	movl	$1, 3016(%rax)
	jmp	.LBB1_12
.LBB1_6:
	movl	$1, %ebx
	jmp	.LBB1_12
.LBB1_3:
	movl	$1, 3016(%rbp)
	movl	$2, %ebx
.LBB1_12:                               # %.thread.preheader
	xorl	%r14d, %r14d
	cmpl	%r15d, %ebx
	jl	.LBB1_14
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_22:                               # %.thread.backedge
                                        #   in Loop: Header=BB1_14 Depth=1
	movslq	%ebx, %rax
	movq	8(%r12,%rax,8), %rsi
	callq	strcpy
	addl	$2, %ebx
	cmpl	%r15d, %ebx
	jge	.LBB1_24
.LBB1_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %r13
	movq	(%r12,%r13,8), %rbp
	movl	$.L.str.4, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_37
# BB#15:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_14 Depth=1
	movq	input(%rip), %rax
	movl	$1, 3016(%rax)
	leal	1(%r13), %ebx
	movq	8(%r12,%r13,8), %rbp
.LBB1_17:                               #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.6, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_18
# BB#19:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.7, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_20
# BB#28:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.8, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_29
# BB#30:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.9, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_31
# BB#32:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$.L.str.11, %esi
	movl	$3, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_33
# BB#34:                                #   in Loop: Header=BB1_14 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.12, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
	cmpl	%r15d, %ebx
	jl	.LBB1_14
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_14 Depth=1
	movq	input(%rip), %rdi
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_14 Depth=1
	movq	input(%rip), %rdi
	movl	$1000, %eax             # imm = 0x3E8
	jmp	.LBB1_21
.LBB1_29:                               #   in Loop: Header=BB1_14 Depth=1
	movq	input(%rip), %rdi
	movl	$2000, %eax             # imm = 0x7D0
.LBB1_21:                               # %.thread.backedge
                                        #   in Loop: Header=BB1_14 Depth=1
	addq	%rax, %rdi
	jmp	.LBB1_22
.LBB1_31:                               #   in Loop: Header=BB1_14 Depth=1
	movslq	%ebx, %rax
	movq	8(%r12,%rax,8), %rdi
	movq	input(%rip), %rdx
	movl	$3008, %eax             # imm = 0xBC0
	addq	%rax, %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	sscanf
	addl	$2, %ebx
	cmpl	%r15d, %ebx
	jl	.LBB1_14
	jmp	.LBB1_24
.LBB1_33:                               #   in Loop: Header=BB1_14 Depth=1
	movq	input(%rip), %rax
	movl	$1, 3012(%rax)
	incl	%ebx
	cmpl	%r15d, %ebx
	jl	.LBB1_14
.LBB1_24:                               # %.thread._crit_edge
	movq	input(%rip), %rdi
	addq	$1000, %rdi             # imm = 0x3E8
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	callq	open64
	movl	%eax, p_out(%rip)
	cmpl	$-1, %eax
	jne	.LBB1_26
# BB#25:
	movq	input(%rip), %rcx
	addq	$1000, %rcx             # imm = 0x3E8
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.13, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_26:
	movq	stdout(%rip), %rdi
	movl	$.L.str.14, %esi
	movl	$.L.str.15, %edx
	movl	$.L.str.16, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rbx
	movq	input(%rip), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbp, %rdx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	$1000, %edx             # imm = 0x3E8
	addq	input(%rip), %rdx
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	$.L.str.21, %esi
	movl	$.L.str.22, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	input(%rip), %rdi
	addq	$2000, %rdi             # imm = 0x7D0
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open64
	movl	%eax, %ebp
	movl	%ebp, p_ref(%rip)
	movq	stdout(%rip), %r14
	movl	$2000, %ebx             # imm = 0x7D0
	addq	input(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbx, %rdx
	cmpl	$-1, %ebp
	je	.LBB1_27
# BB#35:
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	jmp	.LBB1_36
.LBB1_27:
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$71, %esi
	movl	$1, %edx
	callq	fwrite
.LBB1_36:
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$71, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$75, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB1_37:
	callq	JMDecHelpExit
.Lfunc_end1:
	.size	Configure, .Lfunc_end1-Configure
	.cfi_endproc

	.globl	init_conf
	.p2align	4, 0x90
	.type	init_conf,@function
init_conf:                              # @init_conf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 176
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$.L.str.32, %esi
	movq	%r15, %rdi
	callq	fopen64
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.33, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB2_2:
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	1000(%r14), %rdx
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	2000(%r14), %rdx
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	3012(%r14), %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	4(%rsp), %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	4(%rsp), %ecx
	cmpl	$1, %ecx
	je	.LBB2_5
# BB#3:
	testl	%ecx, %ecx
	jne	.LBB2_6
# BB#4:
	movl	$0, 3000(%r14)
	jmp	.LBB2_7
.LBB2_5:
	movl	$1, 3000(%r14)
	jmp	.LBB2_7
.LBB2_6:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.36, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$400, %esi              # imm = 0x190
	callq	error
.LBB2_7:
	leaq	3004(%r14), %rdx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	3008(%r14), %rdx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	3008(%r14), %ecx
	leal	-1(%rcx), %eax
	cmpl	$10, %eax
	jb	.LBB2_9
# BB#8:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.38, %edx
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	snprintf
	movl	$errortext, %edi
	movl	$1, %esi
	callq	error
.LBB2_9:
	movl	$1, 3012(%r14)
	leaq	3020(%r14), %r13
	movl	$0, 3020(%r14)
	movq	img(%rip), %rax
	movl	$0, 6068(%rax)
	leaq	3024(%r14), %r12
	movl	$2, 3024(%r14)
	movl	$2, 6060(%rax)
	leaq	3028(%r14), %r15
	movl	$2, 3028(%r14)
	movl	$2, 6064(%rax)
	leaq	8(%rsp), %rbp
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	16(%rsp), %rdx
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	3020(%r14), %eax
	movq	img(%rip), %rcx
	movl	%eax, 6068(%rcx)
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	3024(%r14), %eax
	movq	img(%rip), %rcx
	movl	%eax, 6060(%rcx)
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	3028(%r14), %eax
	movq	img(%rip), %rcx
	movl	%eax, 6064(%rcx)
	addq	$3016, %r14             # imm = 0xBC8
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fscanf
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	%rbx, %rdi
	callq	fclose
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	init_conf, .Lfunc_end2-init_conf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$3032, %esi             # imm = 0xBD8
	callq	calloc
	movq	%rax, input(%rip)
	testq	%rax, %rax
	jne	.LBB3_2
# BB#1:
	movl	$.L.str.28, %edi
	callq	no_mem_exit
.LBB3_2:
	movl	$1, %edi
	movl	$64, %esi
	callq	calloc
	movq	%rax, snr(%rip)
	testq	%rax, %rax
	jne	.LBB3_4
# BB#3:
	movl	$.L.str.29, %edi
	callq	no_mem_exit
.LBB3_4:
	movl	$1, %edi
	movl	$6112, %esi             # imm = 0x17E0
	callq	calloc
	movq	%rax, img(%rip)
	testq	%rax, %rax
	jne	.LBB3_6
# BB#5:
	movl	$.L.str.30, %edi
	callq	no_mem_exit
.LBB3_6:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	Configure
	xorl	%eax, %eax
	callq	init_old_slice
	movq	input(%rip), %rdi
	movl	3000(%rdi), %esi
	cmpl	$1, %esi
	je	.LBB3_9
# BB#7:
	testl	%esi, %esi
	jne	.LBB3_10
# BB#8:
	callq	OpenBitstreamFile
	jmp	.LBB3_11
.LBB3_9:
	callq	OpenRTPFile
	jmp	.LBB3_11
.LBB3_10:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	printf
.LBB3_11:
	movq	input(%rip), %r14
	movq	img(%rip), %rbp
	movl	$1, %edi
	movl	$152, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, 5592(%rbp)
	testq	%rbx, %rbx
	jne	.LBB3_13
# BB#12:
	movl	3000(%r14), %ecx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.71, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB3_13:                               # %malloc_slice.exit
	callq	create_contexts_MotionInfo
	movq	%rax, 48(%rbx)
	callq	create_contexts_TextureInfo
	movq	%rax, 56(%rbx)
	movl	$3, 24(%rbx)
	movl	$3, %edi
	callq	AllocPartition
	movq	%rax, 40(%rbx)
	movq	img(%rip), %rax
	movl	$-1, 5844(%rax)
	movq	$0, imgY_ref(%rip)
	movq	$0, imgUV_ref(%rip)
	movl	$0, 6088(%rax)
	movl	$0, 6092(%rax)
	movl	$2147483647, 6104(%rax) # imm = 0x7FFFFFFF
	movq	$0, dec_picture(%rip)
	movl	$0, dpb+48(%rip)
	movl	$0, g_nFrame(%rip)
	xorl	%eax, %eax
	callq	init_out_buffer
	movq	input(%rip), %rsi
	movl	3004(%rsi), %eax
	movq	img(%rip), %rdi
	movl	%eax, 5996(%rdi)
	movl	$0, 6000(%rdi)
	movl	$0, (%rdi)
	movl	$2, 44(%rdi)
	movq	$0, 5632(%rdi)
	movq	snr(%rip), %rdx
	movl	$0, (%rdx)
	movl	$0, Bframe_ctr(%rip)
	movq	$0, tot_time(%rip)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, ref_flag(%rip)
	movaps	%xmm0, ref_flag+16(%rip)
	movaps	%xmm0, ref_flag+32(%rip)
	movaps	%xmm0, ref_flag+48(%rip)
	movl	$1, ref_flag+64(%rip)
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader..preheader_crit_edge
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	img(%rip), %rdi
	movq	input(%rip), %rsi
	movq	snr(%rip), %rdx
.LBB3_15:                               # %.preheader..preheader_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	callq	decode_one_frame
	cmpl	$1, %eax
	jne	.LBB3_14
# BB#16:                                # %.preheader._crit_edge
	movq	input(%rip), %rdi
	movq	img(%rip), %rsi
	movq	snr(%rip), %rdx
	callq	report
	movq	img(%rip), %rsi
	callq	free_slice
	xorl	%eax, %eax
	callq	FmoFinit
	callq	free_global_buffers
	callq	flush_dpb
	xorl	%eax, %eax
	callq	CloseBitstreamFile
	movl	p_out(%rip), %edi
	callq	close
	movl	p_ref(%rip), %edi
	cmpl	$-1, %edi
	je	.LBB3_18
# BB#17:
	callq	close
.LBB3_18:
	movq	erc_errorVar(%rip), %rdi
	callq	ercClose
	xorl	%eax, %eax
	callq	CleanUpPPS
	callq	free_dpb
	xorl	%eax, %eax
	callq	uninit_out_buffer
	movq	Co_located(%rip), %rdi
	callq	free_colocated
	movq	input(%rip), %rdi
	callq	free
	movq	snr(%rip), %rdi
	callq	free
	movq	img(%rip), %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.globl	malloc_slice
	.p2align	4, 0x90
	.type	malloc_slice,@function
malloc_slice:                           # @malloc_slice
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$1, %edi
	movl	$152, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, 5592(%r15)
	testq	%rbx, %rbx
	jne	.LBB4_2
# BB#1:
	movl	3000(%r14), %ecx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.71, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB4_2:
	callq	create_contexts_MotionInfo
	movq	%rax, 48(%rbx)
	callq	create_contexts_TextureInfo
	movq	%rax, 56(%rbx)
	movl	$3, 24(%rbx)
	movl	$3, %edi
	callq	AllocPartition
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	malloc_slice, .Lfunc_end4-malloc_slice
	.cfi_endproc

	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	movl	$-1, 5844(%rdi)
	movq	$0, imgY_ref(%rip)
	movq	$0, imgUV_ref(%rip)
	movq	$0, 6088(%rdi)
	movl	$2147483647, 6104(%rdi) # imm = 0x7FFFFFFF
	retq
.Lfunc_end5:
	.size	init, .Lfunc_end5-init
	.cfi_endproc

	.globl	report
	.p2align	4, 0x90
	.type	report,@function
report:                                 # @report
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi45:
	.cfi_def_cfa_offset 368
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movaps	.Lreport.yuv_formats(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	input(%rip), %rax
	movq	stdout(%rip), %rcx
	cmpl	$0, 3016(%rax)
	je	.LBB6_1
# BB#2:
	movl	$.L.str.47, %edi
	movl	$76, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB6_3
.LBB6_1:
	movl	$.L.str.40, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movss	28(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.41, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.42, %esi
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	36(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.43, %esi
	movb	$1, %al
	callq	fprintf
.LBB6_3:
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movl	$.L.str.44, %esi
	movl	$.L.str.45, %edx
	movl	$.L.str.15, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	leaq	48(%rsp), %rbx
	movl	$255, %esi
	movl	$.L.str.34, %edx
	movl	$.L.str.22, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	snprintf
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	fopen64
	testq	%rax, %rax
	je	.LBB6_4
# BB#7:
	movq	%rax, %rdi
	callq	fclose
	leaq	48(%rsp), %rdi
	movl	$.L.str.48, %esi
	callq	fopen64
	movq	%rax, %rbp
	jmp	.LBB6_8
.LBB6_4:
	leaq	48(%rsp), %rdi
	movl	$.L.str.48, %esi
	callq	fopen64
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB6_5
# BB#6:
	movl	$.L.str.50, %edi
	movl	$117, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.51, %edi
	movl	$93, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.52, %edi
	movl	$118, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.53, %edi
	movl	$118, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.50, %edi
	movl	$117, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	jmp	.LBB6_8
.LBB6_5:
	xorl	%ebp, %ebp
	leaq	48(%rsp), %rcx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.49, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB6_8:
	movl	$.L.str.54, %esi
	movl	$.L.str.15, %edx
	movl	$.L.str.16, %ecx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	xorl	%edi, %edi
	callq	time
	movq	%rax, 24(%rsp)
	leaq	24(%rsp), %r13
	movq	%r13, %rdi
	callq	time
	movq	%r13, %rdi
	callq	localtime
	movq	%rax, %rbx
	leaq	48(%rsp), %r13
	movl	$255, %esi
	movl	$.L.str.55, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	fprintf
	movl	$255, %esi
	movl	$.L.str.57, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	strftime
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	fprintf
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	fprintf
	movl	(%r14), %edx
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	movl	48(%r14), %edx
	movl	52(%r14), %ecx
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	movslq	5916(%r14), %rax
	leaq	32(%rsp,%rax,4), %rdx
	movl	$.L.str.61, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	movq	active_pps(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_13
# BB#9:
	cmpl	$0, 12(%rax)
	je	.LBB6_10
# BB#11:
	movl	$.L.str.63, %edi
	jmp	.LBB6_12
.LBB6_10:
	movl	$.L.str.62, %edi
.LBB6_12:
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
.LBB6_13:
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.64, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movss	20(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.64, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.64, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movss	28(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.64, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.64, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movss	36(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.65, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	fprintf
	movq	%rbp, %rdi
	callq	fclose
	leaq	48(%rsp), %rbx
	movl	$255, %esi
	movl	$.L.str.34, %edx
	movl	$.L.str.66, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	snprintf
	movl	$.L.str.48, %esi
	movq	%rbx, %rdi
	callq	fopen64
	movq	%rax, %rbx
	movl	(%r14), %edx
	movl	28(%r14), %r8d
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	24(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	28(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm6
	movss	32(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm7
	movss	36(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movsd	%xmm3, 8(%rsp)
	movl	$0, 16(%rsp)
	movl	$0, (%rsp)
	movl	$.L.str.67, %esi
	movl	$0, %ecx
	movl	$0, %r9d
	xorpd	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movb	$8, %al
	movq	%rbx, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	report, .Lfunc_end6-report
	.cfi_endproc

	.globl	free_slice
	.p2align	4, 0x90
	.type	free_slice,@function
free_slice:                             # @free_slice
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	5592(%r14), %r15
	movq	40(%r15), %rbx
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	56(%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	56(%rbx), %rdi
	callq	free
	movq	112(%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	112(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	48(%r15), %rdi
	callq	delete_contexts_MotionInfo
	movq	56(%r15), %rdi
	callq	delete_contexts_TextureInfo
	movq	5592(%r14), %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	free_slice, .Lfunc_end7-free_slice
	.cfi_endproc

	.globl	init_frext
	.p2align	4, 0x90
	.type	init_frext,@function
init_frext:                             # @init_frext
	.cfi_startproc
# BB#0:
	movl	5876(%rdi), %edx
	leal	(%rdx,%rdx,2), %eax
	leal	-48(%rax,%rax), %eax
	movl	%eax, 5884(%rdi)
	movl	5880(%rdi), %r9d
	cmpl	%r9d, %edx
	movq	active_sps(%rip), %r8
	jg	.LBB8_2
# BB#1:
	cmpl	$0, 32(%r8)
	movl	%r9d, %ecx
	jne	.LBB8_3
.LBB8_2:                                # %._crit_edge
	movl	%edx, %ecx
.LBB8_3:
	xorl	%esi, %esi
	cmpl	$8, %ecx
	setg	%sil
	leal	8(,%rsi,8), %ecx
	movl	%ecx, 5872(%rdi)
	leal	-1(%rdx), %ecx
	movl	$1, %esi
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 5892(%rdi)
	movl	$1, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 5900(%rdi)
	movl	$16, 5952(%rdi)
	movl	$16, 5948(%rdi)
	movl	32(%r8), %edx
	testl	%edx, %edx
	je	.LBB8_5
# BB#4:
	leal	(%r9,%r9,2), %eax
	leal	-48(%rax,%rax), %eax
	movl	%eax, 5888(%rdi)
	leal	-1(%r9), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 5896(%rdi)
	movl	$1, %eax
	movl	%r9d, %ecx
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 5904(%rdi)
	movl	%edx, %ecx
	shll	%cl, %esi
	andl	$-2, %esi
	movl	%esi, 5924(%rdi)
	addl	%esi, %esi
	movl	%esi, 5928(%rdi)
	decl	%edx
	xorl	%eax, %eax
	cmpl	$1, %edx
	seta	%al
	leal	8(,%rax,8), %eax
	movl	%eax, 5964(%rdi)
	movl	%eax, 5956(%rdi)
	movl	32(%r8), %ecx
	orl	$1, %ecx
	xorl	%edx, %edx
	cmpl	$3, %ecx
	sete	%dl
	leal	8(,%rdx,8), %ecx
	jmp	.LBB8_6
.LBB8_5:
	movl	$0, 5888(%rdi)
	movl	$0, 5904(%rdi)
	movl	$0, 5924(%rdi)
	movl	$0, 5928(%rdi)
	movl	$0, 5964(%rdi)
	movl	$0, 5956(%rdi)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB8_6:
	movl	%eax, 5932(%rdi)
	movl	%ecx, 5936(%rdi)
	movl	%ecx, 5968(%rdi)
	movl	%ecx, 5960(%rdi)
	movl	$4, 5976(%rdi)
	movl	$4, 5972(%rdi)
	shrl	$2, %eax
	movl	%eax, 5988(%rdi)
	movl	%eax, 5980(%rdi)
	shrl	$2, %ecx
	movl	%ecx, 5992(%rdi)
	movl	%ecx, 5984(%rdi)
	retq
.Lfunc_end8:
	.size	init_frext, .Lfunc_end8-init_frext
	.cfi_endproc

	.globl	AllocPartition
	.p2align	4, 0x90
	.type	AllocPartition,@function
AllocPartition:                         # @AllocPartition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movslq	%ebp, %rdi
	movl	$56, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB9_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.68, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB9_2:                                # %.preheader
	testl	%ebp, %ebp
	jle	.LBB9_9
# BB#3:                                 # %.lr.ph.preheader
	movl	%ebp, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	testq	%rbp, %rbp
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.69, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB9_6:                                #   in Loop: Header=BB9_4 Depth=1
	movl	$8000000, %edi          # imm = 0x7A1200
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%rbp)
	testq	%rax, %rax
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.70, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB9_8:                                #   in Loop: Header=BB9_4 Depth=1
	addq	$56, %rbx
	decq	%r15
	jne	.LBB9_4
.LBB9_9:                                # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	AllocPartition, .Lfunc_end9-AllocPartition
	.cfi_endproc

	.globl	FreePartition
	.p2align	4, 0x90
	.type	FreePartition,@function
FreePartition:                          # @FreePartition
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	addq	$56, %rbx
	decq	%r15
	jne	.LBB10_2
.LBB10_3:                               # %._crit_edge
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end10:
	.size	FreePartition, .Lfunc_end10-FreePartition
	.cfi_endproc

	.globl	init_global_buffers
	.p2align	4, 0x90
	.type	init_global_buffers,@function
init_global_buffers:                    # @init_global_buffers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 48
.Lcfi78:
	.cfi_offset %rbx, -48
.Lcfi79:
	.cfi_offset %r12, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	cmpl	$0, global_init_done(%rip)
	je	.LBB11_2
# BB#1:
	callq	free_global_buffers
.LBB11_2:
	movq	img(%rip), %rax
	movl	48(%rax), %edx
	movl	52(%rax), %esi
	movl	$imgY_ref, %edi
	callq	get_mem2Dpel
	movl	%eax, %ebp
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB11_4
# BB#3:
	movq	img(%rip), %rax
	movl	56(%rax), %ecx
	movl	64(%rax), %edx
	movl	$imgUV_ref, %edi
	movl	$2, %esi
	callq	get_mem3Dpel
	movl	%eax, %ebx
	addl	%ebp, %ebx
	jmp	.LBB11_5
.LBB11_4:
	movq	$0, imgUV_ref(%rip)
	movl	%ebp, %ebx
.LBB11_5:
	movq	img(%rip), %rbp
	movl	5840(%rbp), %edi
	movl	$408, %esi              # imm = 0x198
	callq	calloc
	movq	%rax, 5600(%rbp)
	testq	%rax, %rax
	jne	.LBB11_7
# BB#6:
	movl	$.L.str.72, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbp
.LBB11_7:
	movl	5840(%rbp), %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 16(%rbp)
	testq	%rax, %rax
	jne	.LBB11_9
# BB#8:
	movl	$.L.str.73, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbp
.LBB11_9:
	movl	5840(%rbp), %esi
	incl	%esi
	movl	$PicPos, %edi
	movl	$2, %edx
	callq	get_mem2Dint
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	img(%rip), %rdi
	cmpl	$0, 5840(%rdi)
	js	.LBB11_12
# BB#10:                                # %.lr.ph35
	movq	PicPos(%rip), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_11:                              # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	5820(%rdi)
	movq	(%rsi,%rcx,8), %rbx
	movl	%edx, (%rbx)
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	5820(%rdi)
	movl	%eax, 4(%rbx)
	movslq	5840(%rdi), %rax
	cmpq	%rax, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB11_11
.LBB11_12:                              # %._crit_edge36
	movl	5820(%rdi), %edx
	movl	5828(%rdi), %esi
	leaq	5544(%rdi), %rdi
	shll	$2, %esi
	shll	$2, %edx
	callq	get_mem2D
	addl	%eax, %ebp
	movq	img(%rip), %rdi
	addq	$5768, %rdi             # imm = 0x1688
	movl	$2, %esi
	movl	$32, %edx
	movl	$3, %ecx
	callq	get_mem3Dint
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movq	img(%rip), %rdi
	addq	$5776, %rdi             # imm = 0x1690
	movl	$6, %esi
	movl	$32, %edx
	movl	$3, %ecx
	callq	get_mem3Dint
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	img(%rip), %rdi
	addq	$5784, %rdi             # imm = 0x1698
	movl	$6, %esi
	movl	$32, %edx
	movl	$32, %ecx
	movl	$3, %r8d
	callq	get_mem4Dint
	movl	%eax, %r12d
	addl	%ebp, %r12d
	movq	img(%rip), %rax
	movl	5840(%rax), %esi
	movl	5924(%rax), %ecx
	leaq	5560(%rax), %rdi
	addl	$4, %ecx
	movl	$4, %edx
	callq	get_mem3Dint
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movl	5820(%rax), %edx
	movl	5828(%rax), %esi
	leaq	5568(%rax), %rdi
	callq	get_mem2Dint
	movl	%eax, %r14d
	movq	img(%rip), %rbp
	movl	5900(%rbp), %ecx
	movl	5904(%rbp), %eax
	cmpl	%eax, %ecx
	jg	.LBB11_14
# BB#13:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 32(%rdx)
	jne	.LBB11_15
.LBB11_14:
	movl	%ecx, %eax
.LBB11_15:
	addl	%r15d, %r12d
	leal	2(%rax,%rax), %ebx
	movslq	%ebx, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 5552(%rbp)
	testq	%rax, %rax
	jne	.LBB11_17
# BB#16:
	movl	$.L.str.74, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbp
	movq	5552(%rbp), %rax
.LBB11_17:
	addl	%r14d, %r12d
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 5552(%rbp)
	cmpl	$2, %ebx
	jl	.LBB11_20
# BB#18:                                # %.lr.ph.preheader
	xorl	%edx, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB11_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	imull	%edi, %edi
	movl	%edi, (%rsi)
	movl	%edi, (%rax,%rdx,4)
	incq	%rdx
	addq	$-4, %rsi
	cmpq	%rcx, %rdx
	jl	.LBB11_19
.LBB11_20:                              # %._crit_edge
	movl	$1, global_init_done(%rip)
	movl	5840(%rbp), %eax
	movl	%eax, 5844(%rbp)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	init_global_buffers, .Lfunc_end11-init_global_buffers
	.cfi_endproc

	.globl	free_global_buffers
	.p2align	4, 0x90
	.type	free_global_buffers,@function
free_global_buffers:                    # @free_global_buffers
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 16
	movq	imgY_ref(%rip), %rdi
	callq	free_mem2Dpel
	movq	imgUV_ref(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	movl	$2, %esi
	callq	free_mem3Dpel
.LBB12_2:
	movq	img(%rip), %rax
	movq	5560(%rax), %rdi
	movl	5844(%rax), %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	5568(%rax), %rdi
	callq	free_mem2Dint
	movq	img(%rip), %rax
	movq	5600(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#3:
	callq	free
.LBB12_4:
	movq	PicPos(%rip), %rdi
	callq	free_mem2Dint
	movq	img(%rip), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	img(%rip), %rax
	movq	5544(%rax), %rdi
	callq	free_mem2D
	movq	img(%rip), %rax
	movq	5768(%rax), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	5776(%rax), %rdi
	movl	$6, %esi
	callq	free_mem3Dint
	movq	img(%rip), %rax
	movq	5784(%rax), %rdi
	movl	$6, %esi
	movl	$32, %edx
	callq	free_mem4Dint
	movq	img(%rip), %rdx
	movslq	5900(%rdx), %rax
	movslq	5904(%rdx), %rcx
	movq	5552(%rdx), %rdi
	cmpl	%ecx, %eax
	jle	.LBB12_6
# BB#5:
	leaq	4(,%rax,4), %rax
	jmp	.LBB12_7
.LBB12_6:
	leaq	4(,%rcx,4), %rax
.LBB12_7:
	subq	%rax, %rdi
	callq	free
	movl	$0, global_init_done(%rip)
	popq	%rax
	retq
.Lfunc_end12:
	.size	free_global_buffers, .Lfunc_end12-free_global_buffers
	.cfi_endproc

	.type	global_init_done,@object # @global_init_done
	.bss
	.globl	global_init_done
	.p2align	2
global_init_done:
	.long	0                       # 0x0
	.size	global_init_done, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n   ldecod [-h] {[defdec.cfg] | {[-p pocScale][-i bitstream.264]...[-o output.yuv] [-r reference.yuv] [-uv]}}\n\n## Parameters\n\n## Options\n   -h  :  prints function usage\n       :  parse <defdec.cfg> for decoder operation.\n   -i  :  Input file name. \n   -o  :  Output file name. If not specified default output is set as test_dec.yuv\n\n   -r  :  Reference file name. If not specified default output is set as test_rec.yuv\n\n   -p  :  Poc Scale. \n   -uv :  write chroma components for monochrome streams(4:2:0)\n\n## Supported video file formats\n   Input : .264 -> H.264 bitstream files. \n   Output: .yuv -> RAW file. Format depends on bitstream information. \n\n## Examples of usage:\n   ldecod\n   ldecod  -h\n   ldecod  default.cfg\n   ldecod  -i bitstream.264 -o output.yuv -r reference.yuv\n"
	.size	.L.str, 783

	.type	input,@object           # @input
	.comm	input,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"test.264"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"test_dec.yuv"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"test_rec.yuv"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-h"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"-s"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"-i"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"-o"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"-r"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"-p"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%d"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"-uv"
	.size	.L.str.11, 4

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Invalid syntax. Use ldecod -h for proper usage"
	.size	.L.str.12, 47

	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Error open file %s "
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"----------------------------- JM %s %s -----------------------------\n"
	.size	.L.str.14, 70

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"12.1"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"(FRExt)"
	.size	.L.str.16, 8

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" Decoder config file                    : %s \n"
	.size	.L.str.17, 47

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"--------------------------------------------------------------------------\n"
	.size	.L.str.18, 76

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" Input H.264 bitstream                  : %s \n"
	.size	.L.str.19, 47

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" Output decoded YUV                     : %s \n"
	.size	.L.str.20, 47

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" Output status file                     : %s \n"
	.size	.L.str.21, 47

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"log.dec"
	.size	.L.str.22, 8

	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	" Input reference file                   : %s does not exist \n"
	.size	.L.str.23, 62

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"                                          SNR values are not available\n"
	.size	.L.str.24, 72

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" Input reference file                   : %s \n"
	.size	.L.str.25, 47

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"POC must = frame# or field# for SNRs to be correct\n"
	.size	.L.str.26, 52

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"  Frame       POC   Pic#   QP   SnrY    SnrU    SnrV   Y:U:V  Time(ms)\n"
	.size	.L.str.27, 72

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"main: input"
	.size	.L.str.28, 12

	.type	snr,@object             # @snr
	.comm	snr,8,8
	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"main: snr"
	.size	.L.str.29, 10

	.type	img,@object             # @img
	.comm	img,8,8
	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"main: img"
	.size	.L.str.30, 10

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Unsupported file format %d, exit\n"
	.size	.L.str.31, 34

	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"r"
	.size	.L.str.32, 2

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Error: Control file %s not found\n"
	.size	.L.str.33, 34

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%s"
	.size	.L.str.34, 3

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%*[^\n]"
	.size	.L.str.35, 7

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"NAL mode %i is not supported"
	.size	.L.str.36, 29

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%d,"
	.size	.L.str.37, 4

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Poc Scale is %d. It has to be within range 1 to 10"
	.size	.L.str.38, 51

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%ld,"
	.size	.L.str.39, 5

	.type	.Lreport.yuv_formats,@object # @report.yuv_formats
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.Lreport.yuv_formats:
	.asciz	"400"
	.asciz	"420"
	.asciz	"422"
	.asciz	"444"
	.size	.Lreport.yuv_formats, 16

	.type	.L.str.40,@object       # @.str.40
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.40:
	.asciz	"-------------------- Average SNR all frames ------------------------------\n"
	.size	.L.str.40, 76

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	" SNR Y(dB)           : %5.2f\n"
	.size	.L.str.41, 30

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	" SNR U(dB)           : %5.2f\n"
	.size	.L.str.42, 30

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	" SNR V(dB)           : %5.2f\n"
	.size	.L.str.43, 30

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" Exit JM %s decoder, ver %s "
	.size	.L.str.44, 29

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"12 (FRExt)"
	.size	.L.str.45, 11

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"\n----------------------- Decoding Completed -------------------------------\n"
	.size	.L.str.47, 77

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"a"
	.size	.L.str.48, 2

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Error open file %s for appending"
	.size	.L.str.49, 33

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	" -------------------------------------------------------------------------------------------------------------------\n"
	.size	.L.str.50, 118

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"|  Decoder statistics. This file is made first time, later runs are appended               |\n"
	.size	.L.str.51, 94

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	" ------------------------------------------------------------------------------------------------------------------- \n"
	.size	.L.str.52, 119

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"|   ver  | Date  | Time  |    Sequence        |#Img| Format  | YUV |Coding|SNRY 1|SNRU 1|SNRV 1|SNRY N|SNRU N|SNRV N|\n"
	.size	.L.str.53, 119

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"|%s/%-4s"
	.size	.L.str.54, 9

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"%d-%b-%Y"
	.size	.L.str.55, 9

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"| %1.5s |"
	.size	.L.str.56, 10

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%H:%M:%S"
	.size	.L.str.57, 9

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%20.20s|"
	.size	.L.str.58, 9

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%3d |"
	.size	.L.str.59, 6

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%4dx%-4d|"
	.size	.L.str.60, 10

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	" %s |"
	.size	.L.str.61, 6

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	" CAVLC|"
	.size	.L.str.62, 8

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	" CABAC|"
	.size	.L.str.63, 8

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%6.3f|"
	.size	.L.str.64, 7

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%6.3f|\n"
	.size	.L.str.65, 8

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"dataDec.txt"
	.size	.L.str.66, 12

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"%3d %2d %2d %2.2f %2.2f %2.2f %5d %2.2f %2.2f %2.2f %5d %2.2f %2.2f %2.2f %5d\n"
	.size	.L.str.67, 79

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"AllocPartition: Memory allocation for Data Partition failed"
	.size	.L.str.68, 60

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"AllocPartition: Memory allocation for Bitstream failed"
	.size	.L.str.69, 55

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"AllocPartition: Memory allocation for streamBuffer failed"
	.size	.L.str.70, 58

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Memory allocation for Slice datastruct in NAL-mode %d failed"
	.size	.L.str.71, 61

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"init_global_buffers: img->mb_data"
	.size	.L.str.72, 34

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"init_global_buffers: img->intra_block"
	.size	.L.str.73, 38

	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"init_img: img->quad"
	.size	.L.str.74, 20

	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
