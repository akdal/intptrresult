	.text
	.file	"libclamav_unarj.bc"
	.globl	cli_unarj_open
	.p2align	4, 0x90
	.type	cli_unarj_open,@function
cli_unarj_open:                         # @cli_unarj_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	8(%rsp), %rsi
	movl	$2, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_3
# BB#1:
	movzwl	8(%rsp), %eax
	cmpl	$60000, %eax            # imm = 0xEA60
	je	.LBB0_6
# BB#2:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_3:
	movl	$.L.str.1, %edi
.LBB0_4:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %eax
.LBB0_5:
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:
	leaq	4(%rsp), %rsi
	movl	$2, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_39
# BB#7:
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebp, %edi
	callq	lseek
	movzwl	4(%rsp), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB0_39
# BB#8:
	cmpl	$2601, %esi             # imm = 0xA29
	jb	.LBB0_10
# BB#9:                                 # %.critedge
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.2, %edi
	jmp	.LBB0_4
.LBB0_10:
	leaq	8(%rsp), %rsi
	movl	$30, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$30, %eax
	jne	.LBB0_39
# BB#11:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	8(%rsp), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	9(%rsp), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	10(%rsp), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	11(%rsp), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	12(%rsp), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	13(%rsp), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	14(%rsp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	8(%rsp), %esi
	cmpq	$29, %rsi
	ja	.LBB0_13
# BB#12:                                # %.critedge4
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.2, %edi
	jmp	.LBB0_4
.LBB0_13:
	cmpb	$30, %sil
	je	.LBB0_15
# BB#14:
	addq	$-30, %rsi
	movl	$1, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_39
.LBB0_15:
	movzwl	4(%rsp), %edi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_39
# BB#16:                                # %.preheader31.i
	movw	$0, 6(%rsp)
	cmpw	$0, 4(%rsp)
	je	.LBB0_29
# BB#17:                                # %.lr.ph35.i.preheader
	xorl	%r15d, %r15d
.LBB0_18:                               # %.lr.ph35.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%r15w, %ebx
	addq	%r14, %rbx
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB0_29
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_18 Depth=1
	incl	%r15d
	movw	%r15w, 6(%rsp)
	movzwl	4(%rsp), %eax
	cmpw	%ax, %r15w
	jb	.LBB0_18
	jmp	.LBB0_22
.LBB0_21:                               # %.._crit_edge36.loopexit_crit_edge.i
	movzwl	4(%rsp), %eax
.LBB0_22:                               # %._crit_edge36.i
	cmpw	%ax, %r15w
	je	.LBB0_29
# BB#23:
	movzwl	%ax, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_29
# BB#24:                                # %.preheader30.i
	movw	$0, 6(%rsp)
	cmpw	$0, 4(%rsp)
	je	.LBB0_33
# BB#25:                                # %.lr.ph.i.preheader
	xorl	%r12d, %r12d
.LBB0_26:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%r12w, %ebx
	addq	%r15, %rbx
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB0_33
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB0_31
# BB#28:                                #   in Loop: Header=BB0_26 Depth=1
	incl	%r12d
	movw	%r12w, 6(%rsp)
	movzwl	4(%rsp), %eax
	cmpw	%ax, %r12w
	jb	.LBB0_26
	jmp	.LBB0_32
.LBB0_29:                               # %.critedge5
	movq	%r14, %rdi
.LBB0_30:
	callq	free
	movl	$.L.str.2, %edi
	jmp	.LBB0_4
.LBB0_31:                               # %.._crit_edge.loopexit_crit_edge.i
	movzwl	4(%rsp), %eax
.LBB0_32:                               # %._crit_edge.i
	cmpw	%ax, %r12w
	jne	.LBB0_34
.LBB0_33:                               # %.critedge7
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	jmp	.LBB0_30
.LBB0_34:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	leaq	44(%rsp), %rsi
	movl	$4, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_39
# BB#35:                                # %.preheader.i.preheader
	leaq	6(%rsp), %r14
.LBB0_36:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, %edx
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_39
# BB#37:                                #   in Loop: Header=BB0_36 Depth=1
	movzwl	6(%rsp), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	6(%rsp), %esi
	testq	%rsi, %rsi
	je	.LBB0_40
# BB#38:                                #   in Loop: Header=BB0_36 Depth=1
	addq	$4, %rsi
	movl	$1, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	jne	.LBB0_36
.LBB0_39:
	movl	$.L.str.2, %edi
	jmp	.LBB0_4
.LBB0_40:                               # %.critedge16
	xorl	%eax, %eax
	jmp	.LBB0_5
.Lfunc_end0:
	.size	cli_unarj_open, .Lfunc_end0-cli_unarj_open
	.cfi_endproc

	.globl	cli_unarj_prepare_file
	.p2align	4, 0x90
	.type	cli_unarj_prepare_file,@function
cli_unarj_prepare_file:                 # @cli_unarj_prepare_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 96
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movl	%edi, %r13d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%r13d, %r13d
	movl	$-111, %ebx
	js	.LBB1_9
# BB#1:
	testq	%rbp, %rbp
	je	.LBB1_9
# BB#2:
	testq	%r12, %r12
	je	.LBB1_9
# BB#3:
	leaq	8(%rsp), %rsi
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB1_6
# BB#4:
	movzwl	8(%rsp), %eax
	cmpl	$60000, %eax            # imm = 0xEA60
	je	.LBB1_10
# BB#5:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_6:
	movl	$.L.str.1, %edi
.LBB1_7:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_8:
	movl	$-124, %ebx
.LBB1_9:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_10:
	leaq	4(%rsp), %rsi
	movl	$2, %ebx
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB1_8
# BB#11:
	movzwl	4(%rsp), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	4(%rsp), %esi
	testl	%esi, %esi
	je	.LBB1_9
# BB#12:
	cmpl	$2601, %esi             # imm = 0xA29
	jb	.LBB1_15
# BB#13:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_8
.LBB1_15:
	leaq	8(%rsp), %rsi
	movl	$30, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$30, %eax
	jne	.LBB1_8
# BB#16:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	8(%rsp), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	9(%rsp), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	10(%rsp), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	11(%rsp), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	12(%rsp), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	13(%rsp), %esi
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	14(%rsp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	15(%rsp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%rsp), %esi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	24(%rsp), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	8(%rsp), %esi
	cmpq	$29, %rsi
	ja	.LBB1_19
# BB#17:
	movl	$.L.str.20, %edi
	jmp	.LBB1_7
.LBB1_19:
	cmpb	$30, %sil
	je	.LBB1_21
# BB#20:
	addq	$-30, %rsi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_8
.LBB1_21:
	movzwl	4(%rsp), %edi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_27
# BB#22:                                # %.preheader36.i
	movw	$0, 6(%rsp)
	cmpw	$0, 4(%rsp)
	je	.LBB1_37
# BB#23:                                # %.lr.ph40.i.preheader
	xorl	%ebp, %ebp
.LBB1_24:                               # %.lr.ph40.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%bp, %ebx
	addq	%r14, %rbx
	movl	$1, %edx
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB1_37
# BB#25:                                #   in Loop: Header=BB1_24 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_29
# BB#26:                                #   in Loop: Header=BB1_24 Depth=1
	incl	%ebp
	movw	%bp, 6(%rsp)
	movzwl	4(%rsp), %eax
	cmpw	%ax, %bp
	jb	.LBB1_24
	jmp	.LBB1_30
.LBB1_27:
	movl	$-114, %ebx
	jmp	.LBB1_9
.LBB1_29:                               # %.._crit_edge41.loopexit_crit_edge.i
	movzwl	4(%rsp), %eax
.LBB1_30:                               # %._crit_edge41.i
	cmpw	%ax, %bp
	je	.LBB1_37
# BB#31:
	movzwl	%ax, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_37
# BB#32:                                # %.preheader35.i
	movw	$0, 6(%rsp)
	cmpw	$0, 4(%rsp)
	je	.LBB1_41
# BB#33:                                # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
.LBB1_34:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%bp, %ebx
	addq	%r15, %rbx
	movl	$1, %edx
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB1_41
# BB#35:                                #   in Loop: Header=BB1_34 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_39
# BB#36:                                #   in Loop: Header=BB1_34 Depth=1
	incl	%ebp
	movw	%bp, 6(%rsp)
	movzwl	4(%rsp), %eax
	cmpw	%ax, %bp
	jb	.LBB1_34
	jmp	.LBB1_40
.LBB1_37:
	movq	%r14, %rdi
.LBB1_38:                               # %arj_read_file_header.exit
	callq	free
	jmp	.LBB1_8
.LBB1_39:                               # %.._crit_edge.loopexit_crit_edge.i
	movzwl	4(%rsp), %eax
.LBB1_40:                               # %._crit_edge.i
	cmpw	%ax, %bp
	jne	.LBB1_42
.LBB1_41:
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	jmp	.LBB1_38
.LBB1_42:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	cli_strdup
	movq	%rax, 16(%r12)
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movl	$4, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_8
# BB#43:                                # %.preheader.i.preheader
	leaq	6(%rsp), %rbx
.LBB1_44:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, %edx
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB1_8
# BB#45:                                #   in Loop: Header=BB1_44 Depth=1
	movzwl	6(%rsp), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	6(%rsp), %esi
	testq	%rsi, %rsi
	je	.LBB1_50
# BB#46:                                #   in Loop: Header=BB1_44 Depth=1
	addq	$4, %rsi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	cmpq	$-1, %rax
	jne	.LBB1_44
	jmp	.LBB1_8
.LBB1_50:
	movl	20(%rsp), %eax
	movl	%eax, (%r12)
	movl	24(%rsp), %eax
	movl	%eax, 4(%r12)
	movb	13(%rsp), %al
	movb	%al, 8(%r12)
	movzbl	12(%rsp), %eax
	andl	$1, %eax
	movl	%eax, 24(%r12)
	movl	$-1, 28(%r12)
	xorl	%eax, %eax
	cmpq	$0, 16(%r12)
	movl	$-114, %ebx
	cmovnel	%eax, %ebx
	jmp	.LBB1_9
.Lfunc_end1:
	.size	cli_unarj_prepare_file, .Lfunc_end1-cli_unarj_prepare_file
	.cfi_endproc

	.globl	cli_unarj_extract_file
	.p2align	4, 0x90
	.type	cli_unarj_extract_file,@function
cli_unarj_extract_file:                 # @cli_unarj_extract_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$14408, %rsp            # imm = 0x3848
.Lcfi30:
	.cfi_def_cfa_offset 14464
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movl	%edi, %r14d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%r14d, %r14d
	movl	$-111, %eax
	js	.LBB2_5
# BB#1:
	testq	%rbp, %rbp
	je	.LBB2_5
# BB#2:
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#3:
	cmpl	$0, 24(%rbx)
	je	.LBB2_6
# BB#4:
	xorl	%r15d, %r15d
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	movl	(%rbx), %ebp
	addq	%rax, %rbp
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	xorl	%edx, %edx
	movl	%r14d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	movl	$-127, %eax
	cmovel	%r15d, %eax
.LBB2_5:
	addq	$14408, %rsp            # imm = 0x3848
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_6:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leaq	13376(%rsp), %r15
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$578, %esi              # imm = 0x242
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	8(%rsp), %r15           # 8-byte Reload
	callq	open
	movl	%eax, %r12d
	movl	%r12d, 28(%r15)
	testl	%r12d, %r12d
	js	.LBB2_18
# BB#7:
	movb	8(%r15), %cl
	movl	%ecx, %eax
	decb	%al
	cmpb	$3, %al
	jae	.LBB2_19
# BB#8:
	movl	$26624, %edi            # imm = 0x6800
	callq	cli_malloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB2_241
# BB#9:
	movl	%r14d, 16(%rsp)
	movl	(%r15), %edx
	movl	%edx, 44(%rsp)
	movw	$0, 32(%rsp)
	leaq	36(%rsp), %r12
	movb	$0, 36(%rsp)
	movl	$0, 40(%rsp)
	movw	$0, 34(%rsp)
	movl	$16, %ebx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               # %.._crit_edge31.i.i.i_crit_edge.i
                                        #   in Loop: Header=BB2_11 Depth=1
	movl	44(%rsp), %edx
.LBB2_11:                               # %._crit_edge31.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %esi
	orl	%esi, %eax
	movw	%ax, 34(%rsp)
	testl	%edx, %edx
	je	.LBB2_14
# BB#12:                                #   in Loop: Header=BB2_11 Depth=1
	decl	%edx
	movl	%edx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_241
# BB#13:                                # %._crit_edge32.i.i.i.i
                                        #   in Loop: Header=BB2_11 Depth=1
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_11 Depth=1
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_15:                               #   in Loop: Header=BB2_11 Depth=1
	movl	$8, 40(%rsp)
	movzbl	%cl, %esi
	movl	$8, %ecx
	cmpl	$9, %ebx
	jge	.LBB2_10
# BB#16:                                # %decode_start.exit.thread.i
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	orl	%esi, %eax
	movw	%ax, 34(%rsp)
	cmpl	$0, 4(%r15)
	je	.LBB2_240
# BB#17:                                # %.lr.ph86.i
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB2_41
.LBB2_18:
	movl	$-115, %eax
	jmp	.LBB2_5
.LBB2_19:
	cmpb	$4, %cl
	je	.LBB2_151
# BB#20:
	movl	$-124, %eax
	testb	%cl, %cl
	jne	.LBB2_5
# BB#21:
	movl	(%r15), %ebx
	xorl	%ebp, %ebp
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%ebx, %ebx
	movl	$0, %eax
	je	.LBB2_232
# BB#22:                                # %.lr.ph.i.preheader
	leaq	16(%rsp), %r15
	movl	%ebx, %r13d
.LBB2_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$8192, %r13d            # imm = 0x2000
	movl	$8192, %ebp             # imm = 0x2000
	cmovbl	%r13d, %ebp
	movl	%r14d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	cli_readn
	cmpl	%ebp, %eax
	jne	.LBB2_229
# BB#24:                                #   in Loop: Header=BB2_23 Depth=1
	movl	%r12d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	jne	.LBB2_230
# BB#25:                                #   in Loop: Header=BB2_23 Depth=1
	subl	%ebp, %r13d
	jne	.LBB2_23
	jmp	.LBB2_231
.LBB2_26:                               #   in Loop: Header=BB2_41 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB2_28
.LBB2_27:                               #   in Loop: Header=BB2_41 Depth=1
	xorl	%ebx, %ebx
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_28:                               # %decode_p.exit.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movq	%rbp, %r12
.LBB2_29:                               # %decode_p.exit.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movzwl	%bx, %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	addl	$65535, %ecx            # imm = 0xFFFF
	movl	%ecx, %eax
	shll	$16, %eax
	movswl	%cx, %ebp
	addl	$26624, %ebp            # imm = 0x6800
	testl	%eax, %eax
	cmovnsl	%ecx, %ebp
	movzwl	%bp, %eax
	cmpl	$26624, %eax            # imm = 0x6800
	jae	.LBB2_233
# BB#30:                                #   in Loop: Header=BB2_41 Depth=1
	leal	-253(%r14), %eax
	movswl	%ax, %ebx
	addl	4(%rsp), %ebx           # 4-byte Folded Reload
	addl	$-254, %r14d
	cmpl	$26366, %r13d           # imm = 0x66FE
	ja	.LBB2_37
# BB#31:                                #   in Loop: Header=BB2_41 Depth=1
	movzwl	%bp, %eax
	cmpl	%eax, %r13d
	jbe	.LBB2_37
# BB#32:                                # %.preheader.i
                                        #   in Loop: Header=BB2_41 Depth=1
	testw	%r14w, %r14w
	js	.LBB2_150
# BB#33:                                # %.lr.ph78.preheader.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movswq	%bp, %rax
	movl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_34:                               # %.lr.ph78.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$26623, %r13            # imm = 0x67FF
	ja	.LBB2_150
# BB#35:                                # %.lr.ph78.i
                                        #   in Loop: Header=BB2_34 Depth=2
	cmpq	$26623, %rax            # imm = 0x67FF
	jg	.LBB2_150
# BB#36:                                #   in Loop: Header=BB2_34 Depth=2
	movq	24(%rsp), %rcx
	movzbl	(%rcx,%rax), %edx
	movb	%dl, (%rcx,%r13)
	incq	%r13
	incq	%rax
	decw	%r14w
	jns	.LBB2_34
	jmp	.LBB2_150
.LBB2_37:                               # %.preheader40.i
                                        #   in Loop: Header=BB2_41 Depth=1
	testw	%r14w, %r14w
	js	.LBB2_150
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph.i32
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax
	movswq	%bp, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	%r13d, %edx
	movb	%cl, (%rax,%rdx)
	incl	%r13d
	cmpl	$26624, %r13d           # imm = 0x6800
	jb	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_38 Depth=2
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	$26624, %edx            # imm = 0x6800
	callq	cli_writen
	xorl	%r13d, %r13d
.LBB2_40:                               #   in Loop: Header=BB2_38 Depth=2
	incl	%ebp
	movswl	%bp, %ebp
	cmpl	$26623, %ebp            # imm = 0x67FF
	movl	$0, %eax
	cmovgw	%ax, %bp
	decw	%r14w
	jns	.LBB2_38
	jmp	.LBB2_150
	.p2align	4, 0x90
.LBB2_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_43 Depth 2
                                        #     Child Loop BB2_51 Depth 2
                                        #     Child Loop BB2_59 Depth 2
                                        #       Child Loop BB2_61 Depth 3
                                        #       Child Loop BB2_64 Depth 3
                                        #       Child Loop BB2_87 Depth 3
                                        #       Child Loop BB2_75 Depth 3
                                        #       Child Loop BB2_95 Depth 3
                                        #     Child Loop BB2_105 Depth 2
                                        #     Child Loop BB2_112 Depth 2
                                        #     Child Loop BB2_116 Depth 2
                                        #     Child Loop BB2_119 Depth 2
                                        #     Child Loop BB2_128 Depth 2
                                        #     Child Loop BB2_131 Depth 2
                                        #     Child Loop BB2_139 Depth 2
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_34 Depth 2
	movw	32(%rsp), %ax
	testw	%ax, %ax
	jne	.LBB2_114
# BB#42:                                #   in Loop: Header=BB2_41 Depth=1
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movzwl	34(%rsp), %ebp
	movw	$0, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	movl	$16, %ebx
	xorl	%eax, %eax
	cmpl	$15, %ecx
	jg	.LBB2_48
	.p2align	4, 0x90
.LBB2_43:                               # %._crit_edge31.i.i.i4.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_46
# BB#44:                                #   in Loop: Header=BB2_43 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_49
# BB#45:                                # %._crit_edge32.i.i.i7.i
                                        #   in Loop: Header=BB2_43 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_47
	.p2align	4, 0x90
.LBB2_46:                               #   in Loop: Header=BB2_43 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_47:                               #   in Loop: Header=BB2_43 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_43
.LBB2_48:                               # %._crit_edge.i.i.i9.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_49:                               # %arj_getbits.exit.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movw	%bp, 32(%rsp)
	movl	$3, %esi
	leaq	16(%rsp), %rdi
	callq	read_pt_len
	movzwl	34(%rsp), %eax
	movl	%eax, %r14d
	shrl	$7, %r14d
	shll	$9, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movl	$9, %ebx
	movzbl	36(%rsp), %edx
	cmpl	$8, %ecx
	jg	.LBB2_56
# BB#50:                                # %._crit_edge31.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$9, %ebx
	.p2align	4, 0x90
.LBB2_51:                               # %._crit_edge31.i.i.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_54
# BB#52:                                #   in Loop: Header=BB2_51 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_57
# BB#53:                                # %._crit_edge32.i.i.i.i.i
                                        #   in Loop: Header=BB2_51 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_51 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_55:                               #   in Loop: Header=BB2_51 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_51
.LBB2_56:                               # %._crit_edge.i.i.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_57:                               # %arj_getbits.exit.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	testw	%r14w, %r14w
	je	.LBB2_103
# BB#58:                                # %.lr.ph67.i.i.i.preheader
                                        #   in Loop: Header=BB2_41 Depth=1
	xorl	%r15d, %r15d
.LBB2_59:                               # %.lr.ph67.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_61 Depth 3
                                        #       Child Loop BB2_64 Depth 3
                                        #       Child Loop BB2_87 Depth 3
                                        #       Child Loop BB2_75 Depth 3
                                        #       Child Loop BB2_95 Depth 3
	movzwl	34(%rsp), %eax
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$510, %ecx              # imm = 0x1FE
	movswl	12850(%rsp,%rcx), %ebp
	cmpl	$19, %ebp
	jl	.LBB2_63
# BB#60:                                # %.preheader40.i.i.i.preheader
                                        #   in Loop: Header=BB2_59 Depth=2
	movl	$128, %ecx
	.p2align	4, 0x90
.LBB2_61:                               # %.preheader40.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        #     Parent Loop BB2_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movswl	%bp, %edx
	cmpl	$1019, %edx             # imm = 0x3FB
	jge	.LBB2_101
# BB#62:                                #   in Loop: Header=BB2_61 Depth=3
	testl	%eax, %ecx
	movswq	%bp, %rdx
	leaq	2090(%rsp,%rdx,2), %rsi
	leaq	52(%rsp,%rdx,2), %rdx
	cmovneq	%rsi, %rdx
	movswl	(%rdx), %ebp
	shrl	%ecx
	cmpl	$18, %ebp
	jg	.LBB2_61
.LBB2_63:                               # %.loopexit41.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	movswq	%bp, %rcx
	movzbl	12830(%rsp,%rcx), %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebx, %ecx
	jge	.LBB2_69
	.p2align	4, 0x90
.LBB2_64:                               # %._crit_edge31.i.i28.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        #     Parent Loop BB2_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_67
# BB#65:                                #   in Loop: Header=BB2_64 Depth=3
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_70
# BB#66:                                # %._crit_edge32.i.i31.i.i
                                        #   in Loop: Header=BB2_64 Depth=3
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_68
	.p2align	4, 0x90
.LBB2_67:                               #   in Loop: Header=BB2_64 Depth=3
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_68:                               #   in Loop: Header=BB2_64 Depth=3
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_64
.LBB2_69:                               # %._crit_edge.i.i36.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_70:                               # %fill_buf.exit.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	movswl	%bp, %eax
	cmpl	$2, %eax
	jg	.LBB2_82
# BB#71:                                #   in Loop: Header=BB2_59 Depth=2
	testw	%bp, %bp
	je	.LBB2_84
# BB#72:                                #   in Loop: Header=BB2_59 Depth=2
	movzwl	%bp, %eax
	cmpl	$1, %eax
	jne	.LBB2_85
# BB#73:                                #   in Loop: Header=BB2_59 Depth=2
	movzwl	34(%rsp), %eax
	movl	%eax, %ebp
	shrl	$12, %ebp
	shll	$4, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movl	$4, %ebx
	movzbl	36(%rsp), %edx
	cmpl	$3, %ecx
	jg	.LBB2_80
# BB#74:                                # %._crit_edge31.i.i15.i.i.i.preheader
                                        #   in Loop: Header=BB2_59 Depth=2
	movl	$4, %ebx
	.p2align	4, 0x90
.LBB2_75:                               # %._crit_edge31.i.i15.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        #     Parent Loop BB2_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_78
# BB#76:                                #   in Loop: Header=BB2_75 Depth=3
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_81
# BB#77:                                # %._crit_edge32.i.i18.i.i.i
                                        #   in Loop: Header=BB2_75 Depth=3
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_79
.LBB2_78:                               #   in Loop: Header=BB2_75 Depth=3
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_79:                               #   in Loop: Header=BB2_75 Depth=3
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_75
.LBB2_80:                               # %._crit_edge.i.i23.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_81:                               # %arj_getbits.exit24.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	movswl	%bp, %ebp
	addl	$3, %ebp
	jmp	.LBB2_94
.LBB2_82:                               #   in Loop: Header=BB2_59 Depth=2
	movswl	%r15w, %eax
	cmpl	$510, %eax              # imm = 0x1FE
	jge	.LBB2_101
# BB#83:                                #   in Loop: Header=BB2_59 Depth=2
	addb	$-2, %bpl
	movswq	%r15w, %rax
	leal	1(%r15), %ecx
	movb	%bpl, 4128(%rsp,%rax)
	movw	%cx, %r15w
	cmpw	%r14w, %r15w
	jl	.LBB2_59
	jmp	.LBB2_98
.LBB2_84:                               #   in Loop: Header=BB2_59 Depth=2
	movw	$1, %bp
	jmp	.LBB2_94
.LBB2_85:                               #   in Loop: Header=BB2_59 Depth=2
	movzwl	34(%rsp), %eax
	movl	%eax, %ebp
	shrl	$7, %ebp
	shll	$9, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movl	$9, %ebx
	movzbl	36(%rsp), %edx
	cmpl	$8, %ecx
	jg	.LBB2_92
# BB#86:                                # %._crit_edge31.i.i27.i.i.i.preheader
                                        #   in Loop: Header=BB2_59 Depth=2
	movl	$9, %ebx
	.p2align	4, 0x90
.LBB2_87:                               # %._crit_edge31.i.i27.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        #     Parent Loop BB2_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_90
# BB#88:                                #   in Loop: Header=BB2_87 Depth=3
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_93
# BB#89:                                # %._crit_edge32.i.i30.i.i.i
                                        #   in Loop: Header=BB2_87 Depth=3
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_91
.LBB2_90:                               #   in Loop: Header=BB2_87 Depth=3
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_91:                               #   in Loop: Header=BB2_87 Depth=3
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_87
.LBB2_92:                               # %._crit_edge.i.i35.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_93:                               # %arj_getbits.exit36.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	addl	$20, %ebp
.LBB2_94:                               # %.lr.ph65.preheader.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	decl	%ebp
	movswq	%r15w, %r15
	.p2align	4, 0x90
.LBB2_95:                               # %.lr.ph65.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        #     Parent Loop BB2_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	$510, %r15              # imm = 0x1FE
	jge	.LBB2_101
# BB#96:                                #   in Loop: Header=BB2_95 Depth=3
	movb	$0, 4128(%rsp,%r15)
	incq	%r15
	decw	%bp
	jns	.LBB2_95
# BB#97:                                # %.backedge.i.i.i
                                        #   in Loop: Header=BB2_59 Depth=2
	cmpw	%r14w, %r15w
	jl	.LBB2_59
.LBB2_98:                               # %.preheader37.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movswl	%r15w, %eax
	cmpl	$509, %eax              # imm = 0x1FD
	jg	.LBB2_100
# BB#99:                                # %.lr.ph.preheader.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movswq	%r15w, %rax
	leaq	4128(%rsp,%rax), %rdi
	movl	$509, %eax              # imm = 0x1FD
	subl	%r15d, %eax
	movzwl	%ax, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_100:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$510, %esi              # imm = 0x1FE
	movl	$12, %ecx
	movl	$4096, %r9d             # imm = 0x1000
	leaq	16(%rsp), %rdi
	leaq	4128(%rsp), %rdx
	leaq	4638(%rsp), %r8
	callq	make_table
	jmp	.LBB2_102
.LBB2_101:                              #   in Loop: Header=BB2_41 Depth=1
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_102:                              # %read_c_len.exit.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB2_113
.LBB2_103:                              #   in Loop: Header=BB2_41 Depth=1
	movzwl	34(%rsp), %eax
	movl	%eax, %ebp
	shrl	$7, %ebp
	shll	$9, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movl	$9, %ebx
	movzbl	36(%rsp), %edx
	cmpl	$8, %ecx
	jg	.LBB2_110
# BB#104:                               # %._crit_edge31.i.i3.i.i.i.preheader
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$9, %ebx
	.p2align	4, 0x90
.LBB2_105:                              # %._crit_edge31.i.i3.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_108
# BB#106:                               #   in Loop: Header=BB2_105 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_111
# BB#107:                               # %._crit_edge32.i.i6.i.i.i
                                        #   in Loop: Header=BB2_105 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_109
.LBB2_108:                              #   in Loop: Header=BB2_105 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_109:                              #   in Loop: Header=BB2_105 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_105
.LBB2_110:                              # %._crit_edge.i.i11.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_111:                              # %arj_getbits.exit12.preheader.i.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	xorl	%esi, %esi
	movl	$510, %edx              # imm = 0x1FE
	leaq	4128(%rsp), %rdi
	callq	memset
	movd	%ebp, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	xorl	%eax, %eax
	movl	4(%rsp), %ebp           # 4-byte Reload
	.p2align	4, 0x90
.LBB2_112:                              # %vector.body
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, 4638(%rsp,%rax,2)
	movdqu	%xmm0, 4654(%rsp,%rax,2)
	movdqu	%xmm0, 4670(%rsp,%rax,2)
	movdqu	%xmm0, 4686(%rsp,%rax,2)
	movdqu	%xmm0, 4702(%rsp,%rax,2)
	movdqu	%xmm0, 4718(%rsp,%rax,2)
	movdqu	%xmm0, 4734(%rsp,%rax,2)
	movdqu	%xmm0, 4750(%rsp,%rax,2)
	movdqu	%xmm0, 4766(%rsp,%rax,2)
	movdqu	%xmm0, 4782(%rsp,%rax,2)
	movdqu	%xmm0, 4798(%rsp,%rax,2)
	movdqu	%xmm0, 4814(%rsp,%rax,2)
	movdqu	%xmm0, 4830(%rsp,%rax,2)
	movdqu	%xmm0, 4846(%rsp,%rax,2)
	movdqu	%xmm0, 4862(%rsp,%rax,2)
	movdqu	%xmm0, 4878(%rsp,%rax,2)
	subq	$-128, %rax
	cmpq	$4096, %rax             # imm = 0x1000
	jne	.LBB2_112
.LBB2_113:                              # %read_c_len.exit.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$-1, %esi
	leaq	16(%rsp), %rdi
	callq	read_pt_len
	movw	32(%rsp), %ax
.LBB2_114:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	decl	%eax
	movw	%ax, 32(%rsp)
	movzwl	34(%rsp), %eax
	movl	%eax, %ecx
	shrl	$3, %ecx
	andl	$8190, %ecx             # imm = 0x1FFE
	movzwl	4638(%rsp,%rcx), %r14d
	cmpl	$510, %r14d             # imm = 0x1FE
	jb	.LBB2_118
# BB#115:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB2_116:                              # %.preheader.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	%r14w, %edx
	cmpl	$1019, %edx             # imm = 0x3FB
	jae	.LBB2_146
# BB#117:                               #   in Loop: Header=BB2_116 Depth=2
	testl	%eax, %ecx
	leaq	2090(%rsp,%rdx,2), %rsi
	leaq	52(%rsp,%rdx,2), %rdx
	cmovneq	%rsi, %rdx
	movzwl	(%rdx), %r14d
	shrl	%ecx
	cmpl	$509, %r14d             # imm = 0x1FD
	ja	.LBB2_116
.LBB2_118:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movzwl	%r14w, %ecx
	movzbl	4128(%rsp,%rcx), %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebx, %ecx
	jge	.LBB2_124
	.p2align	4, 0x90
.LBB2_119:                              # %._crit_edge31.i.i.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_122
# BB#120:                               #   in Loop: Header=BB2_119 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_125
# BB#121:                               # %._crit_edge32.i.i.i
                                        #   in Loop: Header=BB2_119 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_123
	.p2align	4, 0x90
.LBB2_122:                              #   in Loop: Header=BB2_119 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_123:                              #   in Loop: Header=BB2_119 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_119
.LBB2_124:                              # %._crit_edge.i37.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_125:                              # %decode_c.exit.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movswl	%r14w, %eax
	cmpl	$255, %eax
	jle	.LBB2_147
# BB#126:                               #   in Loop: Header=BB2_41 Depth=1
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movq	%r12, %rbp
	movzwl	34(%rsp), %eax
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$510, %ecx              # imm = 0x1FE
	movzwl	12850(%rsp,%rcx), %r12d
	cmpl	$17, %r12d
	jb	.LBB2_130
# BB#127:                               # %.preheader.i11.i.preheader
                                        #   in Loop: Header=BB2_41 Depth=1
	movl	$128, %ecx
	.p2align	4, 0x90
.LBB2_128:                              # %.preheader.i11.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	%r12w, %edx
	cmpl	$1019, %edx             # imm = 0x3FB
	jae	.LBB2_27
# BB#129:                               #   in Loop: Header=BB2_128 Depth=2
	testl	%eax, %ecx
	leaq	2090(%rsp,%rdx,2), %rsi
	leaq	52(%rsp,%rdx,2), %rdx
	cmovneq	%rsi, %rdx
	movzwl	(%rdx), %r12d
	shrl	%ecx
	cmpl	$16, %r12d
	ja	.LBB2_128
.LBB2_130:                              # %.loopexit.i15.i
                                        #   in Loop: Header=BB2_41 Depth=1
	movzwl	%r12w, %ecx
	movzbl	12830(%rsp,%rcx), %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebx, %ecx
	jge	.LBB2_136
	.p2align	4, 0x90
.LBB2_131:                              # %._crit_edge31.i.i18.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_134
# BB#132:                               #   in Loop: Header=BB2_131 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%rbp, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_137
# BB#133:                               # %._crit_edge32.i.i21.i
                                        #   in Loop: Header=BB2_131 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_135
	.p2align	4, 0x90
.LBB2_134:                              #   in Loop: Header=BB2_131 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_135:                              #   in Loop: Header=BB2_131 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_131
.LBB2_136:                              # %._crit_edge.i.i26.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_137:                              # %fill_buf.exit.i.i
                                        #   in Loop: Header=BB2_41 Depth=1
	testw	%r12w, %r12w
	je	.LBB2_26
# BB#138:                               #   in Loop: Header=BB2_41 Depth=1
	decl	%r12d
	movzwl	%r12w, %r15d
	movl	$1, %esi
	movl	%r12d, %ecx
	shll	%cl, %esi
	movzwl	34(%rsp), %eax
	movl	$16, %ecx
	subl	%r15d, %ecx
	movl	%eax, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	movl	%r12d, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	movq	%rbp, %r12
	movl	%esi, %ebp
	cmpl	%r15d, %ecx
	jge	.LBB2_144
	.p2align	4, 0x90
.LBB2_139:                              # %._crit_edge31.i.i.i29.i
                                        #   Parent Loop BB2_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_142
# BB#140:                               #   in Loop: Header=BB2_139 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_145
# BB#141:                               # %._crit_edge32.i.i.i32.i
                                        #   in Loop: Header=BB2_139 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_143
	.p2align	4, 0x90
.LBB2_142:                              #   in Loop: Header=BB2_139 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_143:                              #   in Loop: Header=BB2_139 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %r15d
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_139
.LBB2_144:                              # %._crit_edge.i.i.i37.i
                                        #   in Loop: Header=BB2_41 Depth=1
	subl	%r15d, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_145:                              # %arj_getbits.exit.i38.i
                                        #   in Loop: Header=BB2_41 Depth=1
	addl	%ebp, %ebx
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB2_29
.LBB2_146:                              # %decode_c.exit.thread.i
                                        #   in Loop: Header=BB2_41 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_147:                              #   in Loop: Header=BB2_41 Depth=1
	movq	24(%rsp), %rax
	movl	%r13d, %ecx
	movb	%r14b, (%rax,%rcx)
	incl	%ebp
	incl	%r13d
	cmpl	$26623, %r13d           # imm = 0x67FF
	jbe	.LBB2_149
# BB#148:                               #   in Loop: Header=BB2_41 Depth=1
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	$26624, %edx            # imm = 0x6800
	callq	cli_writen
	xorl	%r13d, %r13d
.LBB2_149:                              #   in Loop: Header=BB2_41 Depth=1
	movl	%ebp, %ebx
.LBB2_150:                              # %.critedge.backedge.i
                                        #   in Loop: Header=BB2_41 Depth=1
	cmpl	4(%r15), %ebx
	movl	%ebx, %ebp
	jb	.LBB2_41
	jmp	.LBB2_234
.LBB2_151:
	movl	$26624, %edi            # imm = 0x6800
	callq	cli_malloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB2_241
# BB#152:
	movl	%r14d, 16(%rsp)
	movl	(%r15), %edx
	movl	%edx, 44(%rsp)
	movb	$0, 36(%rsp)
	movl	$0, 40(%rsp)
	movw	$0, 34(%rsp)
	movl	$16, %ebx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.LBB2_154
	.p2align	4, 0x90
.LBB2_153:                              # %.._crit_edge31.i.i_crit_edge.i
                                        #   in Loop: Header=BB2_154 Depth=1
	movl	44(%rsp), %edx
.LBB2_154:                              # %._crit_edge31.i.i.i34
                                        # =>This Inner Loop Header: Depth=1
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %esi
	orl	%esi, %eax
	movw	%ax, 34(%rsp)
	testl	%edx, %edx
	je	.LBB2_157
# BB#155:                               #   in Loop: Header=BB2_154 Depth=1
	decl	%edx
	movl	%edx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	leaq	36(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_241
# BB#156:                               # %._crit_edge32.i.i.i37
                                        #   in Loop: Header=BB2_154 Depth=1
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_158
.LBB2_157:                              #   in Loop: Header=BB2_154 Depth=1
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_158:                              #   in Loop: Header=BB2_154 Depth=1
	movl	$8, 40(%rsp)
	movzbl	%cl, %esi
	movl	$8, %ecx
	cmpl	$9, %ebx
	jge	.LBB2_153
# BB#159:
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	orl	%esi, %eax
	movw	%ax, 34(%rsp)
	movw	$0, 50(%rsp)
	movw	$0, 48(%rsp)
	cmpl	$0, 4(%r15)
	je	.LBB2_240
# BB#160:                               # %.lr.ph81.i.preheader
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB2_186
.LBB2_161:                              # %._crit_edge.i.i47
                                        #   in Loop: Header=BB2_186 Depth=1
	subl	%ebx, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_162:                              # %fill_buf.exit.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movw	$16, 48(%rsp)
	movw	$16, %ax
	movw	50(%rsp), %dx
	jmp	.LBB2_181
.LBB2_163:                              #   in Loop: Header=BB2_186 Depth=1
	testw	%r14w, %r14w
	je	.LBB2_184
.LBB2_164:                              # %.thread.loopexit.i27.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movswl	%ax, %ecx
	cmpl	%r14d, %ecx
	jge	.LBB2_173
# BB#165:                               #   in Loop: Header=BB2_186 Depth=1
	movzwl	34(%rsp), %eax
	movl	%eax, %esi
	shrl	%cl, %esi
	orl	%esi, %edx
	movw	%dx, 50(%rsp)
	movl	$16, %ebp
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebp, %ecx
	jge	.LBB2_171
	.p2align	4, 0x90
.LBB2_166:                              # %._crit_edge31.i48.i32.i
                                        #   Parent Loop BB2_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_169
# BB#167:                               #   in Loop: Header=BB2_166 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	leaq	36(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_172
# BB#168:                               # %._crit_edge32.i46.i35.i
                                        #   in Loop: Header=BB2_166 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_170
.LBB2_169:                              #   in Loop: Header=BB2_166 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_170:                              #   in Loop: Header=BB2_166 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_166
.LBB2_171:                              # %._crit_edge.i54.i40.i
                                        #   in Loop: Header=BB2_186 Depth=1
	subl	%ebp, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_172:                              # %fill_buf.exit56.i42.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movw	$16, 48(%rsp)
	movw	$16, %ax
	movw	50(%rsp), %dx
.LBB2_173:                              #   in Loop: Header=BB2_186 Depth=1
	movzwl	%dx, %edx
	movl	$16, %ecx
	subl	%r14d, %ecx
	movswl	%dx, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%r14d, %ecx
	shll	%cl, %esi
	movw	%si, 50(%rsp)
	subl	%r14d, %eax
	movw	%ax, 48(%rsp)
.LBB2_174:                              # %decode_ptr.exit.i
                                        #   in Loop: Header=BB2_186 Depth=1
	xorl	%ebp, %ebp
	addl	%edx, %ebx
	movzwl	%bx, %eax
	movl	%r12d, %ebx
	subl	%eax, %ebx
	leal	65535(%rbx), %eax
	addl	$26623, %ebx            # imm = 0x67FF
	testw	%ax, %ax
	cmovnsw	%ax, %bx
	movzwl	%bx, %eax
	cmpl	$26623, %eax            # imm = 0x67FF
	jbe	.LBB2_176
	jmp	.LBB2_236
	.p2align	4, 0x90
.LBB2_175:                              #   in Loop: Header=BB2_176 Depth=2
	incl	%ebx
	movswl	%bx, %ebx
	cmpl	$26623, %ebx            # imm = 0x67FF
	cmovgw	%bp, %bx
	decl	%r13d
.LBB2_176:                              # %.preheader.i50
                                        #   Parent Loop BB2_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testw	%r13w, %r13w
	jle	.LBB2_183
# BB#177:                               # %.lr.ph.i51
                                        #   in Loop: Header=BB2_176 Depth=2
	movq	24(%rsp), %rax
	movswq	%bx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	%r12d, %edx
	movb	%cl, (%rax,%rdx)
	incl	%r12d
	cmpl	$26624, %r12d           # imm = 0x6800
	jb	.LBB2_175
# BB#178:                               #   in Loop: Header=BB2_176 Depth=2
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	$26624, %edx            # imm = 0x6800
	callq	cli_writen
	xorl	%r12d, %r12d
	jmp	.LBB2_175
.LBB2_179:                              #   in Loop: Header=BB2_186 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_209
.LBB2_180:                              #   in Loop: Header=BB2_186 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB2_181:                              #   in Loop: Header=BB2_186 Depth=1
	movl	%edx, %ecx
	shll	$8, %ecx
	movw	%cx, 50(%rsp)
	addl	$-8, %eax
	movw	%ax, 48(%rsp)
	movq	24(%rsp), %rax
	movl	%r12d, %ecx
	movb	%dh, (%rax,%rcx)  # NOREX
	incl	4(%rsp)                 # 4-byte Folded Spill
	incl	%r12d
	cmpl	$26623, %r12d           # imm = 0x67FF
	jbe	.LBB2_183
# BB#182:                               #   in Loop: Header=BB2_186 Depth=1
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	$26624, %edx            # imm = 0x6800
	callq	cli_writen
	xorl	%r12d, %r12d
.LBB2_183:                              # %.backedge.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	4(%r15), %eax
	jb	.LBB2_185
	jmp	.LBB2_237
.LBB2_184:                              #   in Loop: Header=BB2_186 Depth=1
	xorl	%edx, %edx
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB2_174
.LBB2_185:                              # %.backedge._crit_edge.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movw	48(%rsp), %ax
.LBB2_186:                              # %.lr.ph81.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_187 Depth 2
                                        #       Child Loop BB2_189 Depth 3
                                        #     Child Loop BB2_201 Depth 2
                                        #     Child Loop BB2_211 Depth 2
                                        #       Child Loop BB2_213 Depth 3
                                        #     Child Loop BB2_166 Depth 2
                                        #     Child Loop BB2_176 Depth 2
                                        #     Child Loop BB2_224 Depth 2
	xorl	%r14d, %r14d
	movl	$1, %ebx
	xorl	%r13d, %r13d
	leaq	36(%rsp), %r15
	.p2align	4, 0x90
.LBB2_187:                              #   Parent Loop BB2_186 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_189 Depth 3
	testw	%ax, %ax
	jg	.LBB2_196
# BB#188:                               #   in Loop: Header=BB2_187 Depth=2
	movswl	%ax, %esi
	movzwl	34(%rsp), %edx
	movl	%edx, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	movzwl	50(%rsp), %eax
	orl	%edi, %eax
	movw	%ax, 50(%rsp)
	movl	$16, %ebp
	subl	%esi, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	movw	%dx, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %eax
	cmpl	%ebp, %ecx
	jge	.LBB2_194
	.p2align	4, 0x90
.LBB2_189:                              # %._crit_edge31.i.i3.i
                                        #   Parent Loop BB2_186 Depth=1
                                        #     Parent Loop BB2_187 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %eax
	orl	%eax, %edx
	movw	%dx, 34(%rsp)
	movl	44(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_192
# BB#190:                               #   in Loop: Header=BB2_189 Depth=3
	decl	%eax
	movl	%eax, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_195
# BB#191:                               # %._crit_edge32.i.i6.i
                                        #   in Loop: Header=BB2_189 Depth=3
	movb	36(%rsp), %al
	movw	34(%rsp), %dx
	jmp	.LBB2_193
	.p2align	4, 0x90
.LBB2_192:                              #   in Loop: Header=BB2_189 Depth=3
	movb	$0, 36(%rsp)
	xorl	%eax, %eax
.LBB2_193:                              #   in Loop: Header=BB2_189 Depth=3
	movl	$8, 40(%rsp)
	cmpl	$8, %ebp
	movzbl	%al, %eax
	movl	$8, %ecx
	jg	.LBB2_189
.LBB2_194:                              # %._crit_edge.i.i8.i
                                        #   in Loop: Header=BB2_187 Depth=2
	subl	%ebp, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	orl	%eax, %edx
	movw	%dx, 34(%rsp)
.LBB2_195:                              # %fill_buf.exit.i.i44
                                        #   in Loop: Header=BB2_187 Depth=2
	movw	$16, 48(%rsp)
	movw	$16, %ax
.LBB2_196:                              #   in Loop: Header=BB2_187 Depth=2
	movzwl	50(%rsp), %ecx
	movl	%ecx, %edx
	addl	%edx, %edx
	movw	%dx, 50(%rsp)
	decl	%eax
	movw	%ax, 48(%rsp)
	testw	%cx, %cx
	jns	.LBB2_198
# BB#197:                               #   in Loop: Header=BB2_187 Depth=2
	addl	%ebx, %r13d
	leal	(%rbx,%rbx), %ecx
	andl	$131070, %ecx           # imm = 0x1FFFE
	incl	%r14d
	movzwl	%r13w, %r13d
	cmpl	$7, %r14d
	movl	%ecx, %ebx
	jb	.LBB2_187
	jmp	.LBB2_199
.LBB2_198:                              #   in Loop: Header=BB2_186 Depth=1
	testw	%r14w, %r14w
	je	.LBB2_179
.LBB2_199:                              # %.thread.i.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movswl	%ax, %ecx
	cmpl	%r14d, %ecx
	jge	.LBB2_208
# BB#200:                               #   in Loop: Header=BB2_186 Depth=1
	movzwl	34(%rsp), %eax
	movl	%eax, %esi
	shrl	%cl, %esi
	orl	%esi, %edx
	movw	%dx, 50(%rsp)
	movl	$16, %ebp
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebp, %ecx
	jge	.LBB2_206
	.p2align	4, 0x90
.LBB2_201:                              # %._crit_edge31.i48.i.i
                                        #   Parent Loop BB2_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_204
# BB#202:                               #   in Loop: Header=BB2_201 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_207
# BB#203:                               # %._crit_edge32.i46.i.i
                                        #   in Loop: Header=BB2_201 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_205
.LBB2_204:                              #   in Loop: Header=BB2_201 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_205:                              #   in Loop: Header=BB2_201 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_201
.LBB2_206:                              # %._crit_edge.i54.i.i
                                        #   in Loop: Header=BB2_186 Depth=1
	subl	%ebp, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
.LBB2_207:                              # %fill_buf.exit56.i.i
                                        #   in Loop: Header=BB2_186 Depth=1
	movw	$16, 48(%rsp)
	movw	$16, %ax
	movw	50(%rsp), %dx
.LBB2_208:                              #   in Loop: Header=BB2_186 Depth=1
	movzwl	%dx, %esi
	movl	$16, %ecx
	subl	%r14d, %ecx
	movswl	%si, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movl	%r14d, %ecx
	shll	%cl, %edx
	movw	%dx, 50(%rsp)
	subl	%r14d, %eax
	movw	%ax, 48(%rsp)
.LBB2_209:                              # %decode_len.exit.i
                                        #   in Loop: Header=BB2_186 Depth=1
	addl	%esi, %r13d
	testw	%r13w, %r13w
	je	.LBB2_222
# BB#210:                               #   in Loop: Header=BB2_186 Depth=1
	addl	$2, %r13d
	movswl	%r13w, %ecx
	addl	%ecx, 4(%rsp)           # 4-byte Folded Spill
	xorl	%ebx, %ebx
	movl	$512, %r15d             # imm = 0x200
	movl	$9, %r14d
	.p2align	4, 0x90
.LBB2_211:                              #   Parent Loop BB2_186 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_213 Depth 3
	testw	%ax, %ax
	jg	.LBB2_220
# BB#212:                               #   in Loop: Header=BB2_211 Depth=2
	movswl	%ax, %edi
	movzwl	34(%rsp), %esi
	movl	%esi, %ebp
	movl	%eax, %ecx
	shrl	%cl, %ebp
	orl	%ebp, %edx
	movw	%dx, 50(%rsp)
	movl	$16, %ebp
	subl	%edi, %ebp
	movl	%ebp, %ecx
	shll	%cl, %esi
	movw	%si, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %eax
	cmpl	%ebp, %ecx
	jge	.LBB2_218
	.p2align	4, 0x90
.LBB2_213:                              # %._crit_edge31.i.i15.i
                                        #   Parent Loop BB2_186 Depth=1
                                        #     Parent Loop BB2_211 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %eax
	orl	%eax, %esi
	movw	%si, 34(%rsp)
	movl	44(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_216
# BB#214:                               #   in Loop: Header=BB2_213 Depth=3
	decl	%eax
	movl	%eax, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	leaq	36(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_219
# BB#215:                               # %._crit_edge32.i.i18.i
                                        #   in Loop: Header=BB2_213 Depth=3
	movb	36(%rsp), %al
	movw	34(%rsp), %si
	jmp	.LBB2_217
	.p2align	4, 0x90
.LBB2_216:                              #   in Loop: Header=BB2_213 Depth=3
	movb	$0, 36(%rsp)
	xorl	%eax, %eax
.LBB2_217:                              #   in Loop: Header=BB2_213 Depth=3
	movl	$8, 40(%rsp)
	cmpl	$8, %ebp
	movzbl	%al, %eax
	movl	$8, %ecx
	jg	.LBB2_213
.LBB2_218:                              # %._crit_edge.i.i23.i
                                        #   in Loop: Header=BB2_211 Depth=2
	subl	%ebp, %ecx
	movl	%ecx, 40(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	orl	%eax, %esi
	movw	%si, 34(%rsp)
.LBB2_219:                              # %fill_buf.exit.i24.i
                                        #   in Loop: Header=BB2_211 Depth=2
	movw	$16, 48(%rsp)
	movw	$16, %ax
	movw	50(%rsp), %dx
.LBB2_220:                              #   in Loop: Header=BB2_211 Depth=2
	movw	%dx, %cx
	decl	%eax
	testw	%cx, %cx
	leal	(%rcx,%rcx), %edx
	movw	%dx, 50(%rsp)
	movw	%ax, 48(%rsp)
	jns	.LBB2_163
# BB#221:                               #   in Loop: Header=BB2_211 Depth=2
	addl	%r15d, %ebx
	addl	%r15d, %r15d
	andl	$130048, %r15d          # imm = 0x1FC00
	incl	%r14d
	movzwl	%bx, %ebx
	cmpl	$13, %r14d
	jb	.LBB2_211
	jmp	.LBB2_164
.LBB2_222:                              #   in Loop: Header=BB2_186 Depth=1
	movswl	%ax, %ecx
	cmpl	$7, %ecx
	jg	.LBB2_180
# BB#223:                               #   in Loop: Header=BB2_186 Depth=1
	movzwl	34(%rsp), %eax
	movl	%eax, %esi
	shrl	%cl, %esi
	orl	%esi, %edx
	movw	%dx, 50(%rsp)
	movl	$16, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	movw	%ax, 34(%rsp)
	movl	40(%rsp), %ecx
	movzbl	36(%rsp), %edx
	cmpl	%ebx, %ecx
	movq	8(%rsp), %r15           # 8-byte Reload
	jge	.LBB2_161
.LBB2_224:                              # %.lr.ph.i.i
                                        #   Parent Loop BB2_186 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 34(%rsp)
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_227
# BB#225:                               #   in Loop: Header=BB2_224 Depth=2
	decl	%ecx
	movl	%ecx, 44(%rsp)
	movl	16(%rsp), %edi
	movl	$1, %edx
	leaq	36(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_162
# BB#226:                               # %._crit_edge32.i.i
                                        #   in Loop: Header=BB2_224 Depth=2
	movb	36(%rsp), %cl
	movw	34(%rsp), %ax
	jmp	.LBB2_228
.LBB2_227:                              #   in Loop: Header=BB2_224 Depth=2
	movb	$0, 36(%rsp)
	xorl	%ecx, %ecx
.LBB2_228:                              #   in Loop: Header=BB2_224 Depth=2
	movl	$8, 40(%rsp)
	cmpl	$8, %ebx
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB2_224
	jmp	.LBB2_161
.LBB2_229:
	subl	%r13d, %ebx
	jmp	.LBB2_231
.LBB2_230:
	subl	%r13d, %ebx
	subl	%ebp, %ebx
.LBB2_231:
	movl	%ebx, %eax
	movq	8(%rsp), %r15           # 8-byte Reload
	xorl	%ebp, %ebp
.LBB2_232:                              # %arj_unstore.exit
	cmpl	(%r15), %eax
	movl	$-123, %eax
	cmovel	%ebp, %eax
	jmp	.LBB2_5
.LBB2_233:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_234:                              # %.loopexit.i
	testl	%r13d, %r13d
	je	.LBB2_240
# BB#235:
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	%r13d, %edx
	jmp	.LBB2_239
.LBB2_236:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_237:                              # %.loopexit45.i
	testl	%r12d, %r12d
	je	.LBB2_240
# BB#238:
	movl	28(%r15), %edi
	movq	24(%rsp), %rsi
	movl	%r12d, %edx
.LBB2_239:                              # %.loopexit45.thread.i
	callq	cli_writen
.LBB2_240:                              # %.loopexit45.thread.i
	movq	24(%rsp), %rdi
	callq	free
.LBB2_241:                              # %decode_f.exit
	xorl	%eax, %eax
	jmp	.LBB2_5
.Lfunc_end2:
	.size	cli_unarj_extract_file, .Lfunc_end2-cli_unarj_extract_file
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_pt_len,@function
read_pt_len:                            # @read_pt_len
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 112
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movzwl	18(%rbx), %eax
	movl	%eax, %r15d
	shrl	$11, %r15d
	shll	$5, %eax
	movw	%ax, 18(%rbx)
	movl	24(%rbx), %ecx
	leaq	20(%rbx), %r14
	movzbl	20(%rbx), %edx
	movl	$5, %ebp
	cmpl	$4, %ecx
	jg	.LBB3_7
# BB#1:                                 # %.lr.ph.i.i
	movl	$5, %ebp
	.p2align	4, 0x90
.LBB3_2:                                # %._crit_edge31.i.i
                                        # =>This Inner Loop Header: Depth=1
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
	movl	28(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	decl	%ecx
	movl	%ecx, 28(%rbx)
	movl	(%rbx), %edi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_8
# BB#4:                                 # %._crit_edge32.i.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movb	20(%rbx), %cl
	movw	18(%rbx), %ax
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movb	$0, (%r14)
	xorl	%ecx, %ecx
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$8, 24(%rbx)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB3_2
.LBB3_7:                                # %._crit_edge.i.i
	subl	%ebp, %ecx
	movl	%ecx, 24(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
.LBB3_8:                                # %arj_getbits.exit
	testw	%r15w, %r15w
	je	.LBB3_18
# BB#9:                                 # %.lr.ph51
	movzwl	%r15w, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%r12d, %rax
	leaq	12814(%rbx,%rax), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	-19(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	2(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	3(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_38 Depth 2
	movzwl	18(%rbx), %eax
	movl	%eax, %r13d
	shrl	$13, %r13d
	cmpl	$7, %r13d
	jne	.LBB3_14
# BB#11:                                # %.preheader27
                                        #   in Loop: Header=BB3_10 Depth=1
	movw	$7, %r13w
	testb	$16, %ah
	je	.LBB3_14
# BB#12:                                # %.lr.ph46
                                        #   in Loop: Header=BB3_10 Depth=1
	movw	$7, %r13w
	movl	$4096, %ecx             # imm = 0x1000
	.p2align	4, 0x90
.LBB3_13:                               #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrl	%ecx
	incl	%r13d
	testl	%ecx, %eax
	jne	.LBB3_13
.LBB3_14:                               # %.loopexit28
                                        #   in Loop: Header=BB3_10 Depth=1
	movswl	%r13w, %ecx
	leal	-3(%rcx), %ebp
	cmpl	$7, %ecx
	movl	$3, %ecx
	cmovll	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %eax
	movw	%ax, 18(%rbx)
	movl	24(%rbx), %ecx
	movzbl	20(%rbx), %edx
	cmpl	%ebp, %ecx
	jge	.LBB3_32
	.p2align	4, 0x90
.LBB3_15:                               # %._crit_edge31.i
                                        #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
	movl	28(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB3_30
# BB#16:                                #   in Loop: Header=BB3_15 Depth=2
	decl	%ecx
	movl	%ecx, 28(%rbx)
	movl	(%rbx), %edi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_33
# BB#17:                                # %._crit_edge32.i
                                        #   in Loop: Header=BB3_15 Depth=2
	movb	20(%rbx), %cl
	movw	18(%rbx), %ax
	jmp	.LBB3_31
	.p2align	4, 0x90
.LBB3_30:                               #   in Loop: Header=BB3_15 Depth=2
	movb	$0, (%r14)
	xorl	%ecx, %ecx
.LBB3_31:                               #   in Loop: Header=BB3_15 Depth=2
	movl	$8, 24(%rbx)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB3_15
.LBB3_32:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_10 Depth=1
	subl	%ebp, %ecx
	movl	%ecx, 24(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
.LBB3_33:                               # %fill_buf.exit
                                        #   in Loop: Header=BB3_10 Depth=1
	movslq	%r15d, %rax
	incl	%r15d
	movb	%r13b, 12814(%rbx,%rax)
	cmpl	%r12d, %r15d
	jne	.LBB3_34
# BB#36:                                #   in Loop: Header=BB3_10 Depth=1
	movzwl	18(%rbx), %r13d
	leal	(,%r13,4), %eax
	movw	%ax, 18(%rbx)
	movl	24(%rbx), %ecx
	movzbl	20(%rbx), %edx
	movl	$2, %ebp
	cmpl	$1, %ecx
	jg	.LBB3_43
# BB#37:                                # %._crit_edge31.i.i15.preheader
                                        #   in Loop: Header=BB3_10 Depth=1
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB3_38:                               # %._crit_edge31.i.i15
                                        #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
	movl	28(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB3_41
# BB#39:                                #   in Loop: Header=BB3_38 Depth=2
	decl	%ecx
	movl	%ecx, 28(%rbx)
	movl	(%rbx), %edi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_44
# BB#40:                                # %._crit_edge32.i.i18
                                        #   in Loop: Header=BB3_38 Depth=2
	movb	20(%rbx), %cl
	movw	18(%rbx), %ax
	jmp	.LBB3_42
	.p2align	4, 0x90
.LBB3_41:                               #   in Loop: Header=BB3_38 Depth=2
	movb	$0, (%r14)
	xorl	%ecx, %ecx
.LBB3_42:                               #   in Loop: Header=BB3_38 Depth=2
	movl	$8, 24(%rbx)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB3_38
.LBB3_43:                               # %._crit_edge.i.i23
                                        #   in Loop: Header=BB3_10 Depth=1
	subl	%ebp, %ecx
	movl	%ecx, 24(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
.LBB3_44:                               # %arj_getbits.exit24.preheader
                                        #   in Loop: Header=BB3_10 Depth=1
	cmpl	$18, %r12d
	movl	%r12d, %r15d
	jg	.LBB3_34
# BB#45:                                # %arj_getbits.exit24.preheader
                                        #   in Loop: Header=BB3_10 Depth=1
	shrl	$14, %r13d
	testw	%r13w, %r13w
	movl	%r12d, %r15d
	je	.LBB3_34
# BB#46:                                # %arj_getbits.exit24
                                        #   in Loop: Header=BB3_10 Depth=1
	movswl	%r13w, %ebp
	leal	-1(%rbp), %eax
	movzwl	%ax, %edx
	notq	%rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	%rdx, %rax
	cmovaeq	%rax, %rdx
	negq	%rdx
	xorl	%esi, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	memset
	movq	24(%rsp), %r15          # 8-byte Reload
	cmpq	$18, %r15
	jg	.LBB3_34
# BB#47:                                # %arj_getbits.exit24
                                        #   in Loop: Header=BB3_10 Depth=1
	leal	-2(%rbp), %eax
	testw	%ax, %ax
	movq	24(%rsp), %r15          # 8-byte Reload
	js	.LBB3_34
# BB#48:                                # %arj_getbits.exit24.1
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	cmpq	$18, %r15
	jg	.LBB3_34
# BB#49:                                # %arj_getbits.exit24.1
                                        #   in Loop: Header=BB3_10 Depth=1
	addl	$-3, %ebp
	testw	%bp, %bp
	movq	16(%rsp), %r15          # 8-byte Reload
	js	.LBB3_34
# BB#50:                                # %arj_getbits.exit24.2
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_34:                               # %.backedge
                                        #   in Loop: Header=BB3_10 Depth=1
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	jge	.LBB3_27
# BB#35:                                # %.backedge
                                        #   in Loop: Header=BB3_10 Depth=1
	cmpl	$19, %r15d
	jl	.LBB3_10
.LBB3_27:                               # %.preheader25
	cmpl	$18, %r15d
	jg	.LBB3_29
# BB#28:                                # %.lr.ph.preheader
	movslq	%r15d, %rax
	leaq	12814(%rbx,%rax), %rdi
	movl	$18, %edx
	subl	%r15d, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB3_29:                               # %._crit_edge
	leaq	12834(%rbx), %r8
	leaq	12814(%rbx), %rdx
	movl	$19, %esi
	movl	$8, %ecx
	movl	$256, %r9d              # imm = 0x100
	movq	%rbx, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	make_table              # TAILCALL
.LBB3_18:
	movzwl	18(%rbx), %eax
	movl	%eax, %r15d
	shrl	$11, %r15d
	shll	$5, %eax
	movw	%ax, 18(%rbx)
	movl	24(%rbx), %ecx
	movzbl	20(%rbx), %edx
	movl	$5, %ebp
	cmpl	$4, %ecx
	jg	.LBB3_25
# BB#19:                                # %.lr.ph.i.i1
	movl	$5, %ebp
	.p2align	4, 0x90
.LBB3_20:                               # %._crit_edge31.i.i3
                                        # =>This Inner Loop Header: Depth=1
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	shll	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
	movl	28(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB3_23
# BB#21:                                #   in Loop: Header=BB3_20 Depth=1
	decl	%ecx
	movl	%ecx, 28(%rbx)
	movl	(%rbx), %edi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_26
# BB#22:                                # %._crit_edge32.i.i6
                                        #   in Loop: Header=BB3_20 Depth=1
	movb	20(%rbx), %cl
	movw	18(%rbx), %ax
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_23:                               #   in Loop: Header=BB3_20 Depth=1
	movb	$0, (%r14)
	xorl	%ecx, %ecx
.LBB3_24:                               #   in Loop: Header=BB3_20 Depth=1
	movl	$8, 24(%rbx)
	cmpl	$8, %ebp
	movzbl	%cl, %edx
	movl	$8, %ecx
	jg	.LBB3_20
.LBB3_25:                               # %._crit_edge.i.i11
	subl	%ebp, %ecx
	movl	%ecx, 24(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	orl	%edx, %eax
	movw	%ax, 18(%rbx)
.LBB3_26:                               # %.loopexit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12814(%rbx)
	movb	$0, 12832(%rbx)
	movw	$0, 12830(%rbx)
	movd	%r15d, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 12834(%rbx)
	movdqu	%xmm0, 12850(%rbx)
	movdqu	%xmm0, 12866(%rbx)
	movdqu	%xmm0, 12882(%rbx)
	movdqu	%xmm0, 12898(%rbx)
	movdqu	%xmm0, 12914(%rbx)
	movdqu	%xmm0, 12930(%rbx)
	movdqu	%xmm0, 12946(%rbx)
	movdqu	%xmm0, 12962(%rbx)
	movdqu	%xmm0, 12978(%rbx)
	movdqu	%xmm0, 12994(%rbx)
	movdqu	%xmm0, 13010(%rbx)
	movdqu	%xmm0, 13026(%rbx)
	movdqu	%xmm0, 13042(%rbx)
	movdqu	%xmm0, 13058(%rbx)
	movdqu	%xmm0, 13074(%rbx)
	movdqu	%xmm0, 13090(%rbx)
	movdqu	%xmm0, 13106(%rbx)
	movdqu	%xmm0, 13122(%rbx)
	movdqu	%xmm0, 13138(%rbx)
	movdqu	%xmm0, 13154(%rbx)
	movdqu	%xmm0, 13170(%rbx)
	movdqu	%xmm0, 13186(%rbx)
	movdqu	%xmm0, 13202(%rbx)
	movdqu	%xmm0, 13218(%rbx)
	movdqu	%xmm0, 13234(%rbx)
	movdqu	%xmm0, 13250(%rbx)
	movdqu	%xmm0, 13266(%rbx)
	movdqu	%xmm0, 13282(%rbx)
	movdqu	%xmm0, 13298(%rbx)
	movdqu	%xmm0, 13314(%rbx)
	movdqu	%xmm0, 13330(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	read_pt_len, .Lfunc_end3-read_pt_len
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
.LCPI4_1:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI4_2:
	.quad	14                      # 0xe
	.quad	14                      # 0xe
.LCPI4_3:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI4_4:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.text
	.p2align	4, 0x90
	.type	make_table,@function
make_table:                             # @make_table
	.cfi_startproc
# BB#0:                                 # %.preheader7
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 112
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, -104(%rsp)        # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, -78(%rsp)
	movdqu	%xmm0, -94(%rsp)
	testl	%esi, %esi
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movl	%r9d, -12(%rsp)         # 4-byte Spill
	jle	.LBB4_1
# BB#2:                                 # %.lr.ph34.preheader
	movl	%esi, %eax
	leaq	-1(%rax), %r10
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph34.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx,%rbp), %ecx
	incw	-96(%rsp,%rcx,2)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_3
.LBB4_4:                                # %.lr.ph34.prol.loopexit
	cmpq	$3, %r10
	jb	.LBB4_7
# BB#5:                                 # %.lr.ph34.preheader.new
	subq	%rbp, %rax
	leaq	3(%rdx,%rbp), %rcx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %edx
	incw	-96(%rsp,%rdx,2)
	movzbl	-2(%rcx), %edx
	incw	-96(%rsp,%rdx,2)
	movzbl	-1(%rcx), %edx
	incw	-96(%rsp,%rdx,2)
	movzbl	(%rcx), %edx
	incw	-96(%rsp,%rdx,2)
	addq	$4, %rcx
	addq	$-4, %rax
	jne	.LBB4_6
.LBB4_7:                                # %.preheader6.loopexit
	movzwl	-94(%rsp), %r9d
	movzwl	-92(%rsp), %r11d
	movzwl	-90(%rsp), %eax
	movzwl	-88(%rsp), %ebx
	movzwl	-86(%rsp), %ebp
	movzwl	-84(%rsp), %ecx
	movzwl	-82(%rsp), %r13d
	movzwl	-80(%rsp), %r12d
	movzwl	-78(%rsp), %r15d
	movzwl	-76(%rsp), %r14d
	movzwl	-74(%rsp), %r10d
	shll	$15, %r9d
	shll	$14, %r11d
	shll	$13, %eax
	shll	$12, %ebx
	shll	$11, %ebp
	shll	$10, %ecx
	shll	$9, %r13d
	shll	$8, %r12d
	shll	$7, %r15d
	shll	$6, %r14d
	shll	$5, %r10d
	jmp	.LBB4_8
.LBB4_1:
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
.LBB4_8:                                # %.preheader6
	movw	$0, -46(%rsp)
	movw	%r9w, -44(%rsp)
	addl	%r9d, %r11d
	movw	%r11w, -42(%rsp)
	addl	%r11d, %eax
	movw	%ax, -40(%rsp)
	addl	%eax, %ebx
	movw	%bx, -38(%rsp)
	addl	%ebx, %ebp
	movw	%bp, -36(%rsp)
	addl	%ebp, %ecx
	movw	%cx, -34(%rsp)
	addl	%ecx, %r13d
	movw	%r13w, -32(%rsp)
	addl	%r13d, %r12d
	movw	%r12w, -30(%rsp)
	addl	%r12d, %r15d
	movw	%r15w, -28(%rsp)
	addl	%r15d, %r14d
	movw	%r14w, -26(%rsp)
	addl	%r14d, %r10d
	movw	%r10w, -24(%rsp)
	movzwl	-72(%rsp), %eax
	shll	$4, %eax
	addl	%r10d, %eax
	movw	%ax, -22(%rsp)
	movzwl	-70(%rsp), %ecx
	leal	(%rax,%rcx,8), %eax
	movw	%ax, -20(%rsp)
	movzwl	-68(%rsp), %ecx
	leal	(%rax,%rcx,4), %eax
	movw	%ax, -18(%rsp)
	movzwl	-66(%rsp), %ecx
	leal	(%rax,%rcx,2), %eax
	movw	%ax, -16(%rsp)
	addw	-64(%rsp), %ax
	movw	%ax, -14(%rsp)
	movq	-104(%rsp), %r11        # 8-byte Reload
	jne	.LBB4_71
# BB#9:
	movl	$16, %r14d
	subl	%r11d, %r14d
	testl	%r11d, %r11d
	jle	.LBB4_10
# BB#27:                                # %.lr.ph30.preheader
	leal	-1(%r11), %eax
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB4_28:                               # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movzwl	-48(%rsp,%rdx,2), %ebx
	movl	%r14d, %ecx
	shrl	%cl, %ebx
	movw	%bx, -48(%rsp,%rdx,2)
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	movw	%bx, 16(%rsp,%rdx,2)
	incl	%ebp
	decl	%eax
	cmpl	%r11d, %ebp
	jle	.LBB4_28
# BB#11:                                # %.preheader5
	cmpl	$16, %ebp
	jbe	.LBB4_12
	jmp	.LBB4_20
.LBB4_10:
	movl	$1, %ebp
.LBB4_12:                               # %.lr.ph26.preheader
	movl	%ebp, %ebp
	movl	$17, %ecx
	subq	%rbp, %rcx
	cmpq	$3, %rcx
	jbe	.LBB4_13
# BB#16:                                # %min.iters.checked
	movq	%rcx, %rax
	andq	$-4, %rax
	movq	%rcx, %r9
	andq	$-4, %r9
	je	.LBB4_13
# BB#17:                                # %vector.ph
	addq	%rbp, %rax
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm1       # xmm1 = xmm0[0,1,0,1]
	movl	$1, %edx
	movd	%rdx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	paddq	%xmm1, %xmm0
	leaq	20(%rsp,%rbp,2), %rbx
	movdqa	.LCPI4_0(%rip), %xmm8   # xmm8 = [16,16]
	movdqa	.LCPI4_1(%rip), %xmm2   # xmm2 = [4294967295,0,4294967295,0]
	movdqa	.LCPI4_2(%rip), %xmm9   # xmm9 = [14,14]
	movdqa	.LCPI4_3(%rip), %xmm3   # xmm3 = [1,1]
	movdqa	.LCPI4_4(%rip), %xmm5   # xmm5 = [4,4]
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB4_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm8, %xmm6
	psubq	%xmm0, %xmm6
	pand	%xmm2, %xmm6
	movdqa	%xmm9, %xmm7
	psubq	%xmm0, %xmm7
	pand	%xmm2, %xmm7
	pshufd	$78, %xmm6, %xmm1       # xmm1 = xmm6[2,3,0,1]
	movdqa	%xmm3, %xmm4
	psllq	%xmm1, %xmm4
	movdqa	%xmm3, %xmm1
	psllq	%xmm6, %xmm1
	movsd	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1]
	pshufd	$78, %xmm7, %xmm1       # xmm1 = xmm7[2,3,0,1]
	movdqa	%xmm3, %xmm6
	psllq	%xmm1, %xmm6
	movdqa	%xmm3, %xmm1
	psllq	%xmm7, %xmm1
	movsd	%xmm1, %xmm6            # xmm6 = xmm1[0],xmm6[1]
	pshufd	$232, %xmm4, %xmm1      # xmm1 = xmm4[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, -4(%rbx)
	pshufd	$232, %xmm6, %xmm1      # xmm1 = xmm6[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, (%rbx)
	paddq	%xmm5, %xmm0
	addq	$8, %rbx
	addq	$-4, %rbp
	jne	.LBB4_18
# BB#19:                                # %middle.block
	cmpq	%r9, %rcx
	jne	.LBB4_14
	jmp	.LBB4_20
.LBB4_13:
	movq	%rbp, %rax
.LBB4_14:                               # %.lr.ph26.preheader140
	movl	$16, %ecx
	subl	%eax, %ecx
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph26
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	shll	%cl, %edx
	movw	%dx, 16(%rsp,%rax,2)
	incq	%rax
	decl	%ecx
	cmpq	$17, %rax
	jne	.LBB4_15
.LBB4_20:                               # %._crit_edge27
	movslq	%r11d, %rax
	movzwl	-46(%rsp,%rax,2), %eax
	movl	%r14d, -116(%rsp)       # 4-byte Spill
	movl	%r14d, %ecx
	shrl	%cl, %eax
	testl	%eax, %eax
	je	.LBB4_42
# BB#21:
	movl	$1, %ebx
	movl	%r11d, %ecx
	shll	%cl, %ebx
	movl	%ebx, %r14d
	subl	%eax, %r14d
	je	.LBB4_42
# BB#22:                                # %.lr.ph24.preheader
	cmpl	$16, %r14d
	jb	.LBB4_36
# BB#23:                                # %min.iters.checked104
	movl	%r14d, %r9d
	andl	$-16, %r9d
	je	.LBB4_36
# BB#24:                                # %vector.scevcheck
	leal	-1(%rbx), %ecx
	cmpl	%ecx, %eax
	ja	.LBB4_36
# BB#25:                                # %vector.body100.preheader
	leal	-16(%r9), %r11d
	movl	%r11d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andl	$3, %ecx
	je	.LBB4_26
# BB#29:                                # %vector.body100.prol.preheader
	negl	%ecx
	xorl	%ebp, %ebp
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_30:                               # %vector.body100.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %edx
	movdqu	%xmm0, (%r8,%rdx,2)
	movdqu	%xmm0, 16(%r8,%rdx,2)
	addl	$16, %ebp
	incl	%ecx
	jne	.LBB4_30
	jmp	.LBB4_31
.LBB4_26:
	xorl	%ebp, %ebp
.LBB4_31:                               # %vector.body100.prol.loopexit
	cmpl	$48, %r11d
	jb	.LBB4_34
# BB#32:                                # %vector.body100.preheader.new
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_33:                               # %vector.body100
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	16(%rax,%rbp), %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	32(%rax,%rbp), %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	leal	48(%rax,%rbp), %ecx
	movdqu	%xmm0, (%r8,%rcx,2)
	movdqu	%xmm0, 16(%r8,%rcx,2)
	addl	$64, %ebp
	cmpl	%ebp, %r9d
	jne	.LBB4_33
.LBB4_34:                               # %middle.block101
	cmpl	%r9d, %r14d
	movq	-104(%rsp), %r11        # 8-byte Reload
	je	.LBB4_42
# BB#35:
	addl	%r9d, %eax
.LBB4_36:                               # %.lr.ph24.preheader139
	movl	%ebx, %ebp
	subl	%eax, %ebp
	leal	-1(%rbx), %ecx
	subl	%eax, %ecx
	andl	$7, %ebp
	je	.LBB4_39
# BB#37:                                # %.lr.ph24.prol.preheader
	negl	%ebp
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph24.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	incl	%eax
	movw	$0, (%r8,%rdx,2)
	incl	%ebp
	jne	.LBB4_38
.LBB4_39:                               # %.lr.ph24.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB4_42
# BB#40:                                # %.lr.ph24.preheader139.new
	negl	%ebx
	addl	$7, %eax
	.p2align	4, 0x90
.LBB4_41:                               # %.lr.ph24
                                        # =>This Inner Loop Header: Depth=1
	leal	-7(%rax), %ecx
	leal	-6(%rax), %edx
	movw	$0, (%r8,%rcx,2)
	leal	-5(%rax), %ecx
	movw	$0, (%r8,%rdx,2)
	leal	-4(%rax), %edx
	movw	$0, (%r8,%rcx,2)
	leal	-3(%rax), %ecx
	movw	$0, (%r8,%rdx,2)
	leal	-2(%rax), %edx
	movw	$0, (%r8,%rcx,2)
	leal	-1(%rax), %ecx
	movw	$0, (%r8,%rdx,2)
	movw	$0, (%r8,%rcx,2)
	movl	%eax, %ecx
	movw	$0, (%r8,%rcx,2)
	leal	8(%rbx,%rax), %ecx
	leal	8(%rax), %eax
	cmpl	$7, %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jne	.LBB4_41
.LBB4_42:                               # %.loopexit4
	movl	$15, %ecx
	subl	%r11d, %ecx
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	testl	%esi, %esi
	movq	-112(%rsp), %r14        # 8-byte Reload
	movl	-116(%rsp), %ecx        # 4-byte Reload
	jle	.LBB4_71
# BB#43:                                # %.lr.ph20.preheader
	movslq	%esi, %rdx
	leaq	16(%r8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	112(%r8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_44:                               # %.lr.ph20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_53 Depth 2
                                        #     Child Loop BB4_56 Depth 2
                                        #     Child Loop BB4_62 Depth 2
                                        #     Child Loop BB4_65 Depth 2
	movzbl	(%r14,%rbx), %r13d
	testl	%r13d, %r13d
	je	.LBB4_70
# BB#45:                                #   in Loop: Header=BB4_44 Depth=1
	movzwl	-48(%rsp,%r13,2), %r15d
	movzwl	16(%rsp,%r13,2), %eax
	leaq	(%rax,%r15), %r12
	cmpl	%r11d, %r13d
	jle	.LBB4_46
# BB#63:                                #   in Loop: Header=BB4_44 Depth=1
	movq	%rdx, %r10
	movl	%r15d, %eax
	shrl	%cl, %eax
	leaq	(%r8,%rax,2), %rax
	cmpl	%r11d, %r13d
	je	.LBB4_68
# BB#64:                                # %.lr.ph13.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	movl	%r11d, %edx
	subl	%r13d, %edx
	.p2align	4, 0x90
.LBB4_65:                               # %.lr.ph13
                                        #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax), %ebp
	testw	%bp, %bp
	jne	.LBB4_67
# BB#66:                                #   in Loop: Header=BB4_65 Depth=2
	movl	%esi, %ebp
	movw	$0, 36(%rdi,%rbp,2)
	movw	$0, 2074(%rdi,%rbp,2)
	movw	%si, (%rax)
	movl	%esi, %ebp
	leal	1(%rsi), %eax
	movl	%eax, %esi
.LBB4_67:                               #   in Loop: Header=BB4_65 Depth=2
	testl	%r9d, %r15d
	movzwl	%bp, %eax
	leaq	2074(%rdi,%rax,2), %rbp
	leaq	36(%rdi,%rax,2), %rax
	cmovneq	%rbp, %rax
	addl	%r15d, %r15d
	incl	%edx
	jne	.LBB4_65
.LBB4_68:                               # %._crit_edge
                                        #   in Loop: Header=BB4_44 Depth=1
	movw	%bx, (%rax)
	movq	%r10, %rdx
	jmp	.LBB4_69
	.p2align	4, 0x90
.LBB4_46:                               #   in Loop: Header=BB4_44 Depth=1
	cmpl	-12(%rsp), %r12d        # 4-byte Folded Reload
	ja	.LBB4_71
# BB#47:                                # %.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	testw	%ax, %ax
	je	.LBB4_69
# BB#48:                                # %.lr.ph
                                        #   in Loop: Header=BB4_44 Depth=1
	leaq	1(%r15), %r14
	cmpq	%r12, %r14
	cmovbeq	%r12, %r14
	subq	%r15, %r14
	cmpq	$16, %r14
	jb	.LBB4_61
# BB#49:                                # %min.iters.checked122
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	%r14, %rcx
	andq	$-16, %rcx
	movq	%r14, %r11
	andq	$-16, %r11
	je	.LBB4_60
# BB#50:                                # %vector.ph126
                                        #   in Loop: Header=BB4_44 Depth=1
	movd	%ebx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-16(%r11), %rbp
	movl	%ebp, %eax
	shrl	$4, %eax
	incl	%eax
	andq	$3, %rax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	je	.LBB4_51
# BB#52:                                # %vector.body118.prol.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%r15,2), %rcx
	negq	%rax
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB4_53:                               # %vector.body118.prol
                                        #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rcx,%r10,2)
	movdqu	%xmm0, (%rcx,%r10,2)
	addq	$16, %r10
	incq	%rax
	jne	.LBB4_53
	jmp	.LBB4_54
.LBB4_51:                               #   in Loop: Header=BB4_44 Depth=1
	xorl	%r10d, %r10d
.LBB4_54:                               # %vector.body118.prol.loopexit
                                        #   in Loop: Header=BB4_44 Depth=1
	cmpq	$48, %rbp
	jb	.LBB4_57
# BB#55:                                # %vector.ph126.new
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	%r11, %rax
	subq	%r10, %rax
	addq	%r15, %r10
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,2), %rcx
	.p2align	4, 0x90
.LBB4_56:                               # %vector.body118
                                        #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -112(%rcx)
	movdqu	%xmm0, -96(%rcx)
	movdqu	%xmm0, -80(%rcx)
	movdqu	%xmm0, -64(%rcx)
	movdqu	%xmm0, -48(%rcx)
	movdqu	%xmm0, -32(%rcx)
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	subq	$-128, %rcx
	addq	$-64, %rax
	jne	.LBB4_56
.LBB4_57:                               # %middle.block119
                                        #   in Loop: Header=BB4_44 Depth=1
	cmpq	%r11, %r14
	jne	.LBB4_59
# BB#58:                                #   in Loop: Header=BB4_44 Depth=1
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	-112(%rsp), %r14        # 8-byte Reload
	movl	-116(%rsp), %ecx        # 4-byte Reload
	jmp	.LBB4_69
.LBB4_59:                               #   in Loop: Header=BB4_44 Depth=1
	addq	8(%rsp), %r15           # 8-byte Folded Reload
.LBB4_60:                               # %scalar.ph120.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	-104(%rsp), %r11        # 8-byte Reload
.LBB4_61:                               # %scalar.ph120.preheader
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	-112(%rsp), %r14        # 8-byte Reload
	movl	-116(%rsp), %ecx        # 4-byte Reload
	.p2align	4, 0x90
.LBB4_62:                               # %scalar.ph120
                                        #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%bx, (%r8,%r15,2)
	incq	%r15
	cmpq	%r12, %r15
	jb	.LBB4_62
.LBB4_69:                               # %.loopexit
                                        #   in Loop: Header=BB4_44 Depth=1
	movw	%r12w, -48(%rsp,%r13,2)
.LBB4_70:                               #   in Loop: Header=BB4_44 Depth=1
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB4_44
.LBB4_71:                               # %.loopexit3
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	make_table, .Lfunc_end4-make_table
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_unarj_open\n"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Not in ARJ format\n"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Failed to read main header\n"
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"in cli_unarj_prepare_file\n"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"in cli_unarj_extract_file\n"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"PASSWORDed file (skipping)\n"
	.size	.L.str.5, 28

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Target offset: %ld\n"
	.size	.L.str.6, 20

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s/file.uar"
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Filename: %s\n"
	.size	.L.str.8, 14

	.type	is_arj_archive.header_id,@object # @is_arj_archive.header_id
	.section	.rodata,"a",@progbits
is_arj_archive.header_id:
	.ascii	"`\352"
	.size	is_arj_archive.header_id, 2

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"Not an ARJ archive\n"
	.size	.L.str.9, 20

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Header Size: %d\n"
	.size	.L.str.10, 17

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"arj_read_header: invalid header_size: %u\n "
	.size	.L.str.11, 43

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"ARJ Main File Header\n"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"First Header Size: %d\n"
	.size	.L.str.13, 23

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Version: %d\n"
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Min version: %d\n"
	.size	.L.str.15, 17

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Host OS: %d\n"
	.size	.L.str.16, 13

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Flags: 0x%x\n"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Security version: %d\n"
	.size	.L.str.18, 22

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"File type: %d\n"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Format error. First Header Size < 30\n"
	.size	.L.str.20, 38

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Comment: %s\n"
	.size	.L.str.21, 13

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Extended header size: %d\n"
	.size	.L.str.22, 26

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"arj_read_file_header: invalid header_size: %u\n "
	.size	.L.str.23, 48

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ARJ File Header\n"
	.size	.L.str.24, 17

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Method: %d\n"
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Compressed size: %u\n"
	.size	.L.str.26, 21

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Original size: %u\n"
	.size	.L.str.27, 19

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"in arj_unstore\n"
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"UNARJ: bounds exceeded - probably a corrupted file.\n"
	.size	.L.str.29, 53

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ERROR: bounds exceeded\n"
	.size	.L.str.30, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
