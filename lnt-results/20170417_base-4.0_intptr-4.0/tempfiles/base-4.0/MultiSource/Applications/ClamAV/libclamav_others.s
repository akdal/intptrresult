	.text
	.file	"libclamav_others.bc"
	.globl	cli_warnmsg
	.p2align	4, 0x90
	.type	cli_warnmsg,@function
cli_warnmsg:                            # @cli_warnmsg
	.cfi_startproc
# BB#0:
	subq	$8408, %rsp             # imm = 0x20D8
.Lcfi0:
	.cfi_def_cfa_offset 8416
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	movups	.L.str(%rip), %xmm0
	movaps	%xmm0, 208(%rsp)
	movb	$32, 226(%rsp)
	movw	$14951, 224(%rsp)       # imm = 0x3A67
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	8416(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	227(%rsp), %rdi
	movq	%rsp, %rcx
	movl	$8173, %esi             # imm = 0x1FED
	movq	%r10, %rdx
	callq	vsnprintf
	movb	$0, 8399(%rsp)
	movq	stderr(%rip), %rsi
	leaq	208(%rsp), %rdi
	callq	fputs
	addq	$8408, %rsp             # imm = 0x20D8
	retq
.Lfunc_end0:
	.size	cli_warnmsg, .Lfunc_end0-cli_warnmsg
	.cfi_endproc

	.globl	cli_errmsg
	.p2align	4, 0x90
	.type	cli_errmsg,@function
cli_errmsg:                             # @cli_errmsg
	.cfi_startproc
# BB#0:
	subq	$8408, %rsp             # imm = 0x20D8
.Lcfi1:
	.cfi_def_cfa_offset 8416
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	movups	.L.str.1(%rip), %xmm0
	movaps	%xmm0, 208(%rsp)
	movb	$32, 224(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	8416(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	225(%rsp), %rdi
	movq	%rsp, %rcx
	movl	$8175, %esi             # imm = 0x1FEF
	movq	%r10, %rdx
	callq	vsnprintf
	movb	$0, 8399(%rsp)
	movq	stderr(%rip), %rsi
	leaq	208(%rsp), %rdi
	callq	fputs
	addq	$8408, %rsp             # imm = 0x20D8
	retq
.Lfunc_end1:
	.size	cli_errmsg, .Lfunc_end1-cli_errmsg
	.cfi_endproc

	.globl	cli_dbgmsg
	.p2align	4, 0x90
	.type	cli_dbgmsg,@function
cli_dbgmsg:                             # @cli_dbgmsg
	.cfi_startproc
# BB#0:
	cmpb	$0, cli_debug_flag(%rip)
	je	.LBB2_1
# BB#2:
	jmp	puts                    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	cli_dbgmsg, .Lfunc_end2-cli_dbgmsg
	.cfi_endproc

	.globl	cl_debug
	.p2align	4, 0x90
	.type	cl_debug,@function
cl_debug:                               # @cl_debug
	.cfi_startproc
# BB#0:
	movb	$1, cli_debug_flag(%rip)
	retq
.Lfunc_end3:
	.size	cl_debug, .Lfunc_end3-cl_debug
	.cfi_endproc

	.globl	cl_retflevel
	.p2align	4, 0x90
	.type	cl_retflevel,@function
cl_retflevel:                           # @cl_retflevel
	.cfi_startproc
# BB#0:
	movl	$25, %eax
	retq
.Lfunc_end4:
	.size	cl_retflevel, .Lfunc_end4-cl_retflevel
	.cfi_endproc

	.globl	cl_retver
	.p2align	4, 0x90
	.type	cl_retver,@function
cl_retver:                              # @cl_retver
	.cfi_startproc
# BB#0:
	movl	$.L.str.2, %eax
	retq
.Lfunc_end5:
	.size	cl_retver, .Lfunc_end5-cl_retver
	.cfi_endproc

	.globl	cl_strerror
	.p2align	4, 0x90
	.type	cl_strerror,@function
cl_strerror:                            # @cl_strerror
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	addl	$127, %edi
	cmpl	$128, %edi
	ja	.LBB6_29
# BB#1:
	movl	$.L.str.3, %eax
	jmpq	*.LJTI6_0(,%rdi,8)
.LBB6_28:
	movl	$.L.str.30, %eax
	retq
.LBB6_27:
	movl	$.L.str.29, %eax
	retq
.LBB6_26:
	movl	$.L.str.28, %eax
	retq
.LBB6_25:
	movl	$.L.str.27, %eax
	retq
.LBB6_24:
	movl	$.L.str.26, %eax
	retq
.LBB6_22:
	movl	$.L.str.24, %eax
	retq
.LBB6_21:
	movl	$.L.str.23, %eax
	retq
.LBB6_20:
	movl	$.L.str.22, %eax
	retq
.LBB6_19:
	movl	$.L.str.21, %eax
	retq
.LBB6_13:
	movl	$.L.str.15, %eax
	retq
.LBB6_18:
	movl	$.L.str.20, %eax
	retq
.LBB6_17:
	movl	$.L.str.19, %eax
	retq
.LBB6_16:
	movl	$.L.str.18, %eax
	retq
.LBB6_15:
	movl	$.L.str.17, %eax
	retq
.LBB6_14:
	movl	$.L.str.16, %eax
	retq
.LBB6_12:
	movl	$.L.str.14, %eax
	retq
.LBB6_23:
	movl	$.L.str.25, %eax
	retq
.LBB6_29:
	movl	$.L.str.31, %eax
.LBB6_30:
	retq
.LBB6_10:
	movl	$.L.str.12, %eax
	retq
.LBB6_9:
	movl	$.L.str.11, %eax
	retq
.LBB6_11:
	movl	$.L.str.13, %eax
	retq
.LBB6_8:
	movl	$.L.str.10, %eax
	retq
.LBB6_7:
	movl	$.L.str.9, %eax
	retq
.LBB6_6:
	movl	$.L.str.8, %eax
	retq
.LBB6_5:
	movl	$.L.str.7, %eax
	retq
.LBB6_4:
	movl	$.L.str.6, %eax
	retq
.LBB6_3:
	movl	$.L.str.5, %eax
	retq
.LBB6_2:
	movl	$.L.str.4, %eax
	retq
.Lfunc_end6:
	.size	cl_strerror, .Lfunc_end6-cl_strerror
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_28
	.quad	.LBB6_27
	.quad	.LBB6_26
	.quad	.LBB6_25
	.quad	.LBB6_24
	.quad	.LBB6_22
	.quad	.LBB6_21
	.quad	.LBB6_20
	.quad	.LBB6_19
	.quad	.LBB6_13
	.quad	.LBB6_18
	.quad	.LBB6_17
	.quad	.LBB6_16
	.quad	.LBB6_15
	.quad	.LBB6_14
	.quad	.LBB6_12
	.quad	.LBB6_23
	.quad	.LBB6_29
	.quad	.LBB6_10
	.quad	.LBB6_9
	.quad	.LBB6_11
	.quad	.LBB6_29
	.quad	.LBB6_8
	.quad	.LBB6_7
	.quad	.LBB6_6
	.quad	.LBB6_5
	.quad	.LBB6_4
	.quad	.LBB6_3
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_29
	.quad	.LBB6_30
	.quad	.LBB6_2

	.text
	.globl	cli_md5digest
	.p2align	4, 0x90
	.type	cli_md5digest,@function
cli_md5digest:                          # @cli_md5digest
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$8360, %rsp             # imm = 0x20A8
.Lcfi8:
	.cfi_def_cfa_offset 8416
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edi, %r13d
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_1
# BB#2:                                 # %cli_malloc.exit
	leaq	8(%rsp), %r15
	movq	%r15, %rdi
	callq	cli_md5_init
	leaq	160(%rsp), %r12
	jmp	.LBB7_3
.LBB7_10:                               #   in Loop: Header=BB7_3 Depth=1
	movl	$8192, %eax             # imm = 0x2000
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_11:                               # %cli_readn.exit
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	$8192, %eax             # imm = 0x2000
	subl	%ebx, %eax
	jne	.LBB7_12
	jmp	.LBB7_13
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_3 Depth=1
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
	movl	$-1, %eax
.LBB7_12:                               # %cli_readn.exit.thread
                                        #   in Loop: Header=BB7_3 Depth=1
	movslq	%eax, %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	cli_md5_update
.LBB7_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #       Child Loop BB7_5 Depth 3
	movl	$8192, %ebx             # imm = 0x2000
	movq	%r12, %rbp
.LBB7_4:                                # %.thread.outer
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_5 Depth 3
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_5:                                # %.thread
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r13d, %edi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	read
	testl	%eax, %eax
	je	.LBB7_11
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=3
	jns	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=3
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$4, %edi
	je	.LBB7_5
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_4 Depth=2
	movslq	%eax, %rcx
	addq	%rcx, %rbp
	subl	%eax, %ebx
	jne	.LBB7_4
	jmp	.LBB7_10
.LBB7_13:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	cli_md5_final
.LBB7_14:
	movq	%r14, %rax
	addq	$8360, %rsp             # imm = 0x20A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_1:                                # %cli_malloc.exit.thread
	xorl	%r14d, %r14d
	movl	$.L.str.36, %edi
	movl	$16, %esi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB7_14
.Lfunc_end7:
	.size	cli_md5digest, .Lfunc_end7-cli_md5digest
	.cfi_endproc

	.globl	cli_malloc
	.p2align	4, 0x90
	.type	cli_malloc,@function
cli_malloc:                             # @cli_malloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	-1(%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB8_2
# BB#1:
	xorl	%r14d, %r14d
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB8_4
.LBB8_2:
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_3
.LBB8_4:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_3:
	xorl	%r14d, %r14d
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB8_4
.Lfunc_end8:
	.size	cli_malloc, .Lfunc_end8-cli_malloc
	.cfi_endproc

	.globl	cli_readn
	.p2align	4, 0x90
	.type	cli_readn,@function
cli_readn:                              # @cli_readn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movl	%edi, %r15d
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	read
	testl	%eax, %eax
	je	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	js	.LBB9_4
# BB#6:                                 #   in Loop: Header=BB9_1 Depth=1
	subl	%eax, %ebp
	cltq
	addq	%rax, %rbx
	testl	%ebp, %ebp
	jne	.LBB9_1
	jmp	.LBB9_8
	.p2align	4, 0x90
.LBB9_4:                                #   in Loop: Header=BB9_1 Depth=1
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$4, %edi
	jne	.LBB9_5
# BB#7:                                 #   in Loop: Header=BB9_1 Depth=1
	testl	%ebp, %ebp
	jne	.LBB9_1
	jmp	.LBB9_8
.LBB9_2:
	subl	%ebp, %r14d
	jmp	.LBB9_8
.LBB9_5:
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
	movl	$-1, %r14d
.LBB9_8:                                # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	cli_readn, .Lfunc_end9-cli_readn
	.cfi_endproc

	.globl	cli_md5stream
	.p2align	4, 0x90
	.type	cli_md5stream,@function
cli_md5stream:                          # @cli_md5stream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 40
	subq	$8376, %rsp             # imm = 0x20B8
.Lcfi33:
	.cfi_def_cfa_offset 8416
.Lcfi34:
	.cfi_offset %rbx, -40
.Lcfi35:
	.cfi_offset %r12, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	leaq	24(%rsp), %rdi
	callq	cli_md5_init
	leaq	176(%rsp), %rdi
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r12, %rcx
	callq	fread
	testl	%eax, %eax
	je	.LBB10_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	24(%rsp), %r15
	leaq	176(%rsp), %rbx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%eax, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	cli_md5_update
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%rbx, %rdi
	movq	%r12, %rcx
	callq	fread
	testl	%eax, %eax
	jne	.LBB10_2
.LBB10_3:                               # %._crit_edge
	movq	%rsp, %rdi
	leaq	24(%rsp), %rsi
	callq	cli_md5_final
	movl	$33, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB10_7
# BB#4:                                 # %cli_calloc.exit.preheader
	movzbl	(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	leaq	2(%rbx), %rdi
	movzbl	1(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	4(%rbx), %rdi
	movzbl	2(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	6(%rbx), %rdi
	movzbl	3(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	8(%rbx), %rdi
	movzbl	4(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	10(%rbx), %rdi
	movzbl	5(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	12(%rbx), %rdi
	movzbl	6(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	14(%rbx), %rdi
	movzbl	7(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	16(%rbx), %rdi
	movzbl	8(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	18(%rbx), %rdi
	movzbl	9(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	20(%rbx), %rdi
	movzbl	10(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	22(%rbx), %rdi
	movzbl	11(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	24(%rbx), %rdi
	movzbl	12(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	26(%rbx), %rdi
	movzbl	13(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	28(%rbx), %rdi
	movzbl	14(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	%rbx, %rdi
	addq	$30, %rdi
	movzbl	15(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	testq	%r14, %r14
	je	.LBB10_6
# BB#5:
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%r14)
.LBB10_6:
	movq	%rbx, %rax
	addq	$8376, %rsp             # imm = 0x20B8
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB10_7:                               # %cli_calloc.exit.thread
	xorl	%ebx, %ebx
	movl	$.L.str.39, %edi
	movl	$33, %esi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.40, %edi
	callq	perror
	jmp	.LBB10_6
.Lfunc_end10:
	.size	cli_md5stream, .Lfunc_end10-cli_md5stream
	.cfi_endproc

	.globl	cli_calloc
	.p2align	4, 0x90
	.type	cli_calloc,@function
cli_calloc:                             # @cli_calloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	leaq	-1(%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB11_2
# BB#1:
	xorl	%r14d, %r14d
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB11_4
.LBB11_2:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB11_3
.LBB11_4:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB11_3:
	imulq	%r15, %rbx
	xorl	%r14d, %r14d
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.40, %edi
	callq	perror
	jmp	.LBB11_4
.Lfunc_end11:
	.size	cli_calloc, .Lfunc_end11-cli_calloc
	.cfi_endproc

	.globl	cli_md5file
	.p2align	4, 0x90
	.type	cli_md5file,@function
cli_md5file:                            # @cli_md5file
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.33, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_2
# BB#1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	cli_md5stream
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	fclose
	movq	%r14, %rax
	jmp	.LBB12_3
.LBB12_2:
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	xorl	%eax, %eax
.LBB12_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	cli_md5file, .Lfunc_end12-cli_md5file
	.cfi_endproc

	.globl	cli_realloc
	.p2align	4, 0x90
	.type	cli_realloc,@function
cli_realloc:                            # @cli_realloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	leaq	-1(%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB13_2
# BB#1:
	xorl	%r14d, %r14d
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB13_4
.LBB13_2:
	movq	%rbx, %rsi
	callq	realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_3
.LBB13_4:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB13_3:
	xorl	%r14d, %r14d
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.43, %edi
	callq	perror
	jmp	.LBB13_4
.Lfunc_end13:
	.size	cli_realloc, .Lfunc_end13-cli_realloc
	.cfi_endproc

	.globl	cli_realloc2
	.p2align	4, 0x90
	.type	cli_realloc2,@function
cli_realloc2:                           # @cli_realloc2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	-1(%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB14_2
# BB#1:
	xorl	%r15d, %r15d
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB14_5
.LBB14_2:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	realloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB14_3
.LBB14_5:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_3:
	xorl	%r15d, %r15d
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.43, %edi
	callq	perror
	testq	%r14, %r14
	je	.LBB14_5
# BB#4:
	movq	%r14, %rdi
	callq	free
	xorl	%r15d, %r15d
	jmp	.LBB14_5
.Lfunc_end14:
	.size	cli_realloc2, .Lfunc_end14-cli_realloc2
	.cfi_endproc

	.globl	cli_strdup
	.p2align	4, 0x90
	.type	cli_strdup,@function
cli_strdup:                             # @cli_strdup
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -24
.Lcfi64:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB15_1
# BB#2:
	movq	%rbx, %rdi
	callq	__strdup
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB15_4
# BB#3:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%r14d, %r14d
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
	movl	$.L.str.48, %edi
	callq	perror
	jmp	.LBB15_4
.LBB15_1:
	xorl	%r14d, %r14d
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB15_4:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	cli_strdup, .Lfunc_end15-cli_strdup
	.cfi_endproc

	.globl	cli_rndnum
	.p2align	4, 0x90
	.type	cli_rndnum,@function
cli_rndnum:                             # @cli_rndnum
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 48
.Lcfi68:
	.cfi_offset %rbx, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	16(%rsp), %ebp
	callq	clock
	addl	%eax, %ebp
	movl	%ebp, %edi
	callq	srand
	callq	rand
	xorl	%edx, %edx
	divl	%ebx
	movl	%edx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	cli_rndnum, .Lfunc_end16-cli_rndnum
	.cfi_endproc

	.globl	cl_settempdir
	.p2align	4, 0x90
	.type	cl_settempdir,@function
cl_settempdir:                          # @cl_settempdir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB17_8
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	leaq	8(%rax), %r15
	addq	$7, %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB17_3
# BB#2:
	xorl	%ebp, %ebp
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	jmp	.LBB17_5
.LBB17_3:
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB17_4
.LBB17_5:                               # %cli_malloc.exit
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movq	%rbp, %rdi
	callq	putenv
	testl	%eax, %eax
	je	.LBB17_6
# BB#7:
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB17_8
.LBB17_6:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB17_8:
	movb	%r14b, cli_leavetemps_flag(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_4:
	xorl	%ebp, %ebp
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB17_5
.Lfunc_end17:
	.size	cl_settempdir, .Lfunc_end17-cl_settempdir
	.cfi_endproc

	.globl	cli_gentemp
	.p2align	4, 0x90
	.type	cli_gentemp,@function
cli_gentemp:                            # @cli_gentemp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 48
	subq	$224, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 272
.Lcfi85:
	.cfi_offset %rbx, -48
.Lcfi86:
	.cfi_offset %r12, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB18_2
# BB#1:
	movl	$.L.str.52, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.53, %r14d
	cmovneq	%rax, %r14
.LBB18_2:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	$41, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB18_3
# BB#4:                                 # %cli_calloc.exit
	movaps	name_salt(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	xorl	%ebx, %ebx
	leaq	72(%rsp), %r12
	.p2align	4, 0x90
.LBB18_5:                               # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	gettimeofday
	movl	80(%rsp), %ebp
	callq	clock
	addl	%eax, %ebp
	movl	%ebp, %edi
	callq	srand
	callq	rand
	movb	%al, 32(%rsp,%rbx)
	incq	%rbx
	cmpq	$32, %rbx
	jne	.LBB18_5
# BB#6:
	leaq	72(%rsp), %rbx
	movq	%rbx, %rdi
	callq	cli_md5_init
	leaq	16(%rsp), %rsi
	movl	$48, %edx
	movq	%rbx, %rdi
	callq	cli_md5_update
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	cli_md5_final
	movaps	(%rsp), %xmm0
	movaps	%xmm0, name_salt(%rip)
	movl	$33, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB18_7
# BB#8:
	movzbl	(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	leaq	2(%rbx), %rdi
	movzbl	1(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	4(%rbx), %rdi
	movzbl	2(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	6(%rbx), %rdi
	movzbl	3(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	8(%rbx), %rdi
	movzbl	4(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	10(%rbx), %rdi
	movzbl	5(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	12(%rbx), %rdi
	movzbl	6(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	14(%rbx), %rdi
	movzbl	7(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	16(%rbx), %rdi
	movzbl	8(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	18(%rbx), %rdi
	movzbl	9(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	20(%rbx), %rdi
	movzbl	10(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	22(%rbx), %rdi
	movzbl	11(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	24(%rbx), %rdi
	movzbl	12(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	26(%rbx), %rdi
	movzbl	13(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	28(%rbx), %rdi
	movzbl	14(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	%rbx, %rdi
	addq	$30, %rdi
	movzbl	15(%rsp), %edx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	sprintf
	movl	$32, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strncat
	movq	%rbx, %rdi
	callq	free
.LBB18_9:
	movq	%r15, %rax
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_3:
	xorl	%r15d, %r15d
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.40, %edi
	callq	perror
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB18_9
.LBB18_7:
	movl	$.L.str.39, %edi
	movl	$33, %esi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.40, %edi
	callq	perror
	movq	%r15, %rdi
	callq	free
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	xorl	%r15d, %r15d
	jmp	.LBB18_9
.Lfunc_end18:
	.size	cli_gentemp, .Lfunc_end18-cli_gentemp
	.cfi_endproc

	.globl	cli_gentempfd
	.p2align	4, 0x90
	.type	cli_gentempfd,@function
cli_gentempfd:                          # @cli_gentempfd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	callq	cli_gentemp
	movq	%rax, %rcx
	movq	%rcx, (%r14)
	testq	%rcx, %rcx
	je	.LBB19_1
# BB#2:
	xorl	%ebp, %ebp
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	open
	movl	%eax, (%rbx)
	cmpl	$-1, %eax
	jne	.LBB19_4
# BB#3:
	movq	(%r14), %rbx
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	free
	movl	$-123, %ebp
	jmp	.LBB19_4
.LBB19_1:
	movl	$-114, %ebp
.LBB19_4:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end19:
	.size	cli_gentempfd, .Lfunc_end19-cli_gentempfd
	.cfi_endproc

	.globl	cli_rmdirs
	.p2align	4, 0x90
	.type	cli_rmdirs,@function
cli_rmdirs:                             # @cli_rmdirs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi102:
	.cfi_def_cfa_offset 352
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	$448, %esi              # imm = 0x1C0
	callq	chmod
	movq	%rbp, %rdi
	callq	opendir
	movq	%rax, %r13
	movl	$-1, %eax
	testq	%r13, %r13
	je	.LBB20_32
# BB#1:                                 # %.preheader96
	leaq	152(%rsp), %rdx
	movl	$1, %edi
	movq	%rbp, %rsi
	jmp	.LBB20_3
	.p2align	4, 0x90
.LBB20_2:                               # %._crit_edge
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, %rdi
	callq	rewinddir
	movl	$1, %edi
	movq	%rbp, %rsi
	leaq	152(%rsp), %rdx
.LBB20_3:                               # %.preheader96
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_8 Depth 2
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB20_23
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	%rbp, %rdi
	callq	rmdir
	testl	%eax, %eax
	je	.LBB20_23
# BB#5:                                 #   in Loop: Header=BB20_3 Depth=1
	callq	__errno_location
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	(%rax), %edi
	cmpq	$39, %rdi
	ja	.LBB20_26
# BB#6:                                 #   in Loop: Header=BB20_3 Depth=1
	movabsq	$549755945472, %rax     # imm = 0x8000020200
	btq	%rdi, %rax
	jb	.LBB20_8
	jmp	.LBB20_26
	.p2align	4, 0x90
.LBB20_7:                               #   in Loop: Header=BB20_8 Depth=2
	movq	%r15, %rdi
	callq	free
.LBB20_8:                               # %.preheader
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB20_2
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB20_8 Depth=2
	cmpq	$0, (%r14)
	je	.LBB20_8
# BB#10:                                #   in Loop: Header=BB20_8 Depth=2
	cmpb	$46, 19(%r14)
	jne	.LBB20_14
# BB#11:                                #   in Loop: Header=BB20_8 Depth=2
	cmpb	$0, 20(%r14)
	je	.LBB20_8
# BB#12:                                #   in Loop: Header=BB20_8 Depth=2
	cmpb	$46, 20(%r14)
	jne	.LBB20_14
# BB#13:                                #   in Loop: Header=BB20_8 Depth=2
	cmpb	$0, 21(%r14)
	je	.LBB20_8
	.p2align	4, 0x90
.LBB20_14:                              # %.thread
                                        #   in Loop: Header=BB20_8 Depth=2
	addq	$19, %r14
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	leaq	2(%rax,%rbx), %r12
	leaq	1(%rax,%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jae	.LBB20_25
# BB#15:                                #   in Loop: Header=BB20_8 Depth=2
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB20_24
# BB#16:                                # %cli_malloc.exit
                                        #   in Loop: Header=BB20_8 Depth=2
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%r15, %rsi
	leaq	8(%rsp), %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB20_7
# BB#17:                                #   in Loop: Header=BB20_8 Depth=2
	movl	32(%rsp), %eax
	movl	$61440, %ecx            # imm = 0xF000
	andl	%ecx, %eax
	movq	%r15, %rdi
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB20_21
# BB#18:                                #   in Loop: Header=BB20_8 Depth=2
	callq	rmdir
	cmpl	$-1, %eax
	jne	.LBB20_7
# BB#19:                                #   in Loop: Header=BB20_8 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$13, (%rax)
	je	.LBB20_27
# BB#20:                                #   in Loop: Header=BB20_8 Depth=2
	movq	%r15, %rdi
	callq	cli_rmdirs
	testl	%eax, %eax
	je	.LBB20_7
	jmp	.LBB20_28
.LBB20_21:                              #   in Loop: Header=BB20_8 Depth=2
	callq	unlink
	testl	%eax, %eax
	jns	.LBB20_7
# BB#22:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	cli_warnmsg
	jmp	.LBB20_29
.LBB20_23:                              # %._crit_edge117
	movq	%r13, %rdi
	callq	closedir
	xorl	%eax, %eax
	jmp	.LBB20_32
.LBB20_24:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB20_30
.LBB20_25:
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	jmp	.LBB20_30
.LBB20_26:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	jmp	.LBB20_30
.LBB20_27:
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%r13, %rdi
	callq	closedir
	movq	%r15, %rdi
	callq	free
	jmp	.LBB20_31
.LBB20_28:
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_warnmsg
.LBB20_29:
	movq	%r15, %rdi
	callq	free
.LBB20_30:
	movq	%r13, %rdi
	callq	closedir
.LBB20_31:
	movl	$-1, %eax
.LBB20_32:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	cli_rmdirs, .Lfunc_end20-cli_rmdirs
	.cfi_endproc

	.globl	cli_writen
	.p2align	4, 0x90
	.type	cli_writen,@function
cli_writen:                             # @cli_writen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 48
.Lcfi114:
	.cfi_offset %rbx, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movl	%edi, %r15d
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	write
	testl	%eax, %eax
	js	.LBB21_2
# BB#4:                                 #   in Loop: Header=BB21_1 Depth=1
	subl	%eax, %ebp
	cltq
	addq	%rax, %rbx
	testl	%ebp, %ebp
	jne	.LBB21_1
	jmp	.LBB21_6
	.p2align	4, 0x90
.LBB21_2:                               #   in Loop: Header=BB21_1 Depth=1
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$4, %edi
	jne	.LBB21_3
# BB#5:                                 #   in Loop: Header=BB21_1 Depth=1
	testl	%ebp, %ebp
	jne	.LBB21_1
	jmp	.LBB21_6
.LBB21_3:
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
	movl	$-1, %r14d
.LBB21_6:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	cli_writen, .Lfunc_end21-cli_writen
	.cfi_endproc

	.globl	cli_filecopy
	.p2align	4, 0x90
	.type	cli_filecopy,@function
cli_filecopy:                           # @cli_filecopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 48
.Lcfi123:
	.cfi_offset %rbx, -48
.Lcfi124:
	.cfi_offset %r12, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	je	.LBB22_5
# BB#1:
	movl	$577, %esi              # imm = 0x241
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %r12d
	cmpl	$-1, %r12d
	je	.LBB22_2
# BB#3:
	movl	$8192, %edi             # imm = 0x2000
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB22_7
# BB#4:                                 # %cli_malloc.exit.thread
	movl	$.L.str.36, %edi
	movl	$8192, %esi             # imm = 0x2000
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB22_5
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_7 Depth=1
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
.LBB22_7:                               # %cli_writen.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_8 Depth 2
                                        #       Child Loop BB22_9 Depth 3
                                        #     Child Loop BB22_18 Depth 2
                                        #       Child Loop BB22_19 Depth 3
	movl	$8192, %ebp             # imm = 0x2000
	movq	%r14, %rbx
.LBB22_8:                               # %.thread.outer
                                        #   Parent Loop BB22_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_9 Depth 3
	movl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_9:                               # %.thread
                                        #   Parent Loop BB22_7 Depth=1
                                        #     Parent Loop BB22_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	read
	testl	%eax, %eax
	je	.LBB22_16
# BB#10:                                #   in Loop: Header=BB22_9 Depth=3
	jns	.LBB22_14
# BB#11:                                #   in Loop: Header=BB22_9 Depth=3
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$4, %edi
	je	.LBB22_9
	jmp	.LBB22_12
	.p2align	4, 0x90
.LBB22_14:                              #   in Loop: Header=BB22_8 Depth=2
	movslq	%eax, %rcx
	addq	%rcx, %rbx
	subl	%eax, %ebp
	jne	.LBB22_8
# BB#15:                                #   in Loop: Header=BB22_7 Depth=1
	movl	$8192, %ebx             # imm = 0x2000
	jmp	.LBB22_17
	.p2align	4, 0x90
.LBB22_16:                              # %cli_readn.exit
                                        #   in Loop: Header=BB22_7 Depth=1
	movl	$8192, %ebx             # imm = 0x2000
	subl	%ebp, %ebx
	testl	%ebx, %ebx
	jle	.LBB22_13
.LBB22_17:                              # %cli_readn.exit.thread.outer.preheader
                                        #   in Loop: Header=BB22_7 Depth=1
	movq	%r14, %rbp
.LBB22_18:                              # %cli_readn.exit.thread.outer
                                        #   Parent Loop BB22_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_19 Depth 3
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_19:                              # %cli_readn.exit.thread
                                        #   Parent Loop BB22_7 Depth=1
                                        #     Parent Loop BB22_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %edi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	write
	testl	%eax, %eax
	jns	.LBB22_6
# BB#20:                                #   in Loop: Header=BB22_19 Depth=3
	callq	__errno_location
	movl	(%rax), %edi
	cmpl	$4, %edi
	je	.LBB22_19
	jmp	.LBB22_21
	.p2align	4, 0x90
.LBB22_6:                               #   in Loop: Header=BB22_18 Depth=2
	movslq	%eax, %rcx
	addq	%rcx, %rbp
	subl	%eax, %ebx
	jne	.LBB22_18
	jmp	.LBB22_7
.LBB22_12:                              # %cli_readn.exit.thread23
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_errmsg
.LBB22_13:                              # %.loopexit
	movq	%r14, %rdi
	callq	free
	movl	%r15d, %edi
	callq	close
	movl	%r12d, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	close                   # TAILCALL
.LBB22_2:
	movl	%r15d, %edi
	callq	close
.LBB22_5:
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	cli_filecopy, .Lfunc_end22-cli_filecopy
	.cfi_endproc

	.globl	cli_bitset_init
	.p2align	4, 0x90
	.type	cli_bitset_init,@function
cli_bitset_init:                        # @cli_bitset_init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB23_1
# BB#2:                                 # %cli_malloc.exit
	movq	$1024, 8(%r14)          # imm = 0x400
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB23_3
.LBB23_4:                               # %cli_calloc.exit
	movq	%rbx, (%r14)
.LBB23_5:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB23_1:                               # %cli_malloc.exit.thread
	xorl	%r14d, %r14d
	movl	$.L.str.36, %edi
	movl	$16, %esi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.37, %edi
	callq	perror
	jmp	.LBB23_5
.LBB23_3:
	xorl	%ebx, %ebx
	movl	$.L.str.39, %edi
	movl	$1024, %esi             # imm = 0x400
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$.L.str.40, %edi
	callq	perror
	jmp	.LBB23_4
.Lfunc_end23:
	.size	cli_bitset_init, .Lfunc_end23-cli_bitset_init
	.cfi_endproc

	.globl	cli_bitset_free
	.p2align	4, 0x90
	.type	cli_bitset_free,@function
cli_bitset_free:                        # @cli_bitset_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 16
.Lcfi134:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB24_4
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_3
# BB#2:
	callq	free
.LBB24_3:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB24_4:
	popq	%rbx
	retq
.Lfunc_end24:
	.size	cli_bitset_free, .Lfunc_end24-cli_bitset_free
	.cfi_endproc

	.globl	cli_bitset_set
	.p2align	4, 0x90
	.type	cli_bitset_set,@function
cli_bitset_set:                         # @cli_bitset_set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 48
.Lcfi140:
	.cfi_offset %rbx, -48
.Lcfi141:
	.cfi_offset %r12, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r15, %rbp
	shrq	$3, %rbp
	cmpq	8(%r14), %rbp
	jae	.LBB25_2
# BB#1:                                 # %._crit_edge
	movq	(%r14), %r12
	jmp	.LBB25_11
.LBB25_2:
	leaq	1(%rbp), %rax
	movl	$1024, %ecx             # imm = 0x400
	.p2align	4, 0x90
.LBB25_3:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rcx
	jae	.LBB25_4
# BB#5:                                 #   in Loop: Header=BB25_3 Depth=1
	leaq	(%rcx,%rcx), %rbx
	cmpq	%rax, %rbx
	jae	.LBB25_6
# BB#13:                                #   in Loop: Header=BB25_3 Depth=1
	leaq	(,%rcx,4), %rbx
	cmpq	%rax, %rbx
	jae	.LBB25_6
# BB#14:                                #   in Loop: Header=BB25_3 Depth=1
	leaq	(,%rcx,8), %rbx
	cmpq	%rax, %rbx
	jae	.LBB25_6
# BB#15:                                #   in Loop: Header=BB25_3 Depth=1
	movq	%rcx, %rbx
	shlq	$4, %rbx
	cmpq	%rax, %rbx
	jae	.LBB25_6
# BB#16:                                #   in Loop: Header=BB25_3 Depth=1
	movq	%rcx, %rbx
	shlq	$5, %rbx
	cmpq	%rax, %rbx
	jae	.LBB25_6
# BB#17:                                #   in Loop: Header=BB25_3 Depth=1
	shlq	$6, %rcx
	jne	.LBB25_3
# BB#18:
	movq	%rax, %rbx
	jmp	.LBB25_6
.LBB25_4:
	movq	%rcx, %rbx
.LBB25_6:                               # %nearest_power.exit.i
	leaq	-1(%rbx), %rax
	cmpq	$184549376, %rax        # imm = 0xB000000
	jb	.LBB25_8
# BB#7:
	xorl	%r14d, %r14d
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	jmp	.LBB25_12
.LBB25_8:
	movq	(%r14), %rdi
	movq	%rbx, %rsi
	callq	realloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB25_9
# BB#10:                                # %bitset_realloc.exit
	movq	%r12, (%r14)
	movq	8(%r14), %rax
	movq	%r12, %rdi
	addq	%rax, %rdi
	movq	%rbx, %rdx
	subq	%rax, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	%rbx, 8(%r14)
.LBB25_11:
	andb	$7, %r15b
	movl	$1, %r14d
	movl	$1, %eax
	movl	%r15d, %ecx
	shll	%cl, %eax
	movzbl	(%r12,%rbp), %ecx
	orl	%eax, %ecx
	movb	%cl, (%r12,%rbp)
.LBB25_12:                              # %bitset_realloc.exit.thread
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_9:
	xorl	%r14d, %r14d
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$.L.str.43, %edi
	callq	perror
	jmp	.LBB25_12
.Lfunc_end25:
	.size	cli_bitset_set, .Lfunc_end25-cli_bitset_set
	.cfi_endproc

	.globl	cli_bitset_test
	.p2align	4, 0x90
	.type	cli_bitset_test,@function
cli_bitset_test:                        # @cli_bitset_test
	.cfi_startproc
# BB#0:
	movq	%rsi, %rcx
	shrq	$3, %rcx
	xorl	%eax, %eax
	cmpq	8(%rdi), %rcx
	jae	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	movzbl	(%rax,%rcx), %edx
	andb	$7, %sil
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	andl	%edx, %eax
.LBB26_2:
	retq
.Lfunc_end26:
	.size	cli_bitset_test, .Lfunc_end26-cli_bitset_test
	.cfi_endproc

	.type	cli_debug_flag,@object  # @cli_debug_flag
	.bss
	.globl	cli_debug_flag
cli_debug_flag:
	.byte	0                       # 0x0
	.size	cli_debug_flag, 1

	.type	cli_leavetemps_flag,@object # @cli_leavetemps_flag
	.globl	cli_leavetemps_flag
cli_leavetemps_flag:
	.byte	0                       # 0x0
	.size	cli_leavetemps_flag, 1

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LibClamAV Warning: "
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LibClamAV Error: "
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"devel-20071218"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"No viruses detected"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Virus(es) detected"
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Recursion limit exceeded"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"File size limit exceeded"
	.size	.L.str.6, 25

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Files number limit exceeded"
	.size	.L.str.7, 28

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"RAR module failure"
	.size	.L.str.8, 19

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Zip module failure"
	.size	.L.str.9, 19

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"GZip module failure"
	.size	.L.str.10, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"MS Expand module failure"
	.size	.L.str.11, 25

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"MS CAB module failure"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"OLE2 module failure"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Unable to create temporary file"
	.size	.L.str.14, 32

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Unable to create temporary directory"
	.size	.L.str.15, 37

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Unable to synchronize file <-> disk"
	.size	.L.str.16, 36

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Unable to allocate memory"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Unable to open file or directory"
	.size	.L.str.18, 33

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Malformed database"
	.size	.L.str.19, 19

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Too short pattern detected"
	.size	.L.str.20, 27

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Broken or not a CVD file"
	.size	.L.str.21, 25

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"CVD extraction failure"
	.size	.L.str.22, 23

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"MD5 verification error"
	.size	.L.str.23, 23

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Digital signature verification error"
	.size	.L.str.24, 37

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Null argument passed while initialized is required"
	.size	.L.str.25, 51

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Input/Output error"
	.size	.L.str.26, 19

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Bad format or broken data"
	.size	.L.str.27, 26

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Not supported data format"
	.size	.L.str.28, 26

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Unable to lock database directory"
	.size	.L.str.29, 34

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ARJ module failure"
	.size	.L.str.30, 19

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Unknown error code"
	.size	.L.str.31, 19

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%02x"
	.size	.L.str.32, 5

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"rb"
	.size	.L.str.33, 3

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"cli_md5file(): Can't read file %s\n"
	.size	.L.str.34, 35

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"cli_malloc(): Attempt to allocate %u bytes. Please report to http://bugs.clamav.net\n"
	.size	.L.str.35, 85

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"cli_malloc(): Can't allocate memory (%u bytes).\n"
	.size	.L.str.36, 49

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"malloc_problem"
	.size	.L.str.37, 15

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"cli_calloc(): Attempt to allocate %u bytes. Please report to http://bugs.clamav.net\n"
	.size	.L.str.38, 85

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"cli_calloc(): Can't allocate memory (%u bytes).\n"
	.size	.L.str.39, 49

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"calloc_problem"
	.size	.L.str.40, 15

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"cli_realloc(): Attempt to allocate %u bytes. Please report to http://bugs.clamav.net\n"
	.size	.L.str.41, 86

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cli_realloc(): Can't re-allocate memory to %u bytes.\n"
	.size	.L.str.42, 54

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"realloc_problem"
	.size	.L.str.43, 16

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"cli_realloc2(): Attempt to allocate %u bytes. Please report to http://bugs.clamav.net\n"
	.size	.L.str.44, 87

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"cli_realloc2(): Can't re-allocate memory to %u bytes.\n"
	.size	.L.str.45, 55

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"cli_strdup(): s == NULL. Please report to http://bugs.clamav.net\n"
	.size	.L.str.46, 66

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"cli_strdup(): Can't allocate memory (%u bytes).\n"
	.size	.L.str.47, 49

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"strdup_problem"
	.size	.L.str.48, 15

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"TMPDIR=%s"
	.size	.L.str.49, 10

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Setting %s as global temporary directory\n"
	.size	.L.str.50, 42

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Can't set TMPDIR variable - insufficient space in the environment.\n"
	.size	.L.str.51, 68

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"TMPDIR"
	.size	.L.str.52, 7

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"/tmp"
	.size	.L.str.53, 5

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"cli_gentemp('%s'): out of memory\n"
	.size	.L.str.54, 34

	.type	name_salt,@object       # @name_salt
	.data
	.p2align	4
name_salt:
	.ascii	"\020&a\f\b\004H\304\331\220!|\022\013\021\375"
	.size	name_salt, 16

	.type	.L.str.55,@object       # @.str.55
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.55:
	.asciz	"%s/clamav-"
	.size	.L.str.55, 11

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"cli_gentempfd: Can't create temporary file %s: %s\n"
	.size	.L.str.56, 51

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"cli_rmdirs: Can't remove temporary directory %s: %s\n"
	.size	.L.str.57, 53

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%s/%s"
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"cli_rmdirs: Can't remove some temporary directories due to access problem.\n"
	.size	.L.str.61, 76

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"cli_rmdirs: Can't remove nested directory %s\n"
	.size	.L.str.62, 46

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"cli_rmdirs: Couldn't remove %s: %s\n"
	.size	.L.str.63, 36

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"cli_readn: read error: %s\n"
	.size	.L.str.64, 27

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"cli_writen: write error: %s\n"
	.size	.L.str.65, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
