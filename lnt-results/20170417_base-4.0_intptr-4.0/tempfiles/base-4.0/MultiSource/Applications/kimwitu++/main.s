	.text
	.file	"main.bc"
	.section	.text._ZN15cmdline_optionsD2Ev,"axG",@progbits,_ZN15cmdline_optionsD2Ev,comdat
	.weak	_ZN15cmdline_optionsD2Ev
	.p2align	4, 0x90
	.type	_ZN15cmdline_optionsD2Ev,@function
_ZN15cmdline_optionsD2Ev:               # @_ZN15cmdline_optionsD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	312(%rbx), %rdi
	leaq	328(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_2
# BB#1:
	callq	_ZdlPv
.LBB0_2:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	280(%rbx), %rdi
	leaq	296(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_4
# BB#3:
	callq	_ZdlPv
.LBB0_4:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit10
	movq	248(%rbx), %rdi
	leaq	264(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_6
# BB#5:
	callq	_ZdlPv
.LBB0_6:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit11
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_8
# BB#7:
	callq	_ZdlPv
.LBB0_8:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit12
	movq	184(%rbx), %rdi
	leaq	200(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_10
# BB#9:
	callq	_ZdlPv
.LBB0_10:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit13
	movq	152(%rbx), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_12
# BB#11:
	callq	_ZdlPv
.LBB0_12:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit14
	movq	120(%rbx), %rdi
	leaq	136(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_14
# BB#13:
	callq	_ZdlPv
.LBB0_14:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit15
	movq	80(%rbx), %rdi
	leaq	96(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_16
# BB#15:
	callq	_ZdlPv
.LBB0_16:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit16
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_18
# BB#17:
	callq	_ZdlPv
.LBB0_18:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit17
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.LBB0_19
# BB#20:
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.LBB0_19:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit18
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN15cmdline_optionsD2Ev, .Lfunc_end0-_ZN15cmdline_optionsD2Ev
	.cfi_endproc

	.text
	.globl	cleanup_and_die
	.p2align	4, 0x90
	.type	cleanup_and_die,@function
cleanup_and_die:                        # @cleanup_and_die
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	%edi, %ecx
	movq	stderr(%rip), %rdi
	movq	g_progname(%rip), %rax
	movq	8(%rax), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	_ZN2kc5leaveEi
.Lfunc_end1:
	.size	cleanup_and_die, .Lfunc_end1-cleanup_and_die
	.cfi_endproc

	.globl	_ZN2kc5leaveEi
	.p2align	4, 0x90
	.type	_ZN2kc5leaveEi,@function
_ZN2kc5leaveEi:                         # @_ZN2kc5leaveEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB2_2
# BB#1:                                 # %_ZN14kc_filePrinter6fcloseEv.exit.thread.i
	movq	$0, v_ccfile_printer+8(%rip)
	jmp	.LBB2_4
.LBB2_2:                                # %_ZN14kc_filePrinter6fcloseEv.exit.i
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB2_4
# BB#3:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.125, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB2_4:
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB2_6
# BB#5:                                 # %_ZN14kc_filePrinter6fcloseEv.exit2.thread.i
	movq	$0, v_hfile_printer+8(%rip)
	jmp	.LBB2_8
.LBB2_6:                                # %_ZN14kc_filePrinter6fcloseEv.exit2.i
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB2_8
# BB#7:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.125, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB2_8:
	movl	$.L.str.13, %edi
	movl	$4, %esi
	callq	access
	testl	%eax, %eax
	jne	.LBB2_17
# BB#9:
	cmpb	$0, g_options+65(%rip)
	je	.LBB2_15
# BB#10:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.126, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$.L.str.13, %esi
	movl	$11, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB2_28
# BB#11:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i
	cmpb	$0, 56(%rbx)
	jne	.LBB2_12
# BB#13:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
	jmp	.LBB2_14
.LBB2_12:
	movb	67(%rbx), %al
.LBB2_14:                               # %_ZNKSt5ctypeIcE5widenEc.exit.i
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.LBB2_15:
	movl	$.L.str.13, %edi
	callq	remove
	testl	%eax, %eax
	je	.LBB2_17
# BB#16:
	movl	$.L.str.123, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.124, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB2_17:                               # %_ZN2kcL7eremoveEPKc.exit3.i
	movl	$.L.str.10, %edi
	movl	$4, %esi
	callq	access
	testl	%eax, %eax
	jne	.LBB2_26
# BB#18:
	cmpb	$0, g_options+65(%rip)
	je	.LBB2_24
# BB#19:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.126, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$.L.str.10, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_20
.LBB2_28:
	callq	_ZSt16__throw_bad_castv
.LBB2_20:                               # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit7.i
	cmpb	$0, 56(%rbx)
	jne	.LBB2_21
# BB#22:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
	jmp	.LBB2_23
.LBB2_21:
	movb	67(%rbx), %al
.LBB2_23:                               # %_ZNKSt5ctypeIcE5widenEc.exit6.i
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.LBB2_24:
	movl	$.L.str.10, %edi
	callq	remove
	testl	%eax, %eax
	je	.LBB2_26
# BB#25:
	movl	$.L.str.123, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.124, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB2_26:                               # %_ZN2kcL7cleanupEv.exit
	cmpb	$0, gp_no_fatal_problems(%rip)
	jne	.LBB2_29
# BB#27:
	testl	%ebp, %ebp
	movl	$1, %edi
	cmovnel	%ebp, %edi
	callq	exit
.LBB2_29:
	movl	%ebp, %edi
	callq	exit
.Lfunc_end2:
	.size	_ZN2kc5leaveEi, .Lfunc_end2-_ZN2kc5leaveEi
	.cfi_endproc

	.globl	cleanup_and_abort
	.p2align	4, 0x90
	.type	cleanup_and_abort,@function
cleanup_and_abort:                      # @cleanup_and_abort
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	stderr(%rip), %rcx
	cmpl	$11, %ebx
	jne	.LBB3_2
# BB#1:
	movl	$.L.str.3, %edi
	movl	$29, %esi
	jmp	.LBB3_3
.LBB3_2:
	movl	$.L.str.4, %edi
	movl	$34, %esi
.LBB3_3:
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movq	g_progname(%rip), %rax
	movq	8(%rax), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	_ZN2kc5leaveEi
.Lfunc_end3:
	.size	cleanup_and_abort, .Lfunc_end3-cleanup_and_abort
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi16:
	.cfi_def_cfa_offset 528
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	$1, yydebug(%rip)
	movl	$.L.str.56, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, g_progname(%rip)
	movl	$.L.str.5, %edi
	callq	_ZN2kcL16make_pg_filenameEPKc
	movq	%rax, pg_filename(%rip)
	movb	$1, gp_no_fatal_problems(%rip)
	movb	$0, pg_uviewshavebeendefined(%rip)
	movb	$0, pg_rviewshavebeendefined(%rip)
	movb	$0, pg_storageclasseshavebeendefined(%rip)
	movl	$0, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
	movq	(%r14), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdi
	cmoveq	%rbx, %rdi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, g_progname(%rip)
	leaq	16(%rsp), %r12
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movl	%r15d, 80(%rsp)         # 4-byte Spill
	jmp	.LBB4_6
.LBB4_1:                                # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_EvT_S7_RSaIT0_E.exit.loopexit.i.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	32(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_4
	jmp	.LBB4_6
.LBB4_2:                                #   in Loop: Header=BB4_6 Depth=1
	movw	$257, g_options+344(%rip) # imm = 0x101
	movb	$1, g_options+346(%rip)
	jmp	.LBB4_6
.LBB4_3:                                #   in Loop: Header=BB4_6 Depth=1
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	80(%rsp), %r15d         # 4-byte Reload
	testq	%rbp, %rbp
	je	.LBB4_6
.LBB4_4:                                #   in Loop: Header=BB4_6 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # %.backedge.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%rax, %r8
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
.LBB4_6:                                # %.backedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_28 Depth 2
                                        #     Child Loop BB4_70 Depth 2
                                        #     Child Loop BB4_95 Depth 2
	movl	$.L.str.57, %edx
	xorl	%eax, %eax
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incl	%eax
	cmpl	$122, %eax
	ja	.LBB4_6
# BB#7:                                 # %.backedge.i
                                        #   in Loop: Header=BB4_6 Depth=1
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_8:                                #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rax
	testq	%rax, %rax
	movl	$.L.str.70, %ebx
	cmovneq	%rax, %rbx
	movq	g_options+128(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+120, %edi
	jmp	.LBB4_63
.LBB4_9:                                #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+67(%rip)
	jmp	.LBB4_6
.LBB4_10:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rbx
	movq	g_options+224(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+216, %edi
	jmp	.LBB4_63
.LBB4_11:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rbx
	movq	g_options+192(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+184, %edi
	jmp	.LBB4_63
.LBB4_12:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB4_65
.LBB4_13:                               #   in Loop: Header=BB4_6 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	optarg(%rip), %rsi
	movl	$.L.str.71, %edi
	movl	$.L.str.72, %edx
	callq	_ZN2kc9Problem3SEPKcS1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movq	optarg(%rip), %rbp
	jmp	.LBB4_67
.LBB4_14:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+72(%rip)
	jmp	.LBB4_6
.LBB4_15:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+70(%rip)
	jmp	.LBB4_6
.LBB4_16:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+115(%rip)
	jmp	.LBB4_6
.LBB4_17:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+66(%rip)
	jmp	.LBB4_6
.LBB4_18:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rax
	movb	(%rax), %al
	movl	%eax, %ecx
	addb	$-110, %cl
	cmpb	$11, %cl
	ja	.LBB4_20
# BB#19:                                #   in Loop: Header=BB4_6 Depth=1
	movzbl	%cl, %ecx
	movl	$2053, %edx             # imm = 0x805
	btq	%rcx, %rdx
	jb	.LBB4_21
.LBB4_20:                               #   in Loop: Header=BB4_6 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.68, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movq	optarg(%rip), %rax
	movb	(%rax), %al
.LBB4_21:                               #   in Loop: Header=BB4_6 Depth=1
	movb	%al, g_options+74(%rip)
	jmp	.LBB4_6
.LBB4_22:                               #   in Loop: Header=BB4_6 Depth=1
	cmpq	$0, optarg(%rip)
	je	.LBB4_2
# BB#23:                                #   in Loop: Header=BB4_6 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	$0, 48(%rsp)
	movq	%r12, (%rsp)
	movq	$0, 8(%rsp)
	movb	$0, 16(%rsp)
.Ltmp0:
	leaq	32(%rsp), %rdi
	movq	%rsp, %rdx
	xorl	%esi, %esi
	callq	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_
.Ltmp1:
# BB#24:                                # %_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE9push_backERKS5_.exit.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_6 Depth=1
	callq	_ZdlPv
.LBB4_26:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit.i
                                        #   in Loop: Header=BB4_6 Depth=1
	xorl	%ebp, %ebp
	movq	optarg(%rip), %rax
	jmp	.LBB4_28
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_28 Depth=2
	movq	optarg(%rip), %rax
	incq	%rax
	movq	%rax, optarg(%rip)
.LBB4_28:                               #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ebx
	cmpb	$44, %bl
	je	.LBB4_32
# BB#29:                                #   in Loop: Header=BB4_28 Depth=2
	testb	%bl, %bl
	je	.LBB4_68
# BB#30:                                #   in Loop: Header=BB4_28 Depth=2
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	movslq	%ebp, %rbp
	movq	32(%rsp), %r12
	shlq	$5, %rbp
	movq	(%r12,%rbp), %rax
	movq	8(%r12,%rbp), %r13
	leaq	1(%r13), %r14
	leaq	16(%r12,%rbp), %rcx
	cmpq	%rcx, %rax
	je	.LBB4_34
# BB#31:                                #   in Loop: Header=BB4_28 Depth=2
	movq	16(%r12,%rbp), %rcx
	jmp	.LBB4_35
	.p2align	4, 0x90
.LBB4_32:                               # %._crit_edge.i.i.i.i102.i
                                        #   in Loop: Header=BB4_28 Depth=2
	movq	%r12, (%rsp)
	movq	$0, 8(%rsp)
	movb	$0, 16(%rsp)
	movq	40(%rsp), %rsi
	cmpq	48(%rsp), %rsi
	je	.LBB4_39
# BB#33:                                # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE9constructIS6_EEvRS7_PS6_RKT_.exit.i114.i
                                        #   in Loop: Header=BB4_28 Depth=2
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	addq	$32, %rsi
	movq	%rsi, 40(%rsp)
	jmp	.LBB4_40
.LBB4_34:                               #   in Loop: Header=BB4_28 Depth=2
	movl	$15, %ecx
.LBB4_35:                               # %_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8capacityEv.exit.i.i.i
                                        #   in Loop: Header=BB4_28 Depth=2
	leaq	(%r12,%rbp), %r15
	cmpq	%rcx, %r14
	jbe	.LBB4_38
# BB#36:                                #   in Loop: Header=BB4_28 Depth=2
.Ltmp27:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm
.Ltmp28:
# BB#37:                                # %.noexc122.i
                                        #   in Loop: Header=BB4_28 Depth=2
	movq	(%r15), %rax
.LBB4_38:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit.i
                                        #   in Loop: Header=BB4_28 Depth=2
	leaq	8(%r12,%rbp), %rcx
	movb	%bl, (%rax,%r13)
	movq	%r14, (%rcx)
	movq	(%r15), %rax
	movb	$0, 1(%rax,%r13)
	leaq	16(%rsp), %r12
	movl	72(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB4_27
.LBB4_39:                               #   in Loop: Header=BB4_28 Depth=2
.Ltmp3:
	leaq	32(%rsp), %rdi
	movq	%rsp, %rdx
	callq	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_
.Ltmp4:
.LBB4_40:                               # %_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE9push_backERKS5_.exit118.i
                                        #   in Loop: Header=BB4_28 Depth=2
	movq	(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB4_42
# BB#41:                                #   in Loop: Header=BB4_28 Depth=2
	callq	_ZdlPv
.LBB4_42:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit119.i
                                        #   in Loop: Header=BB4_28 Depth=2
	incl	%ebp
	jmp	.LBB4_27
.LBB4_43:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+64(%rip)
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_6
# BB#44:                                #   in Loop: Header=BB4_6 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.58, %edi
	jmp	.LBB4_61
.LBB4_45:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$0, g_options+114(%rip)
	jmp	.LBB4_6
.LBB4_46:                               #   in Loop: Header=BB4_6 Depth=1
	movq	$.L.str.69, pg_line(%rip)
	jmp	.LBB4_6
.LBB4_47:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rbx
	movq	g_options+288(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+280, %edi
	jmp	.LBB4_63
.LBB4_48:                               #   in Loop: Header=BB4_6 Depth=1
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	getcwd
	movq	%rax, %rbx
	movq	g_options+320(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+312, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
	movq	%rbx, %rdi
	callq	free
	movq	g_options+320(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB4_6
# BB#49:                                #   in Loop: Header=BB4_6 Depth=1
	movq	g_options+312(%rip), %rax
	cmpb	$47, -1(%rax,%rbp)
	je	.LBB4_6
# BB#50:                                # %_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8capacityEv.exit.i.i141.i
                                        #   in Loop: Header=BB4_6 Depth=1
	leaq	1(%rbp), %rbx
	movl	$g_options+328, %ecx
	cmpq	%rcx, %rax
	movl	$15, %ecx
	cmovneq	g_options+328(%rip), %rcx
	cmpq	%rcx, %rbx
	jbe	.LBB4_52
# BB#51:                                #   in Loop: Header=BB4_6 Depth=1
	movl	$g_options+312, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbp, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm
	movq	g_options+312(%rip), %rax
.LBB4_52:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit143.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movb	$47, (%rax,%rbp)
	movq	%rbx, g_options+320(%rip)
	movq	g_options+312(%rip), %rax
	movb	$0, 1(%rax,%rbp)
	jmp	.LBB4_6
.LBB4_53:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+69(%rip)
	jmp	.LBB4_6
.LBB4_54:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+71(%rip)
	jmp	.LBB4_6
.LBB4_55:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+113(%rip)
	jmp	.LBB4_6
.LBB4_56:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+68(%rip)
	jmp	.LBB4_6
.LBB4_57:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+112(%rip)
	jmp	.LBB4_6
.LBB4_58:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rax
	testq	%rax, %rax
	movl	$.L.str.67, %ebx
	cmovneq	%rax, %rbx
	movq	g_options+88(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+80, %edi
	jmp	.LBB4_63
.LBB4_59:                               #   in Loop: Header=BB4_6 Depth=1
	movb	$1, g_options+65(%rip)
	cmpb	$0, g_options+64(%rip)
	je	.LBB4_6
# BB#60:                                #   in Loop: Header=BB4_6 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.59, %edi
.LBB4_61:                               # %.backedge.i
                                        #   in Loop: Header=BB4_6 Depth=1
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_6
.LBB4_62:                               #   in Loop: Header=BB4_6 Depth=1
	movq	optarg(%rip), %rbx
	movq	g_options+256(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+248, %edi
	.p2align	4, 0x90
.LBB4_63:                               # %.backedge.i
                                        #   in Loop: Header=BB4_6 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	jmp	.LBB4_5
.LBB4_64:                               #   in Loop: Header=BB4_6 Depth=1
	movw	$257, g_options+72(%rip) # imm = 0x101
	jmp	.LBB4_6
.LBB4_65:                               #   in Loop: Header=BB4_6 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	cmpb	$107, -1(%rbp,%rax)
	jne	.LBB4_67
# BB#66:                                #   in Loop: Header=BB4_6 Depth=1
	cmpb	$46, -2(%rbp,%rax)
	je	.LBB4_13
.LBB4_67:                               #   in Loop: Header=BB4_6 Depth=1
	movq	g_options+160(%rip), %rbx
	movq	%rbp, %rdi
	callq	strlen
	movl	$g_options+152, %edi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	jmp	.LBB4_5
.LBB4_68:                               # %.preheader.i
                                        #   in Loop: Header=BB4_6 Depth=1
	testl	%ebp, %ebp
	js	.LBB4_93
# BB#69:                                # %.lr.ph285.preheader.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movslq	%ebp, %r15
	movq	$-1, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_70:                               # %.lr.ph285.i
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp6:
	movl	$.L.str.60, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp7:
# BB#71:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_86
# BB#72:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp8:
	movl	$.L.str.61, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp9:
# BB#73:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_87
# BB#74:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp10:
	movl	$.L.str.62, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp11:
# BB#75:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_88
# BB#76:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp12:
	movl	$.L.str.63, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp13:
# BB#77:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_89
# BB#78:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp14:
	movl	$.L.str.64, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp15:
# BB#79:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_90
# BB#80:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rdi
	addq	%rbp, %rdi
.Ltmp16:
	movl	$.L.str.65, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp17:
# BB#81:                                #   in Loop: Header=BB4_70 Depth=2
	testl	%eax, %eax
	je	.LBB4_91
# BB#82:                                #   in Loop: Header=BB4_70 Depth=2
.Ltmp18:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %r14
.Ltmp19:
# BB#83:                                #   in Loop: Header=BB4_70 Depth=2
	movq	32(%rsp), %rax
	movq	(%rax,%rbp), %rsi
.Ltmp20:
	movl	$.L.str.66, %edi
	callq	_ZN2kc9Problem2SEPKcS1_
.Ltmp21:
# BB#84:                                #   in Loop: Header=BB4_70 Depth=2
.Ltmp22:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
.Ltmp23:
# BB#85:                                #   in Loop: Header=BB4_70 Depth=2
.Ltmp24:
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.Ltmp25:
	jmp	.LBB4_92
	.p2align	4, 0x90
.LBB4_86:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$1, g_options+344(%rip)
	jmp	.LBB4_92
	.p2align	4, 0x90
.LBB4_87:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$0, g_options+344(%rip)
	jmp	.LBB4_92
.LBB4_88:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$1, g_options+345(%rip)
	jmp	.LBB4_92
.LBB4_89:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$0, g_options+345(%rip)
	jmp	.LBB4_92
.LBB4_90:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$1, g_options+346(%rip)
	jmp	.LBB4_92
.LBB4_91:                               #   in Loop: Header=BB4_70 Depth=2
	movb	$0, g_options+346(%rip)
	.p2align	4, 0x90
.LBB4_92:                               #   in Loop: Header=BB4_70 Depth=2
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r15, %rbx
	jl	.LBB4_70
.LBB4_93:                               # %._crit_edge286.i
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	32(%rsp), %rbp
	movq	40(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB4_3
# BB#94:                                # %.lr.ph.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	80(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB4_95:                               # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdi
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	je	.LBB4_97
# BB#96:                                #   in Loop: Header=BB4_95 Depth=2
	callq	_ZdlPv
.LBB4_97:                               # %_ZSt8_DestroyINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvPT_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB4_95 Depth=2
	addq	$16, %rbp
	cmpq	%rbx, %rbp
	jne	.LBB4_95
	jmp	.LBB4_1
.LBB4_98:
	cmpb	$0, g_options+66(%rip)
	je	.LBB4_100
# BB#99:
	movb	g_options+69(%rip), %al
	testb	%al, %al
	jne	.LBB4_101
.LBB4_100:
	movb	$0, g_options+70(%rip)
.LBB4_101:
	movslq	optind(%rip), %r13
	cmpl	%r15d, %r13d
	jge	.LBB4_159
# BB#102:                               # %.lr.ph.i
	movslq	80(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	248(%rsp), %r12
	leaq	280(%rsp), %r15
	.p2align	4, 0x90
.LBB4_103:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14,%r13,8), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %r14
	cmoveq	%rbp, %r14
	cmpb	$107, -1(%rbp,%rbx)
	je	.LBB4_106
# BB#104:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$46, -2(%rbp,%rbx)
	jne	.LBB4_106
# BB#105:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	(%r14,%r13,8), %rsi
	movl	$.L.str.73, %edi
	jmp	.LBB4_157
	.p2align	4, 0x90
.LBB4_106:                              #   in Loop: Header=BB4_103 Depth=1
	movl	$g_options+184, %esi
	movl	$.L.str.74, %edx
	movq	%rsp, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_114
# BB#107:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp30:
	movl	$g_options+184, %esi
	movl	$.L.str.75, %edx
	leaq	32(%rsp), %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp31:
# BB#108:                               #   in Loop: Header=BB4_103 Depth=1
	movq	32(%rsp), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_115
# BB#109:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp33:
	movl	$g_options+184, %esi
	movl	$.L.str.76, %edx
	leaq	184(%rsp), %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp34:
# BB#110:                               #   in Loop: Header=BB4_103 Depth=1
	movq	184(%rsp), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_116
# BB#111:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp36:
	movl	$g_options+184, %esi
	movl	$.L.str.77, %edx
	leaq	440(%rsp), %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp37:
# BB#112:                               #   in Loop: Header=BB4_103 Depth=1
	movq	440(%rsp), %rbp
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_117
# BB#113:                               #   in Loop: Header=BB4_103 Depth=1
	movl	$.L.str.78, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	sete	%bl
	jmp	.LBB4_118
	.p2align	4, 0x90
.LBB4_114:                              #   in Loop: Header=BB4_103 Depth=1
	movb	$1, %bl
	movq	64(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_124
.LBB4_115:                              #   in Loop: Header=BB4_103 Depth=1
	movb	$1, %bl
	movq	64(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_122
.LBB4_116:                              #   in Loop: Header=BB4_103 Depth=1
	movb	$1, %bl
	movq	64(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_120
.LBB4_117:                              #   in Loop: Header=BB4_103 Depth=1
	movb	$1, %bl
.LBB4_118:                              #   in Loop: Header=BB4_103 Depth=1
	movq	64(%rsp), %r14          # 8-byte Reload
	leaq	456(%rsp), %rax
	cmpq	%rax, %rbp
	je	.LBB4_120
# BB#119:                               #   in Loop: Header=BB4_103 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB4_120:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit145.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	184(%rsp), %rdi
	leaq	200(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_122
# BB#121:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_122:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit146.thread175.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_124
# BB#123:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_124:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit146.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_126
# BB#125:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_126:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit148.i
                                        #   in Loop: Header=BB4_103 Depth=1
	testb	%bl, %bl
	je	.LBB4_154
# BB#127:                               #   in Loop: Header=BB4_103 Depth=1
	movl	$.L.str.79, %esi
	movl	$g_options+184, %edx
	leaq	216(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp39:
	movl	$.L.str.80, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp40:
# BB#128:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp42:
	movl	$g_options+184, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp43:
# BB#129:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp45:
	movl	$.L.str.81, %edx
	leaq	312(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp46:
# BB#130:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp48:
	movl	$g_options+184, %edx
	leaq	344(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp49:
# BB#131:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp51:
	movl	$.L.str.82, %edx
	leaq	376(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp52:
# BB#132:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp54:
	movl	$g_options+184, %edx
	leaq	408(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp55:
# BB#133:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp57:
	movl	$.L.str.83, %edx
	leaq	152(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp58:
# BB#134:                               #   in Loop: Header=BB4_103 Depth=1
	movq	408(%rsp), %rdi
	leaq	424(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_136
# BB#135:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_136:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit149.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	376(%rsp), %rdi
	leaq	392(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_138
# BB#137:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_138:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit150.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	344(%rsp), %rdi
	leaq	360(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_140
# BB#139:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_140:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit151.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	312(%rsp), %rdi
	leaq	328(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_142
# BB#141:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_142:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit152.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	280(%rsp), %rdi
	leaq	296(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_144
# BB#143:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_144:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit153.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	248(%rsp), %rdi
	leaq	264(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_146
# BB#145:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_146:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit154.i
                                        #   in Loop: Header=BB4_103 Depth=1
	movq	216(%rsp), %rdi
	leaq	232(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_148
# BB#147:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
.LBB4_148:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit155.i
                                        #   in Loop: Header=BB4_103 Depth=1
.Ltmp60:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbp
.Ltmp61:
# BB#149:                               #   in Loop: Header=BB4_103 Depth=1
	movq	152(%rsp), %rdi
	movq	(%r14,%r13,8), %rsi
.Ltmp62:
	callq	_ZN2kc9Problem2SEPKcS1_
.Ltmp63:
# BB#150:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp64:
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
.Ltmp65:
# BB#151:                               #   in Loop: Header=BB4_103 Depth=1
.Ltmp66:
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.Ltmp67:
# BB#152:                               #   in Loop: Header=BB4_103 Depth=1
	movq	152(%rsp), %rdi
	leaq	168(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_158
# BB#153:                               #   in Loop: Header=BB4_103 Depth=1
	callq	_ZdlPv
	jmp	.LBB4_158
	.p2align	4, 0x90
.LBB4_154:                              #   in Loop: Header=BB4_103 Depth=1
	movq	(%r14,%r13,8), %rdi
	movl	$.L.str.84, %esi
	callq	fopen
	movq	%rax, yyin(%rip)
	testq	%rax, %rax
	je	.LBB4_156
# BB#155:                               #   in Loop: Header=BB4_103 Depth=1
	movq	%rax, %rdi
	callq	fclose
	jmp	.LBB4_158
.LBB4_156:                              #   in Loop: Header=BB4_103 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	(%r14,%r13,8), %rsi
	movl	$.L.str.85, %edi
.LBB4_157:                              #   in Loop: Header=BB4_103 Depth=1
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_158:                              #   in Loop: Header=BB4_103 Depth=1
	incq	%r13
	cmpq	72(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB4_103
.LBB4_159:                              # %._crit_edge.i
	cmpb	$0, gp_no_fatal_problems(%rip)
	je	.LBB4_464
# BB#160:
	movslq	optind(%rip), %rax
	movl	80(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	jge	.LBB4_164
# BB#161:
	leaq	(%r14,%rax,8), %rcx
	movq	%rcx, _ZL14inputfilenames(%rip)
	subl	%eax, %edx
	movl	%edx, _ZL13no_inputfiles(%rip)
	movl	$0, _ZL17current_inputfile(%rip)
	movq	(%r14,%rax,8), %rdi
	callq	_ZN2kcL16make_pg_filenameEPKc
	movq	%rax, pg_filename(%rip)
	movq	_ZL14inputfilenames(%rip), %rax
	movslq	_ZL17current_inputfile(%rip), %rcx
	movq	(%rax,%rcx,8), %rdi
	callq	_ZN2kcL8openfileEPKcS1_
	movq	%rax, yyin(%rip)
	testq	%rax, %rax
	jne	.LBB4_163
# BB#162:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	_ZL14inputfilenames(%rip), %rax
	movslq	_ZL17current_inputfile(%rip), %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.85, %edi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_163:
	movl	$1, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
.LBB4_164:                              # %_ZN2kcL11processargsEiPPc.exit
	movl	$2, %edi
	movl	$cleanup_and_die, %esi
	callq	signal
	movl	$15, %edi
	movl	$cleanup_and_die, %esi
	callq	signal
	movl	$11, %edi
	movl	$cleanup_and_abort, %esi
	callq	signal
	movl	$4, %edi
	movl	$cleanup_and_abort, %esi
	callq	signal
	movl	$31, %edi
	movl	$cleanup_and_abort, %esi
	callq	signal
	movl	$6, %edi
	movl	$cleanup_and_abort, %esi
	callq	signal
	movl	$1, %edi
	movl	$cleanup_and_die, %esi
	callq	signal
	movl	$3, %edi
	movl	$cleanup_and_die, %esi
	callq	signal
	movl	$7, %edi
	movl	$cleanup_and_abort, %esi
	callq	signal
	movl	$1, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_166
# BB#165:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.6, %esi
	movl	$23, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	callq	_ZNSo5flushEv
.LBB4_166:
	movl	$.L.str.94, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %r12
	movl	$.L.str.95, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$.L.str.96, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$.L.str.97, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$.L.str.98, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$.L.str.100, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc9f_emptyIdEv
	movq	%rax, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, %rbp
	callq	_ZN2kc20EmptyproductionblockEv
	movq	%rax, %r13
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, The_abstract_phylum_decl(%rip)
	movl	$.L.str.101, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc9f_emptyIdEv
	movq	%rax, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, %rbp
	callq	_ZN2kc20EmptyproductionblockEv
	movq	%rax, %r13
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, The_abstract_phylum_ref_decl(%rip)
	movl	$.L.str.102, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc9f_emptyIdEv
	movq	%rax, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, %rbp
	callq	_ZN2kc20EmptyproductionblockEv
	movq	%rax, %r13
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, The_abstract_list_decl(%rip)
	callq	_ZN2kc21NilphylumdeclarationsEv
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$.L.str.103, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	movl	$.L.str.104, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc12NilargumentsEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, 112(%rsp)         # 8-byte Spill
	callq	_ZN2kc15NilalternativesEv
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %r13
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r15, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movl	$.L.str.105, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r13
	movl	$.L.str.106, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc12NilargumentsEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, 104(%rsp)         # 8-byte Spill
	callq	_ZN2kc15NilalternativesEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %rbp
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %r13
	callq	_ZN2kc9NilCtextsEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r15, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbp
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbp, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movl	$.L.str.107, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r13
	movl	$.L.str.108, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	callq	_ZN2kc12NilargumentsEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, 96(%rsp)          # 8-byte Spill
	callq	_ZN2kc15NilalternativesEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %rbp
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %r13
	callq	_ZN2kc9NilCtextsEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%r15, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbp
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbp, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movl	$.L.str.109, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbp
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	movl	$.L.str.110, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	callq	_ZN2kc12NilargumentsEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, 88(%rsp)          # 8-byte Spill
	callq	_ZN2kc15NilalternativesEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %r14
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%rbp, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movl	$.L.str.111, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbp
	movl	$.L.str.99, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	movl	$.L.str.112, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	callq	_ZN2kc12NilargumentsEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, 120(%rsp)         # 8-byte Spill
	callq	_ZN2kc15NilalternativesEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %r14
	callq	_ZN2kc13NilattributesEv
	movq	%rax, %rbx
	callq	_ZN2kc9NilCtextsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%rbp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r15, %rdi
	movq	128(%rsp), %r14         # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, %rdi
	movq	136(%rsp), %rsi         # 8-byte Reload
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	%rax, Thephylumdeclarations(%rip)
	callq	_ZN2kc17NilrwdeclarationsEv
	movq	%rax, Therwdeclarations(%rip)
	callq	_ZN2kc22NilunparsedeclarationsEv
	movq	%rax, Theunparsedeclarations(%rip)
	callq	_ZN2kc14NilargsnumbersEv
	movq	%rax, Theargsnumbers(%rip)
	callq	_ZN2kc17NilfndeclarationsEv
	movq	%rax, Thefndeclarations(%rip)
	callq	_ZN2kc16NillanguagenamesEv
	movq	%rax, Thelanguages(%rip)
	callq	_ZN2kc25Nilbaseclass_declarationsEv
	movq	%rax, Thebaseclasses(%rip)
	movl	$0, 8(%r12)
	movl	$1, 8(%r14)
	movl	$.L.str.17, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$0, 8(%rax)
	movl	$.L.str.18, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$1, 8(%rax)
	movl	$.L.str.38, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$0, 8(%rax)
	movl	$.L.str.39, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$1, 8(%rax)
	movl	$.L.str.24, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$0, 8(%rax)
	movl	$.L.str.25, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$1, 8(%rax)
	movl	$.L.str.31, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$0, 8(%rax)
	movl	$.L.str.32, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$1, 8(%rax)
	callq	_ZN2kc17ITPredefinedUViewEv
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	callq	_ZN2kc17ITPredefinedRViewEv
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	callq	_ZN2kc24ITPredefinedStorageClassEv
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10v_defoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	callq	_ZN2kc12NilviewnamesEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, Theuviewnames(%rip)
	callq	_ZN2kc12NilviewnamesEv
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, Therviewnames(%rip)
	callq	_ZN2kc17NilstorageclassesEv
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc18ConsstorageclassesEPNS_7impl_IDEPNS_19impl_storageclassesE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsstorageclassesEPNS_7impl_IDEPNS_19impl_storageclassesE
	movq	%rax, Thestorageclasses(%rip)
	leaq	16(%r12), %rbp
	movq	%r12, %r13
	addq	$24, %r13
	leaq	16(%r14), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r14, %r15
	addq	$24, %r15
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	jmp	.LBB4_168
	.p2align	4, 0x90
.LBB4_167:                              # %_ZN2kcL21prepare_for_next_fileEv.exit.i
                                        #   in Loop: Header=BB4_168 Depth=1
	movq	%rax, %rdi
	callq	_Z9yyrestartP8_IO_FILE
.LBB4_168:                              # =>This Inner Loop Header: Depth=1
	movl	$.L.str.45, %edi
	callq	_ZN2kcL17mkfunctionincnameEPKc
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$1, 8(%rax)
	movl	$.L.str.44, %edi
	callq	_ZN2kcL17mkfunctionincnameEPKc
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movl	$0, 8(%rax)
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_173
# BB#169:                               #   in Loop: Header=BB4_168 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str.15, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	pg_filename(%rip), %rax
	movq	8(%rax), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rbx
	cmoveq	%rbp, %rbx
	testq	%rbx, %rbx
	je	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_168 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB4_172
	.p2align	4, 0x90
.LBB4_171:                              #   in Loop: Header=BB4_168 Depth=1
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB4_172:                              # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit.i
                                        #   in Loop: Header=BB4_168 Depth=1
	movl	$_ZSt4cout, %edi
	callq	_ZNSo5flushEv
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB4_173:                              #   in Loop: Header=BB4_168 Depth=1
.Ltmp69:
	callq	_Z7yyparsev
.Ltmp70:
# BB#174:                               #   in Loop: Header=BB4_168 Depth=1
	movq	Thefndeclarations(%rip), %rbx
	movq	pg_filename(%rip), %rdi
.Ltmp71:
	callq	_ZN2kc6FnFileEPNS_20impl_casestring__StrE
.Ltmp72:
# BB#175:                               #   in Loop: Header=BB4_168 Depth=1
	movq	%rbx, 8(%rax)
	cmpl	$0, 8(%r12)
	movq	%r13, %rax
	cmoveq	%rbp, %rax
	movq	(%rax), %rbx
	movq	pg_filename(%rip), %rdi
.Ltmp73:
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
.Ltmp74:
# BB#176:                               #   in Loop: Header=BB4_168 Depth=1
	movq	%rbx, 16(%rax)
	cmpl	$0, 8(%r14)
	movq	%r15, %rax
	cmoveq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	(%rax), %rbx
	movq	pg_filename(%rip), %rdi
.Ltmp75:
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
.Ltmp76:
# BB#177:                               #   in Loop: Header=BB4_168 Depth=1
	movq	%rbx, 24(%rax)
	movq	pg_filename(%rip), %rdi
.Ltmp77:
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
.Ltmp78:
# BB#178:                               #   in Loop: Header=BB4_168 Depth=1
	movl	$3, 8(%rax)
.Ltmp79:
	callq	_ZN2kc22NilincludedeclarationsEv
.Ltmp80:
# BB#179:                               #   in Loop: Header=BB4_168 Depth=1
	cmpl	$0, 8(%r12)
	movq	%r13, %rcx
	cmoveq	%rbp, %rcx
	movq	%rax, (%rcx)
.Ltmp81:
	callq	_ZN2kc22NilincludedeclarationsEv
.Ltmp82:
# BB#180:                               #   in Loop: Header=BB4_168 Depth=1
	cmpl	$0, 8(%r14)
	movq	%r15, %rcx
	cmoveq	64(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rax, (%rcx)
.LBB4_181:                              #   in Loop: Header=BB4_168 Depth=1
	callq	_ZN2kc17NilfndeclarationsEv
	movq	%rax, Thefndeclarations(%rip)
	movslq	_ZL17current_inputfile(%rip), %rax
	movl	_ZL13no_inputfiles(%rip), %ecx
	decl	%ecx
	cmpl	%ecx, %eax
	jge	.LBB4_186
# BB#182:                               #   in Loop: Header=BB4_168 Depth=1
	leaq	1(%rax), %rcx
	movl	%ecx, _ZL17current_inputfile(%rip)
	movq	_ZL14inputfilenames(%rip), %rcx
	movq	8(%rcx,%rax,8), %rdi
	callq	_ZN2kcL16make_pg_filenameEPKc
	movq	%rax, pg_filename(%rip)
	movl	$1, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
	movq	yyin(%rip), %rdi
	callq	fclose
	movq	_ZL14inputfilenames(%rip), %rax
	movslq	_ZL17current_inputfile(%rip), %rcx
	movq	(%rax,%rcx,8), %rdi
	callq	_ZN2kcL8openfileEPKcS1_
	movq	%rax, yyin(%rip)
	testq	%rax, %rax
	jne	.LBB4_167
# BB#183:                               #   in Loop: Header=BB4_168 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	_ZL14inputfilenames(%rip), %rax
	movslq	_ZL17current_inputfile(%rip), %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.85, %edi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movq	yyin(%rip), %rax
	jmp	.LBB4_167
.LBB4_184:                              #   in Loop: Header=BB4_168 Depth=1
.Ltmp83:
	cmpl	$1, %edx
	jne	.LBB4_467
# BB#185:                               #   in Loop: Header=BB4_168 Depth=1
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movq	72(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_181
.LBB4_186:
	callq	_Z9do_NORMALv
	cmpl	$0, _ZL13no_inputfiles(%rip)
	jle	.LBB4_188
# BB#187:
	movq	yyin(%rip), %rdi
	callq	fclose
.LBB4_188:
	cmpb	$0, gp_no_fatal_problems(%rip)
	je	.LBB4_465
# BB#189:                               # %_ZN2kcL8do_parseEv.exit
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_192
# BB#190:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.7, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpb	$0, g_options+65(%rip)
	movl	$0, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
	movq	$0, Thebindingidmarks(%rip)
	je	.LBB4_193
# BB#191:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.8, %esi
	movl	$16, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB4_193
.LBB4_192:                              # %.thread
	movl	$0, pg_lineno(%rip)
	movl	$0, pg_column(%rip)
	movl	$0, pg_charpos(%rip)
	movq	$0, Thebindingidmarks(%rip)
.LBB4_193:
	movq	Thephylumdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc16view_check_countE, %edx
	callq	*72(%rax)
	movq	Thephylumdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc10view_checkE, %edx
	callq	*72(%rax)
	movq	Therwdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc10view_checkE, %edx
	callq	*72(%rax)
	movq	Thefnfiles(%rip), %rbx
	jmp	.LBB4_195
	.p2align	4, 0x90
.LBB4_194:                              # %.lr.ph466
                                        #   in Loop: Header=BB4_195 Depth=1
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc10view_checkE, %edx
	callq	*72(%rax)
	movq	16(%rbx), %rbx
.LBB4_195:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$105, %eax
	je	.LBB4_194
# BB#196:                               # %.preheader420
	movq	Thefnfiles(%rip), %rbx
	jmp	.LBB4_198
	.p2align	4, 0x90
.LBB4_197:                              # %.lr.ph461
                                        #   in Loop: Header=BB4_198 Depth=1
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	callq	_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE
	movq	16(%rbx), %rbx
.LBB4_198:                              # %.lr.ph461
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$105, %eax
	je	.LBB4_197
# BB#199:                               # %._crit_edge462
	movq	Thebaseclasses(%rip), %rdi
	callq	_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE
	movq	Theunparsedeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc10view_checkE, %edx
	callq	*72(%rax)
	movq	Theunparsedeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc20view_check_viewnamesE, %edx
	callq	*72(%rax)
	movq	Thephylumdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc15view_check_uniqE, %edx
	callq	*72(%rax)
	movq	Theunparsedeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc23view_checklanguagenamesE, %edx
	callq	*72(%rax)
	movq	Thephylumdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc21view_make_patternrepsE, %edx
	callq	*72(%rax)
	movq	Therwdeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc21view_make_patternrepsE, %edx
	callq	*72(%rax)
	movq	Thefnfiles(%rip), %rbx
	jmp	.LBB4_201
	.p2align	4, 0x90
.LBB4_200:                              # %.lr.ph457
                                        #   in Loop: Header=BB4_201 Depth=1
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc21view_make_patternrepsE, %edx
	callq	*72(%rax)
	movq	16(%rbx), %rbx
.LBB4_201:                              # %.lr.ph457
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$105, %eax
	je	.LBB4_200
# BB#202:                               # %._crit_edge458
	movq	Theunparsedeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc21view_make_patternrepsE, %edx
	callq	*72(%rax)
	movq	Thephylumdeclarations(%rip), %r14
	jmp	.LBB4_204
	.p2align	4, 0x90
.LBB4_203:                              # %.loopexit
                                        #   in Loop: Header=BB4_204 Depth=1
	movq	16(%r14), %r14
.LBB4_204:                              # %._crit_edge458
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_223 Depth 2
                                        #       Child Loop BB4_226 Depth 3
                                        #       Child Loop BB4_231 Depth 3
                                        #     Child Loop BB4_209 Depth 2
                                        #       Child Loop BB4_212 Depth 3
                                        #       Child Loop BB4_217 Depth 3
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$12, %eax
	jne	.LBB4_234
# BB#205:                               # %.lr.ph452
                                        #   in Loop: Header=BB4_204 Depth=1
	movq	8(%r14), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB4_203
# BB#206:                               #   in Loop: Header=BB4_204 Depth=1
	movq	48(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB4_220
# BB#207:                               #   in Loop: Header=BB4_204 Depth=1
	movq	8(%rbx), %r12
	jmp	.LBB4_209
	.p2align	4, 0x90
.LBB4_208:                              # %._crit_edge444
                                        #   in Loop: Header=BB4_209 Depth=2
	movq	16(%r12), %r12
.LBB4_209:                              #   Parent Loop BB4_204 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_212 Depth 3
                                        #       Child Loop BB4_217 Depth 3
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB4_203
# BB#210:                               # %.lr.ph448
                                        #   in Loop: Header=BB4_209 Depth=2
	movq	8(%r12), %r15
	movq	Theuviewnames(%rip), %rsi
	movq	%r15, %rdi
	callq	_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	jmp	.LBB4_212
	.p2align	4, 0x90
.LBB4_211:                              #   in Loop: Header=BB4_212 Depth=3
	movq	16(%rbx), %rbx
.LBB4_212:                              # %.lr.ph448
                                        #   Parent Loop BB4_204 Depth=1
                                        #     Parent Loop BB4_209 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$227, %eax
	jne	.LBB4_215
# BB#213:                               # %.lr.ph439
                                        #   in Loop: Header=BB4_212 Depth=3
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$228, %eax
	jne	.LBB4_211
# BB#214:                               #   in Loop: Header=BB4_212 Depth=3
	movq	16(%rbp), %rdi
	callq	_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE
	jmp	.LBB4_211
	.p2align	4, 0x90
.LBB4_215:                              # %._crit_edge440
                                        #   in Loop: Header=BB4_209 Depth=2
	movq	Therviewnames(%rip), %rsi
	movq	%r15, %rdi
	callq	_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	jmp	.LBB4_217
	.p2align	4, 0x90
.LBB4_216:                              #   in Loop: Header=BB4_217 Depth=3
	movq	16(%rbx), %rbx
.LBB4_217:                              # %._crit_edge440
                                        #   Parent Loop BB4_204 Depth=1
                                        #     Parent Loop BB4_209 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$224, %eax
	jne	.LBB4_208
# BB#218:                               # %.lr.ph443
                                        #   in Loop: Header=BB4_217 Depth=3
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$225, %eax
	jne	.LBB4_216
# BB#219:                               #   in Loop: Header=BB4_217 Depth=3
	movq	16(%rbp), %rdi
	callq	_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE
	jmp	.LBB4_216
	.p2align	4, 0x90
.LBB4_220:                              #   in Loop: Header=BB4_204 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB4_203
# BB#221:                               #   in Loop: Header=BB4_204 Depth=1
	movq	8(%rbx), %r12
	jmp	.LBB4_223
	.p2align	4, 0x90
.LBB4_222:                              # %._crit_edge432
                                        #   in Loop: Header=BB4_223 Depth=2
	movq	16(%r12), %r12
.LBB4_223:                              #   Parent Loop BB4_204 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_226 Depth 3
                                        #       Child Loop BB4_231 Depth 3
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB4_203
# BB#224:                               # %.lr.ph436
                                        #   in Loop: Header=BB4_223 Depth=2
	movq	8(%r12), %r15
	movq	Theuviewnames(%rip), %rsi
	movq	%r15, %rdi
	callq	_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	jmp	.LBB4_226
	.p2align	4, 0x90
.LBB4_225:                              #   in Loop: Header=BB4_226 Depth=3
	movq	16(%rbx), %rbx
.LBB4_226:                              # %.lr.ph436
                                        #   Parent Loop BB4_204 Depth=1
                                        #     Parent Loop BB4_223 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$227, %eax
	jne	.LBB4_229
# BB#227:                               # %.lr.ph427
                                        #   in Loop: Header=BB4_226 Depth=3
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$228, %eax
	jne	.LBB4_225
# BB#228:                               #   in Loop: Header=BB4_226 Depth=3
	movq	16(%rbp), %rdi
	callq	_ZN2kc22check_unparse_patternsEPNS_21impl_unparsedeclsinfoE
	jmp	.LBB4_225
	.p2align	4, 0x90
.LBB4_229:                              # %._crit_edge428
                                        #   in Loop: Header=BB4_223 Depth=2
	movq	Therviewnames(%rip), %rsi
	movq	%r15, %rdi
	callq	_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	jmp	.LBB4_231
	.p2align	4, 0x90
.LBB4_230:                              #   in Loop: Header=BB4_231 Depth=3
	movq	16(%rbx), %rbx
.LBB4_231:                              # %._crit_edge428
                                        #   Parent Loop BB4_204 Depth=1
                                        #     Parent Loop BB4_223 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$224, %eax
	jne	.LBB4_222
# BB#232:                               # %.lr.ph431
                                        #   in Loop: Header=BB4_231 Depth=3
	movq	8(%rbx), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$225, %eax
	jne	.LBB4_230
# BB#233:                               #   in Loop: Header=BB4_231 Depth=3
	movq	16(%rbp), %rdi
	callq	_ZN2kc22check_rewrite_patternsEPNS_21impl_rewriterulesinfoE
	jmp	.LBB4_230
.LBB4_234:                              # %._crit_edge453
	cmpb	$0, pg_languageshavebeendefined(%rip)
	je	.LBB4_236
# BB#235:
	callq	_ZN2kc15collect_stringsEv
.LBB4_236:
	cmpb	$0, gp_no_fatal_problems(%rip)
	leaq	16(%rsp), %rbp
	je	.LBB4_466
# BB#237:
	movq	Thephylumdeclarations(%rip), %rdi
	callq	_ZN2kc18PhylumDeclarationsEPNS_23impl_phylumdeclarationsE
	movq	%rax, %r14
	movq	Theargsnumbers(%rip), %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	testl	%eax, %eax
	jne	.LBB4_239
# BB#238:
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	Theargsnumbers(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsargsnumbersEPNS_17impl_integer__IntEPNS_16impl_argsnumbersE
	movq	%rax, Theargsnumbers(%rip)
.LBB4_239:
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_241
# BB#240:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.9, %esi
	movl	$24, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	callq	_ZNSo5flushEv
.LBB4_241:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.12, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp84:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp85:
# BB#242:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_244
# BB#243:
	callq	_ZdlPv
.LBB4_244:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.14, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp87:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp88:
# BB#245:
.Ltmp90:
	movq	%rsp, %rcx
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.13, %esi
	movl	$.L.str.11, %edx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp91:
# BB#246:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_248
# BB#247:
	callq	_ZdlPv
.LBB4_248:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit290
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %r15
	cmpq	%r15, %rdi
	je	.LBB4_250
# BB#249:
	callq	_ZdlPv
.LBB4_250:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit291
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_258
# BB#251:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp93:
	movq	%rsp, %rdi
	movl	$.L.str.16, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp94:
# BB#252:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp96:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp97:
# BB#253:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit
.Ltmp98:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp99:
# BB#254:                               # %_ZNSolsEPFRSoS_E.exit
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_256
# BB#255:
	callq	_ZdlPv
.LBB4_256:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit292
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_258
# BB#257:
	callq	_ZdlPv
.LBB4_258:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc12view_gen_k_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc12view_gen_k_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_gen_enumphyla_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc24view_gen_enumoperators_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc22view_gen_classdecls1_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_nodetypedefs_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc27view_gen_operatormap_type_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc24view_gen_subphylumdefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc28view_gen_set_subphylumdefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc26view_gen_copy_attributes_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_gen_phylummap_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_gen_operatormap_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	Thestorageclasses(%rip), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc18view_gen_uniqmap_cE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_gen_nodetypes_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc24view_gen_noofoperators_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.17, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.17, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movl	$.L.str.18, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.18, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_assertmacros_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc23view_gen_assertmacros_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc24view_gen_operatordecls_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc22view_gen_classdecls2_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_gen_classdefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc16view_gen_alloc_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc16view_gen_alloc_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc21view_gen_hashtables_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc23view_gen_operatordefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc21view_gen_hashtables_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc22view_gen_error_decls_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc21view_gen_error_defs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	cmpb	$0, g_options+69(%rip)
	jne	.LBB4_260
# BB#259:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc24view_gen_printdotdecls_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc23view_gen_printdotdefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
.LBB4_260:
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_gen_listdefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_gen_copydefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc16view_gen_end_k_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_263
# BB#261:                               # %_ZN14kc_filePrinter6fcloseEv.exit
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_264
# BB#262:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.19, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_264
.LBB4_263:                              # %_ZN14kc_filePrinter6fcloseEv.exit.thread
	movq	$0, v_ccfile_printer+8(%rip)
.LBB4_264:
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_267
# BB#265:                               # %_ZN14kc_filePrinter6fcloseEv.exit300
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_268
# BB#266:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.20, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_268
.LBB4_267:                              # %_ZN14kc_filePrinter6fcloseEv.exit300.thread
	movq	$0, v_hfile_printer+8(%rip)
.LBB4_268:
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.14, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp101:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp102:
# BB#269:
	movq	(%rsp), %rsi
.Ltmp104:
	movl	$.L.str.13, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp105:
# BB#270:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_272
# BB#271:
	callq	_ZdlPv
.LBB4_272:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit301
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_274
# BB#273:
	callq	_ZdlPv
.LBB4_274:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit302
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.12, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp107:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp108:
# BB#275:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_277
# BB#276:
	callq	_ZdlPv
.LBB4_277:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit303
	cmpb	$0, g_options+66(%rip)
	jne	.LBB4_312
# BB#278:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.21, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp110:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp111:
# BB#279:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_281
# BB#280:
	callq	_ZdlPv
.LBB4_281:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit304
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.22, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp113:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp114:
# BB#282:
.Ltmp116:
	movq	%rsp, %rcx
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.13, %esi
	movl	$.L.str.11, %edx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp117:
# BB#283:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_285
# BB#284:
	callq	_ZdlPv
.LBB4_285:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit305
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_287
# BB#286:
	callq	_ZdlPv
.LBB4_287:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit306
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_295
# BB#288:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp119:
	movq	%rsp, %rdi
	movl	$.L.str.23, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp120:
# BB#289:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp122:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp123:
# BB#290:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit307
.Ltmp124:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp125:
# BB#291:                               # %_ZNSolsEPFRSoS_E.exit308
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_293
# BB#292:
	callq	_ZdlPv
.LBB4_293:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit310
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_295
# BB#294:
	callq	_ZdlPv
.LBB4_295:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc22view_gen_csgio_start_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.24, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.24, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc16view_gen_csgio_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_gen_csgio_end_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_gen_csgio_start_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.25, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.25, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc16view_gen_csgio_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_298
# BB#296:                               # %_ZN14kc_filePrinter6fcloseEv.exit321
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_299
# BB#297:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.26, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_299
.LBB4_298:                              # %_ZN14kc_filePrinter6fcloseEv.exit321.thread
	movq	$0, v_ccfile_printer+8(%rip)
.LBB4_299:
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_302
# BB#300:                               # %_ZN14kc_filePrinter6fcloseEv.exit323
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_303
# BB#301:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.27, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_303
.LBB4_302:                              # %_ZN14kc_filePrinter6fcloseEv.exit323.thread
	movq	$0, v_hfile_printer+8(%rip)
.LBB4_303:
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.22, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp127:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp128:
# BB#304:
	movq	(%rsp), %rsi
.Ltmp130:
	movl	$.L.str.13, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp131:
# BB#305:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_307
# BB#306:
	callq	_ZdlPv
.LBB4_307:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit324
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_309
# BB#308:
	callq	_ZdlPv
.LBB4_309:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit325
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.21, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp133:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp134:
# BB#310:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_312
# BB#311:
	callq	_ZdlPv
.LBB4_312:
	cmpb	$0, g_options+67(%rip)
	jne	.LBB4_349
# BB#313:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.28, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp136:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp137:
# BB#314:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_316
# BB#315:
	callq	_ZdlPv
.LBB4_316:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit330
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.29, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp139:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp140:
# BB#317:
.Ltmp142:
	movq	%rsp, %rcx
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.13, %esi
	movl	$.L.str.11, %edx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp143:
# BB#318:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_320
# BB#319:
	callq	_ZdlPv
.LBB4_320:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit331
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_322
# BB#321:
	callq	_ZdlPv
.LBB4_322:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit332
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_330
# BB#323:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp145:
	movq	%rsp, %rdi
	movl	$.L.str.30, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp146:
# BB#324:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp148:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp149:
# BB#325:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit333
.Ltmp150:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp151:
# BB#326:                               # %_ZNSolsEPFRSoS_E.exit334
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_328
# BB#327:
	callq	_ZdlPv
.LBB4_328:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit336
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_330
# BB#329:
	callq	_ZdlPv
.LBB4_330:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc15view_gen_unpk_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.31, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.31, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc21view_uview_class_declE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_unparsedecls_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_gen_end_unpk_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc15view_gen_unpk_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.32, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.32, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc29view_gen_default_types_unpk_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_gen_unparsedefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	cmpb	$0, pg_languageshavebeendefined(%rip)
	je	.LBB4_332
# BB#331:
	callq	_ZN2kc25unparse_string_collectionEv
.LBB4_332:
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_335
# BB#333:                               # %_ZN14kc_filePrinter6fcloseEv.exit344
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_336
# BB#334:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.33, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_336
.LBB4_335:                              # %_ZN14kc_filePrinter6fcloseEv.exit344.thread
	movq	$0, v_ccfile_printer+8(%rip)
.LBB4_336:
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_339
# BB#337:                               # %_ZN14kc_filePrinter6fcloseEv.exit346
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_340
# BB#338:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.34, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_340
.LBB4_339:                              # %_ZN14kc_filePrinter6fcloseEv.exit346.thread
	movq	$0, v_hfile_printer+8(%rip)
.LBB4_340:
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.29, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp153:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp154:
# BB#341:
	movq	(%rsp), %rsi
.Ltmp156:
	movl	$.L.str.13, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp157:
# BB#342:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_344
# BB#343:
	callq	_ZdlPv
.LBB4_344:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit347
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_346
# BB#345:
	callq	_ZdlPv
.LBB4_346:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit348
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.28, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp159:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp160:
# BB#347:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_349
# BB#348:
	callq	_ZdlPv
.LBB4_349:
	cmpb	$0, g_options+68(%rip)
	jne	.LBB4_384
# BB#350:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.35, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp162:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp163:
# BB#351:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_353
# BB#352:
	callq	_ZdlPv
.LBB4_353:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit353
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.36, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp165:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp166:
# BB#354:
.Ltmp168:
	movq	%rsp, %rcx
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.13, %esi
	movl	$.L.str.11, %edx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp169:
# BB#355:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_357
# BB#356:
	callq	_ZdlPv
.LBB4_357:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit354
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_359
# BB#358:
	callq	_ZdlPv
.LBB4_359:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit355
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_367
# BB#360:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp171:
	movq	%rsp, %rdi
	movl	$.L.str.37, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp172:
# BB#361:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp174:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp175:
# BB#362:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit356
.Ltmp176:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp177:
# BB#363:                               # %_ZNSolsEPFRSoS_E.exit357
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_365
# BB#364:
	callq	_ZdlPv
.LBB4_365:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit359
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_367
# BB#366:
	callq	_ZdlPv
.LBB4_367:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_gen_rewritek_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.38, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.38, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc21view_rview_class_declE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_rewritedecls_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_end_rewritek_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_gen_rewritek_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	$.L.str.39, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %rbx
	movl	$.L.str.39, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	leaq	16(%rbx), %rcx
	addq	$24, %rbx
	cmpl	$0, 8(%rax)
	cmoveq	%rcx, %rbx
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_gen_rewritedefs_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_370
# BB#368:                               # %_ZN14kc_filePrinter6fcloseEv.exit367
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_371
# BB#369:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.40, %edi
	movl	$.L.str.13, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_371
.LBB4_370:                              # %_ZN14kc_filePrinter6fcloseEv.exit367.thread
	movq	$0, v_ccfile_printer+8(%rip)
.LBB4_371:
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_374
# BB#372:                               # %_ZN14kc_filePrinter6fcloseEv.exit369
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_375
# BB#373:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.41, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_375
.LBB4_374:                              # %_ZN14kc_filePrinter6fcloseEv.exit369.thread
	movq	$0, v_hfile_printer+8(%rip)
.LBB4_375:
	leaq	32(%rsp), %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.36, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp179:
	movq	%rsp, %rdi
	movl	$g_options+216, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp180:
# BB#376:
	movq	(%rsp), %rsi
.Ltmp182:
	movl	$.L.str.13, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp183:
# BB#377:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_379
# BB#378:
	callq	_ZdlPv
.LBB4_379:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit370
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_381
# BB#380:
	callq	_ZdlPv
.LBB4_381:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit371
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.35, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp185:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp186:
# BB#382:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_384
# BB#383:
	callq	_ZdlPv
.LBB4_384:                              # %.preheader
	movq	Thefnfiles(%rip), %r13
	jmp	.LBB4_386
	.p2align	4, 0x90
.LBB4_385:                              #   in Loop: Header=BB4_386 Depth=1
	movq	g_options+32(%rip), %rsi
	movl	$.L.str.13, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	g_options(%rip), %rsi
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	16(%r13), %r13
	jmp	.LBB4_386
.LBB4_401:                              # %_ZN14kc_filePrinter6fcloseEv.exit379.thread
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	$0, v_hfile_printer+8(%rip)
	jmp	.LBB4_385
	.p2align	4, 0x90
.LBB4_386:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$105, %eax
	jne	.LBB4_402
# BB#387:                               # %.lr.ph
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	8(%r13), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$106, %eax
	jne	.LBB4_391
# BB#388:                               #   in Loop: Header=BB4_386 Depth=1
	movq	16(%r12), %r15
	movl	$.L.str.42, %esi
	movq	%r15, %rdi
	callq	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	movq	%rax, %rbx
	movq	g_options+8(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
	movl	$g_options+216, %esi
	movq	%r15, %rdi
	callq	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%rax, %rbx
	movq	g_options+40(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movl	$g_options+32, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
	movq	%r15, pg_filename(%rip)
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movl	$g_options, %ecx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.13, %esi
	movl	$.L.str.11, %edx
	movl	$g_options+32, %ecx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_394
# BB#389:                               #   in Loop: Header=BB4_386 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str.15, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.43, %esi
	movq	%r15, %rdi
	callq	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_392
# BB#390:                               #   in Loop: Header=BB4_386 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB4_393
	.p2align	4, 0x90
.LBB4_391:                              #   in Loop: Header=BB4_386 Depth=1
	movl	$.L.str.48, %edi
	movl	$1165, %esi             # imm = 0x48D
	movl	$.L.str.49, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	movq	16(%r13), %r13
	jmp	.LBB4_386
.LBB4_392:                              #   in Loop: Header=BB4_386 Depth=1
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB4_393:                              # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
                                        #   in Loop: Header=BB4_386 Depth=1
	movl	$_ZSt4cout, %edi
	callq	_ZNSo5flushEv
.LBB4_394:                              #   in Loop: Header=BB4_386 Depth=1
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_gen_fns_start_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	%r15, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movl	$.L.str.44, %edi
	callq	_ZN2kcL17mkfunctionincnameEPKc
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc14view_gen_fnk_hE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc18view_gen_fns_end_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_gen_fns_start_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc25view_gen_fns_owninclude_cE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	%r15, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movl	$.L.str.45, %edi
	callq	_ZN2kcL17mkfunctionincnameEPKc
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc17view_gen_includesE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_open_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc19view_gen_fnkdecls_cE, %edx
	callq	*72(%rax)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc14view_gen_fnk_cE, %edx
	callq	*72(%rax)
	movq	(%r14), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc20view_close_namespaceE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_ccfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_397
# BB#395:                               # %_ZN14kc_filePrinter6fcloseEv.exit377
                                        #   in Loop: Header=BB4_386 Depth=1
	callq	fclose
	movq	$0, v_ccfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_398
# BB#396:                               #   in Loop: Header=BB4_386 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	g_options+32(%rip), %rsi
	movl	$.L.str.46, %edi
	movl	$.L.str.47, %edx
	movl	$.L.str.13, %ecx
	callq	_ZN2kc9Problem4SEPKcS1_S1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_398
.LBB4_397:                              # %_ZN14kc_filePrinter6fcloseEv.exit377.thread
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	$0, v_ccfile_printer+8(%rip)
.LBB4_398:                              #   in Loop: Header=BB4_386 Depth=1
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_401
# BB#399:                               # %_ZN14kc_filePrinter6fcloseEv.exit379
                                        #   in Loop: Header=BB4_386 Depth=1
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_385
# BB#400:                               #   in Loop: Header=BB4_386 Depth=1
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	g_options(%rip), %rsi
	movl	$.L.str.46, %edi
	movl	$.L.str.47, %edx
	movl	$.L.str.10, %ecx
	callq	_ZN2kc9Problem4SEPKcS1_S1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB4_385
.LBB4_402:                              # %._crit_edge
	cmpb	$0, g_options+71(%rip)
	leaq	16(%rsp), %rbp
	leaq	48(%rsp), %r15
	je	.LBB4_421
# BB#403:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.50, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp188:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp189:
# BB#404:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_406
# BB#405:
	callq	_ZdlPv
.LBB4_406:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit380
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_414
# BB#407:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp191:
	movq	%rsp, %rdi
	movl	$.L.str.50, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp192:
# BB#408:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp194:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp195:
# BB#409:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit381
.Ltmp196:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp197:
# BB#410:                               # %_ZNSolsEPFRSoS_E.exit382
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_412
# BB#411:
	callq	_ZdlPv
.LBB4_412:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit384
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_414
# BB#413:
	callq	_ZdlPv
.LBB4_414:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc23view_gen_operatorcast_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_416
# BB#415:                               # %_ZN14kc_filePrinter6fcloseEv.exit390.thread
	movq	$0, v_hfile_printer+8(%rip)
	jmp	.LBB4_418
.LBB4_416:                              # %_ZN14kc_filePrinter6fcloseEv.exit390
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_418
# BB#417:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.51, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_418:
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.50, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp199:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp200:
# BB#419:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_421
# BB#420:
	callq	_ZdlPv
.LBB4_421:
	movl	$g_options+80, %edi
	movl	$.L.str.43, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
	testl	%eax, %eax
	je	.LBB4_440
# BB#422:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$g_options+80, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp202:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp203:
# BB#423:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_425
# BB#424:
	callq	_ZdlPv
.LBB4_425:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit393
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_433
# BB#426:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp205:
	movq	%rsp, %rdi
	movl	$g_options+80, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Ltmp206:
# BB#427:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp208:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp209:
# BB#428:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit394
.Ltmp210:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp211:
# BB#429:                               # %_ZNSolsEPFRSoS_E.exit395
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_431
# BB#430:
	callq	_ZdlPv
.LBB4_431:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit397
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_433
# BB#432:
	callq	_ZdlPv
.LBB4_433:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc24view_gen_yaccstacktype_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_435
# BB#434:                               # %_ZN14kc_filePrinter6fcloseEv.exit403.thread
	movq	$0, v_hfile_printer+8(%rip)
	jmp	.LBB4_437
.LBB4_435:                              # %_ZN14kc_filePrinter6fcloseEv.exit403
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_437
# BB#436:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movq	g_options+80(%rip), %rsi
	movl	$.L.str.46, %edi
	movl	$.L.str.47, %edx
	movl	$.L.str.10, %ecx
	callq	_ZN2kc9Problem4SEPKcS1_S1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_437:
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$g_options+80, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
	movq	(%rsp), %rsi
.Ltmp213:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp214:
# BB#438:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_440
# BB#439:
	callq	_ZdlPv
.LBB4_440:
	cmpb	$0, g_options+112(%rip)
	je	.LBB4_459
# BB#441:
	movq	%rsp, %rbx
	movl	$g_options+184, %esi
	movl	$.L.str.52, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp216:
	movl	$v_hfile_printer, %edi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	callq	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp217:
# BB#442:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_444
# BB#443:
	callq	_ZdlPv
.LBB4_444:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit406
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_452
# BB#445:
	leaq	32(%rsp), %rbx
	movl	$.L.str.15, %esi
	movl	$g_options+184, %edx
	movq	%rbx, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Ltmp219:
	movq	%rsp, %rdi
	movl	$.L.str.52, %edx
	movq	%rbx, %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp220:
# BB#446:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp222:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp223:
# BB#447:                               # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit407
.Ltmp224:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp225:
# BB#448:                               # %_ZNSolsEPFRSoS_E.exit408
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_450
# BB#449:
	callq	_ZdlPv
.LBB4_450:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit410
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB4_452
# BB#451:
	callq	_ZdlPv
.LBB4_452:
	movq	(%r14), %rax
	movl	$v_hfile_printer, %esi
	movl	$_ZN2kc20view_gen_yxx_union_hE, %edx
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	v_hfile_printer+8(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_454
# BB#453:                               # %_ZN14kc_filePrinter6fcloseEv.exit416.thread
	movq	$0, v_hfile_printer+8(%rip)
	jmp	.LBB4_456
.LBB4_454:                              # %_ZN14kc_filePrinter6fcloseEv.exit416
	callq	fclose
	movq	$0, v_hfile_printer+8(%rip)
	cmpl	$-1, %eax
	jne	.LBB4_456
# BB#455:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.53, %edi
	movl	$.L.str.10, %esi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB4_456:
	movq	%rsp, %rdi
	movl	$g_options+184, %esi
	movl	$.L.str.52, %edx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	movq	(%rsp), %rsi
.Ltmp227:
	movl	$.L.str.10, %edi
	callq	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp228:
# BB#457:
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_459
# BB#458:
	callq	_ZdlPv
.LBB4_459:
	cmpb	$0, g_options+65(%rip)
	je	.LBB4_461
# BB#460:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.7, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.LBB4_461:
	xorl	%edi, %edi
	callq	_ZN2kc5leaveEi
.LBB4_462:
	movq	g_progname(%rip), %rax
	movq	8(%rax), %rsi
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	_ZN2kc5leaveEi
.LBB4_463:
	movl	$.L.str.86, %edi
	movl	$_ZL15kimwitu_version, %esi
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	_ZN2kc5leaveEi
.LBB4_464:
	movl	$1, %edi
	callq	_ZN2kc5leaveEi
.LBB4_465:
	movl	$1, %edi
	callq	_ZN2kc5leaveEi
.LBB4_466:
	movl	$1, %edi
	callq	_ZN2kc5leaveEi
.LBB4_467:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_468:
.Ltmp173:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_469:
.Ltmp147:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_470:
.Ltmp121:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_471:
.Ltmp221:
	jmp	.LBB4_512
.LBB4_472:
.Ltmp207:
	jmp	.LBB4_512
.LBB4_473:
.Ltmp193:
	jmp	.LBB4_512
.LBB4_474:
.Ltmp187:
	jmp	.LBB4_515
.LBB4_475:
.Ltmp184:
	jmp	.LBB4_517
.LBB4_476:
.Ltmp181:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_477:
.Ltmp170:
	jmp	.LBB4_517
.LBB4_478:
.Ltmp167:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_479:
.Ltmp164:
	jmp	.LBB4_515
.LBB4_480:
.Ltmp161:
	jmp	.LBB4_515
.LBB4_481:
.Ltmp158:
	jmp	.LBB4_517
.LBB4_482:
.Ltmp155:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_483:
.Ltmp144:
	jmp	.LBB4_517
.LBB4_484:
.Ltmp141:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_485:
.Ltmp138:
	jmp	.LBB4_515
.LBB4_486:
.Ltmp135:
	jmp	.LBB4_515
.LBB4_487:
.Ltmp132:
	jmp	.LBB4_517
.LBB4_488:
.Ltmp129:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_489:
.Ltmp118:
	jmp	.LBB4_517
.LBB4_490:
.Ltmp115:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_491:
.Ltmp112:
	jmp	.LBB4_515
.LBB4_492:
.Ltmp178:
	jmp	.LBB4_517
.LBB4_493:
.Ltmp152:
	jmp	.LBB4_517
.LBB4_494:
.Ltmp126:
	jmp	.LBB4_517
.LBB4_495:
.Ltmp229:
	jmp	.LBB4_515
.LBB4_496:
.Ltmp226:
	jmp	.LBB4_509
.LBB4_497:
.Ltmp218:
	jmp	.LBB4_515
.LBB4_498:
.Ltmp215:
	jmp	.LBB4_515
.LBB4_499:
.Ltmp212:
	jmp	.LBB4_509
.LBB4_500:
.Ltmp204:
	jmp	.LBB4_515
.LBB4_501:
.Ltmp201:
	jmp	.LBB4_515
.LBB4_502:
.Ltmp198:
	jmp	.LBB4_509
.LBB4_503:
.Ltmp190:
	jmp	.LBB4_515
.LBB4_504:
.Ltmp95:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_505:
.Ltmp109:
	jmp	.LBB4_515
.LBB4_506:
.Ltmp106:
	jmp	.LBB4_517
.LBB4_507:
.Ltmp103:
	movq	%rax, %r14
	jmp	.LBB4_519
.LBB4_508:
.Ltmp92:
.LBB4_509:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_513
# BB#510:
	callq	_ZdlPv
	jmp	.LBB4_513
.LBB4_511:
.Ltmp89:
.LBB4_512:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit295
	movq	%rax, %r14
.LBB4_513:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit295
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %rax
	jmp	.LBB4_547
.LBB4_514:
.Ltmp86:
.LBB4_515:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	jne	.LBB4_562
	jmp	.LBB4_563
.LBB4_516:
.Ltmp100:
.LBB4_517:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB4_519
# BB#518:
	callq	_ZdlPv
.LBB4_519:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit297
	movq	32(%rsp), %rdi
	cmpq	%r15, %rdi
	jne	.LBB4_562
	jmp	.LBB4_563
.LBB4_520:
.Ltmp38:
	movq	%rax, %r14
	movq	184(%rsp), %rdi
	leaq	200(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_523
# BB#521:
	callq	_ZdlPv
	jmp	.LBB4_523
.LBB4_522:
.Ltmp35:
	movq	%rax, %r14
.LBB4_523:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit157.i
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_545
# BB#524:
	callq	_ZdlPv
	jmp	.LBB4_545
.LBB4_525:
.Ltmp56:
	movq	%rax, %r14
	jmp	.LBB4_532
.LBB4_526:
.Ltmp44:
	movq	%rax, %r14
	jmp	.LBB4_541
.LBB4_527:
.Ltmp53:
	movq	%rax, %r14
	jmp	.LBB4_534
.LBB4_528:
.Ltmp50:
	movq	%rax, %r14
	jmp	.LBB4_536
.LBB4_529:
.Ltmp41:
	movq	%rax, %r14
	jmp	.LBB4_543
.LBB4_530:
.Ltmp59:
	movq	%rax, %r14
	movq	408(%rsp), %rdi
	leaq	424(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_532
# BB#531:
	callq	_ZdlPv
.LBB4_532:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit160.i
	movq	376(%rsp), %rdi
	leaq	392(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_534
# BB#533:
	callq	_ZdlPv
.LBB4_534:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit161.i
	movq	344(%rsp), %rdi
	leaq	360(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_536
# BB#535:
	callq	_ZdlPv
.LBB4_536:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit162.i
	movq	312(%rsp), %rdi
	leaq	328(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_539
# BB#537:
	callq	_ZdlPv
	jmp	.LBB4_539
.LBB4_538:
.Ltmp47:
	movq	%rax, %r14
.LBB4_539:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit163.i
	movq	280(%rsp), %rdi
	leaq	296(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_541
# BB#540:
	callq	_ZdlPv
.LBB4_541:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit164.i
	movq	248(%rsp), %rdi
	leaq	264(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_543
# BB#542:
	callq	_ZdlPv
.LBB4_543:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit165.i
	movq	216(%rsp), %rdi
	leaq	232(%rsp), %rax
	jmp	.LBB4_547
.LBB4_544:
.Ltmp32:
	movq	%rax, %r14
.LBB4_545:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit158.i
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	jmp	.LBB4_547
.LBB4_546:
.Ltmp68:
	movq	%rax, %r14
	movq	152(%rsp), %rdi
	leaq	168(%rsp), %rax
.LBB4_547:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit158.i
	cmpq	%rax, %rdi
	jne	.LBB4_562
	jmp	.LBB4_563
.LBB4_548:
.Ltmp2:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB4_555
# BB#549:
	callq	_ZdlPv
	jmp	.LBB4_555
.LBB4_550:
.Ltmp5:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB4_555
# BB#551:
	callq	_ZdlPv
	jmp	.LBB4_555
.LBB4_552:
.Ltmp29:
	jmp	.LBB4_554
.LBB4_553:
.Ltmp26:
.LBB4_554:
	movq	%rax, %r14
.LBB4_555:
	movq	32(%rsp), %rbx
	movq	40(%rsp), %rbp
	cmpq	%rbp, %rbx
	je	.LBB4_560
	.p2align	4, 0x90
.LBB4_556:                              # %.lr.ph.i.i.i.i135.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.LBB4_558
# BB#557:                               #   in Loop: Header=BB4_556 Depth=1
	callq	_ZdlPv
.LBB4_558:                              # %_ZSt8_DestroyINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvPT_.exit.i.i.i.i136.i
                                        #   in Loop: Header=BB4_556 Depth=1
	addq	$16, %rbx
	cmpq	%rbp, %rbx
	jne	.LBB4_556
# BB#559:                               # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_EvT_S7_RSaIT0_E.exit.loopexit.i138.i
	movq	32(%rsp), %rbx
.LBB4_560:                              # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_EvT_S7_RSaIT0_E.exit.i139.i
	testq	%rbx, %rbx
	je	.LBB4_563
# BB#561:
	movq	%rbx, %rdi
.LBB4_562:
	callq	_ZdlPv
.LBB4_563:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_98
	.quad	.LBB4_6
	.quad	.LBB4_8
	.quad	.LBB4_46
	.quad	.LBB4_48
	.quad	.LBB4_16
	.quad	.LBB4_54
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_462
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_462
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_47
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_463
	.quad	.LBB4_22
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_58
	.quad	.LBB4_17
	.quad	.LBB4_53
	.quad	.LBB4_12
	.quad	.LBB4_11
	.quad	.LBB4_6
	.quad	.LBB4_462
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_6
	.quad	.LBB4_45
	.quad	.LBB4_14
	.quad	.LBB4_18
	.quad	.LBB4_55
	.quad	.LBB4_62
	.quad	.LBB4_43
	.quad	.LBB4_56
	.quad	.LBB4_10
	.quad	.LBB4_15
	.quad	.LBB4_9
	.quad	.LBB4_59
	.quad	.LBB4_64
	.quad	.LBB4_6
	.quad	.LBB4_57
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\336\211\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\323\t"                # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp25-.Ltmp6          #   Call between .Ltmp6 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp30-.Ltmp25         #   Call between .Ltmp25 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp39-.Ltmp37         #   Call between .Ltmp37 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp67-.Ltmp60         #   Call between .Ltmp60 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin0   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp69-.Ltmp67         #   Call between .Ltmp67 and .Ltmp69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp82-.Ltmp69         #   Call between .Ltmp69 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp84-.Ltmp82         #   Call between .Ltmp82 and .Ltmp84
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin0   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp87-.Ltmp85         #   Call between .Ltmp85 and .Ltmp87
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin0   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin0   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp93-.Ltmp91         #   Call between .Ltmp91 and .Ltmp93
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin0   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp99-.Ltmp96         #   Call between .Ltmp96 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin0  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp101-.Ltmp99        #   Call between .Ltmp99 and .Ltmp101
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin0  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin0  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp107-.Ltmp105       #   Call between .Ltmp105 and .Ltmp107
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp110-.Ltmp108       #   Call between .Ltmp108 and .Ltmp110
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin0  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp113-.Ltmp111       #   Call between .Ltmp111 and .Ltmp113
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin0  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin0  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp119-.Ltmp117       #   Call between .Ltmp117 and .Ltmp119
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin0  # >> Call Site 41 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin0  #     jumps to .Ltmp121
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin0  # >> Call Site 42 <<
	.long	.Ltmp125-.Ltmp122       #   Call between .Ltmp122 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin0  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin0  # >> Call Site 43 <<
	.long	.Ltmp127-.Ltmp125       #   Call between .Ltmp125 and .Ltmp127
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin0  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin0  # >> Call Site 45 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin0  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin0  # >> Call Site 46 <<
	.long	.Ltmp133-.Ltmp131       #   Call between .Ltmp131 and .Ltmp133
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin0  # >> Call Site 47 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin0  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin0  # >> Call Site 48 <<
	.long	.Ltmp136-.Ltmp134       #   Call between .Ltmp134 and .Ltmp136
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin0  # >> Call Site 49 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin0  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin0  # >> Call Site 50 <<
	.long	.Ltmp139-.Ltmp137       #   Call between .Ltmp137 and .Ltmp139
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin0  # >> Call Site 51 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin0  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin0  # >> Call Site 52 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin0  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin0  # >> Call Site 53 <<
	.long	.Ltmp145-.Ltmp143       #   Call between .Ltmp143 and .Ltmp145
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin0  # >> Call Site 54 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin0  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin0  # >> Call Site 55 <<
	.long	.Ltmp151-.Ltmp148       #   Call between .Ltmp148 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin0  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin0  # >> Call Site 56 <<
	.long	.Ltmp153-.Ltmp151       #   Call between .Ltmp151 and .Ltmp153
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin0  # >> Call Site 57 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin0  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin0  # >> Call Site 58 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin0  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin0  # >> Call Site 59 <<
	.long	.Ltmp159-.Ltmp157       #   Call between .Ltmp157 and .Ltmp159
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin0  # >> Call Site 60 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin0  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin0  # >> Call Site 61 <<
	.long	.Ltmp162-.Ltmp160       #   Call between .Ltmp160 and .Ltmp162
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin0  # >> Call Site 62 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin0  # >> Call Site 63 <<
	.long	.Ltmp165-.Ltmp163       #   Call between .Ltmp163 and .Ltmp165
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin0  # >> Call Site 64 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin0  # >> Call Site 65 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin0  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin0  # >> Call Site 66 <<
	.long	.Ltmp171-.Ltmp169       #   Call between .Ltmp169 and .Ltmp171
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin0  # >> Call Site 67 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin0  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin0  # >> Call Site 68 <<
	.long	.Ltmp177-.Ltmp174       #   Call between .Ltmp174 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin0  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin0  # >> Call Site 69 <<
	.long	.Ltmp179-.Ltmp177       #   Call between .Ltmp177 and .Ltmp179
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin0  # >> Call Site 70 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin0  #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin0  # >> Call Site 71 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin0  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin0  # >> Call Site 72 <<
	.long	.Ltmp185-.Ltmp183       #   Call between .Ltmp183 and .Ltmp185
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin0  # >> Call Site 73 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin0  #     jumps to .Ltmp187
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin0  # >> Call Site 74 <<
	.long	.Ltmp188-.Ltmp186       #   Call between .Ltmp186 and .Ltmp188
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin0  # >> Call Site 75 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin0  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin0  # >> Call Site 76 <<
	.long	.Ltmp191-.Ltmp189       #   Call between .Ltmp189 and .Ltmp191
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin0  # >> Call Site 77 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin0  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin0  # >> Call Site 78 <<
	.long	.Ltmp197-.Ltmp194       #   Call between .Ltmp194 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin0  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin0  # >> Call Site 79 <<
	.long	.Ltmp199-.Ltmp197       #   Call between .Ltmp197 and .Ltmp199
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin0  # >> Call Site 80 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin0  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin0  # >> Call Site 81 <<
	.long	.Ltmp202-.Ltmp200       #   Call between .Ltmp200 and .Ltmp202
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin0  # >> Call Site 82 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin0  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin0  # >> Call Site 83 <<
	.long	.Ltmp205-.Ltmp203       #   Call between .Ltmp203 and .Ltmp205
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin0  # >> Call Site 84 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin0  # >> Call Site 85 <<
	.long	.Ltmp211-.Ltmp208       #   Call between .Ltmp208 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin0  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin0  # >> Call Site 86 <<
	.long	.Ltmp213-.Ltmp211       #   Call between .Ltmp211 and .Ltmp213
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin0  # >> Call Site 87 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin0  #     jumps to .Ltmp215
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin0  # >> Call Site 88 <<
	.long	.Ltmp216-.Ltmp214       #   Call between .Ltmp214 and .Ltmp216
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin0  # >> Call Site 89 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin0  #     jumps to .Ltmp218
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin0  # >> Call Site 90 <<
	.long	.Ltmp219-.Ltmp217       #   Call between .Ltmp217 and .Ltmp219
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin0  # >> Call Site 91 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin0  #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp222-.Lfunc_begin0  # >> Call Site 92 <<
	.long	.Ltmp225-.Ltmp222       #   Call between .Ltmp222 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin0  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin0  # >> Call Site 93 <<
	.long	.Ltmp227-.Ltmp225       #   Call between .Ltmp225 and .Ltmp227
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin0  # >> Call Site 94 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin0  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin0  # >> Call Site 95 <<
	.long	.Lfunc_end4-.Ltmp228    #   Call between .Ltmp228 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	0                       # >> Action Record 1 <<
                                        #   Cleanup
	.byte	0                       #   No further actions
	.byte	1                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 1
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIi                   # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN2kcL16make_pg_filenameEPKc,@function
_ZN2kcL16make_pg_filenameEPKc:          # @_ZN2kcL16make_pg_filenameEPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	strlen
	leaq	3(%rax), %rdi
	callq	_Znam
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$92, %esi
	movq	%r14, %rdi
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	movb	$47, (%rax)
	movl	$92, %esi
	movq	%rax, %rdi
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	strchr
	testq	%rax, %rax
	jne	.LBB5_1
# BB#3:                                 # %._crit_edge
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN2kcL16make_pg_filenameEPKc, .Lfunc_end5-_ZN2kcL16make_pg_filenameEPKc
	.cfi_endproc

	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,comdat
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	.p2align	4, 0x90
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,@function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_: # @_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r13, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	leaq	16(%r12), %r13
	movq	%r13, (%r12)
	movq	(%rsi), %r15
	movq	8(%rsi), %rbx
	testq	%r15, %r15
	jne	.LBB6_2
# BB#1:
	testq	%rbx, %rbx
	jne	.LBB6_17
.LBB6_2:
	movq	%rbx, 8(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB6_3
# BB#4:                                 # %.noexc6.i
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, (%r12)
	movq	8(%rsp), %rcx
	movq	%rcx, 16(%r12)
	testq	%rbx, %rbx
	jne	.LBB6_6
	jmp	.LBB6_9
.LBB6_3:                                # %._crit_edge.i.i.i.i
	movq	%r13, %rax
	testq	%rbx, %rbx
	je	.LBB6_9
.LBB6_6:
	cmpq	$1, %rbx
	jne	.LBB6_8
# BB#7:
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB6_9
.LBB6_8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB6_9:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movq	8(%rsp), %rax
	movq	%rax, 8(%r12)
	movq	(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movq	%r14, %rdi
	callq	strlen
	movabsq	$9223372036854775807, %rcx # imm = 0x7FFFFFFFFFFFFFFF
	subq	8(%r12), %rcx
	cmpq	%rax, %rcx
	jb	.LBB6_10
# BB#15:                                # %_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE15_M_check_lengthEmmPKc.exit.i
.Ltmp230:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp231:
# BB#16:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc.exit
	movq	%r12, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_17:                               # %.noexc.i
	movl	$.L.str.55, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB6_10:
.Ltmp232:
	movl	$.L.str.127, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp233:
# BB#11:                                # %.noexc
.LBB6_12:
.Ltmp234:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	cmpq	%r13, %rdi
	je	.LBB6_14
# BB#13:
	callq	_ZdlPv
.LBB6_14:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_, .Lfunc_end6-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp230-.Lfunc_begin1  #   Call between .Lfunc_begin1 and .Ltmp230
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp234-.Lfunc_begin1  #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin1  #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp233    #   Call between .Ltmp233 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_,comdat
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
	.p2align	4, 0x90
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_,@function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_: # @_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -48
.Lcfi46:
	.cfi_offset %r12, -40
.Lcfi47:
	.cfi_offset %r13, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	leaq	16(%rbx), %r13
	movq	%r13, (%rbx)
	movq	(%rsi), %r15
	movq	8(%rsi), %r12
	testq	%r15, %r15
	jne	.LBB8_2
# BB#1:
	testq	%r12, %r12
	jne	.LBB8_14
.LBB8_2:
	movq	%r12, 8(%rsp)
	cmpq	$15, %r12
	jbe	.LBB8_3
# BB#4:                                 # %.noexc6.i
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, (%rbx)
	movq	8(%rsp), %rcx
	movq	%rcx, 16(%rbx)
	testq	%r12, %r12
	jne	.LBB8_6
	jmp	.LBB8_9
.LBB8_3:                                # %._crit_edge.i.i.i.i
	movq	%r13, %rax
	testq	%r12, %r12
	je	.LBB8_9
.LBB8_6:
	cmpq	$1, %r12
	jne	.LBB8_8
# BB#7:
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB8_9
.LBB8_8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
.LBB8_9:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movq	8(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rcx
	movb	$0, (%rcx,%rax)
	movq	(%r14), %rsi
	movq	8(%r14), %rdx
.Ltmp235:
	movq	%rbx, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp236:
# BB#10:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendERKS4_.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_14:                               # %.noexc.i
	movl	$.L.str.55, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB8_11:
.Ltmp237:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.LBB8_13
# BB#12:
	callq	_ZdlPv
.LBB8_13:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_, .Lfunc_end8-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_SA_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp235-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp235
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin2  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Lfunc_end8-.Ltmp236    #   Call between .Ltmp236 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_,comdat
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
	.p2align	4, 0x90
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_,@function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_: # @_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -48
.Lcfi56:
	.cfi_offset %r12, -40
.Lcfi57:
	.cfi_offset %r13, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	leaq	16(%rbx), %r13
	movq	%r13, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	movq	8(%r14), %rsi
	addq	%r12, %rsi
.Ltmp238:
	movq	%rbx, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm
.Ltmp239:
# BB#1:
	movabsq	$9223372036854775807, %rax # imm = 0x7FFFFFFFFFFFFFFF
	subq	8(%rbx), %rax
	cmpq	%r12, %rax
	jb	.LBB9_2
# BB#4:                                 # %_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE15_M_check_lengthEmmPKc.exit.i
.Ltmp240:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp241:
# BB#5:                                 # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKcm.exit
	movq	(%r14), %rsi
	movq	8(%r14), %rdx
.Ltmp242:
	movq	%rbx, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp243:
# BB#6:                                 # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendERKS4_.exit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB9_2:
.Ltmp244:
	movl	$.L.str.127, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp245:
# BB#3:                                 # %.noexc
.LBB9_7:
.Ltmp246:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.LBB9_9
# BB#8:
	callq	_ZdlPv
.LBB9_9:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_, .Lfunc_end9-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEPKS5_RKS8_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp238-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp245-.Ltmp238       #   Call between .Ltmp238 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin3  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp245    #   Call between .Ltmp245 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$16712, %rsp            # imm = 0x4148
.Lcfi66:
	.cfi_def_cfa_offset 16768
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	$.L.str.84, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB10_1
# BB#3:
	movl	$.L.str.84, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB10_4
# BB#6:
	leaq	176(%rsp), %rdx
	movl	$1, %edi
	movq	%r15, %rsi
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB10_7
.LBB10_8:
	leaq	32(%rsp), %rdx
	movl	$1, %edi
	movq	%rbp, %rsi
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB10_9
.LBB10_10:
	movq	224(%rsp), %rax
	cmpq	80(%rsp), %rax
	jne	.LBB10_11
# BB#12:                                # %.preheader.i.preheader
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	leaq	320(%rsp), %r13
	leaq	8512(%rsp), %rbp
	jmp	.LBB10_13
	.p2align	4, 0x90
.LBB10_19:                              #   in Loop: Header=BB10_13 Depth=1
	cmpq	%r15, %r14
	jne	.LBB10_21
# BB#20:                                #   in Loop: Header=BB10_13 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB10_13
	jmp	.LBB10_21
.LBB10_14:                              #   in Loop: Header=BB10_13 Depth=1
	movl	$.L.str.120, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %r15
	movl	$.L.str.122, %edi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_15
.LBB10_16:                              #   in Loop: Header=BB10_13 Depth=1
	movl	$.L.str.120, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$.L.str.122, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_17
	.p2align	4, 0x90
.LBB10_13:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB10_14
.LBB10_15:                              #   in Loop: Header=BB10_13 Depth=1
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%rbp, %rdi
	movq	%r12, %rcx
	callq	fread
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB10_16
.LBB10_17:                              #   in Loop: Header=BB10_13 Depth=1
	movq	%r15, %rax
	orq	%r14, %rax
	jne	.LBB10_19
# BB#18:
	movb	$1, %r14b
	jmp	.LBB10_22
.LBB10_1:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	rename
	testl	%eax, %eax
	je	.LBB10_40
# BB#2:
	movl	$.L.str.117, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.118, %edi
	movl	$.L.str.119, %edx
	movq	%r15, %rsi
	movq	%rbp, %rcx
	callq	_ZN2kc9Problem4SEPKcS1_S1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	jmp	.LBB10_5
.LBB10_4:
	movl	$.L.str.113, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.114, %edi
	movq	%r15, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
.LBB10_5:
	movq	%rax, %rdi
	addq	$16712, %rsp            # imm = 0x4148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB10_11:
	xorl	%r14d, %r14d
	jmp	.LBB10_23
.LBB10_21:
	xorl	%r14d, %r14d
.LBB10_22:                              # %_ZN2kcL9differentEP8_IO_FILES1_PKcS3_.exit
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB10_23:                              # %_ZN2kcL9differentEP8_IO_FILES1_PKcS3_.exit
	movq	%rbx, %rdi
	callq	fclose
	cmpl	$-1, %eax
	je	.LBB10_24
.LBB10_25:
	movq	%r12, %rdi
	callq	fclose
	cmpl	$-1, %eax
	je	.LBB10_26
.LBB10_27:
	cmpb	$0, g_options+113(%rip)
	sete	%al
	testb	%al, %r14b
	je	.LBB10_28
# BB#32:
	cmpb	$0, g_options+65(%rip)
	je	.LBB10_37
# BB#33:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.116, %esi
	movl	$11, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	testq	%rbp, %rbp
	je	.LBB10_34
# BB#35:
	movq	%rbp, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB10_36
.LBB10_28:
	movq	%rbp, %rdi
	callq	remove
	testl	%eax, %eax
	jne	.LBB10_29
.LBB10_30:                              # %_ZN2kcL7eremoveEPKc.exit24
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	rename
	testl	%eax, %eax
	je	.LBB10_40
# BB#31:
	movl	$.L.str.117, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.118, %edi
	movl	$.L.str.119, %edx
	movq	%r15, %rsi
	movq	%rbp, %rcx
	callq	_ZN2kc9Problem4SEPKcS1_S1_S1_
	jmp	.LBB10_39
.LBB10_34:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB10_36:                              # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movb	$41, 320(%rsp)
	leaq	320(%rsp), %rsi
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.LBB10_37:
	movq	%r15, %rdi
	callq	remove
	testl	%eax, %eax
	jne	.LBB10_38
.LBB10_40:                              # %_ZN2kcL7erenameEPKcS1_.exit
	addq	$16712, %rsp            # imm = 0x4148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_7:
	movl	$.L.str.120, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %r14
	movl	$.L.str.121, %edi
	movq	%r15, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_8
.LBB10_9:
	movl	$.L.str.120, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %r14
	movl	$.L.str.121, %edi
	movq	%rbp, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_10
.LBB10_24:
	movl	$.L.str.113, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.115, %edi
	movq	%r15, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_25
.LBB10_26:
	movl	$.L.str.113, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.115, %edi
	movq	%rbp, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_27
.LBB10_38:
	movl	$.L.str.123, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.124, %edi
	movq	%r15, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
.LBB10_39:                              # %_ZN2kcL7erenameEPKcS1_.exit
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_40
.LBB10_29:
	movl	$.L.str.123, %edi
	callq	perror
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.124, %edi
	movq	%rbp, %rsi
	callq	_ZN2kc9Problem2SEPKcS1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB10_30
.Lfunc_end10:
	.size	_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end10-_ZN2kcL26compare_and_delete_or_moveEPKcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL17mkfunctionincnameEPKc,@function
_ZN2kcL17mkfunctionincnameEPKc:         # @_ZN2kcL17mkfunctionincnameEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 48
.Lcfi78:
	.cfi_offset %rbx, -48
.Lcfi79:
	.cfi_offset %r12, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	pg_filename(%rip), %rbx
	movq	8(%rbx), %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	callq	strcpy
	movq	%rax, %r14
	movl	$46, %esi
	movq	%r14, %rdi
	callq	strrchr
	testq	%rax, %rax
	je	.LBB11_2
# BB#1:
	movb	$0, (%rax)
.LBB11_2:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rbx,%rax), %rdi
	callq	_Znam
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	sprintf
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB11_8
# BB#3:                                 # %.lr.ph.preheader
	movq	%r12, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%al, %ebp
	movl	%ebp, %edi
	callq	isalnum
	cmpb	$95, %bpl
	je	.LBB11_7
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB11_4 Depth=1
	testl	%eax, %eax
	jne	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=1
	movb	$95, -1(%rbx)
.LBB11_7:                               #   in Loop: Header=BB11_4 Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB11_4
.LBB11_8:                               # %._crit_edge
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r15
	testq	%r14, %r14
	je	.LBB11_10
# BB#9:
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB11_10:
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN2kcL17mkfunctionincnameEPKc, .Lfunc_end11-_ZN2kcL17mkfunctionincnameEPKc
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL8openfileEPKcS1_,@function
_ZN2kcL8openfileEPKcS1_:                # @_ZN2kcL8openfileEPKcS1_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 96
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$g_options+248, %edi
	movl	$.L.str.43, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
	testl	%eax, %eax
	je	.LBB12_10
# BB#1:
	leaq	8(%rsp), %r14
	movl	$g_options+248, %esi
	movl	$.L.str.15, %edx
	movq	%r14, %rdi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp247:
	leaq	40(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp248:
# BB#2:
	movq	40(%rsp), %rdi
	movl	$.L.str.84, %esi
	callq	popen
	movq	%rax, %rbx
	movq	40(%rsp), %rdi
	leaq	56(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB12_4
# BB#3:
	callq	_ZdlPv
.LBB12_4:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB12_6
# BB#5:
	callq	_ZdlPv
.LBB12_6:
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_10:
	movl	$.L.str.84, %esi
	movq	%rbx, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	jmp	fopen                   # TAILCALL
.LBB12_7:
.Ltmp249:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB12_9
# BB#8:
	callq	_ZdlPv
.LBB12_9:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit2
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN2kcL8openfileEPKcS1_, .Lfunc_end12-_ZN2kcL8openfileEPKcS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp247-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp247
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin4  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Lfunc_end12-.Ltmp248   #   Call between .Ltmp248 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_,comdat
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_
	.p2align	4, 0x90
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_,@function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_: # @_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 144
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	8(%r14), %rbx
	cmpq	16(%r14), %rbx
	je	.LBB13_29
# BB#1:
	leaq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	-32(%rbx), %r15
	movq	-24(%rbx), %rbp
	testq	%r15, %r15
	jne	.LBB13_3
# BB#2:
	testq	%rbp, %rbp
	jne	.LBB13_71
.LBB13_3:
	movq	%rbp, 24(%rsp)
	cmpq	$15, %rbp
	jbe	.LBB13_5
# BB#4:                                 # %.noexc6.i.i.i
	leaq	24(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, (%rbx)
	movq	24(%rsp), %rcx
	movq	%rcx, 16(%rbx)
.LBB13_5:
	testq	%rbp, %rbp
	je	.LBB13_9
# BB#6:
	cmpq	$1, %rbp
	jne	.LBB13_8
# BB#7:
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB13_9
.LBB13_29:                              # %_ZNKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12_M_check_lenEmPKc.exit
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	(%r14), %rbp
	subq	%rbp, %rbx
	sarq	$5, %rbx
	movl	$1, %ecx
	cmovneq	%rbx, %rcx
	leaq	(%rcx,%rbx), %r13
	movq	%r13, %rax
	shrq	$59, %rax
	movabsq	$576460752303423487, %rax # imm = 0x7FFFFFFFFFFFFFF
	cmovneq	%rax, %r13
	addq	%rbx, %rcx
	cmovbq	%rax, %r13
	testq	%r13, %r13
	je	.LBB13_30
# BB#31:
	cmpq	%rax, %r13
	ja	.LBB13_72
# BB#32:                                # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE8allocateERS7_m.exit.i
	movq	%r13, %rdi
	shlq	$5, %rdi
	callq	_Znwm
	jmp	.LBB13_33
.LBB13_8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	memcpy
.LBB13_9:                               # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE9constructIS6_EEvRS7_PS6_RKT_.exit
	movq	24(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rcx
	movb	$0, (%rcx,%rax)
	addq	$32, 8(%r14)
	leaq	40(%rsp), %r15
	movq	%r15, 24(%rsp)
	movq	(%r12), %rbx
	movq	8(%r12), %rbp
	testq	%rbx, %rbx
	jne	.LBB13_11
# BB#10:                                # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE9constructIS6_EEvRS7_PS6_RKT_.exit
	testq	%rbp, %rbp
	jne	.LBB13_71
.LBB13_11:
	movq	%rbp, 64(%rsp)
	cmpq	$15, %rbp
	jbe	.LBB13_12
# BB#13:                                # %.noexc6.i
	leaq	24(%rsp), %rdi
	leaq	64(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, 24(%rsp)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rsp)
	testq	%rbp, %rbp
	jne	.LBB13_15
	jmp	.LBB13_18
.LBB13_12:                              # %._crit_edge.i.i.i.i
	movq	%r15, %rax
	testq	%rbp, %rbp
	je	.LBB13_18
.LBB13_15:
	cmpq	$1, %rbp
	jne	.LBB13_17
# BB#16:
	movb	(%rbx), %cl
	movb	%cl, (%rax)
	jmp	.LBB13_18
.LBB13_17:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
.LBB13_18:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movq	64(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	24(%rsp), %rcx
	movb	$0, (%rcx,%rax)
	movq	8(%r14), %rbx
	leaq	-64(%rbx), %rbp
	subq	%r13, %rbp
	testq	%rbp, %rbp
	jle	.LBB13_22
# BB#19:                                # %.lr.ph.preheader.i.i.i.i
	shrq	$5, %rbp
	incq	%rbp
	addq	$-96, %rbx
	.p2align	4, 0x90
.LBB13_20:                              # %.lr.ph.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	32(%rbx), %rdi
.Ltmp250:
	movq	%rbx, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_
.Ltmp251:
# BB#21:                                # %.noexc
                                        #   in Loop: Header=BB13_20 Depth=1
	decq	%rbp
	addq	$-32, %rbx
	cmpq	$1, %rbp
	jg	.LBB13_20
.LBB13_22:                              # %_ZSt13copy_backwardIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES6_ET0_T_S8_S7_.exit
.Ltmp253:
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_
.Ltmp254:
# BB#23:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_.exit
	movq	24(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB13_55
# BB#24:
	callq	_ZdlPv
	jmp	.LBB13_55
.LBB13_30:
	xorl	%eax, %eax
.LBB13_33:                              # %_ZNSt12_Vector_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE11_M_allocateEm.exit
	movq	56(%rsp), %r15          # 8-byte Reload
	subq	%rbp, %r15
	sarq	$5, %r15
	shlq	$5, %r15
	leaq	(%rax,%r15), %rdi
	leaq	16(%rax,%r15), %rcx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, (%rax,%r15)
	movq	(%r12), %rsi
	movq	8(%r12), %rbx
	testq	%rsi, %rsi
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	jne	.LBB13_37
# BB#34:                                # %_ZNSt12_Vector_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE11_M_allocateEm.exit
	testq	%rbx, %rbx
	jne	.LBB13_35
.LBB13_37:
	movq	%rbx, 24(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB13_38
# BB#39:                                # %.noexc6.i.i.i43
.Ltmp258:
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rbp
	leaq	24(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp259:
# BB#40:                                # %.noexc46
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rax, (%rdi)
	movq	24(%rsp), %rcx
	movq	%rcx, 16(%r12,%r15)
	movq	72(%rsp), %rsi          # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB13_42
	jmp	.LBB13_45
.LBB13_38:                              # %._crit_edge.i.i.i.i.i.i42
	movq	%rcx, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB13_45
.LBB13_42:
	cmpq	$1, %rbx
	jne	.LBB13_44
# BB#43:
	movb	(%rsi), %cl
	movb	%cl, (%rax)
	jmp	.LBB13_45
.LBB13_44:
	movq	%rdi, %rbp
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rdi
.LBB13_45:
	movq	24(%rsp), %rax
	movq	%rax, 8(%r12,%r15)
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	(%rdi), %rcx
	movb	$0, (%rcx,%rax)
	movq	(%r14), %rdi
	xorl	%ebp, %ebp
.Ltmp260:
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_
.Ltmp261:
# BB#46:                                # %_ZSt34__uninitialized_move_if_noexcept_aIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES6_SaIS5_EET0_T_S9_S8_RT1_.exit
	addq	$32, %rax
	movq	8(%r14), %rsi
.Ltmp262:
	movq	%rax, %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_
	movq	%rax, %r15
.Ltmp263:
# BB#47:                                # %_ZSt34__uninitialized_move_if_noexcept_aIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES6_SaIS5_EET0_T_S9_S8_RT1_.exit50
	movq	(%r14), %rbp
	movq	8(%r14), %rbx
	cmpq	%rbx, %rbp
	je	.LBB13_52
	.p2align	4, 0x90
.LBB13_48:                              # %.lr.ph.i.i.i53
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	je	.LBB13_50
# BB#49:                                #   in Loop: Header=BB13_48 Depth=1
	callq	_ZdlPv
.LBB13_50:                              # %_ZSt8_DestroyINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvPT_.exit.i.i.i54
                                        #   in Loop: Header=BB13_48 Depth=1
	addq	$16, %rbp
	cmpq	%rbx, %rbp
	jne	.LBB13_48
# BB#51:                                # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_EvT_S7_RSaIT0_E.exit55thread-pre-split
	movq	(%r14), %rbp
.LBB13_52:                              # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_EvT_S7_RSaIT0_E.exit55
	testq	%rbp, %rbp
	je	.LBB13_54
# BB#53:
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB13_54:                              # %_ZNSt12_Vector_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_deallocateEPS5_m.exit51
	movq	%r12, (%r14)
	movq	%r15, 8(%r14)
	shlq	$5, %r13
	addq	%r13, %r12
	movq	%r12, 16(%r14)
.LBB13_55:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_71:                              # %.noexc.i
	movl	$.L.str.55, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB13_35:                              # %.noexc.i.i.i40
.Ltmp256:
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$.L.str.55, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp257:
# BB#36:                                # %.noexc45
.LBB13_72:
	callq	_ZSt17__throw_bad_allocv
.LBB13_26:                              # %.loopexit.split-lp
.Ltmp255:
	jmp	.LBB13_27
.LBB13_56:
.Ltmp264:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%rbp, %rbp
	je	.LBB13_57
# BB#59:
	cmpq	%rbp, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB13_64
# BB#60:                                # %.lr.ph.i.i.i.preheader
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB13_61:                              # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.LBB13_63
# BB#62:                                #   in Loop: Header=BB13_61 Depth=1
	callq	_ZdlPv
.LBB13_63:                              # %_ZSt8_DestroyINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvPT_.exit.i.i.i
                                        #   in Loop: Header=BB13_61 Depth=1
	addq	$16, %rbx
	cmpq	%rbp, %rbx
	jne	.LBB13_61
.LBB13_64:                              # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE7destroyERS7_PS6_.exit
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB13_66
.LBB13_65:                              # %_ZN9__gnu_cxx14__alloc_traitsISaINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE7destroyERS7_PS6_.exit.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdlPv
.LBB13_66:                              # %_ZNSt12_Vector_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_deallocateEPS5_m.exit
.Ltmp265:
	callq	__cxa_rethrow
.Ltmp266:
# BB#70:
.LBB13_57:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	cmpq	80(%rsp), %rdi          # 8-byte Folded Reload
	je	.LBB13_65
# BB#58:
	callq	_ZdlPv
	jmp	.LBB13_65
.LBB13_67:
.Ltmp267:
	movq	%rax, %rbx
.Ltmp268:
	callq	__cxa_end_catch
.Ltmp269:
	jmp	.LBB13_68
.LBB13_69:
.Ltmp270:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_25:                              # %.loopexit
.Ltmp252:
.LBB13_27:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	cmpq	%r15, %rdi
	jne	.LBB13_28
.LBB13_68:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_28:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_, .Lfunc_end13-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS5_S7_EERKS5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp250-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp250
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp251-.Ltmp250       #   Call between .Ltmp250 and .Ltmp251
	.long	.Ltmp252-.Lfunc_begin5  #     jumps to .Ltmp252
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin5  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp259-.Ltmp258       #   Call between .Ltmp258 and .Ltmp259
	.long	.Ltmp264-.Lfunc_begin5  #     jumps to .Ltmp264
	.byte	1                       #   On action: 1
	.long	.Ltmp259-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp263-.Ltmp260       #   Call between .Ltmp260 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin5  #     jumps to .Ltmp264
	.byte	1                       #   On action: 1
	.long	.Ltmp263-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp256-.Ltmp263       #   Call between .Ltmp263 and .Ltmp256
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp264-.Lfunc_begin5  #     jumps to .Ltmp264
	.byte	1                       #   On action: 1
	.long	.Ltmp257-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp265-.Ltmp257       #   Call between .Ltmp257 and .Ltmp265
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp266-.Ltmp265       #   Call between .Ltmp265 and .Ltmp266
	.long	.Ltmp267-.Lfunc_begin5  #     jumps to .Ltmp267
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp269-.Ltmp268       #   Call between .Ltmp268 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin5  #     jumps to .Ltmp270
	.byte	1                       #   On action: 1
	.long	.Ltmp269-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Lfunc_end13-.Ltmp269   #   Call between .Ltmp269 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_,comdat
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_
	.p2align	4, 0x90
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_,@function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_: # @_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpq	%r15, %r12
	je	.LBB14_1
# BB#2:                                 # %.lr.ph
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%r12), %rbp
	movq	8(%r12), %r13
	testq	%rbp, %rbp
	jne	.LBB14_7
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	testq	%r13, %r13
	jne	.LBB14_5
.LBB14_7:                               #   in Loop: Header=BB14_3 Depth=1
	movq	%r13, (%rsp)
	cmpq	$15, %r13
	jbe	.LBB14_10
# BB#8:                                 # %.noexc6.i.i
                                        #   in Loop: Header=BB14_3 Depth=1
.Ltmp274:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rsp, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp275:
# BB#9:                                 # %.noexc14
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	%rax, (%r14)
	movq	(%rsp), %rcx
	movq	%rcx, 16(%r14)
.LBB14_10:                              #   in Loop: Header=BB14_3 Depth=1
	testq	%r13, %r13
	je	.LBB14_14
# BB#11:                                #   in Loop: Header=BB14_3 Depth=1
	cmpq	$1, %r13
	jne	.LBB14_13
# BB#12:                                #   in Loop: Header=BB14_3 Depth=1
	movzbl	(%rbp), %ecx
	movb	%cl, (%rax)
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_13:                              #   in Loop: Header=BB14_3 Depth=1
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	memcpy
.LBB14_14:                              #   in Loop: Header=BB14_3 Depth=1
	movq	(%rsp), %rax
	movq	%rax, 8(%r14)
	movq	(%r14), %rcx
	movb	$0, (%rcx,%rax)
	addq	$32, %r12
	addq	$32, %r14
	cmpq	%r15, %r12
	jne	.LBB14_3
	jmp	.LBB14_15
.LBB14_1:
	movq	%rbx, %r14
.LBB14_15:                              # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_5:                               # %.noexc.i.i
.Ltmp271:
	movl	$.L.str.55, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp272:
# BB#6:                                 # %.noexc
.LBB14_17:                              # %.loopexit.split-lp
.Ltmp273:
	jmp	.LBB14_18
.LBB14_16:                              # %.loopexit
.Ltmp276:
.LBB14_18:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpq	%rbx, %r14
	jne	.LBB14_20
	jmp	.LBB14_26
	.p2align	4, 0x90
.LBB14_22:                              # %_ZSt8_DestroyINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvPT_.exit.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.LBB14_26
.LBB14_20:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.LBB14_22
# BB#21:                                #   in Loop: Header=BB14_20 Depth=1
	callq	_ZdlPv
	jmp	.LBB14_22
.LBB14_26:                              # %_ZSt8_DestroyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEvT_S7_.exit
.Ltmp277:
	callq	__cxa_rethrow
.Ltmp278:
# BB#27:
.LBB14_23:
.Ltmp279:
	movq	%rax, %rbx
.Ltmp280:
	callq	__cxa_end_catch
.Ltmp281:
# BB#24:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_25:
.Ltmp282:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_, .Lfunc_end14-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_EET0_T_SA_S9_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp274-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin6  #     jumps to .Ltmp276
	.byte	1                       #   On action: 1
	.long	.Ltmp275-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp271-.Ltmp275       #   Call between .Ltmp275 and .Ltmp271
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin6  #     jumps to .Ltmp273
	.byte	1                       #   On action: 1
	.long	.Ltmp272-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp277-.Ltmp272       #   Call between .Ltmp272 and .Ltmp277
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin6  #     jumps to .Ltmp279
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp281-.Ltmp280       #   Call between .Ltmp280 and .Ltmp281
	.long	.Ltmp282-.Lfunc_begin6  #     jumps to .Ltmp282
	.byte	1                       #   On action: 1
	.long	.Ltmp281-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp281   #   Call between .Ltmp281 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_main.ii,@function
_GLOBAL__sub_I_main.ii:                 # @_GLOBAL__sub_I_main.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movq	$g_options+16, g_options(%rip)
	movq	$0, g_options+8(%rip)
	movb	$0, g_options+16(%rip)
	movq	$g_options+48, g_options+32(%rip)
	movq	$0, g_options+40(%rip)
	movb	$0, g_options+48(%rip)
	movw	$0, g_options+72(%rip)
	movq	$0, g_options+64(%rip)
	movb	$121, g_options+74(%rip)
	movq	$g_options+96, g_options+80(%rip)
	movq	$0, g_options+88(%rip)
	movb	$0, g_options+96(%rip)
	movb	$0, g_options+112(%rip)
	movb	$0, g_options+113(%rip)
	movb	$1, g_options+114(%rip)
	movb	$0, g_options+115(%rip)
	movq	$g_options+136, g_options+120(%rip)
	movq	$0, g_options+128(%rip)
	movb	$0, g_options+136(%rip)
	movq	$g_options+168, g_options+152(%rip)
	movq	$0, g_options+160(%rip)
	movb	$0, g_options+168(%rip)
	movq	$g_options+200, g_options+184(%rip)
	movq	$0, g_options+192(%rip)
	movb	$0, g_options+200(%rip)
	movq	$g_options+232, g_options+216(%rip)
	movw	$25443, g_options+232(%rip) # imm = 0x6363
	movq	$2, g_options+224(%rip)
	movb	$0, g_options+234(%rip)
	movq	$g_options+264, g_options+248(%rip)
	movq	$0, g_options+256(%rip)
	movb	$0, g_options+264(%rip)
	movq	$g_options+296, g_options+280(%rip)
	movq	$0, g_options+288(%rip)
	movb	$0, g_options+296(%rip)
	movq	$g_options+328, g_options+312(%rip)
	movq	$0, g_options+320(%rip)
	movb	$0, g_options+328(%rip)
	movb	$1, g_options+344(%rip)
	movb	$1, g_options+345(%rip)
	movb	$0, g_options+346(%rip)
	movl	$_ZN15cmdline_optionsD2Ev, %edi
	movl	$g_options, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end15:
	.size	_GLOBAL__sub_I_main.ii, .Lfunc_end15-_GLOBAL__sub_I_main.ii
	.cfi_endproc

	.type	kimwitu_copyright,@object # @kimwitu_copyright
	.data
	.globl	kimwitu_copyright
	.p2align	4
kimwitu_copyright:
	.asciz	"@(#)$Author: criswell $"
	.size	kimwitu_copyright, 24

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	g_progname,@object      # @g_progname
	.bss
	.globl	g_progname
	.p2align	3
g_progname:
	.quad	0
	.size	g_progname, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"#line "
	.size	.L.str, 7

	.type	pg_line,@object         # @pg_line
	.data
	.globl	pg_line
	.p2align	3
pg_line:
	.quad	.L.str
	.size	pg_line, 8

	.type	g_options,@object       # @g_options
	.bss
	.globl	g_options
	.p2align	3
g_options:
	.zero	352
	.size	g_options, 352

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%s: received signal %d, cleaning up\n"
	.size	.L.str.2, 37

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"kc++: segmentation violation\n"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"kc++: something horrible happened\n"
	.size	.L.str.4, 35

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"stdin"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Reading input files ..."
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" - done.\n"
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Checking input.\n"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Writing output files ..."
	.size	.L.str.9, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	".k.h.temp"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"w"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"k.h"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	".kc.cc.temp"
	.size	.L.str.13, 12

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"k."
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" "
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"k"
	.size	.L.str.16, 2

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"KC_TYPES_HEADER"
	.size	.L.str.17, 16

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"KC_TYPES"
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"writing temporary k.cc file failed:"
	.size	.L.str.19, 36

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"writing temporary k.h file failed:"
	.size	.L.str.20, 35

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"csgiok.h"
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"csgiok."
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"csgiok"
	.size	.L.str.23, 7

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"KC_CSGIO_HEADER"
	.size	.L.str.24, 16

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"KC_CSGIO"
	.size	.L.str.25, 9

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"writing temporary csgiok.cc file failed:"
	.size	.L.str.26, 41

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"writing temporary csgiok.h file failed:"
	.size	.L.str.27, 40

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"unpk.h"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"unpk."
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"unpk"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"KC_UNPARSE_HEADER"
	.size	.L.str.31, 18

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"KC_UNPARSE"
	.size	.L.str.32, 11

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"writing temporary unpk.cc file failed:"
	.size	.L.str.33, 39

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"writing temporary unpk.h file failed:"
	.size	.L.str.34, 38

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"rk.h"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"rk."
	.size	.L.str.36, 4

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"rk"
	.size	.L.str.37, 3

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"KC_REWRITE_HEADER"
	.size	.L.str.38, 18

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"KC_REWRITE"
	.size	.L.str.39, 11

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"writing temporary rk.cc file failed:"
	.size	.L.str.40, 37

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"writing temporary rk.h file failed:"
	.size	.L.str.41, 36

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"h"
	.size	.L.str.42, 2

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.zero	1
	.size	.L.str.43, 1

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"KC_FUNCTIONS_%s_HEADER"
	.size	.L.str.44, 23

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"KC_FUNCTIONS_%s"
	.size	.L.str.45, 16

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"writing temporary "
	.size	.L.str.46, 19

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	" file failed:"
	.size	.L.str.47, 14

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"main"
	.size	.L.str.48, 5

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/main.cc"
	.size	.L.str.49, 81

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"operatork.h"
	.size	.L.str.50, 12

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"writing temporary operatork.h file failed:"
	.size	.L.str.51, 43

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"yxx_union.h"
	.size	.L.str.52, 12

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"writing temporary yxx_union.h file failed:"
	.size	.L.str.53, 43

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.55, 42

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"kc++"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"crudts:f:n:e:mwlb::yop:M:qvW::hV"
	.size	.L.str.57, 33

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Can't be quiet when asked to be verbose."
	.size	.L.str.58, 41

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Can't be verbose when asked to be quiet."
	.size	.L.str.59, 41

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"drop"
	.size	.L.str.60, 5

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"nodrop"
	.size	.L.str.61, 7

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"equiv"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"noequiv"
	.size	.L.str.63, 8

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"overlap"
	.size	.L.str.64, 8

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"noverlapo"
	.size	.L.str.65, 10

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"Unknown warning sub-option:"
	.size	.L.str.66, 28

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"yystype.h"
	.size	.L.str.67, 10

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Covariant option must be yes or no or pre."
	.size	.L.str.68, 43

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"// line "
	.size	.L.str.69, 9

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"stdafx.h"
	.size	.L.str.70, 9

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Do you really want "
	.size	.L.str.71, 20

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	" as dllexport?"
	.size	.L.str.72, 15

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"input file must have '.k' suffix:"
	.size	.L.str.73, 34

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"k.k"
	.size	.L.str.74, 4

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"rk.k"
	.size	.L.str.75, 5

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"unpk.k"
	.size	.L.str.76, 7

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"csgiok.k"
	.size	.L.str.77, 9

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"stdin.k"
	.size	.L.str.78, 8

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"reserved file mybasenames '"
	.size	.L.str.79, 28

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"k.k', '"
	.size	.L.str.80, 8

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"rk.k', '"
	.size	.L.str.81, 9

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"unpk.k', '"
	.size	.L.str.82, 11

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"csgiok.k' and 'stdin.k' not allowed:"
	.size	.L.str.83, 37

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"r"
	.size	.L.str.84, 2

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"cannot open "
	.size	.L.str.85, 13

	.type	_ZL14inputfilenames,@object # @_ZL14inputfilenames
	.local	_ZL14inputfilenames
	.comm	_ZL14inputfilenames,8,8
	.type	_ZL13no_inputfiles,@object # @_ZL13no_inputfiles
	.local	_ZL13no_inputfiles
	.comm	_ZL13no_inputfiles,4,4
	.type	_ZL17current_inputfile,@object # @_ZL17current_inputfile
	.local	_ZL17current_inputfile
	.comm	_ZL17current_inputfile,4,4
	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"Kimwitu++ version %s\n\nCopyright (C) 1997-2003 Michael Piefel, Humboldt-University Berlin\nKimwitu++ comes with ABSOLUTELY NO WARRANTY; for details see GPL.\nThis is free software, and you are welcome to redistribute it under\ncertain conditions; for details see GPL.\n"
	.size	.L.str.86, 265

	.type	_ZL15kimwitu_version,@object # @_ZL15kimwitu_version
	.data
_ZL15kimwitu_version:
	.asciz	"2.3.8"
	.size	_ZL15kimwitu_version, 6

	.type	.L.str.87,@object       # @.str.87
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.87:
	.asciz	"Usage: %s [options] file...\n\n"
	.size	.L.str.87, 30

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"  Other:\n  -M, --msg-format=PAT    specifies format of (error) messages, PAT can contain:\n                            %%p (program name), %%s (severity), %%f (file name),\n                            %%d (current working directory), %%l (line number),\n                            %%c (column); the actual message is appended\n  -q, --quiet             quiet operation\n  -v, --verbose           print additional status information while processing\n  -W                      enable all warnings; use comma-seperated list for\n                            detailed control (can be prefixed with 'no')\n                            drop - dropped rule bodies (no code generated)\n                            equiv - equivalent patterns (cannot match)\n                            overlap - possibly overlapping patterns\n  -h, --help              display this help and exit\n  -V, --version           output version information and exit\n"
	.size	.L.str.92, 924

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"HEADER"
	.size	.L.str.94, 7

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"CODE"
	.size	.L.str.95, 5

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"base_uview"
	.size	.L.str.96, 11

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"base_rview"
	.size	.L.str.97, 11

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"kc_not_uniq"
	.size	.L.str.98, 12

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"uniq"
	.size	.L.str.99, 5

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"abstract_phylum"
	.size	.L.str.100, 16

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"abstract_phylum_ref"
	.size	.L.str.101, 20

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"abstract_list"
	.size	.L.str.102, 14

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"voidptr"
	.size	.L.str.103, 8

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"_VoidPtr"
	.size	.L.str.104, 9

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"integer"
	.size	.L.str.105, 8

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"_Int"
	.size	.L.str.106, 5

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"real"
	.size	.L.str.107, 5

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"_Real"
	.size	.L.str.108, 6

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"casestring"
	.size	.L.str.109, 11

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"_Str"
	.size	.L.str.110, 5

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"nocasestring"
	.size	.L.str.111, 13

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"NoCaseStr"
	.size	.L.str.112, 10

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"kc++ error (in 'compare_and_delete_or_move')"
	.size	.L.str.113, 45

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"could not open temporary file"
	.size	.L.str.114, 30

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"error while closing"
	.size	.L.str.115, 20

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"(unchanged:"
	.size	.L.str.116, 12

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"kc++ error (in 'erename')"
	.size	.L.str.117, 26

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"error while renaming"
	.size	.L.str.118, 21

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"to"
	.size	.L.str.119, 3

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"kc++ error (in 'different')"
	.size	.L.str.120, 28

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"error stat'ing"
	.size	.L.str.121, 15

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"error while reading from"
	.size	.L.str.122, 25

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"kc++ error (in 'eremove')"
	.size	.L.str.123, 26

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"error while removing"
	.size	.L.str.124, 21

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"writing temporary file failed:"
	.size	.L.str.125, 31

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"removing "
	.size	.L.str.126, 10

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"basic_string::append"
	.size	.L.str.127, 21

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_main.ii
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Options:\n  Kimwitu++ Features:\n  -c, --no-csgio          no read/write functions (csgio.{h,cc}) are generated\n  -r, --no-rewrite        no code for rewrite rules (rk.{h,cc}) is generated\n  -u, --no-unparse        no code for unparse rules (unpk.{h,cc}) is generated\n  -d, --no-printdot       no printdot functions are generated\n  -t, --no-hashtables     no code for hashtables is generated (works only when\n                            both --no-csgio and --no-printdot are specified)\n      --operator-cast     generate operatork.h for operator_cast<>"
	.size	.Lstr, 551

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"  C++ Compiler Options:\n  -n, --covariant=C       use covariant return types: yes|no|pre\n      --stdafx[=FILE]     include for Microsoft precompiled header files is\n                            generated (default stdafx.h)\n  -e, --dllexport=STRING  generates string between keyword class and the\n                            class name of all operators and phyla\n  -m, --smart-pointer     generates code for smart pointers (reference counting)\n  -w, --weak-pointer      generates code for weak pointers\n                            (implies --smart-pointer)"
	.size	.Lstr.1, 555

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"  Files:\n  -s, --suffix=EXT        extension for generated source files (default cc)\n  -f, --file-prefix=PREF  prefix for generated files\n  -o, --overwrite         always write generated files even if not changed.\n  -b, --yystype[=FILE]    generates file (default yystype.h) containing YYSTYPE\n                            for yacc or bison\n  -y, --yxx-union         generates file yxx_union.h for yacc++"
	.size	.Lstr.2, 404

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"  Advanced:\n  -l, --no-linedirec      doesn't print line directives ('#line')\n      --comment-line      print line comments ('//line') instead of directives\n      --dir-line          prepends the current working directory to the file name in line directives\n      --rw-loop           generates a non recursive rewrite function\n  -p, --pipe=CMD          process all files while piping them through CMD."
	.size	.Lstr.3, 402


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
