	.text
	.file	"par-alloc.bc"
	.globl	TreeAlloc
	.p2align	4, 0x90
	.type	TreeAlloc,@function
TreeAlloc:                              # @TreeAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movl	%edi, %ebp
	testl	%ebp, %ebp
	je	.LBB0_2
# BB#1:
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r12
	decl	%ebp
	movl	%r15d, %ebx
	shrl	$31, %ebx
	addl	%r15d, %ebx
	sarl	%ebx
	leal	(%rbx,%r14), %esi
	movl	%ebp, %edi
	movl	%ebx, %edx
	callq	TreeAlloc
	movq	%rax, %r15
	movl	%ebp, %edi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	TreeAlloc
	movl	$1, (%r12)
	movq	%r15, 8(%r12)
	movq	%rax, 16(%r12)
	movq	%r12, %rax
	jmp	.LBB0_3
.LBB0_2:
	xorl	%eax, %eax
.LBB0_3:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	TreeAlloc, .Lfunc_end0-TreeAlloc
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
