	.text
	.file	"dp_dec.bc"
	.globl	unpc_block
	.p2align	4, 0x90
	.type	unpc_block,@function
unpc_block:                             # @unpc_block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, -64(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r13
	movl	96(%rsp), %r11d
	leal	-1(%r11), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	(%r13), %ebx
	movl	%ebx, (%rsi)
	testl	%r8d, %r8d
	je	.LBB0_29
# BB#1:
	movl	$32, %r10d
	subl	%r9d, %r10d
	cmpl	$31, %r8d
	jne	.LBB0_11
# BB#2:                                 # %.preheader551
	cmpl	$2, %edx
	jl	.LBB0_103
# BB#3:                                 # %.lr.ph594.preheader
	movl	%edx, %ebp
	addl	$3, %edx
	leaq	-2(%rbp), %rdi
	andq	$3, %rdx
	je	.LBB0_4
# BB#5:                                 # %.lr.ph594.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph594.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	4(%r13,%rax,4), %ebx
	movl	%r10d, %ecx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, 4(%rsi,%rax,4)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB0_6
# BB#7:                                 # %.lr.ph594.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rdi
	jae	.LBB0_9
	jmp	.LBB0_103
.LBB0_29:
	cmpq	%rsi, %r13
	je	.LBB0_103
# BB#30:
	cmpl	$2, %edx
	jl	.LBB0_103
# BB#31:
	addq	$4, %rsi
	addq	$4, %r13
	movslq	%edx, %rax
	leaq	-4(,%rax,4), %rdx
	movq	%rsi, %rdi
	movq	%r13, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memcpy                  # TAILCALL
.LBB0_11:                               # %.preheader550
	leal	1(%r8), %r9d
	testl	%r8d, %r8d
	jle	.LBB0_20
# BB#12:                                # %.lr.ph590.preheader
	movl	%r9d, %r14d
	leal	3(%r9), %ebp
	leaq	-2(%r14), %rdi
	andq	$3, %rbp
	je	.LBB0_13
# BB#14:                                # %.lr.ph590.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph590.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	4(%r13,%rax,4), %ebx
	movl	%r10d, %ecx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, 4(%rsi,%rax,4)
	incq	%rax
	cmpq	%rax, %rbp
	jne	.LBB0_15
# BB#16:                                # %.lr.ph590.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rdi
	jae	.LBB0_18
	jmp	.LBB0_20
.LBB0_13:
	movl	$1, %eax
	cmpq	$3, %rdi
	jb	.LBB0_20
.LBB0_18:                               # %.lr.ph590.preheader.new
	subq	%rax, %r14
	leaq	12(%rsi,%rax,4), %rbp
	leaq	12(%r13,%rax,4), %rdi
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph590
                                        # =>This Inner Loop Header: Depth=1
	addl	-12(%rdi), %ebx
	movl	%r10d, %ecx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -12(%rbp)
	addl	-8(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -8(%rbp)
	addl	-4(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -4(%rbp)
	addl	(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, (%rbp)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %r14
	jne	.LBB0_19
.LBB0_20:                               # %._crit_edge591
	cmpl	$4, %r8d
	movq	%r11, -56(%rsp)         # 8-byte Spill
	je	.LBB0_51
# BB#21:                                # %._crit_edge591
	cmpl	$8, %r8d
	jne	.LBB0_22
# BB#71:
	movq	-64(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %ecx
	movw	%cx, -122(%rsp)         # 2-byte Spill
	movzwl	2(%rax), %ecx
	movw	%cx, -40(%rsp)          # 2-byte Spill
	movzwl	4(%rax), %ecx
	movw	%cx, -80(%rsp)          # 2-byte Spill
	movzwl	6(%rax), %ecx
	movw	%cx, -48(%rsp)          # 2-byte Spill
	movzwl	8(%rax), %ecx
	movw	%cx, -88(%rsp)          # 2-byte Spill
	movzwl	10(%rax), %ecx
	movw	%cx, -96(%rsp)          # 2-byte Spill
	movzwl	12(%rax), %r15d
	movzwl	14(%rax), %ecx
	cmpl	%edx, %r9d
	movw	%cx, -112(%rsp)         # 2-byte Spill
	jge	.LBB0_97
# BB#72:                                # %.lr.ph579.preheader
	movslq	%r9d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%r13,%rax,4), %r12
	decl	%edx
	subl	%r8d, %edx
	jmp	.LBB0_73
.LBB0_86:                               #   in Loop: Header=BB0_73 Depth=1
	movl	%ebx, %r9d
	movl	%r11d, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %r11d
	orl	%ecx, %r11d
	addl	%r11d, %r15d
	movl	%r15d, %ebx
	imull	%eax, %r11d
	movq	-56(%rsp), %rcx         # 8-byte Reload
	sarl	%cl, %r11d
	subl	%r11d, %r8d
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	-104(%rsp), %r12        # 8-byte Reload
	movw	%bx, -112(%rsp)         # 2-byte Spill
	js	.LBB0_88
# BB#87:                                #   in Loop: Header=BB0_73 Depth=1
	movq	%rcx, %r11
	movzwl	-72(%rsp), %r15d        # 2-byte Folded Reload
	jmp	.LBB0_96
.LBB0_88:                               #   in Loop: Header=BB0_73 Depth=1
	movl	%r14d, %eax
	negl	%eax
	movl	%eax, %edi
	shrl	$31, %edi
	sarl	$31, %r14d
	orl	%edi, %r14d
	addl	%r14d, %ebp
	movl	%ebp, %r15d
	imull	%eax, %r14d
	sarl	%cl, %r14d
	addl	%r14d, %r14d
	subl	%r14d, %r8d
	movq	%rcx, %r11
	jns	.LBB0_96
# BB#89:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-32(%rsp), %edi         # 4-byte Reload
	movl	%edi, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %edi
	orl	%ecx, %edi
	addl	%edi, %r9d
	movl	%r9d, %ecx
	movw	%cx, -96(%rsp)          # 2-byte Spill
	imull	%eax, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-3, %edi, %eax
	addl	%eax, %r8d
	jns	.LBB0_96
# BB#90:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-24(%rsp), %edi         # 4-byte Reload
	movl	%edi, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %edi
	orl	%ecx, %edi
	movl	24(%rsp), %ecx          # 4-byte Reload
	addl	%edi, %ecx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	movw	%cx, -88(%rsp)          # 2-byte Spill
	imull	%eax, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	shll	$2, %edi
	subl	%edi, %r8d
	jns	.LBB0_96
# BB#91:                                #   in Loop: Header=BB0_73 Depth=1
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	%edi, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %edi
	orl	%ecx, %edi
	movl	16(%rsp), %ecx          # 4-byte Reload
	addl	%edi, %ecx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	movw	%cx, -48(%rsp)          # 2-byte Spill
	imull	%eax, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-5, %edi, %eax
	addl	%eax, %r8d
	jns	.LBB0_96
# BB#92:                                #   in Loop: Header=BB0_73 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	%edi, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %edi
	orl	%ecx, %edi
	movl	8(%rsp), %ecx           # 4-byte Reload
	addl	%edi, %ecx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	movw	%cx, -80(%rsp)          # 2-byte Spill
	imull	%eax, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-6, %edi, %eax
	addl	%eax, %r8d
	jns	.LBB0_96
# BB#93:                                #   in Loop: Header=BB0_73 Depth=1
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	%edi, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %edi
	orl	%ecx, %edi
	movl	(%rsp), %ecx            # 4-byte Reload
	addl	%edi, %ecx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	movw	%cx, -40(%rsp)          # 2-byte Spill
	imull	%eax, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-7, %edi, %eax
	addl	%eax, %r8d
	jns	.LBB0_96
# BB#94:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-4(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	addl	-8(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, %eax
	jmp	.LBB0_95
	.p2align	4, 0x90
.LBB0_73:                               # %.lr.ph579
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, -120(%rsp)        # 8-byte Spill
	movl	(%rsi), %r9d
	movl	%r9d, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	subl	-4(%rsi,%rdi,4), %eax
	movl	%r9d, %ebp
	subl	-8(%rsi,%rdi,4), %ebp
	movl	%r9d, %ebx
	subl	-12(%rsi,%rdi,4), %ebx
	movl	%r9d, %ecx
	subl	-16(%rsi,%rdi,4), %ecx
	movl	%r9d, %r14d
	subl	-20(%rsi,%rdi,4), %r14d
	movl	%r9d, %r13d
	subl	-24(%rsi,%rdi,4), %r13d
	movswl	-122(%rsp), %edx        # 2-byte Folded Reload
	movl	%eax, -4(%rsp)          # 4-byte Spill
	movl	%eax, %r8d
	movl	%edx, -8(%rsp)          # 4-byte Spill
	imull	%edx, %r8d
	movswl	-40(%rsp), %edx         # 2-byte Folded Reload
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movl	%ebp, %eax
	movl	%edx, (%rsp)            # 4-byte Spill
	imull	%edx, %eax
	movswl	-80(%rsp), %ebp         # 2-byte Folded Reload
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %edx
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	imull	%ebp, %edx
	movswl	-48(%rsp), %ebx         # 2-byte Folded Reload
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%ecx, %ebp
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	imull	%ebx, %ebp
	addl	%r8d, %eax
	movswl	-88(%rsp), %ebx         # 2-byte Folded Reload
	addl	%edx, %eax
	movl	%r14d, -24(%rsp)        # 4-byte Spill
	movl	%r14d, %ecx
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	imull	%ebx, %ecx
	addl	%ebp, %eax
	movswl	-96(%rsp), %ebx         # 2-byte Folded Reload
	addl	%ecx, %eax
	movl	%r13d, -32(%rsp)        # 4-byte Spill
	imull	%ebx, %r13d
	addl	%r13d, %eax
	movl	%r9d, %r14d
	subl	-28(%rsi,%rdi,4), %r14d
	movw	%r15w, -72(%rsp)        # 2-byte Spill
	movswl	%r15w, %ebp
	movl	%r14d, %ecx
	imull	%ebp, %ecx
	addl	%ecx, %eax
	movq	%r11, %rdx
	movl	%r9d, %r11d
	subl	-32(%rsi,%rdi,4), %r11d
	movzwl	-112(%rsp), %ecx        # 2-byte Folded Reload
	movswl	%cx, %r15d
	movl	%r11d, %ecx
	imull	%r15d, %ecx
	addl	%ecx, %eax
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %r13d
	subl	%eax, %r13d
	movq	%r12, -104(%rsp)        # 8-byte Spill
	movl	(%r12), %r8d
	movl	%r8d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r8d, %r12d
	sarl	$31, %r12d
	orl	%eax, %r12d
	movl	%edx, %ecx
	sarl	%cl, %r13d
	addl	%r8d, %r9d
	addl	%r13d, %r9d
	movl	%r10d, %ecx
	shll	%cl, %r9d
	sarl	%cl, %r9d
	movl	%r9d, (%rsi,%rdi,4)
	testl	%r12d, %r12d
	jle	.LBB0_84
# BB#74:                                #   in Loop: Header=BB0_73 Depth=1
	movl	%ebx, %r9d
	movl	%r11d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r11d, %ebx
	sarl	$31, %ebx
	orl	%eax, %ebx
	subl	%ebx, %r15d
	movl	%r15d, %edi
	imull	%r11d, %ebx
	movl	%edx, %ecx
	sarl	%cl, %ebx
	subl	%ebx, %r8d
	movq	%rdx, %r11
	movw	%di, -112(%rsp)         # 2-byte Spill
	jle	.LBB0_75
# BB#77:                                #   in Loop: Header=BB0_73 Depth=1
	movl	%r14d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r14d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	subl	%edx, %ebp
	movl	%ebp, %r15d
	imull	%r14d, %edx
	movl	%r11d, %ecx
	sarl	%cl, %edx
	addl	%edx, %edx
	subl	%edx, %r8d
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	-104(%rsp), %r12        # 8-byte Reload
	jle	.LBB0_96
# BB#78:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-32(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	subl	%edi, %r9d
	movl	%r9d, %eax
	movw	%ax, -96(%rsp)          # 2-byte Spill
	imull	%ecx, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-3, %edi, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.LBB0_96
# BB#79:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-24(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	movl	24(%rsp), %eax          # 4-byte Reload
	subl	%edi, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -88(%rsp)          # 2-byte Spill
	imull	%ecx, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	shll	$2, %edi
	subl	%edi, %r8d
	jle	.LBB0_96
# BB#80:                                #   in Loop: Header=BB0_73 Depth=1
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	movl	16(%rsp), %eax          # 4-byte Reload
	subl	%edi, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -48(%rsp)          # 2-byte Spill
	imull	%ecx, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-5, %edi, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.LBB0_96
# BB#81:                                #   in Loop: Header=BB0_73 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	movl	8(%rsp), %eax           # 4-byte Reload
	subl	%edi, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -80(%rsp)          # 2-byte Spill
	imull	%ecx, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-6, %edi, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.LBB0_96
# BB#82:                                #   in Loop: Header=BB0_73 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	movl	(%rsp), %eax            # 4-byte Reload
	subl	%edi, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -40(%rsp)          # 2-byte Spill
	imull	%ecx, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	imull	$-7, %edi, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.LBB0_96
# BB#83:                                #   in Loop: Header=BB0_73 Depth=1
	movl	-4(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	movl	-8(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
.LBB0_95:                               #   in Loop: Header=BB0_73 Depth=1
	movw	%ax, -122(%rsp)         # 2-byte Spill
	jmp	.LBB0_96
	.p2align	4, 0x90
.LBB0_84:                               #   in Loop: Header=BB0_73 Depth=1
	js	.LBB0_86
# BB#85:                                #   in Loop: Header=BB0_73 Depth=1
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	-56(%rsp), %r11         # 8-byte Reload
	jmp	.LBB0_76
	.p2align	4, 0x90
.LBB0_75:                               #   in Loop: Header=BB0_73 Depth=1
	movq	-120(%rsp), %rdx        # 8-byte Reload
.LBB0_76:                               #   in Loop: Header=BB0_73 Depth=1
	movzwl	-72(%rsp), %r15d        # 2-byte Folded Reload
	movq	-104(%rsp), %r12        # 8-byte Reload
.LBB0_96:                               #   in Loop: Header=BB0_73 Depth=1
	addq	$4, %rsi
	addq	$4, %r12
	decl	%edx
	jne	.LBB0_73
.LBB0_97:                               # %._crit_edge580
	movq	-64(%rsp), %rax         # 8-byte Reload
	movzwl	-122(%rsp), %ecx        # 2-byte Folded Reload
	movw	%cx, (%rax)
	movzwl	-40(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 2(%rax)
	movzwl	-80(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 4(%rax)
	movzwl	-48(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 6(%rax)
	movzwl	-88(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 8(%rax)
	movzwl	-96(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 10(%rax)
	movw	%r15w, 12(%rax)
	movzwl	-112(%rsp), %ecx        # 2-byte Folded Reload
	movw	%cx, 14(%rax)
	jmp	.LBB0_103
.LBB0_51:
	movq	-64(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %ecx
	movw	%cx, -88(%rsp)          # 2-byte Spill
	movzwl	2(%rax), %ecx
	movw	%cx, -96(%rsp)          # 2-byte Spill
	movzwl	4(%rax), %r14d
	movzwl	6(%rax), %ecx
	cmpl	%edx, %r9d
	jge	.LBB0_52
# BB#53:                                # %.lr.ph563.preheader
	movl	%r10d, -24(%rsp)        # 4-byte Spill
	movslq	%r9d, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	(%r13,%rax,4), %rbx
	decl	%edx
	subl	%r8d, %edx
	movl	%ecx, %r10d
	movq	-40(%rsp), %r11         # 8-byte Reload
	jmp	.LBB0_54
.LBB0_22:                               # %.preheader548
	cmpl	%edx, %r9d
	jge	.LBB0_103
# BB#23:                                # %.lr.ph556
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	testl	%r8d, %r8d
	movslq	%r8d, %rbx
	movq	%rdx, -120(%rsp)        # 8-byte Spill
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	movq	%r13, -72(%rsp)         # 8-byte Spill
	movq	%rbx, -96(%rsp)         # 8-byte Spill
	jle	.LBB0_24
# BB#32:                                # %.lr.ph556.split.us.preheader
	movslq	%r9d, %r14
	movl	%r8d, %eax
	movl	%r8d, %ecx
	andl	$7, %ecx
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	subq	%rcx, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-16(%rsi,%r14,4), %rdx
	leaq	-4(%rsi,%r14,4), %r9
	movq	%rbx, %rax
	shlq	$32, %rax
	subq	%rax, %rdi
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	movq	%r14, -32(%rsp)         # 8-byte Spill
	movq	%r8, -88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph556.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_40 Depth 2
                                        #     Child Loop BB0_36 Depth 2
                                        #     Child Loop BB0_45 Depth 2
                                        #     Child Loop BB0_48 Depth 2
	leaq	(%rsi,%r14,4), %r11
	movq	%r14, %rax
	subq	-32(%rsp), %rax         # 8-byte Folded Reload
	movl	(%rsi,%rax,4), %r15d
	cmpl	$7, %r8d
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	ja	.LBB0_37
# BB#34:                                #   in Loop: Header=BB0_33 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	movabsq	$4294967296, %r8        # imm = 0x100000000
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_37:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_33 Depth=1
	movq	-40(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movabsq	$4294967296, %r8        # imm = 0x100000000
	je	.LBB0_38
# BB#39:                                # %vector.ph
                                        #   in Loop: Header=BB0_33 Depth=1
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm2        # xmm2 = xmm0[0,0,0,0]
	pxor	%xmm0, %xmm0
	movq	%rax, %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB0_40:                               # %vector.body
                                        #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp), %xmm3         # xmm3 = mem[0],zero
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm3
	movq	(%rbp), %xmm4           # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	movdqu	-16(%rdx), %xmm5
	movdqu	(%rdx), %xmm6
	pshufd	$27, %xmm6, %xmm6       # xmm6 = xmm6[3,2,1,0]
	pshufd	$27, %xmm5, %xmm5       # xmm5 = xmm5[3,2,1,0]
	psubd	%xmm2, %xmm6
	psubd	%xmm2, %xmm5
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm7, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	pshufd	$245, %xmm5, %xmm3      # xmm3 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm3      # xmm3 = xmm4[0,2,2,3]
	punpckldq	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	paddd	%xmm6, %xmm0
	paddd	%xmm5, %xmm1
	addq	$16, %rbp
	addq	$-32, %rdx
	addq	$-8, %rcx
	jne	.LBB0_40
# BB#41:                                # %middle.block
                                        #   in Loop: Header=BB0_33 Depth=1
	cmpl	$0, -24(%rsp)           # 4-byte Folded Reload
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ebp
	movq	%rax, %rbx
	jne	.LBB0_35
	jmp	.LBB0_42
.LBB0_38:                               #   in Loop: Header=BB0_33 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB0_35:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_33 Depth=1
	leaq	(,%rbx,4), %rcx
	movq	%r9, %rax
	subq	%rcx, %rax
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,2), %rcx
	movq	-48(%rsp), %rdx         # 8-byte Reload
	subq	%rbx, %rdx
	.p2align	4, 0x90
.LBB0_36:                               # %scalar.ph
                                        #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rcx), %ebx
	movl	(%rax), %edi
	subl	%r15d, %edi
	imull	%ebx, %edi
	addl	%edi, %ebp
	addq	$-4, %rax
	addq	$2, %rcx
	decq	%rdx
	jne	.LBB0_36
.LBB0_42:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_33 Depth=1
	leaq	-4(%rsi,%r14,4), %r12
	movl	(%r13,%r14,4), %r13d
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	addl	-16(%rsp), %ebp         # 4-byte Folded Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %ebp
	addl	%r15d, %ebp
	addl	%r13d, %ebp
	movl	%r10d, %ecx
	shll	%cl, %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%r11)
	testl	%edx, %edx
	jle	.LBB0_43
# BB#47:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB0_33 Depth=1
	movl	$1, %edi
	movq	-80(%rsp), %rbp         # 8-byte Reload
	movq	-96(%rsp), %rbx         # 8-byte Reload
	movq	-64(%rsp), %r11         # 8-byte Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader.us
                                        #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jle	.LBB0_50
# BB#49:                                #   in Loop: Header=BB0_48 Depth=2
	movq	%rbp, %rax
	sarq	$30, %rax
	movl	%r15d, %esi
	subl	(%r12,%rax), %esi
	movl	%esi, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%esi, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movzwl	-2(%r11,%rbx,2), %eax
	subl	%edx, %eax
	movw	%ax, -2(%r11,%rbx,2)
	decq	%rbx
	imull	%esi, %edx
	sarl	%cl, %edx
	imull	%edi, %edx
	addq	%r8, %rbp
	incl	%edi
	subl	%edx, %r13d
	jg	.LBB0_48
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_33 Depth=1
	movq	-64(%rsp), %rdx         # 8-byte Reload
	jns	.LBB0_50
# BB#44:                                # %.preheader546.us.preheader
                                        #   in Loop: Header=BB0_33 Depth=1
	movl	$1, %edi
	movq	-80(%rsp), %rbp         # 8-byte Reload
	movq	-96(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_45:                               # %.preheader546.us
                                        #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	jle	.LBB0_50
# BB#46:                                #   in Loop: Header=BB0_45 Depth=2
	movq	%rbp, %rcx
	sarq	$30, %rcx
	movl	%r15d, %ebx
	subl	(%r12,%rcx), %ebx
	movl	%ebx, %r11d
	negl	%r11d
	movl	%r11d, %ecx
	shrl	$31, %ecx
	sarl	$31, %ebx
	orl	%ecx, %ebx
	movzwl	-2(%rdx,%rax,2), %ecx
	addl	%ebx, %ecx
	movw	%cx, -2(%rdx,%rax,2)
	decq	%rax
	imull	%r11d, %ebx
	movq	-56(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %ebx
	imull	%edi, %ebx
	addq	%r8, %rbp
	incl	%edi
	subl	%ebx, %r13d
	js	.LBB0_45
	.p2align	4, 0x90
.LBB0_50:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_33 Depth=1
	incq	%r14
	movq	-112(%rsp), %rdx        # 8-byte Reload
	addq	$4, %rdx
	addq	$4, %r9
	cmpl	-120(%rsp), %r14d       # 4-byte Folded Reload
	movq	-104(%rsp), %rsi        # 8-byte Reload
	movq	-72(%rsp), %r13         # 8-byte Reload
	movq	-88(%rsp), %r8          # 8-byte Reload
	jne	.LBB0_33
	jmp	.LBB0_103
.LBB0_4:
	movl	$1, %eax
	cmpq	$3, %rdi
	jb	.LBB0_103
.LBB0_9:                                # %.lr.ph594.preheader.new
	subq	%rax, %rbp
	leaq	12(%rsi,%rax,4), %rsi
	leaq	12(%r13,%rax,4), %rdi
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph594
                                        # =>This Inner Loop Header: Depth=1
	addl	-12(%rdi), %ebx
	movl	%r10d, %ecx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -12(%rsi)
	addl	-8(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -8(%rsi)
	addl	-4(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, -4(%rsi)
	addl	(%rdi), %ebx
	shll	%cl, %ebx
	sarl	%cl, %ebx
	movl	%ebx, (%rsi)
	addq	$16, %rsi
	addq	$16, %rdi
	addq	$-4, %rbp
	jne	.LBB0_10
	jmp	.LBB0_103
.LBB0_52:
	movl	%ecx, %r10d
	jmp	.LBB0_70
.LBB0_64:                               #   in Loop: Header=BB0_54 Depth=1
	movl	%r12d, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %r12d
	orl	%edx, %r12d
	movl	-32(%rsp), %edx         # 4-byte Reload
	addl	%r12d, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -96(%rsp)          # 2-byte Spill
	imull	%eax, %r12d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %r12d
	imull	$-3, %r12d, %eax
	addl	%eax, %r9d
	movq	-104(%rsp), %rsi        # 8-byte Reload
	jns	.LBB0_67
# BB#65:                                #   in Loop: Header=BB0_54 Depth=1
	movl	-48(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	addl	-80(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, %eax
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_54:                               # %.lr.ph563
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, -120(%rsp)        # 8-byte Spill
	movl	(%rsi), %edi
	movl	%edi, %eax
	subl	-4(%rsi,%r11,4), %eax
	movl	%edi, %r12d
	subl	-8(%rsi,%r11,4), %r12d
	movl	%edi, %r13d
	subl	-12(%rsi,%r11,4), %r13d
	movl	%edi, %r15d
	subl	-16(%rsi,%r11,4), %r15d
	movswl	-88(%rsp), %ecx         # 2-byte Folded Reload
	movl	%eax, -48(%rsp)         # 4-byte Spill
	movl	%ecx, -80(%rsp)         # 4-byte Spill
	imull	%ecx, %eax
	movswl	-96(%rsp), %edx         # 2-byte Folded Reload
	movl	%r12d, %ecx
	movl	%edx, -32(%rsp)         # 4-byte Spill
	imull	%edx, %ecx
	movw	%r14w, -72(%rsp)        # 2-byte Spill
	movswl	%r14w, %r14d
	movl	%r13d, %edx
	imull	%r14d, %edx
	movswl	%r10w, %r8d
	movl	%r15d, %ebp
	imull	%r8d, %ebp
	addl	%eax, %ecx
	addl	%edx, %ecx
	addl	%ebp, %ecx
	movq	-16(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ecx, %eax
	movq	%rbx, -112(%rsp)        # 8-byte Spill
	movl	(%rbx), %r9d
	movl	%r9d, %ecx
	negl	%ecx
	shrl	$31, %ecx
	movl	%r9d, %edx
	sarl	$31, %edx
	orl	%ecx, %edx
	movq	-56(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %ecx
	sarl	%cl, %eax
	addl	%r9d, %edi
	addl	%eax, %edi
	movl	-24(%rsp), %ecx         # 4-byte Reload
	shll	%cl, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	movl	%edi, (%rsi,%r11,4)
	testl	%edx, %edx
	jle	.LBB0_59
# BB#55:                                #   in Loop: Header=BB0_54 Depth=1
	movl	%r15d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r15d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	subl	%edx, %r8d
	movl	%r8d, %r10d
	imull	%r15d, %edx
	movl	%ebp, %ecx
	sarl	%cl, %edx
	subl	%edx, %r9d
	jle	.LBB0_60
# BB#56:                                #   in Loop: Header=BB0_54 Depth=1
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	subl	%edx, %r14d
                                        # kill: %R14W<def> %R14W<kill> %R14D<kill>
	imull	%r13d, %edx
	movl	%ebp, %ecx
	sarl	%cl, %edx
	addl	%edx, %edx
	subl	%edx, %r9d
	jle	.LBB0_63
# BB#57:                                #   in Loop: Header=BB0_54 Depth=1
	movl	%r12d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r12d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	-32(%rsp), %eax         # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -96(%rsp)          # 2-byte Spill
	imull	%r12d, %edx
	movl	%ebp, %ecx
	sarl	%cl, %edx
	imull	$-3, %edx, %eax
	addl	%eax, %r9d
	testl	%r9d, %r9d
	movq	-104(%rsp), %rsi        # 8-byte Reload
	jle	.LBB0_67
# BB#58:                                #   in Loop: Header=BB0_54 Depth=1
	movl	-48(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	movl	-80(%rsp), %eax         # 4-byte Reload
	subl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
.LBB0_66:                               #   in Loop: Header=BB0_54 Depth=1
	movw	%ax, -88(%rsp)          # 2-byte Spill
.LBB0_67:                               #   in Loop: Header=BB0_54 Depth=1
	movq	-120(%rsp), %rdx        # 8-byte Reload
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_59:                               #   in Loop: Header=BB0_54 Depth=1
	jns	.LBB0_60
# BB#61:                                #   in Loop: Header=BB0_54 Depth=1
	movl	%r15d, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %r15d
	orl	%ecx, %r15d
	addl	%r15d, %r8d
	movl	%r8d, %r10d
	imull	%eax, %r15d
	movq	-56(%rsp), %rcx         # 8-byte Reload
	sarl	%cl, %r15d
	subl	%r15d, %r9d
	js	.LBB0_62
	.p2align	4, 0x90
.LBB0_60:                               #   in Loop: Header=BB0_54 Depth=1
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	-104(%rsp), %rsi        # 8-byte Reload
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movzwl	-72(%rsp), %r14d        # 2-byte Folded Reload
	jmp	.LBB0_69
.LBB0_62:                               #   in Loop: Header=BB0_54 Depth=1
	movl	%r13d, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %r13d
	orl	%edx, %r13d
	addl	%r13d, %r14d
                                        # kill: %R14W<def> %R14W<kill> %R14D<kill>
	imull	%eax, %r13d
	sarl	%cl, %r13d
	addl	%r13d, %r13d
	subl	%r13d, %r9d
	js	.LBB0_64
.LBB0_63:                               #   in Loop: Header=BB0_54 Depth=1
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movq	-104(%rsp), %rsi        # 8-byte Reload
.LBB0_68:                               #   in Loop: Header=BB0_54 Depth=1
	movq	-112(%rsp), %rbx        # 8-byte Reload
.LBB0_69:                               #   in Loop: Header=BB0_54 Depth=1
	addq	$4, %rsi
	addq	$4, %rbx
	decl	%edx
	jne	.LBB0_54
.LBB0_70:                               # %._crit_edge564
	movq	-64(%rsp), %rax         # 8-byte Reload
	movzwl	-88(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, (%rax)
	movzwl	-96(%rsp), %ecx         # 2-byte Folded Reload
	movw	%cx, 2(%rax)
	movw	%r14w, 4(%rax)
	movw	%r10w, 6(%rax)
.LBB0_103:                              # %.loopexit549
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_24:                               # %.lr.ph556.split.preheader
	leaq	1(%rbx), %r11
	movslq	%r9d, %r14
	movq	-56(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	movq	-16(%rsp), %rax         # 8-byte Reload
	sarl	%cl, %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%rbx, %rax
	shlq	$32, %rax
	movq	%rdi, %r9
	subq	%rax, %r9
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph556.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_100 Depth 2
                                        #     Child Loop BB0_27 Depth 2
	leaq	-4(%rsi,%r11,4), %r15
	movq	%r11, %rax
	subq	%r14, %rax
	movl	(%rsi,%rax,4), %r12d
	movl	(%r13,%r11,4), %r13d
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movq	-16(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	addl	%r13d, %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movl	%eax, (%rsi,%r11,4)
	testl	%edx, %edx
	jle	.LBB0_98
# BB#26:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$1, %ebx
	movq	%r9, %rax
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movabsq	$4294967296, %r8        # imm = 0x100000000
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	jle	.LBB0_102
# BB#28:                                #   in Loop: Header=BB0_27 Depth=2
	movq	%rax, %rcx
	sarq	$30, %rcx
	movl	%r12d, %ebp
	subl	(%r15,%rcx), %ebp
	movl	%ebp, %ecx
	negl	%ecx
	shrl	$31, %ecx
	movl	%ebp, %edi
	sarl	$31, %edi
	orl	%ecx, %edi
	movzwl	-2(%rsi,%rdx,2), %ecx
	subl	%edi, %ecx
	movw	%cx, -2(%rsi,%rdx,2)
	decq	%rdx
	imull	%ebp, %edi
	movq	-56(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %edi
	imull	%ebx, %edi
	addq	%r8, %rax
	incl	%ebx
	subl	%edi, %r13d
	jg	.LBB0_27
	jmp	.LBB0_102
	.p2align	4, 0x90
.LBB0_98:                               #   in Loop: Header=BB0_25 Depth=1
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movabsq	$4294967296, %r8        # imm = 0x100000000
	jns	.LBB0_102
# BB#99:                                # %.preheader546.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	$1, %ebx
	movq	%r9, %rax
	movq	-96(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_100:                              # %.preheader546
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdx, %rdx
	jle	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_100 Depth=2
	movq	%rax, %rcx
	sarq	$30, %rcx
	movl	%r12d, %edi
	subl	(%r15,%rcx), %edi
	movl	%edi, %ecx
	negl	%ecx
	movl	%ecx, %ebp
	shrl	$31, %ebp
	sarl	$31, %edi
	orl	%ebp, %edi
	movzwl	-2(%rsi,%rdx,2), %ebp
	addl	%edi, %ebp
	movw	%bp, -2(%rsi,%rdx,2)
	decq	%rdx
	imull	%ecx, %edi
	movq	-56(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %edi
	imull	%ebx, %edi
	addq	%r8, %rax
	incl	%ebx
	subl	%edi, %r13d
	js	.LBB0_100
	.p2align	4, 0x90
.LBB0_102:                              # %.loopexit
                                        #   in Loop: Header=BB0_25 Depth=1
	incq	%r11
	cmpl	-120(%rsp), %r11d       # 4-byte Folded Reload
	movq	-104(%rsp), %rsi        # 8-byte Reload
	movq	-72(%rsp), %r13         # 8-byte Reload
	jne	.LBB0_25
	jmp	.LBB0_103
.Lfunc_end0:
	.size	unpc_block, .Lfunc_end0-unpc_block
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
