	.text
	.file	"gim_contact.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	1148846080              # float 1000
	.long	1148846080              # float 1000
	.long	1148846080              # float 1000
	.long	1148846080              # float 1000
.LCPI0_2:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
.LCPI0_3:
	.long	1151770624              # float 1333
	.long	1151770624              # float 1333
	.long	1151770624              # float 1333
	.long	1151770624              # float 1333
.LCPI0_4:
	.long	1157976064              # float 2133
	.long	1157976064              # float 2133
	.long	1157976064              # float 2133
	.long	1157976064              # float 2133
.LCPI0_5:
	.long	1077936128              # float 3
	.long	1077936128              # float 3
	.long	1077936128              # float 3
	.long	1077936128              # float 3
.LCPI0_6:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_18:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_7:
	.long	1148846080              # float 1000
.LCPI0_8:
	.long	1065353216              # float 1
.LCPI0_9:
	.long	1151770624              # float 1333
.LCPI0_10:
	.long	1157976064              # float 2133
.LCPI0_11:
	.long	1077936128              # float 3
.LCPI0_12:
	.long	925353388               # float 9.99999974E-6
.LCPI0_13:
	.long	2139095039              # float 3.40282347E+38
.LCPI0_14:
	.long	869711765               # float 1.00000001E-7
.LCPI0_15:
	.long	3204448256              # float -0.5
.LCPI0_16:
	.long	1069547520              # float 1.5
.LCPI0_17:
	.long	3072837036              # float -9.99999974E-6
	.text
	.globl	_ZN17gim_contact_array14merge_contactsERKS_b
	.p2align	4, 0x90
	.type	_ZN17gim_contact_array14merge_contactsERKS_b,@function
_ZN17gim_contact_array14merge_contactsERKS_b: # @_ZN17gim_contact_array14merge_contactsERKS_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpl	$0, 8(%rbx)
	je	.LBB0_2
# BB#1:                                 # %_ZN9gim_arrayI11GIM_CONTACTE11clear_rangeEj.exit.i
	movl	$0, 8(%rbx)
.LBB0_2:                                # %_ZN9gim_arrayI11GIM_CONTACTE5clearEv.exit
	movl	8(%rsi), %ebp
	cmpq	$1, %rbp
	jne	.LBB0_5
# BB#3:
	movq	(%rsi), %rbp
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.LBB0_12
# BB#4:                                 # %._ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv.exit_crit_edge.i
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	jmp	.LBB0_13
.LBB0_5:
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	testl	%ebp, %ebp
	movq	$0, 24(%rsp)
	je	.LBB0_14
# BB#6:                                 # %_ZN9gim_arrayI15GIM_RSORT_TOKENEC2Ej.exit
	leaq	(,%rbp,8), %rdi
	callq	_Z9gim_allocm
	movq	%rax, %r14
	movq	%r14, 24(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	8(%rax), %r13d
	testq	%r13, %r13
	je	.LBB0_15
# BB#7:
	cmpl	%r13d, %ebp
	movl	%r13d, %ecx
	movq	%r14, %rdi
	jae	.LBB0_10
# BB#8:
	movq	%r13, %rdi
	shlq	$3, %rdi
.Ltmp0:
	callq	_Z9gim_allocm
	movq	%rax, %rdi
.Ltmp1:
# BB#9:                                 # %_ZN9gim_arrayI15GIM_RSORT_TOKENE6resizeEjb.exit
	movq	%rdi, 24(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_20
.LBB0_10:                               # %.lr.ph136
	movq	(%rax), %r8
	movl	%ecx, %r10d
	cmpl	$1, %ecx
	movl	$1, %r9d
	cmovaq	%r10, %r9
	cmpq	$4, %r9
	jb	.LBB0_21
# BB#16:                                # %min.iters.checked
	movl	$4294967292, %ecx       # imm = 0xFFFFFFFC
	andq	%r9, %rcx
	je	.LBB0_21
# BB#17:                                # %vector.body.preheader
	leaq	152(%r8), %rsi
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movaps	.LCPI0_1(%rip), %xmm8   # xmm8 = [1.000000e+03,1.000000e+03,1.000000e+03,1.000000e+03]
	movaps	.LCPI0_2(%rip), %xmm9   # xmm9 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	movaps	.LCPI0_3(%rip), %xmm10  # xmm10 = [1.333000e+03,1.333000e+03,1.333000e+03,1.333000e+03]
	movaps	.LCPI0_4(%rip), %xmm11  # xmm11 = [2.133000e+03,2.133000e+03,2.133000e+03,2.133000e+03]
	movaps	.LCPI0_5(%rip), %xmm5   # xmm5 = [3.000000e+00,3.000000e+00,3.000000e+00,3.000000e+00]
	movdqa	.LCPI0_6(%rip), %xmm6   # xmm6 = [4,4,4,4]
	movq	%rcx, %rdx
	movq	%rdi, %rbp
	.p2align	4, 0x90
.LBB0_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	-104(%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	movss	-56(%rsi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	-152(%rsi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movss	-148(%rsi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm8, %xmm2
	addps	%xmm9, %xmm2
	cvttps2dq	%xmm2, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	-100(%rsi), %xmm7       # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	movss	-52(%rsi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	mulps	%xmm10, %xmm3
	cvttps2dq	%xmm3, %xmm2
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	-96(%rsi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movss	-48(%rsi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	-144(%rsi), %xmm4       # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	unpcklps	%xmm7, %xmm4    # xmm4 = xmm4[0],xmm7[0],xmm4[1],xmm7[1]
	mulps	%xmm11, %xmm4
	addps	%xmm5, %xmm4
	cvttps2dq	%xmm4, %xmm3
	pslld	$4, %xmm2
	paddd	%xmm1, %xmm2
	pslld	$8, %xmm3
	paddd	%xmm2, %xmm3
	movdqa	%xmm3, %xmm1
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	punpckhdq	%xmm0, %xmm3    # xmm3 = xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movdqu	%xmm3, 16(%rbp)
	movdqu	%xmm1, (%rbp)
	paddd	%xmm6, %xmm0
	addq	$192, %rsi
	addq	$32, %rbp
	addq	$-4, %rdx
	jne	.LBB0_18
# BB#19:                                # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB0_22
	jmp	.LBB0_24
.LBB0_12:
	movl	%eax, 8(%rbx)
	movl	$192, %edi
	callq	_Z9gim_allocm
	movq	%rax, (%rbx)
	movl	$4, 12(%rbx)
	movl	8(%rbx), %ecx
.LBB0_13:                               # %_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_.exit
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	incl	8(%rbx)
	jmp	.LBB0_73
.LBB0_14:
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	jmp	.LBB0_26
.LBB0_15:
	xorl	%r13d, %r13d
	jmp	.LBB0_25
.LBB0_21:
	xorl	%ecx, %ecx
.LBB0_22:                               # %scalar.ph.preheader
	leaq	(%rcx,%rcx,2), %rdx
	shlq	$4, %rdx
	leaq	8(%r8,%rdx), %rsi
	movss	.LCPI0_7(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI0_8(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_10(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_11(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_23:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm1, %xmm5
	cvttss2si	%xmm5, %edx
	movss	-4(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	cvttss2si	%xmm5, %eax
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %ebp
	shll	$4, %eax
	addl	%edx, %eax
	shll	$8, %ebp
	addl	%eax, %ebp
	movl	%ebp, (%rdi,%rcx,8)
	movl	%ecx, 4(%rdi,%rcx,8)
	incq	%rcx
	addq	$48, %rsi
	cmpq	%r10, %rcx
	jb	.LBB0_23
.LBB0_24:                               # %._crit_edge.loopexit
	movq	24(%rsp), %r14
.LBB0_25:                               # %._crit_edge
	movq	%r14, %rdi
.LBB0_26:                               # %._crit_edge
.Ltmp3:
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r13d, %esi
	movq	%rdi, %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_
.Ltmp4:
# BB#27:
	movl	(%rbp), %r12d
	movl	4(%rbp), %eax
	leaq	(%rax,%rax,2), %r14
	shlq	$4, %r14
	movq	8(%rsp), %r8            # 8-byte Reload
	addq	(%r8), %r14
	movl	8(%rbx), %r15d
	movl	12(%rbx), %eax
	cmpl	%r15d, %eax
	movq	%rbp, %r10
	jbe	.LBB0_29
# BB#28:                                # %._ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv.exit_crit_edge.i93
	movq	(%rbx), %rax
	movl	4(%rsp), %r9d           # 4-byte Reload
	jmp	.LBB0_38
.LBB0_29:
	movl	%eax, 8(%rbx)
	addl	%r15d, %r15d
	addl	$4, %r15d
	je	.LBB0_32
# BB#30:
	testl	%eax, %eax
	je	.LBB0_35
# BB#31:
	movq	(%rbx), %rdi
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rsi
	movl	%r15d, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdx
.Ltmp6:
	callq	_Z11gim_reallocPvmm
.Ltmp7:
	jmp	.LBB0_36
.LBB0_32:
	movl	$0, 12(%rbx)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_74
# BB#33:
.Ltmp10:
	callq	_Z8gim_freePv
.Ltmp11:
# BB#34:                                # %.noexc97
	movq	$0, (%rbx)
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	jmp	.LBB0_37
.LBB0_35:
	movl	%r15d, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
.Ltmp8:
	callq	_Z9gim_allocm
.Ltmp9:
.LBB0_36:                               # %.noexc99
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movq	%rax, (%rbx)
	movl	%r15d, 12(%rbx)
.LBB0_37:
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB0_38:
	movl	8(%rbx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	32(%r14), %xmm2
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	movl	8(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	cmpl	$2, %r13d
	jb	.LBB0_71
# BB#39:                                # %.lr.ph
	leaq	(%rax,%rax,2), %rcx
	shlq	$4, %rcx
	addq	(%rbx), %rcx
	movl	%r13d, %r11d
	xorl	%r14d, %r14d
	movl	$1, %r15d
	movss	.LCPI0_17(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movq	%r11, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_41
.LBB0_40:                               #   in Loop: Header=BB0_41 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_54 Depth 2
	movl	%r12d, %esi
	movl	(%r10,%r15,8), %r12d
	movl	4(%r10,%r15,8), %edx
	movq	(%r8), %rax
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	leaq	(%rax,%rdx), %r13
	cmpl	%r12d, %esi
	jne	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_41 Depth=1
	movss	32(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	%xmm4, %xmm2
	movss	32(%rax,%rdx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB0_48
# BB#43:                                #   in Loop: Header=BB0_41 Depth=1
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movups	32(%r13), %xmm2
	movups	%xmm2, 32(%rcx)
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	xorl	%r14d, %r14d
	jmp	.LBB0_70
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_41 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_59
# BB#45:                                #   in Loop: Header=BB0_41 Depth=1
	testb	%r9b, %r9b
	je	.LBB0_59
# BB#46:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	movss	24(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movl	%r14d, %eax
	testb	$1, %al
	jne	.LBB0_52
# BB#47:                                #   in Loop: Header=BB0_41 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %r14d
	jne	.LBB0_53
	jmp	.LBB0_55
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_41 Depth=1
	testb	%r9b, %r9b
	je	.LBB0_70
# BB#49:                                #   in Loop: Header=BB0_41 Depth=1
	cmpl	$7, %r14d
	ja	.LBB0_70
# BB#50:                                #   in Loop: Header=BB0_41 Depth=1
	subss	%xmm1, %xmm0
	andps	.LCPI0_18(%rip), %xmm0
	movss	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_70
# BB#51:                                #   in Loop: Header=BB0_41 Depth=1
	movl	%r14d, %esi
	shlq	$4, %rsi
	movups	16(%rax,%rdx), %xmm0
	movups	%xmm0, 48(%rsp,%rsi)
	incl	%r14d
	jmp	.LBB0_70
.LBB0_52:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB0_41 Depth=1
	addps	48(%rsp), %xmm0
	addss	56(%rsp), %xmm1
	movl	$1, %edx
	cmpl	$1, %r14d
	je	.LBB0_55
.LBB0_53:                               # %.lr.ph.preheader.i.new
                                        #   in Loop: Header=BB0_41 Depth=1
	subq	%rdx, %rax
	shlq	$4, %rdx
	leaq	72(%rsp), %rsi
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_54:                               # %.lr.ph.i
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addps	-24(%rdx), %xmm0
	addss	-16(%rdx), %xmm1
	addps	-8(%rdx), %xmm0
	addss	(%rdx), %xmm1
	addq	$32, %rdx
	addq	$-2, %rax
	jne	.LBB0_54
.LBB0_55:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movaps	%xmm0, %xmm2
	mulss	%xmm2, %xmm2
	movaps	%xmm0, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm1, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm3, %xmm2
	xorl	%r14d, %r14d
	movss	.LCPI0_12(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	ja	.LBB0_59
# BB#56:                                #   in Loop: Header=BB0_41 Depth=1
	movss	.LCPI0_14(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movss	.LCPI0_13(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	jae	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_41 Depth=1
	movd	%xmm2, %eax
	mulss	.LCPI0_15(%rip), %xmm2
	shrl	%eax
	movl	$1597463007, %edx       # imm = 0x5F3759DF
	subl	%eax, %edx
	movd	%edx, %xmm3
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm2
	addss	.LCPI0_16(%rip), %xmm2
	mulss	%xmm3, %xmm2
	movaps	%xmm2, %xmm3
.LBB0_58:                               #   in Loop: Header=BB0_41 Depth=1
	mulss	%xmm3, %xmm1
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rcx)
	movlps	%xmm2, 24(%rcx)
.LBB0_59:                               # %_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3j.exit
                                        #   in Loop: Header=BB0_41 Depth=1
	movl	8(%rbx), %ebp
	movl	12(%rbx), %eax
	cmpl	%ebp, %eax
	jbe	.LBB0_61
# BB#60:                                # %._ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv.exit_crit_edge.i83
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_61:                               #   in Loop: Header=BB0_41 Depth=1
	movl	%eax, 8(%rbx)
	addl	%ebp, %ebp
	addl	$4, %ebp
	je	.LBB0_64
# BB#62:                                #   in Loop: Header=BB0_41 Depth=1
	testl	%eax, %eax
	je	.LBB0_67
# BB#63:                                #   in Loop: Header=BB0_41 Depth=1
	movq	(%rbx), %rdi
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rsi
	movl	%ebp, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdx
.Ltmp13:
	callq	_Z11gim_reallocPvmm
.Ltmp14:
	jmp	.LBB0_68
.LBB0_64:                               #   in Loop: Header=BB0_41 Depth=1
	movl	$0, 12(%rbx)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_40
# BB#65:                                #   in Loop: Header=BB0_41 Depth=1
.Ltmp17:
	callq	_Z8gim_freePv
.Ltmp18:
# BB#66:                                # %.noexc87
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	$0, (%rbx)
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movss	.LCPI0_17(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	jmp	.LBB0_69
.LBB0_67:                               #   in Loop: Header=BB0_41 Depth=1
	movl	%ebp, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
.Ltmp15:
	callq	_Z9gim_allocm
.Ltmp16:
.LBB0_68:                               #   in Loop: Header=BB0_41 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	4(%rsp), %r9d           # 4-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movss	.LCPI0_17(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movq	%rax, (%rbx)
	movl	%ebp, 12(%rbx)
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB0_69:                               #   in Loop: Header=BB0_41 Depth=1
	movl	8(%rbx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movups	32(%r13), %xmm2
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	movl	8(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	leaq	(%rax,%rax,2), %rcx
	shlq	$4, %rcx
	addq	(%rbx), %rcx
.LBB0_70:                               #   in Loop: Header=BB0_41 Depth=1
	incq	%r15
	cmpq	%r11, %r15
	jb	.LBB0_41
.LBB0_71:                               # %_ZN9gim_arrayI15GIM_RSORT_TOKENE5clearEv.exit.i.i72
	testq	%r10, %r10
	je	.LBB0_73
# BB#72:
	movq	%r10, %rdi
	callq	_Z8gim_freePv
	movq	$0, 24(%rsp)
.LBB0_73:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_20:
	movq	%rdi, %r14
	jmp	.LBB0_26
.LBB0_74:
	xorl	%eax, %eax
	movl	4(%rsp), %r9d           # 4-byte Reload
	jmp	.LBB0_38
.LBB0_75:                               # %.thread
.Ltmp2:
	movq	%rax, %rbx
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_80
	jmp	.LBB0_82
.LBB0_76:
.Ltmp12:
	jmp	.LBB0_79
.LBB0_77:
.Ltmp5:
	jmp	.LBB0_79
.LBB0_78:
.Ltmp19:
.LBB0_79:                               # %_ZN9gim_arrayI15GIM_RSORT_TOKENE5clearEv.exit.i.i
	movq	%rax, %rbx
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB0_82
.LBB0_80:
.Ltmp20:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_Z8gim_freePv
.Ltmp21:
# BB#81:                                # %.noexc
	movq	$0, 24(%rsp)
.LBB0_82:                               # %_ZN9gim_arrayI15GIM_RSORT_TOKENED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_83:
.Ltmp22:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN17gim_contact_array14merge_contactsERKS_b, .Lfunc_end0-_ZN17gim_contact_array14merge_contactsERKS_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp16-.Ltmp13         #   Call between .Ltmp13 and .Ltmp16
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp20-.Ltmp16         #   Call between .Ltmp16 and .Ltmp20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Lfunc_end0-.Ltmp21     #   Call between .Ltmp21 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_,"axG",@progbits,_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_,comdat
	.weak	_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_
	.p2align	4, 0x90
	.type	_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_,@function
_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_: # @_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%esi, %r11d
	shrl	%r11d
	je	.LBB1_4
# BB#1:                                 # %.lr.ph33.preheader
	movl	%r11d, %r8d
	movq	%r8, %r14
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
	leal	-1(%r14), %r9d
	movl	(%rdi,%r9,8), %ecx
	movl	4(%rdi,%r9,8), %r10d
	cmpq	%r14, %r8
	jae	.LBB1_8
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	%r14d, %eax
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i19.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.i19
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbx,%rbx), %eax
	cmpl	%esi, %eax
	jge	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_9 Depth=2
	movslq	%eax, %rdx
	movl	-8(%rdi,%rdx,8), %ebp
	subl	(%rdi,%rdx,8), %ebp
	shrl	$31, %ebp
	orl	%ebp, %eax
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=2
	movslq	%eax, %rdx
	cmpl	-8(%rdi,%rdx,8), %ecx
	jns	.LBB1_12
# BB#13:                                # %.thread.i23
                                        #   in Loop: Header=BB1_9 Depth=2
	leaq	-1(%rdx), %rbp
	decl	%ebx
	movq	(%rdi,%rbp,8), %rbp
	movq	%rbp, (%rdi,%rbx,8)
	cmpl	%r11d, %edx
	movl	%eax, %ebx
	jbe	.LBB1_9
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=1
	movl	%ebx, %eax
.LBB1_14:                               # %_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_.exit29
                                        #   in Loop: Header=BB1_2 Depth=1
	decl	%eax
	shlq	$32, %r10
	orq	%rcx, %r10
	movq	%r10, (%rdi,%rax,8)
	decq	%r14
	testl	%r9d, %r9d
	jne	.LBB1_2
.LBB1_4:                                # %.preheader
	cmpl	$2, %esi
	jb	.LBB1_22
# BB#5:                                 # %.lr.ph
	movl	%esi, %r8d
	.p2align	4, 0x90
.LBB1_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
	leal	-1(%r8), %ecx
	movq	(%rdi,%rcx,8), %rax
	movl	(%rdi), %edx
	movl	4(%rdi), %esi
	movq	%rax, (%rdi)
	shlq	$32, %rsi
	orq	%rdx, %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movl	(%rdi), %r10d
	movl	4(%rdi), %r9d
	movl	%ecx, %esi
	shrl	%esi
	je	.LBB1_7
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_6 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph.i
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rax,%rax), %edx
	cmpl	%ecx, %edx
	jge	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	movslq	%edx, %rbp
	movl	-8(%rdi,%rbp,8), %ebx
	subl	(%rdi,%rbp,8), %ebx
	shrl	$31, %ebx
	orl	%ebx, %edx
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=2
	movslq	%edx, %rbx
	cmpl	-8(%rdi,%rbx,8), %r10d
	jns	.LBB1_19
# BB#20:                                # %.thread.i
                                        #   in Loop: Header=BB1_16 Depth=2
	leaq	-1(%rbx), %rbp
	decl	%eax
	movq	(%rdi,%rbp,8), %rbp
	movq	%rbp, (%rdi,%rax,8)
	cmpl	%esi, %ebx
	movl	%edx, %eax
	jbe	.LBB1_16
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_6 Depth=1
	movl	$1, %edx
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_6 Depth=1
	movl	%eax, %edx
.LBB1_21:                               # %_Z13gim_down_heapI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jjT0_.exit
                                        #   in Loop: Header=BB1_6 Depth=1
	decl	%edx
	shlq	$32, %r9
	orq	%r10, %r9
	movq	%r9, (%rdi,%rdx,8)
	decq	%r8
	cmpl	$1, %ecx
	ja	.LBB1_6
.LBB1_22:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_, .Lfunc_end1-_Z13gim_heap_sortI15GIM_RSORT_TOKEN26GIM_RSORT_TOKEN_COMPARATOREvPT_jT0_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN17gim_contact_array21merge_contacts_uniqueERKS_
	.p2align	4, 0x90
	.type	_ZN17gim_contact_array21merge_contacts_uniqueERKS_,@function
_ZN17gim_contact_array21merge_contacts_uniqueERKS_: # @_ZN17gim_contact_array21merge_contacts_uniqueERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, 8(%r14)
	je	.LBB3_2
# BB#1:                                 # %_ZN9gim_arrayI11GIM_CONTACTE11clear_rangeEj.exit.i
	movl	$0, 8(%r14)
.LBB3_2:                                # %_ZN9gim_arrayI11GIM_CONTACTE5clearEv.exit
	movl	8(%rsi), %eax
	cmpl	$1, %eax
	jne	.LBB3_7
# BB#3:
	decl	%eax
	leaq	(%rax,%rax,2), %rbx
	shlq	$4, %rbx
	addq	(%rsi), %rbx
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.LBB3_5
# BB#4:                                 # %._ZN9gim_arrayI11GIM_CONTACTE12growingCheckEv.exit_crit_edge.i
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	jmp	.LBB3_6
.LBB3_5:
	movl	%eax, 8(%r14)
	movl	$192, %edi
	callq	_Z9gim_allocm
	movq	%rax, (%r14)
	movl	$4, 12(%r14)
	movl	8(%r14), %ecx
.LBB3_6:                                # %_ZN9gim_arrayI11GIM_CONTACTE9push_backERKS0_.exit
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	incl	8(%r14)
.LBB3_7:                                # %cdce.end
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN17gim_contact_array21merge_contacts_uniqueERKS_, .Lfunc_end3-_ZN17gim_contact_array21merge_contacts_uniqueERKS_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
