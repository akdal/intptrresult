	.text
	.file	"parsetcommon.bc"
	.globl	AllocPPS
	.p2align	4, 0x90
	.type	AllocPPS,@function
AllocPPS:                               # @AllocPPS
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1160, %edi             # imm = 0x488
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_2:
	movl	$240000, %edi           # imm = 0x3A980
	movl	$1, %esi
	callq	calloc
	movq	%rax, 1104(%rbx)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_4:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	AllocPPS, .Lfunc_end0-AllocPPS
	.cfi_endproc

	.globl	AllocSPS
	.p2align	4, 0x90
	.type	AllocSPS,@function
AllocSPS:                               # @AllocSPS
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movl	$3064, %edi             # imm = 0xBF8
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
# BB#1:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB1_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	AllocSPS, .Lfunc_end1-AllocSPS
	.cfi_endproc

	.globl	FreePPS
	.p2align	4, 0x90
	.type	FreePPS,@function
FreePPS:                                # @FreePPS
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	1104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	FreePPS, .Lfunc_end2-FreePPS
	.cfi_endproc

	.globl	FreeSPS
	.p2align	4, 0x90
	.type	FreeSPS,@function
FreeSPS:                                # @FreeSPS
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	FreeSPS, .Lfunc_end3-FreeSPS
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	sps_is_equal
	.p2align	4, 0x90
	.type	sps_is_equal,@function
sps_is_equal:                           # @sps_is_equal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	cmpl	$0, (%rdi)
	je	.LBB4_25
# BB#1:
	cmpl	$0, (%rsi)
	je	.LBB4_25
# BB#2:
	movl	4(%rdi), %ecx
	movl	8(%rdi), %edx
	cmpl	4(%rsi), %ecx
	sete	%cl
	cmpl	8(%rsi), %edx
	sete	%dl
	andb	%cl, %dl
	movl	12(%rdi), %ecx
	cmpl	12(%rsi), %ecx
	sete	%r8b
	movl	16(%rdi), %ecx
	cmpl	16(%rsi), %ecx
	sete	%cl
	andb	%r8b, %cl
	andb	%dl, %cl
	movl	24(%rdi), %edx
	cmpl	24(%rsi), %edx
	sete	%r8b
	movl	28(%rdi), %edx
	cmpl	28(%rsi), %edx
	sete	%r9b
	andb	%r8b, %r9b
	movl	1008(%rdi), %edx
	cmpl	1008(%rsi), %edx
	sete	%dl
	andb	%r9b, %dl
	andb	%cl, %dl
	movl	1012(%rdi), %r8d
	cmpl	1012(%rsi), %r8d
	sete	%cl
	andb	%dl, %cl
	movzbl	%cl, %r10d
	cmpb	$1, %r10b
	jne	.LBB4_3
# BB#4:
	cmpl	$1, %r8d
	je	.LBB4_7
# BB#5:
	testl	%r8d, %r8d
	jne	.LBB4_18
# BB#6:
	movl	1016(%rdi), %ecx
	xorl	%r10d, %r10d
	cmpl	1016(%rsi), %ecx
	sete	%r10b
	jmp	.LBB4_18
.LBB4_3:
	movl	%r10d, %eax
	popq	%rbx
	retq
.LBB4_7:
	movl	1020(%rdi), %ecx
	movl	1024(%rdi), %edx
	cmpl	1020(%rsi), %ecx
	sete	%cl
	cmpl	1024(%rsi), %edx
	sete	%dl
	andb	%cl, %dl
	movl	1028(%rdi), %ecx
	cmpl	1028(%rsi), %ecx
	sete	%cl
	andb	%dl, %cl
	movl	1032(%rdi), %r8d
	cmpl	1032(%rsi), %r8d
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %r10d
	cmpb	$1, %r10b
	jne	.LBB4_8
# BB#9:                                 # %.preheader
	testl	%r8d, %r8d
	je	.LBB4_18
# BB#10:                                # %.lr.ph
	cmpl	$8, %r8d
	jae	.LBB4_12
# BB#11:
	xorl	%ebx, %ebx
	jmp	.LBB4_17
.LBB4_8:
	movl	%r10d, %eax
	popq	%rbx
	retq
.LBB4_12:                               # %min.iters.checked
	movl	%r8d, %r9d
	andl	$7, %r9d
	movq	%r8, %rbx
	subq	%r9, %rbx
	je	.LBB4_13
# BB#14:                                # %vector.ph
	movl	$-1, %edx
	movd	%edx, %xmm1
	movd	%r10d, %xmm0
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	leaq	1052(%rdi), %r10
	leaq	1052(%rsi), %rdx
	pcmpeqd	%xmm1, %xmm1
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movq	%rbx, %r11
	.p2align	4, 0x90
.LBB4_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%r10), %xmm3
	movdqu	(%r10), %xmm4
	movdqu	-16(%rdx), %xmm5
	movdqu	(%rdx), %xmm6
	pcmpeqd	%xmm3, %xmm5
	pcmpeqd	%xmm4, %xmm6
	pand	%xmm2, %xmm0
	pand	%xmm5, %xmm0
	pand	%xmm2, %xmm1
	pand	%xmm6, %xmm1
	addq	$32, %r10
	addq	$32, %rdx
	addq	$-8, %r11
	jne	.LBB4_15
# BB#16:                                # %middle.block
	pand	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pand	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pand	%xmm0, %xmm1
	movd	%xmm1, %r10d
	testl	%r9d, %r9d
	jne	.LBB4_17
	jmp	.LBB4_18
.LBB4_13:
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_17:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	1036(%rdi,%rbx,4), %edx
	xorl	%ecx, %ecx
	cmpl	1036(%rsi,%rbx,4), %edx
	sete	%cl
	andl	%ecx, %r10d
	incq	%rbx
	cmpq	%r8, %rbx
	jb	.LBB4_17
.LBB4_18:                               # %.loopexit
	movl	2060(%rdi), %ecx
	movl	2064(%rdi), %edx
	xorl	%ebx, %ebx
	cmpl	2060(%rsi), %ecx
	sete	%bl
	xorl	%ecx, %ecx
	cmpl	2064(%rsi), %edx
	sete	%cl
	andl	%r10d, %ecx
	movl	2068(%rdi), %r8d
	xorl	%edx, %edx
	cmpl	2068(%rsi), %r8d
	sete	%dl
	andl	%ebx, %edx
	movl	2072(%rdi), %r8d
	xorl	%ebx, %ebx
	cmpl	2072(%rsi), %r8d
	sete	%bl
	andl	%edx, %ebx
	andl	%ecx, %ebx
	movl	2076(%rdi), %edx
	xorl	%ecx, %ecx
	cmpl	2076(%rsi), %edx
	sete	%cl
	andl	%ebx, %ecx
	je	.LBB4_25
# BB#19:
	testl	%edx, %edx
	jne	.LBB4_21
# BB#20:
	movl	2080(%rdi), %edx
	xorl	%ebx, %ebx
	cmpl	2080(%rsi), %edx
	sete	%bl
	andl	%ebx, %ecx
.LBB4_21:
	movl	2084(%rdi), %ebx
	movl	2088(%rdi), %r8d
	xorl	%edx, %edx
	cmpl	2084(%rsi), %ebx
	sete	%dl
	andl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpl	2088(%rsi), %r8d
	sete	%cl
	andl	%edx, %ecx
	je	.LBB4_25
# BB#22:
	testl	%r8d, %r8d
	je	.LBB4_24
# BB#23:
	movl	2092(%rdi), %eax
	movl	2096(%rdi), %edx
	xorl	%ebx, %ebx
	cmpl	2092(%rsi), %eax
	sete	%bl
	andl	%ecx, %ebx
	xorl	%eax, %eax
	cmpl	2096(%rsi), %edx
	sete	%al
	movl	2100(%rdi), %ecx
	xorl	%edx, %edx
	cmpl	2100(%rsi), %ecx
	sete	%dl
	andl	%eax, %edx
	movl	2104(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	2104(%rsi), %eax
	sete	%cl
	andl	%edx, %ecx
	andl	%ebx, %ecx
.LBB4_24:
	movl	2108(%rdi), %edx
	xorl	%eax, %eax
	cmpl	2108(%rsi), %edx
	sete	%al
	andl	%ecx, %eax
.LBB4_25:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	sps_is_equal, .Lfunc_end4-sps_is_equal
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	pps_is_equal
	.p2align	4, 0x90
	.type	pps_is_equal,@function
pps_is_equal:                           # @pps_is_equal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	cmpl	$0, (%rdi)
	je	.LBB5_21
# BB#1:
	cmpl	$0, (%rsi)
	je	.LBB5_21
# BB#2:
	movl	4(%rdi), %eax
	movl	8(%rdi), %ecx
	cmpl	4(%rsi), %eax
	sete	%al
	cmpl	8(%rsi), %ecx
	sete	%cl
	andb	%al, %cl
	movl	12(%rdi), %eax
	cmpl	12(%rsi), %eax
	sete	%al
	movl	984(%rdi), %edx
	cmpl	984(%rsi), %edx
	sete	%dl
	andb	%al, %dl
	andb	%cl, %dl
	movl	988(%rdi), %r9d
	cmpl	988(%rsi), %r9d
	sete	%al
	andb	%dl, %al
	movzbl	%al, %eax
	cmpb	$1, %al
	jne	.LBB5_21
# BB#3:
	testl	%r9d, %r9d
	je	.LBB5_20
# BB#4:
	movl	992(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	992(%rsi), %ecx
	sete	%al
	jne	.LBB5_21
# BB#5:
	cmpl	$6, %ecx
	ja	.LBB5_20
# BB#6:
	movl	%ecx, %ecx
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_16:
	movl	1092(%rdi), %eax
	movl	1096(%rdi), %ecx
	cmpl	1092(%rsi), %eax
	sete	%al
	cmpl	1096(%rsi), %ecx
	sete	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	jmp	.LBB5_20
.LBB5_14:                               # %.preheader.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r10d
	movl	996(%rdi,%r10,4), %r8d
	xorl	%edx, %edx
	cmpl	996(%rsi,%r10,4), %r8d
	sete	%dl
	andl	%edx, %eax
	incl	%ecx
	cmpl	%r9d, %ecx
	jbe	.LBB5_15
	jmp	.LBB5_20
.LBB5_7:                                # %.lr.ph
	cmpl	$7, %r9d
	jbe	.LBB5_8
# BB#9:                                 # %min.iters.checked
	movl	%r9d, %r8d
	andl	$7, %r8d
	movq	%r9, %rdx
	subq	%r8, %rdx
	je	.LBB5_8
# BB#10:                                # %vector.ph
	movl	$-1, %ecx
	movd	%ecx, %xmm1
	movd	%eax, %xmm0
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	leaq	1076(%rdi), %rax
	leaq	1076(%rsi), %rcx
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI5_0(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movq	%rdx, %r10
.LBB5_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rax), %xmm3
	movdqu	-32(%rax), %xmm4
	movdqu	-16(%rax), %xmm5
	movdqu	(%rax), %xmm6
	movdqu	-48(%rcx), %xmm7
	movdqu	-32(%rcx), %xmm1
	pcmpeqd	%xmm3, %xmm7
	pand	%xmm0, %xmm7
	movdqu	-16(%rcx), %xmm0
	pcmpeqd	%xmm4, %xmm1
	pand	%xmm2, %xmm1
	movdqu	(%rcx), %xmm2
	pcmpeqd	%xmm5, %xmm0
	pand	%xmm8, %xmm0
	pand	%xmm7, %xmm0
	pcmpeqd	%xmm6, %xmm2
	pand	%xmm8, %xmm2
	pand	%xmm1, %xmm2
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %r10
	jne	.LBB5_11
# BB#12:                                # %middle.block
	pand	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	pand	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pand	%xmm0, %xmm1
	movd	%xmm1, %eax
	testl	%r8d, %r8d
	jne	.LBB5_13
	jmp	.LBB5_20
.LBB5_17:
	movl	1100(%rdi), %r8d
	xorl	%eax, %eax
	cmpl	1100(%rsi), %r8d
	sete	%al
	jne	.LBB5_21
# BB#18:                                # %.preheader111
	movq	1104(%rdi), %r9
	movq	1104(%rsi), %r10
	xorl	%edx, %edx
.LBB5_19:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movl	(%r9,%rcx,4), %r11d
	xorl	%ebx, %ebx
	cmpl	(%r10,%rcx,4), %r11d
	sete	%bl
	andl	%ebx, %eax
	incl	%edx
	cmpl	%r8d, %edx
	jbe	.LBB5_19
	jmp	.LBB5_20
.LBB5_8:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	1028(%rdi,%rdx,4), %r8d
	xorl	%ecx, %ecx
	cmpl	1028(%rsi,%rdx,4), %r8d
	sete	%cl
	andl	%eax, %ecx
	movl	1060(%rdi,%rdx,4), %r8d
	xorl	%eax, %eax
	cmpl	1060(%rsi,%rdx,4), %r8d
	sete	%al
	andl	%ecx, %eax
	incq	%rdx
	cmpq	%r9, %rdx
	jb	.LBB5_13
.LBB5_20:                               # %.loopexit
	movl	1112(%rdi), %ecx
	movl	1116(%rdi), %r8d
	xorl	%ebx, %ebx
	cmpl	1112(%rsi), %ecx
	sete	%bl
	xorl	%edx, %edx
	cmpl	1116(%rsi), %r8d
	sete	%dl
	andl	%eax, %edx
	movl	1120(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	1120(%rsi), %eax
	sete	%cl
	andl	%ebx, %ecx
	movl	1124(%rdi), %eax
	xorl	%ebx, %ebx
	cmpl	1124(%rsi), %eax
	sete	%bl
	andl	%ecx, %ebx
	movl	1128(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	1128(%rsi), %eax
	sete	%cl
	andl	%ebx, %ecx
	andl	%edx, %ecx
	movl	1132(%rdi), %eax
	xorl	%edx, %edx
	cmpl	1132(%rsi), %eax
	sete	%dl
	movl	1136(%rdi), %eax
	xorl	%ebx, %ebx
	cmpl	1136(%rsi), %eax
	sete	%bl
	andl	%edx, %ebx
	movl	1144(%rdi), %eax
	xorl	%edx, %edx
	cmpl	1144(%rsi), %eax
	sete	%dl
	andl	%ebx, %edx
	movl	1148(%rdi), %eax
	xorl	%ebx, %ebx
	cmpl	1148(%rsi), %eax
	sete	%bl
	andl	%edx, %ebx
	movl	1152(%rdi), %edx
	xorl	%eax, %eax
	cmpl	1152(%rsi), %edx
	sete	%al
	andl	%ebx, %eax
	andl	%ecx, %eax
.LBB5_21:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	pps_is_equal, .Lfunc_end5-pps_is_equal
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_14
	.quad	.LBB5_20
	.quad	.LBB5_7
	.quad	.LBB5_16
	.quad	.LBB5_16
	.quad	.LBB5_16
	.quad	.LBB5_17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"AllocPPS: PPS"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"AllocPPS: slice_group_id"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"AllocSPS: SPS"
	.size	.L.str.2, 14

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
