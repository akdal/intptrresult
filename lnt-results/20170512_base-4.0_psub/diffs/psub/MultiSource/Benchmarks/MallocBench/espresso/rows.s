	.text
	.file	"rows.bc"
	.globl	sm_row_alloc
	.p2align	4, 0x90
	.type	sm_row_alloc,@function
sm_row_alloc:                           # @sm_row_alloc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$56, %edi
	callq	malloc
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	$0, 48(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	sm_row_alloc, .Lfunc_end0-sm_row_alloc
	.cfi_endproc

	.globl	sm_row_free
	.p2align	4, 0x90
	.type	sm_row_free,@function
sm_row_free:                            # @sm_row_free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB1_1
# BB#2:                                 # %._crit_edge
	testq	%r14, %r14
	je	.LBB1_3
.LBB1_4:                                # %._crit_edge.thread
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB1_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	sm_row_free, .Lfunc_end1-sm_row_free
	.cfi_endproc

	.globl	sm_row_dup
	.p2align	4, 0x90
	.type	sm_row_dup,@function
sm_row_dup:                             # @sm_row_dup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r15
	movq	$0, (%r15)
	movl	$0, 8(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 16(%r15)
	movq	$0, 48(%r15)
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rbx), %esi
	movq	%r15, %rdi
	callq	sm_row_insert
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	sm_row_dup, .Lfunc_end2-sm_row_dup
	.cfi_endproc

	.globl	sm_row_insert
	.p2align	4, 0x90
	.type	sm_row_insert,@function
sm_row_insert:                          # @sm_row_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$48, %edi
	callq	malloc
	movq	$0, 40(%rax)
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB3_1
# BB#3:
	cmpl	%ebp, 4(%rcx)
	jge	.LBB3_5
# BB#4:
	movl	%ebp, 4(%rax)
	movq	%rax, 24(%rcx)
	movq	%rcx, 32(%rax)
	movq	%rax, 24(%r14)
	xorl	%ecx, %ecx
	movl	$24, %edx
	jmp	.LBB3_11
.LBB3_1:
	movl	%ebp, 4(%rax)
	movq	%rax, 16(%r14)
	movq	%rax, 24(%r14)
	movq	$0, 24(%rax)
	xorl	%ecx, %ecx
	jmp	.LBB3_10
.LBB3_5:
	movq	16(%r14), %rbx
	movl	4(%rbx), %ecx
	cmpl	%ebp, %ecx
	jle	.LBB3_6
# BB#16:
	movl	%ebp, 4(%rax)
	movq	%rax, 32(%rbx)
	movq	%rbx, 24(%rax)
	movq	%rax, 16(%r14)
	xorl	%ecx, %ecx
	jmp	.LBB3_10
.LBB3_6:                                # %.preheader
	jge	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rbx
	movl	4(%rbx), %ecx
	cmpl	%ebp, %ecx
	jl	.LBB3_7
.LBB3_8:                                # %._crit_edge
	cmpl	%ebp, %ecx
	jle	.LBB3_12
# BB#9:
	movl	%ebp, 4(%rax)
	movq	32(%rbx), %rcx
	movq	24(%rcx), %rdx
	movq	%rax, 32(%rdx)
	movq	%rdx, 24(%rax)
	movq	%rax, 24(%rcx)
.LBB3_10:                               # %.sink.split
	movl	$32, %edx
.LBB3_11:                               # %.sink.split
	movq	%rcx, (%rax,%rdx)
	incl	4(%r14)
	movq	%rax, %rbx
.LBB3_12:
	testq	%rax, %rax
	je	.LBB3_15
# BB#13:
	cmpq	%rbx, %rax
	je	.LBB3_15
# BB#14:
	movq	%rax, %rdi
	callq	free
.LBB3_15:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	sm_row_insert, .Lfunc_end3-sm_row_insert
	.cfi_endproc

	.globl	sm_row_remove
	.p2align	4, 0x90
	.type	sm_row_remove,@function
sm_row_remove:                          # @sm_row_remove
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB4_5
# BB#1:                                 # %.lr.ph.preheader
	leaq	16(%rdi), %rcx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%esi, 4(%rax)
	jge	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_2
	jmp	.LBB4_5
.LBB4_4:                                # %.critedge
	jne	.LBB4_5
# BB#6:
	movq	24(%rax), %rdx
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	leaq	24(%rsi), %rsi
	cmoveq	%rcx, %rsi
	movq	%rdx, (%rsi)
	movq	24(%rax), %rcx
	movq	32(%rax), %rdx
	leaq	24(%rdi), %rsi
	testq	%rcx, %rcx
	leaq	32(%rcx), %rcx
	cmoveq	%rsi, %rcx
	movq	%rdx, (%rcx)
	decl	4(%rdi)
	movq	%rax, %rdi
	jmp	free                    # TAILCALL
.LBB4_5:                                # %.critedge20
	retq
.Lfunc_end4:
	.size	sm_row_remove, .Lfunc_end4-sm_row_remove
	.cfi_endproc

	.globl	sm_row_find
	.p2align	4, 0x90
	.type	sm_row_find,@function
sm_row_find:                            # @sm_row_find
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB5_2
	jmp	.LBB5_5
	.p2align	4, 0x90
.LBB5_3:                                #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB5_5
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%esi, 4(%rax)
	jl	.LBB5_3
# BB#4:                                 # %.critedge
	je	.LBB5_6
.LBB5_5:                                # %.critedge11
	xorl	%eax, %eax
.LBB5_6:
	retq
.Lfunc_end5:
	.size	sm_row_find, .Lfunc_end5-sm_row_find
	.cfi_endproc

	.globl	sm_row_contains
	.p2align	4, 0x90
	.type	sm_row_contains,@function
sm_row_contains:                        # @sm_row_contains
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.LBB6_9
# BB#1:                                 # %.lr.ph.preheader
	addq	$16, %rsi
.LBB6_2:                                # %.lr.ph.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB6_8
# BB#3:                                 # %.lr.ph.preheader36
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	4(%rcx), %edx
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	4(%rsi), %edx
	jl	.LBB6_8
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	je	.LBB6_6
# BB#7:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_4
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_6:                                # %.outer
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	24(%rcx), %rcx
	addq	$24, %rsi
	testq	%rcx, %rcx
	jne	.LBB6_2
	jmp	.LBB6_9
.LBB6_8:
	xorl	%eax, %eax
.LBB6_9:                                # %.outer._crit_edge
	retq
.Lfunc_end6:
	.size	sm_row_contains, .Lfunc_end6-sm_row_contains
	.cfi_endproc

	.globl	sm_row_intersects
	.p2align	4, 0x90
	.type	sm_row_intersects,@function
sm_row_intersects:                      # @sm_row_intersects
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB7_8
# BB#1:
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.LBB7_8
.LBB7_2:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
	movl	4(%rcx), %esi
	.p2align	4, 0x90
.LBB7_3:                                #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	4(%rdx), %esi
	jl	.LBB7_4
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=2
	jle	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=2
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB7_3
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_2
.LBB7_8:                                # %.loopexit
	retq
.LBB7_6:
	movl	$1, %eax
	retq
.Lfunc_end7:
	.size	sm_row_intersects, .Lfunc_end7-sm_row_intersects
	.cfi_endproc

	.globl	sm_row_compare
	.p2align	4, 0x90
	.type	sm_row_compare,@function
sm_row_compare:                         # @sm_row_compare
	.cfi_startproc
# BB#0:
	movq	16(%rsi), %rcx
	movq	16(%rdi), %rdx
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_1 Depth=1
	movq	24(%rcx), %rcx
	movq	24(%rdx), %rdx
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	setne	%sil
	testq	%rcx, %rcx
	setne	%al
	je	.LBB8_5
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	testq	%rdx, %rdx
	je	.LBB8_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	4(%rdx), %eax
	subl	4(%rcx), %eax
	je	.LBB8_4
	jmp	.LBB8_6
.LBB8_5:                                # %._crit_edge
	movzbl	%al, %ecx
	negl	%ecx
	testb	%sil, %sil
	movl	$1, %eax
	cmovel	%ecx, %eax
.LBB8_6:
	retq
.Lfunc_end8:
	.size	sm_row_compare, .Lfunc_end8-sm_row_compare
	.cfi_endproc

	.globl	sm_row_and
	.p2align	4, 0x90
	.type	sm_row_and,@function
sm_row_and:                             # @sm_row_and
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r12, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r12
	movq	$0, (%r12)
	movl	$0, 8(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	$0, 48(%r12)
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.LBB9_8
# BB#1:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_3
	jmp	.LBB9_8
	.p2align	4, 0x90
.LBB9_9:                                #   in Loop: Header=BB9_3 Depth=1
	movq	%r12, %rdi
	callq	sm_row_insert
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.LBB9_8
# BB#10:                                #   in Loop: Header=BB9_3 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB9_8
.LBB9_3:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movl	4(%r15), %esi
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	4(%rbx), %esi
	jl	.LBB9_5
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=2
	jle	.LBB9_9
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=2
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_4
	jmp	.LBB9_8
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	movq	24(%r15), %r15
	testq	%r15, %r15
	jne	.LBB9_3
.LBB9_8:                                # %.loopexit
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	sm_row_and, .Lfunc_end9-sm_row_and
	.cfi_endproc

	.globl	sm_row_hash
	.p2align	4, 0x90
	.type	sm_row_hash,@function
sm_row_hash:                            # @sm_row_hash
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.LBB10_3
	.p2align	4, 0x90
.LBB10_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	shll	$4, %eax
	addl	%edx, %eax
	addl	4(%rcx), %eax
	cltd
	idivl	%esi
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_1
.LBB10_3:                               # %._crit_edge
	movl	%edx, %eax
	retq
.Lfunc_end10:
	.size	sm_row_hash, .Lfunc_end10-sm_row_hash
	.cfi_endproc

	.globl	sm_row_remove_element
	.p2align	4, 0x90
	.type	sm_row_remove_element,@function
sm_row_remove_element:                  # @sm_row_remove_element
	.cfi_startproc
# BB#0:
	movq	24(%rsi), %rax
	movq	32(%rsi), %rcx
	leaq	16(%rdi), %rdx
	testq	%rcx, %rcx
	leaq	24(%rcx), %rcx
	cmoveq	%rdx, %rcx
	movq	%rax, (%rcx)
	movq	24(%rsi), %rax
	movq	32(%rsi), %rcx
	leaq	24(%rdi), %rdx
	testq	%rax, %rax
	leaq	32(%rax), %rax
	cmoveq	%rdx, %rax
	movq	%rcx, (%rax)
	decl	4(%rdi)
	testq	%rsi, %rsi
	je	.LBB11_1
# BB#2:
	movq	%rsi, %rdi
	jmp	free                    # TAILCALL
.LBB11_1:
	retq
.Lfunc_end11:
	.size	sm_row_remove_element, .Lfunc_end11-sm_row_remove_element
	.cfi_endproc

	.globl	sm_row_print
	.p2align	4, 0x90
	.type	sm_row_print,@function
sm_row_print:                           # @sm_row_print
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB12_3
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rbx), %edx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_1
.LBB12_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	sm_row_print, .Lfunc_end12-sm_row_print
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" %d"
	.size	.L.str, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
