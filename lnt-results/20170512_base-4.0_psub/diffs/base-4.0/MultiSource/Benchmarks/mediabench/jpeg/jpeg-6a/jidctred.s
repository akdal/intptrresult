	.text
	.file	"jidctred.bc"
	.globl	jpeg_idct_4x4
	.p2align	4, 0x90
	.type	jpeg_idct_4x4,@function
jpeg_idct_4x4:                          # @jpeg_idct_4x4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, -124(%rsp)        # 4-byte Spill
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movq	408(%rdi), %r8
	movq	88(%rsi), %r9
	movl	$8, %r13d
	leaq	-112(%rsp), %r12
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_7:                                # %.backedge.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	addq	$2, %rdx
	addq	$4, %r9
	addq	$4, %r12
	movl	%eax, %r13d
.LBB0_1:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movl	$3, %eax
	cmpl	$4, %r13d
	je	.LBB0_7
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movswq	16(%rdx), %r10
	movswq	32(%rdx), %rdi
	movl	%edi, %eax
	orl	%r10d, %eax
	movswq	48(%rdx), %r11
	movswq	80(%rdx), %r14
	movl	%r14d, %ebx
	orl	%r11d, %ebx
	orl	%eax, %ebx
	movswq	96(%rdx), %rax
	movl	%eax, %ebp
	orl	%ebx, %ebp
	movswq	112(%rdx), %r15
	movl	%r15d, %ebx
	orw	%bp, %bx
	je	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movswq	(%rdx), %rbp
	movslq	(%r9), %rbx
	imulq	%rbp, %rbx
	shlq	$14, %rbx
	movslq	64(%r9), %rbp
	imulq	%rdi, %rbp
	movslq	192(%r9), %rdi
	imulq	%rax, %rdi
	imulq	$15137, %rbp, %rax      # imm = 0x3B21
	imulq	$-6270, %rdi, %rdi      # imm = 0xE782
	addq	%rax, %rdi
	leaq	2048(%rbx,%rdi), %rax
	subq	%rdi, %rbx
	movslq	224(%r9), %rdi
	imulq	%r15, %rdi
	movslq	160(%r9), %rbp
	imulq	%r14, %rbp
	movslq	96(%r9), %rsi
	imulq	%r11, %rsi
	movslq	32(%r9), %r11
	imulq	%r10, %r11
	imulq	$-1730, %rdi, %r10      # imm = 0xF93E
	imulq	$11893, %rbp, %r14      # imm = 0x2E75
	addq	%r10, %r14
	imulq	$-17799, %rsi, %r10     # imm = 0xBA79
	addq	%r14, %r10
	imulq	$8697, %r11, %r14       # imm = 0x21F9
	addq	%r10, %r14
	imulq	$-4176, %rdi, %rdi      # imm = 0xEFB0
	imulq	$-4926, %rbp, %rbp      # imm = 0xECC2
	addq	%rdi, %rbp
	imulq	$7373, %rsi, %rsi       # imm = 0x1CCD
	addq	%rbp, %rsi
	imulq	$20995, %r11, %rdi      # imm = 0x5203
	addq	%rsi, %rdi
	leaq	(%rax,%rdi), %rsi
	shrq	$12, %rsi
	movl	%esi, (%r12)
	subq	%rdi, %rax
	shrq	$12, %rax
	movl	%eax, 96(%r12)
	leaq	2048(%rbx,%r14), %rdi
	addq	$2048, %rbx             # imm = 0x800
	shrq	$12, %rdi
	subq	%r14, %rbx
	shrq	$12, %rbx
	movl	$16, %eax
	jmp	.LBB0_5
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	movswl	(%rdx), %edi
	imull	(%r9), %edi
	shll	$2, %edi
	movl	%edi, (%r12)
	movl	%edi, 64(%r12)
	movl	$24, %eax
	movl	%edi, %ebx
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movl	%edi, 32(%r12)
	movl	%ebx, (%r12,%rax,4)
	cmpl	$2, %r13d
	jl	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	decl	%r13d
	movl	%r13d, %eax
	jmp	.LBB0_7
.LBB0_8:                                # %.preheader
	movl	-124(%rsp), %ecx        # 4-byte Reload
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	(%rax,%r9), %r12
	addq	%rcx, %r12
	movslq	-108(%rsp,%r9,4), %r10
	movslq	-104(%rsp,%r9,4), %rax
	movl	%eax, %edi
	orl	%r10d, %edi
	movslq	-100(%rsp,%r9,4), %r11
	movslq	-92(%rsp,%r9,4), %r14
	movl	%r14d, %ebx
	orl	%r11d, %ebx
	orl	%edi, %ebx
	movslq	-88(%rsp,%r9,4), %rbp
	movl	%ebp, %edi
	orl	%ebx, %edi
	movslq	-84(%rsp,%r9,4), %r15
	movl	-112(%rsp,%r9,4), %ebx
	movl	%r15d, %esi
	orl	%edi, %esi
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	movslq	%ebx, %rbx
	shlq	$14, %rbx
	imulq	$15137, %rax, %rsi      # imm = 0x3B21
	imulq	$-6270, %rbp, %rax      # imm = 0xE782
	addq	%rsi, %rax
	leal	262144(%rbx,%rax), %esi
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	subl	%eax, %ebx
	imull	$-1730, %r15d, %r13d    # imm = 0xF93E
	imull	$11893, %r14d, %edi     # imm = 0x2E75
	imull	$-17799, %r11d, %ebp    # imm = 0xBA79
	imull	$8697, %r10d, %eax      # imm = 0x21F9
	addl	%ebp, %eax
	addl	%edi, %eax
	addl	%r13d, %eax
	imull	$-4176, %r15d, %r15d    # imm = 0xEFB0
	imull	$-4926, %r14d, %r14d    # imm = 0xECC2
	imull	$7373, %r11d, %edi      # imm = 0x1CCD
	imull	$20995, %r10d, %ebp     # imm = 0x5203
	addl	%edi, %ebp
	addl	%r14d, %ebp
	addl	%r15d, %ebp
	leal	(%rsi,%rbp), %edi
	shrl	$19, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movzbl	128(%r8,%rdi), %edx
	movb	%dl, (%r12)
	subl	%ebp, %esi
	shrl	$19, %esi
	andl	$1023, %esi             # imm = 0x3FF
	movzbl	128(%r8,%rsi), %edx
	movb	%dl, 3(%r12)
	leal	262144(%rbx), %edx
	leal	262144(%rbx,%rax), %esi
	shrl	$19, %esi
	andl	$1023, %esi             # imm = 0x3FF
	movzbl	128(%r8,%rsi), %ebx
	movb	%bl, 1(%r12)
	subl	%eax, %edx
	shrl	$19, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movzbl	128(%r8,%rdx), %eax
	movl	$2, %edi
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	addl	$16, %ebx
	shrl	$5, %ebx
	andl	$1023, %ebx             # imm = 0x3FF
	movzbl	128(%r8,%rbx), %eax
	movb	%al, (%r12)
	movb	%al, 1(%r12)
	movb	%al, 2(%r12)
	movl	$3, %edi
.LBB0_12:                               #   in Loop: Header=BB0_9 Depth=1
	movb	%al, (%r12,%rdi)
	addq	$8, %r9
	cmpq	$32, %r9
	jne	.LBB0_9
# BB#13:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_idct_4x4, .Lfunc_end0-jpeg_idct_4x4
	.cfi_endproc

	.globl	jpeg_idct_2x2
	.p2align	4, 0x90
	.type	jpeg_idct_2x2,@function
jpeg_idct_2x2:                          # @jpeg_idct_2x2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	408(%rdi), %r9
	movq	88(%rsi), %rsi
	movl	$8, %edi
	leaq	-64(%rsp), %r11
	movl	$84, %r10d
	cmpl	$6, %edi
	jbe	.LBB1_2
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_7:                                # %.backedge.backedge
	addq	$4, %r11
	addq	$4, %rsi
	addq	$2, %rdx
	decl	%edi
	cmpl	$6, %edi
	ja	.LBB1_3
.LBB1_2:                                # %.backedge
	btl	%edi, %r10d
	jb	.LBB1_7
.LBB1_3:
	movswq	16(%rdx), %r14
	movswq	48(%rdx), %r15
	movl	%r15d, %eax
	orl	%r14d, %eax
	movswq	80(%rdx), %r12
	movl	%r12d, %ebp
	orl	%eax, %ebp
	movswq	112(%rdx), %rbx
	movl	%ebx, %eax
	orw	%bp, %ax
	je	.LBB1_4
# BB#5:
	movswq	(%rdx), %rbp
	movslq	(%rsi), %rax
	imulq	%rbp, %rax
	shlq	$15, %rax
	movslq	224(%rsi), %rbp
	imulq	%rbx, %rbp
	imulq	$-5906, %rbp, %rbx      # imm = 0xE8EE
	movslq	160(%rsi), %rbp
	imulq	%r12, %rbp
	imulq	$6967, %rbp, %rbp       # imm = 0x1B37
	addq	%rbx, %rbp
	movslq	96(%rsi), %rbx
	imulq	%r15, %rbx
	imulq	$-10426, %rbx, %rbx     # imm = 0xD746
	addq	%rbp, %rbx
	movslq	32(%rsi), %rbp
	imulq	%r14, %rbp
	imulq	$29692, %rbp, %rbp      # imm = 0x73FC
	addq	%rbx, %rbp
	leaq	4096(%rax,%rbp), %rbx
	orq	$4096, %rax             # imm = 0x1000
	shrq	$13, %rbx
	movl	%ebx, (%r11)
	subq	%rbp, %rax
	shrq	$13, %rax
	jmp	.LBB1_6
.LBB1_4:
	movswl	(%rdx), %eax
	imull	(%rsi), %eax
	shll	$2, %eax
	movl	%eax, (%r11)
.LBB1_6:
	movl	%eax, 32(%r11)
	cmpl	$2, %edi
	jge	.LBB1_7
# BB#8:                                 # %.preheader
	movl	%r8d, %r8d
	movq	(%rcx), %r10
	movslq	-60(%rsp), %r11
	movslq	-52(%rsp), %rbx
	movl	%ebx, %eax
	orl	%r11d, %eax
	movslq	-44(%rsp), %rdx
	movl	%edx, %esi
	orl	%eax, %esi
	movslq	-36(%rsp), %rax
	movl	%eax, %edi
	orl	%esi, %edi
	movl	-64(%rsp), %esi
	je	.LBB1_9
# BB#10:
	imull	$-5906, %eax, %eax      # imm = 0xE8EE
	imull	$6967, %edx, %edx       # imm = 0x1B37
	imull	$-10426, %ebx, %edi     # imm = 0xD746
	imull	$29692, %r11d, %ebp     # imm = 0x73FC
	addl	%edi, %ebp
	addl	%edx, %ebp
	addl	%eax, %ebp
	shll	$15, %esi
	leal	524288(%rsi), %eax
	leal	524288(%rsi,%rbp), %edx
	shrl	$20, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movb	128(%r9,%rdx), %dl
	movb	%dl, (%r10,%r8)
	subl	%ebp, %eax
	shrl	$20, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movb	128(%r9,%rax), %al
	jmp	.LBB1_11
.LBB1_9:
	addl	$16, %esi
	shrl	$5, %esi
	andl	$1023, %esi             # imm = 0x3FF
	movb	128(%r9,%rsi), %al
	movb	%al, (%r10,%r8)
.LBB1_11:
	movb	%al, 1(%r10,%r8)
	movq	8(%rcx), %rax
	movslq	-28(%rsp), %rsi
	movslq	-20(%rsp), %rdi
	movl	%edi, %ecx
	orl	%esi, %ecx
	movslq	-12(%rsp), %rdx
	movl	%edx, %ebp
	orl	%ecx, %ebp
	movslq	-4(%rsp), %rbx
	movl	%ebx, %ecx
	orl	%ebp, %ecx
	movslq	-32(%rsp), %rcx
	je	.LBB1_13
# BB#12:
	imull	$-5906, %ebx, %ebp      # imm = 0xE8EE
	imull	$6967, %edx, %edx       # imm = 0x1B37
	imull	$-10426, %edi, %edi     # imm = 0xD746
	imull	$29692, %esi, %esi      # imm = 0x73FC
	addl	%edi, %esi
	addl	%edx, %esi
	addl	%ebp, %esi
	shll	$15, %ecx
	leal	524288(%rcx), %edx
	leal	524288(%rcx,%rsi), %ecx
	shrl	$20, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movb	128(%r9,%rcx), %cl
	movb	%cl, (%rax,%r8)
	subl	%esi, %edx
	shrl	$20, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movb	128(%r9,%rdx), %cl
	jmp	.LBB1_14
.LBB1_13:
	addl	$16, %ecx
	shrl	$5, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movb	128(%r9,%rcx), %cl
	movb	%cl, (%rax,%r8)
.LBB1_14:
	movb	%cl, 1(%rax,%r8)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_idct_2x2, .Lfunc_end1-jpeg_idct_2x2
	.cfi_endproc

	.globl	jpeg_idct_1x1
	.p2align	4, 0x90
	.type	jpeg_idct_1x1,@function
jpeg_idct_1x1:                          # @jpeg_idct_1x1
	.cfi_startproc
# BB#0:
	movq	408(%rdi), %rax
	movq	88(%rsi), %rsi
	movzwl	(%rdx), %edx
	imull	(%rsi), %edx
	addl	$4, %edx
	shrl	$3, %edx
	andl	$1023, %edx             # imm = 0x3FF
	movb	128(%rax,%rdx), %al
	movq	(%rcx), %rcx
	movl	%r8d, %edx
	movb	%al, (%rcx,%rdx)
	retq
.Lfunc_end2:
	.size	jpeg_idct_1x1, .Lfunc_end2-jpeg_idct_1x1
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
