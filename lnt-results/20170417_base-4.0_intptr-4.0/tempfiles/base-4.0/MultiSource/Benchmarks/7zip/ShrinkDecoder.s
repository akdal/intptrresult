	.text
	.file	"ShrinkDecoder.bc"
	.globl	_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	32(%rsp), %r15
	movq	%r15, %rdi
	callq	_ZN9CInBufferC1Ev
	movq	$0, 104(%rsp)
	movl	$0, 112(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 144(%rsp)
.Ltmp0:
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r15, %rdi
	callq	_ZN9CInBuffer6CreateEj
.Ltmp1:
# BB#1:                                 # %_ZN5NBitl12CBaseDecoderI9CInBufferE6CreateEj.exit
	movl	$-2147024882, %r13d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB0_93
# BB#2:
.Ltmp2:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp3:
# BB#3:                                 # %_ZN5NBitl12CBaseDecoderI9CInBufferE9SetStreamEP19ISequentialInStream.exit
.Ltmp4:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp5:
# BB#4:
	movq	$32, 24(%rsp)
	movl	$0, 80(%rsp)
.Ltmp6:
	leaq	104(%rsp), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN10COutBuffer6CreateEj
.Ltmp7:
# BB#5:
	testb	%al, %al
	je	.LBB0_93
# BB#6:
.Ltmp8:
	leaq	104(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp9:
# BB#7:
.Ltmp10:
	leaq	104(%rsp), %rdi
	callq	_ZN10COutBuffer4InitEv
.Ltmp11:
# BB#8:                                 # %.critedge158.thread.preheader
	leaq	12(%rbx), %rbp
	leaq	32780(%rbx), %rdi
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$257, %edx              # imm = 0x101
	callq	memset
	xorl	%esi, %esi
	movl	$24576, %edx            # imm = 0x6000
	movq	%rbp, %rdi
	callq	memset
	leaq	33037(%rbx), %rdi
	movl	$1, %esi
	movl	$7935, %edx             # imm = 0x1EFF
	callq	memset
	leaq	41229(%rbx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$257, %r12d             # imm = 0x101
	movl	$9, 12(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.LBB0_18
.LBB0_9:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$8192, %r12d            # imm = 0x2000
.LBB0_10:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB0_12
.LBB0_11:                               # %.critedge159
                                        #   in Loop: Header=BB0_18 Depth=1
	movb	$0, 32780(%rbx,%r12)
	movw	%r14w, 12(%rbx,%r12,2)
	leal	1(%r12), %r12d
	movb	$1, %al
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB0_12:                               # %.critedge160
                                        #   in Loop: Header=BB0_18 Depth=1
.Ltmp22:
	leaq	104(%rsp), %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %rbp
.Ltmp23:
# BB#13:                                #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, 176(%rsp)
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	je	.LBB0_17
# BB#14:                                #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, %rax
	subq	88(%rsp), %rax          # 8-byte Folded Reload
	cmpq	$262145, %rax           # imm = 0x40001
	jb	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_18 Depth=1
	movq	32(%rsp), %rax
	addq	64(%rsp), %rax
	subq	48(%rsp), %rax
	movl	80(%rsp), %ecx
	addq	%rax, %rcx
	movl	$32, %eax
	subl	24(%rsp), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, 168(%rsp)
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp25:
	leaq	168(%rsp), %rsi
	leaq	176(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp26:
# BB#16:                                #   in Loop: Header=BB0_18 Depth=1
	testl	%r13d, %r13d
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	jne	.LBB0_93
.LBB0_17:                               # %.critedge158.thread189
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB0_18:                               # %.critedge158.thread.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
                                        #       Child Loop BB0_29 Depth 3
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_63 Depth 4
                                        #         Child Loop BB0_71 Depth 4
                                        #     Child Loop BB0_79 Depth 2
                                        #     Child Loop BB0_86 Depth 2
                                        #     Child Loop BB0_90 Depth 2
	movl	%r14d, %ecx
	movw	%cx, 18(%rsp)           # 2-byte Spill
	movl	24(%rsp), %ebp
	jmp	.LBB0_21
.LBB0_19:                               # %.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpl	$1, %eax
	jne	.LBB0_93
# BB#20:                                #   in Loop: Header=BB0_21 Depth=2
	xorl	%eax, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	$13, %ecx
	setl	%al
	addl	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	.p2align	4, 0x90
.LBB0_21:                               # %.critedge158.thread.outer203
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_29 Depth 3
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_63 Depth 4
                                        #         Child Loop BB0_71 Depth 4
	movl	$1, %edx
	movl	12(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	decl	%edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%eax, 8(%rsp)           # 4-byte Spill
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	jmp	.LBB0_29
.LBB0_22:                               # %.preheader..critedge_crit_edge
                                        #   in Loop: Header=BB0_29 Depth=3
	leaq	33037(%rbx,%r12), %rcx
	addq	$257, %r12              # imm = 0x101
	movq	%r12, %rax
	jmp	.LBB0_28
.LBB0_23:                               #   in Loop: Header=BB0_29 Depth=3
	incl	%eax
	jmp	.LBB0_27
.LBB0_24:                               #   in Loop: Header=BB0_29 Depth=3
	addl	$2, %eax
	jmp	.LBB0_27
.LBB0_25:                               #   in Loop: Header=BB0_29 Depth=3
	addl	$3, %eax
	jmp	.LBB0_27
.LBB0_26:                               #   in Loop: Header=BB0_29 Depth=3
	addl	$4, %eax
.LBB0_27:                               # %.critedge
                                        #   in Loop: Header=BB0_29 Depth=3
	leaq	32780(%rbx,%rax), %rcx
	movl	%eax, %r12d
.LBB0_28:                               # %.critedge
                                        #   in Loop: Header=BB0_29 Depth=3
	movb	$0, (%rcx)
	movzwl	18(%rsp), %ecx          # 2-byte Folded Reload
	movw	%cx, 12(%rbx,%rax,2)
	incl	%r12d
	movb	$1, %al
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movb	$1, %al
.LBB0_29:                               # %.critedge158.thread
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_63 Depth 4
                                        #         Child Loop BB0_71 Depth 4
	andb	$1, %al
	movb	%al, 7(%rsp)            # 1-byte Spill
	cmpl	$8, %ebp
	jae	.LBB0_31
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_52:                               #   in Loop: Header=BB0_29 Depth=3
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_29 Depth=3
	decl	%r12d
	movb	$1, 32780(%rbx,%r12)
.LBB0_54:                               # %.preheader193.preheader
                                        #   in Loop: Header=BB0_29 Depth=3
	xorl	%esi, %esi
	movl	$7935, %edx             # imm = 0x1EFF
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	memset
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_55:                               # %.preheader192
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, 33037(%rbx,%rax)
	jne	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_55 Depth=4
	movzwl	526(%rbx,%rax,2), %ecx
	movb	$1, 40972(%rbx,%rcx)
.LBB0_57:                               # %.preheader192.1471
                                        #   in Loop: Header=BB0_55 Depth=4
	cmpb	$0, 33038(%rbx,%rax)
	jne	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_55 Depth=4
	movzwl	528(%rbx,%rax,2), %ecx
	movb	$1, 40972(%rbx,%rcx)
.LBB0_59:                               # %.preheader192.2472
                                        #   in Loop: Header=BB0_55 Depth=4
	cmpb	$0, 33039(%rbx,%rax)
	jne	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_55 Depth=4
	movzwl	530(%rbx,%rax,2), %ecx
	movb	$1, 40972(%rbx,%rcx)
.LBB0_61:                               #   in Loop: Header=BB0_55 Depth=4
	addq	$3, %rax
	cmpq	$7935, %rax             # imm = 0x1EFF
	jne	.LBB0_55
# BB#62:                                # %.preheader191.preheader
                                        #   in Loop: Header=BB0_29 Depth=3
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_63:                               # %.preheader191
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, 41229(%rbx,%rax)
	jne	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_63 Depth=4
	movb	$1, 33037(%rbx,%rax)
.LBB0_65:                               # %.preheader191.1473
                                        #   in Loop: Header=BB0_63 Depth=4
	cmpb	$0, 41230(%rbx,%rax)
	jne	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_63 Depth=4
	movb	$1, 33038(%rbx,%rax)
.LBB0_67:                               # %.preheader191.2474
                                        #   in Loop: Header=BB0_63 Depth=4
	cmpb	$0, 41231(%rbx,%rax)
	jne	.LBB0_69
# BB#68:                                #   in Loop: Header=BB0_63 Depth=4
	movb	$1, 33039(%rbx,%rax)
.LBB0_69:                               #   in Loop: Header=BB0_63 Depth=4
	addq	$3, %rax
	cmpq	$7935, %rax             # imm = 0x1EFF
	jne	.LBB0_63
# BB#70:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_29 Depth=3
	movl	$257, %eax              # imm = 0x101
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_71:                               # %.preheader
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, 33037(%rbx,%r12)
	jne	.LBB0_22
# BB#72:                                # %.preheader.1475
                                        #   in Loop: Header=BB0_71 Depth=4
	cmpb	$0, 33038(%rbx,%r12)
	jne	.LBB0_23
# BB#73:                                # %.preheader.2476
                                        #   in Loop: Header=BB0_71 Depth=4
	cmpb	$0, 33039(%rbx,%r12)
	jne	.LBB0_24
# BB#74:                                # %.preheader.3477
                                        #   in Loop: Header=BB0_71 Depth=4
	cmpb	$0, 33040(%rbx,%r12)
	jne	.LBB0_25
# BB#75:                                # %.preheader.4478
                                        #   in Loop: Header=BB0_71 Depth=4
	cmpb	$0, 33041(%rbx,%r12)
	jne	.LBB0_26
# BB#76:                                #   in Loop: Header=BB0_71 Depth=4
	addl	$5, %eax
	addq	$5, %r12
	leal	257(%r12), %ecx
	cmpl	$8192, %ecx             # imm = 0x2000
	jb	.LBB0_71
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_38:                               # %._ZN5NBitl12CBaseDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
                                        #   in Loop: Header=BB0_29 Depth=3
	movl	28(%rsp), %r14d
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_30:                               # %.critedge158.thread.loopexit
                                        #   in Loop: Header=BB0_29 Depth=3
	addl	$257, %r12d             # imm = 0x101
	cmpl	$8, %ebp
	jb	.LBB0_38
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	32(%rsp), %rax
	cmpq	40(%rsp), %rax
	jb	.LBB0_35
# BB#32:                                #   in Loop: Header=BB0_31 Depth=4
.Ltmp13:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp14:
# BB#33:                                # %.noexc
                                        #   in Loop: Header=BB0_31 Depth=4
	testb	%al, %al
	je	.LBB0_36
# BB#34:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_31 Depth=4
	movq	32(%rsp), %rax
.LBB0_35:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB0_31 Depth=4
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movzbl	(%rax), %eax
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_36:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB0_31 Depth=4
	incl	80(%rsp)
	movb	$-1, %al
.LBB0_37:                               #   in Loop: Header=BB0_31 Depth=4
	movzbl	%al, %r14d
	movl	24(%rsp), %ebp
	movl	$32, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r14d
	orl	28(%rsp), %r14d
	movl	%r14d, 28(%rsp)
	addl	$-8, %ebp
	movl	%ebp, 24(%rsp)
	cmpl	$7, %ebp
	ja	.LBB0_31
.LBB0_39:                               # %.loopexit202
                                        #   in Loop: Header=BB0_29 Depth=3
	movl	12(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %ebp
	movl	%ebp, 24(%rsp)
	movl	%r14d, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movl	%esi, 28(%rsp)
	movl	80(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_41
# BB#40:                                # %_ZNK5NBitl12CBaseDecoderI9CInBufferE17ExtraBitsWereReadEv.exit
                                        #   in Loop: Header=BB0_29 Depth=3
	movl	$32, %ecx
	subl	%ebp, %ecx
	shll	$3, %eax
	cmpl	%eax, %ecx
	jb	.LBB0_92
.LBB0_41:                               # %_ZNK5NBitl12CBaseDecoderI9CInBufferE17ExtraBitsWereReadEv.exit.thread
                                        #   in Loop: Header=BB0_29 Depth=3
	andl	20(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r14d, %eax
	movl	$1, %r13d
	cmpb	$0, 32780(%rbx,%rax)
	jne	.LBB0_93
# BB#42:                                #   in Loop: Header=BB0_29 Depth=3
	cmpl	$256, %r14d             # imm = 0x100
	jne	.LBB0_77
# BB#43:                                #   in Loop: Header=BB0_29 Depth=3
	cmpl	$8, %ebp
	jb	.LBB0_51
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph.i.i166
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        #       Parent Loop BB0_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	32(%rsp), %rax
	cmpq	40(%rsp), %rax
	jb	.LBB0_48
# BB#45:                                #   in Loop: Header=BB0_44 Depth=4
.Ltmp28:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp29:
# BB#46:                                # %.noexc173
                                        #   in Loop: Header=BB0_44 Depth=4
	testb	%al, %al
	je	.LBB0_49
# BB#47:                                # %._crit_edge.i.i.i168
                                        #   in Loop: Header=BB0_44 Depth=4
	movq	32(%rsp), %rax
.LBB0_48:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i169
                                        #   in Loop: Header=BB0_44 Depth=4
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movzbl	(%rax), %eax
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_49:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i170
                                        #   in Loop: Header=BB0_44 Depth=4
	incl	80(%rsp)
	movb	$-1, %al
.LBB0_50:                               #   in Loop: Header=BB0_44 Depth=4
	movzbl	%al, %esi
	movl	24(%rsp), %ebp
	movl	$32, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	28(%rsp), %esi
	movl	%esi, 28(%rsp)
	addl	$-8, %ebp
	movl	%ebp, 24(%rsp)
	cmpl	$7, %ebp
	ja	.LBB0_44
.LBB0_51:                               # %.loopexit
                                        #   in Loop: Header=BB0_29 Depth=3
	movl	%esi, %eax
	andl	20(%rsp), %eax          # 4-byte Folded Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %ebp
	movl	%ebp, 24(%rsp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movl	%esi, 28(%rsp)
	cmpl	$2, %eax
	je	.LBB0_52
	jmp	.LBB0_19
.LBB0_77:                               # %.preheader196
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpl	$256, %r14d             # imm = 0x100
	jb	.LBB0_80
# BB#78:                                # %.lr.ph
                                        #   in Loop: Header=BB0_18 Depth=1
	leal	-1(%r12), %esi
	movl	$-1, %edi
	xorl	%eax, %eax
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB0_79:                               #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%esi, %edx
	cmovel	%eax, %edi
	movl	%edx, %edx
	movzbl	16396(%rbx,%rdx), %ecx
	movb	%cl, 24588(%rbx,%rax)
	incq	%rax
	movzwl	12(%rbx,%rdx,2), %edx
	cmpl	$255, %edx
	ja	.LBB0_79
	jmp	.LBB0_81
.LBB0_80:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$-1, %edi
	xorl	%eax, %eax
	movl	%r14d, %edx
.LBB0_81:                               # %._crit_edge
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movslq	%eax, %rcx
	movb	%dl, 24588(%rbx,%rcx)
	je	.LBB0_84
# BB#82:                                #   in Loop: Header=BB0_18 Depth=1
	leal	-1(%r12), %ecx
	movb	%dl, 16396(%rbx,%rcx)
	testl	%edi, %edi
	js	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_18 Depth=1
	movslq	%edi, %rcx
	movb	%dl, 24588(%rbx,%rcx)
.LBB0_84:                               # %_ZN10COutBuffer9WriteByteEh.exit.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%eax, %eax
	js	.LBB0_89
# BB#85:                                # %.lr.ph262.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	incl	%eax
	movslq	%eax, %rbp
	addq	$24587, %rbp            # imm = 0x600B
	.p2align	4, 0x90
.LBB0_86:                               # %.lr.ph262
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx,%rbp), %eax
	movq	104(%rsp), %rcx
	movl	112(%rsp), %edx
	leal	1(%rdx), %esi
	movl	%esi, 112(%rsp)
	movb	%al, (%rcx,%rdx)
	movl	112(%rsp), %eax
	cmpl	116(%rsp), %eax
	jne	.LBB0_88
# BB#87:                                #   in Loop: Header=BB0_86 Depth=2
.Ltmp19:
	leaq	104(%rsp), %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp20:
.LBB0_88:                               # %_ZN10COutBuffer9WriteByteEh.exit.backedge
                                        #   in Loop: Header=BB0_86 Depth=2
	leaq	-1(%rbp), %rax
	addq	$-24587, %rbp           # imm = 0x9FF5
	cmpq	$1, %rbp
	movq	%rax, %rbp
	jg	.LBB0_86
.LBB0_89:                               # %.preheader194
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpl	$8191, %r12d            # imm = 0x1FFF
	ja	.LBB0_10
	.p2align	4, 0x90
.LBB0_90:                               # %.lr.ph264
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %r12d
	cmpb	$0, 32780(%rbx,%r12)
	jne	.LBB0_11
# BB#91:                                #   in Loop: Header=BB0_90 Depth=2
	incl	%r12d
	cmpl	$8192, %r12d            # imm = 0x2000
	jb	.LBB0_90
	jmp	.LBB0_9
.LBB0_92:                               # %.critedge158.thread188
.Ltmp16:
	leaq	104(%rsp), %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r13d
.Ltmp17:
.LBB0_93:                               # %.critedge158.thread187
.Ltmp39:
	leaq	104(%rsp), %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp40:
# BB#94:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_96
# BB#95:
	movq	(%rdi), %rax
.Ltmp45:
	callq	*16(%rax)
.Ltmp46:
.LBB0_96:                               # %_ZN10COutBufferD2Ev.exit
.Ltmp57:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp58:
# BB#97:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_99
# BB#98:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_99:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit180
	movl	%r13d, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_100:
.Ltmp27:
	jmp	.LBB0_114
.LBB0_101:                              # %.loopexit.split-lp198
.Ltmp18:
	jmp	.LBB0_114
.LBB0_102:                              # %.loopexit.split-lp
.Ltmp24:
	jmp	.LBB0_114
.LBB0_103:
.Ltmp47:
	movq	%rax, %rbx
	jmp	.LBB0_117
.LBB0_104:
.Ltmp59:
	movq	%rax, %rbx
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_120
# BB#105:
	movq	(%rdi), %rax
.Ltmp60:
	callq	*16(%rax)
.Ltmp61:
	jmp	.LBB0_120
.LBB0_106:
.Ltmp62:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_107:
.Ltmp41:
	movq	%rax, %rbx
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_117
# BB#108:
	movq	(%rdi), %rax
.Ltmp42:
	callq	*16(%rax)
.Ltmp43:
	jmp	.LBB0_117
.LBB0_109:
.Ltmp44:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_110:                              # %.loopexit195
.Ltmp21:
	jmp	.LBB0_114
.LBB0_111:
.Ltmp12:
	jmp	.LBB0_114
.LBB0_112:
.Ltmp30:
	jmp	.LBB0_114
.LBB0_113:                              # %.loopexit197
.Ltmp15:
.LBB0_114:
	movq	%rax, %rbx
.Ltmp31:
	leaq	104(%rsp), %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp32:
# BB#115:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_117
# BB#116:
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
.LBB0_117:
.Ltmp48:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp49:
# BB#118:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_120
# BB#119:
	movq	(%rdi), %rax
.Ltmp54:
	callq	*16(%rax)
.Ltmp55:
.LBB0_120:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_121:
.Ltmp33:
	movq	%rax, %rbx
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_127
# BB#122:
	movq	(%rdi), %rax
.Ltmp34:
	callq	*16(%rax)
.Ltmp35:
	jmp	.LBB0_127
.LBB0_123:
.Ltmp36:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_124:
.Ltmp56:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_125:
.Ltmp50:
	movq	%rax, %rbx
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_127
# BB#126:
	movq	(%rdi), %rax
.Ltmp51:
	callq	*16(%rax)
.Ltmp52:
.LBB0_127:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_128:
.Ltmp53:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end0-_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\264\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\253\002"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp0          #   Call between .Ltmp0 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp11         #   Call between .Ltmp11 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp26         #   Call between .Ltmp26 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp60-.Ltmp58         #   Call between .Ltmp58 and .Ltmp60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	1                       #   On action: 1
	.long	.Ltmp55-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp34-.Ltmp55         #   Call between .Ltmp55 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
.Ltmp63:
	callq	_ZN9NCompress7NShrink8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp64:
.LBB2_6:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB2_1:
.Ltmp65:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$3, %ebx
	jne	.LBB2_3
# BB#2:
	callq	__cxa_begin_catch
	jmp	.LBB2_4
.LBB2_3:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB2_5
.LBB2_4:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB2_6
.LBB2_5:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB2_6
.Lfunc_end2:
	.size	_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end2-_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp63-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	5                       #   On action: 3
	.long	.Ltmp64-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp64     #   Call between .Ltmp64 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB3_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB3_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB3_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB3_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB3_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB3_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB3_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB3_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB3_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB3_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB3_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB3_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB3_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB3_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB3_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB3_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB3_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end3-_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress7NShrink8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress7NShrink8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress7NShrink8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoder6AddRefEv,@function
_ZN9NCompress7NShrink8CDecoder6AddRefEv: # @_ZN9NCompress7NShrink8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN9NCompress7NShrink8CDecoder6AddRefEv, .Lfunc_end4-_ZN9NCompress7NShrink8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress7NShrink8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress7NShrink8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress7NShrink8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoder7ReleaseEv,@function
_ZN9NCompress7NShrink8CDecoder7ReleaseEv: # @_ZN9NCompress7NShrink8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB5_2:
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN9NCompress7NShrink8CDecoder7ReleaseEv, .Lfunc_end5-_ZN9NCompress7NShrink8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end6-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN9NCompress7NShrink8CDecoderD0Ev,"axG",@progbits,_ZN9NCompress7NShrink8CDecoderD0Ev,comdat
	.weak	_ZN9NCompress7NShrink8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress7NShrink8CDecoderD0Ev,@function
_ZN9NCompress7NShrink8CDecoderD0Ev:     # @_ZN9NCompress7NShrink8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end7:
	.size	_ZN9NCompress7NShrink8CDecoderD0Ev, .Lfunc_end7-_ZN9NCompress7NShrink8CDecoderD0Ev
	.cfi_endproc

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress7NShrink8CDecoderE,@object # @_ZTVN9NCompress7NShrink8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress7NShrink8CDecoderE
	.p2align	3
_ZTVN9NCompress7NShrink8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress7NShrink8CDecoderE
	.quad	_ZN9NCompress7NShrink8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress7NShrink8CDecoder6AddRefEv
	.quad	_ZN9NCompress7NShrink8CDecoder7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN9NCompress7NShrink8CDecoderD0Ev
	.quad	_ZN9NCompress7NShrink8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.size	_ZTVN9NCompress7NShrink8CDecoderE, 64

	.type	_ZTSN9NCompress7NShrink8CDecoderE,@object # @_ZTSN9NCompress7NShrink8CDecoderE
	.globl	_ZTSN9NCompress7NShrink8CDecoderE
	.p2align	4
_ZTSN9NCompress7NShrink8CDecoderE:
	.asciz	"N9NCompress7NShrink8CDecoderE"
	.size	_ZTSN9NCompress7NShrink8CDecoderE, 30

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress7NShrink8CDecoderE,@object # @_ZTIN9NCompress7NShrink8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress7NShrink8CDecoderE
	.p2align	4
_ZTIN9NCompress7NShrink8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress7NShrink8CDecoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress7NShrink8CDecoderE, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
