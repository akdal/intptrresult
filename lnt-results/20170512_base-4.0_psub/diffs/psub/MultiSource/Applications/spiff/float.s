	.text
	.file	"float.bc"
	.globl	F_isfloat
	.p2align	4, 0x90
	.type	F_isfloat,@function
F_isfloat:                              # @F_isfloat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	xorl	%r15d, %r15d
	testl	%edx, %edx
	je	.LBB0_5
# BB#1:
	movb	(%rbx), %al
	cmpb	$45, %al
	je	.LBB0_3
# BB#2:
	cmpb	$43, %al
	jne	.LBB0_5
.LBB0_3:
	incq	%rbx
	movl	$1, %ebp
	jmp	.LBB0_6
.LBB0_5:
	xorl	%ebp, %ebp
.LBB0_6:                                # %.preheader47
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	(%rbx), %rax
	testb	$8, 1(%rcx,%rax,2)
	je	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph61
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movsbq	1(%rbx), %rax
	incq	%rbx
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB0_7
# BB#8:
	movl	$1, %r15d
.LBB0_9:                                # %._crit_edge62
	cmpb	$46, %al
	jne	.LBB0_11
# BB#10:
	incq	%rbx
	incl	%ebp
	jmp	.LBB0_12
.LBB0_11:
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jne	.LBB0_27
.LBB0_12:                               # %.preheader46
	movsbq	(%rbx), %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB0_15
# BB#13:                                # %._crit_edge
	testl	%r15d, %r15d
	jne	.LBB0_16
# BB#14:
	xorl	%eax, %eax
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movsbq	1(%rbx), %rdx
	incq	%rbx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge.thread
	xorl	%eax, %eax
	addb	$-68, %dl
	cmpb	$33, %dl
	ja	.LBB0_26
# BB#17:                                # %._crit_edge.thread
	movzbl	%dl, %edx
	movabsq	$12884901891, %rsi      # imm = 0x300000003
	btq	%rdx, %rsi
	jae	.LBB0_26
# BB#18:
	movb	1(%rbx), %dl
	cmpb	$45, %dl
	je	.LBB0_20
# BB#19:
	cmpb	$43, %dl
	jne	.LBB0_21
.LBB0_20:
	movb	2(%rbx), %dl
	addq	$2, %rbx
	movl	$2, %eax
	jmp	.LBB0_22
.LBB0_21:
	incq	%rbx
	movl	$1, %eax
.LBB0_22:
	movsbq	%dl, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB0_24
# BB#23:
	movl	%ebp, %eax
	jmp	.LBB0_27
.LBB0_24:                               # %.lr.ph.preheader
	incq	%rbx
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movsbq	(%rbx), %rdx
	incq	%rbx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB0_25
.LBB0_26:                               # %.loopexit
	addl	%ebp, %eax
.LBB0_27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	F_isfloat, .Lfunc_end0-F_isfloat
	.cfi_endproc

	.globl	F_atof
	.p2align	4, 0x90
	.type	F_atof,@function
F_atof:                                 # @F_atof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 272
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	R_makefloat
	movq	%rax, %r15
	movl	$0, 4(%r15)
	movb	$0, 16(%rsp)
	movb	(%r14), %al
	cmpb	$45, %al
	je	.LBB1_2
# BB#1:
	cmpb	$43, %al
	movq	%r14, %rbp
	je	.LBB1_3
	jmp	.LBB1_4
.LBB1_2:
	movl	$1, 4(%r15)
.LBB1_3:                                # %.preheader72.preheader
	leaq	1(%r14), %rbp
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader72
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbp), %rbx
	incq	%rbp
	cmpq	$48, %rbx
	je	.LBB1_4
# BB#5:                                 # %.preheader71
	movq	%r14, (%rsp)            # 8-byte Spill
	callq	__ctype_b_loc
	movq	%rax, %r13
	movq	(%r13), %rcx
	testb	$8, 1(%rcx,%rbx,2)
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jne	.LBB1_6
# BB#9:                                 # %.preheader70
	leaq	-1(%rbp), %r15
	cmpb	$46, %bl
	cmoveq	%rbp, %r15
	movb	(%r15), %al
	xorl	%r14d, %r14d
	cmpb	$48, %al
	jne	.LBB1_10
# BB#28:                                # %.lr.ph85.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph85
                                        # =>This Inner Loop Header: Depth=1
	movb	1(%r15,%rdx), %al
	incq	%rdx
	cmpb	$48, %al
	je	.LBB1_29
# BB#11:                                # %.preheader69.loopexit
	movl	%edx, %ebp
	negl	%ebp
	addq	%rdx, %r15
	xorl	%r14d, %r14d
	jmp	.LBB1_12
.LBB1_6:                                # %.lr.ph91.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movb	%bl, 16(%rsp,%r14)
	movb	$0, 17(%rsp,%r14)
	movq	(%r13), %rcx
	movsbq	(%rbp,%r14), %rbx
	incq	%r14
	testb	$8, 1(%rcx,%rbx,2)
	jne	.LBB1_7
# BB#8:                                 # %._crit_edge92..preheader69_crit_edge
	cmpb	$46, %bl
	leaq	-1(%rbp,%r14), %rax
	leaq	(%rbp,%r14), %r15
	cmovneq	%rax, %r15
	movb	(%r15), %al
	movl	%r14d, %ebp
	jmp	.LBB1_12
.LBB1_10:
	xorl	%ebp, %ebp
.LBB1_12:                               # %.preheader69
	movsbq	%al, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB1_15
# BB#13:                                # %.lr.ph79.preheader
	movslq	%r14d, %rcx
	leaq	17(%rsp,%rcx), %rcx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movb	%al, -1(%rcx)
	movb	$0, (%rcx)
	movq	(%r13), %rdx
	movsbq	1(%r15), %rax
	incq	%r15
	incq	%rcx
	incl	%r14d
	testb	$8, 1(%rdx,%rax,2)
	jne	.LBB1_14
.LBB1_15:                               # %._crit_edge80
	addb	$-68, %al
	cmpb	$33, %al
	ja	.LBB1_18
# BB#16:                                # %._crit_edge80
	movzbl	%al, %eax
	movabsq	$12884901891, %rcx      # imm = 0x300000003
	btq	%rax, %rcx
	jae	.LBB1_18
# BB#17:
	incq	%r15
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	addl	%ebp, %eax
	movl	%eax, %ebp
.LBB1_18:
	testl	%r12d, %r12d
	je	.LBB1_26
# BB#19:
	movb	(%r15), %al
	cmpb	$45, %al
	je	.LBB1_21
# BB#20:
	cmpb	$43, %al
	jne	.LBB1_22
.LBB1_21:
	incq	%r15
.LBB1_22:                               # %.preheader68
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%r15), %rcx
	incq	%r15
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB1_23
# BB#24:
	testb	%cl, %cl
	je	.LBB1_26
# BB#25:
	movl	$Z_err_buf, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB1_26:                               # %.preheader
	testl	%r14d, %r14d
	movq	8(%rsp), %rbx           # 8-byte Reload
	jle	.LBB1_33
# BB#27:                                # %.lr.ph.preheader
	movslq	%r14d, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$48, 16(%rsp,%rcx)
	jne	.LBB1_32
# BB#30:                                #   in Loop: Header=BB1_31 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB1_31
.LBB1_33:                               # %._crit_edge
	movq	8(%rbx), %rax
	movw	$48, (%rax)
	movq	$0, (%rbx)
	jmp	.LBB1_34
.LBB1_32:
	leaq	16(%rsp), %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_trimzeros
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	strcpy
	movl	%ebp, (%rbx)
.LBB1_34:
	movq	%rbx, %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	F_atof, .Lfunc_end1-F_atof
	.cfi_endproc

	.globl	F_floatsub
	.p2align	4, 0x90
	.type	F_floatsub,@function
F_floatsub:                             # @F_floatsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	F_floatsub.needinit(%rip), %al
	testb	%al, %al
	jne	.LBB2_2
# BB#1:
	xorl	%eax, %eax
	callq	R_makefloat
	movq	%rax, F_floatsub.result(%rip)
	movb	$1, F_floatsub.needinit(%rip)
.LBB2_2:
	movb	$0, F_floatsub.man1(%rip)
	movb	$0, F_floatsub.man2(%rip)
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	R_getexp
	movl	%eax, %r12d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	cmpl	%ebp, %r12d
	jge	.LBB2_3
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph90
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatsub.man1, %edi
	callq	strlen
	movw	$48, F_floatsub.man1(%rax)
	incl	%r12d
	cmpl	%r12d, %ebp
	jne	.LBB2_5
# BB#6:
	movl	%ebp, %r12d
	jmp	.LBB2_9
.LBB2_3:                                # %.preheader76
	jle	.LBB2_7
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph85
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatsub.man2, %edi
	callq	strlen
	movw	$48, F_floatsub.man2(%rax)
	incl	%ebp
	cmpl	%ebp, %r12d
	jne	.LBB2_4
	jmp	.LBB2_9
.LBB2_7:                                # %._crit_edge86
	cmpl	%ebp, %r12d
	je	.LBB2_9
# BB#8:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB2_9:                                # %._crit_edge86.thread
	movq	8(%r15), %rsi
	movl	$F_floatsub.man1, %edi
	callq	strcat
	movq	8(%r14), %rsi
	movl	$F_floatsub.man2, %edi
	callq	strcat
	movl	$F_floatsub.man1, %edi
	movl	$F_floatsub.man2, %esi
	callq	strcmp
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB2_10
# BB#11:
	movl	$F_floatsub.man1, %edi
	callq	strlen
	movq	%rax, %rbp
	movl	$F_floatsub.man2, %edi
	callq	strlen
	movq	%rax, %r15
	cmpq	%r15, %rbp
	jbe	.LBB2_15
# BB#12:
	subl	%r15d, %ebp
	testl	%ebp, %ebp
	jle	.LBB2_19
# BB#13:                                # %.lr.ph.i.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatsub.man2, %edi
	callq	strlen
	movw	$48, F_floatsub.man2(%rax)
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB2_14
	jmp	.LBB2_19
.LBB2_10:
	movq	F_floatsub.result(%rip), %rax
	movq	8(%rax), %rcx
	movw	$48, (%rcx)
	movl	$0, (%rax)
	jmp	.LBB2_36
.LBB2_15:
	jae	.LBB2_19
# BB#16:
	subl	%ebp, %r15d
	testl	%r15d, %r15d
	jle	.LBB2_19
# BB#17:                                # %.lr.ph.i73.preheader
	incl	%r15d
	.p2align	4, 0x90
.LBB2_18:                               # %.lr.ph.i73
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatsub.man1, %edi
	callq	strlen
	movw	$48, F_floatsub.man1(%rax)
	decl	%r15d
	cmpl	$1, %r15d
	jg	.LBB2_18
.LBB2_19:                               # %addzeros.exit
	movl	$F_floatsub.man1, %edi
	callq	strlen
	movq	%rax, %rbp
	movl	$F_floatsub.man2, %edi
	callq	strlen
	cmpq	%rax, %rbp
	je	.LBB2_21
# BB#20:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB2_21:
	testl	%r14d, %r14d
	movl	$F_floatsub.man2, %r14d
	movl	$F_floatsub.man1, %eax
	movl	$F_floatsub.man1, %ebp
	cmovsq	%r14, %rbp
	cmovsq	%rax, %r14
	movq	%rbp, %rdi
	callq	strlen
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	addq	%rax, %rcx
	movb	$0, F_floatsub.diff(%rax)
	testl	%ecx, %ecx
	js	.LBB2_27
# BB#22:                                # %.lr.ph83.preheader
	movslq	%ecx, %rax
	incq	%rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_23:                               # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	testl	%edi, %edi
	movzbl	-1(%rbp,%rax), %ebx
	je	.LBB2_29
# BB#24:                                #   in Loop: Header=BB2_23 Depth=1
	cmpb	$48, %bl
	cmovnel	%esi, %edi
	movb	$57, %cl
	je	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_23 Depth=1
	decb	%bl
	movl	%ebx, %ecx
.LBB2_26:                               #   in Loop: Header=BB2_23 Depth=1
	movzbl	-1(%r14,%rax), %edx
	jmp	.LBB2_32
	.p2align	4, 0x90
.LBB2_29:                               #   in Loop: Header=BB2_23 Depth=1
	movzbl	-1(%r14,%rax), %edx
	xorl	%edi, %edi
	cmpb	%dl, %bl
	setl	%r8b
	movb	$58, %cl
	jl	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_23 Depth=1
	movl	%ebx, %ecx
.LBB2_31:                               #   in Loop: Header=BB2_23 Depth=1
	movb	%r8b, %dil
.LBB2_32:                               #   in Loop: Header=BB2_23 Depth=1
	addb	$48, %cl
	subb	%dl, %cl
	movb	%cl, F_floatsub.diff-1(%rax)
	decq	%rax
	jg	.LBB2_23
.LBB2_27:                               # %.preheader
	cmpb	$48, F_floatsub.diff(%rip)
	jne	.LBB2_28
# BB#33:                                # %.lr.ph.preheader
	movl	$F_floatsub.diff, %esi
	.p2align	4, 0x90
.LBB2_34:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	decl	%r12d
	cmpb	$48, 1(%rsi)
	leaq	1(%rsi), %rsi
	je	.LBB2_34
	jmp	.LBB2_35
.LBB2_28:
	movl	$F_floatsub.diff, %esi
.LBB2_35:                               # %._crit_edge
	movq	F_floatsub.result(%rip), %rax
	movl	%r12d, (%rax)
	movq	8(%rax), %rdi
	callq	strcpy
	movq	F_floatsub.result(%rip), %rax
.LBB2_36:
	movl	$0, 4(%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	F_floatsub, .Lfunc_end2-F_floatsub
	.cfi_endproc

	.globl	F_floatcmp
	.p2align	4, 0x90
	.type	F_floatcmp,@function
F_floatcmp:                             # @F_floatcmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$0, (%rbx)
	jne	.LBB3_7
# BB#1:
	movq	8(%rbx), %rax
	cmpb	$48, (%rax)
	jne	.LBB3_7
# BB#2:
	cmpb	$0, 1(%rax)
	je	.LBB3_3
.LBB3_7:                                # %.thread
	cmpl	$0, (%r14)
	jne	.LBB3_11
# BB#8:
	movq	8(%r14), %rax
	cmpb	$48, (%rax)
	jne	.LBB3_11
# BB#9:
	cmpb	$0, 1(%rax)
	je	.LBB3_10
.LBB3_11:                               # %.thread110
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ecx
	movl	$-1, %eax
	cmpl	%ecx, %ebp
	jl	.LBB3_15
# BB#12:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ecx
	movl	$1, %eax
	cmpl	%ecx, %ebp
	jg	.LBB3_15
# BB#13:
	movq	8(%rbx), %rsi
	movl	$F_floatcmp.man1, %edi
	callq	strcpy
	movl	$F_floatcmp.man1, %edi
	xorl	%eax, %eax
	callq	S_trimzeros
	movq	8(%r14), %rsi
	movl	$F_floatcmp.man2, %edi
	callq	strcpy
	movl	$F_floatcmp.man2, %edi
	xorl	%eax, %eax
	callq	S_trimzeros
	movl	$F_floatcmp.man1, %edi
	movl	$F_floatcmp.man2, %esi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	strcmp                  # TAILCALL
.LBB3_3:
	cmpl	$0, (%r14)
	jne	.LBB3_14
# BB#4:
	movq	8(%r14), %rax
	cmpb	$48, (%rax)
	jne	.LBB3_14
# BB#5:
	cmpb	$0, 1(%rax)
	je	.LBB3_6
.LBB3_14:                               # %.thread108
	movl	$-1, %eax
	jmp	.LBB3_15
.LBB3_10:
	movl	$1, %eax
.LBB3_15:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB3_6:
	xorl	%eax, %eax
	jmp	.LBB3_15
.Lfunc_end3:
	.size	F_floatcmp, .Lfunc_end3-F_floatcmp
	.cfi_endproc

	.globl	F_floatmul
	.p2align	4, 0x90
	.type	F_floatmul,@function
F_floatmul:                             # @F_floatmul
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 96
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	F_floatmul.needinit(%rip), %al
	testb	%al, %al
	jne	.LBB4_2
# BB#1:
	xorl	%eax, %eax
	callq	R_makefloat
	movq	%rax, F_floatmul.result(%rip)
	movb	$1, F_floatmul.needinit(%rip)
.LBB4_2:
	cmpl	$0, (%r15)
	jne	.LBB4_5
# BB#3:
	movq	8(%r15), %rax
	cmpb	$48, (%rax)
	jne	.LBB4_5
# BB#4:
	cmpb	$0, 1(%rax)
	je	.LBB4_22
.LBB4_5:                                # %.thread
	cmpl	$0, (%r14)
	je	.LBB4_20
# BB#6:                                 # %.thread..thread122_crit_edge
	leaq	8(%r14), %rbx
	jmp	.LBB4_7
.LBB4_20:
	leaq	8(%r14), %rbx
	movq	8(%r14), %rax
	cmpb	$48, (%rax)
	jne	.LBB4_7
# BB#21:
	cmpb	$0, 1(%rax)
	je	.LBB4_22
.LBB4_7:                                # %.thread122
	movq	8(%r15), %rsi
	movl	$F_floatmul.man1, %edi
	callq	strcpy
	movq	(%rbx), %rsi
	movl	$F_floatmul.man2, %edi
	callq	strcpy
	movl	$F_floatmul.man1, %edi
	callq	strlen
	movq	%rax, %rbx
	movl	$F_floatmul.man2, %edi
	callq	strlen
	leaq	-1(%rax), %rsi
	xorl	%ecx, %ecx
	decq	%rbx
	movl	$0, %edx
	js	.LBB4_10
# BB#8:                                 # %.lr.ph134.preheader
	leaq	F_floatmul.man1(%rbx), %rdi
	xorl	%edx, %edx
	movl	$F_floatmul.man1, %ebp
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph134
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rdi), %ebx
	leal	-48(%rdx,%rbx), %edx
	decq	%rdi
	cmpq	%rbp, %rdi
	jae	.LBB4_9
.LBB4_10:                               # %.preheader124
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	testq	%rsi, %rsi
	js	.LBB4_13
# BB#11:                                # %.lr.ph130.preheader
	leaq	F_floatmul.man2-1(%rax), %rax
	xorl	%ecx, %ecx
	movl	$F_floatmul.man2, %esi
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph130
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rax), %edi
	leal	-48(%rcx,%rdi), %ecx
	decq	%rax
	cmpq	%rsi, %rax
	jae	.LBB4_12
.LBB4_13:                               # %._crit_edge131
	cmpl	%ecx, %edx
	movl	$F_floatmul.man2, %r13d
	movl	$F_floatmul.man1, %eax
	movl	$F_floatmul.man1, %ebp
	cmovgq	%r13, %rbp
	cmovgq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	S_trimzeros
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	S_trimzeros
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbp
	addl	%ebp, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	decq	%rbp
	movw	$48, F_floatmul.prod(%rip)
	js	.LBB4_36
# BB#14:                                # %.preheader.preheader
	addq	8(%rsp), %rbp           # 8-byte Folded Reload
	movl	$F_floatmul.prod, %r15d
	.p2align	4, 0x90
.LBB4_15:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
                                        #       Child Loop BB4_24 Depth 3
	cmpb	$49, (%rbp)
	jl	.LBB4_35
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_15 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph
                                        #   Parent Loop BB4_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_24 Depth 3
	movl	$F_floatmul.prod, %edi
	callq	strlen
	movq	%rax, %r14
	decq	%r14
	movq	%r13, %rdi
	callq	strlen
	decq	%rax
	movb	$0, _F_stradd.result+199(%rip)
	testq	%r14, %r14
	setns	%bl
	testq	%rax, %rax
	jns	.LBB4_23
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB4_17 Depth=2
	movq	%r14, %rcx
	shrq	$63, %rcx
	testb	%cl, %cl
	je	.LBB4_23
# BB#19:                                #   in Loop: Header=BB4_17 Depth=2
	movl	$_F_stradd.result+198, %esi
	jmp	.LBB4_33
	.p2align	4, 0x90
.LBB4_23:                               # %.critedge.preheader.i
                                        #   in Loop: Header=BB4_17 Depth=2
	movq	%rbp, %r9
	leaq	(%r13,%rax), %r8
	shrq	$63, %rax
	leaq	F_floatmul.prod(%r14), %rdx
	xorl	%edi, %edi
	movl	$_F_stradd.result+198, %esi
	.p2align	4, 0x90
.LBB4_24:                               # %.critedge.i
                                        #   Parent Loop BB4_15 Depth=1
                                        #     Parent Loop BB4_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %bl
	je	.LBB4_25
# BB#26:                                #   in Loop: Header=BB4_24 Depth=3
	movsbl	(%rdx), %ebx
	addl	$-48, %ebx
	decq	%rdx
	jmp	.LBB4_27
	.p2align	4, 0x90
.LBB4_25:                               #   in Loop: Header=BB4_24 Depth=3
	xorl	%ebx, %ebx
.LBB4_27:                               #   in Loop: Header=BB4_24 Depth=3
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.LBB4_29
# BB#28:                                #   in Loop: Header=BB4_24 Depth=3
	movsbl	(%r8), %ecx
	addl	$-48, %ecx
	decq	%r8
.LBB4_29:                               #   in Loop: Header=BB4_24 Depth=3
	addl	%edi, %ebx
	leal	(%rbx,%rcx), %ebp
	xorl	%edi, %edi
	cmpl	$9, %ebp
	setg	%dil
	leal	246(%rcx,%rbx), %eax
	cmovlel	%ebp, %eax
	addl	$48, %eax
	movb	%al, (%rsi)
	decq	%rsi
	cmpq	%r15, %rdx
	setae	%bl
	setb	%cl
	cmpq	%r13, %r8
	setb	%al
	jae	.LBB4_24
# BB#30:                                #   in Loop: Header=BB4_24 Depth=3
	testb	%cl, %cl
	je	.LBB4_24
# BB#31:                                # %._crit_edge.i
                                        #   in Loop: Header=BB4_17 Depth=2
	cmpl	$10, %ebp
	jl	.LBB4_32
# BB#38:                                #   in Loop: Header=BB4_17 Depth=2
	movb	$49, (%rsi)
	movq	%r9, %rbp
	jmp	.LBB4_34
	.p2align	4, 0x90
.LBB4_32:                               #   in Loop: Header=BB4_17 Depth=2
	movq	%r9, %rbp
.LBB4_33:                               # %._crit_edge.thread.i
                                        #   in Loop: Header=BB4_17 Depth=2
	incq	%rsi
.LBB4_34:                               # %_F_stradd.exit
                                        #   in Loop: Header=BB4_17 Depth=2
	movl	$F_floatmul.prod, %edi
	callq	strcpy
	incl	%r12d
	movsbl	(%rbp), %eax
	addl	$-48, %eax
	cmpl	%eax, %r12d
	jl	.LBB4_17
.LBB4_35:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_15 Depth=1
	movq	%r13, %rdi
	callq	strlen
	movw	$48, (%r13,%rax)
	decq	%rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jae	.LBB4_15
.LBB4_36:                               # %._crit_edge
	movq	F_floatmul.result(%rip), %rax
	movq	8(%rax), %rdi
	movl	$F_floatmul.prod, %esi
	callq	strcpy
	xorl	%eax, %eax
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	R_getexp
	subl	16(%rsp), %ebx          # 4-byte Folded Reload
	addl	%eax, %ebx
	movl	$F_floatmul.prod, %edi
	callq	strlen
	movq	%rax, %rcx
	addl	%ebx, %ecx
	movq	F_floatmul.result(%rip), %rax
	movl	%ecx, (%rax)
	movl	4(%r14), %ecx
	xorl	%edx, %edx
	cmpl	4(%rbp), %ecx
	setne	%dl
	movl	%edx, 4(%rax)
.LBB4_37:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_22:
	movq	F_floatmul.result(%rip), %rax
	movq	8(%rax), %rcx
	movw	$48, (%rcx)
	movq	$0, (%rax)
	jmp	.LBB4_37
.Lfunc_end4:
	.size	F_floatmul, .Lfunc_end4-F_floatmul
	.cfi_endproc

	.globl	_F_xor
	.p2align	4, 0x90
	.type	_F_xor,@function
_F_xor:                                 # @_F_xor
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	sete	%al
	testl	%esi, %esi
	setne	%cl
	movl	%eax, %edx
	orb	%cl, %dl
	andb	%cl, %al
	xorb	$1, %dl
	orb	%al, %dl
	movzbl	%dl, %eax
	retq
.Lfunc_end5:
	.size	_F_xor, .Lfunc_end5-_F_xor
	.cfi_endproc

	.globl	_F_ABSDIFF
	.p2align	4, 0x90
	.type	_F_ABSDIFF,@function
_F_ABSDIFF:                             # @_F_ABSDIFF
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	negl	%eax
	cmovll	%edi, %eax
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	movl	%ecx, %edx
	subl	%eax, %edx
	subl	%ecx, %eax
	cmovll	%edx, %eax
	retq
.Lfunc_end6:
	.size	_F_ABSDIFF, .Lfunc_end6-_F_ABSDIFF
	.cfi_endproc

	.globl	F_floatmagadd
	.p2align	4, 0x90
	.type	F_floatmagadd,@function
F_floatmagadd:                          # @F_floatmagadd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 64
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movb	F_floatmagadd.needinit(%rip), %al
	testb	%al, %al
	jne	.LBB7_2
# BB#1:
	xorl	%eax, %eax
	callq	R_makefloat
	movq	%rax, F_floatmagadd.result(%rip)
	movb	$1, F_floatmagadd.needinit(%rip)
.LBB7_2:
	movb	$0, F_floatmagadd.man1(%rip)
	movb	$0, F_floatmagadd.man2(%rip)
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	testl	%ebp, %ebp
	setns	%r15b
	testl	%eax, %eax
	sets	%bl
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	R_getexp
	movl	%eax, %r13d
	xorl	%eax, %eax
	xorb	%r15b, %bl
	je	.LBB7_3
# BB#4:
	movq	%r12, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	negl	%ebp
	testl	%r13d, %r13d
	cmovnsl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %r15d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ebx
	negl	%ebx
	testl	%r15d, %r15d
	cmovnsl	%eax, %ebx
	addl	%ebp, %ebx
	testl	%ebx, %ebx
	jg	.LBB7_6
	jmp	.LBB7_10
.LBB7_3:
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%r13d, %ebx
	negl	%ebx
	cmovll	%r13d, %ebx
	movl	%ecx, %eax
	subl	%ebx, %eax
	subl	%ecx, %ebx
	cmovll	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB7_10
.LBB7_6:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movslq	%ebx, %r15
	cmpl	%eax, %ebp
	jge	.LBB7_9
# BB#7:
	movq	8(%r12), %rdi
	jmp	.LBB7_8
.LBB7_10:
	movq	8(%r12), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	8(%r14), %rdi
	callq	strlen
	cmpq	%rax, %rbp
	movq	%r14, %rax
	cmovaq	%r12, %rax
	movq	8(%rax), %rdi
	callq	strlen
	cmpq	$199, %rax
	jae	.LBB7_11
	jmp	.LBB7_12
.LBB7_9:
	movq	8(%r14), %rdi
.LBB7_8:
	callq	strlen
	addq	%r15, %rax
	cmpq	$200, %rax
	jb	.LBB7_12
.LBB7_11:
	movabsq	$28539428274794087, %rax # imm = 0x65647574696E67
	movq	%rax, Z_err_buf+32(%rip)
	movups	.L.str.5+16(%rip), %xmm0
	movups	%xmm0, Z_err_buf+16(%rip)
	movups	.L.str.5(%rip), %xmm0
	movups	%xmm0, Z_err_buf(%rip)
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB7_12:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	R_getexp
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	cmpl	%eax, %ebp
	jge	.LBB7_17
# BB#13:
	testl	%ebx, %ebx
	movq	%r14, %rdi
	jle	.LBB7_21
# BB#14:                                # %.lr.ph.i.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB7_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatmagadd.man1, %edi
	callq	strlen
	movw	$48, F_floatmagadd.man1(%rax)
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB7_15
# BB#16:
	movq	%r14, %rdi
	jmp	.LBB7_21
.LBB7_17:
	testl	%ebx, %ebx
	movq	%r12, %rdi
	jle	.LBB7_21
# BB#18:                                # %.lr.ph.i48.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i48
                                        # =>This Inner Loop Header: Depth=1
	movl	$F_floatmagadd.man2, %edi
	callq	strlen
	movw	$48, F_floatmagadd.man2(%rax)
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB7_19
# BB#20:
	movq	%r12, %rdi
.LBB7_21:                               # %addzeros.exit
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	R_getexp
	movl	%eax, %r15d
	movq	8(%r12), %rsi
	movl	$F_floatmagadd.man1, %r13d
	movl	$F_floatmagadd.man1, %edi
	callq	strcat
	movq	8(%r14), %rsi
	movl	$F_floatmagadd.man2, %r14d
	movl	$F_floatmagadd.man2, %edi
	callq	strcat
	movl	$F_floatmagadd.man1, %edi
	callq	strlen
	movq	%rax, %r12
	movl	$F_floatmagadd.man2, %edi
	callq	strlen
	movq	%rax, %rbx
	cmpq	%rbx, %r12
	cmovaq	%r13, %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r14
	decq	%r12
	decq	%rbx
	movb	$0, _F_stradd.result+199(%rip)
	testq	%r12, %r12
	setns	%dl
	testq	%rbx, %rbx
	jns	.LBB7_25
# BB#22:                                # %addzeros.exit
	movq	%r12, %rax
	shrq	$63, %rax
	testb	%al, %al
	je	.LBB7_25
# BB#23:
	movl	$_F_stradd.result+198, %esi
	jmp	.LBB7_24
.LBB7_25:                               # %.critedge.preheader.i
	leaq	F_floatmagadd.man2(%rbx), %r10
	shrq	$63, %rbx
	leaq	F_floatmagadd.man1(%r12), %r11
	movl	$_F_stradd.result+198, %esi
	movl	$F_floatmagadd.man1, %r8d
	movl	$F_floatmagadd.man2, %r9d
	.p2align	4, 0x90
.LBB7_26:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	testb	$1, %dl
	je	.LBB7_27
# BB#28:                                #   in Loop: Header=BB7_26 Depth=1
	movsbl	(%r11), %edx
	addl	$-48, %edx
	decq	%r11
	testb	$1, %bl
	je	.LBB7_30
	jmp	.LBB7_31
	.p2align	4, 0x90
.LBB7_27:                               #   in Loop: Header=BB7_26 Depth=1
	xorl	%edx, %edx
	testb	$1, %bl
	jne	.LBB7_31
.LBB7_30:                               #   in Loop: Header=BB7_26 Depth=1
	movsbl	(%r10), %edi
	addl	$-48, %edi
	decq	%r10
.LBB7_31:                               #   in Loop: Header=BB7_26 Depth=1
	addl	%ebp, %edx
	leal	(%rdx,%rdi), %eax
	xorl	%ebp, %ebp
	cmpl	$9, %eax
	setg	%bpl
	leal	246(%rdi,%rdx), %ecx
	cmovlel	%eax, %ecx
	addl	$48, %ecx
	movb	%cl, (%rsi)
	decq	%rsi
	cmpq	%r8, %r11
	setae	%dl
	setb	%cl
	cmpq	%r9, %r10
	setb	%bl
	jae	.LBB7_26
# BB#32:                                #   in Loop: Header=BB7_26 Depth=1
	testb	%cl, %cl
	je	.LBB7_26
# BB#33:                                # %._crit_edge.i
	cmpl	$10, %eax
	jl	.LBB7_24
# BB#34:
	movb	$49, (%rsi)
	jmp	.LBB7_35
.LBB7_24:                               # %._crit_edge.thread.i
	incq	%rsi
.LBB7_35:                               # %_F_stradd.exit
	movl	$F_floatmagadd.man1, %ebx
	movl	$F_floatmagadd.man1, %edi
	callq	strcpy
	movl	$F_floatmagadd.man1, %edi
	callq	strlen
	movq	%rax, %rbp
	subl	%r14d, %r15d
	addl	%r15d, %ebp
	cmpb	$48, F_floatmagadd.man1(%rip)
	jne	.LBB7_38
# BB#36:                                # %.lr.ph.preheader
	movl	$F_floatmagadd.man1, %ebx
	.p2align	4, 0x90
.LBB7_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebp
	cmpb	$48, 1(%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB7_37
.LBB7_38:                               # %._crit_edge
	movq	F_floatmagadd.result(%rip), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	F_floatmagadd.result(%rip), %rax
	movl	%ebp, (%rax)
	movl	$0, 4(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	F_floatmagadd, .Lfunc_end7-F_floatmagadd
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"didn't use up all of %s in atocf"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"0"
	.size	.L.str.1, 2

	.type	F_floatsub.result,@object # @F_floatsub.result
	.local	F_floatsub.result
	.comm	F_floatsub.result,8,8
	.type	F_floatsub.needinit,@object # @F_floatsub.needinit
	.local	F_floatsub.needinit
	.comm	F_floatsub.needinit,1,4
	.type	F_floatsub.man1,@object # @F_floatsub.man1
	.local	F_floatsub.man1
	.comm	F_floatsub.man1,200,16
	.type	F_floatsub.man2,@object # @F_floatsub.man2
	.local	F_floatsub.man2
	.comm	F_floatsub.man2,200,16
	.type	F_floatsub.diff,@object # @F_floatsub.diff
	.local	F_floatsub.diff
	.comm	F_floatsub.diff,200,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"mantissas didn't get lined up properly in floatsub"
	.size	.L.str.2, 51

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"lengths not equal in F_floatsub"
	.size	.L.str.3, 32

	.type	F_floatcmp.man1,@object # @F_floatcmp.man1
	.local	F_floatcmp.man1
	.comm	F_floatcmp.man1,200,16
	.type	F_floatcmp.man2,@object # @F_floatcmp.man2
	.local	F_floatcmp.man2
	.comm	F_floatcmp.man2,200,16
	.type	F_floatmul.prod,@object # @F_floatmul.prod
	.local	F_floatmul.prod
	.comm	F_floatmul.prod,200,16
	.type	F_floatmul.man1,@object # @F_floatmul.man1
	.local	F_floatmul.man1
	.comm	F_floatmul.man1,200,16
	.type	F_floatmul.man2,@object # @F_floatmul.man2
	.local	F_floatmul.man2
	.comm	F_floatmul.man2,200,16
	.type	F_floatmul.result,@object # @F_floatmul.result
	.local	F_floatmul.result
	.comm	F_floatmul.result,8,8
	.type	F_floatmul.needinit,@object # @F_floatmul.needinit
	.local	F_floatmul.needinit
	.comm	F_floatmul.needinit,1,4
	.type	F_floatmagadd.result,@object # @F_floatmagadd.result
	.local	F_floatmagadd.result
	.comm	F_floatmagadd.result,8,8
	.type	F_floatmagadd.needinit,@object # @F_floatmagadd.needinit
	.local	F_floatmagadd.needinit
	.comm	F_floatmagadd.needinit,1,4
	.type	F_floatmagadd.man1,@object # @F_floatmagadd.man1
	.local	F_floatmagadd.man1
	.comm	F_floatmagadd.man1,200,16
	.type	F_floatmagadd.man2,@object # @F_floatmagadd.man2
	.local	F_floatmagadd.man2
	.comm	F_floatmagadd.man2,200,16
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"numbers differ by too much in magnitude"
	.size	.L.str.5, 40

	.type	_F_stradd.result,@object # @_F_stradd.result
	.local	_F_stradd.result
	.comm	_F_stradd.result,200,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
