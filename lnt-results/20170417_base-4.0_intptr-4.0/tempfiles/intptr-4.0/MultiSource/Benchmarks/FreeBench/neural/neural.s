	.text
	.file	"neural.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1056964608              # float 0.5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$2, %ebp
	jne	.LBB0_1
# BB#2:
	movq	8(%rbx), %rdi
	movl	$.L.str.6, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_3
# BB#5:
	leaq	48(%rsp), %rbx
	movl	$99, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, NNWIDTH(%rip)
	movl	$99, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, NNHEIGHT(%rip)
	movl	$99, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, NUMPATS(%rip)
	movl	NNWIDTH(%rip), %esi
	movl	NNHEIGHT(%rip), %edx
	movl	%edx, %eax
	imull	%esi, %eax
	movl	%eax, NNTOT(%rip)
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	NUMPATS(%rip), %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, vnames(%rip)
	leaq	(,%r14,4), %rdi
	callq	malloc
	movq	%rax, stored(%rip)
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#6:
	testq	%rax, %rax
	je	.LBB0_12
# BB#7:
	movslq	NNTOT(%rip), %r12
	leaq	(,%r12,8), %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, Tmatrix(%rip)
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#8:                                 # %.preheader57
	testl	%r12d, %r12d
	jle	.LBB0_13
# BB#9:                                 # %.lr.ph68.preheader
	leaq	(,%r12,4), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph68
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, (%rbx,%rbp,8)
	movq	Tmatrix(%rip), %rbx
	cmpq	$0, (%rbx,%rbp,8)
	je	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_11 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB0_11
.LBB0_13:                               # %._crit_edge69
	leaq	(,%r14,8), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, vectors(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, newvectors(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, generators(%rip)
	testq	%rax, %rax
	je	.LBB0_12
# BB#14:                                # %._crit_edge69
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#15:                                # %._crit_edge69
	testq	%r12, %r12
	je	.LBB0_12
# BB#16:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_22
# BB#17:                                # %.lr.ph
	movslq	NNTOT(%rip), %r14
	shlq	$2, %r14
	movslq	NUMPATS(%rip), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, (%rbx,%rbp,8)
	movq	%r14, %rdi
	callq	malloc
	movq	newvectors(%rip), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	%r14, %rdi
	callq	malloc
	movq	generators(%rip), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	vectors(%rip), %rbx
	cmpq	$0, (%rbx,%rbp,8)
	je	.LBB0_12
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	movq	newvectors(%rip), %rax
	cmpq	$0, (%rax,%rbp,8)
	je	.LBB0_12
# BB#21:                                #   in Loop: Header=BB0_19 Depth=1
	movq	generators(%rip), %rax
	cmpq	$0, (%rax,%rbp,8)
	je	.LBB0_12
# BB#18:                                #   in Loop: Header=BB0_19 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB0_19
.LBB0_22:                               # %._crit_edge
	movslq	NNWIDTH(%rip), %rdi
	addq	$2, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#23:                                # %.preheader.i
	cmpl	$0, NUMPATS(%rip)
	jle	.LBB0_33
# BB#24:                                # %.lr.ph38.i.preheader
	movl	$1, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph38.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_27 Depth 2
                                        #       Child Loop BB0_29 Depth 3
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	movb	(%rbx), %al
	movq	vnames(%rip), %rcx
	movb	%al, (%rcx,%r14)
	cmpl	$0, NNHEIGHT(%rip)
	jle	.LBB0_32
# BB#26:                                # %.lr.ph35.i.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph35.i
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_29 Depth 3
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	cmpl	$0, NNWIDTH(%rip)
	jle	.LBB0_31
# BB#28:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_27 Depth=2
	movslq	%r12d, %rax
	movq	vectors(%rip), %rcx
	shlq	$2, %rax
	addq	(%rcx,%r14,8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.i
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$88, (%rbx,%rcx)
	movl	$-1, %edx
	cmovel	%ebp, %edx
	movl	%edx, (%rax,%rcx,4)
	incq	%rcx
	movslq	NNWIDTH(%rip), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB0_29
# BB#30:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_27 Depth=2
	addl	%ecx, %r12d
.LBB0_31:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_27 Depth=2
	incl	%r13d
	cmpl	NNHEIGHT(%rip), %r13d
	jl	.LBB0_27
.LBB0_32:                               # %._crit_edge36.i
                                        #   in Loop: Header=BB0_25 Depth=1
	incq	%r14
	movslq	NUMPATS(%rip), %rax
	cmpq	%rax, %r14
	jl	.LBB0_25
.LBB0_33:                               # %readvector.exit
	movq	%r15, %rdi
	callq	fclose
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr, %edi
	callq	puts
	movslq	NUMPATS(%rip), %r8
	testq	%r8, %r8
	jle	.LBB0_68
# BB#34:                                # %.lr.ph58.i
	cmpl	$1, %r8d
	je	.LBB0_68
# BB#35:                                # %.lr.ph.i34.lr.ph
	movslq	NNTOT(%rip), %r10
	testl	%r10d, %r10d
	jle	.LBB0_64
# BB#36:                                # %.lr.ph.i34.us.preheader
	movq	vectors(%rip), %r15
	leaq	-8(%r10), %rax
	shrq	$3, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	1(%rax), %eax
	movq	%r10, %r13
	andq	$-8, %r13
	andl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$1, %r11d
	movdqa	.LCPI0_0(%rip), %xmm8   # xmm8 = [1,1,1,1]
	xorl	%ecx, %ecx
	movq	%r8, 40(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i34.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_49 Depth 3
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_61 Depth 3
	movq	(%r15,%rcx,8), %r12
	leaq	48(%r12), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_38
.LBB0_43:                               # %vector.body92.prol
                                        #   in Loop: Header=BB0_38 Depth=2
	movdqu	(%r12), %xmm3
	movdqu	16(%r12), %xmm4
	movdqu	(%r9), %xmm1
	movdqu	16(%r9), %xmm2
	pcmpeqd	%xmm3, %xmm1
	pandn	%xmm8, %xmm1
	pcmpeqd	%xmm4, %xmm2
	pandn	%xmm8, %xmm2
	movl	$8, %ecx
	movdqa	%xmm1, %xmm3
	movdqa	%xmm2, %xmm4
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_47
.LBB0_45:                               # %vector.body92.preheader.new
                                        #   in Loop: Header=BB0_38 Depth=2
	movq	%r13, %rdx
	subq	%rcx, %rdx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rcx,4), %rdi
	leaq	48(%r9,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB0_46:                               # %vector.body92
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-48(%rdi), %xmm5
	movdqu	-32(%rdi), %xmm6
	movdqu	-16(%rdi), %xmm10
	movdqu	(%rdi), %xmm9
	movdqu	-48(%rbx), %xmm0
	movdqu	-32(%rbx), %xmm7
	movdqu	-16(%rbx), %xmm1
	movdqu	(%rbx), %xmm2
	pcmpeqd	%xmm5, %xmm0
	pandn	%xmm8, %xmm0
	pcmpeqd	%xmm6, %xmm7
	pandn	%xmm8, %xmm7
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm7
	pcmpeqd	%xmm10, %xmm1
	pandn	%xmm8, %xmm1
	pcmpeqd	%xmm9, %xmm2
	pandn	%xmm8, %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm7, %xmm2
	addq	$64, %rdi
	addq	$64, %rbx
	addq	$-16, %rdx
	movdqa	%xmm1, %xmm3
	movdqa	%xmm2, %xmm4
	jne	.LBB0_46
.LBB0_47:                               # %middle.block93
                                        #   in Loop: Header=BB0_38 Depth=2
	cmpq	%r13, %r10
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	movq	%r13, %rbx
	jne	.LBB0_48
	jmp	.LBB0_50
.LBB0_55:                               # %vector.body.prol
                                        #   in Loop: Header=BB0_38 Depth=2
	movdqu	(%r12), %xmm0
	movdqu	16(%r12), %xmm3
	movdqu	(%r9), %xmm2
	movdqu	16(%r9), %xmm4
	pxor	%xmm1, %xmm1
	psubd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	psubd	%xmm4, %xmm2
	pcmpeqd	%xmm0, %xmm1
	pandn	%xmm8, %xmm1
	pcmpeqd	%xmm3, %xmm2
	pandn	%xmm8, %xmm2
	movl	$8, %ecx
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_59
.LBB0_57:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_38 Depth=2
	movq	%r13, %rbx
	subq	%rcx, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rcx,4), %rdi
	leaq	48(%r9,%rcx,4), %r8
	.p2align	4, 0x90
.LBB0_58:                               # %vector.body
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-48(%r8), %xmm0
	movdqu	-32(%r8), %xmm3
	movdqu	-16(%r8), %xmm10
	movdqu	(%r8), %xmm9
	pxor	%xmm6, %xmm6
	psubd	%xmm0, %xmm6
	pxor	%xmm0, %xmm0
	psubd	%xmm3, %xmm0
	movdqu	-48(%rdi), %xmm3
	movdqu	-32(%rdi), %xmm7
	movdqu	-16(%rdi), %xmm5
	movdqu	(%rdi), %xmm4
	pcmpeqd	%xmm6, %xmm3
	pandn	%xmm8, %xmm3
	pcmpeqd	%xmm0, %xmm7
	pandn	%xmm8, %xmm7
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm7
	pxor	%xmm1, %xmm1
	psubd	%xmm10, %xmm1
	pxor	%xmm2, %xmm2
	psubd	%xmm9, %xmm2
	pcmpeqd	%xmm5, %xmm1
	pandn	%xmm8, %xmm1
	pcmpeqd	%xmm4, %xmm2
	pandn	%xmm8, %xmm2
	paddd	%xmm3, %xmm1
	paddd	%xmm7, %xmm2
	addq	$64, %rdi
	addq	$64, %r8
	addq	$-16, %rbx
	jne	.LBB0_58
.LBB0_59:                               # %middle.block
                                        #   in Loop: Header=BB0_38 Depth=2
	cmpq	%r13, %r10
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	movq	%r13, %rbx
	movq	40(%rsp), %r8           # 8-byte Reload
	jne	.LBB0_60
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_38:                               # %.lr.ph.split.us.i.us
                                        #   Parent Loop BB0_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_49 Depth 3
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_61 Depth 3
	cmpl	$8, %r10d
	movq	(%r15,%r11,8), %r9
	jb	.LBB0_39
# BB#40:                                # %min.iters.checked96
                                        #   in Loop: Header=BB0_38 Depth=2
	testq	%r13, %r13
	je	.LBB0_39
# BB#41:                                # %vector.body92.preheader
                                        #   in Loop: Header=BB0_38 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_38 Depth=2
	pxor	%xmm3, %xmm3
                                        # implicit-def: %XMM1
                                        # implicit-def: %XMM2
	xorl	%ecx, %ecx
	pxor	%xmm4, %xmm4
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_45
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_38 Depth=2
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB0_48:                               # %scalar.ph94.preheader
                                        #   in Loop: Header=BB0_38 Depth=2
	movq	%r10, %rcx
	subq	%rbx, %rcx
	leaq	(%r9,%rbx,4), %rdx
	leaq	(%r12,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB0_49:                               # %scalar.ph94
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx), %eax
	xorl	%ebp, %ebp
	cmpl	(%rdx), %eax
	setne	%bpl
	addl	%ebp, %edi
	addq	$4, %rdx
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_49
.LBB0_50:                               # %.lr.ph.i22.us.preheader.i.us
                                        #   in Loop: Header=BB0_38 Depth=2
	xorl	%r14d, %r14d
	cmpl	$2, %edi
	setl	%r14b
	cmpl	$8, %r10d
	jb	.LBB0_51
# BB#52:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_38 Depth=2
	testq	%r13, %r13
	je	.LBB0_51
# BB#53:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_38 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_38 Depth=2
	pxor	%xmm1, %xmm1
	xorl	%ecx, %ecx
	pxor	%xmm2, %xmm2
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_57
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_51:                               #   in Loop: Header=BB0_38 Depth=2
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB0_60:                               # %.lr.ph.i22.us.i.us.preheader
                                        #   in Loop: Header=BB0_38 Depth=2
	movq	%r10, %rcx
	subq	%rbx, %rcx
	leaq	(%r9,%rbx,4), %rax
	leaq	(%r12,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB0_61:                               # %.lr.ph.i22.us.i.us
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx), %ebp
	xorl	%edx, %edx
	addl	(%rax), %ebp
	setne	%dl
	addl	%edx, %edi
	addq	$4, %rax
	addq	$4, %rbx
	decq	%rcx
	jne	.LBB0_61
.LBB0_62:                               # %hamming.exit29.us.i.us
                                        #   in Loop: Header=BB0_38 Depth=2
	addl	%esi, %r14d
	xorl	%esi, %esi
	cmpl	$2, %edi
	setl	%sil
	addl	%r14d, %esi
	incq	%r11
	cmpq	%r8, %r11
	jl	.LBB0_38
# BB#63:                                # %.loopexit.i.us
                                        #   in Loop: Header=BB0_37 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %r11
	cmpq	%r8, %r11
	jl	.LBB0_37
	jmp	.LBB0_66
.LBB0_64:                               # %.lr.ph.i34.preheader
	cmpl	$1, %r8d
	movl	$2, %eax
	cmovgl	%r8d, %eax
	leal	-4(%rax,%rax), %edi
	xorl	%esi, %esi
	movl	$3, %eax
	movl	$-3, %ecx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_65:                               # %.lr.ph.i34
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%eax, %r8d
	leal	2(%rsi,%rdi), %esi
	movl	%eax, %edi
	cmovgel	%r8d, %edi
	addl	%ecx, %edi
	addl	%edi, %edi
	incq	%rdx
	incl	%eax
	decl	%ecx
	cmpq	%r8, %rdx
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	jl	.LBB0_65
.LBB0_66:                               # %._crit_edge.i35
	testl	%esi, %esi
	je	.LBB0_68
# BB#67:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
.LBB0_68:                               # %checkham.exit
	movl	$.Lstr.1, %edi
	callq	puts
	movl	NNTOT(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_69
# BB#70:                                # %.preheader3.us.preheader.i
	movq	Tmatrix(%rip), %r13
	movl	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %r15
	leaq	-1(%r14), %r12
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	andq	$7, %rbx
	je	.LBB0_72
	.p2align	4, 0x90
.LBB0_71:                               # %.preheader3.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_71
.LBB0_72:                               # %.preheader3.us.i.prol.loopexit
	cmpq	$7, %r12
	jae	.LBB0_74
# BB#73:
	xorl	%r12d, %r12d
	jmp	.LBB0_76
.LBB0_69:
	movl	$2, %r12d
	testl	%r14d, %r14d
	jg	.LBB0_78
	jmp	.LBB0_85
.LBB0_74:                               # %.preheader3.us.preheader.i.new
	subq	%rbp, %r14
	leaq	56(%r13,%rbp,8), %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_75:                               # %.preheader3.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r14
	jne	.LBB0_75
	jmp	.LBB0_76
.LBB0_1:
	movq	stderr(%rip), %rdi
	decl	%ebp
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%rbx), %rdx
	movl	$.L.str.5, %esi
	jmp	.LBB0_4
.LBB0_3:
	movq	stderr(%rip), %rdi
	movq	8(%rbx), %rdx
	movl	$.L.str.7, %esi
.LBB0_4:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_12:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_76:                               # %.preheader1.ithread-pre-split
	movl	NNTOT(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_85
.LBB0_78:                               # %.preheader.us.preheader.i
	movq	Tmatrix(%rip), %rax
	movq	vectors(%rip), %rcx
	movl	%r14d, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_79:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_80 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_80:                               #   Parent Loop BB0_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %rsi
	jne	.LBB0_81
# BB#82:                                #   in Loop: Header=BB0_80 Depth=2
	movq	(%rax,%rsi,8), %rbp
	movl	$0, (%rbp,%rsi,4)
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_81:                               #   in Loop: Header=BB0_80 Depth=2
	movq	(%rcx,%r12,8), %rbx
	movl	(%rbx,%rdi,4), %ebp
	imull	(%rbx,%rsi,4), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	movq	(%rax,%rsi,8), %rbp
	addss	(%rbp,%rdi,4), %xmm0
	movss	%xmm0, (%rbp,%rdi,4)
.LBB0_83:                               #   in Loop: Header=BB0_80 Depth=2
	incq	%rdi
	cmpq	%rdi, %rdx
	jne	.LBB0_80
# BB#84:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB0_79 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB0_79
.LBB0_85:                               # %._crit_edge6.i
	incq	%r12
	cmpq	$10, %r12
	jne	.LBB0_76
# BB#86:                                # %generateT.exit
	movl	$1, nmode(%rip)
	movl	$.Lstr.2, %edi
	callq	puts
	movslq	NNTOT(%rip), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#87:                                # %.preheader3.preheader.i
	movl	NUMPATS(%rip), %eax
	movq	%rbx, %r14
	addq	$4, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_88:                               # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_90 Depth 2
                                        #       Child Loop BB0_99 Depth 3
                                        #       Child Loop BB0_101 Depth 3
                                        #         Child Loop BB0_102 Depth 4
	testl	%eax, %eax
	jle	.LBB0_109
# BB#89:                                # %.lr.ph13.i.preheader
                                        #   in Loop: Header=BB0_88 Depth=1
	movl	$1, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_90:                               # %.lr.ph13.i
                                        #   Parent Loop BB0_88 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_99 Depth 3
                                        #       Child Loop BB0_101 Depth 3
                                        #         Child Loop BB0_102 Depth 4
	movq	vectors(%rip), %rax
	movq	(%rax,%r12,8), %rdi
	movq	newvectors(%rip), %rax
	movq	(%rax,%r12,8), %rsi
	cmpl	$2, nmode(%rip)
	jne	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_90 Depth=2
	callq	run
	jmp	.LBB0_93
	.p2align	4, 0x90
.LBB0_92:                               #   in Loop: Header=BB0_90 Depth=2
	callq	runcont
.LBB0_93:                               # %.preheader2.i
                                        #   in Loop: Header=BB0_90 Depth=2
	movslq	NNTOT(%rip), %r8
	testq	%r8, %r8
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	jle	.LBB0_107
# BB#94:                                # %.lr.ph.i39
                                        #   in Loop: Header=BB0_90 Depth=2
	movq	vectors(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	newvectors(%rip), %rcx
	movq	(%rcx,%r12,8), %rdi
	testb	$1, %r8b
	jne	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_90 Depth=2
	xorl	%edx, %edx
	jmp	.LBB0_97
	.p2align	4, 0x90
.LBB0_96:                               #   in Loop: Header=BB0_90 Depth=2
	movl	(%rax), %ecx
	subl	(%rdi), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	ucomiss	%xmm2, %xmm0
	cmovnel	%r15d, %r13d
	cmovpl	%r15d, %r13d
	movl	$1, %edx
.LBB0_97:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_90 Depth=2
	movl	%r8d, %ecx
	cmpl	$1, %r8d
	je	.LBB0_100
# BB#98:                                # %.lr.ph.i39.new
                                        #   in Loop: Header=BB0_90 Depth=2
	subq	%rdx, %r8
	leaq	(%r14,%rdx,4), %rsi
	leaq	4(%rdi,%rdx,4), %rdi
	leaq	4(%rax,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_99:                               #   Parent Loop BB0_88 Depth=1
                                        #     Parent Loop BB0_90 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdx), %ebp
	subl	-4(%rdi), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, -4(%rsi)
	movl	(%rdx), %ebp
	subl	(%rdi), %ebp
	ucomiss	%xmm2, %xmm0
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rsi)
	cmovnel	%r15d, %r13d
	cmovpl	%r15d, %r13d
	ucomiss	%xmm2, %xmm0
	cmovnel	%r15d, %r13d
	cmovpl	%r15d, %r13d
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$8, %rdx
	addq	$-2, %r8
	jne	.LBB0_99
.LBB0_100:                              # %.preheader.us.preheader.i42
                                        #   in Loop: Header=BB0_90 Depth=2
	movq	Tmatrix(%rip), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_101:                              # %.preheader.us.i43
                                        #   Parent Loop BB0_88 Depth=1
                                        #     Parent Loop BB0_90 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_102 Depth 4
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_102:                              #   Parent Loop BB0_88 Depth=1
                                        #     Parent Loop BB0_90 Depth=2
                                        #       Parent Loop BB0_101 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rdi, %rsi
	jne	.LBB0_103
# BB#104:                               #   in Loop: Header=BB0_102 Depth=4
	movq	(%rdx,%rsi,8), %rbp
	movl	$0, (%rbp,%rsi,4)
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_103:                              #   in Loop: Header=BB0_102 Depth=4
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rax,%rdi,4), %xmm0
	mulss	(%rbx,%rsi,4), %xmm0
	movq	(%rdx,%rsi,8), %rbp
	addss	(%rbp,%rdi,4), %xmm0
	movss	%xmm0, (%rbp,%rdi,4)
.LBB0_105:                              #   in Loop: Header=BB0_102 Depth=4
	incq	%rdi
	cmpq	%rdi, %rcx
	jne	.LBB0_102
# BB#106:                               # %._crit_edge.us.i45
                                        #   in Loop: Header=BB0_101 Depth=3
	incq	%rsi
	cmpq	%rcx, %rsi
	jne	.LBB0_101
.LBB0_107:                              # %._crit_edge9.i
                                        #   in Loop: Header=BB0_90 Depth=2
	incq	%r12
	movslq	NUMPATS(%rip), %rax
	cmpq	%rax, %r12
	jl	.LBB0_90
# BB#108:                               # %._crit_edge14.i
                                        #   in Loop: Header=BB0_88 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_88
.LBB0_109:                              # %delta.exit
	movl	$.Lstr.3, %edi
	callq	puts
	cmpl	$0, NUMPATS(%rip)
	jle	.LBB0_135
# BB#110:                               # %.lr.ph30.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_111:                              # %.lr.ph30.i
                                        # =>This Inner Loop Header: Depth=1
	movq	vectors(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	newvectors(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	cmpl	$2, nmode(%rip)
	jne	.LBB0_117
# BB#112:                               #   in Loop: Header=BB0_111 Depth=1
	callq	run
	jmp	.LBB0_118
	.p2align	4, 0x90
.LBB0_117:                              #   in Loop: Header=BB0_111 Depth=1
	callq	runcont
.LBB0_118:                              #   in Loop: Header=BB0_111 Depth=1
	incq	%rbx
	movslq	NUMPATS(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_111
# BB#113:                               # %.preheader.i48
	testl	%eax, %eax
	jle	.LBB0_135
# BB#114:                               # %.lr.ph.i50.preheader
	movdqa	.LCPI0_0(%rip), %xmm10  # xmm10 = [1,1,1,1]
	xorl	%ebx, %ebx
	jmp	.LBB0_115
.LBB0_123:                              #   in Loop: Header=BB0_115 Depth=1
	pxor	%xmm2, %xmm2
                                        # implicit-def: %XMM0
                                        # implicit-def: %XMM1
	xorl	%esi, %esi
	pxor	%xmm3, %xmm3
	testq	%rcx, %rcx
	je	.LBB0_128
.LBB0_126:                              # %vector.body121.preheader.new
                                        #   in Loop: Header=BB0_115 Depth=1
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	leaq	48(%r8,%rsi,4), %rbp
	leaq	48(%r9,%rsi,4), %rcx
	.p2align	4, 0x90
.LBB0_127:                              # %vector.body121
                                        #   Parent Loop BB0_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%rbp), %xmm4
	movdqu	-32(%rbp), %xmm5
	movdqu	-16(%rbp), %xmm9
	movdqu	(%rbp), %xmm8
	movdqu	-48(%rcx), %xmm7
	movdqu	-32(%rcx), %xmm6
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	pcmpeqd	%xmm4, %xmm7
	pandn	%xmm10, %xmm7
	pcmpeqd	%xmm5, %xmm6
	pandn	%xmm10, %xmm6
	paddd	%xmm2, %xmm7
	paddd	%xmm3, %xmm6
	pcmpeqd	%xmm9, %xmm0
	pandn	%xmm10, %xmm0
	pcmpeqd	%xmm8, %xmm1
	pandn	%xmm10, %xmm1
	paddd	%xmm7, %xmm0
	paddd	%xmm6, %xmm1
	addq	$64, %rbp
	addq	$64, %rcx
	addq	$-16, %rdx
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm3
	jne	.LBB0_127
.LBB0_128:                              # %middle.block122
                                        #   in Loop: Header=BB0_115 Depth=1
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edx
	cmpq	%rdi, %rax
	jne	.LBB0_129
	jmp	.LBB0_131
	.p2align	4, 0x90
.LBB0_115:                              # %.lr.ph.i50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_127 Depth 2
                                        #     Child Loop BB0_130 Depth 2
	movslq	NNTOT(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_116
# BB#119:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_115 Depth=1
	movq	vectors(%rip), %rcx
	movq	(%rcx,%rbx,8), %r8
	movq	newvectors(%rip), %rcx
	movq	(%rcx,%rbx,8), %r9
	cmpl	$8, %eax
	jb	.LBB0_120
# BB#121:                               # %min.iters.checked125
                                        #   in Loop: Header=BB0_115 Depth=1
	movq	%rax, %rdi
	andq	$-8, %rdi
	je	.LBB0_120
# BB#122:                               # %vector.body121.preheader
                                        #   in Loop: Header=BB0_115 Depth=1
	leaq	-8(%rdi), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB0_123
# BB#124:                               # %vector.body121.prol
                                        #   in Loop: Header=BB0_115 Depth=1
	movdqu	(%r8), %xmm2
	movdqu	16(%r8), %xmm3
	movdqu	(%r9), %xmm0
	movdqu	16(%r9), %xmm1
	pcmpeqd	%xmm2, %xmm0
	pandn	%xmm10, %xmm0
	pcmpeqd	%xmm3, %xmm1
	pandn	%xmm10, %xmm1
	movl	$8, %esi
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm3
	testq	%rcx, %rcx
	jne	.LBB0_126
	jmp	.LBB0_128
	.p2align	4, 0x90
.LBB0_120:                              #   in Loop: Header=BB0_115 Depth=1
	xorl	%edi, %edi
	xorl	%edx, %edx
.LBB0_129:                              # %scalar.ph123.preheader
                                        #   in Loop: Header=BB0_115 Depth=1
	subq	%rdi, %rax
	leaq	(%r9,%rdi,4), %rcx
	leaq	(%r8,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB0_130:                              # %scalar.ph123
                                        #   Parent Loop BB0_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %edi
	xorl	%ebp, %ebp
	cmpl	(%rcx), %edi
	setne	%bpl
	addl	%ebp, %edx
	addq	$4, %rcx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_130
.LBB0_131:                              # %hamming.exit.i
                                        #   in Loop: Header=BB0_115 Depth=1
	leaq	(,%rbx,4), %rax
	addq	stored(%rip), %rax
	testl	%edx, %edx
	je	.LBB0_132
# BB#133:                               #   in Loop: Header=BB0_115 Depth=1
	movl	$0, (%rax)
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	jmp	.LBB0_134
	.p2align	4, 0x90
.LBB0_116:                              # %hamming.exit.thread.i
                                        #   in Loop: Header=BB0_115 Depth=1
	leaq	(,%rbx,4), %rax
	addq	stored(%rip), %rax
.LBB0_132:                              #   in Loop: Header=BB0_115 Depth=1
	movl	$1, (%rax)
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
.LBB0_134:                              #   in Loop: Header=BB0_115 Depth=1
	incq	%rbx
	movslq	NUMPATS(%rip), %rax
	cmpq	%rax, %rbx
	movdqa	.LCPI0_0(%rip), %xmm10  # xmm10 = [1,1,1,1]
	jl	.LBB0_115
.LBB0_135:                              # %storecheck.exit
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.p2align	4, 0x90
	.type	run,@function
run:                                    # @run
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movl	NNTOT(%rip), %r13d
	movslq	%r13d, %r14
	leaq	(,%r14,4), %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.LBB1_51
# BB#1:
	testq	%r15, %r15
	je	.LBB1_51
# BB#2:                                 # %.preheader74
	testl	%r13d, %r13d
	jle	.LBB1_4
# BB#3:                                 # %.lr.ph95
	leal	-1(%r13), %eax
	leaq	4(,%rax,4), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB1_4:                                # %.preheader73
	movq	Tmatrix(%rip), %r10
	leaq	-1(%r13), %r9
	leaq	-8(%r14), %rax
	shrq	$3, %rax
	movl	%r13d, %r8d
	andl	$1, %r8d
	movq	%r14, %rcx
	andq	$-8, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	4(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	48(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	48(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movdqa	.LCPI1_0(%rip), %xmm9   # xmm9 = [1,1,1,1]
	xorps	%xmm8, %xmm8
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader69.us.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_7:                                # %.preheader69.us
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_12 Depth 3
	testq	%r8, %r8
	movq	(%r10,%r11,8), %rdx
	jne	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	testq	%r9, %r9
	jne	.LBB1_12
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_7 Depth=2
	cvtsi2ssl	(%rbx), %xmm2
	mulss	(%rdx), %xmm2
	addss	%xmm8, %xmm2
	cvttss2si	%xmm2, %ebp
	movl	$1, %edi
	testq	%r9, %r9
	je	.LBB1_11
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_5 Depth=1
                                        #     Parent Loop BB1_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2ssl	(%rbx,%rdi,4), %xmm2
	mulss	(%rdx,%rdi,4), %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	addss	%xmm2, %xmm3
	cvttss2si	%xmm3, %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	4(%rbx,%rdi,4), %xmm2
	mulss	4(%rdx,%rdi,4), %xmm2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	addss	%xmm2, %xmm3
	cvttss2si	%xmm3, %ebp
	addq	$2, %rdi
	cmpq	%rdi, %r13
	jne	.LBB1_12
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=2
	movl	$1, %edi
.LBB1_13:                               # %._crit_edge80.us
                                        #   in Loop: Header=BB1_7 Depth=2
	sarl	$31, %ebp
	orl	$1, %ebp
	movl	%ebp, (%r15,%r11,4)
	incq	%r11
	cmpq	%rdi, %r11
	jne	.LBB1_7
# BB#14:                                # %.preheader71
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%r13d, %r13d
	jle	.LBB1_52
# BB#15:                                # %.lr.ph84.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	testq	%r8, %r8
	jne	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_5 Depth=1
	xorl	%eax, %eax
	testq	%r9, %r9
	jne	.LBB1_21
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph84.prol
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpl	$0, (%rbx)
	jne	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_5 Depth=1
	movl	(%r15), %eax
	movl	%eax, (%rbx)
.LBB1_19:                               # %.lr.ph84.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	$1, %eax
	testq	%r9, %r9
	je	.LBB1_27
.LBB1_21:                               # %.lr.ph84.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rdx
	subq	%rax, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdi
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph84
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, -4(%rdi)
	jne	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=2
	movl	-4(%rbp), %eax
	movl	%eax, -4(%rdi)
.LBB1_24:                               # %.lr.ph84.1137
                                        #   in Loop: Header=BB1_22 Depth=2
	cmpl	$0, (%rdi)
	jne	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_22 Depth=2
	movl	(%rbp), %eax
	movl	%eax, (%rdi)
.LBB1_26:                               #   in Loop: Header=BB1_22 Depth=2
	addq	$8, %rdi
	addq	$8, %rbp
	addq	$-2, %rdx
	jne	.LBB1_22
.LBB1_27:                               # %._crit_edge85
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%r13d, %r13d
	jle	.LBB1_52
# BB#28:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpl	$8, %r13d
	jb	.LBB1_29
# BB#30:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB1_29
# BB#31:                                # %vector.body.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB1_32
# BB#33:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_5 Depth=1
	movdqu	(%r15), %xmm4
	movdqu	16(%r15), %xmm5
	movdqu	(%rbx), %xmm2
	movdqu	16(%rbx), %xmm3
	pcmpeqd	%xmm4, %xmm2
	pandn	%xmm9, %xmm2
	pcmpeqd	%xmm5, %xmm3
	pandn	%xmm9, %xmm3
	movl	$8, %eax
	movdqa	%xmm2, %xmm4
	movdqa	%xmm3, %xmm5
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB1_35
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB1_38:                               # %.lr.ph.i.preheader129
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r14, %rdi
	subq	%rax, %rdi
	leaq	(%rbx,%rax,4), %rbp
	leaq	(%r15,%rax,4), %rax
	.p2align	4, 0x90
.LBB1_39:                               # %.lr.ph.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	xorl	%esi, %esi
	cmpl	(%rbp), %ecx
	setne	%sil
	addl	%esi, %edx
	addq	$4, %rbp
	addq	$4, %rax
	decq	%rdi
	jne	.LBB1_39
.LBB1_40:                               # %hamming.exit
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%edx, %edx
	je	.LBB1_52
# BB#41:                                # %.lr.ph88.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_42:                               # %.lr.ph88
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rax,4), %ecx
	cmpl	(%rbx,%rax,4), %ecx
	jne	.LBB1_44
# BB#43:                                #   in Loop: Header=BB1_42 Depth=2
	incq	%rax
	cmpq	%r14, %rax
	jl	.LBB1_42
.LBB1_44:                               # %hamming.exit.thread
                                        #   in Loop: Header=BB1_5 Depth=1
	cltq
	movl	(%r15,%rax,4), %ecx
	movl	%ecx, (%rbx,%rax,4)
	incl	%r12d
	cmpl	$500, %r12d             # imm = 0x1F4
	jl	.LBB1_5
	jmp	.LBB1_45
.LBB1_32:                               #   in Loop: Header=BB1_5 Depth=1
	pxor	%xmm4, %xmm4
                                        # implicit-def: %XMM2
                                        # implicit-def: %XMM3
	xorl	%eax, %eax
	pxor	%xmm5, %xmm5
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB1_37
.LBB1_35:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	subq	%rax, %rdi
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB1_36:                               # %vector.body
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%rdx), %xmm6
	movdqu	-32(%rdx), %xmm7
	movdqu	-16(%rdx), %xmm11
	movdqu	(%rdx), %xmm10
	movdqu	-48(%rbp), %xmm0
	movdqu	-32(%rbp), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	pcmpeqd	%xmm6, %xmm0
	pandn	%xmm9, %xmm0
	pcmpeqd	%xmm7, %xmm1
	pandn	%xmm9, %xmm1
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	pcmpeqd	%xmm11, %xmm2
	pandn	%xmm9, %xmm2
	pcmpeqd	%xmm10, %xmm3
	pandn	%xmm9, %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	addq	$64, %rdx
	addq	$64, %rbp
	addq	$-16, %rdi
	movdqa	%xmm2, %xmm4
	movdqa	%xmm3, %xmm5
	jne	.LBB1_36
.LBB1_37:                               # %middle.block
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, %r14
	paddd	%xmm3, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	jne	.LBB1_38
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader72
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
                                        #       Child Loop BB1_12 Depth 3
                                        #     Child Loop BB1_22 Depth 2
                                        #     Child Loop BB1_36 Depth 2
                                        #     Child Loop BB1_39 Depth 2
                                        #     Child Loop BB1_42 Depth 2
	testl	%r13d, %r13d
	jg	.LBB1_6
.LBB1_52:                               # %hamming.exit.thread.thread
	incl	%r12d
.LBB1_45:                               # %.loopexit
	cmpl	$500, %r12d             # imm = 0x1F4
	jne	.LBB1_47
# BB#46:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	NNTOT(%rip), %r13d
.LBB1_47:                               # %.preheader
	testl	%r13d, %r13d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jle	.LBB1_50
# BB#48:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	incq	%rax
	movslq	NNTOT(%rip), %rcx
	cmpq	%rcx, %rax
	jl	.LBB1_49
.LBB1_50:                               # %._crit_edge
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_51:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	run, .Lfunc_end1-run
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI2_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI2_6:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	4641240890982006784     # double 200
.LCPI2_2:
	.quad	-4582131145872769024    # double -200
.LCPI2_3:
	.quad	4607182418800017408     # double 1
.LCPI2_5:
	.quad	4604480259023595110     # double 0.69999999999999996
	.text
	.p2align	4, 0x90
	.type	runcont,@function
runcont:                                # @runcont
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 128
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	NNTOT(%rip), %r13
	leaq	(,%r13,4), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	malloc
	testq	%rbx, %rbx
	je	.LBB2_79
# BB#1:
	testq	%rax, %rax
	je	.LBB2_79
# BB#2:                                 # %.preheader113
	testl	%r13d, %r13d
	jle	.LBB2_5
# BB#3:                                 # %.lr.ph145.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph145
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rdx,4), %ecx
	movl	%ecx, (%r14,%rdx,4)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movss	%xmm0, (%rbx,%rdx,4)
	incq	%rdx
	movslq	NNTOT(%rip), %r13
	cmpq	%r13, %rdx
	jl	.LBB2_4
.LBB2_5:                                # %.preheader111.preheader
	movq	%rbx, %rcx
	addq	$48, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	48(%rax), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	48(%r14), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movsd	.LCPI2_5(%rip), %xmm3   # xmm3 = mem[0],zero
	xorl	%ebp, %ebp
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB2_6:                                # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_20 Depth 3
                                        #     Child Loop BB2_12 Depth 2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_39 Depth 4
                                        #         Child Loop BB2_41 Depth 4
                                        #     Child Loop BB2_32 Depth 2
                                        #     Child Loop BB2_34 Depth 2
                                        #     Child Loop BB2_54 Depth 2
                                        #     Child Loop BB2_57 Depth 2
                                        #     Child Loop BB2_60 Depth 2
                                        #     Child Loop BB2_72 Depth 2
                                        #     Child Loop BB2_74 Depth 2
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB2_10
# BB#7:                                 # %.preheader107.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader107
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_20 Depth 3
	testl	%r13d, %r13d
	jle	.LBB2_9
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB2_8 Depth=2
	movq	Tmatrix(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movslq	%r13d, %rcx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB2_19
	.p2align	4, 0x90
.LBB2_18:                               #   Parent Loop BB2_6 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%r15,%rdx,4), %xmm1
	mulss	(%rax,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_18
.LBB2_19:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_8 Depth=2
	cmpq	$3, %rsi
	jb	.LBB2_21
	.p2align	4, 0x90
.LBB2_20:                               #   Parent Loop BB2_6 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%r15,%rdx,4), %xmm1
	mulss	(%rax,%rdx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	4(%r15,%rdx,4), %xmm0
	mulss	4(%rax,%rdx,4), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	8(%r15,%rdx,4), %xmm1
	mulss	8(%rax,%rdx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	12(%r15,%rdx,4), %xmm0
	mulss	12(%rax,%rdx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$4, %rdx
	cmpq	%rcx, %rdx
	jl	.LBB2_20
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_8 Depth=2
	xorps	%xmm0, %xmm0
.LBB2_21:                               # %._crit_edge
                                        #   in Loop: Header=BB2_8 Depth=2
	cvtss2sd	%xmm0, %xmm0
	xorpd	.LCPI2_0(%rip), %xmm0
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	callq	exp
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	exp
	addsd	.LCPI2_3(%rip), %xmm0
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%rbx,%rbp,4)
	incq	%rbp
	movslq	NNTOT(%rip), %r13
	cmpq	%r13, %rbp
	movsd	.LCPI2_5(%rip), %xmm3   # xmm3 = mem[0],zero
	jl	.LBB2_8
.LBB2_10:                               # %.preheader110
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%r12, %rbp
	testl	%ebp, %ebp
	je	.LBB2_11
.LBB2_22:                               # %.preheader109
                                        #   in Loop: Header=BB2_6 Depth=1
	testl	%r13d, %r13d
	jle	.LBB2_80
# BB#23:                                # %.lr.ph131
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%rbp, %r12
	movslq	%r13d, %r8
	cmpl	$7, %r13d
	ja	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_25:                               # %min.iters.checked213
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%r8, %rcx
	andq	$-8, %rcx
	movl	$1, %r9d
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB2_26
# BB#27:                                # %vector.body209.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	leaq	-8(%rcx), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_28
# BB#29:                                # %vector.body209.prol
                                        #   in Loop: Header=BB2_6 Depth=1
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	xorps	%xmm2, %xmm2
	cmpnltps	%xmm0, %xmm2
	movaps	.LCPI2_6(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, %xmm4
	orps	%xmm4, %xmm2
	xorps	%xmm0, %xmm0
	cmpnltps	%xmm1, %xmm0
	orps	%xmm4, %xmm0
	movups	%xmm2, (%rbp)
	movups	%xmm0, 16(%rbp)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB2_31
	jmp	.LBB2_33
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader106.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_12:                               # %.preheader106
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_39 Depth 4
                                        #         Child Loop BB2_41 Depth 4
	testl	%r13d, %r13d
	jle	.LBB2_80
# BB#13:                                # %.lr.ph124.preheader
                                        #   in Loop: Header=BB2_12 Depth=2
	movl	$1, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph124
                                        #   Parent Loop BB2_6 Depth=1
                                        #     Parent Loop BB2_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_39 Depth 4
                                        #         Child Loop BB2_41 Depth 4
	movss	(%rbx,%r12,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	andps	.LCPI2_4(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jbe	.LBB2_44
# BB#15:                                # %.preheader
                                        #   in Loop: Header=BB2_14 Depth=3
	testl	%r13d, %r13d
	jle	.LBB2_16
# BB#38:                                # %.lr.ph119
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	Tmatrix(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movslq	%r13d, %rcx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB2_40
	.p2align	4, 0x90
.LBB2_39:                               #   Parent Loop BB2_6 Depth=1
                                        #     Parent Loop BB2_12 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	(%rax,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbx,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB2_39
.LBB2_40:                               # %.prol.loopexit241
                                        #   in Loop: Header=BB2_14 Depth=3
	cmpq	$3, %rsi
	jb	.LBB2_42
	.p2align	4, 0x90
.LBB2_41:                               #   Parent Loop BB2_6 Depth=1
                                        #     Parent Loop BB2_12 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	(%rax,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbx,%rdx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	4(%rax,%rdx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	4(%rbx,%rdx,4), %xmm0
	addss	%xmm1, %xmm0
	movss	8(%rax,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rbx,%rdx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	12(%rax,%rdx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rbx,%rdx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$4, %rdx
	cmpq	%rcx, %rdx
	jl	.LBB2_41
	jmp	.LBB2_42
.LBB2_16:                               #   in Loop: Header=BB2_14 Depth=3
	xorps	%xmm0, %xmm0
.LBB2_42:                               # %._crit_edge120
                                        #   in Loop: Header=BB2_14 Depth=3
	cvtss2sd	%xmm0, %xmm0
	xorpd	.LCPI2_0(%rip), %xmm0
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	callq	exp
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	exp
	addsd	.LCPI2_3(%rip), %xmm0
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%rbx,%r12,4)
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI2_5(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm0, %xmm3
	jbe	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_14 Depth=3
	xorl	%ebp, %ebp
.LBB2_44:                               #   in Loop: Header=BB2_14 Depth=3
	incq	%r12
	movslq	NNTOT(%rip), %r13
	cmpq	%r13, %r12
	jl	.LBB2_14
# BB#45:                                # %._crit_edge125
                                        #   in Loop: Header=BB2_12 Depth=2
	incl	%r14d
	cmpl	$49, %r14d
	jg	.LBB2_22
# BB#46:                                # %._crit_edge125
                                        #   in Loop: Header=BB2_12 Depth=2
	testl	%ebp, %ebp
	je	.LBB2_12
	jmp	.LBB2_22
.LBB2_26:                               #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB2_34
.LBB2_28:                               #   in Loop: Header=BB2_6 Depth=1
	xorl	%edi, %edi
	movaps	.LCPI2_6(%rip), %xmm4   # xmm4 = [1,1,1,1]
	testq	%rdx, %rdx
	je	.LBB2_33
.LBB2_31:                               # %vector.body209.preheader.new
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rsi
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_32:                               # %vector.body209
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	xorps	%xmm2, %xmm2
	cmpnltps	%xmm0, %xmm2
	orps	%xmm4, %xmm2
	xorps	%xmm0, %xmm0
	cmpnltps	%xmm1, %xmm0
	orps	%xmm4, %xmm0
	movups	%xmm2, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	xorps	%xmm2, %xmm2
	cmpnltps	%xmm0, %xmm2
	orps	%xmm4, %xmm2
	xorps	%xmm0, %xmm0
	cmpnltps	%xmm1, %xmm0
	orps	%xmm4, %xmm0
	movups	%xmm2, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$64, %rsi
	addq	$64, %rdi
	addq	$-16, %rdx
	jne	.LBB2_32
.LBB2_33:                               # %middle.block210
                                        #   in Loop: Header=BB2_6 Depth=1
	cmpq	%rcx, %r8
	je	.LBB2_35
	.p2align	4, 0x90
.LBB2_34:                               # %scalar.ph211
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	movss	(%rbx,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movl	$-1, %edx
	cmoval	%r9d, %edx
	movl	%edx, (%rbp,%rcx,4)
	incq	%rcx
	cmpq	%r8, %rcx
	jl	.LBB2_34
.LBB2_35:                               # %._crit_edge132
                                        #   in Loop: Header=BB2_6 Depth=1
	testl	%r13d, %r13d
	jle	.LBB2_80
# BB#36:                                # %.lr.ph.i
                                        #   in Loop: Header=BB2_6 Depth=1
	cmpl	$8, %r13d
	jae	.LBB2_47
# BB#37:                                #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_47:                               # %min.iters.checked188
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%r8, %rcx
	andq	$-8, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	je	.LBB2_48
# BB#49:                                # %vector.body184.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	leaq	-8(%rcx), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_50
# BB#51:                                # %vector.body184.prol
                                        #   in Loop: Header=BB2_6 Depth=1
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm4
	movq	8(%rsp), %rsi           # 8-byte Reload
	movdqu	(%rsi), %xmm0
	movdqu	16(%rsi), %xmm1
	pcmpeqd	%xmm2, %xmm0
	movdqa	.LCPI2_6(%rip), %xmm2   # xmm2 = [1,1,1,1]
	pandn	%xmm2, %xmm0
	pcmpeqd	%xmm4, %xmm1
	pandn	%xmm2, %xmm1
	movl	$8, %edi
	movdqa	%xmm0, %xmm10
	movdqa	%xmm1, %xmm2
	jmp	.LBB2_52
.LBB2_48:                               #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB2_56
.LBB2_50:                               #   in Loop: Header=BB2_6 Depth=1
	pxor	%xmm10, %xmm10
                                        # implicit-def: %XMM0
                                        # implicit-def: %XMM1
	xorl	%edi, %edi
	xorps	%xmm2, %xmm2
.LBB2_52:                               # %vector.body184.prol.loopexit
                                        #   in Loop: Header=BB2_6 Depth=1
	testq	%rdx, %rdx
	movdqa	.LCPI2_6(%rip), %xmm11  # xmm11 = [1,1,1,1]
	je	.LBB2_55
# BB#53:                                # %vector.body184.preheader.new
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdi,4), %rsi
	movq	64(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_54:                               # %vector.body184
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%rsi), %xmm4
	movdqu	-32(%rsi), %xmm5
	movdqu	-16(%rsi), %xmm9
	movdqu	(%rsi), %xmm8
	movdqu	-48(%rdi), %xmm7
	movdqu	-32(%rdi), %xmm6
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	pcmpeqd	%xmm4, %xmm7
	pandn	%xmm11, %xmm7
	pcmpeqd	%xmm5, %xmm6
	pandn	%xmm11, %xmm6
	paddd	%xmm10, %xmm7
	paddd	%xmm2, %xmm6
	pcmpeqd	%xmm9, %xmm0
	pandn	%xmm11, %xmm0
	pcmpeqd	%xmm8, %xmm1
	pandn	%xmm11, %xmm1
	paddd	%xmm7, %xmm0
	paddd	%xmm6, %xmm1
	addq	$64, %rsi
	addq	$64, %rdi
	addq	$-16, %rdx
	movdqa	%xmm0, %xmm10
	movdqa	%xmm1, %xmm2
	jne	.LBB2_54
.LBB2_55:                               # %middle.block185
                                        #   in Loop: Header=BB2_6 Depth=1
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edx
	cmpq	%rcx, %r8
	je	.LBB2_58
	.p2align	4, 0x90
.LBB2_56:                               # %scalar.ph186.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rdi
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB2_57:                               # %scalar.ph186
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %ebp
	xorl	%eax, %eax
	cmpl	(%rdi), %ebp
	setne	%al
	addl	%eax, %edx
	addq	$4, %rdi
	addq	$4, %rcx
	decq	%rsi
	jne	.LBB2_57
.LBB2_58:                               # %hamming.exit
                                        #   in Loop: Header=BB2_6 Depth=1
	testl	%edx, %edx
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB2_80
# BB#59:                                # %.lr.ph135.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_60:                               # %.lr.ph135
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rcx,4), %edx
	cmpl	(%rax,%rcx,4), %edx
	jne	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_60 Depth=2
	incq	%rcx
	cmpq	%r8, %rcx
	jl	.LBB2_60
.LBB2_62:                               # %.critedge
                                        #   in Loop: Header=BB2_6 Depth=1
	movslq	%ecx, %rdx
	movl	(%rax,%rdx,4), %ecx
	movl	%ecx, (%r14,%rdx,4)
	movslq	NNTOT(%rip), %r13
	testq	%r13, %r13
	movl	20(%rsp), %ebp          # 4-byte Reload
	jle	.LBB2_75
# BB#63:                                # %.lr.ph140
                                        #   in Loop: Header=BB2_6 Depth=1
	cmpl	$8, %r13d
	jae	.LBB2_65
# BB#64:                                #   in Loop: Header=BB2_6 Depth=1
	xorl	%edi, %edi
	jmp	.LBB2_74
	.p2align	4, 0x90
.LBB2_65:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%r13, %rdi
	andq	$-8, %rdi
	je	.LBB2_66
# BB#67:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	leaq	-8(%rdi), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB2_68
# BB#69:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_6 Depth=1
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	cvtdq2ps	%xmm0, %xmm0
	cvtdq2ps	%xmm1, %xmm1
	movups	%xmm0, (%rbx)
	movups	%xmm1, 16(%rbx)
	movl	$8, %r8d
	testq	%rcx, %rcx
	jne	.LBB2_71
	jmp	.LBB2_73
.LBB2_66:                               #   in Loop: Header=BB2_6 Depth=1
	xorl	%edi, %edi
	jmp	.LBB2_74
.LBB2_68:                               #   in Loop: Header=BB2_6 Depth=1
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.LBB2_73
.LBB2_71:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%rdi, %rcx
	subq	%r8, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r8,4), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r8,4), %rsi
	.p2align	4, 0x90
.LBB2_72:                               # %vector.body
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	cvtdq2ps	%xmm0, %xmm0
	cvtdq2ps	%xmm1, %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	cvtdq2ps	%xmm0, %xmm0
	cvtdq2ps	%xmm1, %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$64, %rdx
	addq	$64, %rsi
	addq	$-16, %rcx
	jne	.LBB2_72
.LBB2_73:                               # %middle.block
                                        #   in Loop: Header=BB2_6 Depth=1
	cmpq	%rdi, %r13
	je	.LBB2_75
	.p2align	4, 0x90
.LBB2_74:                               # %scalar.ph
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%r14,%rdi,4), %xmm0
	movd	%xmm0, (%rbx,%rdi,4)
	incq	%rdi
	cmpq	%r13, %rdi
	jl	.LBB2_74
.LBB2_75:                               # %hamming.exit.thread
                                        #   in Loop: Header=BB2_6 Depth=1
	incl	%ebp
	cmpl	$500, %ebp              # imm = 0x1F4
	jl	.LBB2_6
	jmp	.LBB2_76
.LBB2_80:                               # %hamming.exit.thread.thread
	movl	20(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
.LBB2_76:                               # %.loopexit
	cmpl	$500, %ebp              # imm = 0x1F4
	jne	.LBB2_78
# BB#77:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_78:
	movl	%ebp, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_79:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	runcont, .Lfunc_end2-runcont
	.cfi_endproc

	.type	nmode,@object           # @nmode
	.data
	.globl	nmode
	.p2align	2
nmode:
	.long	2                       # 0x2
	.size	nmode, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compile date: %s\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"today"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Wrong number of arguments, 1 needed, %d specified.\n"
	.size	.L.str.4, 52

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"USAGE: %s <datafile>\n"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ABORT: Could not read datafile %s\n"
	.size	.L.str.7, 35

	.type	NNWIDTH,@object         # @NNWIDTH
	.comm	NNWIDTH,4,4
	.type	NNHEIGHT,@object        # @NNHEIGHT
	.comm	NNHEIGHT,4,4
	.type	NUMPATS,@object         # @NUMPATS
	.comm	NUMPATS,4,4
	.type	NNTOT,@object           # @NNTOT
	.comm	NNTOT,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Matrix size is %dx%d\n"
	.size	.L.str.8, 22

	.type	vnames,@object          # @vnames
	.comm	vnames,8,8
	.type	stored,@object          # @stored
	.comm	stored,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"ABORT: Out of memory\n"
	.size	.L.str.9, 22

	.type	Tmatrix,@object         # @Tmatrix
	.comm	Tmatrix,8,8
	.type	vectors,@object         # @vectors
	.comm	vectors,8,8
	.type	newvectors,@object      # @newvectors
	.comm	newvectors,8,8
	.type	generators,@object      # @generators
	.comm	generators,8,8
	.type	randnum,@object         # @randnum
	.comm	randnum,8,8
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"WARNING: %d vectors have a hamming distance <2, please modify input vectors!\n"
	.size	.L.str.14, 78

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Warning! No stable state reached after 500 iterations, aborting!"
	.size	.L.str.16, 65

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Warning! No stable state reached after 500 iterations!"
	.size	.L.str.17, 55

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s"
	.size	.L.str.18, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Pattern %d stored.\n"
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Pattern %d: hamming distance=%-.2d.\n"
	.size	.L.str.21, 37

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Checking hamming distances..."
	.size	.Lstr, 30

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Generating T matrix..."
	.size	.Lstr.1, 23

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Delta learning..."
	.size	.Lstr.2, 18

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"Store check..."
	.size	.Lstr.3, 15

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.4:
	.asciz	"Vectors read from file!"
	.size	.Lstr.4, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
