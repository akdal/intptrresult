	.text
	.file	"ArchiveCommandLine.bc"
	.globl	_ZNK15CArchiveCommand18IsFromExtractGroupEv
	.p2align	4, 0x90
	.type	_ZNK15CArchiveCommand18IsFromExtractGroupEv,@function
_ZNK15CArchiveCommand18IsFromExtractGroupEv: # @_ZNK15CArchiveCommand18IsFromExtractGroupEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	addl	$-3, %eax
	cmpl	$3, %eax
	setb	%al
	retq
.Lfunc_end0:
	.size	_ZNK15CArchiveCommand18IsFromExtractGroupEv, .Lfunc_end0-_ZNK15CArchiveCommand18IsFromExtractGroupEv
	.cfi_endproc

	.globl	_ZNK15CArchiveCommand11GetPathModeEv
	.p2align	4, 0x90
	.type	_ZNK15CArchiveCommand11GetPathModeEv,@function
_ZNK15CArchiveCommand11GetPathModeEv:   # @_ZNK15CArchiveCommand11GetPathModeEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$3, %ecx
	je	.LBB1_3
# BB#1:
	cmpl	$5, %ecx
	je	.LBB1_3
# BB#2:
	movl	$2, %eax
.LBB1_3:
	retq
.Lfunc_end1:
	.size	_ZNK15CArchiveCommand11GetPathModeEv, .Lfunc_end1-_ZNK15CArchiveCommand11GetPathModeEv
	.cfi_endproc

	.globl	_ZNK15CArchiveCommand17IsFromUpdateGroupEv
	.p2align	4, 0x90
	.type	_ZNK15CArchiveCommand17IsFromUpdateGroupEv,@function
_ZNK15CArchiveCommand17IsFromUpdateGroupEv: # @_ZNK15CArchiveCommand17IsFromUpdateGroupEv
	.cfi_startproc
# BB#0:
	cmpl	$3, (%rdi)
	setb	%al
	retq
.Lfunc_end2:
	.size	_ZNK15CArchiveCommand17IsFromUpdateGroupEv, .Lfunc_end2-_ZNK15CArchiveCommand17IsFromUpdateGroupEv
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwED2Ev,"axG",@progbits,_ZN11CStringBaseIwED2Ev,comdat
	.weak	_ZN11CStringBaseIwED2Ev
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwED2Ev,@function
_ZN11CStringBaseIwED2Ev:                # @_ZN11CStringBaseIwED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	_ZN11CStringBaseIwED2Ev, .Lfunc_end3-_ZN11CStringBaseIwED2Ev
	.cfi_endproc

	.text
	.globl	_ZN25CArchiveCommandLineParserC2Ev
	.p2align	4, 0x90
	.type	_ZN25CArchiveCommandLineParserC2Ev,@function
_ZN25CArchiveCommandLineParserC2Ev:     # @_ZN25CArchiveCommandLineParserC2Ev
	.cfi_startproc
# BB#0:
	movl	$30, %esi
	jmp	_ZN18NCommandLineParser7CParserC1Ei # TAILCALL
.Lfunc_end4:
	.size	_ZN25CArchiveCommandLineParserC2Ev, .Lfunc_end4-_ZN25CArchiveCommandLineParserC2Ev
	.cfi_endproc

	.globl	_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions
	.p2align	4, 0x90
	.type	_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions,@function
_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions: # @_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rax
	movq	%rdi, %r14
.Ltmp0:
	movl	$_ZL12kSwitchForms, %esi
	movq	%rax, %rdx
	callq	_ZN18NCommandLineParser7CParser12ParseStringsEPKNS_11CSwitchFormERK13CObjectVectorI11CStringBaseIwEE
.Ltmp1:
# BB#1:
	movq	stdin(%rip), %rdi
	callq	fileno
	movl	%eax, %edi
	callq	isatty
	testl	%eax, %eax
	setne	2(%rbx)
	movq	stdout(%rip), %rdi
	callq	fileno
	movl	%eax, %edi
	callq	isatty
	testl	%eax, %eax
	setne	3(%rbx)
	movq	stderr(%rip), %rdi
	callq	fileno
	movl	%eax, %edi
	callq	isatty
	testl	%eax, %eax
	setne	4(%rbx)
	movl	$20, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 5(%rbx)
	movl	$21, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 6(%rbx)
	movl	$3, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	xorb	$1, %al
	movb	%al, 7(%rbx)
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	$1, %bpl
	cmpb	$0, (%rax)
	jne	.LBB5_4
# BB#2:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	jne	.LBB5_4
# BB#3:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	setne	%bpl
.LBB5_4:
	movb	%bpl, (%rbx)
	movb	$0, 1(%rbx)
	movl	$25, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB5_7
# BB#5:
	movl	$25, %esi
	movq	%r14, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB5_7
# BB#6:
	movb	$1, 1(%rbx)
.LBB5_7:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB5_8:
.Ltmp2:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
.Ltmp3:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp4:
# BB#9:                                 # %.noexc
.LBB5_10:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	callq	__cxa_end_catch
.Ltmp7:
# BB#11:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_12:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions, .Lfunc_end5-_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	1                       #   On action: 1
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL23ThrowUserErrorExceptionv,@function
_ZL23ThrowUserErrorExceptionv:          # @_ZL23ThrowUserErrorExceptionv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Lfunc_end6:
	.size	_ZL23ThrowUserErrorExceptionv, .Lfunc_end6-_ZL23ThrowUserErrorExceptionv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.byte	73                      # 0x49
	.byte	110                     # 0x6e
	.byte	99                      # 0x63
	.byte	111                     # 0x6f
	.byte	114                     # 0x72
	.byte	114                     # 0x72
	.byte	101                     # 0x65
	.byte	99                      # 0x63
	.byte	116                     # 0x74
	.byte	32                      # 0x20
	.byte	118                     # 0x76
	.byte	111                     # 0x6f
	.byte	108                     # 0x6c
	.byte	117                     # 0x75
	.byte	109                     # 0x6d
	.byte	101                     # 0x65
.LCPI8_1:
	.zero	16
	.text
	.globl	_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions
	.p2align	4, 0x90
	.type	_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions,@function
_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions: # @_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$648, %rsp              # imm = 0x288
.Lcfi13:
	.cfi_def_cfa_offset 704
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	28(%rdi), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB8_489
# BB#1:
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rbp
	movslq	8(%rax), %r14
	movq	$0, 296(%rsp)
	movq	%r14, %r15
	incq	%r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 288(%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 300(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_2:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_2
# BB#3:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%r14d, 296(%rsp)
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp10:
# BB#4:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit.i
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
.Ltmp11:
	movl	$16, %edi
	callq	_Znam
.Ltmp12:
# BB#5:
	movq	%rax, 256(%rsp)
	movl	$0, (%rax)
	movl	$4, 268(%rsp)
.Ltmp14:
	leaq	288(%rsp), %rdx
	leaq	256(%rsp), %rcx
	movl	$9, %edi
	movl	$_ZL14g_CommandForms, %esi
	callq	_ZN18NCommandLineParser12ParseCommandEiPKNS_12CCommandFormERK11CStringBaseIwERS4_
.Ltmp15:
# BB#6:
	testl	%eax, %eax
	js	.LBB8_8
# BB#7:
	movl	%eax, 48(%r13)
	movb	$1, %bl
	jmp	.LBB8_9
.LBB8_8:
	xorl	%ebx, %ebx
.LBB8_9:
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_11
# BB#10:
	callq	_ZdaPv
.LBB8_11:                               # %_ZN11CStringBaseIwED2Ev.exit10.i
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#12:
	callq	_ZdaPv
.LBB8_13:                               # %_ZL19ParseArchiveCommandRK11CStringBaseIwER15CArchiveCommand.exit
	testb	%bl, %bl
	je	.LBB8_489
# BB#14:
	movl	$27, %esi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 96(%r13)
	movl	$29, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 97(%r13)
	movl	$28, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_16
# BB#15:
	movl	$28, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movl	40(%rax), %eax
	shrl	$31, %eax
	movb	%al, g_CaseSensitive(%rip)
.LBB8_16:
	movl	$18, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_18
# BB#17:
	movl	$18, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movl	40(%rax), %eax
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	addl	%ecx, %ecx
	testl	%eax, %eax
	movl	$1, %r15d
	cmovnel	%ecx, %r15d
	jmp	.LBB8_19
.LBB8_18:
	movl	$2, %r15d
.LBB8_19:
	movl	$11, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_21
# BB#20:
	leaq	16(%r13), %rbx
	movl	$11, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	leaq	8(%rax), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	%r15d, %ecx
	callq	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
	movb	$1, %bl
	jmp	.LBB8_22
.LBB8_21:
	xorl	%ebx, %ebx
.LBB8_22:
	movl	$12, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_24
# BB#23:
	movl	%ebx, %r14d
	leaq	16(%r13), %rbx
	movl	$12, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	leaq	8(%rax), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r14d, %ebx
	movl	%r15d, %ecx
	callq	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
.LBB8_24:
	movl	$15, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_26
# BB#25:                                # %._crit_edge643
	movl	48(%r13), %ecx
	xorl	%r14d, %r14d
	jmp	.LBB8_27
.LBB8_26:
	movl	48(%r13), %ecx
	leal	-7(%rcx), %eax
	cmpl	$1, %eax
	seta	%r14b
.LBB8_27:
	addl	$-3, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	cmpl	$3, %ecx
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %rdx          # 8-byte Reload
	ja	.LBB8_30
# BB#28:
	xorl	%ecx, %ecx
	cmpb	$0, 5(%r13)
	movl	$1, %r12d
	jne	.LBB8_46
# BB#29:
	movl	%r14d, %eax
	xorb	$1, %al
	je	.LBB8_31
	jmp	.LBB8_46
.LBB8_30:                               # %select.unfold
	testb	%r14b, %r14b
	je	.LBB8_35
.LBB8_31:
	cmpl	$1, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_505
# BB#32:
	movq	32(%rdx), %rax
	movq	8(%rax), %r12
	leaq	56(%r13), %rax
	cmpq	%rax, %r12
	je	.LBB8_36
# BB#33:
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	$0, 64(%r13)
	movq	56(%r13), %rbx
	movl	$0, (%rbx)
	movslq	8(%r12), %rbp
	incq	%rbp
	movl	68(%r13), %r13d
	cmpl	%r13d, %ebp
	jne	.LBB8_37
# BB#34:
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB8_42
.LBB8_35:
	movl	$1, %r12d
	xorl	%ecx, %ecx
	jmp	.LBB8_46
.LBB8_36:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movl	64(%r13), %eax
	jmp	.LBB8_45
.LBB8_37:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB8_40
# BB#38:
	testl	%r13d, %r13d
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB8_41
# BB#39:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	64(%r13), %rcx
	jmp	.LBB8_41
.LBB8_40:
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB8_41:                               # %._crit_edge16.i.i
	movq	%rax, 56(%r13)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 68(%r13)
	movq	%rax, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB8_42:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB8_43:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_43
# BB#44:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r12), %eax
	movl	%eax, 64(%r13)
	movl	32(%rsp), %ebx          # 4-byte Reload
.LBB8_45:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movl	$2, %r12d
	testl	%eax, %eax
	movb	%r14b, %cl
	je	.LBB8_489
.LBB8_46:                               # %.thread
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	leaq	16(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	28(%rdx), %eax
	cmpl	%r12d, %eax
	setne	%cl
	orb	%cl, %bl
	jne	.LBB8_56
# BB#47:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i
	movl	$8, %edi
	callq	_Znam
	movq	%rax, 288(%rsp)
	movl	$2, 300(%rsp)
	movl	$42, (%rax)
	movl	$0, 4(%rax)
	movl	$1, 296(%rsp)
	testl	%r15d, %r15d
	je	.LBB8_50
# BB#48:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i
	cmpl	$1, %r15d
	jne	.LBB8_51
# BB#49:
.Ltmp17:
	leaq	288(%rsp), %rdi
	callq	_Z23DoesNameContainWildCardRK11CStringBaseIwE
                                        # kill: %AL<def> %AL<kill> %EAX<def>
.Ltmp18:
	jmp	.LBB8_52
.LBB8_50:
	movb	$1, %al
	jmp	.LBB8_52
.LBB8_51:
	xorl	%eax, %eax
.LBB8_52:                               # %.noexc.i
.Ltmp19:
	movzbl	%al, %ecx
	leaq	288(%rsp), %rdx
	movl	$1, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp20:
# BB#53:                                # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit3.i
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_55
# BB#54:
	callq	_ZdaPv
.LBB8_55:                               # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	28(%rdx), %eax
.LBB8_56:                               # %.preheader.i
	cmpl	%r12d, %eax
	jle	.LBB8_81
# BB#57:                                # %.lr.ph.i
	cmpl	$1, %r15d
	je	.LBB8_73
# BB#58:                                # %.lr.ph.i
	movl	%r12d, %ebp
	leaq	160(%rsp), %rbx
	testl	%r15d, %r15d
	jne	.LBB8_66
	.p2align	4, 0x90
.LBB8_59:                               # %.lr.ph.split.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB8_483
# BB#60:                                #   in Loop: Header=BB8_59 Depth=1
	movq	(%rax), %rdx
	cmpl	$64, (%rdx)
	jne	.LBB8_64
# BB#61:                                #   in Loop: Header=BB8_59 Depth=1
	decl	%ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	160(%rsp), %rsi
.Ltmp22:
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
.Ltmp23:
# BB#62:                                #   in Loop: Header=BB8_59 Depth=1
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_65
# BB#63:                                #   in Loop: Header=BB8_59 Depth=1
	callq	_ZdaPv
	jmp	.LBB8_65
	.p2align	4, 0x90
.LBB8_64:                               # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit.us11.i
                                        #   in Loop: Header=BB8_59 Depth=1
	movl	$1, %esi
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.LBB8_65:                               # %_ZN11CStringBaseIwED2Ev.exit6.us13.i
                                        #   in Loop: Header=BB8_59 Depth=1
	incq	%rbp
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	28(%rdx), %rax
	cmpq	%rax, %rbp
	jl	.LBB8_59
	jmp	.LBB8_81
	.p2align	4, 0x90
.LBB8_66:                               # %.lr.ph.split.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB8_483
# BB#67:                                #   in Loop: Header=BB8_66 Depth=1
	movq	(%rax), %rdx
	cmpl	$64, (%rdx)
	jne	.LBB8_71
# BB#68:                                #   in Loop: Header=BB8_66 Depth=1
	decl	%ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	160(%rsp), %rsi
.Ltmp28:
	movl	$1, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %ecx
	callq	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
.Ltmp29:
# BB#69:                                #   in Loop: Header=BB8_66 Depth=1
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_72
# BB#70:                                #   in Loop: Header=BB8_66 Depth=1
	callq	_ZdaPv
	jmp	.LBB8_72
	.p2align	4, 0x90
.LBB8_71:                               # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit.i
                                        #   in Loop: Header=BB8_66 Depth=1
	movl	$1, %esi
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.LBB8_72:                               # %_ZN11CStringBaseIwED2Ev.exit6.i
                                        #   in Loop: Header=BB8_66 Depth=1
	incq	%rbp
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	28(%rdx), %rax
	cmpq	%rax, %rbp
	jl	.LBB8_66
	jmp	.LBB8_81
.LBB8_73:                               # %.lr.ph.split.us.preheader.i
	movl	%r12d, %ebx
	leaq	160(%rsp), %r14
	.p2align	4, 0x90
.LBB8_74:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdx), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	8(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB8_483
# BB#75:                                #   in Loop: Header=BB8_74 Depth=1
	movq	(%rbp), %rax
	cmpl	$64, (%rax)
	jne	.LBB8_79
# BB#76:                                #   in Loop: Header=BB8_74 Depth=1
	decl	%ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	160(%rsp), %rsi
.Ltmp25:
	movl	$1, %edx
	movl	$1, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
.Ltmp26:
# BB#77:                                #   in Loop: Header=BB8_74 Depth=1
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_80
# BB#78:                                #   in Loop: Header=BB8_74 Depth=1
	callq	_ZdaPv
	jmp	.LBB8_80
	.p2align	4, 0x90
.LBB8_79:                               # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit.us.i
                                        #   in Loop: Header=BB8_74 Depth=1
	movq	%rbp, %rdi
	callq	_Z23DoesNameContainWildCardRK11CStringBaseIwE
	movzbl	%al, %ecx
	movl	$1, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.LBB8_80:                               # %_ZN11CStringBaseIwED2Ev.exit6.us.i
                                        #   in Loop: Header=BB8_74 Depth=1
	incq	%rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	28(%rdx), %rax
	cmpq	%rax, %rbx
	jl	.LBB8_74
.LBB8_81:                               # %_ZL33AddToCensorFromNonSwitchesStringsiRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEN13NRecursedType5EEnumEbj.exit
	movq	%rdx, %rbx
	movl	$6, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 8(%r13)
	movl	$26, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movzbl	(%rax), %eax
	xorl	$1, %eax
	movl	%eax, global_use_lstat(%rip)
	movl	$7, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	testb	%al, %al
	movb	%al, 72(%r13)
	je	.LBB8_93
# BB#82:
	movl	$7, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax), %r14
	leaq	80(%r13), %rax
	cmpq	%rax, %r14
	je	.LBB8_93
# BB#83:
	movl	$0, 88(%r13)
	movq	80(%r13), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %rbp
	incq	%rbp
	movl	92(%r13), %r13d
	cmpl	%r13d, %ebp
	jne	.LBB8_85
# BB#84:
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB8_90
.LBB8_85:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_88
# BB#86:
	testl	%r13d, %r13d
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB8_89
# BB#87:                                # %._crit_edge.thread.i.i221
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	88(%r13), %rax
	jmp	.LBB8_89
.LBB8_88:
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB8_89:                               # %._crit_edge16.i.i222
	movq	%r15, 80(%r13)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 92(%r13)
	movq	%r15, %rbx
.LBB8_90:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i223
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB8_91:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_91
# BB#92:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i226
	movl	8(%r14), %eax
	movl	%eax, 88(%r13)
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB8_93:                               # %_ZN11CStringBaseIwEaSERKS0_.exit227
	movl	$24, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movb	%al, 9(%r13)
	movl	$5, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB8_105
# BB#94:
	movl	$5, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax), %r14
	leaq	544(%r13), %rax
	cmpq	%rax, %r14
	je	.LBB8_105
# BB#95:
	movl	$0, 552(%r13)
	movq	544(%r13), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %rbp
	incq	%rbp
	movl	556(%r13), %r13d
	cmpl	%r13d, %ebp
	jne	.LBB8_97
# BB#96:
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB8_102
.LBB8_97:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_100
# BB#98:
	testl	%r13d, %r13d
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB8_101
# BB#99:                                # %._crit_edge.thread.i.i231
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	552(%r13), %rax
	jmp	.LBB8_101
.LBB8_100:
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB8_101:                              # %._crit_edge16.i.i232
	movq	%r15, 544(%r13)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 556(%r13)
	movq	%r15, %rbx
.LBB8_102:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i233
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB8_103:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_103
# BB#104:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i236
	movl	8(%r14), %eax
	movl	%eax, 552(%r13)
.LBB8_105:                              # %_ZN11CStringBaseIwEaSERKS0_.exit237
	cmpl	$3, 120(%rsp)           # 4-byte Folded Reload
	ja	.LBB8_129
# BB#106:
	cmpl	$1, 28(%r13)
	jne	.LBB8_504
# BB#107:                               # %_ZNK9NWildcard7CCensor14AllAreRelativeEv.exit
	movq	32(%r13), %rax
	movq	(%rax), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB8_504
# BB#108:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 232(%rsp)
	movq	$8, 248(%rsp)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 224(%rsp)
.Ltmp160:
	movl	$13, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp161:
# BB#109:
	cmpb	$0, (%rax)
	je	.LBB8_112
# BB#110:
.Ltmp162:
	movl	$13, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp163:
# BB#111:
	addq	$8, %rax
.Ltmp164:
	leaq	224(%rsp), %rdi
	movl	$1, %edx
	movl	$2, %ecx
	movq	%rax, %rsi
	callq	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
.Ltmp165:
.LBB8_112:
.Ltmp166:
	movl	$14, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp167:
# BB#113:
	cmpb	$0, (%rax)
	je	.LBB8_116
# BB#114:
.Ltmp168:
	movl	$14, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp169:
# BB#115:
	addq	$8, %rax
.Ltmp170:
	leaq	224(%rsp), %rdi
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%rax, %rsi
	callq	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
.Ltmp171:
.LBB8_116:
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	je	.LBB8_180
# BB#117:                               # %_Z11MyStringLenIwEiPKT_.exit.i
.Ltmp173:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp174:
# BB#118:                               # %.preheader22.i.i
	leaq	56(%r13), %r14
	movq	$42, (%rbp)
	movslq	64(%r13), %rbx
	testq	%rbx, %rbx
	jle	.LBB8_123
# BB#119:                               # %.preheader.us.preheader.i.i
	movq	(%r14), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_120:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rax
	jge	.LBB8_122
# BB#121:                               #   in Loop: Header=BB8_120 Depth=1
	movl	(%rcx,%rax,4), %edx
	cmpl	(%rbp), %edx
	je	.LBB8_265
.LBB8_122:                              #   in Loop: Header=BB8_120 Depth=1
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB8_120
.LBB8_123:                              # %_ZNK11CStringBaseIwE4FindERKS0_.exit.thread.preheader
.Ltmp176:
	movl	$8, %edi
	callq	_Znam
.Ltmp177:
# BB#124:                               # %.preheader22.i.i253
	movq	$63, (%rax)
	movl	$-1, %r15d
	testl	%ebx, %ebx
	jle	.LBB8_268
# BB#125:                               # %.preheader.us.preheader.i.i255
	movq	(%r14), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_126:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rcx
	jge	.LBB8_128
# BB#127:                               #   in Loop: Header=BB8_126 Depth=1
	movl	(%rdx,%rcx,4), %esi
	cmpl	(%rax), %esi
	je	.LBB8_267
.LBB8_128:                              #   in Loop: Header=BB8_126 Depth=1
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	.LBB8_126
	jmp	.LBB8_268
.LBB8_129:
	movl	48(%r13), %eax
	cmpl	$2, %eax
	ja	.LBB8_133
# BB#130:
	je	.LBB8_181
# BB#131:
	testl	%eax, %eax
	jne	.LBB8_182
# BB#132:
	movups	_ZN14NUpdateArchive13kAddActionSetE+12(%rip), %xmm0
	movups	%xmm0, 204(%rsp)
	movups	_ZN14NUpdateArchive13kAddActionSetE(%rip), %xmm0
	jmp	.LBB8_183
.LBB8_133:
	cmpl	$8, %eax
	je	.LBB8_398
# BB#134:
	cmpl	$7, %eax
	jne	.LBB8_509
# BB#135:
	movl	$-1, 572(%r13)
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 564(%r13)
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB8_139
# BB#136:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	movl	%r12d, %ecx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rdi
	leaq	288(%rsp), %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB8_509
# BB#137:
	movq	288(%rsp), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.LBB8_509
# BB#138:                               # %_ZL21ConvertStringToUInt32PKwRj.exit.thread
	movl	%eax, 564(%r13)
.LBB8_139:                              # %.preheader514
	movl	$8, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpl	$0, 20(%rax)
	jle	.LBB8_398
# BB#140:                               # %.lr.ph602
	leaq	576(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	$-1, %r12
	.p2align	4, 0x90
.LBB8_141:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_145 Depth 2
                                        #     Child Loop BB8_174 Depth 2
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax,%r15,8), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 288(%rsp)
	movslq	8(%rbp), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB8_143
# BB#142:                               # %._crit_edge16.i.i424
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	cmovoq	%r12, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rdi
	movq	%rdi, 288(%rsp)
	movl	$0, (%rdi)
	movl	%ebx, 300(%rsp)
	jmp	.LBB8_144
	.p2align	4, 0x90
.LBB8_143:                              #   in Loop: Header=BB8_141 Depth=1
	xorl	%edi, %edi
.LBB8_144:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i425
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	(%rbp), %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB8_145:                              #   Parent Loop BB8_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_145
# BB#146:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit429
                                        #   in Loop: Header=BB8_141 Depth=1
	movl	%r14d, 296(%rsp)
.Ltmp31:
	callq	_Z13MyStringUpperPw
.Ltmp32:
# BB#147:                               # %_ZN11CStringBaseIwE9MakeUpperEv.exit
                                        #   in Loop: Header=BB8_141 Depth=1
	movl	296(%rsp), %ecx
	cmpl	$1, %ecx
	jle	.LBB8_490
# BB#148:                               #   in Loop: Header=BB8_141 Depth=1
	movq	288(%rsp), %rax
	movl	(%rax), %edx
	cmpl	$77, %edx
	je	.LBB8_155
# BB#149:                               #   in Loop: Header=BB8_141 Depth=1
	cmpl	$68, %edx
	jne	.LBB8_487
# BB#150:                               #   in Loop: Header=BB8_141 Depth=1
	xorl	%ecx, %ecx
	cmpl	$61, 4(%rax)
	sete	%cl
	leaq	4(%rax,%rcx,4), %rdi
.Ltmp46:
	leaq	464(%rsp), %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
.Ltmp47:
# BB#151:                               # %.noexc434
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB8_496
# BB#152:                               # %.noexc434
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	464(%rsp), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.LBB8_496
# BB#153:                               #   in Loop: Header=BB8_141 Depth=1
	cmpl	$32, %eax
	jae	.LBB8_498
# BB#154:                               #   in Loop: Header=BB8_141 Depth=1
	movl	$1, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	%edx, 572(%r13)
	jmp	.LBB8_177
	.p2align	4, 0x90
.LBB8_155:                              #   in Loop: Header=BB8_141 Depth=1
	cmpl	$84, 4(%rax)
	jne	.LBB8_161
# BB#156:                               #   in Loop: Header=BB8_141 Depth=1
	xorl	%ecx, %ecx
	cmpl	$61, 8(%rax)
	sete	%cl
	cmpl	$0, 8(%rax,%rcx,4)
	je	.LBB8_177
# BB#157:                               #   in Loop: Header=BB8_141 Depth=1
	leaq	8(%rax,%rcx,4), %rdi
.Ltmp40:
	leaq	472(%rsp), %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
.Ltmp41:
# BB#158:                               # %.noexc442
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB8_506
# BB#159:                               # %.noexc442
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	472(%rsp), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.LBB8_506
# BB#160:                               # %.thread507
                                        #   in Loop: Header=BB8_141 Depth=1
	movl	%eax, 568(%r13)
	jmp	.LBB8_177
	.p2align	4, 0x90
.LBB8_161:                              #   in Loop: Header=BB8_141 Depth=1
	cmpl	$61, 4(%rax)
	jne	.LBB8_487
# BB#162:                               #   in Loop: Header=BB8_141 Depth=1
	cmpl	$0, 8(%rax)
	je	.LBB8_177
# BB#163:                               #   in Loop: Header=BB8_141 Depth=1
	addl	$-2, %ecx
.Ltmp34:
	movl	$2, %edx
	leaq	480(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	288(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp35:
# BB#164:                               # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB8_141 Depth=1
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB8_170
# BB#165:                               #   in Loop: Header=BB8_141 Depth=1
	movl	$0, 584(%r13)
	movq	576(%r13), %rbx
	movl	$0, (%rbx)
	movslq	488(%rsp), %rbp
	incq	%rbp
	movl	588(%r13), %r14d
	cmpl	%r14d, %ebp
	je	.LBB8_173
# BB#166:                               #   in Loop: Header=BB8_141 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	cmovoq	%r12, %rax
.Ltmp37:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp38:
# BB#167:                               # %.noexc456
                                        #   in Loop: Header=BB8_141 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_171
# BB#168:                               # %.noexc456
                                        #   in Loop: Header=BB8_141 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB8_172
# BB#169:                               # %._crit_edge.thread.i.i450
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	584(%rax), %rax
	jmp	.LBB8_172
.LBB8_170:                              # %_ZNK11CStringBaseIwE3MidEi.exit._ZN11CStringBaseIwEaSERKS0_.exit457_crit_edge
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	480(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_176
	jmp	.LBB8_177
.LBB8_171:                              #   in Loop: Header=BB8_141 Depth=1
	xorl	%eax, %eax
.LBB8_172:                              # %._crit_edge16.i.i451
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r13, 576(%rcx)
	movl	$0, (%r13,%rax,4)
	movl	%ebp, 588(%rcx)
	movq	%r13, %rbx
.LBB8_173:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i452
                                        #   in Loop: Header=BB8_141 Depth=1
	movq	480(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_174:                              #   Parent Loop BB8_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_174
# BB#175:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i455
                                        #   in Loop: Header=BB8_141 Depth=1
	movl	488(%rsp), %eax
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	%eax, 584(%r13)
	testq	%rdi, %rdi
	je	.LBB8_177
.LBB8_176:                              #   in Loop: Header=BB8_141 Depth=1
	callq	_ZdaPv
	.p2align	4, 0x90
.LBB8_177:                              #   in Loop: Header=BB8_141 Depth=1
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_179
# BB#178:                               #   in Loop: Header=BB8_141 Depth=1
	callq	_ZdaPv
.LBB8_179:                              # %_ZN11CStringBaseIwED2Ev.exit462
                                        #   in Loop: Header=BB8_141 Depth=1
	incq	%r15
	movl	$8, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movslq	20(%rax), %rax
	cmpq	%rax, %r15
	jl	.LBB8_141
	jmp	.LBB8_398
.LBB8_180:
	xorl	%ebx, %ebx
	jmp	.LBB8_271
.LBB8_181:
	movups	_ZN14NUpdateArchive16kDeleteActionSetE+12(%rip), %xmm0
	movups	%xmm0, 204(%rsp)
	movups	_ZN14NUpdateArchive16kDeleteActionSetE(%rip), %xmm0
	jmp	.LBB8_183
.LBB8_182:
	movups	_ZN14NUpdateArchive16kUpdateActionSetE+12(%rip), %xmm0
	movups	%xmm0, 204(%rsp)
	movups	_ZN14NUpdateArchive16kUpdateActionSetE(%rip), %xmm0
.LBB8_183:
	movaps	%xmm0, 192(%rsp)
	movb	$1, 296(%r13)
	leaq	264(%r13), %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	$0, 488(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 480(%rsp)
	movl	$0, (%rax)
	movl	$4, 492(%rsp)
	leaq	496(%rsp), %rdi
.Ltmp59:
	callq	_ZN12CArchivePathC2Ev
.Ltmp60:
# BB#184:                               # %_ZN21CUpdateArchiveCommandC2Ev.exit.i
	movups	204(%rsp), %xmm0
	movups	%xmm0, 628(%rsp)
	movaps	192(%rsp), %xmm0
	movups	%xmm0, 616(%rsp)
.Ltmp62:
	leaq	480(%rsp), %rsi
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_
.Ltmp63:
# BB#185:
.Ltmp64:
	movl	$16, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp65:
# BB#186:
	cmpb	$0, (%rax)
	je	.LBB8_254
# BB#187:
.Ltmp66:
	movl	$16, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	%rax, %r13
.Ltmp67:
# BB#188:
	cmpl	$0, 20(%r13)
	jle	.LBB8_254
# BB#189:                               # %.lr.ph.i.i
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_190:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_194 Depth 2
                                        #       Child Loop BB8_198 Depth 3
                                        #       Child Loop BB8_205 Depth 3
                                        #     Child Loop BB8_225 Depth 2
                                        #     Child Loop BB8_246 Depth 2
	movq	24(%r13), %rax
	movq	(%rax,%r12,8), %r14
	movq	kUpdateIgnoreItselfPostStringID(%rip), %rsi
	movq	(%r14), %rdi
.Ltmp68:
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp69:
# BB#191:                               # %.noexc.i390
                                        #   in Loop: Header=BB8_190 Depth=1
	testl	%eax, %eax
	je	.LBB8_216
# BB#192:                               #   in Loop: Header=BB8_190 Depth=1
	movups	204(%rsp), %xmm0
	movups	%xmm0, 92(%rsp)
	movaps	192(%rsp), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	$0, 48(%rsp)
.Ltmp70:
	movl	$16, %edi
	callq	_Znam
.Ltmp71:
# BB#193:                               # %.noexc40.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%rax, 40(%rsp)
	movl	$0, (%rax)
	movl	$4, 52(%rsp)
	xorl	%ebx, %ebx
.LBB8_194:                              #   Parent Loop BB8_190 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_198 Depth 3
                                        #       Child Loop BB8_205 Depth 3
	movslq	8(%r14), %rax
	cmpq	%rax, %rbx
	jge	.LBB8_240
# BB#195:                               #   in Loop: Header=BB8_194 Depth=2
	movq	(%r14), %rax
	movl	(%rax,%rbx,4), %edi
.Ltmp72:
	callq	_Z11MyCharUpperw
.Ltmp73:
# BB#196:                               # %.noexc.i.i
                                        #   in Loop: Header=BB8_194 Depth=2
	movq	_ZL21kUpdatePairStateIDSet(%rip), %rcx
	movl	(%rcx), %edx
	cmpl	%eax, %edx
	movq	%rcx, %rbp
	je	.LBB8_200
# BB#197:                               # %.lr.ph.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB8_194 Depth=2
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB8_198:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB8_190 Depth=1
                                        #     Parent Loop BB8_194 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edx, %edx
	je	.LBB8_210
# BB#199:                               #   in Loop: Header=BB8_198 Depth=3
	movl	4(%rbp), %edx
	addq	$4, %rbp
	cmpl	%eax, %edx
	jne	.LBB8_198
.LBB8_200:                              # %_ZNK11CStringBaseIwE4FindEw.exit.i.i.i
                                        #   in Loop: Header=BB8_194 Depth=2
	subq	%rcx, %rbp
	movq	%rbp, %rax
	shrq	$2, %rax
	testl	%eax, %eax
	js	.LBB8_210
# BB#201:                               #   in Loop: Header=BB8_194 Depth=2
	movq	%rbx, %rax
	orq	$1, %rax
	movslq	8(%r14), %rcx
	cmpq	%rcx, %rax
	jge	.LBB8_481
# BB#202:                               #   in Loop: Header=BB8_194 Depth=2
	movq	(%r14), %rcx
	movl	(%rcx,%rax,4), %edi
.Ltmp74:
	callq	_Z11MyCharUpperw
.Ltmp75:
# BB#203:                               # %.noexc27.i.i
                                        #   in Loop: Header=BB8_194 Depth=2
	movq	_ZL22kUpdatePairActionIDSet(%rip), %rdx
	movl	(%rdx), %esi
	cmpl	%eax, %esi
	movq	%rdx, %rcx
	je	.LBB8_207
# BB#204:                               # %.lr.ph.i.i46.i.i.i.preheader
                                        #   in Loop: Header=BB8_194 Depth=2
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB8_205:                              # %.lr.ph.i.i46.i.i.i
                                        #   Parent Loop BB8_190 Depth=1
                                        #     Parent Loop BB8_194 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB8_481
# BB#206:                               #   in Loop: Header=BB8_205 Depth=3
	movl	4(%rcx), %esi
	addq	$4, %rcx
	cmpl	%eax, %esi
	jne	.LBB8_205
.LBB8_207:                              # %_ZNK11CStringBaseIwE4FindEw.exit48.i.i.i
                                        #   in Loop: Header=BB8_194 Depth=2
	subq	%rdx, %rcx
	shrq	$2, %rcx
	testl	%ecx, %ecx
	js	.LBB8_481
# BB#208:                               #   in Loop: Header=BB8_194 Depth=2
	cmpl	$4, %ecx
	jae	.LBB8_485
# BB#209:                               # %_ZL23GetUpdatePairActionTypei.exit.i.i.i
                                        #   in Loop: Header=BB8_194 Depth=2
	shlq	$30, %rbp
	sarq	$30, %rbp
	movl	%ecx, 80(%rsp,%rbp)
	addq	$2, %rbx
	cmpl	%ecx, _ZL35kUpdatePairStateNotSupportedActions(%rbp)
	jne	.LBB8_194
	jmp	.LBB8_481
	.p2align	4, 0x90
.LBB8_210:                              # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movl	8(%r14), %ecx
	subl	%ebx, %ecx
.Ltmp81:
	leaq	128(%rsp), %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp82:
# BB#211:                               # %.noexc26.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movl	$0, 48(%rsp)
	movq	40(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	136(%rsp), %rbp
	incq	%rbp
	movl	52(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB8_224
# BB#212:                               #   in Loop: Header=BB8_190 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp83:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp84:
# BB#213:                               # %.noexc.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_222
# BB#214:                               # %.noexc.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB8_223
# BB#215:                               # %._crit_edge.thread.i.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%rsp), %rax
	jmp	.LBB8_223
	.p2align	4, 0x90
.LBB8_216:                              #   in Loop: Header=BB8_190 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 296(%rax)
	je	.LBB8_253
# BB#217:                               #   in Loop: Header=BB8_190 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$0, 296(%rax)
	movl	276(%rax), %eax
	cmpl	$2, %eax
	movl	$1, %ebx
	cmovll	%eax, %ebx
	testl	%eax, %eax
	jle	.LBB8_221
# BB#218:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	280(%rax), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB8_221
# BB#219:                               #   in Loop: Header=BB8_190 Depth=1
.Ltmp114:
	movq	%rbp, %rdi
	callq	_ZN21CUpdateArchiveCommandD2Ev
.Ltmp115:
# BB#220:                               #   in Loop: Header=BB8_190 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB8_221:                              # %_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii.exit.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
.Ltmp117:
	xorl	%esi, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp118:
	jmp	.LBB8_253
.LBB8_222:                              #   in Loop: Header=BB8_190 Depth=1
	xorl	%eax, %eax
.LBB8_223:                              # %._crit_edge16.i.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%r15, 40(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 52(%rsp)
	movq	%r15, %rbx
.LBB8_224:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	128(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_225:                              #   Parent Loop BB8_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_225
# BB#226:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movl	136(%rsp), %ecx
	movl	%ecx, 48(%rsp)
	testq	%rdi, %rdi
	je	.LBB8_228
# BB#227:                               #   in Loop: Header=BB8_190 Depth=1
	callq	_ZdaPv
	movl	48(%rsp), %ecx
.LBB8_228:                              # %_ZL25ParseUpdateCommandString2RK11CStringBaseIwERN14NUpdateArchive10CActionSetERS0_.exit.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	testl	%ecx, %ecx
	je	.LBB8_241
# BB#229:                               #   in Loop: Header=BB8_190 Depth=1
	movq	40(%rsp), %rax
	movl	(%rax), %edi
.Ltmp86:
	callq	_Z11MyCharUpperw
.Ltmp87:
# BB#230:                               #   in Loop: Header=BB8_190 Depth=1
	cmpl	$33, %eax
	jne	.LBB8_500
# BB#231:                               #   in Loop: Header=BB8_190 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 288(%rsp)
.Ltmp91:
	movl	$16, %edi
	callq	_Znam
.Ltmp92:
# BB#232:                               # %.noexc33.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%rax, 288(%rsp)
	movl	$0, (%rax)
	movl	$4, 300(%rsp)
.Ltmp94:
	leaq	304(%rsp), %rdi
	callq	_ZN12CArchivePathC2Ev
.Ltmp95:
# BB#233:                               # %_ZN21CUpdateArchiveCommandC2Ev.exit.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movl	48(%rsp), %ecx
	decl	%ecx
.Ltmp97:
	movl	$1, %edx
	leaq	272(%rsp), %rdi
	leaq	40(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp98:
# BB#234:                               # %_ZNK11CStringBaseIwE3MidEi.exit.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movslq	280(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB8_502
# BB#235:                               #   in Loop: Header=BB8_190 Depth=1
	movl	$0, 296(%rsp)
	movq	288(%rsp), %rbx
	movl	$0, (%rbx)
	incq	%rbp
	movl	300(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB8_245
# BB#236:                               #   in Loop: Header=BB8_190 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp100:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp101:
# BB#237:                               # %.noexc40.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_243
# BB#238:                               # %.noexc40.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB8_244
# BB#239:                               # %._crit_edge.thread.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	296(%rsp), %rax
	jmp	.LBB8_244
.LBB8_240:                              # %_ZL25ParseUpdateCommandString2RK11CStringBaseIwERN14NUpdateArchive10CActionSetERS0_.exit.thread.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movl	$0, 48(%rsp)
	movq	40(%rsp), %rax
	movl	$0, (%rax)
.LBB8_241:                              #   in Loop: Header=BB8_190 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 296(%rax)
	je	.LBB8_251
# BB#242:                               #   in Loop: Header=BB8_190 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	280(%rax), %rax
	movq	(%rax), %rax
	movups	92(%rsp), %xmm0
	movups	%xmm0, 148(%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 136(%rax)
	jmp	.LBB8_251
.LBB8_243:                              #   in Loop: Header=BB8_190 Depth=1
	xorl	%eax, %eax
.LBB8_244:                              # %._crit_edge16.i.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	%r15, 288(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 300(%rsp)
	movq	%r15, %rbx
.LBB8_245:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
	movq	272(%rsp), %rax
	.p2align	4, 0x90
.LBB8_246:                              #   Parent Loop BB8_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_246
# BB#247:                               #   in Loop: Header=BB8_190 Depth=1
	movl	280(%rsp), %eax
	movl	%eax, 296(%rsp)
	movups	92(%rsp), %xmm0
	leaq	304(%rsp), %rax
	movups	%xmm0, 132(%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 120(%rax)
.Ltmp102:
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	288(%rsp), %rsi
	callq	_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_
.Ltmp103:
# BB#248:                               #   in Loop: Header=BB8_190 Depth=1
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_250
# BB#249:                               #   in Loop: Header=BB8_190 Depth=1
	callq	_ZdaPv
.LBB8_250:                              # %_ZN11CStringBaseIwED2Ev.exit41.i.i
                                        #   in Loop: Header=BB8_190 Depth=1
.Ltmp105:
	leaq	288(%rsp), %rdi
	callq	_ZN21CUpdateArchiveCommandD2Ev
.Ltmp106:
.LBB8_251:                              #   in Loop: Header=BB8_190 Depth=1
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_253
# BB#252:                               #   in Loop: Header=BB8_190 Depth=1
	callq	_ZdaPv
.LBB8_253:                              # %.noexc39.i
                                        #   in Loop: Header=BB8_190 Depth=1
	incq	%r12
	movslq	20(%r13), %rax
	cmpq	%rax, %r12
	jl	.LBB8_190
.LBB8_254:                              # %_ZL24ParseUpdateCommandStringR14CUpdateOptionsRK13CObjectVectorI11CStringBaseIwEERKN14NUpdateArchive10CActionSetE.exit.i
.Ltmp120:
	movl	$10, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp121:
# BB#255:
	cmpb	$0, (%rax)
	je	.LBB8_404
# BB#256:
.Ltmp122:
	movl	$10, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp123:
# BB#257:
	movq	24(%rax), %rax
	movq	(%rax), %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	496(%rax), %rdi
	cmpl	$0, 8(%r13)
	je	.LBB8_264
# BB#258:
	cmpq	%rdi, %r13
	je	.LBB8_404
# BB#259:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$0, 504(%rbp)
	movq	496(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	8(%r13), %r14
	incq	%r14
	movl	508(%rbp), %r12d
	cmpl	%r12d, %r14d
	je	.LBB8_401
# BB#260:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp124:
	callq	_Znam
	movq	%rax, %r15
.Ltmp125:
# BB#261:                               # %.noexc42.i
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_399
# BB#262:                               # %.noexc42.i
	testl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB8_400
# BB#263:                               # %._crit_edge.thread.i.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	504(%rbp), %rax
	jmp	.LBB8_400
.LBB8_264:
.Ltmp126:
	callq	_ZN8NWindows5NFile10NDirectory13MyGetTempPathER11CStringBaseIwE
.Ltmp127:
	jmp	.LBB8_404
.LBB8_265:                              # %_ZNK11CStringBaseIwE4FindERKS0_.exit
	cmpl	$-1, %eax
	je	.LBB8_123
# BB#266:                               # %_ZN11CStringBaseIwED2Ev.exit266.thread
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB8_269
.LBB8_267:                              # %.critedge.us.i.i262
	movl	%ecx, %r15d
.LBB8_268:                              # %_ZN11CStringBaseIwED2Ev.exit266
	movq	%rax, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_ZdaPv
	cmpl	$-1, %r15d
	je	.LBB8_270
.LBB8_269:
	xorl	%ebx, %ebx
.Ltmp179:
	leaq	224(%rsp), %rdi
	movl	$1, %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp180:
	jmp	.LBB8_271
.LBB8_270:
	movb	$1, %bl
.LBB8_271:                              # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit
.Ltmp181:
	leaq	224(%rsp), %rdi
	callq	_ZN9NWildcard7CCensor13ExtendExcludeEv
.Ltmp182:
# BB#272:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 5(%rax)
	je	.LBB8_277
# BB#273:
.Ltmp183:
	movl	$20, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp184:
# BB#274:
	movq	24(%rax), %rax
	movq	(%rax), %rbx
	movslq	8(%rbx), %r13
	leaq	1(%r13), %r15
	testl	%r15d, %r15d
	je	.LBB8_305
# BB#275:                               # %._crit_edge16.i.i271
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp185:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp186:
# BB#276:                               # %.noexc276
	movl	$0, (%rbp)
	jmp	.LBB8_306
.LBB8_277:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 488(%rsp)
	movq	$8, 504(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 480(%rsp)
	movups	%xmm0, 296(%rsp)
	movq	$8, 312(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 288(%rsp)
	movups	%xmm0, 328(%rsp)
	movq	$4, 344(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 320(%rsp)
	movups	%xmm0, 360(%rsp)
	movq	$4, 376(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 352(%rsp)
	movups	%xmm0, 392(%rsp)
	movq	$8, 408(%rsp)
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, 384(%rsp)
	movups	%xmm0, 88(%rsp)
	movq	$8, 104(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
	movups	%xmm0, 200(%rsp)
	movq	$4, 216(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 192(%rsp)
.Ltmp202:
	leaq	224(%rsp), %rdi
	leaq	288(%rsp), %rsi
	leaq	80(%rsp), %rbp
	leaq	192(%rsp), %r8
	xorl	%edx, %edx
	movq	%rbp, %rcx
	callq	_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE
.Ltmp203:
# BB#278:
	testl	%eax, %eax
	jne	.LBB8_508
# BB#279:
	cmpl	$0, 92(%rsp)
	jg	.LBB8_508
# BB#280:
.Ltmp205:
	leaq	192(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp206:
# BB#281:
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
.Ltmp208:
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp209:
# BB#282:
.Ltmp214:
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp215:
# BB#283:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movl	396(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB8_299
# BB#284:                               # %.lr.ph598
	xorl	%ebp, %ebp
	leaq	288(%rsp), %r12
	leaq	480(%rsp), %r13
	.p2align	4, 0x90
.LBB8_285:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_293 Depth 2
	movq	400(%rsp), %rcx
	movq	(%rcx,%rbp,8), %rcx
	testb	$16, 48(%rcx)
	jne	.LBB8_298
# BB#286:                               #   in Loop: Header=BB8_285 Depth=1
.Ltmp217:
	leaq	80(%rsp), %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	_ZNK9CDirItems10GetPhyPathEi
.Ltmp218:
# BB#287:                               #   in Loop: Header=BB8_285 Depth=1
.Ltmp220:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp221:
# BB#288:                               # %.noexc307
                                        #   in Loop: Header=BB8_285 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	88(%rsp), %r14
	leaq	1(%r14), %r15
	testl	%r15d, %r15d
	je	.LBB8_291
# BB#289:                               # %._crit_edge16.i.i.i302
                                        #   in Loop: Header=BB8_285 Depth=1
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp222:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp223:
# BB#290:                               # %.noexc.i303
                                        #   in Loop: Header=BB8_285 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r15d, 12(%rbx)
	jmp	.LBB8_292
.LBB8_291:                              #   in Loop: Header=BB8_285 Depth=1
	xorl	%eax, %eax
.LBB8_292:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i304
                                        #   in Loop: Header=BB8_285 Depth=1
	movq	80(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_293:                              #   Parent Loop BB8_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_293
# BB#294:                               #   in Loop: Header=BB8_285 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp225:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp226:
# BB#295:                               #   in Loop: Header=BB8_285 Depth=1
	movq	496(%rsp), %rax
	movslq	492(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 492(%rsp)
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_297
# BB#296:                               #   in Loop: Header=BB8_285 Depth=1
	callq	_ZdaPv
.LBB8_297:                              # %_ZN11CStringBaseIwED2Ev.exit312
                                        #   in Loop: Header=BB8_285 Depth=1
	movl	396(%rsp), %eax
.LBB8_298:                              #   in Loop: Header=BB8_285 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB8_285
.LBB8_299:                              # %._crit_edge599
.Ltmp228:
	leaq	288(%rsp), %rdi
	callq	_ZN9CDirItemsD2Ev
.Ltmp229:
# BB#300:
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	je	.LBB8_320
# BB#301:
.Ltmp231:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp232:
# BB#302:                               # %.noexc319
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	8(%rsp), %r15           # 8-byte Reload
	movslq	64(%r15), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB8_321
# BB#303:                               # %._crit_edge16.i.i.i314
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp233:
	callq	_Znam
.Ltmp234:
# BB#304:                               # %.noexc.i315
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB8_322
.LBB8_305:
	xorl	%ebp, %ebp
.LBB8_306:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i272
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_307:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbp,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_307
# BB#308:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit
.Ltmp187:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp188:
# BB#309:                               # %.noexc281
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %r12
	cmovnoq	%rax, %r12
.Ltmp189:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp190:
# BB#310:                               # %.noexc.i277
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r15d, 12(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_311:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i278
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_311
# BB#312:
	movl	%r13d, 8(%rbx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	subq	$-128, %rdi
.Ltmp192:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp193:
# BB#313:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	144(%rdx), %rax
	movslq	140(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 140(%rdx)
.Ltmp194:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp195:
# BB#314:                               # %.noexc288
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.Ltmp196:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp197:
# BB#315:                               # %.noexc.i284
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r15d, 12(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_316:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i285
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_316
# BB#317:
	movl	%r13d, 8(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	160(%rax), %rdi
.Ltmp199:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp200:
# BB#318:
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	176(%r14), %rax
	movslq	172(%r14), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 172(%r14)
	testq	%rbp, %rbp
	je	.LBB8_369
# BB#319:
	movq	%rbp, %rdi
	callq	_ZdaPv
	cmpl	$2, 120(%rsp)           # 4-byte Folded Reload
	jbe	.LBB8_370
	jmp	.LBB8_396
.LBB8_320:                              # %._crit_edge650
	movl	492(%rsp), %eax
	leaq	480(%rsp), %rbp
	jmp	.LBB8_326
.LBB8_321:
	xorl	%eax, %eax
.LBB8_322:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i316
	movq	56(%r15), %rcx
	.p2align	4, 0x90
.LBB8_323:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_323
# BB#324:
	movl	%r14d, 8(%rbx)
.Ltmp236:
	leaq	480(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp237:
# BB#325:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit323
	movq	496(%rsp), %rax
	movslq	492(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 492(%rsp)
.LBB8_326:
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	je	.LBB8_511
# BB#327:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rsp)
	movq	$8, 152(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%rsp)
	testl	%eax, %eax
	jle	.LBB8_342
# BB#328:                               # %.lr.ph595
	xorl	%r14d, %r14d
	leaq	176(%rsp), %r12
	leaq	128(%rsp), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	$-1, %r13
	.p2align	4, 0x90
.LBB8_329:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_337 Depth 2
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rsp)
.Ltmp238:
	movl	$16, %edi
	callq	_Znam
.Ltmp239:
# BB#330:                               #   in Loop: Header=BB8_329 Depth=1
	movq	%rax, 176(%rsp)
	movl	$0, (%rax)
	movl	$4, 188(%rsp)
	movq	496(%rsp), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rdi
.Ltmp241:
	movq	%r12, %rsi
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwE
.Ltmp242:
# BB#331:                               #   in Loop: Header=BB8_329 Depth=1
.Ltmp243:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp244:
# BB#332:                               # %.noexc331
                                        #   in Loop: Header=BB8_329 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	184(%rsp), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB8_335
# BB#333:                               # %._crit_edge16.i.i.i326
                                        #   in Loop: Header=BB8_329 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	cmovoq	%r13, %rax
.Ltmp245:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp246:
# BB#334:                               # %.noexc.i327
                                        #   in Loop: Header=BB8_329 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB8_336
	.p2align	4, 0x90
.LBB8_335:                              #   in Loop: Header=BB8_329 Depth=1
	xorl	%eax, %eax
.LBB8_336:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i328
                                        #   in Loop: Header=BB8_329 Depth=1
	movq	176(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_337:                              #   Parent Loop BB8_329 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_337
# BB#338:                               #   in Loop: Header=BB8_329 Depth=1
	movl	%r15d, 8(%rbp)
.Ltmp248:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp249:
# BB#339:                               #   in Loop: Header=BB8_329 Depth=1
	movq	144(%rsp), %rax
	movslq	140(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 140(%rsp)
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_341
# BB#340:                               #   in Loop: Header=BB8_329 Depth=1
	callq	_ZdaPv
.LBB8_341:                              # %_ZN11CStringBaseIwED2Ev.exit336
                                        #   in Loop: Header=BB8_329 Depth=1
	incq	%r14
	movslq	492(%rsp), %rax
	cmpq	%rax, %r14
	jl	.LBB8_329
.LBB8_342:                              # %._crit_edge596
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
	movq	$4, 64(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 40(%rsp)
.Ltmp251:
	leaq	128(%rsp), %rdi
	leaq	40(%rsp), %rsi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	%rsi, 456(%rsp)         # 8-byte Spill
	callq	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
.Ltmp252:
# BB#343:
	movq	8(%rsp), %r13           # 8-byte Reload
	subq	$-128, %r13
	movl	52(%rsp), %esi
.Ltmp253:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp254:
# BB#344:
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	160(%rax), %rdi
	movl	52(%rsp), %esi
.Ltmp255:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp256:
# BB#345:                               # %.preheader
	cmpl	$0, 52(%rsp)
	jle	.LBB8_364
# BB#346:                               # %.lr.ph
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_347:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_353 Depth 2
                                        #     Child Loop BB8_361 Depth 2
	movq	56(%rsp), %rax
	movq	496(%rsp), %rcx
	movslq	(%rax,%r15,4), %rax
	movq	(%rcx,%rax,8), %r12
.Ltmp258:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp259:
# BB#348:                               # %.noexc343
                                        #   in Loop: Header=BB8_347 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r12), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB8_351
# BB#349:                               # %._crit_edge16.i.i.i338
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp260:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp261:
# BB#350:                               # %.noexc.i339
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB8_352
	.p2align	4, 0x90
.LBB8_351:                              #   in Loop: Header=BB8_347 Depth=1
	xorl	%eax, %eax
.LBB8_352:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i340
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	(%r12), %rcx
	.p2align	4, 0x90
.LBB8_353:                              #   Parent Loop BB8_347 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_353
# BB#354:                               #   in Loop: Header=BB8_347 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp263:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp264:
# BB#355:                               #   in Loop: Header=BB8_347 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	144(%rdx), %rax
	movslq	140(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 140(%rdx)
	movq	56(%rsp), %rax
	movq	144(%rsp), %rcx
	movslq	(%rax,%r15,4), %rax
	movq	(%rcx,%rax,8), %rbp
.Ltmp265:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp266:
# BB#356:                               # %.noexc353
                                        #   in Loop: Header=BB8_347 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%rbp), %r14
	leaq	1(%r14), %r12
	testl	%r12d, %r12d
	je	.LBB8_359
# BB#357:                               # %._crit_edge16.i.i.i348
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp267:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp268:
# BB#358:                               # %.noexc.i349
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB8_360
	.p2align	4, 0x90
.LBB8_359:                              #   in Loop: Header=BB8_347 Depth=1
	xorl	%eax, %eax
.LBB8_360:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i350
                                        #   in Loop: Header=BB8_347 Depth=1
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB8_361:                              #   Parent Loop BB8_347 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_361
# BB#362:                               #   in Loop: Header=BB8_347 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp270:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp271:
# BB#363:                               #   in Loop: Header=BB8_347 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	176(%rdx), %rax
	movslq	172(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 172(%rdx)
	incq	%r15
	movslq	52(%rsp), %rax
	cmpq	%rax, %r15
	jl	.LBB8_347
.LBB8_364:                              # %._crit_edge
.Ltmp275:
	leaq	40(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp276:
# BB#365:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%rsp)
.Ltmp286:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp287:
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
# BB#366:
.Ltmp292:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp293:
# BB#367:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit361
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 480(%rsp)
.Ltmp295:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp296:
# BB#368:
.Ltmp301:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp302:
.LBB8_369:                              # %_ZN11CStringBaseIwED2Ev.exit293
	cmpl	$2, 120(%rsp)           # 4-byte Folded Reload
	ja	.LBB8_396
.LBB8_370:
	leaq	192(%r14), %rsi
.Ltmp304:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE
.Ltmp305:
# BB#371:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 6(%rax)
	je	.LBB8_374
# BB#372:
	cmpb	$0, 3(%rax)
	je	.LBB8_374
# BB#373:
	cmpb	$0, 4(%rax)
	jne	.LBB8_513
.LBB8_374:
.Ltmp308:
	movl	$9, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp309:
# BB#375:
	cmpb	$0, (%rax)
	je	.LBB8_390
# BB#376:
.Ltmp310:
	movl	$9, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp311:
# BB#377:
	movq	24(%rax), %rax
	movq	(%rax), %r14
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	104(%rax), %r15
	cmpq	%r15, %r14
	je	.LBB8_389
# BB#378:
	movl	$0, 112(%rax)
	movq	104(%rax), %rbp
	movl	$0, (%rbp)
	movslq	8(%r14), %rbx
	incq	%rbx
	movl	116(%rax), %r13d
	cmpl	%r13d, %ebx
	jne	.LBB8_380
# BB#379:
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB8_386
.LBB8_380:
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp312:
	callq	_Znam
	movq	%rax, %r12
.Ltmp313:
# BB#381:                               # %.noexc383
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB8_384
# BB#382:                               # %.noexc383
	testl	%r13d, %r13d
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB8_385
# BB#383:                               # %._crit_edge.thread.i.i377
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	112(%r13), %rax
	jmp	.LBB8_385
.LBB8_384:
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB8_385:                              # %._crit_edge16.i.i378
	movq	%r12, 104(%r13)
	movl	$0, (%r12,%rax,4)
	movl	%ebx, 116(%r13)
	movq	%r12, %rbp
.LBB8_386:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i379
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB8_387:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB8_387
# BB#388:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i382
	movl	8(%r14), %eax
	movl	%eax, 112(%r13)
.LBB8_389:                              # %_ZN11CStringBaseIwEaSERKS0_.exit384
.Ltmp314:
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE
.Ltmp315:
.LBB8_390:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, 120(%rax)
.Ltmp316:
	movl	$22, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp317:
# BB#391:
	cmpb	$0, (%rax)
	je	.LBB8_394
# BB#392:
.Ltmp318:
	movl	$22, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp319:
# BB#393:
	movslq	40(%rax), %rax
	movl	k_OverwriteModes(,%rax,4), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 120(%rcx)
	jmp	.LBB8_396
.LBB8_394:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 8(%rax)
	je	.LBB8_396
# BB#395:
	movl	$1, 120(%rax)
.LBB8_396:
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 224(%rsp)
.Ltmp321:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp322:
# BB#397:                               # %_ZN9NWildcard7CCensorD2Ev.exit
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.LBB8_398:                              # %_ZN11CStringBaseIwEaSERKS0_.exit421
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9NWildcard7CCensor13ExtendExcludeEv
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_399:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB8_400:                              # %._crit_edge16.i.i.i394
	movq	%r15, 496(%rbp)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 508(%rbp)
	movq	%r15, %rbx
.LBB8_401:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i395
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB8_402:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_402
# BB#403:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i.i
	movl	8(%r13), %eax
	movl	%eax, 504(%rbp)
.LBB8_404:                              # %_ZN11CStringBaseIwEaSERKS0_.exit.i
.Ltmp129:
	movl	$19, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp130:
# BB#405:
	movb	(%rax), %al
	testb	%al, %al
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	%al, 424(%rcx)
	je	.LBB8_418
# BB#406:
.Ltmp131:
	movl	$19, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp132:
# BB#407:
	movq	24(%rax), %rax
	movq	(%rax), %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	432(%rbp), %rax
	cmpq	%rax, %r14
	je	.LBB8_418
# BB#408:
	movl	$0, 440(%rbp)
	movq	432(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %r13
	incq	%r13
	movl	444(%rbp), %r12d
	cmpl	%r12d, %r13d
	je	.LBB8_415
# BB#409:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp133:
	callq	_Znam
	movq	%rax, %r15
.Ltmp134:
# BB#410:                               # %.noexc52.i
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_413
# BB#411:                               # %.noexc52.i
	testl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB8_414
# BB#412:                               # %._crit_edge.thread.i.i46.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	440(%rbp), %rax
	jmp	.LBB8_414
.LBB8_413:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB8_414:                              # %._crit_edge16.i.i47.i
	movq	%r15, 432(%rbp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 444(%rbp)
	movq	%r15, %rbx
.LBB8_415:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i48.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB8_416:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_416
# BB#417:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i51.i
	movl	8(%r14), %eax
	movl	%eax, 440(%rbp)
.LBB8_418:                              # %_ZN11CStringBaseIwEaSERKS0_.exit53.i
.Ltmp135:
	movl	$17, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp136:
# BB#419:
	cmpb	$0, (%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB8_447
# BB#420:
.Ltmp137:
	movl	$17, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp138:
# BB#421:                               # %.preheader.i398
	cmpl	$0, 20(%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB8_447
# BB#422:                               # %.lr.ph.i399
	leaq	512(%rbp), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
                                        # implicit-def: %RCX
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_423:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_428 Depth 2
	movq	24(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rbp
	movslq	8(%rax), %r14
	leaq	1(%r14), %rax
	testl	%eax, %eax
	je	.LBB8_426
# BB#424:                               # %._crit_edge16.i.i.i54.i
                                        #   in Loop: Header=BB8_423 Depth=1
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp140:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp141:
# BB#425:                               # %.noexc62.i
                                        #   in Loop: Header=BB8_423 Depth=1
	movl	$0, (%r15)
	movq	%r15, %rbx
	jmp	.LBB8_427
	.p2align	4, 0x90
.LBB8_426:                              #   in Loop: Header=BB8_423 Depth=1
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
.LBB8_427:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i55.i
                                        #   in Loop: Header=BB8_423 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_428:                              #   Parent Loop BB8_423 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_428
# BB#429:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i.i
                                        #   in Loop: Header=BB8_423 Depth=1
.Ltmp142:
	movq	%rbx, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp143:
# BB#430:                               # %_ZN11CStringBaseIwE9MakeUpperEv.exit.i.i
                                        #   in Loop: Header=BB8_423 Depth=1
.Ltmp145:
	movq	%rbx, %rdi
	leaq	288(%rsp), %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
	movq	%rax, %r13
.Ltmp146:
# BB#431:                               #   in Loop: Header=BB8_423 Depth=1
	movq	288(%rsp), %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	xorl	%ebp, %ebp
	testl	%esi, %esi
	je	.LBB8_435
# BB#432:                               #   in Loop: Header=BB8_423 Depth=1
	leal	1(%rsi), %eax
	cmpl	%r14d, %eax
	jl	.LBB8_435
# BB#433:                               #   in Loop: Header=BB8_423 Depth=1
	movb	$1, %bpl
	cmpl	%r14d, %esi
	jne	.LBB8_438
# BB#434:                               #   in Loop: Header=BB8_423 Depth=1
	movq	%r13, 24(%rsp)          # 8-byte Spill
.LBB8_435:                              #   in Loop: Header=BB8_423 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_437
# BB#436:                               #   in Loop: Header=BB8_423 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB8_437:                              # %_ZL16ParseComplexSizeRK11CStringBaseIwERy.exit.i
                                        #   in Loop: Header=BB8_423 Depth=1
	testb	%bpl, %bpl
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB8_445
	jmp	.LBB8_493
	.p2align	4, 0x90
.LBB8_438:                              #   in Loop: Header=BB8_423 Depth=1
	shlq	$30, %rcx
	sarq	$30, %rcx
	movl	(%rbx,%rcx), %ecx
	addl	$-66, %ecx
	cmpl	$11, %ecx
	ja	.LBB8_492
# BB#439:                               #   in Loop: Header=BB8_423 Depth=1
	movl	$10, %eax
	jmpq	*.LJTI8_0(,%rcx,8)
.LBB8_440:                              #   in Loop: Header=BB8_423 Depth=1
	movl	$30, %eax
	jmp	.LBB8_442
.LBB8_441:                              #   in Loop: Header=BB8_423 Depth=1
	movl	$20, %eax
.LBB8_442:                              #   in Loop: Header=BB8_423 Depth=1
	movl	$64, %ecx
	subl	%eax, %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rdx
	cmpq	%rdx, %r13
	jae	.LBB8_492
# BB#443:                               #   in Loop: Header=BB8_423 Depth=1
	movl	%eax, %ecx
	shlq	%cl, %r13
.LBB8_444:                              # %.thread.i.thread.i
                                        #   in Loop: Header=BB8_423 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB8_445:                              #   in Loop: Header=BB8_423 Depth=1
.Ltmp154:
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp155:
# BB#446:                               #   in Loop: Header=BB8_423 Depth=1
	movq	528(%rbp), %rax
	movslq	524(%rbp), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 524(%rbp)
	incq	%r12
	movq	72(%rsp), %rax          # 8-byte Reload
	movslq	20(%rax), %rcx
	cmpq	%rcx, %r12
	jl	.LBB8_423
.LBB8_447:                              # %_ZL20SetAddCommandOptionsN12NCommandType5EEnumERKN18NCommandLineParser7CParserER14CUpdateOptions.exit
	leaq	480(%rsp), %rdi
	callq	_ZN21CUpdateArchiveCommandD2Ev
	leaq	232(%rbp), %rsi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	movl	%eax, %ecx
	xorb	$1, %cl
	movb	%cl, 560(%rbp)
	testb	%al, %al
	je	.LBB8_452
.LBB8_448:
	movl	$23, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
	movb	(%rax), %al
	testb	%al, %al
	movb	%al, 473(%rbp)
	je	.LBB8_467
# BB#449:
	movl	$23, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax), %r12
	leaq	480(%rbp), %rdx
	cmpq	%rdx, %r12
	je	.LBB8_454
# BB#450:
	movl	$0, 488(%rbp)
	movq	480(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	8(%r12), %r14
	incq	%r14
	movl	492(%rbp), %r13d
	cmpl	%r13d, %r14d
	jne	.LBB8_455
# BB#451:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB8_462
.LBB8_452:
	cmpb	$0, 6(%rbp)
	je	.LBB8_458
# BB#453:
	cmpb	$0, 4(%rbp)
	jne	.LBB8_448
	jmp	.LBB8_459
.LBB8_454:                              # %._ZN11CStringBaseIwEaSERKS0_.exit411_crit_edge
	movl	488(%rbp), %eax
	testl	%eax, %eax
	jg	.LBB8_465
	jmp	.LBB8_467
.LBB8_455:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_460
# BB#456:
	testl	%r13d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB8_461
# BB#457:                               # %._crit_edge.thread.i.i405
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	488(%rbp), %rax
	jmp	.LBB8_461
.LBB8_458:                              # %.thread504
	cmpb	$0, 3(%rbp)
	jne	.LBB8_448
.LBB8_459:
	movb	$0, 560(%rbp)
	jmp	.LBB8_448
.LBB8_460:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB8_461:                              # %._crit_edge16.i.i406
	movq	%r15, 480(%rbp)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 492(%rbp)
	movq	%r15, %rbx
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB8_462:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i407
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB8_463:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_463
# BB#464:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i410
	movl	8(%r12), %eax
	movl	%eax, 488(%rbp)
	testl	%eax, %eax
	jle	.LBB8_467
.LBB8_465:
	movq	(%rdx), %rdi
	cmpl	$46, (%rdi)
	jne	.LBB8_467
# BB#466:
	movb	$1, 474(%rbp)
	movslq	%eax, %rdx
	shlq	$2, %rdx
	leaq	4(%rdi), %rsi
	callq	memmove
	decl	488(%rbp)
.LBB8_467:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
	movb	6(%rbp), %cl
	movb	%cl, 472(%rbp)
	movb	5(%rbp), %al
	movb	%al, 449(%rbp)
	testb	%cl, %cl
	je	.LBB8_469
# BB#468:                               # %_ZN11CStringBaseIwE6DeleteEii.exit
	movb	473(%rbp), %dl
	testb	%dl, %dl
	jne	.LBB8_510
.LBB8_469:
	testb	%cl, %cl
	je	.LBB8_471
# BB#470:
	cmpb	$0, 3(%rbp)
	jne	.LBB8_512
.LBB8_471:
	testb	%al, %al
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB8_398
# BB#472:
	movl	$20, %esi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movq	24(%rax), %rax
	movq	(%rax), %r14
	leaq	456(%rbp), %rax
	cmpq	%rax, %r14
	je	.LBB8_398
# BB#473:
	movl	$0, 464(%rbp)
	movq	456(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %r13
	incq	%r13
	movl	468(%rbp), %r12d
	cmpl	%r12d, %r13d
	je	.LBB8_478
# BB#474:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_477
# BB#475:
	testl	%r12d, %r12d
	jle	.LBB8_477
# BB#476:                               # %._crit_edge.thread.i.i415
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	464(%rbp), %rax
.LBB8_477:                              # %._crit_edge16.i.i416
	movq	%r15, 456(%rbp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 468(%rbp)
	movq	%r15, %rbx
.LBB8_478:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i417
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB8_479:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB8_479
# BB#480:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i420
	movl	8(%r14), %eax
	movl	%eax, 464(%rbp)
	jmp	.LBB8_398
.LBB8_481:                              # %.loopexit.i.i
.Ltmp79:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp80:
# BB#482:                               # %.noexc29.i.i
.LBB8_483:                              # %.us-lcssa.us.i
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.48, (%rax)
.LBB8_484:                              # %.us-lcssa.us.i
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB8_485:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$98111603, (%rax)       # imm = 0x5D91073
.Ltmp77:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp78:
# BB#486:                               # %.noexc28.i.i
.LBB8_487:
.Ltmp54:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp55:
# BB#488:                               # %.noexc460
.LBB8_489:
	callq	_ZL23ThrowUserErrorExceptionv
.LBB8_490:
.Ltmp56:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp57:
# BB#491:                               # %.noexc431
.LBB8_492:                              # %.thread.i.i
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB8_493:                              # %.loopexit89.i
	movl	$16, %edi
	callq	__cxa_allocate_exception
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.Ltmp148:
	movl	$22, %edi
	callq	_Znam
.Ltmp149:
# BB#494:                               # %.noexc71.i
	movq	%rax, (%rbx)
	movl	$22, 12(%rbx)
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [73,110,99,111,114,114,101,99,116,32,118,111,108,117,109,101]
	movups	%xmm0, (%rax)
	movb	$32, 16(%rax)
	movb	$115, 17(%rax)
	movb	$105, 18(%rax)
	movb	$122, 19(%rax)
	movb	$101, 20(%rax)
	movb	$0, 21(%rax)
	movl	$21, 8(%rbx)
.Ltmp151:
	movl	$_ZTI28CArchiveCommandLineException, %esi
	movl	$_ZN11CStringBaseIcED2Ev, %edx
	movq	%rbx, %rdi
	callq	__cxa_throw
.Ltmp152:
# BB#495:                               # %.noexc65.i
.LBB8_496:
.Ltmp51:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp52:
# BB#497:                               # %.noexc436
.LBB8_498:
.Ltmp49:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp50:
# BB#499:                               # %.noexc438
.LBB8_500:
.Ltmp88:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp89:
# BB#501:                               # %.noexc30.i.i
.LBB8_502:
.Ltmp108:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp109:
# BB#503:                               # %.noexc37.i.i
.LBB8_504:                              # %_ZNK9NWildcard7CCensor14AllAreRelativeEv.exit.thread
	movl	$.L.str.4, %edi
	callq	_ZL14ThrowExceptionPKc
.LBB8_505:
	callq	_ZL23ThrowUserErrorExceptionv
.LBB8_506:
.Ltmp43:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp44:
# BB#507:                               # %.noexc444
.LBB8_508:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.6, (%rax)
.Ltmp330:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp331:
	jmp	.LBB8_514
.LBB8_509:
	callq	_ZL23ThrowUserErrorExceptionv
.LBB8_510:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.8, (%rax)
	jmp	.LBB8_484
.LBB8_511:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.7, (%rax)
.Ltmp327:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp328:
	jmp	.LBB8_514
.LBB8_512:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.52, (%rax)
	jmp	.LBB8_484
.LBB8_513:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.50, (%rax)
.Ltmp306:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp307:
.LBB8_514:
.LBB8_515:
.Ltmp332:
	jmp	.LBB8_541
.LBB8_516:                              # %.loopexit.split-lp521
.Ltmp45:
	jmp	.LBB8_592
.LBB8_517:                              # %.loopexit.split-lp50.i.i
.Ltmp110:
	jmp	.LBB8_576
.LBB8_518:                              # %.loopexit.split-lp526
.Ltmp53:
	jmp	.LBB8_592
.LBB8_519:                              # %.loopexit.split-lp.i
.Ltmp153:
	movq	%rax, %r14
	jmp	.LBB8_617
.LBB8_520:
.Ltmp150:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	__cxa_free_exception
	jmp	.LBB8_617
.LBB8_521:                              # %.loopexit.split-lp516
.Ltmp58:
	jmp	.LBB8_592
.LBB8_522:                              # %.loopexit.split-lp.loopexit.split-lp.i.i
.Ltmp90:
	jmp	.LBB8_614
.LBB8_523:
.Ltmp235:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB8_627
.LBB8_524:
.Ltmp128:
	movq	%rax, %r14
	jmp	.LBB8_617
.LBB8_525:
.Ltmp39:
	movq	%rax, %r14
	movq	480(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_593
# BB#526:
	callq	_ZdaPv
	jmp	.LBB8_593
.LBB8_527:
.Ltmp303:
	jmp	.LBB8_590
.LBB8_528:
.Ltmp297:
	movq	%rax, %r14
.Ltmp298:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp299:
	jmp	.LBB8_629
.LBB8_529:
.Ltmp300:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_530:
.Ltmp294:
	movq	%rax, %r14
	jmp	.LBB8_627
.LBB8_531:
.Ltmp288:
	movq	%rax, %r14
.Ltmp289:
	leaq	128(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp290:
	jmp	.LBB8_627
.LBB8_532:
.Ltmp291:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_533:
.Ltmp277:
	jmp	.LBB8_605
.LBB8_534:
.Ltmp329:
	movq	%rax, %r14
	jmp	.LBB8_627
.LBB8_535:
.Ltmp230:
	movq	%rax, %r14
	jmp	.LBB8_627
.LBB8_536:
.Ltmp216:
	movq	%rax, %r14
	jmp	.LBB8_609
.LBB8_537:
.Ltmp210:
	movq	%rax, %r14
.Ltmp211:
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp212:
	jmp	.LBB8_609
.LBB8_538:
.Ltmp213:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_539:
.Ltmp207:
	movq	%rax, %r14
	jmp	.LBB8_542
.LBB8_540:
.Ltmp204:
.LBB8_541:
	movq	%rax, %r14
.Ltmp333:
	leaq	192(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp334:
.LBB8_542:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
.Ltmp335:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp336:
# BB#543:
.Ltmp341:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp342:
	jmp	.LBB8_609
.LBB8_544:
.Ltmp337:
	movq	%rax, %rbx
.Ltmp338:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp339:
	jmp	.LBB8_639
.LBB8_545:
.Ltmp340:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_546:                              # %_ZN11CStringBaseIwED2Ev.exit268
.Ltmp178:
	movq	%rax, %r14
	jmp	.LBB8_574
.LBB8_547:
.Ltmp175:
	jmp	.LBB8_590
.LBB8_548:
.Ltmp61:
	movq	%rax, %r14
	movq	480(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_631
# BB#549:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_550:
.Ltmp116:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB8_617
.LBB8_551:
.Ltmp198:
	jmp	.LBB8_553
.LBB8_552:
.Ltmp191:
.LBB8_553:                              # %.body
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	je	.LBB8_629
	jmp	.LBB8_574
.LBB8_554:                              # %.loopexit520
.Ltmp42:
	jmp	.LBB8_592
.LBB8_555:
.Ltmp36:
	jmp	.LBB8_592
.LBB8_556:
.Ltmp323:
	movq	%rax, %r14
.Ltmp324:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp325:
	jmp	.LBB8_631
.LBB8_557:
.Ltmp326:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_558:                              # %.loopexit.split-lp
.Ltmp257:
	jmp	.LBB8_623
.LBB8_559:
.Ltmp21:
	movq	%rax, %r14
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_631
# BB#560:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_561:
.Ltmp85:
	movq	%rax, %r14
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_615
# BB#562:
	callq	_ZdaPv
	jmp	.LBB8_615
.LBB8_563:
.Ltmp16:
	movq	%rax, %r14
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_565
# BB#564:
	callq	_ZdaPv
.LBB8_565:
	movq	288(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_583
	jmp	.LBB8_631
.LBB8_566:
.Ltmp99:
	movq	%rax, %r14
	jmp	.LBB8_578
.LBB8_567:
.Ltmp96:
	movq	%rax, %r14
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_615
# BB#568:
	callq	_ZdaPv
	jmp	.LBB8_615
.LBB8_569:
.Ltmp107:
	jmp	.LBB8_614
.LBB8_570:
.Ltmp224:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB8_607
.LBB8_571:                              # %.loopexit525
.Ltmp48:
	jmp	.LBB8_592
.LBB8_572:
.Ltmp201:
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB8_629
.LBB8_574:
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB8_629
.LBB8_575:                              # %.loopexit49.i.i
.Ltmp104:
.LBB8_576:
	movq	%rax, %r14
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_578
# BB#577:
	callq	_ZdaPv
.LBB8_578:
.Ltmp111:
	leaq	288(%rsp), %rdi
	callq	_ZN21CUpdateArchiveCommandD2Ev
.Ltmp112:
	jmp	.LBB8_615
.LBB8_579:
.Ltmp113:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_580:
.Ltmp219:
	movq	%rax, %r14
	jmp	.LBB8_609
.LBB8_581:                              # %.loopexit.split-lp93.i
.Ltmp139:
	movq	%rax, %r14
	jmp	.LBB8_617
.LBB8_582:                              # %.thread.i
.Ltmp13:
	movq	%rax, %r14
.LBB8_583:
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_584:
.Ltmp172:
	jmp	.LBB8_590
.LBB8_585:
.Ltmp269:
	jmp	.LBB8_587
.LBB8_586:
.Ltmp262:
.LBB8_587:                              # %.body345
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB8_624
.LBB8_588:
.Ltmp247:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB8_620
.LBB8_589:
.Ltmp320:
.LBB8_590:                              # %_ZN11CStringBaseIwED2Ev.exit294
	movq	%rax, %r14
	jmp	.LBB8_629
.LBB8_591:                              # %.loopexit515
.Ltmp33:
.LBB8_592:
	movq	%rax, %r14
.LBB8_593:
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_631
# BB#594:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_595:                              # %.us-lcssa8.us-lcssa.i
.Ltmp30:
	jmp	.LBB8_598
.LBB8_596:                              # %.us-lcssa8.us.i
.Ltmp27:
	jmp	.LBB8_598
.LBB8_597:                              # %.us-lcssa8.us-lcssa.us.i
.Ltmp24:
.LBB8_598:                              # %.us-lcssa8.i
	movq	%rax, %r14
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_631
# BB#599:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_600:
.Ltmp144:
	jmp	.LBB8_602
.LBB8_601:
.Ltmp147:
.LBB8_602:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB8_617
# BB#603:
	movq	%r15, %rdi
	jmp	.LBB8_616
.LBB8_604:
.Ltmp240:
.LBB8_605:
	movq	%rax, %r14
	jmp	.LBB8_625
.LBB8_606:
.Ltmp227:
	movq	%rax, %r14
.LBB8_607:                              # %.body309
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_609
# BB#608:
	callq	_ZdaPv
.LBB8_609:
.Ltmp343:
	leaq	288(%rsp), %rdi
	callq	_ZN9CDirItemsD2Ev
.Ltmp344:
	jmp	.LBB8_627
.LBB8_610:                              # %.loopexit.split-lp.loopexit.i.i
.Ltmp93:
	jmp	.LBB8_614
.LBB8_611:                              # %.loopexit88.i
.Ltmp156:
	movq	%rax, %r14
	jmp	.LBB8_617
.LBB8_612:                              # %.loopexit92.i
.Ltmp119:
	movq	%rax, %r14
	jmp	.LBB8_617
.LBB8_613:                              # %.loopexit43.i.i
.Ltmp76:
.LBB8_614:
	movq	%rax, %r14
.LBB8_615:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_617
.LBB8_616:
	callq	_ZdaPv
.LBB8_617:                              # %.body63.i
.Ltmp157:
	leaq	480(%rsp), %rdi
	callq	_ZN21CUpdateArchiveCommandD2Ev
.Ltmp158:
	jmp	.LBB8_631
.LBB8_618:
.Ltmp159:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_619:
.Ltmp250:
	movq	%rax, %r14
.LBB8_620:                              # %.body333
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_625
# BB#621:
	callq	_ZdaPv
	jmp	.LBB8_625
.LBB8_622:                              # %.loopexit
.Ltmp272:
.LBB8_623:                              # %.body345
	movq	%rax, %r14
.LBB8_624:                              # %.body345
.Ltmp273:
	movq	456(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp274:
.LBB8_625:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%rsp)
.Ltmp278:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp279:
# BB#626:
.Ltmp284:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp285:
.LBB8_627:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 480(%rsp)
.Ltmp345:
	leaq	480(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp346:
# BB#628:
.Ltmp351:
	leaq	480(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp352:
.LBB8_629:                              # %_ZN11CStringBaseIwED2Ev.exit294
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 224(%rsp)
.Ltmp353:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp354:
# BB#630:                               # %_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev.exit.i
.Ltmp359:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp360:
.LBB8_631:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_632:
.Ltmp280:
	movq	%rax, %rbx
.Ltmp281:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp282:
	jmp	.LBB8_639
.LBB8_633:
.Ltmp283:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_634:
.Ltmp347:
	movq	%rax, %rbx
.Ltmp348:
	leaq	480(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp349:
	jmp	.LBB8_639
.LBB8_635:
.Ltmp350:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_636:
.Ltmp355:
	movq	%rax, %rbx
.Ltmp356:
	leaq	224(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp357:
	jmp	.LBB8_639
.LBB8_637:
.Ltmp358:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_638:
.Ltmp361:
	movq	%rax, %rbx
.LBB8_639:                              # %.body299
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions, .Lfunc_end8-_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_444
	.quad	.LBB8_492
	.quad	.LBB8_492
	.quad	.LBB8_492
	.quad	.LBB8_492
	.quad	.LBB8_440
	.quad	.LBB8_492
	.quad	.LBB8_492
	.quad	.LBB8_492
	.quad	.LBB8_442
	.quad	.LBB8_492
	.quad	.LBB8_441
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\342\214\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\331\f"                # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp15         #   Call between .Ltmp15 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp17         #   Call between .Ltmp17 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp20         #   Call between .Ltmp20 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp28-.Ltmp23         #   Call between .Ltmp23 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp25-.Ltmp29         #   Call between .Ltmp29 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp160-.Ltmp26        #   Call between .Ltmp26 and .Ltmp160
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin1  # >> Call Site 13 <<
	.long	.Ltmp171-.Ltmp160       #   Call between .Ltmp160 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin1  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin1  # >> Call Site 14 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin1  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin1  # >> Call Site 15 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin1  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin1  # >> Call Site 16 <<
	.long	.Ltmp31-.Ltmp177        #   Call between .Ltmp177 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 19 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 20 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 21 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 22 <<
	.long	.Ltmp59-.Ltmp38         #   Call between .Ltmp38 and .Ltmp59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 23 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin1   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin1   # >> Call Site 24 <<
	.long	.Ltmp67-.Ltmp62         #   Call between .Ltmp62 and .Ltmp67
	.long	.Ltmp139-.Lfunc_begin1  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin1   # >> Call Site 25 <<
	.long	.Ltmp71-.Ltmp68         #   Call between .Ltmp68 and .Ltmp71
	.long	.Ltmp119-.Lfunc_begin1  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin1   # >> Call Site 26 <<
	.long	.Ltmp75-.Ltmp72         #   Call between .Ltmp72 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin1   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin1   # >> Call Site 27 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp93-.Lfunc_begin1   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin1   # >> Call Site 28 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin1   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin1  # >> Call Site 29 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin1  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin1  # >> Call Site 30 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin1  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin1   # >> Call Site 31 <<
	.long	.Ltmp92-.Ltmp86         #   Call between .Ltmp86 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin1   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin1   # >> Call Site 32 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin1   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin1   # >> Call Site 33 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin1   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin1  # >> Call Site 34 <<
	.long	.Ltmp103-.Ltmp100       #   Call between .Ltmp100 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin1  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin1  # >> Call Site 35 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin1  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin1  # >> Call Site 36 <<
	.long	.Ltmp123-.Ltmp120       #   Call between .Ltmp120 and .Ltmp123
	.long	.Ltmp139-.Lfunc_begin1  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin1  # >> Call Site 37 <<
	.long	.Ltmp127-.Ltmp124       #   Call between .Ltmp124 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin1  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin1  # >> Call Site 38 <<
	.long	.Ltmp186-.Ltmp179       #   Call between .Ltmp179 and .Ltmp186
	.long	.Ltmp320-.Lfunc_begin1  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin1  # >> Call Site 39 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin1  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin1  # >> Call Site 40 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin1  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin1  # >> Call Site 41 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin1  #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin1  # >> Call Site 42 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin1  #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin1  # >> Call Site 43 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin1  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin1  # >> Call Site 44 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp227-.Lfunc_begin1  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp222-.Lfunc_begin1  # >> Call Site 45 <<
	.long	.Ltmp223-.Ltmp222       #   Call between .Ltmp222 and .Ltmp223
	.long	.Ltmp224-.Lfunc_begin1  #     jumps to .Ltmp224
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin1  # >> Call Site 46 <<
	.long	.Ltmp226-.Ltmp225       #   Call between .Ltmp225 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin1  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin1  # >> Call Site 47 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin1  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin1  # >> Call Site 48 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp329-.Lfunc_begin1  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin1  # >> Call Site 49 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin1  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin1  # >> Call Site 50 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp201-.Lfunc_begin1  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin1  # >> Call Site 51 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin1  #     jumps to .Ltmp191
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin1  # >> Call Site 52 <<
	.long	.Ltmp195-.Ltmp192       #   Call between .Ltmp192 and .Ltmp195
	.long	.Ltmp201-.Lfunc_begin1  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin1  # >> Call Site 53 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin1  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin1  # >> Call Site 54 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin1  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin1  # >> Call Site 55 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp329-.Lfunc_begin1  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin1  # >> Call Site 56 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	.Ltmp240-.Lfunc_begin1  #     jumps to .Ltmp240
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin1  # >> Call Site 57 <<
	.long	.Ltmp244-.Ltmp241       #   Call between .Ltmp241 and .Ltmp244
	.long	.Ltmp250-.Lfunc_begin1  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin1  # >> Call Site 58 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin1  #     jumps to .Ltmp247
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin1  # >> Call Site 59 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin1  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin1  # >> Call Site 60 <<
	.long	.Ltmp256-.Ltmp251       #   Call between .Ltmp251 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin1  #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin1  # >> Call Site 61 <<
	.long	.Ltmp259-.Ltmp258       #   Call between .Ltmp258 and .Ltmp259
	.long	.Ltmp272-.Lfunc_begin1  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin1  # >> Call Site 62 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin1  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin1  # >> Call Site 63 <<
	.long	.Ltmp266-.Ltmp263       #   Call between .Ltmp263 and .Ltmp266
	.long	.Ltmp272-.Lfunc_begin1  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin1  # >> Call Site 64 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin1  #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin1  # >> Call Site 65 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin1  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin1  # >> Call Site 66 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin1  #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin1  # >> Call Site 67 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin1  #     jumps to .Ltmp288
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin1  # >> Call Site 68 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin1  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp295-.Lfunc_begin1  # >> Call Site 69 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin1  #     jumps to .Ltmp297
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin1  # >> Call Site 70 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin1  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin1  # >> Call Site 71 <<
	.long	.Ltmp319-.Ltmp304       #   Call between .Ltmp304 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin1  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin1  # >> Call Site 72 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin1  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin1  # >> Call Site 73 <<
	.long	.Ltmp129-.Ltmp322       #   Call between .Ltmp322 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin1  # >> Call Site 74 <<
	.long	.Ltmp138-.Ltmp129       #   Call between .Ltmp129 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin1  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin1  # >> Call Site 75 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp156-.Lfunc_begin1  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin1  # >> Call Site 76 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin1  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin1  # >> Call Site 77 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin1  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin1  # >> Call Site 78 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin1  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin1  # >> Call Site 79 <<
	.long	.Ltmp79-.Ltmp155        #   Call between .Ltmp155 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin1   # >> Call Site 80 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp90-.Lfunc_begin1   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin1   # >> Call Site 81 <<
	.long	.Ltmp77-.Ltmp80         #   Call between .Ltmp80 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin1   # >> Call Site 82 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp90-.Lfunc_begin1   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 83 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp58-.Lfunc_begin1   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 84 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 85 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin1   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 86 <<
	.long	.Ltmp148-.Ltmp57        #   Call between .Ltmp57 and .Ltmp148
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin1  # >> Call Site 87 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin1  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin1  # >> Call Site 88 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin1  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 89 <<
	.long	.Ltmp50-.Ltmp51         #   Call between .Ltmp51 and .Ltmp50
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin1   # >> Call Site 90 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin1   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin1  # >> Call Site 91 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin1  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin1  # >> Call Site 92 <<
	.long	.Ltmp43-.Ltmp109        #   Call between .Ltmp109 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 93 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 94 <<
	.long	.Ltmp330-.Ltmp44        #   Call between .Ltmp44 and .Ltmp330
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin1  # >> Call Site 95 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin1  #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp331-.Lfunc_begin1  # >> Call Site 96 <<
	.long	.Ltmp327-.Ltmp331       #   Call between .Ltmp331 and .Ltmp327
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin1  # >> Call Site 97 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin1  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin1  # >> Call Site 98 <<
	.long	.Ltmp306-.Ltmp328       #   Call between .Ltmp328 and .Ltmp306
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp306-.Lfunc_begin1  # >> Call Site 99 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp320-.Lfunc_begin1  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin1  # >> Call Site 100 <<
	.long	.Ltmp298-.Ltmp307       #   Call between .Ltmp307 and .Ltmp298
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin1  # >> Call Site 101 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin1  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp289-.Lfunc_begin1  # >> Call Site 102 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin1  #     jumps to .Ltmp291
	.byte	1                       #   On action: 1
	.long	.Ltmp211-.Lfunc_begin1  # >> Call Site 103 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin1  #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp333-.Lfunc_begin1  # >> Call Site 104 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp335-.Lfunc_begin1  # >> Call Site 105 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin1  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp341-.Lfunc_begin1  # >> Call Site 106 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp338-.Lfunc_begin1  # >> Call Site 107 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin1  #     jumps to .Ltmp340
	.byte	1                       #   On action: 1
	.long	.Ltmp339-.Lfunc_begin1  # >> Call Site 108 <<
	.long	.Ltmp324-.Ltmp339       #   Call between .Ltmp339 and .Ltmp324
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin1  # >> Call Site 109 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin1  #     jumps to .Ltmp326
	.byte	1                       #   On action: 1
	.long	.Ltmp325-.Lfunc_begin1  # >> Call Site 110 <<
	.long	.Ltmp111-.Ltmp325       #   Call between .Ltmp325 and .Ltmp111
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin1  # >> Call Site 111 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin1  #     jumps to .Ltmp113
	.byte	1                       #   On action: 1
	.long	.Ltmp112-.Lfunc_begin1  # >> Call Site 112 <<
	.long	.Ltmp343-.Ltmp112       #   Call between .Ltmp112 and .Ltmp343
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp343-.Lfunc_begin1  # >> Call Site 113 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp157-.Lfunc_begin1  # >> Call Site 114 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin1  #     jumps to .Ltmp159
	.byte	1                       #   On action: 1
	.long	.Ltmp273-.Lfunc_begin1  # >> Call Site 115 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp278-.Lfunc_begin1  # >> Call Site 116 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin1  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp284-.Lfunc_begin1  # >> Call Site 117 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp345-.Lfunc_begin1  # >> Call Site 118 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin1  #     jumps to .Ltmp347
	.byte	1                       #   On action: 1
	.long	.Ltmp351-.Lfunc_begin1  # >> Call Site 119 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp353-.Lfunc_begin1  # >> Call Site 120 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin1  #     jumps to .Ltmp355
	.byte	1                       #   On action: 1
	.long	.Ltmp359-.Lfunc_begin1  # >> Call Site 121 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin1  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp360-.Lfunc_begin1  # >> Call Site 122 <<
	.long	.Ltmp281-.Ltmp360       #   Call between .Ltmp360 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin1  # >> Call Site 123 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin1  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp348-.Lfunc_begin1  # >> Call Site 124 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin1  #     jumps to .Ltmp350
	.byte	1                       #   On action: 1
	.long	.Ltmp356-.Lfunc_begin1  # >> Call Site 125 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin1  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj,@function
_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj: # @_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%edx, (%rsp)            # 4-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r13
	cmpl	$0, 12(%rbx)
	jle	.LBB9_31
# BB#1:                                 # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_6 Depth 2
	movq	16(%rbx), %rax
	movq	(%rax,%r14,8), %rbp
	cmpl	$1, 8(%rbp)
	jle	.LBB9_32
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	(%rbp), %rax
	movl	(%rax), %edi
	callq	_Z11MyCharUpperw
	xorl	%r15d, %r15d
	cmpl	$82, %eax
	movl	4(%rsp), %r12d          # 4-byte Reload
	jne	.LBB9_11
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	$12, %edi
	callq	_Znam
	movabsq	$193273528368, %rcx     # imm = 0x2D00000030
	movq	%rcx, (%rax)
	movl	$0, 8(%rax)
	movq	(%rbp), %rcx
	movl	4(%rcx), %ecx
	cmpl	$48, %ecx
	movq	%rax, %r15
	je	.LBB9_9
# BB#5:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	$48, %edx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB9_6:                                # %.lr.ph.i.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %edx
	je	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_6 Depth=2
	movl	4(%r15), %edx
	addq	$4, %r15
	cmpl	%ecx, %edx
	jne	.LBB9_6
.LBB9_9:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB9_2 Depth=1
	subq	%rax, %r15
	shrq	$2, %r15
	jmp	.LBB9_10
.LBB9_7:                                #   in Loop: Header=BB9_2 Depth=1
	movl	$-1, %r15d
.LBB9_10:                               # %_ZN11CStringBaseIwED2Ev.exit4
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, %rdi
	callq	_ZdaPv
	xorl	%r12d, %r12d
	cmpl	$1, %r15d
	sete	%r12b
	addl	%r12d, %r12d
	testl	%r15d, %r15d
	movl	$1, %eax
	cmovel	%eax, %r12d
	sarl	$31, %r15d
	addl	$2, %r15d
.LBB9_11:                               #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rbp), %ecx
	leal	2(%r15), %eax
	cmpl	%eax, %ecx
	jl	.LBB9_32
# BB#12:                                #   in Loop: Header=BB9_2 Depth=1
	leal	1(%r15), %edx
	subl	%edx, %ecx
	leaq	8(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	(%rbp), %rax
	movl	%r15d, %ecx
	movl	(%rax,%rcx,4), %eax
	cmpl	$64, %eax
	je	.LBB9_27
# BB#13:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	$33, %eax
	jne	.LBB9_25
# BB#14:                                #   in Loop: Header=BB9_2 Depth=1
	testl	%r12d, %r12d
	je	.LBB9_18
# BB#15:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	$1, %r12d
	jne	.LBB9_16
# BB#17:                                #   in Loop: Header=BB9_2 Depth=1
.Ltmp364:
	leaq	8(%rsp), %rdi
	callq	_Z23DoesNameContainWildCardRK11CStringBaseIwE
                                        # kill: %AL<def> %AL<kill> %EAX<def>
.Ltmp365:
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_27:                               #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rsp), %rsi
.Ltmp362:
	movzbl	(%rsp), %edx            # 1-byte Folded Reload
	movq	%r13, %rdi
	movl	%r12d, %ecx
	callq	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
.Ltmp363:
	jmp	.LBB9_28
.LBB9_18:                               #   in Loop: Header=BB9_2 Depth=1
	movb	$1, %al
	jmp	.LBB9_19
.LBB9_16:                               #   in Loop: Header=BB9_2 Depth=1
	xorl	%eax, %eax
.LBB9_19:                               # %.noexc2
                                        #   in Loop: Header=BB9_2 Depth=1
.Ltmp366:
	movzbl	(%rsp), %esi            # 1-byte Folded Reload
	movzbl	%al, %ecx
	movq	%r13, %rdi
	leaq	8(%rsp), %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp367:
.LBB9_28:                               # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_30
# BB#29:                                #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdaPv
.LBB9_30:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%r14
	movslq	12(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB9_2
.LBB9_31:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_32:
	callq	_ZL23ThrowUserErrorExceptionv
.LBB9_25:
.Ltmp369:
	movl	$.L.str.41, %edi
	callq	_ZL14ThrowExceptionPKc
.Ltmp370:
# BB#26:                                # %.noexc
.LBB9_21:                               # %.loopexit.split-lp
.Ltmp371:
	jmp	.LBB9_22
.LBB9_20:                               # %.loopexit
.Ltmp368:
.LBB9_22:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_24
# BB#23:
	callq	_ZdaPv
.LBB9_24:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj, .Lfunc_end9-_ZL26AddSwitchWildCardsToCensorRN9NWildcard7CCensorERK13CObjectVectorI11CStringBaseIwEEbN13NRecursedType5EEnumEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp364-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp364
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp364-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp367-.Ltmp364       #   Call between .Ltmp364 and .Ltmp367
	.long	.Ltmp368-.Lfunc_begin2  #     jumps to .Ltmp368
	.byte	0                       #   On action: cleanup
	.long	.Ltmp367-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp369-.Ltmp367       #   Call between .Ltmp367 and .Ltmp369
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin2  #     jumps to .Ltmp371
	.byte	0                       #   On action: cleanup
	.long	.Ltmp370-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Lfunc_end9-.Ltmp370    #   Call between .Ltmp370 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL14ThrowExceptionPKc,@function
_ZL14ThrowExceptionPKc:                 # @_ZL14ThrowExceptionPKc
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$16, %edi
	callq	__cxa_allocate_exception
	movq	%rax, %rbx
.Ltmp372:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN28CArchiveCommandLineExceptionC2EPKc
.Ltmp373:
# BB#1:
	movl	$_ZTI28CArchiveCommandLineException, %esi
	movl	$_ZN11CStringBaseIcED2Ev, %edx
	movq	%rbx, %rdi
	callq	__cxa_throw
.LBB10_2:
.Ltmp374:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	__cxa_free_exception
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZL14ThrowExceptionPKc, .Lfunc_end10-_ZL14ThrowExceptionPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp372-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp372
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp373-.Ltmp372       #   Call between .Ltmp372 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin3  #     jumps to .Ltmp374
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Lfunc_end10-.Ltmp373   #   Call between .Ltmp373 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp375:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp376:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB11_2:
.Ltmp377:
	movq	%rax, %r14
.Ltmp378:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp379:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_4:
.Ltmp380:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end11-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp375-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin4  #     jumps to .Ltmp377
	.byte	0                       #   On action: cleanup
	.long	.Ltmp376-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp378-.Ltmp376       #   Call between .Ltmp376 and .Ltmp378
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp378-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp379-.Ltmp378       #   Call between .Ltmp378 and .Ltmp379
	.long	.Ltmp380-.Lfunc_begin4  #     jumps to .Ltmp380
	.byte	1                       #   On action: 1
	.long	.Ltmp379-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp379   #   Call between .Ltmp379 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9CDirItemsD2Ev,"axG",@progbits,_ZN9CDirItemsD2Ev,comdat
	.weak	_ZN9CDirItemsD2Ev
	.p2align	4, 0x90
	.type	_ZN9CDirItemsD2Ev,@function
_ZN9CDirItemsD2Ev:                      # @_ZN9CDirItemsD2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, 96(%r15)
.Ltmp381:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp382:
# BB#1:
.Ltmp387:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp388:
# BB#2:                                 # %_ZN13CObjectVectorI8CDirItemED2Ev.exit
	leaq	64(%r15), %rdi
.Ltmp392:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp393:
# BB#3:
	leaq	32(%r15), %rdi
.Ltmp397:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp398:
# BB#4:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp409:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp410:
# BB#5:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_8:
.Ltmp411:
	movq	%rax, %r14
.Ltmp412:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp413:
	jmp	.LBB12_9
.LBB12_10:
.Ltmp414:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_13:
.Ltmp399:
	movq	%rax, %r14
	jmp	.LBB12_16
.LBB12_14:
.Ltmp394:
	movq	%rax, %r14
	jmp	.LBB12_15
.LBB12_11:
.Ltmp389:
	movq	%rax, %r14
	jmp	.LBB12_12
.LBB12_6:
.Ltmp383:
	movq	%rax, %r14
.Ltmp384:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp385:
.LBB12_12:                              # %.body
	leaq	64(%r15), %rdi
.Ltmp390:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp391:
.LBB12_15:
	leaq	32(%r15), %rdi
.Ltmp395:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp396:
.LBB12_16:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp400:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp401:
# BB#17:
.Ltmp406:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp407:
.LBB12_9:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_7:
.Ltmp386:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_18:
.Ltmp402:
	movq	%rax, %rbx
.Ltmp403:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp404:
	jmp	.LBB12_21
.LBB12_19:
.Ltmp405:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_20:
.Ltmp408:
	movq	%rax, %rbx
.LBB12_21:                              # %.body4
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN9CDirItemsD2Ev, .Lfunc_end12-_ZN9CDirItemsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp381-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin5  #     jumps to .Ltmp383
	.byte	0                       #   On action: cleanup
	.long	.Ltmp387-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp388-.Ltmp387       #   Call between .Ltmp387 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin5  #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp392-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin5  #     jumps to .Ltmp394
	.byte	0                       #   On action: cleanup
	.long	.Ltmp397-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp398-.Ltmp397       #   Call between .Ltmp397 and .Ltmp398
	.long	.Ltmp399-.Lfunc_begin5  #     jumps to .Ltmp399
	.byte	0                       #   On action: cleanup
	.long	.Ltmp409-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp410-.Ltmp409       #   Call between .Ltmp409 and .Ltmp410
	.long	.Ltmp411-.Lfunc_begin5  #     jumps to .Ltmp411
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp412-.Ltmp410       #   Call between .Ltmp410 and .Ltmp412
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp412-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp413-.Ltmp412       #   Call between .Ltmp412 and .Ltmp413
	.long	.Ltmp414-.Lfunc_begin5  #     jumps to .Ltmp414
	.byte	1                       #   On action: 1
	.long	.Ltmp384-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp385-.Ltmp384       #   Call between .Ltmp384 and .Ltmp385
	.long	.Ltmp386-.Lfunc_begin5  #     jumps to .Ltmp386
	.byte	1                       #   On action: 1
	.long	.Ltmp390-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp396-.Ltmp390       #   Call between .Ltmp390 and .Ltmp396
	.long	.Ltmp408-.Lfunc_begin5  #     jumps to .Ltmp408
	.byte	1                       #   On action: 1
	.long	.Ltmp400-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp401-.Ltmp400       #   Call between .Ltmp400 and .Ltmp401
	.long	.Ltmp402-.Lfunc_begin5  #     jumps to .Ltmp402
	.byte	1                       #   On action: 1
	.long	.Ltmp406-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp407-.Ltmp406       #   Call between .Ltmp406 and .Ltmp407
	.long	.Ltmp408-.Lfunc_begin5  #     jumps to .Ltmp408
	.byte	1                       #   On action: 1
	.long	.Ltmp407-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp403-.Ltmp407       #   Call between .Ltmp407 and .Ltmp403
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp403-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp404-.Ltmp403       #   Call between .Ltmp403 and .Ltmp404
	.long	.Ltmp405-.Lfunc_begin5  #     jumps to .Ltmp405
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE,@function
_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE: # @_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 128
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	$8, %esi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpb	$0, (%rax)
	je	.LBB13_70
# BB#1:                                 # %.preheader
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	cmpl	$0, 20(%rax)
	jle	.LBB13_70
# BB#2:                                 # %.lr.ph
	xorl	%r14d, %r14d
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_7 Depth 2
                                        #     Child Loop BB13_33 Depth 2
                                        #     Child Loop BB13_45 Depth 2
                                        #     Child Loop BB13_18 Depth 2
                                        #     Child Loop BB13_57 Depth 2
                                        #     Child Loop BB13_63 Depth 2
	movq	$0, 8(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	leaq	16(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp415:
	movl	$16, %edi
	callq	_Znam
.Ltmp416:
# BB#4:                                 # %_ZN9CPropertyC2Ev.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
.Ltmp418:
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
.Ltmp419:
# BB#5:                                 #   in Loop: Header=BB13_3 Depth=1
	movq	24(%rax), %rax
	movq	(%rax,%r14,8), %rbp
	movq	(%rbp), %rax
	movl	(%rax), %ecx
	cmpl	$61, %ecx
	movq	%rax, %rbx
	movq	%r14, 64(%rsp)          # 8-byte Spill
	je	.LBB13_9
# BB#6:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph.i.i
                                        #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ecx, %ecx
	je	.LBB13_10
# BB#8:                                 #   in Loop: Header=BB13_7 Depth=2
	movl	4(%rbx), %ecx
	addq	$4, %rbx
	cmpl	$61, %ecx
	jne	.LBB13_7
.LBB13_9:                               # %_ZNK11CStringBaseIwE4FindEw.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	subq	%rax, %rbx
	shrq	$2, %rbx
	testl	%ebx, %ebx
	js	.LBB13_10
# BB#24:                                #   in Loop: Header=BB13_3 Depth=1
.Ltmp421:
	xorl	%edx, %edx
	leaq	32(%rsp), %rdi
	movq	%rbp, %r13
	movq	%rbp, %rsi
	movl	%ebx, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp422:
# BB#25:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	40(%rsp), %r15
	incq	%r15
	movl	12(%rsp), %eax
	cmpl	%eax, %r15d
	je	.LBB13_32
# BB#26:                                #   in Loop: Header=BB13_3 Depth=1
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp424:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp425:
# BB#27:                                # %.noexc39
                                        #   in Loop: Header=BB13_3 Depth=1
	testq	%rbp, %rbp
	movl	52(%rsp), %eax          # 4-byte Reload
	je	.LBB13_28
# BB#29:                                # %.noexc39
                                        #   in Loop: Header=BB13_3 Depth=1
	testl	%eax, %eax
	movl	$0, %eax
	jle	.LBB13_31
# BB#30:                                # %._crit_edge.thread.i.i33
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB13_31
	.p2align	4, 0x90
.LBB13_10:                              # %_ZNK11CStringBaseIwE4FindEw.exit.thread
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rsp, %rcx
	cmpq	%rcx, %rbp
	je	.LBB13_20
# BB#11:                                #   in Loop: Header=BB13_3 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	8(%rbp), %r13
	incq	%r13
	movl	12(%rsp), %r14d
	cmpl	%r14d, %r13d
	je	.LBB13_18
# BB#12:                                #   in Loop: Header=BB13_3 Depth=1
	movq	%r13, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp433:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp434:
# BB#13:                                # %.noexc
                                        #   in Loop: Header=BB13_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB13_14
# BB#15:                                # %.noexc
                                        #   in Loop: Header=BB13_3 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB13_17
# BB#16:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB13_17
.LBB13_28:                              #   in Loop: Header=BB13_3 Depth=1
	xorl	%eax, %eax
.LBB13_31:                              # %._crit_edge16.i.i34
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%r14, (%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%r15d, 12(%rsp)
	movq	%r14, %rbp
.LBB13_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i35
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_33:                              #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_33
# BB#34:                                #   in Loop: Header=BB13_3 Depth=1
	movl	40(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB13_36
# BB#35:                                #   in Loop: Header=BB13_3 Depth=1
	callq	_ZdaPv
.LBB13_36:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	incl	%ebx
	movl	8(%r13), %ecx
	subl	%ebx, %ecx
.Ltmp427:
	leaq	32(%rsp), %rdi
	movq	%r13, %rsi
	movl	%ebx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp428:
# BB#37:                                # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	40(%rsp), %rbp
	incq	%rbp
	movl	28(%rsp), %r15d
	cmpl	%r15d, %ebp
	je	.LBB13_44
# BB#38:                                #   in Loop: Header=BB13_3 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp430:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp431:
# BB#39:                                # %.noexc51
                                        #   in Loop: Header=BB13_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB13_40
# BB#41:                                # %.noexc51
                                        #   in Loop: Header=BB13_3 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB13_43
# BB#42:                                # %._crit_edge.thread.i.i45
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB13_43
.LBB13_40:                              #   in Loop: Header=BB13_3 Depth=1
	xorl	%eax, %eax
.LBB13_43:                              # %._crit_edge16.i.i46
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%r14, 16(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 28(%rsp)
	movq	%r14, %rbx
.LBB13_44:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i47
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_45:                              #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_45
# BB#46:                                #   in Loop: Header=BB13_3 Depth=1
	movl	40(%rsp), %eax
	movl	%eax, 24(%rsp)
	testq	%rdi, %rdi
	je	.LBB13_20
# BB#47:                                #   in Loop: Header=BB13_3 Depth=1
	callq	_ZdaPv
	jmp	.LBB13_20
.LBB13_14:                              #   in Loop: Header=BB13_3 Depth=1
	xorl	%eax, %eax
.LBB13_17:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%r15, (%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 12(%rsp)
	movq	(%rbp), %rax
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB13_18:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB13_18
# BB#19:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	8(%rbp), %eax
	movl	%eax, 8(%rsp)
.LBB13_20:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB13_3 Depth=1
.Ltmp435:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp436:
# BB#21:                                # %.noexc56
                                        #   in Loop: Header=BB13_3 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB13_22
# BB#54:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp437:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp438:
# BB#55:                                # %.noexc62
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%r13, (%r15)
	movl	$0, (%r13)
	movl	%ebp, 12(%r15)
	movq	%r13, %rbx
	jmp	.LBB13_56
	.p2align	4, 0x90
.LBB13_22:                              #   in Loop: Header=BB13_3 Depth=1
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.LBB13_56:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_57:                              #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB13_57
# BB#58:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	%r14d, 8(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r15)
	movslq	24(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB13_59
# BB#60:                                # %._crit_edge16.i.i4.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp440:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp441:
# BB#61:                                # %.noexc.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%rcx, 16(%r15)
	movl	$0, (%rcx)
	movl	%ebp, 28(%r15)
	jmp	.LBB13_62
	.p2align	4, 0x90
.LBB13_59:                              #   in Loop: Header=BB13_3 Depth=1
	xorl	%ecx, %ecx
.LBB13_62:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i5.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	16(%rsp), %rsi
	.p2align	4, 0x90
.LBB13_63:                              #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB13_63
# BB#64:                                #   in Loop: Header=BB13_3 Depth=1
	movl	%r14d, 24(%r15)
.Ltmp443:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp444:
# BB#65:                                #   in Loop: Header=BB13_3 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_67
# BB#66:                                #   in Loop: Header=BB13_3 Depth=1
	callq	_ZdaPv
.LBB13_67:                              # %_ZN11CStringBaseIwED2Ev.exit.i58
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	je	.LBB13_69
# BB#68:                                #   in Loop: Header=BB13_3 Depth=1
	callq	_ZdaPv
.LBB13_69:                              # %_ZN9CPropertyD2Ev.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	incq	%r14
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	_ZNK18NCommandLineParser7CParserixEm
	movslq	20(%rax), %rax
	cmpq	%rax, %r14
	jl	.LBB13_3
.LBB13_70:                              # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_52:
.Ltmp432:
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_77
# BB#53:
	callq	_ZdaPv
	jmp	.LBB13_77
.LBB13_49:
.Ltmp426:
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_77
# BB#50:
	callq	_ZdaPv
	jmp	.LBB13_77
.LBB13_51:
.Ltmp429:
	jmp	.LBB13_76
.LBB13_48:
.Ltmp423:
	jmp	.LBB13_76
.LBB13_71:
.Ltmp442:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB13_74
# BB#72:
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB13_74
.LBB13_73:
.Ltmp439:
	movq	%rax, %r14
.LBB13_74:                              # %.body63
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB13_77
.LBB13_75:
.Ltmp420:
	jmp	.LBB13_76
.LBB13_80:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp417:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB13_81
.LBB13_23:
.Ltmp445:
.LBB13_76:
	movq	%rax, %r14
.LBB13_77:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_79
# BB#78:
	callq	_ZdaPv
.LBB13_79:                              # %_ZN11CStringBaseIwED2Ev.exit.i59
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_82
.LBB13_81:                              # %unwind_resume
	callq	_ZdaPv
.LBB13_82:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE, .Lfunc_end13-_ZL16SetMethodOptionsRKN18NCommandLineParser7CParserER13CObjectVectorI9CPropertyE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\254\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp415-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp415
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp415-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp416-.Ltmp415       #   Call between .Ltmp415 and .Ltmp416
	.long	.Ltmp417-.Lfunc_begin6  #     jumps to .Ltmp417
	.byte	0                       #   On action: cleanup
	.long	.Ltmp418-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp419-.Ltmp418       #   Call between .Ltmp418 and .Ltmp419
	.long	.Ltmp420-.Lfunc_begin6  #     jumps to .Ltmp420
	.byte	0                       #   On action: cleanup
	.long	.Ltmp421-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp422-.Ltmp421       #   Call between .Ltmp421 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin6  #     jumps to .Ltmp423
	.byte	0                       #   On action: cleanup
	.long	.Ltmp424-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin6  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp433-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp434-.Ltmp433       #   Call between .Ltmp433 and .Ltmp434
	.long	.Ltmp445-.Lfunc_begin6  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin6  #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin6  #     jumps to .Ltmp432
	.byte	0                       #   On action: cleanup
	.long	.Ltmp435-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp436-.Ltmp435       #   Call between .Ltmp435 and .Ltmp436
	.long	.Ltmp445-.Lfunc_begin6  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp437-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin6  #     jumps to .Ltmp439
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin6  #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp443-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp444-.Ltmp443       #   Call between .Ltmp443 and .Ltmp444
	.long	.Ltmp445-.Lfunc_begin6  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp444-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Lfunc_end13-.Ltmp444   #   Call between .Ltmp444 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj,@function
_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj: # @_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 96
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsp)
	movq	$8, 32(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp446:
	movq	%rbp, %rdi
	callq	_ZN8NWindows5NFile5NFind13DoesFileExistEPKw
.Ltmp447:
# BB#1:
	testb	%al, %al
	je	.LBB14_17
# BB#2:
.Ltmp450:
	leaq	8(%rsp), %r14
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj
.Ltmp451:
# BB#3:
	testb	%al, %al
	je	.LBB14_18
# BB#4:                                 # %.preheader
	cmpl	$0, 20(%rsp)
	jle	.LBB14_15
# BB#5:                                 # %.lr.ph
	xorl	%ebx, %ebx
	cmpl	$1, %r12d
	je	.LBB14_11
# BB#6:                                 # %.lr.ph
	movzbl	%r15b, %ebp
	testl	%r12d, %r12d
	jne	.LBB14_9
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph.split.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax
	movq	(%rax,%rbx,8), %rdx
.Ltmp455:
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp456:
# BB#8:                                 # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit.us10
                                        #   in Loop: Header=BB14_7 Depth=1
	incq	%rbx
	movslq	20(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB14_7
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_9:                               # %.lr.ph.split.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax
	movq	(%rax,%rbx,8), %rdx
.Ltmp463:
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp464:
# BB#10:                                # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit
                                        #   in Loop: Header=BB14_9 Depth=1
	incq	%rbx
	movslq	20(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB14_9
	jmp	.LBB14_15
.LBB14_11:                              # %.lr.ph.split.us.preheader
	movzbl	%r15b, %r15d
	.p2align	4, 0x90
.LBB14_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax
	movq	(%rax,%rbx,8), %rbp
.Ltmp458:
	movq	%rbp, %rdi
	callq	_Z23DoesNameContainWildCardRK11CStringBaseIwE
.Ltmp459:
# BB#13:                                # %.noexc.us
                                        #   in Loop: Header=BB14_12 Depth=1
.Ltmp460:
	movzbl	%al, %ecx
	movq	%r13, %rdi
	movl	%r15d, %esi
	movq	%rbp, %rdx
	callq	_ZN9NWildcard7CCensor7AddItemEbRK11CStringBaseIwEb
.Ltmp461:
# BB#14:                                # %_ZL15AddNameToCensorRN9NWildcard7CCensorERK11CStringBaseIwEbN13NRecursedType5EEnumE.exit.us
                                        #   in Loop: Header=BB14_12 Depth=1
	incq	%rbx
	movslq	20(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB14_12
.LBB14_15:                              # %._crit_edge
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp475:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp476:
# BB#16:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit2
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_17:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.46, (%rax)
.Ltmp448:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp449:
	jmp	.LBB14_19
.LBB14_18:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.47, (%rax)
.Ltmp452:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp453:
.LBB14_19:
.LBB14_20:
.Ltmp477:
	movq	%rax, %rbx
.Ltmp478:
	leaq	8(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp479:
	jmp	.LBB14_29
.LBB14_21:
.Ltmp480:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_22:
.Ltmp454:
	movq	%rax, %rbx
	leaq	8(%rsp), %r14
	jmp	.LBB14_27
.LBB14_23:                              # %.us-lcssa.us-lcssa
.Ltmp465:
	jmp	.LBB14_26
.LBB14_24:                              # %.us-lcssa.us-lcssa.us
.Ltmp457:
	jmp	.LBB14_26
.LBB14_25:                              # %.us-lcssa.us
.Ltmp462:
.LBB14_26:                              # %.us-lcssa
	movq	%rax, %rbx
.LBB14_27:                              # %.us-lcssa
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 8(%rsp)
.Ltmp466:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp467:
# BB#28:
.Ltmp472:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp473:
.LBB14_29:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_30:
.Ltmp474:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB14_31:
.Ltmp468:
	movq	%rax, %rbx
.Ltmp469:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp470:
# BB#32:                                # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB14_33:
.Ltmp471:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj, .Lfunc_end14-_ZL23AddToCensorFromListFileRN9NWildcard7CCensorEPKwbN13NRecursedType5EEnumEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp446-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp451-.Ltmp446       #   Call between .Ltmp446 and .Ltmp451
	.long	.Ltmp454-.Lfunc_begin7  #     jumps to .Ltmp454
	.byte	0                       #   On action: cleanup
	.long	.Ltmp455-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin7  #     jumps to .Ltmp457
	.byte	0                       #   On action: cleanup
	.long	.Ltmp463-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp464-.Ltmp463       #   Call between .Ltmp463 and .Ltmp464
	.long	.Ltmp465-.Lfunc_begin7  #     jumps to .Ltmp465
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp461-.Ltmp458       #   Call between .Ltmp458 and .Ltmp461
	.long	.Ltmp462-.Lfunc_begin7  #     jumps to .Ltmp462
	.byte	0                       #   On action: cleanup
	.long	.Ltmp475-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin7  #     jumps to .Ltmp477
	.byte	0                       #   On action: cleanup
	.long	.Ltmp476-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp448-.Ltmp476       #   Call between .Ltmp476 and .Ltmp448
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp448-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp449-.Ltmp448       #   Call between .Ltmp448 and .Ltmp449
	.long	.Ltmp454-.Lfunc_begin7  #     jumps to .Ltmp454
	.byte	0                       #   On action: cleanup
	.long	.Ltmp449-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp452-.Ltmp449       #   Call between .Ltmp449 and .Ltmp452
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp452-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp453-.Ltmp452       #   Call between .Ltmp452 and .Ltmp453
	.long	.Ltmp454-.Lfunc_begin7  #     jumps to .Ltmp454
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin7  #     jumps to .Ltmp480
	.byte	1                       #   On action: 1
	.long	.Ltmp466-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp467-.Ltmp466       #   Call between .Ltmp466 and .Ltmp467
	.long	.Ltmp468-.Lfunc_begin7  #     jumps to .Ltmp468
	.byte	1                       #   On action: 1
	.long	.Ltmp472-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp473-.Ltmp472       #   Call between .Ltmp472 and .Ltmp473
	.long	.Ltmp474-.Lfunc_begin7  #     jumps to .Ltmp474
	.byte	1                       #   On action: 1
	.long	.Ltmp473-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp469-.Ltmp473       #   Call between .Ltmp473 and .Ltmp469
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin7  #     jumps to .Ltmp471
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN28CArchiveCommandLineExceptionC2EPKc,"axG",@progbits,_ZN28CArchiveCommandLineExceptionC2EPKc,comdat
	.weak	_ZN28CArchiveCommandLineExceptionC2EPKc
	.p2align	4, 0x90
	.type	_ZN28CArchiveCommandLineExceptionC2EPKc,@function
_ZN28CArchiveCommandLineExceptionC2EPKc: # @_ZN28CArchiveCommandLineExceptionC2EPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 48
.Lcfi80:
	.cfi_offset %rbx, -40
.Lcfi81:
	.cfi_offset %r12, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB15_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%r15), %eax
	movslq	%eax, %r12
	cmpl	$-1, %r15d
	movq	$-1, %rdi
	cmovgeq	%r12, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB15_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB15_3
# BB#4:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%r15d, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	_ZN28CArchiveCommandLineExceptionC2EPKc, .Lfunc_end15-_ZN28CArchiveCommandLineExceptionC2EPKc
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcED2Ev,"axG",@progbits,_ZN11CStringBaseIcED2Ev,comdat
	.weak	_ZN11CStringBaseIcED2Ev
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcED2Ev,@function
_ZN11CStringBaseIcED2Ev:                # @_ZN11CStringBaseIcED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB16_1:
	retq
.Lfunc_end16:
	.size	_ZN11CStringBaseIcED2Ev, .Lfunc_end16-_ZN11CStringBaseIcED2Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev: # @_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, (%rbx)
.Ltmp481:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp482:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB17_2:
.Ltmp483:
	movq	%rax, %r14
.Ltmp484:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp485:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp486:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev, .Lfunc_end17-_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp481-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp482-.Ltmp481       #   Call between .Ltmp481 and .Ltmp482
	.long	.Ltmp483-.Lfunc_begin8  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp482-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp484-.Ltmp482       #   Call between .Ltmp482 and .Ltmp484
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp485-.Ltmp484       #   Call between .Ltmp484 and .Ltmp485
	.long	.Ltmp486-.Lfunc_begin8  #     jumps to .Ltmp486
	.byte	1                       #   On action: 1
	.long	.Ltmp485-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp485   #   Call between .Ltmp485 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev: # @_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, (%rbx)
.Ltmp487:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp488:
# BB#1:
.Ltmp493:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp494:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp495:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp489:
	movq	%rax, %r14
.Ltmp490:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp491:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp492:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev, .Lfunc_end18-_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp487-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp488-.Ltmp487       #   Call between .Ltmp487 and .Ltmp488
	.long	.Ltmp489-.Lfunc_begin9  #     jumps to .Ltmp489
	.byte	0                       #   On action: cleanup
	.long	.Ltmp493-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp494-.Ltmp493       #   Call between .Ltmp493 and .Ltmp494
	.long	.Ltmp495-.Lfunc_begin9  #     jumps to .Ltmp495
	.byte	0                       #   On action: cleanup
	.long	.Ltmp490-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp491-.Ltmp490       #   Call between .Ltmp490 and .Ltmp491
	.long	.Ltmp492-.Lfunc_begin9  #     jumps to .Ltmp492
	.byte	1                       #   On action: 1
	.long	.Ltmp491-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp491   #   Call between .Ltmp491 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 64
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB19_8
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_7
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	leaq	16(%rbp), %rdi
.Ltmp496:
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp497:
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_6
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	callq	_ZdaPv
.LBB19_6:                               # %_ZN9NWildcard5CPairD2Ev.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_7:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB19_2
.LBB19_8:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB19_9:
.Ltmp498:
	movq	%rax, %rbx
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_11
# BB#10:
	callq	_ZdaPv
.LBB19_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp496-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp497-.Ltmp496       #   Call between .Ltmp496 and .Ltmp497
	.long	.Ltmp498-.Lfunc_begin10 #     jumps to .Ltmp498
	.byte	0                       #   On action: cleanup
	.long	.Ltmp497-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end19-.Ltmp497   #   Call between .Ltmp497 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NWildcard11CCensorNodeD2Ev,"axG",@progbits,_ZN9NWildcard11CCensorNodeD2Ev,comdat
	.weak	_ZN9NWildcard11CCensorNodeD2Ev
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNodeD2Ev,@function
_ZN9NWildcard11CCensorNodeD2Ev:         # @_ZN9NWildcard11CCensorNodeD2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	88(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 88(%r15)
.Ltmp499:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp500:
# BB#1:
.Ltmp505:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp506:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp516:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp517:
# BB#3:
.Ltmp522:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp523:
# BB#4:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit6
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp534:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp535:
# BB#5:
.Ltmp540:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp541:
# BB#6:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_14
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB20_14:                              # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB20_26:
.Ltmp542:
	movq	%rax, %r14
	jmp	.LBB20_20
.LBB20_12:
.Ltmp536:
	movq	%rax, %r14
.Ltmp537:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp538:
	jmp	.LBB20_20
.LBB20_13:
.Ltmp539:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_25:
.Ltmp524:
	movq	%rax, %r14
	jmp	.LBB20_18
.LBB20_10:
.Ltmp518:
	movq	%rax, %r14
.Ltmp519:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp520:
	jmp	.LBB20_18
.LBB20_11:
.Ltmp521:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_15:
.Ltmp507:
	movq	%rax, %r14
	jmp	.LBB20_16
.LBB20_8:
.Ltmp501:
	movq	%rax, %r14
.Ltmp502:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp503:
.LBB20_16:                              # %.body
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp508:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp509:
# BB#17:
.Ltmp514:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp515:
.LBB20_18:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit11
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp525:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp526:
# BB#19:
.Ltmp531:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp532:
.LBB20_20:                              # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit14
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_22
# BB#21:
	callq	_ZdaPv
.LBB20_22:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_9:
.Ltmp504:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_23:
.Ltmp510:
	movq	%rax, %r14
.Ltmp511:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp512:
	jmp	.LBB20_30
.LBB20_24:
.Ltmp513:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_27:
.Ltmp527:
	movq	%rax, %r14
.Ltmp528:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp529:
	jmp	.LBB20_30
.LBB20_28:
.Ltmp530:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_29:
.Ltmp533:
	movq	%rax, %r14
.LBB20_30:                              # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN9NWildcard11CCensorNodeD2Ev, .Lfunc_end20-_ZN9NWildcard11CCensorNodeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp499-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp500-.Ltmp499       #   Call between .Ltmp499 and .Ltmp500
	.long	.Ltmp501-.Lfunc_begin11 #     jumps to .Ltmp501
	.byte	0                       #   On action: cleanup
	.long	.Ltmp505-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp506-.Ltmp505       #   Call between .Ltmp505 and .Ltmp506
	.long	.Ltmp507-.Lfunc_begin11 #     jumps to .Ltmp507
	.byte	0                       #   On action: cleanup
	.long	.Ltmp516-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp517-.Ltmp516       #   Call between .Ltmp516 and .Ltmp517
	.long	.Ltmp518-.Lfunc_begin11 #     jumps to .Ltmp518
	.byte	0                       #   On action: cleanup
	.long	.Ltmp522-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp523-.Ltmp522       #   Call between .Ltmp522 and .Ltmp523
	.long	.Ltmp524-.Lfunc_begin11 #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp534-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin11 #     jumps to .Ltmp536
	.byte	0                       #   On action: cleanup
	.long	.Ltmp540-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp541-.Ltmp540       #   Call between .Ltmp540 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin11 #     jumps to .Ltmp542
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin11 #     jumps to .Ltmp539
	.byte	1                       #   On action: 1
	.long	.Ltmp519-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp520-.Ltmp519       #   Call between .Ltmp519 and .Ltmp520
	.long	.Ltmp521-.Lfunc_begin11 #     jumps to .Ltmp521
	.byte	1                       #   On action: 1
	.long	.Ltmp502-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp503-.Ltmp502       #   Call between .Ltmp502 and .Ltmp503
	.long	.Ltmp504-.Lfunc_begin11 #     jumps to .Ltmp504
	.byte	1                       #   On action: 1
	.long	.Ltmp508-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Ltmp509-.Ltmp508       #   Call between .Ltmp508 and .Ltmp509
	.long	.Ltmp510-.Lfunc_begin11 #     jumps to .Ltmp510
	.byte	1                       #   On action: 1
	.long	.Ltmp514-.Lfunc_begin11 # >> Call Site 11 <<
	.long	.Ltmp515-.Ltmp514       #   Call between .Ltmp514 and .Ltmp515
	.long	.Ltmp533-.Lfunc_begin11 #     jumps to .Ltmp533
	.byte	1                       #   On action: 1
	.long	.Ltmp525-.Lfunc_begin11 # >> Call Site 12 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin11 #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp531-.Lfunc_begin11 # >> Call Site 13 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin11 #     jumps to .Ltmp533
	.byte	1                       #   On action: 1
	.long	.Ltmp532-.Lfunc_begin11 # >> Call Site 14 <<
	.long	.Ltmp511-.Ltmp532       #   Call between .Ltmp532 and .Ltmp511
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp511-.Lfunc_begin11 # >> Call Site 15 <<
	.long	.Ltmp512-.Ltmp511       #   Call between .Ltmp511 and .Ltmp512
	.long	.Ltmp513-.Lfunc_begin11 #     jumps to .Ltmp513
	.byte	1                       #   On action: 1
	.long	.Ltmp528-.Lfunc_begin11 # >> Call Site 16 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin11 #     jumps to .Ltmp530
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 32
.Lcfi116:
	.cfi_offset %rbx, -24
.Lcfi117:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp543:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp544:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB21_2:
.Ltmp545:
	movq	%rax, %r14
.Ltmp546:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp547:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_4:
.Ltmp548:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev, .Lfunc_end21-_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp543-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin12 #     jumps to .Ltmp545
	.byte	0                       #   On action: cleanup
	.long	.Ltmp544-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp546-.Ltmp544       #   Call between .Ltmp544 and .Ltmp546
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp546-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp547-.Ltmp546       #   Call between .Ltmp546 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin12 #     jumps to .Ltmp548
	.byte	1                       #   On action: 1
	.long	.Ltmp547-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp547   #   Call between .Ltmp547 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp549:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp550:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB22_2:
.Ltmp551:
	movq	%rax, %r14
.Ltmp552:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp553:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp554:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev, .Lfunc_end22-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp549-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin13 #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp550-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp552-.Ltmp550       #   Call between .Ltmp550 and .Ltmp552
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp552-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp553-.Ltmp552       #   Call between .Ltmp552 and .Ltmp553
	.long	.Ltmp554-.Lfunc_begin13 #     jumps to .Ltmp554
	.byte	1                       #   On action: 1
	.long	.Ltmp553-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp553   #   Call between .Ltmp553 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp555:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp556:
# BB#1:
.Ltmp561:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp562:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_5:
.Ltmp563:
	movq	%rax, %r14
	jmp	.LBB23_6
.LBB23_3:
.Ltmp557:
	movq	%rax, %r14
.Ltmp558:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp559:
.LBB23_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_4:
.Ltmp560:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev, .Lfunc_end23-_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp555-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp556-.Ltmp555       #   Call between .Ltmp555 and .Ltmp556
	.long	.Ltmp557-.Lfunc_begin14 #     jumps to .Ltmp557
	.byte	0                       #   On action: cleanup
	.long	.Ltmp561-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp562-.Ltmp561       #   Call between .Ltmp561 and .Ltmp562
	.long	.Ltmp563-.Lfunc_begin14 #     jumps to .Ltmp563
	.byte	0                       #   On action: cleanup
	.long	.Ltmp558-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp559-.Ltmp558       #   Call between .Ltmp558 and .Ltmp559
	.long	.Ltmp560-.Lfunc_begin14 #     jumps to .Ltmp560
	.byte	1                       #   On action: 1
	.long	.Ltmp559-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp559   #   Call between .Ltmp559 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 64
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB24_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB24_6
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbp)
.Ltmp564:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp565:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
                                        #   in Loop: Header=BB24_2 Depth=1
.Ltmp570:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp571:
# BB#5:                                 # %_ZN9NWildcard5CItemD2Ev.exit
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB24_6:                               #   in Loop: Header=BB24_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB24_2
.LBB24_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB24_8:
.Ltmp566:
	movq	%rax, %rbx
.Ltmp567:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp568:
	jmp	.LBB24_11
.LBB24_9:
.Ltmp569:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_10:
.Ltmp572:
	movq	%rax, %rbx
.LBB24_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii, .Lfunc_end24-_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp564-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp565-.Ltmp564       #   Call between .Ltmp564 and .Ltmp565
	.long	.Ltmp566-.Lfunc_begin15 #     jumps to .Ltmp566
	.byte	0                       #   On action: cleanup
	.long	.Ltmp570-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp571-.Ltmp570       #   Call between .Ltmp570 and .Ltmp571
	.long	.Ltmp572-.Lfunc_begin15 #     jumps to .Ltmp572
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp567-.Ltmp571       #   Call between .Ltmp571 and .Ltmp567
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp567-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp568-.Ltmp567       #   Call between .Ltmp567 and .Ltmp568
	.long	.Ltmp569-.Lfunc_begin15 #     jumps to .Ltmp569
	.byte	1                       #   On action: 1
	.long	.Ltmp568-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Lfunc_end24-.Ltmp568   #   Call between .Ltmp568 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -24
.Lcfi145:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp573:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp574:
# BB#1:
.Ltmp579:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp580:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_5:
.Ltmp581:
	movq	%rax, %r14
	jmp	.LBB25_6
.LBB25_3:
.Ltmp575:
	movq	%rax, %r14
.Ltmp576:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp577:
.LBB25_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_4:
.Ltmp578:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev, .Lfunc_end25-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp573-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp574-.Ltmp573       #   Call between .Ltmp573 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin16 #     jumps to .Ltmp575
	.byte	0                       #   On action: cleanup
	.long	.Ltmp579-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin16 #     jumps to .Ltmp581
	.byte	0                       #   On action: cleanup
	.long	.Ltmp576-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp577-.Ltmp576       #   Call between .Ltmp576 and .Ltmp577
	.long	.Ltmp578-.Lfunc_begin16 #     jumps to .Ltmp578
	.byte	1                       #   On action: 1
	.long	.Ltmp577-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end25-.Ltmp577   #   Call between .Ltmp577 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 64
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB26_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB26_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB26_5
# BB#3:                                 #   in Loop: Header=BB26_2 Depth=1
.Ltmp582:
	movq	%rbp, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp583:
# BB#4:                                 #   in Loop: Header=BB26_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB26_5:                               #   in Loop: Header=BB26_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB26_2
.LBB26_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB26_7:
.Ltmp584:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii, .Lfunc_end26-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp582-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin17 #     jumps to .Ltmp584
	.byte	0                       #   On action: cleanup
	.long	.Ltmp583-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp583   #   Call between .Ltmp583 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CDirItemED2Ev,"axG",@progbits,_ZN13CObjectVectorI8CDirItemED2Ev,comdat
	.weak	_ZN13CObjectVectorI8CDirItemED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemED2Ev,@function
_ZN13CObjectVectorI8CDirItemED2Ev:      # @_ZN13CObjectVectorI8CDirItemED2Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, (%rbx)
.Ltmp585:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp586:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB27_2:
.Ltmp587:
	movq	%rax, %r14
.Ltmp588:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp589:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_4:
.Ltmp590:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN13CObjectVectorI8CDirItemED2Ev, .Lfunc_end27-_ZN13CObjectVectorI8CDirItemED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp585-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp586-.Ltmp585       #   Call between .Ltmp585 and .Ltmp586
	.long	.Ltmp587-.Lfunc_begin18 #     jumps to .Ltmp587
	.byte	0                       #   On action: cleanup
	.long	.Ltmp586-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp588-.Ltmp586       #   Call between .Ltmp586 and .Ltmp588
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp588-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp589-.Ltmp588       #   Call between .Ltmp588 and .Ltmp589
	.long	.Ltmp590-.Lfunc_begin18 #     jumps to .Ltmp590
	.byte	1                       #   On action: 1
	.long	.Ltmp589-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp589   #   Call between .Ltmp589 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CDirItemED0Ev,"axG",@progbits,_ZN13CObjectVectorI8CDirItemED0Ev,comdat
	.weak	_ZN13CObjectVectorI8CDirItemED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemED0Ev,@function
_ZN13CObjectVectorI8CDirItemED0Ev:      # @_ZN13CObjectVectorI8CDirItemED0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, (%rbx)
.Ltmp591:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp592:
# BB#1:
.Ltmp597:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp598:
# BB#2:                                 # %_ZN13CObjectVectorI8CDirItemED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_5:
.Ltmp599:
	movq	%rax, %r14
	jmp	.LBB28_6
.LBB28_3:
.Ltmp593:
	movq	%rax, %r14
.Ltmp594:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp595:
.LBB28_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_4:
.Ltmp596:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZN13CObjectVectorI8CDirItemED0Ev, .Lfunc_end28-_ZN13CObjectVectorI8CDirItemED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp591-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp592-.Ltmp591       #   Call between .Ltmp591 and .Ltmp592
	.long	.Ltmp593-.Lfunc_begin19 #     jumps to .Ltmp593
	.byte	0                       #   On action: cleanup
	.long	.Ltmp597-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp598-.Ltmp597       #   Call between .Ltmp597 and .Ltmp598
	.long	.Ltmp599-.Lfunc_begin19 #     jumps to .Ltmp599
	.byte	0                       #   On action: cleanup
	.long	.Ltmp594-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp595-.Ltmp594       #   Call between .Ltmp594 and .Ltmp595
	.long	.Ltmp596-.Lfunc_begin19 #     jumps to .Ltmp596
	.byte	1                       #   On action: 1
	.long	.Ltmp595-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end28-.Ltmp595   #   Call between .Ltmp595 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CDirItemE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI8CDirItemE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemE6DeleteEii,@function
_ZN13CObjectVectorI8CDirItemE6DeleteEii: # @_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 64
.Lcfi176:
	.cfi_offset %rbx, -56
.Lcfi177:
	.cfi_offset %r12, -48
.Lcfi178:
	.cfi_offset %r13, -40
.Lcfi179:
	.cfi_offset %r14, -32
.Lcfi180:
	.cfi_offset %r15, -24
.Lcfi181:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB29_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB29_6
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB29_5
# BB#4:                                 #   in Loop: Header=BB29_2 Depth=1
	callq	_ZdaPv
.LBB29_5:                               # %_ZN8CDirItemD2Ev.exit
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB29_6:                               #   in Loop: Header=BB29_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB29_2
.LBB29_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end29:
	.size	_ZN13CObjectVectorI8CDirItemE6DeleteEii, .Lfunc_end29-_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 64
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB30_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB30_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB30_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB30_5
.LBB30_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB30_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp600:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp601:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB30_35
# BB#12:
	movq	%rbx, %r13
.LBB30_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB30_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB30_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB30_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB30_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB30_15
.LBB30_14:
	xorl	%esi, %esi
.LBB30_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB30_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB30_16
.LBB30_29:
	movq	%r13, %rbx
.LBB30_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp602:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp603:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB30_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB30_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB30_8
.LBB30_3:
	xorl	%eax, %eax
.LBB30_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB30_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB30_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB30_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB30_30
.LBB30_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB30_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB30_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB30_24
	jmp	.LBB30_25
.LBB30_22:
	xorl	%ecx, %ecx
.LBB30_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB30_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB30_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB30_27
.LBB30_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB30_15
	jmp	.LBB30_29
.LBB30_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp604:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end30-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp600-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp600
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp600-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp603-.Ltmp600       #   Call between .Ltmp600 and .Ltmp603
	.long	.Ltmp604-.Lfunc_begin20 #     jumps to .Ltmp604
	.byte	0                       #   On action: cleanup
	.long	.Ltmp603-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end30-.Ltmp603   #   Call between .Ltmp603 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_,"axG",@progbits,_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_,comdat
	.weak	_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_,@function
_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_: # @_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r15
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 48
.Lcfi200:
	.cfi_offset %rbx, -48
.Lcfi201:
	.cfi_offset %r12, -40
.Lcfi202:
	.cfi_offset %r13, -32
.Lcfi203:
	.cfi_offset %r14, -24
.Lcfi204:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$168, %edi
	callq	_Znwm
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movslq	8(%r15), %r13
	leaq	1(%r13), %rbx
	testl	%ebx, %ebx
	je	.LBB31_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp605:
	callq	_Znam
.Ltmp606:
# BB#3:                                 # %.noexc
	movq	%rax, (%r12)
	movl	$0, (%rax)
	movl	%ebx, 12(%r12)
	jmp	.LBB31_4
.LBB31_1:
	xorl	%eax, %eax
.LBB31_4:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB31_5:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB31_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%r13d, 8(%r12)
	movq	%r12, %rdi
	addq	$16, %rdi
	leaq	16(%r15), %rsi
.Ltmp608:
	callq	_ZN12CArchivePathC2ERKS_
.Ltmp609:
# BB#7:
	movups	136(%r15), %xmm0
	movups	148(%r15), %xmm1
	movups	%xmm1, 148(%r12)
	movups	%xmm0, 136(%r12)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%r12, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB31_10:
.Ltmp607:
	movq	%rax, %rbx
	jmp	.LBB31_11
.LBB31_8:
.Ltmp610:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB31_11
# BB#9:
	callq	_ZdaPv
.LBB31_11:                              # %.body
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_, .Lfunc_end31-_ZN13CObjectVectorI21CUpdateArchiveCommandE3AddERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin21-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp605-.Lfunc_begin21 #   Call between .Lfunc_begin21 and .Ltmp605
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp605-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin21 #     jumps to .Ltmp607
	.byte	0                       #   On action: cleanup
	.long	.Ltmp608-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin21 #     jumps to .Ltmp610
	.byte	0                       #   On action: cleanup
	.long	.Ltmp609-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end31-.Ltmp609   #   Call between .Ltmp609 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN21CUpdateArchiveCommandD2Ev,"axG",@progbits,_ZN21CUpdateArchiveCommandD2Ev,comdat
	.weak	_ZN21CUpdateArchiveCommandD2Ev
	.p2align	4, 0x90
	.type	_ZN21CUpdateArchiveCommandD2Ev,@function
_ZN21CUpdateArchiveCommandD2Ev:         # @_ZN21CUpdateArchiveCommandD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 16
.Lcfi206:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	callq	_ZdaPv
.LBB32_2:                               # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_4
# BB#3:
	callq	_ZdaPv
.LBB32_4:                               # %_ZN11CStringBaseIwED2Ev.exit1.i
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_6
# BB#5:
	callq	_ZdaPv
.LBB32_6:                               # %_ZN11CStringBaseIwED2Ev.exit2.i
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_8
# BB#7:
	callq	_ZdaPv
.LBB32_8:                               # %_ZN11CStringBaseIwED2Ev.exit3.i
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_10
# BB#9:
	callq	_ZdaPv
.LBB32_10:                              # %_ZN11CStringBaseIwED2Ev.exit4.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_12
# BB#11:
	callq	_ZdaPv
.LBB32_12:                              # %_ZN11CStringBaseIwED2Ev.exit5.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_14
# BB#13:
	callq	_ZdaPv
.LBB32_14:                              # %_ZN12CArchivePathD2Ev.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_15
# BB#16:
	popq	%rbx
	jmp	_ZdaPv                  # TAILCALL
.LBB32_15:                              # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	retq
.Lfunc_end32:
	.size	_ZN21CUpdateArchiveCommandD2Ev, .Lfunc_end32-_ZN21CUpdateArchiveCommandD2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI33_0:
	.zero	16
	.section	.text._ZN12CArchivePathC2Ev,"axG",@progbits,_ZN12CArchivePathC2Ev,comdat
	.weak	_ZN12CArchivePathC2Ev
	.p2align	4, 0x90
	.type	_ZN12CArchivePathC2Ev,@function
_ZN12CArchivePathC2Ev:                  # @_ZN12CArchivePathC2Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi211:
	.cfi_def_cfa_offset 48
.Lcfi212:
	.cfi_offset %rbx, -40
.Lcfi213:
	.cfi_offset %r12, -32
.Lcfi214:
	.cfi_offset %r14, -24
.Lcfi215:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	$4, 12(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
.Ltmp611:
	movl	$16, %edi
	callq	_Znam
.Ltmp612:
# BB#1:
	movq	%rax, 16(%rbx)
	movl	$0, (%rax)
	movl	$4, 28(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp614:
	movl	$16, %edi
	callq	_Znam
.Ltmp615:
# BB#2:
	movq	%rax, 32(%rbx)
	movl	$0, (%rax)
	movl	$4, 44(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp617:
	movl	$16, %edi
	callq	_Znam
.Ltmp618:
# BB#3:
	movq	%rax, 48(%rbx)
	movl	$0, (%rax)
	movl	$4, 60(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp620:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp621:
# BB#4:
	movq	%r15, 64(%rbx)
	movl	$0, (%r15)
	movl	$4, 76(%rbx)
	movb	$0, 80(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
.Ltmp623:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp624:
# BB#5:
	leaq	64(%rbx), %r15
	movq	%r12, 88(%rbx)
	movl	$0, (%r12)
	movl	$4, 100(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp626:
	movl	$16, %edi
	callq	_Znam
.Ltmp627:
# BB#6:
	movq	%rax, 104(%rbx)
	movl	$0, (%rax)
	movl	$4, 116(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB33_12:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp628:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB33_13
	jmp	.LBB33_14
.LBB33_11:                              # %_ZN11CStringBaseIwED2Ev.exit.thread
.Ltmp625:
	movq	%rax, %r14
.LBB33_13:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB33_14
.LBB33_10:
.Ltmp622:
	movq	%rax, %r14
.LBB33_14:                              # %_ZN11CStringBaseIwED2Ev.exit12
	leaq	48(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB33_16
# BB#15:
	callq	_ZdaPv
	jmp	.LBB33_16
.LBB33_9:
.Ltmp619:
	movq	%rax, %r14
.LBB33_16:                              # %_ZN11CStringBaseIwED2Ev.exit13
	leaq	32(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB33_18
# BB#17:
	callq	_ZdaPv
	jmp	.LBB33_18
.LBB33_8:
.Ltmp616:
	movq	%rax, %r14
.LBB33_18:                              # %_ZN11CStringBaseIwED2Ev.exit14
	leaq	16(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB33_20
# BB#19:
	callq	_ZdaPv
	jmp	.LBB33_20
.LBB33_7:
.Ltmp613:
	movq	%rax, %r14
.LBB33_20:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_22
# BB#21:
	callq	_ZdaPv
.LBB33_22:                              # %_ZN11CStringBaseIwED2Ev.exit16
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZN12CArchivePathC2Ev, .Lfunc_end33-_ZN12CArchivePathC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin22-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp611-.Lfunc_begin22 #   Call between .Lfunc_begin22 and .Ltmp611
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp611-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin22 #     jumps to .Ltmp613
	.byte	0                       #   On action: cleanup
	.long	.Ltmp614-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin22 #     jumps to .Ltmp616
	.byte	0                       #   On action: cleanup
	.long	.Ltmp617-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Ltmp618-.Ltmp617       #   Call between .Ltmp617 and .Ltmp618
	.long	.Ltmp619-.Lfunc_begin22 #     jumps to .Ltmp619
	.byte	0                       #   On action: cleanup
	.long	.Ltmp620-.Lfunc_begin22 # >> Call Site 5 <<
	.long	.Ltmp621-.Ltmp620       #   Call between .Ltmp620 and .Ltmp621
	.long	.Ltmp622-.Lfunc_begin22 #     jumps to .Ltmp622
	.byte	0                       #   On action: cleanup
	.long	.Ltmp623-.Lfunc_begin22 # >> Call Site 6 <<
	.long	.Ltmp624-.Ltmp623       #   Call between .Ltmp623 and .Ltmp624
	.long	.Ltmp625-.Lfunc_begin22 #     jumps to .Ltmp625
	.byte	0                       #   On action: cleanup
	.long	.Ltmp626-.Lfunc_begin22 # >> Call Site 7 <<
	.long	.Ltmp627-.Ltmp626       #   Call between .Ltmp626 and .Ltmp627
	.long	.Ltmp628-.Lfunc_begin22 #     jumps to .Ltmp628
	.byte	0                       #   On action: cleanup
	.long	.Ltmp627-.Lfunc_begin22 # >> Call Site 8 <<
	.long	.Lfunc_end33-.Ltmp627   #   Call between .Ltmp627 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN12CArchivePathC2ERKS_,"axG",@progbits,_ZN12CArchivePathC2ERKS_,comdat
	.weak	_ZN12CArchivePathC2ERKS_
	.p2align	4, 0x90
	.type	_ZN12CArchivePathC2ERKS_,@function
_ZN12CArchivePathC2ERKS_:               # @_ZN12CArchivePathC2ERKS_
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%rbp
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi220:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi222:
	.cfi_def_cfa_offset 96
.Lcfi223:
	.cfi_offset %rbx, -56
.Lcfi224:
	.cfi_offset %r12, -48
.Lcfi225:
	.cfi_offset %r13, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movslq	8(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB34_3
.LBB34_1:
	xorl	%eax, %eax
.LBB34_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB34_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB34_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%ebp, 8(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	movslq	24(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_6
# BB#7:                                 # %._crit_edge16.i.i15
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp629:
	callq	_Znam
.Ltmp630:
# BB#8:                                 # %.noexc
	movq	%rax, 16(%r13)
	movl	$0, (%rax)
	movl	%ebx, 28(%r13)
	jmp	.LBB34_9
.LBB34_6:
	xorl	%eax, %eax
.LBB34_9:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i16
	leaq	16(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	16(%r14), %rcx
	.p2align	4, 0x90
.LBB34_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB34_10
# BB#11:
	movl	%ebp, 24(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r13)
	movslq	40(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_12
# BB#13:                                # %._crit_edge16.i.i20
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp632:
	callq	_Znam
.Ltmp633:
# BB#14:                                # %.noexc24
	movq	%rax, 32(%r13)
	movl	$0, (%rax)
	movl	%ebx, 44(%r13)
	jmp	.LBB34_15
.LBB34_12:
	xorl	%eax, %eax
.LBB34_15:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
	leaq	32(%r13), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	32(%r14), %rcx
	.p2align	4, 0x90
.LBB34_16:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB34_16
# BB#17:
	movl	%ebp, 40(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r13)
	movslq	56(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_18
# BB#19:                                # %._crit_edge16.i.i26
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp635:
	callq	_Znam
.Ltmp636:
# BB#20:                                # %.noexc30
	movq	%rax, 48(%r13)
	movl	$0, (%rax)
	movl	%ebx, 60(%r13)
	jmp	.LBB34_21
.LBB34_18:
	xorl	%eax, %eax
.LBB34_21:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i27
	leaq	48(%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	48(%r14), %rcx
	.p2align	4, 0x90
.LBB34_22:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB34_22
# BB#23:
	movl	%ebp, 56(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r13)
	movslq	72(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_24
# BB#25:                                # %._crit_edge16.i.i32
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp638:
	callq	_Znam
	movq	%rax, %r15
.Ltmp639:
# BB#26:                                # %.noexc36
	movq	%r15, 64(%r13)
	movl	$0, (%r15)
	movl	%ebx, 76(%r13)
	jmp	.LBB34_27
.LBB34_24:
	xorl	%r15d, %r15d
.LBB34_27:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i33
	leaq	64(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	64(%r14), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB34_28:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB34_28
# BB#29:
	movl	%ebp, 72(%r13)
	movb	80(%r14), %al
	movb	%al, 80(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%r13)
	movslq	96(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_30
# BB#31:                                # %._crit_edge16.i.i38
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp641:
	callq	_Znam
	movq	%rax, %r12
.Ltmp642:
# BB#32:                                # %.noexc42
	movq	%r12, 88(%r13)
	movl	$0, (%r12)
	movl	%ebx, 100(%r13)
	jmp	.LBB34_33
.LBB34_30:
	xorl	%r12d, %r12d
.LBB34_33:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i39
	movq	88(%r14), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB34_34:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB34_34
# BB#35:
	movl	%ebp, 96(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r13)
	movslq	112(%r14), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB34_36
# BB#37:                                # %._crit_edge16.i.i44
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp644:
	callq	_Znam
.Ltmp645:
# BB#38:                                # %.noexc48
	movq	%rax, 104(%r13)
	movl	$0, (%rax)
	movl	%ebx, 116(%r13)
	jmp	.LBB34_39
.LBB34_36:
	xorl	%eax, %eax
.LBB34_39:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i45
	movq	104(%r14), %rcx
	.p2align	4, 0x90
.LBB34_40:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB34_40
# BB#41:
	movl	%ebp, 112(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB34_47:
.Ltmp646:
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB34_49
# BB#48:
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.LBB34_50
	jmp	.LBB34_51
.LBB34_46:
.Ltmp643:
	movq	%rax, %r14
.LBB34_49:                              # %_ZN11CStringBaseIwED2Ev.exit
	testq	%r15, %r15
	je	.LBB34_51
.LBB34_50:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB34_51
.LBB34_45:
.Ltmp640:
	movq	%rax, %r14
.LBB34_51:                              # %_ZN11CStringBaseIwED2Ev.exit50
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB34_53
# BB#52:
	callq	_ZdaPv
	jmp	.LBB34_53
.LBB34_44:
.Ltmp637:
	movq	%rax, %r14
.LBB34_53:                              # %_ZN11CStringBaseIwED2Ev.exit51
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB34_55
# BB#54:
	callq	_ZdaPv
	jmp	.LBB34_55
.LBB34_43:
.Ltmp634:
	movq	%rax, %r14
.LBB34_55:                              # %_ZN11CStringBaseIwED2Ev.exit52
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB34_57
# BB#56:
	callq	_ZdaPv
	jmp	.LBB34_57
.LBB34_42:
.Ltmp631:
	movq	%rax, %r14
.LBB34_57:                              # %_ZN11CStringBaseIwED2Ev.exit53
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB34_59
# BB#58:
	callq	_ZdaPv
.LBB34_59:                              # %_ZN11CStringBaseIwED2Ev.exit54
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZN12CArchivePathC2ERKS_, .Lfunc_end34-_ZN12CArchivePathC2ERKS_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin23-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp629-.Lfunc_begin23 #   Call between .Lfunc_begin23 and .Ltmp629
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp629-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp630-.Ltmp629       #   Call between .Ltmp629 and .Ltmp630
	.long	.Ltmp631-.Lfunc_begin23 #     jumps to .Ltmp631
	.byte	0                       #   On action: cleanup
	.long	.Ltmp632-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp633-.Ltmp632       #   Call between .Ltmp632 and .Ltmp633
	.long	.Ltmp634-.Lfunc_begin23 #     jumps to .Ltmp634
	.byte	0                       #   On action: cleanup
	.long	.Ltmp635-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp636-.Ltmp635       #   Call between .Ltmp635 and .Ltmp636
	.long	.Ltmp637-.Lfunc_begin23 #     jumps to .Ltmp637
	.byte	0                       #   On action: cleanup
	.long	.Ltmp638-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp639-.Ltmp638       #   Call between .Ltmp638 and .Ltmp639
	.long	.Ltmp640-.Lfunc_begin23 #     jumps to .Ltmp640
	.byte	0                       #   On action: cleanup
	.long	.Ltmp641-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp642-.Ltmp641       #   Call between .Ltmp641 and .Ltmp642
	.long	.Ltmp643-.Lfunc_begin23 #     jumps to .Ltmp643
	.byte	0                       #   On action: cleanup
	.long	.Ltmp644-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp645-.Ltmp644       #   Call between .Ltmp644 and .Ltmp645
	.long	.Ltmp646-.Lfunc_begin23 #     jumps to .Ltmp646
	.byte	0                       #   On action: cleanup
	.long	.Ltmp645-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Lfunc_end34-.Ltmp645   #   Call between .Ltmp645 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -24
.Lcfi233:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp647:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp648:
# BB#1:
.Ltmp653:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp654:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_5:
.Ltmp655:
	movq	%rax, %r14
	jmp	.LBB35_6
.LBB35_3:
.Ltmp649:
	movq	%rax, %r14
.Ltmp650:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp651:
.LBB35_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp652:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end35-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp647-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp648-.Ltmp647       #   Call between .Ltmp647 and .Ltmp648
	.long	.Ltmp649-.Lfunc_begin24 #     jumps to .Ltmp649
	.byte	0                       #   On action: cleanup
	.long	.Ltmp653-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp654-.Ltmp653       #   Call between .Ltmp653 and .Ltmp654
	.long	.Ltmp655-.Lfunc_begin24 #     jumps to .Ltmp655
	.byte	0                       #   On action: cleanup
	.long	.Ltmp650-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp651-.Ltmp650       #   Call between .Ltmp650 and .Ltmp651
	.long	.Ltmp652-.Lfunc_begin24 #     jumps to .Ltmp652
	.byte	1                       #   On action: 1
	.long	.Ltmp651-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp651   #   Call between .Ltmp651 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi237:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi238:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi240:
	.cfi_def_cfa_offset 64
.Lcfi241:
	.cfi_offset %rbx, -56
.Lcfi242:
	.cfi_offset %r12, -48
.Lcfi243:
	.cfi_offset %r13, -40
.Lcfi244:
	.cfi_offset %r14, -32
.Lcfi245:
	.cfi_offset %r15, -24
.Lcfi246:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB36_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB36_6
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_5
# BB#4:                                 #   in Loop: Header=BB36_2 Depth=1
	callq	_ZdaPv
.LBB36_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB36_6:                               #   in Loop: Header=BB36_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB36_2
.LBB36_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end36:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end36-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi249:
	.cfi_def_cfa_offset 32
.Lcfi250:
	.cfi_offset %rbx, -24
.Lcfi251:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp656:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp657:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_2:
.Ltmp658:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end37:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end37-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp656-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp657-.Ltmp656       #   Call between .Ltmp656 and .Ltmp657
	.long	.Ltmp658-.Lfunc_begin25 #     jumps to .Ltmp658
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Lfunc_end37-.Ltmp657   #   Call between .Ltmp657 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi254:
	.cfi_def_cfa_offset 32
.Lcfi255:
	.cfi_offset %rbx, -24
.Lcfi256:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp659:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp660:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_2:
.Ltmp661:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end38-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp659-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp660-.Ltmp659       #   Call between .Ltmp659 and .Ltmp660
	.long	.Ltmp661-.Lfunc_begin26 #     jumps to .Ltmp661
	.byte	0                       #   On action: cleanup
	.long	.Ltmp660-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp660   #   Call between .Ltmp660 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI39_0:
	.long	80                      # 0x50
	.long	81                      # 0x51
	.long	82                      # 0x52
	.long	88                      # 0x58
.LCPI39_1:
	.long	89                      # 0x59
	.long	90                      # 0x5a
	.long	87                      # 0x57
	.long	0                       # 0x0
.LCPI39_2:
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	51                      # 0x33
.LCPI39_3:
	.zero	16
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ArchiveCommandLine.ii,@function
_GLOBAL__sub_I_ArchiveCommandLine.ii:   # @_GLOBAL__sub_I_ArchiveCommandLine.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 16
	movq	$.L.str.5, _ZL12kSwitchForms(%rip)
	movl	$0, _ZL12kSwitchForms+8(%rip)
	movb	$0, _ZL12kSwitchForms+12(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, _ZL12kSwitchForms+16(%rip)
	movq	$.L.str.9, _ZL12kSwitchForms+32(%rip)
	movl	$0, _ZL12kSwitchForms+40(%rip)
	movb	$0, _ZL12kSwitchForms+44(%rip)
	movaps	%xmm0, _ZL12kSwitchForms+48(%rip)
	movq	$.L.str.10, _ZL12kSwitchForms+64(%rip)
	movl	$0, _ZL12kSwitchForms+72(%rip)
	movb	$0, _ZL12kSwitchForms+76(%rip)
	movaps	%xmm0, _ZL12kSwitchForms+80(%rip)
	movq	$.L.str.11, _ZL12kSwitchForms+96(%rip)
	movl	$0, _ZL12kSwitchForms+104(%rip)
	movb	$0, _ZL12kSwitchForms+108(%rip)
	movaps	%xmm0, _ZL12kSwitchForms+112(%rip)
	movq	$.L.str.12, _ZL12kSwitchForms+128(%rip)
	movl	$0, _ZL12kSwitchForms+136(%rip)
	movb	$0, _ZL12kSwitchForms+140(%rip)
	movaps	%xmm0, _ZL12kSwitchForms+144(%rip)
	xorps	%xmm2, %xmm2
	movq	$.L.str.13, _ZL12kSwitchForms+160(%rip)
	movl	$3, _ZL12kSwitchForms+168(%rip)
	movb	$0, _ZL12kSwitchForms+172(%rip)
	movl	$1, _ZL12kSwitchForms+176(%rip)
	movl	$0, _ZL12kSwitchForms+180(%rip)
	movl	$.L.str.14, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+184(%rip)
	movl	$0, _ZL12kSwitchForms+200(%rip)
	movb	$0, _ZL12kSwitchForms+204(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+208(%rip)
	movq	$.L.str.15, _ZL12kSwitchForms+224(%rip)
	movl	$3, _ZL12kSwitchForms+232(%rip)
	movb	$0, _ZL12kSwitchForms+236(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+240(%rip)
	movq	$.L.str.16, _ZL12kSwitchForms+256(%rip)
	movl	$3, _ZL12kSwitchForms+264(%rip)
	movb	$1, _ZL12kSwitchForms+268(%rip)
	movl	$1, _ZL12kSwitchForms+272(%rip)
	movl	$0, _ZL12kSwitchForms+276(%rip)
	movl	$.L.str.17, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+280(%rip)
	movl	$3, _ZL12kSwitchForms+296(%rip)
	movb	$0, _ZL12kSwitchForms+300(%rip)
	movl	$1, _ZL12kSwitchForms+304(%rip)
	movl	$0, _ZL12kSwitchForms+308(%rip)
	movl	$.L.str.18, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+312(%rip)
	movl	$3, _ZL12kSwitchForms+328(%rip)
	movb	$0, _ZL12kSwitchForms+332(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+336(%rip)
	movq	$.L.str.19, _ZL12kSwitchForms+352(%rip)
	movl	$3, _ZL12kSwitchForms+360(%rip)
	movb	$1, _ZL12kSwitchForms+364(%rip)
	movl	$2, _ZL12kSwitchForms+368(%rip)
	movl	$0, _ZL12kSwitchForms+372(%rip)
	movq	$0, _ZL12kSwitchForms+376(%rip)
	movq	$.L.str.20, _ZL12kSwitchForms+384(%rip)
	movl	$3, _ZL12kSwitchForms+392(%rip)
	movb	$1, _ZL12kSwitchForms+396(%rip)
	movl	$2, _ZL12kSwitchForms+400(%rip)
	movl	$0, _ZL12kSwitchForms+404(%rip)
	movl	$.L.str.21, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+408(%rip)
	movl	$3, _ZL12kSwitchForms+424(%rip)
	movb	$1, _ZL12kSwitchForms+428(%rip)
	movl	$2, _ZL12kSwitchForms+432(%rip)
	movl	$0, _ZL12kSwitchForms+436(%rip)
	movl	$.L.str.22, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+440(%rip)
	movl	$3, _ZL12kSwitchForms+456(%rip)
	movb	$1, _ZL12kSwitchForms+460(%rip)
	movl	$2, _ZL12kSwitchForms+464(%rip)
	movl	$0, _ZL12kSwitchForms+468(%rip)
	movl	$.L.str.23, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+472(%rip)
	movl	$0, _ZL12kSwitchForms+488(%rip)
	movb	$0, _ZL12kSwitchForms+492(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+496(%rip)
	movq	$.L.str.24, _ZL12kSwitchForms+512(%rip)
	movl	$3, _ZL12kSwitchForms+520(%rip)
	movb	$1, _ZL12kSwitchForms+524(%rip)
	movl	$1, _ZL12kSwitchForms+528(%rip)
	movl	$0, _ZL12kSwitchForms+532(%rip)
	movl	$.L.str.25, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+536(%rip)
	movl	$3, _ZL12kSwitchForms+552(%rip)
	movb	$1, _ZL12kSwitchForms+556(%rip)
	movl	$1, _ZL12kSwitchForms+560(%rip)
	movl	$0, _ZL12kSwitchForms+564(%rip)
	movl	$.L.str.26, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, _ZL12kSwitchForms+568(%rip)
	movl	$4, _ZL12kSwitchForms+584(%rip)
	movb	$0, _ZL12kSwitchForms+588(%rip)
	movl	$0, _ZL12kSwitchForms+592(%rip)
	movl	$0, _ZL12kSwitchForms+596(%rip)
	movl	$.L.str.28, %eax
	movd	%rax, %xmm0
	movl	$.L.str.39, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, _ZL12kSwitchForms+600(%rip)
	movl	$3, _ZL12kSwitchForms+616(%rip)
	movb	$0, _ZL12kSwitchForms+620(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+624(%rip)
	movq	$.L.str.29, _ZL12kSwitchForms+640(%rip)
	movl	$3, _ZL12kSwitchForms+648(%rip)
	movb	$0, _ZL12kSwitchForms+652(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+656(%rip)
	movq	$.L.str.30, _ZL12kSwitchForms+672(%rip)
	movl	$0, _ZL12kSwitchForms+680(%rip)
	movb	$0, _ZL12kSwitchForms+684(%rip)
	movaps	%xmm2, _ZL12kSwitchForms+688(%rip)
	movq	$.L.str.31, _ZL12kSwitchForms+704(%rip)
	movl	$4, _ZL12kSwitchForms+712(%rip)
	movb	$0, _ZL12kSwitchForms+716(%rip)
	movl	$1, _ZL12kSwitchForms+720(%rip)
	movl	$1, _ZL12kSwitchForms+724(%rip)
	movl	$.L.str.32, %eax
	movd	%rax, %xmm0
	movl	$.L.str.40, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, _ZL12kSwitchForms+728(%rip)
	movl	$3, _ZL12kSwitchForms+744(%rip)
	movb	$0, _ZL12kSwitchForms+748(%rip)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, _ZL12kSwitchForms+752(%rip)
	movq	$.L.str.33, _ZL12kSwitchForms+768(%rip)
	movl	$0, _ZL12kSwitchForms+776(%rip)
	movb	$0, _ZL12kSwitchForms+780(%rip)
	movdqa	%xmm0, _ZL12kSwitchForms+784(%rip)
	movq	$.L.str.34, _ZL12kSwitchForms+800(%rip)
	movl	$3, _ZL12kSwitchForms+808(%rip)
	movb	$0, _ZL12kSwitchForms+812(%rip)
	movdqa	%xmm0, _ZL12kSwitchForms+816(%rip)
	movq	$.L.str.35, _ZL12kSwitchForms+832(%rip)
	movl	$0, _ZL12kSwitchForms+840(%rip)
	movb	$0, _ZL12kSwitchForms+844(%rip)
	movdqa	%xmm0, _ZL12kSwitchForms+848(%rip)
	movq	$.L.str.36, _ZL12kSwitchForms+864(%rip)
	movl	$0, _ZL12kSwitchForms+872(%rip)
	movb	$0, _ZL12kSwitchForms+876(%rip)
	movdqa	%xmm0, _ZL12kSwitchForms+880(%rip)
	movq	$.L.str.37, _ZL12kSwitchForms+896(%rip)
	movl	$4, _ZL12kSwitchForms+904(%rip)
	movb	$0, _ZL12kSwitchForms+908(%rip)
	movl	$0, _ZL12kSwitchForms+912(%rip)
	movl	$0, _ZL12kSwitchForms+916(%rip)
	movl	$.L.str.38, %eax
	movd	%rax, %xmm0
	movl	$.L.str.3, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, _ZL12kSwitchForms+920(%rip)
	movl	$0, _ZL12kSwitchForms+936(%rip)
	movb	$0, _ZL12kSwitchForms+940(%rip)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, _ZL12kSwitchForms+944(%rip)
	movdqu	%xmm0, _ZL21kUpdatePairStateIDSet(%rip)
	movl	$32, %edi
	callq	_Znam
	movq	%rax, _ZL21kUpdatePairStateIDSet(%rip)
	movl	$8, _ZL21kUpdatePairStateIDSet+12(%rip)
	movaps	.LCPI39_0(%rip), %xmm0  # xmm0 = [80,81,82,88]
	movups	%xmm0, (%rax)
	movaps	.LCPI39_1(%rip), %xmm0  # xmm0 = [89,90,87,0]
	movups	%xmm0, 16(%rax)
	movl	$7, _ZL21kUpdatePairStateIDSet+8(%rip)
	movl	$_ZN11CStringBaseIwED2Ev, %edi
	movl	$_ZL21kUpdatePairStateIDSet, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZL22kUpdatePairActionIDSet(%rip)
	movl	$20, %edi
	callq	_Znam
	movq	%rax, _ZL22kUpdatePairActionIDSet(%rip)
	movl	$5, _ZL22kUpdatePairActionIDSet+12(%rip)
	movaps	.LCPI39_2(%rip), %xmm0  # xmm0 = [48,49,50,51]
	movups	%xmm0, (%rax)
	movl	$0, 16(%rax)
	movl	$4, _ZL22kUpdatePairActionIDSet+8(%rip)
	movl	$_ZN11CStringBaseIwED2Ev, %edi
	movl	$_ZL22kUpdatePairActionIDSet, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end39:
	.size	_GLOBAL__sub_I_ArchiveCommandLine.ii, .Lfunc_end39-_GLOBAL__sub_I_ArchiveCommandLine.ii
	.cfi_endproc

	.type	k_OverwriteModes,@object # @k_OverwriteModes
	.data
	.globl	k_OverwriteModes
	.p2align	4
k_OverwriteModes:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.size	k_OverwriteModes, 16

	.type	_ZL21kUpdatePairStateIDSet,@object # @_ZL21kUpdatePairStateIDSet
	.local	_ZL21kUpdatePairStateIDSet
	.comm	_ZL21kUpdatePairStateIDSet,16,8
	.type	_ZL22kUpdatePairActionIDSet,@object # @_ZL22kUpdatePairActionIDSet
	.local	_ZL22kUpdatePairActionIDSet
	.comm	_ZL22kUpdatePairActionIDSet,16,8
	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.3:
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.size	.L.str.3, 8

	.type	kUpdateIgnoreItselfPostStringID,@object # @kUpdateIgnoreItselfPostStringID
	.data
	.globl	kUpdateIgnoreItselfPostStringID
	.p2align	3
kUpdateIgnoreItselfPostStringID:
	.quad	.L.str.3
	.size	kUpdateIgnoreItselfPostStringID, 8

	.type	_ZL12kSwitchForms,@object # @_ZL12kSwitchForms
	.local	_ZL12kSwitchForms
	.comm	_ZL12kSwitchForms,960,16
	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"Cannot use absolute pathnames for this command"
	.size	.L.str.4, 47

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.5:
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"cannot find archive"
	.size	.L.str.6, 20

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"there is no such archive"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"stdout mode and email mode cannot be combined"
	.size	.L.str.8, 46

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.9:
	.long	72                      # 0x48
	.long	0                       # 0x0
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
	.p2align	2
.L.str.10:
	.long	45                      # 0x2d
	.long	72                      # 0x48
	.long	69                      # 0x45
	.long	76                      # 0x4c
	.long	80                      # 0x50
	.long	0                       # 0x0
	.size	.L.str.10, 24

	.type	.L.str.11,@object       # @.str.11
	.p2align	2
.L.str.11:
	.long	66                      # 0x42
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.11, 12

	.type	.L.str.12,@object       # @.str.12
	.p2align	2
.L.str.12:
	.long	66                      # 0x42
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str.12, 12

	.type	.L.str.13,@object       # @.str.13
	.p2align	2
.L.str.13:
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.13, 8

	.type	.L.str.14,@object       # @.str.14
	.p2align	2
.L.str.14:
	.long	89                      # 0x59
	.long	0                       # 0x0
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
	.p2align	2
.L.str.15:
	.long	80                      # 0x50
	.long	0                       # 0x0
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
	.p2align	2
.L.str.16:
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.16, 8

	.type	.L.str.17,@object       # @.str.17
	.p2align	2
.L.str.17:
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.size	.L.str.17, 8

	.type	.L.str.18,@object       # @.str.18
	.p2align	2
.L.str.18:
	.long	87                      # 0x57
	.long	0                       # 0x0
	.size	.L.str.18, 8

	.type	.L.str.19,@object       # @.str.19
	.p2align	2
.L.str.19:
	.long	73                      # 0x49
	.long	0                       # 0x0
	.size	.L.str.19, 8

	.type	.L.str.20,@object       # @.str.20
	.p2align	2
.L.str.20:
	.long	88                      # 0x58
	.long	0                       # 0x0
	.size	.L.str.20, 8

	.type	.L.str.21,@object       # @.str.21
	.p2align	2
.L.str.21:
	.long	65                      # 0x41
	.long	73                      # 0x49
	.long	0                       # 0x0
	.size	.L.str.21, 12

	.type	.L.str.22,@object       # @.str.22
	.p2align	2
.L.str.22:
	.long	65                      # 0x41
	.long	88                      # 0x58
	.long	0                       # 0x0
	.size	.L.str.22, 12

	.type	.L.str.23,@object       # @.str.23
	.p2align	2
.L.str.23:
	.long	65                      # 0x41
	.long	78                      # 0x4e
	.long	0                       # 0x0
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
	.p2align	2
.L.str.24:
	.long	85                      # 0x55
	.long	0                       # 0x0
	.size	.L.str.24, 8

	.type	.L.str.25,@object       # @.str.25
	.p2align	2
.L.str.25:
	.long	86                      # 0x56
	.long	0                       # 0x0
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
	.p2align	2
.L.str.26:
	.long	82                      # 0x52
	.long	0                       # 0x0
	.size	.L.str.26, 8

	.type	.L.str.28,@object       # @.str.28
	.p2align	2
.L.str.28:
	.long	83                      # 0x53
	.long	70                      # 0x46
	.long	88                      # 0x58
	.long	0                       # 0x0
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
	.p2align	2
.L.str.29:
	.long	83                      # 0x53
	.long	73                      # 0x49
	.long	0                       # 0x0
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
	.p2align	2
.L.str.30:
	.long	83                      # 0x53
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
	.p2align	2
.L.str.31:
	.long	65                      # 0x41
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.size	.L.str.31, 12

	.type	.L.str.32,@object       # @.str.32
	.p2align	2
.L.str.32:
	.long	83                      # 0x53
	.long	69                      # 0x45
	.long	77                      # 0x4d
	.long	76                      # 0x4c
	.long	0                       # 0x0
	.size	.L.str.32, 20

	.type	.L.str.33,@object       # @.str.33
	.p2align	2
.L.str.33:
	.long	65                      # 0x41
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str.33, 12

	.type	.L.str.34,@object       # @.str.34
	.p2align	2
.L.str.34:
	.long	83                      # 0x53
	.long	76                      # 0x4c
	.long	80                      # 0x50
	.long	0                       # 0x0
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
	.p2align	2
.L.str.35:
	.long	76                      # 0x4c
	.long	0                       # 0x0
	.size	.L.str.35, 8

	.type	.L.str.36,@object       # @.str.36
	.p2align	2
.L.str.36:
	.long	83                      # 0x53
	.long	76                      # 0x4c
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.36, 16

	.type	.L.str.37,@object       # @.str.37
	.p2align	2
.L.str.37:
	.long	83                      # 0x53
	.long	83                      # 0x53
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.37, 16

	.type	.L.str.38,@object       # @.str.38
	.p2align	2
.L.str.38:
	.long	83                      # 0x53
	.long	67                      # 0x43
	.long	82                      # 0x52
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.38, 20

	.type	.L.str.39,@object       # @.str.39
	.p2align	2
.L.str.39:
	.long	48                      # 0x30
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.size	.L.str.39, 12

	.type	.L.str.40,@object       # @.str.40
	.p2align	2
.L.str.40:
	.long	97                      # 0x61
	.long	115                     # 0x73
	.long	117                     # 0x75
	.long	116                     # 0x74
	.long	0                       # 0x0
	.size	.L.str.40, 20

	.type	.L.str.41,@object       # @.str.41
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.41:
	.asciz	"Incorrect command line"
	.size	.L.str.41, 23

	.type	_ZL14g_CommandForms,@object # @_ZL14g_CommandForms
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL14g_CommandForms:
	.quad	.L.str.42
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.24
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.43
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.13
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.44
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.20
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.35
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.45
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.19
	.byte	0                       # 0x0
	.zero	7
	.size	_ZL14g_CommandForms, 144

	.type	.L.str.42,@object       # @.str.42
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.42:
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.42, 8

	.type	.L.str.43,@object       # @.str.43
	.p2align	2
.L.str.43:
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str.43, 8

	.type	.L.str.44,@object       # @.str.44
	.p2align	2
.L.str.44:
	.long	69                      # 0x45
	.long	0                       # 0x0
	.size	.L.str.44, 8

	.type	.L.str.45,@object       # @.str.45
	.p2align	2
.L.str.45:
	.long	66                      # 0x42
	.long	0                       # 0x0
	.size	.L.str.45, 8

	.type	.L.str.46,@object       # @.str.46
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.46:
	.asciz	"Cannot find listfile"
	.size	.L.str.46, 21

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Incorrect item in listfile.\nCheck charset encoding and -scs switch."
	.size	.L.str.47, 68

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Empty file path"
	.size	.L.str.48, 16

	.type	_ZTS28CArchiveCommandLineException,@object # @_ZTS28CArchiveCommandLineException
	.section	.rodata._ZTS28CArchiveCommandLineException,"aG",@progbits,_ZTS28CArchiveCommandLineException,comdat
	.weak	_ZTS28CArchiveCommandLineException
	.p2align	4
_ZTS28CArchiveCommandLineException:
	.asciz	"28CArchiveCommandLineException"
	.size	_ZTS28CArchiveCommandLineException, 31

	.type	_ZTS11CStringBaseIcE,@object # @_ZTS11CStringBaseIcE
	.section	.rodata._ZTS11CStringBaseIcE,"aG",@progbits,_ZTS11CStringBaseIcE,comdat
	.weak	_ZTS11CStringBaseIcE
	.p2align	4
_ZTS11CStringBaseIcE:
	.asciz	"11CStringBaseIcE"
	.size	_ZTS11CStringBaseIcE, 17

	.type	_ZTI11CStringBaseIcE,@object # @_ZTI11CStringBaseIcE
	.section	.rodata._ZTI11CStringBaseIcE,"aG",@progbits,_ZTI11CStringBaseIcE,comdat
	.weak	_ZTI11CStringBaseIcE
	.p2align	3
_ZTI11CStringBaseIcE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CStringBaseIcE
	.size	_ZTI11CStringBaseIcE, 16

	.type	_ZTI28CArchiveCommandLineException,@object # @_ZTI28CArchiveCommandLineException
	.section	.rodata._ZTI28CArchiveCommandLineException,"aG",@progbits,_ZTI28CArchiveCommandLineException,comdat
	.weak	_ZTI28CArchiveCommandLineException
	.p2align	4
_ZTI28CArchiveCommandLineException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28CArchiveCommandLineException
	.quad	_ZTI11CStringBaseIcE
	.size	_ZTI28CArchiveCommandLineException, 24

	.type	_ZTV13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTV13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard5CPairEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard5CPairEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard5CPairEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard5CPairEE:
	.asciz	"13CObjectVectorIN9NWildcard5CPairEE"
	.size	_ZTS13CObjectVectorIN9NWildcard5CPairEE, 36

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard5CPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard5CPairEE, 24

	.type	_ZTV13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard5CItemEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard5CItemEE:
	.asciz	"13CObjectVectorIN9NWildcard5CItemEE"
	.size	_ZTS13CObjectVectorIN9NWildcard5CItemEE, 36

	.type	_ZTI13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard5CItemEE, 24

	.type	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.asciz	"13CObjectVectorIN9NWildcard11CCensorNodeEE"
	.size	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE, 43

	.type	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE, 24

	.type	_ZTV13CObjectVectorI8CDirItemE,@object # @_ZTV13CObjectVectorI8CDirItemE
	.section	.rodata._ZTV13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTV13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTV13CObjectVectorI8CDirItemE
	.p2align	3
_ZTV13CObjectVectorI8CDirItemE:
	.quad	0
	.quad	_ZTI13CObjectVectorI8CDirItemE
	.quad	_ZN13CObjectVectorI8CDirItemED2Ev
	.quad	_ZN13CObjectVectorI8CDirItemED0Ev
	.quad	_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.size	_ZTV13CObjectVectorI8CDirItemE, 40

	.type	_ZTS13CObjectVectorI8CDirItemE,@object # @_ZTS13CObjectVectorI8CDirItemE
	.section	.rodata._ZTS13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTS13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTS13CObjectVectorI8CDirItemE
	.p2align	4
_ZTS13CObjectVectorI8CDirItemE:
	.asciz	"13CObjectVectorI8CDirItemE"
	.size	_ZTS13CObjectVectorI8CDirItemE, 27

	.type	_ZTI13CObjectVectorI8CDirItemE,@object # @_ZTI13CObjectVectorI8CDirItemE
	.section	.rodata._ZTI13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTI13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTI13CObjectVectorI8CDirItemE
	.p2align	4
_ZTI13CObjectVectorI8CDirItemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI8CDirItemE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI8CDirItemE, 24

	.type	.L.str.50,@object       # @.str.50
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.50:
	.asciz	"I won't write data and program's messages to same terminal"
	.size	.L.str.50, 59

	.type	_ZL35kUpdatePairStateNotSupportedActions,@object # @_ZL35kUpdatePairStateNotSupportedActions
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL35kUpdatePairStateNotSupportedActions:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.size	_ZL35kUpdatePairStateNotSupportedActions, 28

	.type	.L.str.52,@object       # @.str.52
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.52:
	.asciz	"I won't write compressed data to a terminal"
	.size	.L.str.52, 44

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ArchiveCommandLine.ii

	.globl	_ZN25CArchiveCommandLineParserC1Ev
	.type	_ZN25CArchiveCommandLineParserC1Ev,@function
_ZN25CArchiveCommandLineParserC1Ev = _ZN25CArchiveCommandLineParserC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
