	.text
	.file	"check_functions.bc"
	.globl	_Z11return_nullP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z11return_nullP7roadletP7vehicle9direction,@function
_Z11return_nullP7roadletP7vehicle9direction: # @_Z11return_nullP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_Z11return_nullP7roadletP7vehicle9direction, .Lfunc_end0-_Z11return_nullP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z8is_emptyP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z8is_emptyP7roadletP7vehicle9direction,@function
_Z8is_emptyP7roadletP7vehicle9direction: # @_Z8is_emptyP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	cmoveq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	_Z8is_emptyP7roadletP7vehicle9direction, .Lfunc_end1-_Z8is_emptyP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z14lane_switch_okP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z14lane_switch_okP7roadletP7vehicle9direction,@function
_Z14lane_switch_okP7roadletP7vehicle9direction: # @_Z14lane_switch_okP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 192
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	24(%rbx), %r15d
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$16, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$_ZSt4cout, %edi
	callq	_ZlsRSo7vehicle
	movq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$18, %ecx
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	movq	%rbx, %rdi
	callq	_ZlsRSo7roadlet
	movb	$10, 159(%rsp)
	leaq	159(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpq	$0, 8(%r14)
	je	.LBB2_2
# BB#1:
	xorl	%r14d, %r14d
	jmp	.LBB2_6
.LBB2_2:
	leal	4(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	leal	4(%r15,%rcx), %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	cltq
	imulq	$954437177, %rax, %rcx  # imm = 0x38E38E39
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$33, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,8), %ecx
	subl	%ecx, %eax
	cltq
	movq	16(%r14,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#3:
	cmpq	$0, 8(%rax)
	je	.LBB2_5
# BB#4:
	xorl	%r14d, %r14d
	jmp	.LBB2_6
.LBB2_5:                                # %.critedge
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2, %esi
	movl	$23, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.LBB2_6:
	movq	%r14, %rax
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_Z14lane_switch_okP7roadletP7vehicle9direction, .Lfunc_end2-_Z14lane_switch_okP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z6straitP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z6straitP7roadletP7vehicle9direction,@function
_Z6straitP7roadletP7vehicle9direction:  # @_Z6straitP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	jne	.LBB3_2
# BB#1:
	movl	24(%rsi), %edi
	movl	%edx, %esi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB3_3
.LBB3_2:                                # %.critedge
	xorl	%ebx, %ebx
.LBB3_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_Z6straitP7roadletP7vehicle9direction, .Lfunc_end3-_Z6straitP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z14strait_or_leftP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z14strait_or_leftP7roadletP7vehicle9direction,@function
_Z14strait_or_leftP7roadletP7vehicle9direction: # @_Z14strait_or_leftP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	je	.LBB4_1
.LBB4_3:                                # %.critedge
	xorl	%ebx, %ebx
	jmp	.LBB4_4
.LBB4_1:
	movl	24(%rbp), %edi
	movl	%r14d, %esi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB4_4
# BB#2:
	movl	24(%rbp), %eax
	leal	6(%rax), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	leal	6(%rax,%rdx), %eax
	andl	$-8, %eax
	subl	%eax, %ecx
	movslq	%ecx, %rdi
	imulq	$954437177, %rdi, %rax  # imm = 0x38E38E39
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$33, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	subl	%eax, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r14d, %esi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	je	.LBB4_3
.LBB4_4:                                # %.critedge1
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_Z14strait_or_leftP7roadletP7vehicle9direction, .Lfunc_end4-_Z14strait_or_leftP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z15strait_or_rightP7roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z15strait_or_rightP7roadletP7vehicle9direction,@function
_Z15strait_or_rightP7roadletP7vehicle9direction: # @_Z15strait_or_rightP7roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	je	.LBB5_1
.LBB5_3:                                # %.critedge
	xorl	%ebx, %ebx
	jmp	.LBB5_4
.LBB5_1:
	movl	24(%rbp), %edi
	movl	%r14d, %esi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB5_4
# BB#2:
	movl	24(%rbp), %eax
	leal	2(%rax), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	leal	2(%rax,%rdx), %eax
	andl	$-8, %eax
	subl	%eax, %ecx
	movslq	%ecx, %rdi
	imulq	$954437177, %rdi, %rax  # imm = 0x38E38E39
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$33, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	subl	%eax, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r14d, %esi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	je	.LBB5_3
.LBB5_4:                                # %.critedge1
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_Z15strait_or_rightP7roadletP7vehicle9direction, .Lfunc_end5-_Z15strait_or_rightP7roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z11green_lightP20intersection_roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z11green_lightP20intersection_roadletP7vehicle9direction,@function
_Z11green_lightP20intersection_roadletP7vehicle9direction: # @_Z11green_lightP20intersection_roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movl	N(%rip), %esi
	movl	%ebp, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB6_2
# BB#1:
	movl	S(%rip), %esi
	movl	%ebp, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	je	.LBB6_4
.LBB6_2:
	cmpq	$0, 8(%rbx)
	jne	.LBB6_6
# BB#3:
	movq	144(%rbx), %rax
	cmpl	$2, 8(%rax)
	jne	.LBB6_6
	jmp	.LBB6_7
.LBB6_4:
	cmpq	$0, 8(%rbx)
	jne	.LBB6_6
# BB#5:
	movq	144(%rbx), %rax
	cmpl	$0, 8(%rax)
	je	.LBB6_7
.LBB6_6:
	xorl	%ebx, %ebx
.LBB6_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z11green_lightP20intersection_roadletP7vehicle9direction, .Lfunc_end6-_Z11green_lightP20intersection_roadletP7vehicle9direction
	.cfi_endproc

	.globl	_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction
	.p2align	4, 0x90
	.type	_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction,@function
_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction: # @_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 80
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$_ZSt4cout, %edi
	movl	$.L.str.3, %esi
	movl	$22, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	N(%rip), %esi
	movl	%r15d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB7_2
# BB#1:
	movl	S(%rip), %esi
	movl	%r15d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	je	.LBB7_3
.LBB7_2:
	movq	144(%rbx), %rax
	movl	8(%rax), %ecx
	movl	%ecx, %edx
	orl	$1, %edx
	xorl	%eax, %eax
	cmpl	$1, %edx
	sete	%al
	cmpl	$2, %ecx
.LBB7_4:
	sete	%cl
	movzbl	%cl, %r12d
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB7_7
# BB#5:
	leal	2(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	leal	2(%r15,%rcx), %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	cltq
	imulq	$954437177, %rax, %rcx  # imm = 0x38E38E39
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$33, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,8), %ecx
	subl	%ecx, %eax
	movslq	%eax, %rcx
	cmpq	$0, 16(%rbx,%rcx,8)
	je	.LBB7_7
# BB#6:
	movl	%eax, 28(%r14)
	movl	$_ZSt4cout, %edi
	movl	$.L.str.4, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$_ZSt4cout, %edi
	callq	_ZlsRSo7vehicle
	movq	%rax, %r15
	movl	$.L.str.5, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movslq	28(%r14), %rsi
	imulq	$954437177, %rsi, %rax  # imm = 0x38E38E39
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$33, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	subl	%eax, %esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZlsRSo9direction
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$1, %ebp
.LBB7_7:
	testl	%r12d, %r12d
	je	.LBB7_9
# BB#8:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.7, %esi
	movl	$7, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.LBB7_9:
	testl	%ebp, %ebp
	je	.LBB7_11
# BB#10:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.8, %esi
	movl	$13, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.LBB7_11:
	xorl	%eax, %eax
	cmpq	$0, 8(%rbx)
	cmovneq	%rax, %rbx
	orl	%r12d, %ebp
	cmoveq	%rax, %rbx
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_3:
	movq	144(%rbx), %rax
	movl	8(%rax), %ecx
	movl	%ecx, %edx
	orl	$1, %edx
	xorl	%eax, %eax
	cmpl	$3, %edx
	sete	%al
	testl	%ecx, %ecx
	jmp	.LBB7_4
.Lfunc_end7:
	.size	_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction, .Lfunc_end7-_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_check_functions.ii,@function
_GLOBAL__sub_I_check_functions.ii:      # @_GLOBAL__sub_I_check_functions.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end8:
	.size	_GLOBAL__sub_I_check_functions.ii, .Lfunc_end8-_GLOBAL__sub_I_check_functions.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"lane switch for "
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" at "
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"lane switch said true \n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"green or right on red\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"plan for "
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" is "
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"green!\n"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"right on red\n"
	.size	.L.str.8, 14

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_check_functions.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
