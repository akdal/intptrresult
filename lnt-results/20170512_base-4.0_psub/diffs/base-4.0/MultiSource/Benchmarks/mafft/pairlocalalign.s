	.text
	.file	"pairlocalalign.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4652007308841189376     # double 1000
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
.LCPI0_2:
	.quad	4603579539098121011     # double 0.59999999999999998
.LCPI0_3:
	.quad	-4620693217682128896    # double -0.5
	.text
	.globl	arguments
	.p2align	4, 0x90
	.type	arguments,@function
arguments:                              # @arguments
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %r14d
	movb	$0, foldalignopt(%rip)
	movq	$0, inputfile(%rip)
	movl	$0, fftkeika(%rip)
	movl	$-1000, pslocal(%rip)   # imm = 0xFC18
	movl	$0, constraint(%rip)
	movl	$62, nblosum(%rip)
	movl	$0, fmodel(%rip)
	movl	$0, calledByXced(%rip)
	movl	$0, devide(%rip)
	movb	$0, use_fft(%rip)
	movl	$1, fftscore(%rip)
	movl	$0, fftRepeatStop(%rip)
	movl	$0, fftNoAnchStop(%rip)
	movl	$3, weight(%rip)
	movl	$1, utree(%rip)
	movl	$1, tbutree(%rip)
	movl	$0, refine(%rip)
	movl	$1, check(%rip)
	movq	$0, cut(%rip)
	movl	$0, disp(%rip)
	movl	$1, outgap(%rip)
	movb	$65, alg(%rip)
	movl	$0, mix(%rip)
	movl	$0, tbitr(%rip)
	movl	$5, scmtd(%rip)
	movl	$0, tbweight(%rip)
	movl	$3, tbrweight(%rip)
	movl	$0, checkC(%rip)
	movl	$120, treemethod(%rip)
	movl	$0, contin(%rip)
	movl	$1, scoremtx(%rip)
	movl	$0, kobetsubunkatsu(%rip)
	movl	$0, divpairscore(%rip)
	movb	$0, out_align_instead_of_hat3(%rip)
	movl	$100009, dorp(%rip)     # imm = 0x186A9
	movl	$100009, ppenalty(%rip) # imm = 0x186A9
	movl	$100009, ppenalty_OP(%rip) # imm = 0x186A9
	movl	$100009, ppenalty_ex(%rip) # imm = 0x186A9
	movl	$100009, ppenalty_EX(%rip) # imm = 0x186A9
	movl	$100009, poffset(%rip)  # imm = 0x186A9
	movl	$100009, kimuraR(%rip)  # imm = 0x186A9
	movl	$100009, pamN(%rip)     # imm = 0x186A9
	movl	$981668463, geta2(%rip) # imm = 0x3A83126F
	movl	$100009, fftWinSize(%rip) # imm = 0x186A9
	movl	$100009, fftThreshold(%rip) # imm = 0x186A9
	movl	$100009, RNAppenalty(%rip) # imm = 0x186A9
	movl	$100009, RNApthr(%rip)  # imm = 0x186A9
	cmpl	$2, %r14d
	jl	.LBB0_1
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	leaq	8(%rbp), %rbx
	movq	8(%rbp), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB0_52
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	decl	%r14d
	leaq	1(%rcx), %rax
	movq	%rax, (%rbx)
	movb	1(%rcx), %cl
	testb	%cl, %cl
	je	.LBB0_51
.LBB0_4:                                # %.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	%cl, %edx
	leal	-65(%rdx), %ecx
	cmpl	$57, %ecx
	ja	.LBB0_59
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=2
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_40:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$65, alg(%rip)
	jmp	.LBB0_60
.LBB0_33:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$66, alg(%rip)
	jmp	.LBB0_60
.LBB0_42:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$67, alg(%rip)
	jmp	.LBB0_60
.LBB0_24:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$100, dorp(%rip)
	jmp	.LBB0_60
.LBB0_43:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$1, use_fft(%rip)
	jmp	.LBB0_60
.LBB0_35:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$72, alg(%rip)
	jmp	.LBB0_60
.LBB0_46:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$0, tbutree(%rip)
	jmp	.LBB0_60
.LBB0_39:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$75, alg(%rip)
	jmp	.LBB0_60
.LBB0_31:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$76, alg(%rip)
	jmp	.LBB0_60
.LBB0_36:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$77, alg(%rip)
	jmp	.LBB0_60
.LBB0_38:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$78, alg(%rip)
	jmp	.LBB0_60
.LBB0_25:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$112, dorp(%rip)
	jmp	.LBB0_60
.LBB0_27:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$1, calledByXced(%rip)
	jmp	.LBB0_60
.LBB0_37:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$82, alg(%rip)
	jmp	.LBB0_60
.LBB0_29:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$83, alg(%rip)
	jmp	.LBB0_60
.LBB0_34:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$84, alg(%rip)
	jmp	.LBB0_60
.LBB0_41:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$86, alg(%rip)
	jmp	.LBB0_60
.LBB0_58:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$1, checkC(%rip)
	jmp	.LBB0_60
.LBB0_22:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$1, fmodel(%rip)
	jmp	.LBB0_60
.LBB0_26:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$0, fftscore(%rip)
	jmp	.LBB0_60
.LBB0_21:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$1, out_align_instead_of_hat3(%rip)
	jmp	.LBB0_60
.LBB0_23:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$-1, fmodel(%rip)
	jmp	.LBB0_60
.LBB0_32:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$115, alg(%rip)
	jmp	.LBB0_60
.LBB0_30:                               #   in Loop: Header=BB0_4 Depth=2
	movb	$116, alg(%rip)
	jmp	.LBB0_60
.LBB0_44:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$3, tbrweight(%rip)
	jmp	.LBB0_60
.LBB0_28:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$1, disp(%rip)
	jmp	.LBB0_60
.LBB0_45:                               #   in Loop: Header=BB0_4 Depth=2
	movl	$1, divpairscore(%rip)
	jmp	.LBB0_60
.LBB0_59:                               #   in Loop: Header=BB0_4 Depth=2
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	(%rbx), %rax
	.p2align	4, 0x90
.LBB0_60:                               # %.backedge
                                        #   in Loop: Header=BB0_4 Depth=2
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movzbl	1(%rax), %ecx
	testb	%cl, %cl
	movq	%rdx, %rax
	jne	.LBB0_4
	jmp	.LBB0_51
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, ppenalty_EX(%rip)
	jmp	.LBB0_50
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, ppenalty_OP(%rip)
	jmp	.LBB0_50
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, nblosum(%rip)
	movl	$1, scoremtx(%rip)
	jmp	.LBB0_50
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, ppenalty(%rip)
	jmp	.LBB0_50
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, ppenalty_ex(%rip)
	jmp	.LBB0_50
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, poffset(%rip)
	jmp	.LBB0_50
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, kimuraR(%rip)
	jmp	.LBB0_50
.LBB0_18:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	callq	strtod
	mulsd	.LCPI0_0(%rip), %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, ppslocal(%rip)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, pslocal(%rip)
	jmp	.LBB0_50
.LBB0_49:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, fftWinSize(%rip)
	jmp	.LBB0_50
.LBB0_48:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, fftThreshold(%rip)
	jmp	.LBB0_50
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdx
	addq	$16, %rbp
	movq	%rdx, whereispairalign(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	jmp	.LBB0_7
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdx
	addq	$16, %rbp
	movq	%rdx, inputfile(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	jmp	.LBB0_7
.LBB0_15:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	%ecx, pamN(%rip)
	movl	$0, scoremtx(%rip)
	movl	$201, TMorJTT(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	jmp	.LBB0_16
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdi
	addq	$16, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	%ecx, pamN(%rip)
	movl	$0, scoremtx(%rip)
	movl	$202, TMorJTT(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
.LBB0_16:                               # %.backedge46
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	jmp	.LBB0_50
.LBB0_47:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$foldalignopt, %edi
	callq	strlen
	movw	$32, foldalignopt(%rax)
	movq	16(%rbp), %rsi
	addq	$16, %rbp
	movl	$foldalignopt, %edi
	callq	strcat
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$foldalignopt, %edx
	jmp	.LBB0_7
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rbp), %rdx
	addq	$16, %rbp
	movq	%rdx, laraparams(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
.LBB0_7:                                # %.backedge46
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	callq	fprintf
	.p2align	4, 0x90
.LBB0_50:                               # %.backedge46
                                        #   in Loop: Header=BB0_2 Depth=1
	decl	%r14d
	movq	%rbp, %rbx
.LBB0_51:                               # %.backedge46
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$1, %r14d
	movq	%rbx, %rbp
	jg	.LBB0_2
	jmp	.LBB0_52
.LBB0_1:
	movq	%rbp, %rbx
.LBB0_52:                               # %.critedge
	cmpl	$1, %r14d
	je	.LBB0_55
# BB#53:                                # %.critedge
	cmpl	$2, %r14d
	jne	.LBB0_61
# BB#54:                                # %.thread
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	callq	strtod
	movsd	%xmm0, cut(%rip)
.LBB0_55:
	movl	outgap(%rip), %eax
	cmpl	$1, tbitr(%rip)
	jne	.LBB0_63
# BB#56:
	testl	%eax, %eax
	je	.LBB0_57
.LBB0_63:
	testl	%eax, %eax
	jne	.LBB0_66
# BB#64:
	cmpb	$67, alg(%rip)
	je	.LBB0_65
.LBB0_66:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_61:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$29, %esi
	jmp	.LBB0_62
.LBB0_57:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$32, %esi
	jmp	.LBB0_62
.LBB0_65:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$27, %esi
.LBB0_62:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	arguments, .Lfunc_end0-arguments
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_40
	.quad	.LBB0_33
	.quad	.LBB0_42
	.quad	.LBB0_24
	.quad	.LBB0_11
	.quad	.LBB0_43
	.quad	.LBB0_59
	.quad	.LBB0_35
	.quad	.LBB0_59
	.quad	.LBB0_46
	.quad	.LBB0_39
	.quad	.LBB0_31
	.quad	.LBB0_36
	.quad	.LBB0_38
	.quad	.LBB0_10
	.quad	.LBB0_25
	.quad	.LBB0_27
	.quad	.LBB0_37
	.quad	.LBB0_29
	.quad	.LBB0_34
	.quad	.LBB0_59
	.quad	.LBB0_41
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_58
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_59
	.quad	.LBB0_22
	.quad	.LBB0_14
	.quad	.LBB0_59
	.quad	.LBB0_19
	.quad	.LBB0_26
	.quad	.LBB0_8
	.quad	.LBB0_9
	.quad	.LBB0_12
	.quad	.LBB0_6
	.quad	.LBB0_15
	.quad	.LBB0_13
	.quad	.LBB0_18
	.quad	.LBB0_17
	.quad	.LBB0_21
	.quad	.LBB0_47
	.quad	.LBB0_20
	.quad	.LBB0_59
	.quad	.LBB0_23
	.quad	.LBB0_32
	.quad	.LBB0_30
	.quad	.LBB0_59
	.quad	.LBB0_44
	.quad	.LBB0_49
	.quad	.LBB0_28
	.quad	.LBB0_45
	.quad	.LBB0_48

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	45                      # 0x2d
	.long	45                      # 0x2d
	.long	45                      # 0x2d
	.long	45                      # 0x2d
.LCPI1_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	countamino
	.p2align	4, 0x90
	.type	countamino,@function
countamino:                             # @countamino
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%rsi), %ecx
	incq	%rcx
	xorl	%eax, %eax
	cmpq	$8, %rcx
	jb	.LBB1_12
# BB#3:                                 # %min.iters.checked
	movabsq	$8589934584, %r9        # imm = 0x1FFFFFFF8
	movq	%rcx, %r8
	andq	%r9, %r8
	andq	%rcx, %r9
	je	.LBB1_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r9), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB1_5
# BB#6:                                 # %vector.body.prol
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movd	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movdqa	.LCPI1_0(%rip), %xmm2   # xmm2 = [45,45,45,45]
	pcmpeqd	%xmm2, %xmm0
	movdqa	.LCPI1_1(%rip), %xmm3   # xmm3 = [1,1,1,1]
	pandn	%xmm3, %xmm0
	pcmpeqd	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	movl	$8, %edx
	testq	%rax, %rax
	jne	.LBB1_8
	jmp	.LBB1_10
.LBB1_1:
	xorl	%eax, %eax
	retq
.LBB1_5:
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	je	.LBB1_10
.LBB1_8:                                # %vector.body.preheader.new
	movq	%r9, %rax
	subq	%rdx, %rax
	leaq	12(%rdi,%rdx), %rdx
	pxor	%xmm2, %xmm2
	movdqa	.LCPI1_0(%rip), %xmm3   # xmm3 = [45,45,45,45]
	movdqa	.LCPI1_1(%rip), %xmm4   # xmm4 = [1,1,1,1]
	.p2align	4, 0x90
.LBB1_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rdx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	movd	-8(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1],xmm6[2],xmm2[2],xmm6[3],xmm2[3],xmm6[4],xmm2[4],xmm6[5],xmm2[5],xmm6[6],xmm2[6],xmm6[7],xmm2[7]
	punpcklwd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1],xmm6[2],xmm2[2],xmm6[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm5
	pandn	%xmm4, %xmm5
	pcmpeqd	%xmm3, %xmm6
	pandn	%xmm4, %xmm6
	paddd	%xmm0, %xmm5
	paddd	%xmm1, %xmm6
	movd	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movd	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm0
	pandn	%xmm4, %xmm0
	pcmpeqd	%xmm3, %xmm1
	pandn	%xmm4, %xmm1
	paddd	%xmm5, %xmm0
	paddd	%xmm6, %xmm1
	addq	$16, %rdx
	addq	$-16, %rax
	jne	.LBB1_9
.LBB1_10:                               # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	cmpq	%r9, %rcx
	je	.LBB1_13
# BB#11:
	subl	%r8d, %esi
	addq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%ecx, %ecx
	cmpb	$45, (%rdi)
	leaq	1(%rdi), %rdi
	setne	%cl
	addl	%ecx, %eax
	decl	%esi
	jne	.LBB1_12
.LBB1_13:                               # %._crit_edge
	retq
.Lfunc_end1:
	.size	countamino, .Lfunc_end1-countamino
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4652007308841189376     # double 1000
.LCPI2_2:
	.quad	4611686018427387904     # double 2
.LCPI2_3:
	.quad	-4556649414143246336    # double -9999
.LCPI2_4:
	.quad	4607182418800017408     # double 1
.LCPI2_5:
	.quad	-4616189618054758400    # double -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Lcfi12:
	.cfi_def_cfa_offset 1216
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	callq	arguments
	movq	inputfile(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_3
# BB#1:
	movl	$.L.str.11, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_4
# BB#2:
	movq	stderr(%rip), %rdi
	movq	inputfile(%rip), %rdx
	movl	$.L.str.12, %esi
	jmp	.LBB2_300
.LBB2_3:
	movq	stdin(%rip), %rbx
.LBB2_4:
	movq	%rbx, %rdi
	callq	getnumlen
	movq	%rbx, %rdi
	callq	rewind
	movl	njob(%rip), %ecx
	cmpl	$1, %ecx
	jle	.LBB2_301
# BB#5:
	cmpl	$50001, %ecx            # imm = 0xC351
	jge	.LBB2_302
# BB#6:
	movl	nlenmax(%rip), %eax
	leal	1(%rax,%rax,8), %esi
	movl	%ecx, %edi
	callq	AllocateCharMtx
	movq	%rax, main.seq(%rip)
	movl	njob(%rip), %edi
	movl	nlenmax(%rip), %eax
	leal	1(%rax,%rax,8), %esi
	callq	AllocateCharMtx
	movq	%rax, main.aseq(%rip)
	movl	njob(%rip), %edi
	movl	nlenmax(%rip), %eax
	leal	1(%rax,%rax,8), %esi
	callq	AllocateCharMtx
	movq	%rax, main.bseq(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, main.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, main.mseq2(%rip)
	movslq	nlenmax(%rip), %r14
	movl	njob(%rip), %edi
	callq	AllocateDoubleVec
	movq	%rax, main.eff(%rip)
	movq	main.seq(%rip), %rcx
	movl	$main.name, %esi
	movl	$main.nlen, %edx
	movq	%rbx, %rdi
	callq	readData
	movq	%rbx, %rdi
	callq	fclose
	movl	njob(%rip), %edi
	movq	main.seq(%rip), %rsi
	callq	constants
	callq	initSignalSM
	callq	initFiles
	movq	trap_g(%rip), %rbx
	cmpl	$100, dorp(%rip)
	jne	.LBB2_9
# BB#7:
	movl	$.L.str.22, %edi
.LBB2_8:
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	jmp	.LBB2_16
.LBB2_9:
	movl	scoremtx(%rip), %eax
	cmpl	$2, %eax
	je	.LBB2_13
# BB#10:
	cmpl	$1, %eax
	je	.LBB2_14
# BB#11:
	testl	%eax, %eax
	jne	.LBB2_16
# BB#12:
	movl	pamN(%rip), %edx
	movl	$.L.str.23, %esi
	jmp	.LBB2_15
.LBB2_13:
	movl	$.L.str.25, %edi
	jmp	.LBB2_8
.LBB2_14:
	movl	nblosum(%rip), %edx
	movl	$.L.str.24, %esi
.LBB2_15:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
.LBB2_16:
	movq	stderr(%rip), %rdi
	cvtsi2sdl	ppenalty(%rip), %xmm0
	movsd	.LCPI2_0(%rip), %xmm3   # xmm3 = mem[0],zero
	divsd	%xmm3, %xmm0
	cvtsi2sdl	ppenalty_ex(%rip), %xmm1
	divsd	%xmm3, %xmm1
	cvtsi2sdl	poffset(%rip), %xmm2
	divsd	%xmm3, %xmm2
	movl	$.L.str.26, %esi
	movb	$3, %al
	callq	fprintf
	cmpb	$0, use_fft(%rip)
	je	.LBB2_18
# BB#17:
	movl	$.L.str.27, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB2_18:
	movl	$.L.str.28, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	tbrweight(%rip), %eax
	cmpl	$3, %eax
	je	.LBB2_21
# BB#19:
	testl	%eax, %eax
	jne	.LBB2_23
# BB#20:
	movl	$.L.str.29, %edi
	movl	$11, %esi
	jmp	.LBB2_22
.LBB2_21:
	movl	$.L.str.30, %edi
	movl	$24, %esi
.LBB2_22:
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB2_23:
	movl	tbweight(%rip), %eax
	orl	tbitr(%rip), %eax
	je	.LBB2_33
# BB#24:
	movl	$.L.str.31, %edi
	movl	$21, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	tbitr(%rip), %eax
	movl	tbrweight(%rip), %ecx
	testl	%eax, %eax
	je	.LBB2_27
# BB#25:
	testl	%ecx, %ecx
	jne	.LBB2_27
# BB#26:
	movl	$.L.str.32, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	tbitr(%rip), %eax
	movl	tbrweight(%rip), %ecx
.LBB2_27:
	cmpl	$3, %ecx
	jne	.LBB2_30
# BB#28:
	testl	%eax, %eax
	je	.LBB2_30
# BB#29:
	movl	$.L.str.33, %edi
	movl	$21, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB2_30:
	cmpl	$0, tbweight(%rip)
	je	.LBB2_32
# BB#31:
	movl	$.L.str.34, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB2_32:
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
.LBB2_33:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	ppenalty(%rip), %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm3
	divsd	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	ppenalty_ex(%rip), %xmm1
	divsd	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	poffset(%rip), %xmm2
	divsd	%xmm3, %xmm2
	movl	$.L.str.26, %esi
	movb	$3, %al
	movq	%rbx, %rdi
	callq	fprintf
	movb	alg(%rip), %al
	addb	$-65, %al
	cmpb	$32, %al
	ja	.LBB2_36
# BB#34:
	movzbl	%al, %eax
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_35:
	movl	$.L.str.37, %edi
	movl	$13, %esi
	jmp	.LBB2_41
.LBB2_36:
	movl	$.L.str.40, %edi
	movl	$18, %esi
	jmp	.LBB2_41
.LBB2_37:
	movl	$.L.str.39, %edi
	movl	$15, %esi
	jmp	.LBB2_41
.LBB2_38:
	movl	$.L.str.38, %edi
	jmp	.LBB2_40
.LBB2_39:
	movl	$.L.str.36, %edi
.LBB2_40:
	movl	$12, %esi
.LBB2_41:
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpb	$0, use_fft(%rip)
	je	.LBB2_44
# BB#42:
	movl	$.L.str.27, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$100, dorp(%rip)
	jne	.LBB2_45
# BB#43:
	movl	$.L.str.41, %edi
	movl	$22, %esi
	jmp	.LBB2_48
.LBB2_44:
	movl	$.L.str.46, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	jmp	.LBB2_49
.LBB2_45:
	cmpl	$0, fftscore(%rip)
	je	.LBB2_47
# BB#46:
	movl	$.L.str.42, %edi
	movl	$28, %esi
	jmp	.LBB2_48
.LBB2_47:
	movl	$.L.str.43, %edi
	movl	$23, %esi
.LBB2_48:
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	fftThreshold(%rip), %edx
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	fftWinSize(%rip), %edx
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
.LBB2_49:                               # %WriteOptions.exit
	movq	%rbx, %rdi
	callq	fflush
	movq	main.seq(%rip), %rdi
	callq	seqcheck
	testb	%al, %al
	jne	.LBB2_303
# BB#50:                                # %.preheader26
	movslq	njob(%rip), %rbp
	testq	%rbp, %rbp
	movq	%r14, 88(%rsp)          # 8-byte Spill
	jle	.LBB2_62
# BB#51:                                # %.lr.ph38
	movq	main.eff(%rip), %rax
	cmpl	$3, %ebp
	jbe	.LBB2_56
# BB#52:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-4, %rcx
	je	.LBB2_56
# BB#53:                                # %vector.body.preheader
	leaq	-4(%rcx), %rdx
	movl	%edx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB2_130
# BB#54:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%esi, %esi
	movapd	.LCPI2_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB2_55:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, (%rax,%rsi,8)
	movupd	%xmm0, 16(%rax,%rsi,8)
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB2_55
	jmp	.LBB2_131
.LBB2_56:
	xorl	%ecx, %ecx
.LBB2_57:                               # %scalar.ph.preheader
	movabsq	$4607182418800017408, %rdx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB2_58:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB2_58
.LBB2_59:                               # %.preheader
	movq	main.bseq(%rip), %rax
	testl	%ebp, %ebp
	jle	.LBB2_63
# BB#60:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_61:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbx,8), %rdi
	movq	main.seq(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	callq	gappick0
	incq	%rbx
	movslq	njob(%rip), %rbp
	movq	main.bseq(%rip), %rax
	cmpq	%rbp, %rbx
	jl	.LBB2_61
	jmp	.LBB2_63
.LBB2_62:                               # %.preheader.thread
	movq	main.bseq(%rip), %rax
.LBB2_63:                               # %._crit_edge
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	main.aseq(%rip), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	main.mseq1(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	main.mseq2(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	main.eff(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movslq	%ebp, %rbx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB2_69
# BB#64:                                # %.lr.ph92.i.preheader
	xorl	%ebx, %ebx
	movabsq	$-4616189618054758400, %r15 # imm = 0xBFF0000000000000
	.p2align	4, 0x90
.LBB2_65:                               # %.lr.ph92.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_67 Depth 2
	movslq	%ebp, %r14
	movl	$80, %esi
	movq	%r14, %rdi
	callq	calloc
	pcmpeqd	%xmm0, %xmm0
	movq	%rax, (%r12,%rbx,8)
	testl	%r14d, %r14d
	jle	.LBB2_68
# BB#66:                                # %.lr.ph88.i.preheader
                                        #   in Loop: Header=BB2_65 Depth=1
	movl	$24, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_67:                               # %.lr.ph88.i
                                        #   Parent Loop BB2_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, (%rax,%rcx)
	movq	%r15, 16(%rax,%rcx)
	movq	$0, -16(%rax,%rcx)
	movq	(%r12,%rbx,8), %rax
	movl	$0, -24(%rax,%rcx)
	incq	%rdx
	movslq	njob(%rip), %rbp
	addq	$80, %rcx
	cmpq	%rbp, %rdx
	jl	.LBB2_67
.LBB2_68:                               # %._crit_edge89.i
                                        #   in Loop: Header=BB2_65 Depth=1
	incq	%rbx
	movslq	%ebp, %rax
	cmpq	%rax, %rbx
	jl	.LBB2_65
.LBB2_69:                               # %._crit_edge93.i
	cmpq	$0, pairalign.effarr1(%rip)
	jne	.LBB2_71
# BB#70:
	movl	%ebp, %edi
	movl	%ebp, %esi
	callq	AllocateDoubleMtx
	movq	%rax, pairalign.distancemtx(%rip)
	movl	njob(%rip), %edi
	callq	AllocateDoubleVec
	movq	%rax, pairalign.effarr1(%rip)
	movl	njob(%rip), %edi
	callq	AllocateDoubleVec
	movq	%rax, pairalign.effarr2(%rip)
	movl	$150, %edi
	callq	AllocateCharVec
	movq	%rax, pairalign.indication1(%rip)
	movl	$150, %edi
	callq	AllocateCharVec
	movq	%rax, pairalign.indication2(%rip)
	movl	njob(%rip), %edi
	movl	%edi, %esi
	callq	AllocateCharMtx
	movq	%rax, pairalign.pair(%rip)
	movl	njob(%rip), %ebp
.LBB2_71:                               # %.preheader28.i
	testl	%ebp, %ebp
	movq	40(%rsp), %r14          # 8-byte Reload
	jle	.LBB2_80
# BB#72:                                # %.preheader27.lr.ph.i
	movq	pairalign.pair(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_73:                               # %.preheader27.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_75 Depth 2
	testl	%ebp, %ebp
	jle	.LBB2_76
# BB#74:                                # %.lr.ph83.i
                                        #   in Loop: Header=BB2_73 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_75:                               #   Parent Loop BB2_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx,8), %rsi
	movb	$0, (%rsi,%rdx)
	incq	%rdx
	movslq	njob(%rip), %rbp
	cmpq	%rbp, %rdx
	jl	.LBB2_75
.LBB2_76:                               # %._crit_edge84.i
                                        #   in Loop: Header=BB2_73 Depth=1
	incq	%rcx
	movslq	%ebp, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB2_73
# BB#77:                                # %.preheader26.i
	testl	%ebp, %ebp
	jle	.LBB2_80
# BB#78:                                # %.lr.ph80.i.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_79:                               # %.lr.ph80.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdx
	movb	$1, (%rdx,%rcx)
	incq	%rcx
	movslq	njob(%rip), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB2_79
.LBB2_80:                               # %._crit_edge81.i
	movb	alg(%rip), %al
	cmpb	$72, %al
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB2_135
# BB#81:
	cmpb	$66, %al
	je	.LBB2_155
.LBB2_82:
	cmpb	$84, %al
	je	.LBB2_156
.LBB2_83:
	cmpb	$115, %al
	movq	%r12, 80(%rsp)          # 8-byte Spill
	jne	.LBB2_94
# BB#84:
	movq	stderr(%rip), %rcx
	movl	$.L.str.53, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movslq	njob(%rip), %rbx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$.L.str.72, %edi
	movl	$.L.str.11, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_305
# BB#85:                                # %.preheader.i4.i
	testl	%ebx, %ebx
	jle	.LBB2_93
# BB#86:                                # %.lr.ph.i5.i
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_87:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_90 Depth 2
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	jne	.LBB2_297
# BB#88:                                #   in Loop: Header=BB2_87 Depth=1
	movl	$62, %edi
	movq	%rbp, %rsi
	callq	ungetc
	movl	$999, %esi              # imm = 0x3E7
	leaq	160(%rsp), %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$16, %r14d
	xorl	%r12d, %r12d
	jmp	.LBB2_90
	.p2align	4, 0x90
.LBB2_89:                               #   in Loop: Header=BB2_90 Depth=2
	movl	$999, %esi              # imm = 0x3E7
	leaq	160(%rsp), %r13
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rbp,%rbx,8), %rdi
	movq	%r14, %rsi
	callq	realloc
	movq	%rax, %r15
	movq	%r15, (%rbp,%rbx,8)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	$100, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, -16(%r15,%r14)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %r12
	addq	$8, %r14
.LBB2_90:                               #   Parent Loop BB2_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	movl	%r15d, %edi
	movq	%rbp, %rsi
	callq	ungetc
	cmpl	$-1, %r15d
	je	.LBB2_92
# BB#91:                                #   in Loop: Header=BB2_90 Depth=2
	cmpl	$62, %r15d
	jne	.LBB2_89
.LBB2_92:                               # %readhat4.exit.i.i
                                        #   in Loop: Header=BB2_87 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbx,%rbp,8), %rdi
	movabsq	$8589934592, %rax       # imm = 0x200000000
	leaq	(%r12,%rax), %rsi
	sarq	$29, %rsi
	callq	realloc
	movq	%rax, (%rbx,%rbp,8)
	sarq	$29, %r12
	movq	$0, (%rax,%r12)
	incq	%rbp
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	cmpq	%rbx, %rbp
	movq	32(%rsp), %rbp          # 8-byte Reload
	jl	.LBB2_87
.LBB2_93:                               # %preparebpp.exit.i
	movq	%rbp, %rdi
	callq	fclose
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.54, %edi
	movl	$71, %esi
	movl	$1, %edx
	callq	fwrite
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB2_95
.LBB2_94:
                                        # implicit-def: %RAX
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB2_95:
	movslq	njob(%rip), %rcx
	cmpq	$2, %rcx
	jge	.LBB2_157
.LBB2_96:                               # %.preheader24.i
	testl	%ecx, %ecx
	jle	.LBB2_113
# BB#97:                                # %.lr.ph64.i
	movq	pairalign.distancemtx(%rip), %r10
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_98:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_100 Depth 2
	movq	(%r14,%rsi,8), %rdi
	movb	(%rdi), %bl
	testb	%bl, %bl
	je	.LBB2_101
# BB#99:                                # %.lr.ph61.i.preheader
                                        #   in Loop: Header=BB2_98 Depth=1
	incq	%rdi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_100:                              # %.lr.ph61.i
                                        #   Parent Loop BB2_98 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rbx
	movq	%rbx, %rbp
	shlq	$9, %rbp
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rbp,%rbx,4), %xmm1
	addss	%xmm1, %xmm0
	movzbl	(%rdi), %ebx
	incq	%rdi
	testb	%bl, %bl
	jne	.LBB2_100
	jmp	.LBB2_102
	.p2align	4, 0x90
.LBB2_101:                              #   in Loop: Header=BB2_98 Depth=1
	pxor	%xmm0, %xmm0
.LBB2_102:                              # %._crit_edge62.i
                                        #   in Loop: Header=BB2_98 Depth=1
	cvtss2sd	%xmm0, %xmm0
	movq	(%r10,%rsi,8), %rdi
	movsd	%xmm0, (%rdi,%rsi,8)
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_98
# BB#103:                               # %._crit_edge65.i
	cmpl	$2, %ecx
	jl	.LBB2_113
# BB#104:                               # %.lr.ph56.i
	decl	%ecx
	movl	njob(%rip), %edx
	movslq	%edx, %r8
	movl	$1, %r11d
	xorl	%r9d, %r9d
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm2, %xmm2
	movsd	.LCPI2_4(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB2_105:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_107 Depth 2
	movq	%r9, %rbp
	leaq	1(%rbp), %r9
	cmpq	%r8, %r9
	jge	.LBB2_112
# BB#106:                               # %.lr.ph53.i
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	(%r10,%rbp,8), %rsi
	movq	%r11, %rbx
	.p2align	4, 0x90
.LBB2_107:                              #   Parent Loop BB2_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rbx,8), %xmm4    # xmm4 = mem[0],zero
	ucomisd	%xmm1, %xmm4
	movapd	%xmm0, %xmm5
	jne	.LBB2_108
	jnp	.LBB2_111
.LBB2_108:                              #   in Loop: Header=BB2_107 Depth=2
	movq	(%r10,%rbx,8), %rax
	movsd	(%rax,%rbx,8), %xmm5    # xmm5 = mem[0],zero
	ucomisd	(%rsi,%rbp,8), %xmm5
	movq	%rbx, %rdi
	cmovaq	%rbp, %rdi
	cmovaq	%rsi, %rax
	movslq	%edi, %rdi
	movsd	(%rax,%rdi,8), %xmm6    # xmm6 = mem[0],zero
	ucomisd	%xmm2, %xmm6
	movapd	%xmm0, %xmm5
	jne	.LBB2_109
	jnp	.LBB2_111
.LBB2_109:                              #   in Loop: Header=BB2_107 Depth=2
	ucomisd	%xmm6, %xmm4
	movapd	%xmm0, %xmm5
	ja	.LBB2_111
# BB#110:                               #   in Loop: Header=BB2_107 Depth=2
	divsd	%xmm6, %xmm4
	movapd	%xmm3, %xmm5
	subsd	%xmm4, %xmm5
	addsd	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB2_111:                              #   in Loop: Header=BB2_107 Depth=2
	movsd	%xmm5, (%rsi,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB2_107
.LBB2_112:                              # %.loopexit23.i
                                        #   in Loop: Header=BB2_105 Depth=1
	incq	%r11
	cmpq	%rcx, %r9
	jne	.LBB2_105
.LBB2_113:                              # %._crit_edge57.i
	movl	$.L.str.47, %edi
	movl	$.L.str.58, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_115
# BB#114:
	movl	$.L.str.59, %edi
	callq	ErrorExit
.LBB2_115:
	movb	out_align_instead_of_hat3(%rip), %al
	movl	njob(%rip), %esi
	movq	pairalign.distancemtx(%rip), %rcx
	testb	%al, %al
	je	.LBB2_117
# BB#116:
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	WriteHat2plain
	jmp	.LBB2_118
.LBB2_117:
	movl	$main.name, %edx
	movq	%rbx, %rdi
	callq	WriteHat2
.LBB2_118:
	movq	%rbx, %rdi
	callq	fclose
	cmpb	$1, out_align_instead_of_hat3(%rip)
	jne	.LBB2_285
# BB#119:                               # %..loopexit22_crit_edge.i
	movq	%r12, %rdi
	movl	njob(%rip), %esi
.LBB2_120:                              # %.loopexit22.i
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	FreeLocalHomTable
	cmpb	$115, alg(%rip)
	jne	.LBB2_129
# BB#121:                               # %.preheader.i
	cmpl	$0, njob(%rip)
	jle	.LBB2_128
# BB#122:                               # %.lr.ph39.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_123:                              # %.lr.ph39.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_125 Depth 2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_127
# BB#124:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_123 Depth=1
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB2_125:                              # %.lr.ph.i
                                        #   Parent Loop BB2_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB2_125
# BB#126:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB2_123 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rbx
.LBB2_127:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_123 Depth=1
	movq	%rbx, %rdi
	callq	free
	incq	%rbp
	movslq	njob(%rip), %rax
	cmpq	%rax, %rbp
	jl	.LBB2_123
.LBB2_128:                              # %._crit_edge40.i
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB2_129:                              # %pairalign.exit
	movq	trap_g(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	trap_g(%rip), %rdi
	callq	fclose
	movq	stdout(%rip), %rdi
	cmpl	$100, dorp(%rip)
	movl	$.L.str.20, %eax
	movl	$.L.str.21, %ecx
	cmoveq	%rax, %rcx
	movsbl	alg(%rip), %r8d
	movl	$.L.str.18, %esi
	movl	$.L.str.19, %edx
	movl	$modelname, %r9d
	xorl	%eax, %eax
	callq	fprintf
	xorl	%eax, %eax
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_130:
	xorl	%esi, %esi
.LBB2_131:                              # %vector.body.prol.loopexit
	cmpq	$28, %rdx
	jb	.LBB2_134
# BB#132:                               # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	240(%rax,%rsi,8), %rsi
	movapd	.LCPI2_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB2_133:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rsi)
	movupd	%xmm0, -224(%rsi)
	movupd	%xmm0, -208(%rsi)
	movupd	%xmm0, -192(%rsi)
	movupd	%xmm0, -176(%rsi)
	movupd	%xmm0, -160(%rsi)
	movupd	%xmm0, -144(%rsi)
	movupd	%xmm0, -128(%rsi)
	movupd	%xmm0, -112(%rsi)
	movupd	%xmm0, -96(%rsi)
	movupd	%xmm0, -80(%rsi)
	movupd	%xmm0, -64(%rsi)
	movupd	%xmm0, -48(%rsi)
	movupd	%xmm0, -32(%rsi)
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB2_133
.LBB2_134:                              # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB2_57
	jmp	.LBB2_59
.LBB2_135:
	movq	stderr(%rip), %rdi
	movl	$.L.str.48, %esi
	movl	$foldalignopt, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	njob(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB2_149
# BB#136:                               # %.lr.ph22.preheader.i.i
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_137:                              # %.lr.ph22.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_139 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	jmp	.LBB2_139
	.p2align	4, 0x90
.LBB2_138:                              #   in Loop: Header=BB2_139 Depth=2
	movb	%dl, (%rcx)
	incq	%rcx
.LBB2_139:                              #   Parent Loop BB2_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %eax
	movl	%eax, %ebx
	addb	$-65, %bl
	cmpb	$52, %bl
	ja	.LBB2_144
# BB#140:                               #   in Loop: Header=BB2_139 Depth=2
	movb	$97, %dl
	movzbl	%bl, %edi
	jmpq	*.LJTI2_1(,%rdi,8)
.LBB2_141:                              #   in Loop: Header=BB2_139 Depth=2
	movb	$117, %dl
	jmp	.LBB2_138
.LBB2_142:                              #   in Loop: Header=BB2_139 Depth=2
	movb	$99, %dl
	jmp	.LBB2_138
.LBB2_143:                              #   in Loop: Header=BB2_139 Depth=2
	movb	$103, %dl
	jmp	.LBB2_138
.LBB2_144:                              #   in Loop: Header=BB2_139 Depth=2
	testb	%al, %al
	je	.LBB2_148
.LBB2_145:                              #   in Loop: Header=BB2_139 Depth=2
	cmpb	$99, %al
	je	.LBB2_147
# BB#146:                               #   in Loop: Header=BB2_139 Depth=2
	movb	$110, %al
	.p2align	4, 0x90
.LBB2_147:                              #   in Loop: Header=BB2_139 Depth=2
	movl	%eax, %edx
	jmp	.LBB2_138
	.p2align	4, 0x90
.LBB2_148:                              # %t2u.exit.i.i
                                        #   in Loop: Header=BB2_137 Depth=1
	incq	%rsi
	cmpq	%r14, %rsi
	jne	.LBB2_137
.LBB2_149:                              # %._crit_edge23.i.i
	movq	%r12, %r13
	movl	$.L.str.62, %edi
	movl	$.L.str.58, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_313
# BB#150:                               # %.preheader.i.i
	testl	%r14d, %r14d
	movq	40(%rsp), %r12          # 8-byte Reload
	jle	.LBB2_153
# BB#151:                               # %.lr.ph.preheader.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_152:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rbp), %rbx
	movl	$.L.str.64, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	(%r12,%rbp,8), %rdx
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	cmpq	%rbx, %r14
	movq	%rbx, %rbp
	jne	.LBB2_152
.LBB2_153:                              # %._crit_edge.i.i
	movq	%r15, %rdi
	callq	fclose
	movq	whereispairalign(%rip), %rdx
	movl	$callfoldalign.com, %edi
	movl	$.L.str.66, %esi
	movl	$foldalignopt, %ecx
	xorl	%eax, %eax
	callq	sprintf
	movl	$callfoldalign.com, %edi
	callq	system
	testl	%eax, %eax
	movq	stderr(%rip), %rcx
	jne	.LBB2_314
# BB#154:                               # %callfoldalign.exit.i
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movb	alg(%rip), %al
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%r13, %r12
	cmpb	$66, %al
	jne	.LBB2_82
.LBB2_155:
	movq	stderr(%rip), %rcx
	movl	$.L.str.49, %edi
	movl	$56, %esi
	movl	$1, %edx
	callq	fwrite
	movl	njob(%rip), %edi
	movl	$.L.str.50, %edx
	movq	%r14, %rsi
	callq	calllara
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movb	alg(%rip), %al
	cmpb	$84, %al
	jne	.LBB2_83
.LBB2_156:
	movq	stderr(%rip), %rcx
	movl	$.L.str.51, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movl	njob(%rip), %edi
	movl	$.L.str.52, %edx
	movq	%r14, %rsi
	callq	calllara
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movb	alg(%rip), %al
	jmp	.LBB2_83
.LBB2_157:                              # %.lr.ph78.i
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %r15
	movq	%rcx, %rax
	decq	%rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	pxor	%xmm0, %xmm0
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_158:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_180 Depth 2
                                        #       Child Loop BB2_250 Depth 3
                                        #       Child Loop BB2_253 Depth 3
                                        #       Child Loop BB2_256 Depth 3
                                        #       Child Loop BB2_162 Depth 3
                                        #       Child Loop BB2_192 Depth 3
                                        #       Child Loop BB2_203 Depth 3
                                        #       Child Loop BB2_214 Depth 3
                                        #       Child Loop BB2_225 Depth 3
	movq	%rax, %rbx
	movq	stderr(%rip), %rdi
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	leaq	1(%rbx), %rax
	movslq	njob(%rip), %rcx
	cmpq	%rcx, %rax
	jge	.LBB2_284
# BB#159:                               # %.lr.ph73.i
                                        #   in Loop: Header=BB2_158 Depth=1
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	shlq	$8, %rax
	leaq	main.name(%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB2_180
.LBB2_160:                              # %t2u.exit.i18.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	(%r14), %rax
	jmp	.LBB2_162
	.p2align	4, 0x90
.LBB2_161:                              #   in Loop: Header=BB2_162 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_162:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_167
# BB#163:                               #   in Loop: Header=BB2_162 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_4(,%rcx,8)
.LBB2_164:                              #   in Loop: Header=BB2_162 Depth=3
	movb	$117, %dl
	jmp	.LBB2_161
.LBB2_165:                              #   in Loop: Header=BB2_162 Depth=3
	movb	$99, %dl
	jmp	.LBB2_161
.LBB2_166:                              #   in Loop: Header=BB2_162 Depth=3
	movb	$103, %dl
	jmp	.LBB2_161
.LBB2_167:                              #   in Loop: Header=BB2_162 Depth=3
	testb	%bl, %bl
	je	.LBB2_171
.LBB2_168:                              #   in Loop: Header=BB2_162 Depth=3
	cmpb	$99, %bl
	je	.LBB2_170
# BB#169:                               #   in Loop: Header=BB2_162 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_170:                              #   in Loop: Header=BB2_162 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_161
.LBB2_171:                              # %t2u.exit4.i.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.95, %edi
	movl	$.L.str.58, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_306
# BB#172:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.97, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rbx, %rdi
	callq	write1seq
	movl	$.L.str.98, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	callq	write1seq
	movq	%rbx, %rdi
	callq	fclose
	movl	$.L.str.99, %edi
	callq	system
	movq	whereispairalign(%rip), %rdx
	movl	$callmxscarna_giving_bpp.com, %edi
	movl	$.L.str.100, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$callmxscarna_giving_bpp.com, %edi
	callq	system
	testl	%eax, %eax
	jne	.LBB2_307
# BB#173:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.102, %edi
	movl	$.L.str.11, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_308
# BB#174:                               # %callmxscarna_giving_bpp.exit.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movl	$callmxscarna_giving_bpp.com, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdx
	callq	fgets
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rsi
	movq	%rbx, %rdi
	callq	load1SeqWithoutName_new
	movl	$callmxscarna_giving_bpp.com, %edi
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdx
	callq	fgets
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	callq	load1SeqWithoutName_new
	movq	%rbx, %rdi
	callq	fclose
	movq	(%rbp), %rdi
	movq	(%r14), %rsi
	movl	penalty(%rip), %edx
	callq	naivepairscore11
	cvtsi2ssl	%eax, %xmm0
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movq	%r15, %r14
	jmp	.LBB2_267
.LBB2_175:                              #   in Loop: Header=BB2_180 Depth=2
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_268
.LBB2_176:                              #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.78, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	G__align11
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movl	$0, 4(%rsp)
	movl	$0, (%rsp)
	cmpb	$0, (%r13)
	je	.LBB2_179
.LBB2_177:                              #   in Loop: Header=BB2_180 Depth=2
	movq	(%rbp), %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	(%rbx), %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	strcpy
	movl	160(%rsp), %eax
	movl	%eax, 4(%rsp)
	movl	108(%rsp), %eax
	movl	%eax, (%rsp)
.LBB2_178:                              # %recallpairfoldalign.exit.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	%r13, %rdi
	callq	free
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB2_267
.LBB2_179:                              #   in Loop: Header=BB2_180 Depth=2
	movq	stderr(%rip), %rdi
	leal	1(%r12), %ecx
	movl	$.L.str.80, %esi
	xorl	%eax, %eax
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	jmp	.LBB2_178
	.p2align	4, 0x90
.LBB2_180:                              #   Parent Loop BB2_158 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_250 Depth 3
                                        #       Child Loop BB2_253 Depth 3
                                        #       Child Loop BB2_256 Depth 3
                                        #       Child Loop BB2_162 Depth 3
                                        #       Child Loop BB2_192 Depth 3
                                        #       Child Loop BB2_203 Depth 3
                                        #       Child Loop BB2_214 Depth 3
                                        #       Child Loop BB2_225 Depth 3
	movq	(%r14,%rbx,8), %rsi
	cmpb	$0, (%rsi)
	je	.LBB2_184
# BB#181:                               #   in Loop: Header=BB2_180 Depth=2
	movq	(%r14,%r12,8), %rax
	cmpb	$0, (%rax)
	je	.LBB2_184
# BB#182:                               #   in Loop: Header=BB2_180 Depth=2
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	(%r15,%rbx,8), %rdi
	callq	strcpy
	movq	(%r15,%r12,8), %rdi
	movq	(%r14,%r12,8), %rsi
	callq	strcpy
	movq	pairalign.pair(%rip), %rdi
	movq	pairalign.effarr1(%rip), %r8
	subq	$8, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	movl	%ebx, %esi
	movq	%r15, %rdx
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movq	%r12, %r13
	movq	%rbp, %r12
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %r9
	pushq	pairalign.indication1(%rip)
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	conjuctionfortbfast
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
	movq	pairalign.pair(%rip), %rdi
	movq	pairalign.effarr2(%rip), %r8
	subq	$8, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%rbp, %r9
	pushq	pairalign.indication2(%rip)
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	conjuctionfortbfast
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	cmpb	$0, use_fft(%rip)
	je	.LBB2_185
# BB#183:                               #   in Loop: Header=BB2_180 Depth=2
	movq	pairalign.effarr1(%rip), %rdx
	movq	pairalign.effarr2(%rip), %rcx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ebx, %r8d
	movl	%ebp, %r9d
	leaq	156(%rsp), %rax
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	movq	96(%rsp), %r15          # 8-byte Reload
	pushq	%r15
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	Falign
	addq	$16, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -16
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_267
	.p2align	4, 0x90
.LBB2_184:                              #   in Loop: Header=BB2_180 Depth=2
	movq	pairalign.distancemtx(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movabsq	$-4556649414143246336, %rcx # imm = 0xC0C3878000000000
	movq	%rcx, (%rax,%r12,8)
	jmp	.LBB2_282
.LBB2_185:                              #   in Loop: Header=BB2_180 Depth=2
	movsbl	alg(%rip), %eax
	movl	%eax, %ecx
	addl	$-65, %ecx
	cmpl	$51, %ecx
	movq	88(%rsp), %r15          # 8-byte Reload
	ja	.LBB2_175
# BB#186:                               #   in Loop: Header=BB2_180 Depth=2
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmpq	*.LJTI2_2(,%rcx,8)
.LBB2_187:                              #   in Loop: Header=BB2_180 Depth=2
	cmpq	$0, recalllara.fp(%rip)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_190
# BB#188:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.81, %edi
	movl	$.L.str.11, %esi
	callq	fopen
	movq	%rax, recalllara.fp(%rip)
	testq	%rax, %rax
	je	.LBB2_312
# BB#189:                               #   in Loop: Header=BB2_180 Depth=2
	movl	%r15d, %edi
	callq	AllocateCharVec
	movq	%rax, recalllara.ungap1(%rip)
	movl	%r15d, %edi
	callq	AllocateCharVec
	movq	%rax, recalllara.ungap2(%rip)
	movl	%r15d, %edi
	callq	AllocateCharVec
	movq	%rax, recalllara.ori1(%rip)
	movl	%r15d, %edi
	callq	AllocateCharVec
	movq	%rax, recalllara.ori2(%rip)
.LBB2_190:                              #   in Loop: Header=BB2_180 Depth=2
	movq	recalllara.ori1(%rip), %rdi
	movq	(%rbp), %rsi
	callq	strcpy
	movq	recalllara.ori2(%rip), %rdi
	movq	(%rbx), %rsi
	callq	strcpy
	movq	recalllara.fp(%rip), %rdx
	movl	$recalllara.com, %edi
	movl	$999, %esi              # imm = 0x3E7
	callq	fgets
	movq	recalllara.fp(%rip), %rdx
	movl	$recalllara.com, %edi
	movl	$9999, %esi             # imm = 0x270F
	callq	myfgets
	movq	(%rbp), %rdi
	movl	$recalllara.com, %esi
	callq	strcpy
	movq	recalllara.fp(%rip), %rdx
	movl	$recalllara.com, %edi
	movl	$9999, %esi             # imm = 0x270F
	callq	myfgets
	movq	(%rbx), %rdi
	movl	$recalllara.com, %esi
	callq	strcpy
	movq	recalllara.ungap1(%rip), %rdi
	movq	(%rbp), %rsi
	callq	gappick0
	movq	recalllara.ungap2(%rip), %rdi
	movq	(%rbx), %rsi
	callq	gappick0
	movq	recalllara.ungap1(%rip), %rdi
	movq	%rdi, %rax
	jmp	.LBB2_192
	.p2align	4, 0x90
.LBB2_191:                              #   in Loop: Header=BB2_192 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_192:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_197
# BB#193:                               #   in Loop: Header=BB2_192 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_5(,%rcx,8)
.LBB2_194:                              #   in Loop: Header=BB2_192 Depth=3
	movb	$117, %dl
	jmp	.LBB2_191
.LBB2_195:                              #   in Loop: Header=BB2_192 Depth=3
	movb	$99, %dl
	jmp	.LBB2_191
.LBB2_196:                              #   in Loop: Header=BB2_192 Depth=3
	movb	$103, %dl
	jmp	.LBB2_191
.LBB2_197:                              #   in Loop: Header=BB2_192 Depth=3
	testb	%bl, %bl
	je	.LBB2_201
.LBB2_198:                              #   in Loop: Header=BB2_192 Depth=3
	cmpb	$99, %bl
	je	.LBB2_200
# BB#199:                               #   in Loop: Header=BB2_192 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_200:                              #   in Loop: Header=BB2_192 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_191
.LBB2_201:                              # %t2u.exit.i12.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	recalllara.ungap2(%rip), %r13
	movq	%r13, %rax
	jmp	.LBB2_203
	.p2align	4, 0x90
.LBB2_202:                              #   in Loop: Header=BB2_203 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_203:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_208
# BB#204:                               #   in Loop: Header=BB2_203 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_6(,%rcx,8)
.LBB2_205:                              #   in Loop: Header=BB2_203 Depth=3
	movb	$117, %dl
	jmp	.LBB2_202
.LBB2_206:                              #   in Loop: Header=BB2_203 Depth=3
	movb	$99, %dl
	jmp	.LBB2_202
.LBB2_207:                              #   in Loop: Header=BB2_203 Depth=3
	movb	$103, %dl
	jmp	.LBB2_202
.LBB2_208:                              #   in Loop: Header=BB2_203 Depth=3
	testb	%bl, %bl
	je	.LBB2_212
.LBB2_209:                              #   in Loop: Header=BB2_203 Depth=3
	cmpb	$99, %bl
	je	.LBB2_211
# BB#210:                               #   in Loop: Header=BB2_203 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_211:                              #   in Loop: Header=BB2_203 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_202
.LBB2_212:                              # %t2u.exit36.i.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	recalllara.ori1(%rip), %rsi
	movq	%rsi, %rax
	jmp	.LBB2_214
	.p2align	4, 0x90
.LBB2_213:                              #   in Loop: Header=BB2_214 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_214:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_219
# BB#215:                               #   in Loop: Header=BB2_214 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_7(,%rcx,8)
.LBB2_216:                              #   in Loop: Header=BB2_214 Depth=3
	movb	$117, %dl
	jmp	.LBB2_213
.LBB2_217:                              #   in Loop: Header=BB2_214 Depth=3
	movb	$99, %dl
	jmp	.LBB2_213
.LBB2_218:                              #   in Loop: Header=BB2_214 Depth=3
	movb	$103, %dl
	jmp	.LBB2_213
.LBB2_219:                              #   in Loop: Header=BB2_214 Depth=3
	testb	%bl, %bl
	je	.LBB2_223
.LBB2_220:                              #   in Loop: Header=BB2_214 Depth=3
	cmpb	$99, %bl
	je	.LBB2_222
# BB#221:                               #   in Loop: Header=BB2_214 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_222:                              #   in Loop: Header=BB2_214 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_213
.LBB2_223:                              # %t2u.exit32.i.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	recalllara.ori2(%rip), %rbp
	movq	%rbp, %rax
	jmp	.LBB2_225
	.p2align	4, 0x90
.LBB2_224:                              #   in Loop: Header=BB2_225 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_225:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_230
# BB#226:                               #   in Loop: Header=BB2_225 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_8(,%rcx,8)
.LBB2_227:                              #   in Loop: Header=BB2_225 Depth=3
	movb	$117, %dl
	jmp	.LBB2_224
.LBB2_228:                              #   in Loop: Header=BB2_225 Depth=3
	movb	$99, %dl
	jmp	.LBB2_224
.LBB2_229:                              #   in Loop: Header=BB2_225 Depth=3
	movb	$103, %dl
	jmp	.LBB2_224
.LBB2_230:                              #   in Loop: Header=BB2_225 Depth=3
	testb	%bl, %bl
	je	.LBB2_234
.LBB2_231:                              #   in Loop: Header=BB2_225 Depth=3
	cmpb	$99, %bl
	je	.LBB2_233
# BB#232:                               #   in Loop: Header=BB2_225 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_233:                              #   in Loop: Header=BB2_225 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_224
.LBB2_234:                              # %t2u.exit28.i.i
                                        #   in Loop: Header=BB2_180 Depth=2
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_299
# BB#235:                               #   in Loop: Header=BB2_180 Depth=2
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_299
# BB#236:                               # %recalllara.exit.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movl	penalty(%rip), %edx
	callq	naivepairscore11
	cvtsi2ssl	%eax, %xmm0
	jmp	.LBB2_266
.LBB2_237:                              #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %edx
	callq	G__align11
	jmp	.LBB2_266
.LBB2_238:                              #   in Loop: Header=BB2_180 Depth=2
	movq	recallpairfoldalign.fp(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_240
# BB#239:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.75, %edi
	movl	$.L.str.11, %esi
	callq	fopen
	movq	%rax, %rbp
	movq	%rbp, recallpairfoldalign.fp(%rip)
	testq	%rbp, %rbp
	je	.LBB2_311
.LBB2_240:                              #   in Loop: Header=BB2_180 Depth=2
	movl	$1, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	%rax, %r13
	movl	$1, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdx
	movq	%rbp, %rdi
	movq	%rax, %r8
	movq	%r13, %rcx
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	48(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r15
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	leaq	116(%rsp), %rax
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	leaq	176(%rsp), %rax
	pushq	%rax
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	readpairfoldalign
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	movl	$foldalignopt, %edi
	movl	$.L.str.77, %esi
	callq	strstr
	testq	%rax, %rax
	movq	stderr(%rip), %rcx
	jne	.LBB2_176
# BB#241:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.79, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	movq	%rsp, %r8
	callq	L__align11
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	cmpb	$0, (%r13)
	jne	.LBB2_177
	jmp	.LBB2_179
.LBB2_242:                              #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %edx
	callq	genG__align11
	jmp	.LBB2_266
.LBB2_243:                              #   in Loop: Header=BB2_180 Depth=2
	movl	penalty(%rip), %esi
	movl	penalty_ex(%rip), %edx
	movl	$amino_dis, %edi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movl	%r15d, %r9d
	callq	G__align11_noalign
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	movq	%rsp, %r8
	callq	L__align11
	jmp	.LBB2_267
.LBB2_244:                              #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %edx
	callq	MSalign11
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	jmp	.LBB2_267
.LBB2_245:                              #   in Loop: Header=BB2_180 Depth=2
	movl	penalty(%rip), %esi
	movl	penalty_ex(%rip), %edx
	movl	$amino_dis, %edi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movl	%r15d, %r9d
	callq	G__align11_noalign
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	leaq	4(%rsp), %rcx
	movq	%rsp, %r8
	callq	genL__align11
	jmp	.LBB2_267
.LBB2_246:                              #   in Loop: Header=BB2_180 Depth=2
	movq	pairalign.effarr1(%rip), %rdx
	movq	pairalign.effarr2(%rip), %rcx
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, %r8d
	movl	%ebp, %r9d
	pushq	%r15
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	callq	Aalign
	addq	$16, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_266
.LBB2_247:                              #   in Loop: Header=BB2_180 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	movq	(%rax,%r12,8), %rbp
	movl	$.L.str.90, %edi
	movl	$.L.str.58, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_310
# BB#248:                               #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.92, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_251
# BB#249:                               # %.lr.ph8.i.i.preheader
                                        #   in Loop: Header=BB2_180 Depth=2
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB2_250:                              # %.lr.ph8.i.i
                                        #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	(%rbx), %rsi
	addq	$8, %rbx
	testq	%rsi, %rsi
	jne	.LBB2_250
.LBB2_251:                              # %._crit_edge9.i.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movl	$.L.str.93, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB2_254
# BB#252:                               # %.lr.ph.i13.i.preheader
                                        #   in Loop: Header=BB2_180 Depth=2
	addq	$8, %rbp
	.p2align	4, 0x90
.LBB2_253:                              # %.lr.ph.i13.i
                                        #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	(%rbp), %rsi
	addq	$8, %rbp
	testq	%rsi, %rsi
	jne	.LBB2_253
.LBB2_254:                              # %._crit_edge.i14.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movq	%r14, %rdi
	callq	fclose
	movl	$.L.str.94, %edi
	callq	system
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB2_256
	.p2align	4, 0x90
.LBB2_255:                              #   in Loop: Header=BB2_256 Depth=3
	movb	%dl, (%rax)
	incq	%rax
.LBB2_256:                              #   Parent Loop BB2_158 Depth=1
                                        #     Parent Loop BB2_180 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movl	%ebx, %ecx
	addb	$-65, %cl
	cmpb	$52, %cl
	ja	.LBB2_261
# BB#257:                               #   in Loop: Header=BB2_256 Depth=3
	movb	$97, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_3(,%rcx,8)
.LBB2_258:                              #   in Loop: Header=BB2_256 Depth=3
	movb	$117, %dl
	jmp	.LBB2_255
.LBB2_259:                              #   in Loop: Header=BB2_256 Depth=3
	movb	$99, %dl
	jmp	.LBB2_255
.LBB2_260:                              #   in Loop: Header=BB2_256 Depth=3
	movb	$103, %dl
	jmp	.LBB2_255
.LBB2_261:                              #   in Loop: Header=BB2_256 Depth=3
	testb	%bl, %bl
	je	.LBB2_160
.LBB2_262:                              #   in Loop: Header=BB2_256 Depth=3
	cmpb	$99, %bl
	je	.LBB2_264
# BB#263:                               #   in Loop: Header=BB2_256 Depth=3
	movb	$110, %bl
	.p2align	4, 0x90
.LBB2_264:                              #   in Loop: Header=BB2_256 Depth=3
	movl	%ebx, %edx
	jmp	.LBB2_255
.LBB2_265:                              #   in Loop: Header=BB2_180 Depth=2
	movl	penalty(%rip), %esi
	movl	penalty_ex(%rip), %edx
	movl	$amino_dis, %edi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	%r15d, %r9d
	callq	G__align11_noalign
.LBB2_266:                              # %thread-pre-split.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	.p2align	4, 0x90
.LBB2_267:                              # %thread-pre-split.i
                                        #   in Loop: Header=BB2_180 Depth=2
	movb	alg(%rip), %al
.LBB2_268:                              #   in Loop: Header=BB2_180 Depth=2
	cmpb	$116, %al
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB2_271
# BB#269:                               #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	je	.LBB2_277
# BB#270:                               #   in Loop: Header=BB2_180 Depth=2
	movq	(%rbp), %rax
	cmpb	$0, (%rax)
	je	.LBB2_277
.LBB2_271:                              #   in Loop: Header=BB2_180 Depth=2
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	pairalign.distancemtx(%rip), %rax
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movsd	%xmm0, (%rax,%r12,8)
	movb	alg(%rip), %al
	cmpb	$116, %al
	je	.LBB2_279
# BB#272:                               #   in Loop: Header=BB2_180 Depth=2
	testb	$1, out_align_instead_of_hat3(%rip)
	jne	.LBB2_279
# BB#273:                               #   in Loop: Header=BB2_180 Depth=2
	cmpb	$86, %al
	je	.LBB2_279
# BB#274:                               #   in Loop: Header=BB2_180 Depth=2
	cmpb	$83, %al
	je	.LBB2_279
# BB#275:                               #   in Loop: Header=BB2_180 Depth=2
	cmpb	$72, %al
	jne	.LBB2_278
# BB#276:                               #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	movq	(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%r12,%r12,4), %rbp
	shlq	$4, %rbp
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	addq	(%rax,%r15,8), %rbp
	movl	4(%rsp), %r13d
	movl	(%rsp), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	cvttss2si	8(%rsp), %eax   # 4-byte Folded Reload
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movq	%rbx, %rdi
	callq	strlen
	subq	$8, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%r15, %rbx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%r13d, %ecx
	movl	80(%rsp), %r8d          # 4-byte Reload
	movl	76(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	callq	putlocalhom_ext
	addq	$16, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset -16
	cmpb	$116, alg(%rip)
	jne	.LBB2_280
	jmp	.LBB2_282
.LBB2_277:                              #   in Loop: Header=BB2_180 Depth=2
	movq	pairalign.distancemtx(%rip), %rax
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movabsq	$-4556649414143246336, %rcx # imm = 0xC0C3878000000000
	movq	%rcx, (%rax,%r12,8)
	cmpb	$116, alg(%rip)
	jne	.LBB2_280
	jmp	.LBB2_282
.LBB2_278:                              #   in Loop: Header=BB2_180 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movq	(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%r12,%r12,4), %rbp
	shlq	$4, %rbp
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	addq	(%rax,%rbx,8), %rbp
	movl	4(%rsp), %r13d
	movl	(%rsp), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	cvttss2si	8(%rsp), %eax   # 4-byte Folded Reload
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movq	%r15, %rdi
	callq	strlen
	subq	$8, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%r13d, %ecx
	movl	80(%rsp), %r8d          # 4-byte Reload
	movl	76(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	putlocalhom2
	addq	$16, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -16
	.p2align	4, 0x90
.LBB2_279:                              #   in Loop: Header=BB2_180 Depth=2
	cmpb	$116, alg(%rip)
	je	.LBB2_282
.LBB2_280:                              #   in Loop: Header=BB2_180 Depth=2
	movb	out_align_instead_of_hat3(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	jne	.LBB2_282
# BB#281:                               #   in Loop: Header=BB2_180 Depth=2
	movq	stdout(%rip), %rdi
	leal	1(%r12), %ecx
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.56, %esi
	movb	$1, %al
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	callq	write1seq
	movq	stdout(%rip), %rdi
	movq	%r12, %rax
	shlq	$8, %rax
	leaq	main.name(%rax), %rdx
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movq	(%rbp), %rsi
	callq	write1seq
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	.p2align	4, 0x90
.LBB2_282:                              #   in Loop: Header=BB2_180 Depth=2
	incq	%r12
	movslq	njob(%rip), %rcx
	cmpq	%rcx, %r12
	jl	.LBB2_180
# BB#283:                               #   in Loop: Header=BB2_158 Depth=1
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
.LBB2_284:                              # %.loopexit25.i
                                        #   in Loop: Header=BB2_158 Depth=1
	incq	112(%rsp)               # 8-byte Folded Spill
	cmpq	120(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB2_158
	jmp	.LBB2_96
.LBB2_285:
	movq	stderr(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movl	njob(%rip), %esi
	cmpl	$2, %esi
	jl	.LBB2_296
# BB#286:                               # %.lr.ph50.preheader.i
	leal	-1(%rsi), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %r15d
	xorl	%r12d, %r12d
	movsd	.LCPI2_5(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB2_287:                              # %.lr.ph50.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_289 Depth 2
                                        #       Child Loop BB2_290 Depth 3
	movq	%r12, %r13
	leaq	1(%r13), %r12
	movslq	%esi, %rax
	cmpq	%rax, %r12
	jge	.LBB2_295
# BB#288:                               # %.lr.ph47.i
                                        #   in Loop: Header=BB2_287 Depth=1
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB2_289:                              #   Parent Loop BB2_287 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_290 Depth 3
	leaq	(%r14,%r14,4), %rbx
	shlq	$4, %rbx
	movq	80(%rsp), %rax          # 8-byte Reload
	addq	(%rax,%r13,8), %rbx
	je	.LBB2_294
	.p2align	4, 0x90
.LBB2_290:                              # %.lr.ph43.i
                                        #   Parent Loop BB2_287 Depth=1
                                        #     Parent Loop BB2_289 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jne	.LBB2_291
	jnp	.LBB2_292
.LBB2_291:                              #   in Loop: Header=BB2_290 Depth=3
	movq	stdout(%rip), %rdi
	movl	48(%rbx), %r8d
	movl	24(%rbx), %r9d
	movl	28(%rbx), %r10d
	movl	32(%rbx), %r11d
	movl	36(%rbx), %ebp
	subq	$8, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.61, %esi
	movb	$1, %al
	movl	%r13d, %edx
	movl	%r14d, %ecx
	pushq	%rbp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	movsd	.LCPI2_5(%rip), %xmm1   # xmm1 = mem[0],zero
	addq	$32, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -32
.LBB2_292:                              #   in Loop: Header=BB2_290 Depth=3
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_290
# BB#293:                               # %._crit_edge44.i.loopexit
                                        #   in Loop: Header=BB2_289 Depth=2
	movl	njob(%rip), %esi
.LBB2_294:                              # %._crit_edge44.i
                                        #   in Loop: Header=BB2_289 Depth=2
	incq	%r14
	movslq	%esi, %rax
	cmpq	%rax, %r14
	jl	.LBB2_289
.LBB2_295:                              # %.loopexit.i
                                        #   in Loop: Header=BB2_287 Depth=1
	incq	%r15
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	jne	.LBB2_287
	jmp	.LBB2_120
.LBB2_296:
	movq	%r12, %rdi
	jmp	.LBB2_120
.LBB2_297:
	movq	stderr(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$13, %esi
.LBB2_298:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB2_299:
	movq	stderr(%rip), %rcx
	movl	$.L.str.83, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	recalllara.ungap1(%rip), %rdx
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	recalllara.ori1(%rip), %rdx
	movl	$.L.str.86, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.87, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	recalllara.ungap2(%rip), %rdx
	movl	$.L.str.88, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	recalllara.ori2(%rip), %rdx
	movl	$.L.str.89, %esi
.LBB2_300:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB2_301:
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	jmp	.LBB2_304
.LBB2_302:
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	movl	$50000, %edx            # imm = 0xC350
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$56, %esi
	jmp	.LBB2_298
.LBB2_303:
	movq	stderr(%rip), %rdi
	movsbl	%al, %edx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
.LBB2_304:
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB2_305:
	movq	stderr(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$17, %esi
	jmp	.LBB2_298
.LBB2_306:
	movq	stderr(%rip), %rcx
	movl	$.L.str.96, %edi
	movl	$27, %esi
	jmp	.LBB2_298
.LBB2_307:
	movq	stderr(%rip), %rcx
	movl	$.L.str.101, %edi
	movl	$18, %esi
	jmp	.LBB2_298
.LBB2_308:
	movq	stderr(%rip), %rcx
	movl	$.L.str.103, %edi
.LBB2_309:
	movl	$25, %esi
	jmp	.LBB2_298
.LBB2_310:
	movq	stderr(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$24, %esi
	jmp	.LBB2_298
.LBB2_311:
	movq	stderr(%rip), %rcx
	movl	$.L.str.76, %edi
	movl	$26, %esi
	jmp	.LBB2_298
.LBB2_312:
	movq	stderr(%rip), %rcx
	movl	$.L.str.82, %edi
	movl	$21, %esi
	jmp	.LBB2_298
.LBB2_313:
	movq	stderr(%rip), %rcx
	movl	$.L.str.63, %edi
	jmp	.LBB2_309
.LBB2_314:
	movl	$.L.str.67, %edi
	movl	$19, %esi
	jmp	.LBB2_298
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_35
	.quad	.LBB2_36
	.quad	.LBB2_37
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_38
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_39
.LJTI2_1:
	.quad	.LBB2_138
	.quad	.LBB2_145
	.quad	.LBB2_142
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_143
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_141
	.quad	.LBB2_141
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_138
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_147
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_145
	.quad	.LBB2_141
	.quad	.LBB2_147
.LJTI2_2:
	.quad	.LBB2_237
	.quad	.LBB2_187
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_238
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_242
	.quad	.LBB2_243
	.quad	.LBB2_244
	.quad	.LBB2_245
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_187
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_246
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_268
	.quad	.LBB2_247
	.quad	.LBB2_265
.LJTI2_3:
	.quad	.LBB2_255
	.quad	.LBB2_262
	.quad	.LBB2_259
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_260
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_258
	.quad	.LBB2_258
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_255
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_264
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_262
	.quad	.LBB2_258
	.quad	.LBB2_264
.LJTI2_4:
	.quad	.LBB2_161
	.quad	.LBB2_168
	.quad	.LBB2_165
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_166
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_164
	.quad	.LBB2_164
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_161
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_170
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_168
	.quad	.LBB2_164
	.quad	.LBB2_170
.LJTI2_5:
	.quad	.LBB2_191
	.quad	.LBB2_198
	.quad	.LBB2_195
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_196
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_194
	.quad	.LBB2_194
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_191
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_200
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_198
	.quad	.LBB2_194
	.quad	.LBB2_200
.LJTI2_6:
	.quad	.LBB2_202
	.quad	.LBB2_209
	.quad	.LBB2_206
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_207
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_205
	.quad	.LBB2_205
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_202
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_211
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_209
	.quad	.LBB2_205
	.quad	.LBB2_211
.LJTI2_7:
	.quad	.LBB2_213
	.quad	.LBB2_220
	.quad	.LBB2_217
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_218
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_216
	.quad	.LBB2_216
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_213
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_222
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_220
	.quad	.LBB2_216
	.quad	.LBB2_222
.LJTI2_8:
	.quad	.LBB2_224
	.quad	.LBB2_231
	.quad	.LBB2_228
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_229
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_227
	.quad	.LBB2_227
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_224
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_233
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_231
	.quad	.LBB2_227
	.quad	.LBB2_233

	.text
	.p2align	4, 0x90
	.type	calllara,@function
calllara:                               # @calllara
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movl	%edi, %ebp
	testl	%ebp, %ebp
	jle	.LBB3_9
# BB#1:                                 # %.lr.ph22.preheader
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph22
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.68, %edi
	movl	$.L.str.58, %esi
	callq	fopen
	movq	%rax, %r13
	decl	%ebx
	jne	.LBB3_2
# BB#3:                                 # %._crit_edge23
	testq	%r13, %r13
	je	.LBB3_9
# BB#4:                                 # %.preheader
	testl	%ebp, %ebp
	jle	.LBB3_7
# BB#5:                                 # %.lr.ph.preheader
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rbp), %rbx
	movl	$.L.str.64, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	(%r15,%rbp,8), %rdx
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	cmpq	%rbx, %r12
	movq	%rbx, %rbp
	jne	.LBB3_6
.LBB3_7:                                # %._crit_edge
	movq	%r13, %rdi
	callq	fclose
	movq	whereispairalign(%rip), %rdx
	movl	$calllara.com, %edi
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movq	%r14, %rcx
	callq	sprintf
	movl	$calllara.com, %edi
	callq	system
	testl	%eax, %eax
	jne	.LBB3_8
# BB#11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_9:                                # %._crit_edge23.thread
	movq	stderr(%rip), %rcx
	movl	$.L.str.69, %edi
	movl	$20, %esi
	jmp	.LBB3_10
.LBB3_8:
	movq	stderr(%rip), %rcx
	movl	$.L.str.71, %edi
	movl	$14, %esi
.LBB3_10:                               # %._crit_edge23.thread
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	calllara, .Lfunc_end3-calllara
	.cfi_endproc

	.type	foldalignopt,@object    # @foldalignopt
	.local	foldalignopt
	.comm	foldalignopt,1000,16
	.type	laraparams,@object      # @laraparams
	.local	laraparams
	.comm	laraparams,8,8
	.type	out_align_instead_of_hat3,@object # @out_align_instead_of_hat3
	.local	out_align_instead_of_hat3
	.comm	out_align_instead_of_hat3,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"inputfile = %s\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"jtt %d\n"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"TM %d\n"
	.size	.L.str.2, 7

	.type	whereispairalign,@object # @whereispairalign
	.local	whereispairalign
	.comm	whereispairalign,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"whereispairalign = %s\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"laraparams = %s\n"
	.size	.L.str.4, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"foldalignopt = %s\n"
	.size	.L.str.6, 19

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"illegal option %c\n"
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"options: Check source file !\n"
	.size	.L.str.8, 30

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"conflicting options : o, m or u\n"
	.size	.L.str.9, 33

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"conflicting options : C, o\n"
	.size	.L.str.10, 28

	.type	main.nlen,@object       # @main.nlen
	.local	main.nlen
	.comm	main.nlen,200000,16
	.type	main.name,@object       # @main.name
	.local	main.name
	.comm	main.name,12800000,16
	.type	main.seq,@object        # @main.seq
	.local	main.seq
	.comm	main.seq,8,8
	.type	main.mseq1,@object      # @main.mseq1
	.local	main.mseq1
	.comm	main.mseq1,8,8
	.type	main.mseq2,@object      # @main.mseq2
	.local	main.mseq2
	.comm	main.mseq2,8,8
	.type	main.aseq,@object       # @main.aseq
	.local	main.aseq
	.comm	main.aseq,8,8
	.type	main.bseq,@object       # @main.bseq
	.local	main.bseq
	.comm	main.bseq,8,8
	.type	main.eff,@object        # @main.eff
	.local	main.eff
	.comm	main.eff,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"r"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Cannot open %s\n"
	.size	.L.str.12, 16

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"At least 2 sequences should be input!\nOnly %d sequence found.\n"
	.size	.L.str.13, 63

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"The number of sequences must be < %d\n"
	.size	.L.str.14, 38

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Please try the splittbfast program for such large data.\n"
	.size	.L.str.15, 57

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Illegal character %c\n"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"done.\n"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s (%s) Version 6.624b alg=%c, model=%s\n"
	.size	.L.str.18, 41

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"<progname>"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"nuc"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"aa"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"DNA\n"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"JTT %dPAM\n"
	.size	.L.str.23, 11

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"BLOSUM %d\n"
	.size	.L.str.24, 11

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"M-Y\n"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Gap Penalty = %+5.2f, %+5.2f, %+5.2f\n"
	.size	.L.str.26, 38

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"FFT on\n"
	.size	.L.str.27, 8

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"tree-base method\n"
	.size	.L.str.28, 18

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"unweighted\n"
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"clustalw-like weighting\n"
	.size	.L.str.30, 25

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"iterate at each step\n"
	.size	.L.str.31, 22

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"  unweighted\n"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"  reversely weighted\n"
	.size	.L.str.33, 22

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"  weighted\n"
	.size	.L.str.34, 12

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Algorithm A\n"
	.size	.L.str.36, 13

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Algorithm A+\n"
	.size	.L.str.37, 14

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Apgorithm S\n"
	.size	.L.str.38, 13

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Apgorithm A+/C\n"
	.size	.L.str.39, 16

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Unknown algorithm\n"
	.size	.L.str.40, 19

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Basis : 4 nucleotides\n"
	.size	.L.str.41, 23

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Basis : Polarity and Volume\n"
	.size	.L.str.42, 29

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Basis : 20 amino acids\n"
	.size	.L.str.43, 24

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Threshold   of anchors = %d%%\n"
	.size	.L.str.44, 31

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"window size of anchors = %dsites\n"
	.size	.L.str.45, 34

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"FFT off\n"
	.size	.L.str.46, 9

	.type	pairalign.indication1,@object # @pairalign.indication1
	.local	pairalign.indication1
	.comm	pairalign.indication1,8,8
	.type	pairalign.indication2,@object # @pairalign.indication2
	.local	pairalign.indication2
	.comm	pairalign.indication2,8,8
	.type	pairalign.distancemtx,@object # @pairalign.distancemtx
	.local	pairalign.distancemtx
	.comm	pairalign.distancemtx,8,8
	.type	pairalign.effarr1,@object # @pairalign.effarr1
	.local	pairalign.effarr1
	.comm	pairalign.effarr1,8,8
	.type	pairalign.effarr2,@object # @pairalign.effarr2
	.local	pairalign.effarr2
	.comm	pairalign.effarr2,8,8
	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"hat2"
	.size	.L.str.47, 5

	.type	pairalign.pair,@object  # @pairalign.pair
	.local	pairalign.pair
	.comm	pairalign.pair,8,8
	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Calling FOLDALIGN with option '%s'\n"
	.size	.L.str.48, 36

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Running LARA (Bauer et al. http://www.planet-lisa.net/)\n"
	.size	.L.str.49, 57

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.zero	1
	.size	.L.str.50, 1

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Running SLARA (Bauer et al. http://www.planet-lisa.net/)\n"
	.size	.L.str.51, 58

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"-s"
	.size	.L.str.52, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Preparing bpp\n"
	.size	.L.str.53, 15

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Running MXSCARNA (Tabei et al. http://www.ncrna.org/software/mxscarna)\n"
	.size	.L.str.54, 72

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"% 5d / %d\r"
	.size	.L.str.55, 11

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"sequence %d - sequence %d, pairwise score = %.0f\n"
	.size	.L.str.56, 50

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	">%s\n"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"w"
	.size	.L.str.58, 2

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Cannot open hat2."
	.size	.L.str.59, 18

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"##### writing hat3\n"
	.size	.L.str.60, 20

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"%d %d %d %7.5f %d %d %d %d\n"
	.size	.L.str.61, 28

	.type	callfoldalign.com,@object # @callfoldalign.com
	.local	callfoldalign.com
	.comm	callfoldalign.com,10000,16
	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"_foldalignin"
	.size	.L.str.62, 13

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Cannot open _foldalignin\n"
	.size	.L.str.63, 26

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	">%d\n"
	.size	.L.str.64, 5

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%s\n"
	.size	.L.str.65, 4

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"env PATH=%s  foldalign210 %s _foldalignin > _foldalignout "
	.size	.L.str.66, 59

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Error in foldalign\n"
	.size	.L.str.67, 20

	.type	calllara.com,@object    # @calllara.com
	.local	calllara.com
	.comm	calllara.com,10000,16
	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"_larain"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"Cannot open _larain\n"
	.size	.L.str.69, 21

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"env PATH=%s:/bin:/usr/bin mafft_lara -i _larain -w _laraout -o _lara.params %s"
	.size	.L.str.70, 79

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Error in lara\n"
	.size	.L.str.71, 15

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"hat4"
	.size	.L.str.72, 5

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Cannot open hat4\n"
	.size	.L.str.73, 18

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"Format error\n"
	.size	.L.str.74, 14

	.type	recallpairfoldalign.fp,@object # @recallpairfoldalign.fp
	.local	recallpairfoldalign.fp
	.comm	recallpairfoldalign.fp,8,8
	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"_foldalignout"
	.size	.L.str.75, 14

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Cannot open _foldalignout\n"
	.size	.L.str.76, 27

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"-global"
	.size	.L.str.77, 8

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"Calling G__align11\n"
	.size	.L.str.78, 20

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"Calling L__align11\n"
	.size	.L.str.79, 20

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"FOLDALIGN returned no alignment between %d and %d.  Sequence alignment is used instead.\n"
	.size	.L.str.80, 89

	.type	recalllara.fp,@object   # @recalllara.fp
	.local	recalllara.fp
	.comm	recalllara.fp,8,8
	.type	recalllara.ungap1,@object # @recalllara.ungap1
	.local	recalllara.ungap1
	.comm	recalllara.ungap1,8,8
	.type	recalllara.ungap2,@object # @recalllara.ungap2
	.local	recalllara.ungap2
	.comm	recalllara.ungap2,8,8
	.type	recalllara.ori1,@object # @recalllara.ori1
	.local	recalllara.ori1
	.comm	recalllara.ori1,8,8
	.type	recalllara.ori2,@object # @recalllara.ori2
	.local	recalllara.ori2
	.comm	recalllara.ori2,8,8
	.type	recalllara.com,@object  # @recalllara.com
	.local	recalllara.com
	.comm	recalllara.com,10000,16
	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"_laraout"
	.size	.L.str.81, 9

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"Cannot open _laraout\n"
	.size	.L.str.82, 22

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"SEQUENCE CHANGED!!\n"
	.size	.L.str.83, 20

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"*mseq1  = %s\n"
	.size	.L.str.84, 14

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"ungap1  = %s\n"
	.size	.L.str.85, 14

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"ori1    = %s\n"
	.size	.L.str.86, 14

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"*mseq2  = %s\n"
	.size	.L.str.87, 14

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"ungap2  = %s\n"
	.size	.L.str.88, 14

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"ori2    = %s\n"
	.size	.L.str.89, 14

	.type	callmxscarna_giving_bpp.com,@object # @callmxscarna_giving_bpp.com
	.local	callmxscarna_giving_bpp.com
	.comm	callmxscarna_giving_bpp.com,10000,16
	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"_bpporg"
	.size	.L.str.90, 8

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Cannot write to _bpporg\n"
	.size	.L.str.91, 25

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	">a\n"
	.size	.L.str.92, 4

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	">b\n"
	.size	.L.str.93, 4

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"tr -d '\\r' < _bpporg > _bpp"
	.size	.L.str.94, 28

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"_mxscarnainorg"
	.size	.L.str.95, 15

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"Cannot open _mxscarnainorg\n"
	.size	.L.str.96, 28

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	">1\n"
	.size	.L.str.97, 4

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	">2\n"
	.size	.L.str.98, 4

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"tr -d '\\r' < _mxscarnainorg > _mxscarnain"
	.size	.L.str.99, 42

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"env PATH=%s mxscarnamod -readbpp _mxscarnain > _mxscarnaout 2>_dum"
	.size	.L.str.100, 67

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"Error in mxscarna\n"
	.size	.L.str.101, 19

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"_mxscarnaout"
	.size	.L.str.102, 13

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"Cannot open _mxscarnaout\n"
	.size	.L.str.103, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
