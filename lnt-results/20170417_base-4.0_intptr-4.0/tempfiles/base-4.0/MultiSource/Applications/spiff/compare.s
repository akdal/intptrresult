	.text
	.file	"compare.bc"
	.globl	X_com
	.p2align	4, 0x90
	.type	X_com,@function
X_com:                                  # @X_com
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movslq	%edi, %rax
	movq	_K_ato(,%rax,8), %rcx
	movslq	%esi, %rax
	movq	_K_bto(,%rax,8), %rax
	testb	$2, %dl
	jne	.LBB0_12
# BB#1:
	movl	8(%rcx), %esi
	movl	$1, %r12d
	cmpl	8(%rax), %esi
	jne	.LBB0_51
# BB#2:
	cmpl	$2, %esi
	je	.LBB0_24
# BB#3:
	cmpl	$1, %esi
	jne	.LBB0_35
# BB#4:
	movq	16(%rcx), %r15
	movq	16(%rax), %rbp
	testb	$4, %dl
	je	.LBB0_13
# BB#5:                                 # %.lr.ph.i.preheader.i
	movb	(%rbp), %bl
	testb	%bl, %bl
	je	.LBB0_23
# BB#6:                                 # %.lr.ph.i11
	callq	__ctype_b_loc
	movq	(%rax), %rax
	incq	%rbp
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%r15), %r13
	testb	$4, 1(%rax,%r13,2)
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movsbq	%bl, %r14
	testb	$4, 1(%rax,%r14,2)
	jne	.LBB0_10
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	cmpb	%bl, %r13b
	je	.LBB0_11
	jmp	.LBB0_52
.LBB0_10:                               #   in Loop: Header=BB0_7 Depth=1
	movq	%rax, %rbx
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r13,4), %ecx
	cmpl	(%rax,%r14,4), %ecx
	movq	%rbx, %rax
	jne	.LBB0_52
.LBB0_11:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_7 Depth=1
	incq	%r15
	movzbl	(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB0_7
	jmp	.LBB0_23
.LBB0_12:
	movq	16(%rcx), %r15
	movq	16(%rax), %rbp
	testb	$4, %dl
	jne	.LBB0_15
.LBB0_13:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	strcmp                  # TAILCALL
.LBB0_15:                               # %.lr.ph.i.preheader
	movb	(%rbp), %bl
	testb	%bl, %bl
	je	.LBB0_23
# BB#16:                                # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %r14
	incq	%rbp
	.p2align	4, 0x90
.LBB0_17:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%r15), %r12
	testb	$4, 1(%r14,%r12,2)
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	movsbq	%bl, %r13
	testb	$4, 1(%r14,%r13,2)
	jne	.LBB0_21
.LBB0_19:                               #   in Loop: Header=BB0_17 Depth=1
	cmpb	%bl, %r12b
	je	.LBB0_22
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_21:                               #   in Loop: Header=BB0_17 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r12,4), %ecx
	cmpl	(%rax,%r13,4), %ecx
	jne	.LBB0_51
.LBB0_22:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_17 Depth=1
	incq	%r15
	movzbl	(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB0_17
.LBB0_23:                               # %.critedge.i
	xorl	%r12d, %r12d
	cmpb	$0, (%r15)
	setne	%r12b
	jmp	.LBB0_52
.LBB0_24:
	movq	24(%rcx), %r15
	movq	32(%rcx), %rdi
	movq	24(%rax), %r14
	movq	32(%rax), %rsi
	xorl	%eax, %eax
	callq	T_picktol
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_36
# BB#25:
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph72.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB0_29
# BB#27:                                # %.lr.ph72.i.i
                                        #   in Loop: Header=BB0_26 Depth=1
	cmpl	$2, %eax
	jne	.LBB0_34
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_26 Depth=1
	movq	8(%rbx), %rax
	cmpl	$0, (%rax)
	jne	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_26 Depth=1
	movq	8(%rax), %rax
	cmpb	$48, (%rax)
	jne	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_26 Depth=1
	cmpb	$0, 1(%rax)
	je	.LBB0_34
	.p2align	4, 0x90
.LBB0_32:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_26 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	R_getexp
	movl	%eax, %r12d
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	callq	R_getexp
	decl	%eax
	cmpl	%eax, %r12d
	jge	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_26 Depth=1
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	R_getexp
	movl	%eax, %r13d
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	callq	R_getexp
	decl	%eax
	cmpl	%eax, %r13d
	jl	.LBB0_52
.LBB0_34:                               #   in Loop: Header=BB0_26 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_26
	jmp	.LBB0_37
.LBB0_35:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movl	$-1, %r12d
	jmp	.LBB0_52
.LBB0_36:                               # %.preheader.thread.i.i
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_37:                               # %._crit_edge.i.i
	movl	4(%r15), %eax
	cmpl	4(%r14), %eax
	jne	.LBB0_39
# BB#38:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	F_floatsub
	jmp	.LBB0_40
.LBB0_39:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	F_floatmagadd
.LBB0_40:
	movq	%rax, %r12
	testq	%rbp, %rbp
	je	.LBB0_51
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph.i10.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movl	(%rbp), %eax
	testl	%eax, %eax
	je	.LBB0_48
# BB#42:                                # %.lr.ph.i10.i
                                        #   in Loop: Header=BB0_41 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_45
# BB#43:                                #   in Loop: Header=BB0_41 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	F_floatcmp
	testl	%eax, %eax
	jle	.LBB0_46
# BB#44:                                #   in Loop: Header=BB0_41 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	jmp	.LBB0_47
.LBB0_45:                               #   in Loop: Header=BB0_41 Depth=1
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB0_48
.LBB0_46:                               #   in Loop: Header=BB0_41 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
.LBB0_47:                               #   in Loop: Header=BB0_41 Depth=1
	movq	%rbx, %rsi
	callq	F_floatmul
	movq	%rax, %rbx
.LBB0_48:                               #   in Loop: Header=BB0_41 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	F_floatcmp
	testl	%eax, %eax
	jle	.LBB0_28
# BB#49:                                #   in Loop: Header=BB0_41 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_41
.LBB0_51:
	movl	$1, %r12d
.LBB0_52:                               # %_X_strcmp.exit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_28:
	xorl	%r12d, %r12d
	jmp	.LBB0_52
.Lfunc_end0:
	.size	X_com, .Lfunc_end0-X_com
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"fell off switch in _X_cmptokens"
	.size	.L.str, 32

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"_X_floatdiff called with a null tolerance"
	.size	.L.str.1, 42

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"bad value for type of tolerance in floatdiff"
	.size	.L.str.3, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
