	.text
	.file	"btOverlappingPairCache.bc"
	.globl	_ZN28btHashedOverlappingPairCacheC2Ev
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCacheC2Ev,@function
_ZN28btHashedOverlappingPairCacheC2Ev:  # @_ZN28btHashedOverlappingPairCacheC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV28btHashedOverlappingPairCache+16, (%r15)
	movb	$1, 32(%r15)
	movq	$0, 24(%r15)
	movl	$0, 12(%r15)
	movl	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movb	$1, 80(%r15)
	movq	$0, 72(%r15)
	movl	$0, 60(%r15)
	movl	$0, 64(%r15)
	movb	$1, 112(%r15)
	movq	$0, 104(%r15)
	movl	$0, 92(%r15)
	movl	$0, 96(%r15)
	movq	$0, 120(%r15)
.Ltmp0:
	movl	$64, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp1:
# BB#1:                                 # %.noexc13
	movslq	12(%r15), %rax
	testq	%rax, %rax
	jle	.LBB0_4
# BB#2:                                 # %.lr.ph.i.i
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbx,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbx,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbx,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB0_3
.LBB0_4:                                # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#5:
	cmpb	$0, 32(%r15)
	je	.LBB0_7
# BB#6:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
.LBB0_7:                                # %.noexc14
	movq	$0, 24(%r15)
.LBB0_8:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
	movb	$1, 32(%r15)
	movq	%rbx, 24(%r15)
	movl	$2, 16(%r15)
.Ltmp4:
	movq	%r15, %rdi
	callq	_ZN28btHashedOverlappingPairCache10growTablesEv
.Ltmp5:
# BB#9:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_10:
.Ltmp6:
	movq	%rax, %r14
	movq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_14
# BB#11:
	cmpb	$0, 112(%r15)
	je	.LBB0_13
# BB#12:
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
.LBB0_13:                               # %.noexc11
	movq	$0, 104(%r15)
.LBB0_14:
	movb	$1, 112(%r15)
	movq	$0, 104(%r15)
	movq	$0, 92(%r15)
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#15:
	cmpb	$0, 80(%r15)
	je	.LBB0_17
# BB#16:
.Ltmp9:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
.LBB0_17:                               # %.noexc9
	movq	$0, 72(%r15)
.LBB0_18:
	movb	$1, 80(%r15)
	movq	$0, 72(%r15)
	movq	$0, 60(%r15)
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#19:
	cmpb	$0, 32(%r15)
	je	.LBB0_21
# BB#20:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB0_21:                               # %.noexc
	movq	$0, 24(%r15)
.LBB0_22:
	movb	$1, 32(%r15)
	movq	$0, 24(%r15)
	movq	$0, 12(%r15)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_23:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN28btHashedOverlappingPairCacheC2Ev, .Lfunc_end0-_ZN28btHashedOverlappingPairCacheC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp7          #   Call between .Ltmp7 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btHashedOverlappingPairCache10growTablesEv
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache10growTablesEv,@function
_ZN28btHashedOverlappingPairCache10growTablesEv: # @_ZN28btHashedOverlappingPairCache10growTablesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	16(%r13), %r14d
	movl	60(%r13), %r12d
	cmpl	%r14d, %r12d
	jge	.LBB1_72
# BB#1:
	movslq	%r14d, %rbp
	movslq	%r12d, %rcx
	cmpl	%r14d, 64(%r13)
	jge	.LBB1_4
# BB#2:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testl	%r14d, %r14d
	je	.LBB1_5
# BB#3:
	leaq	(,%rbp,4), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	60(%r13), %ecx
	jmp	.LBB1_6
.LBB1_4:                                # %..lr.ph.i50_crit_edge
	leaq	72(%r13), %rbx
	movq	72(%r13), %r15
	jmp	.LBB1_33
.LBB1_5:
	xorl	%r15d, %r15d
	movl	%r12d, %ecx
.LBB1_6:                                # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i38
	movq	72(%r13), %rdi
	testl	%ecx, %ecx
	jle	.LBB1_9
# BB#7:                                 # %.lr.ph.i.i.i40
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB1_11
# BB#8:
	xorl	%edx, %edx
	jmp	.LBB1_24
.LBB1_9:                                # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i44
	testq	%rdi, %rdi
	jne	.LBB1_30
# BB#10:                                # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i46
	leaq	80(%r13), %rbx
	jmp	.LBB1_32
.LBB1_11:                               # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB1_15
# BB#12:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB1_16
# BB#13:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB1_16
.LBB1_15:
	xorl	%edx, %edx
.LBB1_24:                               # %scalar.ph.preheader
	movq	%rbp, %rbx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB1_27
# BB#25:                                # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB1_26:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB1_26
.LBB1_27:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movq	%rbx, %rbp
	jb	.LBB1_30
# BB#28:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_29:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB1_29
.LBB1_30:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i47
	leaq	80(%r13), %rbx
	cmpb	$0, 80(%r13)
	je	.LBB1_32
# BB#31:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_32:                               # %.lr.ph.i50.sink.split
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	72(%r13), %rax
	movb	$1, (%rbx)
	movq	%rax, %rbx
	movq	%r15, 72(%r13)
	movl	%r14d, 64(%r13)
.LBB1_33:                               # %.lr.ph.i50
	leaq	(%r15,%rcx,4), %rdi
	shlq	$2, %rbp
	shlq	$2, %rcx
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	%r14d, 60(%r13)
	movl	92(%r13), %ecx
	cmpl	%r14d, %ecx
	jge	.LBB1_67
# BB#34:
	movslq	%ecx, %rax
	cmpl	%r14d, 96(%r13)
	jge	.LBB1_37
# BB#35:
	testl	%r14d, %r14d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB1_38
# BB#36:
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	92(%r13), %ecx
	jmp	.LBB1_39
.LBB1_37:                               # %..lr.ph.i_crit_edge
	movq	104(%r13), %r15
	jmp	.LBB1_66
.LBB1_38:
	xorl	%r15d, %r15d
.LBB1_39:                               # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
	movq	104(%r13), %rdi
	testl	%ecx, %ecx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jle	.LBB1_42
# BB#40:                                # %.lr.ph.i.i.i
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB1_44
# BB#41:
	xorl	%edx, %edx
	jmp	.LBB1_57
.LBB1_42:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB1_63
# BB#43:                                # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i
	leaq	112(%r13), %rbx
	jmp	.LBB1_65
.LBB1_44:                               # %min.iters.checked91
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB1_48
# BB#45:                                # %vector.memcheck103
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB1_49
# BB#46:                                # %vector.memcheck103
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB1_49
.LBB1_48:
	xorl	%edx, %edx
.LBB1_57:                               # %scalar.ph89.preheader
	movq	%rbp, %rbx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB1_60
# BB#58:                                # %scalar.ph89.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB1_59:                               # %scalar.ph89.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB1_59
.LBB1_60:                               # %scalar.ph89.prol.loopexit
	cmpq	$7, %rsi
	movq	%rbx, %rbp
	jb	.LBB1_63
# BB#61:                                # %scalar.ph89.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_62:                               # %scalar.ph89
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB1_62
.LBB1_63:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
	leaq	112(%r13), %rbx
	cmpb	$0, 112(%r13)
	je	.LBB1_65
# BB#64:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_65:                               # %.lr.ph.i.sink.split
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	$1, (%rbx)
	movq	%r15, 104(%r13)
	movl	%r14d, 96(%r13)
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB1_66:                               # %.lr.ph.i
	leaq	(%r15,%rax,4), %rdi
	shlq	$2, %rax
	subq	%rax, %rbp
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
.LBB1_67:                               # %_ZN20btAlignedObjectArrayIiE6resizeEiRKi.exit
	movl	%r14d, 92(%r13)
	testl	%r14d, %r14d
	jle	.LBB1_69
# BB#68:                                # %.lr.ph65
	movq	(%rbx), %rdi
	decl	%r14d
	leaq	4(,%r14,4), %r14
	movl	$255, %esi
	movq	%r14, %rdx
	callq	memset
	movq	104(%r13), %rdi
	movl	$255, %esi
	movq	%r14, %rdx
	callq	memset
.LBB1_69:                               # %.preheader
	testl	%r12d, %r12d
	jle	.LBB1_72
# BB#70:                                # %.lr.ph
	movq	24(%r13), %rax
	movq	72(%r13), %rcx
	movq	104(%r13), %rdx
	addq	$8, %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_71:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rax), %rdi
	movq	(%rax), %rbp
	movl	24(%rbp), %ebp
	shll	$16, %ebp
	orl	24(%rdi), %ebp
	movl	%ebp, %edi
	shll	$15, %edi
	notl	%edi
	addl	%ebp, %edi
	movl	%edi, %ebp
	sarl	$10, %ebp
	xorl	%edi, %ebp
	leal	(%rbp,%rbp,8), %edi
	movl	%edi, %ebp
	sarl	$6, %ebp
	xorl	%edi, %ebp
	movl	%ebp, %edi
	shll	$11, %edi
	notl	%edi
	addl	%ebp, %edi
	movl	%edi, %ebp
	sarl	$16, %ebp
	xorl	%edi, %ebp
	movl	16(%r13), %edi
	decl	%edi
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	movl	%ebp, (%rdx,%rsi,4)
	movl	%esi, (%rcx,%rdi,4)
	incq	%rsi
	addq	$32, %rax
	cmpq	%rsi, %r12
	jne	.LBB1_71
.LBB1_72:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_16:                               # %vector.body.preheader
	movq	%rbp, %r8
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_19
# BB#17:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_18:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB1_18
	jmp	.LBB1_20
.LBB1_49:                               # %vector.body87.preheader
	movq	%rbp, %r8
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_52
# BB#50:                                # %vector.body87.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_51:                               # %vector.body87.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB1_51
	jmp	.LBB1_53
.LBB1_19:
	xorl	%ebx, %ebx
.LBB1_20:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB1_23
# BB#21:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB1_22:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB1_22
.LBB1_23:                               # %middle.block
	cmpq	%rdx, %rax
	movq	%r8, %rbp
	je	.LBB1_30
	jmp	.LBB1_24
.LBB1_52:
	xorl	%ebx, %ebx
.LBB1_53:                               # %vector.body87.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB1_56
# BB#54:                                # %vector.body87.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
.LBB1_55:                               # %vector.body87
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB1_55
.LBB1_56:                               # %middle.block88
	cmpq	%rdx, %rax
	movq	%r8, %rbp
	je	.LBB1_63
	jmp	.LBB1_57
.Lfunc_end1:
	.size	_ZN28btHashedOverlappingPairCache10growTablesEv, .Lfunc_end1-_ZN28btHashedOverlappingPairCache10growTablesEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN28btHashedOverlappingPairCacheD2Ev
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCacheD2Ev,@function
_ZN28btHashedOverlappingPairCacheD2Ev:  # @_ZN28btHashedOverlappingPairCacheD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV28btHashedOverlappingPairCache+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:
	cmpb	$0, 112(%rbx)
	je	.LBB3_3
# BB#2:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp15:
.LBB3_3:                                # %.noexc
	movq	$0, 104(%rbx)
.LBB3_4:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#5:
	cmpb	$0, 80(%rbx)
	je	.LBB3_7
# BB#6:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB3_7:                                # %.noexc5
	movq	$0, 72(%rbx)
.LBB3_8:
	movb	$1, 80(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 60(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_12
# BB#9:
	cmpb	$0, 32(%rbx)
	je	.LBB3_11
# BB#10:
.Ltmp25:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp26:
.LBB3_11:                               # %.noexc8
	movq	$0, 24(%rbx)
.LBB3_12:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_18:
.Ltmp27:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_19:
.Ltmp21:
	movq	%rax, %r14
	jmp	.LBB3_20
.LBB3_13:
.Ltmp16:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#14:
	cmpb	$0, 80(%rbx)
	je	.LBB3_16
# BB#15:
.Ltmp17:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp18:
.LBB3_16:                               # %.noexc10
	movq	$0, 72(%rbx)
.LBB3_17:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit11
	movb	$1, 80(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 60(%rbx)
.LBB3_20:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_24
# BB#21:
	cmpb	$0, 32(%rbx)
	je	.LBB3_23
# BB#22:
.Ltmp22:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp23:
.LBB3_23:                               # %.noexc13
	movq	$0, 24(%rbx)
.LBB3_24:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev.exit14
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_26:
.Ltmp24:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN28btHashedOverlappingPairCacheD2Ev, .Lfunc_end3-_ZN28btHashedOverlappingPairCacheD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp26         #   Call between .Ltmp26 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp23-.Ltmp17         #   Call between .Ltmp17 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end3-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btHashedOverlappingPairCacheD0Ev
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCacheD0Ev,@function
_ZN28btHashedOverlappingPairCacheD0Ev:  # @_ZN28btHashedOverlappingPairCacheD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp28:
	callq	_ZN28btHashedOverlappingPairCacheD2Ev
.Ltmp29:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp30:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN28btHashedOverlappingPairCacheD0Ev, .Lfunc_end4-_ZN28btHashedOverlappingPairCacheD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher,@function
_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher: # @_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	(%r14), %rax
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	*104(%rax)
	movq	$0, 16(%rbx)
.LBB5_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher, .Lfunc_end5-_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_endproc

	.globl	_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher,@function
_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher: # @_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	subq	$40, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 48
	movq	$_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback+16, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 24(%rsp)
	movq	%rdx, 32(%rsp)
	movq	(%rdi), %rax
.Ltmp31:
	leaq	8(%rsp), %rsi
	callq	*96(%rax)
.Ltmp32:
# BB#1:
	addq	$40, %rsp
	retq
.LBB6_2:
.Ltmp33:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end6-_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 32
	movq	$_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback+16, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	(%rdi), %rax
.Ltmp34:
	leaq	8(%rsp), %rsi
	callq	*96(%rax)
.Ltmp35:
# BB#1:
	addq	$24, %rsp
	retq
.LBB7_2:
.Ltmp36:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end7-_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp34-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin4   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_,@function
_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_: # @_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	incl	gFindPairs(%rip)
	movl	24(%rsi), %eax
	cmpl	24(%rdx), %eax
	movq	%rsi, %rax
	cmovgq	%rdx, %rax
	cmovgq	%rsi, %rdx
	movl	24(%rax), %r9d
	movl	24(%rdx), %r8d
	movl	%r8d, %eax
	shll	$16, %eax
	orl	%r9d, %eax
	movl	%eax, %ecx
	shll	$15, %ecx
	notl	%ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	sarl	$10, %eax
	xorl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	movl	%eax, %ecx
	sarl	$6, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %eax
	shll	$11, %eax
	notl	%eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	sarl	$16, %ecx
	xorl	%eax, %ecx
	movl	16(%rdi), %eax
	decl	%eax
	andl	%ecx, %eax
	cmpl	60(%rdi), %eax
	jge	.LBB8_1
# BB#2:
	movq	72(%rdi), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	cmpl	$-1, %ecx
	je	.LBB8_3
# BB#4:                                 # %.lr.ph
	movq	24(%rdi), %r10
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rcx
	movq	%rcx, %rdx
	shlq	$5, %rdx
	movq	(%r10,%rdx), %rsi
	cmpl	%r9d, 24(%rsi)
	jne	.LBB8_7
# BB#6:                                 # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	8(%r10,%rdx), %rsi
	cmpl	%r8d, 24(%rsi)
	je	.LBB8_8
.LBB8_7:                                # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit.thread
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	104(%rdi), %rdx
	movl	(%rdx,%rcx,4), %ecx
	cmpl	$-1, %ecx
	jne	.LBB8_5
	jmp	.LBB8_9
.LBB8_1:
	xorl	%eax, %eax
	retq
.LBB8_3:
	xorl	%eax, %eax
	retq
.LBB8_8:                                # %.critedge
	addq	%rdx, %r10
	movq	%r10, %rax
.LBB8_9:                                # %.loopexit
	retq
.Lfunc_end8:
	.size	_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_, .Lfunc_end8-_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_,@function
_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_: # @_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 112
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	movl	24(%rsi), %eax
	cmpl	24(%r14), %eax
	movq	%rsi, %r15
	cmovgq	%r14, %r15
	cmovgq	%rsi, %r14
	movl	24(%r15), %eax
	movl	24(%r14), %ecx
	movl	%ecx, %edx
	shll	$16, %edx
	orl	%eax, %edx
	movl	%edx, %esi
	shll	$15, %esi
	notl	%esi
	addl	%edx, %esi
	movl	%esi, %edx
	sarl	$10, %edx
	xorl	%esi, %edx
	leal	(%rdx,%rdx,8), %edx
	movl	%edx, %esi
	sarl	$6, %esi
	xorl	%edx, %esi
	movl	%esi, %edx
	shll	$11, %edx
	notl	%edx
	addl	%esi, %edx
	movl	%edx, %r13d
	sarl	$16, %r13d
	xorl	%edx, %r13d
	movl	16(%r12), %r8d
	leal	-1(%r8), %ebp
	andl	%r13d, %ebp
	movq	72(%r12), %rdx
	movslq	%ebp, %rsi
	movl	(%rdx,%rsi,4), %edx
	cmpl	$-1, %edx
	je	.LBB9_6
# BB#1:                                 # %.lr.ph.i
	movq	24(%r12), %rbx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	(%rbx,%rsi), %rdi
	cmpl	%eax, 24(%rdi)
	jne	.LBB9_4
# BB#3:                                 # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rbx,%rsi), %rdi
	cmpl	%ecx, 24(%rdi)
	je	.LBB9_5
.LBB9_4:                                # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit.thread.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	104(%r12), %rsi
	movl	(%rsi,%rdx,4), %edx
	cmpl	$-1, %edx
	jne	.LBB9_2
.LBB9_6:                                # %_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i.exit.thread
	movq	%r8, 8(%rsp)            # 8-byte Spill
	leaq	8(%r12), %rdi
	movl	12(%r12), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rsi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_
	movq	%rax, %rbx
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB9_8
# BB#7:
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
.LBB9_8:
	movslq	4(%rsp), %rdi           # 4-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	16(%r12), %eax
	jge	.LBB9_10
# BB#9:
	movq	%rdi, %rbp
	movq	%r12, %rdi
	callq	_ZN28btHashedOverlappingPairCache10growTablesEv
	movq	%rbp, %rdi
	movl	16(%r12), %ebp
	decl	%ebp
	andl	%r13d, %ebp
.LBB9_10:
	movl	24(%r15), %eax
	cmpl	24(%r14), %eax
	movq	%r14, %rax
	cmovlq	%r15, %rax
	cmovlq	%r14, %r15
	movq	%rax, (%rbx)
	movq	%r15, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movslq	%ebp, %rax
	movq	72(%r12), %rcx
	movq	104(%r12), %rdx
	movl	(%rcx,%rax,4), %esi
	movl	%esi, (%rdx,%rdi,4)
	movl	%edi, (%rcx,%rax,4)
.LBB9_11:
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_5:                                # %_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i.exit
	addq	%rsi, %rbx
	jne	.LBB9_11
	jmp	.LBB9_6
.Lfunc_end9:
	.size	_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_, .Lfunc_end9-_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -48
.Lcfi55:
	.cfi_offset %r12, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	4(%r12), %eax
	movslq	%eax, %r14
	cmpl	8(%r12), %r14d
	jne	.LBB10_13
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB10_13
# BB#2:
	testl	%ebp, %ebp
	je	.LBB10_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%r12), %eax
	testl	%eax, %eax
	jg	.LBB10_6
	jmp	.LBB10_8
.LBB10_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB10_8
.LBB10_6:                               # %.lr.ph.i.i
	movslq	%eax, %rcx
	movl	$24, %edx
	.p2align	4, 0x90
.LBB10_7:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movups	-24(%rsi,%rdx), %xmm0
	movups	%xmm0, -24(%rbx,%rdx)
	movq	-8(%rsi,%rdx), %rdi
	movq	%rdi, -8(%rbx,%rdx)
	movq	(%rsi,%rdx), %rsi
	movq	%rsi, (%rbx,%rdx)
	addq	$32, %rdx
	decq	%rcx
	jne	.LBB10_7
.LBB10_8:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_12
# BB#9:
	cmpb	$0, 24(%r12)
	je	.LBB10_11
# BB#10:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%r12), %eax
.LBB10_11:
	movq	$0, 16(%r12)
.LBB10_12:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv.exit.i
	movb	$1, 24(%r12)
	movq	%rbx, 16(%r12)
	movl	%ebp, 8(%r12)
.LBB10_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
	incl	%eax
	movl	%eax, 4(%r12)
	movq	16(%r12), %rax
	shlq	$5, %r14
	movups	(%r15), %xmm0
	movups	%xmm0, (%rax,%r14)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%r14)
	movq	24(%r15), %rcx
	movq	%rcx, 24(%rax,%r14)
	addq	16(%r12), %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_, .Lfunc_end10-_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_
	.cfi_endproc

	.text
	.globl	_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher,@function
_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher: # @_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 80
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, %rbx
	incl	gRemovePairs(%rip)
	movl	24(%rsi), %eax
	cmpl	24(%r12), %eax
	movq	%rsi, %r8
	cmovgq	%r12, %r8
	cmovgq	%rsi, %r12
	movl	24(%r8), %eax
	movl	24(%r12), %edi
	movl	%edi, %edx
	shll	$16, %edx
	orl	%eax, %edx
	movl	%edx, %esi
	shll	$15, %esi
	notl	%esi
	addl	%edx, %esi
	movl	%esi, %edx
	sarl	$10, %edx
	xorl	%esi, %edx
	leal	(%rdx,%rdx,8), %edx
	movl	%edx, %esi
	sarl	$6, %esi
	xorl	%edx, %esi
	movl	%esi, %edx
	shll	$11, %edx
	notl	%edx
	addl	%esi, %edx
	movl	%edx, %esi
	sarl	$16, %esi
	xorl	%edx, %esi
	movl	16(%rbx), %edx
	decl	%edx
	andl	%esi, %edx
	movq	72(%rbx), %rsi
	movslq	%edx, %r14
	movl	(%rsi,%r14,4), %edx
	cmpl	$-1, %edx
	je	.LBB11_22
# BB#1:                                 # %.lr.ph.i
	movq	24(%rbx), %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movq	%rdx, %r13
	shlq	$5, %r13
	movq	(%rbp,%r13), %rsi
	cmpl	%eax, 24(%rsi)
	jne	.LBB11_4
# BB#3:                                 # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	8(%rbp,%r13), %rsi
	cmpl	%edi, 24(%rsi)
	je	.LBB11_6
.LBB11_4:                               # %_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii.exit.thread.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	104(%rbx), %rsi
	movl	(%rsi,%rdx,4), %edx
	cmpl	$-1, %edx
	jne	.LBB11_2
	jmp	.LBB11_23
.LBB11_6:                               # %_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i.exit
	movq	%rbp, %r15
	addq	%r13, %r15
	je	.LBB11_22
# BB#7:
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rdx
	callq	*64(%rax)
	movq	24(%rbp,%r13), %r11
	subq	24(%rbx), %r15
	movq	%r15, %rbp
	shrq	$5, %rbp
	movq	72(%rbx), %rdx
	movq	104(%rbx), %rax
	leaq	(%rdx,%r14,4), %r8
	movl	(%rdx,%r14,4), %edi
	cmpl	%ebp, %edi
	jne	.LBB11_9
# BB#8:                                 # %._crit_edge80.thread
	shlq	$27, %r15
	movslq	%ebp, %r13
	sarq	$30, %r15
	movl	(%rax,%r15), %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB11_12
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %edx
	movslq	%edx, %rsi
	movl	(%rax,%rsi,4), %edi
	cmpl	%ebp, %edi
	jne	.LBB11_9
# BB#10:                                # %._crit_edge80
	shlq	$27, %r15
	sarq	$30, %r15
	cmpl	$-1, %edx
	movslq	%ebp, %r13
	movl	(%rax,%r15), %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB11_12
# BB#11:
	leaq	(%rax,%rsi,4), %r8
.LBB11_12:
	movl	%edx, (%r8)
	movslq	12(%rbx), %r15
	leaq	-1(%r15), %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_14
# BB#13:
	movq	(%rdi), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r15, %r12
	movq	%r11, %r15
	callq	*24(%rax)
	movq	%r15, %r11
	movq	%r12, %r15
.LBB11_14:
	cmpl	%ebp, %r14d
	je	.LBB11_21
# BB#15:
	movq	24(%rbx), %r9
	movq	%r14, %r10
	shlq	$5, %r10
	movq	(%r9,%r10), %rax
	movq	8(%r9,%r10), %rcx
	movl	24(%rcx), %ecx
	shll	$16, %ecx
	orl	24(%rax), %ecx
	movl	%ecx, %eax
	shll	$15, %eax
	notl	%eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	sarl	$10, %ecx
	xorl	%eax, %ecx
	leal	(%rcx,%rcx,8), %eax
	movl	%eax, %ecx
	sarl	$6, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %eax
	shll	$11, %eax
	notl	%eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	sarl	$16, %ecx
	xorl	%eax, %ecx
	movl	16(%rbx), %eax
	decl	%eax
	andl	%ecx, %eax
	movq	72(%rbx), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx,4), %r8
	movl	(%rcx,%rdx,4), %esi
	cmpl	%r14d, %esi
	movq	104(%rbx), %rdi
	jne	.LBB11_17
# BB#16:                                # %._crit_edge.thread
	movl	-4(%rdi,%r15,4), %esi
	jmp	.LBB11_20
	.p2align	4, 0x90
.LBB11_17:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %ecx
	movslq	%ecx, %rax
	movl	(%rdi,%rax,4), %esi
	cmpl	%r14d, %esi
	jne	.LBB11_17
# BB#18:                                # %._crit_edge
	cmpl	$-1, %ecx
	movl	-4(%rdi,%r15,4), %esi
	je	.LBB11_20
# BB#19:
	leaq	(%rdi,%rax,4), %r8
.LBB11_20:
	movl	%esi, (%r8)
	movq	%r13, %rax
	shlq	$5, %rax
	movups	(%r9,%r10), %xmm0
	movups	16(%r9,%r10), %xmm1
	movups	%xmm1, 16(%r9,%rax)
	movups	%xmm0, (%r9,%rax)
	movq	72(%rbx), %rax
	movq	104(%rbx), %rcx
	movl	(%rax,%rdx,4), %esi
	movl	%esi, (%rcx,%r13,4)
	movl	%ebp, (%rax,%rdx,4)
.LBB11_21:                              # %_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i.exit.thread
	decl	12(%rbx)
	jmp	.LBB11_23
.LBB11_22:
	xorl	%r11d, %r11d
.LBB11_23:                              # %_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i.exit.thread
	movq	%r11, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher, .Lfunc_end11-_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_endproc

	.globl	_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher,@function
_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher: # @_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, 12(%r12)
	jle	.LBB12_6
# BB#1:                                 # %.lr.ph.lr.ph
	xorl	%r14d, %r14d
	jmp	.LBB12_2
.LBB12_4:                               # %.outer
                                        #   in Loop: Header=BB12_2 Depth=1
	incq	%r14
	jmp	.LBB12_5
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rbp
	movq	%r14, %r13
	shlq	$5, %r13
	leaq	(%rbp,%r13), %rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	*16(%rax)
	testb	%al, %al
	je	.LBB12_4
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	(%r12), %rax
	movq	(%rbx), %rsi
	movq	8(%rbp,%r13), %rdx
	movq	%r12, %rdi
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	*24(%rax)
	decl	gOverlappingPairs(%rip)
.LBB12_5:                               # %.outer
                                        #   in Loop: Header=BB12_2 Depth=1
	movslq	12(%r12), %rax
	cmpq	%rax, %r14
	jl	.LBB12_2
.LBB12_6:                               # %.outer._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher, .Lfunc_end12-_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_endproc

	.globl	_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher,@function
_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher: # @_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 96
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
	movl	12(%r15), %r8d
	testl	%r8d, %r8d
	jle	.LBB13_1
# BB#23:                                # %.lr.ph38
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_24:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_38 Depth 2
	movq	24(%r15), %r13
	cmpl	%ecx, %edx
	jne	.LBB13_25
# BB#26:                                #   in Loop: Header=BB13_24 Depth=1
	leal	(%rcx,%rcx), %r12d
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovel	%eax, %r12d
	cmpl	%r12d, %ecx
	jge	.LBB13_27
# BB#28:                                #   in Loop: Header=BB13_24 Depth=1
	testl	%r12d, %r12d
	je	.LBB13_29
# BB#30:                                #   in Loop: Header=BB13_24 Depth=1
	movslq	%r12d, %rdi
	shlq	$5, %rdi
.Ltmp37:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp38:
# BB#31:                                # %._ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi.exit.i_crit_edge
                                        #   in Loop: Header=BB13_24 Depth=1
	movl	4(%rsp), %ecx
	testl	%ecx, %ecx
	jg	.LBB13_33
	jmp	.LBB13_39
	.p2align	4, 0x90
.LBB13_25:                              #   in Loop: Header=BB13_24 Depth=1
	movl	%ecx, %r12d
	movl	%edx, %edi
	jmp	.LBB13_46
	.p2align	4, 0x90
.LBB13_27:                              #   in Loop: Header=BB13_24 Depth=1
	movl	%ecx, %r12d
	jmp	.LBB13_45
.LBB13_29:                              #   in Loop: Header=BB13_24 Depth=1
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	jle	.LBB13_39
.LBB13_33:                              # %.lr.ph.i.i
                                        #   in Loop: Header=BB13_24 Depth=1
	movslq	%ecx, %rax
	movq	16(%rsp), %rdi
	testb	$1, %al
	jne	.LBB13_35
# BB#34:                                #   in Loop: Header=BB13_24 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %ecx
	jne	.LBB13_37
	jmp	.LBB13_40
.LBB13_39:                              # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
                                        #   in Loop: Header=BB13_24 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB13_40
	jmp	.LBB13_44
.LBB13_35:                              #   in Loop: Header=BB13_24 Depth=1
	movups	(%rdi), %xmm0
	movups	%xmm0, (%r14)
	movq	16(%rdi), %rdx
	movq	%rdx, 16(%r14)
	movq	24(%rdi), %rdx
	movq	%rdx, 24(%r14)
	movl	$1, %esi
	cmpl	$1, %ecx
	je	.LBB13_40
.LBB13_37:                              # %.lr.ph.i.i.new
                                        #   in Loop: Header=BB13_24 Depth=1
	subq	%rsi, %rax
	shlq	$5, %rsi
	leaq	56(%rdi,%rsi), %rdx
	leaq	56(%r14,%rsi), %rsi
	.p2align	4, 0x90
.LBB13_38:                              #   Parent Loop BB13_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-56(%rdx), %xmm0
	movups	%xmm0, -56(%rsi)
	movq	-40(%rdx), %rbx
	movq	%rbx, -40(%rsi)
	movq	-32(%rdx), %rbx
	movq	%rbx, -32(%rsi)
	movups	-24(%rdx), %xmm0
	movups	%xmm0, -24(%rsi)
	movq	-8(%rdx), %rbx
	movq	%rbx, -8(%rsi)
	movq	(%rdx), %rbx
	movq	%rbx, (%rsi)
	addq	$64, %rdx
	addq	$64, %rsi
	addq	$-2, %rax
	jne	.LBB13_38
.LBB13_40:                              # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i.thread
                                        #   in Loop: Header=BB13_24 Depth=1
	cmpb	$0, 24(%rsp)
	je	.LBB13_43
# BB#41:                                #   in Loop: Header=BB13_24 Depth=1
.Ltmp39:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp40:
# BB#42:                                # %..noexc20_crit_edge
                                        #   in Loop: Header=BB13_24 Depth=1
	movl	4(%rsp), %ecx
.LBB13_43:                              # %.noexc20
                                        #   in Loop: Header=BB13_24 Depth=1
	movq	$0, 16(%rsp)
.LBB13_44:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv.exit.i
                                        #   in Loop: Header=BB13_24 Depth=1
	movb	$1, 24(%rsp)
	movq	%r14, 16(%rsp)
	movl	%r12d, 8(%rsp)
	movl	12(%r15), %r8d
.LBB13_45:                              # %.noexc
                                        #   in Loop: Header=BB13_24 Depth=1
	movl	%ecx, %edi
.LBB13_46:                              # %.noexc
                                        #   in Loop: Header=BB13_24 Depth=1
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	(%r13,%rax), %rcx
	movslq	%edi, %rdx
	movq	%rdx, %rbx
	shlq	$5, %rbx
	movups	(%rcx), %xmm0
	movups	%xmm0, (%r14,%rbx)
	movq	16(%r13,%rax), %rcx
	movq	%rcx, 16(%r14,%rbx)
	movq	24(%r13,%rax), %rax
	movq	%rax, 24(%r14,%rbx)
	incl	%edx
	movl	%edx, 4(%rsp)
	incq	%rbp
	movslq	%r8d, %rax
	cmpq	%rax, %rbp
	movl	%r12d, %ecx
	jl	.LBB13_24
# BB#18:                                # %.preheader25
	testl	%edi, %edi
	js	.LBB13_2
# BB#19:                                # %.lr.ph35
	movl	$8, %ebp
	movl	$1, %ebx
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB13_20
	.p2align	4, 0x90
.LBB13_22:                              # %._crit_edge48
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	16(%rsp), %r14
	addq	$32, %rbp
	incq	%rbx
.LBB13_20:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	-8(%r14,%rbp), %rsi
	movq	(%r14,%rbp), %rdx
.Ltmp42:
	movq	%r15, %rdi
	movq	%r12, %rcx
	callq	*24(%rax)
.Ltmp43:
# BB#21:                                #   in Loop: Header=BB13_20 Depth=1
	movslq	4(%rsp), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB13_22
	jmp	.LBB13_2
.LBB13_1:
	xorl	%edx, %edx
.LBB13_2:                               # %.preheader
	cmpl	$0, 92(%r15)
	jle	.LBB13_6
# BB#3:                                 # %.lr.ph33
	movq	104(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_4:                               # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rax,%rcx,4)
	incq	%rcx
	movslq	92(%r15), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB13_4
# BB#5:                                 # %._crit_edge.loopexit
	movl	4(%rsp), %edx
.LBB13_6:                               # %._crit_edge
	cmpl	$2, %edx
	jl	.LBB13_9
# BB#7:
	decl	%edx
.Ltmp45:
	movq	%rsp, %rdi
	xorl	%esi, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
.Ltmp46:
# BB#8:                                 # %._ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit.preheader_crit_edge
	movl	4(%rsp), %edx
.LBB13_9:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit.preheader
	testl	%edx, %edx
	jle	.LBB13_13
# BB#10:                                # %.lr.ph
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB13_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	16(%rsp), %rcx
	movq	-8(%rcx,%rbp), %rsi
	movq	(%rcx,%rbp), %rdx
.Ltmp48:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp49:
# BB#12:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit
                                        #   in Loop: Header=BB13_11 Depth=1
	incq	%rbx
	movslq	4(%rsp), %rax
	addq	$32, %rbp
	cmpq	%rax, %rbx
	jl	.LBB13_11
.LBB13_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit._crit_edge
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_17
# BB#14:
	cmpb	$0, 24(%rsp)
	je	.LBB13_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB13_16:
	movq	$0, 16(%rsp)
.LBB13_17:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_50:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp47:
	jmp	.LBB13_51
.LBB13_49:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp41:
	jmp	.LBB13_51
.LBB13_48:                              # %.loopexit.split-lp.loopexit
.Ltmp44:
	jmp	.LBB13_51
.LBB13_47:                              # %.loopexit
.Ltmp50:
.LBB13_51:                              # %.loopexit.split-lp
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_55
# BB#52:
	cmpb	$0, 24(%rsp)
	je	.LBB13_54
# BB#53:
.Ltmp51:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp52:
.LBB13_54:                              # %.noexc22
	movq	$0, 16(%rsp)
.LBB13_55:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_56:
.Ltmp53:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher, .Lfunc_end13-_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp37-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp40-.Ltmp37         #   Call between .Ltmp37 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin5   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin5   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin5   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin5   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp51-.Ltmp49         #   Call between .Ltmp49 and .Ltmp51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin5   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Lfunc_end13-.Ltmp52    #   Call between .Ltmp52 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher,@function
_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher: # @_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 64
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*112(%rax)
	testb	%al, %al
	jne	.LBB14_7
# BB#2:
	movl	24(%r13), %eax
	cmpl	24(%r12), %eax
	movq	%r12, %rax
	cmovlq	%r13, %rax
	movq	%r13, %rdx
	cmovlq	%r12, %rdx
	movslq	12(%rbx), %rcx
	testq	%rcx, %rcx
	jle	.LBB14_7
# BB#3:                                 # %.lr.ph.i
	movq	24(%rbx), %rsi
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rax, (%rsi,%rbp)
	jne	.LBB14_6
# BB#5:                                 # %_ZeqRK16btBroadphasePairS1_.exit.i
                                        #   in Loop: Header=BB14_4 Depth=1
	cmpq	%rdx, 8(%rsi,%rbp)
	je	.LBB14_9
.LBB14_6:                               # %_ZeqRK16btBroadphasePairS1_.exit.thread.i
                                        #   in Loop: Header=BB14_4 Depth=1
	incq	%rdi
	addq	$32, %rbp
	cmpq	%rcx, %rdi
	jl	.LBB14_4
	jmp	.LBB14_8
.LBB14_9:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_.exit
	cmpl	%ecx, %edi
	jge	.LBB14_7
# BB#10:
	decl	gOverlappingPairs(%rip)
	movq	(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	24(%rsi,%rbp), %r14
	addq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	*64(%rax)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_12
# BB#11:
	movq	(%rdi), %rax
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	*24(%rax)
.LBB14_12:
	movslq	16(%rbx), %rax
	shlq	$5, %rax
	movq	24(%rbx), %rcx
	movups	(%rcx,%rbp), %xmm0
	movups	16(%rcx,%rbp), %xmm1
	movups	-32(%rcx,%rax), %xmm2
	movups	-16(%rcx,%rax), %xmm3
	movups	%xmm3, 16(%rcx,%rbp)
	movups	%xmm2, (%rcx,%rbp)
	movq	24(%rbx), %rcx
	movups	%xmm0, -32(%rcx,%rax)
	movups	%xmm1, -16(%rcx,%rax)
	decl	12(%rbx)
	jmp	.LBB14_8
.LBB14_7:
	xorl	%r14d, %r14d
.LBB14_8:                               # %.thread
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher, .Lfunc_end14-_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.zero	16
	.text
	.globl	_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,@function
_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_: # @_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 80
.Lcfi116:
	.cfi_offset %rbx, -40
.Lcfi117:
	.cfi_offset %r12, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_3
# BB#1:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	testb	%al, %al
	jne	.LBB15_5
	jmp	.LBB15_8
.LBB15_3:
	movzwl	10(%r14), %eax
	testw	8(%r12), %ax
	je	.LBB15_8
# BB#4:                                 # %_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit
	movzwl	10(%r12), %eax
	testw	8(%r14), %ax
	je	.LBB15_8
.LBB15_5:
	leaq	8(%r15), %rdi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rsi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6expandERKS0_
	movq	%rax, %rbx
	movl	24(%r12), %eax
	cmpl	24(%r14), %eax
	movq	%r14, %rax
	cmovlq	%r12, %rax
	movq	%r12, %rcx
	cmovlq	%r14, %rcx
	movq	%rax, (%rbx)
	movq	%rcx, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	incl	gOverlappingPairs(%rip)
	incl	gAddedPairs(%rip)
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_9
# BB#6:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	jmp	.LBB15_9
.LBB15_8:
	xorl	%ebx, %ebx
.LBB15_9:                               # %_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit.thread
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_, .Lfunc_end15-_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_,@function
_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_: # @_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -40
.Lcfi126:
	.cfi_offset %r12, -32
.Lcfi127:
	.cfi_offset %r14, -24
.Lcfi128:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_3
# BB#1:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	*16(%rax)
	testb	%al, %al
	jne	.LBB16_5
	jmp	.LBB16_15
.LBB16_3:
	movzwl	10(%r15), %eax
	testw	8(%r12), %ax
	je	.LBB16_15
# BB#4:                                 # %_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit
	movzwl	10(%r12), %eax
	testw	8(%r15), %ax
	je	.LBB16_15
.LBB16_5:
	movl	24(%r12), %eax
	cmpl	24(%r15), %eax
	movq	%r15, %rcx
	cmovlq	%r12, %rcx
	cmovlq	%r15, %r12
	movslq	12(%r14), %rdx
	testq	%rdx, %rdx
	jle	.LBB16_15
# BB#6:                                 # %.lr.ph.i
	movq	24(%r14), %rdi
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_7:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, (%rdi,%rsi)
	jne	.LBB16_9
# BB#8:                                 # %_ZeqRK16btBroadphasePairS1_.exit.i
                                        #   in Loop: Header=BB16_7 Depth=1
	cmpq	%r12, 8(%rdi,%rsi)
	je	.LBB16_13
.LBB16_9:                               # %_ZeqRK16btBroadphasePairS1_.exit.thread.i
                                        #   in Loop: Header=BB16_7 Depth=1
	incq	%rbx
	addq	$32, %rsi
	cmpq	%rdx, %rbx
	jl	.LBB16_7
	jmp	.LBB16_16
.LBB16_13:                              # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_.exit
	cmpl	%edx, %ebx
	jge	.LBB16_15
# BB#14:
	addq	24(%r14), %rsi
	movq	%rsi, %rax
	jmp	.LBB16_16
.LBB16_15:
	xorl	%eax, %eax
.LBB16_16:                              # %_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_, .Lfunc_end16-_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.zero	16
	.text
	.globl	_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher,@function
_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher: # @_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 64
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, 12(%r12)
	jle	.LBB17_6
# BB#1:                                 # %.lr.ph.lr.ph
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rbp
	shlq	$5, %rbp
	movq	24(%r12), %rbx
	addq	%rbp, %rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	*16(%rax)
	testb	%al, %al
	je	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	*64(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	12(%r12), %rax
	shlq	$5, %rax
	movq	24(%r12), %rcx
	movups	(%rcx,%rbp), %xmm0
	movups	16(%rcx,%rbp), %xmm1
	movups	-32(%rcx,%rax), %xmm2
	movups	-16(%rcx,%rax), %xmm3
	movups	%xmm3, 16(%rcx,%rbp)
	movups	%xmm2, (%rcx,%rbp)
	movq	24(%r12), %rcx
	movups	%xmm0, -32(%rcx,%rax)
	movups	%xmm1, -16(%rcx,%rax)
	movslq	12(%r12), %rax
	decq	%rax
	movl	%eax, 12(%r12)
	decl	gOverlappingPairs(%rip)
	cmpq	%rax, %r13
	jl	.LBB17_2
	jmp	.LBB17_6
.LBB17_4:                               # %.outer
                                        #   in Loop: Header=BB17_2 Depth=1
	incq	%r13
	movslq	12(%r12), %rax
	cmpq	%rax, %r13
	jl	.LBB17_2
.LBB17_6:                               # %.outer._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher, .Lfunc_end17-_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_endproc

	.globl	_ZN28btSortedOverlappingPairCacheC2Ev
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCacheC2Ev,@function
_ZN28btSortedOverlappingPairCacheC2Ev:  # @_ZN28btSortedOverlappingPairCacheC2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -24
.Lcfi146:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	$_ZTV28btSortedOverlappingPairCache+16, (%r14)
	movb	$1, 32(%r14)
	movq	$0, 24(%r14)
	movl	$0, 12(%r14)
	movl	$0, 16(%r14)
	movb	$0, 40(%r14)
	movb	$1, 41(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r14)
.Ltmp54:
	movl	$64, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp55:
# BB#1:                                 # %.noexc6
	movslq	12(%r14), %rax
	testq	%rax, %rax
	jle	.LBB18_4
# BB#2:                                 # %.lr.ph.i.i
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB18_3:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbx,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbx,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbx,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB18_3
.LBB18_4:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_8
# BB#5:
	cmpb	$0, 32(%r14)
	je	.LBB18_7
# BB#6:
.Ltmp56:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp57:
.LBB18_7:                               # %.noexc7
	movq	$0, 24(%r14)
.LBB18_8:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
	movb	$1, 32(%r14)
	movq	%rbx, 24(%r14)
	movl	$2, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB18_9:
.Ltmp58:
	movq	%rax, %rbx
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_13
# BB#10:
	cmpb	$0, 32(%r14)
	je	.LBB18_12
# BB#11:
.Ltmp59:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp60:
.LBB18_12:                              # %.noexc
	movq	$0, 24(%r14)
.LBB18_13:
	movb	$1, 32(%r14)
	movq	$0, 24(%r14)
	movq	$0, 12(%r14)
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_14:
.Ltmp61:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN28btSortedOverlappingPairCacheC2Ev, .Lfunc_end18-_ZN28btSortedOverlappingPairCacheC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp54-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp57-.Ltmp54         #   Call between .Ltmp54 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin6   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin6   #     jumps to .Ltmp61
	.byte	1                       #   On action: 1
	.long	.Ltmp60-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Lfunc_end18-.Ltmp60    #   Call between .Ltmp60 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btSortedOverlappingPairCacheD2Ev
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCacheD2Ev,@function
_ZN28btSortedOverlappingPairCacheD2Ev:  # @_ZN28btSortedOverlappingPairCacheD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 16
.Lcfi148:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV28btSortedOverlappingPairCache+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_4
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB19_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB19_3:                               # %.noexc
	movq	$0, 24(%rbx)
.LBB19_4:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN28btSortedOverlappingPairCacheD2Ev, .Lfunc_end19-_ZN28btSortedOverlappingPairCacheD2Ev
	.cfi_endproc

	.globl	_ZN28btSortedOverlappingPairCacheD0Ev
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCacheD0Ev,@function
_ZN28btSortedOverlappingPairCacheD0Ev:  # @_ZN28btSortedOverlappingPairCacheD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 32
.Lcfi152:
	.cfi_offset %rbx, -24
.Lcfi153:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV28btSortedOverlappingPairCache+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_3
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB20_3
# BB#2:
.Ltmp62:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp63:
.LBB20_3:                               # %.noexc.i
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_4:
.Ltmp64:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN28btSortedOverlappingPairCacheD0Ev, .Lfunc_end20-_ZN28btSortedOverlappingPairCacheD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp62-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin7   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp63    #   Call between .Ltmp63 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher,@function
_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher: # @_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -24
.Lcfi158:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	(%r14), %rax
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	*104(%rax)
	movq	$0, 16(%rbx)
	decl	gRemovePairs(%rip)
.LBB21_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher, .Lfunc_end21-_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_endproc

	.globl	_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher,@function
_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher: # @_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	subq	$40, %rsp
.Lcfi159:
	.cfi_def_cfa_offset 48
	movq	$_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback+16, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 24(%rsp)
	movq	%rdx, 32(%rsp)
	movq	(%rdi), %rax
.Ltmp65:
	leaq	8(%rsp), %rsi
	callq	*96(%rax)
.Ltmp66:
# BB#1:
	addq	$40, %rsp
	retq
.LBB22_2:
.Ltmp67:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end22-_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp65-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin8   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp66    #   Call between .Ltmp66 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	subq	$24, %rsp
.Lcfi160:
	.cfi_def_cfa_offset 32
	movq	$_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback+16, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	(%rdi), %rax
.Ltmp68:
	leaq	8(%rsp), %rsi
	callq	*96(%rax)
.Ltmp69:
# BB#1:
	addq	$24, %rsp
	retq
.LBB23_2:
.Ltmp70:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end23-_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp68-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin9   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp69    #   Call between .Ltmp69 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN17btOverlapCallbackD2Ev,"axG",@progbits,_ZN17btOverlapCallbackD2Ev,comdat
	.weak	_ZN17btOverlapCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN17btOverlapCallbackD2Ev,@function
_ZN17btOverlapCallbackD2Ev:             # @_ZN17btOverlapCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end24:
	.size	_ZN17btOverlapCallbackD2Ev, .Lfunc_end24-_ZN17btOverlapCallbackD2Ev
	.cfi_endproc

	.text
	.globl	_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher,@function
_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher: # @_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end25:
	.size	_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher, .Lfunc_end25-_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,"axG",@progbits,_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,comdat
	.weak	_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,@function
_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_: # @_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -32
.Lcfi165:
	.cfi_offset %r14, -24
.Lcfi166:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	incl	gAddedPairs(%rip)
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB26_3
# BB#1:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	*16(%rax)
	testb	%al, %al
	jne	.LBB26_2
	jmp	.LBB26_5
.LBB26_3:
	movzwl	10(%r14), %eax
	testw	8(%rbx), %ax
	je	.LBB26_5
# BB#4:                                 # %_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit
	movzwl	10(%rbx), %eax
	testw	8(%r14), %ax
	je	.LBB26_5
.LBB26_2:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_ # TAILCALL
.LBB26_5:                               # %_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_.exit.thread
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_, .Lfunc_end26-_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,@function
_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv: # @_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end27:
	.size	_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end27-_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv,@function
_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv: # @_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end28:
	.size	_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end28-_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv,"axG",@progbits,_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv,comdat
	.weak	_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv,@function
_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv: # @_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	retq
.Lfunc_end29:
	.size	_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv, .Lfunc_end29-_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv
	.cfi_endproc

	.section	.text._ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv,"axG",@progbits,_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv,comdat
	.weak	_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv
	.p2align	4, 0x90
	.type	_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv,@function
_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv: # @_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	retq
.Lfunc_end30:
	.size	_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv, .Lfunc_end30-_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,"axG",@progbits,_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,comdat
	.weak	_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,@function
_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback: # @_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_startproc
# BB#0:
	movq	%rsi, 40(%rdi)
	retq
.Lfunc_end31:
	.size	_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback, .Lfunc_end31-_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv,"axG",@progbits,_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv,comdat
	.weak	_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv,@function
_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv: # @_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end32:
	.size	_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv, .Lfunc_end32-_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv
	.cfi_endproc

	.section	.text._ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,"axG",@progbits,_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,comdat
	.weak	_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.p2align	4, 0x90
	.type	_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,@function
_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback: # @_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end33:
	.size	_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback, .Lfunc_end33-_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_endproc

	.section	.text._ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,@function
_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv: # @_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end34:
	.size	_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end34-_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv,@function
_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv: # @_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end35:
	.size	_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end35-_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv,"axG",@progbits,_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv,comdat
	.weak	_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv,@function
_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv: # @_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	retq
.Lfunc_end36:
	.size	_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv, .Lfunc_end36-_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv
	.cfi_endproc

	.section	.text._ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv,"axG",@progbits,_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv,comdat
	.weak	_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv
	.p2align	4, 0x90
	.type	_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv,@function
_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv: # @_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	retq
.Lfunc_end37:
	.size	_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv, .Lfunc_end37-_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv
	.cfi_endproc

	.section	.text._ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,"axG",@progbits,_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,comdat
	.weak	_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,@function
_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback: # @_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_startproc
# BB#0:
	movq	%rsi, 48(%rdi)
	retq
.Lfunc_end38:
	.size	_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback, .Lfunc_end38-_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_endproc

	.section	.text._ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv,"axG",@progbits,_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv,comdat
	.weak	_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv,@function
_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv: # @_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv
	.cfi_startproc
# BB#0:
	movb	41(%rdi), %al
	retq
.Lfunc_end39:
	.size	_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv, .Lfunc_end39-_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv
	.cfi_endproc

	.section	.text._ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,"axG",@progbits,_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,comdat
	.weak	_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.p2align	4, 0x90
	.type	_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,@function
_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback: # @_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_startproc
# BB#0:
	movq	%rsi, 56(%rdi)
	retq
.Lfunc_end40:
	.size	_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback, .Lfunc_end40-_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev,@function
_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev: # @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end41:
	.size	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev, .Lfunc_end41-_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair,@function
_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair: # @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	je	.LBB42_2
# BB#1:
	cmpq	%rax, 8(%rsi)
	jne	.LBB42_3
.LBB42_2:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*64(%rcx)
.LBB42_3:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end42:
	.size	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair, .Lfunc_end42-_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev,@function
_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev: # @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end43:
	.size	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev, .Lfunc_end43-_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair,@function
_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair: # @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	sete	%cl
	cmpq	%rax, 8(%rsi)
	sete	%al
	orb	%cl, %al
	retq
.Lfunc_end44:
	.size	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair, .Lfunc_end44-_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev,@function
_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev: # @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end45:
	.size	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev, .Lfunc_end45-_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair,@function
_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair: # @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	je	.LBB46_2
# BB#1:
	cmpq	%rax, 8(%rsi)
	jne	.LBB46_3
.LBB46_2:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*64(%rcx)
.LBB46_3:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end46:
	.size	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair, .Lfunc_end46-_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev,@function
_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev: # @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end47:
	.size	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev, .Lfunc_end47-_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair,@function
_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair: # @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	sete	%cl
	cmpq	%rax, 8(%rsi)
	sete	%al
	orb	%cl, %al
	retq
.Lfunc_end48:
	.size	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair, .Lfunc_end48-_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 64
.Lcfi176:
	.cfi_offset %rbx, -56
.Lcfi177:
	.cfi_offset %r12, -48
.Lcfi178:
	.cfi_offset %r13, -40
.Lcfi179:
	.cfi_offset %r14, -32
.Lcfi180:
	.cfi_offset %r15, -24
.Lcfi181:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	movq	%rdi, %r15
	movq	%rdx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB49_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB49_2 Depth 2
                                        #       Child Loop BB49_20 Depth 3
                                        #       Child Loop BB49_4 Depth 3
                                        #       Child Loop BB49_57 Depth 3
                                        #       Child Loop BB49_33 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	(%r8,%rax), %r10
	movq	8(%r8,%rax), %r13
	movq	16(%r8,%rax), %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	jmp	.LBB49_2
	.p2align	4, 0x90
.LBB49_48:                              # %._crit_edge
                                        #   in Loop: Header=BB49_2 Depth=2
	movq	16(%r15), %r8
.LBB49_2:                               #   Parent Loop BB49_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB49_20 Depth 3
                                        #       Child Loop BB49_4 Depth 3
                                        #       Child Loop BB49_57 Depth 3
                                        #       Child Loop BB49_33 Depth 3
	movslq	%r12d, %r12
	testq	%r10, %r10
	je	.LBB49_3
# BB#19:                                # %.split.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	movl	24(%r10), %r11d
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB49_20
	.p2align	4, 0x90
.LBB49_55:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread
                                        #   in Loop: Header=BB49_20 Depth=3
	incq	%r12
	addq	$32, %rax
.LBB49_20:                              # %.split
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %ebx
	testq	%rdi, %rdi
	movl	$-1, %ebp
	je	.LBB49_22
# BB#21:                                #   in Loop: Header=BB49_20 Depth=3
	movl	24(%rdi), %ebp
.LBB49_22:                              #   in Loop: Header=BB49_20 Depth=3
	movq	-8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB49_24
# BB#23:                                #   in Loop: Header=BB49_20 Depth=3
	movl	24(%rcx), %ebx
.LBB49_24:                              #   in Loop: Header=BB49_20 Depth=3
	testq	%r13, %r13
	je	.LBB49_25
# BB#26:                                #   in Loop: Header=BB49_20 Depth=3
	movl	24(%r13), %r14d
	cmpl	%r11d, %ebp
	jg	.LBB49_55
	jmp	.LBB49_28
	.p2align	4, 0x90
.LBB49_25:                              #   in Loop: Header=BB49_20 Depth=3
	movl	$-1, %r14d
	cmpl	%r11d, %ebp
	jg	.LBB49_55
.LBB49_28:                              #   in Loop: Header=BB49_20 Depth=3
	cmpq	%r10, %rdi
	setne	%bpl
	cmpl	%r14d, %ebx
	jg	.LBB49_53
# BB#29:                                #   in Loop: Header=BB49_20 Depth=3
	testb	%bpl, %bpl
	jne	.LBB49_53
# BB#30:                                #   in Loop: Header=BB49_20 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB49_31
# BB#52:                                #   in Loop: Header=BB49_20 Depth=3
	cmpq	%r9, (%rax)
	ja	.LBB49_55
	jmp	.LBB49_31
	.p2align	4, 0x90
.LBB49_53:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB49_20 Depth=3
	cmpq	%r10, %rdi
	jne	.LBB49_31
# BB#54:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB49_20 Depth=3
	cmpl	%r14d, %ebx
	jg	.LBB49_55
	jmp	.LBB49_31
	.p2align	4, 0x90
.LBB49_3:                               # %.split.us.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rdi
	jmp	.LBB49_4
	.p2align	4, 0x90
.LBB49_18:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread.us
                                        #   in Loop: Header=BB49_4 Depth=3
	incq	%r12
	addq	$32, %rdi
.LBB49_4:                               # %.split.us
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rdi), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %eax
	je	.LBB49_6
# BB#5:                                 #   in Loop: Header=BB49_4 Depth=3
	movl	24(%rbx), %eax
.LBB49_6:                               #   in Loop: Header=BB49_4 Depth=3
	movq	-8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB49_8
# BB#7:                                 #   in Loop: Header=BB49_4 Depth=3
	movl	24(%rcx), %r11d
.LBB49_8:                               #   in Loop: Header=BB49_4 Depth=3
	testq	%r13, %r13
	je	.LBB49_9
# BB#10:                                #   in Loop: Header=BB49_4 Depth=3
	movl	24(%r13), %ebp
	testl	%eax, %eax
	jns	.LBB49_18
	jmp	.LBB49_12
	.p2align	4, 0x90
.LBB49_9:                               #   in Loop: Header=BB49_4 Depth=3
	movl	$-1, %ebp
	testl	%eax, %eax
	jns	.LBB49_18
.LBB49_12:                              #   in Loop: Header=BB49_4 Depth=3
	testq	%rbx, %rbx
	setne	%al
	cmpl	%ebp, %r11d
	jg	.LBB49_16
# BB#13:                                #   in Loop: Header=BB49_4 Depth=3
	testb	%al, %al
	jne	.LBB49_16
# BB#14:                                #   in Loop: Header=BB49_4 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB49_31
# BB#15:                                #   in Loop: Header=BB49_4 Depth=3
	cmpq	%r9, (%rdi)
	ja	.LBB49_18
	jmp	.LBB49_31
.LBB49_16:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB49_4 Depth=3
	testq	%rbx, %rbx
	jne	.LBB49_31
# BB#17:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB49_4 Depth=3
	cmpl	%ebp, %r11d
	jg	.LBB49_18
	.p2align	4, 0x90
.LBB49_31:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	movslq	%edx, %rdx
	testq	%r10, %r10
	je	.LBB49_32
# BB#56:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader47
                                        #   in Loop: Header=BB49_2 Depth=2
	movl	24(%r10), %r11d
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB49_57
	.p2align	4, 0x90
.LBB49_71:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread
                                        #   in Loop: Header=BB49_57 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB49_57:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %r14d
	testq	%rdi, %rdi
	movl	$-1, %ecx
	je	.LBB49_59
# BB#58:                                #   in Loop: Header=BB49_57 Depth=3
	movl	24(%rdi), %ecx
.LBB49_59:                              #   in Loop: Header=BB49_57 Depth=3
	testq	%r13, %r13
	je	.LBB49_61
# BB#60:                                #   in Loop: Header=BB49_57 Depth=3
	movl	24(%r13), %r14d
.LBB49_61:                              #   in Loop: Header=BB49_57 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB49_62
# BB#63:                                #   in Loop: Header=BB49_57 Depth=3
	movl	24(%rbp), %ebx
	cmpl	%ecx, %r11d
	jg	.LBB49_71
	jmp	.LBB49_65
	.p2align	4, 0x90
.LBB49_62:                              #   in Loop: Header=BB49_57 Depth=3
	movl	$-1, %ebx
	cmpl	%ecx, %r11d
	jg	.LBB49_71
.LBB49_65:                              #   in Loop: Header=BB49_57 Depth=3
	cmpq	%rdi, %r10
	setne	%cl
	cmpl	%ebx, %r14d
	jg	.LBB49_69
# BB#66:                                #   in Loop: Header=BB49_57 Depth=3
	testb	%cl, %cl
	jne	.LBB49_69
# BB#67:                                #   in Loop: Header=BB49_57 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB49_45
# BB#68:                                #   in Loop: Header=BB49_57 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB49_71
	jmp	.LBB49_45
	.p2align	4, 0x90
.LBB49_69:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB49_57 Depth=3
	cmpq	%rdi, %r10
	jne	.LBB49_45
# BB#70:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB49_57 Depth=3
	cmpl	%ebx, %r14d
	jg	.LBB49_71
	jmp	.LBB49_45
	.p2align	4, 0x90
.LBB49_32:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB49_33
	.p2align	4, 0x90
.LBB49_51:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread.us
                                        #   in Loop: Header=BB49_33 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB49_33:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %ecx
	je	.LBB49_35
# BB#34:                                #   in Loop: Header=BB49_33 Depth=3
	movl	24(%rbx), %ecx
.LBB49_35:                              #   in Loop: Header=BB49_33 Depth=3
	testq	%r13, %r13
	je	.LBB49_37
# BB#36:                                #   in Loop: Header=BB49_33 Depth=3
	movl	24(%r13), %r11d
.LBB49_37:                              #   in Loop: Header=BB49_33 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB49_38
# BB#39:                                #   in Loop: Header=BB49_33 Depth=3
	movl	24(%rbp), %edi
	cmpl	$-1, %ecx
	jl	.LBB49_51
	jmp	.LBB49_41
	.p2align	4, 0x90
.LBB49_38:                              #   in Loop: Header=BB49_33 Depth=3
	movl	$-1, %edi
	cmpl	$-1, %ecx
	jl	.LBB49_51
.LBB49_41:                              #   in Loop: Header=BB49_33 Depth=3
	testq	%rbx, %rbx
	setne	%cl
	cmpl	%edi, %r11d
	jg	.LBB49_49
# BB#42:                                #   in Loop: Header=BB49_33 Depth=3
	testb	%cl, %cl
	jne	.LBB49_49
# BB#43:                                #   in Loop: Header=BB49_33 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB49_45
# BB#44:                                #   in Loop: Header=BB49_33 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB49_51
	jmp	.LBB49_45
	.p2align	4, 0x90
.LBB49_49:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB49_33 Depth=3
	testq	%rbx, %rbx
	jne	.LBB49_45
# BB#50:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB49_33 Depth=3
	cmpl	%edi, %r11d
	jg	.LBB49_51
	.p2align	4, 0x90
.LBB49_45:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread41
                                        #   in Loop: Header=BB49_2 Depth=2
	cmpl	%edx, %r12d
	jg	.LBB49_47
# BB#46:                                #   in Loop: Header=BB49_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	movups	(%r8,%rax), %xmm0
	movups	16(%r8,%rax), %xmm1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	movups	(%r8,%rcx), %xmm2
	movups	16(%r8,%rcx), %xmm3
	movups	%xmm3, 16(%r8,%rax)
	movups	%xmm2, (%r8,%rax)
	movq	16(%r15), %rax
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	leal	1(%r12), %r12d
	leal	-1(%rdx), %edx
.LBB49_47:                              #   in Loop: Header=BB49_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB49_48
# BB#72:                                #   in Loop: Header=BB49_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB49_74
# BB#73:                                #   in Loop: Header=BB49_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
.LBB49_74:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpl	%edx, %r12d
	jl	.LBB49_1
# BB#75:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end49:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii, .Lfunc_end49-_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_endproc

	.type	gOverlappingPairs,@object # @gOverlappingPairs
	.bss
	.globl	gOverlappingPairs
	.p2align	2
gOverlappingPairs:
	.long	0                       # 0x0
	.size	gOverlappingPairs, 4

	.type	gRemovePairs,@object    # @gRemovePairs
	.globl	gRemovePairs
	.p2align	2
gRemovePairs:
	.long	0                       # 0x0
	.size	gRemovePairs, 4

	.type	gAddedPairs,@object     # @gAddedPairs
	.globl	gAddedPairs
	.p2align	2
gAddedPairs:
	.long	0                       # 0x0
	.size	gAddedPairs, 4

	.type	gFindPairs,@object      # @gFindPairs
	.globl	gFindPairs
	.p2align	2
gFindPairs:
	.long	0                       # 0x0
	.size	gFindPairs, 4

	.type	_ZTV28btHashedOverlappingPairCache,@object # @_ZTV28btHashedOverlappingPairCache
	.section	.rodata,"a",@progbits
	.globl	_ZTV28btHashedOverlappingPairCache
	.p2align	3
_ZTV28btHashedOverlappingPairCache:
	.quad	0
	.quad	_ZTI28btHashedOverlappingPairCache
	.quad	_ZN28btHashedOverlappingPairCacheD2Ev
	.quad	_ZN28btHashedOverlappingPairCacheD0Ev
	.quad	_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.quad	_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.quad	_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv
	.quad	_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.quad	_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv
	.quad	_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.quad	_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.quad	_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.quad	_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv
	.quad	_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.quad	_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.size	_ZTV28btHashedOverlappingPairCache, 152

	.type	_ZTV28btSortedOverlappingPairCache,@object # @_ZTV28btSortedOverlappingPairCache
	.globl	_ZTV28btSortedOverlappingPairCache
	.p2align	3
_ZTV28btSortedOverlappingPairCache:
	.quad	0
	.quad	_ZTI28btSortedOverlappingPairCache
	.quad	_ZN28btSortedOverlappingPairCacheD2Ev
	.quad	_ZN28btSortedOverlappingPairCacheD0Ev
	.quad	_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.quad	_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.quad	_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv
	.quad	_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.quad	_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv
	.quad	_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.quad	_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.quad	_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_
	.quad	_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv
	.quad	_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.quad	_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher
	.size	_ZTV28btSortedOverlappingPairCache, 152

	.type	_ZTS28btHashedOverlappingPairCache,@object # @_ZTS28btHashedOverlappingPairCache
	.globl	_ZTS28btHashedOverlappingPairCache
	.p2align	4
_ZTS28btHashedOverlappingPairCache:
	.asciz	"28btHashedOverlappingPairCache"
	.size	_ZTS28btHashedOverlappingPairCache, 31

	.type	_ZTS22btOverlappingPairCache,@object # @_ZTS22btOverlappingPairCache
	.section	.rodata._ZTS22btOverlappingPairCache,"aG",@progbits,_ZTS22btOverlappingPairCache,comdat
	.weak	_ZTS22btOverlappingPairCache
	.p2align	4
_ZTS22btOverlappingPairCache:
	.asciz	"22btOverlappingPairCache"
	.size	_ZTS22btOverlappingPairCache, 25

	.type	_ZTS25btOverlappingPairCallback,@object # @_ZTS25btOverlappingPairCallback
	.section	.rodata._ZTS25btOverlappingPairCallback,"aG",@progbits,_ZTS25btOverlappingPairCallback,comdat
	.weak	_ZTS25btOverlappingPairCallback
	.p2align	4
_ZTS25btOverlappingPairCallback:
	.asciz	"25btOverlappingPairCallback"
	.size	_ZTS25btOverlappingPairCallback, 28

	.type	_ZTI25btOverlappingPairCallback,@object # @_ZTI25btOverlappingPairCallback
	.section	.rodata._ZTI25btOverlappingPairCallback,"aG",@progbits,_ZTI25btOverlappingPairCallback,comdat
	.weak	_ZTI25btOverlappingPairCallback
	.p2align	3
_ZTI25btOverlappingPairCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS25btOverlappingPairCallback
	.size	_ZTI25btOverlappingPairCallback, 16

	.type	_ZTI22btOverlappingPairCache,@object # @_ZTI22btOverlappingPairCache
	.section	.rodata._ZTI22btOverlappingPairCache,"aG",@progbits,_ZTI22btOverlappingPairCache,comdat
	.weak	_ZTI22btOverlappingPairCache
	.p2align	4
_ZTI22btOverlappingPairCache:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btOverlappingPairCache
	.quad	_ZTI25btOverlappingPairCallback
	.size	_ZTI22btOverlappingPairCache, 24

	.type	_ZTI28btHashedOverlappingPairCache,@object # @_ZTI28btHashedOverlappingPairCache
	.section	.rodata,"a",@progbits
	.globl	_ZTI28btHashedOverlappingPairCache
	.p2align	4
_ZTI28btHashedOverlappingPairCache:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28btHashedOverlappingPairCache
	.quad	_ZTI22btOverlappingPairCache
	.size	_ZTI28btHashedOverlappingPairCache, 24

	.type	_ZTS28btSortedOverlappingPairCache,@object # @_ZTS28btSortedOverlappingPairCache
	.globl	_ZTS28btSortedOverlappingPairCache
	.p2align	4
_ZTS28btSortedOverlappingPairCache:
	.asciz	"28btSortedOverlappingPairCache"
	.size	_ZTS28btSortedOverlappingPairCache, 31

	.type	_ZTI28btSortedOverlappingPairCache,@object # @_ZTI28btSortedOverlappingPairCache
	.globl	_ZTI28btSortedOverlappingPairCache
	.p2align	4
_ZTI28btSortedOverlappingPairCache:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28btSortedOverlappingPairCache
	.quad	_ZTI22btOverlappingPairCache
	.size	_ZTI28btSortedOverlappingPairCache, 24

	.type	_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.p2align	3
_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.quad	0
	.quad	_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.quad	_ZN17btOverlapCallbackD2Ev
	.quad	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.quad	_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.size	_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 40

	.type	_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.p2align	4
_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.asciz	"ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback"
	.size	_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 110

	.type	_ZTS17btOverlapCallback,@object # @_ZTS17btOverlapCallback
	.section	.rodata._ZTS17btOverlapCallback,"aG",@progbits,_ZTS17btOverlapCallback,comdat
	.weak	_ZTS17btOverlapCallback
	.p2align	4
_ZTS17btOverlapCallback:
	.asciz	"17btOverlapCallback"
	.size	_ZTS17btOverlapCallback, 20

	.type	_ZTI17btOverlapCallback,@object # @_ZTI17btOverlapCallback
	.section	.rodata._ZTI17btOverlapCallback,"aG",@progbits,_ZTI17btOverlapCallback,comdat
	.weak	_ZTI17btOverlapCallback
	.p2align	3
_ZTI17btOverlapCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17btOverlapCallback
	.size	_ZTI17btOverlapCallback, 16

	.type	_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.quad	_ZTI17btOverlapCallback
	.size	_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 24

	.type	_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	3
_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.quad	0
	.quad	_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.quad	_ZN17btOverlapCallbackD2Ev
	.quad	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.quad	_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.size	_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 40

	.type	_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	4
_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.asciz	"ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback"
	.size	_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 129

	.type	_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	4
_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.quad	_ZTI17btOverlapCallback
	.size	_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 24

	.type	_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.p2align	3
_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.quad	0
	.quad	_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.quad	_ZN17btOverlapCallbackD2Ev
	.quad	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev
	.quad	_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair
	.size	_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 40

	.type	_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.p2align	4
_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.asciz	"ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback"
	.size	_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 110

	.type	_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback,@object # @_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.p2align	4
_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback
	.quad	_ZTI17btOverlapCallback
	.size	_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, 24

	.type	_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	3
_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.quad	0
	.quad	_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.quad	_ZN17btOverlapCallbackD2Ev
	.quad	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev
	.quad	_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair
	.size	_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 40

	.type	_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	4
_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.asciz	"ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback"
	.size	_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 129

	.type	_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback,@object # @_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.p2align	4
_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback
	.quad	_ZTI17btOverlapCallback
	.size	_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, 24


	.globl	_ZN28btHashedOverlappingPairCacheC1Ev
	.type	_ZN28btHashedOverlappingPairCacheC1Ev,@function
_ZN28btHashedOverlappingPairCacheC1Ev = _ZN28btHashedOverlappingPairCacheC2Ev
	.globl	_ZN28btHashedOverlappingPairCacheD1Ev
	.type	_ZN28btHashedOverlappingPairCacheD1Ev,@function
_ZN28btHashedOverlappingPairCacheD1Ev = _ZN28btHashedOverlappingPairCacheD2Ev
	.globl	_ZN28btSortedOverlappingPairCacheC1Ev
	.type	_ZN28btSortedOverlappingPairCacheC1Ev,@function
_ZN28btSortedOverlappingPairCacheC1Ev = _ZN28btSortedOverlappingPairCacheC2Ev
	.globl	_ZN28btSortedOverlappingPairCacheD1Ev
	.type	_ZN28btSortedOverlappingPairCacheD1Ev,@function
_ZN28btSortedOverlappingPairCacheD1Ev = _ZN28btSortedOverlappingPairCacheD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
