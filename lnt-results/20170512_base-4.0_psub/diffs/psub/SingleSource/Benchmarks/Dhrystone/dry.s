	.text
	.file	"dry.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	Proc0
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	Proc0
	.p2align	4, 0x90
	.type	Proc0,@function
Proc0:                                  # @Proc0
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	callq	clock
	callq	clock
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, PtrGlbNext(%rip)
	movl	$56, %edi
	callq	malloc
	movq	%rax, PtrGlb(%rip)
	movq	%rbx, (%rax)
	movabsq	$42953967927296, %rcx   # imm = 0x271100000000
	movq	%rcx, 8(%rax)
	movl	$40, 16(%rax)
	movups	.L.str+15(%rip), %xmm0
	movups	%xmm0, 35(%rax)
	movups	.L.str(%rip), %xmm0
	movups	%xmm0, 20(%rax)
	movl	$10, Array2Glob+1660(%rip)
	callq	clock
	xorl	%eax, %eax
	movabsq	$34359738376, %rcx      # imm = 0x800000008
	.p2align	4, 0x90
.LBB1_1:                                # %Func2.exit.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movb	$65, Char1Glob(%rip)
	movb	$66, Char2Glob(%rip)
	movl	$7, Array1Glob+32(%rip)
	movl	$7, Array1Glob+36(%rip)
	movl	$8, Array1Glob+152(%rip)
	movq	%rcx, Array2Glob+1664(%rip)
	incl	Array2Glob+1660(%rip)
	movl	$7, Array2Glob+5744(%rip)
	movq	PtrGlb(%rip), %rdx
	movl	$5, 16(%rdx)
	movq	(%rdx), %rdx
	movl	$5, 16(%rdx)
	movq	%rdx, (%rdx)
	movb	Char2Glob(%rip), %dl
	cmpb	$65, %dl
	jl	.LBB1_4
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movb	$65, %bl
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incb	%bl
	cmpb	%dl, %bl
	jle	.LBB1_3
.LBB1_4:                                # %Proc2.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	incl	%eax
	cmpl	$100000000, %eax        # imm = 0x5F5E100
	jne	.LBB1_1
# BB#5:
	movl	$1, BoolGlob(%rip)
	movl	$5, IntGlob(%rip)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Proc0, .Lfunc_end1-Proc0
	.cfi_endproc

	.globl	Proc1
	.p2align	4, 0x90
	.type	Proc1,@function
Proc1:                                  # @Proc1
	.cfi_startproc
# BB#0:
	movl	$5, 16(%rdi)
	movq	(%rdi), %rax
	movl	$5, 16(%rax)
	movq	%rax, (%rax)
	retq
.Lfunc_end2:
	.size	Proc1, .Lfunc_end2-Proc1
	.cfi_endproc

	.globl	Proc2
	.p2align	4, 0x90
	.type	Proc2,@function
Proc2:                                  # @Proc2
	.cfi_startproc
# BB#0:
	cmpb	$65, Char1Glob(%rip)
	jne	.LBB3_2
# BB#1:
	movl	(%rdi), %eax
	addl	$9, %eax
	subl	IntGlob(%rip), %eax
	movl	%eax, (%rdi)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	Proc2, .Lfunc_end3-Proc2
	.cfi_endproc

	.globl	Proc3
	.p2align	4, 0x90
	.type	Proc3,@function
Proc3:                                  # @Proc3
	.cfi_startproc
# BB#0:
	movq	PtrGlb(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movl	IntGlob(%rip), %eax
	movq	PtrGlb(%rip), %rcx
	addl	$12, %eax
	movl	%eax, 16(%rcx)
	retq
.Lfunc_end4:
	.size	Proc3, .Lfunc_end4-Proc3
	.cfi_endproc

	.globl	Proc4
	.p2align	4, 0x90
	.type	Proc4,@function
Proc4:                                  # @Proc4
	.cfi_startproc
# BB#0:
	movb	$66, Char2Glob(%rip)
	retq
.Lfunc_end5:
	.size	Proc4, .Lfunc_end5-Proc4
	.cfi_endproc

	.globl	Proc5
	.p2align	4, 0x90
	.type	Proc5,@function
Proc5:                                  # @Proc5
	.cfi_startproc
# BB#0:
	movb	$65, Char1Glob(%rip)
	movl	$0, BoolGlob(%rip)
	retq
.Lfunc_end6:
	.size	Proc5, .Lfunc_end6-Proc5
	.cfi_endproc

	.globl	Proc6
	.p2align	4, 0x90
	.type	Proc6,@function
Proc6:                                  # @Proc6
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$10001, %edi            # imm = 0x2711
	setne	%al
	addl	$10001, %eax            # imm = 0x2711
	movl	%eax, (%rsi)
	cmpl	$10000, %edi            # imm = 0x2710
	jg	.LBB7_4
# BB#1:
	testl	%edi, %edi
	je	.LBB7_6
# BB#2:
	cmpl	$10000, %edi            # imm = 0x2710
	jne	.LBB7_10
# BB#3:
	xorl	%ecx, %ecx
	cmpl	$100, IntGlob(%rip)
	movl	$10002, %eax            # imm = 0x2712
	cmovgl	%ecx, %eax
	jmp	.LBB7_9
.LBB7_4:
	cmpl	$10001, %edi            # imm = 0x2711
	je	.LBB7_5
# BB#7:
	cmpl	$10003, %edi            # imm = 0x2713
	jne	.LBB7_10
# BB#8:
	movl	$10001, %eax            # imm = 0x2711
	jmp	.LBB7_9
.LBB7_6:
	xorl	%eax, %eax
	jmp	.LBB7_9
.LBB7_5:
	movl	$10000, %eax            # imm = 0x2710
.LBB7_9:                                # %.sink.split
	movl	%eax, (%rsi)
.LBB7_10:
	retq
.Lfunc_end7:
	.size	Proc6, .Lfunc_end7-Proc6
	.cfi_endproc

	.globl	Proc7
	.p2align	4, 0x90
	.type	Proc7,@function
Proc7:                                  # @Proc7
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	2(%rdi,%rsi), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end8:
	.size	Proc7, .Lfunc_end8-Proc7
	.cfi_endproc

	.globl	Proc8
	.p2align	4, 0x90
	.type	Proc8,@function
Proc8:                                  # @Proc8
	.cfi_startproc
# BB#0:
	movslq	%edx, %rax
	leaq	5(%rax), %r8
	leal	5(%rax), %edx
	movl	%ecx, 20(%rdi,%rax,4)
	movl	%ecx, 24(%rdi,%rax,4)
	movl	%edx, 140(%rdi,%rax,4)
	imulq	$204, %r8, %rcx
	addq	%rsi, %rcx
	movl	%edx, 20(%rcx,%rax,4)
	movl	%edx, 24(%rcx,%rax,4)
	incl	16(%rcx,%rax,4)
	movl	20(%rdi,%rax,4), %ecx
	leal	25(%rax), %edx
	movslq	%edx, %rdx
	imulq	$204, %rdx, %rdx
	addq	%rsi, %rdx
	movl	%ecx, 20(%rdx,%rax,4)
	movl	$5, IntGlob(%rip)
	retq
.Lfunc_end9:
	.size	Proc8, .Lfunc_end9-Proc8
	.cfi_endproc

	.globl	Func1
	.p2align	4, 0x90
	.type	Func1,@function
Func1:                                  # @Func1
	.cfi_startproc
# BB#0:
	xorl	%ecx, %ecx
	xorb	%dil, %sil
	movl	$10000, %eax            # imm = 0x2710
	cmovnel	%ecx, %eax
	retq
.Lfunc_end10:
	.size	Func1, .Lfunc_end10-Func1
	.cfi_endproc

	.globl	Func2
	.p2align	4, 0x90
	.type	Func2,@function
Func2:                                  # @Func2
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	.p2align	4, 0x90
.LBB11_1:                               # %select.unfold
                                        # =>This Inner Loop Header: Depth=1
	cltq
	leaq	1(%rax), %rcx
	movzbl	1(%rsi,%rax), %edx
	cmpb	(%rdi,%rax), %dl
	cmovnel	%ecx, %eax
	cmpl	$2, %eax
	jl	.LBB11_1
# BB#2:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end11:
	.size	Func2, .Lfunc_end11-Func2
	.cfi_endproc

	.globl	Func3
	.p2align	4, 0x90
	.type	Func3,@function
Func3:                                  # @Func3
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$10001, %edi            # imm = 0x2711
	sete	%al
	retq
.Lfunc_end12:
	.size	Func3, .Lfunc_end12-Func3
	.cfi_endproc

	.type	Version,@object         # @Version
	.data
	.globl	Version
Version:
	.asciz	"1.1"
	.size	Version, 4

	.type	PtrGlbNext,@object      # @PtrGlbNext
	.comm	PtrGlbNext,8,8
	.type	PtrGlb,@object          # @PtrGlb
	.comm	PtrGlb,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"DHRYSTONE PROGRAM, SOME STRING"
	.size	.L.str, 31

	.type	Array2Glob,@object      # @Array2Glob
	.comm	Array2Glob,10404,16
	.type	BoolGlob,@object        # @BoolGlob
	.comm	BoolGlob,4,4
	.type	Array1Glob,@object      # @Array1Glob
	.comm	Array1Glob,204,16
	.type	Char2Glob,@object       # @Char2Glob
	.comm	Char2Glob,1,1
	.type	Char1Glob,@object       # @Char1Glob
	.comm	Char1Glob,1,1
	.type	IntGlob,@object         # @IntGlob
	.comm	IntGlob,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
