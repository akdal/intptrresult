	.text
	.file	"pcg_struct.bc"
	.globl	hypre_StructKrylovCAlloc
	.p2align	4, 0x90
	.type	hypre_StructKrylovCAlloc,@function
hypre_StructKrylovCAlloc:               # @hypre_StructKrylovCAlloc
	.cfi_startproc
# BB#0:
	jmp	hypre_CAlloc            # TAILCALL
.Lfunc_end0:
	.size	hypre_StructKrylovCAlloc, .Lfunc_end0-hypre_StructKrylovCAlloc
	.cfi_endproc

	.globl	hypre_StructKrylovFree
	.p2align	4, 0x90
	.type	hypre_StructKrylovFree,@function
hypre_StructKrylovFree:                 # @hypre_StructKrylovFree
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	hypre_Free
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	hypre_StructKrylovFree, .Lfunc_end1-hypre_StructKrylovFree
	.cfi_endproc

	.globl	hypre_StructKrylovCreateVector
	.p2align	4, 0x90
	.type	hypre_StructKrylovCreateVector,@function
hypre_StructKrylovCreateVector:         # @hypre_StructKrylovCreateVector
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	(%rdi), %eax
	movq	8(%rdi), %rsi
	movl	%eax, %edi
	callq	hypre_StructVectorCreate
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	hypre_StructVectorInitialize
	movq	%rbx, %rdi
	callq	hypre_StructVectorAssemble
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_StructKrylovCreateVector, .Lfunc_end2-hypre_StructKrylovCreateVector
	.cfi_endproc

	.globl	hypre_StructKrylovCreateVectorArray
	.p2align	4, 0x90
	.type	hypre_StructKrylovCreateVectorArray,@function
hypre_StructKrylovCreateVectorArray:    # @hypre_StructKrylovCreateVectorArray
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r12, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movl	%edi, %ebx
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	testl	%ebx, %ebx
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph
	movl	%ebx, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%r15), %edi
	movq	8(%r15), %rsi
	movq	%rbx, %rdx
	callq	HYPRE_StructVectorCreate
	movq	(%rbx), %rdi
	callq	HYPRE_StructVectorInitialize
	movq	(%rbx), %rdi
	callq	HYPRE_StructVectorAssemble
	addq	$8, %rbx
	decq	%r12
	jne	.LBB3_2
.LBB3_3:                                # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	hypre_StructKrylovCreateVectorArray, .Lfunc_end3-hypre_StructKrylovCreateVectorArray
	.cfi_endproc

	.globl	hypre_StructKrylovDestroyVector
	.p2align	4, 0x90
	.type	hypre_StructKrylovDestroyVector,@function
hypre_StructKrylovDestroyVector:        # @hypre_StructKrylovDestroyVector
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorDestroy # TAILCALL
.Lfunc_end4:
	.size	hypre_StructKrylovDestroyVector, .Lfunc_end4-hypre_StructKrylovDestroyVector
	.cfi_endproc

	.globl	hypre_StructKrylovMatvecCreate
	.p2align	4, 0x90
	.type	hypre_StructKrylovMatvecCreate,@function
hypre_StructKrylovMatvecCreate:         # @hypre_StructKrylovMatvecCreate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	hypre_StructMatvecCreate
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	hypre_StructMatvecSetup
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	hypre_StructKrylovMatvecCreate, .Lfunc_end5-hypre_StructKrylovMatvecCreate
	.cfi_endproc

	.globl	hypre_StructKrylovMatvec
	.p2align	4, 0x90
	.type	hypre_StructKrylovMatvec,@function
hypre_StructKrylovMatvec:               # @hypre_StructKrylovMatvec
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatvecCompute # TAILCALL
.Lfunc_end6:
	.size	hypre_StructKrylovMatvec, .Lfunc_end6-hypre_StructKrylovMatvec
	.cfi_endproc

	.globl	hypre_StructKrylovMatvecDestroy
	.p2align	4, 0x90
	.type	hypre_StructKrylovMatvecDestroy,@function
hypre_StructKrylovMatvecDestroy:        # @hypre_StructKrylovMatvecDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructMatvecDestroy # TAILCALL
.Lfunc_end7:
	.size	hypre_StructKrylovMatvecDestroy, .Lfunc_end7-hypre_StructKrylovMatvecDestroy
	.cfi_endproc

	.globl	hypre_StructKrylovInnerProd
	.p2align	4, 0x90
	.type	hypre_StructKrylovInnerProd,@function
hypre_StructKrylovInnerProd:            # @hypre_StructKrylovInnerProd
	.cfi_startproc
# BB#0:
	jmp	hypre_StructInnerProd   # TAILCALL
.Lfunc_end8:
	.size	hypre_StructKrylovInnerProd, .Lfunc_end8-hypre_StructKrylovInnerProd
	.cfi_endproc

	.globl	hypre_StructKrylovCopyVector
	.p2align	4, 0x90
	.type	hypre_StructKrylovCopyVector,@function
hypre_StructKrylovCopyVector:           # @hypre_StructKrylovCopyVector
	.cfi_startproc
# BB#0:
	jmp	hypre_StructCopy        # TAILCALL
.Lfunc_end9:
	.size	hypre_StructKrylovCopyVector, .Lfunc_end9-hypre_StructKrylovCopyVector
	.cfi_endproc

	.globl	hypre_StructKrylovClearVector
	.p2align	4, 0x90
	.type	hypre_StructKrylovClearVector,@function
hypre_StructKrylovClearVector:          # @hypre_StructKrylovClearVector
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	jmp	hypre_StructVectorSetConstantValues # TAILCALL
.Lfunc_end10:
	.size	hypre_StructKrylovClearVector, .Lfunc_end10-hypre_StructKrylovClearVector
	.cfi_endproc

	.globl	hypre_StructKrylovScaleVector
	.p2align	4, 0x90
	.type	hypre_StructKrylovScaleVector,@function
hypre_StructKrylovScaleVector:          # @hypre_StructKrylovScaleVector
	.cfi_startproc
# BB#0:
	jmp	hypre_StructScale       # TAILCALL
.Lfunc_end11:
	.size	hypre_StructKrylovScaleVector, .Lfunc_end11-hypre_StructKrylovScaleVector
	.cfi_endproc

	.globl	hypre_StructKrylovAxpy
	.p2align	4, 0x90
	.type	hypre_StructKrylovAxpy,@function
hypre_StructKrylovAxpy:                 # @hypre_StructKrylovAxpy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructAxpy        # TAILCALL
.Lfunc_end12:
	.size	hypre_StructKrylovAxpy, .Lfunc_end12-hypre_StructKrylovAxpy
	.cfi_endproc

	.globl	hypre_StructKrylovIdentitySetup
	.p2align	4, 0x90
	.type	hypre_StructKrylovIdentitySetup,@function
hypre_StructKrylovIdentitySetup:        # @hypre_StructKrylovIdentitySetup
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	hypre_StructKrylovIdentitySetup, .Lfunc_end13-hypre_StructKrylovIdentitySetup
	.cfi_endproc

	.globl	hypre_StructKrylovIdentity
	.p2align	4, 0x90
	.type	hypre_StructKrylovIdentity,@function
hypre_StructKrylovIdentity:             # @hypre_StructKrylovIdentity
	.cfi_startproc
# BB#0:
	movq	%rdx, %rdi
	movq	%rcx, %rsi
	jmp	hypre_StructCopy        # TAILCALL
.Lfunc_end14:
	.size	hypre_StructKrylovIdentity, .Lfunc_end14-hypre_StructKrylovIdentity
	.cfi_endproc

	.globl	hypre_StructKrylovCommInfo
	.p2align	4, 0x90
	.type	hypre_StructKrylovCommInfo,@function
hypre_StructKrylovCommInfo:             # @hypre_StructKrylovCommInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	(%rdi), %ebp
	movl	%ebp, %edi
	movq	%rdx, %rsi
	callq	hypre_MPI_Comm_size
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	hypre_MPI_Comm_rank
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	hypre_StructKrylovCommInfo, .Lfunc_end15-hypre_StructKrylovCommInfo
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
