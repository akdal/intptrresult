	.text
	.file	"lauxlib.bc"
	.globl	luaL_argerror
	.p2align	4, 0x90
	.type	luaL_argerror,@function
luaL_argerror:                          # @luaL_argerror
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$128, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 160
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB0_1
# BB#2:
	leaq	8(%rsp), %rdx
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	lua_getinfo
	movq	24(%rsp), %rdi
	movl	$.L.str.2, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_5
# BB#3:
	decl	%ebp
	je	.LBB0_4
.LBB0_5:
	movq	16(%rsp), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_7
# BB#6:
	movq	$.L.str.4, 16(%rsp)
	movl	$.L.str.4, %ecx
.LBB0_7:
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movq	%r14, %r8
	callq	luaL_error
	jmp	.LBB0_8
.LBB0_1:
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	luaL_error
	jmp	.LBB0_8
.LBB0_4:
	movq	16(%rsp), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	luaL_error
.LBB0_8:
	addq	$128, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	luaL_argerror, .Lfunc_end0-luaL_argerror
	.cfi_endproc

	.globl	luaL_error
	.p2align	4, 0x90
	.type	luaL_error,@function
luaL_error:                             # @luaL_error
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	subq	$328, %rsp              # imm = 0x148
.Lcfi9:
	.cfi_def_cfa_offset 352
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	leaq	208(%rsp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB1_5
# BB#3:
	leaq	208(%rsp), %rdx
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	lua_getinfo
	movl	248(%rsp), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_5
# BB#4:
	leaq	264(%rsp), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	lua_pushfstring
	jmp	.LBB1_6
.LBB1_5:
	movl	$.L.str.9, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
.LBB1_6:                                # %luaL_where.exit
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_pushvfstring
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_concat
	movq	%rbx, %rdi
	callq	lua_error
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	luaL_error, .Lfunc_end1-luaL_error
	.cfi_endproc

	.globl	luaL_typerror
	.p2align	4, 0x90
	.type	luaL_typerror,@function
luaL_typerror:                          # @luaL_typerror
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	luaL_argerror           # TAILCALL
.Lfunc_end2:
	.size	luaL_typerror, .Lfunc_end2-luaL_typerror
	.cfi_endproc

	.globl	luaL_where
	.p2align	4, 0x90
	.type	luaL_where,@function
luaL_where:                             # @luaL_where
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$128, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdx
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB3_3
# BB#1:
	leaq	8(%rsp), %rdx
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	lua_getinfo
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_3
# BB#2:
	leaq	64(%rsp), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	lua_pushfstring
	jmp	.LBB3_4
.LBB3_3:
	movl	$.L.str.9, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
.LBB3_4:
	addq	$128, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	luaL_where, .Lfunc_end3-luaL_where
	.cfi_endproc

	.globl	luaL_checkoption
	.p2align	4, 0x90
	.type	luaL_checkoption,@function
luaL_checkoption:                       # @luaL_checkoption
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%esi, %r14d
	movq	%rdi, %r15
	testq	%rdx, %rdx
	je	.LBB4_2
# BB#1:
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	luaL_optlstring
	movq	%rax, %rbx
	jmp	.LBB4_4
.LBB4_2:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_tolstring
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB4_4
# BB#3:
	movl	$4, %esi
	movq	%r15, %rdi
	callq	lua_typename
	movq	%rax, %rbp
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_type
	movq	%r15, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	xorl	%ebx, %ebx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	lua_pushfstring
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
.LBB4_4:                                # %luaL_checklstring.exit
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_9
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=1
	movq	8(%r12,%rbp,8), %rdi
	incq	%rbp
	testq	%rdi, %rdi
	jne	.LBB4_6
.LBB4_8:                                # %._crit_edge
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	lua_pushfstring
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_argerror           # TAILCALL
.LBB4_9:                                # %.loopexit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	luaL_checkoption, .Lfunc_end4-luaL_checkoption
	.cfi_endproc

	.globl	luaL_optlstring
	.p2align	4, 0x90
	.type	luaL_optlstring,@function
luaL_optlstring:                        # @luaL_optlstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %rbp
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB5_3
# BB#1:
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	lua_tolstring
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_8
# BB#2:
	movl	$4, %esi
	movq	%rbp, %rdi
	callq	lua_typename
	movq	%rax, %r15
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	lua_type
	movq	%rbp, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	xorl	%ebx, %ebx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	lua_pushfstring
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
	jmp	.LBB5_8
.LBB5_3:
	testq	%r15, %r15
	je	.LBB5_8
# BB#4:
	testq	%rbx, %rbx
	je	.LBB5_6
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	jmp	.LBB5_7
.LBB5_6:
	xorl	%eax, %eax
.LBB5_7:                                # %luaL_checklstring.exit
	movq	%rax, (%r15)
.LBB5_8:                                # %luaL_checklstring.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	luaL_optlstring, .Lfunc_end5-luaL_optlstring
	.cfi_endproc

	.globl	luaL_checklstring
	.p2align	4, 0x90
	.type	luaL_checklstring,@function
luaL_checklstring:                      # @luaL_checklstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	lua_tolstring
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB6_2
# BB#1:
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	lua_typename
	movq	%rax, %r15
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
.LBB6_2:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	luaL_checklstring, .Lfunc_end6-luaL_checklstring
	.cfi_endproc

	.globl	luaL_newmetatable
	.p2align	4, 0x90
	.type	luaL_newmetatable,@function
luaL_newmetatable:                      # @luaL_newmetatable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%r14, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.LBB7_2
# BB#1:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_setfield
	movl	$1, %eax
.LBB7_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	luaL_newmetatable, .Lfunc_end7-luaL_newmetatable
	.cfi_endproc

	.globl	luaL_checkudata
	.p2align	4, 0x90
	.type	luaL_checkudata,@function
luaL_checkudata:                        # @luaL_checkudata
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 48
.Lcfi59:
	.cfi_offset %rbx, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_touserdata
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB8_4
# BB#1:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_getmetatable
	testl	%eax, %eax
	je	.LBB8_4
# BB#2:
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movl	$-2, %edx
	movq	%rbx, %rdi
	callq	lua_rawequal
	testl	%eax, %eax
	je	.LBB8_4
# BB#3:
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	jmp	.LBB8_5
.LBB8_4:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	xorl	%r15d, %r15d
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
.LBB8_5:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	luaL_checkudata, .Lfunc_end8-luaL_checkudata
	.cfi_endproc

	.globl	luaL_checkstack
	.p2align	4, 0x90
	.type	luaL_checkstack,@function
luaL_checkstack:                        # @luaL_checkstack
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	callq	lua_checkstack
	testl	%eax, %eax
	je	.LBB9_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_2:
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaL_error              # TAILCALL
.Lfunc_end9:
	.size	luaL_checkstack, .Lfunc_end9-luaL_checkstack
	.cfi_endproc

	.globl	luaL_checktype
	.p2align	4, 0x90
	.type	luaL_checktype,@function
luaL_checktype:                         # @luaL_checktype
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	lua_type
	cmpl	%ebp, %eax
	jne	.LBB10_2
# BB#1:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB10_2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_typename
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	luaL_argerror           # TAILCALL
.Lfunc_end10:
	.size	luaL_checktype, .Lfunc_end10-luaL_checktype
	.cfi_endproc

	.globl	luaL_checkany
	.p2align	4, 0x90
	.type	luaL_checkany,@function
luaL_checkany:                          # @luaL_checkany
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 32
.Lcfi77:
	.cfi_offset %rbx, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_type
	cmpl	$-1, %eax
	je	.LBB11_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB11_2:
	movl	$.L.str.12, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	luaL_argerror           # TAILCALL
.Lfunc_end11:
	.size	luaL_checkany, .Lfunc_end11-luaL_checkany
	.cfi_endproc

	.globl	luaL_checknumber
	.p2align	4, 0x90
	.type	luaL_checknumber,@function
luaL_checknumber:                       # @luaL_checknumber
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_tonumber
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB12_3
	jp	.LBB12_3
# BB#1:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	lua_isnumber
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	testl	%eax, %eax
	jne	.LBB12_3
# BB#2:
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_typename
	movq	%rax, %r14
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB12_3:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	luaL_checknumber, .Lfunc_end12-luaL_checknumber
	.cfi_endproc

	.globl	luaL_optnumber
	.p2align	4, 0x90
	.type	luaL_optnumber,@function
luaL_optnumber:                         # @luaL_optnumber
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB13_1
# BB#2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	luaL_checknumber        # TAILCALL
.LBB13_1:
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	luaL_optnumber, .Lfunc_end13-luaL_optnumber
	.cfi_endproc

	.globl	luaL_checkinteger
	.p2align	4, 0x90
	.type	luaL_checkinteger,@function
luaL_checkinteger:                      # @luaL_checkinteger
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 48
.Lcfi96:
	.cfi_offset %rbx, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbp
	callq	lua_tointeger
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB14_3
# BB#1:
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	lua_isnumber
	testl	%eax, %eax
	jne	.LBB14_3
# BB#2:
	movl	$3, %esi
	movq	%rbp, %rdi
	callq	lua_typename
	movq	%rax, %r15
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	lua_type
	movq	%rbp, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	lua_pushfstring
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	callq	luaL_argerror
.LBB14_3:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	luaL_checkinteger, .Lfunc_end14-luaL_checkinteger
	.cfi_endproc

	.globl	luaL_optinteger
	.p2align	4, 0x90
	.type	luaL_optinteger,@function
luaL_optinteger:                        # @luaL_optinteger
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB15_1
# BB#2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	luaL_checkinteger       # TAILCALL
.LBB15_1:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end15:
	.size	luaL_optinteger, .Lfunc_end15-luaL_optinteger
	.cfi_endproc

	.globl	luaL_getmetafield
	.p2align	4, 0x90
	.type	luaL_getmetafield,@function
luaL_getmetafield:                      # @luaL_getmetafield
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 32
.Lcfi109:
	.cfi_offset %rbx, -32
.Lcfi110:
	.cfi_offset %r14, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	callq	lua_getmetatable
	xorl	%r14d, %r14d
	testl	%eax, %eax
	je	.LBB16_4
# BB#1:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB16_2
# BB#3:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	movl	$1, %r14d
	jmp	.LBB16_4
.LBB16_2:
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
.LBB16_4:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	luaL_getmetafield, .Lfunc_end16-luaL_getmetafield
	.cfi_endproc

	.globl	luaL_callmeta
	.p2align	4, 0x90
	.type	luaL_callmeta,@function
luaL_callmeta:                          # @luaL_callmeta
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 48
.Lcfi117:
	.cfi_offset %rbx, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %rbx
	leal	9999(%r14), %eax
	cmpl	$9999, %eax             # imm = 0x270F
	ja	.LBB17_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%r14,%rax), %r14d
.LBB17_2:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_getmetatable
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB17_6
# BB#3:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	lua_pushstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB17_4
# BB#5:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_pushvalue
	movl	$1, %ebp
	movl	$1, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	jmp	.LBB17_6
.LBB17_4:
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
.LBB17_6:                               # %luaL_getmetafield.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	luaL_callmeta, .Lfunc_end17-luaL_callmeta
	.cfi_endproc

	.globl	luaL_register
	.p2align	4, 0x90
	.type	luaL_register,@function
luaL_register:                          # @luaL_register
	.cfi_startproc
# BB#0:
	xorl	%ecx, %ecx
	jmp	luaL_openlib            # TAILCALL
.Lfunc_end18:
	.size	luaL_register, .Lfunc_end18-luaL_register
	.cfi_endproc

	.globl	luaL_openlib
	.p2align	4, 0x90
	.type	luaL_openlib,@function
luaL_openlib:                           # @luaL_openlib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 64
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r13
	testq	%r15, %r15
	je	.LBB19_10
# BB#1:
	cmpq	$0, (%r12)
	je	.LBB19_2
# BB#3:                                 # %.lr.ph.i.preheader
	leaq	16(%r12), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpq	$0, (%rax)
	leaq	16(%rax), %rax
	jne	.LBB19_4
	jmp	.LBB19_5
.LBB19_2:
	xorl	%ebp, %ebp
.LBB19_5:                               # %libsize.exit
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.13, %edx
	movl	$1, %ecx
	movq	%r13, %rdi
	callq	luaL_findtable
	movl	$-1, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	lua_getfield
	movl	$-1, %esi
	movq	%r13, %rdi
	callq	lua_type
	cmpl	$5, %eax
	je	.LBB19_9
# BB#6:
	movl	$-2, %esi
	movq	%r13, %rdi
	callq	lua_settop
	movl	$-10002, %esi           # imm = 0xD8EE
	movq	%r13, %rdi
	movq	%r15, %rdx
	movl	%ebp, %ecx
	callq	luaL_findtable
	testq	%rax, %rax
	je	.LBB19_8
# BB#7:
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	luaL_error
.LBB19_8:
	movl	$-1, %esi
	movq	%r13, %rdi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	lua_setfield
.LBB19_9:
	movl	$-2, %esi
	movq	%r13, %rdi
	callq	lua_remove
	movl	%r14d, %esi
	notl	%esi
	movq	%r13, %rdi
	callq	lua_insert
.LBB19_10:                              # %.preheader35
	cmpq	$0, (%r12)
	je	.LBB19_18
# BB#11:                                # %.preheader.lr.ph
	movl	$-2, %r15d
	subl	%r14d, %r15d
	testl	%r14d, %r14d
	jle	.LBB19_12
# BB#14:                                # %.preheader.us.preheader
	movl	%r14d, %ebp
	negl	%ebp
	.p2align	4, 0x90
.LBB19_15:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_16 Depth 2
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB19_16:                              #   Parent Loop BB19_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	lua_pushvalue
	decl	%ebx
	jne	.LBB19_16
# BB#17:                                # %._crit_edge.us
                                        #   in Loop: Header=BB19_15 Depth=1
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	movl	%r14d, %edx
	callq	lua_pushcclosure
	movq	(%r12), %rdx
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	lua_setfield
	cmpq	$0, 16(%r12)
	leaq	16(%r12), %r12
	jne	.LBB19_15
	jmp	.LBB19_18
.LBB19_12:                              # %.preheader.preheader
	addq	$16, %r12
	.p2align	4, 0x90
.LBB19_13:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%r12), %rsi
	movq	%r13, %rdi
	movl	%r14d, %edx
	callq	lua_pushcclosure
	movq	-16(%r12), %rdx
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	lua_setfield
	cmpq	$0, (%r12)
	leaq	16(%r12), %r12
	jne	.LBB19_13
.LBB19_18:                              # %._crit_edge38
	notl	%r14d
	movq	%r13, %rdi
	movl	%r14d, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	lua_settop              # TAILCALL
.Lfunc_end19:
	.size	luaL_openlib, .Lfunc_end19-luaL_openlib
	.cfi_endproc

	.globl	luaL_findtable
	.p2align	4, 0x90
	.type	luaL_findtable,@function
luaL_findtable:                         # @luaL_findtable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 64
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rdi, %r15
	callq	lua_pushvalue
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movl	$46, %esi
	movq	%r12, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB20_3
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	%r12, %rbx
.LBB20_3:                               #   in Loop: Header=BB20_1 Depth=1
	movq	%rbx, %rbp
	subq	%r12, %rbp
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	lua_pushlstring
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB20_4
# BB#7:                                 #   in Loop: Header=BB20_1 Depth=1
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_type
	cmpl	$5, %eax
	je	.LBB20_5
	jmp	.LBB20_8
	.p2align	4, 0x90
.LBB20_4:                               #   in Loop: Header=BB20_1 Depth=1
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_settop
	cmpb	$46, (%rbx)
	movl	%r14d, %edx
	cmovel	%r13d, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	lua_createtable
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	lua_pushlstring
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_pushvalue
	movl	$-4, %esi
	movq	%r15, %rdi
	callq	lua_settable
.LBB20_5:                               #   in Loop: Header=BB20_1 Depth=1
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_remove
	cmpb	$46, (%rbx)
	leaq	1(%rbx), %r12
	je	.LBB20_1
# BB#6:
	xorl	%r12d, %r12d
	jmp	.LBB20_9
.LBB20_8:
	movl	$-3, %esi
	movq	%r15, %rdi
	callq	lua_settop
.LBB20_9:                               # %.loopexit
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	luaL_findtable, .Lfunc_end20-luaL_findtable
	.cfi_endproc

	.globl	luaL_gsub
	.p2align	4, 0x90
	.type	luaL_gsub,@function
luaL_gsub:                              # @luaL_gsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 56
	subq	$8248, %rsp             # imm = 0x2038
.Lcfi153:
	.cfi_def_cfa_offset 8304
.Lcfi154:
	.cfi_offset %rbx, -56
.Lcfi155:
	.cfi_offset %r12, -48
.Lcfi156:
	.cfi_offset %r13, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 48(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 32(%rsp)
	movl	$0, 40(%rsp)
	movq	%r13, %rdi
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	callq	strstr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB21_13
# BB#1:                                 # %.lr.ph
	leaq	8248(%rsp), %r12
	leaq	32(%rsp), %r14
	.p2align	4, 0x90
.LBB21_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_4 Depth 2
                                        #     Child Loop BB21_9 Depth 2
	cmpq	%r13, %r15
	je	.LBB21_7
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	%r13, %rbx
	subq	%r15, %rbx
	.p2align	4, 0x90
.LBB21_4:                               # %.lr.ph.i
                                        #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rax
	cmpq	%r12, %rax
	jb	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_4 Depth=2
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	32(%rsp), %rax
.LBB21_6:                               #   in Loop: Header=BB21_4 Depth=2
	movzbl	(%r13), %ecx
	incq	%r13
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%rsp)
	movb	%cl, (%rax)
	incq	%rbx
	jne	.LBB21_4
.LBB21_7:                               # %luaL_addlstring.exit
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB21_12
# BB#8:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.i.i
                                        #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decq	%rbx
	movq	32(%rsp), %rax
	cmpq	%r12, %rax
	jb	.LBB21_11
# BB#10:                                #   in Loop: Header=BB21_9 Depth=2
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	32(%rsp), %rax
.LBB21_11:                              #   in Loop: Header=BB21_9 Depth=2
	movzbl	(%rbp), %ecx
	incq	%rbp
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%rsp)
	movb	%cl, (%rax)
	testq	%rbx, %rbx
	jne	.LBB21_9
.LBB21_12:                              # %luaL_addstring.exit
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	%r15, %r13
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	movq	%r13, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	strstr
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB21_2
.LBB21_13:                              # %._crit_edge
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	leaq	56(%rsp), %rbx
	je	.LBB21_18
# BB#14:                                # %.lr.ph.i.i14
	leaq	8248(%rsp), %r15
	leaq	32(%rsp), %r14
	.p2align	4, 0x90
.LBB21_15:                              # =>This Inner Loop Header: Depth=1
	decq	%rbp
	movq	32(%rsp), %rax
	cmpq	%r15, %rax
	jb	.LBB21_17
# BB#16:                                #   in Loop: Header=BB21_15 Depth=1
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	32(%rsp), %rax
.LBB21_17:                              #   in Loop: Header=BB21_15 Depth=1
	movzbl	(%r13), %ecx
	incq	%r13
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%rsp)
	movb	%cl, (%rax)
	testq	%rbp, %rbp
	jne	.LBB21_15
.LBB21_18:                              # %luaL_addstring.exit18
	movq	32(%rsp), %rdx
	subq	%rbx, %rdx
	je	.LBB21_19
# BB#20:
	movq	48(%rsp), %rdi
	movq	%rbx, %rsi
	callq	lua_pushlstring
	movq	%rbx, 32(%rsp)
	movl	40(%rsp), %esi
	incl	%esi
	movl	%esi, 40(%rsp)
	jmp	.LBB21_21
.LBB21_19:                              # %.emptybuffer.exit_crit_edge.i
	movl	40(%rsp), %esi
.LBB21_21:                              # %luaL_pushresult.exit
	movq	48(%rsp), %rdi
	callq	lua_concat
	movl	$1, 40(%rsp)
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	lua_tolstring
	addq	$8248, %rsp             # imm = 0x2038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	luaL_gsub, .Lfunc_end21-luaL_gsub
	.cfi_endproc

	.globl	luaL_buffinit
	.p2align	4, 0x90
	.type	luaL_buffinit,@function
luaL_buffinit:                          # @luaL_buffinit
	.cfi_startproc
# BB#0:
	movq	%rdi, 16(%rsi)
	leaq	24(%rsi), %rax
	movq	%rax, (%rsi)
	movl	$0, 8(%rsi)
	retq
.Lfunc_end22:
	.size	luaL_buffinit, .Lfunc_end22-luaL_buffinit
	.cfi_endproc

	.globl	luaL_addlstring
	.p2align	4, 0x90
	.type	luaL_addlstring,@function
luaL_addlstring:                        # @luaL_addlstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 48
.Lcfi165:
	.cfi_offset %rbx, -40
.Lcfi166:
	.cfi_offset %r12, -32
.Lcfi167:
	.cfi_offset %r14, -24
.Lcfi168:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB23_5
# BB#1:                                 # %.lr.ph
	leaq	8216(%r15), %r12
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	decq	%r14
	movq	(%r15), %rax
	cmpq	%r12, %rax
	jb	.LBB23_4
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	%r15, %rdi
	callq	luaL_prepbuffer
	movq	(%r15), %rax
.LBB23_4:                               #   in Loop: Header=BB23_2 Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movb	%cl, (%rax)
	testq	%r14, %r14
	jne	.LBB23_2
.LBB23_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	luaL_addlstring, .Lfunc_end23-luaL_addlstring
	.cfi_endproc

	.globl	luaL_addstring
	.p2align	4, 0x90
	.type	luaL_addstring,@function
luaL_addstring:                         # @luaL_addstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 48
.Lcfi174:
	.cfi_offset %rbx, -40
.Lcfi175:
	.cfi_offset %r12, -32
.Lcfi176:
	.cfi_offset %r14, -24
.Lcfi177:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB24_5
# BB#1:                                 # %.lr.ph.i
	leaq	8216(%r14), %r12
	.p2align	4, 0x90
.LBB24_2:                               # =>This Inner Loop Header: Depth=1
	decq	%r15
	movq	(%r14), %rax
	cmpq	%r12, %rax
	jb	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movq	%r14, %rdi
	callq	luaL_prepbuffer
	movq	(%r14), %rax
.LBB24_4:                               #   in Loop: Header=BB24_2 Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	leaq	1(%rax), %rdx
	movq	%rdx, (%r14)
	movb	%cl, (%rax)
	testq	%r15, %r15
	jne	.LBB24_2
.LBB24_5:                               # %luaL_addlstring.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	luaL_addstring, .Lfunc_end24-luaL_addstring
	.cfi_endproc

	.globl	luaL_pushresult
	.p2align	4, 0x90
	.type	luaL_pushresult,@function
luaL_pushresult:                        # @luaL_pushresult
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 32
.Lcfi181:
	.cfi_offset %rbx, -24
.Lcfi182:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdx
	leaq	24(%rbx), %r14
	subq	%r14, %rdx
	je	.LBB25_1
# BB#2:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	lua_pushlstring
	movq	%r14, (%rbx)
	leaq	8(%rbx), %r14
	movl	8(%rbx), %esi
	incl	%esi
	movl	%esi, 8(%rbx)
	leaq	16(%rbx), %rbx
	jmp	.LBB25_3
.LBB25_1:                               # %.emptybuffer.exit_crit_edge
	leaq	8(%rbx), %r14
	movl	8(%rbx), %esi
	addq	$16, %rbx
.LBB25_3:                               # %emptybuffer.exit
	movq	(%rbx), %rdi
	callq	lua_concat
	movl	$1, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	luaL_pushresult, .Lfunc_end25-luaL_pushresult
	.cfi_endproc

	.globl	luaL_prepbuffer
	.p2align	4, 0x90
	.type	luaL_prepbuffer,@function
luaL_prepbuffer:                        # @luaL_prepbuffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi186:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi187:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 64
.Lcfi190:
	.cfi_offset %rbx, -56
.Lcfi191:
	.cfi_offset %r12, -48
.Lcfi192:
	.cfi_offset %r13, -40
.Lcfi193:
	.cfi_offset %r14, -32
.Lcfi194:
	.cfi_offset %r15, -24
.Lcfi195:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	(%r15), %rdx
	leaq	24(%r15), %rbx
	subq	%rbx, %rdx
	je	.LBB26_6
# BB#1:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	callq	lua_pushlstring
	movq	%rbx, (%r15)
	movl	8(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r15)
	testl	%eax, %eax
	jle	.LBB26_6
# BB#2:
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	16(%r15), %r12
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	lua_objlen
	movq	%rax, %r13
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB26_3:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r14d
	movl	%r14d, %esi
	notl	%esi
	movq	%r12, %rdi
	callq	lua_objlen
	movl	8(%r15), %ecx
	movl	%ecx, %edx
	subl	%r14d, %edx
	cmpl	$8, %edx
	setg	%sil
	cmpq	%r13, %rax
	setb	%bl
	movl	%ebx, %edx
	orb	%sil, %dl
	movl	$0, %edi
	cmoveq	%rdi, %rax
	movzbl	%dl, %ebp
	addl	%r14d, %ebp
	orb	%sil, %bl
	je	.LBB26_5
# BB#4:                                 #   in Loop: Header=BB26_3 Depth=1
	addq	%rax, %r13
	cmpl	%ecx, %ebp
	jl	.LBB26_3
.LBB26_5:
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	lua_concat
	movl	$1, %eax
	subl	%ebp, %eax
	addl	%eax, 8(%r15)
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB26_6:                               # %adjuststack.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	luaL_prepbuffer, .Lfunc_end26-luaL_prepbuffer
	.cfi_endproc

	.globl	luaL_addvalue
	.p2align	4, 0x90
	.type	luaL_addvalue,@function
luaL_addvalue:                          # @luaL_addvalue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi197:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi199:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi200:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi202:
	.cfi_def_cfa_offset 64
.Lcfi203:
	.cfi_offset %rbx, -56
.Lcfi204:
	.cfi_offset %r12, -48
.Lcfi205:
	.cfi_offset %r13, -40
.Lcfi206:
	.cfi_offset %r14, -32
.Lcfi207:
	.cfi_offset %r15, -24
.Lcfi208:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r14
	movq	%rsp, %rdx
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_tolstring
	movq	(%rsp), %rdx
	movq	(%rbx), %rcx
	leaq	8216(%rbx), %rsi
	subq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jbe	.LBB27_1
# BB#2:
	leaq	24(%rbx), %rbp
	subq	%rbp, %rcx
	je	.LBB27_3
# BB#4:
	movq	16(%rbx), %rdi
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	lua_pushlstring
	movq	%rbp, (%rbx)
	leaq	8(%rbx), %r15
	incl	8(%rbx)
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_insert
	jmp	.LBB27_5
.LBB27_1:
	movq	%rcx, %rdi
	movq	%rax, %rsi
	callq	memcpy
	movq	(%rsp), %rax
	addq	%rax, (%rbx)
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_settop
	jmp	.LBB27_10
.LBB27_3:                               # %.emptybuffer.exit.thread_crit_edge
	leaq	8(%rbx), %r15
.LBB27_5:                               # %emptybuffer.exit.thread
	movl	(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r15)
	testl	%eax, %eax
	jle	.LBB27_10
# BB#6:
	movq	16(%rbx), %r14
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_objlen
	movq	%rax, %r12
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB27_7:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r13d
	movl	%r13d, %esi
	notl	%esi
	movq	%r14, %rdi
	callq	lua_objlen
	movl	(%r15), %ecx
	movl	%ecx, %edx
	subl	%r13d, %edx
	cmpl	$8, %edx
	setg	%sil
	cmpq	%r12, %rax
	setb	%bl
	movl	%ebx, %edx
	orb	%sil, %dl
	movl	$0, %edi
	cmoveq	%rdi, %rax
	movzbl	%dl, %ebp
	addl	%r13d, %ebp
	orb	%sil, %bl
	je	.LBB27_9
# BB#8:                                 #   in Loop: Header=BB27_7 Depth=1
	addq	%rax, %r12
	cmpl	%ecx, %ebp
	jl	.LBB27_7
.LBB27_9:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	lua_concat
	movl	$1, %eax
	subl	%ebp, %eax
	addl	%eax, (%r15)
.LBB27_10:                              # %adjuststack.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	luaL_addvalue, .Lfunc_end27-luaL_addvalue
	.cfi_endproc

	.globl	luaL_ref
	.p2align	4, 0x90
	.type	luaL_ref,@function
luaL_ref:                               # @luaL_ref
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -32
.Lcfi213:
	.cfi_offset %r14, -24
.Lcfi214:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	leal	9999(%r14), %eax
	cmpl	$9999, %eax             # imm = 0x270F
	ja	.LBB28_2
# BB#1:
	movq	%r15, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%r14,%rax), %r14d
.LBB28_2:
	movl	$-1, %ebx
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB28_3
# BB#4:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_tointeger
	movq	%rax, %rbx
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_settop
	movq	%r15, %rdi
	movl	%r14d, %esi
	testl	%ebx, %ebx
	je	.LBB28_6
# BB#5:
	movl	%ebx, %edx
	callq	lua_rawgeti
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_rawseti
	jmp	.LBB28_7
.LBB28_3:
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_settop
	jmp	.LBB28_8
.LBB28_6:
	callq	lua_objlen
	movq	%rax, %rbx
	incl	%ebx
.LBB28_7:
	movq	%r15, %rdi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	lua_rawseti
.LBB28_8:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	luaL_ref, .Lfunc_end28-luaL_ref
	.cfi_endproc

	.globl	luaL_unref
	.p2align	4, 0x90
	.type	luaL_unref,@function
luaL_unref:                             # @luaL_unref
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 32
.Lcfi218:
	.cfi_offset %rbx, -32
.Lcfi219:
	.cfi_offset %r14, -24
.Lcfi220:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r14
	testl	%ebp, %ebp
	js	.LBB29_4
# BB#1:
	leal	9999(%rbx), %eax
	cmpl	$9999, %eax             # imm = 0x270F
	ja	.LBB29_3
# BB#2:
	movq	%r14, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rbx,%rax), %ebx
.LBB29_3:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	lua_rawgeti
	movq	%r14, %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	lua_rawseti
	movslq	%ebp, %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%ebx, %esi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	lua_rawseti             # TAILCALL
.LBB29_4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end29:
	.size	luaL_unref, .Lfunc_end29-luaL_unref
	.cfi_endproc

	.globl	luaL_loadfile
	.p2align	4, 0x90
	.type	luaL_loadfile,@function
luaL_loadfile:                          # @luaL_loadfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi221:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi222:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi223:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi224:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi225:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 56
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi227:
	.cfi_def_cfa_offset 8272
.Lcfi228:
	.cfi_offset %rbx, -56
.Lcfi229:
	.cfi_offset %r12, -48
.Lcfi230:
	.cfi_offset %r13, -40
.Lcfi231:
	.cfi_offset %r14, -32
.Lcfi232:
	.cfi_offset %r15, -24
.Lcfi233:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	lua_gettop
	movl	%eax, %r14d
	incl	%r14d
	movl	$0, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB30_1
# BB#7:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	lua_pushfstring
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
	testq	%rbp, %rbp
	jne	.LBB30_2
# BB#8:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_tolstring
	leaq	1(%rax), %rcx
	movl	$.L.str.23, %esi
	movl	$.L.str.18, %edx
	jmp	.LBB30_9
.LBB30_1:
	movl	$.L.str.15, %esi
	movl	$6, %edx
	movq	%r15, %rdi
	callq	lua_pushlstring
	movq	stdin(%rip), %rbp
	movq	%rbp, 16(%rsp)
.LBB30_2:
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$35, %eax
	jne	.LBB30_11
# BB#3:
	movl	$1, 8(%rsp)
	.p2align	4, 0x90
.LBB30_4:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB30_10
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=1
	cmpl	$-1, %eax
	jne	.LBB30_4
# BB#6:                                 # %.thread
	testq	%rbx, %rbx
	setne	%r13b
	movl	$-1, %eax
	jmp	.LBB30_18
.LBB30_10:
	movq	%rbp, %rdi
	callq	_IO_getc
.LBB30_11:
	testq	%rbx, %rbx
	setne	%r13b
	je	.LBB30_18
# BB#12:
	cmpl	$27, %eax
	jne	.LBB30_18
# BB#13:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	freopen
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
	testq	%rbp, %rbp
	je	.LBB30_14
	.p2align	4, 0x90
.LBB30_15:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$27, %eax
	je	.LBB30_17
# BB#16:                                # %.preheader
                                        #   in Loop: Header=BB30_15 Depth=1
	cmpl	$-1, %eax
	jne	.LBB30_15
.LBB30_17:                              # %.critedge
	movl	$0, 8(%rsp)
	movb	$1, %r13b
.LBB30_18:
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	ungetc
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	leaq	8(%rsp), %rdx
	movl	$getF, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	callq	lua_load
	movl	%eax, %ebx
	movq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	ferror
	movl	%eax, %r12d
	testb	%r13b, %r13b
	je	.LBB30_20
# BB#19:
	movq	%rbp, %rdi
	callq	fclose
.LBB30_20:
	testl	%r12d, %r12d
	je	.LBB30_22
# BB#21:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_settop
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_tolstring
	leaq	1(%rax), %rcx
	movl	$.L.str.23, %esi
	movl	$.L.str.22, %edx
	jmp	.LBB30_9
.LBB30_22:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_remove
	jmp	.LBB30_23
.LBB30_14:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_tolstring
	leaq	1(%rax), %rcx
	movl	$.L.str.23, %esi
	movl	$.L.str.21, %edx
.LBB30_9:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %r8
	callq	lua_pushfstring
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	lua_remove
	movl	$6, %ebx
.LBB30_23:
	movl	%ebx, %eax
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	luaL_loadfile, .Lfunc_end30-luaL_loadfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	getF,@function
getF:                                   # @getF
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -24
.Lcfi238:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	cmpl	$0, (%rbx)
	je	.LBB31_2
# BB#1:
	movl	$0, (%rbx)
	movq	$1, (%r14)
	movl	$.L.str.24, %ebx
	jmp	.LBB31_5
.LBB31_2:
	movq	8(%rbx), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB31_4
# BB#3:
	xorl	%ebx, %ebx
	jmp	.LBB31_5
.LBB31_4:
	movq	8(%rbx), %rcx
	addq	$16, %rbx
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%rbx, %rdi
	callq	fread
	movq	%rax, (%r14)
	testq	%rax, %rax
	cmoveq	%rax, %rbx
.LBB31_5:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end31:
	.size	getF, .Lfunc_end31-getF
	.cfi_endproc

	.globl	luaL_loadbuffer
	.p2align	4, 0x90
	.type	luaL_loadbuffer,@function
luaL_loadbuffer:                        # @luaL_loadbuffer
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi239:
	.cfi_def_cfa_offset 32
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	leaq	8(%rsp), %rdx
	movl	$getS, %esi
	callq	lua_load
	addq	$24, %rsp
	retq
.Lfunc_end32:
	.size	luaL_loadbuffer, .Lfunc_end32-luaL_loadbuffer
	.cfi_endproc

	.p2align	4, 0x90
	.type	getS,@function
getS:                                   # @getS
	.cfi_startproc
# BB#0:
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.LBB33_1
# BB#2:
	movq	%rax, (%rdx)
	movq	$0, 8(%rsi)
	movq	(%rsi), %rax
	retq
.LBB33_1:
	xorl	%eax, %eax
	retq
.Lfunc_end33:
	.size	getS, .Lfunc_end33-getS
	.cfi_endproc

	.globl	luaL_loadstring
	.p2align	4, 0x90
	.type	luaL_loadstring,@function
luaL_loadstring:                        # @luaL_loadstring
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi240:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi241:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi242:
	.cfi_def_cfa_offset 48
.Lcfi243:
	.cfi_offset %rbx, -24
.Lcfi244:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rdx
	movl	$getS, %esi
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	lua_load
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end34:
	.size	luaL_loadstring, .Lfunc_end34-luaL_loadstring
	.cfi_endproc

	.globl	luaL_newstate
	.p2align	4, 0x90
	.type	luaL_newstate,@function
luaL_newstate:                          # @luaL_newstate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 16
.Lcfi246:
	.cfi_offset %rbx, -16
	movl	$l_alloc, %edi
	xorl	%esi, %esi
	callq	lua_newstate
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB35_2
# BB#1:
	movl	$panic, %esi
	movq	%rbx, %rdi
	callq	lua_atpanic
.LBB35_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end35:
	.size	luaL_newstate, .Lfunc_end35-luaL_newstate
	.cfi_endproc

	.p2align	4, 0x90
	.type	l_alloc,@function
l_alloc:                                # @l_alloc
	.cfi_startproc
# BB#0:
	testq	%rcx, %rcx
	je	.LBB36_1
# BB#2:
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	jmp	realloc                 # TAILCALL
.LBB36_1:
	pushq	%rax
.Lcfi247:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end36:
	.size	l_alloc, .Lfunc_end36-l_alloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	panic,@function
panic:                                  # @panic
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 16
.Lcfi249:
	.cfi_offset %rbx, -16
	movq	stderr(%rip), %rbx
	movl	$-1, %esi
	xorl	%edx, %edx
	callq	lua_tolstring
	movq	%rax, %rcx
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end37:
	.size	panic, .Lfunc_end37-panic
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"bad argument #%d (%s)"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"n"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"method"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"calling '%s' on bad self (%s)"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"?"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"bad argument #%d to '%s' (%s)"
	.size	.L.str.5, 30

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s expected, got %s"
	.size	.L.str.6, 20

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Sl"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s:%d: "
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.zero	1
	.size	.L.str.9, 1

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"invalid option '%s'"
	.size	.L.str.10, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"stack overflow (%s)"
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"value expected"
	.size	.L.str.12, 15

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"_LOADED"
	.size	.L.str.13, 8

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"name conflict for module '%s'"
	.size	.L.str.14, 30

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"=stdin"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"@%s"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"r"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"open"
	.size	.L.str.18, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"rb"
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"reopen"
	.size	.L.str.21, 7

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"read"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"cannot %s %s: %s"
	.size	.L.str.23, 17

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\n"
	.size	.L.str.24, 2

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PANIC: unprotected error in call to Lua API (%s)\n"
	.size	.L.str.25, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
