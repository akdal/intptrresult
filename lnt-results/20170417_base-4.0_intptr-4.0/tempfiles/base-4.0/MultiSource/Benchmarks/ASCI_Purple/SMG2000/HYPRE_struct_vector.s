	.text
	.file	"HYPRE_struct_vector.bc"
	.globl	HYPRE_StructVectorCreate
	.p2align	4, 0x90
	.type	HYPRE_StructVectorCreate,@function
HYPRE_StructVectorCreate:               # @HYPRE_StructVectorCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	callq	hypre_StructVectorCreate
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	HYPRE_StructVectorCreate, .Lfunc_end0-HYPRE_StructVectorCreate
	.cfi_endproc

	.globl	HYPRE_StructVectorDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructVectorDestroy,@function
HYPRE_StructVectorDestroy:              # @HYPRE_StructVectorDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorDestroy # TAILCALL
.Lfunc_end1:
	.size	HYPRE_StructVectorDestroy, .Lfunc_end1-HYPRE_StructVectorDestroy
	.cfi_endproc

	.globl	HYPRE_StructVectorInitialize
	.p2align	4, 0x90
	.type	HYPRE_StructVectorInitialize,@function
HYPRE_StructVectorInitialize:           # @HYPRE_StructVectorInitialize
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorInitialize # TAILCALL
.Lfunc_end2:
	.size	HYPRE_StructVectorInitialize, .Lfunc_end2-HYPRE_StructVectorInitialize
	.cfi_endproc

	.globl	HYPRE_StructVectorSetValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorSetValues,@function
HYPRE_StructVectorSetValues:            # @HYPRE_StructVectorSetValues
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 32
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%rdi), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edx
	movl	%edx, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB3_2
.LBB3_3:                                # %._crit_edge
	leaq	12(%rsp), %rsi
	xorl	%edx, %edx
	callq	hypre_StructVectorSetValues
	addq	$24, %rsp
	retq
.Lfunc_end3:
	.size	HYPRE_StructVectorSetValues, .Lfunc_end3-HYPRE_StructVectorSetValues
	.cfi_endproc

	.globl	HYPRE_StructVectorSetBoxValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorSetBoxValues,@function
HYPRE_StructVectorSetBoxValues:         # @HYPRE_StructVectorSetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 64
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%r15), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, (%rsp,%rcx,4)
	movl	(%rdx,%rcx,4), %edi
	movl	%edi, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	callq	hypre_BoxCreate
	movq	%rax, %rbx
	movq	%rsp, %rsi
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	callq	hypre_BoxSetExtents
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	hypre_StructVectorSetBoxValues
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	hypre_BoxDestroy
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	HYPRE_StructVectorSetBoxValues, .Lfunc_end4-HYPRE_StructVectorSetBoxValues
	.cfi_endproc

	.globl	HYPRE_StructVectorAddToValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorAddToValues,@function
HYPRE_StructVectorAddToValues:          # @HYPRE_StructVectorAddToValues
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 32
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%rdi), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edx
	movl	%edx, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	leaq	12(%rsp), %rsi
	movl	$1, %edx
	callq	hypre_StructVectorSetValues
	addq	$24, %rsp
	retq
.Lfunc_end5:
	.size	HYPRE_StructVectorAddToValues, .Lfunc_end5-HYPRE_StructVectorAddToValues
	.cfi_endproc

	.globl	HYPRE_StructVectorAddToBoxValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorAddToBoxValues,@function
HYPRE_StructVectorAddToBoxValues:       # @HYPRE_StructVectorAddToBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%r15), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, (%rsp,%rcx,4)
	movl	(%rdx,%rcx,4), %edi
	movl	%edi, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge
	callq	hypre_BoxCreate
	movq	%rax, %rbx
	movq	%rsp, %rsi
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	callq	hypre_BoxSetExtents
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	hypre_StructVectorSetBoxValues
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	hypre_BoxDestroy
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	HYPRE_StructVectorAddToBoxValues, .Lfunc_end6-HYPRE_StructVectorAddToBoxValues
	.cfi_endproc

	.globl	HYPRE_StructVectorGetValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorGetValues,@function
HYPRE_StructVectorGetValues:            # @HYPRE_StructVectorGetValues
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 32
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%rdi), %r8
	cmpl	$0, 4(%r8)
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %eax
	movl	%eax, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%r8), %rax
	cmpq	%rax, %rcx
	jl	.LBB7_2
.LBB7_3:                                # %._crit_edge
	leaq	12(%rsp), %rsi
	callq	hypre_StructVectorGetValues
	addq	$24, %rsp
	retq
.Lfunc_end7:
	.size	HYPRE_StructVectorGetValues, .Lfunc_end7-HYPRE_StructVectorGetValues
	.cfi_endproc

	.globl	HYPRE_StructVectorGetBoxValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorGetBoxValues,@function
HYPRE_StructVectorGetBoxValues:         # @HYPRE_StructVectorGetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	8(%r15), %rax
	cmpl	$0, 4(%rax)
	jle	.LBB8_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, (%rsp,%rcx,4)
	movl	(%rdx,%rcx,4), %edi
	movl	%edi, 12(%rsp,%rcx,4)
	incq	%rcx
	movslq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jl	.LBB8_2
.LBB8_3:                                # %._crit_edge
	callq	hypre_BoxCreate
	movq	%rax, %rbx
	movq	%rsp, %rsi
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	callq	hypre_BoxSetExtents
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	hypre_StructVectorGetBoxValues
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	hypre_BoxDestroy
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	HYPRE_StructVectorGetBoxValues, .Lfunc_end8-HYPRE_StructVectorGetBoxValues
	.cfi_endproc

	.globl	HYPRE_StructVectorAssemble
	.p2align	4, 0x90
	.type	HYPRE_StructVectorAssemble,@function
HYPRE_StructVectorAssemble:             # @HYPRE_StructVectorAssemble
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorAssemble # TAILCALL
.Lfunc_end9:
	.size	HYPRE_StructVectorAssemble, .Lfunc_end9-HYPRE_StructVectorAssemble
	.cfi_endproc

	.globl	HYPRE_StructVectorPrint
	.p2align	4, 0x90
	.type	HYPRE_StructVectorPrint,@function
HYPRE_StructVectorPrint:                # @HYPRE_StructVectorPrint
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorPrint # TAILCALL
.Lfunc_end10:
	.size	HYPRE_StructVectorPrint, .Lfunc_end10-HYPRE_StructVectorPrint
	.cfi_endproc

	.globl	HYPRE_StructVectorSetNumGhost
	.p2align	4, 0x90
	.type	HYPRE_StructVectorSetNumGhost,@function
HYPRE_StructVectorSetNumGhost:          # @HYPRE_StructVectorSetNumGhost
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorSetNumGhost # TAILCALL
.Lfunc_end11:
	.size	HYPRE_StructVectorSetNumGhost, .Lfunc_end11-HYPRE_StructVectorSetNumGhost
	.cfi_endproc

	.globl	HYPRE_StructVectorSetConstantValues
	.p2align	4, 0x90
	.type	HYPRE_StructVectorSetConstantValues,@function
HYPRE_StructVectorSetConstantValues:    # @HYPRE_StructVectorSetConstantValues
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorSetConstantValues # TAILCALL
.Lfunc_end12:
	.size	HYPRE_StructVectorSetConstantValues, .Lfunc_end12-HYPRE_StructVectorSetConstantValues
	.cfi_endproc

	.globl	HYPRE_StructVectorGetMigrateCommPkg
	.p2align	4, 0x90
	.type	HYPRE_StructVectorGetMigrateCommPkg,@function
HYPRE_StructVectorGetMigrateCommPkg:    # @HYPRE_StructVectorGetMigrateCommPkg
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	callq	hypre_StructVectorGetMigrateCommPkg
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	HYPRE_StructVectorGetMigrateCommPkg, .Lfunc_end13-HYPRE_StructVectorGetMigrateCommPkg
	.cfi_endproc

	.globl	HYPRE_StructVectorMigrate
	.p2align	4, 0x90
	.type	HYPRE_StructVectorMigrate,@function
HYPRE_StructVectorMigrate:              # @HYPRE_StructVectorMigrate
	.cfi_startproc
# BB#0:
	jmp	hypre_StructVectorMigrate # TAILCALL
.Lfunc_end14:
	.size	HYPRE_StructVectorMigrate, .Lfunc_end14-HYPRE_StructVectorMigrate
	.cfi_endproc

	.globl	HYPRE_CommPkgDestroy
	.p2align	4, 0x90
	.type	HYPRE_CommPkgDestroy,@function
HYPRE_CommPkgDestroy:                   # @HYPRE_CommPkgDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_CommPkgDestroy    # TAILCALL
.Lfunc_end15:
	.size	HYPRE_CommPkgDestroy, .Lfunc_end15-HYPRE_CommPkgDestroy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
