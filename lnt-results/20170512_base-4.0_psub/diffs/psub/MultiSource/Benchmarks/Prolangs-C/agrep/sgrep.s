	.text
	.file	"sgrep.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI0_1:
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.byte	26                      # 0x1a
	.byte	27                      # 0x1b
	.byte	28                      # 0x1c
	.byte	29                      # 0x1d
	.byte	30                      # 0x1e
	.byte	31                      # 0x1f
.LCPI0_2:
	.byte	32                      # 0x20
	.byte	33                      # 0x21
	.byte	34                      # 0x22
	.byte	35                      # 0x23
	.byte	36                      # 0x24
	.byte	37                      # 0x25
	.byte	38                      # 0x26
	.byte	39                      # 0x27
	.byte	40                      # 0x28
	.byte	41                      # 0x29
	.byte	42                      # 0x2a
	.byte	43                      # 0x2b
	.byte	44                      # 0x2c
	.byte	45                      # 0x2d
	.byte	46                      # 0x2e
	.byte	47                      # 0x2f
.LCPI0_3:
	.byte	48                      # 0x30
	.byte	49                      # 0x31
	.byte	50                      # 0x32
	.byte	51                      # 0x33
	.byte	52                      # 0x34
	.byte	53                      # 0x35
	.byte	54                      # 0x36
	.byte	55                      # 0x37
	.byte	56                      # 0x38
	.byte	57                      # 0x39
	.byte	58                      # 0x3a
	.byte	59                      # 0x3b
	.byte	60                      # 0x3c
	.byte	61                      # 0x3d
	.byte	62                      # 0x3e
	.byte	63                      # 0x3f
.LCPI0_4:
	.byte	64                      # 0x40
	.byte	65                      # 0x41
	.byte	66                      # 0x42
	.byte	67                      # 0x43
	.byte	68                      # 0x44
	.byte	69                      # 0x45
	.byte	70                      # 0x46
	.byte	71                      # 0x47
	.byte	72                      # 0x48
	.byte	73                      # 0x49
	.byte	74                      # 0x4a
	.byte	75                      # 0x4b
	.byte	76                      # 0x4c
	.byte	77                      # 0x4d
	.byte	78                      # 0x4e
	.byte	79                      # 0x4f
.LCPI0_5:
	.byte	80                      # 0x50
	.byte	81                      # 0x51
	.byte	82                      # 0x52
	.byte	83                      # 0x53
	.byte	84                      # 0x54
	.byte	85                      # 0x55
	.byte	86                      # 0x56
	.byte	87                      # 0x57
	.byte	88                      # 0x58
	.byte	89                      # 0x59
	.byte	90                      # 0x5a
	.byte	91                      # 0x5b
	.byte	92                      # 0x5c
	.byte	93                      # 0x5d
	.byte	94                      # 0x5e
	.byte	95                      # 0x5f
.LCPI0_6:
	.byte	96                      # 0x60
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
.LCPI0_7:
	.byte	112                     # 0x70
	.byte	113                     # 0x71
	.byte	114                     # 0x72
	.byte	115                     # 0x73
	.byte	116                     # 0x74
	.byte	117                     # 0x75
	.byte	118                     # 0x76
	.byte	119                     # 0x77
	.byte	120                     # 0x78
	.byte	121                     # 0x79
	.byte	122                     # 0x7a
	.byte	123                     # 0x7b
	.byte	124                     # 0x7c
	.byte	125                     # 0x7d
	.byte	126                     # 0x7e
	.byte	127                     # 0x7f
.LCPI0_8:
	.byte	128                     # 0x80
	.byte	129                     # 0x81
	.byte	130                     # 0x82
	.byte	131                     # 0x83
	.byte	132                     # 0x84
	.byte	133                     # 0x85
	.byte	134                     # 0x86
	.byte	135                     # 0x87
	.byte	136                     # 0x88
	.byte	137                     # 0x89
	.byte	138                     # 0x8a
	.byte	139                     # 0x8b
	.byte	140                     # 0x8c
	.byte	141                     # 0x8d
	.byte	142                     # 0x8e
	.byte	143                     # 0x8f
.LCPI0_9:
	.byte	144                     # 0x90
	.byte	145                     # 0x91
	.byte	146                     # 0x92
	.byte	147                     # 0x93
	.byte	148                     # 0x94
	.byte	149                     # 0x95
	.byte	150                     # 0x96
	.byte	151                     # 0x97
	.byte	152                     # 0x98
	.byte	153                     # 0x99
	.byte	154                     # 0x9a
	.byte	155                     # 0x9b
	.byte	156                     # 0x9c
	.byte	157                     # 0x9d
	.byte	158                     # 0x9e
	.byte	159                     # 0x9f
.LCPI0_10:
	.byte	160                     # 0xa0
	.byte	161                     # 0xa1
	.byte	162                     # 0xa2
	.byte	163                     # 0xa3
	.byte	164                     # 0xa4
	.byte	165                     # 0xa5
	.byte	166                     # 0xa6
	.byte	167                     # 0xa7
	.byte	168                     # 0xa8
	.byte	169                     # 0xa9
	.byte	170                     # 0xaa
	.byte	171                     # 0xab
	.byte	172                     # 0xac
	.byte	173                     # 0xad
	.byte	174                     # 0xae
	.byte	175                     # 0xaf
.LCPI0_11:
	.byte	176                     # 0xb0
	.byte	177                     # 0xb1
	.byte	178                     # 0xb2
	.byte	179                     # 0xb3
	.byte	180                     # 0xb4
	.byte	181                     # 0xb5
	.byte	182                     # 0xb6
	.byte	183                     # 0xb7
	.byte	184                     # 0xb8
	.byte	185                     # 0xb9
	.byte	186                     # 0xba
	.byte	187                     # 0xbb
	.byte	188                     # 0xbc
	.byte	189                     # 0xbd
	.byte	190                     # 0xbe
	.byte	191                     # 0xbf
.LCPI0_12:
	.byte	192                     # 0xc0
	.byte	193                     # 0xc1
	.byte	194                     # 0xc2
	.byte	195                     # 0xc3
	.byte	196                     # 0xc4
	.byte	197                     # 0xc5
	.byte	198                     # 0xc6
	.byte	199                     # 0xc7
	.byte	200                     # 0xc8
	.byte	201                     # 0xc9
	.byte	202                     # 0xca
	.byte	203                     # 0xcb
	.byte	204                     # 0xcc
	.byte	205                     # 0xcd
	.byte	206                     # 0xce
	.byte	207                     # 0xcf
.LCPI0_13:
	.byte	208                     # 0xd0
	.byte	209                     # 0xd1
	.byte	210                     # 0xd2
	.byte	211                     # 0xd3
	.byte	212                     # 0xd4
	.byte	213                     # 0xd5
	.byte	214                     # 0xd6
	.byte	215                     # 0xd7
	.byte	216                     # 0xd8
	.byte	217                     # 0xd9
	.byte	218                     # 0xda
	.byte	219                     # 0xdb
	.byte	220                     # 0xdc
	.byte	221                     # 0xdd
	.byte	222                     # 0xde
	.byte	223                     # 0xdf
.LCPI0_14:
	.byte	224                     # 0xe0
	.byte	225                     # 0xe1
	.byte	226                     # 0xe2
	.byte	227                     # 0xe3
	.byte	228                     # 0xe4
	.byte	229                     # 0xe5
	.byte	230                     # 0xe6
	.byte	231                     # 0xe7
	.byte	232                     # 0xe8
	.byte	233                     # 0xe9
	.byte	234                     # 0xea
	.byte	235                     # 0xeb
	.byte	236                     # 0xec
	.byte	237                     # 0xed
	.byte	238                     # 0xee
	.byte	239                     # 0xef
.LCPI0_15:
	.byte	240                     # 0xf0
	.byte	241                     # 0xf1
	.byte	242                     # 0xf2
	.byte	243                     # 0xf3
	.byte	244                     # 0xf4
	.byte	245                     # 0xf5
	.byte	246                     # 0xf6
	.byte	247                     # 0xf7
	.byte	248                     # 0xf8
	.byte	249                     # 0xf9
	.byte	250                     # 0xfa
	.byte	251                     # 0xfb
	.byte	252                     # 0xfc
	.byte	253                     # 0xfd
	.byte	254                     # 0xfe
	.byte	255                     # 0xff
.LCPI0_16:
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
	.byte	112                     # 0x70
	.text
	.globl	char_tr
	.p2align	4, 0x90
	.type	char_tr,@function
char_tr:                                # @char_tr
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$264, %rsp              # imm = 0x108
.Lcfi4:
	.cfi_def_cfa_offset 304
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movaps	%xmm0, TR(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	movaps	%xmm0, TR+16(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	movaps	%xmm0, TR+32(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	movaps	%xmm0, TR+48(%rip)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79]
	movaps	%xmm0, TR+64(%rip)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95]
	movaps	%xmm0, TR+80(%rip)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
	movaps	%xmm0, TR+96(%rip)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127]
	movaps	%xmm0, TR+112(%rip)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143]
	movaps	%xmm0, TR+128(%rip)
	movaps	.LCPI0_9(%rip), %xmm0   # xmm0 = [144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159]
	movaps	%xmm0, TR+144(%rip)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175]
	movaps	%xmm0, TR+160(%rip)
	movaps	.LCPI0_11(%rip), %xmm0  # xmm0 = [176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191]
	movaps	%xmm0, TR+176(%rip)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207]
	movaps	%xmm0, TR+192(%rip)
	movaps	.LCPI0_13(%rip), %xmm0  # xmm0 = [208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223]
	movaps	%xmm0, TR+208(%rip)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239]
	movaps	%xmm0, TR+224(%rip)
	movaps	.LCPI0_15(%rip), %xmm0  # xmm0 = [240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255]
	movaps	%xmm0, TR+240(%rip)
	cmpl	$0, NOUPPER(%rip)
	je	.LBB0_2
# BB#1:                                 # %.preheader29.preheader
	movaps	.LCPI0_16(%rip), %xmm0  # xmm0 = [97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112]
	movups	%xmm0, TR+65(%rip)
	movabsq	$8680537053616894577, %rax # imm = 0x7877767574737271
	movq	%rax, TR+81(%rip)
	movw	$31353, TR+89(%rip)     # imm = 0x7A79
.LBB0_2:                                # %.loopexit30
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB0_9
# BB#3:                                 # %.preheader
	xorl	%ebx, %ebx
	callq	__ctype_b_loc
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	testb	$8, (%rcx,%rbx,2)
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movb	$-128, TR(%rbx)
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	movq	(%rax), %rcx
	testb	$8, 2(%rcx,%rbx,2)
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	movb	$-128, TR+1(%rbx)
.LBB0_8:                                #   in Loop: Header=BB0_4 Depth=1
	addq	$2, %rbx
	cmpq	$128, %rbx
	jne	.LBB0_4
.LBB0_9:                                # %.loopexit
	cmpl	$0, WHOLELINE(%rip)
	je	.LBB0_11
# BB#10:
	movslq	(%r14), %rdx
	movq	%rsp, %r15
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movb	$10, (%r12)
	leaq	1(%r12), %rdi
	movslq	(%r14), %rdx
	movq	%r15, %rsi
	callq	memcpy
	movslq	(%r14), %rax
	movb	$10, 1(%r12,%rax)
	movslq	(%r14), %rax
	movb	$0, 2(%r12,%rax)
	addl	$2, (%r14)
.LBB0_11:
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	char_tr, .Lfunc_end0-char_tr
	.cfi_endproc

	.globl	s_output
	.p2align	4, 0x90
	.type	s_output,@function
s_output:                               # @s_output
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	$0, SILENT(%rip)
	je	.LBB1_1
.LBB1_13:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_1:
	cmpl	$0, COUNT(%rip)
	je	.LBB1_5
# BB#2:                                 # %.preheader13
	movslq	(%r14), %rcx
	cmpb	$10, (%r15,%rcx)
	je	.LBB1_13
# BB#3:                                 # %.lr.ph15.preheader
	leaq	1(%r15,%rcx), %rax
	leal	1(%rcx), %ecx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph15
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%r14)
	incl	%ecx
	cmpb	$10, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB1_4
	jmp	.LBB1_13
.LBB1_5:
	cmpl	$1, FNAME(%rip)
	jne	.LBB1_7
# BB#6:
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB1_7:
	movslq	(%r14), %rcx
	leaq	1(%rcx), %rbx
	shlq	$32, %rcx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rcx, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpb	$10, -2(%r15,%rbx)
	leaq	-1(%rbx), %rbx
	jne	.LBB1_8
# BB#9:                                 # %.preheader
	sarq	$32, %rax
	movb	(%r15,%rax), %cl
	cmpb	$10, %cl
	je	.LBB1_12
# BB#10:                                # %.lr.ph.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	1(%r15,%rbx), %ecx
	incq	%rbx
	cmpb	$10, %cl
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	%ebx, (%r14)
	jmp	.LBB1_13
.Lfunc_end1:
	.size	s_output, .Lfunc_end1-s_output
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI2_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	verify
	.p2align	4, 0x90
	.type	verify,@function
verify:                                 # @verify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$1944, %rsp             # imm = 0x798
.Lcfi21:
	.cfi_def_cfa_offset 2000
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	$-1, %edi
	jl	.LBB2_9
# BB#1:                                 # %.lr.ph139.preheader
	leal	2(%rdi), %r9d
	cmpl	$7, %r9d
	ja	.LBB2_4
# BB#2:
	xorl	%eax, %eax
	jmp	.LBB2_3
.LBB2_4:                                # %min.iters.checked
	movq	%r8, %r11
	movl	%r9d, %r8d
	andl	$7, %r8d
	movq	%r9, %rax
	subq	%r8, %rax
	je	.LBB2_5
# BB#6:                                 # %vector.body.preheader
	leaq	-96(%rsp), %rbx
	leaq	928(%rsp), %rbp
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI2_2(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rax, %r10
	.p2align	4, 0x90
.LBB2_7:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqa	%xmm0, -16(%rbx)
	movdqa	%xmm3, (%rbx)
	movdqa	%xmm0, -16(%rbp)
	movdqa	%xmm3, (%rbp)
	paddd	%xmm2, %xmm0
	addq	$32, %rbx
	addq	$32, %rbp
	addq	$-8, %r10
	jne	.LBB2_7
# BB#8:                                 # %middle.block
	testl	%r8d, %r8d
	movq	%r11, %r8
	jne	.LBB2_3
	jmp	.LBB2_9
.LBB2_5:
	xorl	%eax, %eax
	movq	%r11, %r8
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph139
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, -112(%rsp,%rax,4)
	movl	%eax, 912(%rsp,%rax,4)
	incq	%rax
	cmpq	%rax, %r9
	jne	.LBB2_3
.LBB2_9:                                # %.preheader124
	testl	%esi, %esi
	jle	.LBB2_61
# BB#10:                                # %.preheader123.lr.ph
	movslq	%esi, %r9
	addq	%r8, %r9
	leal	2(%rdi), %r11d
	movl	%r11d, %eax
	andl	$7, %eax
	movq	%r11, %r12
	movq	%rax, -120(%rsp)        # 8-byte Spill
	subq	%rax, %r12
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI2_2(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%r8, -128(%rsp)         # 8-byte Spill
	movl	%edx, %r15d
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader123
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_13 Depth 2
                                        #     Child Loop BB2_22 Depth 2
                                        #     Child Loop BB2_32 Depth 2
                                        #     Child Loop BB2_34 Depth 2
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_47 Depth 2
                                        #     Child Loop BB2_58 Depth 2
                                        #     Child Loop BB2_54 Depth 2
	testl	%r15d, %r15d
	jle	.LBB2_16
# BB#12:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	leal	1(%r15), %r13d
	decq	%r13
	movq	%rcx, %r14
	leaq	916(%rsp), %rsi
	leaq	-108(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp), %eax
	movzbl	(%r14), %ebx
	cmpb	(%r8), %bl
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_13 Depth=2
	movl	(%rbp), %ebx
	cmpl	%eax, %ebx
	cmovlel	%ebx, %eax
	movl	-4(%rsi), %ebx
	cmpl	%eax, %ebx
	cmovlel	%ebx, %eax
	incl	%eax
.LBB2_15:                               #   in Loop: Header=BB2_13 Depth=2
	movl	%eax, (%rsi)
	addq	$4, %rbp
	addq	$4, %rsi
	incq	%r14
	decq	%r13
	jne	.LBB2_13
.LBB2_16:                               # %._crit_edge
                                        #   in Loop: Header=BB2_11 Depth=1
	movslq	%r15d, %rsi
	movb	(%rcx,%rsi), %al
	cmpb	(%r8), %al
	jne	.LBB2_17
# BB#18:                                #   in Loop: Header=BB2_11 Depth=1
	movl	-112(%rsp,%rsi,4), %eax
	incl	%r15d
	movl	%eax, 916(%rsp,%rsi,4)
	cmpl	%edx, %eax
	jl	.LBB2_20
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_17:                               # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB2_11 Depth=1
	movl	912(%rsp,%rsi,4), %eax
	cmpl	%edx, %eax
	jge	.LBB2_21
.LBB2_20:                               #   in Loop: Header=BB2_11 Depth=1
	incl	%eax
	movslq	%r15d, %rsi
	incl	%r15d
	movl	%eax, 920(%rsp,%rsi,4)
.LBB2_21:                               # %.preheader122.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	leaq	1(%r8), %rax
	movslq	%r15d, %rsi
	leal	1(%rsi), %ebp
	leaq	912(%rsp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB2_22:                               # %.preheader122
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebp
	cmpl	%edx, (%rsi)
	leaq	-4(%rsi), %rsi
	jg	.LBB2_22
# BB#23:                                #   in Loop: Header=BB2_11 Depth=1
	cmpl	%edi, %ebp
	jge	.LBB2_24
# BB#25:                                #   in Loop: Header=BB2_11 Depth=1
	cmpb	$10, (%rax)
	cmovel	%edx, %ebp
	jne	.LBB2_36
# BB#26:                                #   in Loop: Header=BB2_11 Depth=1
	cmpl	$-1, %edi
	jl	.LBB2_36
# BB#27:                                # %.lr.ph128.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpl	$8, %r11d
	jae	.LBB2_29
# BB#28:                                #   in Loop: Header=BB2_11 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_29:                               # %min.iters.checked194
                                        #   in Loop: Header=BB2_11 Depth=1
	testq	%r12, %r12
	je	.LBB2_30
# BB#31:                                # %vector.body190.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	movq	%r12, %rsi
	leaq	928(%rsp), %rbx
	leaq	-96(%rsp), %rbp
	movdqa	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB2_32:                               # %vector.body190
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm3, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm3, -16(%rbp)
	movdqa	%xmm4, (%rbp)
	movdqa	%xmm3, -16(%rbx)
	movdqa	%xmm4, (%rbx)
	paddd	%xmm2, %xmm3
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rsi
	jne	.LBB2_32
# BB#33:                                # %middle.block191
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpl	$0, -120(%rsp)          # 4-byte Folded Reload
	movq	%r12, %rsi
	movl	%edx, %ebp
	jne	.LBB2_34
	jmp	.LBB2_36
.LBB2_30:                               #   in Loop: Header=BB2_11 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_34:                               # %.lr.ph128
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, -112(%rsp,%rsi,4)
	movl	%esi, 912(%rsp,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %r11
	jne	.LBB2_34
# BB#35:                                #   in Loop: Header=BB2_11 Depth=1
	movl	%edx, %ebp
.LBB2_36:                               # %.loopexit121
                                        #   in Loop: Header=BB2_11 Depth=1
	testl	%ebp, %ebp
	jle	.LBB2_41
# BB#37:                                # %.lr.ph131.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	leal	1(%rbp), %r15d
	decq	%r15
	movq	%rcx, %rbx
	leaq	-108(%rsp), %rsi
	leaq	916(%rsp), %r13
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph131
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r13), %r14d
	movzbl	(%rbx), %r10d
	cmpb	(%rax), %r10b
	je	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_38 Depth=2
	movl	(%r13), %r10d
	cmpl	%r14d, %r10d
	cmovlel	%r10d, %r14d
	movl	-4(%rsi), %r10d
	cmpl	%r14d, %r10d
	cmovlel	%r10d, %r14d
	incl	%r14d
.LBB2_40:                               #   in Loop: Header=BB2_38 Depth=2
	movl	%r14d, (%rsi)
	addq	$4, %r13
	addq	$4, %rsi
	incq	%rbx
	decq	%r15
	jne	.LBB2_38
.LBB2_41:                               # %._crit_edge132
                                        #   in Loop: Header=BB2_11 Depth=1
	movslq	%ebp, %rsi
	movb	(%rcx,%rsi), %al
	cmpb	1(%r8), %al
	jne	.LBB2_42
# BB#43:                                #   in Loop: Header=BB2_11 Depth=1
	movl	912(%rsp,%rsi,4), %eax
	incl	%ebp
	movl	%eax, -108(%rsp,%rsi,4)
	cmpl	%edx, %eax
	jl	.LBB2_45
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_42:                               # %._crit_edge132._crit_edge
                                        #   in Loop: Header=BB2_11 Depth=1
	movl	-112(%rsp,%rsi,4), %eax
	cmpl	%edx, %eax
	jge	.LBB2_46
.LBB2_45:                               #   in Loop: Header=BB2_11 Depth=1
	incl	%eax
	movslq	%ebp, %rsi
	incl	%ebp
	movl	%eax, -104(%rsp,%rsi,4)
.LBB2_46:                               # %.preheader119.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	addq	$2, %r8
	movslq	%ebp, %rax
	leal	1(%rax), %r15d
	leaq	-112(%rsp,%rax,4), %rax
	.p2align	4, 0x90
.LBB2_47:                               # %.preheader119
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%r15d
	cmpl	%edx, (%rax)
	leaq	-4(%rax), %rax
	jg	.LBB2_47
# BB#48:                                #   in Loop: Header=BB2_11 Depth=1
	cmpl	%edi, %r15d
	jge	.LBB2_49
# BB#50:                                #   in Loop: Header=BB2_11 Depth=1
	cmpb	$10, (%r8)
	cmovel	%edx, %r15d
	jne	.LBB2_60
# BB#51:                                #   in Loop: Header=BB2_11 Depth=1
	cmpl	$-1, %edi
	jl	.LBB2_60
# BB#52:                                # %.lr.ph134.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpl	$7, %r11d
	jbe	.LBB2_53
# BB#56:                                # %min.iters.checked173
                                        #   in Loop: Header=BB2_11 Depth=1
	testq	%r12, %r12
	je	.LBB2_53
# BB#57:                                # %vector.body169.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	movq	%r12, %rax
	leaq	928(%rsp), %rsi
	leaq	-96(%rsp), %rbx
	movdqa	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB2_58:                               # %vector.body169
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm3, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm3, -16(%rbx)
	movdqa	%xmm4, (%rbx)
	movdqa	%xmm3, -16(%rsi)
	movdqa	%xmm4, (%rsi)
	paddd	%xmm2, %xmm3
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB2_58
# BB#59:                                # %middle.block170
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpl	$0, -120(%rsp)          # 4-byte Folded Reload
	movq	%r12, %rax
	movl	%edx, %r15d
	jne	.LBB2_54
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_53:                               #   in Loop: Header=BB2_11 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_54:                               # %.lr.ph134
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, -112(%rsp,%rax,4)
	movl	%eax, 912(%rsp,%rax,4)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB2_54
# BB#55:                                #   in Loop: Header=BB2_11 Depth=1
	movl	%edx, %r15d
.LBB2_60:                               # %.backedge
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpq	%r9, %r8
	jb	.LBB2_11
.LBB2_61:
	xorl	%eax, %eax
	jmp	.LBB2_62
.LBB2_49:
	movq	%r8, %rax
.LBB2_24:
	movq	-128(%rsp), %rcx        # 8-byte Reload
	subl	%ecx, %eax
	decl	%eax
.LBB2_62:                               # %.loopexit125
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$1944, %rsp             # imm = 0x798
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	verify, .Lfunc_end2-verify
	.cfi_endproc

	.globl	bm
	.p2align	4, 0x90
	.type	bm,@function
bm:                                     # @bm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rdi, %r15
	cmpq	%r14, %rbx
	ja	.LBB3_14
# BB#1:                                 # %.lr.ph55
	movl	shift_1(%rip), %edi
	movl	%esi, %r13d
	movslq	%esi, %rbp
	negq	%rbp
	leaq	-1(%r13), %r12
	xorl	%eax, %eax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_25 Depth 2
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_18 Depth 2
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_23 Depth 2
	cltq
	movzbl	(%rbx,%rax), %ecx
	leaq	(%rbx,%rax), %rbx
	movb	SHIFT(%rcx), %al
	testb	%al, %al
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %eax
	leaq	(%rbx,%rax), %rcx
	movzbl	(%rbx,%rax), %eax
	movzbl	SHIFT(%rax), %eax
	leaq	(%rcx,%rax), %rdx
	movzbl	(%rax,%rcx), %eax
	movzbl	SHIFT(%rax), %eax
	leaq	(%rdx,%rax), %rbx
	movzbl	(%rax,%rdx), %eax
	movzbl	SHIFT(%rax), %eax
	testb	%al, %al
	jne	.LBB3_25
.LBB3_4:                                # %.preheader45
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r12,%rcx), %edx
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %edx
	movzbl	TR(%rdx), %edx
	movzbl	(%rbx,%rcx), %esi
	cmpb	TR(%rsi), %dl
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=2
	incl	%eax
	decq	%rcx
	movl	%ecx, %edx
	addl	%r13d, %edx
	jne	.LBB3_5
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	%r13d, %eax
	jne	.LBB3_8
# BB#9:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r14, %rbx
	ja	.LBB3_14
# BB#10:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB3_15
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movzbl	1(%rbx), %ecx
	movl	$1, %eax
	cmpb	$-128, TR(%rcx)
	jne	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movzbl	(%rbx,%rbp), %ecx
	cmpb	$-128, TR(%rcx)
	jne	.LBB3_13
.LBB3_15:                               #   in Loop: Header=BB3_2 Depth=1
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB3_14
# BB#16:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB3_20
# BB#17:                                # %.preheader43.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB3_18:                               # %.preheader43
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, 1(%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB3_18
# BB#19:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %eax
	cmpq	%r14, %rbx
	jbe	.LBB3_2
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%edi, %eax
.LBB3_13:                               # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r14, %rbx
	jbe	.LBB3_2
	jmp	.LBB3_14
.LBB3_20:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, FNAME(%rip)
	je	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
	.p2align	4, 0x90
.LBB3_22:                               # %.preheader42
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, -1(%rbx)
	leaq	-1(%rbx), %rbx
	jne	.LBB3_22
	.p2align	4, 0x90
.LBB3_23:                               # %.preheader
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rbx), %ebp
	incq	%rbx
	movq	stdout(%rip), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
	cmpl	$10, %ebp
	jne	.LBB3_23
# BB#24:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %eax
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmpq	%r14, %rbx
	jbe	.LBB3_2
.LBB3_14:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	bm, .Lfunc_end3-bm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	4                       # 0x4
	.quad	5                       # 0x5
.LCPI4_1:
	.quad	536870912               # 0x20000000
	.quad	268435456               # 0x10000000
.LCPI4_2:
	.quad	2147483648              # 0x80000000
	.quad	1073741824              # 0x40000000
.LCPI4_3:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
.LCPI4_4:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI4_5:
	.quad	2147483648              # 0x80000000
	.quad	2147483648              # 0x80000000
.LCPI4_6:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
.LCPI4_7:
	.quad	6                       # 0x6
	.quad	6                       # 0x6
.LCPI4_8:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.text
	.globl	initmask
	.p2align	4, 0x90
	.type	initmask,@function
initmask:                               # @initmask
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r13, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movl	%ecx, %eax
	movl	%edx, %r14d
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	$0, (%r8)
	testl	%eax, %eax
	js	.LBB4_1
# BB#2:                                 # %.lr.ph63.preheader
	leal	1(%rax), %edx
	xorl	%edi, %edi
	cmpl	$4, %edx
	jae	.LBB4_4
# BB#3:
	xorl	%ecx, %ecx
	jmp	.LBB4_13
.LBB4_1:
	xorl	%edi, %edi
	jmp	.LBB4_15
.LBB4_4:                                # %min.iters.checked
	movl	%edx, %esi
	andl	$-4, %esi
	movl	$0, %ecx
	je	.LBB4_13
# BB#5:                                 # %vector.body.preheader
	leal	-4(%rsi), %edi
	movl	%edi, %ecx
	shrl	$2, %ecx
	btl	$2, %edi
	jb	.LBB4_6
# BB#7:                                 # %vector.body.prol
	movdqa	.LCPI4_0(%rip), %xmm3   # xmm3 = [4,5]
	movapd	.LCPI4_1(%rip), %xmm2   # xmm2 = [536870912,268435456]
	movapd	.LCPI4_2(%rip), %xmm1   # xmm1 = [2147483648,1073741824]
	movl	$4, %edi
	testl	%ecx, %ecx
	jne	.LBB4_10
	jmp	.LBB4_9
.LBB4_6:
	movl	$1, %edi
	movd	%rdi, %xmm3
	pslldq	$8, %xmm3               # xmm3 = zero,zero,zero,zero,zero,zero,zero,zero,xmm3[0,1,2,3,4,5,6,7]
	xorpd	%xmm1, %xmm1
	xorl	%edi, %edi
	xorpd	%xmm2, %xmm2
	testl	%ecx, %ecx
	je	.LBB4_9
.LBB4_10:                               # %vector.body.preheader.new
	movl	%esi, %ecx
	subl	%edi, %ecx
	movdqa	.LCPI4_3(%rip), %xmm8   # xmm8 = [2,2]
	movdqa	.LCPI4_4(%rip), %xmm12  # xmm12 = [4294967295,0,4294967295,0]
	movdqa	.LCPI4_5(%rip), %xmm7   # xmm7 = [2147483648,2147483648]
	movdqa	.LCPI4_6(%rip), %xmm9   # xmm9 = [4,4]
	movdqa	.LCPI4_7(%rip), %xmm10  # xmm10 = [6,6]
	movdqa	.LCPI4_8(%rip), %xmm11  # xmm11 = [8,8]
	.p2align	4, 0x90
.LBB4_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm0
	paddq	%xmm8, %xmm0
	pand	%xmm12, %xmm0
	movdqa	%xmm3, %xmm6
	pand	%xmm12, %xmm6
	pshufd	$78, %xmm6, %xmm4       # xmm4 = xmm6[2,3,0,1]
	movdqa	%xmm7, %xmm5
	psrlq	%xmm4, %xmm5
	movdqa	%xmm7, %xmm4
	psrlq	%xmm6, %xmm4
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	pshufd	$78, %xmm0, %xmm4       # xmm4 = xmm0[2,3,0,1]
	movdqa	%xmm7, %xmm6
	psrlq	%xmm4, %xmm6
	movdqa	%xmm7, %xmm4
	psrlq	%xmm0, %xmm4
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	orpd	%xmm1, %xmm5
	orpd	%xmm2, %xmm6
	movdqa	%xmm3, %xmm0
	paddq	%xmm9, %xmm0
	pand	%xmm12, %xmm0
	movdqa	%xmm3, %xmm4
	paddq	%xmm10, %xmm4
	pand	%xmm12, %xmm4
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	movdqa	%xmm7, %xmm1
	psrlq	%xmm2, %xmm1
	movdqa	%xmm7, %xmm2
	psrlq	%xmm0, %xmm2
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	pshufd	$78, %xmm4, %xmm0       # xmm0 = xmm4[2,3,0,1]
	movdqa	%xmm7, %xmm2
	psrlq	%xmm0, %xmm2
	movdqa	%xmm7, %xmm0
	psrlq	%xmm4, %xmm0
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	orpd	%xmm5, %xmm1
	orpd	%xmm6, %xmm2
	paddq	%xmm11, %xmm3
	addl	$-8, %ecx
	jne	.LBB4_11
	jmp	.LBB4_12
.LBB4_9:
	movapd	.LCPI4_1(%rip), %xmm2   # xmm2 = [536870912,268435456]
	movapd	.LCPI4_2(%rip), %xmm1   # xmm1 = [2147483648,1073741824]
.LBB4_12:                               # %middle.block
	orpd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	por	%xmm1, %xmm0
	movd	%xmm0, %edi
	cmpl	%esi, %edx
	movl	%esi, %ecx
	je	.LBB4_14
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2147483648, %esi      # imm = 0x80000000
	shrl	%cl, %esi
	orl	%esi, %edi
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB4_13
.LBB4_14:                               # %._crit_edge64
	movl	%edi, (%r8)
.LBB4_15:
	leal	-1(%r14), %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movl	%edi, (%r8)
	testl	%r14d, %r14d
	jle	.LBB4_52
# BB#16:                                # %.lr.ph60.preheader
	movl	%r14d, %r12d
	leaq	-1(%r12), %rax
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	andq	$3, %rdx
	je	.LBB4_21
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph60.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rcx), %ebx
	cmpb	$94, %bl
	je	.LBB4_19
# BB#18:                                # %.lr.ph60.prol
                                        #   in Loop: Header=BB4_17 Depth=1
	cmpb	$36, %bl
	jne	.LBB4_20
.LBB4_19:                               #   in Loop: Header=BB4_17 Depth=1
	movb	$10, (%r15,%rcx)
.LBB4_20:                               #   in Loop: Header=BB4_17 Depth=1
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB4_17
.LBB4_21:                               # %.lr.ph60.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB4_36
# BB#22:                                # %.lr.ph60.preheader.new
	movq	%r12, %rax
	subq	%rcx, %rax
	leaq	3(%r15,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB4_25
# BB#24:                                # %.lr.ph60
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpb	$36, %dl
	jne	.LBB4_26
.LBB4_25:                               #   in Loop: Header=BB4_23 Depth=1
	movb	$10, -3(%rcx)
.LBB4_26:                               # %.lr.ph60.190
                                        #   in Loop: Header=BB4_23 Depth=1
	movzbl	-2(%rcx), %edx
	cmpb	$36, %dl
	je	.LBB4_28
# BB#27:                                # %.lr.ph60.190
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpb	$94, %dl
	jne	.LBB4_29
.LBB4_28:                               #   in Loop: Header=BB4_23 Depth=1
	movb	$10, -2(%rcx)
.LBB4_29:                               # %.lr.ph60.291
                                        #   in Loop: Header=BB4_23 Depth=1
	movzbl	-1(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB4_31
# BB#30:                                # %.lr.ph60.291
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpb	$36, %dl
	jne	.LBB4_32
.LBB4_31:                               #   in Loop: Header=BB4_23 Depth=1
	movb	$10, -1(%rcx)
.LBB4_32:                               # %.lr.ph60.392
                                        #   in Loop: Header=BB4_23 Depth=1
	movzbl	(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB4_34
# BB#33:                                # %.lr.ph60.392
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpb	$36, %dl
	jne	.LBB4_35
.LBB4_34:                               #   in Loop: Header=BB4_23 Depth=1
	movb	$10, (%rcx)
.LBB4_35:                               #   in Loop: Header=BB4_23 Depth=1
	addq	$4, %rcx
	addq	$-4, %rax
	jne	.LBB4_23
.LBB4_36:                               # %.preheader52.preheader
	movl	$255, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r13, %rdi
	callq	memset
	testl	%r14d, %r14d
	jle	.LBB4_51
# BB#37:                                # %.lr.ph55.split.us.preheader
	movl	%r12d, %r9d
	andl	$1, %r9d
	leaq	1(%r15), %r8
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph55.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_45 Depth 2
	testq	%r9, %r9
	movzbl	(%r15,%r10), %edi
	jne	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_38 Depth=1
	xorl	%r11d, %r11d
	cmpl	$1, %r14d
	jne	.LBB4_44
	jmp	.LBB4_50
	.p2align	4, 0x90
.LBB4_40:                               #   in Loop: Header=BB4_38 Depth=1
	cmpb	(%r15), %dil
	jne	.LBB4_42
# BB#41:                                #   in Loop: Header=BB4_38 Depth=1
	andb	$127, 3(%r13,%rdi,4)
.LBB4_42:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_38 Depth=1
	movl	$1, %r11d
	cmpl	$1, %r14d
	je	.LBB4_50
.LBB4_44:                               # %.lr.ph55.split.us.new
                                        #   in Loop: Header=BB4_38 Depth=1
	movq	%r12, %rax
	subq	%r11, %rax
	leaq	(%r8,%r11), %rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_45:                               #   Parent Loop BB4_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	-1(%rbx,%rsi), %dil
	jne	.LBB4_47
# BB#46:                                #   in Loop: Header=BB4_45 Depth=2
	leal	(%r11,%rsi), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	notl	%edx
	andl	%edx, (%r13,%rdi,4)
.LBB4_47:                               #   in Loop: Header=BB4_45 Depth=2
	cmpb	(%rbx,%rsi), %dil
	jne	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_45 Depth=2
	leal	1(%r11,%rsi), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	notl	%edx
	andl	%edx, (%r13,%rdi,4)
.LBB4_49:                               #   in Loop: Header=BB4_45 Depth=2
	addq	$2, %rsi
	cmpq	%rsi, %rax
	jne	.LBB4_45
.LBB4_50:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_38 Depth=1
	incq	%r10
	cmpq	%r12, %r10
	jne	.LBB4_38
	jmp	.LBB4_51
.LBB4_52:                               # %.preheader52.preheader.thread
	movl	$255, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r13, %rdi
	callq	memset
.LBB4_51:                               # %._crit_edge56
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	initmask, .Lfunc_end4-initmask
	.cfi_endproc

	.globl	prep
	.p2align	4, 0x90
	.type	prep,@function
prep:                                   # @prep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 128
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%edx, %r9d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	leal	1(%r9), %r14d
	movl	%esi, %eax
	cltd
	idivl	%r14d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, %ecx
	imull	%r14d, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movd	%eax, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, SHIFT+240(%rip)
	movdqa	%xmm0, SHIFT+224(%rip)
	movdqa	%xmm0, SHIFT+208(%rip)
	movdqa	%xmm0, SHIFT+192(%rip)
	movdqa	%xmm0, SHIFT+176(%rip)
	movdqa	%xmm0, SHIFT+160(%rip)
	movdqa	%xmm0, SHIFT+144(%rip)
	movdqa	%xmm0, SHIFT+128(%rip)
	movdqa	%xmm0, SHIFT+112(%rip)
	movdqa	%xmm0, SHIFT+96(%rip)
	movdqa	%xmm0, SHIFT+80(%rip)
	movdqa	%xmm0, SHIFT+64(%rip)
	movdqa	%xmm0, SHIFT+48(%rip)
	movdqa	%xmm0, SHIFT+32(%rip)
	movdqa	%xmm0, SHIFT+16(%rip)
	movdqa	%xmm0, SHIFT(%rip)
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	%esi, %eax
	jge	.LBB5_5
# BB#1:                                 # %.lr.ph113.preheader
	movslq	%esi, %rdi
	movslq	%eax, %r8
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph113
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	8(%rsp)                 # 4-byte Folded Reload
	movzbl	-1(%rbx,%rdi), %eax
	decq	%rdi
	movzbl	SHIFT(%rax), %ebp
	cmpl	%edx, %ebp
	jle	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movb	%dl, SHIFT(%rax)
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
	incl	%ecx
	cmpq	%r8, %rdi
	jg	.LBB5_2
.LBB5_5:                                # %._crit_edge114
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, shift_1(%rip)
	testl	%r9d, %r9d
	setns	%al
	cmpl	$2, %ecx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	jb	.LBB5_6
# BB#7:                                 # %._crit_edge114
	testb	%al, %al
	je	.LBB5_6
# BB#8:                                 # %.preheader83.us104.preheader.preheader
	leal	-1(%rsi), %r11d
	movl	%r14d, %r15d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	andl	$1, %r15d
	movl	%r11d, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rax), %ebp
	xorl	%edx, %edx
	movl	%eax, %ecx
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB5_9:                                # %.preheader83.us104.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_10 Depth 2
                                        #       Child Loop BB5_12 Depth 3
	movq	%rdx, 40(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	imull	8(%rsp), %edx           # 4-byte Folded Reload
	movl	%r11d, %edi
	subl	%edx, %edi
	movl	$1, %edx
	.p2align	4, 0x90
.LBB5_10:                               # %.preheader83.us104
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_12 Depth 3
	movl	%edi, %eax
	subl	%edx, %eax
	testq	%r15, %r15
	movslq	%eax, %r13
	jne	.LBB5_31
# BB#11:                                #   in Loop: Header=BB5_10 Depth=2
	movl	%ecx, %r14d
	xorl	%esi, %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB5_36
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_31:                               #   in Loop: Header=BB5_10 Depth=2
	movb	(%rbx,%r13), %al
	movq	48(%rsp), %rsi          # 8-byte Reload
	cmpb	(%rbx,%rsi), %al
	jne	.LBB5_32
# BB#33:                                #   in Loop: Header=BB5_10 Depth=2
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	movl	%ecx, %r14d
	movl	$1, %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	jge	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_10 Depth=2
	movl	%edx, shift_1(%rip)
	movl	%edx, %ecx
	movl	%edx, %r14d
	movl	$1, %esi
	testl	%eax, %eax
	jne	.LBB5_36
	jmp	.LBB5_19
.LBB5_32:                               #   in Loop: Header=BB5_10 Depth=2
	movl	%ecx, %r14d
	movl	$1, %esi
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_35:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_10 Depth=2
	testl	%eax, %eax
	je	.LBB5_19
.LBB5_36:                               # %.preheader83.us104.new
                                        #   in Loop: Header=BB5_10 Depth=2
	movq	56(%rsp), %r10          # 8-byte Reload
	subq	%rsi, %r10
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	imull	%esi, %eax
	movl	%r11d, %r8d
	subl	%eax, %r8d
	incl	%esi
	imull	%ecx, %esi
	movl	%r11d, %r9d
	subl	%esi, %r9d
	xorl	%r12d, %r12d
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB5_12:                               #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx,%r13), %eax
	leal	(%r8,%r12), %esi
	cmpb	(%rbx,%rsi), %al
	jne	.LBB5_15
# BB#13:                                #   in Loop: Header=BB5_12 Depth=3
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jge	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_12 Depth=3
	movl	%edx, shift_1(%rip)
	movl	%edx, %ecx
.LBB5_15:                               #   in Loop: Header=BB5_12 Depth=3
	movzbl	(%rbx,%r13), %eax
	leal	(%r9,%r12), %esi
	cmpb	(%rbx,%rsi), %al
	jne	.LBB5_18
# BB#16:                                #   in Loop: Header=BB5_12 Depth=3
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jge	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_12 Depth=3
	movl	%edx, shift_1(%rip)
	movl	%edx, %ecx
.LBB5_18:                               #   in Loop: Header=BB5_12 Depth=3
	subl	%ebp, %r12d
	addq	$-2, %r10
	jne	.LBB5_12
.LBB5_19:                               # %._crit_edge93.us108
                                        #   in Loop: Header=BB5_10 Depth=2
	incq	%rdx
	cmpq	64(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB5_10
# BB#20:                                # %._crit_edge96.us
                                        #   in Loop: Header=BB5_9 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	incl	%edx
	movl	28(%rsp), %r14d         # 4-byte Reload
	cmpl	%r14d, %edx
	jne	.LBB5_9
	jmp	.LBB5_21
.LBB5_6:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
.LBB5_21:                               # %._crit_edge100
	testl	%ecx, %ecx
	jne	.LBB5_23
# BB#22:
	movl	$1, shift_1(%rip)
.LBB5_23:                               # %.preheader.preheader
	movl	$MEMBER, %edi
	xorl	%esi, %esi
	movl	$8192, %edx             # imm = 0x2000
	callq	memset
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$3, %ecx
	movl	$3, %eax
	cmovbl	%ecx, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	js	.LBB5_40
# BB#24:                                # %.lr.ph88
	testl	%eax, %eax
	je	.LBB5_25
# BB#37:                                # %.lr.ph88.split.us.preheader
	addl	$-3, %esi
	.p2align	4, 0x90
.LBB5_38:                               # =>This Inner Loop Header: Depth=1
	leal	2(%rsi), %ecx
	cmpl	$1, %eax
	movslq	%ecx, %rcx
	movzbl	(%rbx,%rcx), %ecx
	jbe	.LBB5_39
# BB#41:                                #   in Loop: Header=BB5_38 Depth=1
	leal	1(%rsi), %edx
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %edx
	leal	(%rdx,%rcx,4), %ecx
	cmpl	$3, 8(%rsp)             # 4-byte Folded Reload
	jb	.LBB5_39
# BB#42:                                #   in Loop: Header=BB5_38 Depth=1
	movslq	%esi, %rdx
	movzbl	(%rbx,%rdx), %edx
	leal	(%rdx,%rcx,4), %ecx
.LBB5_39:                               # %._crit_edge.us
                                        #   in Loop: Header=BB5_38 Depth=1
	movl	%ecx, %ecx
	movb	$1, MEMBER(%rcx)
	subl	8(%rsp), %esi           # 4-byte Folded Reload
	decl	%r14d
	jne	.LBB5_38
	jmp	.LBB5_40
.LBB5_25:                               # %.lr.ph88.split.preheader
	xorl	%eax, %eax
	andl	$7, %r14d
	je	.LBB5_27
	.p2align	4, 0x90
.LBB5_26:                               # %.lr.ph88.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %r14d
	jne	.LBB5_26
.LBB5_27:                               # %.lr.ph88.split.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB5_30
# BB#28:                                # %.lr.ph88.split.preheader.new
	incl	%ecx
	subl	%eax, %ecx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph88.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$-8, %ecx
	jne	.LBB5_29
.LBB5_30:                               # %._crit_edge89.loopexit
	movb	$1, MEMBER(%rip)
.LBB5_40:                               # %._crit_edge89
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	prep, .Lfunc_end5-prep
	.cfi_endproc

	.globl	agrep
	.p2align	4, 0x90
	.type	agrep,@function
agrep:                                  # @agrep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$16696, %rsp            # imm = 0x4138
.Lcfi70:
	.cfi_def_cfa_offset 16752
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r12
	movq	%rdx, %r9
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	1(%r8), %ecx
	movl	%esi, %eax
	cltd
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	idivl	%ecx
	movq	$0, 304(%rsp)
	movl	shift_1(%rip), %r14d
	cmpl	$4, %eax
	movl	$3, %edi
	cmovll	%eax, %edi
	decl	%eax
	movq	%r8, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	(%r8,%rsi), %r8d
	movl	%edi, %r13d
	addl	$3, %r13d
	leaq	-2(%rdi), %r11
	andl	$3, %r13d
	movq	%r13, %rbp
	negq	%rbp
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	movq	%r9, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpl	$1, %edi
	ja	.LBB6_13
	.p2align	4, 0x90
.LBB6_1:                                # %.outer.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
	cmpq	%r12, %r9
	jae	.LBB6_29
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	cltq
	leaq	(%r9,%rax), %rcx
	cmpq	%r12, %rcx
	jae	.LBB6_29
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	movzbl	(%r9,%rax), %eax
	movb	SHIFT(%rax), %al
	movq	%rcx, %r9
	testb	%al, %al
	je	.LBB6_8
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader201
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %eax
	addq	%r9, %rax
	cmpq	%r12, %rax
	jae	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=2
	movzbl	(%rax), %ecx
	movzbl	SHIFT(%rcx), %ecx
	leaq	(%rax,%rcx), %r9
	cmpq	%r12, %r9
	jae	.LBB6_8
# BB#4:                                 #   in Loop: Header=BB6_5 Depth=2
	movzbl	(%rax,%rcx), %eax
	movzbl	SHIFT(%rax), %eax
	testb	%al, %al
	jne	.LBB6_5
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_1 Depth=1
	movq	%rax, %r9
.LBB6_8:                                # %.loopexit202
                                        #   in Loop: Header=BB6_1 Depth=1
	cmpq	%r12, %r9
	jae	.LBB6_29
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	movzbl	(%r9), %eax
	cmpb	$0, MEMBER(%rax)
	movl	%r14d, %eax
	je	.LBB6_1
.LBB6_10:                               # %.us-lcssa235.us
	movl	%r9d, %eax
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ecx
	subl	%esi, %ecx
	subl	16(%rsp), %ecx          # 4-byte Folded Reload
	leal	-10(%rcx), %edx
	cmpl	48(%rsp), %edx          # 4-byte Folded Reload
	jle	.LBB6_12
# BB#11:
	addl	$-2, %ecx
	movslq	%r10d, %rdx
	incl	%r10d
	movl	%ecx, 312(%rsp,%rdx,8)
.LBB6_12:
	addl	%r8d, %eax
	movslq	%r10d, %rcx
	movl	%eax, 308(%rsp,%rcx,8)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	cmpl	$1, %edi
	jbe	.LBB6_1
	.p2align	4, 0x90
.LBB6_13:                               # %.outer.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
                                        #     Child Loop BB6_23 Depth 2
                                        #     Child Loop BB6_27 Depth 2
	cmpq	%r12, %r9
	jae	.LBB6_29
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	cltq
	leaq	(%r9,%rax), %rcx
	cmpq	%r12, %rcx
	jae	.LBB6_29
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	movzbl	(%r9,%rax), %eax
	movb	SHIFT(%rax), %al
	movq	%rcx, %r9
	testb	%al, %al
	je	.LBB6_20
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader201.us
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %eax
	addq	%r9, %rax
	cmpq	%r12, %rax
	jae	.LBB6_19
# BB#18:                                #   in Loop: Header=BB6_17 Depth=2
	movzbl	(%rax), %ecx
	movzbl	SHIFT(%rcx), %ecx
	leaq	(%rax,%rcx), %r9
	cmpq	%r12, %r9
	jae	.LBB6_20
# BB#16:                                #   in Loop: Header=BB6_17 Depth=2
	movzbl	(%rax,%rcx), %eax
	movzbl	SHIFT(%rax), %eax
	testb	%al, %al
	jne	.LBB6_17
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_13 Depth=1
	movq	%rax, %r9
.LBB6_20:                               # %.loopexit202.us
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpq	%r12, %r9
	jae	.LBB6_29
# BB#21:                                # %.lr.ph232.us
                                        #   in Loop: Header=BB6_13 Depth=1
	testq	%r13, %r13
	movzbl	(%r9), %eax
	je	.LBB6_25
# BB#22:                                # %.prol.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_23:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r9,%rdx), %ecx
	leal	(%rcx,%rax,4), %eax
	decq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB6_23
# BB#24:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	$1, %ecx
	subq	%rdx, %rcx
	cmpq	$3, %r11
	jae	.LBB6_26
	jmp	.LBB6_28
	.p2align	4, 0x90
.LBB6_25:                               #   in Loop: Header=BB6_13 Depth=1
	movl	$1, %ecx
	cmpq	$3, %r11
	jb	.LBB6_28
.LBB6_26:                               # %.lr.ph232.us.new
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	%r9, %rdx
	subq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_27:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %r15d
	leal	(%r15,%rax,4), %eax
	movzbl	-1(%rdx), %ebx
	leal	(%rbx,%rax,4), %eax
	movzbl	-2(%rdx), %ebx
	leal	(%rbx,%rax,4), %eax
	movzbl	-3(%rdx), %ebx
	leal	(%rbx,%rax,4), %eax
	addq	$4, %rcx
	addq	$-4, %rdx
	cmpq	%rdi, %rcx
	jb	.LBB6_27
.LBB6_28:                               # %._crit_edge233.us
                                        #   in Loop: Header=BB6_13 Depth=1
	cltq
	cmpb	$0, MEMBER(%rax)
	movl	%r14d, %eax
	je	.LBB6_13
	jmp	.LBB6_10
.LBB6_29:                               # %.us-lcssa.us
	cmpl	$0, 312(%rsp)
	jns	.LBB6_31
# BB#30:
	movl	$0, 312(%rsp)
.LBB6_31:
	testl	%r10d, %r10d
	movq	16(%rsp), %r11          # 8-byte Reload
	js	.LBB6_79
# BB#32:                                # %.lr.ph228
	subl	24(%rsp), %r12d         # 4-byte Folded Reload
	movl	endposition(%rip), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movslq	%r11d, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	leaq	4(,%rax,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	%r10d, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	leaq	-1(%rax), %rbp
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-2(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	andl	$1, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	andl	$3, %ebp
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	%rbp, 96(%rsp)          # 8-byte Spill
.LBB6_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_38 Depth 2
                                        #     Child Loop BB6_42 Depth 2
                                        #     Child Loop BB6_44 Depth 2
                                        #       Child Loop BB6_52 Depth 3
                                        #       Child Loop BB6_69 Depth 3
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	304(%rsp,%rax,8), %r13d
	movl	308(%rsp,%rax,8), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	cmpl	%r12d, %eax
	jle	.LBB6_35
# BB#34:                                #   in Loop: Header=BB6_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%r12d, 308(%rsp,%rax,8)
	movl	%r12d, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
.LBB6_35:                               #   in Loop: Header=BB6_33 Depth=1
	testl	%r13d, %r13d
	movl	$0, %eax
	cmovsl	%eax, %r13d
	testl	%r11d, %r11d
	movl	$-1, 112(%rsp)
	movl	$-1, 208(%rsp)
	movl	$2147483647, 116(%rsp)  # imm = 0x7FFFFFFF
	movl	$2147483647, 212(%rsp)  # imm = 0x7FFFFFFF
	jle	.LBB6_43
# BB#36:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_33 Depth=1
	movl	$-1, %eax
	testq	%rbp, %rbp
	je	.LBB6_40
# BB#37:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB6_33 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_38:                               # %.lr.ph.prol
                                        #   Parent Loop BB6_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	shrl	%eax
	andl	%ecx, %eax
	movl	%eax, 116(%rsp,%rsi,4)
	movl	%eax, 212(%rsp,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %rbp
	jne	.LBB6_38
# BB#39:                                # %.lr.ph.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB6_33 Depth=1
	incq	%rsi
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jae	.LBB6_41
	jmp	.LBB6_43
	.p2align	4, 0x90
.LBB6_40:                               #   in Loop: Header=BB6_33 Depth=1
	movl	$1, %esi
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_43
.LBB6_41:                               # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB6_33 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	leaq	220(%rsp), %rdx
	leaq	(%rdx,%rsi,4), %rdx
	leaq	124(%rsp), %rdi
	leaq	(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB6_42:                               # %.lr.ph
                                        #   Parent Loop BB6_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edi
	shrl	%edi
	andl	%eax, %edi
	movl	%edi, -12(%rsi)
	movl	%edi, -12(%rdx)
	movl	%edi, %eax
	shrl	%eax
	andl	%edi, %eax
	movl	%eax, -8(%rsi)
	movl	%eax, -8(%rdx)
	movl	%eax, %edi
	shrl	%edi
	andl	%eax, %edi
	movl	%edi, -4(%rsi)
	movl	%edi, -4(%rdx)
	movl	%edi, %eax
	shrl	%eax
	andl	%edi, %eax
	movl	%eax, (%rsi)
	movl	%eax, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-4, %rcx
	jne	.LBB6_42
.LBB6_43:                               # %.preheader198
                                        #   in Loop: Header=BB6_33 Depth=1
	cmpl	36(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB6_78
	.p2align	4, 0x90
.LBB6_44:                               # %.lr.ph223
                                        #   Parent Loop BB6_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_52 Depth 3
                                        #       Child Loop BB6_69 Depth 3
	movslq	%r13d, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%rax), %ebx
	cmpq	$10, %rbx
	jne	.LBB6_47
# BB#45:                                # %.lr.ph223
                                        #   in Loop: Header=BB6_44 Depth=2
	testl	%r11d, %r11d
	js	.LBB6_47
# BB#46:                                # %.lr.ph206.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	$255, %esi
	leaq	112(%rsp), %rdi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	memset
	movl	$255, %esi
	leaq	208(%rsp), %rdi
	movq	%rbp, %rdx
	callq	memset
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB6_47:                               # %.loopexit197
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	Mask(,%rbx,4), %r8d
	movl	112(%rsp), %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	orl	%r8d, %ecx
	testl	%r11d, %r11d
	movl	%ecx, 208(%rsp)
	jle	.LBB6_53
# BB#48:                                # %.lr.ph208.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_44 Depth=2
	movl	$1, %ebp
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB6_51
	jmp	.LBB6_53
	.p2align	4, 0x90
.LBB6_50:                               # %.lr.ph208.prol
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	116(%rsp), %esi
	movl	%esi, %edi
	shrl	%edi
	orl	%r8d, %edi
	andl	%r9d, %edi
	andl	%r9d, %ecx
	shrl	%ecx
	andl	%edi, %ecx
	movl	%ecx, 212(%rsp)
	movl	%esi, %r9d
	movl	$2, %ebp
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB6_53
.LBB6_51:                               # %.lr.ph208.preheader.new
                                        #   in Loop: Header=BB6_44 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	subq	%rbp, %rsi
	leaq	124(%rsp), %rax
	leaq	-8(%rax,%rbp,4), %rdi
	leaq	220(%rsp), %rax
	leaq	-8(%rax,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB6_52:                               # %.lr.ph208
                                        #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi), %eax
	movl	%eax, %ebx
	shrl	%ebx
	orl	%r8d, %ebx
	andl	%r9d, %ecx
	shrl	%ecx
	andl	%r9d, %ecx
	andl	%ebx, %ecx
	movl	%ecx, -4(%rbp)
	movl	(%rdi), %r9d
	movl	%r9d, %edx
	shrl	%edx
	orl	%r8d, %edx
	andl	%eax, %edx
	andl	%eax, %ecx
	shrl	%ecx
	andl	%edx, %ecx
	movl	%ecx, (%rbp)
	addq	$8, %rdi
	addq	$8, %rbp
	addq	$-2, %rsi
	jne	.LBB6_52
.LBB6_53:                               # %._crit_edge
                                        #   in Loop: Header=BB6_44 Depth=2
	leal	1(%r13), %eax
	movl	48(%rsp), %ecx          # 4-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	testl	208(%rsp,%rdx,4), %ecx
	je	.LBB6_55
# BB#54:                                #   in Loop: Header=BB6_44 Depth=2
	movl	%r15d, %r14d
	movl	%eax, %r15d
	jmp	.LBB6_60
	.p2align	4, 0x90
.LBB6_55:                               #   in Loop: Header=BB6_44 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB6_79
# BB#56:                                #   in Loop: Header=BB6_44 Depth=2
	movl	%eax, 12(%rsp)
	cmpl	%r15d, %r13d
	jl	.LBB6_58
# BB#57:                                #   in Loop: Header=BB6_44 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	12(%rsp), %rsi
	callq	s_output
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	12(%rsp), %r15d
.LBB6_58:                               #   in Loop: Header=BB6_44 Depth=2
	testl	%r11d, %r11d
	js	.LBB6_63
# BB#59:                                # %.lr.ph211.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	$255, %esi
	leaq	112(%rsp), %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memset
	movl	$255, %esi
	leaq	208(%rsp), %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	%r15d, %r14d
.LBB6_60:                               # %.loopexit195
                                        #   in Loop: Header=BB6_44 Depth=2
	leal	1(%r15), %r13d
	movslq	%r15d, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movb	(%rcx,%rax), %bl
	cmpb	$10, %bl
	jne	.LBB6_64
# BB#61:                                # %.loopexit195
                                        #   in Loop: Header=BB6_44 Depth=2
	testl	%r11d, %r11d
	js	.LBB6_64
# BB#62:                                # %.lr.ph213.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	$255, %esi
	leaq	112(%rsp), %rdi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	memset
	movl	$255, %esi
	leaq	208(%rsp), %rdi
	movq	%rbp, %rdx
	callq	memset
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB6_64
.LBB6_63:                               # %.loopexit195.thread
                                        #   in Loop: Header=BB6_44 Depth=2
	leal	1(%r15), %r13d
	movslq	%r15d, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movb	(%rcx,%rax), %bl
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB6_64:                               # %.loopexit194
                                        #   in Loop: Header=BB6_44 Depth=2
	movzbl	%bl, %eax
	movl	Mask(,%rax,4), %r8d
	movl	208(%rsp), %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	orl	%r8d, %ecx
	testl	%r11d, %r11d
	movl	%ecx, 112(%rsp)
	jle	.LBB6_70
# BB#65:                                # %.lr.ph216.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB6_67
# BB#66:                                #   in Loop: Header=BB6_44 Depth=2
	movl	$1, %ebx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB6_68
	jmp	.LBB6_70
	.p2align	4, 0x90
.LBB6_67:                               # %.lr.ph216.prol
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	212(%rsp), %esi
	movl	%esi, %edi
	shrl	%edi
	orl	%r8d, %edi
	andl	%r9d, %edi
	andl	%r9d, %ecx
	shrl	%ecx
	andl	%edi, %ecx
	movl	%ecx, 116(%rsp)
	movl	%esi, %r9d
	movl	$2, %ebx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB6_70
.LBB6_68:                               # %.lr.ph216.preheader.new
                                        #   in Loop: Header=BB6_44 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	subq	%rbx, %rsi
	leaq	220(%rsp), %rax
	leaq	-8(%rax,%rbx,4), %rdi
	leaq	124(%rsp), %rax
	leaq	-8(%rax,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB6_69:                               # %.lr.ph216
                                        #   Parent Loop BB6_33 Depth=1
                                        #     Parent Loop BB6_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi), %eax
	movl	%eax, %ebp
	shrl	%ebp
	orl	%r8d, %ebp
	andl	%r9d, %ecx
	shrl	%ecx
	andl	%r9d, %ecx
	andl	%ebp, %ecx
	movl	%ecx, -4(%rbx)
	movl	(%rdi), %r9d
	movl	%r9d, %edx
	shrl	%edx
	orl	%r8d, %edx
	andl	%eax, %edx
	andl	%eax, %ecx
	shrl	%ecx
	andl	%edx, %ecx
	movl	%ecx, (%rbx)
	addq	$8, %rdi
	addq	$8, %rbx
	addq	$-2, %rsi
	jne	.LBB6_69
.LBB6_70:                               # %._crit_edge217
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	48(%rsp), %eax          # 4-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	testl	112(%rsp,%rcx,4), %eax
	jne	.LBB6_77
# BB#71:                                #   in Loop: Header=BB6_44 Depth=2
	movl	%r13d, 12(%rsp)
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB6_79
# BB#72:                                #   in Loop: Header=BB6_44 Depth=2
	cmpl	%r14d, %r15d
	jl	.LBB6_74
# BB#73:                                #   in Loop: Header=BB6_44 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	12(%rsp), %rsi
	callq	s_output
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	12(%rsp), %r14d
.LBB6_74:                               #   in Loop: Header=BB6_44 Depth=2
	testl	%r11d, %r11d
	js	.LBB6_76
# BB#75:                                # %.lr.ph220.preheader
                                        #   in Loop: Header=BB6_44 Depth=2
	movl	$255, %esi
	leaq	112(%rsp), %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memset
	movl	$255, %esi
	leaq	208(%rsp), %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB6_76:                               #   in Loop: Header=BB6_44 Depth=2
	movl	%r14d, %r13d
.LBB6_77:                               # %.backedge
                                        #   in Loop: Header=BB6_44 Depth=2
	cmpl	36(%rsp), %r13d         # 4-byte Folded Reload
	movl	%r14d, %r15d
	jl	.LBB6_44
.LBB6_78:                               # %._crit_edge224
                                        #   in Loop: Header=BB6_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	104(%rsp), %rax         # 8-byte Folded Reload
	leaq	1(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rbp          # 8-byte Reload
	jl	.LBB6_33
.LBB6_79:                               # %.loopexit199
	addq	$16696, %rsp            # imm = 0x4138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	agrep, .Lfunc_end6-agrep
	.cfi_endproc

	.globl	prep_bm
	.p2align	4, 0x90
	.type	prep_bm,@function
prep_bm:                                # @prep_bm
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, SHIFT+240(%rip)
	movdqa	%xmm0, SHIFT+224(%rip)
	movdqa	%xmm0, SHIFT+208(%rip)
	movdqa	%xmm0, SHIFT+192(%rip)
	movdqa	%xmm0, SHIFT+176(%rip)
	movdqa	%xmm0, SHIFT+160(%rip)
	movdqa	%xmm0, SHIFT+144(%rip)
	movdqa	%xmm0, SHIFT+128(%rip)
	movdqa	%xmm0, SHIFT+112(%rip)
	movdqa	%xmm0, SHIFT+96(%rip)
	movdqa	%xmm0, SHIFT+80(%rip)
	movdqa	%xmm0, SHIFT+64(%rip)
	movdqa	%xmm0, SHIFT+48(%rip)
	movdqa	%xmm0, SHIFT+32(%rip)
	movdqa	%xmm0, SHIFT+16(%rip)
	movdqa	%xmm0, SHIFT(%rip)
	leal	-1(%rsi), %r9d
	testl	%esi, %esi
	jle	.LBB7_1
# BB#6:                                 # %.lr.ph34.preheader
	movslq	%esi, %r10
	testb	$1, %r10b
	je	.LBB7_9
# BB#7:                                 # %.lr.ph34.prol
	movzbl	-1(%rdi,%r10), %eax
	decq	%r10
	movzbl	TR(%rax), %eax
	movzbl	SHIFT(%rax), %ecx
	cmpl	%r9d, %ecx
	jl	.LBB7_9
# BB#8:
	movl	%r9d, %ecx
	subl	%r10d, %ecx
	movb	%cl, SHIFT(%rax)
.LBB7_9:                                # %.lr.ph34.prol.loopexit
	movslq	%r9d, %r8
	cmpl	$1, %esi
	je	.LBB7_2
# BB#10:                                # %.lr.ph34.preheader.new
	leal	2(%r9), %r11d
	leaq	2(%r10), %rcx
	subl	%r10d, %r11d
	leal	1(%r9), %eax
	subl	%r10d, %eax
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rdi,%rcx), %edx
	movzbl	TR(%rdx), %r10d
	movzbl	SHIFT(%r10), %edx
	cmpl	%r9d, %edx
	jl	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_11 Depth=1
	movb	%al, SHIFT(%r10)
.LBB7_13:                               # %.lr.ph34.142
                                        #   in Loop: Header=BB7_11 Depth=1
	movzbl	-4(%rdi,%rcx), %edx
	movzbl	TR(%rdx), %r10d
	movzbl	SHIFT(%r10), %edx
	cmpl	%r9d, %edx
	jl	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_11 Depth=1
	movb	%r11b, SHIFT(%r10)
.LBB7_15:                               #   in Loop: Header=BB7_11 Depth=1
	addq	$-2, %rcx
	addb	$2, %r11b
	addb	$2, %al
	cmpq	$2, %rcx
	jg	.LBB7_11
	jmp	.LBB7_2
.LBB7_1:                                # %.._crit_edge35_crit_edge
	movslq	%r9d, %r8
.LBB7_2:                                # %._crit_edge35
	movl	%r9d, shift_1(%rip)
	movslq	%esi, %rax
	addq	$-2, %rax
	testl	%eax, %eax
	js	.LBB7_17
# BB#3:                                 # %.lr.ph.preheader
	movzbl	(%rdi,%r8), %ecx
	movb	TR(%rcx), %cl
	movl	%r9d, %esi
	addq	%rdi, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rdx), %edi
	cmpb	%cl, TR(%rdi)
	je	.LBB7_5
# BB#16:                                #   in Loop: Header=BB7_4 Depth=1
	decq	%rdx
	leal	(%rsi,%rdx), %edi
	testl	%edi, %edi
	jg	.LBB7_4
	jmp	.LBB7_17
.LBB7_5:                                # %.thread
	movl	$1, %r9d
	subl	%edx, %r9d
	movl	%r9d, shift_1(%rip)
.LBB7_17:                               # %._crit_edge
	testl	%r9d, %r9d
	jne	.LBB7_19
# BB#18:
	movl	$1, shift_1(%rip)
.LBB7_19:
	cmpl	$0, NOUPPER(%rip)
	je	.LBB7_21
# BB#20:                                # %.preheader.preheader
	movdqu	SHIFT+97(%rip), %xmm0
	movdqu	%xmm0, SHIFT+65(%rip)
	movb	SHIFT+113(%rip), %al
	movb	%al, SHIFT+81(%rip)
	movb	SHIFT+114(%rip), %al
	movb	%al, SHIFT+82(%rip)
	movb	SHIFT+115(%rip), %al
	movb	%al, SHIFT+83(%rip)
	movb	SHIFT+116(%rip), %al
	movb	%al, SHIFT+84(%rip)
	movb	SHIFT+117(%rip), %al
	movb	%al, SHIFT+85(%rip)
	movb	SHIFT+118(%rip), %al
	movb	%al, SHIFT+86(%rip)
	movb	SHIFT+119(%rip), %al
	movb	%al, SHIFT+87(%rip)
	movb	SHIFT+120(%rip), %al
	movb	%al, SHIFT+88(%rip)
	movb	SHIFT+121(%rip), %al
	movb	%al, SHIFT+89(%rip)
	movb	SHIFT+122(%rip), %al
	movb	%al, SHIFT+90(%rip)
.LBB7_21:                               # %.loopexit
	retq
.Lfunc_end7:
	.size	prep_bm, .Lfunc_end7-prep_bm
	.cfi_endproc

	.globl	a_monkey
	.p2align	4, 0x90
	.type	a_monkey,@function
a_monkey:                               # @a_monkey
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 96
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %r15
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	cmpq	%r14, %r15
	jae	.LBB8_8
# BB#1:                                 # %.lr.ph59
	movl	Hashmask(%rip), %r13d
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	subl	%ebx, %eax
	movslq	%eax, %r12
	leal	(%rbx,%rcx,2), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
                                        #       Child Loop BB8_4 Depth 3
                                        #     Child Loop BB8_14 Depth 2
                                        #     Child Loop BB8_18 Depth 2
                                        #     Child Loop BB8_20 Depth 2
	leaq	(%r15,%r12), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_3:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_4 Depth 3
	movzbl	(%rbp), %ecx
	decq	%rbp
	cmpb	$0, MEMBER_1(%rcx)
	je	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shll	$8, %ecx
	movzbl	(%rbp), %edx
	orl	%ecx, %edx
	andl	%r13d, %edx
	decq	%rbp
	cmpb	$0, MEMBER_1(%rdx)
	movl	%edx, %ecx
	jne	.LBB8_4
.LBB8_5:                                # %._crit_edge
                                        #   in Loop: Header=BB8_3 Depth=2
	incl	%eax
	cmpl	%ebx, %eax
	jbe	.LBB8_3
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	%r15, %rbp
	ja	.LBB8_7
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%ebx, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %r8
	callq	verify
	testl	%eax, %eax
	jle	.LBB8_23
# BB#10:                                #   in Loop: Header=BB8_2 Depth=1
	movslq	%eax, %rbp
	leaq	(%r15,%rbp), %rax
	cmpq	%r14, %rax
	ja	.LBB8_8
# BB#11:                                #   in Loop: Header=BB8_2 Depth=1
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB8_8
# BB#12:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB8_15
# BB#13:                                # %.preheader47.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	leaq	-1(%r15,%rbp), %rbp
	.p2align	4, 0x90
.LBB8_14:                               # %.preheader47
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, 1(%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB8_14
	jmp	.LBB8_7
.LBB8_23:                               #   in Loop: Header=BB8_2 Depth=1
	addq	24(%rsp), %r15          # 8-byte Folded Reload
	movq	%r15, %rbp
	jmp	.LBB8_7
.LBB8_15:                               #   in Loop: Header=BB8_2 Depth=1
	cmpl	$0, FNAME(%rip)
	je	.LBB8_17
# BB#16:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB8_17:                               # %.preheader46.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	leaq	1(%r15,%rbp), %rbp
	.p2align	4, 0x90
.LBB8_18:                               # %.preheader46
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, -2(%rbp)
	leaq	-1(%rbp), %rbp
	jne	.LBB8_18
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movb	(%rbp), %al
	cmpb	$10, %al
	je	.LBB8_22
	.p2align	4, 0x90
.LBB8_20:                               # %.lr.ph54
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	1(%rbp), %eax
	incq	%rbp
	cmpb	$10, %al
	jne	.LBB8_20
.LBB8_22:                               # %._crit_edge55
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB8_7:                                # %.backedge
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	%r14, %rbp
	movq	%rbp, %r15
	jb	.LBB8_2
.LBB8_8:                                # %._crit_edge60
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	a_monkey, .Lfunc_end8-a_monkey
	.cfi_endproc

	.globl	monkey
	.p2align	4, 0x90
	.type	monkey,@function
monkey:                                 # @monkey
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 48
.Lcfi95:
	.cfi_offset %rbx, -48
.Lcfi96:
	.cfi_offset %r12, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%esi, %r12d
	movq	%rdi, %r14
	movslq	%r12d, %rax
	leaq	-1(%rdx,%rax), %rbx
	cmpq	%r15, %rbx
	jae	.LBB9_22
# BB#1:                                 # %.preheader54.preheader
	decq	%rax
	addq	%rax, %r14
	.p2align	4, 0x90
.LBB9_2:                                # %.preheader54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_23 Depth 2
                                        #     Child Loop BB9_5 Depth 2
                                        #     Child Loop BB9_12 Depth 2
                                        #     Child Loop BB9_16 Depth 2
                                        #     Child Loop BB9_19 Depth 2
	movzbl	(%rbx), %eax
	movzbl	-1(%rbx), %ecx
	movb	SHIFT_2(%rcx,%rax,8), %al
	testb	%al, %al
	je	.LBB9_4
	.p2align	4, 0x90
.LBB9_23:                               # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %eax
	movzbl	(%rbx,%rax), %ecx
	movzbl	-1(%rbx,%rax), %edx
	leaq	(%rbx,%rax), %rbx
	movzbl	SHIFT_2(%rdx,%rcx,8), %eax
	testb	%al, %al
	jne	.LBB9_23
.LBB9_4:                                # %.preheader53
                                        #   in Loop: Header=BB9_2 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rcx
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB9_5:                                #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %esi
	movzbl	TR(%rsi), %esi
	movzbl	(%rdx), %edi
	cmpb	TR(%rdi), %sil
	jne	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=2
	incl	%eax
	decq	%rdx
	decq	%rcx
	cmpl	%eax, %r12d
	jne	.LBB9_5
.LBB9_7:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	%r12d, %eax
	jne	.LBB9_21
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpq	%r15, %rbx
	jae	.LBB9_22
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=1
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB9_22
# BB#10:                                #   in Loop: Header=BB9_2 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB9_13
# BB#11:                                # %.preheader52.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	addq	$-2, %rbx
	.p2align	4, 0x90
.LBB9_12:                               # %.preheader52
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, 2(%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB9_12
	jmp	.LBB9_21
.LBB9_13:                               #   in Loop: Header=BB9_2 Depth=1
	cmpl	$0, FNAME(%rip)
	je	.LBB9_15
# BB#14:                                #   in Loop: Header=BB9_2 Depth=1
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB9_15:                               # %.preheader51.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	leaq	1(%rbx), %rax
	decq	%rbx
	.p2align	4, 0x90
.LBB9_16:                               # %.preheader51
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rbp
	leaq	-1(%rbp), %rbx
	cmpb	$10, -2(%rax)
	leaq	-1(%rax), %rax
	jne	.LBB9_16
# BB#17:                                # %.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movb	(%rax), %cl
	cmpb	$10, %cl
	jne	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_2 Depth=1
	decq	%rax
	movq	%rax, %rbp
	jmp	.LBB9_20
	.p2align	4, 0x90
.LBB9_19:                               # %.lr.ph59
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%cl, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	2(%rbp), %ecx
	incq	%rbp
	cmpb	$10, %cl
	jne	.LBB9_19
.LBB9_20:                               # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB9_21:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jb	.LBB9_2
.LBB9_22:                               # %._crit_edge62
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	monkey, .Lfunc_end9-monkey
	.cfi_endproc

	.globl	am_preprocess
	.p2align	4, 0x90
	.type	am_preprocess,@function
am_preprocess:                          # @am_preprocess
	.cfi_startproc
# BB#0:                                 # %.preheader20
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -24
.Lcfi104:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	strlen
	movq	%rax, %r14
	movl	$65535, Hashmask(%rip)  # imm = 0xFFFF
	movl	$MEMBER_1, %edi
	xorl	%esi, %esi
	movl	$65536, %edx            # imm = 0x10000
	callq	memset
	testl	%r14d, %r14d
	jle	.LBB10_11
# BB#1:                                 # %.lr.ph24.preheader
	movslq	%r14d, %rax
	leaq	-1(%rax), %rcx
	movq	%rax, %rdx
	andq	$3, %rdx
	je	.LBB10_4
# BB#2:                                 # %.lr.ph24.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph24.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbx,%rax), %esi
	decq	%rax
	movb	$1, MEMBER_1(%rsi)
	incq	%rdx
	jne	.LBB10_3
.LBB10_4:                               # %.lr.ph24.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB10_5
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph24
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbx,%rax), %ecx
	movb	$1, MEMBER_1(%rcx)
	movzbl	-2(%rbx,%rax), %ecx
	movb	$1, MEMBER_1(%rcx)
	movzbl	-3(%rbx,%rax), %ecx
	movb	$1, MEMBER_1(%rcx)
	movzbl	-4(%rbx,%rax), %ecx
	addq	$-4, %rax
	movb	$1, MEMBER_1(%rcx)
	testq	%rax, %rax
	jg	.LBB10_12
.LBB10_5:                               # %.preheader
	cmpl	$2, %r14d
	jl	.LBB10_11
# BB#6:                                 # %.lr.ph.preheader
	movslq	%r14d, %rcx
	leaq	-1(%rcx), %rax
	testb	$1, %al
	je	.LBB10_8
# BB#7:                                 # %.lr.ph.prol
	movzbl	-1(%rbx,%rcx), %edx
	shlq	$8, %rdx
	leaq	-2(%rcx), %rax
	movzbl	-2(%rbx,%rcx), %esi
	movb	$1, MEMBER_1(%rdx,%rsi)
.LBB10_8:                               # %.lr.ph.prol.loopexit
	cmpq	$2, %rcx
	je	.LBB10_11
# BB#9:                                 # %.lr.ph.preheader.new
	incq	%rax
	.p2align	4, 0x90
.LBB10_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbx,%rax), %ecx
	shlq	$8, %rcx
	movzbl	-2(%rbx,%rax), %edx
	movb	$1, MEMBER_1(%rcx,%rdx)
	movzbl	-2(%rbx,%rax), %ecx
	shlq	$8, %rcx
	movzbl	-3(%rbx,%rax), %edx
	movb	$1, MEMBER_1(%rcx,%rdx)
	addq	$-2, %rax
	cmpq	$1, %rax
	jg	.LBB10_10
.LBB10_11:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	am_preprocess, .Lfunc_end10-am_preprocess
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.quad	14                      # 0xe
	.quad	15                      # 0xf
.LCPI11_1:
	.quad	12                      # 0xc
	.quad	13                      # 0xd
.LCPI11_2:
	.quad	10                      # 0xa
	.quad	11                      # 0xb
.LCPI11_3:
	.quad	8                       # 0x8
	.quad	9                       # 0x9
.LCPI11_4:
	.quad	6                       # 0x6
	.quad	7                       # 0x7
.LCPI11_5:
	.quad	4                       # 0x4
	.quad	5                       # 0x5
.LCPI11_6:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI11_7:
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
.LCPI11_8:
	.zero	16,128
.LCPI11_9:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
	.text
	.globl	m_preprocess
	.p2align	4, 0x90
	.type	m_preprocess,@function
m_preprocess:                           # @m_preprocess
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	subq	$312, %rsp              # imm = 0x138
.Lcfi107:
	.cfi_def_cfa_offset 336
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	strlen
	movq	%rax, %r14
	movl	$SHIFT_2, %edi
	movl	$4096, %edx             # imm = 0x1000
	movl	%r14d, %esi
	callq	memset
	movslq	%r14d, %r9
	leaq	-1(%r9), %rax
	cmpl	$2, %r9d
	jl	.LBB11_38
# BB#1:                                 # %.lr.ph46
	movslq	%r14d, %rdx
	decq	%rdx
	movl	%eax, %r8d
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	movl	$1, %ecx
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # %min.iters.checked
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_3 Depth 2
	movzbl	(%rbx,%rdx), %edi
	shlq	$3, %rdi
	movd	%rdi, %xmm1
	pshufd	$68, %xmm1, %xmm7       # xmm7 = xmm1[0,1,0,1]
	movl	$256, %esi              # imm = 0x100
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movdqa	.LCPI11_6(%rip), %xmm15 # xmm15 = [2,3]
	movdqa	.LCPI11_5(%rip), %xmm8  # xmm8 = [4,5]
	movdqa	.LCPI11_4(%rip), %xmm9  # xmm9 = [6,7]
	movdqa	.LCPI11_3(%rip), %xmm10 # xmm10 = [8,9]
	movdqa	.LCPI11_2(%rip), %xmm11 # xmm11 = [10,11]
	movdqa	.LCPI11_1(%rip), %xmm12 # xmm12 = [12,13]
	movdqa	.LCPI11_0(%rip), %xmm13 # xmm13 = [14,15]
	.p2align	4, 0x90
.LBB11_3:                               # %vector.body
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm2, 32(%rsp)         # 16-byte Spill
	paddq	%xmm7, %xmm2
	movd	%xmm2, %rcx
	movdqu	SHIFT_2(%rcx), %xmm4
	pshufd	$78, %xmm4, %xmm5       # xmm5 = xmm4[2,3,0,1]
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm3
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	movdqa	%xmm4, %xmm0
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	punpckhwd	%xmm1, %xmm4    # xmm4 = xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	movdqa	16(%rsp), %xmm1         # 16-byte Reload
	pcmpeqd	%xmm1, %xmm4
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm6      # xmm6 = xmm4[0,2,2,3]
	pcmpeqd	%xmm1, %xmm0
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm4      # xmm4 = xmm0[0,2,2,3]
	punpcklqdq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0]
	psllw	$15, %xmm4
	psraw	$15, %xmm4
	movdqa	.LCPI11_7(%rip), %xmm0  # xmm0 = [255,255,255,255,255,255,255,255]
	movdqa	%xmm0, %xmm6
	pand	%xmm6, %xmm4
	pcmpeqd	%xmm1, %xmm5
	pshuflw	$232, %xmm5, %xmm0      # xmm0 = xmm5[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pcmpeqd	%xmm1, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0]
	psllw	$15, %xmm3
	psraw	$15, %xmm3
	pand	%xmm6, %xmm3
	packuswb	%xmm3, %xmm4
	psllw	$7, %xmm4
	pand	.LCPI11_8(%rip), %xmm4
	pxor	%xmm14, %xmm14
	pcmpgtb	%xmm4, %xmm14
	movdqa	%xmm14, 288(%rsp)
	testb	$1, 288(%rsp)
	je	.LBB11_5
# BB#4:                                 # %pred.store.if
                                        #   in Loop: Header=BB11_3 Depth=2
	movb	%al, SHIFT_2(%rcx)
.LBB11_5:                               # %pred.store.continue
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 272(%rsp)
	testb	$1, 273(%rsp)
	je	.LBB11_7
# BB#6:                                 # %pred.store.if58
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_7:                               # %pred.store.continue59
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm15, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 256(%rsp)
	testb	$1, 258(%rsp)
	je	.LBB11_9
# BB#8:                                 # %pred.store.if60
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_9:                               # %pred.store.continue61
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 240(%rsp)
	testb	$1, 243(%rsp)
	je	.LBB11_11
# BB#10:                                # %pred.store.if62
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_11:                              # %pred.store.continue63
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm8, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 224(%rsp)
	testb	$1, 228(%rsp)
	je	.LBB11_13
# BB#12:                                # %pred.store.if64
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_13:                              # %pred.store.continue65
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 208(%rsp)
	testb	$1, 213(%rsp)
	je	.LBB11_15
# BB#14:                                # %pred.store.if66
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_15:                              # %pred.store.continue67
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm9, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 192(%rsp)
	testb	$1, 198(%rsp)
	je	.LBB11_17
# BB#16:                                # %pred.store.if68
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_17:                              # %pred.store.continue69
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 176(%rsp)
	testb	$1, 183(%rsp)
	je	.LBB11_19
# BB#18:                                # %pred.store.if70
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_19:                              # %pred.store.continue71
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm10, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 160(%rsp)
	testb	$1, 168(%rsp)
	je	.LBB11_21
# BB#20:                                # %pred.store.if72
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_21:                              # %pred.store.continue73
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 144(%rsp)
	testb	$1, 153(%rsp)
	je	.LBB11_23
# BB#22:                                # %pred.store.if74
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_23:                              # %pred.store.continue75
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm11, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 128(%rsp)
	testb	$1, 138(%rsp)
	je	.LBB11_25
# BB#24:                                # %pred.store.if76
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_25:                              # %pred.store.continue77
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 112(%rsp)
	testb	$1, 123(%rsp)
	je	.LBB11_27
# BB#26:                                # %pred.store.if78
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_27:                              # %pred.store.continue79
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm12, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 96(%rsp)
	testb	$1, 108(%rsp)
	je	.LBB11_29
# BB#28:                                # %pred.store.if80
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_29:                              # %pred.store.continue81
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 80(%rsp)
	testb	$1, 93(%rsp)
	je	.LBB11_31
# BB#30:                                # %pred.store.if82
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_31:                              # %pred.store.continue83
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm13, %xmm2
	paddq	%xmm7, %xmm2
	movdqa	%xmm14, 64(%rsp)
	testb	$1, 78(%rsp)
	je	.LBB11_33
# BB#32:                                # %pred.store.if84
                                        #   in Loop: Header=BB11_3 Depth=2
	movd	%xmm2, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_33:                              # %pred.store.continue85
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	%xmm14, 48(%rsp)
	testb	$1, 63(%rsp)
	je	.LBB11_35
# BB#34:                                # %pred.store.if86
                                        #   in Loop: Header=BB11_3 Depth=2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movd	%xmm0, %rcx
	movb	%al, SHIFT_2(%rcx)
.LBB11_35:                              # %pred.store.continue87
                                        #   in Loop: Header=BB11_3 Depth=2
	movdqa	.LCPI11_9(%rip), %xmm0  # xmm0 = [16,16]
	movdqa	32(%rsp), %xmm2         # 16-byte Reload
	paddq	%xmm0, %xmm2
	paddq	%xmm0, %xmm15
	paddq	%xmm0, %xmm8
	paddq	%xmm0, %xmm9
	paddq	%xmm0, %xmm10
	paddq	%xmm0, %xmm11
	paddq	%xmm0, %xmm12
	paddq	%xmm0, %xmm13
	addq	$-16, %rsi
	jne	.LBB11_3
# BB#36:                                # %middle.block
                                        #   in Loop: Header=BB11_2 Depth=1
	movzbl	-1(%rbx,%rdx), %ecx
	movzbl	SHIFT_2(%rcx,%rdi), %esi
	cmpl	%eax, %esi
	jl	.LBB11_37
# BB#46:                                #   in Loop: Header=BB11_2 Depth=1
	movl	%r8d, %esi
	subl	%edx, %esi
	movb	%sil, SHIFT_2(%rdi,%rcx)
.LBB11_37:                              # %.backedge
                                        #   in Loop: Header=BB11_2 Depth=1
	cmpq	$1, %rdx
	leaq	-1(%rdx), %rdx
	jg	.LBB11_2
.LBB11_38:                              # %._crit_edge47
	movl	%eax, shift_1(%rip)
	addq	$-2, %r9
	testl	%r9d, %r9d
	js	.LBB11_43
# BB#39:                                # %.lr.ph
	movb	(%rbx,%rax), %cl
	movl	%eax, %esi
	addq	%rbx, %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_40:                              # =>This Inner Loop Header: Depth=1
	cmpb	%cl, (%r9,%rdx)
	je	.LBB11_41
# BB#42:                                #   in Loop: Header=BB11_40 Depth=1
	decq	%rdx
	leal	(%rsi,%rdx), %edi
	testl	%edi, %edi
	jg	.LBB11_40
	jmp	.LBB11_43
.LBB11_41:                              # %.thread
	movl	$1, %eax
	subl	%edx, %eax
	movl	%eax, shift_1(%rip)
.LBB11_43:                              # %._crit_edge
	testl	%eax, %eax
	jne	.LBB11_45
# BB#44:
	movl	$1, shift_1(%rip)
.LBB11_45:
	movb	$0, SHIFT_2(%rip)
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	m_preprocess, .Lfunc_end11-m_preprocess
	.cfi_endproc

	.globl	monkey4
	.p2align	4, 0x90
	.type	monkey4,@function
monkey4:                                # @monkey4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi116:
	.cfi_def_cfa_offset 96
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r14
	movq	%rdx, %rbp
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	cmpq	%r14, %rbp
	jae	.LBB12_8
# BB#1:                                 # %.lr.ph62
	movl	Hashmask(%rip), %r13d
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	-1(%rcx), %eax
	subl	%ebx, %eax
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	(%rbx,%rcx,2), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
                                        #       Child Loop BB12_4 Depth 3
                                        #     Child Loop BB12_21 Depth 2
                                        #     Child Loop BB12_16 Depth 2
                                        #     Child Loop BB12_19 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rbp,%rax), %r15
	movq	MEMBER_D(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_3:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_4 Depth 3
	movzbl	(%r15), %edx
	decq	%r15
	movzbl	char_map(%rdx), %edx
	.p2align	4, 0x90
.LBB12_4:                               #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15), %esi
	movzbl	char_map(%rsi), %esi
	leal	(%rsi,%rdx,8), %edx
	andl	%r13d, %edx
	decq	%r15
	cmpb	$0, (%rax,%rdx)
	jne	.LBB12_4
# BB#5:                                 #   in Loop: Header=BB12_3 Depth=2
	incl	%ecx
	cmpl	%ebx, %ecx
	jbe	.LBB12_3
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpq	%rbp, %r15
	ja	.LBB12_7
# BB#9:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%ebx, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	callq	verify
	testl	%eax, %eax
	jle	.LBB12_22
# BB#10:                                #   in Loop: Header=BB12_2 Depth=1
	movslq	%eax, %r12
	leaq	(%rbp,%r12), %r15
	cmpq	%r14, %r15
	ja	.LBB12_8
# BB#11:                                #   in Loop: Header=BB12_2 Depth=1
	incl	num_of_matched(%rip)
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB12_8
# BB#12:                                #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB12_13
	.p2align	4, 0x90
.LBB12_21:                              # %.preheader56
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%r15)
	leaq	1(%r15), %r15
	jne	.LBB12_21
	jmp	.LBB12_7
.LBB12_22:                              #   in Loop: Header=BB12_2 Depth=1
	addq	16(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rbp, %r15
	jmp	.LBB12_7
.LBB12_13:                              #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, FNAME(%rip)
	je	.LBB12_15
# BB#14:                                #   in Loop: Header=BB12_2 Depth=1
	movl	$.L.str.2, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB12_15:                              # %.preheader55.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	leaq	1(%rbp,%r12), %rax
	leaq	-1(%rbp,%r12), %rcx
	.p2align	4, 0x90
.LBB12_16:                              # %.preheader55
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r15
	leaq	-1(%r15), %rcx
	cmpb	$10, -2(%rax)
	leaq	-1(%rax), %rax
	jne	.LBB12_16
# BB#17:                                # %.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movb	(%rax), %cl
	cmpb	$10, %cl
	jne	.LBB12_19
# BB#18:                                #   in Loop: Header=BB12_2 Depth=1
	decq	%rax
	movq	%rax, %r15
	jmp	.LBB12_20
	.p2align	4, 0x90
.LBB12_19:                              # %.lr.ph
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%cl, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	2(%r15), %ecx
	incq	%r15
	cmpb	$10, %cl
	jne	.LBB12_19
.LBB12_20:                              # %._crit_edge
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	addq	$2, %r15
	.p2align	4, 0x90
.LBB12_7:                               # %.backedge
                                        #   in Loop: Header=BB12_2 Depth=1
	cmpq	%r14, %r15
	movq	%r15, %rbp
	jb	.LBB12_2
.LBB12_8:                               # %._crit_edge63
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	monkey4, .Lfunc_end12-monkey4
	.cfi_endproc

	.globl	blog
	.p2align	4, 0x90
	.type	blog,@function
blog:                                   # @blog
	.cfi_startproc
# BB#0:
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	addl	%esi, %ecx
	movl	$1, %eax
	cmpl	%edi, %ecx
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %eax
	movl	%edi, %edx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	%edi, %edx
	incl	%eax
	cmpl	%ecx, %edx
	jl	.LBB13_2
.LBB13_3:                               # %._crit_edge
	retq
.Lfunc_end13:
	.size	blog, .Lfunc_end13-blog
	.cfi_endproc

	.globl	prep4
	.p2align	4, 0x90
	.type	prep4,@function
prep4:                                  # @prep4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -32
.Lcfi127:
	.cfi_offset %r14, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, char_map+240(%rip)
	movaps	%xmm0, char_map+224(%rip)
	movaps	%xmm0, char_map+208(%rip)
	movaps	%xmm0, char_map+192(%rip)
	movaps	%xmm0, char_map+176(%rip)
	movaps	%xmm0, char_map+160(%rip)
	movaps	%xmm0, char_map+144(%rip)
	movaps	%xmm0, char_map+128(%rip)
	movaps	%xmm0, char_map+112(%rip)
	movaps	%xmm0, char_map+96(%rip)
	movaps	%xmm0, char_map+80(%rip)
	movaps	%xmm0, char_map+64(%rip)
	movaps	%xmm0, char_map+48(%rip)
	movaps	%xmm0, char_map+32(%rip)
	movaps	%xmm0, char_map+16(%rip)
	movaps	%xmm0, char_map(%rip)
	movb	$4, char_map+65(%rip)
	movb	$4, char_map+97(%rip)
	movb	$1, char_map+103(%rip)
	movb	$2, char_map+116(%rip)
	movb	$3, char_map+99(%rip)
	movb	$5, char_map+110(%rip)
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
	addl	%ebp, %ecx
	cmpl	$5, %ecx
	jl	.LBB14_5
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %eax
	movl	$4, %edx
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	shll	$2, %edx
	incl	%eax
	cmpl	%ecx, %edx
	jl	.LBB14_2
# BB#3:                                 # %blog.exit
	movb	%al, BSize(%rip)
	movl	$1, Hashmask(%rip)
	movzbl	%al, %ecx
	leal	(%rcx,%rcx,2), %ecx
	cmpl	$2, %ecx
	jae	.LBB14_6
# BB#4:
	movl	$2, %edi
	jmp	.LBB14_9
.LBB14_5:                               # %blog.exit.thread
	movb	$1, BSize(%rip)
	movl	$1, Hashmask(%rip)
	movl	$1, %eax
.LBB14_6:                               # %.lr.ph46
	movzbl	%al, %eax
	leal	(%rax,%rax,2), %eax
	movl	$1, %edi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB14_7:                               # =>This Inner Loop Header: Depth=1
	leal	1(%rdi,%rdi), %edi
	incl	%ecx
	cmpl	%eax, %ecx
	jl	.LBB14_7
# BB#8:                                 # %._crit_edge47
	movl	%edi, Hashmask(%rip)
	incl	%edi
.LBB14_9:
	callq	malloc
	movq	%rax, MEMBER_D(%rip)
	movb	$0, (%rax)
	cmpl	$0, Hashmask(%rip)
	je	.LBB14_12
# BB#10:                                # %._crit_edge69.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB14_11:                              # %._crit_edge69
                                        # =>This Inner Loop Header: Depth=1
	movq	MEMBER_D(%rip), %rcx
	movb	$0, (%rcx,%rax)
	incq	%rax
	movl	Hashmask(%rip), %ecx
	cmpq	%rcx, %rax
	jbe	.LBB14_11
.LBB14_12:                              # %.preheader33
	movb	BSize(%rip), %al
	testb	%al, %al
	je	.LBB14_25
# BB#13:                                # %.preheader32.lr.ph
	movslq	%ebp, %r8
	movl	$1, %r9d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_14:                              # %.preheader32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_16 Depth 2
                                        #       Child Loop BB14_21 Depth 3
	cmpq	%r8, %rdx
	jge	.LBB14_24
# BB#15:                                # %.preheader.preheader
                                        #   in Loop: Header=BB14_14 Depth=1
	movl	%edx, %r10d
	andl	$1, %r10d
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB14_16:                              # %.preheader
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_21 Depth 3
	leaq	-1(%rdi), %rbp
	testq	%r10, %r10
	jne	.LBB14_17
# BB#18:                                #   in Loop: Header=BB14_16 Depth=2
	movsbq	(%r14,%rbp), %rax
	movzbl	char_map(%rax), %eax
	movl	$1, %esi
	testq	%rdx, %rdx
	jne	.LBB14_20
	jmp	.LBB14_22
	.p2align	4, 0x90
.LBB14_17:                              #   in Loop: Header=BB14_16 Depth=2
	xorl	%esi, %esi
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.LBB14_22
.LBB14_20:                              # %.preheader.new
                                        #   in Loop: Header=BB14_16 Depth=2
	movq	%r9, %rcx
	subq	%rsi, %rcx
	movq	%r14, %rbx
	subq	%rsi, %rbx
	.p2align	4, 0x90
.LBB14_21:                              #   Parent Loop BB14_14 Depth=1
                                        #     Parent Loop BB14_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	-1(%rbx,%rdi), %rsi
	movzbl	char_map(%rsi), %esi
	leal	(%rsi,%rax,8), %eax
	movsbq	-2(%rbx,%rdi), %rsi
	movzbl	char_map(%rsi), %esi
	leal	(%rsi,%rax,8), %eax
	addq	$-2, %rbx
	addq	$-2, %rcx
	jne	.LBB14_21
.LBB14_22:                              # %._crit_edge
                                        #   in Loop: Header=BB14_16 Depth=2
	movq	MEMBER_D(%rip), %rcx
	movl	%eax, %eax
	movb	$1, (%rcx,%rax)
	cmpq	%rdx, %rbp
	movq	%rbp, %rdi
	jg	.LBB14_16
# BB#23:                                # %._crit_edge39.loopexit52
                                        #   in Loop: Header=BB14_14 Depth=1
	movb	BSize(%rip), %al
.LBB14_24:                              # %._crit_edge39
                                        #   in Loop: Header=BB14_14 Depth=1
	incq	%rdx
	movzbl	%al, %ecx
	incq	%r9
	cmpq	%rcx, %rdx
	jl	.LBB14_14
.LBB14_25:                              # %._crit_edge43
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	prep4, .Lfunc_end14-prep4
	.cfi_endproc

	.globl	sgrep
	.p2align	4, 0x90
	.type	sgrep,@function
sgrep:                                  # @sgrep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$10536, %rsp            # imm = 0x2928
.Lcfi135:
	.cfi_def_cfa_offset 10592
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	%esi, 20(%rsp)
	movb	(%r14), %al
	cmpb	$94, %al
	je	.LBB15_2
# BB#1:
	cmpb	$36, %al
	jne	.LBB15_3
.LBB15_2:
	movb	$10, (%r14)
.LBB15_3:
	movslq	%esi, %rax
	movb	-1(%r14,%rax), %cl
	cmpb	$94, %cl
	je	.LBB15_5
# BB#4:
	cmpb	$36, %cl
	jne	.LBB15_6
.LBB15_5:
	movb	$10, -1(%r14,%rax)
.LBB15_6:
	leaq	20(%rsp), %rsi
	movq	%r14, %rdi
	callq	char_tr
	movb	$10, 2079(%rsp)
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	cmpl	$1, WHOLELINE(%rip)
	movl	$2047, %r12d            # imm = 0x7FF
	adcl	$0, %r12d
	movslq	20(%rsp), %r13
	cmpq	$256, %r13              # imm = 0x100
	jge	.LBB15_99
# BB#7:
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%ebx, %ebx
	movl	%ebx, %r15d
	je	.LBB15_8
# BB#11:
	cmpl	$0, DNA(%rip)
	je	.LBB15_13
# BB#12:
	movq	%r14, %rdi
	movl	%r13d, %esi
	callq	prep4
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jg	.LBB15_58
	jmp	.LBB15_72
.LBB15_8:
	movq	%r14, %rdi
	cmpl	$21, %r13d
	jl	.LBB15_10
# BB#9:
	callq	m_preprocess
	jmp	.LBB15_58
.LBB15_13:
	cmpl	$24, 8(%rsp)            # 4-byte Folded Reload
	jl	.LBB15_23
# BB#14:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	movl	$65535, Hashmask(%rip)  # imm = 0xFFFF
	movl	$MEMBER_1, %edi
	xorl	%esi, %esi
	movl	$65536, %edx            # imm = 0x10000
	callq	memset
	testl	%ebp, %ebp
	jle	.LBB15_58
# BB#15:                                # %.lr.ph24.preheader.i
	movslq	%ebp, %rcx
	leaq	1(%rcx), %rax
	.p2align	4, 0x90
.LBB15_16:                              # %.lr.ph24.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-2(%r14,%rax), %edx
	movb	$1, MEMBER_1(%rdx)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB15_16
# BB#17:                                # %.preheader.i
	cmpl	$2, %ebp
	jl	.LBB15_57
# BB#18:                                # %.lr.ph.preheader.i
	leaq	-1(%rcx), %rax
	testb	$1, %al
	je	.LBB15_20
# BB#19:                                # %.lr.ph.i.prol
	movzbl	-1(%r14,%rcx), %edx
	shlq	$8, %rdx
	leaq	-2(%rcx), %rax
	movzbl	-2(%r14,%rcx), %esi
	movb	$1, MEMBER_1(%rdx,%rsi)
.LBB15_20:                              # %.lr.ph.i.prol.loopexit
	cmpq	$2, %rcx
	je	.LBB15_57
# BB#21:                                # %.lr.ph.preheader.i.new
	incq	%rax
	.p2align	4, 0x90
.LBB15_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%r14,%rax), %ecx
	shlq	$8, %rcx
	movzbl	-2(%r14,%rax), %edx
	movb	$1, MEMBER_1(%rcx,%rdx)
	movzbl	-2(%r14,%rax), %ecx
	shlq	$8, %rcx
	movzbl	-3(%r14,%rax), %edx
	movb	$1, MEMBER_1(%rcx,%rdx)
	addq	$-2, %rax
	cmpq	$1, %rax
	jg	.LBB15_22
	jmp	.LBB15_57
.LBB15_10:
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	prep_bm
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jg	.LBB15_58
	jmp	.LBB15_72
.LBB15_23:                              # %._crit_edge64.i
	movq	%r14, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	callq	prep
	leal	-1(%r13), %ecx
	movl	$-2147483648, %eax      # imm = 0x80000000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%eax, endposition(%rip)
	testl	%r13d, %r13d
	jle	.LBB15_71
# BB#24:                                # %.lr.ph60.preheader.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	leaq	-1(%rcx), %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	andq	$3, %rdx
	je	.LBB15_29
	.p2align	4, 0x90
.LBB15_25:                              # %.lr.ph60.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rcx), %ebx
	cmpb	$94, %bl
	je	.LBB15_27
# BB#26:                                # %.lr.ph60.i.prol
                                        #   in Loop: Header=BB15_25 Depth=1
	cmpb	$36, %bl
	jne	.LBB15_28
.LBB15_27:                              #   in Loop: Header=BB15_25 Depth=1
	movb	$10, (%r14,%rcx)
.LBB15_28:                              #   in Loop: Header=BB15_25 Depth=1
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB15_25
.LBB15_29:                              # %.lr.ph60.i.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB15_44
# BB#30:                                # %.lr.ph60.preheader.i.new
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rcx, %rax
	leaq	3(%r14,%rcx), %rcx
	.p2align	4, 0x90
.LBB15_31:                              # %.lr.ph60.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB15_33
# BB#32:                                # %.lr.ph60.i
                                        #   in Loop: Header=BB15_31 Depth=1
	cmpb	$36, %dl
	jne	.LBB15_34
.LBB15_33:                              #   in Loop: Header=BB15_31 Depth=1
	movb	$10, -3(%rcx)
.LBB15_34:                              # %.lr.ph60.i.1115
                                        #   in Loop: Header=BB15_31 Depth=1
	movzbl	-2(%rcx), %edx
	cmpb	$36, %dl
	je	.LBB15_36
# BB#35:                                # %.lr.ph60.i.1115
                                        #   in Loop: Header=BB15_31 Depth=1
	cmpb	$94, %dl
	jne	.LBB15_37
.LBB15_36:                              #   in Loop: Header=BB15_31 Depth=1
	movb	$10, -2(%rcx)
.LBB15_37:                              # %.lr.ph60.i.2116
                                        #   in Loop: Header=BB15_31 Depth=1
	movzbl	-1(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB15_39
# BB#38:                                # %.lr.ph60.i.2116
                                        #   in Loop: Header=BB15_31 Depth=1
	cmpb	$36, %dl
	jne	.LBB15_40
.LBB15_39:                              #   in Loop: Header=BB15_31 Depth=1
	movb	$10, -1(%rcx)
.LBB15_40:                              # %.lr.ph60.i.3117
                                        #   in Loop: Header=BB15_31 Depth=1
	movzbl	(%rcx), %edx
	cmpb	$94, %dl
	je	.LBB15_42
# BB#41:                                # %.lr.ph60.i.3117
                                        #   in Loop: Header=BB15_31 Depth=1
	cmpb	$36, %dl
	jne	.LBB15_43
.LBB15_42:                              #   in Loop: Header=BB15_31 Depth=1
	movb	$10, (%rcx)
.LBB15_43:                              #   in Loop: Header=BB15_31 Depth=1
	addq	$4, %rcx
	addq	$-4, %rax
	jne	.LBB15_31
.LBB15_44:                              # %.lr.ph55.split.us.preheader.i
	movl	$Mask, %edi
	movl	$255, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	movq	8(%rsp), %r9            # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$1, %r9d
	leaq	1(%r14), %r8
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB15_45:                              # %.lr.ph55.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_51 Depth 2
	testq	%r9, %r9
	movzbl	(%r14,%r10), %edi
	jne	.LBB15_46
# BB#98:                                #   in Loop: Header=BB15_45 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB15_50
	jmp	.LBB15_56
	.p2align	4, 0x90
.LBB15_46:                              #   in Loop: Header=BB15_45 Depth=1
	cmpb	(%r14), %dil
	jne	.LBB15_48
# BB#47:                                #   in Loop: Header=BB15_45 Depth=1
	andb	$127, Mask+3(,%rdi,4)
.LBB15_48:                              # %.prol.loopexit
                                        #   in Loop: Header=BB15_45 Depth=1
	movl	$1, %ebp
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB15_56
.LBB15_50:                              # %.lr.ph55.split.us.i.new
                                        #   in Loop: Header=BB15_45 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	subq	%rbp, %rbx
	leaq	(%r8,%rbp), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_51:                              #   Parent Loop BB15_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	-1(%rdx,%rax), %dil
	jne	.LBB15_53
# BB#52:                                #   in Loop: Header=BB15_51 Depth=2
	leal	(%rbp,%rax), %ecx
	movl	$-2147483648, %esi      # imm = 0x80000000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	notl	%esi
	andl	%esi, Mask(,%rdi,4)
.LBB15_53:                              #   in Loop: Header=BB15_51 Depth=2
	cmpb	(%rdx,%rax), %dil
	jne	.LBB15_55
# BB#54:                                #   in Loop: Header=BB15_51 Depth=2
	leal	1(%rbp,%rax), %ecx
	movl	$-2147483648, %esi      # imm = 0x80000000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	notl	%esi
	andl	%esi, Mask(,%rdi,4)
.LBB15_55:                              #   in Loop: Header=BB15_51 Depth=2
	addq	$2, %rax
	cmpq	%rax, %rbx
	jne	.LBB15_51
.LBB15_56:                              # %._crit_edge.us.i
                                        #   in Loop: Header=BB15_45 Depth=1
	incq	%r10
	cmpq	8(%rsp), %r10           # 8-byte Folded Reload
	movl	%r15d, %ebx
	jne	.LBB15_45
.LBB15_57:                              # %am_preprocess.exit.preheader
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB15_72
.LBB15_58:                              # %.lr.ph78
	movb	-1(%r14,%r13), %al
	testq	%r13, %r13
	movl	$1, %ecx
	movl	$1, %edx
	cmovgq	%r13, %rdx
	cmpq	$31, %rdx
	jbe	.LBB15_59
# BB#61:                                # %min.iters.checked
	movabsq	$9223372036854775776, %rsi # imm = 0x7FFFFFFFFFFFFFE0
	movq	%rdx, %rcx
	andq	%rsi, %rcx
	andq	%rdx, %rsi
	je	.LBB15_62
# BB#63:                                # %vector.ph
	movzbl	%al, %edi
	movd	%edi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-32(%rsi), %rbx
	movl	%ebx, %ebp
	shrl	$5, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB15_64
# BB#65:                                # %vector.body.prol.preheader
	negq	%rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_66:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, 10273(%rsp,%rdi)
	movdqu	%xmm0, 10289(%rsp,%rdi)
	addq	$32, %rdi
	incq	%rbp
	jne	.LBB15_66
	jmp	.LBB15_67
.LBB15_62:
	movl	$1, %ecx
	jmp	.LBB15_59
.LBB15_64:
	xorl	%edi, %edi
.LBB15_67:                              # %vector.body.prol.loopexit
	cmpq	$96, %rbx
	jb	.LBB15_69
	.p2align	4, 0x90
.LBB15_68:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbp
	orq	$1, %rbp
	movdqu	%xmm0, 10272(%rsp,%rbp)
	movdqu	%xmm0, 10288(%rsp,%rbp)
	leaq	32(%rdi), %rbp
	orq	$1, %rbp
	movdqu	%xmm0, 10272(%rsp,%rbp)
	movdqu	%xmm0, 10288(%rsp,%rbp)
	leaq	64(%rdi), %rbp
	orq	$1, %rbp
	movdqu	%xmm0, 10272(%rsp,%rbp)
	movdqu	%xmm0, 10288(%rsp,%rbp)
	leaq	96(%rdi), %rbp
	orq	$1, %rbp
	movdqu	%xmm0, 10272(%rsp,%rbp)
	movdqu	%xmm0, 10288(%rsp,%rbp)
	subq	$-128, %rdi
	cmpq	%rsi, %rdi
	jne	.LBB15_68
.LBB15_69:                              # %middle.block
	cmpq	%rsi, %rdx
	movl	%r15d, %ebx
	je	.LBB15_72
# BB#70:
	orq	$1, %rcx
.LBB15_59:                              # %am_preprocess.exit.preheader105
	decq	%rcx
	.p2align	4, 0x90
.LBB15_60:                              # %am_preprocess.exit
                                        # =>This Inner Loop Header: Depth=1
	movb	%al, 10273(%rsp,%rcx)
	incq	%rcx
	cmpq	%r13, %rcx
	jl	.LBB15_60
.LBB15_72:                              # %.preheader
	movq	%r14, 24(%rsp)          # 8-byte Spill
	leaq	2080(%rsp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	16(%rsp), %edi          # 4-byte Reload
	callq	read
	testl	%eax, %eax
	jle	.LBB15_97
# BB#73:                                # %.lr.ph
	testl	%ebx, %ebx
	je	.LBB15_80
# BB#74:
	movl	$1024, %ebp             # imm = 0x400
	.p2align	4, 0x90
.LBB15_75:                              # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_76 Depth 2
	cltq
	leaq	2079(%rsp,%rax), %rcx
	addq	$2047, %rax             # imm = 0x7FF
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_76:                              #   Parent Loop BB15_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rdx), %rsi
	leaq	-1(%rdx), %r15
	cmpq	$2049, %rsi             # imm = 0x801
	jl	.LBB15_78
# BB#77:                                #   in Loop: Header=BB15_76 Depth=2
	cmpb	$10, (%rcx,%rdx)
	movq	%r15, %rdx
	jne	.LBB15_76
.LBB15_78:                              # %.critedge
                                        #   in Loop: Header=BB15_75 Depth=1
	movslq	%r12d, %rcx
	cmpl	$0, DNA(%rip)
	movb	$10, 31(%rsp,%rcx)
	leaq	32(%rsp,%rcx), %rdx
	leaq	32(%rsp,%rax), %rax
	leaq	1(%r15,%rax), %r14
	je	.LBB15_90
# BB#79:                                #   in Loop: Header=BB15_75 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	monkey4
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB15_94
	jmp	.LBB15_96
	.p2align	4, 0x90
.LBB15_90:                              #   in Loop: Header=BB15_75 Depth=1
	cmpl	$24, %r13d
	jl	.LBB15_92
# BB#91:                                #   in Loop: Header=BB15_75 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	a_monkey
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB15_94
	jmp	.LBB15_96
.LBB15_92:                              #   in Loop: Header=BB15_75 Depth=1
	movl	%r13d, %esi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	agrep
	cmpl	$0, FILENAMEONLY(%rip)
	je	.LBB15_96
	.p2align	4, 0x90
.LBB15_94:                              #   in Loop: Header=BB15_75 Depth=1
	movl	num_of_matched(%rip), %eax
	testl	%eax, %eax
	jne	.LBB15_95
.LBB15_96:                              #   in Loop: Header=BB15_75 Depth=1
	leal	2048(%r15), %r12d
	cmpl	$1023, %r12d            # imm = 0x3FF
	cmovlel	%ebp, %r12d
	leaq	32(%rsp,%r12), %rdi
	negq	%r15
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	strncpy
	incl	%r12d
	movl	$8192, %edx             # imm = 0x2000
	movl	16(%rsp), %edi          # 4-byte Reload
	leaq	2080(%rsp), %rsi
	callq	read
	testl	%eax, %eax
	jg	.LBB15_75
	jmp	.LBB15_97
.LBB15_80:                              # %.lr.ph.split.us.preheader
	movl	$1024, %r14d            # imm = 0x400
	.p2align	4, 0x90
.LBB15_81:                              # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_82 Depth 2
	cltq
	leaq	2079(%rsp,%rax), %rcx
	addq	$2047, %rax             # imm = 0x7FF
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_82:                              #   Parent Loop BB15_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rdx), %rsi
	leaq	-1(%rdx), %rbx
	cmpq	$2049, %rsi             # imm = 0x801
	jl	.LBB15_84
# BB#83:                                #   in Loop: Header=BB15_82 Depth=2
	cmpb	$10, (%rcx,%rdx)
	movq	%rbx, %rdx
	jne	.LBB15_82
.LBB15_84:                              # %.critedge.us
                                        #   in Loop: Header=BB15_81 Depth=1
	cmpl	$20, %r13d
	movslq	%r12d, %rcx
	movb	$10, 31(%rsp,%rcx)
	leaq	32(%rsp,%rcx), %rdx
	leaq	32(%rsp,%rax), %rax
	leaq	1(%rbx,%rax), %rbp
	jle	.LBB15_85
# BB#86:                                #   in Loop: Header=BB15_81 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movq	%rbp, %rcx
	callq	monkey
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB15_88
	jmp	.LBB15_89
	.p2align	4, 0x90
.LBB15_85:                              #   in Loop: Header=BB15_81 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movq	%rbp, %rcx
	callq	bm
	cmpl	$0, FILENAMEONLY(%rip)
	je	.LBB15_89
.LBB15_88:                              #   in Loop: Header=BB15_81 Depth=1
	movl	num_of_matched(%rip), %eax
	testl	%eax, %eax
	jne	.LBB15_95
.LBB15_89:                              #   in Loop: Header=BB15_81 Depth=1
	leal	2048(%rbx), %r12d
	cmpl	$1023, %r12d            # imm = 0x3FF
	cmovlel	%r14d, %r12d
	leaq	32(%rsp,%r12), %rdi
	negq	%rbx
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	incl	%r12d
	movl	$8192, %edx             # imm = 0x2000
	movl	16(%rsp), %edi          # 4-byte Reload
	leaq	2080(%rsp), %rsi
	callq	read
	testl	%eax, %eax
	jg	.LBB15_81
	jmp	.LBB15_97
.LBB15_95:                              # %.us-lcssa.us
	movl	$CurrentFileName, %edi
	callq	puts
.LBB15_97:                              # %.loopexit
	addq	$10536, %rsp            # imm = 0x2928
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_71:                              # %am_preprocess.exit.preheader.thread91
	movl	$Mask, %edi
	movl	$255, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	jmp	.LBB15_72
.LBB15_99:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end15:
	.size	sgrep, .Lfunc_end15-sgrep
	.cfi_endproc

	.type	TR,@object              # @TR
	.comm	TR,256,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: "
	.size	.L.str, 5

	.type	shift_1,@object         # @shift_1
	.comm	shift_1,4,4
	.type	SHIFT,@object           # @SHIFT
	.comm	SHIFT,256,16
	.type	MEMBER,@object          # @MEMBER
	.comm	MEMBER,8192,16
	.type	Hashmask,@object        # @Hashmask
	.comm	Hashmask,4,4
	.type	MEMBER_1,@object        # @MEMBER_1
	.comm	MEMBER_1,65536,16
	.type	SHIFT_2,@object         # @SHIFT_2
	.comm	SHIFT_2,4096,16
	.type	char_map,@object        # @char_map
	.comm	char_map,256,16
	.type	MEMBER_D,@object        # @MEMBER_D
	.comm	MEMBER_D,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s:"
	.size	.L.str.2, 4

	.type	BSize,@object           # @BSize
	.comm	BSize,1,1
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: pattern too long\n"
	.size	.L.str.3, 22

	.type	pat,@object             # @pat
	.comm	pat,256,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
