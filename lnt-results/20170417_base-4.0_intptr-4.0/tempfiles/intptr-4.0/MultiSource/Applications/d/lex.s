	.text
	.file	"lex.bc"
	.globl	build_scanners
	.p2align	4, 0x90
	.type	build_scanners,@function
build_scanners:                         # @build_scanners
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$64, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movl	88(%rbx), %r8d
	testq	%r8, %r8
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB0_441
# BB#1:                                 # %.lr.ph123
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	96(%rbx), %r10
	xorl	%r11d, %r11d
	jmp	.LBB0_3
.LBB0_2:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, 400(%r9)
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_10 Depth 3
	movq	(%r10,%r11,8), %r9
	cmpq	$0, 400(%r9)
	jne	.LBB0_15
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	testq	%r11, %r11
	jle	.LBB0_15
# BB#5:                                 # %.lr.ph118.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph118
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_10 Depth 3
	movq	(%r10,%rdi,8), %r14
	cmpq	$0, 400(%r14)
	jne	.LBB0_14
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=2
	movl	136(%r14), %ebx
	cmpl	136(%r9), %ebx
	jne	.LBB0_14
# BB#8:                                 # %.preheader61
                                        #   in Loop: Header=BB0_6 Depth=2
	testl	%ebx, %ebx
	je	.LBB0_12
# BB#9:                                 # %.lr.ph113.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	144(%r9), %rsi
	movq	144(%r14), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph113
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi,%rax,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rcx,%rax,8), %rbp
	cmpq	8(%rbp), %rdx
	jne	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_10 Depth=3
	incq	%rax
	cmpl	%ebx, %eax
	jb	.LBB0_10
	jmp	.LBB0_13
.LBB0_12:                               #   in Loop: Header=BB0_6 Depth=2
	xorl	%eax, %eax
.LBB0_13:                               # %._crit_edge114
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpl	%ebx, %eax
	jae	.LBB0_2
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_6 Depth=2
	incq	%rdi
	cmpq	%r11, %rdi
	jl	.LBB0_6
.LBB0_15:                               # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r11
	cmpq	%r8, %r11
	jb	.LBB0_3
# BB#16:                                # %.preheader
	testl	%r8d, %r8d
	je	.LBB0_442
# BB#17:                                # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rax, %rcx
	addq	$24, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	104(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_53 Depth 3
                                        #     Child Loop BB0_186 Depth 2
                                        #     Child Loop BB0_240 Depth 2
                                        #       Child Loop BB0_241 Depth 3
                                        #         Child Loop BB0_243 Depth 4
                                        #           Child Loop BB0_245 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #           Child Loop BB0_255 Depth 5
                                        #         Child Loop BB0_259 Depth 4
                                        #     Child Loop BB0_280 Depth 2
                                        #     Child Loop BB0_291 Depth 2
                                        #       Child Loop BB0_292 Depth 3
                                        #       Child Loop BB0_299 Depth 3
                                        #         Child Loop BB0_301 Depth 4
                                        #         Child Loop BB0_304 Depth 4
                                        #       Child Loop BB0_307 Depth 3
                                        #         Child Loop BB0_309 Depth 4
                                        #     Child Loop BB0_323 Depth 2
                                        #     Child Loop BB0_334 Depth 2
                                        #     Child Loop BB0_339 Depth 2
                                        #       Child Loop BB0_340 Depth 3
                                        #     Child Loop BB0_348 Depth 2
                                        #     Child Loop BB0_351 Depth 2
                                        #       Child Loop BB0_352 Depth 3
                                        #         Child Loop BB0_357 Depth 4
                                        #           Child Loop BB0_359 Depth 5
                                        #             Child Loop BB0_360 Depth 6
                                        #         Child Loop BB0_375 Depth 4
                                        #         Child Loop BB0_386 Depth 4
                                        #           Child Loop BB0_388 Depth 5
                                        #             Child Loop BB0_389 Depth 6
                                        #     Child Loop BB0_417 Depth 2
                                        #     Child Loop BB0_419 Depth 2
                                        #     Child Loop BB0_424 Depth 2
                                        #       Child Loop BB0_425 Depth 3
	movq	96(%rdx), %rcx
	movq	(%rcx,%rdi,8), %rbx
	cmpl	$0, 136(%rbx)
	je	.LBB0_440
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movq	400(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_18 Depth=1
	movups	360(%rcx), %xmm0
	movups	%xmm0, 360(%rbx)
	movups	296(%rcx), %xmm0
	movups	312(%rcx), %xmm1
	movups	328(%rcx), %xmm2
	movups	344(%rcx), %xmm3
	movups	%xmm3, 344(%rbx)
	movups	%xmm2, 328(%rbx)
	movups	%xmm1, 312(%rbx)
	movups	%xmm0, 296(%rbx)
	jmp	.LBB0_440
	.p2align	4, 0x90
.LBB0_21:                               # %new_NFAState.exit.preheader.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r15
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r15, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	$1, 8(%rdi)
	movq	%r15, 24(%rdi)
	cmpl	$0, 136(%rbx)
	je	.LBB0_422
# BB#22:                                # %.lr.ph471.i
                                        #   in Loop: Header=BB0_18 Depth=1
	leaq	8(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	24(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_42
.LBB0_23:                               #   in Loop: Header=BB0_42 Depth=2
	cmpl	$2, %ecx
	movq	24(%rsp), %rbx          # 8-byte Reload
	ja	.LBB0_25
.LBB0_24:                               #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rax,%rcx,8)
	jmp	.LBB0_26
.LBB0_25:                               #   in Loop: Header=BB0_42 Depth=2
	movq	%r14, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_26:                               # %new_NFAState.exit.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movl	$1, %eax
	jmp	.LBB0_183
.LBB0_27:                               #   in Loop: Header=BB0_42 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	jmp	.LBB0_32
.LBB0_28:                               #   in Loop: Header=BB0_42 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	jmp	.LBB0_34
.LBB0_29:                               #   in Loop: Header=BB0_42 Depth=2
	cmpl	$2, %ecx
	ja	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %eax
	movl	%eax, 8(%rdi)
	movq	%rbx, 24(%rdi,%rcx,8)
	jmp	.LBB0_32
.LBB0_31:                               #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB0_32:                               # %new_NFAState.exit366.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB0_46
.LBB0_33:                               #   in Loop: Header=BB0_42 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_35
.LBB0_34:                               #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	xorl	%eax, %eax
	jmp	.LBB0_36
.LBB0_35:                               #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
.LBB0_36:                               # %new_NFAState.exit363.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 16(%r15)
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r15)
	movl	%eax, %eax
	movq	%rbx, 24(%r15,%rax,8)
	jmp	.LBB0_46
.LBB0_37:                               #   in Loop: Header=BB0_42 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	xorl	%eax, %eax
	jmp	.LBB0_41
.LBB0_38:                               #   in Loop: Header=BB0_42 Depth=2
	cmpl	$2, %ecx
	ja	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %eax
	movl	%eax, 8(%rdi)
	movq	%rbx, 24(%rdi,%rcx,8)
	xorl	%eax, %eax
	jmp	.LBB0_41
.LBB0_40:                               #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
.LBB0_41:                               # %new_NFAState.exit364.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movq	16(%r15), %rcx
	leal	1(%rax), %edx
	movl	%edx, 8(%r15)
	movl	%eax, %eax
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_42:                               #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_53 Depth 3
	movq	144(%rbx), %rcx
	movq	(%rcx,%r12,8), %r14
	movl	(%r14), %ecx
	cmpl	$1, %ecx
	je	.LBB0_49
# BB#43:                                #   in Loop: Header=BB0_42 Depth=2
	testl	%ecx, %ecx
	jne	.LBB0_183
# BB#44:                                #   in Loop: Header=BB0_42 Depth=2
	movq	16(%r15), %rbp
	cmpl	$0, 8(%r15)
	je	.LBB0_84
# BB#45:                                #   in Loop: Header=BB0_42 Depth=2
	movq	(%rbp), %rbx
.LBB0_46:                               #   in Loop: Header=BB0_42 Depth=2
	movq	10296(%rbx), %rax
	leaq	10304(%rbx), %rdx
	testq	%rax, %rax
	je	.LBB0_90
# BB#47:                                #   in Loop: Header=BB0_42 Depth=2
	addq	$10288, %rbx            # imm = 0x2830
	movl	(%rbx), %ecx
	cmpq	%rdx, %rax
	je	.LBB0_91
# BB#48:                                #   in Loop: Header=BB0_42 Depth=2
	testb	$7, %cl
	jne	.LBB0_92
	jmp	.LBB0_93
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_42 Depth=2
	movq	8(%r14), %rcx
	cmpl	$0, (%rcx)
	jne	.LBB0_183
# BB#50:                                #   in Loop: Header=BB0_42 Depth=2
	testb	$8, 36(%rcx)
	movq	24(%rcx), %r13
	movb	(%r13), %bl
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_94
# BB#51:                                # %.preheader407.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movq	%r15, %r14
	testb	%bl, %bl
	jne	.LBB0_53
	jmp	.LBB0_178
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_53 Depth=3
	testq	%rcx, %rcx
	je	.LBB0_61
# BB#56:                                #   in Loop: Header=BB0_53 Depth=3
	leaq	24(%r14,%rax,8), %rdx
	cmpq	%rdx, %rcx
	je	.LBB0_65
# BB#57:                                #   in Loop: Header=BB0_53 Depth=3
	leaq	8(%r14,%rax,8), %rbx
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r14)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_69
# BB#58:                                #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_71
# BB#59:                                #   in Loop: Header=BB0_53 Depth=3
	testb	$7, %al
	je	.LBB0_73
# BB#60:                                #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB0_74
.LBB0_61:                               #   in Loop: Header=BB0_53 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_70
# BB#62:                                #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_75
# BB#63:                                #   in Loop: Header=BB0_53 Depth=3
	testb	$7, %al
	je	.LBB0_77
# BB#64:                                #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_78
.LBB0_65:                               #   in Loop: Header=BB0_53 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_79
# BB#66:                                #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_80
# BB#67:                                #   in Loop: Header=BB0_53 Depth=3
	testb	$7, %al
	je	.LBB0_82
# BB#68:                                #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_83
.LBB0_69:                               #   in Loop: Header=BB0_53 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_72
.LBB0_70:                               #   in Loop: Header=BB0_53 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	jmp	.LBB0_76
.LBB0_71:                               #   in Loop: Header=BB0_53 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_73
.LBB0_72:                               #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%r14, 24(%rdx,%rax,8)
	jmp	.LBB0_74
.LBB0_73:                               #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB0_74:                               # %new_NFAState.exit370.i
                                        #   in Loop: Header=BB0_53 Depth=3
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB0_52
.LBB0_75:                               #   in Loop: Header=BB0_53 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_77
.LBB0_76:                               #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	jmp	.LBB0_78
.LBB0_77:                               #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_78:                               # %new_NFAState.exit367.i
                                        #   in Loop: Header=BB0_53 Depth=3
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	24(%r14,%rax,8), %rdx
	movq	%rdx, 16(%r14,%rax,8)
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbx, 16(%rcx,%rdx,8)
	movq	%rbx, %r14
	jmp	.LBB0_52
.LBB0_79:                               #   in Loop: Header=BB0_53 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	jmp	.LBB0_81
.LBB0_80:                               #   in Loop: Header=BB0_53 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_82
.LBB0_81:                               #   in Loop: Header=BB0_53 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	jmp	.LBB0_83
.LBB0_82:                               #   in Loop: Header=BB0_53 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_83:                               # %new_NFAState.exit368.i
                                        #   in Loop: Header=BB0_53 Depth=3
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r14,%rax,8), %rcx
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbx, (%rcx,%rdx,8)
	movq	%rbx, %r14
	jmp	.LBB0_52
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph467.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%bl, %eax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r14,%rax,8), %rcx
	cmpl	$0, 8(%r14,%rax,8)
	je	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_53 Depth=3
	movq	(%rcx), %r14
.LBB0_52:                               #   in Loop: Header=BB0_53 Depth=3
	movzbl	1(%r13), %ebx
	incq	%r13
	testb	%bl, %bl
	jne	.LBB0_53
	jmp	.LBB0_178
.LBB0_84:                               #   in Loop: Header=BB0_42 Depth=2
	movl	$10368, %edi            # imm = 0x2880
	testq	%rbp, %rbp
	je	.LBB0_169
# BB#85:                                #   in Loop: Header=BB0_42 Depth=2
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rax
	cmpq	48(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_173
# BB#86:                                #   in Loop: Header=BB0_42 Depth=2
	testq	%rax, %rax
	je	.LBB0_27
# BB#87:                                #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx), %ecx
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB0_29
# BB#88:                                #   in Loop: Header=BB0_42 Depth=2
	testb	$7, %cl
	je	.LBB0_31
# BB#89:                                #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB0_32
.LBB0_90:                               #   in Loop: Header=BB0_42 Depth=2
	movq	%rdx, 10296(%rbx)
	movl	10288(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10288(%rbx)
	movq	%r14, 10304(%rbx,%rax,8)
	jmp	.LBB0_182
.LBB0_91:                               #   in Loop: Header=BB0_42 Depth=2
	cmpl	$2, %ecx
	ja	.LBB0_93
.LBB0_92:                               #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, (%rbx)
	movq	%r14, (%rax,%rcx,8)
	jmp	.LBB0_182
.LBB0_93:                               #   in Loop: Header=BB0_42 Depth=2
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB0_182
.LBB0_94:                               # %.preheader408.i
                                        #   in Loop: Header=BB0_42 Depth=2
	testb	%bl, %bl
	je	.LBB0_177
# BB#95:                                # %.lr.ph462.i
                                        #   in Loop: Header=BB0_42 Depth=2
	callq	__ctype_b_loc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB0_96:                               #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	%bl, %ebp
	testb	$4, 1(%rax,%rbp,2)
	jne	.LBB0_100
# BB#97:                                #   in Loop: Header=BB0_96 Depth=3
	leaq	(%rbp,%rbp,4), %rax
	movq	16(%r14,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB0_103
# BB#98:                                #   in Loop: Header=BB0_96 Depth=3
	leaq	24(%r14,%rax,8), %rsi
	movl	8(%r14,%rax,8), %ecx
	cmpq	%rsi, %rdx
	je	.LBB0_111
# BB#99:                                #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %cl
	jne	.LBB0_112
	jmp	.LBB0_116
	.p2align	4, 0x90
.LBB0_100:                              #   in Loop: Header=BB0_96 Depth=3
	callq	__ctype_toupper_loc
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r14,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB0_107
# BB#101:                               #   in Loop: Header=BB0_96 Depth=3
	leaq	24(%r14,%rax,8), %rsi
	movl	8(%r14,%rax,8), %ecx
	cmpq	%rsi, %rdx
	je	.LBB0_120
# BB#102:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %cl
	jne	.LBB0_121
	jmp	.LBB0_125
.LBB0_103:                              #   in Loop: Header=BB0_96 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_129
# BB#104:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_135
# BB#105:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_137
# BB#106:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_138
.LBB0_107:                              #   in Loop: Header=BB0_96 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbp
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbp, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%rbp)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_130
# BB#108:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_139
# BB#109:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_141
# BB#110:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB0_142
.LBB0_111:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %ecx
	ja	.LBB0_116
.LBB0_112:                              #   in Loop: Header=BB0_96 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_131
# BB#113:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_143
# BB#114:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_145
# BB#115:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_146
.LBB0_116:                              #   in Loop: Header=BB0_96 Depth=3
	leaq	8(%r14,%rax,8), %rbx
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r14)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_133
# BB#117:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_151
# BB#118:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_153
# BB#119:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB0_154
.LBB0_120:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %ecx
	ja	.LBB0_125
.LBB0_121:                              #   in Loop: Header=BB0_96 Depth=3
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbp
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbp, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%rbp)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_132
# BB#122:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_147
# BB#123:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_149
# BB#124:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB0_150
.LBB0_125:                              #   in Loop: Header=BB0_96 Depth=3
	leaq	8(%r14,%rax,8), %rbx
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbp
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbp, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%rbp)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_134
# BB#126:                               #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_155
# BB#127:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	je	.LBB0_157
# BB#128:                               #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB0_158
.LBB0_129:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	jmp	.LBB0_136
.LBB0_130:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_140
.LBB0_131:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdi)
	movl	8(%rdi), %eax
	jmp	.LBB0_144
.LBB0_132:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_148
.LBB0_133:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_152
.LBB0_134:                              #   in Loop: Header=BB0_96 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_156
.LBB0_135:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_137
.LBB0_136:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	jmp	.LBB0_138
.LBB0_137:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_138:                              # %new_NFAState.exit375.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	24(%r14,%rax,8), %rdx
	movq	%rdx, 16(%r14,%rax,8)
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbx, 16(%rcx,%rdx,8)
	movq	%rbx, %r14
	jmp	.LBB0_168
.LBB0_139:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_141
.LBB0_140:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rbp, 24(%rdx,%rax,8)
	jmp	.LBB0_142
.LBB0_141:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_142:                              # %new_NFAState.exit371.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movq	(%rbx), %rax
	movzbl	(%r13), %ecx
	movslq	(%rax,%rcx,4), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	24(%r14,%rax,8), %rdx
	movq	%rdx, 16(%r14,%rax,8)
	movq	(%rbx), %rax
	movzbl	(%r13), %edx
	movslq	(%rax,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbp, 16(%rcx,%rdx,8)
	jmp	.LBB0_159
.LBB0_143:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_145
.LBB0_144:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rbx, 24(%rdi,%rax,8)
	jmp	.LBB0_146
.LBB0_145:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_146:                              # %new_NFAState.exit376.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movzbl	(%r13), %eax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r14,%rax,8), %rcx
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbx, (%rcx,%rdx,8)
	movq	%rbx, %r14
	jmp	.LBB0_168
.LBB0_147:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_149
.LBB0_148:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rbp, 24(%rdx,%rax,8)
	jmp	.LBB0_150
.LBB0_149:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_150:                              # %new_NFAState.exit372.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movq	(%rbx), %rax
	movzbl	(%r13), %ecx
	movslq	(%rax,%rcx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r14,%rax,8), %rcx
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbp, (%rcx,%rdx,8)
	jmp	.LBB0_159
.LBB0_151:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_153
.LBB0_152:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%r14, 24(%rdx,%rax,8)
	jmp	.LBB0_154
.LBB0_153:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB0_154:                              # %new_NFAState.exit378.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
	jmp	.LBB0_167
.LBB0_155:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_157
.LBB0_156:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rbp, 24(%rdx,%rax,8)
	jmp	.LBB0_158
.LBB0_157:                              #   in Loop: Header=BB0_96 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_158:                              # %new_NFAState.exit374.i
                                        #   in Loop: Header=BB0_96 Depth=3
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_159:                              #   in Loop: Header=BB0_96 Depth=3
	callq	__ctype_tolower_loc
	movq	(%rax), %rcx
	movzbl	(%r13), %edx
	movslq	(%rcx,%rdx,4), %rcx
	leaq	(%rcx,%rcx,4), %rsi
	movq	16(%r14,%rsi,8), %rcx
	leaq	24(%r14,%rsi,8), %rdx
	testq	%rcx, %rcx
	je	.LBB0_162
# BB#160:                               #   in Loop: Header=BB0_96 Depth=3
	leaq	8(%r14,%rsi,8), %rdi
	movl	8(%r14,%rsi,8), %eax
	cmpq	%rdx, %rcx
	je	.LBB0_163
# BB#161:                               #   in Loop: Header=BB0_96 Depth=3
	testb	$7, %al
	jne	.LBB0_164
	jmp	.LBB0_165
.LBB0_162:                              #   in Loop: Header=BB0_96 Depth=3
	leaq	16(%r14,%rsi,8), %rcx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movzbl	(%r13), %edx
	movslq	(%rax,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r14,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14,%rax,8)
	movq	%rbp, 8(%rcx,%rdx,8)
	jmp	.LBB0_166
.LBB0_163:                              #   in Loop: Header=BB0_96 Depth=3
	cmpl	$2, %eax
	ja	.LBB0_165
.LBB0_164:                              #   in Loop: Header=BB0_96 Depth=3
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB0_166
.LBB0_165:                              #   in Loop: Header=BB0_96 Depth=3
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_166:                              #   in Loop: Header=BB0_96 Depth=3
	movq	%rbp, %r14
.LBB0_167:                              #   in Loop: Header=BB0_96 Depth=3
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_168:                              #   in Loop: Header=BB0_96 Depth=3
	movzbl	1(%r13), %ebx
	incq	%r13
	testb	%bl, %bl
	jne	.LBB0_96
	jmp	.LBB0_178
.LBB0_169:                              #   in Loop: Header=BB0_42 Depth=2
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rbx)
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_28
# BB#170:                               #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_33
# BB#171:                               #   in Loop: Header=BB0_42 Depth=2
	testb	$7, %al
	je	.LBB0_35
# BB#172:                               #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	xorl	%eax, %eax
	jmp	.LBB0_36
.LBB0_173:                              #   in Loop: Header=BB0_42 Depth=2
	testq	%rax, %rax
	je	.LBB0_37
# BB#174:                               #   in Loop: Header=BB0_42 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx), %ecx
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB0_38
# BB#175:                               #   in Loop: Header=BB0_42 Depth=2
	testb	$7, %cl
	je	.LBB0_40
# BB#176:                               #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rcx), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rax,%rcx,8)
	xorl	%eax, %eax
	jmp	.LBB0_41
.LBB0_177:                              #   in Loop: Header=BB0_42 Depth=2
	movq	%r15, %r14
.LBB0_178:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movq	10296(%r14), %rax
	leaq	10304(%r14), %rdx
	testq	%rax, %rax
	je	.LBB0_181
# BB#179:                               #   in Loop: Header=BB0_42 Depth=2
	addq	$10288, %r14            # imm = 0x2830
	movl	(%r14), %ecx
	cmpq	%rdx, %rax
	je	.LBB0_23
# BB#180:                               #   in Loop: Header=BB0_42 Depth=2
	testb	$7, %cl
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_24
	jmp	.LBB0_25
.LBB0_181:                              #   in Loop: Header=BB0_42 Depth=2
	movq	%rdx, 10296(%r14)
	movl	10288(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10288(%r14)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 10304(%r14,%rax,8)
	.p2align	4, 0x90
.LBB0_182:                              # %new_NFAState.exit.i
                                        #   in Loop: Header=BB0_42 Depth=2
	movl	$1, %eax
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_183:                              # %new_NFAState.exit.i
                                        #   in Loop: Header=BB0_42 Depth=2
	incq	%r12
	movl	136(%rbx), %ecx
	cmpl	%ecx, %r12d
	jb	.LBB0_42
# BB#184:                               # %.preheader.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_236
# BB#185:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_18 Depth=1
	leaq	10248(%r15), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	addq	$10264, %r13            # imm = 0x2818
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_186:                              #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%rbx), %rcx
	movq	(%rcx,%r14,8), %rbp
	cmpl	$1, (%rbp)
	jne	.LBB0_235
# BB#187:                               #   in Loop: Header=BB0_186 Depth=2
	movq	8(%rbp), %rcx
	cmpl	$1, (%rcx)
	jne	.LBB0_235
# BB#188:                               #   in Loop: Header=BB0_186 Depth=2
	movq	24(%rcx), %rax
	movq	%rax, 144(%rsp)
	movq	10256(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_191
# BB#189:                               #   in Loop: Header=BB0_186 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ebx
	cmpq	%r13, %rax
	je	.LBB0_195
# BB#190:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %bl
	jne	.LBB0_196
	jmp	.LBB0_200
.LBB0_191:                              #   in Loop: Header=BB0_186 Depth=2
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r12, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r12)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_204
# BB#192:                               #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_207
# BB#193:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %al
	je	.LBB0_209
# BB#194:                               #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r12, (%rcx,%rax,8)
	jmp	.LBB0_210
.LBB0_195:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %ebx
	ja	.LBB0_200
.LBB0_196:                              #   in Loop: Header=BB0_186 Depth=2
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r12, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r12)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_205
# BB#197:                               #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_211
# BB#198:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %al
	je	.LBB0_213
# BB#199:                               #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r12, (%rcx,%rax,8)
	jmp	.LBB0_214
.LBB0_200:                              #   in Loop: Header=BB0_186 Depth=2
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r12, %rdi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r12)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_206
# BB#201:                               #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_215
# BB#202:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %al
	je	.LBB0_217
# BB#203:                               #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r12, (%rcx,%rax,8)
	jmp	.LBB0_218
.LBB0_204:                              #   in Loop: Header=BB0_186 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_208
.LBB0_205:                              #   in Loop: Header=BB0_186 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_212
.LBB0_206:                              #   in Loop: Header=BB0_186 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	jmp	.LBB0_216
.LBB0_207:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_209
.LBB0_208:                              #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%r12, 24(%rdx,%rax,8)
	jmp	.LBB0_210
.LBB0_209:                              #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r12, %rsi
	callq	vec_add_internal
.LBB0_210:                              # %new_NFAState.exit379.i
                                        #   in Loop: Header=BB0_186 Depth=2
	movq	%r13, 10256(%r15)
	movl	10248(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10248(%r15)
	movq	%r12, 10264(%r15,%rax,8)
	jmp	.LBB0_219
.LBB0_211:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_213
.LBB0_212:                              #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%r12, 24(%rdx,%rax,8)
	jmp	.LBB0_214
.LBB0_213:                              #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r12, %rsi
	callq	vec_add_internal
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebx
.LBB0_214:                              # %new_NFAState.exit380.i
                                        #   in Loop: Header=BB0_186 Depth=2
	movq	10256(%r15), %rax
	leal	1(%rbx), %ecx
	movl	%ecx, 10248(%r15)
	movl	%ebx, %ecx
	movq	%r12, (%rax,%rcx,8)
	jmp	.LBB0_219
.LBB0_215:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_217
.LBB0_216:                              #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movq	%r12, 24(%rdx,%rax,8)
	jmp	.LBB0_218
.LBB0_217:                              #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r12, %rsi
	callq	vec_add_internal
.LBB0_218:                              # %new_NFAState.exit382.i
                                        #   in Loop: Header=BB0_186 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	vec_add_internal
.LBB0_219:                              #   in Loop: Header=BB0_186 Depth=2
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	(%rsi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rsi)
	movl	%eax, (%rbx)
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_223
# BB#220:                               #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB0_224
# BB#221:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %al
	je	.LBB0_226
# BB#222:                               #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%edx, (%rdi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB0_227
.LBB0_223:                              #   in Loop: Header=BB0_186 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rsi)
	movl	8(%rsi), %eax
	jmp	.LBB0_225
.LBB0_224:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_226
.LBB0_225:                              #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rsi)
	movq	%rbx, 24(%rsi,%rax,8)
	jmp	.LBB0_227
.LBB0_226:                              #   in Loop: Header=BB0_186 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_227:                              # %new_NFAState.exit383.i
                                        #   in Loop: Header=BB0_186 Depth=2
	movq	8(%rbp), %rax
	movzbl	36(%rax), %eax
	shrb	$3, %al
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, 56(%rsi)
	movq	%rsi, %rdi
	leaq	144(%rsp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	build_regex_nfa
	movq	10296(%rbx), %rax
	leaq	10304(%rbx), %rdx
	testq	%rax, %rax
	je	.LBB0_230
# BB#228:                               #   in Loop: Header=BB0_186 Depth=2
	addq	$10288, %rbx            # imm = 0x2830
	movl	(%rbx), %ecx
	cmpq	%rdx, %rax
	je	.LBB0_231
# BB#229:                               #   in Loop: Header=BB0_186 Depth=2
	testb	$7, %cl
	jne	.LBB0_232
	jmp	.LBB0_233
.LBB0_230:                              #   in Loop: Header=BB0_186 Depth=2
	movq	%rdx, 10296(%rbx)
	movl	10288(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10288(%rbx)
	movq	%rbp, 10304(%rbx,%rax,8)
	jmp	.LBB0_234
.LBB0_231:                              #   in Loop: Header=BB0_186 Depth=2
	cmpl	$2, %ecx
	ja	.LBB0_233
.LBB0_232:                              #   in Loop: Header=BB0_186 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, (%rbx)
	movq	%rbp, (%rax,%rcx,8)
	jmp	.LBB0_234
.LBB0_233:                              #   in Loop: Header=BB0_186 Depth=2
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB0_234:                              #   in Loop: Header=BB0_186 Depth=2
	movl	$1, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_235:                              #   in Loop: Header=BB0_186 Depth=2
	incq	%r14
	cmpl	136(%rbx), %r14d
	jb	.LBB0_186
.LBB0_236:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%eax, %eax
	je	.LBB0_422
# BB#237:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$2096, %edi             # imm = 0x830
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$2096, %edx             # imm = 0x830
	movq	%rbx, %rdi
	callq	memset
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	$0, 96(%rsp)
	leaq	16(%rbx), %rax
	movq	%rax, 8(%rbx)
	movl	$1, (%rbx)
	movq	%r15, 16(%rbx)
	movq	%rbx, %rdi
	callq	nfa_closure
	leaq	80(%rsp), %rax
	movq	%rax, 72(%rsp)
	movl	$1, 64(%rsp)
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 80(%rsp)
	xorl	%eax, %eax
	jmp	.LBB0_240
	.p2align	4, 0x90
.LBB0_238:                              #   in Loop: Header=BB0_240 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	1(%rdx), %rax
	movl	64(%rsp), %r15d
	cmpl	%r15d, %eax
	jae	.LBB0_277
# BB#239:                               # %..lr.ph119_crit_edge.i.i
                                        #   in Loop: Header=BB0_240 Depth=2
	movq	72(%rsp), %rcx
	movq	8(%rcx,%rdx,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
.LBB0_240:                              # %.lr.ph119.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_241 Depth 3
                                        #         Child Loop BB0_243 Depth 4
                                        #           Child Loop BB0_245 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #           Child Loop BB0_255 Depth 5
                                        #         Child Loop BB0_259 Depth 4
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_241:                              # %.preheader85.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_243 Depth 4
                                        #           Child Loop BB0_245 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #           Child Loop BB0_255 Depth 5
                                        #         Child Loop BB0_259 Depth 4
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_276
# BB#242:                               # %.preheader.lr.ph.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_243:                              # %.preheader.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        #       Parent Loop BB0_241 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_245 Depth 5
	movq	(%rax,%r15,8), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rsi,4), %rsi
	cmpl	$0, 8(%rdx,%rsi,8)
	je	.LBB0_249
# BB#244:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_243 Depth=4
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_245:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        #       Parent Loop BB0_241 Depth=3
                                        #         Parent Loop BB0_243 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	testq	%r12, %r12
	jne	.LBB0_247
# BB#246:                               #   in Loop: Header=BB0_245 Depth=5
	movl	$2096, %edi             # imm = 0x830
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$2096, %edx             # imm = 0x830
	movq	%rbx, %rdi
	callq	memset
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	%rbx, %rbp
	movq	%rbx, %r12
.LBB0_247:                              #   in Loop: Header=BB0_245 Depth=5
	movq	(%rax,%r15,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(,%rcx,8), %rcx
	leaq	(%rcx,%rcx,4), %r14
	movq	16(%rax,%r14), %rax
	movq	(%rax,%r13,8), %rsi
	movq	%rbp, %rdi
	callq	set_add
	incq	%r13
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	cmpl	8(%rcx,%r14), %r13d
	jb	.LBB0_245
# BB#248:                               # %._crit_edge.loopexit.i.i
                                        #   in Loop: Header=BB0_243 Depth=4
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
.LBB0_249:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_243 Depth=4
	incq	%r15
	cmpl	%ecx, %r15d
	jb	.LBB0_243
# BB#250:                               # %._crit_edge108.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	testq	%r12, %r12
	je	.LBB0_276
# BB#251:                               #   in Loop: Header=BB0_241 Depth=3
	movq	%rbx, %rdi
	callq	set_to_vec
	movq	%r12, %rdi
	callq	nfa_closure
	movl	64(%rsp), %r9d
	testq	%r9, %r9
	movq	72(%rsp), %r8
	je	.LBB0_262
# BB#252:                               # %.lr.ph113.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	movl	(%r12), %edx
	xorl	%ebp, %ebp
	testl	%edx, %edx
	je	.LBB0_259
.LBB0_253:                              # %.lr.ph113.split.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        #       Parent Loop BB0_241 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_255 Depth 5
	movq	(%r8,%rbp,8), %rax
	cmpl	(%rax), %edx
	jne	.LBB0_257
# BB#254:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_253 Depth=4
	movq	8(%r12), %rdi
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_255:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        #       Parent Loop BB0_241 Depth=3
                                        #         Parent Loop BB0_253 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rdi,%rax,8), %rcx
	cmpq	(%rsi,%rax,8), %rcx
	jne	.LBB0_257
# BB#256:                               #   in Loop: Header=BB0_255 Depth=5
	incq	%rax
	cmpl	%edx, %eax
	jb	.LBB0_255
	jmp	.LBB0_271
	.p2align	4, 0x90
.LBB0_257:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB0_253 Depth=4
	incq	%rbp
	cmpl	%r9d, %ebp
	jb	.LBB0_253
# BB#258:                               #   in Loop: Header=BB0_241 Depth=3
	movl	%r9d, %edx
	testq	%r8, %r8
	jne	.LBB0_263
	jmp	.LBB0_266
	.p2align	4, 0x90
.LBB0_259:                              # %.lr.ph113.split.us.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_240 Depth=2
                                        #       Parent Loop BB0_241 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r8,%rbp,8), %rax
	cmpl	$0, (%rax)
	je	.LBB0_270
# BB#260:                               #   in Loop: Header=BB0_259 Depth=4
	incq	%rbp
	cmpl	%r9d, %ebp
	jb	.LBB0_259
# BB#261:                               #   in Loop: Header=BB0_241 Depth=3
	movl	%r9d, %edx
	jmp	.LBB0_263
.LBB0_262:                              #   in Loop: Header=BB0_241 Depth=3
	xorl	%edx, %edx
	testq	%r8, %r8
	je	.LBB0_266
.LBB0_263:                              # %._crit_edge114.thread.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	leaq	80(%rsp), %rax
	cmpq	%rax, %r8
	je	.LBB0_267
# BB#264:                               #   in Loop: Header=BB0_241 Depth=3
	testb	$7, %dl
	je	.LBB0_269
# BB#265:                               #   in Loop: Header=BB0_241 Depth=3
	leal	1(%rdx), %eax
	movl	%eax, 64(%rsp)
	movl	%edx, %eax
	movq	%r12, (%r8,%rax,8)
	jmp	.LBB0_275
.LBB0_266:                              #   in Loop: Header=BB0_241 Depth=3
	leaq	80(%rsp), %rax
	movq	%rax, 72(%rsp)
	leal	1(%r9), %eax
	movl	%eax, 64(%rsp)
	movq	%rbx, 80(%rsp,%r9,8)
	jmp	.LBB0_275
.LBB0_267:                              #   in Loop: Header=BB0_241 Depth=3
	cmpl	$2, %edx
	ja	.LBB0_269
# BB#268:                               #   in Loop: Header=BB0_241 Depth=3
	leal	1(%rdx), %eax
	movl	%eax, 64(%rsp)
	movl	%edx, %eax
	movq	%r12, 80(%rsp,%rax,8)
	jmp	.LBB0_275
.LBB0_269:                              #   in Loop: Header=BB0_241 Depth=3
	leaq	64(%rsp), %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
	jmp	.LBB0_275
.LBB0_270:                              # %eq_dfa_state.exit.loopexit122.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	movq	8(%r12), %rdi
.LBB0_271:                              # %eq_dfa_state.exit.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	testq	%rdi, %rdi
	je	.LBB0_274
# BB#272:                               # %eq_dfa_state.exit.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.LBB0_274
# BB#273:                               #   in Loop: Header=BB0_241 Depth=3
	callq	free
.LBB0_274:                              # %free_DFAState.exit.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	movq	%rbx, %rdi
	callq	free
	movq	72(%rsp), %rax
	movq	(%rax,%rbp,8), %r12
.LBB0_275:                              #   in Loop: Header=BB0_241 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r12, 40(%rax,%rcx,8)
.LBB0_276:                              # %._crit_edge108.thread.i.i
                                        #   in Loop: Header=BB0_241 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB0_241
	jmp	.LBB0_238
.LBB0_277:                              # %._crit_edge120.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	296(%rax), %r13
	movl	$0, 296(%rax)
	movq	$0, 304(%rax)
	testl	%r15d, %r15d
	je	.LBB0_327
# BB#278:                               # %.lr.ph152.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	leaq	312(%rax), %r14
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	jmp	.LBB0_280
	.p2align	4, 0x90
.LBB0_279:                              # %._crit_edge166.i.i.i
                                        #   in Loop: Header=BB0_280 Depth=2
	movq	304(%rbx), %r12
.LBB0_280:                              #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$4184, %edi             # imm = 0x1058
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$4184, %edx             # imm = 0x1058
	movq	%rbx, %rdi
	callq	memset
	movq	72(%rsp), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	%rbx, 2088(%rcx)
	movq	(%rax,%rbp,8), %rax
	movq	2088(%rax), %rsi
	movl	%ebp, (%rsi)
	testq	%r12, %r12
	je	.LBB0_284
# BB#281:                               #   in Loop: Header=BB0_280 Depth=2
	movl	(%r13), %eax
	cmpq	%r14, %r12
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_285
# BB#282:                               #   in Loop: Header=BB0_280 Depth=2
	testb	$7, %al
	je	.LBB0_287
# BB#283:                               #   in Loop: Header=BB0_280 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, (%r13)
	movq	%rsi, (%r12,%rax,8)
	jmp	.LBB0_288
	.p2align	4, 0x90
.LBB0_284:                              #   in Loop: Header=BB0_280 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%r14, 304(%rbx)
	movl	296(%rbx), %eax
	jmp	.LBB0_286
	.p2align	4, 0x90
.LBB0_285:                              #   in Loop: Header=BB0_280 Depth=2
	cmpl	$2, %eax
	ja	.LBB0_287
.LBB0_286:                              #   in Loop: Header=BB0_280 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 296(%rbx)
	movq	%rsi, 312(%rbx,%rax,8)
	jmp	.LBB0_288
	.p2align	4, 0x90
.LBB0_287:                              #   in Loop: Header=BB0_280 Depth=2
	movq	%r13, %rdi
	callq	vec_add_internal
	movl	64(%rsp), %r15d
.LBB0_288:                              #   in Loop: Header=BB0_280 Depth=2
	incq	%rbp
	cmpl	%r15d, %ebp
	jb	.LBB0_279
# BB#289:                               # %.preheader132.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%r15d, %r15d
	je	.LBB0_328
# BB#290:                               # %.preheader131.i.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_291:                              # %.preheader131.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_292 Depth 3
                                        #       Child Loop BB0_299 Depth 3
                                        #         Child Loop BB0_301 Depth 4
                                        #         Child Loop BB0_304 Depth 4
                                        #       Child Loop BB0_307 Depth 3
                                        #         Child Loop BB0_309 Depth 4
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_292:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	72(%rsp), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	40(%rcx,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB0_294
# BB#293:                               #   in Loop: Header=BB0_292 Depth=3
	movq	2088(%rdx), %rdx
	movq	2088(%rcx), %rcx
	movq	%rdx, 8(%rcx,%rax,8)
.LBB0_294:                              #   in Loop: Header=BB0_292 Depth=3
	movq	72(%rsp), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	48(%rcx,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB0_296
# BB#295:                               #   in Loop: Header=BB0_292 Depth=3
	movq	2088(%rdx), %rdx
	movq	2088(%rcx), %rcx
	movq	%rdx, 16(%rcx,%rax,8)
.LBB0_296:                              #   in Loop: Header=BB0_292 Depth=3
	addq	$2, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_292
# BB#297:                               # %.preheader130.i.i.i
                                        #   in Loop: Header=BB0_291 Depth=2
	movq	72(%rsp), %rdi
	movq	(%rdi,%r14,8), %r12
	movl	(%r12), %r8d
	testq	%r8, %r8
	je	.LBB0_320
# BB#298:                               # %.preheader128.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_291 Depth=2
	movq	8(%r12), %rcx
	movl	$-2147483648, %ebp      # imm = 0x80000000
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_299:                              # %.preheader128.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_301 Depth 4
                                        #         Child Loop BB0_304 Depth 4
	movq	(%rcx,%r10,8), %rdx
	movl	10288(%rdx), %ebx
	testq	%rbx, %rbx
	je	.LBB0_305
# BB#300:                               # %.lr.ph.i81.i.i
                                        #   in Loop: Header=BB0_299 Depth=3
	movq	10296(%rdx), %r11
	leaq	-1(%rbx), %r9
	movq	%rbx, %r15
	xorl	%esi, %esi
	andq	$3, %r15
	je	.LBB0_302
	.p2align	4, 0x90
.LBB0_301:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        #       Parent Loop BB0_299 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r11,%rsi,8), %rdx
	movq	8(%rdx), %rdx
	movl	8(%rdx), %edx
	cmpl	%edx, %ebp
	cmovll	%edx, %ebp
	incq	%rsi
	cmpq	%rsi, %r15
	jne	.LBB0_301
.LBB0_302:                              # %.prol.loopexit
                                        #   in Loop: Header=BB0_299 Depth=3
	cmpq	$3, %r9
	jb	.LBB0_305
# BB#303:                               # %.lr.ph.i81.i.i.new
                                        #   in Loop: Header=BB0_299 Depth=3
	subq	%rsi, %rbx
	leaq	24(%r11,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB0_304:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        #       Parent Loop BB0_299 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-24(%rdx), %rsi
	movq	-16(%rdx), %rax
	movq	8(%rsi), %rsi
	movl	8(%rsi), %esi
	cmpl	%esi, %ebp
	cmovll	%esi, %ebp
	movq	8(%rax), %rax
	movl	8(%rax), %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	movq	-8(%rdx), %rax
	movq	8(%rax), %rax
	movl	8(%rax), %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movl	8(%rax), %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB0_304
.LBB0_305:                              # %._crit_edge.i82.i.i
                                        #   in Loop: Header=BB0_299 Depth=3
	incq	%r10
	cmpq	%r8, %r10
	jne	.LBB0_299
# BB#306:                               # %.preheader.i83.i.i.preheader
                                        #   in Loop: Header=BB0_291 Depth=2
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_307:                              # %.preheader.i83.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_309 Depth 4
	movq	(%rcx,%r15,8), %rdx
	cmpl	$0, 10288(%rdx)
	je	.LBB0_318
# BB#308:                               # %.lr.ph144.i.i.i.preheader
                                        #   in Loop: Header=BB0_307 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_309:                              # %.lr.ph144.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_291 Depth=2
                                        #       Parent Loop BB0_307 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	10296(%rdx), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	8(%rsi), %rax
	cmpl	%ebp, 8(%rax)
	jne	.LBB0_317
# BB#310:                               #   in Loop: Header=BB0_309 Depth=4
	movq	2088(%r12), %rax
	movq	2064(%rax), %rcx
	leaq	2072(%rax), %rdx
	testq	%rcx, %rcx
	je	.LBB0_313
# BB#311:                               #   in Loop: Header=BB0_309 Depth=4
	movl	2056(%rax), %edi
	cmpq	%rdx, %rcx
	je	.LBB0_314
# BB#312:                               #   in Loop: Header=BB0_309 Depth=4
	testb	$7, %dil
	jne	.LBB0_315
	jmp	.LBB0_316
.LBB0_313:                              #   in Loop: Header=BB0_309 Depth=4
	movq	%rdx, 2064(%rax)
	movq	(%rdi,%r14,8), %rcx
	movq	2088(%rcx), %rcx
	movl	2056(%rcx), %edx
	leal	1(%rdx), %edi
	movl	%edi, 2056(%rcx)
	movq	%rsi, 2072(%rax,%rdx,8)
	jmp	.LBB0_317
.LBB0_314:                              #   in Loop: Header=BB0_309 Depth=4
	cmpl	$2, %edi
	ja	.LBB0_316
.LBB0_315:                              #   in Loop: Header=BB0_309 Depth=4
	leal	1(%rdi), %edx
	movl	%edx, 2056(%rax)
	movq	%rsi, (%rcx,%rdi,8)
	jmp	.LBB0_317
.LBB0_316:                              #   in Loop: Header=BB0_309 Depth=4
	addq	$2056, %rax             # imm = 0x808
	movq	%rax, %rdi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB0_317:                              #   in Loop: Header=BB0_309 Depth=4
	incq	%rbx
	movq	72(%rsp), %rdi
	movq	(%rdi,%r14,8), %r12
	movq	8(%r12), %rcx
	movq	(%rcx,%r15,8), %rdx
	cmpl	10288(%rdx), %ebx
	jb	.LBB0_309
.LBB0_318:                              # %._crit_edge145.i.i.i
                                        #   in Loop: Header=BB0_307 Depth=3
	incq	%r15
	cmpl	(%r12), %r15d
	jb	.LBB0_307
# BB#319:                               # %._crit_edge147.i.loopexit.i.i
                                        #   in Loop: Header=BB0_291 Depth=2
	movl	64(%rsp), %r15d
.LBB0_320:                              # %._crit_edge147.i.i.i
                                        #   in Loop: Header=BB0_291 Depth=2
	incq	%r14
	cmpl	%r15d, %r14d
	jb	.LBB0_291
# BB#321:                               # %dfa_to_scanner.exit.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%r15d, %r15d
	je	.LBB0_329
# BB#322:                               # %.lr.ph.i76.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_323:                              # %.lr.ph.i76.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_326
# BB#324:                               # %.lr.ph.i76.i.i
                                        #   in Loop: Header=BB0_323 Depth=2
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_326
# BB#325:                               #   in Loop: Header=BB0_323 Depth=2
	callq	free
.LBB0_326:                              # %free_DFAState.exit.i.i.i
                                        #   in Loop: Header=BB0_323 Depth=2
	movq	%rbx, %rdi
	callq	free
	incq	%rbp
	movq	72(%rsp), %rdi
	cmpl	64(%rsp), %ebp
	jb	.LBB0_323
	jmp	.LBB0_329
.LBB0_327:                              # %dfa_to_scanner.exit.thread.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_330
	jmp	.LBB0_332
.LBB0_328:                              # %dfa_to_scanner.exit.thread158.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	72(%rsp), %rdi
.LBB0_329:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_332
.LBB0_330:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	leaq	80(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB0_332
# BB#331:                               #   in Loop: Header=BB0_18 Depth=1
	callq	free
.LBB0_332:                              # %nfa_to_scanner.exit.i
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpl	$0, (%r13)
	je	.LBB0_336
# BB#333:                               # %.lr.ph53.i.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_334:                              # %.lr.ph53.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	304(%rbp), %rax
	movq	(%rax,%rbx,8), %rsi
	leaq	2096(%rsi), %rdi
	addq	$2056, %rsi             # imm = 0x808
	callq	set_union
	incq	%rbx
	movl	296(%rbp), %eax
	cmpl	%eax, %ebx
	jb	.LBB0_334
# BB#335:                               # %.preheader43.i.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, %rbx
	testl	%eax, %eax
	jne	.LBB0_338
	jmp	.LBB0_414
.LBB0_336:                              #   in Loop: Header=BB0_18 Depth=1
	xorl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_337:                              # %.preheader43.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%eax, %eax
	je	.LBB0_414
.LBB0_338:                              # %.lr.ph50.i.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_339:                              # %.lr.ph50.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_340 Depth 3
	movq	304(%rbx), %rax
	movq	(%rax,%r14,8), %rbx
	leaq	2096(%rbx), %r15
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB0_340:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_339 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rbp,8), %rsi
	testq	%rsi, %rsi
	je	.LBB0_343
# BB#341:                               #   in Loop: Header=BB0_340 Depth=3
	cmpq	%rsi, %rbx
	je	.LBB0_343
# BB#342:                               #   in Loop: Header=BB0_340 Depth=3
	addq	$2096, %rsi             # imm = 0x830
	movq	%r15, %rdi
	callq	set_union
	testl	%eax, %eax
	movl	$1, %eax
	cmovnel	%eax, %r12d
.LBB0_343:                              #   in Loop: Header=BB0_340 Depth=3
	incq	%rbp
	cmpq	$257, %rbp              # imm = 0x101
	jne	.LBB0_340
# BB#344:                               #   in Loop: Header=BB0_339 Depth=2
	incq	%r14
	movl	(%r13), %eax
	cmpl	%eax, %r14d
	movq	24(%rsp), %rbx          # 8-byte Reload
	jb	.LBB0_339
# BB#345:                               # %.loopexit.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%r12d, %r12d
	jne	.LBB0_337
# BB#346:                               # %.preheader.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testl	%eax, %eax
	je	.LBB0_414
# BB#347:                               # %.lr.ph.i.i389.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%ebp, %ebp
	movl	$2096, %r14d            # imm = 0x830
	.p2align	4, 0x90
.LBB0_348:                              # %.lr.ph.i.i389.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	304(%rbx), %rax
	movq	(%rax,%rbp,8), %rbx
	addq	%r14, %rbx
	movq	%rbx, %rdi
	callq	set_to_vec
	movq	%rbx, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	callq	sort_VecAction
	incq	%rbp
	movl	296(%rbx), %eax
	cmpl	%eax, %ebp
	jb	.LBB0_348
# BB#349:                               # %compute_liveness.exit.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	$1, trans_hash_fns+16(%rip)
	leaq	336(%rbx), %r15
	testl	%eax, %eax
	je	.LBB0_415
# BB#350:                               # %.lr.ph99.i.i.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%r15, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_351:                              # %.lr.ph99.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_352 Depth 3
                                        #         Child Loop BB0_357 Depth 4
                                        #           Child Loop BB0_359 Depth 5
                                        #             Child Loop BB0_360 Depth 6
                                        #         Child Loop BB0_375 Depth 4
                                        #         Child Loop BB0_386 Depth 4
                                        #           Child Loop BB0_388 Depth 5
                                        #             Child Loop BB0_389 Depth 6
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	304(%rax), %rax
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_352:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_357 Depth 4
                                        #           Child Loop BB0_359 Depth 5
                                        #             Child Loop BB0_360 Depth 6
                                        #         Child Loop BB0_375 Depth 4
                                        #         Child Loop BB0_386 Depth 4
                                        #           Child Loop BB0_388 Depth 5
                                        #             Child Loop BB0_389 Depth 6
	testq	%rbx, %rbx
	jne	.LBB0_354
# BB#353:                               #   in Loop: Header=BB0_352 Depth=3
	movl	$88, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 80(%rbx)
	movq	%rbx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
.LBB0_354:                              #   in Loop: Header=BB0_352 Depth=3
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	8(%r14,%rbp,8), %r12
	testq	%r12, %r12
	je	.LBB0_402
# BB#355:                               #   in Loop: Header=BB0_352 Depth=3
	leaq	8(%rbx), %rdi
	movl	2096(%r14), %eax
	testl	%eax, %eax
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	je	.LBB0_384
# BB#356:                               # %.preheader65.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	leaq	24(%rbx), %r8
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movq	%r8, 40(%rsp)           # 8-byte Spill
.LBB0_357:                              # %.preheader65.i.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_359 Depth 5
                                        #             Child Loop BB0_360 Depth 6
	cmpl	2096(%r12), %r13d
	jae	.LBB0_373
# BB#358:                               # %.preheader63.preheader.i.i.i.i
                                        #   in Loop: Header=BB0_357 Depth=4
	movslq	%r13d, %r13
.LBB0_359:                              # %.preheader63.i.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        #         Parent Loop BB0_357 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_360 Depth 6
	movslq	%r15d, %rbp
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB0_360:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        #         Parent Loop BB0_357 Depth=4
                                        #           Parent Loop BB0_359 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	2104(%r14), %rcx
	movq	(%rcx,%rbp), %rsi
	movq	2104(%r12), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	32(%rcx), %ecx
	cmpl	%ecx, 32(%rsi)
	je	.LBB0_372
# BB#361:                               #   in Loop: Header=BB0_360 Depth=6
	jae	.LBB0_371
# BB#362:                               #   in Loop: Header=BB0_360 Depth=6
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_366
# BB#363:                               #   in Loop: Header=BB0_360 Depth=6
	movl	(%rdi), %eax
	cmpq	%r8, %rcx
	je	.LBB0_367
# BB#364:                               #   in Loop: Header=BB0_360 Depth=6
	testb	$7, %al
	je	.LBB0_369
# BB#365:                               #   in Loop: Header=BB0_360 Depth=6
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	%rsi, (%rcx,%rax,8)
	jmp	.LBB0_370
	.p2align	4, 0x90
.LBB0_366:                              #   in Loop: Header=BB0_360 Depth=6
	movq	%r8, 16(%rbx)
	movl	8(%rbx), %eax
	jmp	.LBB0_368
	.p2align	4, 0x90
.LBB0_367:                              #   in Loop: Header=BB0_360 Depth=6
	cmpl	$2, %eax
	ja	.LBB0_369
.LBB0_368:                              #   in Loop: Header=BB0_360 Depth=6
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	movq	%rsi, 24(%rbx,%rax,8)
	jmp	.LBB0_370
	.p2align	4, 0x90
.LBB0_369:                              #   in Loop: Header=BB0_360 Depth=6
	callq	vec_add_internal
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB0_370:                              #   in Loop: Header=BB0_360 Depth=6
	movl	2096(%r14), %eax
	incl	%r15d
	addq	$8, %rbp
	cmpl	%eax, %r15d
	jb	.LBB0_360
	jmp	.LBB0_384
	.p2align	4, 0x90
.LBB0_371:                              #   in Loop: Header=BB0_359 Depth=5
	incq	%r13
	cmpl	2096(%r12), %r13d
	jb	.LBB0_359
	jmp	.LBB0_373
	.p2align	4, 0x90
.LBB0_372:                              #   in Loop: Header=BB0_357 Depth=4
	incl	%r15d
	incl	%r13d
	cmpl	%eax, %r15d
	jb	.LBB0_357
	jmp	.LBB0_384
.LBB0_373:                              # %.preheader.i.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	cmpl	%eax, %r15d
	jae	.LBB0_384
# BB#374:                               # %.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	movslq	%r15d, %rbp
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB0_375:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_379
# BB#376:                               #   in Loop: Header=BB0_375 Depth=4
	movl	(%rdi), %eax
	cmpq	%r8, %rcx
	je	.LBB0_380
# BB#377:                               #   in Loop: Header=BB0_375 Depth=4
	testb	$7, %al
	je	.LBB0_382
# BB#378:                               #   in Loop: Header=BB0_375 Depth=4
	movq	2104(%r14), %rdx
	movq	(%rdx,%rbp), %rdx
	leal	1(%rax), %esi
	movl	%esi, (%rdi)
	movq	%rdx, (%rcx,%rax,8)
	jmp	.LBB0_383
	.p2align	4, 0x90
.LBB0_379:                              #   in Loop: Header=BB0_375 Depth=4
	movq	2104(%r14), %rax
	movq	(%rax,%rbp), %rax
	movq	%r8, 16(%rbx)
	movl	8(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 8(%rbx)
	movq	%rax, 24(%rbx,%rcx,8)
	jmp	.LBB0_383
	.p2align	4, 0x90
.LBB0_380:                              #   in Loop: Header=BB0_375 Depth=4
	cmpl	$2, %eax
	ja	.LBB0_382
# BB#381:                               #   in Loop: Header=BB0_375 Depth=4
	movq	2104(%r14), %rcx
	movq	(%rcx,%rbp), %rcx
	leal	1(%rax), %edx
	movl	%edx, 8(%rbx)
	movq	%rcx, 24(%rbx,%rax,8)
	jmp	.LBB0_383
	.p2align	4, 0x90
.LBB0_382:                              #   in Loop: Header=BB0_375 Depth=4
	movq	2104(%r14), %rax
	movq	(%rax,%rbp), %rsi
	callq	vec_add_internal
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB0_383:                              # %.backedge.i.i.i.i
                                        #   in Loop: Header=BB0_375 Depth=4
	addq	$8, %rbp
	incl	%r15d
	cmpl	2096(%r14), %r15d
	jb	.LBB0_375
	.p2align	4, 0x90
.LBB0_384:                              # %action_diff.exit.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	movl	2056(%r14), %eax
	testl	%eax, %eax
	je	.LBB0_402
# BB#385:                               # %.preheader41.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	leaq	48(%rbx), %r12
	leaq	64(%rbx), %r15
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
.LBB0_386:                              # %.preheader41.i.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_388 Depth 5
                                        #             Child Loop BB0_389 Depth 6
	movl	(%rdi), %r10d
	cmpl	%r10d, %r13d
	jae	.LBB0_402
# BB#387:                               # %.preheader.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB0_386 Depth=4
	movq	2064(%r14), %r8
	movq	16(%rbx), %r9
	movslq	%r13d, %r13
.LBB0_388:                              # %.preheader.i62.i.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        #         Parent Loop BB0_386 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_389 Depth 6
	movq	(%r9,%r13,8), %rdx
	movl	32(%rdx), %edx
	movslq	%ebp, %rsi
	leaq	(%r8,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB0_389:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_351 Depth=2
                                        #       Parent Loop BB0_352 Depth=3
                                        #         Parent Loop BB0_386 Depth=4
                                        #           Parent Loop BB0_388 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rcx), %rsi
	cmpl	%edx, 32(%rsi)
	je	.LBB0_393
# BB#390:                               #   in Loop: Header=BB0_389 Depth=6
	jae	.LBB0_392
# BB#391:                               #   in Loop: Header=BB0_389 Depth=6
	incl	%ebp
	addq	$8, %rcx
	cmpl	%eax, %ebp
	jb	.LBB0_389
	jmp	.LBB0_402
	.p2align	4, 0x90
.LBB0_392:                              #   in Loop: Header=BB0_388 Depth=5
	incq	%r13
	cmpl	%r10d, %r13d
	jb	.LBB0_388
	jmp	.LBB0_402
	.p2align	4, 0x90
.LBB0_393:                              #   in Loop: Header=BB0_386 Depth=4
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_397
# BB#394:                               #   in Loop: Header=BB0_386 Depth=4
	movl	(%r12), %eax
	cmpq	%r15, %rcx
	je	.LBB0_398
# BB#395:                               #   in Loop: Header=BB0_386 Depth=4
	testb	$7, %al
	je	.LBB0_400
# BB#396:                               #   in Loop: Header=BB0_386 Depth=4
	leal	1(%rax), %edx
	movl	%edx, (%r12)
	movq	%rsi, (%rcx,%rax,8)
	jmp	.LBB0_401
.LBB0_397:                              #   in Loop: Header=BB0_386 Depth=4
	movq	%r15, 56(%rbx)
	movl	48(%rbx), %eax
	jmp	.LBB0_399
.LBB0_398:                              #   in Loop: Header=BB0_386 Depth=4
	cmpl	$2, %eax
	ja	.LBB0_400
.LBB0_399:                              #   in Loop: Header=BB0_386 Depth=4
	leal	1(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movq	%rsi, 64(%rbx,%rax,8)
	jmp	.LBB0_401
.LBB0_400:                              #   in Loop: Header=BB0_386 Depth=4
	movq	%r12, %rdi
	callq	vec_add_internal
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB0_401:                              #   in Loop: Header=BB0_386 Depth=4
	incl	%ebp
	incl	%r13d
	movl	2056(%r14), %eax
	cmpl	%eax, %ebp
	jb	.LBB0_386
	.p2align	4, 0x90
.LBB0_402:                              # %action_intersect.exit.i.i.i
                                        #   in Loop: Header=BB0_352 Depth=3
	movl	$trans_hash_fns, %edx
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	set_add_fn
	movq	%rax, %r12
	cmpq	%rbx, %r12
	je	.LBB0_410
# BB#403:                               #   in Loop: Header=BB0_352 Depth=3
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_406
# BB#404:                               #   in Loop: Header=BB0_352 Depth=3
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_406
# BB#405:                               #   in Loop: Header=BB0_352 Depth=3
	callq	free
.LBB0_406:                              #   in Loop: Header=BB0_352 Depth=3
	movl	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_409
# BB#407:                               #   in Loop: Header=BB0_352 Depth=3
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_409
# BB#408:                               #   in Loop: Header=BB0_352 Depth=3
	callq	free
.LBB0_409:                              #   in Loop: Header=BB0_352 Depth=3
	movl	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	jmp	.LBB0_411
	.p2align	4, 0x90
.LBB0_410:                              #   in Loop: Header=BB0_352 Depth=3
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB0_411:                              #   in Loop: Header=BB0_352 Depth=3
	movq	%r12, 2136(%r14,%rbp,8)
	incq	%rbp
	cmpq	$256, %rbp              # imm = 0x100
	jne	.LBB0_352
# BB#412:                               #   in Loop: Header=BB0_351 Depth=2
	movq	128(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpl	(%r13), %ecx
	jb	.LBB0_351
# BB#413:                               #   in Loop: Header=BB0_18 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_415
.LBB0_414:                              # %compute_liveness.exit.i.thread.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	$1, trans_hash_fns+16(%rip)
	leaq	336(%rbx), %r15
.LBB0_415:                              # %._crit_edge100.i.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	%r15, %rdi
	callq	set_to_vec
	movl	336(%rbx), %eax
	testq	%rax, %rax
	je	.LBB0_420
# BB#416:                               # %.lr.ph.i7.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	344(%rbx), %rcx
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$7, %rdi
	je	.LBB0_418
	.p2align	4, 0x90
.LBB0_417:                              #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rbp
	movl	%edx, (%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB0_417
.LBB0_418:                              # %.prol.loopexit349
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpq	$7, %rsi
	jb	.LBB0_421
	.p2align	4, 0x90
.LBB0_419:                              #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, (%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	leal	1(%rdx), %edi
	movl	%edi, (%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	leal	2(%rdx), %edi
	movl	%edi, (%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	leal	3(%rdx), %edi
	movl	%edi, (%rsi)
	movq	32(%rcx,%rdx,8), %rsi
	leal	4(%rdx), %edi
	movl	%edi, (%rsi)
	movq	40(%rcx,%rdx,8), %rsi
	leal	5(%rdx), %edi
	movl	%edi, (%rsi)
	movq	48(%rcx,%rdx,8), %rsi
	leal	6(%rdx), %edi
	movl	%edi, (%rsi)
	movq	56(%rcx,%rdx,8), %rsi
	leal	7(%rdx), %edi
	movl	%edi, (%rsi)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.LBB0_419
	jmp	.LBB0_421
.LBB0_420:                              #   in Loop: Header=BB0_18 Depth=1
	xorl	%eax, %eax
.LBB0_421:                              #   in Loop: Header=BB0_18 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	addl	%eax, 48(%rdi)
.LBB0_422:                              # %._crit_edge.thread.i
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpl	$0, 8(%rdi)
	movq	16(%rdi), %rdi
	je	.LBB0_436
# BB#423:                               # %.lr.ph.i393.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_424:                              # %.lr.ph.i393.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_425 Depth 3
	movq	(%rdi,%r14,8), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_425:                              #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_424 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx,%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_428
# BB#426:                               #   in Loop: Header=BB0_425 Depth=3
	leaq	24(%rbx,%rbp), %rax
	cmpq	%rdi, %rax
	je	.LBB0_428
# BB#427:                               #   in Loop: Header=BB0_425 Depth=3
	callq	free
.LBB0_428:                              #   in Loop: Header=BB0_425 Depth=3
	movl	$0, 8(%rbx,%rbp)
	movq	$0, 16(%rbx,%rbp)
	addq	$40, %rbp
	cmpq	$10240, %rbp            # imm = 0x2800
	jne	.LBB0_425
# BB#429:                               #   in Loop: Header=BB0_424 Depth=2
	movq	10256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_432
# BB#430:                               #   in Loop: Header=BB0_424 Depth=2
	leaq	10264(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_432
# BB#431:                               #   in Loop: Header=BB0_424 Depth=2
	callq	free
.LBB0_432:                              #   in Loop: Header=BB0_424 Depth=2
	movl	$0, 10248(%rbx)
	movq	$0, 10256(%rbx)
	movq	10296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_435
# BB#433:                               #   in Loop: Header=BB0_424 Depth=2
	leaq	10304(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB0_435
# BB#434:                               #   in Loop: Header=BB0_424 Depth=2
	callq	free
.LBB0_435:                              # %free_NFAState.exit.i.i
                                        #   in Loop: Header=BB0_424 Depth=2
	movq	%rbx, %rdi
	callq	free
	incq	%r14
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rdi
	cmpl	8(%rax), %r14d
	jb	.LBB0_424
.LBB0_436:                              # %._crit_edge.i399.i
                                        #   in Loop: Header=BB0_18 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_439
# BB#437:                               # %._crit_edge.i399.i
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	je	.LBB0_439
# BB#438:                               #   in Loop: Header=BB0_18 Depth=1
	callq	free
.LBB0_439:                              # %build_state_scanner.exit
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, 8(%rax)
	movq	$0, 16(%rax)
	movl	52(%rax), %esi
	incl	%esi
	movl	%esi, 52(%rax)
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	120(%rsp), %rdi         # 8-byte Reload
.LBB0_440:                              #   in Loop: Header=BB0_18 Depth=1
	incq	%rdi
	cmpl	88(%rdx), %edi
	jb	.LBB0_18
	jmp	.LBB0_443
.LBB0_441:
	xorl	%esi, %esi
	cmpl	$0, verbose_level(%rip)
	jne	.LBB0_444
	jmp	.LBB0_445
.LBB0_442:
	xorl	%esi, %esi
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_443:                              # %._crit_edge
	cmpl	$0, verbose_level(%rip)
	je	.LBB0_445
.LBB0_444:
	movl	48(%rax), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_445:
	movq	%rax, %rdi
	callq	free
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	build_scanners, .Lfunc_end0-build_scanners
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_regex_nfa,@function
build_regex_nfa:                        # @build_regex_nfa
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi19:
	.cfi_def_cfa_offset 400
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%rsi), %r12
	movq	%r12, 40(%rsp)
	leaq	8(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	24(%rdi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	10248(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	10264(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r15, %r14
	movq	%r15, 56(%rsp)          # 8-byte Spill
	jmp	.LBB1_1
.LBB1_97:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r13, %r15
	jmp	.LBB1_1
.LBB1_48:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_52
.LBB1_49:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	movq	%r14, 24(%rbx,%rax,8)
	jmp	.LBB1_53
.LBB1_52:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_53:                               # %new_NFAState.exit388
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
	movq	%rbp, %r12
	jmp	.LBB1_1
.LBB1_204:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_208
.LBB1_205:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_209
.LBB1_208:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_209:                              # %new_NFAState.exit384
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_210:                              #   in Loop: Header=BB1_1 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rcx
	movslq	(%rcx,%rbx,4), %rcx
	leaq	(%rcx,%rcx,4), %rsi
	movq	16(%r13,%rsi,8), %rcx
	leaq	24(%r13,%rsi,8), %rdx
	testq	%rcx, %rcx
	je	.LBB1_211
# BB#212:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	8(%r13,%rsi,8), %rdi
	movl	8(%r13,%rsi,8), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_213
# BB#215:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_178
# BB#216:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	jmp	.LBB1_217
.LBB1_211:                              #   in Loop: Header=BB1_1 Depth=1
	leaq	16(%r13,%rsi,8), %rcx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movslq	(%rax,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r13,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r13,%rax,8)
	movq	%r14, 8(%rcx,%rdx,8)
	jmp	.LBB1_218
.LBB1_213:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_178
# BB#214:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %edx
	incl	%edx
.LBB1_217:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%edx, (%rdi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_218
.LBB1_178:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_218:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rbp, %r12
	movq	%r13, %r15
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_113:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rbp, %r12
	movq	%r13, %r14
.LBB1_1:                                # %.backedge400
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_57 Depth 2
                                        #       Child Loop BB1_71 Depth 3
                                        #     Child Loop BB1_84 Depth 2
                                        #     Child Loop BB1_88 Depth 2
	movq	%r14, %r13
	leaq	1(%r12), %rbp
	movq	%rbp, 40(%rsp)
	movb	(%r12), %bl
	movl	%ebx, %eax
	addb	$-40, %al
	cmpb	$52, %al
	ja	.LBB1_2
# BB#227:                               # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, (%r14)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_9
# BB#11:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_14
.LBB1_2:                                # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpb	$124, %bl
	je	.LBB1_15
# BB#3:                                 # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	testb	%bl, %bl
	jne	.LBB1_144
	jmp	.LBB1_4
.LBB1_118:                              #   in Loop: Header=BB1_1 Depth=1
	movq	10256(%r15), %rax
	leaq	10264(%r15), %rdx
	testq	%rax, %rax
	je	.LBB1_119
# BB#120:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	10248(%r15), %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB1_121
# BB#123:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %cl
	je	.LBB1_126
# BB#124:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rcx), %edx
	jmp	.LBB1_125
.LBB1_135:                              #   in Loop: Header=BB1_1 Depth=1
	movq	10256(%r13), %rax
	leaq	10264(%r13), %rdx
	testq	%rax, %rax
	je	.LBB1_128
# BB#136:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	10248(%r13), %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB1_137
# BB#139:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %cl
	je	.LBB1_142
# BB#140:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rcx), %edx
	jmp	.LBB1_141
.LBB1_107:                              #   in Loop: Header=BB1_1 Depth=1
	movq	10256(%r15), %rcx
	leaq	10264(%r15), %rdx
	testq	%rcx, %rcx
	je	.LBB1_108
# BB#109:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	10248(%r15), %rdi
	movl	(%rdi), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_110
# BB#114:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_116
# BB#115:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	%r13, (%rcx,%rax,8)
	jmp	.LBB1_113
.LBB1_54:                               #   in Loop: Header=BB1_1 Depth=1
	xorl	%r15d, %r15d
	cmpb	$94, (%rbp)
	jne	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_1 Depth=1
	addq	$2, %r12
	movq	%r12, 40(%rsp)
	movl	$1, %r15d
	movq	%r12, %rbp
.LBB1_56:                               #   in Loop: Header=BB1_1 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm0, 304(%rsp)
	movaps	%xmm0, 288(%rsp)
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movb	$-1, %al
	jmp	.LBB1_57
.LBB1_143:                              #   in Loop: Header=BB1_1 Depth=1
	leaq	2(%r12), %rbp
	movq	%rbp, 40(%rsp)
	movb	1(%r12), %bl
	testb	%bl, %bl
	je	.LBB1_67
.LBB1_144:                              #   in Loop: Header=BB1_1 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	cmpl	$0, 56(%r12)
	je	.LBB1_145
# BB#146:                               #   in Loop: Header=BB1_1 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bl, %ebx
	testb	$4, 1(%rax,%rbx,2)
	je	.LBB1_147
# BB#179:                               #   in Loop: Header=BB1_1 Depth=1
	callq	__ctype_tolower_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movslq	(%rax,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r13,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB1_180
# BB#189:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	24(%r13,%rax,8), %rsi
	movl	8(%r13,%rax,8), %ecx
	cmpq	%rsi, %rdx
	je	.LBB1_190
# BB#200:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %cl
	jne	.LBB1_191
	jmp	.LBB1_201
.LBB1_145:                              # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movzbl	%bl, %ebx
.LBB1_147:                              #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rbx,%rbx,4), %rbx
	leaq	8(%r13,%rbx,8), %r15
	movq	16(%r13,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB1_148
# BB#157:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	24(%r13,%rbx,8), %rdx
	movl	(%r15), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_158
# BB#168:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	jne	.LBB1_159
	jmp	.LBB1_169
.LBB1_15:                               #   in Loop: Header=BB1_1 Depth=1
	movq	10256(%r13), %rcx
	leaq	10264(%r13), %rdx
	testq	%rcx, %rcx
	je	.LBB1_16
# BB#17:                                #   in Loop: Header=BB1_1 Depth=1
	leaq	10248(%r13), %rdi
	movl	(%rdi), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_18
# BB#20:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx,%rax,8)
	jmp	.LBB1_23
.LBB1_148:                              #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_149
# BB#150:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_151
# BB#153:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_155
# BB#154:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_156
.LBB1_7:                                #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
	movl	8(%rbx), %eax
	jmp	.LBB1_10
.LBB1_119:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, 10256(%r15)
	movl	10248(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10248(%r15)
	movq	%r13, 10264(%r15,%rax,8)
	jmp	.LBB1_127
.LBB1_108:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, 10256(%r15)
	movl	10248(%r15), %eax
	leal	1(%rax), %ecx
	jmp	.LBB1_112
.LBB1_158:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_169
.LBB1_159:                              #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_160
# BB#161:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_162
# BB#164:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_166
# BB#165:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_167
.LBB1_169:                              #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_170
# BB#171:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_172
# BB#174:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_176
# BB#175:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_177
.LBB1_16:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, 10256(%r13)
	movl	10248(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10248(%r13)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 10264(%r13,%rax,8)
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_23
.LBB1_180:                              #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_181
# BB#182:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_183
# BB#185:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_187
# BB#186:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_188
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_13
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	movq	%r14, 24(%rbx,%rax,8)
	jmp	.LBB1_14
.LBB1_13:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_14:                               # %new_NFAState.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	leaq	40(%rsp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	build_regex_nfa
	movq	40(%rsp), %r12
	movq	%r13, %r15
	jmp	.LBB1_1
.LBB1_121:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %ecx
	ja	.LBB1_126
# BB#122:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, %edx
	incl	%edx
.LBB1_125:                              #   in Loop: Header=BB1_1 Depth=1
	movl	%edx, (%rdi)
	movq	%r13, (%rax,%rcx,8)
	jmp	.LBB1_127
.LBB1_126:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%r13, %rsi
	callq	vec_add_internal
.LBB1_127:                              #   in Loop: Header=BB1_1 Depth=1
	movq	10256(%r13), %rcx
	leaq	10264(%r13), %rdx
	testq	%rcx, %rcx
	je	.LBB1_128
# BB#129:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	10248(%r13), %rdi
	movl	(%rdi), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_130
# BB#133:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_142
# BB#134:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	%r15, (%rcx,%rax,8)
	jmp	.LBB1_113
.LBB1_128:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, 10256(%r13)
	movl	10248(%r13), %eax
	leal	1(%rax), %ecx
.LBB1_132:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, 10248(%r13)
	movq	%r15, 10264(%r13,%rax,8)
	jmp	.LBB1_113
.LBB1_137:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %ecx
	ja	.LBB1_142
# BB#138:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, %edx
	incl	%edx
.LBB1_141:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%edx, (%rdi)
	movq	%r15, (%rax,%rcx,8)
	jmp	.LBB1_113
.LBB1_110:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_116
# BB#111:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ecx
	incl	%ecx
.LBB1_112:                              # %.backedge400
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, 10248(%r15)
	movq	%r13, 10264(%r15,%rax,8)
	jmp	.LBB1_113
.LBB1_116:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%r13, %rsi
	callq	vec_add_internal
	jmp	.LBB1_113
.LBB1_130:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_142
# BB#131:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ecx
	incl	%ecx
	jmp	.LBB1_132
.LBB1_142:                              #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, %rsi
	callq	vec_add_internal
	jmp	.LBB1_113
.LBB1_74:                               # %.loopexit
                                        #   in Loop: Header=BB1_57 Depth=2
	movzbl	%cl, %eax
	movb	$1, 80(%rsp,%rax)
	movq	%r12, %rbp
	movb	%cl, %al
	.p2align	4, 0x90
.LBB1_57:                               # %.backedge
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_71 Depth 3
	leaq	1(%rbp), %r12
	movb	(%rbp), %cl
	cmpb	$91, %cl
	jg	.LBB1_60
# BB#58:                                # %.backedge
                                        #   in Loop: Header=BB1_57 Depth=2
	cmpb	$45, %cl
	jne	.LBB1_59
# BB#64:                                #   in Loop: Header=BB1_57 Depth=2
	movb	1(%rbp), %cl
	cmpb	$92, %cl
	je	.LBB1_69
# BB#65:                                #   in Loop: Header=BB1_57 Depth=2
	addq	$2, %rbp
	testb	%cl, %cl
	jne	.LBB1_70
	jmp	.LBB1_66
	.p2align	4, 0x90
.LBB1_69:                               #   in Loop: Header=BB1_57 Depth=2
	movb	2(%rbp), %cl
	addq	$3, %rbp
.LBB1_70:                               #   in Loop: Header=BB1_57 Depth=2
	testb	%cl, %cl
	jne	.LBB1_71
	jmp	.LBB1_66
	.p2align	4, 0x90
.LBB1_72:                               # %.lr.ph
                                        #   in Loop: Header=BB1_71 Depth=3
	movzbl	%al, %eax
	movb	$1, 80(%rsp,%rax)
	incb	%al
.LBB1_71:                               # %.preheader
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_57 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	%cl, %al
	ja	.LBB1_57
	jmp	.LBB1_72
.LBB1_60:                               # %.backedge
                                        #   in Loop: Header=BB1_57 Depth=2
	cmpb	$92, %cl
	je	.LBB1_73
# BB#61:                                # %.backedge
                                        #   in Loop: Header=BB1_57 Depth=2
	cmpb	$93, %cl
	je	.LBB1_62
	jmp	.LBB1_74
.LBB1_59:                               # %.backedge
                                        #   in Loop: Header=BB1_57 Depth=2
	testb	%cl, %cl
	je	.LBB1_62
	jmp	.LBB1_74
.LBB1_73:                               #   in Loop: Header=BB1_57 Depth=2
	movb	1(%rbp), %cl
	addq	$2, %rbp
	movq	%rbp, %r12
	jmp	.LBB1_74
.LBB1_149:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_152
.LBB1_62:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r12, 40(%rsp)
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%eax, (%r14)
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_63
# BB#75:                                #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_76
# BB#79:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_81
# BB#80:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	testl	%r15d, %r15d
	jne	.LBB1_83
	jmp	.LBB1_87
.LBB1_160:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_163
.LBB1_18:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	ja	.LBB1_22
# BB#19:                                #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ecx
	incl	%ecx
	movl	%ecx, 10248(%r13)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 10264(%r13,%rax,8)
	jmp	.LBB1_23
.LBB1_22:                               #   in Loop: Header=BB1_1 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	vec_add_internal
.LBB1_23:                               #   in Loop: Header=BB1_1 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	10256(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_24
# BB#33:                                #   in Loop: Header=BB1_1 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpq	72(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_34
# BB#44:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	jne	.LBB1_35
	jmp	.LBB1_45
.LBB1_24:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, (%r14)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_25
# BB#26:                                #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_27
# BB#29:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_32
.LBB1_34:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_45
.LBB1_35:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, (%r14)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_36
# BB#37:                                #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_38
# BB#40:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_43
.LBB1_45:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	movl	%eax, (%r14)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_48
# BB#50:                                #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_53
.LBB1_190:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %ecx
	ja	.LBB1_201
.LBB1_191:                              #   in Loop: Header=BB1_1 Depth=1
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_192
# BB#193:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_194
# BB#196:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_198
# BB#197:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_199
.LBB1_201:                              #   in Loop: Header=BB1_1 Depth=1
	leaq	8(%r13,%rax,8), %r15
	movl	$10368, %edi            # imm = 0x2880
	callq	malloc
	movq	%rax, %r14
	xorl	%esi, %esi
	movl	$10368, %edx            # imm = 0x2880
	movq	%r14, %rdi
	callq	memset
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	movl	%eax, (%r14)
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB1_202
# BB#203:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB1_204
# BB#206:                               #   in Loop: Header=BB1_1 Depth=1
	testb	$7, %al
	je	.LBB1_208
# BB#207:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r14, (%rcx,%rax,8)
	jmp	.LBB1_209
.LBB1_170:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_173
.LBB1_151:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_155
.LBB1_152:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_156
.LBB1_155:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_156:                              # %new_NFAState.exit390
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%r13,%rbx,8), %rax
	leaq	24(%r13,%rbx,8), %rcx
	movq	%rcx, 8(%r15)
	movl	(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movq	%r14, 24(%rax,%rcx,8)
	jmp	.LBB1_218
.LBB1_162:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_166
.LBB1_163:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_167
.LBB1_166:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_167:                              # %new_NFAState.exit391
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r15), %rax
	movl	(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movq	%r14, (%rax,%rcx,8)
	jmp	.LBB1_218
.LBB1_25:                               #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
	movl	8(%rbx), %eax
	jmp	.LBB1_28
.LBB1_36:                               #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
	movl	8(%rbx), %eax
	jmp	.LBB1_39
.LBB1_181:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_184
.LBB1_172:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_176
.LBB1_173:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_177
.LBB1_176:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_177:                              # %new_NFAState.exit393
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, %rdi
	jmp	.LBB1_178
.LBB1_192:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_195
.LBB1_63:                               #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rdx)
	movl	8(%rdx), %eax
	leal	1(%rax), %ecx
	jmp	.LBB1_78
.LBB1_46:                               #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
	movl	8(%rbx), %eax
	jmp	.LBB1_49
.LBB1_27:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_31
.LBB1_28:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	movq	%r14, 24(%rbx,%rax,8)
	jmp	.LBB1_32
.LBB1_31:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_32:                               # %new_NFAState.exit385
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 10256(%rdx)
	movl	10248(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 10248(%rdx)
	movq	%r14, 10264(%rdx,%rax,8)
	movq	%rbp, %r12
	jmp	.LBB1_1
.LBB1_38:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_42
.LBB1_39:                               #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	movq	%r14, 24(%rbx,%rax,8)
	jmp	.LBB1_43
.LBB1_42:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_43:                               # %new_NFAState.exit386
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	10256(%rsi), %rax
	movl	10248(%rsi), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 10248(%rsi)
	movq	%r14, (%rax,%rcx,8)
	movq	%rbp, %r12
	jmp	.LBB1_1
.LBB1_202:                              #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movl	8(%r12), %eax
	jmp	.LBB1_205
.LBB1_183:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_187
.LBB1_184:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_188
.LBB1_187:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_188:                              # %new_NFAState.exit394
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	(%r15), %rax
	movslq	(%rax,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	8(%r13,%rax,8), %rcx
	leaq	24(%r13,%rax,8), %rdx
	movq	%rdx, 16(%r13,%rax,8)
	movq	(%r15), %rax
	movslq	(%rax,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movl	8(%r13,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r13,%rax,8)
	movq	%r14, 16(%rcx,%rdx,8)
	jmp	.LBB1_210
.LBB1_194:                              #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_198
.LBB1_195:                              #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%r14, 24(%r12,%rax,8)
	jmp	.LBB1_199
.LBB1_198:                              #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
.LBB1_199:                              # %new_NFAState.exit395
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	(%r15), %rax
	movslq	(%rax,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rax
	movq	16(%r13,%rax,8), %rcx
	movl	8(%r13,%rax,8), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r13,%rax,8)
	movq	%r14, (%rcx,%rdx,8)
	jmp	.LBB1_210
.LBB1_76:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2, %eax
	ja	.LBB1_81
# BB#77:                                #   in Loop: Header=BB1_1 Depth=1
	leal	1(%rax), %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB1_78:                               # %new_NFAState.exit389.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%ecx, 8(%rdx)
	movq	%r14, 24(%rdx,%rax,8)
	testl	%r15d, %r15d
	jne	.LBB1_83
	jmp	.LBB1_87
.LBB1_81:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	vec_add_internal
	testl	%r15d, %r15d
	je	.LBB1_87
.LBB1_83:                               # %new_NFAState.exit389.preheader.split.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	xorl	%ebp, %ebp
	leaq	81(%rsp), %rbx
	jmp	.LBB1_84
	.p2align	4, 0x90
.LBB1_85:                               #   in Loop: Header=BB1_84 Depth=2
	movq	56(%r13,%rbp), %rax
	leaq	64(%r13,%rbp), %rdx
	testq	%rax, %rax
	je	.LBB1_86
# BB#98:                                #   in Loop: Header=BB1_84 Depth=2
	movl	48(%r13,%rbp), %ecx
	cmpq	%rax, %rdx
	je	.LBB1_99
# BB#101:                               #   in Loop: Header=BB1_84 Depth=2
	testb	$7, %cl
	je	.LBB1_104
# BB#102:                               #   in Loop: Header=BB1_84 Depth=2
	leal	1(%rcx), %edx
	jmp	.LBB1_103
.LBB1_86:                               #   in Loop: Header=BB1_84 Depth=2
	movq	%rdx, 56(%r13,%rbp)
	movl	48(%r13,%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 48(%r13,%rbp)
	leaq	(%r13,%rax,8), %rax
	movq	%r14, 64(%rbp,%rax)
	jmp	.LBB1_105
.LBB1_99:                               #   in Loop: Header=BB1_84 Depth=2
	cmpl	$2, %ecx
	ja	.LBB1_104
# BB#100:                               #   in Loop: Header=BB1_84 Depth=2
	movl	%ecx, %edx
	incl	%edx
.LBB1_103:                              # %new_NFAState.exit389
                                        #   in Loop: Header=BB1_84 Depth=2
	movl	%edx, 48(%r13,%rbp)
	movq	%r14, (%rax,%rcx,8)
	jmp	.LBB1_105
.LBB1_104:                              #   in Loop: Header=BB1_84 Depth=2
	leaq	48(%r13,%rbp), %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
	jmp	.LBB1_105
	.p2align	4, 0x90
.LBB1_84:                               # %new_NFAState.exit389.preheader.split
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rbx)
	je	.LBB1_85
.LBB1_105:                              # %new_NFAState.exit389
                                        #   in Loop: Header=BB1_84 Depth=2
	incq	%rbx
	addq	$40, %rbp
	cmpq	$10200, %rbp            # imm = 0x27D8
	jne	.LBB1_84
# BB#106:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r13, %r15
	jmp	.LBB1_1
.LBB1_87:                               # %new_NFAState.exit389.preheader.split.us.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	81(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_88:                               # %new_NFAState.exit389.preheader.split.us
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rbx)
	je	.LBB1_96
# BB#89:                                #   in Loop: Header=BB1_88 Depth=2
	movq	56(%r13,%rbp), %rax
	testq	%rax, %rax
	leaq	64(%r13,%rbp), %rcx
	je	.LBB1_95
# BB#90:                                #   in Loop: Header=BB1_88 Depth=2
	cmpq	%rax, %rcx
	movl	48(%r13,%rbp), %ecx
	je	.LBB1_93
# BB#91:                                #   in Loop: Header=BB1_88 Depth=2
	testb	$7, %cl
	jne	.LBB1_92
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_95:                               #   in Loop: Header=BB1_88 Depth=2
	movq	%rcx, 56(%r13,%rbp)
	movl	48(%r13,%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 48(%r13,%rbp)
	leaq	(%r13,%rax,8), %rax
	movq	%r14, 64(%rbp,%rax)
	jmp	.LBB1_96
.LBB1_93:                               #   in Loop: Header=BB1_88 Depth=2
	cmpl	$3, %ecx
	jae	.LBB1_94
.LBB1_92:                               #   in Loop: Header=BB1_88 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 48(%r13,%rbp)
	movq	%r14, (%rax,%rcx,8)
	jmp	.LBB1_96
.LBB1_94:                               #   in Loop: Header=BB1_88 Depth=2
	leaq	48(%r13,%rbp), %rdi
	movq	%r14, %rsi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB1_96:                               # %new_NFAState.exit389.us
                                        #   in Loop: Header=BB1_88 Depth=2
	addq	$40, %rbp
	incq	%rbx
	cmpq	$10200, %rbp            # imm = 0x27D8
	jne	.LBB1_88
	jmp	.LBB1_97
.LBB1_66:                               # %.loopexit398
	movq	%rbp, 40(%rsp)
.LBB1_67:                               # %.loopexit399
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	d_fail
	jmp	.LBB1_68
.LBB1_4:
	movq	10256(%r13), %rcx
	leaq	10264(%r13), %rdx
	testq	%rcx, %rcx
	je	.LBB1_5
# BB#219:
	leaq	10248(%r13), %rdi
	movl	(%rdi), %eax
	cmpq	%rdx, %rcx
	je	.LBB1_220
# BB#224:
	testb	$7, %al
	je	.LBB1_226
# BB#225:
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx,%rax,8)
	jmp	.LBB1_223
.LBB1_5:
	movq	%rdx, 10256(%r13)
	movl	10248(%r13), %eax
	leal	1(%rax), %ecx
	jmp	.LBB1_222
.LBB1_220:
	cmpl	$2, %eax
	ja	.LBB1_226
# BB#221:
	movl	%eax, %ecx
	incl	%ecx
.LBB1_222:
	movl	%ecx, 10248(%r13)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 10264(%r13,%rax,8)
	jmp	.LBB1_223
.LBB1_226:
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	vec_add_internal
.LBB1_223:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
.LBB1_68:
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	build_regex_nfa, .Lfunc_end1-build_regex_nfa
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_6
	.quad	.LBB1_4
	.quad	.LBB1_118
	.quad	.LBB1_135
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_107
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_144
	.quad	.LBB1_54
	.quad	.LBB1_143

	.text
	.p2align	4, 0x90
	.type	nfa_closure,@function
nfa_closure:                            # @nfa_closure
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB2_1
# BB#2:                                 # %.preheader45.lr.ph
	leaq	16(%r12), %r14
	movq	8(%r12), %rdi
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
	movq	(%rdi,%r15,8), %rcx
	cmpl	$0, 10248(%rcx)
	je	.LBB2_19
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jne	.LBB2_6
	.p2align	4, 0x90
.LBB2_9:                                # %._crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB2_21
.LBB2_10:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r14, %rdi
	je	.LBB2_11
# BB#13:                                #   in Loop: Header=BB2_3 Depth=1
	testb	$7, %al
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_3 Depth=1
	movq	10256(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rcx
	leal	1(%rax), %edx
	movl	%edx, (%r12)
	movl	%eax, %eax
	movq	%rcx, (%rdi,%rax,8)
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$2, %eax
	ja	.LBB2_15
# BB#12:                                #   in Loop: Header=BB2_3 Depth=1
	movq	16(%r12,%r15,8), %rcx
	movq	10256(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rcx
	leal	1(%rax), %edx
	movl	%edx, (%r12)
	movl	%eax, %eax
	movq	%rcx, 16(%r12,%rax,8)
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_3 Depth=1
	movq	10256(%rcx), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r12, %rdi
	callq	vec_add_internal
.LBB2_16:                               # %.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%rbx
	movq	8(%r12), %rdi
	movq	(%rdi,%r15,8), %rcx
	cmpl	10248(%rcx), %ebx
	jae	.LBB2_18
# BB#17:                                # %.loopexit..preheader_crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB2_9
.LBB2_6:                                # %.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	10256(%rcx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	(%rdi,%rsi,8), %rdx
	je	.LBB2_16
# BB#7:                                 #   in Loop: Header=BB2_8 Depth=2
	incq	%rsi
	cmpl	%eax, %esi
	jb	.LBB2_8
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_18:                               # %._crit_edge54.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	(%r12), %eax
.LBB2_19:                               # %._crit_edge54
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%r15
	cmpl	%eax, %r15d
	jb	.LBB2_3
	jmp	.LBB2_20
.LBB2_1:                                # %.._crit_edge56_crit_edge
	movq	8(%r12), %rdi
	xorl	%eax, %eax
.LBB2_20:                               # %._crit_edge56
	movl	%eax, %esi
	movl	$8, %edx
	movl	$nfacmp, %ecx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	qsort                   # TAILCALL
.LBB2_21:
	ud2
.Lfunc_end2:
	.size	nfa_closure, .Lfunc_end2-nfa_closure
	.cfi_endproc

	.p2align	4, 0x90
	.type	nfacmp,@function
nfacmp:                                 # @nfacmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	(%rax), %eax
	movq	(%rsi), %rcx
	movl	(%rcx), %ecx
	cmpl	%ecx, %eax
	sbbl	%edx, %edx
	cmpl	%ecx, %eax
	movl	$1, %eax
	cmovbel	%edx, %eax
	retq
.Lfunc_end3:
	.size	nfacmp, .Lfunc_end3-nfacmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	trans_hash_fn,@function
trans_hash_fn:                          # @trans_hash_fn
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	$0, 16(%rsi)
	je	.LBB4_1
.LBB4_5:                                # %.preheader
	movl	48(%rdi), %r8d
	testq	%r8, %r8
	je	.LBB4_10
# BB#6:                                 # %.lr.ph
	movq	56(%rdi), %rdx
	leaq	-1(%r8), %r9
	movq	%r8, %rcx
	xorl	%esi, %esi
	andq	$3, %rcx
	je	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	imull	$3, 32(%rdi), %edi
	addl	%edi, %eax
	incq	%rsi
	cmpq	%rsi, %rcx
	jne	.LBB4_7
.LBB4_8:                                # %.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rcx
	imull	$3, 32(%rcx), %ecx
	addl	%eax, %ecx
	movq	8(%rdx,%rsi,8), %rax
	imull	$3, 32(%rax), %eax
	addl	%ecx, %eax
	movq	16(%rdx,%rsi,8), %rcx
	imull	$3, 32(%rcx), %ecx
	addl	%eax, %ecx
	movq	24(%rdx,%rsi,8), %rax
	imull	$3, 32(%rax), %eax
	addl	%ecx, %eax
	addq	$4, %rsi
	cmpq	%r8, %rsi
	jb	.LBB4_9
.LBB4_10:                               # %._crit_edge
	retq
.LBB4_1:                                # %.preheader18
	movl	8(%rdi), %r9d
	testq	%r9, %r9
	je	.LBB4_5
# BB#2:                                 # %.lr.ph23
	movq	16(%rdi), %rdx
	leaq	-1(%r9), %r8
	movq	%r9, %r10
	xorl	%esi, %esi
	xorl	%eax, %eax
	andq	$3, %r10
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rcx
	imull	$3, 32(%rcx), %ecx
	addl	%ecx, %eax
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_3
.LBB4_4:                                # %.prol.loopexit32
	cmpq	$3, %r8
	jb	.LBB4_5
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rcx
	imull	$3, 32(%rcx), %ecx
	addl	%eax, %ecx
	movq	8(%rdx,%rsi,8), %rax
	imull	$3, 32(%rax), %eax
	addl	%ecx, %eax
	movq	16(%rdx,%rsi,8), %rcx
	imull	$3, 32(%rcx), %ecx
	addl	%eax, %ecx
	movq	24(%rdx,%rsi,8), %rax
	imull	$3, 32(%rax), %eax
	addl	%ecx, %eax
	addq	$4, %rsi
	cmpq	%r9, %rsi
	jb	.LBB4_11
	jmp	.LBB4_5
.Lfunc_end4:
	.size	trans_hash_fn, .Lfunc_end4-trans_hash_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	trans_cmp_fn,@function
trans_cmp_fn:                           # @trans_cmp_fn
	.cfi_startproc
# BB#0:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_2
# BB#1:
	movl	8(%rdi), %ecx
	movl	$1, %eax
	cmpl	8(%rsi), %ecx
	jne	.LBB5_14
.LBB5_2:
	movl	48(%rdi), %r11d
	movl	$1, %eax
	cmpl	48(%rsi), %r11d
	jne	.LBB5_14
# BB#3:
	testq	%rdx, %rdx
	je	.LBB5_4
.LBB5_8:                                # %.preheader
	testl	%r11d, %r11d
	je	.LBB5_9
# BB#12:                                # %.lr.ph
	movq	56(%rdi), %rdx
	movq	56(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rcx
	cmpq	(%rsi,%rdi,8), %rcx
	jne	.LBB5_14
# BB#10:                                #   in Loop: Header=BB5_13 Depth=1
	incq	%rdi
	cmpl	%r11d, %edi
	jb	.LBB5_13
# BB#11:
	xorl	%eax, %eax
	retq
.LBB5_4:                                # %.preheader22
	movl	8(%rdi), %r8d
	testl	%r8d, %r8d
	je	.LBB5_8
# BB#5:                                 # %.lr.ph28
	movq	16(%rdi), %r9
	movq	16(%rsi), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rcx
	cmpq	(%r10,%rdx,8), %rcx
	jne	.LBB5_14
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=1
	incq	%rdx
	cmpl	%r8d, %edx
	jb	.LBB5_6
	jmp	.LBB5_8
.LBB5_14:                               # %.loopexit
	retq
.LBB5_9:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	trans_cmp_fn, .Lfunc_end5-trans_cmp_fn
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d scanners %d transitions\n"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"bad (part of) regex: %s\n"
	.size	.L.str.1, 25

	.type	trans_hash_fns,@object  # @trans_hash_fns
	.data
	.p2align	3
trans_hash_fns:
	.quad	trans_hash_fn
	.quad	trans_cmp_fn
	.zero	16
	.size	trans_hash_fns, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
