	.text
	.file	"MultiStream.bc"
	.globl	_ZN12CMultiStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN12CMultiStream4ReadEPvjPj,@function
_ZN12CMultiStream4ReadEPvjPj:           # @_ZN12CMultiStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r13d
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	%r13d, 4(%rsp)
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	movl	$0, (%r14)
.LBB0_2:
	testl	%r13d, %r13d
	je	.LBB0_3
# BB#4:
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_5
# BB#6:
	movl	32(%r12), %esi
	movl	52(%r12), %edx
	movq	56(%r12), %r8
	xorl	%edi, %edi
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_11:                               # %.thread
                                        #   in Loop: Header=BB0_7 Depth=1
	leal	(%rdi,%rdx), %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	addl	%ecx, %esi
	sarl	%esi
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rcx
	movq	(%r8,%rcx,8), %rbp
	movq	16(%rbp), %rcx
	movq	%rax, %rbx
	subq	%rcx, %rbx
	jae	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movl	%esi, %edx
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	movq	8(%rbp), %rdi
	addq	%rdi, %rcx
	cmpq	%rcx, %rax
	jb	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_7 Depth=1
	incl	%esi
	movl	%esi, %edi
	jmp	.LBB0_11
.LBB0_3:
	xorl	%eax, %eax
	jmp	.LBB0_19
.LBB0_5:
	xorl	%edx, %edx
	cmpq	%rcx, %rax
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%edx, %eax
	jmp	.LBB0_19
.LBB0_12:
	movl	%esi, 32(%r12)
	cmpq	24(%rbp), %rbx
	je	.LBB0_15
# BB#13:
	leaq	24(%rbp), %rcx
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB0_19
# BB#14:                                # %._crit_edge
	movq	8(%rbp), %rdi
.LBB0_15:
	subq	%rbx, %rdi
	movl	%r13d, %eax
	cmpq	%rdi, %rax
	jbe	.LBB0_17
# BB#16:
	movl	%edi, 4(%rsp)
	movl	%edi, %r13d
.LBB0_17:
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	movq	%r15, %rsi
	movl	%r13d, %edx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	addq	%rcx, 16(%r12)
	addq	%rcx, 24(%rbp)
	testq	%r14, %r14
	je	.LBB0_19
# BB#18:
	movl	%ecx, (%r14)
.LBB0_19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN12CMultiStream4ReadEPvjPj, .Lfunc_end0-_ZN12CMultiStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN12CMultiStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN12CMultiStream4SeekExjPy,@function
_ZN12CMultiStream4SeekExjPy:            # @_ZN12CMultiStream4SeekExjPy
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB1_6
# BB#1:
	cmpl	$2, %edx
	je	.LBB1_4
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$1, %edx
	jne	.LBB1_8
# BB#3:
	leaq	16(%rdi), %rax
	jmp	.LBB1_5
.LBB1_4:
	leaq	24(%rdi), %rax
.LBB1_5:                                # %.sink.split
	addq	(%rax), %rsi
.LBB1_6:
	movq	%rsi, 16(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB1_8
# BB#7:
	movq	%rsi, (%rcx)
.LBB1_8:
	retq
.Lfunc_end1:
	.size	_ZN12CMultiStream4SeekExjPy, .Lfunc_end1-_ZN12CMultiStream4SeekExjPy
	.cfi_endproc

	.section	.text._ZN12CMultiStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv,@function
_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv: # @_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB2_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB2_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB2_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB2_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB2_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB2_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB2_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB2_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB2_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB2_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB2_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB2_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB2_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB2_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB2_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB2_32
.LBB2_16:                               # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB2_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB2_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB2_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB2_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB2_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB2_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB2_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB2_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB2_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB2_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB2_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB2_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB2_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB2_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB2_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB2_33
.LBB2_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB2_33:                               # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end2-_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN12CMultiStream6AddRefEv,"axG",@progbits,_ZN12CMultiStream6AddRefEv,comdat
	.weak	_ZN12CMultiStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN12CMultiStream6AddRefEv,@function
_ZN12CMultiStream6AddRefEv:             # @_ZN12CMultiStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN12CMultiStream6AddRefEv, .Lfunc_end3-_ZN12CMultiStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN12CMultiStream7ReleaseEv,"axG",@progbits,_ZN12CMultiStream7ReleaseEv,comdat
	.weak	_ZN12CMultiStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN12CMultiStream7ReleaseEv,@function
_ZN12CMultiStream7ReleaseEv:            # @_ZN12CMultiStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN12CMultiStream7ReleaseEv, .Lfunc_end4-_ZN12CMultiStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN12CMultiStreamD2Ev,"axG",@progbits,_ZN12CMultiStreamD2Ev,comdat
	.weak	_ZN12CMultiStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN12CMultiStreamD2Ev,@function
_ZN12CMultiStreamD2Ev:                  # @_ZN12CMultiStreamD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CMultiStream+16, (%rbx)
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp1:
# BB#1:                                 # %_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_3:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp4:
# BB#4:                                 # %.body
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_2:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN12CMultiStreamD2Ev, .Lfunc_end5-_ZN12CMultiStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12CMultiStreamD0Ev,"axG",@progbits,_ZN12CMultiStreamD0Ev,comdat
	.weak	_ZN12CMultiStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN12CMultiStreamD0Ev,@function
_ZN12CMultiStreamD0Ev:                  # @_ZN12CMultiStreamD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CMultiStream+16, (%rbx)
	leaq	40(%rbx), %r15
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, 40(%rbx)
.Ltmp6:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp7:
# BB#1:                                 # %_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev.exit.i
.Ltmp12:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp13:
# BB#2:                                 # %_ZN12CMultiStreamD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB6_5:
.Ltmp14:
	movq	%rax, %r14
	jmp	.LBB6_6
.LBB6_3:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
.LBB6_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN12CMultiStreamD0Ev, .Lfunc_end6-_ZN12CMultiStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp15:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp16:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB7_2:
.Ltmp17:
	movq	%rax, %r14
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp19:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev, .Lfunc_end7-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp18-.Ltmp16         #   Call between .Ltmp16 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end8:
	.size	__clang_call_terminate, .Lfunc_end8-__clang_call_terminate

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp21:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp22:
# BB#1:
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
# BB#2:                                 # %_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_5:
.Ltmp29:
	movq	%rax, %r14
	jmp	.LBB9_6
.LBB9_3:
.Ltmp23:
	movq	%rax, %r14
.Ltmp24:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp25:
.LBB9_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev, .Lfunc_end9-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp25     #   Call between .Ltmp25 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB10_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB10_5:                               # %_ZN12CMultiStream14CSubStreamInfoD2Ev.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB10_2
.LBB10_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB10_8:
.Ltmp32:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii, .Lfunc_end10-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin4   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp31    #   Call between .Ltmp31 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV12CMultiStream,@object # @_ZTV12CMultiStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV12CMultiStream
	.p2align	3
_ZTV12CMultiStream:
	.quad	0
	.quad	_ZTI12CMultiStream
	.quad	_ZN12CMultiStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN12CMultiStream6AddRefEv
	.quad	_ZN12CMultiStream7ReleaseEv
	.quad	_ZN12CMultiStreamD2Ev
	.quad	_ZN12CMultiStreamD0Ev
	.quad	_ZN12CMultiStream4ReadEPvjPj
	.quad	_ZN12CMultiStream4SeekExjPy
	.size	_ZTV12CMultiStream, 72

	.type	_ZTS12CMultiStream,@object # @_ZTS12CMultiStream
	.globl	_ZTS12CMultiStream
_ZTS12CMultiStream:
	.asciz	"12CMultiStream"
	.size	_ZTS12CMultiStream, 15

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI12CMultiStream,@object # @_ZTI12CMultiStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI12CMultiStream
	.p2align	4
_ZTI12CMultiStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS12CMultiStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI12CMultiStream, 56

	.type	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	3
_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 40

	.type	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	4
_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.asciz	"13CObjectVectorIN12CMultiStream14CSubStreamInfoEE"
	.size	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 50

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	4
_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
