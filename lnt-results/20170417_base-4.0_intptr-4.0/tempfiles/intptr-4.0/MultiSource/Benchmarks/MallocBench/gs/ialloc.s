	.text
	.file	"ialloc.bc"
	.globl	alloc_init
	.p2align	4, 0x90
	.type	alloc_init,@function
alloc_init:                             # @alloc_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$as_current, %edi
	xorl	%esi, %esi
	movl	$392, %edx              # imm = 0x188
	callq	memset
	movl	%ebx, as_current+56(%rip)
	movl	$8, as_current+60(%rip)
	movq	%r15, as_current+64(%rip)
	movq	%r14, as_current+72(%rip)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	alloc_init, .Lfunc_end0-alloc_init
	.cfi_endproc

	.globl	alloc_status
	.p2align	4, 0x90
	.type	alloc_status,@function
alloc_status:                           # @alloc_status
	.cfi_startproc
# BB#0:
	movq	as_current+8(%rip), %rax
	movq	as_current(%rip), %rcx
	movq	as_current+24(%rip), %rdx
	subq	%rcx, %rax
	addq	%rdx, %rax
	subq	as_current+16(%rip), %rax
	addq	as_current+80(%rip), %rax
	movq	%rax, (%rdi)
	subq	%rcx, %rdx
	addq	as_current+88(%rip), %rdx
	movq	%rdx, (%rsi)
	retq
.Lfunc_end1:
	.size	alloc_status, .Lfunc_end1-alloc_status
	.cfi_endproc

	.globl	alloc
	.p2align	4, 0x90
	.type	alloc,@function
alloc:                                  # @alloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %ebx
	imull	%r14d, %ebx
	cmpl	as_current+60(%rip), %ebx
	jb	.LBB2_3
# BB#1:
	leal	24(%rbx), %esi
	movl	$1, %edi
	callq	*as_current+64(%rip)
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB2_3
# BB#2:                                 # %alloc_large.exit
	movq	%rcx, %rax
	addq	$24, %rax
	movq	as_current+360(%rip), %rdx
	movq	%rdx, (%rcx)
	movl	%ebx, 8(%rcx)
	movl	as_current+376(%rip), %edx
	movl	%edx, 12(%rcx)
	movq	$as_current, 16(%rcx)
	movq	%rcx, as_current+360(%rip)
	jmp	.LBB2_16
.LBB2_3:                                # %alloc_large.exit.thread
	leal	7(%rbx), %ecx
	movl	%ecx, %ebp
	andl	$-8, %ebp
	cmpl	$255, %ebp
	ja	.LBB2_6
# BB#4:
	shrl	$3, %ecx
	movq	as_current+104(,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB2_6
# BB#5:
	movq	(%rax), %rdx
	movq	%rdx, as_current+104(,%rcx,8)
	jmp	.LBB2_16
.LBB2_6:                                # %.thread
	movq	as_current+16(%rip), %rdx
	movq	as_current+8(%rip), %rax
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	%ecx, %ebp
	jbe	.LBB2_13
# BB#7:
	movl	as_current+56(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.5, %edx
	callq	*as_current+64(%rip)
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB2_8
# BB#9:
	movq	as_current+8(%rip), %rax
	movq	as_current(%rip), %rdx
	movq	as_current+24(%rip), %rsi
	subq	%rdx, %rax
	addq	%rsi, %rax
	subq	as_current+16(%rip), %rax
	addq	%rax, as_current+80(%rip)
	subq	%rdx, %rsi
	addq	%rsi, as_current+88(%rip)
	incl	as_current+96(%rip)
	movq	as_current+48(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_10
# BB#11:
	movups	as_current+32(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movups	as_current+16(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movups	as_current(%rip), %xmm0
	movups	%xmm0, (%rax)
	movq	as_current+48(%rip), %rsi
	jmp	.LBB2_12
.LBB2_8:
	xorl	%eax, %eax
	jmp	.LBB2_16
.LBB2_10:
	xorl	%esi, %esi
.LBB2_12:                               # %alloc_add_chunk.exit
	leaq	48(%rcx), %rax
	movq	%rax, as_current+8(%rip)
	movq	%rax, as_current(%rip)
	movl	as_current+56(%rip), %edx
	addq	%rcx, %rdx
	movq	%rdx, as_current+16(%rip)
	movq	%rdx, as_current+24(%rip)
	movq	%rsi, as_current+40(%rip)
	movl	as_current+376(%rip), %esi
	movl	%esi, as_current+32(%rip)
	movq	%rcx, as_current+48(%rip)
.LBB2_13:
	cmpl	$1, %r14d
	jne	.LBB2_15
# BB#14:
	movl	%ebx, %eax
	subq	%rax, %rdx
	movq	%rdx, as_current+16(%rip)
	movq	%rdx, %rax
	jmp	.LBB2_16
.LBB2_15:
	movl	%ebp, %ecx
	addq	%rax, %rcx
	movq	%rcx, as_current+8(%rip)
.LBB2_16:                               # %alloc_add_chunk.exit.thread
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	alloc, .Lfunc_end2-alloc
	.cfi_endproc

	.globl	alloc_large
	.p2align	4, 0x90
	.type	alloc_large,@function
alloc_large:                            # @alloc_large
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	leal	24(%r14), %esi
	movl	$1, %edi
	callq	*64(%rbx)
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB3_1
# BB#2:
	movq	%rcx, %rax
	addq	$24, %rax
	movq	360(%rbx), %rdx
	movq	%rdx, (%rcx)
	movl	%r14d, 8(%rcx)
	movl	376(%rbx), %edx
	movl	%edx, 12(%rcx)
	movq	%rbx, 16(%rcx)
	movq	%rcx, 360(%rbx)
	jmp	.LBB3_3
.LBB3_1:
	xorl	%eax, %eax
.LBB3_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	alloc_large, .Lfunc_end3-alloc_large
	.cfi_endproc

	.globl	alloc_add_chunk
	.p2align	4, 0x90
	.type	alloc_add_chunk,@function
alloc_add_chunk:                        # @alloc_add_chunk
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	56(%rbx), %esi
	movl	$1, %edi
	movl	$.L.str.5, %edx
	callq	*64(%rbx)
	testq	%rax, %rax
	je	.LBB4_1
# BB#2:
	movq	as_current+8(%rip), %rcx
	movq	as_current(%rip), %rdx
	movq	as_current+24(%rip), %rsi
	subq	%rdx, %rcx
	addq	%rsi, %rcx
	subq	as_current+16(%rip), %rcx
	addq	as_current+80(%rip), %rcx
	movq	%rcx, 80(%rbx)
	subq	%rdx, %rsi
	addq	as_current+88(%rip), %rsi
	movq	%rsi, 88(%rbx)
	incl	96(%rbx)
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_4
# BB#3:
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 32(%rcx)
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	48(%rbx), %rcx
.LBB4_4:
	leaq	48(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	%rdx, (%rbx)
	movl	56(%rbx), %edx
	addq	%rax, %rdx
	movq	%rdx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rcx, 40(%rbx)
	movl	376(%rbx), %ecx
	movl	%ecx, 32(%rbx)
	movq	%rax, 48(%rbx)
	movl	$1, %eax
	popq	%rbx
	retq
.LBB4_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	alloc_add_chunk, .Lfunc_end4-alloc_add_chunk
	.cfi_endproc

	.globl	alloc_free
	.p2align	4, 0x90
	.type	alloc_free,@function
alloc_free:                             # @alloc_free
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	imull	%edx, %eax
	cmpl	as_current+60(%rip), %eax
	jae	.LBB5_1
# BB#6:
	cmpq	%rdi, as_current+16(%rip)
	je	.LBB5_7
# BB#11:
	addl	$7, %eax
	andl	$-8, %eax
	leaq	(%rdi,%rax), %rdx
	movq	as_current+8(%rip), %rcx
	cmpq	%rcx, %rdx
	je	.LBB5_12
# BB#16:
	cmpq	%rdi, as_current(%rip)
	ja	.LBB5_18
# BB#17:
	cmpq	%rdi, as_current+24(%rip)
	jbe	.LBB5_18
# BB#29:
	leal	-8(%rax), %edx
	cmpl	$247, %edx
	ja	.LBB5_32
# BB#30:
	cmpq	%rdi, %rcx
	jbe	.LBB5_32
.LBB5_31:
	movq	as_current+104(%rax), %rcx
	movq	%rcx, (%rdi)
	movq	%rdi, as_current+104(%rax)
	retq
.LBB5_1:
	movq	-8(%rdi), %r8
	movl	-12(%rdi), %edx
	cmpl	376(%r8), %edx
	jne	.LBB5_32
# BB#2:
	movq	360(%r8), %rdx
	testq	%rdx, %rdx
	je	.LBB5_32
# BB#3:                                 # %.lr.ph.i.preheader
	leaq	360(%r8), %rsi
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rdx), %rcx
	cmpq	%rdi, %rcx
	je	.LBB5_33
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_4
	jmp	.LBB5_32
.LBB5_7:
	movl	as_current+376(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB5_10
# BB#8:
	cmpl	%ecx, as_current+32(%rip)
	jge	.LBB5_10
# BB#9:
	movq	as_current+368(%rip), %rcx
	cmpq	%rdi, 16(%rcx)
	jbe	.LBB5_32
.LBB5_10:
	movl	%eax, %eax
	addq	%rax, %rdi
	movq	%rdi, as_current+16(%rip)
	retq
.LBB5_12:
	movl	as_current+376(%rip), %eax
	testl	%eax, %eax
	je	.LBB5_15
# BB#13:
	cmpl	%eax, as_current+32(%rip)
	jge	.LBB5_15
# BB#14:
	movq	as_current+368(%rip), %rax
	cmpq	%rdi, 8(%rax)
	ja	.LBB5_32
.LBB5_15:
	movq	%rdi, as_current+8(%rip)
	retq
.LBB5_18:
	movq	as_current+40(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB5_32
# BB#19:                                # %.lr.ph.preheader
	movl	as_current+376(%rip), %edx
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	32(%rcx), %esi
	subl	%edx, %esi
	jne	.LBB5_21
# BB#24:                                #   in Loop: Header=BB5_20 Depth=1
	cmpq	%rdi, (%rcx)
	ja	.LBB5_28
# BB#25:                                #   in Loop: Header=BB5_20 Depth=1
	cmpq	%rdi, 24(%rcx)
	ja	.LBB5_26
.LBB5_28:                               #   in Loop: Header=BB5_20 Depth=1
	movq	40(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_20
	jmp	.LBB5_32
.LBB5_33:
	movq	(%rdx), %rcx
	movq	%rcx, (%rsi)
	movq	72(%r8), %r8
	addl	$24, %eax
	movl	$1, %esi
	movl	$.L.str.6, %ecx
	movq	%rdx, %rdi
	movl	%eax, %edx
	jmpq	*%r8                    # TAILCALL
.LBB5_21:                               # %.lr.ph
	cmpl	$-1, %esi
	jne	.LBB5_32
# BB#22:
	leal	-8(%rax), %ecx
	cmpl	$247, %ecx
	ja	.LBB5_32
# BB#23:
	movq	as_current+368(%rip), %rcx
	jmp	.LBB5_27
.LBB5_26:
	leal	-8(%rax), %edx
	cmpl	$247, %edx
	ja	.LBB5_32
.LBB5_27:
	cmpq	%rdi, 8(%rcx)
	ja	.LBB5_31
.LBB5_32:                               # %alloc_free_large.exit
	retq
.Lfunc_end5:
	.size	alloc_free, .Lfunc_end5-alloc_free
	.cfi_endproc

	.globl	alloc_free_large
	.p2align	4, 0x90
	.type	alloc_free_large,@function
alloc_free_large:                       # @alloc_free_large
	.cfi_startproc
# BB#0:
	movl	%esi, %r8d
	movq	%rdi, %rcx
	movq	-8(%rcx), %rdx
	movl	-12(%rcx), %esi
	cmpl	376(%rdx), %esi
	jne	.LBB6_5
# BB#1:
	movq	360(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#2:                                 # %.lr.ph.preheader
	leaq	360(%rdx), %rsi
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rdi), %rax
	cmpq	%rcx, %rax
	je	.LBB6_6
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	%rdi, %rsi
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_3
.LBB6_5:                                # %.loopexit
	retq
.LBB6_6:
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	72(%rdx), %rax
	addl	$24, %r8d
	movl	$1, %esi
	movl	$.L.str.6, %ecx
	movl	%r8d, %edx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end6:
	.size	alloc_free_large, .Lfunc_end6-alloc_free_large
	.cfi_endproc

	.globl	alloc_grow
	.p2align	4, 0x90
	.type	alloc_grow,@function
alloc_grow:                             # @alloc_grow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %ebp
	movl	%esi, %r15d
	movq	%rdi, %r12
	movl	%ebp, %ebx
	imull	%r15d, %ebx
	movl	%ebp, %edi
	imull	%edx, %edi
	movl	%edi, %eax
	subl	%ebx, %eax
	je	.LBB7_12
# BB#1:
	cmpl	as_current+60(%rip), %edi
	jae	.LBB7_9
# BB#2:
	movq	as_current+16(%rip), %r8
	cmpq	%r12, %r8
	je	.LBB7_7
# BB#3:                                 # %..thread_crit_edge
	movq	as_current+8(%rip), %rsi
	jmp	.LBB7_4
.LBB7_7:
	movq	as_current+8(%rip), %rsi
	movq	%r12, %rcx
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	jle	.LBB7_8
.LBB7_4:                                # %.thread
	leal	7(%rbx), %eax
	andl	$-8, %eax
	leaq	(%r12,%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_9
# BB#5:
	addl	$7, %edi
	andl	$-8, %edi
	subl	%eax, %edi
	subq	%rsi, %r8
	cmpq	%r8, %rdi
	jle	.LBB7_6
.LBB7_9:                                # %.thread73
	movl	%edx, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	alloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_10
# BB#11:
	movl	%ebx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movq	%r12, %rdi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	alloc_free
	movq	%r13, %r12
	jmp	.LBB7_12
.LBB7_10:
	xorl	%r12d, %r12d
	jmp	.LBB7_12
.LBB7_8:
	movq	%r12, %rdi
	subq	%rax, %rdi
	movq	%rdi, as_current+16(%rip)
	movl	%ebx, %edx
	movq	%r12, %rsi
	callq	memcpy
	movq	as_current+16(%rip), %r12
	jmp	.LBB7_12
.LBB7_6:
	addq	%rdi, %rsi
	movq	%rsi, as_current+8(%rip)
.LBB7_12:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	alloc_grow, .Lfunc_end7-alloc_grow
	.cfi_endproc

	.globl	alloc_shrink
	.p2align	4, 0x90
	.type	alloc_shrink,@function
alloc_shrink:                           # @alloc_shrink
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %ebp
	movl	%edx, %eax
	movl	%esi, %r15d
	movq	%rdi, %r13
	movl	%ebp, %edx
	imull	%r15d, %edx
	movl	%ebp, %ebx
	imull	%eax, %ebx
	cmpl	%edx, %ebx
	je	.LBB8_10
# BB#1:
	cmpl	as_current+60(%rip), %edx
	jae	.LBB8_2
# BB#4:
	cmpq	%r13, as_current+16(%rip)
	je	.LBB8_5
# BB#9:
	addl	$7, %ebx
	andl	$-8, %ebx
	leaq	(%r13,%rbx), %rdi
	addl	$7, %edx
	andl	$-8, %edx
	subl	%ebx, %edx
	movl	$1, %esi
	movl	$.L.str, %ecx
	callq	alloc_free
	jmp	.LBB8_10
.LBB8_2:
	movl	%eax, %edi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	alloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB8_10
# BB#3:
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memcpy
	movq	%r13, %rdi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	alloc_free
	movq	%r12, %r13
	jmp	.LBB8_10
.LBB8_5:
	movl	%edx, %eax
	addq	%r13, %rax
	testl	%ebx, %ebx
	je	.LBB8_8
# BB#6:                                 # %.lr.ph.preheader
	movl	%ebx, %ecx
	addq	%r13, %rcx
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rcx), %edx
	decq	%rcx
	movb	%dl, -1(%rax)
	decq	%rax
	cmpq	%r13, %rcx
	ja	.LBB8_7
.LBB8_8:                                # %._crit_edge
	movq	%rax, as_current+16(%rip)
	movq	%rax, %r13
.LBB8_10:
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	alloc_shrink, .Lfunc_end8-alloc_shrink
	.cfi_endproc

	.globl	alloc_save_state
	.p2align	4, 0x90
	.type	alloc_save_state,@function
alloc_save_state:                       # @alloc_save_state
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$400, %esi              # imm = 0x190
	movl	$.L.str.1, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB9_1
# BB#2:
	movl	$as_current, %esi
	movl	$392, %edx              # imm = 0x188
	movq	%rbx, %rdi
	callq	memcpy
	movl	$as_current+104, %edi
	xorl	%esi, %esi
	movl	$264, %edx              # imm = 0x108
	callq	memset
	movq	%rbx, as_current+368(%rip)
	incl	as_current+376(%rip)
	movq	$0, as_current+384(%rip)
	jmp	.LBB9_3
.LBB9_1:
	xorl	%ebx, %ebx
.LBB9_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	alloc_save_state, .Lfunc_end9-alloc_save_state
	.cfi_endproc

	.globl	alloc_save_change
	.p2align	4, 0x90
	.type	alloc_save_change,@function
alloc_save_change:                      # @alloc_save_change
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 48
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	cmpl	$0, as_current+376(%rip)
	je	.LBB10_4
# BB#1:
	leal	24(%rbx), %esi
	movl	$1, %edi
	movl	$.L.str.2, %edx
	callq	alloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB10_2
# BB#3:
	movl	%ebx, %edx
	movq	as_current+384(%rip), %rax
	movq	%rax, (%rbp)
	movq	%r14, 8(%rbp)
	movl	%ebx, 16(%rbp)
	movq	%rbp, %rdi
	addq	$24, %rdi
	movq	%r14, %rsi
	callq	memcpy
	movq	%rbp, as_current+384(%rip)
	jmp	.LBB10_4
.LBB10_2:
	movl	$-1, %r15d
.LBB10_4:
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	alloc_save_change, .Lfunc_end10-alloc_save_change
	.cfi_endproc

	.globl	alloc_save_level
	.p2align	4, 0x90
	.type	alloc_save_level,@function
alloc_save_level:                       # @alloc_save_level
	.cfi_startproc
# BB#0:
	movl	as_current+376(%rip), %eax
	retq
.Lfunc_end11:
	.size	alloc_save_level, .Lfunc_end11-alloc_save_level
	.cfi_endproc

	.globl	alloc_is_since_save
	.p2align	4, 0x90
	.type	alloc_is_since_save,@function
alloc_is_since_save:                    # @alloc_is_since_save
	.cfi_startproc
# BB#0:
	movq	392(%rsi), %r8
	cmpq	%rdi, (%rsi)
	ja	.LBB12_6
# BB#1:
	cmpq	%rdi, 24(%rsi)
	jbe	.LBB12_6
# BB#2:
	cmpq	%rdi, 8(%rsi)
	jbe	.LBB12_4
# BB#3:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB12_6:
	movl	376(%rsi), %eax
	cmpl	%eax, 32(%r8)
	jle	.LBB12_11
# BB#7:                                 # %.lr.ph53.preheader
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB12_8:                               # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, (%rdx)
	ja	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	cmpq	%rdi, 24(%rdx)
	ja	.LBB12_17
.LBB12_10:                              #   in Loop: Header=BB12_8 Depth=1
	movq	40(%rdx), %rdx
	cmpl	%eax, 32(%rdx)
	jg	.LBB12_8
.LBB12_11:                              # %.preheader
	xorl	%eax, %eax
	cmpq	%rsi, %r8
	jne	.LBB12_13
	jmp	.LBB12_18
	.p2align	4, 0x90
.LBB12_19:                              # %._crit_edge
                                        #   in Loop: Header=BB12_13 Depth=1
	movq	368(%r8), %r8
	cmpq	%rsi, %r8
	je	.LBB12_18
.LBB12_13:                              # %.lr.ph50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_16 Depth 2
	movq	360(%r8), %rdx
	testq	%rdx, %rdx
	jne	.LBB12_16
	jmp	.LBB12_19
	.p2align	4, 0x90
.LBB12_14:                              #   in Loop: Header=BB12_16 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB12_19
.LBB12_16:                              # %.lr.ph
                                        #   Parent Loop BB12_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	24(%rdx), %rcx
	cmpq	%rdi, %rcx
	jne	.LBB12_14
.LBB12_17:
	movl	$1, %eax
.LBB12_18:                              # %.critedge
	retq
.LBB12_4:
	cmpq	16(%rsi), %rdi
	setb	%al
	movzbl	%al, %eax
	retq
.Lfunc_end12:
	.size	alloc_is_since_save, .Lfunc_end12-alloc_is_since_save
	.cfi_endproc

	.globl	alloc_restore_state_check
	.p2align	4, 0x90
	.type	alloc_restore_state_check,@function
alloc_restore_state_check:              # @alloc_restore_state_check
	.cfi_startproc
# BB#0:
	movq	392(%rdi), %rax
	movq	368(%rax), %rcx
	xorl	%eax, %eax
	cmpq	%rdi, %rcx
	jne	.LBB13_3
	jmp	.LBB13_5
	.p2align	4, 0x90
.LBB13_1:                               #   in Loop: Header=BB13_3 Depth=1
	movq	368(%rcx), %rcx
	cmpq	%rdi, %rcx
	je	.LBB13_5
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jne	.LBB13_1
# BB#4:
	movl	$-1, %eax
.LBB13_5:                               # %._crit_edge
	retq
.Lfunc_end13:
	.size	alloc_restore_state_check, .Lfunc_end13-alloc_restore_state_check
	.cfi_endproc

	.globl	alloc_restore_state
	.p2align	4, 0x90
	.type	alloc_restore_state,@function
alloc_restore_state:                    # @alloc_restore_state
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r12, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	392(%r14), %r12
	.p2align	4, 0x90
.LBB14_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #     Child Loop BB14_5 Depth 2
	movq	48(%r12), %rax
	movq	368(%r12), %r15
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	32(%r12), %xmm2
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.LBB14_3
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph
                                        #   in Loop: Header=BB14_3 Depth=2
	movq	(%rdi), %rax
	movq	%rax, 360(%r12)
	movl	8(%rdi), %edx
	addl	$24, %edx
	movl	$1, %esi
	movl	$.L.str.3, %ecx
	callq	*72(%r12)
.LBB14_3:                               # %.lr.ph
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	jne	.LBB14_2
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	384(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB14_7
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph35
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rsi
	movl	16(%rbx), %edx
	callq	memcpy
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_5
.LBB14_7:                               # %._crit_edge36
                                        #   in Loop: Header=BB14_1 Depth=1
	movl	$392, %edx              # imm = 0x188
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	$1, %esi
	movl	$400, %edx              # imm = 0x190
	movl	$.L.str.4, %ecx
	movq	%r15, %rdi
	callq	alloc_free
	cmpq	%r14, %r15
	jne	.LBB14_1
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	alloc_restore_state, .Lfunc_end14-alloc_restore_state
	.cfi_endproc

	.type	as_current,@object      # @as_current
	.comm	as_current,392,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloc_shrink"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"alloc_save_state"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"alloc_save_change"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"alloc_restore_state(malloc'ed)"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"alloc_restore_state"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"alloc chunk"
	.size	.L.str.5, 12

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"large object"
	.size	.L.str.6, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
