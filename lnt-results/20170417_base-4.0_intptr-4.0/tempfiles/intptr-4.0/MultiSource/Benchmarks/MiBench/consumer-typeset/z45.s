	.text
	.file	"z45.bc"
	.globl	ReadLines
	.p2align	4, 0x90
	.type	ReadLines,@function
ReadLines:                              # @ReadLines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	(%rsp), %r9             # 8-byte Reload
	callq	Error
.LBB0_2:
	movl	$16000, %edi            # imm = 0x3E80
	callq	malloc
	movq	%rax, %r13
	testq	%rbp, %rbp
	movq	%r13, 8(%rsp)           # 8-byte Spill
	je	.LBB0_4
# BB#3:
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, (%r13)
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	leaq	8(%r13), %r13
.LBB0_4:
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%r12, (%r13)
	addq	$8, %r13
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	movq	%r15, 48(%rsp)          # 8-byte Spill
	je	.LBB0_39
# BB#5:                                 # %.lr.ph100.preheader
	movq	%r12, %r15
	addq	$4096, %r15             # imm = 0x1000
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$16000, %rax            # imm = 0x3E80
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$2000, %eax             # imm = 0x7D0
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_28 Depth 2
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_9
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	cmpl	8(%rsp), %r15d          # 4-byte Folded Reload
	jle	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_9:                                #   in Loop: Header=BB0_6 Depth=1
	cmpq	%r15, %rbx
	je	.LBB0_10
.LBB0_31:                               #   in Loop: Header=BB0_6 Depth=1
	cmpl	$10, %r14d
	je	.LBB0_32
.LBB0_38:                               #   in Loop: Header=BB0_6 Depth=1
	movb	%r14b, (%rbx)
	incq	%rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	jne	.LBB0_6
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_6 Depth=1
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_6 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r9
	callq	Error
.LBB0_12:                               #   in Loop: Header=BB0_6 Depth=1
	leaq	4096(%r12), %r15
	movq	-8(%r13), %rcx
	cmpq	%rbx, %rcx
	je	.LBB0_13
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rbx, %r8
	subq	%rcx, %r8
	cmpq	$32, %r8
	jb	.LBB0_15
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%r8, %rdx
	andq	$-32, %rdx
	je	.LBB0_15
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_6 Depth=1
	cmpq	%rbx, %r12
	jae	.LBB0_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_6 Depth=1
	leaq	(%r12,%r8), %rax
	cmpq	%rax, %rcx
	jae	.LBB0_19
.LBB0_15:                               #   in Loop: Header=BB0_6 Depth=1
	movq	%r12, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %eax
	incq	%rcx
	movb	%al, (%rsi)
	leaq	1(%r12,%rdx), %rsi
	incq	%rdx
	cmpq	%rcx, %rbx
	jne	.LBB0_28
.LBB0_29:                               # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%r12, %rbx
	addq	%r8, %rbx
	movb	$0, (%r12,%r8)
	movq	%r12, -8(%r13)
	cmpq	$4096, %r8              # imm = 0x1000
	jne	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_6 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$3, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r9
	callq	Error
	cmpl	$10, %r14d
	jne	.LBB0_38
	jmp	.LBB0_32
.LBB0_13:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB0_6 Depth=1
	movb	$0, (%r12)
	movq	%r12, -8(%r13)
	movq	%r12, %rbx
	cmpl	$10, %r14d
	jne	.LBB0_38
.LBB0_32:                               #   in Loop: Header=BB0_6 Depth=1
	movb	$0, (%rbx)
	incq	%rbx
	cmpq	24(%rsp), %r13          # 8-byte Folded Reload
	je	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	jmp	.LBB0_37
.LBB0_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	leaq	-32(%rdx), %rsi
	movl	%esi, %eax
	shrl	$5, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB0_20
# BB#21:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	negq	%rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_22:                               # %vector.body.prol
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rbp), %xmm0
	movups	16(%rcx,%rbp), %xmm1
	movups	%xmm0, (%r12,%rbp)
	movups	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rax
	jne	.LBB0_22
	jmp	.LBB0_23
.LBB0_20:                               #   in Loop: Header=BB0_6 Depth=1
	xorl	%ebp, %ebp
.LBB0_23:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	cmpq	$96, %rsi
	jb	.LBB0_26
# BB#24:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rcx,%rbp), %rdi
	leaq	112(%r12,%rbp), %rax
	.p2align	4, 0x90
.LBB0_25:                               # %vector.body
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	subq	$-128, %rdi
	subq	$-128, %rax
	addq	$-128, %rsi
	jne	.LBB0_25
.LBB0_26:                               # %middle.block
                                        #   in Loop: Header=BB0_6 Depth=1
	cmpq	%rdx, %r8
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB0_29
# BB#27:                                #   in Loop: Header=BB0_6 Depth=1
	leaq	(%r12,%rdx), %rsi
	addq	%rdx, %rcx
	jmp	.LBB0_28
.LBB0_34:                               #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	leal	(%r13,%r13), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movslq	%eax, %r14
	leaq	(,%r14,8), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_6 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$4, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	(%rsp), %r9             # 8-byte Reload
	callq	Error
.LBB0_36:                               #   in Loop: Header=BB0_6 Depth=1
	movslq	%r13d, %rax
	leaq	(%rbp,%rax,8), %r13
	movq	%rbp, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	(%rbp,%r14,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	44(%rsp), %ebp          # 4-byte Reload
.LBB0_37:                               # %.outer
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rbx, (%r13)
	addq	$8, %r13
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	movl	%ebp, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB0_6
.LBB0_39:                               # %.outer._crit_edge
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rcx, %r13
	shrq	$3, %r13
	decl	%r13d
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movq	%rcx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ReadLines, .Lfunc_end0-ReadLines
	.cfi_endproc

	.globl	WriteLines
	.p2align	4, 0x90
	.type	WriteLines,@function
WriteLines:                             # @WriteLines
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testl	%edx, %edx
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r15d
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	fputs
	movl	$10, %edi
	movq	%r14, %rsi
	callq	fputc
	addq	$8, %rbx
	decq	%r15
	jne	.LBB1_2
.LBB1_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	WriteLines, .Lfunc_end1-WriteLines
	.cfi_endproc

	.globl	SortLines
	.p2align	4, 0x90
	.type	SortLines,@function
SortLines:                              # @SortLines
	.cfi_startproc
# BB#0:
	movslq	%esi, %rsi
	cmpl	$0, UseCollate(%rip)
	movl	$pstrcollcmp, %eax
	movl	$pstrcmp, %ecx
	cmovneq	%rax, %rcx
	movl	$8, %edx
	jmp	qsort                   # TAILCALL
.Lfunc_end2:
	.size	SortLines, .Lfunc_end2-SortLines
	.cfi_endproc

	.p2align	4, 0x90
	.type	pstrcollcmp,@function
pstrcollcmp:                            # @pstrcollcmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	jmp	strcollcmp              # TAILCALL
.Lfunc_end3:
	.size	pstrcollcmp, .Lfunc_end3-pstrcollcmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	pstrcmp,@function
pstrcmp:                                # @pstrcmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	jmp	strcmp                  # TAILCALL
.Lfunc_end4:
	.size	pstrcmp, .Lfunc_end4-pstrcmp
	.cfi_endproc

	.globl	SortFile
	.p2align	4, 0x90
	.type	SortFile,@function
SortFile:                               # @SortFile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r13, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$.L.str.5, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB5_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$5, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r9
	callq	Error
.LBB5_2:
	movl	$.L.str.7, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB5_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$45, %edi
	movl	$6, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r9
	callq	Error
.LBB5_4:
	leaq	12(%rsp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	ReadLines
	movq	%rax, %r12
	cmpl	$0, UseCollate(%rip)
	movslq	12(%rsp), %r15
	movl	$pstrcollcmp, %eax
	movl	$pstrcmp, %ecx
	cmovneq	%rax, %rcx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	qsort
	movq	%r14, %rdi
	callq	fclose
	testq	%r15, %r15
	jle	.LBB5_7
# BB#5:                                 # %.lr.ph.preheader.i
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	%r13, %rsi
	callq	fputs
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
	addq	$8, %r12
	decq	%rbx
	jne	.LBB5_6
.LBB5_7:                                # %WriteLines.exit
	movq	%r13, %rdi
	callq	fclose
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	SortFile, .Lfunc_end5-SortFile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"run out of memory when reading index file %s"
	.size	.L.str, 45

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"assert failed in %s"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ReadLines: lines and buff overlap!"
	.size	.L.str.2, 35

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"line too long when reading index file %s"
	.size	.L.str.3, 41

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"r"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cannot open index file %s for reading"
	.size	.L.str.6, 38

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"w"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"cannot open index file %s for writing"
	.size	.L.str.8, 38


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
