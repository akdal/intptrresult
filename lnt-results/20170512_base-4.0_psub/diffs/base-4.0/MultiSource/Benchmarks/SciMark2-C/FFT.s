	.text
	.file	"FFT.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4617315517961601024     # double 5
.LCPI0_1:
	.quad	-4611686018427387904    # double -2
.LCPI0_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	FFT_num_flops
	.p2align	4, 0x90
	.type	FFT_num_flops,@function
FFT_num_flops:                          # @FFT_num_flops
	.cfi_startproc
# BB#0:
	movl	%edi, %edx
	xorl	%ecx, %ecx
	cmpl	$2, %edx
	jl	.LBB0_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	incl	%ecx
	cmpl	%edx, %eax
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge.i
	movl	$1, %eax
	shll	%cl, %eax
	cmpl	%edx, %eax
	jne	.LBB0_5
# BB#4:                                 # %int_log2.exit
	cvtsi2sdl	%edx, %xmm0
	cvtsi2sdl	%ecx, %xmm1
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	addsd	.LCPI0_1(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	addsd	.LCPI0_2(%rip), %xmm0
	addsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	retq
.LBB0_5:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	FFT_num_flops, .Lfunc_end0-FFT_num_flops
	.cfi_endproc

	.globl	FFT_bitreverse
	.p2align	4, 0x90
	.type	FFT_bitreverse,@function
FFT_bitreverse:                         # @FFT_bitreverse
	.cfi_startproc
# BB#0:
	cmpl	$4, %edi
	jl	.LBB1_7
# BB#1:                                 # %.lr.ph57
	movl	%edi, %r9d
	shrl	$31, %r9d
	addl	%edi, %r9d
	sarl	%r9d
	leal	-1(%r9), %ecx
	sarl	%r9d
	movslq	%ecx, %r8
	xorl	%edx, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	movslq	%edi, %rcx
	cmpq	%rcx, %rdx
	jge	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	leaq	(%rdx,%rdx), %rcx
	leal	(%rdi,%rdi), %eax
	movups	(%rsi,%rcx,8), %xmm0
	movslq	%eax, %r10
	movq	(%rsi,%r10,8), %rax
	movq	%rax, (%rsi,%rcx,8)
	orq	$1, %rcx
	movl	%r10d, %eax
	orl	$1, %eax
	cltq
	movq	(%rsi,%rax,8), %rax
	movq	%rax, (%rsi,%rcx,8)
	movups	%xmm0, (%rsi,%r10,8)
.LBB1_4:                                # %.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%r9d, %ecx
	cmpl	%r9d, %edi
	jl	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%ecx, %edi
	sarl	%ecx
	cmpl	%ecx, %edi
	jge	.LBB1_5
.LBB1_6:                                # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	addl	%ecx, %edi
	incq	%rdx
	cmpq	%r8, %rdx
	jl	.LBB1_2
.LBB1_7:                                # %._crit_edge58
	retq
.Lfunc_end1:
	.size	FFT_bitreverse, .Lfunc_end1-FFT_bitreverse
	.cfi_endproc

	.globl	FFT_transform
	.p2align	4, 0x90
	.type	FFT_transform,@function
FFT_transform:                          # @FFT_transform
	.cfi_startproc
# BB#0:
	movl	$-1, %edx
	jmp	FFT_transform_internal  # TAILCALL
.Lfunc_end2:
	.size	FFT_transform, .Lfunc_end2-FFT_transform
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI3_1:
	.quad	4602678819172646912     # double 0.5
.LCPI3_2:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	FFT_transform_internal,@function
FFT_transform_internal:                 # @FFT_transform_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 128
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%edi, %eax
	andl	$-2, %eax
	cmpl	$2, %eax
	je	.LBB3_24
# BB#1:
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	xorl	%ecx, %ecx
	cmpl	$4, %eax
	jl	.LBB3_4
# BB#2:                                 # %.lr.ph.i.preheader
	movl	$1, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	incl	%ecx
	cmpl	%esi, %eax
	jl	.LBB3_3
.LBB3_4:                                # %._crit_edge.i
	movl	$1, %eax
	shll	%cl, %eax
	cmpl	%esi, %eax
	jne	.LBB3_25
# BB#5:                                 # %int_log2.exit
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB3_24
# BB#6:
	cmpl	$4, 4(%rsp)             # 4-byte Folded Reload
	jl	.LBB3_13
# BB#7:                                 # %.lr.ph57.i
	leal	-1(%rsi), %eax
	movl	%esi, %r9d
	sarl	%r9d
	movslq	%eax, %r8
	xorl	%r11d, %r11d
	xorl	%ebp, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
	movslq	%ebp, %rax
	cmpq	%rax, %r11
	jge	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	leaq	(%r11,%r11), %rax
	leal	(%rbp,%rbp), %ebx
	movupd	(%rdi,%rax,8), %xmm0
	movslq	%ebx, %r10
	movq	(%rdi,%r10,8), %rbx
	movq	%rbx, (%rdi,%rax,8)
	orq	$1, %rax
	movl	%r10d, %ebx
	orl	$1, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi,%rbx,8), %rbx
	movq	%rbx, (%rdi,%rax,8)
	movupd	%xmm0, (%rdi,%r10,8)
.LBB3_10:                               # %.preheader.i
                                        #   in Loop: Header=BB3_8 Depth=1
	movl	%r9d, %eax
	cmpl	%r9d, %ebp
	jl	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.i146
                                        #   Parent Loop BB3_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subl	%eax, %ebp
	sarl	%eax
	cmpl	%eax, %ebp
	jge	.LBB3_11
.LBB3_12:                               # %._crit_edge.i147
                                        #   in Loop: Header=BB3_8 Depth=1
	addl	%eax, %ebp
	incq	%r11
	cmpq	%r8, %r11
	jl	.LBB3_8
.LBB3_13:                               # %FFT_bitreverse.exit.preheader
	testl	%ecx, %ecx
	jle	.LBB3_24
# BB#14:                                # %.lr.ph163
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	addsd	%xmm0, %xmm0
	mulsd	.LCPI3_0(%rip), %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movslq	%esi, %rbp
	cmpl	$2, 4(%rsp)             # 4-byte Folded Reload
	setl	3(%rsp)                 # 1-byte Folded Spill
	movl	$1, %r12d
	xorl	%eax, %eax
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_17 Depth 2
                                        #     Child Loop BB3_20 Depth 2
                                        #       Child Loop BB3_21 Depth 3
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	addsd	%xmm1, %xmm1
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	sin
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI3_1(%rip), %xmm0
	callq	sin
	cmpl	$2, 4(%rsp)             # 4-byte Folded Reload
	jl	.LBB3_18
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB3_15 Depth=1
	leal	(%r12,%r12), %eax
	cltq
	leal	(,%r12,4), %ecx
	movslq	%ecx, %rcx
	shlq	$3, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_17:                               #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rdx,%rax,8), %xmm1
	movsd	(%rdx), %xmm2           # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdx,%rax,8)
	movsd	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movsd	%xmm2, 8(%rdx,%rax,8)
	movupd	(%rdx), %xmm2
	addpd	%xmm1, %xmm2
	movupd	%xmm2, (%rdx)
	addq	%rax, %rsi
	addq	%rcx, %rdx
	cmpq	%rbp, %rsi
	jl	.LBB3_17
.LBB3_18:                               # %.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	leal	(%r12,%r12), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpl	$2, %r12d
	setl	%al
	orb	3(%rsp), %al            # 1-byte Folded Reload
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	jne	.LBB3_23
# BB#19:                                # %.lr.ph157.split.us.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	movapd	%xmm0, %xmm1
	addsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leal	3(%rcx), %r9d
	leal	(,%r12,4), %eax
	movslq	%eax, %rsi
	shlq	$3, %rsi
	leal	2(%rcx), %r10d
	movl	$1, %r11d
	movl	$3, %ebx
	movl	$2, %edx
	movsd	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph157.split.us
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_21 Depth 3
	movslq	%r9d, %rdi
	movslq	%r10d, %rax
	movslq	%ebx, %r8
	movslq	%edx, %r15
	movapd	%xmm0, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	mulpd	%xmm7, %xmm2
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	addpd	%xmm0, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movapd	%xmm0, %xmm3
	mulpd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	subpd	%xmm3, %xmm0
	movapd	%xmm0, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movq	16(%rsp), %r14          # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_21:                               #   Parent Loop BB3_15 Depth=1
                                        #     Parent Loop BB3_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r14,%rax,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%r14,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movaps	%xmm2, %xmm6
	mulsd	%xmm4, %xmm6
	subsd	%xmm6, %xmm5
	mulsd	%xmm0, %xmm4
	mulsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	movsd	(%r14,%r15,8), %xmm4    # xmm4 = mem[0],zero
	subsd	%xmm5, %xmm4
	movsd	%xmm4, (%r14,%rax,8)
	movsd	(%r14,%r8,8), %xmm4     # xmm4 = mem[0],zero
	subsd	%xmm3, %xmm4
	movsd	%xmm4, (%r14,%rdi,8)
	addsd	(%r14,%r15,8), %xmm5
	movsd	%xmm5, (%r14,%r15,8)
	addsd	(%r14,%r8,8), %xmm3
	movsd	%xmm3, (%r14,%r8,8)
	addq	%rcx, %r13
	addq	%rsi, %r14
	cmpq	%rbp, %r13
	jl	.LBB3_21
# BB#22:                                # %._crit_edge.us
                                        #   in Loop: Header=BB3_20 Depth=2
	incl	%r11d
	addl	$2, %r9d
	addl	$2, %r10d
	addl	$2, %ebx
	addl	$2, %edx
	cmpl	%r12d, %r11d
	jne	.LBB3_20
.LBB3_23:                               # %FFT_bitreverse.exit
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %eax
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, %r12d
	jne	.LBB3_15
.LBB3_24:                               # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_25:
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	FFT_transform_internal, .Lfunc_end3-FFT_transform_internal
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	FFT_inverse
	.p2align	4, 0x90
	.type	FFT_inverse,@function
FFT_inverse:                            # @FFT_inverse
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	movl	$1, %edx
	callq	FFT_transform_internal
	testl	%ebx, %ebx
	jle	.LBB4_10
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
	cvtsi2sdl	%eax, %xmm1
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movl	%ebx, %eax
	cmpl	$4, %ebx
	jb	.LBB4_7
# BB#3:                                 # %min.iters.checked
	andl	$3, %ebx
	movq	%rax, %rcx
	subq	%rbx, %rcx
	je	.LBB4_7
# BB#4:                                 # %vector.ph
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%r14), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB4_5:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm2
	movupd	(%rdx), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rdx)
	movupd	%xmm3, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB4_5
# BB#6:                                 # %middle.block
	testl	%ebx, %ebx
	jne	.LBB4_8
	jmp	.LBB4_10
.LBB4_7:
	xorl	%ecx, %ecx
.LBB4_8:                                # %.lr.ph.preheader19
	leaq	(%r14,%rcx,8), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx)
	addq	$8, %rdx
	decq	%rax
	jne	.LBB4_9
.LBB4_10:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	FFT_inverse, .Lfunc_end4-FFT_inverse
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FFT: Data length is not a power of 2!: %d "
	.size	.L.str, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
