	.text
	.file	"mincov.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	sm_minimum_cover
	.p2align	4, 0x90
	.type	sm_minimum_cover,@function
sm_minimum_cover:                       # @sm_minimum_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	$0, 48(%r15)
	jle	.LBB0_1
# BB#2:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, 56(%rsp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	movl	%eax, 24(%rsp)
	movl	%ebx, 28(%rsp)
	movl	$-1, 32(%rsp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	setne	%al
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 36(%rsp)
	movl	$0, 52(%rsp)
	movl	%eax, 64(%rsp)
	movl	$-1, 68(%rsp)
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_5
# BB#3:                                 # %.lr.ph65.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	addl	4(%rax), %ebp
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_4
.LBB0_5:                                # %._crit_edge66
	movl	72(%r15), %eax
	imull	48(%r15), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	56(%r15), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_9
# BB#6:                                 # %.lr.ph
	testq	%r14, %r14
	je	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rax), %rcx
	addl	(%r14,%rcx,4), %ebx
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_7
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_8
.LBB0_9:                                # %._crit_edge
	xorl	%eax, %eax
	callq	solution_alloc
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sm_dup
	movq	%rax, %r13
	leaq	24(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movl	%ebx, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	sm_mincov
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	solution_free
	cmpl	$0, 24(%rsp)
	je	.LBB0_13
# BB#10:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	movl	16(%rsp), %eax          # 4-byte Reload
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	cmpl	$0, 64(%rsp)
	je	.LBB0_12
# BB#11:
	movl	$.Lstr, %edi
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	puts
	movl	68(%rsp), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_12:
	movl	48(%r15), %esi
	movl	72(%r15), %edx
	mulsd	.LCPI0_0(%rip), %xmm0
	movl	$.L.str.2, %edi
	movb	$1, %al
	movl	%ebp, %ecx
	callq	printf
	movq	(%r14), %rax
	movl	4(%rax), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%r14), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	56(%rsp), %rax
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %rcx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	44(%rsp), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	48(%rsp), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	36(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	32(%rsp), %esi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_13:
	movq	(%r14), %rdi
	xorl	%eax, %eax
	callq	sm_row_dup
	movq	%rax, %rbx
	movq	32(%r15), %rbp
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_14 Depth=1
	movq	32(%rbp), %rbp
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB0_15
# BB#18:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_14 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	sm_row_intersects
	testl	%eax, %eax
	jne	.LBB0_17
# BB#19:                                # %verify_cover.exit
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movl	$91, %ecx
	movl	$.L.str.12, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	callq	fflush
	callq	abort
.LBB0_15:                               # %.loopexit
	movq	%r14, %rdi
	callq	solution_free
	jmp	.LBB0_16
.LBB0_1:
	xorl	%eax, %eax
	callq	sm_row_alloc
	movq	%rax, %rbx
.LBB0_16:
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sm_minimum_cover, .Lfunc_end0-sm_minimum_cover
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	-4616189618054758400    # double -1
.LCPI1_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	sm_mincov
	.p2align	4, 0x90
	.type	sm_mincov,@function
sm_mincov:                              # @sm_mincov
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	144(%rsp), %rax
	incl	12(%rax)
	cmpl	%ebp, 8(%rax)
	jge	.LBB1_2
# BB#1:
	movl	%ebp, 8(%rax)
.LBB1_2:
	cmpl	$0, (%rax)
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	je	.LBB1_4
# BB#3:
	cmpl	%ebp, 4(%rax)
	setge	%al
	movl	%eax, 32(%rsp)          # 4-byte Spill
	jmp	.LBB1_5
.LBB1_4:
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
.LBB1_5:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
.LBB1_6:                                # %.critedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
                                        #     Child Loop BB1_12 Depth 2
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rdx, %rsi
	callq	sm_col_dominance
	movl	%eax, %r13d
	xorl	%eax, %eax
	callq	sm_row_alloc
	movq	%rax, %r14
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, 4(%rbx)
	jne	.LBB1_7
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	movq	16(%rbx), %rax
	movl	4(%rax), %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_insert
.LBB1_7:                                #   in Loop: Header=BB1_8 Depth=2
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_8
.LBB1_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph44.i
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx), %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	solution_accept
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, 8(%r15)
	jge	.LBB1_16
# BB#11:                                #   in Loop: Header=BB1_12 Depth=2
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_12
.LBB1_13:                               # %._crit_edge45.i
                                        #   in Loop: Header=BB1_6 Depth=1
	movl	4(%r14), %ebx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_row_dominance
	cmpl	$0, %ebx
	movq	24(%rsp), %rdx          # 8-byte Reload
	jg	.LBB1_6
# BB#14:                                # %._crit_edge45.i
                                        #   in Loop: Header=BB1_6 Depth=1
	testl	%r13d, %r13d
	jg	.LBB1_6
# BB#15:                                # %._crit_edge45.i
                                        #   in Loop: Header=BB1_6 Depth=1
	testl	%eax, %eax
	jg	.LBB1_6
	jmp	.LBB1_17
.LBB1_16:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_free
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB1_17:                               # %select_essential.exit
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	%r8d, 8(%r15)
	jge	.LBB1_24
# BB#18:
	testq	%rdx, %rdx
	movl	16(%rsp), %ebx          # 4-byte Reload
	je	.LBB1_25
.LBB1_19:
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rdx, %rsi
	callq	sm_maximal_independent_set
	movq	%rax, %r13
	movl	8(%r13), %eax
	addl	8(%r15), %eax
	cmpl	%ebx, %eax
	cmovll	%ebx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	callq	sm_row_alloc
	movq	%rax, %r14
	testq	%r13, %r13
	je	.LBB1_26
# BB#20:
	movq	(%r13), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_28
	.p2align	4, 0x90
.LBB1_22:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_23 Depth 2
	movq	(%r12), %rax
	movslq	4(%rbx), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB1_21
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph88.i
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbp), %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_insert
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_23
.LBB1_21:                               # %._crit_edge89.i
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_22
	jmp	.LBB1_28
.LBB1_24:
	xorl	%r14d, %r14d
	jmp	.LBB1_88
.LBB1_25:
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	144(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$0, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	%ebx, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebp, %r9d
	callq	gimpel_reduce
	movq	24(%rsp), %rdx          # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB1_87
	jmp	.LBB1_19
.LBB1_26:
	movq	56(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_28
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph84.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_insert
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_27
.LBB1_28:                               # %.loopexit.i
	movq	16(%r14), %rax
	testq	%rax, %rax
	movq	%r13, 56(%rsp)          # 8-byte Spill
	je	.LBB1_41
# BB#29:                                # %.lr.ph78.i
	movq	24(%rsp), %rbp          # 8-byte Reload
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$-1, %r13d
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	testq	%rbp, %rbp
	je	.LBB1_42
# BB#30:                                # %.lr.ph78.split.i.preheader
	movl	16(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph78.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_38 Depth 2
	movslq	4(%rax), %rcx
	testq	%rcx, %rcx
	js	.LBB1_35
# BB#32:                                #   in Loop: Header=BB1_31 Depth=1
	cmpl	24(%r12), %ecx
	jge	.LBB1_35
# BB#33:                                #   in Loop: Header=BB1_31 Depth=1
	movq	16(%r12), %rdx
	movq	(%rdx,%rcx,8), %rcx
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_35:                               #   in Loop: Header=BB1_31 Depth=1
	xorl	%ecx, %ecx
.LBB1_36:                               #   in Loop: Header=BB1_31 Depth=1
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_39
# BB#37:                                # %.lr.ph.i156
                                        #   in Loop: Header=BB1_31 Depth=1
	movq	(%r12), %rsi
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB1_38:                               #   Parent Loop BB1_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rdi
	movq	(%rsi,%rdi,8), %rdi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	4(%rdi), %xmm4
	addsd	%xmm0, %xmm4
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	addsd	%xmm5, %xmm3
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_38
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_31 Depth=1
	xorpd	%xmm3, %xmm3
.LBB1_40:                               # %._crit_edge.i157
                                        #   in Loop: Header=BB1_31 Depth=1
	movslq	(%rcx), %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	(%rbp,%rcx,4), %xmm4
	divsd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm3
	maxsd	%xmm2, %xmm3
	cmoval	%ecx, %r13d
	movq	24(%rax), %rax
	testq	%rax, %rax
	movapd	%xmm3, %xmm2
	jne	.LBB1_31
	jmp	.LBB1_55
.LBB1_41:
	movl	$-1, %r13d
	movl	16(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB1_55
.LBB1_42:                               # %.lr.ph78.split.us.i.preheader
	movl	16(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_43:                               # %.lr.ph78.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_50 Depth 2
	movslq	4(%rax), %rcx
	testq	%rcx, %rcx
	js	.LBB1_47
# BB#44:                                #   in Loop: Header=BB1_43 Depth=1
	cmpl	24(%r12), %ecx
	jge	.LBB1_47
# BB#45:                                #   in Loop: Header=BB1_43 Depth=1
	movq	16(%r12), %rdx
	movq	(%rdx,%rcx,8), %rcx
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_47:                               #   in Loop: Header=BB1_43 Depth=1
	xorl	%ecx, %ecx
.LBB1_48:                               #   in Loop: Header=BB1_43 Depth=1
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_51
# BB#49:                                # %.lr.ph.us.i
                                        #   in Loop: Header=BB1_43 Depth=1
	movq	(%r12), %rsi
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB1_50:                               #   Parent Loop BB1_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rdi
	movq	(%rsi,%rdi,8), %rdi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	4(%rdi), %xmm4
	addsd	%xmm0, %xmm4
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	addsd	%xmm5, %xmm3
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_50
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_51:                               #   in Loop: Header=BB1_43 Depth=1
	xorpd	%xmm3, %xmm3
.LBB1_52:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_43 Depth=1
	ucomisd	%xmm2, %xmm3
	jbe	.LBB1_54
# BB#53:                                #   in Loop: Header=BB1_43 Depth=1
	movl	(%rcx), %r13d
	movapd	%xmm3, %xmm2
.LBB1_54:                               #   in Loop: Header=BB1_43 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_43
.LBB1_55:                               # %select_column.exit
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_free
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	solution_free
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_57
# BB#56:
	movq	144(%rsp), %rax
	movq	%rax, %rcx
	movl	28(%rcx), %eax
	addl	%ebx, %eax
	movl	%eax, 44(%rcx)
.LBB1_57:
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB1_59
# BB#58:
	movq	144(%rsp), %rbp
	cmpl	$0, 16(%rbp)
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	printf
	movl	48(%r12), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	72(%r12), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	28(%rbp), %r14d
	movl	8(%r15), %ebx
	addl	%r14d, %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%r14,%rax), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	addl	16(%rsp), %r14d         # 4-byte Folded Reload
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	32(%rbp), %rax
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, (%rsp)
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	56(%rsp), %esi          # 4-byte Reload
	movl	84(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %ecx
	movl	16(%rsp), %ebx          # 4-byte Reload
	movl	80(%rsp), %r8d          # 4-byte Reload
	movl	%r14d, %r9d
	callq	printf
.LBB1_59:
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpl	%ebp, %ebx
	jge	.LBB1_64
# BB#60:
	cmpl	$0, 48(%r12)
	je	.LBB1_67
# BB#61:
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_block_partition
	testl	%eax, %eax
	movq	144(%rsp), %rbx
	je	.LBB1_72
# BB#62:
	movq	72(%rsp), %rdx
	movl	72(%rdx), %ecx
	movq	64(%rsp), %rax
	cmpl	72(%rax), %ecx
	jle	.LBB1_80
# BB#63:
	movq	%rax, 72(%rsp)
	movq	%rdx, 64(%rsp)
	movq	%rdx, %rcx
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_81
	jmp	.LBB1_82
.LBB1_64:
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB1_66
# BB#65:
	movl	$.Lstr.2, %edi
	callq	puts
.LBB1_66:
	movq	$0, 48(%rsp)
	jmp	.LBB1_87
.LBB1_67:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	solution_dup
	movq	%rax, 48(%rsp)
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	movq	144(%rsp), %rbx
	je	.LBB1_69
# BB#68:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB1_69:
	cmpl	$0, (%rbx)
	je	.LBB1_87
# BB#70:
	cmpl	$0, 16(%rbx)
	jne	.LBB1_87
# BB#71:
	movq	48(%rsp), %rax
	movl	28(%rbx), %r14d
	addl	8(%rax), %r14d
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	32(%rbx), %rax
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %rcx
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	20(%rsp), %edx          # 4-byte Reload
	callq	printf
	jmp	.LBB1_87
.LBB1_72:
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB1_74
# BB#73:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
.LBB1_74:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_dup
	movq	%rax, %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	solution_dup
	movq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movl	%r13d, %ecx
	callq	solution_accept
	movl	20(%rsp), %r9d          # 4-byte Reload
	incl	%r9d
	movq	144(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%ebp, %r8d
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	callq	sm_mincov
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	solution_free
	xorl	%eax, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sm_free
	testq	%r14, %r14
	je	.LBB1_76
# BB#75:
	movl	8(%r14), %eax
	cmpl	%ebp, %eax
	cmovlel	%eax, %ebp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
.LBB1_76:
	movq	144(%rsp), %rax
	cmpl	$0, 40(%rax)
	jne	.LBB1_88
# BB#77:
	testq	%r14, %r14
	je	.LBB1_79
# BB#78:
	movl	16(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 8(%r14)
	je	.LBB1_88
.LBB1_79:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_dup
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	solution_dup
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	solution_reject
	movq	144(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	sm_mincov
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	solution_free
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sm_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	solution_choose_best
	movq	%rax, 48(%rsp)
	jmp	.LBB1_87
.LBB1_80:
	movq	%rax, %rcx
	movq	%rdx, %rax
	cmpb	$0, 32(%rsp)            # 1-byte Folded Reload
	je	.LBB1_82
.LBB1_81:
	movl	48(%rax), %esi
	movl	48(%rcx), %edx
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_82:
	incl	20(%rbx)
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	solution_alloc
	incl	16(%rbx)
	movq	%rax, %rbp
	movq	72(%rsp), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r8d
	subl	8(%r15), %r8d
	movl	20(%rsp), %r9d          # 4-byte Reload
	incl	%r9d
	movq	%rbx, (%rsp)
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%r9d, %r12d
	callq	sm_mincov
	movq	%rax, %r13
	decl	16(%rbx)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	solution_free
	movq	72(%rsp), %rdi
	xorl	%eax, %eax
	callq	sm_free
	testq	%r13, %r13
	je	.LBB1_86
# BB#83:
	movq	%rbx, %r14
	movq	(%r13), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_85
	.p2align	4, 0x90
.LBB1_84:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rbx), %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	solution_add
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_84
.LBB1_85:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	solution_free
	movq	64(%rsp), %rdi
	movq	%r14, (%rsp)
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	callq	sm_mincov
	movq	%rax, %r14
.LBB1_86:
	movq	%r14, 48(%rsp)
	movq	64(%rsp), %rdi
	xorl	%eax, %eax
	callq	sm_free
.LBB1_87:
	movq	48(%rsp), %r14
.LBB1_88:
	movq	%r14, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sm_mincov, .Lfunc_end1-sm_mincov
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"lower bound = %d\n"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"matrix     = %d by %d with %d elements (%4.3f%%)\n"
	.size	.L.str.2, 50

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cover size = %d elements\n"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cover cost = %d\n"
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"time       = %s\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"components = %d\n"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"gimpel     = %d\n"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"nodes      = %d\n"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"max_depth  = %d\n"
	.size	.L.str.9, 17

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Fatal error: file %s, line %d\n%s\n"
	.size	.L.str.10, 34

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/espresso/mincov.c"
	.size	.L.str.11, 91

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"mincov: internal error -- cover verification failed\n"
	.size	.L.str.12, 53

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"ABSMIN[%2d]%s"
	.size	.L.str.13, 14

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"*"
	.size	.L.str.14, 2

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" "
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	" %3dx%3d sel=%3d bnd=%3d lb=%3d %12s "
	.size	.L.str.16, 38

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"new 'best' solution %d at level %d (time is %s)\n"
	.size	.L.str.19, 49

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"comp %d %d\n"
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"pick=%d\n"
	.size	.L.str.21, 9

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"**** heuristic covering ..."
	.size	.Lstr, 28

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"BEST"
	.size	.Lstr.1, 5

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"bounded"
	.size	.Lstr.2, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
