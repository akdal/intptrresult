	.text
	.file	"libclamav_md5.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.text
	.globl	cli_md5_init
	.p2align	4, 0x90
	.type	cli_md5_init,@function
cli_md5_init:                           # @cli_md5_init
	.cfi_startproc
# BB#0:
	movl	$-1732584194, 16(%rdi)  # imm = 0x98BADCFE
	movl	$271733878, 20(%rdi)    # imm = 0x10325476
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,0,1732584193,4023233417]
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end0:
	.size	cli_md5_init, .Lfunc_end0-cli_md5_init
	.cfi_endproc

	.globl	cli_md5_update
	.p2align	4, 0x90
	.type	cli_md5_update,@function
cli_md5_update:                         # @cli_md5_update
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	(%r12), %eax
	leal	(%r15,%rax), %edx
	andl	$536870911, %edx        # imm = 0x1FFFFFFF
	movl	%edx, (%r12)
	movl	4(%r12), %ecx
	cmpl	%eax, %edx
	jae	.LBB1_2
# BB#1:
	incl	%ecx
	movl	%ecx, 4(%r12)
.LBB1_2:                                # %._crit_edge
	movq	%r15, %rdx
	shrq	$29, %rdx
	addl	%ecx, %edx
	movl	%edx, 4(%r12)
	andl	$63, %eax
	je	.LBB1_5
# BB#3:
	movl	$64, %ebx
	subq	%rax, %rbx
	leaq	24(%r12,%rax), %rdi
	cmpq	%r15, %rbx
	ja	.LBB1_8
# BB#4:
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, %r14
	subq	%rbx, %r15
	leaq	24(%r12), %rsi
	movl	$64, %edx
	movq	%r12, %rdi
	callq	body
.LBB1_5:
	cmpq	$64, %r15
	jb	.LBB1_7
# BB#6:
	movq	%r15, %rdx
	andq	$-64, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	body
	movq	%rax, %r14
	andl	$63, %r15d
.LBB1_7:
	addq	$24, %r12
	movq	%r12, %rdi
.LBB1_8:
	movq	%r14, %rsi
	movq	%r15, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	memcpy                  # TAILCALL
.Lfunc_end1:
	.size	cli_md5_update, .Lfunc_end1-cli_md5_update
	.cfi_endproc

	.p2align	4, 0x90
	.type	body,@function
body:                                   # @body
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	8(%rdi), %ecx
	movl	12(%rdi), %r8d
	movl	16(%rdi), %ebp
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	movl	20(%rdi), %edi
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r8, -32(%rsp)          # 8-byte Spill
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	movl	%ebx, -68(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	movl	%ebp, %edx
	movq	%rax, %rcx
	xorl	%edx, %eax
	movq	%rdx, %rdi
	andl	%r8d, %eax
	xorl	%ecx, %eax
	movq	%rcx, %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movl	(%rsi), %ecx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	addl	%ebx, %eax
	movl	4(%rsi), %ebp
	movq	%rbp, -104(%rsp)        # 8-byte Spill
	leal	-680876936(%rcx,%rax), %eax
	roll	$7, %eax
	movl	%r8d, %ecx
	addl	%r8d, %eax
	xorl	%edi, %ecx
	andl	%eax, %ecx
	leal	(%rdx,%rbp), %edx
	xorl	%edi, %ecx
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	leal	-389564586(%rcx,%rdx), %ecx
	roll	$12, %ecx
	movl	%eax, %edx
	xorl	%r8d, %edx
	addl	%eax, %ecx
	andl	%ecx, %edx
	movl	8(%rsi), %ebp
	movq	%rbp, -64(%rsp)         # 8-byte Spill
	leal	(%rdi,%rbp), %ebp
	xorl	%r8d, %edx
	leal	606105819(%rdx,%rbp), %edi
	roll	$17, %edi
	movl	%ecx, %edx
	xorl	%eax, %edx
	addl	%ecx, %edi
	andl	%edi, %edx
	movl	12(%rsi), %ebp
	movq	%rbp, -56(%rsp)         # 8-byte Spill
	leal	(%r8,%rbp), %ebp
	xorl	%eax, %edx
	leal	-1044525330(%rdx,%rbp), %ebp
	roll	$22, %ebp
	movl	%edi, %edx
	xorl	%ecx, %edx
	addl	%edi, %ebp
	andl	%ebp, %edx
	movl	16(%rsi), %ebx
	addl	%ebx, %eax
	movl	%ebx, %r10d
	xorl	%ecx, %edx
	leal	-176418897(%rdx,%rax), %eax
	roll	$7, %eax
	movl	%ebp, %edx
	xorl	%edi, %edx
	addl	%ebp, %eax
	andl	%eax, %edx
	movl	20(%rsi), %ebx
	addl	%ebx, %ecx
	movl	%ebx, %r14d
	xorl	%edi, %edx
	leal	1200080426(%rdx,%rcx), %ecx
	roll	$12, %ecx
	movl	%eax, %edx
	xorl	%ebp, %edx
	addl	%eax, %ecx
	andl	%ecx, %edx
	movl	24(%rsi), %ebx
	addl	%ebx, %edi
	movl	%ebx, %r8d
	movl	%r8d, -72(%rsp)         # 4-byte Spill
	xorl	%ebp, %edx
	leal	-1473231341(%rdx,%rdi), %r9d
	roll	$17, %r9d
	movl	%ecx, %edx
	xorl	%eax, %edx
	addl	%ecx, %r9d
	andl	%r9d, %edx
	movl	28(%rsi), %edi
	movl	%edi, -112(%rsp)        # 4-byte Spill
	addl	%edi, %ebp
	xorl	%eax, %edx
	leal	-45705983(%rdx,%rbp), %r11d
	roll	$22, %r11d
	movl	%r9d, %edx
	xorl	%ecx, %edx
	addl	%r9d, %r11d
	andl	%r11d, %edx
	movl	32(%rsi), %edi
	movl	%edi, -116(%rsp)        # 4-byte Spill
	addl	%edi, %eax
	xorl	%ecx, %edx
	leal	1770035416(%rdx,%rax), %eax
	roll	$7, %eax
	movl	%r11d, %edx
	xorl	%r9d, %edx
	addl	%r11d, %eax
	andl	%eax, %edx
	movl	36(%rsi), %edi
	addl	%edi, %ecx
	movl	%edi, %ebp
	xorl	%r9d, %edx
	leal	-1958414417(%rdx,%rcx), %r12d
	roll	$12, %r12d
	movl	%eax, %edx
	xorl	%r11d, %edx
	addl	%eax, %r12d
	andl	%r12d, %edx
	movl	40(%rsi), %ecx
	addl	%ecx, %r9d
	movl	%ecx, %r15d
	movl	%r15d, -92(%rsp)        # 4-byte Spill
	xorl	%r11d, %edx
	leal	-42063(%rdx,%r9), %edx
	roll	$17, %edx
	movl	%r12d, %edi
	xorl	%eax, %edi
	addl	%r12d, %edx
	andl	%edx, %edi
	movl	44(%rsi), %ecx
	addl	%ecx, %r11d
	movl	%ecx, %ebx
	movl	%ebx, -108(%rsp)        # 4-byte Spill
	xorl	%eax, %edi
	leal	-1990404162(%rdi,%r11), %r11d
	roll	$22, %r11d
	movl	%edx, %edi
	xorl	%r12d, %edi
	addl	%edx, %r11d
	andl	%r11d, %edi
	movl	48(%rsi), %ecx
	movl	%ecx, -84(%rsp)         # 4-byte Spill
	addl	%ecx, %eax
	xorl	%r12d, %edi
	leal	1804603682(%rdi,%rax), %ecx
	roll	$7, %ecx
	movl	%r11d, %edi
	xorl	%edx, %edi
	addl	%r11d, %ecx
	andl	%ecx, %edi
	movl	52(%rsi), %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	addl	%eax, %r12d
	xorl	%edx, %edi
	leal	-40341101(%rdi,%r12), %r13d
	roll	$12, %r13d
	movl	%ecx, %eax
	xorl	%r11d, %eax
	addl	%ecx, %r13d
	andl	%r13d, %eax
	movl	56(%rsi), %edi
	addl	%edi, %edx
	xorl	%r11d, %eax
	leal	-1502002290(%rax,%rdx), %r12d
	roll	$17, %r12d
	movl	%r13d, %eax
	xorl	%ecx, %eax
	addl	%r13d, %r12d
	andl	%r12d, %eax
	movl	60(%rsi), %edx
	addl	%edx, %r11d
	movl	%edx, -76(%rsp)         # 4-byte Spill
	xorl	%ecx, %eax
	leal	1236535329(%rax,%r11), %r11d
	roll	$22, %r11d
	addl	%r12d, %r11d
	movl	%r11d, %eax
	xorl	%r12d, %eax
	andl	%r13d, %eax
	xorl	%r12d, %eax
	addl	-104(%rsp), %ecx        # 4-byte Folded Reload
	leal	-165796510(%rax,%rcx), %eax
	roll	$5, %eax
	addl	%r11d, %eax
	movl	%eax, %ecx
	xorl	%r11d, %ecx
	andl	%r12d, %ecx
	xorl	%r11d, %ecx
	addl	%r8d, %r13d
	leal	-1069501632(%rcx,%r13), %r13d
	roll	$9, %r13d
	addl	%eax, %r13d
	movl	%r13d, %ecx
	xorl	%eax, %ecx
	andl	%r11d, %ecx
	xorl	%eax, %ecx
	addl	%ebx, %r12d
	leal	643717713(%rcx,%r12), %r12d
	roll	$14, %r12d
	addl	%r13d, %r12d
	movl	%r12d, %ecx
	xorl	%r13d, %ecx
	andl	%eax, %ecx
	addl	-48(%rsp), %r11d        # 4-byte Folded Reload
	xorl	%r13d, %ecx
	leal	-373897302(%rcx,%r11), %r11d
	roll	$20, %r11d
	addl	%r12d, %r11d
	movl	%r11d, %ecx
	xorl	%r12d, %ecx
	andl	%r13d, %ecx
	xorl	%r12d, %ecx
	addl	%r14d, %eax
	movl	%r14d, %r9d
	movl	%r9d, -88(%rsp)         # 4-byte Spill
	leal	-701558691(%rcx,%rax), %eax
	roll	$5, %eax
	addl	%r11d, %eax
	movl	%eax, %ecx
	xorl	%r11d, %ecx
	andl	%r12d, %ecx
	xorl	%r11d, %ecx
	addl	%r15d, %r13d
	leal	38016083(%rcx,%r13), %r13d
	roll	$9, %r13d
	addl	%eax, %r13d
	movl	%r13d, %ecx
	xorl	%eax, %ecx
	andl	%r11d, %ecx
	xorl	%eax, %ecx
	addl	%edx, %r12d
	leal	-660478335(%rcx,%r12), %r12d
	roll	$14, %r12d
	addl	%r13d, %r12d
	movl	%r12d, %ecx
	xorl	%r13d, %ecx
	andl	%eax, %ecx
	movl	%r10d, %r8d
	movl	%r8d, -80(%rsp)         # 4-byte Spill
	addl	%r8d, %r11d
	xorl	%r13d, %ecx
	leal	-405537848(%rcx,%r11), %r11d
	roll	$20, %r11d
	addl	%r12d, %r11d
	movl	%r11d, %ecx
	xorl	%r12d, %ecx
	andl	%r13d, %ecx
	xorl	%r12d, %ecx
	movl	%ebp, %edx
	movl	%edx, -120(%rsp)        # 4-byte Spill
	addl	%edx, %eax
	leal	568446438(%rcx,%rax), %eax
	roll	$5, %eax
	addl	%r11d, %eax
	movl	%eax, %ecx
	xorl	%r11d, %ecx
	andl	%r12d, %ecx
	xorl	%r11d, %ecx
	addl	%edi, %r13d
	movl	%edi, %r10d
	leal	-1019803690(%rcx,%r13), %r13d
	roll	$9, %r13d
	addl	%eax, %r13d
	movl	%r13d, %ecx
	xorl	%eax, %ecx
	andl	%r11d, %ecx
	xorl	%eax, %ecx
	addl	-56(%rsp), %r12d        # 4-byte Folded Reload
	leal	-187363961(%rcx,%r12), %r12d
	roll	$14, %r12d
	addl	%r13d, %r12d
	movl	%r12d, %ecx
	xorl	%r13d, %ecx
	andl	%eax, %ecx
	movl	-116(%rsp), %edi        # 4-byte Reload
	addl	%edi, %r11d
	xorl	%r13d, %ecx
	leal	1163531501(%rcx,%r11), %r11d
	roll	$20, %r11d
	addl	%r12d, %r11d
	movl	%r11d, %ecx
	xorl	%r12d, %ecx
	andl	%r13d, %ecx
	xorl	%r12d, %ecx
	movl	-124(%rsp), %r15d       # 4-byte Reload
	addl	%r15d, %eax
	leal	-1444681467(%rcx,%rax), %eax
	roll	$5, %eax
	addl	%r11d, %eax
	movl	%eax, %ecx
	xorl	%r11d, %ecx
	andl	%r12d, %ecx
	xorl	%r11d, %ecx
	movq	-64(%rsp), %rbx         # 8-byte Reload
	addl	%ebx, %r13d
	leal	-51403784(%rcx,%r13), %r13d
	roll	$9, %r13d
	addl	%eax, %r13d
	movl	%r13d, %ecx
	xorl	%eax, %ecx
	andl	%r11d, %ecx
	xorl	%eax, %ecx
	movl	-112(%rsp), %r14d       # 4-byte Reload
	addl	%r14d, %r12d
	leal	1735328473(%rcx,%r12), %edx
	roll	$14, %edx
	addl	%r13d, %edx
	movl	%edx, %r12d
	xorl	%r13d, %r12d
	movl	%r12d, %ecx
	andl	%eax, %ecx
	movl	-84(%rsp), %ebp         # 4-byte Reload
	addl	%ebp, %r11d
	xorl	%r13d, %ecx
	leal	-1926607734(%rcx,%r11), %ecx
	roll	$20, %ecx
	addl	%edx, %ecx
	xorl	%ecx, %r12d
	addl	%r9d, %eax
	leal	-378558(%r12,%rax), %eax
	roll	$4, %eax
	addl	%ecx, %eax
	movl	%ecx, %r11d
	xorl	%edx, %r11d
	xorl	%eax, %r11d
	addl	%edi, %r13d
	leal	-2022574463(%r11,%r13), %r13d
	roll	$11, %r13d
	addl	%eax, %r13d
	movl	%eax, %r11d
	xorl	%ecx, %r11d
	xorl	%r13d, %r11d
	addl	-108(%rsp), %edx        # 4-byte Folded Reload
	leal	1839030562(%r11,%rdx), %r11d
	roll	$16, %r11d
	addl	%r13d, %r11d
	movl	%r13d, %edx
	xorl	%eax, %edx
	xorl	%r11d, %edx
	addl	%r10d, %ecx
	leal	-35309556(%rdx,%rcx), %r12d
	roll	$23, %r12d
	addl	%r11d, %r12d
	movl	%r11d, %ecx
	xorl	%r13d, %ecx
	xorl	%r12d, %ecx
	addl	-104(%rsp), %eax        # 4-byte Folded Reload
	leal	-1530992060(%rcx,%rax), %eax
	roll	$4, %eax
	addl	%r12d, %eax
	movl	%r12d, %ecx
	xorl	%r11d, %ecx
	xorl	%eax, %ecx
	addl	%r8d, %r13d
	leal	1272893353(%rcx,%r13), %ecx
	roll	$11, %ecx
	addl	%eax, %ecx
	movl	%eax, %edx
	xorl	%r12d, %edx
	xorl	%ecx, %edx
	addl	%r14d, %r11d
	leal	-155497632(%rdx,%r11), %r11d
	roll	$16, %r11d
	addl	%ecx, %r11d
	movl	%ecx, %edx
	xorl	%eax, %edx
	xorl	%r11d, %edx
	movl	-92(%rsp), %r14d        # 4-byte Reload
	addl	%r14d, %r12d
	leal	-1094730640(%rdx,%r12), %r12d
	roll	$23, %r12d
	addl	%r11d, %r12d
	movl	%r11d, %edx
	xorl	%ecx, %edx
	xorl	%r12d, %edx
	addl	%r15d, %eax
	leal	681279174(%rdx,%rax), %eax
	roll	$4, %eax
	addl	%r12d, %eax
	movl	%r12d, %edx
	xorl	%r11d, %edx
	xorl	%eax, %edx
	movq	-48(%rsp), %rdi         # 8-byte Reload
	addl	%edi, %ecx
	leal	-358537222(%rdx,%rcx), %ecx
	roll	$11, %ecx
	addl	%eax, %ecx
	movl	%eax, %edx
	xorl	%r12d, %edx
	xorl	%ecx, %edx
	movq	-56(%rsp), %r8          # 8-byte Reload
	addl	%r8d, %r11d
	leal	-722521979(%rdx,%r11), %r13d
	roll	$16, %r13d
	addl	%ecx, %r13d
	movl	%ecx, %edx
	xorl	%eax, %edx
	xorl	%r13d, %edx
	movl	-72(%rsp), %r15d        # 4-byte Reload
	addl	%r15d, %r12d
	leal	76029189(%rdx,%r12), %r12d
	roll	$23, %r12d
	addl	%r13d, %r12d
	movl	%r13d, %edx
	xorl	%ecx, %edx
	xorl	%r12d, %edx
	addl	-120(%rsp), %eax        # 4-byte Folded Reload
	leal	-640364487(%rdx,%rax), %r11d
	roll	$4, %r11d
	addl	%r12d, %r11d
	movl	%r12d, %eax
	xorl	%r13d, %eax
	xorl	%r11d, %eax
	addl	%ebp, %ecx
	leal	-421815835(%rax,%rcx), %eax
	roll	$11, %eax
	addl	%r11d, %eax
	movl	%r11d, %ecx
	xorl	%r12d, %ecx
	xorl	%eax, %ecx
	movl	-76(%rsp), %r9d         # 4-byte Reload
	addl	%r9d, %r13d
	leal	530742520(%rcx,%r13), %r13d
	roll	$16, %r13d
	addl	%eax, %r13d
	movl	%eax, %ecx
	xorl	%r11d, %ecx
	xorl	%r13d, %ecx
	addl	%ebx, %r12d
	leal	-995338651(%rcx,%r12), %r12d
	addl	%edi, %r11d
	movl	%eax, %ecx
	addl	-112(%rsp), %eax        # 4-byte Folded Reload
	roll	$23, %r12d
	addl	%r13d, %r12d
	notl	%ecx
	orl	%r12d, %ecx
	xorl	%r13d, %ecx
	leal	-198630844(%rcx,%r11), %ecx
	roll	$6, %ecx
	addl	%r12d, %ecx
	movl	%r13d, %edx
	notl	%edx
	orl	%ecx, %edx
	xorl	%r12d, %edx
	leal	1126891415(%rdx,%rax), %eax
	addl	%r10d, %r13d
	roll	$10, %eax
	addl	%ecx, %eax
	movl	%r12d, %edx
	notl	%edx
	orl	%eax, %edx
	xorl	%ecx, %edx
	leal	-1416354905(%rdx,%r13), %edi
	addl	-88(%rsp), %r12d        # 4-byte Folded Reload
	roll	$15, %edi
	addl	%eax, %edi
	movl	%ecx, %edx
	notl	%edx
	orl	%edi, %edx
	xorl	%eax, %edx
	leal	-57434055(%rdx,%r12), %ebx
	addl	%ebp, %ecx
	movl	%eax, %edx
	addl	%r8d, %eax
	roll	$21, %ebx
	addl	%edi, %ebx
	notl	%edx
	orl	%ebx, %edx
	xorl	%edi, %edx
	leal	1700485571(%rdx,%rcx), %ecx
	roll	$6, %ecx
	addl	%ebx, %ecx
	movl	%edi, %edx
	notl	%edx
	orl	%ecx, %edx
	xorl	%ebx, %edx
	leal	-1894986606(%rdx,%rax), %eax
	addl	%r14d, %edi
	roll	$10, %eax
	addl	%ecx, %eax
	movl	%ebx, %edx
	notl	%edx
	orl	%eax, %edx
	xorl	%ecx, %edx
	leal	-1051523(%rdx,%rdi), %edi
	addl	-104(%rsp), %ebx        # 4-byte Folded Reload
	roll	$15, %edi
	addl	%eax, %edi
	movl	%ecx, %edx
	notl	%edx
	orl	%edi, %edx
	xorl	%eax, %edx
	leal	-2054922799(%rdx,%rbx), %ebx
	roll	$21, %ebx
	addl	-116(%rsp), %ecx        # 4-byte Folded Reload
	movl	%eax, %edx
	notl	%edx
	addl	%edi, %ebx
	orl	%ebx, %edx
	xorl	%edi, %edx
	leal	1873313359(%rdx,%rcx), %ecx
	roll	$6, %ecx
	addl	%ebx, %ecx
	addl	%r9d, %eax
	movl	%edi, %edx
	notl	%edx
	orl	%ecx, %edx
	xorl	%ebx, %edx
	leal	-30611744(%rdx,%rax), %ebp
	roll	$10, %ebp
	addl	%ecx, %ebp
	addl	%r15d, %edi
	movl	%ebx, %eax
	notl	%eax
	orl	%ebp, %eax
	xorl	%ecx, %eax
	leal	-1560198380(%rax,%rdi), %r8d
	roll	$15, %r8d
	addl	-124(%rsp), %ebx        # 4-byte Folded Reload
	movl	%ecx, %eax
	addl	%ebp, %r8d
	notl	%eax
	orl	%r8d, %eax
	xorl	%ebp, %eax
	leal	1309151649(%rax,%rbx), %ebx
	roll	$21, %ebx
	addl	-80(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ebp, %edx
	notl	%edx
	addl	%r8d, %ebx
	orl	%ebx, %edx
	xorl	%r8d, %edx
	leal	-145523070(%rdx,%rcx), %ecx
	roll	$6, %ecx
	addl	%ebx, %ecx
	addl	-108(%rsp), %ebp        # 4-byte Folded Reload
	movl	%r8d, %edx
	notl	%edx
	orl	%ecx, %edx
	xorl	%ebx, %edx
	leal	-1120210379(%rdx,%rbp), %edi
	roll	$10, %edi
	addl	%ecx, %edi
	addl	-64(%rsp), %r8d         # 4-byte Folded Reload
	movl	%ebx, %eax
	notl	%eax
	orl	%edi, %eax
	xorl	%ecx, %eax
	leal	718787259(%rax,%r8), %ebp
	movq	-32(%rsp), %r8          # 8-byte Reload
	roll	$15, %ebp
	addl	-120(%rsp), %ebx        # 4-byte Folded Reload
	movl	%ecx, %eax
	addl	%edi, %ebp
	notl	%eax
	orl	%ebp, %eax
	xorl	%edi, %eax
	leal	-343485551(%rax,%rbx), %eax
	addl	-68(%rsp), %ecx         # 4-byte Folded Reload
	roll	$21, %eax
	addl	%ebp, %r8d
	addl	%eax, %r8d
	movq	-8(%rsp), %rdx          # 8-byte Reload
	addl	-24(%rsp), %ebp         # 4-byte Folded Reload
	addl	-16(%rsp), %edi         # 4-byte Folded Reload
	addq	$64, %rsi
	addq	$-64, %rdx
	jne	.LBB2_1
# BB#2:
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	%ecx, 8(%rax)
	movl	%r8d, 12(%rax)
	movl	%ebp, 16(%rax)
	movl	%edi, 20(%rax)
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	body, .Lfunc_end2-body
	.cfi_endproc

	.globl	cli_md5_final
	.p2align	4, 0x90
	.type	cli_md5_final,@function
cli_md5_final:                          # @cli_md5_final
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r12, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %edx
	andl	$63, %edx
	leaq	24(%rbx), %r15
	movb	$-128, 24(%rbx,%rdx)
	leaq	1(%rdx), %r12
	xorq	$63, %rdx
	cmpq	$8, %rdx
	jae	.LBB3_1
# BB#2:
	leaq	24(%rbx,%r12), %rdi
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	callq	memset
	movl	$64, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	body
	movl	$56, %edx
	jmp	.LBB3_3
.LBB3_1:                                # %._crit_edge
	addq	$-8, %rdx
.LBB3_3:
	leaq	24(%rbx,%r12), %rdi
	xorl	%esi, %esi
	callq	memset
	movl	(%rbx), %eax
	leal	(,%rax,8), %ecx
	movl	%ecx, (%rbx)
	movb	%cl, 80(%rbx)
	movl	%eax, %ecx
	shrl	$5, %ecx
	movb	%cl, 81(%rbx)
	movl	%eax, %ecx
	shrl	$13, %ecx
	movb	%cl, 82(%rbx)
	shrl	$21, %eax
	movb	%al, 83(%rbx)
	movl	4(%rbx), %eax
	movb	%al, 84(%rbx)
	movb	%ah, 85(%rbx)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 86(%rbx)
	shrl	$24, %eax
	movb	%al, 87(%rbx)
	movl	$64, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	body
	movb	8(%rbx), %al
	movb	%al, (%r14)
	movb	9(%rbx), %al
	movb	%al, 1(%r14)
	movb	10(%rbx), %al
	movb	%al, 2(%r14)
	movb	11(%rbx), %al
	movb	%al, 3(%r14)
	movb	12(%rbx), %al
	movb	%al, 4(%r14)
	movb	13(%rbx), %al
	movb	%al, 5(%r14)
	movb	14(%rbx), %al
	movb	%al, 6(%r14)
	movb	15(%rbx), %al
	movb	%al, 7(%r14)
	movb	16(%rbx), %al
	movb	%al, 8(%r14)
	movb	17(%rbx), %al
	movb	%al, 9(%r14)
	movb	18(%rbx), %al
	movb	%al, 10(%r14)
	movb	19(%rbx), %al
	movb	%al, 11(%r14)
	movb	20(%rbx), %al
	movb	%al, 12(%r14)
	movb	21(%rbx), %al
	movb	%al, 13(%r14)
	movb	22(%rbx), %al
	movb	%al, 14(%r14)
	movb	23(%rbx), %al
	movb	%al, 15(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	cli_md5_final, .Lfunc_end3-cli_md5_final
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
