	.text
	.file	"libclamav_regex_strlcpy.bc"
	.globl	cli_strlcpy
	.p2align	4, 0x90
	.type	cli_strlcpy,@function
cli_strlcpy:                            # @cli_strlcpy
	.cfi_startproc
# BB#0:
	testq	%rdx, %rdx
	movq	%rsi, %rax
	je	.LBB0_5
# BB#1:                                 # %.preheader.preheader
	movl	$1, %ecx
	subq	%rdx, %rcx
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rdi)
	incq	%rdi
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB0_2
	jmp	.LBB0_6
.LBB0_4:
	movb	$0, (%rdi)
	.p2align	4, 0x90
.LBB0_5:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_5
.LBB0_6:                                # %.loopexit
	notq	%rsi
	addq	%rax, %rsi
	movq	%rsi, %rax
	retq
.Lfunc_end0:
	.size	cli_strlcpy, .Lfunc_end0-cli_strlcpy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
