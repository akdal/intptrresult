	.text
	.file	"ProgressMt.bc"
	.globl	_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo,@function
_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo: # @_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, %rbx
	leaq	88(%rbx), %rdi
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	pthread_mutex_lock
	leaq	8(%rbx), %r13
.Ltmp0:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp1:
# BB#1:
	leaq	40(%rbx), %rbp
.Ltmp2:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp3:
# BB#2:                                 # %.preheader
	testl	%r12d, %r12d
	jle	.LBB0_7
# BB#3:                                 # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
.Ltmp4:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp5:
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	24(%rbx), %rax
	movslq	20(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%rbx)
.Ltmp6:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp7:
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	56(%rbx), %rax
	movslq	52(%rbx), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 52(%rbx)
	incl	%r14d
	cmpl	%r12d, %r14d
	jl	.LBB0_4
.LBB0_7:                                # %._crit_edge
	testq	%r15, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
	je	.LBB0_9
# BB#8:
	movq	(%r15), %rax
.Ltmp9:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp10:
.LBB0_9:                                # %.noexc
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp11:
	callq	*16(%rax)
.Ltmp12:
.LBB0_11:
	movq	%r15, (%rbx)
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	pthread_mutex_unlock    # TAILCALL
.LBB0_12:
.Ltmp13:
	jmp	.LBB0_13
.LBB0_14:
.Ltmp8:
.LBB0_13:
	movq	%rax, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo, .Lfunc_end0-_ZN24CMtCompressProgressMixer4InitEiP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp4           #   Call between .Ltmp4 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24CMtCompressProgressMixer6ReinitEi
	.p2align	4, 0x90
	.type	_ZN24CMtCompressProgressMixer6ReinitEi,@function
_ZN24CMtCompressProgressMixer6ReinitEi: # @_ZN24CMtCompressProgressMixer6ReinitEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	leaq	88(%rbx), %rbp
	movq	%rbp, %rdi
	callq	pthread_mutex_lock
	movq	24(%rbx), %rax
	movslq	%r14d, %rcx
	movq	$0, (%rax,%rcx,8)
	movq	56(%rbx), %rax
	movq	$0, (%rax,%rcx,8)
	movq	%rbp, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	pthread_mutex_unlock    # TAILCALL
.Lfunc_end1:
	.size	_ZN24CMtCompressProgressMixer6ReinitEi, .Lfunc_end1-_ZN24CMtCompressProgressMixer6ReinitEi
	.cfi_endproc

	.globl	_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_
	.p2align	4, 0x90
	.type	_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_,@function
_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_: # @_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	88(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	testq	%rbp, %rbp
	je	.LBB2_2
# BB#1:
	movq	(%rbp), %rax
	movq	24(%rbx), %rcx
	movslq	%r15d, %rdx
	movq	%rax, %rsi
	subq	(%rcx,%rdx,8), %rsi
	movq	%rax, (%rcx,%rdx,8)
	addq	%rsi, 72(%rbx)
.LBB2_2:
	testq	%r12, %r12
	je	.LBB2_4
# BB#3:
	movq	(%r12), %rax
	movq	56(%rbx), %rcx
	movslq	%r15d, %rdx
	movq	%rax, %rsi
	subq	(%rcx,%rdx,8), %rsi
	movq	%rax, (%rcx,%rdx,8)
	addq	%rsi, 80(%rbx)
.LBB2_4:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#6:
	movq	(%rdi), %rax
	leaq	72(%rbx), %rsi
	addq	$80, %rbx
.Ltmp14:
	movq	%rbx, %rdx
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp15:
	jmp	.LBB2_7
.LBB2_5:
	xorl	%ebx, %ebx
.LBB2_7:
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_8:
.Ltmp16:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_, .Lfunc_end2-_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp15     #   Call between .Ltmp15 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_,@function
_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_: # @_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r13, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	16(%rdi), %rbx
	movslq	24(%rdi), %r13
	leaq	88(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	testq	%r12, %r12
	je	.LBB3_2
# BB#1:
	movq	(%r12), %rax
	movq	24(%rbx), %rcx
	movq	%rax, %rdx
	subq	(%rcx,%r13,8), %rdx
	movq	%rax, (%rcx,%r13,8)
	addq	%rdx, 72(%rbx)
.LBB3_2:
	testq	%r15, %r15
	je	.LBB3_4
# BB#3:
	movq	(%r15), %rax
	movq	56(%rbx), %rcx
	movq	%rax, %rdx
	subq	(%rcx,%r13,8), %rdx
	movq	%rax, (%rcx,%r13,8)
	addq	%rdx, 80(%rbx)
.LBB3_4:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
# BB#6:
	movq	(%rdi), %rax
	leaq	72(%rbx), %rsi
	addq	$80, %rbx
.Ltmp17:
	movq	%rbx, %rdx
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp18:
	jmp	.LBB3_7
.LBB3_5:
	xorl	%ebx, %ebx
.LBB3_7:                                # %_ZN24CMtCompressProgressMixer12SetRatioInfoEiPKyS1_.exit
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB3_8:
.Ltmp19:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_, .Lfunc_end3-_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv,@function
_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv: # @_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB4_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB4_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB4_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB4_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB4_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB4_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB4_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB4_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB4_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB4_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB4_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB4_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB4_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB4_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB4_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB4_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB4_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv, .Lfunc_end4-_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN19CMtCompressProgress6AddRefEv,"axG",@progbits,_ZN19CMtCompressProgress6AddRefEv,comdat
	.weak	_ZN19CMtCompressProgress6AddRefEv
	.p2align	4, 0x90
	.type	_ZN19CMtCompressProgress6AddRefEv,@function
_ZN19CMtCompressProgress6AddRefEv:      # @_ZN19CMtCompressProgress6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN19CMtCompressProgress6AddRefEv, .Lfunc_end5-_ZN19CMtCompressProgress6AddRefEv
	.cfi_endproc

	.section	.text._ZN19CMtCompressProgress7ReleaseEv,"axG",@progbits,_ZN19CMtCompressProgress7ReleaseEv,comdat
	.weak	_ZN19CMtCompressProgress7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN19CMtCompressProgress7ReleaseEv,@function
_ZN19CMtCompressProgress7ReleaseEv:     # @_ZN19CMtCompressProgress7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB6_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB6_2:
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN19CMtCompressProgress7ReleaseEv, .Lfunc_end6-_ZN19CMtCompressProgress7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end7-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN19CMtCompressProgressD0Ev,"axG",@progbits,_ZN19CMtCompressProgressD0Ev,comdat
	.weak	_ZN19CMtCompressProgressD0Ev
	.p2align	4, 0x90
	.type	_ZN19CMtCompressProgressD0Ev,@function
_ZN19CMtCompressProgressD0Ev:           # @_ZN19CMtCompressProgressD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end8:
	.size	_ZN19CMtCompressProgressD0Ev, .Lfunc_end8-_ZN19CMtCompressProgressD0Ev
	.cfi_endproc

	.type	_ZTV19CMtCompressProgress,@object # @_ZTV19CMtCompressProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTV19CMtCompressProgress
	.p2align	3
_ZTV19CMtCompressProgress:
	.quad	0
	.quad	_ZTI19CMtCompressProgress
	.quad	_ZN19CMtCompressProgress14QueryInterfaceERK4GUIDPPv
	.quad	_ZN19CMtCompressProgress6AddRefEv
	.quad	_ZN19CMtCompressProgress7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN19CMtCompressProgressD0Ev
	.quad	_ZN19CMtCompressProgress12SetRatioInfoEPKyS1_
	.size	_ZTV19CMtCompressProgress, 64

	.type	_ZTS19CMtCompressProgress,@object # @_ZTS19CMtCompressProgress
	.globl	_ZTS19CMtCompressProgress
	.p2align	4
_ZTS19CMtCompressProgress:
	.asciz	"19CMtCompressProgress"
	.size	_ZTS19CMtCompressProgress, 22

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI19CMtCompressProgress,@object # @_ZTI19CMtCompressProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTI19CMtCompressProgress
	.p2align	4
_ZTI19CMtCompressProgress:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS19CMtCompressProgress
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI19CMtCompressProgress, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
