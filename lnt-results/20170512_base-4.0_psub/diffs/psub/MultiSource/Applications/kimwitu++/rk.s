	.text
	.file	"rk.bc"
	.globl	_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE,@function
_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE: # @_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	40(%rbx), %rax
	je	.LBB0_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB0_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE, .Lfunc_end0-_ZN2kc15impl_uniqID_Str7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE,@function
_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE: # @_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	40(%rbx), %rax
	je	.LBB1_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB1_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE, .Lfunc_end1-_ZN2kc10impl_ID_Id7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE,@function
_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE: # @_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB2_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc3IntEPNS_17impl_integer__IntE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB2_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE, .Lfunc_end2-_ZN2kc12impl_INT_Int7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE: # @_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB3_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc6StringEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB3_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE, .Lfunc_end3-_ZN2kc18impl_STRING_String7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE: # @_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB4_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18PhylumDeclarationsEPNS_23impl_phylumdeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB4_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE, .Lfunc_end4-_ZN2kc46impl_phylumdeclarationsroot_PhylumDeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE: # @_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB5_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB5_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB5_4
.LBB5_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB5_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE, .Lfunc_end5-_ZN2kc23impl_phylumdeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE,@function
_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE: # @_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB6_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB6_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB6_4
.LBB6_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB6_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE, .Lfunc_end6-_ZN2kc16impl_phylumnames7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 64
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	48(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	56(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	32(%rbp), %r14
	jne	.LBB7_4
# BB#1:
	cmpq	40(%rbp), %r15
	jne	.LBB7_4
# BB#2:
	cmpq	48(%rbp), %r12
	jne	.LBB7_4
# BB#3:
	cmpq	56(%rbp), %rax
	je	.LBB7_5
.LBB7_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17PhylumDeclarationEPNS_7impl_IDEPNS_18impl_storageoptionEPNS_20impl_productionblockEPNS_17impl_Ccode_optionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB7_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE, .Lfunc_end7-_ZN2kc40impl_phylumdeclaration_PhylumDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE: # @_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB8_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc21PositiveStorageOptionEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB8_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE, .Lfunc_end8-_ZN2kc40impl_storageoption_PositiveStorageOption7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE: # @_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB9_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc21NegativeStorageOptionEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB9_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE, .Lfunc_end9-_ZN2kc40impl_storageoption_NegativeStorageOption7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE: # @_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end10:
	.size	_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE, .Lfunc_end10-_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE: # @_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB11_4
# BB#1:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r15
	jne	.LBB11_3
# BB#2:
	cmpq	24(%rbx), %rax
	je	.LBB11_4
.LBB11_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsstorageclassesEPNS_7impl_IDEPNS_19impl_storageclassesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB11_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE, .Lfunc_end11-_ZN2kc19impl_storageclasses7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE,@function
_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE: # @_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB12_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc22PredefinedAlternativesEPNS_17impl_alternativesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB12_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE, .Lfunc_end12-_ZN2kc43impl_productionblock_PredefinedAlternatives7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE: # @_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB13_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc19NonlistAlternativesEPNS_17impl_alternativesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB13_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE, .Lfunc_end13-_ZN2kc40impl_productionblock_NonlistAlternatives7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE: # @_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB14_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB14_3
.LBB14_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ListAlternativesEPNS_17impl_alternativesEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB14_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE, .Lfunc_end14-_ZN2kc37impl_productionblock_ListAlternatives7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE: # @_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end15:
	.size	_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE, .Lfunc_end15-_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE,@function
_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE: # @_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -32
.Lcfi86:
	.cfi_offset %r14, -24
.Lcfi87:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB16_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB16_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB16_4
.LBB16_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB16_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE, .Lfunc_end16-_ZN2kc17impl_alternatives7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE: # @_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -32
.Lcfi92:
	.cfi_offset %r14, -24
.Lcfi93:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	40(%rbx), %r14
	jne	.LBB17_2
# BB#1:
	cmpq	48(%rbx), %rax
	je	.LBB17_3
.LBB17_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB17_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE, .Lfunc_end17-_ZN2kc28impl_alternative_Alternative7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc14impl_arguments7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc14impl_arguments7rewriteERNS_11rview_classE,@function
_ZN2kc14impl_arguments7rewriteERNS_11rview_classE: # @_ZN2kc14impl_arguments7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -32
.Lcfi98:
	.cfi_offset %r14, -24
.Lcfi99:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB18_4
# BB#1:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r15
	jne	.LBB18_3
# BB#2:
	cmpq	24(%rbx), %rax
	je	.LBB18_4
.LBB18_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsargumentsEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB18_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	_ZN2kc14impl_arguments7rewriteERNS_11rview_classE, .Lfunc_end18-_ZN2kc14impl_arguments7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE: # @_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB19_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB19_3
.LBB19_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8ArgumentEPNS_7impl_IDEPNS_17impl_integer__IntE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB19_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE, .Lfunc_end19-_ZN2kc22impl_argument_Argument7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE: # @_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 32
.Lcfi109:
	.cfi_offset %rbx, -32
.Lcfi110:
	.cfi_offset %r14, -24
.Lcfi111:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB20_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB20_3
.LBB20_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB20_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE, .Lfunc_end20-_ZN2kc29impl_Ccode_option_CcodeOption7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc15impl_attributes7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc15impl_attributes7rewriteERNS_11rview_classE,@function
_ZN2kc15impl_attributes7rewriteERNS_11rview_classE: # @_ZN2kc15impl_attributes7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -32
.Lcfi116:
	.cfi_offset %r14, -24
.Lcfi117:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB21_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB21_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB21_4
.LBB21_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc14ConsattributesEPNS_14impl_attributeEPNS_15impl_attributesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB21_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN2kc15impl_attributes7rewriteERNS_11rview_classE, .Lfunc_end21-_ZN2kc15impl_attributes7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE: # @_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 48
.Lcfi123:
	.cfi_offset %rbx, -48
.Lcfi124:
	.cfi_offset %r12, -40
.Lcfi125:
	.cfi_offset %r13, -32
.Lcfi126:
	.cfi_offset %r14, -24
.Lcfi127:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB22_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB22_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB22_4
.LBB22_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc9AttributeEPNS_7impl_IDES1_PNS_36impl_attribute_initialisation_optionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB22_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE, .Lfunc_end22-_ZN2kc24impl_attribute_Attribute7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE,@function
_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE: # @_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB23_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc27Yesattribute_initialisationEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB23_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE, .Lfunc_end23-_ZN2kc64impl_attribute_initialisation_option_Yesattribute_initialisation7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE,@function
_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE: # @_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end24:
	.size	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE, .Lfunc_end24-_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE,@function
_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE: # @_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 32
.Lcfi136:
	.cfi_offset %rbx, -32
.Lcfi137:
	.cfi_offset %r14, -24
.Lcfi138:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB25_4
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r15
	jne	.LBB25_3
# BB#2:
	cmpq	32(%rbx), %rax
	je	.LBB25_4
.LBB25_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15ConsCexpressionEPNS_21impl_Cexpression_elemEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB25_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE, .Lfunc_end25-_ZN2kc16impl_Cexpression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE: # @_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi141:
	.cfi_def_cfa_offset 32
.Lcfi142:
	.cfi_offset %rbx, -24
.Lcfi143:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB26_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16CExpressionArrayEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB26_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE, .Lfunc_end26-_ZN2kc38impl_Cexpression_elem_CExpressionArray7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE: # @_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 32
.Lcfi147:
	.cfi_offset %rbx, -24
.Lcfi148:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB27_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc15CExpressionPackEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB27_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end27:
	.size	_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE, .Lfunc_end27-_ZN2kc37impl_Cexpression_elem_CExpressionPack7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE,@function
_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE: # @_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 32
.Lcfi152:
	.cfi_offset %rbx, -24
.Lcfi153:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB28_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13CExpressionSQEPNS_18impl_CexpressionSQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB28_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE, .Lfunc_end28-_ZN2kc35impl_Cexpression_elem_CExpressionSQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE,@function
_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE: # @_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -24
.Lcfi158:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB29_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13CExpressionDQEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB29_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE, .Lfunc_end29-_ZN2kc35impl_Cexpression_elem_CExpressionDQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE,@function
_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE: # @_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end30:
	.size	_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE, .Lfunc_end30-_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE: # @_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB31_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc20CExpressionDollarvarEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB31_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end31:
	.size	_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE, .Lfunc_end31-_ZN2kc42impl_Cexpression_elem_CExpressionDollarvar7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE: # @_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB32_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc15CExpressionPartEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB32_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end32:
	.size	_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE, .Lfunc_end32-_ZN2kc37impl_Cexpression_elem_CExpressionPart7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE: # @_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -32
.Lcfi173:
	.cfi_offset %r14, -24
.Lcfi174:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB33_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB33_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB33_4
.LBB33_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConsCexpressionDQEPNS_23impl_CexpressionDQ_elemEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB33_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE, .Lfunc_end33-_ZN2kc18impl_CexpressionDQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE,@function
_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE: # @_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end34:
	.size	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE, .Lfunc_end34-_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE: # @_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 32
.Lcfi178:
	.cfi_offset %rbx, -24
.Lcfi179:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB35_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc17CExpressionDQPartEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB35_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end35:
	.size	_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE, .Lfunc_end35-_ZN2kc41impl_CexpressionDQ_elem_CExpressionDQPart7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE: # @_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 32
.Lcfi183:
	.cfi_offset %rbx, -32
.Lcfi184:
	.cfi_offset %r14, -24
.Lcfi185:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB36_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB36_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB36_4
.LBB36_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConsCexpressionSQEPNS_23impl_CexpressionSQ_elemEPNS_18impl_CexpressionSQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB36_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE, .Lfunc_end36-_ZN2kc18impl_CexpressionSQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE,@function
_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE: # @_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end37:
	.size	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE, .Lfunc_end37-_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE: # @_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 32
.Lcfi189:
	.cfi_offset %rbx, -24
.Lcfi190:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB38_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc17CExpressionSQPartEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB38_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end38:
	.size	_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE, .Lfunc_end38-_ZN2kc41impl_CexpressionSQ_elem_CExpressionSQPart7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE: # @_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 32
.Lcfi194:
	.cfi_offset %rbx, -32
.Lcfi195:
	.cfi_offset %r14, -24
.Lcfi196:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB39_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB39_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB39_4
.LBB39_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsidCexpressionsEPNS_18impl_idCexpressionEPNS_19impl_idCexpressionsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB39_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end39:
	.size	_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE, .Lfunc_end39-_ZN2kc19impl_idCexpressions7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE: # @_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 32
.Lcfi200:
	.cfi_offset %rbx, -32
.Lcfi201:
	.cfi_offset %r14, -24
.Lcfi202:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r14
	jne	.LBB40_2
# BB#1:
	cmpq	24(%rbx), %rax
	je	.LBB40_3
.LBB40_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13IdCexpressionEPNS_7impl_IDEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB40_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end40:
	.size	_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE, .Lfunc_end40-_ZN2kc32impl_idCexpression_IdCexpression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE,@function
_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE: # @_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 32
.Lcfi206:
	.cfi_offset %rbx, -32
.Lcfi207:
	.cfi_offset %r14, -24
.Lcfi208:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB41_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB41_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB41_4
.LBB41_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10ConsCtextsEPNS_10impl_CtextEPNS_11impl_CtextsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB41_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE, .Lfunc_end41-_ZN2kc11impl_Ctexts7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE,@function
_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE: # @_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -32
.Lcfi213:
	.cfi_offset %r14, -24
.Lcfi214:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB42_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB42_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB42_4
.LBB42_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsincludefilesEPNS_16impl_includefileEPNS_17impl_includefilesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB42_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end42:
	.size	_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE, .Lfunc_end42-_ZN2kc17impl_includefiles7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE: # @_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi217:
	.cfi_def_cfa_offset 32
.Lcfi218:
	.cfi_offset %rbx, -24
.Lcfi219:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB43_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc11IncludeFileEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB43_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end43:
	.size	_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE, .Lfunc_end43-_ZN2kc28impl_includefile_IncludeFile7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE: # @_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 32
.Lcfi223:
	.cfi_offset %rbx, -32
.Lcfi224:
	.cfi_offset %r14, -24
.Lcfi225:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB44_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB44_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB44_4
.LBB44_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc23ConsincludedeclarationsEPNS_23impl_includedeclarationEPNS_24impl_includedeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB44_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end44:
	.size	_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE, .Lfunc_end44-_ZN2kc24impl_includedeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 32
.Lcfi229:
	.cfi_offset %rbx, -24
.Lcfi230:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB45_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18IncludeDeclarationEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB45_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end45:
	.size	_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE, .Lfunc_end45-_ZN2kc42impl_includedeclaration_IncludeDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE: # @_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 32
.Lcfi234:
	.cfi_offset %rbx, -32
.Lcfi235:
	.cfi_offset %r14, -24
.Lcfi236:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB46_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB46_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB46_4
.LBB46_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsrwdeclarationsEPNS_18impl_rwdeclarationEPNS_19impl_rwdeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB46_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end46:
	.size	_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE, .Lfunc_end46-_ZN2kc19impl_rwdeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 32
.Lcfi240:
	.cfi_offset %rbx, -32
.Lcfi241:
	.cfi_offset %r14, -24
.Lcfi242:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB47_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB47_3
.LBB47_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13RwDeclarationEPNS_20impl_outmostpatternsEPNS_19impl_rewriteclausesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB47_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end47:
	.size	_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE, .Lfunc_end47-_ZN2kc32impl_rwdeclaration_RwDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE: # @_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 32
.Lcfi246:
	.cfi_offset %rbx, -32
.Lcfi247:
	.cfi_offset %r14, -24
.Lcfi248:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB48_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB48_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB48_4
.LBB48_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsrewriteclausesEPNS_18impl_rewriteclauseEPNS_19impl_rewriteclausesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB48_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end48:
	.size	_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE, .Lfunc_end48-_ZN2kc19impl_rewriteclauses7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE: # @_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 32
.Lcfi252:
	.cfi_offset %rbx, -32
.Lcfi253:
	.cfi_offset %r14, -24
.Lcfi254:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB49_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB49_3
.LBB49_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13RewriteClauseEPNS_14impl_viewnamesEPNS_9impl_termE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB49_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE, .Lfunc_end49-_ZN2kc32impl_rewriteclause_RewriteClause7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE: # @_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi255:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi256:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi257:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi258:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 48
.Lcfi260:
	.cfi_offset %rbx, -48
.Lcfi261:
	.cfi_offset %r12, -40
.Lcfi262:
	.cfi_offset %r13, -32
.Lcfi263:
	.cfi_offset %r14, -24
.Lcfi264:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	(%r13), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB50_16
# BB#1:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB50_7
# BB#2:
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB50_7
# BB#3:
	movq	32(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB50_7
# BB#4:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB50_7
# BB#5:
	movq	24(%r15), %rax
	movq	32(%rax), %rdi
	cmpq	%r13, %rdi
	je	.LBB50_16
# BB#6:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB50_7:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB50_13
# BB#8:
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB50_13
# BB#9:
	movq	32(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB50_13
# BB#10:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB50_13
# BB#11:
	movq	24(%r15), %rax
	movq	32(%rax), %rdi
	movq	%r12, %rsi
	callq	_ZN2kc6concatEPKNS_18impl_patternchainsES2_
	cmpq	%r13, %rax
	je	.LBB50_16
# BB#12:
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rcx                   # TAILCALL
.LBB50_13:
	cmpq	24(%r13), %r15
	jne	.LBB50_15
# BB#14:
	cmpq	32(%r13), %r12
	je	.LBB50_16
.LBB50_15:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc17ConspatternchainsEPNS_17impl_patternchainEPNS_18impl_patternchainsE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB50_16:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end50:
	.size	_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE, .Lfunc_end50-_ZN2kc18impl_patternchains7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE,@function
_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE: # @_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 48
.Lcfi270:
	.cfi_offset %rbx, -48
.Lcfi271:
	.cfi_offset %r12, -40
.Lcfi272:
	.cfi_offset %r13, -32
.Lcfi273:
	.cfi_offset %r14, -24
.Lcfi274:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB51_65
# BB#1:
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %rbx
	movq	32(%r15), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_10
# BB#2:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_10
# BB#3:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_10
# BB#4:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_10
# BB#5:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_10
# BB#6:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_10
# BB#7:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_10
# BB#8:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB51_10
# BB#9:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movq	32(%rax), %r13
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	24(%rax), %rbx
	movq	%rbx, %rsi
	callq	_ZN2kc6concatEPKNS_17impl_patternchainES2_
	movq	%rax, %r12
	movq	%r13, %rdi
	callq	_ZN2kc21PatternchainitemGroupEPNS_18impl_patternchainsE
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	movq	%rax, %rbx
	callq	_ZN2kc16NilpatternchainsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConspatternchainsEPNS_17impl_patternchainEPNS_18impl_patternchainsE
	movq	%r12, %rdi
	jmp	.LBB51_53
.LBB51_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_18
# BB#11:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_18
# BB#12:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_18
# BB#13:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_18
# BB#14:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_18
# BB#15:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_18
# BB#16:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_18
# BB#17:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	je	.LBB51_39
.LBB51_18:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_27
# BB#19:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_27
# BB#20:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_27
# BB#21:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_27
# BB#22:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_27
# BB#23:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_27
# BB#24:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_27
# BB#25:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB51_27
# BB#26:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	24(%rax), %rsi
	jmp	.LBB51_45
.LBB51_27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_35
# BB#28:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_35
# BB#29:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_35
# BB#30:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_35
# BB#31:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_35
# BB#32:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_35
# BB#33:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_35
# BB#34:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	je	.LBB51_44
.LBB51_35:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_40
# BB#36:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_40
# BB#37:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_40
# BB#38:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_40
.LBB51_39:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movq	32(%rax), %rbx
	movq	%r12, %rsi
	callq	_ZN2kc6concatEPKNS_17impl_patternchainES2_
	movq	%rax, %r13
	movq	%rbx, %rdi
	callq	_ZN2kc21PatternchainitemGroupEPNS_18impl_patternchainsE
	movq	%rax, %rdi
	movq	%r12, %rsi
	jmp	.LBB51_52
.LBB51_40:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_46
# BB#41:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_46
# BB#42:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_46
# BB#43:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_46
.LBB51_44:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movq	%r12, %rsi
.LBB51_45:
	callq	_ZN2kc6concatEPKNS_17impl_patternchainES2_
	cmpq	%r15, %rax
	jne	.LBB51_61
	jmp	.LBB51_65
.LBB51_46:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_54
# BB#47:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_54
# BB#48:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_54
# BB#49:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_54
# BB#50:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB51_54
# BB#51:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %r12
	movq	%rbx, %rdi
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	movq	%rax, %r13
	movq	%r12, %rdi
	callq	_ZN2kc21PatternchainitemGroupEPNS_18impl_patternchainsE
	movq	%rax, %r12
	callq	_ZN2kc15NilpatternchainEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	movq	%rbx, %rdi
	movq	%rax, %rsi
.LBB51_52:
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	movq	%rax, %rbx
	callq	_ZN2kc16NilpatternchainsEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConspatternchainsEPNS_17impl_patternchainEPNS_18impl_patternchainsE
	movq	%r13, %rdi
.LBB51_53:
	movq	%rax, %rsi
	callq	_ZN2kc17ConspatternchainsEPNS_17impl_patternchainEPNS_18impl_patternchainsE
	movq	%rax, %rdi
	callq	_ZN2kc21PatternchainitemGroupEPNS_18impl_patternchainsE
	movq	%rax, %rbx
	callq	_ZN2kc15NilpatternchainEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	jmp	.LBB51_60
.LBB51_54:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB51_62
# BB#55:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB51_62
# BB#56:
	movq	24(%r12), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB51_62
# BB#57:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB51_62
# BB#58:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB51_62
# BB#59:
	movq	24(%r12), %rax
	movq	32(%rax), %rax
	movq	24(%rax), %rsi
	movq	%rbx, %rdi
.LBB51_60:
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	cmpq	%r15, %rax
	je	.LBB51_65
.LBB51_61:
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rcx                   # TAILCALL
.LBB51_62:
	cmpq	24(%r15), %rbx
	jne	.LBB51_64
# BB#63:
	cmpq	32(%r15), %r12
	je	.LBB51_65
.LBB51_64:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc16ConspatternchainEPNS_21impl_patternchainitemEPNS_17impl_patternchainE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*40(%rax)
	movq	%rbx, %r15
.LBB51_65:                              # %.thread
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end51:
	.size	_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE, .Lfunc_end51-_ZN2kc17impl_patternchain7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE,@function
_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE: # @_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 32
.Lcfi278:
	.cfi_offset %rbx, -32
.Lcfi279:
	.cfi_offset %r14, -24
.Lcfi280:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB52_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB52_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB52_4
.LBB52_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc19ConsoutmostpatternsEPNS_19impl_outmostpatternEPNS_20impl_outmostpatternsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB52_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end52:
	.size	_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE, .Lfunc_end52-_ZN2kc20impl_outmostpatterns7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE,@function
_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE: # @_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi283:
	.cfi_def_cfa_offset 32
.Lcfi284:
	.cfi_offset %rbx, -24
.Lcfi285:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB53_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc24PatternchainitemDollaridEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB53_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end53:
	.size	_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE, .Lfunc_end53-_ZN2kc46impl_patternchainitem_PatternchainitemDollarid7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE,@function
_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE: # @_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi286:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 32
.Lcfi289:
	.cfi_offset %rbx, -32
.Lcfi290:
	.cfi_offset %r14, -24
.Lcfi291:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	32(%r15), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB54_6
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB54_6
# BB#2:
	movq	24(%rbx), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB54_6
# BB#3:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB54_6
# BB#4:
	movq	24(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%r15, %rdi
	je	.LBB54_8
# BB#5:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB54_6:
	cmpq	32(%r15), %rbx
	je	.LBB54_8
# BB#7:
	movq	%rbx, %rdi
	callq	_ZN2kc21PatternchainitemGroupEPNS_18impl_patternchainsE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*40(%rax)
	movq	%rbx, %r15
.LBB54_8:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end54:
	.size	_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE, .Lfunc_end54-_ZN2kc43impl_patternchainitem_PatternchainitemGroup7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE,@function
_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE: # @_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -24
.Lcfi296:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB55_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc23PatternchainitemOutmostEPNS_19impl_outmostpatternE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB55_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE, .Lfunc_end55-_ZN2kc45impl_patternchainitem_PatternchainitemOutmost7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE: # @_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi297:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi298:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi299:
	.cfi_def_cfa_offset 32
.Lcfi300:
	.cfi_offset %rbx, -24
.Lcfi301:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB56_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc9OPDefaultEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB56_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end56:
	.size	_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE, .Lfunc_end56-_ZN2kc29impl_outmostpattern_OPDefault7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE: # @_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi304:
	.cfi_def_cfa_offset 32
.Lcfi305:
	.cfi_offset %rbx, -24
.Lcfi306:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB57_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10OPWildcardEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB57_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end57:
	.size	_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE, .Lfunc_end57-_ZN2kc30impl_outmostpattern_OPWildcard7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE: # @_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 32
.Lcfi310:
	.cfi_offset %rbx, -32
.Lcfi311:
	.cfi_offset %r14, -24
.Lcfi312:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB58_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB58_3
.LBB58_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17OPNonLeafVariableEPNS_7impl_IDEPNS_19impl_outmostpatternE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB58_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end58:
	.size	_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE, .Lfunc_end58-_ZN2kc37impl_outmostpattern_OPNonLeafVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE: # @_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi313:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi314:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi315:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi316:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi317:
	.cfi_def_cfa_offset 48
.Lcfi318:
	.cfi_offset %rbx, -48
.Lcfi319:
	.cfi_offset %r12, -40
.Lcfi320:
	.cfi_offset %r13, -32
.Lcfi321:
	.cfi_offset %r14, -24
.Lcfi322:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	32(%r13), %r14
	jne	.LBB59_3
# BB#1:
	cmpq	40(%r13), %r15
	jne	.LBB59_3
# BB#2:
	cmpq	48(%r13), %rax
	je	.LBB59_4
.LBB59_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc10OPOperatorEPNS_7impl_IDEPNS_13impl_patternsEPNS_16impl_CexpressionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB59_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end59:
	.size	_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE, .Lfunc_end59-_ZN2kc30impl_outmostpattern_OPOperator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE: # @_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi323:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi324:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi325:
	.cfi_def_cfa_offset 32
.Lcfi326:
	.cfi_offset %rbx, -32
.Lcfi327:
	.cfi_offset %r14, -24
.Lcfi328:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB60_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB60_3
.LBB60_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18OPOperatorWildcardEPNS_7impl_IDEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB60_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end60:
	.size	_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE, .Lfunc_end60-_ZN2kc38impl_outmostpattern_OPOperatorWildcard7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE: # @_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi331:
	.cfi_def_cfa_offset 32
.Lcfi332:
	.cfi_offset %rbx, -24
.Lcfi333:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB61_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc11PIntLiteralEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB61_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end61:
	.size	_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE, .Lfunc_end61-_ZN2kc24impl_pattern_PIntLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE: # @_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi336:
	.cfi_def_cfa_offset 32
.Lcfi337:
	.cfi_offset %rbx, -24
.Lcfi338:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB62_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14PStringLiteralEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB62_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end62:
	.size	_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE, .Lfunc_end62-_ZN2kc27impl_pattern_PStringLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE: # @_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end63:
	.size	_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE, .Lfunc_end63-_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE: # @_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi341:
	.cfi_def_cfa_offset 32
.Lcfi342:
	.cfi_offset %rbx, -32
.Lcfi343:
	.cfi_offset %r14, -24
.Lcfi344:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB64_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB64_3
.LBB64_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16PNonLeafVariableEPNS_7impl_IDEPNS_12impl_patternE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB64_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end64:
	.size	_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE, .Lfunc_end64-_ZN2kc29impl_pattern_PNonLeafVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE: # @_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi345:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi346:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi347:
	.cfi_def_cfa_offset 32
.Lcfi348:
	.cfi_offset %rbx, -32
.Lcfi349:
	.cfi_offset %r14, -24
.Lcfi350:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB65_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB65_3
.LBB65_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9POperatorEPNS_7impl_IDEPNS_13impl_patternsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB65_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end65:
	.size	_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE, .Lfunc_end65-_ZN2kc22impl_pattern_POperator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE: # @_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi351:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi352:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi353:
	.cfi_def_cfa_offset 32
.Lcfi354:
	.cfi_offset %rbx, -24
.Lcfi355:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB66_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc9PVariableEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB66_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end66:
	.size	_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE, .Lfunc_end66-_ZN2kc22impl_pattern_PVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc13impl_patterns7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc13impl_patterns7rewriteERNS_11rview_classE,@function
_ZN2kc13impl_patterns7rewriteERNS_11rview_classE: # @_ZN2kc13impl_patterns7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi356:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi357:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi358:
	.cfi_def_cfa_offset 32
.Lcfi359:
	.cfi_offset %rbx, -32
.Lcfi360:
	.cfi_offset %r14, -24
.Lcfi361:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB67_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB67_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB67_4
.LBB67_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc12ConspatternsEPNS_12impl_patternEPNS_13impl_patternsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB67_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end67:
	.size	_ZN2kc13impl_patterns7rewriteERNS_11rview_classE, .Lfunc_end67-_ZN2kc13impl_patterns7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE: # @_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi362:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi363:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi364:
	.cfi_def_cfa_offset 32
.Lcfi365:
	.cfi_offset %rbx, -24
.Lcfi366:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB68_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc11TIntLiteralEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB68_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end68:
	.size	_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE, .Lfunc_end68-_ZN2kc21impl_term_TIntLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE: # @_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi367:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi368:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi369:
	.cfi_def_cfa_offset 32
.Lcfi370:
	.cfi_offset %rbx, -24
.Lcfi371:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB69_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14TStringLiteralEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB69_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end69:
	.size	_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE, .Lfunc_end69-_ZN2kc24impl_term_TStringLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE,@function
_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE: # @_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi372:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi373:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi374:
	.cfi_def_cfa_offset 32
.Lcfi375:
	.cfi_offset %rbx, -24
.Lcfi376:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB70_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc6TCTermEPNS_18impl_CexpressionSQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB70_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end70:
	.size	_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE, .Lfunc_end70-_ZN2kc16impl_term_TCTerm7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE: # @_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi377:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi378:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi379:
	.cfi_def_cfa_offset 32
.Lcfi380:
	.cfi_offset %rbx, -32
.Lcfi381:
	.cfi_offset %r14, -24
.Lcfi382:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r14
	jne	.LBB71_2
# BB#1:
	cmpq	32(%rbx), %rax
	je	.LBB71_3
.LBB71_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13TMemberVarDotEPNS_9impl_termEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB71_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end71:
	.size	_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE, .Lfunc_end71-_ZN2kc23impl_term_TMemberVarDot7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE,@function
_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE: # @_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi383:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi384:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi385:
	.cfi_def_cfa_offset 32
.Lcfi386:
	.cfi_offset %rbx, -32
.Lcfi387:
	.cfi_offset %r14, -24
.Lcfi388:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r14
	jne	.LBB72_2
# BB#1:
	cmpq	32(%rbx), %rax
	je	.LBB72_3
.LBB72_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10TMemberVarEPNS_9impl_termEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB72_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end72:
	.size	_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE, .Lfunc_end72-_ZN2kc20impl_term_TMemberVar7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE,@function
_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE: # @_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi389:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi390:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi391:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi392:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi393:
	.cfi_def_cfa_offset 48
.Lcfi394:
	.cfi_offset %rbx, -48
.Lcfi395:
	.cfi_offset %r12, -40
.Lcfi396:
	.cfi_offset %r13, -32
.Lcfi397:
	.cfi_offset %r14, -24
.Lcfi398:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	24(%r13), %r14
	jne	.LBB73_3
# BB#1:
	cmpq	32(%r13), %r15
	jne	.LBB73_3
# BB#2:
	cmpq	40(%r13), %rax
	je	.LBB73_4
.LBB73_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc10TMethodDotEPNS_9impl_termEPNS_7impl_IDEPNS_10impl_termsE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB73_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end73:
	.size	_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE, .Lfunc_end73-_ZN2kc20impl_term_TMethodDot7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE,@function
_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE: # @_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi399:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi400:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi401:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi402:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi403:
	.cfi_def_cfa_offset 48
.Lcfi404:
	.cfi_offset %rbx, -48
.Lcfi405:
	.cfi_offset %r12, -40
.Lcfi406:
	.cfi_offset %r13, -32
.Lcfi407:
	.cfi_offset %r14, -24
.Lcfi408:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	24(%r13), %r14
	jne	.LBB74_3
# BB#1:
	cmpq	32(%r13), %r15
	jne	.LBB74_3
# BB#2:
	cmpq	40(%r13), %rax
	je	.LBB74_4
.LBB74_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc7TMethodEPNS_9impl_termEPNS_7impl_IDEPNS_10impl_termsE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB74_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end74:
	.size	_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE, .Lfunc_end74-_ZN2kc17impl_term_TMethod7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE: # @_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi409:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi410:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi411:
	.cfi_def_cfa_offset 32
.Lcfi412:
	.cfi_offset %rbx, -32
.Lcfi413:
	.cfi_offset %r14, -24
.Lcfi414:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r14
	jne	.LBB75_2
# BB#1:
	cmpq	32(%rbx), %rax
	je	.LBB75_3
.LBB75_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9TOperatorEPNS_7impl_IDEPNS_10impl_termsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB75_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end75:
	.size	_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE, .Lfunc_end75-_ZN2kc19impl_term_TOperator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE: # @_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi415:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi416:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi417:
	.cfi_def_cfa_offset 32
.Lcfi418:
	.cfi_offset %rbx, -24
.Lcfi419:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB76_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc9TVariableEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB76_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end76:
	.size	_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE, .Lfunc_end76-_ZN2kc19impl_term_TVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc10impl_terms7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc10impl_terms7rewriteERNS_11rview_classE,@function
_ZN2kc10impl_terms7rewriteERNS_11rview_classE: # @_ZN2kc10impl_terms7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi420:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi421:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi422:
	.cfi_def_cfa_offset 32
.Lcfi423:
	.cfi_offset %rbx, -32
.Lcfi424:
	.cfi_offset %r14, -24
.Lcfi425:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB77_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB77_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB77_4
.LBB77_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConstermsEPNS_9impl_termEPNS_10impl_termsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB77_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end77:
	.size	_ZN2kc10impl_terms7rewriteERNS_11rview_classE, .Lfunc_end77-_ZN2kc10impl_terms7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE,@function
_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE: # @_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi426:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi427:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi428:
	.cfi_def_cfa_offset 32
.Lcfi429:
	.cfi_offset %rbx, -32
.Lcfi430:
	.cfi_offset %r14, -24
.Lcfi431:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB78_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB78_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB78_4
.LBB78_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11ConsfnfilesEPNS_11impl_fnfileEPNS_12impl_fnfilesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB78_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end78:
	.size	_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE, .Lfunc_end78-_ZN2kc12impl_fnfiles7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE: # @_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi432:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi433:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi434:
	.cfi_def_cfa_offset 32
.Lcfi435:
	.cfi_offset %rbx, -24
.Lcfi436:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	16(%rbx), %rax
	je	.LBB79_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc6FnFileEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB79_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end79:
	.size	_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE, .Lfunc_end79-_ZN2kc18impl_fnfile_FnFile7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE: # @_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi437:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi438:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi439:
	.cfi_def_cfa_offset 32
.Lcfi440:
	.cfi_offset %rbx, -32
.Lcfi441:
	.cfi_offset %r14, -24
.Lcfi442:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB80_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB80_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB80_4
.LBB80_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsfndeclarationsEPNS_18impl_fndeclarationEPNS_19impl_fndeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB80_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end80:
	.size	_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE, .Lfunc_end80-_ZN2kc19impl_fndeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi443:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi444:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi445:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi446:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi447:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi448:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi449:
	.cfi_def_cfa_offset 64
.Lcfi450:
	.cfi_offset %rbx, -56
.Lcfi451:
	.cfi_offset %r12, -48
.Lcfi452:
	.cfi_offset %r13, -40
.Lcfi453:
	.cfi_offset %r14, -32
.Lcfi454:
	.cfi_offset %r15, -24
.Lcfi455:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	40(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	48(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	56(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	64(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	40(%rbp), %r14
	jne	.LBB81_4
# BB#1:
	cmpq	48(%rbp), %r15
	jne	.LBB81_4
# BB#2:
	cmpq	56(%rbp), %r12
	jne	.LBB81_4
# BB#3:
	cmpq	64(%rbp), %rax
	je	.LBB81_5
.LBB81_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc19AcMemberDeclarationEPNS_30impl_ac_declaration_specifiersEPNS_18impl_ac_declaratorEPNS_34impl_ac_constant_expression_optionEPNS_12impl_fnclassE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB81_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end81:
	.size	_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE, .Lfunc_end81-_ZN2kc38impl_fndeclaration_AcMemberDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi456:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi457:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi458:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi459:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi460:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi461:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi462:
	.cfi_def_cfa_offset 80
.Lcfi463:
	.cfi_offset %rbx, -56
.Lcfi464:
	.cfi_offset %r12, -48
.Lcfi465:
	.cfi_offset %r13, -40
.Lcfi466:
	.cfi_offset %r14, -32
.Lcfi467:
	.cfi_offset %r15, -24
.Lcfi468:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	80(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	%rax, %r13
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*24(%rax)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpq	40(%rbx), %rdi
	jne	.LBB82_7
# BB#1:
	cmpq	48(%rbx), %rsi
	jne	.LBB82_7
# BB#2:
	cmpq	56(%rbx), %r12
	jne	.LBB82_7
# BB#3:
	cmpq	64(%rbx), %r14
	jne	.LBB82_7
# BB#4:
	cmpq	72(%rbx), %r15
	jne	.LBB82_7
# BB#5:
	cmpq	80(%rbx), %r13
	jne	.LBB82_7
# BB#6:
	cmpq	88(%rbx), %rax
	je	.LBB82_8
.LBB82_7:
	movq	%rax, (%rsp)
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%r13, %r9
	callq	_ZN2kc15FnAcDeclarationEPNS_30impl_ac_declaration_specifiersEPNS_18impl_ac_declaratorEPNS_24impl_ac_declaration_listEPNS_26impl_ac_opt_base_init_listEPNS_10impl_CtextEPNS_7impl_IDEPNS_12impl_fnclassE
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB82_8:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end82:
	.size	_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE, .Lfunc_end82-_ZN2kc34impl_fndeclaration_FnAcDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE: # @_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end83:
	.size	_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE, .Lfunc_end83-_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE,@function
_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE: # @_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end84:
	.size	_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE, .Lfunc_end84-_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE: # @_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end85:
	.size	_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE, .Lfunc_end85-_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE: # @_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end86:
	.size	_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE, .Lfunc_end86-_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE: # @_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi469:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi470:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi471:
	.cfi_def_cfa_offset 32
.Lcfi472:
	.cfi_offset %rbx, -24
.Lcfi473:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB87_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc8StaticFnEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB87_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end87:
	.size	_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE, .Lfunc_end87-_ZN2kc21impl_fnclass_StaticFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE: # @_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end88:
	.size	_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE, .Lfunc_end88-_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE,@function
_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE: # @_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi474:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi475:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi476:
	.cfi_def_cfa_offset 32
.Lcfi477:
	.cfi_offset %rbx, -32
.Lcfi478:
	.cfi_offset %r14, -24
.Lcfi479:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB89_4
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r15
	jne	.LBB89_3
# BB#2:
	cmpq	32(%rbx), %rax
	je	.LBB89_4
.LBB89_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConsCtextEPNS_15impl_Ctext_elemEPNS_10impl_CtextE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB89_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end89:
	.size	_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE, .Lfunc_end89-_ZN2kc10impl_Ctext7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE,@function
_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE: # @_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi480:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi481:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi482:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi483:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi484:
	.cfi_def_cfa_offset 48
.Lcfi485:
	.cfi_offset %rbx, -48
.Lcfi486:
	.cfi_offset %r12, -40
.Lcfi487:
	.cfi_offset %r13, -32
.Lcfi488:
	.cfi_offset %r14, -24
.Lcfi489:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	24(%r13), %r14
	jne	.LBB90_3
# BB#1:
	cmpq	32(%r13), %r15
	jne	.LBB90_3
# BB#2:
	cmpq	40(%r13), %rax
	je	.LBB90_4
.LBB90_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc19CTextWithexpressionEPNS_20impl_withexpressionsEPNS_14impl_withcasesEPNS_16impl_contextinfoE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB90_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end90:
	.size	_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE, .Lfunc_end90-_ZN2kc35impl_Ctext_elem_CTextWithexpression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE: # @_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi490:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi491:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi492:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi493:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi494:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi495:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi496:
	.cfi_def_cfa_offset 64
.Lcfi497:
	.cfi_offset %rbx, -56
.Lcfi498:
	.cfi_offset %r12, -48
.Lcfi499:
	.cfi_offset %r13, -40
.Lcfi500:
	.cfi_offset %r14, -32
.Lcfi501:
	.cfi_offset %r15, -24
.Lcfi502:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r14
	jne	.LBB91_5
# BB#1:
	cmpq	32(%rbx), %r15
	jne	.LBB91_5
# BB#2:
	cmpq	40(%rbx), %r12
	jne	.LBB91_5
# BB#3:
	cmpq	48(%rbx), %rbp
	jne	.LBB91_5
# BB#4:
	cmpq	56(%rbx), %rax
	je	.LBB91_6
.LBB91_5:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r8
	callq	_ZN2kc22CTextForeachexpressionEPNS_17impl_patternchainEPNS_19impl_idCexpressionsEPNS_20impl_withexpressionsEPNS_10impl_CtextEPNS_18impl_foreach_afterE
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB91_6:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end91:
	.size	_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE, .Lfunc_end91-_ZN2kc38impl_Ctext_elem_CTextForeachexpression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE: # @_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi503:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi504:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi505:
	.cfi_def_cfa_offset 32
.Lcfi506:
	.cfi_offset %rbx, -24
.Lcfi507:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB92_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10CTextCbodyEPNS_10impl_CtextE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB92_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end92:
	.size	_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE, .Lfunc_end92-_ZN2kc26impl_Ctext_elem_CTextCbody7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE: # @_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi508:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi509:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi510:
	.cfi_def_cfa_offset 32
.Lcfi511:
	.cfi_offset %rbx, -24
.Lcfi512:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB93_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18CTextCexpressionSQEPNS_18impl_CexpressionSQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB93_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end93:
	.size	_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE, .Lfunc_end93-_ZN2kc34impl_Ctext_elem_CTextCexpressionSQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE: # @_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi513:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi514:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi515:
	.cfi_def_cfa_offset 32
.Lcfi516:
	.cfi_offset %rbx, -24
.Lcfi517:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB94_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18CTextCexpressionDQEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB94_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end94:
	.size	_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE, .Lfunc_end94-_ZN2kc34impl_Ctext_elem_CTextCexpressionDQ7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE: # @_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi518:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi519:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi520:
	.cfi_def_cfa_offset 32
.Lcfi521:
	.cfi_offset %rbx, -24
.Lcfi522:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB95_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc7CTextNlEPNS_17impl_integer__IntE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB95_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end95:
	.size	_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE, .Lfunc_end95-_ZN2kc23impl_Ctext_elem_CTextNl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE: # @_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi523:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi524:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi525:
	.cfi_def_cfa_offset 32
.Lcfi526:
	.cfi_offset %rbx, -24
.Lcfi527:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB96_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14CTextDollarVarEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB96_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end96:
	.size	_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE, .Lfunc_end96-_ZN2kc30impl_Ctext_elem_CTextDollarVar7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE,@function
_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE: # @_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi528:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi529:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi530:
	.cfi_def_cfa_offset 32
.Lcfi531:
	.cfi_offset %rbx, -24
.Lcfi532:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB97_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc9CTextLineEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB97_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end97:
	.size	_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE, .Lfunc_end97-_ZN2kc25impl_Ctext_elem_CTextLine7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE,@function
_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE: # @_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi533:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi534:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi535:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi536:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi537:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi538:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi539:
	.cfi_def_cfa_offset 64
.Lcfi540:
	.cfi_offset %rbx, -56
.Lcfi541:
	.cfi_offset %r12, -48
.Lcfi542:
	.cfi_offset %r13, -40
.Lcfi543:
	.cfi_offset %r14, -32
.Lcfi544:
	.cfi_offset %r15, -24
.Lcfi545:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	48(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	24(%rbp), %r14
	jne	.LBB98_4
# BB#1:
	cmpq	32(%rbp), %r15
	jne	.LBB98_4
# BB#2:
	cmpq	40(%rbp), %r12
	jne	.LBB98_4
# BB#3:
	cmpq	48(%rbp), %rax
	je	.LBB98_5
.LBB98_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc12ForeachAfterEPNS_17impl_patternchainEPNS_19impl_idCexpressionsEPNS_20impl_withexpressionsEPNS_10impl_CtextE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB98_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end98:
	.size	_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE, .Lfunc_end98-_ZN2kc31impl_foreach_after_ForeachAfter7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE: # @_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end99:
	.size	_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE, .Lfunc_end99-_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE: # @_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end100:
	.size	_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE, .Lfunc_end100-_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE: # @_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi546:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi547:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi548:
	.cfi_def_cfa_offset 32
.Lcfi549:
	.cfi_offset %rbx, -24
.Lcfi550:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB101_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16InForeachContextEPNS_17impl_patternchainE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB101_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end101:
	.size	_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE, .Lfunc_end101-_ZN2kc33impl_contextinfo_InForeachContext7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE,@function
_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE: # @_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi551:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi552:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi553:
	.cfi_def_cfa_offset 32
.Lcfi554:
	.cfi_offset %rbx, -32
.Lcfi555:
	.cfi_offset %r14, -24
.Lcfi556:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB102_4
# BB#1:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r15
	jne	.LBB102_3
# BB#2:
	cmpq	40(%rbx), %rax
	je	.LBB102_4
.LBB102_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc19ConswithexpressionsEPNS_19impl_withexpressionEPNS_20impl_withexpressionsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB102_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end102:
	.size	_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE, .Lfunc_end102-_ZN2kc20impl_withexpressions7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE: # @_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi557:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi558:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi559:
	.cfi_def_cfa_offset 32
.Lcfi560:
	.cfi_offset %rbx, -24
.Lcfi561:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB103_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13WECexpressionEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB103_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end103:
	.size	_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE, .Lfunc_end103-_ZN2kc33impl_withexpression_WECexpression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE: # @_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi562:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi563:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi564:
	.cfi_def_cfa_offset 32
.Lcfi565:
	.cfi_offset %rbx, -24
.Lcfi566:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB104_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10WEVariableEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB104_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end104:
	.size	_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE, .Lfunc_end104-_ZN2kc30impl_withexpression_WEVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc14impl_withcases7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc14impl_withcases7rewriteERNS_11rview_classE,@function
_ZN2kc14impl_withcases7rewriteERNS_11rview_classE: # @_ZN2kc14impl_withcases7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi567:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi568:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi569:
	.cfi_def_cfa_offset 32
.Lcfi570:
	.cfi_offset %rbx, -32
.Lcfi571:
	.cfi_offset %r14, -24
.Lcfi572:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB105_4
# BB#1:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r15
	jne	.LBB105_3
# BB#2:
	cmpq	24(%rbx), %rax
	je	.LBB105_4
.LBB105_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConswithcasesEPNS_13impl_withcaseEPNS_14impl_withcasesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB105_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end105:
	.size	_ZN2kc14impl_withcases7rewriteERNS_11rview_classE, .Lfunc_end105-_ZN2kc14impl_withcases7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE: # @_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi573:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi574:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi575:
	.cfi_def_cfa_offset 32
.Lcfi576:
	.cfi_offset %rbx, -32
.Lcfi577:
	.cfi_offset %r14, -24
.Lcfi578:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r14
	jne	.LBB106_2
# BB#1:
	cmpq	24(%rbx), %rax
	je	.LBB106_3
.LBB106_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8WithcaseEPNS_18impl_patternchainsEPNS_10impl_CtextE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB106_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end106:
	.size	_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE, .Lfunc_end106-_ZN2kc22impl_withcase_Withcase7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE: # @_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi579:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi580:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi581:
	.cfi_def_cfa_offset 32
.Lcfi582:
	.cfi_offset %rbx, -32
.Lcfi583:
	.cfi_offset %r14, -24
.Lcfi584:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB107_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB107_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB107_4
.LBB107_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc23ConsunparsedeclarationsEPNS_23impl_unparsedeclarationEPNS_24impl_unparsedeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB107_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end107:
	.size	_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE, .Lfunc_end107-_ZN2kc24impl_unparsedeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi585:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi586:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi587:
	.cfi_def_cfa_offset 32
.Lcfi588:
	.cfi_offset %rbx, -32
.Lcfi589:
	.cfi_offset %r14, -24
.Lcfi590:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r14
	jne	.LBB108_2
# BB#1:
	cmpq	24(%rbx), %rax
	je	.LBB108_3
.LBB108_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18UnparseDeclarationEPNS_20impl_outmostpatternsEPNS_19impl_unparseclausesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB108_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end108:
	.size	_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE, .Lfunc_end108-_ZN2kc42impl_unparsedeclaration_UnparseDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE: # @_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi591:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi592:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi593:
	.cfi_def_cfa_offset 32
.Lcfi594:
	.cfi_offset %rbx, -32
.Lcfi595:
	.cfi_offset %r14, -24
.Lcfi596:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB109_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB109_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB109_4
.LBB109_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsunparseclausesEPNS_18impl_unparseclauseEPNS_19impl_unparseclausesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB109_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end109:
	.size	_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE, .Lfunc_end109-_ZN2kc19impl_unparseclauses7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE: # @_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi597:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi598:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi599:
	.cfi_def_cfa_offset 32
.Lcfi600:
	.cfi_offset %rbx, -32
.Lcfi601:
	.cfi_offset %r14, -24
.Lcfi602:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB110_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB110_3
.LBB110_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13UnparseClauseEPNS_14impl_viewnamesEPNS_17impl_unparseitemsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB110_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end110:
	.size	_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE, .Lfunc_end110-_ZN2kc32impl_unparseclause_UnparseClause7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE,@function
_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE: # @_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi603:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi604:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi605:
	.cfi_def_cfa_offset 32
.Lcfi606:
	.cfi_offset %rbx, -32
.Lcfi607:
	.cfi_offset %r14, -24
.Lcfi608:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB111_4
# BB#1:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r15
	jne	.LBB111_3
# BB#2:
	cmpq	24(%rbx), %rax
	je	.LBB111_4
.LBB111_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB111_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end111:
	.size	_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE, .Lfunc_end111-_ZN2kc14impl_viewnames7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE,@function
_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE: # @_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi609:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi610:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi611:
	.cfi_def_cfa_offset 32
.Lcfi612:
	.cfi_offset %rbx, -32
.Lcfi613:
	.cfi_offset %r14, -24
.Lcfi614:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB112_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB112_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB112_4
.LBB112_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsunparseitemsEPNS_16impl_unparseitemEPNS_17impl_unparseitemsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB112_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end112:
	.size	_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE, .Lfunc_end112-_ZN2kc17impl_unparseitems7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE: # @_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi615:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi616:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi617:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi618:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi619:
	.cfi_def_cfa_offset 48
.Lcfi620:
	.cfi_offset %rbx, -48
.Lcfi621:
	.cfi_offset %r12, -40
.Lcfi622:
	.cfi_offset %r13, -32
.Lcfi623:
	.cfi_offset %r14, -24
.Lcfi624:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	16(%r13), %r14
	jne	.LBB113_3
# BB#1:
	cmpq	24(%r13), %r15
	jne	.LBB113_3
# BB#2:
	cmpq	32(%r13), %rax
	je	.LBB113_4
.LBB113_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12UViewVarDeclEPNS_7impl_IDES1_PNS_16impl_CexpressionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB113_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end113:
	.size	_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE, .Lfunc_end113-_ZN2kc29impl_unparseitem_UViewVarDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE: # @_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi625:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi626:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi627:
	.cfi_def_cfa_offset 32
.Lcfi628:
	.cfi_offset %rbx, -32
.Lcfi629:
	.cfi_offset %r14, -24
.Lcfi630:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r14
	jne	.LBB114_2
# BB#1:
	cmpq	24(%rbx), %rax
	je	.LBB114_3
.LBB114_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7UnpBodyEPNS_19impl_languageoptionEPNS_17impl_unparseitemsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB114_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end114:
	.size	_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE, .Lfunc_end114-_ZN2kc24impl_unparseitem_UnpBody7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE,@function
_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE: # @_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi631:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi632:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi633:
	.cfi_def_cfa_offset 32
.Lcfi634:
	.cfi_offset %rbx, -32
.Lcfi635:
	.cfi_offset %r14, -24
.Lcfi636:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	16(%rbx), %r14
	jne	.LBB115_2
# BB#1:
	cmpq	24(%rbx), %rax
	je	.LBB115_3
.LBB115_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8UnpCtextEPNS_19impl_languageoptionEPNS_10impl_CtextE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB115_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end115:
	.size	_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE, .Lfunc_end115-_ZN2kc25impl_unparseitem_UnpCtext7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE: # @_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi637:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi638:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi639:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi640:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi641:
	.cfi_def_cfa_offset 48
.Lcfi642:
	.cfi_offset %rbx, -48
.Lcfi643:
	.cfi_offset %r12, -40
.Lcfi644:
	.cfi_offset %r13, -32
.Lcfi645:
	.cfi_offset %r14, -24
.Lcfi646:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	16(%r13), %r14
	jne	.LBB116_3
# BB#1:
	cmpq	24(%r13), %r15
	jne	.LBB116_3
# BB#2:
	cmpq	32(%r13), %rax
	je	.LBB116_4
.LBB116_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc10UnpSubexprEPNS_19impl_languageoptionEPNS_15impl_unpsubtermEPNS_19impl_viewnameoptionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB116_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end116:
	.size	_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE, .Lfunc_end116-_ZN2kc27impl_unparseitem_UnpSubexpr7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE: # @_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi647:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi648:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi649:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi650:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi651:
	.cfi_def_cfa_offset 48
.Lcfi652:
	.cfi_offset %rbx, -48
.Lcfi653:
	.cfi_offset %r12, -40
.Lcfi654:
	.cfi_offset %r13, -32
.Lcfi655:
	.cfi_offset %r14, -24
.Lcfi656:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	16(%r13), %r14
	jne	.LBB117_3
# BB#1:
	cmpq	24(%r13), %r15
	jne	.LBB117_3
# BB#2:
	cmpq	32(%r13), %rax
	je	.LBB117_4
.LBB117_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc6UnpStrEPNS_19impl_languageoptionEPNS_18impl_CexpressionDQEPNS_19impl_viewnameoptionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB117_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end117:
	.size	_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE, .Lfunc_end117-_ZN2kc23impl_unparseitem_UnpStr7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE: # @_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi657:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi658:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi659:
	.cfi_def_cfa_offset 32
.Lcfi660:
	.cfi_offset %rbx, -32
.Lcfi661:
	.cfi_offset %r14, -24
.Lcfi662:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB118_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB118_3
.LBB118_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17UnpCastedVariableEPNS_7impl_IDES1_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB118_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end118:
	.size	_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE, .Lfunc_end118-_ZN2kc33impl_unpsubterm_UnpCastedVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE: # @_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi663:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi664:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi665:
	.cfi_def_cfa_offset 32
.Lcfi666:
	.cfi_offset %rbx, -32
.Lcfi667:
	.cfi_offset %r14, -24
.Lcfi668:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB119_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB119_3
.LBB119_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16UnpDollarvarAttrEPNS_8impl_INTEPNS_18impl_unpattributesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB119_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end119:
	.size	_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE, .Lfunc_end119-_ZN2kc32impl_unpsubterm_UnpDollarvarAttr7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE: # @_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi669:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi670:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi671:
	.cfi_def_cfa_offset 32
.Lcfi672:
	.cfi_offset %rbx, -32
.Lcfi673:
	.cfi_offset %r14, -24
.Lcfi674:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB120_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB120_3
.LBB120_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10UnpSubAttrEPNS_7impl_IDEPNS_18impl_unpattributesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB120_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end120:
	.size	_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE, .Lfunc_end120-_ZN2kc26impl_unpsubterm_UnpSubAttr7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE: # @_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi675:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi676:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi677:
	.cfi_def_cfa_offset 32
.Lcfi678:
	.cfi_offset %rbx, -24
.Lcfi679:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB121_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16UnpDollarvarTermEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB121_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end121:
	.size	_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE, .Lfunc_end121-_ZN2kc32impl_unpsubterm_UnpDollarvarTerm7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE: # @_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi680:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi681:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi682:
	.cfi_def_cfa_offset 32
.Lcfi683:
	.cfi_offset %rbx, -24
.Lcfi684:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB122_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10UnpSubTermEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB122_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end122:
	.size	_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE, .Lfunc_end122-_ZN2kc26impl_unpsubterm_UnpSubTerm7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE: # @_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi685:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi686:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi687:
	.cfi_def_cfa_offset 32
.Lcfi688:
	.cfi_offset %rbx, -32
.Lcfi689:
	.cfi_offset %r14, -24
.Lcfi690:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB123_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB123_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB123_4
.LBB123_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConsunpattributesEPNS_7impl_IDEPNS_18impl_unpattributesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB123_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end123:
	.size	_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE, .Lfunc_end123-_ZN2kc18impl_unpattributes7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE,@function
_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE: # @_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi691:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi692:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi693:
	.cfi_def_cfa_offset 32
.Lcfi694:
	.cfi_offset %rbx, -24
.Lcfi695:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB124_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc11YesViewnameEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB124_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end124:
	.size	_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE, .Lfunc_end124-_ZN2kc31impl_viewnameoption_YesViewname7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE: # @_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end125:
	.size	_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE, .Lfunc_end125-_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE: # @_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi696:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi697:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi698:
	.cfi_def_cfa_offset 32
.Lcfi699:
	.cfi_offset %rbx, -24
.Lcfi700:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB126_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc12LanguageListEPNS_18impl_languagenamesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB126_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end126:
	.size	_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE, .Lfunc_end126-_ZN2kc32impl_languageoption_LanguageList7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE: # @_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end127:
	.size	_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE, .Lfunc_end127-_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE: # @_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi701:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi702:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi703:
	.cfi_def_cfa_offset 32
.Lcfi704:
	.cfi_offset %rbx, -32
.Lcfi705:
	.cfi_offset %r14, -24
.Lcfi706:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB128_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB128_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB128_4
.LBB128_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConslanguagenamesEPNS_7impl_IDEPNS_18impl_languagenamesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB128_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end128:
	.size	_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE, .Lfunc_end128-_ZN2kc18impl_languagenames7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE: # @_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end129:
	.size	_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE, .Lfunc_end129-_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE: # @_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end130:
	.size	_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE, .Lfunc_end130-_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE: # @_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end131:
	.size	_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE, .Lfunc_end131-_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE: # @_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi707:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi708:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi709:
	.cfi_def_cfa_offset 32
.Lcfi710:
	.cfi_offset %rbx, -32
.Lcfi711:
	.cfi_offset %r14, -24
.Lcfi712:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB132_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB132_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB132_4
.LBB132_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc26ConsscopetypefilelinestackEPNS_22impl_scopetypefilelineEPNS_27impl_scopetypefilelinestackE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB132_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end132:
	.size	_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE, .Lfunc_end132-_ZN2kc27impl_scopetypefilelinestack7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE: # @_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi713:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi714:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi715:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi716:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi717:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi718:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi719:
	.cfi_def_cfa_offset 64
.Lcfi720:
	.cfi_offset %rbx, -56
.Lcfi721:
	.cfi_offset %r12, -48
.Lcfi722:
	.cfi_offset %r13, -40
.Lcfi723:
	.cfi_offset %r14, -32
.Lcfi724:
	.cfi_offset %r15, -24
.Lcfi725:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB133_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB133_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB133_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB133_5
.LBB133_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc17ScopeTypeFileLineEPNS_17impl_integer__IntEPNS_11impl_IDtypeEPNS_20impl_casestring__StrES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB133_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end133:
	.size	_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE, .Lfunc_end133-_ZN2kc40impl_scopetypefileline_ScopeTypeFileLine7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE: # @_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi726:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi727:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi728:
	.cfi_def_cfa_offset 32
.Lcfi729:
	.cfi_offset %rbx, -24
.Lcfi730:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB134_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14ITLanguageNameEPNS_17impl_integer__IntE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB134_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end134:
	.size	_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE, .Lfunc_end134-_ZN2kc26impl_IDtype_ITLanguageName7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE: # @_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi731:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi732:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi733:
	.cfi_def_cfa_offset 32
.Lcfi734:
	.cfi_offset %rbx, -32
.Lcfi735:
	.cfi_offset %r14, -24
.Lcfi736:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB135_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB135_3
.LBB135_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ITPatternVariableEPNS_7impl_IDEPNS_17impl_integer__IntE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB135_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end135:
	.size	_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE, .Lfunc_end135-_ZN2kc29impl_IDtype_ITPatternVariable7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE: # @_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi737:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi738:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi739:
	.cfi_def_cfa_offset 32
.Lcfi740:
	.cfi_offset %rbx, -24
.Lcfi741:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB136_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14ITUserFunctionEPNS_12impl_fnclassE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB136_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end136:
	.size	_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE, .Lfunc_end136-_ZN2kc26impl_IDtype_ITUserFunction7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE: # @_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end137:
	.size	_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE, .Lfunc_end137-_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE: # @_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end138:
	.size	_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE, .Lfunc_end138-_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE: # @_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end139:
	.size	_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE, .Lfunc_end139-_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE: # @_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end140:
	.size	_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE, .Lfunc_end140-_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE: # @_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end141:
	.size	_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE, .Lfunc_end141-_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE: # @_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end142:
	.size	_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE, .Lfunc_end142-_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE: # @_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end143:
	.size	_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE, .Lfunc_end143-_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE: # @_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi742:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi743:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi744:
	.cfi_def_cfa_offset 32
.Lcfi745:
	.cfi_offset %rbx, -32
.Lcfi746:
	.cfi_offset %r14, -24
.Lcfi747:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB144_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB144_3
.LBB144_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc14ITUserOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB144_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end144:
	.size	_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE, .Lfunc_end144-_ZN2kc26impl_IDtype_ITUserOperator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE: # @_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi748:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi749:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi750:
	.cfi_def_cfa_offset 32
.Lcfi751:
	.cfi_offset %rbx, -32
.Lcfi752:
	.cfi_offset %r14, -24
.Lcfi753:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB145_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB145_3
.LBB145_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20ITPredefinedOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB145_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end145:
	.size	_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE, .Lfunc_end145-_ZN2kc32impl_IDtype_ITPredefinedOperator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE: # @_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi754:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi755:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi756:
	.cfi_def_cfa_offset 32
.Lcfi757:
	.cfi_offset %rbx, -24
.Lcfi758:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB146_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc12ITUserPhylumEPNS_22impl_phylumdeclarationE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB146_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end146:
	.size	_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE, .Lfunc_end146-_ZN2kc24impl_IDtype_ITUserPhylum7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE: # @_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi759:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi760:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi761:
	.cfi_def_cfa_offset 32
.Lcfi762:
	.cfi_offset %rbx, -24
.Lcfi763:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB147_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18ITPredefinedPhylumEPNS_22impl_phylumdeclarationE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB147_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end147:
	.size	_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE, .Lfunc_end147-_ZN2kc30impl_IDtype_ITPredefinedPhylum7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE: # @_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end148:
	.size	_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE, .Lfunc_end148-_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc14impl_operators7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc14impl_operators7rewriteERNS_11rview_classE,@function
_ZN2kc14impl_operators7rewriteERNS_11rview_classE: # @_ZN2kc14impl_operators7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi764:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi765:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi766:
	.cfi_def_cfa_offset 32
.Lcfi767:
	.cfi_offset %rbx, -32
.Lcfi768:
	.cfi_offset %r14, -24
.Lcfi769:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB149_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB149_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB149_4
.LBB149_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsoperatorsEPNS_7impl_IDEPNS_14impl_operatorsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB149_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end149:
	.size	_ZN2kc14impl_operators7rewriteERNS_11rview_classE, .Lfunc_end149-_ZN2kc14impl_operators7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc10impl_phyla7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc10impl_phyla7rewriteERNS_11rview_classE,@function
_ZN2kc10impl_phyla7rewriteERNS_11rview_classE: # @_ZN2kc10impl_phyla7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi770:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi771:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi772:
	.cfi_def_cfa_offset 32
.Lcfi773:
	.cfi_offset %rbx, -32
.Lcfi774:
	.cfi_offset %r14, -24
.Lcfi775:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB150_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB150_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB150_4
.LBB150_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConsphylaEPNS_7impl_IDEPNS_10impl_phylaE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB150_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end150:
	.size	_ZN2kc10impl_phyla7rewriteERNS_11rview_classE, .Lfunc_end150-_ZN2kc10impl_phyla7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc14impl_variables7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc14impl_variables7rewriteERNS_11rview_classE,@function
_ZN2kc14impl_variables7rewriteERNS_11rview_classE: # @_ZN2kc14impl_variables7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi776:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi777:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi778:
	.cfi_def_cfa_offset 32
.Lcfi779:
	.cfi_offset %rbx, -32
.Lcfi780:
	.cfi_offset %r14, -24
.Lcfi781:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB151_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB151_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB151_4
.LBB151_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsvariablesEPNS_7impl_IDEPNS_14impl_variablesE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB151_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end151:
	.size	_ZN2kc14impl_variables7rewriteERNS_11rview_classE, .Lfunc_end151-_ZN2kc14impl_variables7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE: # @_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end152:
	.size	_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE, .Lfunc_end152-_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE: # @_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end153:
	.size	_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE, .Lfunc_end153-_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE: # @_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end154:
	.size	_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE, .Lfunc_end154-_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE,@function
_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE: # @_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end155:
	.size	_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE, .Lfunc_end155-_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE: # @_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end156:
	.size	_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE, .Lfunc_end156-_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE: # @_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi782:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi783:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi784:
	.cfi_def_cfa_offset 32
.Lcfi785:
	.cfi_offset %rbx, -32
.Lcfi786:
	.cfi_offset %r14, -24
.Lcfi787:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB157_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB157_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB157_4
.LBB157_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc26ConspatternrepresentationsEPNS_26impl_patternrepresentationEPNS_27impl_patternrepresentationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB157_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end157:
	.size	_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE, .Lfunc_end157-_ZN2kc27impl_patternrepresentations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE: # @_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi788:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi789:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi790:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi791:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi792:
	.cfi_def_cfa_offset 48
.Lcfi793:
	.cfi_offset %rbx, -40
.Lcfi794:
	.cfi_offset %r12, -32
.Lcfi795:
	.cfi_offset %r14, -24
.Lcfi796:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB158_8
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	je	.LBB158_2
# BB#4:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	jne	.LBB158_5
.LBB158_2:
	cmpq	%rbx, %r15
	je	.LBB158_8
# BB#3:
	movq	(%r15), %rax
	movq	24(%rax), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB158_5:
	cmpq	8(%rbx), %r12
	jne	.LBB158_7
# BB#6:
	cmpq	16(%rbx), %r15
	je	.LBB158_8
.LBB158_7:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc25ConspatternrepresentationEPNS_31impl_elem_patternrepresentationEPNS_26impl_patternrepresentationE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB158_8:                              # %.thread
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end158:
	.size	_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE, .Lfunc_end158-_ZN2kc26impl_patternrepresentation7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE: # @_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi797:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi798:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi799:
	.cfi_def_cfa_offset 32
.Lcfi800:
	.cfi_offset %rbx, -32
.Lcfi801:
	.cfi_offset %r14, -24
.Lcfi802:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB159_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB159_3
.LBB159_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc12PRIntLiteralEPNS_9impl_pathEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB159_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end159:
	.size	_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE, .Lfunc_end159-_ZN2kc44impl_elem_patternrepresentation_PRIntLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE,@function
_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE: # @_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi803:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi804:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi805:
	.cfi_def_cfa_offset 32
.Lcfi806:
	.cfi_offset %rbx, -32
.Lcfi807:
	.cfi_offset %r14, -24
.Lcfi808:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB160_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB160_3
.LBB160_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15PRStringLiteralEPNS_9impl_pathEPNS_18impl_CexpressionDQE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB160_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end160:
	.size	_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE, .Lfunc_end160-_ZN2kc47impl_elem_patternrepresentation_PRStringLiteral7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE: # @_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end161:
	.size	_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE, .Lfunc_end161-_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE: # @_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi809:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi810:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi811:
	.cfi_def_cfa_offset 32
.Lcfi812:
	.cfi_offset %rbx, -24
.Lcfi813:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB162_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10PRWildcardEPNS_9impl_pathE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB162_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end162:
	.size	_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE, .Lfunc_end162-_ZN2kc42impl_elem_patternrepresentation_PRWildcard7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE,@function
_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE: # @_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi814:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi815:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi816:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi817:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi818:
	.cfi_def_cfa_offset 48
.Lcfi819:
	.cfi_offset %rbx, -48
.Lcfi820:
	.cfi_offset %r12, -40
.Lcfi821:
	.cfi_offset %r13, -32
.Lcfi822:
	.cfi_offset %r14, -24
.Lcfi823:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	32(%r13), %r14
	jne	.LBB163_3
# BB#1:
	cmpq	40(%r13), %r15
	jne	.LBB163_3
# BB#2:
	cmpq	48(%r13), %rax
	je	.LBB163_4
.LBB163_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc16PRNonLeafBindingEPNS_9impl_pathEPNS_7impl_IDEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB163_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end163:
	.size	_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE, .Lfunc_end163-_ZN2kc48impl_elem_patternrepresentation_PRNonLeafBinding7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE,@function
_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE: # @_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi824:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi825:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi826:
	.cfi_def_cfa_offset 32
.Lcfi827:
	.cfi_offset %rbx, -24
.Lcfi828:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	32(%rbx), %rax
	je	.LBB164_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc15PRUserPredicateEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB164_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end164:
	.size	_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE, .Lfunc_end164-_ZN2kc47impl_elem_patternrepresentation_PRUserPredicate7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE,@function
_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE: # @_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi829:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi830:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi831:
	.cfi_def_cfa_offset 32
.Lcfi832:
	.cfi_offset %rbx, -32
.Lcfi833:
	.cfi_offset %r14, -24
.Lcfi834:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB165_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB165_3
.LBB165_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15PROperPredicateEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB165_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end165:
	.size	_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE, .Lfunc_end165-_ZN2kc47impl_elem_patternrepresentation_PROperPredicate7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE,@function
_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE: # @_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi835:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi836:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi837:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi838:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi839:
	.cfi_def_cfa_offset 48
.Lcfi840:
	.cfi_offset %rbx, -48
.Lcfi841:
	.cfi_offset %r12, -40
.Lcfi842:
	.cfi_offset %r13, -32
.Lcfi843:
	.cfi_offset %r14, -24
.Lcfi844:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	32(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	32(%r13), %r14
	jne	.LBB166_3
# BB#1:
	cmpq	40(%r13), %r15
	jne	.LBB166_3
# BB#2:
	cmpq	48(%r13), %rax
	je	.LBB166_4
.LBB166_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc14PRVarPredicateEPNS_10impl_pathsEPNS_7impl_IDEPNS_26impl_patternrepresentationE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB166_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end166:
	.size	_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE, .Lfunc_end166-_ZN2kc46impl_elem_patternrepresentation_PRVarPredicate7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE: # @_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi845:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi846:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi847:
	.cfi_def_cfa_offset 32
.Lcfi848:
	.cfi_offset %rbx, -32
.Lcfi849:
	.cfi_offset %r14, -24
.Lcfi850:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	32(%rbx), %r14
	jne	.LBB167_2
# BB#1:
	cmpq	40(%rbx), %rax
	je	.LBB167_3
.LBB167_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9PRBindingEPNS_9impl_pathEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB167_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end167:
	.size	_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE, .Lfunc_end167-_ZN2kc41impl_elem_patternrepresentation_PRBinding7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc9impl_path7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc9impl_path7rewriteERNS_11rview_classE,@function
_ZN2kc9impl_path7rewriteERNS_11rview_classE: # @_ZN2kc9impl_path7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi851:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi852:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi853:
	.cfi_def_cfa_offset 32
.Lcfi854:
	.cfi_offset %rbx, -32
.Lcfi855:
	.cfi_offset %r14, -24
.Lcfi856:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB168_4
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	24(%rbx), %r15
	jne	.LBB168_3
# BB#2:
	cmpq	32(%rbx), %rax
	je	.LBB168_4
.LBB168_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8ConspathEPNS_17impl_integer__IntEPNS_9impl_pathE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB168_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end168:
	.size	_ZN2kc9impl_path7rewriteERNS_11rview_classE, .Lfunc_end168-_ZN2kc9impl_path7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc10impl_paths7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc10impl_paths7rewriteERNS_11rview_classE,@function
_ZN2kc10impl_paths7rewriteERNS_11rview_classE: # @_ZN2kc10impl_paths7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi857:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi858:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi859:
	.cfi_def_cfa_offset 32
.Lcfi860:
	.cfi_offset %rbx, -32
.Lcfi861:
	.cfi_offset %r14, -24
.Lcfi862:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB169_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB169_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB169_4
.LBB169_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc9ConspathsEPNS_9impl_pathEPNS_10impl_pathsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB169_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end169:
	.size	_ZN2kc10impl_paths7rewriteERNS_11rview_classE, .Lfunc_end169-_ZN2kc10impl_paths7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE,@function
_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE: # @_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi863:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi864:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi865:
	.cfi_def_cfa_offset 32
.Lcfi866:
	.cfi_offset %rbx, -32
.Lcfi867:
	.cfi_offset %r14, -24
.Lcfi868:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB170_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB170_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB170_4
.LBB170_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15ConsargsnumbersEPNS_17impl_integer__IntEPNS_16impl_argsnumbersE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB170_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end170:
	.size	_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE, .Lfunc_end170-_ZN2kc16impl_argsnumbers7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE: # @_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi869:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi870:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi871:
	.cfi_def_cfa_offset 32
.Lcfi872:
	.cfi_offset %rbx, -32
.Lcfi873:
	.cfi_offset %r14, -24
.Lcfi874:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB171_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB171_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB171_4
.LBB171_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20ConsrewriterulesinfoEPNS_20impl_rewriteruleinfoEPNS_21impl_rewriterulesinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB171_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end171:
	.size	_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE, .Lfunc_end171-_ZN2kc21impl_rewriterulesinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE: # @_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi875:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi876:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi877:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi878:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi879:
	.cfi_def_cfa_offset 48
.Lcfi880:
	.cfi_offset %rbx, -48
.Lcfi881:
	.cfi_offset %r12, -40
.Lcfi882:
	.cfi_offset %r13, -32
.Lcfi883:
	.cfi_offset %r14, -24
.Lcfi884:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB172_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB172_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB172_4
.LBB172_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc15RewriteruleinfoEPNS_26impl_patternrepresentationES1_PNS_18impl_rewriteclauseE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB172_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end172:
	.size	_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE, .Lfunc_end172-_ZN2kc36impl_rewriteruleinfo_Rewriteruleinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE: # @_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi885:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi886:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi887:
	.cfi_def_cfa_offset 32
.Lcfi888:
	.cfi_offset %rbx, -32
.Lcfi889:
	.cfi_offset %r14, -24
.Lcfi890:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB173_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB173_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB173_4
.LBB173_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17ConswithcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB173_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end173:
	.size	_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE, .Lfunc_end173-_ZN2kc18impl_withcasesinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE: # @_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi891:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi892:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi893:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi894:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi895:
	.cfi_def_cfa_offset 48
.Lcfi896:
	.cfi_offset %rbx, -48
.Lcfi897:
	.cfi_offset %r12, -40
.Lcfi898:
	.cfi_offset %r13, -32
.Lcfi899:
	.cfi_offset %r14, -24
.Lcfi900:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB174_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB174_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB174_4
.LBB174_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12WithcaseinfoEPNS_26impl_patternrepresentationES1_PNS_10impl_CtextE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB174_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end174:
	.size	_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE, .Lfunc_end174-_ZN2kc30impl_withcaseinfo_Withcaseinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE: # @_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi901:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi902:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi903:
	.cfi_def_cfa_offset 32
.Lcfi904:
	.cfi_offset %rbx, -32
.Lcfi905:
	.cfi_offset %r14, -24
.Lcfi906:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB175_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB175_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB175_4
.LBB175_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20ConsrewriteviewsinfoEPNS_20impl_rewriteviewinfoEPNS_21impl_rewriteviewsinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB175_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end175:
	.size	_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE, .Lfunc_end175-_ZN2kc21impl_rewriteviewsinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE: # @_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi907:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi908:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi909:
	.cfi_def_cfa_offset 32
.Lcfi910:
	.cfi_offset %rbx, -32
.Lcfi911:
	.cfi_offset %r14, -24
.Lcfi912:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB176_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB176_3
.LBB176_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15RewriteviewinfoEPNS_7impl_IDEPNS_21impl_rewriterulesinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB176_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end176:
	.size	_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE, .Lfunc_end176-_ZN2kc36impl_rewriteviewinfo_Rewriteviewinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE: # @_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi913:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi914:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi915:
	.cfi_def_cfa_offset 32
.Lcfi916:
	.cfi_offset %rbx, -32
.Lcfi917:
	.cfi_offset %r14, -24
.Lcfi918:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB177_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB177_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB177_4
.LBB177_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20ConsunparseviewsinfoEPNS_20impl_unparseviewinfoEPNS_21impl_unparseviewsinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB177_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end177:
	.size	_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE, .Lfunc_end177-_ZN2kc21impl_unparseviewsinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE: # @_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi919:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi920:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi921:
	.cfi_def_cfa_offset 32
.Lcfi922:
	.cfi_offset %rbx, -32
.Lcfi923:
	.cfi_offset %r14, -24
.Lcfi924:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB178_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB178_3
.LBB178_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15UnparseviewinfoEPNS_7impl_IDEPNS_21impl_unparsedeclsinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB178_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end178:
	.size	_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE, .Lfunc_end178-_ZN2kc36impl_unparseviewinfo_Unparseviewinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE,@function
_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE: # @_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi925:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi926:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi927:
	.cfi_def_cfa_offset 32
.Lcfi928:
	.cfi_offset %rbx, -32
.Lcfi929:
	.cfi_offset %r14, -24
.Lcfi930:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB179_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB179_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB179_4
.LBB179_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc20ConsunparsedeclsinfoEPNS_20impl_unparsedeclinfoEPNS_21impl_unparsedeclsinfoE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB179_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end179:
	.size	_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE, .Lfunc_end179-_ZN2kc21impl_unparsedeclsinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE: # @_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi931:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi932:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi933:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi934:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi935:
	.cfi_def_cfa_offset 48
.Lcfi936:
	.cfi_offset %rbx, -48
.Lcfi937:
	.cfi_offset %r12, -40
.Lcfi938:
	.cfi_offset %r13, -32
.Lcfi939:
	.cfi_offset %r14, -24
.Lcfi940:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB180_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB180_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB180_4
.LBB180_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc15UnparsedeclinfoEPNS_26impl_patternrepresentationES1_PNS_18impl_unparseclauseE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB180_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end180:
	.size	_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE, .Lfunc_end180-_ZN2kc36impl_unparsedeclinfo_Unparsedeclinfo7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi941:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi942:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi943:
	.cfi_def_cfa_offset 32
.Lcfi944:
	.cfi_offset %rbx, -32
.Lcfi945:
	.cfi_offset %r14, -24
.Lcfi946:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB181_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB181_3
.LBB181_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13AcDeclarationEPNS_30impl_ac_declaration_specifiersEPNS_28impl_ac_init_declarator_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB181_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end181:
	.size	_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE, .Lfunc_end181-_ZN2kc33impl_ac_declaration_AcDeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE: # @_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi947:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi948:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi949:
	.cfi_def_cfa_offset 32
.Lcfi950:
	.cfi_offset %rbx, -32
.Lcfi951:
	.cfi_offset %r14, -24
.Lcfi952:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB182_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB182_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB182_4
.LBB182_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc23Consac_declaration_listEPNS_19impl_ac_declarationEPNS_24impl_ac_declaration_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB182_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end182:
	.size	_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE, .Lfunc_end182-_ZN2kc24impl_ac_declaration_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE: # @_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi953:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi954:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi955:
	.cfi_def_cfa_offset 32
.Lcfi956:
	.cfi_offset %rbx, -32
.Lcfi957:
	.cfi_offset %r14, -24
.Lcfi958:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB183_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB183_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB183_4
.LBB183_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc29Consac_declaration_specifiersEPNS_29impl_ac_declaration_specifierEPNS_30impl_ac_declaration_specifiersE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB183_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end183:
	.size	_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE, .Lfunc_end183-_ZN2kc30impl_ac_declaration_specifiers7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE,@function
_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE: # @_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi959:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi960:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi961:
	.cfi_def_cfa_offset 32
.Lcfi962:
	.cfi_offset %rbx, -24
.Lcfi963:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB184_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18AcDeclSpecTypeQualEPNS_22impl_ac_type_qualifierE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB184_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end184:
	.size	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE, .Lfunc_end184-_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeQual7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE,@function
_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE: # @_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi964:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi965:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi966:
	.cfi_def_cfa_offset 32
.Lcfi967:
	.cfi_offset %rbx, -24
.Lcfi968:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB185_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc18AcDeclSpecTypeSpecEPNS_22impl_ac_type_specifierE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB185_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end185:
	.size	_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE, .Lfunc_end185-_ZN2kc48impl_ac_declaration_specifier_AcDeclSpecTypeSpec7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE,@function
_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE: # @_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi969:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi970:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi971:
	.cfi_def_cfa_offset 32
.Lcfi972:
	.cfi_offset %rbx, -24
.Lcfi973:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB186_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc21AcDeclSpecStorageSpecEPNS_31impl_ac_storage_class_specifierE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB186_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end186:
	.size	_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE, .Lfunc_end186-_ZN2kc51impl_ac_declaration_specifier_AcDeclSpecStorageSpec7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE: # @_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end187:
	.size	_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE, .Lfunc_end187-_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE: # @_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end188:
	.size	_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE, .Lfunc_end188-_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE: # @_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end189:
	.size	_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE, .Lfunc_end189-_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE: # @_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end190:
	.size	_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE, .Lfunc_end190-_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE: # @_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end191:
	.size	_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE, .Lfunc_end191-_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE: # @_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end192:
	.size	_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE, .Lfunc_end192-_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi974:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi975:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi976:
	.cfi_def_cfa_offset 32
.Lcfi977:
	.cfi_offset %rbx, -24
.Lcfi978:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB193_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10AcTypeSpecEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB193_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end193:
	.size	_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE, .Lfunc_end193-_ZN2kc33impl_ac_type_specifier_AcTypeSpec7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE: # @_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end194:
	.size	_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE, .Lfunc_end194-_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end195:
	.size	_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE, .Lfunc_end195-_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end196:
	.size	_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE, .Lfunc_end196-_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE: # @_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end197:
	.size	_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE, .Lfunc_end197-_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE: # @_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi979:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi980:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi981:
	.cfi_def_cfa_offset 32
.Lcfi982:
	.cfi_offset %rbx, -32
.Lcfi983:
	.cfi_offset %r14, -24
.Lcfi984:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB198_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB198_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB198_4
.LBB198_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc27Consac_init_declarator_listEPNS_23impl_ac_init_declaratorEPNS_28impl_ac_init_declarator_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB198_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end198:
	.size	_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE, .Lfunc_end198-_ZN2kc28impl_ac_init_declarator_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE,@function
_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE: # @_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi985:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi986:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi987:
	.cfi_def_cfa_offset 32
.Lcfi988:
	.cfi_offset %rbx, -24
.Lcfi989:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB199_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10AcInitDeclEPNS_18impl_ac_declaratorE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB199_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end199:
	.size	_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE, .Lfunc_end199-_ZN2kc34impl_ac_init_declarator_AcInitDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE,@function
_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE: # @_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi990:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi991:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi992:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi993:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi994:
	.cfi_def_cfa_offset 48
.Lcfi995:
	.cfi_offset %rbx, -48
.Lcfi996:
	.cfi_offset %r12, -40
.Lcfi997:
	.cfi_offset %r13, -32
.Lcfi998:
	.cfi_offset %r14, -24
.Lcfi999:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB200_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB200_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB200_4
.LBB200_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12AcDeclaratorEPNS_22impl_ac_pointer_optionEPNS_18impl_ac_ref_optionEPNS_25impl_ac_direct_declaratorE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB200_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end200:
	.size	_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE, .Lfunc_end200-_ZN2kc31impl_ac_declarator_AcDeclarator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE: # @_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1000:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1001:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1002:
	.cfi_def_cfa_offset 32
.Lcfi1003:
	.cfi_offset %rbx, -24
.Lcfi1004:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB201_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16AcOperatorDeclIdEPNS_21impl_ac_operator_nameE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB201_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end201:
	.size	_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE, .Lfunc_end201-_ZN2kc42impl_ac_direct_declarator_AcOperatorDeclId7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE,@function
_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE: # @_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1005:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1006:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1007:
	.cfi_def_cfa_offset 32
.Lcfi1008:
	.cfi_offset %rbx, -32
.Lcfi1009:
	.cfi_offset %r14, -24
.Lcfi1010:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB202_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB202_3
.LBB202_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18AcConvOperatorDeclEPNS_7impl_IDES1_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB202_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end202:
	.size	_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE, .Lfunc_end202-_ZN2kc44impl_ac_direct_declarator_AcConvOperatorDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE,@function
_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE: # @_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1011:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1012:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1013:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1014:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1015:
	.cfi_def_cfa_offset 48
.Lcfi1016:
	.cfi_offset %rbx, -48
.Lcfi1017:
	.cfi_offset %r12, -40
.Lcfi1018:
	.cfi_offset %r13, -32
.Lcfi1019:
	.cfi_offset %r14, -24
.Lcfi1020:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB203_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB203_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB203_4
.LBB203_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12AcMemberDeclEPNS_7impl_IDES1_PNS_32impl_ac_constant_expression_listE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB203_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end203:
	.size	_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE, .Lfunc_end203-_ZN2kc38impl_ac_direct_declarator_AcMemberDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE,@function
_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE: # @_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1021:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1022:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1023:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1024:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1025:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1026:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1027:
	.cfi_def_cfa_offset 64
.Lcfi1028:
	.cfi_offset %rbx, -56
.Lcfi1029:
	.cfi_offset %r12, -48
.Lcfi1030:
	.cfi_offset %r13, -40
.Lcfi1031:
	.cfi_offset %r14, -32
.Lcfi1032:
	.cfi_offset %r15, -24
.Lcfi1033:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB204_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB204_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB204_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB204_5
.LBB204_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc20AcQualifiedDeclProtoEPNS_28impl_ac_class_qualifier_listEPNS_25impl_ac_direct_declaratorEPNS_27impl_ac_parameter_type_listEPNS_22impl_ac_type_qualifierE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB204_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end204:
	.size	_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE, .Lfunc_end204-_ZN2kc46impl_ac_direct_declarator_AcQualifiedDeclProto7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE,@function
_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE: # @_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1034:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1035:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1036:
	.cfi_def_cfa_offset 32
.Lcfi1037:
	.cfi_offset %rbx, -32
.Lcfi1038:
	.cfi_offset %r14, -24
.Lcfi1039:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB205_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB205_3
.LBB205_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17AcDirectDeclProtoEPNS_25impl_ac_direct_declaratorEPNS_27impl_ac_parameter_type_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB205_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end205:
	.size	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE, .Lfunc_end205-_ZN2kc43impl_ac_direct_declarator_AcDirectDeclProto7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE,@function
_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE: # @_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1040:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1041:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1042:
	.cfi_def_cfa_offset 32
.Lcfi1043:
	.cfi_offset %rbx, -32
.Lcfi1044:
	.cfi_offset %r14, -24
.Lcfi1045:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB206_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB206_3
.LBB206_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17AcDirectDeclArrayEPNS_25impl_ac_direct_declaratorEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB206_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end206:
	.size	_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE, .Lfunc_end206-_ZN2kc43impl_ac_direct_declarator_AcDirectDeclArray7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE,@function
_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE: # @_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1046:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1047:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1048:
	.cfi_def_cfa_offset 32
.Lcfi1049:
	.cfi_offset %rbx, -24
.Lcfi1050:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB207_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16AcDirectDeclPackEPNS_18impl_ac_declaratorE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB207_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end207:
	.size	_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE, .Lfunc_end207-_ZN2kc42impl_ac_direct_declarator_AcDirectDeclPack7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE: # @_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1051:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1052:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1053:
	.cfi_def_cfa_offset 32
.Lcfi1054:
	.cfi_offset %rbx, -24
.Lcfi1055:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB208_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14AcDirectDeclIdEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB208_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end208:
	.size	_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE, .Lfunc_end208-_ZN2kc40impl_ac_direct_declarator_AcDirectDeclId7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1056:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1057:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1058:
	.cfi_def_cfa_offset 32
.Lcfi1059:
	.cfi_offset %rbx, -24
.Lcfi1060:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB209_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc10YespointerEPNS_15impl_ac_pointerE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB209_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end209:
	.size	_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE, .Lfunc_end209-_ZN2kc33impl_ac_pointer_option_Yespointer7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE: # @_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end210:
	.size	_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE, .Lfunc_end210-_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE: # @_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1061:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1062:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1063:
	.cfi_def_cfa_offset 32
.Lcfi1064:
	.cfi_offset %rbx, -32
.Lcfi1065:
	.cfi_offset %r14, -24
.Lcfi1066:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB211_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB211_3
.LBB211_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13AcPointerConsEPNS_27impl_ac_type_qualifier_listEPNS_15impl_ac_pointerE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB211_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end211:
	.size	_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE, .Lfunc_end211-_ZN2kc29impl_ac_pointer_AcPointerCons7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE: # @_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1067:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1068:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1069:
	.cfi_def_cfa_offset 32
.Lcfi1070:
	.cfi_offset %rbx, -24
.Lcfi1071:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB212_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc12AcPointerNilEPNS_27impl_ac_type_qualifier_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB212_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end212:
	.size	_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE, .Lfunc_end212-_ZN2kc28impl_ac_pointer_AcPointerNil7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE: # @_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end213:
	.size	_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE, .Lfunc_end213-_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE: # @_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end214:
	.size	_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE, .Lfunc_end214-_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE,@function
_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE: # @_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1072:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1073:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1074:
	.cfi_def_cfa_offset 32
.Lcfi1075:
	.cfi_offset %rbx, -24
.Lcfi1076:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	24(%rbx), %rax
	je	.LBB215_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc14AcOperatorNameEPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB215_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end215:
	.size	_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE, .Lfunc_end215-_ZN2kc36impl_ac_operator_name_AcOperatorName7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE: # @_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1077:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1078:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1079:
	.cfi_def_cfa_offset 32
.Lcfi1080:
	.cfi_offset %rbx, -32
.Lcfi1081:
	.cfi_offset %r14, -24
.Lcfi1082:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB216_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB216_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB216_4
.LBB216_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc32Consac_class_qualifier_help_listEPNS_25impl_ac_direct_declaratorEPNS_33impl_ac_class_qualifier_help_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB216_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end216:
	.size	_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE, .Lfunc_end216-_ZN2kc33impl_ac_class_qualifier_help_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE: # @_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1083:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1084:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1085:
	.cfi_def_cfa_offset 32
.Lcfi1086:
	.cfi_offset %rbx, -32
.Lcfi1087:
	.cfi_offset %r14, -24
.Lcfi1088:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB217_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB217_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB217_4
.LBB217_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc27Consac_class_qualifier_listEPNS_7impl_IDEPNS_28impl_ac_class_qualifier_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB217_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end217:
	.size	_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE, .Lfunc_end217-_ZN2kc28impl_ac_class_qualifier_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE: # @_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1089:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1090:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1091:
	.cfi_def_cfa_offset 32
.Lcfi1092:
	.cfi_offset %rbx, -32
.Lcfi1093:
	.cfi_offset %r14, -24
.Lcfi1094:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB218_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB218_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB218_4
.LBB218_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc26Consac_type_qualifier_listEPNS_22impl_ac_type_qualifierEPNS_27impl_ac_type_qualifier_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB218_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end218:
	.size	_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE, .Lfunc_end218-_ZN2kc27impl_ac_type_qualifier_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE,@function
_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE: # @_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1095:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1096:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1097:
	.cfi_def_cfa_offset 32
.Lcfi1098:
	.cfi_offset %rbx, -24
.Lcfi1099:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB219_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13AcParList3DotEPNS_22impl_ac_parameter_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB219_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end219:
	.size	_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE, .Lfunc_end219-_ZN2kc41impl_ac_parameter_type_list_AcParList3Dot7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE: # @_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1102:
	.cfi_def_cfa_offset 32
.Lcfi1103:
	.cfi_offset %rbx, -24
.Lcfi1104:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB220_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc9AcParListEPNS_22impl_ac_parameter_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB220_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end220:
	.size	_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE, .Lfunc_end220-_ZN2kc37impl_ac_parameter_type_list_AcParList7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE: # @_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1106:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1107:
	.cfi_def_cfa_offset 32
.Lcfi1108:
	.cfi_offset %rbx, -32
.Lcfi1109:
	.cfi_offset %r14, -24
.Lcfi1110:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB221_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB221_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB221_4
.LBB221_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc21Consac_parameter_listEPNS_29impl_ac_parameter_declarationEPNS_22impl_ac_parameter_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB221_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end221:
	.size	_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE, .Lfunc_end221-_ZN2kc22impl_ac_parameter_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE,@function
_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE: # @_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1111:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1112:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1113:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1114:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1115:
	.cfi_def_cfa_offset 48
.Lcfi1116:
	.cfi_offset %rbx, -48
.Lcfi1117:
	.cfi_offset %r12, -40
.Lcfi1118:
	.cfi_offset %r13, -32
.Lcfi1119:
	.cfi_offset %r14, -24
.Lcfi1120:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB222_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB222_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB222_4
.LBB222_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc16AcParDeclAbsdeclEPNS_30impl_ac_declaration_specifiersEPNS_27impl_ac_abstract_declaratorEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB222_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end222:
	.size	_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE, .Lfunc_end222-_ZN2kc46impl_ac_parameter_declaration_AcParDeclAbsdecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE,@function
_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE: # @_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1122:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1123:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1124:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1125:
	.cfi_def_cfa_offset 48
.Lcfi1126:
	.cfi_offset %rbx, -48
.Lcfi1127:
	.cfi_offset %r12, -40
.Lcfi1128:
	.cfi_offset %r13, -32
.Lcfi1129:
	.cfi_offset %r14, -24
.Lcfi1130:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB223_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB223_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB223_4
.LBB223_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc13AcParDeclDeclEPNS_30impl_ac_declaration_specifiersEPNS_18impl_ac_declaratorEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB223_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end223:
	.size	_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE, .Lfunc_end223-_ZN2kc43impl_ac_parameter_declaration_AcParDeclDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE: # @_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1132:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1133:
	.cfi_def_cfa_offset 32
.Lcfi1134:
	.cfi_offset %rbx, -32
.Lcfi1135:
	.cfi_offset %r14, -24
.Lcfi1136:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB224_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB224_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB224_4
.LBB224_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc22Consac_identifier_listEPNS_7impl_IDEPNS_23impl_ac_identifier_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB224_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end224:
	.size	_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE, .Lfunc_end224-_ZN2kc23impl_ac_identifier_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE,@function
_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE: # @_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1137:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1138:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1139:
	.cfi_def_cfa_offset 32
.Lcfi1140:
	.cfi_offset %rbx, -32
.Lcfi1141:
	.cfi_offset %r14, -24
.Lcfi1142:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB225_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB225_3
.LBB225_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16AcAbsdeclDirdeclEPNS_22impl_ac_pointer_optionEPNS_34impl_ac_direct_abstract_declaratorE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB225_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end225:
	.size	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE, .Lfunc_end225-_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclDirdecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE,@function
_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE: # @_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1143:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1144:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1145:
	.cfi_def_cfa_offset 32
.Lcfi1146:
	.cfi_offset %rbx, -24
.Lcfi1147:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB226_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16AcAbsdeclPointerEPNS_15impl_ac_pointerE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB226_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end226:
	.size	_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE, .Lfunc_end226-_ZN2kc44impl_ac_abstract_declarator_AcAbsdeclPointer7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE,@function
_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE: # @_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1148:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1149:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1150:
	.cfi_def_cfa_offset 32
.Lcfi1151:
	.cfi_offset %rbx, -24
.Lcfi1152:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB227_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc32Yesac_direct_abstract_declaratorEPNS_34impl_ac_direct_abstract_declaratorE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB227_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end227:
	.size	_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE, .Lfunc_end227-_ZN2kc74impl_ac_direct_abstract_declarator_option_Yesac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE,@function
_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE: # @_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end228:
	.size	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE, .Lfunc_end228-_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE,@function
_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE: # @_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1155:
	.cfi_def_cfa_offset 32
.Lcfi1156:
	.cfi_offset %rbx, -32
.Lcfi1157:
	.cfi_offset %r14, -24
.Lcfi1158:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB229_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB229_3
.LBB229_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc14AcDirAbsdeclFnEPNS_41impl_ac_direct_abstract_declarator_optionEPNS_27impl_ac_parameter_type_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB229_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end229:
	.size	_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE, .Lfunc_end229-_ZN2kc49impl_ac_direct_abstract_declarator_AcDirAbsdeclFn7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE,@function
_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE: # @_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1159:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1160:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1161:
	.cfi_def_cfa_offset 32
.Lcfi1162:
	.cfi_offset %rbx, -32
.Lcfi1163:
	.cfi_offset %r14, -24
.Lcfi1164:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB230_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB230_3
.LBB230_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc17AcDirAbsdeclArrayEPNS_41impl_ac_direct_abstract_declarator_optionEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB230_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end230:
	.size	_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE, .Lfunc_end230-_ZN2kc52impl_ac_direct_abstract_declarator_AcDirAbsdeclArray7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE,@function
_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE: # @_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1167:
	.cfi_def_cfa_offset 32
.Lcfi1168:
	.cfi_offset %rbx, -24
.Lcfi1169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB231_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc16AcDirAbsdeclPackEPNS_27impl_ac_abstract_declaratorE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB231_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end231:
	.size	_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE, .Lfunc_end231-_ZN2kc51impl_ac_direct_abstract_declarator_AcDirAbsdeclPack7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE,@function
_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE: # @_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end232:
	.size	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE, .Lfunc_end232-_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE,@function
_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE: # @_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1170:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1171:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1172:
	.cfi_def_cfa_offset 32
.Lcfi1173:
	.cfi_offset %rbx, -24
.Lcfi1174:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB233_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc25Yesac_constant_expressionEPNS_27impl_ac_constant_expressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB233_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end233:
	.size	_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE, .Lfunc_end233-_ZN2kc60impl_ac_constant_expression_option_Yesac_constant_expression7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE,@function
_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE: # @_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1175:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1176:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1177:
	.cfi_def_cfa_offset 32
.Lcfi1178:
	.cfi_offset %rbx, -24
.Lcfi1179:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB234_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc11AcConstExprEPNS_16impl_CexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB234_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end234:
	.size	_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE, .Lfunc_end234-_ZN2kc39impl_ac_constant_expression_AcConstExpr7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE: # @_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1180:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1181:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1182:
	.cfi_def_cfa_offset 32
.Lcfi1183:
	.cfi_offset %rbx, -32
.Lcfi1184:
	.cfi_offset %r14, -24
.Lcfi1185:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB235_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB235_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB235_4
.LBB235_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc31Consac_constant_expression_listEPNS_27impl_ac_constant_expressionEPNS_32impl_ac_constant_expression_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB235_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end235:
	.size	_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE, .Lfunc_end235-_ZN2kc32impl_ac_constant_expression_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE,@function
_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE: # @_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1186:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1187:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1188:
	.cfi_def_cfa_offset 32
.Lcfi1189:
	.cfi_offset %rbx, -24
.Lcfi1190:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB236_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13AcYesBaseInitEPNS_22impl_ac_base_init_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB236_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end236:
	.size	_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE, .Lfunc_end236-_ZN2kc40impl_ac_opt_base_init_list_AcYesBaseInit7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE,@function
_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE: # @_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end237:
	.size	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE, .Lfunc_end237-_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE: # @_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1191:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1192:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1193:
	.cfi_def_cfa_offset 32
.Lcfi1194:
	.cfi_offset %rbx, -32
.Lcfi1195:
	.cfi_offset %r14, -24
.Lcfi1196:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB238_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB238_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB238_4
.LBB238_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc21Consac_base_init_listEPNS_17impl_ac_base_initEPNS_22impl_ac_base_init_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB238_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end238:
	.size	_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE, .Lfunc_end238-_ZN2kc22impl_ac_base_init_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE: # @_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1197:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1198:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1199:
	.cfi_def_cfa_offset 32
.Lcfi1200:
	.cfi_offset %rbx, -32
.Lcfi1201:
	.cfi_offset %r14, -24
.Lcfi1202:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB239_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB239_3
.LBB239_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10AcBaseInitEPNS_7impl_IDEPNS_27impl_ac_constant_expressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB239_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end239:
	.size	_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE, .Lfunc_end239-_ZN2kc28impl_ac_base_init_AcBaseInit7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE: # @_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1203:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1204:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1205:
	.cfi_def_cfa_offset 32
.Lcfi1206:
	.cfi_offset %rbx, -32
.Lcfi1207:
	.cfi_offset %r14, -24
.Lcfi1208:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB240_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB240_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB240_4
.LBB240_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc26Consbaseclass_declarationsEPNS_19impl_baseclass_declEPNS_27impl_baseclass_declarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB240_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end240:
	.size	_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE, .Lfunc_end240-_ZN2kc27impl_baseclass_declarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE,@function
_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE: # @_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1209:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1210:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1211:
	.cfi_def_cfa_offset 32
.Lcfi1212:
	.cfi_offset %rbx, -32
.Lcfi1213:
	.cfi_offset %r14, -24
.Lcfi1214:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB241_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB241_3
.LBB241_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13BaseClassDeclEPNS_7impl_IDEPNS_19impl_baseclass_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB241_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end241:
	.size	_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE, .Lfunc_end241-_ZN2kc33impl_baseclass_decl_BaseClassDecl7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE: # @_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1215:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1216:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1217:
	.cfi_def_cfa_offset 32
.Lcfi1218:
	.cfi_offset %rbx, -32
.Lcfi1219:
	.cfi_offset %r14, -24
.Lcfi1220:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB242_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB242_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB242_4
.LBB242_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18Consbaseclass_listEPNS_7impl_IDEPNS_19impl_baseclass_listE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB242_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end242:
	.size	_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE, .Lfunc_end242-_ZN2kc19impl_baseclass_list7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE,@function
_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE: # @_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1221:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1222:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1223:
	.cfi_def_cfa_offset 32
.Lcfi1224:
	.cfi_offset %rbx, -32
.Lcfi1225:
	.cfi_offset %r14, -24
.Lcfi1226:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB243_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB243_3
.LBB243_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB243_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end243:
	.size	_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE, .Lfunc_end243-_ZN2kc18impl_error_Warning7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE: # @_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1227:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1228:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1229:
	.cfi_def_cfa_offset 32
.Lcfi1230:
	.cfi_offset %rbx, -32
.Lcfi1231:
	.cfi_offset %r14, -24
.Lcfi1232:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB244_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB244_3
.LBB244_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB244_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end244:
	.size	_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE, .Lfunc_end244-_ZN2kc19impl_error_NonFatal7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE,@function
_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE: # @_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1233:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1234:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1235:
	.cfi_def_cfa_offset 32
.Lcfi1236:
	.cfi_offset %rbx, -32
.Lcfi1237:
	.cfi_offset %r14, -24
.Lcfi1238:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB245_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB245_3
.LBB245_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB245_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end245:
	.size	_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE, .Lfunc_end245-_ZN2kc16impl_error_Fatal7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1244:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1245:
	.cfi_def_cfa_offset 64
.Lcfi1246:
	.cfi_offset %rbx, -56
.Lcfi1247:
	.cfi_offset %r12, -48
.Lcfi1248:
	.cfi_offset %r13, -40
.Lcfi1249:
	.cfi_offset %r14, -32
.Lcfi1250:
	.cfi_offset %r15, -24
.Lcfi1251:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpq	8(%rbx), %rdi
	jne	.LBB246_6
# BB#1:
	cmpq	16(%rbx), %rbp
	jne	.LBB246_6
# BB#2:
	cmpq	24(%rbx), %r15
	jne	.LBB246_6
# BB#3:
	cmpq	32(%rbx), %r12
	jne	.LBB246_6
# BB#4:
	cmpq	40(%rbx), %r14
	jne	.LBB246_6
# BB#5:
	cmpq	48(%rbx), %rax
	je	.LBB246_7
.LBB246_6:
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%rax, %r9
	callq	_ZN2kc8Problem6EPNS_20impl_casestring__StrES1_S1_S1_S1_S1_
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB246_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end246:
	.size	_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE, .Lfunc_end246-_ZN2kc21impl_problem_Problem67rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1252:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1253:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1254:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1255:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1256:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1257:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1258:
	.cfi_def_cfa_offset 64
.Lcfi1259:
	.cfi_offset %rbx, -56
.Lcfi1260:
	.cfi_offset %r12, -48
.Lcfi1261:
	.cfi_offset %r13, -40
.Lcfi1262:
	.cfi_offset %r14, -32
.Lcfi1263:
	.cfi_offset %r15, -24
.Lcfi1264:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB247_5
# BB#1:
	cmpq	16(%rbx), %r15
	jne	.LBB247_5
# BB#2:
	cmpq	24(%rbx), %r12
	jne	.LBB247_5
# BB#3:
	cmpq	32(%rbx), %rbp
	jne	.LBB247_5
# BB#4:
	cmpq	40(%rbx), %rax
	je	.LBB247_6
.LBB247_5:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r8
	callq	_ZN2kc8Problem5EPNS_20impl_casestring__StrES1_S1_S1_S1_
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB247_6:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end247:
	.size	_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE, .Lfunc_end247-_ZN2kc21impl_problem_Problem57rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1267:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1268:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1269:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1270:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1271:
	.cfi_def_cfa_offset 64
.Lcfi1272:
	.cfi_offset %rbx, -56
.Lcfi1273:
	.cfi_offset %r12, -48
.Lcfi1274:
	.cfi_offset %r13, -40
.Lcfi1275:
	.cfi_offset %r14, -32
.Lcfi1276:
	.cfi_offset %r15, -24
.Lcfi1277:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB248_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB248_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB248_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB248_5
.LBB248_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc8Problem4EPNS_20impl_casestring__StrES1_S1_S1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB248_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end248:
	.size	_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE, .Lfunc_end248-_ZN2kc21impl_problem_Problem47rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE,@function
_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE: # @_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1278:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1279:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1280:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1281:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1282:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1283:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1284:
	.cfi_def_cfa_offset 64
.Lcfi1285:
	.cfi_offset %rbx, -56
.Lcfi1286:
	.cfi_offset %r12, -48
.Lcfi1287:
	.cfi_offset %r13, -40
.Lcfi1288:
	.cfi_offset %r14, -32
.Lcfi1289:
	.cfi_offset %r15, -24
.Lcfi1290:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB249_5
# BB#1:
	cmpq	16(%rbx), %r15
	jne	.LBB249_5
# BB#2:
	cmpq	24(%rbx), %r12
	jne	.LBB249_5
# BB#3:
	cmpq	32(%rbx), %rbp
	jne	.LBB249_5
# BB#4:
	cmpq	40(%rbx), %rax
	je	.LBB249_6
.LBB249_5:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r8
	callq	_ZN2kc12Problem3int1EPNS_20impl_casestring__StrES1_S1_PNS_17impl_integer__IntES1_
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB249_6:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end249:
	.size	_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE, .Lfunc_end249-_ZN2kc25impl_problem_Problem3int17rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1291:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1292:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1293:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1294:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1295:
	.cfi_def_cfa_offset 48
.Lcfi1296:
	.cfi_offset %rbx, -48
.Lcfi1297:
	.cfi_offset %r12, -40
.Lcfi1298:
	.cfi_offset %r13, -32
.Lcfi1299:
	.cfi_offset %r14, -24
.Lcfi1300:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB250_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB250_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB250_4
.LBB250_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc8Problem3EPNS_20impl_casestring__StrES1_S1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB250_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end250:
	.size	_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE, .Lfunc_end250-_ZN2kc21impl_problem_Problem37rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1301:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1302:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1303:
	.cfi_def_cfa_offset 32
.Lcfi1304:
	.cfi_offset %rbx, -32
.Lcfi1305:
	.cfi_offset %r14, -24
.Lcfi1306:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB251_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB251_3
.LBB251_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8Problem2EPNS_20impl_casestring__StrES1_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB251_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end251:
	.size	_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE, .Lfunc_end251-_ZN2kc21impl_problem_Problem27rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE,@function
_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE: # @_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1307:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1308:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1309:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1310:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1311:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1312:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1313:
	.cfi_def_cfa_offset 64
.Lcfi1314:
	.cfi_offset %rbx, -56
.Lcfi1315:
	.cfi_offset %r12, -48
.Lcfi1316:
	.cfi_offset %r13, -40
.Lcfi1317:
	.cfi_offset %r14, -32
.Lcfi1318:
	.cfi_offset %r15, -24
.Lcfi1319:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB252_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB252_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB252_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB252_5
.LBB252_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc24Problem1storageoption1IDEPNS_20impl_casestring__StrEPNS_18impl_storageoptionES1_PNS_7impl_IDE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB252_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end252:
	.size	_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE, .Lfunc_end252-_ZN2kc37impl_problem_Problem1storageoption1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE: # @_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1320:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1321:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1322:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1323:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1324:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1325:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1326:
	.cfi_def_cfa_offset 64
.Lcfi1327:
	.cfi_offset %rbx, -56
.Lcfi1328:
	.cfi_offset %r12, -48
.Lcfi1329:
	.cfi_offset %r13, -40
.Lcfi1330:
	.cfi_offset %r14, -32
.Lcfi1331:
	.cfi_offset %r15, -24
.Lcfi1332:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpq	8(%rbx), %rdi
	jne	.LBB253_6
# BB#1:
	cmpq	16(%rbx), %rbp
	jne	.LBB253_6
# BB#2:
	cmpq	24(%rbx), %r15
	jne	.LBB253_6
# BB#3:
	cmpq	32(%rbx), %r12
	jne	.LBB253_6
# BB#4:
	cmpq	40(%rbx), %r14
	jne	.LBB253_6
# BB#5:
	cmpq	48(%rbx), %rax
	je	.LBB253_7
.LBB253_6:
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%rax, %r9
	callq	_ZN2kc17Problem1INT1ID1IDEPNS_20impl_casestring__StrEPNS_8impl_INTES1_PNS_7impl_IDES1_S5_
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB253_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end253:
	.size	_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE, .Lfunc_end253-_ZN2kc30impl_problem_Problem1INT1ID1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE,@function
_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE: # @_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1333:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1334:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1335:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1336:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1337:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1338:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1339:
	.cfi_def_cfa_offset 64
.Lcfi1340:
	.cfi_offset %rbx, -56
.Lcfi1341:
	.cfi_offset %r12, -48
.Lcfi1342:
	.cfi_offset %r13, -40
.Lcfi1343:
	.cfi_offset %r14, -32
.Lcfi1344:
	.cfi_offset %r15, -24
.Lcfi1345:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpq	8(%rbx), %rdi
	jne	.LBB254_6
# BB#1:
	cmpq	16(%rbx), %rbp
	jne	.LBB254_6
# BB#2:
	cmpq	24(%rbx), %r15
	jne	.LBB254_6
# BB#3:
	cmpq	32(%rbx), %r12
	jne	.LBB254_6
# BB#4:
	cmpq	40(%rbx), %r14
	jne	.LBB254_6
# BB#5:
	cmpq	48(%rbx), %rax
	je	.LBB254_7
.LBB254_6:
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%rax, %r9
	callq	_ZN2kc16Problem1ID1ID1IDEPNS_20impl_casestring__StrEPNS_7impl_IDES1_S3_S1_S3_
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%rbp, %rbx
.LBB254_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end254:
	.size	_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE, .Lfunc_end254-_ZN2kc29impl_problem_Problem1ID1ID1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE,@function
_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE: # @_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1346:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1347:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1348:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1349:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1350:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1351:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1352:
	.cfi_def_cfa_offset 64
.Lcfi1353:
	.cfi_offset %rbx, -56
.Lcfi1354:
	.cfi_offset %r12, -48
.Lcfi1355:
	.cfi_offset %r13, -40
.Lcfi1356:
	.cfi_offset %r14, -32
.Lcfi1357:
	.cfi_offset %r15, -24
.Lcfi1358:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB255_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB255_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB255_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB255_5
.LBB255_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc14Problem1INT1IDEPNS_20impl_casestring__StrEPNS_8impl_INTES1_PNS_7impl_IDE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB255_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end255:
	.size	_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE, .Lfunc_end255-_ZN2kc27impl_problem_Problem1INT1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE,@function
_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE: # @_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1359:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1360:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1361:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1362:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1363:
	.cfi_def_cfa_offset 48
.Lcfi1364:
	.cfi_offset %rbx, -48
.Lcfi1365:
	.cfi_offset %r12, -40
.Lcfi1366:
	.cfi_offset %r13, -32
.Lcfi1367:
	.cfi_offset %r14, -24
.Lcfi1368:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*24(%rax)
	cmpq	8(%r13), %r14
	jne	.LBB256_3
# BB#1:
	cmpq	16(%r13), %r15
	jne	.LBB256_3
# BB#2:
	cmpq	24(%r13), %rax
	je	.LBB256_4
.LBB256_3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12Problem1int1EPNS_20impl_casestring__StrEPNS_17impl_integer__IntES1_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*40(%rax)
	movq	%rbx, %r13
.LBB256_4:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end256:
	.size	_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE, .Lfunc_end256-_ZN2kc25impl_problem_Problem1int17rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE: # @_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1369:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1370:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1371:
	.cfi_def_cfa_offset 32
.Lcfi1372:
	.cfi_offset %rbx, -32
.Lcfi1373:
	.cfi_offset %r14, -24
.Lcfi1374:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB257_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB257_3
.LBB257_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11Problem1INTEPNS_20impl_casestring__StrEPNS_8impl_INTE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB257_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end257:
	.size	_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE, .Lfunc_end257-_ZN2kc24impl_problem_Problem1INT7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE,@function
_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE: # @_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1375:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1376:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1377:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1378:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1379:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1380:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1381:
	.cfi_def_cfa_offset 64
.Lcfi1382:
	.cfi_offset %rbx, -56
.Lcfi1383:
	.cfi_offset %r12, -48
.Lcfi1384:
	.cfi_offset %r13, -40
.Lcfi1385:
	.cfi_offset %r14, -32
.Lcfi1386:
	.cfi_offset %r15, -24
.Lcfi1387:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB258_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB258_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB258_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB258_5
.LBB258_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc12Problem1t1IDEPNS_20impl_casestring__StrEPNS_11impl_IDtypeES1_PNS_7impl_IDE
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB258_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end258:
	.size	_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE, .Lfunc_end258-_ZN2kc25impl_problem_Problem1t1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE,@function
_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE: # @_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1388:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1389:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi1390:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi1391:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi1392:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi1393:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi1394:
	.cfi_def_cfa_offset 64
.Lcfi1395:
	.cfi_offset %rbx, -56
.Lcfi1396:
	.cfi_offset %r12, -48
.Lcfi1397:
	.cfi_offset %r13, -40
.Lcfi1398:
	.cfi_offset %r14, -32
.Lcfi1399:
	.cfi_offset %r15, -24
.Lcfi1400:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	movq	%rax, %r12
	movq	32(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*24(%rax)
	cmpq	8(%rbp), %r14
	jne	.LBB259_4
# BB#1:
	cmpq	16(%rbp), %r15
	jne	.LBB259_4
# BB#2:
	cmpq	24(%rbp), %r12
	jne	.LBB259_4
# BB#3:
	cmpq	32(%rbp), %rax
	je	.LBB259_5
.LBB259_4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc13Problem1ID1IDEPNS_20impl_casestring__StrEPNS_7impl_IDES1_S3_
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movq	%rbx, %rbp
.LBB259_5:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end259:
	.size	_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE, .Lfunc_end259-_ZN2kc26impl_problem_Problem1ID1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE: # @_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1401:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1402:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1403:
	.cfi_def_cfa_offset 32
.Lcfi1404:
	.cfi_offset %rbx, -32
.Lcfi1405:
	.cfi_offset %r14, -24
.Lcfi1406:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB260_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB260_3
.LBB260_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10Problem1weEPNS_20impl_casestring__StrEPNS_19impl_withexpressionE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB260_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end260:
	.size	_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE, .Lfunc_end260-_ZN2kc23impl_problem_Problem1we7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE,@function
_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE: # @_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1407:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1408:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1409:
	.cfi_def_cfa_offset 32
.Lcfi1410:
	.cfi_offset %rbx, -32
.Lcfi1411:
	.cfi_offset %r14, -24
.Lcfi1412:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB261_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB261_3
.LBB261_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11Problem1tIDEPNS_20impl_casestring__StrEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB261_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end261:
	.size	_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE, .Lfunc_end261-_ZN2kc24impl_problem_Problem1tID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE,@function
_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE: # @_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1413:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1414:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1415:
	.cfi_def_cfa_offset 32
.Lcfi1416:
	.cfi_offset %rbx, -32
.Lcfi1417:
	.cfi_offset %r14, -24
.Lcfi1418:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r14
	jne	.LBB262_2
# BB#1:
	cmpq	16(%rbx), %rax
	je	.LBB262_3
.LBB262_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc10Problem1IDEPNS_20impl_casestring__StrEPNS_7impl_IDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB262_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end262:
	.size	_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE, .Lfunc_end262-_ZN2kc23impl_problem_Problem1ID7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE,@function
_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE: # @_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1419:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1420:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1421:
	.cfi_def_cfa_offset 32
.Lcfi1422:
	.cfi_offset %rbx, -24
.Lcfi1423:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	8(%rbx), %rax
	je	.LBB263_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc8Problem1EPNS_20impl_casestring__StrE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB263_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end263:
	.size	_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE, .Lfunc_end263-_ZN2kc21impl_problem_Problem17rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE: # @_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1424:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1425:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1426:
	.cfi_def_cfa_offset 32
.Lcfi1427:
	.cfi_offset %rbx, -32
.Lcfi1428:
	.cfi_offset %r14, -24
.Lcfi1429:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB264_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB264_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB264_4
.LBB264_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc27ConsaddedphylumdeclarationsEPNS_27impl_addedphylumdeclarationEPNS_28impl_addedphylumdeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB264_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end264:
	.size	_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE, .Lfunc_end264-_ZN2kc28impl_addedphylumdeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE: # @_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1430:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1431:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1432:
	.cfi_def_cfa_offset 32
.Lcfi1433:
	.cfi_offset %rbx, -24
.Lcfi1434:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	16(%rbx), %rax
	je	.LBB265_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc22AddedPhylumdeclarationEPNS_11impl_uniqIDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB265_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end265:
	.size	_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE, .Lfunc_end265-_ZN2kc50impl_addedphylumdeclaration_AddedPhylumdeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE,@function
_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE: # @_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1435:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1436:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1437:
	.cfi_def_cfa_offset 32
.Lcfi1438:
	.cfi_offset %rbx, -32
.Lcfi1439:
	.cfi_offset %r14, -24
.Lcfi1440:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB266_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB266_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB266_4
.LBB266_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc29ConscountedphylumdeclarationsEPNS_29impl_countedphylumdeclarationEPNS_30impl_countedphylumdeclarationsE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB266_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end266:
	.size	_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE, .Lfunc_end266-_ZN2kc30impl_countedphylumdeclarations7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE,@function
_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE: # @_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1441:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1442:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1443:
	.cfi_def_cfa_offset 32
.Lcfi1444:
	.cfi_offset %rbx, -24
.Lcfi1445:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	16(%rbx), %rax
	je	.LBB267_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc24CountedPhylumdeclarationEPNS_11impl_uniqIDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB267_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end267:
	.size	_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE, .Lfunc_end267-_ZN2kc54impl_countedphylumdeclaration_CountedPhylumdeclaration7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE: # @_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end268:
	.size	_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE, .Lfunc_end268-_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE,@function
_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE: # @_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end269:
	.size	_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE, .Lfunc_end269-_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE,@function
_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE: # @_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end270:
	.size	_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE, .Lfunc_end270-_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE,@function
_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE: # @_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1446:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1447:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi1448:
	.cfi_def_cfa_offset 32
.Lcfi1449:
	.cfi_offset %rbx, -32
.Lcfi1450:
	.cfi_offset %r14, -24
.Lcfi1451:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	testb	%al, %al
	jne	.LBB271_4
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
	cmpq	8(%rbx), %r15
	jne	.LBB271_3
# BB#2:
	cmpq	16(%rbx), %rax
	je	.LBB271_4
.LBB271_3:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc18ConsbindingidmarksEPNS_18impl_bindingidmarkEPNS_19impl_bindingidmarksE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB271_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end271:
	.size	_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE, .Lfunc_end271-_ZN2kc19impl_bindingidmarks7rewriteERNS_11rview_classE
	.cfi_endproc

	.globl	_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE,@function
_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE: # @_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1452:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1453:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1454:
	.cfi_def_cfa_offset 32
.Lcfi1455:
	.cfi_offset %rbx, -24
.Lcfi1456:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpq	16(%rbx), %rax
	je	.LBB272_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZN2kc13BindingIdMarkEPNS_11impl_uniqIDE
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movq	%r14, %rbx
.LBB272_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end272:
	.size	_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE, .Lfunc_end272-_ZN2kc32impl_bindingidmark_BindingIdMark7rewriteERNS_11rview_classE
	.cfi_endproc

	.section	.text._ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv,"axG",@progbits,_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv,comdat
	.weak	_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv,@function
_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv: # @_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$16, %eax
	retq
.Lfunc_end273:
	.size	_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv, .Lfunc_end273-_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE,"axG",@progbits,_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE,comdat
	.weak	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.p2align	4, 0x90
	.type	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE,@function
_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE: # @_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end274:
	.size	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE, .Lfunc_end274-_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.cfi_endproc

	.section	.text._ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_,"axG",@progbits,_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_,comdat
	.weak	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.p2align	4, 0x90
	.type	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_,@function
_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_: # @_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end275:
	.size	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_, .Lfunc_end275-_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.cfi_endproc

	.section	.text._ZN2kc20impl_abstract_phylum11post_createEv,"axG",@progbits,_ZN2kc20impl_abstract_phylum11post_createEv,comdat
	.weak	_ZN2kc20impl_abstract_phylum11post_createEv
	.p2align	4, 0x90
	.type	_ZN2kc20impl_abstract_phylum11post_createEv,@function
_ZN2kc20impl_abstract_phylum11post_createEv: # @_ZN2kc20impl_abstract_phylum11post_createEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end276:
	.size	_ZN2kc20impl_abstract_phylum11post_createEv, .Lfunc_end276-_ZN2kc20impl_abstract_phylum11post_createEv
	.cfi_endproc

	.section	.text._ZN2kc34impl_storageoption_NoStorageOptionD0Ev,"axG",@progbits,_ZN2kc34impl_storageoption_NoStorageOptionD0Ev,comdat
	.weak	_ZN2kc34impl_storageoption_NoStorageOptionD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc34impl_storageoption_NoStorageOptionD0Ev,@function
_ZN2kc34impl_storageoption_NoStorageOptionD0Ev: # @_ZN2kc34impl_storageoption_NoStorageOptionD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end277:
	.size	_ZN2kc34impl_storageoption_NoStorageOptionD0Ev, .Lfunc_end277-_ZN2kc34impl_storageoption_NoStorageOptionD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv,"axG",@progbits,_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv,comdat
	.weak	_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv,@function
_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv: # @_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$21, %eax
	retq
.Lfunc_end278:
	.size	_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv, .Lfunc_end278-_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc41impl_productionblock_EmptyproductionblockD0Ev,"axG",@progbits,_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev,comdat
	.weak	_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev,@function
_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev: # @_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end279:
	.size	_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev, .Lfunc_end279-_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv,"axG",@progbits,_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv,comdat
	.weak	_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv,@function
_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv: # @_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$35, %eax
	retq
.Lfunc_end280:
	.size	_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv, .Lfunc_end280-_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev,"axG",@progbits,_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev,comdat
	.weak	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev,@function
_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev: # @_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end281:
	.size	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev, .Lfunc_end281-_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv,"axG",@progbits,_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv,comdat
	.weak	_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv,@function
_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv: # @_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$41, %eax
	retq
.Lfunc_end282:
	.size	_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv, .Lfunc_end282-_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev,"axG",@progbits,_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev,comdat
	.weak	_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev,@function
_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev: # @_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end283:
	.size	_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev, .Lfunc_end283-_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv,"axG",@progbits,_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv,comdat
	.weak	_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv,@function
_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv: # @_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$49, %eax
	retq
.Lfunc_end284:
	.size	_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv, .Lfunc_end284-_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev,"axG",@progbits,_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev,comdat
	.weak	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev,@function
_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev: # @_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end285:
	.size	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev, .Lfunc_end285-_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv,"axG",@progbits,_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv,comdat
	.weak	_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv,@function
_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv: # @_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$53, %eax
	retq
.Lfunc_end286:
	.size	_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv, .Lfunc_end286-_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev,"axG",@progbits,_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev,comdat
	.weak	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev,@function
_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev: # @_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end287:
	.size	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev, .Lfunc_end287-_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc22impl_pattern_PWildcard8prod_selEv,"axG",@progbits,_ZNK2kc22impl_pattern_PWildcard8prod_selEv,comdat
	.weak	_ZNK2kc22impl_pattern_PWildcard8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc22impl_pattern_PWildcard8prod_selEv,@function
_ZNK2kc22impl_pattern_PWildcard8prod_selEv: # @_ZNK2kc22impl_pattern_PWildcard8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$88, %eax
	retq
.Lfunc_end288:
	.size	_ZNK2kc22impl_pattern_PWildcard8prod_selEv, .Lfunc_end288-_ZNK2kc22impl_pattern_PWildcard8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc22impl_pattern_PWildcardD0Ev,"axG",@progbits,_ZN2kc22impl_pattern_PWildcardD0Ev,comdat
	.weak	_ZN2kc22impl_pattern_PWildcardD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc22impl_pattern_PWildcardD0Ev,@function
_ZN2kc22impl_pattern_PWildcardD0Ev:     # @_ZN2kc22impl_pattern_PWildcardD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end289:
	.size	_ZN2kc22impl_pattern_PWildcardD0Ev, .Lfunc_end289-_ZN2kc22impl_pattern_PWildcardD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv,"axG",@progbits,_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv,comdat
	.weak	_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv,@function
_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv: # @_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$116, %eax
	retq
.Lfunc_end290:
	.size	_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv, .Lfunc_end290-_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc27impl_fnclass_ConvOperatorFnD0Ev,"axG",@progbits,_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev,comdat
	.weak	_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev,@function
_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev: # @_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end291:
	.size	_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev, .Lfunc_end291-_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc25impl_fnclass_DestructorFn8prod_selEv,"axG",@progbits,_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv,comdat
	.weak	_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv,@function
_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv: # @_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$115, %eax
	retq
.Lfunc_end292:
	.size	_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv, .Lfunc_end292-_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc25impl_fnclass_DestructorFnD0Ev,"axG",@progbits,_ZN2kc25impl_fnclass_DestructorFnD0Ev,comdat
	.weak	_ZN2kc25impl_fnclass_DestructorFnD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc25impl_fnclass_DestructorFnD0Ev,@function
_ZN2kc25impl_fnclass_DestructorFnD0Ev:  # @_ZN2kc25impl_fnclass_DestructorFnD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end293:
	.size	_ZN2kc25impl_fnclass_DestructorFnD0Ev, .Lfunc_end293-_ZN2kc25impl_fnclass_DestructorFnD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv,"axG",@progbits,_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv,comdat
	.weak	_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv,@function
_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv: # @_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$114, %eax
	retq
.Lfunc_end294:
	.size	_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv, .Lfunc_end294-_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc26impl_fnclass_ConstructorFnD0Ev,"axG",@progbits,_ZN2kc26impl_fnclass_ConstructorFnD0Ev,comdat
	.weak	_ZN2kc26impl_fnclass_ConstructorFnD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc26impl_fnclass_ConstructorFnD0Ev,@function
_ZN2kc26impl_fnclass_ConstructorFnD0Ev: # @_ZN2kc26impl_fnclass_ConstructorFnD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end295:
	.size	_ZN2kc26impl_fnclass_ConstructorFnD0Ev, .Lfunc_end295-_ZN2kc26impl_fnclass_ConstructorFnD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc21impl_fnclass_MemberFn8prod_selEv,"axG",@progbits,_ZNK2kc21impl_fnclass_MemberFn8prod_selEv,comdat
	.weak	_ZNK2kc21impl_fnclass_MemberFn8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc21impl_fnclass_MemberFn8prod_selEv,@function
_ZNK2kc21impl_fnclass_MemberFn8prod_selEv: # @_ZNK2kc21impl_fnclass_MemberFn8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$113, %eax
	retq
.Lfunc_end296:
	.size	_ZNK2kc21impl_fnclass_MemberFn8prod_selEv, .Lfunc_end296-_ZNK2kc21impl_fnclass_MemberFn8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc21impl_fnclass_MemberFnD0Ev,"axG",@progbits,_ZN2kc21impl_fnclass_MemberFnD0Ev,comdat
	.weak	_ZN2kc21impl_fnclass_MemberFnD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc21impl_fnclass_MemberFnD0Ev,@function
_ZN2kc21impl_fnclass_MemberFnD0Ev:      # @_ZN2kc21impl_fnclass_MemberFnD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end297:
	.size	_ZN2kc21impl_fnclass_MemberFnD0Ev, .Lfunc_end297-_ZN2kc21impl_fnclass_MemberFnD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc21impl_fnclass_GlobalFn8prod_selEv,"axG",@progbits,_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv,comdat
	.weak	_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv,@function
_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv: # @_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$111, %eax
	retq
.Lfunc_end298:
	.size	_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv, .Lfunc_end298-_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc21impl_fnclass_GlobalFnD0Ev,"axG",@progbits,_ZN2kc21impl_fnclass_GlobalFnD0Ev,comdat
	.weak	_ZN2kc21impl_fnclass_GlobalFnD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc21impl_fnclass_GlobalFnD0Ev,@function
_ZN2kc21impl_fnclass_GlobalFnD0Ev:      # @_ZN2kc21impl_fnclass_GlobalFnD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end299:
	.size	_ZN2kc21impl_fnclass_GlobalFnD0Ev, .Lfunc_end299-_ZN2kc21impl_fnclass_GlobalFnD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv,"axG",@progbits,_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv,comdat
	.weak	_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv,@function
_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv: # @_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$127, %eax
	retq
.Lfunc_end300:
	.size	_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv, .Lfunc_end300-_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc33impl_foreach_after_NoForeachAfterD0Ev,"axG",@progbits,_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev,comdat
	.weak	_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev,@function
_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev: # @_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end301:
	.size	_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev, .Lfunc_end301-_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv,"axG",@progbits,_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv,comdat
	.weak	_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv,@function
_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv: # @_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$130, %eax
	retq
.Lfunc_end302:
	.size	_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv, .Lfunc_end302-_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc36impl_contextinfo_NotInForeachContextD0Ev,"axG",@progbits,_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev,comdat
	.weak	_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev,@function
_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev: # @_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end303:
	.size	_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev, .Lfunc_end303-_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv,"axG",@progbits,_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv,comdat
	.weak	_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv,@function
_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv: # @_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$160, %eax
	retq
.Lfunc_end304:
	.size	_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv, .Lfunc_end304-_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc30impl_viewnameoption_NoViewnameD0Ev,"axG",@progbits,_ZN2kc30impl_viewnameoption_NoViewnameD0Ev,comdat
	.weak	_ZN2kc30impl_viewnameoption_NoViewnameD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc30impl_viewnameoption_NoViewnameD0Ev,@function
_ZN2kc30impl_viewnameoption_NoViewnameD0Ev: # @_ZN2kc30impl_viewnameoption_NoViewnameD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end305:
	.size	_ZN2kc30impl_viewnameoption_NoViewnameD0Ev, .Lfunc_end305-_ZN2kc30impl_viewnameoption_NoViewnameD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv,"axG",@progbits,_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv,comdat
	.weak	_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv,@function
_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv: # @_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$162, %eax
	retq
.Lfunc_end306:
	.size	_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv, .Lfunc_end306-_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc34impl_languageoption_NoLanguagenameD0Ev,"axG",@progbits,_ZN2kc34impl_languageoption_NoLanguagenameD0Ev,comdat
	.weak	_ZN2kc34impl_languageoption_NoLanguagenameD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc34impl_languageoption_NoLanguagenameD0Ev,@function
_ZN2kc34impl_languageoption_NoLanguagenameD0Ev: # @_ZN2kc34impl_languageoption_NoLanguagenameD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end307:
	.size	_ZN2kc34impl_languageoption_NoLanguagenameD0Ev, .Lfunc_end307-_ZN2kc34impl_languageoption_NoLanguagenameD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv,"axG",@progbits,_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv,comdat
	.weak	_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv,@function
_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv: # @_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$168, %eax
	retq
.Lfunc_end308:
	.size	_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv, .Lfunc_end308-_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc27impl_fileline_PosNoFileLineD0Ev,"axG",@progbits,_ZN2kc27impl_fileline_PosNoFileLineD0Ev,comdat
	.weak	_ZN2kc27impl_fileline_PosNoFileLineD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc27impl_fileline_PosNoFileLineD0Ev,@function
_ZN2kc27impl_fileline_PosNoFileLineD0Ev: # @_ZN2kc27impl_fileline_PosNoFileLineD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end309:
	.size	_ZN2kc27impl_fileline_PosNoFileLineD0Ev, .Lfunc_end309-_ZN2kc27impl_fileline_PosNoFileLineD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc24impl_fileline_NoFileLine8prod_selEv,"axG",@progbits,_ZNK2kc24impl_fileline_NoFileLine8prod_selEv,comdat
	.weak	_ZNK2kc24impl_fileline_NoFileLine8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc24impl_fileline_NoFileLine8prod_selEv,@function
_ZNK2kc24impl_fileline_NoFileLine8prod_selEv: # @_ZNK2kc24impl_fileline_NoFileLine8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$167, %eax
	retq
.Lfunc_end310:
	.size	_ZNK2kc24impl_fileline_NoFileLine8prod_selEv, .Lfunc_end310-_ZNK2kc24impl_fileline_NoFileLine8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc24impl_fileline_NoFileLineD0Ev,"axG",@progbits,_ZN2kc24impl_fileline_NoFileLineD0Ev,comdat
	.weak	_ZN2kc24impl_fileline_NoFileLineD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc24impl_fileline_NoFileLineD0Ev,@function
_ZN2kc24impl_fileline_NoFileLineD0Ev:   # @_ZN2kc24impl_fileline_NoFileLineD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end311:
	.size	_ZN2kc24impl_fileline_NoFileLineD0Ev, .Lfunc_end311-_ZN2kc24impl_fileline_NoFileLineD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc22impl_fileline_FileLine8prod_selEv,"axG",@progbits,_ZNK2kc22impl_fileline_FileLine8prod_selEv,comdat
	.weak	_ZNK2kc22impl_fileline_FileLine8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc22impl_fileline_FileLine8prod_selEv,@function
_ZNK2kc22impl_fileline_FileLine8prod_selEv: # @_ZNK2kc22impl_fileline_FileLine8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$166, %eax
	retq
.Lfunc_end312:
	.size	_ZNK2kc22impl_fileline_FileLine8prod_selEv, .Lfunc_end312-_ZNK2kc22impl_fileline_FileLine8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc22impl_fileline_FileLineD0Ev,"axG",@progbits,_ZN2kc22impl_fileline_FileLineD0Ev,comdat
	.weak	_ZN2kc22impl_fileline_FileLineD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc22impl_fileline_FileLineD0Ev,@function
_ZN2kc22impl_fileline_FileLineD0Ev:     # @_ZN2kc22impl_fileline_FileLineD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end313:
	.size	_ZN2kc22impl_fileline_FileLineD0Ev, .Lfunc_end313-_ZN2kc22impl_fileline_FileLineD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc23impl_IDtype_ITUserRView8prod_selEv,"axG",@progbits,_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv,comdat
	.weak	_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv,@function
_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv: # @_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$183, %eax
	retq
.Lfunc_end314:
	.size	_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv, .Lfunc_end314-_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc23impl_IDtype_ITUserRViewD0Ev,"axG",@progbits,_ZN2kc23impl_IDtype_ITUserRViewD0Ev,comdat
	.weak	_ZN2kc23impl_IDtype_ITUserRViewD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc23impl_IDtype_ITUserRViewD0Ev,@function
_ZN2kc23impl_IDtype_ITUserRViewD0Ev:    # @_ZN2kc23impl_IDtype_ITUserRViewD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end315:
	.size	_ZN2kc23impl_IDtype_ITUserRViewD0Ev, .Lfunc_end315-_ZN2kc23impl_IDtype_ITUserRViewD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv,"axG",@progbits,_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv,comdat
	.weak	_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv,@function
_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv: # @_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$182, %eax
	retq
.Lfunc_end316:
	.size	_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv, .Lfunc_end316-_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev,"axG",@progbits,_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev,comdat
	.weak	_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev,@function
_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev: # @_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end317:
	.size	_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev, .Lfunc_end317-_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv,"axG",@progbits,_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv,comdat
	.weak	_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv,@function
_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv: # @_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$181, %eax
	retq
.Lfunc_end318:
	.size	_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv, .Lfunc_end318-_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc22impl_IDtype_ITUViewVarD0Ev,"axG",@progbits,_ZN2kc22impl_IDtype_ITUViewVarD0Ev,comdat
	.weak	_ZN2kc22impl_IDtype_ITUViewVarD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc22impl_IDtype_ITUViewVarD0Ev,@function
_ZN2kc22impl_IDtype_ITUViewVarD0Ev:     # @_ZN2kc22impl_IDtype_ITUViewVarD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end319:
	.size	_ZN2kc22impl_IDtype_ITUViewVarD0Ev, .Lfunc_end319-_ZN2kc22impl_IDtype_ITUViewVarD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc23impl_IDtype_ITUserUView8prod_selEv,"axG",@progbits,_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv,comdat
	.weak	_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv,@function
_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv: # @_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$180, %eax
	retq
.Lfunc_end320:
	.size	_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv, .Lfunc_end320-_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc23impl_IDtype_ITUserUViewD0Ev,"axG",@progbits,_ZN2kc23impl_IDtype_ITUserUViewD0Ev,comdat
	.weak	_ZN2kc23impl_IDtype_ITUserUViewD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc23impl_IDtype_ITUserUViewD0Ev,@function
_ZN2kc23impl_IDtype_ITUserUViewD0Ev:    # @_ZN2kc23impl_IDtype_ITUserUViewD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end321:
	.size	_ZN2kc23impl_IDtype_ITUserUViewD0Ev, .Lfunc_end321-_ZN2kc23impl_IDtype_ITUserUViewD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv,"axG",@progbits,_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv,comdat
	.weak	_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv,@function
_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv: # @_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$179, %eax
	retq
.Lfunc_end322:
	.size	_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv, .Lfunc_end322-_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev,"axG",@progbits,_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev,comdat
	.weak	_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev,@function
_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev: # @_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end323:
	.size	_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev, .Lfunc_end323-_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv,"axG",@progbits,_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv,comdat
	.weak	_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv,@function
_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv: # @_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$178, %eax
	retq
.Lfunc_end324:
	.size	_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv, .Lfunc_end324-_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc26impl_IDtype_ITStorageClassD0Ev,"axG",@progbits,_ZN2kc26impl_IDtype_ITStorageClassD0Ev,comdat
	.weak	_ZN2kc26impl_IDtype_ITStorageClassD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc26impl_IDtype_ITStorageClassD0Ev,@function
_ZN2kc26impl_IDtype_ITStorageClassD0Ev: # @_ZN2kc26impl_IDtype_ITStorageClassD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end325:
	.size	_ZN2kc26impl_IDtype_ITStorageClassD0Ev, .Lfunc_end325-_ZN2kc26impl_IDtype_ITStorageClassD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv,"axG",@progbits,_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv,comdat
	.weak	_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv,@function
_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv: # @_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$177, %eax
	retq
.Lfunc_end326:
	.size	_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv, .Lfunc_end326-_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev,"axG",@progbits,_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev,comdat
	.weak	_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev,@function
_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev: # @_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end327:
	.size	_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev, .Lfunc_end327-_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc21impl_IDtype_ITUnknown8prod_selEv,"axG",@progbits,_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv,comdat
	.weak	_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv,@function
_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv: # @_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$172, %eax
	retq
.Lfunc_end328:
	.size	_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv, .Lfunc_end328-_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc21impl_IDtype_ITUnknownD0Ev,"axG",@progbits,_ZN2kc21impl_IDtype_ITUnknownD0Ev,comdat
	.weak	_ZN2kc21impl_IDtype_ITUnknownD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc21impl_IDtype_ITUnknownD0Ev,@function
_ZN2kc21impl_IDtype_ITUnknownD0Ev:      # @_ZN2kc21impl_IDtype_ITUnknownD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end329:
	.size	_ZN2kc21impl_IDtype_ITUnknownD0Ev, .Lfunc_end329-_ZN2kc21impl_IDtype_ITUnknownD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv,"axG",@progbits,_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv,comdat
	.weak	_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv,@function
_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv: # @_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$194, %eax
	retq
.Lfunc_end330:
	.size	_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv, .Lfunc_end330-_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev,"axG",@progbits,_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev,comdat
	.weak	_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev,@function
_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev: # @_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end331:
	.size	_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev, .Lfunc_end331-_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv,"axG",@progbits,_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv,comdat
	.weak	_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv,@function
_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv: # @_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$193, %eax
	retq
.Lfunc_end332:
	.size	_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv, .Lfunc_end332-_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev,"axG",@progbits,_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev,comdat
	.weak	_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev,@function
_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev: # @_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end333:
	.size	_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev, .Lfunc_end333-_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc19impl_tribool_Bigger8prod_selEv,"axG",@progbits,_ZNK2kc19impl_tribool_Bigger8prod_selEv,comdat
	.weak	_ZNK2kc19impl_tribool_Bigger8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc19impl_tribool_Bigger8prod_selEv,@function
_ZNK2kc19impl_tribool_Bigger8prod_selEv: # @_ZNK2kc19impl_tribool_Bigger8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$197, %eax
	retq
.Lfunc_end334:
	.size	_ZNK2kc19impl_tribool_Bigger8prod_selEv, .Lfunc_end334-_ZNK2kc19impl_tribool_Bigger8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc19impl_tribool_BiggerD0Ev,"axG",@progbits,_ZN2kc19impl_tribool_BiggerD0Ev,comdat
	.weak	_ZN2kc19impl_tribool_BiggerD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc19impl_tribool_BiggerD0Ev,@function
_ZN2kc19impl_tribool_BiggerD0Ev:        # @_ZN2kc19impl_tribool_BiggerD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end335:
	.size	_ZN2kc19impl_tribool_BiggerD0Ev, .Lfunc_end335-_ZN2kc19impl_tribool_BiggerD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc20impl_tribool_Smaller8prod_selEv,"axG",@progbits,_ZNK2kc20impl_tribool_Smaller8prod_selEv,comdat
	.weak	_ZNK2kc20impl_tribool_Smaller8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc20impl_tribool_Smaller8prod_selEv,@function
_ZNK2kc20impl_tribool_Smaller8prod_selEv: # @_ZNK2kc20impl_tribool_Smaller8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$196, %eax
	retq
.Lfunc_end336:
	.size	_ZNK2kc20impl_tribool_Smaller8prod_selEv, .Lfunc_end336-_ZNK2kc20impl_tribool_Smaller8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc20impl_tribool_SmallerD0Ev,"axG",@progbits,_ZN2kc20impl_tribool_SmallerD0Ev,comdat
	.weak	_ZN2kc20impl_tribool_SmallerD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc20impl_tribool_SmallerD0Ev,@function
_ZN2kc20impl_tribool_SmallerD0Ev:       # @_ZN2kc20impl_tribool_SmallerD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end337:
	.size	_ZN2kc20impl_tribool_SmallerD0Ev, .Lfunc_end337-_ZN2kc20impl_tribool_SmallerD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc18impl_tribool_Equal8prod_selEv,"axG",@progbits,_ZNK2kc18impl_tribool_Equal8prod_selEv,comdat
	.weak	_ZNK2kc18impl_tribool_Equal8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc18impl_tribool_Equal8prod_selEv,@function
_ZNK2kc18impl_tribool_Equal8prod_selEv: # @_ZNK2kc18impl_tribool_Equal8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$195, %eax
	retq
.Lfunc_end338:
	.size	_ZNK2kc18impl_tribool_Equal8prod_selEv, .Lfunc_end338-_ZNK2kc18impl_tribool_Equal8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc18impl_tribool_EqualD0Ev,"axG",@progbits,_ZN2kc18impl_tribool_EqualD0Ev,comdat
	.weak	_ZN2kc18impl_tribool_EqualD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc18impl_tribool_EqualD0Ev,@function
_ZN2kc18impl_tribool_EqualD0Ev:         # @_ZN2kc18impl_tribool_EqualD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end339:
	.size	_ZN2kc18impl_tribool_EqualD0Ev, .Lfunc_end339-_ZN2kc18impl_tribool_EqualD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv,"axG",@progbits,_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv,comdat
	.weak	_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv,@function
_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv: # @_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$208, %eax
	retq
.Lfunc_end340:
	.size	_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv, .Lfunc_end340-_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev,"axG",@progbits,_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev,comdat
	.weak	_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev,@function
_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev: # @_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end341:
	.size	_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev, .Lfunc_end341-_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv,"axG",@progbits,_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv,comdat
	.weak	_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv,@function
_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv: # @_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$245, %eax
	retq
.Lfunc_end342:
	.size	_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv, .Lfunc_end342-_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev,"axG",@progbits,_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev,comdat
	.weak	_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev,@function
_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev: # @_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end343:
	.size	_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev, .Lfunc_end343-_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv,"axG",@progbits,_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv,comdat
	.weak	_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv,@function
_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv: # @_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$244, %eax
	retq
.Lfunc_end344:
	.size	_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv, .Lfunc_end344-_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev,"axG",@progbits,_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev,comdat
	.weak	_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev,@function
_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev: # @_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end345:
	.size	_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev, .Lfunc_end345-_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv,"axG",@progbits,_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv,comdat
	.weak	_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv,@function
_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv: # @_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$243, %eax
	retq
.Lfunc_end346:
	.size	_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv, .Lfunc_end346-_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev,"axG",@progbits,_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev,comdat
	.weak	_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev,@function
_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev: # @_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end347:
	.size	_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev, .Lfunc_end347-_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv,"axG",@progbits,_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv,comdat
	.weak	_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv,@function
_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv: # @_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$242, %eax
	retq
.Lfunc_end348:
	.size	_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv, .Lfunc_end348-_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev,"axG",@progbits,_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev,comdat
	.weak	_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev,@function
_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev: # @_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end349:
	.size	_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev, .Lfunc_end349-_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv,"axG",@progbits,_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv,comdat
	.weak	_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv,@function
_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv: # @_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$241, %eax
	retq
.Lfunc_end350:
	.size	_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv, .Lfunc_end350-_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev,"axG",@progbits,_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev,comdat
	.weak	_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev,@function
_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev: # @_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end351:
	.size	_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev, .Lfunc_end351-_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv,"axG",@progbits,_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv,comdat
	.weak	_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv,@function
_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv: # @_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$240, %eax
	retq
.Lfunc_end352:
	.size	_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv, .Lfunc_end352-_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev,"axG",@progbits,_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev,comdat
	.weak	_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev,@function
_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev: # @_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end353:
	.size	_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev, .Lfunc_end353-_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv,"axG",@progbits,_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv,comdat
	.weak	_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv,@function
_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv: # @_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$250, %eax
	retq
.Lfunc_end354:
	.size	_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv, .Lfunc_end354-_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev,"axG",@progbits,_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev,comdat
	.weak	_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev,@function
_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev: # @_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end355:
	.size	_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev, .Lfunc_end355-_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv,"axG",@progbits,_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv,comdat
	.weak	_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv,@function
_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv: # @_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$249, %eax
	retq
.Lfunc_end356:
	.size	_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv, .Lfunc_end356-_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev,"axG",@progbits,_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev,comdat
	.weak	_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev,@function
_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev: # @_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end357:
	.size	_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev, .Lfunc_end357-_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv,"axG",@progbits,_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv,comdat
	.weak	_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv,@function
_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv: # @_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$248, %eax
	retq
.Lfunc_end358:
	.size	_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv, .Lfunc_end358-_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev,"axG",@progbits,_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev,comdat
	.weak	_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev,@function
_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev: # @_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end359:
	.size	_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev, .Lfunc_end359-_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv,"axG",@progbits,_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv,comdat
	.weak	_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv,@function
_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv: # @_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$247, %eax
	retq
.Lfunc_end360:
	.size	_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv, .Lfunc_end360-_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc20impl_abstract_phylumD2Ev,"axG",@progbits,_ZN2kc20impl_abstract_phylumD2Ev,comdat
	.weak	_ZN2kc20impl_abstract_phylumD2Ev
	.p2align	4, 0x90
	.type	_ZN2kc20impl_abstract_phylumD2Ev,@function
_ZN2kc20impl_abstract_phylumD2Ev:       # @_ZN2kc20impl_abstract_phylumD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end361:
	.size	_ZN2kc20impl_abstract_phylumD2Ev, .Lfunc_end361-_ZN2kc20impl_abstract_phylumD2Ev
	.cfi_endproc

	.section	.text._ZN2kc30impl_ac_type_qualifier_AcConstD0Ev,"axG",@progbits,_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev,comdat
	.weak	_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev,@function
_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev: # @_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end362:
	.size	_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev, .Lfunc_end362-_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv,"axG",@progbits,_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv,comdat
	.weak	_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv,@function
_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv: # @_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$263, %eax              # imm = 0x107
	retq
.Lfunc_end363:
	.size	_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv, .Lfunc_end363-_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc32impl_ac_pointer_option_NopointerD0Ev,"axG",@progbits,_ZN2kc32impl_ac_pointer_option_NopointerD0Ev,comdat
	.weak	_ZN2kc32impl_ac_pointer_option_NopointerD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc32impl_ac_pointer_option_NopointerD0Ev,@function
_ZN2kc32impl_ac_pointer_option_NopointerD0Ev: # @_ZN2kc32impl_ac_pointer_option_NopointerD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end364:
	.size	_ZN2kc32impl_ac_pointer_option_NopointerD0Ev, .Lfunc_end364-_ZN2kc32impl_ac_pointer_option_NopointerD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv,"axG",@progbits,_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv,comdat
	.weak	_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv,@function
_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv: # @_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$268, %eax              # imm = 0x10C
	retq
.Lfunc_end365:
	.size	_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv, .Lfunc_end365-_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc24impl_ac_ref_option_AcRefD0Ev,"axG",@progbits,_ZN2kc24impl_ac_ref_option_AcRefD0Ev,comdat
	.weak	_ZN2kc24impl_ac_ref_option_AcRefD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc24impl_ac_ref_option_AcRefD0Ev,@function
_ZN2kc24impl_ac_ref_option_AcRefD0Ev:   # @_ZN2kc24impl_ac_ref_option_AcRefD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end366:
	.size	_ZN2kc24impl_ac_ref_option_AcRefD0Ev, .Lfunc_end366-_ZN2kc24impl_ac_ref_option_AcRefD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv,"axG",@progbits,_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv,comdat
	.weak	_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv,@function
_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv: # @_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$267, %eax              # imm = 0x10B
	retq
.Lfunc_end367:
	.size	_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv, .Lfunc_end367-_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc26impl_ac_ref_option_AcNoRefD0Ev,"axG",@progbits,_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev,comdat
	.weak	_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev,@function
_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev: # @_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end368:
	.size	_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev, .Lfunc_end368-_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv,"axG",@progbits,_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv,comdat
	.weak	_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv,@function
_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv: # @_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$286, %eax              # imm = 0x11E
	retq
.Lfunc_end369:
	.size	_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv, .Lfunc_end369-_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev,"axG",@progbits,_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev,comdat
	.weak	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev,@function
_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev: # @_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end370:
	.size	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev, .Lfunc_end370-_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv,"axG",@progbits,_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv,comdat
	.weak	_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv,@function
_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv: # @_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$292, %eax              # imm = 0x124
	retq
.Lfunc_end371:
	.size	_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv, .Lfunc_end371-_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev,"axG",@progbits,_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev,comdat
	.weak	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev,@function
_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev: # @_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end372:
	.size	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev, .Lfunc_end372-_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv,"axG",@progbits,_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv,comdat
	.weak	_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv,@function
_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv: # @_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$296, %eax              # imm = 0x128
	retq
.Lfunc_end373:
	.size	_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv, .Lfunc_end373-_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev,"axG",@progbits,_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev,comdat
	.weak	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev,@function
_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev: # @_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end374:
	.size	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev, .Lfunc_end374-_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc19impl_charruns_Stars8prod_selEv,"axG",@progbits,_ZNK2kc19impl_charruns_Stars8prod_selEv,comdat
	.weak	_ZNK2kc19impl_charruns_Stars8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc19impl_charruns_Stars8prod_selEv,@function
_ZNK2kc19impl_charruns_Stars8prod_selEv: # @_ZNK2kc19impl_charruns_Stars8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$335, %eax              # imm = 0x14F
	retq
.Lfunc_end375:
	.size	_ZNK2kc19impl_charruns_Stars8prod_selEv, .Lfunc_end375-_ZNK2kc19impl_charruns_Stars8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc19impl_charruns_StarsD0Ev,"axG",@progbits,_ZN2kc19impl_charruns_StarsD0Ev,comdat
	.weak	_ZN2kc19impl_charruns_StarsD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc19impl_charruns_StarsD0Ev,@function
_ZN2kc19impl_charruns_StarsD0Ev:        # @_ZN2kc19impl_charruns_StarsD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end376:
	.size	_ZN2kc19impl_charruns_StarsD0Ev, .Lfunc_end376-_ZN2kc19impl_charruns_StarsD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv,"axG",@progbits,_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv,comdat
	.weak	_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv,@function
_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv: # @_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$334, %eax              # imm = 0x14E
	retq
.Lfunc_end377:
	.size	_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv, .Lfunc_end377-_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc28impl_charruns_QuotedNewlinesD0Ev,"axG",@progbits,_ZN2kc28impl_charruns_QuotedNewlinesD0Ev,comdat
	.weak	_ZN2kc28impl_charruns_QuotedNewlinesD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc28impl_charruns_QuotedNewlinesD0Ev,@function
_ZN2kc28impl_charruns_QuotedNewlinesD0Ev: # @_ZN2kc28impl_charruns_QuotedNewlinesD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end378:
	.size	_ZN2kc28impl_charruns_QuotedNewlinesD0Ev, .Lfunc_end378-_ZN2kc28impl_charruns_QuotedNewlinesD0Ev
	.cfi_endproc

	.section	.text._ZNK2kc22impl_charruns_Newlines8prod_selEv,"axG",@progbits,_ZNK2kc22impl_charruns_Newlines8prod_selEv,comdat
	.weak	_ZNK2kc22impl_charruns_Newlines8prod_selEv
	.p2align	4, 0x90
	.type	_ZNK2kc22impl_charruns_Newlines8prod_selEv,@function
_ZNK2kc22impl_charruns_Newlines8prod_selEv: # @_ZNK2kc22impl_charruns_Newlines8prod_selEv
	.cfi_startproc
# BB#0:
	movl	$333, %eax              # imm = 0x14D
	retq
.Lfunc_end379:
	.size	_ZNK2kc22impl_charruns_Newlines8prod_selEv, .Lfunc_end379-_ZNK2kc22impl_charruns_Newlines8prod_selEv
	.cfi_endproc

	.section	.text._ZN2kc22impl_charruns_NewlinesD0Ev,"axG",@progbits,_ZN2kc22impl_charruns_NewlinesD0Ev,comdat
	.weak	_ZN2kc22impl_charruns_NewlinesD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc22impl_charruns_NewlinesD0Ev,@function
_ZN2kc22impl_charruns_NewlinesD0Ev:     # @_ZN2kc22impl_charruns_NewlinesD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end380:
	.size	_ZN2kc22impl_charruns_NewlinesD0Ev, .Lfunc_end380-_ZN2kc22impl_charruns_NewlinesD0Ev
	.cfi_endproc

	.type	_ZN2kc10base_rviewE,@object # @_ZN2kc10base_rviewE
	.bss
	.globl	_ZN2kc10base_rviewE
	.p2align	2
_ZN2kc10base_rviewE:
	.zero	4
	.size	_ZN2kc10base_rviewE, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"base_rview"
	.size	.L.str, 11

	.type	_ZN2kc6rviewsE,@object  # @_ZN2kc6rviewsE
	.data
	.globl	_ZN2kc6rviewsE
	.p2align	4
_ZN2kc6rviewsE:
	.quad	.L.str
	.quad	_ZN2kc10base_rviewE
	.zero	16
	.size	_ZN2kc6rviewsE, 32

	.type	_ZTVN2kc34impl_storageoption_NoStorageOptionE,@object # @_ZTVN2kc34impl_storageoption_NoStorageOptionE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN2kc34impl_storageoption_NoStorageOptionE
	.p2align	3
_ZTVN2kc34impl_storageoption_NoStorageOptionE:
	.quad	0
	.quad	_ZTIN2kc34impl_storageoption_NoStorageOptionE
	.quad	_ZNK2kc34impl_storageoption_NoStorageOption8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc34impl_storageoption_NoStorageOption7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc34impl_storageoption_NoStorageOptionD0Ev
	.quad	_ZN2kc34impl_storageoption_NoStorageOption10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc34impl_storageoption_NoStorageOptionE, 104

	.type	_ZTSN2kc34impl_storageoption_NoStorageOptionE,@object # @_ZTSN2kc34impl_storageoption_NoStorageOptionE
	.globl	_ZTSN2kc34impl_storageoption_NoStorageOptionE
	.p2align	4
_ZTSN2kc34impl_storageoption_NoStorageOptionE:
	.asciz	"N2kc34impl_storageoption_NoStorageOptionE"
	.size	_ZTSN2kc34impl_storageoption_NoStorageOptionE, 42

	.type	_ZTSN2kc18impl_storageoptionE,@object # @_ZTSN2kc18impl_storageoptionE
	.section	.rodata._ZTSN2kc18impl_storageoptionE,"aG",@progbits,_ZTSN2kc18impl_storageoptionE,comdat
	.weak	_ZTSN2kc18impl_storageoptionE
	.p2align	4
_ZTSN2kc18impl_storageoptionE:
	.asciz	"N2kc18impl_storageoptionE"
	.size	_ZTSN2kc18impl_storageoptionE, 26

	.type	_ZTIN2kc18impl_storageoptionE,@object # @_ZTIN2kc18impl_storageoptionE
	.section	.rodata._ZTIN2kc18impl_storageoptionE,"aG",@progbits,_ZTIN2kc18impl_storageoptionE,comdat
	.weak	_ZTIN2kc18impl_storageoptionE
	.p2align	4
_ZTIN2kc18impl_storageoptionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc18impl_storageoptionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc18impl_storageoptionE, 24

	.type	_ZTIN2kc34impl_storageoption_NoStorageOptionE,@object # @_ZTIN2kc34impl_storageoption_NoStorageOptionE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc34impl_storageoption_NoStorageOptionE
	.p2align	4
_ZTIN2kc34impl_storageoption_NoStorageOptionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc34impl_storageoption_NoStorageOptionE
	.quad	_ZTIN2kc18impl_storageoptionE
	.size	_ZTIN2kc34impl_storageoption_NoStorageOptionE, 24

	.type	_ZTVN2kc41impl_productionblock_EmptyproductionblockE,@object # @_ZTVN2kc41impl_productionblock_EmptyproductionblockE
	.globl	_ZTVN2kc41impl_productionblock_EmptyproductionblockE
	.p2align	3
_ZTVN2kc41impl_productionblock_EmptyproductionblockE:
	.quad	0
	.quad	_ZTIN2kc41impl_productionblock_EmptyproductionblockE
	.quad	_ZNK2kc41impl_productionblock_Emptyproductionblock8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc41impl_productionblock_Emptyproductionblock7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc41impl_productionblock_EmptyproductionblockD0Ev
	.quad	_ZN2kc41impl_productionblock_Emptyproductionblock10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc41impl_productionblock_EmptyproductionblockE, 104

	.type	_ZTSN2kc41impl_productionblock_EmptyproductionblockE,@object # @_ZTSN2kc41impl_productionblock_EmptyproductionblockE
	.globl	_ZTSN2kc41impl_productionblock_EmptyproductionblockE
	.p2align	4
_ZTSN2kc41impl_productionblock_EmptyproductionblockE:
	.asciz	"N2kc41impl_productionblock_EmptyproductionblockE"
	.size	_ZTSN2kc41impl_productionblock_EmptyproductionblockE, 49

	.type	_ZTSN2kc20impl_productionblockE,@object # @_ZTSN2kc20impl_productionblockE
	.section	.rodata._ZTSN2kc20impl_productionblockE,"aG",@progbits,_ZTSN2kc20impl_productionblockE,comdat
	.weak	_ZTSN2kc20impl_productionblockE
	.p2align	4
_ZTSN2kc20impl_productionblockE:
	.asciz	"N2kc20impl_productionblockE"
	.size	_ZTSN2kc20impl_productionblockE, 28

	.type	_ZTIN2kc20impl_productionblockE,@object # @_ZTIN2kc20impl_productionblockE
	.section	.rodata._ZTIN2kc20impl_productionblockE,"aG",@progbits,_ZTIN2kc20impl_productionblockE,comdat
	.weak	_ZTIN2kc20impl_productionblockE
	.p2align	4
_ZTIN2kc20impl_productionblockE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc20impl_productionblockE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc20impl_productionblockE, 24

	.type	_ZTIN2kc41impl_productionblock_EmptyproductionblockE,@object # @_ZTIN2kc41impl_productionblock_EmptyproductionblockE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc41impl_productionblock_EmptyproductionblockE
	.p2align	4
_ZTIN2kc41impl_productionblock_EmptyproductionblockE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc41impl_productionblock_EmptyproductionblockE
	.quad	_ZTIN2kc20impl_productionblockE
	.size	_ZTIN2kc41impl_productionblock_EmptyproductionblockE, 24

	.type	_ZTVN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE,@object # @_ZTVN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.globl	_ZTVN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.p2align	3
_ZTVN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE:
	.quad	0
	.quad	_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.quad	_ZNK2kc63impl_attribute_initialisation_option_Noattribute_initialisation8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisationD0Ev
	.quad	_ZN2kc63impl_attribute_initialisation_option_Noattribute_initialisation10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE, 104

	.type	_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE,@object # @_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.globl	_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.p2align	4
_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE:
	.asciz	"N2kc63impl_attribute_initialisation_option_Noattribute_initialisationE"
	.size	_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE, 71

	.type	_ZTSN2kc36impl_attribute_initialisation_optionE,@object # @_ZTSN2kc36impl_attribute_initialisation_optionE
	.section	.rodata._ZTSN2kc36impl_attribute_initialisation_optionE,"aG",@progbits,_ZTSN2kc36impl_attribute_initialisation_optionE,comdat
	.weak	_ZTSN2kc36impl_attribute_initialisation_optionE
	.p2align	4
_ZTSN2kc36impl_attribute_initialisation_optionE:
	.asciz	"N2kc36impl_attribute_initialisation_optionE"
	.size	_ZTSN2kc36impl_attribute_initialisation_optionE, 44

	.type	_ZTIN2kc36impl_attribute_initialisation_optionE,@object # @_ZTIN2kc36impl_attribute_initialisation_optionE
	.section	.rodata._ZTIN2kc36impl_attribute_initialisation_optionE,"aG",@progbits,_ZTIN2kc36impl_attribute_initialisation_optionE,comdat
	.weak	_ZTIN2kc36impl_attribute_initialisation_optionE
	.p2align	4
_ZTIN2kc36impl_attribute_initialisation_optionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc36impl_attribute_initialisation_optionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc36impl_attribute_initialisation_optionE, 24

	.type	_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE,@object # @_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.p2align	4
_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE
	.quad	_ZTIN2kc36impl_attribute_initialisation_optionE
	.size	_ZTIN2kc63impl_attribute_initialisation_option_Noattribute_initialisationE, 24

	.type	_ZTVN2kc35impl_Cexpression_elem_CExpressionNlE,@object # @_ZTVN2kc35impl_Cexpression_elem_CExpressionNlE
	.globl	_ZTVN2kc35impl_Cexpression_elem_CExpressionNlE
	.p2align	3
_ZTVN2kc35impl_Cexpression_elem_CExpressionNlE:
	.quad	0
	.quad	_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE
	.quad	_ZNK2kc35impl_Cexpression_elem_CExpressionNl8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc35impl_Cexpression_elem_CExpressionNl7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc35impl_Cexpression_elem_CExpressionNlD0Ev
	.quad	_ZN2kc35impl_Cexpression_elem_CExpressionNl10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc35impl_Cexpression_elem_CExpressionNlE, 104

	.type	_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE,@object # @_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE
	.globl	_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE
	.p2align	4
_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE:
	.asciz	"N2kc35impl_Cexpression_elem_CExpressionNlE"
	.size	_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE, 43

	.type	_ZTSN2kc21impl_Cexpression_elemE,@object # @_ZTSN2kc21impl_Cexpression_elemE
	.section	.rodata._ZTSN2kc21impl_Cexpression_elemE,"aG",@progbits,_ZTSN2kc21impl_Cexpression_elemE,comdat
	.weak	_ZTSN2kc21impl_Cexpression_elemE
	.p2align	4
_ZTSN2kc21impl_Cexpression_elemE:
	.asciz	"N2kc21impl_Cexpression_elemE"
	.size	_ZTSN2kc21impl_Cexpression_elemE, 29

	.type	_ZTIN2kc21impl_Cexpression_elemE,@object # @_ZTIN2kc21impl_Cexpression_elemE
	.section	.rodata._ZTIN2kc21impl_Cexpression_elemE,"aG",@progbits,_ZTIN2kc21impl_Cexpression_elemE,comdat
	.weak	_ZTIN2kc21impl_Cexpression_elemE
	.p2align	4
_ZTIN2kc21impl_Cexpression_elemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc21impl_Cexpression_elemE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc21impl_Cexpression_elemE, 24

	.type	_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE,@object # @_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE
	.p2align	4
_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc35impl_Cexpression_elem_CExpressionNlE
	.quad	_ZTIN2kc21impl_Cexpression_elemE
	.size	_ZTIN2kc35impl_Cexpression_elem_CExpressionNlE, 24

	.type	_ZTVN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE,@object # @_ZTVN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.globl	_ZTVN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.p2align	3
_ZTVN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE:
	.quad	0
	.quad	_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.quad	_ZNK2kc39impl_CexpressionDQ_elem_CExpressionDQNl8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNlD0Ev
	.quad	_ZN2kc39impl_CexpressionDQ_elem_CExpressionDQNl10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE, 104

	.type	_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE,@object # @_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.globl	_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.p2align	4
_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE:
	.asciz	"N2kc39impl_CexpressionDQ_elem_CExpressionDQNlE"
	.size	_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE, 47

	.type	_ZTSN2kc23impl_CexpressionDQ_elemE,@object # @_ZTSN2kc23impl_CexpressionDQ_elemE
	.section	.rodata._ZTSN2kc23impl_CexpressionDQ_elemE,"aG",@progbits,_ZTSN2kc23impl_CexpressionDQ_elemE,comdat
	.weak	_ZTSN2kc23impl_CexpressionDQ_elemE
	.p2align	4
_ZTSN2kc23impl_CexpressionDQ_elemE:
	.asciz	"N2kc23impl_CexpressionDQ_elemE"
	.size	_ZTSN2kc23impl_CexpressionDQ_elemE, 31

	.type	_ZTIN2kc23impl_CexpressionDQ_elemE,@object # @_ZTIN2kc23impl_CexpressionDQ_elemE
	.section	.rodata._ZTIN2kc23impl_CexpressionDQ_elemE,"aG",@progbits,_ZTIN2kc23impl_CexpressionDQ_elemE,comdat
	.weak	_ZTIN2kc23impl_CexpressionDQ_elemE
	.p2align	4
_ZTIN2kc23impl_CexpressionDQ_elemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc23impl_CexpressionDQ_elemE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc23impl_CexpressionDQ_elemE, 24

	.type	_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE,@object # @_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.p2align	4
_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE
	.quad	_ZTIN2kc23impl_CexpressionDQ_elemE
	.size	_ZTIN2kc39impl_CexpressionDQ_elem_CExpressionDQNlE, 24

	.type	_ZTVN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE,@object # @_ZTVN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.globl	_ZTVN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.p2align	3
_ZTVN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE:
	.quad	0
	.quad	_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.quad	_ZNK2kc39impl_CexpressionSQ_elem_CExpressionSQNl8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNlD0Ev
	.quad	_ZN2kc39impl_CexpressionSQ_elem_CExpressionSQNl10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE, 104

	.type	_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE,@object # @_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.globl	_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.p2align	4
_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE:
	.asciz	"N2kc39impl_CexpressionSQ_elem_CExpressionSQNlE"
	.size	_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE, 47

	.type	_ZTSN2kc23impl_CexpressionSQ_elemE,@object # @_ZTSN2kc23impl_CexpressionSQ_elemE
	.section	.rodata._ZTSN2kc23impl_CexpressionSQ_elemE,"aG",@progbits,_ZTSN2kc23impl_CexpressionSQ_elemE,comdat
	.weak	_ZTSN2kc23impl_CexpressionSQ_elemE
	.p2align	4
_ZTSN2kc23impl_CexpressionSQ_elemE:
	.asciz	"N2kc23impl_CexpressionSQ_elemE"
	.size	_ZTSN2kc23impl_CexpressionSQ_elemE, 31

	.type	_ZTIN2kc23impl_CexpressionSQ_elemE,@object # @_ZTIN2kc23impl_CexpressionSQ_elemE
	.section	.rodata._ZTIN2kc23impl_CexpressionSQ_elemE,"aG",@progbits,_ZTIN2kc23impl_CexpressionSQ_elemE,comdat
	.weak	_ZTIN2kc23impl_CexpressionSQ_elemE
	.p2align	4
_ZTIN2kc23impl_CexpressionSQ_elemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc23impl_CexpressionSQ_elemE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc23impl_CexpressionSQ_elemE, 24

	.type	_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE,@object # @_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.p2align	4
_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE
	.quad	_ZTIN2kc23impl_CexpressionSQ_elemE
	.size	_ZTIN2kc39impl_CexpressionSQ_elem_CExpressionSQNlE, 24

	.type	_ZTVN2kc22impl_pattern_PWildcardE,@object # @_ZTVN2kc22impl_pattern_PWildcardE
	.globl	_ZTVN2kc22impl_pattern_PWildcardE
	.p2align	3
_ZTVN2kc22impl_pattern_PWildcardE:
	.quad	0
	.quad	_ZTIN2kc22impl_pattern_PWildcardE
	.quad	_ZNK2kc22impl_pattern_PWildcard8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc22impl_pattern_PWildcard7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc22impl_pattern_PWildcardD0Ev
	.quad	_ZN2kc22impl_pattern_PWildcard10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc22impl_pattern_PWildcardE, 104

	.type	_ZTSN2kc22impl_pattern_PWildcardE,@object # @_ZTSN2kc22impl_pattern_PWildcardE
	.globl	_ZTSN2kc22impl_pattern_PWildcardE
	.p2align	4
_ZTSN2kc22impl_pattern_PWildcardE:
	.asciz	"N2kc22impl_pattern_PWildcardE"
	.size	_ZTSN2kc22impl_pattern_PWildcardE, 30

	.type	_ZTSN2kc12impl_patternE,@object # @_ZTSN2kc12impl_patternE
	.section	.rodata._ZTSN2kc12impl_patternE,"aG",@progbits,_ZTSN2kc12impl_patternE,comdat
	.weak	_ZTSN2kc12impl_patternE
	.p2align	4
_ZTSN2kc12impl_patternE:
	.asciz	"N2kc12impl_patternE"
	.size	_ZTSN2kc12impl_patternE, 20

	.type	_ZTIN2kc12impl_patternE,@object # @_ZTIN2kc12impl_patternE
	.section	.rodata._ZTIN2kc12impl_patternE,"aG",@progbits,_ZTIN2kc12impl_patternE,comdat
	.weak	_ZTIN2kc12impl_patternE
	.p2align	4
_ZTIN2kc12impl_patternE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc12impl_patternE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc12impl_patternE, 24

	.type	_ZTIN2kc22impl_pattern_PWildcardE,@object # @_ZTIN2kc22impl_pattern_PWildcardE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc22impl_pattern_PWildcardE
	.p2align	4
_ZTIN2kc22impl_pattern_PWildcardE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_pattern_PWildcardE
	.quad	_ZTIN2kc12impl_patternE
	.size	_ZTIN2kc22impl_pattern_PWildcardE, 24

	.type	_ZTVN2kc27impl_fnclass_ConvOperatorFnE,@object # @_ZTVN2kc27impl_fnclass_ConvOperatorFnE
	.globl	_ZTVN2kc27impl_fnclass_ConvOperatorFnE
	.p2align	3
_ZTVN2kc27impl_fnclass_ConvOperatorFnE:
	.quad	0
	.quad	_ZTIN2kc27impl_fnclass_ConvOperatorFnE
	.quad	_ZNK2kc27impl_fnclass_ConvOperatorFn8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc27impl_fnclass_ConvOperatorFn7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc27impl_fnclass_ConvOperatorFnD0Ev
	.quad	_ZN2kc27impl_fnclass_ConvOperatorFn10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc27impl_fnclass_ConvOperatorFnE, 104

	.type	_ZTSN2kc27impl_fnclass_ConvOperatorFnE,@object # @_ZTSN2kc27impl_fnclass_ConvOperatorFnE
	.globl	_ZTSN2kc27impl_fnclass_ConvOperatorFnE
	.p2align	4
_ZTSN2kc27impl_fnclass_ConvOperatorFnE:
	.asciz	"N2kc27impl_fnclass_ConvOperatorFnE"
	.size	_ZTSN2kc27impl_fnclass_ConvOperatorFnE, 35

	.type	_ZTSN2kc12impl_fnclassE,@object # @_ZTSN2kc12impl_fnclassE
	.section	.rodata._ZTSN2kc12impl_fnclassE,"aG",@progbits,_ZTSN2kc12impl_fnclassE,comdat
	.weak	_ZTSN2kc12impl_fnclassE
	.p2align	4
_ZTSN2kc12impl_fnclassE:
	.asciz	"N2kc12impl_fnclassE"
	.size	_ZTSN2kc12impl_fnclassE, 20

	.type	_ZTIN2kc12impl_fnclassE,@object # @_ZTIN2kc12impl_fnclassE
	.section	.rodata._ZTIN2kc12impl_fnclassE,"aG",@progbits,_ZTIN2kc12impl_fnclassE,comdat
	.weak	_ZTIN2kc12impl_fnclassE
	.p2align	4
_ZTIN2kc12impl_fnclassE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc12impl_fnclassE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc12impl_fnclassE, 24

	.type	_ZTIN2kc27impl_fnclass_ConvOperatorFnE,@object # @_ZTIN2kc27impl_fnclass_ConvOperatorFnE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc27impl_fnclass_ConvOperatorFnE
	.p2align	4
_ZTIN2kc27impl_fnclass_ConvOperatorFnE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc27impl_fnclass_ConvOperatorFnE
	.quad	_ZTIN2kc12impl_fnclassE
	.size	_ZTIN2kc27impl_fnclass_ConvOperatorFnE, 24

	.type	_ZTVN2kc25impl_fnclass_DestructorFnE,@object # @_ZTVN2kc25impl_fnclass_DestructorFnE
	.globl	_ZTVN2kc25impl_fnclass_DestructorFnE
	.p2align	3
_ZTVN2kc25impl_fnclass_DestructorFnE:
	.quad	0
	.quad	_ZTIN2kc25impl_fnclass_DestructorFnE
	.quad	_ZNK2kc25impl_fnclass_DestructorFn8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc25impl_fnclass_DestructorFn7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc25impl_fnclass_DestructorFnD0Ev
	.quad	_ZN2kc25impl_fnclass_DestructorFn10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc25impl_fnclass_DestructorFnE, 104

	.type	_ZTSN2kc25impl_fnclass_DestructorFnE,@object # @_ZTSN2kc25impl_fnclass_DestructorFnE
	.globl	_ZTSN2kc25impl_fnclass_DestructorFnE
	.p2align	4
_ZTSN2kc25impl_fnclass_DestructorFnE:
	.asciz	"N2kc25impl_fnclass_DestructorFnE"
	.size	_ZTSN2kc25impl_fnclass_DestructorFnE, 33

	.type	_ZTIN2kc25impl_fnclass_DestructorFnE,@object # @_ZTIN2kc25impl_fnclass_DestructorFnE
	.globl	_ZTIN2kc25impl_fnclass_DestructorFnE
	.p2align	4
_ZTIN2kc25impl_fnclass_DestructorFnE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc25impl_fnclass_DestructorFnE
	.quad	_ZTIN2kc12impl_fnclassE
	.size	_ZTIN2kc25impl_fnclass_DestructorFnE, 24

	.type	_ZTVN2kc26impl_fnclass_ConstructorFnE,@object # @_ZTVN2kc26impl_fnclass_ConstructorFnE
	.globl	_ZTVN2kc26impl_fnclass_ConstructorFnE
	.p2align	3
_ZTVN2kc26impl_fnclass_ConstructorFnE:
	.quad	0
	.quad	_ZTIN2kc26impl_fnclass_ConstructorFnE
	.quad	_ZNK2kc26impl_fnclass_ConstructorFn8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc26impl_fnclass_ConstructorFn7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc26impl_fnclass_ConstructorFnD0Ev
	.quad	_ZN2kc26impl_fnclass_ConstructorFn10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc26impl_fnclass_ConstructorFnE, 104

	.type	_ZTSN2kc26impl_fnclass_ConstructorFnE,@object # @_ZTSN2kc26impl_fnclass_ConstructorFnE
	.globl	_ZTSN2kc26impl_fnclass_ConstructorFnE
	.p2align	4
_ZTSN2kc26impl_fnclass_ConstructorFnE:
	.asciz	"N2kc26impl_fnclass_ConstructorFnE"
	.size	_ZTSN2kc26impl_fnclass_ConstructorFnE, 34

	.type	_ZTIN2kc26impl_fnclass_ConstructorFnE,@object # @_ZTIN2kc26impl_fnclass_ConstructorFnE
	.globl	_ZTIN2kc26impl_fnclass_ConstructorFnE
	.p2align	4
_ZTIN2kc26impl_fnclass_ConstructorFnE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc26impl_fnclass_ConstructorFnE
	.quad	_ZTIN2kc12impl_fnclassE
	.size	_ZTIN2kc26impl_fnclass_ConstructorFnE, 24

	.type	_ZTVN2kc21impl_fnclass_MemberFnE,@object # @_ZTVN2kc21impl_fnclass_MemberFnE
	.globl	_ZTVN2kc21impl_fnclass_MemberFnE
	.p2align	3
_ZTVN2kc21impl_fnclass_MemberFnE:
	.quad	0
	.quad	_ZTIN2kc21impl_fnclass_MemberFnE
	.quad	_ZNK2kc21impl_fnclass_MemberFn8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc21impl_fnclass_MemberFn7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc21impl_fnclass_MemberFnD0Ev
	.quad	_ZN2kc21impl_fnclass_MemberFn10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc21impl_fnclass_MemberFnE, 104

	.type	_ZTSN2kc21impl_fnclass_MemberFnE,@object # @_ZTSN2kc21impl_fnclass_MemberFnE
	.globl	_ZTSN2kc21impl_fnclass_MemberFnE
	.p2align	4
_ZTSN2kc21impl_fnclass_MemberFnE:
	.asciz	"N2kc21impl_fnclass_MemberFnE"
	.size	_ZTSN2kc21impl_fnclass_MemberFnE, 29

	.type	_ZTIN2kc21impl_fnclass_MemberFnE,@object # @_ZTIN2kc21impl_fnclass_MemberFnE
	.globl	_ZTIN2kc21impl_fnclass_MemberFnE
	.p2align	4
_ZTIN2kc21impl_fnclass_MemberFnE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc21impl_fnclass_MemberFnE
	.quad	_ZTIN2kc12impl_fnclassE
	.size	_ZTIN2kc21impl_fnclass_MemberFnE, 24

	.type	_ZTVN2kc21impl_fnclass_GlobalFnE,@object # @_ZTVN2kc21impl_fnclass_GlobalFnE
	.globl	_ZTVN2kc21impl_fnclass_GlobalFnE
	.p2align	3
_ZTVN2kc21impl_fnclass_GlobalFnE:
	.quad	0
	.quad	_ZTIN2kc21impl_fnclass_GlobalFnE
	.quad	_ZNK2kc21impl_fnclass_GlobalFn8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc21impl_fnclass_GlobalFn7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc21impl_fnclass_GlobalFnD0Ev
	.quad	_ZN2kc21impl_fnclass_GlobalFn10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc21impl_fnclass_GlobalFnE, 104

	.type	_ZTSN2kc21impl_fnclass_GlobalFnE,@object # @_ZTSN2kc21impl_fnclass_GlobalFnE
	.globl	_ZTSN2kc21impl_fnclass_GlobalFnE
	.p2align	4
_ZTSN2kc21impl_fnclass_GlobalFnE:
	.asciz	"N2kc21impl_fnclass_GlobalFnE"
	.size	_ZTSN2kc21impl_fnclass_GlobalFnE, 29

	.type	_ZTIN2kc21impl_fnclass_GlobalFnE,@object # @_ZTIN2kc21impl_fnclass_GlobalFnE
	.globl	_ZTIN2kc21impl_fnclass_GlobalFnE
	.p2align	4
_ZTIN2kc21impl_fnclass_GlobalFnE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc21impl_fnclass_GlobalFnE
	.quad	_ZTIN2kc12impl_fnclassE
	.size	_ZTIN2kc21impl_fnclass_GlobalFnE, 24

	.type	_ZTVN2kc33impl_foreach_after_NoForeachAfterE,@object # @_ZTVN2kc33impl_foreach_after_NoForeachAfterE
	.globl	_ZTVN2kc33impl_foreach_after_NoForeachAfterE
	.p2align	3
_ZTVN2kc33impl_foreach_after_NoForeachAfterE:
	.quad	0
	.quad	_ZTIN2kc33impl_foreach_after_NoForeachAfterE
	.quad	_ZNK2kc33impl_foreach_after_NoForeachAfter8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc33impl_foreach_after_NoForeachAfter7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc33impl_foreach_after_NoForeachAfterD0Ev
	.quad	_ZN2kc33impl_foreach_after_NoForeachAfter10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc33impl_foreach_after_NoForeachAfterE, 104

	.type	_ZTSN2kc33impl_foreach_after_NoForeachAfterE,@object # @_ZTSN2kc33impl_foreach_after_NoForeachAfterE
	.globl	_ZTSN2kc33impl_foreach_after_NoForeachAfterE
	.p2align	4
_ZTSN2kc33impl_foreach_after_NoForeachAfterE:
	.asciz	"N2kc33impl_foreach_after_NoForeachAfterE"
	.size	_ZTSN2kc33impl_foreach_after_NoForeachAfterE, 41

	.type	_ZTSN2kc18impl_foreach_afterE,@object # @_ZTSN2kc18impl_foreach_afterE
	.section	.rodata._ZTSN2kc18impl_foreach_afterE,"aG",@progbits,_ZTSN2kc18impl_foreach_afterE,comdat
	.weak	_ZTSN2kc18impl_foreach_afterE
	.p2align	4
_ZTSN2kc18impl_foreach_afterE:
	.asciz	"N2kc18impl_foreach_afterE"
	.size	_ZTSN2kc18impl_foreach_afterE, 26

	.type	_ZTIN2kc18impl_foreach_afterE,@object # @_ZTIN2kc18impl_foreach_afterE
	.section	.rodata._ZTIN2kc18impl_foreach_afterE,"aG",@progbits,_ZTIN2kc18impl_foreach_afterE,comdat
	.weak	_ZTIN2kc18impl_foreach_afterE
	.p2align	4
_ZTIN2kc18impl_foreach_afterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc18impl_foreach_afterE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc18impl_foreach_afterE, 24

	.type	_ZTIN2kc33impl_foreach_after_NoForeachAfterE,@object # @_ZTIN2kc33impl_foreach_after_NoForeachAfterE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc33impl_foreach_after_NoForeachAfterE
	.p2align	4
_ZTIN2kc33impl_foreach_after_NoForeachAfterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc33impl_foreach_after_NoForeachAfterE
	.quad	_ZTIN2kc18impl_foreach_afterE
	.size	_ZTIN2kc33impl_foreach_after_NoForeachAfterE, 24

	.type	_ZTVN2kc36impl_contextinfo_NotInForeachContextE,@object # @_ZTVN2kc36impl_contextinfo_NotInForeachContextE
	.globl	_ZTVN2kc36impl_contextinfo_NotInForeachContextE
	.p2align	3
_ZTVN2kc36impl_contextinfo_NotInForeachContextE:
	.quad	0
	.quad	_ZTIN2kc36impl_contextinfo_NotInForeachContextE
	.quad	_ZNK2kc36impl_contextinfo_NotInForeachContext8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc36impl_contextinfo_NotInForeachContext7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc36impl_contextinfo_NotInForeachContextD0Ev
	.quad	_ZN2kc36impl_contextinfo_NotInForeachContext10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc36impl_contextinfo_NotInForeachContextE, 104

	.type	_ZTSN2kc36impl_contextinfo_NotInForeachContextE,@object # @_ZTSN2kc36impl_contextinfo_NotInForeachContextE
	.globl	_ZTSN2kc36impl_contextinfo_NotInForeachContextE
	.p2align	4
_ZTSN2kc36impl_contextinfo_NotInForeachContextE:
	.asciz	"N2kc36impl_contextinfo_NotInForeachContextE"
	.size	_ZTSN2kc36impl_contextinfo_NotInForeachContextE, 44

	.type	_ZTSN2kc16impl_contextinfoE,@object # @_ZTSN2kc16impl_contextinfoE
	.section	.rodata._ZTSN2kc16impl_contextinfoE,"aG",@progbits,_ZTSN2kc16impl_contextinfoE,comdat
	.weak	_ZTSN2kc16impl_contextinfoE
	.p2align	4
_ZTSN2kc16impl_contextinfoE:
	.asciz	"N2kc16impl_contextinfoE"
	.size	_ZTSN2kc16impl_contextinfoE, 24

	.type	_ZTIN2kc16impl_contextinfoE,@object # @_ZTIN2kc16impl_contextinfoE
	.section	.rodata._ZTIN2kc16impl_contextinfoE,"aG",@progbits,_ZTIN2kc16impl_contextinfoE,comdat
	.weak	_ZTIN2kc16impl_contextinfoE
	.p2align	4
_ZTIN2kc16impl_contextinfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc16impl_contextinfoE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc16impl_contextinfoE, 24

	.type	_ZTIN2kc36impl_contextinfo_NotInForeachContextE,@object # @_ZTIN2kc36impl_contextinfo_NotInForeachContextE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc36impl_contextinfo_NotInForeachContextE
	.p2align	4
_ZTIN2kc36impl_contextinfo_NotInForeachContextE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc36impl_contextinfo_NotInForeachContextE
	.quad	_ZTIN2kc16impl_contextinfoE
	.size	_ZTIN2kc36impl_contextinfo_NotInForeachContextE, 24

	.type	_ZTVN2kc30impl_viewnameoption_NoViewnameE,@object # @_ZTVN2kc30impl_viewnameoption_NoViewnameE
	.globl	_ZTVN2kc30impl_viewnameoption_NoViewnameE
	.p2align	3
_ZTVN2kc30impl_viewnameoption_NoViewnameE:
	.quad	0
	.quad	_ZTIN2kc30impl_viewnameoption_NoViewnameE
	.quad	_ZNK2kc30impl_viewnameoption_NoViewname8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc30impl_viewnameoption_NoViewname7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc30impl_viewnameoption_NoViewnameD0Ev
	.quad	_ZN2kc30impl_viewnameoption_NoViewname10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc30impl_viewnameoption_NoViewnameE, 104

	.type	_ZTSN2kc30impl_viewnameoption_NoViewnameE,@object # @_ZTSN2kc30impl_viewnameoption_NoViewnameE
	.globl	_ZTSN2kc30impl_viewnameoption_NoViewnameE
	.p2align	4
_ZTSN2kc30impl_viewnameoption_NoViewnameE:
	.asciz	"N2kc30impl_viewnameoption_NoViewnameE"
	.size	_ZTSN2kc30impl_viewnameoption_NoViewnameE, 38

	.type	_ZTSN2kc19impl_viewnameoptionE,@object # @_ZTSN2kc19impl_viewnameoptionE
	.section	.rodata._ZTSN2kc19impl_viewnameoptionE,"aG",@progbits,_ZTSN2kc19impl_viewnameoptionE,comdat
	.weak	_ZTSN2kc19impl_viewnameoptionE
	.p2align	4
_ZTSN2kc19impl_viewnameoptionE:
	.asciz	"N2kc19impl_viewnameoptionE"
	.size	_ZTSN2kc19impl_viewnameoptionE, 27

	.type	_ZTIN2kc19impl_viewnameoptionE,@object # @_ZTIN2kc19impl_viewnameoptionE
	.section	.rodata._ZTIN2kc19impl_viewnameoptionE,"aG",@progbits,_ZTIN2kc19impl_viewnameoptionE,comdat
	.weak	_ZTIN2kc19impl_viewnameoptionE
	.p2align	4
_ZTIN2kc19impl_viewnameoptionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc19impl_viewnameoptionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc19impl_viewnameoptionE, 24

	.type	_ZTIN2kc30impl_viewnameoption_NoViewnameE,@object # @_ZTIN2kc30impl_viewnameoption_NoViewnameE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc30impl_viewnameoption_NoViewnameE
	.p2align	4
_ZTIN2kc30impl_viewnameoption_NoViewnameE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc30impl_viewnameoption_NoViewnameE
	.quad	_ZTIN2kc19impl_viewnameoptionE
	.size	_ZTIN2kc30impl_viewnameoption_NoViewnameE, 24

	.type	_ZTVN2kc34impl_languageoption_NoLanguagenameE,@object # @_ZTVN2kc34impl_languageoption_NoLanguagenameE
	.globl	_ZTVN2kc34impl_languageoption_NoLanguagenameE
	.p2align	3
_ZTVN2kc34impl_languageoption_NoLanguagenameE:
	.quad	0
	.quad	_ZTIN2kc34impl_languageoption_NoLanguagenameE
	.quad	_ZNK2kc34impl_languageoption_NoLanguagename8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc34impl_languageoption_NoLanguagename7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc34impl_languageoption_NoLanguagenameD0Ev
	.quad	_ZN2kc34impl_languageoption_NoLanguagename10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc34impl_languageoption_NoLanguagenameE, 104

	.type	_ZTSN2kc34impl_languageoption_NoLanguagenameE,@object # @_ZTSN2kc34impl_languageoption_NoLanguagenameE
	.globl	_ZTSN2kc34impl_languageoption_NoLanguagenameE
	.p2align	4
_ZTSN2kc34impl_languageoption_NoLanguagenameE:
	.asciz	"N2kc34impl_languageoption_NoLanguagenameE"
	.size	_ZTSN2kc34impl_languageoption_NoLanguagenameE, 42

	.type	_ZTSN2kc19impl_languageoptionE,@object # @_ZTSN2kc19impl_languageoptionE
	.section	.rodata._ZTSN2kc19impl_languageoptionE,"aG",@progbits,_ZTSN2kc19impl_languageoptionE,comdat
	.weak	_ZTSN2kc19impl_languageoptionE
	.p2align	4
_ZTSN2kc19impl_languageoptionE:
	.asciz	"N2kc19impl_languageoptionE"
	.size	_ZTSN2kc19impl_languageoptionE, 27

	.type	_ZTIN2kc19impl_languageoptionE,@object # @_ZTIN2kc19impl_languageoptionE
	.section	.rodata._ZTIN2kc19impl_languageoptionE,"aG",@progbits,_ZTIN2kc19impl_languageoptionE,comdat
	.weak	_ZTIN2kc19impl_languageoptionE
	.p2align	4
_ZTIN2kc19impl_languageoptionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc19impl_languageoptionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc19impl_languageoptionE, 24

	.type	_ZTIN2kc34impl_languageoption_NoLanguagenameE,@object # @_ZTIN2kc34impl_languageoption_NoLanguagenameE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc34impl_languageoption_NoLanguagenameE
	.p2align	4
_ZTIN2kc34impl_languageoption_NoLanguagenameE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc34impl_languageoption_NoLanguagenameE
	.quad	_ZTIN2kc19impl_languageoptionE
	.size	_ZTIN2kc34impl_languageoption_NoLanguagenameE, 24

	.type	_ZTVN2kc27impl_fileline_PosNoFileLineE,@object # @_ZTVN2kc27impl_fileline_PosNoFileLineE
	.globl	_ZTVN2kc27impl_fileline_PosNoFileLineE
	.p2align	3
_ZTVN2kc27impl_fileline_PosNoFileLineE:
	.quad	0
	.quad	_ZTIN2kc27impl_fileline_PosNoFileLineE
	.quad	_ZNK2kc27impl_fileline_PosNoFileLine8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc27impl_fileline_PosNoFileLine7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc27impl_fileline_PosNoFileLineD0Ev
	.quad	_ZN2kc27impl_fileline_PosNoFileLine10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc27impl_fileline_PosNoFileLineE, 104

	.type	_ZTSN2kc27impl_fileline_PosNoFileLineE,@object # @_ZTSN2kc27impl_fileline_PosNoFileLineE
	.globl	_ZTSN2kc27impl_fileline_PosNoFileLineE
	.p2align	4
_ZTSN2kc27impl_fileline_PosNoFileLineE:
	.asciz	"N2kc27impl_fileline_PosNoFileLineE"
	.size	_ZTSN2kc27impl_fileline_PosNoFileLineE, 35

	.type	_ZTSN2kc13impl_filelineE,@object # @_ZTSN2kc13impl_filelineE
	.section	.rodata._ZTSN2kc13impl_filelineE,"aG",@progbits,_ZTSN2kc13impl_filelineE,comdat
	.weak	_ZTSN2kc13impl_filelineE
	.p2align	4
_ZTSN2kc13impl_filelineE:
	.asciz	"N2kc13impl_filelineE"
	.size	_ZTSN2kc13impl_filelineE, 21

	.type	_ZTIN2kc13impl_filelineE,@object # @_ZTIN2kc13impl_filelineE
	.section	.rodata._ZTIN2kc13impl_filelineE,"aG",@progbits,_ZTIN2kc13impl_filelineE,comdat
	.weak	_ZTIN2kc13impl_filelineE
	.p2align	4
_ZTIN2kc13impl_filelineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc13impl_filelineE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc13impl_filelineE, 24

	.type	_ZTIN2kc27impl_fileline_PosNoFileLineE,@object # @_ZTIN2kc27impl_fileline_PosNoFileLineE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc27impl_fileline_PosNoFileLineE
	.p2align	4
_ZTIN2kc27impl_fileline_PosNoFileLineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc27impl_fileline_PosNoFileLineE
	.quad	_ZTIN2kc13impl_filelineE
	.size	_ZTIN2kc27impl_fileline_PosNoFileLineE, 24

	.type	_ZTVN2kc24impl_fileline_NoFileLineE,@object # @_ZTVN2kc24impl_fileline_NoFileLineE
	.globl	_ZTVN2kc24impl_fileline_NoFileLineE
	.p2align	3
_ZTVN2kc24impl_fileline_NoFileLineE:
	.quad	0
	.quad	_ZTIN2kc24impl_fileline_NoFileLineE
	.quad	_ZNK2kc24impl_fileline_NoFileLine8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc24impl_fileline_NoFileLine7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc24impl_fileline_NoFileLineD0Ev
	.quad	_ZN2kc24impl_fileline_NoFileLine10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc24impl_fileline_NoFileLineE, 104

	.type	_ZTSN2kc24impl_fileline_NoFileLineE,@object # @_ZTSN2kc24impl_fileline_NoFileLineE
	.globl	_ZTSN2kc24impl_fileline_NoFileLineE
	.p2align	4
_ZTSN2kc24impl_fileline_NoFileLineE:
	.asciz	"N2kc24impl_fileline_NoFileLineE"
	.size	_ZTSN2kc24impl_fileline_NoFileLineE, 32

	.type	_ZTIN2kc24impl_fileline_NoFileLineE,@object # @_ZTIN2kc24impl_fileline_NoFileLineE
	.globl	_ZTIN2kc24impl_fileline_NoFileLineE
	.p2align	4
_ZTIN2kc24impl_fileline_NoFileLineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc24impl_fileline_NoFileLineE
	.quad	_ZTIN2kc13impl_filelineE
	.size	_ZTIN2kc24impl_fileline_NoFileLineE, 24

	.type	_ZTVN2kc22impl_fileline_FileLineE,@object # @_ZTVN2kc22impl_fileline_FileLineE
	.globl	_ZTVN2kc22impl_fileline_FileLineE
	.p2align	3
_ZTVN2kc22impl_fileline_FileLineE:
	.quad	0
	.quad	_ZTIN2kc22impl_fileline_FileLineE
	.quad	_ZNK2kc22impl_fileline_FileLine8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc22impl_fileline_FileLine7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc22impl_fileline_FileLineD0Ev
	.quad	_ZN2kc22impl_fileline_FileLine10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc22impl_fileline_FileLineE, 104

	.type	_ZTSN2kc22impl_fileline_FileLineE,@object # @_ZTSN2kc22impl_fileline_FileLineE
	.globl	_ZTSN2kc22impl_fileline_FileLineE
	.p2align	4
_ZTSN2kc22impl_fileline_FileLineE:
	.asciz	"N2kc22impl_fileline_FileLineE"
	.size	_ZTSN2kc22impl_fileline_FileLineE, 30

	.type	_ZTIN2kc22impl_fileline_FileLineE,@object # @_ZTIN2kc22impl_fileline_FileLineE
	.globl	_ZTIN2kc22impl_fileline_FileLineE
	.p2align	4
_ZTIN2kc22impl_fileline_FileLineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_fileline_FileLineE
	.quad	_ZTIN2kc13impl_filelineE
	.size	_ZTIN2kc22impl_fileline_FileLineE, 24

	.type	_ZTVN2kc23impl_IDtype_ITUserRViewE,@object # @_ZTVN2kc23impl_IDtype_ITUserRViewE
	.globl	_ZTVN2kc23impl_IDtype_ITUserRViewE
	.p2align	3
_ZTVN2kc23impl_IDtype_ITUserRViewE:
	.quad	0
	.quad	_ZTIN2kc23impl_IDtype_ITUserRViewE
	.quad	_ZNK2kc23impl_IDtype_ITUserRView8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc23impl_IDtype_ITUserRView7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc23impl_IDtype_ITUserRViewD0Ev
	.quad	_ZN2kc23impl_IDtype_ITUserRView10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc23impl_IDtype_ITUserRViewE, 104

	.type	_ZTSN2kc23impl_IDtype_ITUserRViewE,@object # @_ZTSN2kc23impl_IDtype_ITUserRViewE
	.globl	_ZTSN2kc23impl_IDtype_ITUserRViewE
	.p2align	4
_ZTSN2kc23impl_IDtype_ITUserRViewE:
	.asciz	"N2kc23impl_IDtype_ITUserRViewE"
	.size	_ZTSN2kc23impl_IDtype_ITUserRViewE, 31

	.type	_ZTSN2kc11impl_IDtypeE,@object # @_ZTSN2kc11impl_IDtypeE
	.section	.rodata._ZTSN2kc11impl_IDtypeE,"aG",@progbits,_ZTSN2kc11impl_IDtypeE,comdat
	.weak	_ZTSN2kc11impl_IDtypeE
	.p2align	4
_ZTSN2kc11impl_IDtypeE:
	.asciz	"N2kc11impl_IDtypeE"
	.size	_ZTSN2kc11impl_IDtypeE, 19

	.type	_ZTIN2kc11impl_IDtypeE,@object # @_ZTIN2kc11impl_IDtypeE
	.section	.rodata._ZTIN2kc11impl_IDtypeE,"aG",@progbits,_ZTIN2kc11impl_IDtypeE,comdat
	.weak	_ZTIN2kc11impl_IDtypeE
	.p2align	4
_ZTIN2kc11impl_IDtypeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc11impl_IDtypeE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc11impl_IDtypeE, 24

	.type	_ZTIN2kc23impl_IDtype_ITUserRViewE,@object # @_ZTIN2kc23impl_IDtype_ITUserRViewE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc23impl_IDtype_ITUserRViewE
	.p2align	4
_ZTIN2kc23impl_IDtype_ITUserRViewE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc23impl_IDtype_ITUserRViewE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc23impl_IDtype_ITUserRViewE, 24

	.type	_ZTVN2kc29impl_IDtype_ITPredefinedRViewE,@object # @_ZTVN2kc29impl_IDtype_ITPredefinedRViewE
	.globl	_ZTVN2kc29impl_IDtype_ITPredefinedRViewE
	.p2align	3
_ZTVN2kc29impl_IDtype_ITPredefinedRViewE:
	.quad	0
	.quad	_ZTIN2kc29impl_IDtype_ITPredefinedRViewE
	.quad	_ZNK2kc29impl_IDtype_ITPredefinedRView8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc29impl_IDtype_ITPredefinedRView7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc29impl_IDtype_ITPredefinedRViewD0Ev
	.quad	_ZN2kc29impl_IDtype_ITPredefinedRView10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc29impl_IDtype_ITPredefinedRViewE, 104

	.type	_ZTSN2kc29impl_IDtype_ITPredefinedRViewE,@object # @_ZTSN2kc29impl_IDtype_ITPredefinedRViewE
	.globl	_ZTSN2kc29impl_IDtype_ITPredefinedRViewE
	.p2align	4
_ZTSN2kc29impl_IDtype_ITPredefinedRViewE:
	.asciz	"N2kc29impl_IDtype_ITPredefinedRViewE"
	.size	_ZTSN2kc29impl_IDtype_ITPredefinedRViewE, 37

	.type	_ZTIN2kc29impl_IDtype_ITPredefinedRViewE,@object # @_ZTIN2kc29impl_IDtype_ITPredefinedRViewE
	.globl	_ZTIN2kc29impl_IDtype_ITPredefinedRViewE
	.p2align	4
_ZTIN2kc29impl_IDtype_ITPredefinedRViewE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc29impl_IDtype_ITPredefinedRViewE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc29impl_IDtype_ITPredefinedRViewE, 24

	.type	_ZTVN2kc22impl_IDtype_ITUViewVarE,@object # @_ZTVN2kc22impl_IDtype_ITUViewVarE
	.globl	_ZTVN2kc22impl_IDtype_ITUViewVarE
	.p2align	3
_ZTVN2kc22impl_IDtype_ITUViewVarE:
	.quad	0
	.quad	_ZTIN2kc22impl_IDtype_ITUViewVarE
	.quad	_ZNK2kc22impl_IDtype_ITUViewVar8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc22impl_IDtype_ITUViewVar7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc22impl_IDtype_ITUViewVarD0Ev
	.quad	_ZN2kc22impl_IDtype_ITUViewVar10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc22impl_IDtype_ITUViewVarE, 104

	.type	_ZTSN2kc22impl_IDtype_ITUViewVarE,@object # @_ZTSN2kc22impl_IDtype_ITUViewVarE
	.globl	_ZTSN2kc22impl_IDtype_ITUViewVarE
	.p2align	4
_ZTSN2kc22impl_IDtype_ITUViewVarE:
	.asciz	"N2kc22impl_IDtype_ITUViewVarE"
	.size	_ZTSN2kc22impl_IDtype_ITUViewVarE, 30

	.type	_ZTIN2kc22impl_IDtype_ITUViewVarE,@object # @_ZTIN2kc22impl_IDtype_ITUViewVarE
	.globl	_ZTIN2kc22impl_IDtype_ITUViewVarE
	.p2align	4
_ZTIN2kc22impl_IDtype_ITUViewVarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_IDtype_ITUViewVarE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc22impl_IDtype_ITUViewVarE, 24

	.type	_ZTVN2kc23impl_IDtype_ITUserUViewE,@object # @_ZTVN2kc23impl_IDtype_ITUserUViewE
	.globl	_ZTVN2kc23impl_IDtype_ITUserUViewE
	.p2align	3
_ZTVN2kc23impl_IDtype_ITUserUViewE:
	.quad	0
	.quad	_ZTIN2kc23impl_IDtype_ITUserUViewE
	.quad	_ZNK2kc23impl_IDtype_ITUserUView8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc23impl_IDtype_ITUserUView7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc23impl_IDtype_ITUserUViewD0Ev
	.quad	_ZN2kc23impl_IDtype_ITUserUView10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc23impl_IDtype_ITUserUViewE, 104

	.type	_ZTSN2kc23impl_IDtype_ITUserUViewE,@object # @_ZTSN2kc23impl_IDtype_ITUserUViewE
	.globl	_ZTSN2kc23impl_IDtype_ITUserUViewE
	.p2align	4
_ZTSN2kc23impl_IDtype_ITUserUViewE:
	.asciz	"N2kc23impl_IDtype_ITUserUViewE"
	.size	_ZTSN2kc23impl_IDtype_ITUserUViewE, 31

	.type	_ZTIN2kc23impl_IDtype_ITUserUViewE,@object # @_ZTIN2kc23impl_IDtype_ITUserUViewE
	.globl	_ZTIN2kc23impl_IDtype_ITUserUViewE
	.p2align	4
_ZTIN2kc23impl_IDtype_ITUserUViewE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc23impl_IDtype_ITUserUViewE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc23impl_IDtype_ITUserUViewE, 24

	.type	_ZTVN2kc29impl_IDtype_ITPredefinedUViewE,@object # @_ZTVN2kc29impl_IDtype_ITPredefinedUViewE
	.globl	_ZTVN2kc29impl_IDtype_ITPredefinedUViewE
	.p2align	3
_ZTVN2kc29impl_IDtype_ITPredefinedUViewE:
	.quad	0
	.quad	_ZTIN2kc29impl_IDtype_ITPredefinedUViewE
	.quad	_ZNK2kc29impl_IDtype_ITPredefinedUView8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc29impl_IDtype_ITPredefinedUView7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc29impl_IDtype_ITPredefinedUViewD0Ev
	.quad	_ZN2kc29impl_IDtype_ITPredefinedUView10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc29impl_IDtype_ITPredefinedUViewE, 104

	.type	_ZTSN2kc29impl_IDtype_ITPredefinedUViewE,@object # @_ZTSN2kc29impl_IDtype_ITPredefinedUViewE
	.globl	_ZTSN2kc29impl_IDtype_ITPredefinedUViewE
	.p2align	4
_ZTSN2kc29impl_IDtype_ITPredefinedUViewE:
	.asciz	"N2kc29impl_IDtype_ITPredefinedUViewE"
	.size	_ZTSN2kc29impl_IDtype_ITPredefinedUViewE, 37

	.type	_ZTIN2kc29impl_IDtype_ITPredefinedUViewE,@object # @_ZTIN2kc29impl_IDtype_ITPredefinedUViewE
	.globl	_ZTIN2kc29impl_IDtype_ITPredefinedUViewE
	.p2align	4
_ZTIN2kc29impl_IDtype_ITPredefinedUViewE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc29impl_IDtype_ITPredefinedUViewE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc29impl_IDtype_ITPredefinedUViewE, 24

	.type	_ZTVN2kc26impl_IDtype_ITStorageClassE,@object # @_ZTVN2kc26impl_IDtype_ITStorageClassE
	.globl	_ZTVN2kc26impl_IDtype_ITStorageClassE
	.p2align	3
_ZTVN2kc26impl_IDtype_ITStorageClassE:
	.quad	0
	.quad	_ZTIN2kc26impl_IDtype_ITStorageClassE
	.quad	_ZNK2kc26impl_IDtype_ITStorageClass8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc26impl_IDtype_ITStorageClass7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc26impl_IDtype_ITStorageClassD0Ev
	.quad	_ZN2kc26impl_IDtype_ITStorageClass10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc26impl_IDtype_ITStorageClassE, 104

	.type	_ZTSN2kc26impl_IDtype_ITStorageClassE,@object # @_ZTSN2kc26impl_IDtype_ITStorageClassE
	.globl	_ZTSN2kc26impl_IDtype_ITStorageClassE
	.p2align	4
_ZTSN2kc26impl_IDtype_ITStorageClassE:
	.asciz	"N2kc26impl_IDtype_ITStorageClassE"
	.size	_ZTSN2kc26impl_IDtype_ITStorageClassE, 34

	.type	_ZTIN2kc26impl_IDtype_ITStorageClassE,@object # @_ZTIN2kc26impl_IDtype_ITStorageClassE
	.globl	_ZTIN2kc26impl_IDtype_ITStorageClassE
	.p2align	4
_ZTIN2kc26impl_IDtype_ITStorageClassE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc26impl_IDtype_ITStorageClassE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc26impl_IDtype_ITStorageClassE, 24

	.type	_ZTVN2kc36impl_IDtype_ITPredefinedStorageClassE,@object # @_ZTVN2kc36impl_IDtype_ITPredefinedStorageClassE
	.globl	_ZTVN2kc36impl_IDtype_ITPredefinedStorageClassE
	.p2align	3
_ZTVN2kc36impl_IDtype_ITPredefinedStorageClassE:
	.quad	0
	.quad	_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE
	.quad	_ZNK2kc36impl_IDtype_ITPredefinedStorageClass8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc36impl_IDtype_ITPredefinedStorageClass7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc36impl_IDtype_ITPredefinedStorageClassD0Ev
	.quad	_ZN2kc36impl_IDtype_ITPredefinedStorageClass10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc36impl_IDtype_ITPredefinedStorageClassE, 104

	.type	_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE,@object # @_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE
	.globl	_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE
	.p2align	4
_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE:
	.asciz	"N2kc36impl_IDtype_ITPredefinedStorageClassE"
	.size	_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE, 44

	.type	_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE,@object # @_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE
	.globl	_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE
	.p2align	4
_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc36impl_IDtype_ITPredefinedStorageClassE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc36impl_IDtype_ITPredefinedStorageClassE, 24

	.type	_ZTVN2kc21impl_IDtype_ITUnknownE,@object # @_ZTVN2kc21impl_IDtype_ITUnknownE
	.globl	_ZTVN2kc21impl_IDtype_ITUnknownE
	.p2align	3
_ZTVN2kc21impl_IDtype_ITUnknownE:
	.quad	0
	.quad	_ZTIN2kc21impl_IDtype_ITUnknownE
	.quad	_ZNK2kc21impl_IDtype_ITUnknown8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc21impl_IDtype_ITUnknown7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc21impl_IDtype_ITUnknownD0Ev
	.quad	_ZN2kc21impl_IDtype_ITUnknown10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc21impl_IDtype_ITUnknownE, 104

	.type	_ZTSN2kc21impl_IDtype_ITUnknownE,@object # @_ZTSN2kc21impl_IDtype_ITUnknownE
	.globl	_ZTSN2kc21impl_IDtype_ITUnknownE
	.p2align	4
_ZTSN2kc21impl_IDtype_ITUnknownE:
	.asciz	"N2kc21impl_IDtype_ITUnknownE"
	.size	_ZTSN2kc21impl_IDtype_ITUnknownE, 29

	.type	_ZTIN2kc21impl_IDtype_ITUnknownE,@object # @_ZTIN2kc21impl_IDtype_ITUnknownE
	.globl	_ZTIN2kc21impl_IDtype_ITUnknownE
	.p2align	4
_ZTIN2kc21impl_IDtype_ITUnknownE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc21impl_IDtype_ITUnknownE
	.quad	_ZTIN2kc11impl_IDtypeE
	.size	_ZTIN2kc21impl_IDtype_ITUnknownE, 24

	.type	_ZTVN2kc33impl_dollarvarstatus_DVDisallowedE,@object # @_ZTVN2kc33impl_dollarvarstatus_DVDisallowedE
	.globl	_ZTVN2kc33impl_dollarvarstatus_DVDisallowedE
	.p2align	3
_ZTVN2kc33impl_dollarvarstatus_DVDisallowedE:
	.quad	0
	.quad	_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE
	.quad	_ZNK2kc33impl_dollarvarstatus_DVDisallowed8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc33impl_dollarvarstatus_DVDisallowed7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc33impl_dollarvarstatus_DVDisallowedD0Ev
	.quad	_ZN2kc33impl_dollarvarstatus_DVDisallowed10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc33impl_dollarvarstatus_DVDisallowedE, 104

	.type	_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE,@object # @_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE
	.globl	_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE
	.p2align	4
_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE:
	.asciz	"N2kc33impl_dollarvarstatus_DVDisallowedE"
	.size	_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE, 41

	.type	_ZTSN2kc20impl_dollarvarstatusE,@object # @_ZTSN2kc20impl_dollarvarstatusE
	.section	.rodata._ZTSN2kc20impl_dollarvarstatusE,"aG",@progbits,_ZTSN2kc20impl_dollarvarstatusE,comdat
	.weak	_ZTSN2kc20impl_dollarvarstatusE
	.p2align	4
_ZTSN2kc20impl_dollarvarstatusE:
	.asciz	"N2kc20impl_dollarvarstatusE"
	.size	_ZTSN2kc20impl_dollarvarstatusE, 28

	.type	_ZTIN2kc20impl_dollarvarstatusE,@object # @_ZTIN2kc20impl_dollarvarstatusE
	.section	.rodata._ZTIN2kc20impl_dollarvarstatusE,"aG",@progbits,_ZTIN2kc20impl_dollarvarstatusE,comdat
	.weak	_ZTIN2kc20impl_dollarvarstatusE
	.p2align	4
_ZTIN2kc20impl_dollarvarstatusE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc20impl_dollarvarstatusE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc20impl_dollarvarstatusE, 24

	.type	_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE,@object # @_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE
	.p2align	4
_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc33impl_dollarvarstatus_DVDisallowedE
	.quad	_ZTIN2kc20impl_dollarvarstatusE
	.size	_ZTIN2kc33impl_dollarvarstatus_DVDisallowedE, 24

	.type	_ZTVN2kc30impl_dollarvarstatus_DVAllowedE,@object # @_ZTVN2kc30impl_dollarvarstatus_DVAllowedE
	.globl	_ZTVN2kc30impl_dollarvarstatus_DVAllowedE
	.p2align	3
_ZTVN2kc30impl_dollarvarstatus_DVAllowedE:
	.quad	0
	.quad	_ZTIN2kc30impl_dollarvarstatus_DVAllowedE
	.quad	_ZNK2kc30impl_dollarvarstatus_DVAllowed8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc30impl_dollarvarstatus_DVAllowed7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc30impl_dollarvarstatus_DVAllowedD0Ev
	.quad	_ZN2kc30impl_dollarvarstatus_DVAllowed10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc30impl_dollarvarstatus_DVAllowedE, 104

	.type	_ZTSN2kc30impl_dollarvarstatus_DVAllowedE,@object # @_ZTSN2kc30impl_dollarvarstatus_DVAllowedE
	.globl	_ZTSN2kc30impl_dollarvarstatus_DVAllowedE
	.p2align	4
_ZTSN2kc30impl_dollarvarstatus_DVAllowedE:
	.asciz	"N2kc30impl_dollarvarstatus_DVAllowedE"
	.size	_ZTSN2kc30impl_dollarvarstatus_DVAllowedE, 38

	.type	_ZTIN2kc30impl_dollarvarstatus_DVAllowedE,@object # @_ZTIN2kc30impl_dollarvarstatus_DVAllowedE
	.globl	_ZTIN2kc30impl_dollarvarstatus_DVAllowedE
	.p2align	4
_ZTIN2kc30impl_dollarvarstatus_DVAllowedE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc30impl_dollarvarstatus_DVAllowedE
	.quad	_ZTIN2kc20impl_dollarvarstatusE
	.size	_ZTIN2kc30impl_dollarvarstatus_DVAllowedE, 24

	.type	_ZTVN2kc19impl_tribool_BiggerE,@object # @_ZTVN2kc19impl_tribool_BiggerE
	.globl	_ZTVN2kc19impl_tribool_BiggerE
	.p2align	3
_ZTVN2kc19impl_tribool_BiggerE:
	.quad	0
	.quad	_ZTIN2kc19impl_tribool_BiggerE
	.quad	_ZNK2kc19impl_tribool_Bigger8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc19impl_tribool_Bigger7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc19impl_tribool_BiggerD0Ev
	.quad	_ZN2kc19impl_tribool_Bigger10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc19impl_tribool_BiggerE, 104

	.type	_ZTSN2kc19impl_tribool_BiggerE,@object # @_ZTSN2kc19impl_tribool_BiggerE
	.globl	_ZTSN2kc19impl_tribool_BiggerE
	.p2align	4
_ZTSN2kc19impl_tribool_BiggerE:
	.asciz	"N2kc19impl_tribool_BiggerE"
	.size	_ZTSN2kc19impl_tribool_BiggerE, 27

	.type	_ZTSN2kc12impl_triboolE,@object # @_ZTSN2kc12impl_triboolE
	.section	.rodata._ZTSN2kc12impl_triboolE,"aG",@progbits,_ZTSN2kc12impl_triboolE,comdat
	.weak	_ZTSN2kc12impl_triboolE
	.p2align	4
_ZTSN2kc12impl_triboolE:
	.asciz	"N2kc12impl_triboolE"
	.size	_ZTSN2kc12impl_triboolE, 20

	.type	_ZTIN2kc12impl_triboolE,@object # @_ZTIN2kc12impl_triboolE
	.section	.rodata._ZTIN2kc12impl_triboolE,"aG",@progbits,_ZTIN2kc12impl_triboolE,comdat
	.weak	_ZTIN2kc12impl_triboolE
	.p2align	4
_ZTIN2kc12impl_triboolE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc12impl_triboolE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc12impl_triboolE, 24

	.type	_ZTIN2kc19impl_tribool_BiggerE,@object # @_ZTIN2kc19impl_tribool_BiggerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc19impl_tribool_BiggerE
	.p2align	4
_ZTIN2kc19impl_tribool_BiggerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc19impl_tribool_BiggerE
	.quad	_ZTIN2kc12impl_triboolE
	.size	_ZTIN2kc19impl_tribool_BiggerE, 24

	.type	_ZTVN2kc20impl_tribool_SmallerE,@object # @_ZTVN2kc20impl_tribool_SmallerE
	.globl	_ZTVN2kc20impl_tribool_SmallerE
	.p2align	3
_ZTVN2kc20impl_tribool_SmallerE:
	.quad	0
	.quad	_ZTIN2kc20impl_tribool_SmallerE
	.quad	_ZNK2kc20impl_tribool_Smaller8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc20impl_tribool_Smaller7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc20impl_tribool_SmallerD0Ev
	.quad	_ZN2kc20impl_tribool_Smaller10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc20impl_tribool_SmallerE, 104

	.type	_ZTSN2kc20impl_tribool_SmallerE,@object # @_ZTSN2kc20impl_tribool_SmallerE
	.globl	_ZTSN2kc20impl_tribool_SmallerE
	.p2align	4
_ZTSN2kc20impl_tribool_SmallerE:
	.asciz	"N2kc20impl_tribool_SmallerE"
	.size	_ZTSN2kc20impl_tribool_SmallerE, 28

	.type	_ZTIN2kc20impl_tribool_SmallerE,@object # @_ZTIN2kc20impl_tribool_SmallerE
	.globl	_ZTIN2kc20impl_tribool_SmallerE
	.p2align	4
_ZTIN2kc20impl_tribool_SmallerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc20impl_tribool_SmallerE
	.quad	_ZTIN2kc12impl_triboolE
	.size	_ZTIN2kc20impl_tribool_SmallerE, 24

	.type	_ZTVN2kc18impl_tribool_EqualE,@object # @_ZTVN2kc18impl_tribool_EqualE
	.globl	_ZTVN2kc18impl_tribool_EqualE
	.p2align	3
_ZTVN2kc18impl_tribool_EqualE:
	.quad	0
	.quad	_ZTIN2kc18impl_tribool_EqualE
	.quad	_ZNK2kc18impl_tribool_Equal8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc18impl_tribool_Equal7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc18impl_tribool_EqualD0Ev
	.quad	_ZN2kc18impl_tribool_Equal10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc18impl_tribool_EqualE, 104

	.type	_ZTSN2kc18impl_tribool_EqualE,@object # @_ZTSN2kc18impl_tribool_EqualE
	.globl	_ZTSN2kc18impl_tribool_EqualE
	.p2align	4
_ZTSN2kc18impl_tribool_EqualE:
	.asciz	"N2kc18impl_tribool_EqualE"
	.size	_ZTSN2kc18impl_tribool_EqualE, 26

	.type	_ZTIN2kc18impl_tribool_EqualE,@object # @_ZTIN2kc18impl_tribool_EqualE
	.globl	_ZTIN2kc18impl_tribool_EqualE
	.p2align	4
_ZTIN2kc18impl_tribool_EqualE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc18impl_tribool_EqualE
	.quad	_ZTIN2kc12impl_triboolE
	.size	_ZTIN2kc18impl_tribool_EqualE, 24

	.type	_ZTVN2kc41impl_elem_patternrepresentation_PRDefaultE,@object # @_ZTVN2kc41impl_elem_patternrepresentation_PRDefaultE
	.globl	_ZTVN2kc41impl_elem_patternrepresentation_PRDefaultE
	.p2align	3
_ZTVN2kc41impl_elem_patternrepresentation_PRDefaultE:
	.quad	0
	.quad	_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE
	.quad	_ZNK2kc41impl_elem_patternrepresentation_PRDefault8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc41impl_elem_patternrepresentation_PRDefault7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc41impl_elem_patternrepresentation_PRDefaultD0Ev
	.quad	_ZN2kc41impl_elem_patternrepresentation_PRDefault10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc41impl_elem_patternrepresentation_PRDefaultE, 104

	.type	_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE,@object # @_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE
	.globl	_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE
	.p2align	4
_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE:
	.asciz	"N2kc41impl_elem_patternrepresentation_PRDefaultE"
	.size	_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE, 49

	.type	_ZTSN2kc31impl_elem_patternrepresentationE,@object # @_ZTSN2kc31impl_elem_patternrepresentationE
	.section	.rodata._ZTSN2kc31impl_elem_patternrepresentationE,"aG",@progbits,_ZTSN2kc31impl_elem_patternrepresentationE,comdat
	.weak	_ZTSN2kc31impl_elem_patternrepresentationE
	.p2align	4
_ZTSN2kc31impl_elem_patternrepresentationE:
	.asciz	"N2kc31impl_elem_patternrepresentationE"
	.size	_ZTSN2kc31impl_elem_patternrepresentationE, 39

	.type	_ZTIN2kc31impl_elem_patternrepresentationE,@object # @_ZTIN2kc31impl_elem_patternrepresentationE
	.section	.rodata._ZTIN2kc31impl_elem_patternrepresentationE,"aG",@progbits,_ZTIN2kc31impl_elem_patternrepresentationE,comdat
	.weak	_ZTIN2kc31impl_elem_patternrepresentationE
	.p2align	4
_ZTIN2kc31impl_elem_patternrepresentationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc31impl_elem_patternrepresentationE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc31impl_elem_patternrepresentationE, 24

	.type	_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE,@object # @_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE
	.p2align	4
_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc41impl_elem_patternrepresentation_PRDefaultE
	.quad	_ZTIN2kc31impl_elem_patternrepresentationE
	.size	_ZTIN2kc41impl_elem_patternrepresentation_PRDefaultE, 24

	.type	_ZTVN2kc41impl_ac_storage_class_specifier_AcVirtualE,@object # @_ZTVN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.globl	_ZTVN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.p2align	3
_ZTVN2kc41impl_ac_storage_class_specifier_AcVirtualE:
	.quad	0
	.quad	_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.quad	_ZNK2kc41impl_ac_storage_class_specifier_AcVirtual8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcVirtual7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcVirtualD0Ev
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcVirtual10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc41impl_ac_storage_class_specifier_AcVirtualE, 104

	.type	_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE,@object # @_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.globl	_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.p2align	4
_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE:
	.asciz	"N2kc41impl_ac_storage_class_specifier_AcVirtualE"
	.size	_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE, 49

	.type	_ZTSN2kc31impl_ac_storage_class_specifierE,@object # @_ZTSN2kc31impl_ac_storage_class_specifierE
	.section	.rodata._ZTSN2kc31impl_ac_storage_class_specifierE,"aG",@progbits,_ZTSN2kc31impl_ac_storage_class_specifierE,comdat
	.weak	_ZTSN2kc31impl_ac_storage_class_specifierE
	.p2align	4
_ZTSN2kc31impl_ac_storage_class_specifierE:
	.asciz	"N2kc31impl_ac_storage_class_specifierE"
	.size	_ZTSN2kc31impl_ac_storage_class_specifierE, 39

	.type	_ZTIN2kc31impl_ac_storage_class_specifierE,@object # @_ZTIN2kc31impl_ac_storage_class_specifierE
	.section	.rodata._ZTIN2kc31impl_ac_storage_class_specifierE,"aG",@progbits,_ZTIN2kc31impl_ac_storage_class_specifierE,comdat
	.weak	_ZTIN2kc31impl_ac_storage_class_specifierE
	.p2align	4
_ZTIN2kc31impl_ac_storage_class_specifierE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc31impl_ac_storage_class_specifierE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc31impl_ac_storage_class_specifierE, 24

	.type	_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE,@object # @_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.p2align	4
_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc41impl_ac_storage_class_specifier_AcVirtualE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc41impl_ac_storage_class_specifier_AcVirtualE, 24

	.type	_ZTVN2kc41impl_ac_storage_class_specifier_AcTypedefE,@object # @_ZTVN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.globl	_ZTVN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.p2align	3
_ZTVN2kc41impl_ac_storage_class_specifier_AcTypedefE:
	.quad	0
	.quad	_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.quad	_ZNK2kc41impl_ac_storage_class_specifier_AcTypedef8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcTypedef7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcTypedefD0Ev
	.quad	_ZN2kc41impl_ac_storage_class_specifier_AcTypedef10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc41impl_ac_storage_class_specifier_AcTypedefE, 104

	.type	_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE,@object # @_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.globl	_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.p2align	4
_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE:
	.asciz	"N2kc41impl_ac_storage_class_specifier_AcTypedefE"
	.size	_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE, 49

	.type	_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE,@object # @_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.globl	_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.p2align	4
_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc41impl_ac_storage_class_specifier_AcTypedefE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc41impl_ac_storage_class_specifier_AcTypedefE, 24

	.type	_ZTVN2kc40impl_ac_storage_class_specifier_AcExternE,@object # @_ZTVN2kc40impl_ac_storage_class_specifier_AcExternE
	.globl	_ZTVN2kc40impl_ac_storage_class_specifier_AcExternE
	.p2align	3
_ZTVN2kc40impl_ac_storage_class_specifier_AcExternE:
	.quad	0
	.quad	_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE
	.quad	_ZNK2kc40impl_ac_storage_class_specifier_AcExtern8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcExtern7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcExternD0Ev
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcExtern10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc40impl_ac_storage_class_specifier_AcExternE, 104

	.type	_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE,@object # @_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE
	.globl	_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE
	.p2align	4
_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE:
	.asciz	"N2kc40impl_ac_storage_class_specifier_AcExternE"
	.size	_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE, 48

	.type	_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE,@object # @_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE
	.globl	_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE
	.p2align	4
_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc40impl_ac_storage_class_specifier_AcExternE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc40impl_ac_storage_class_specifier_AcExternE, 24

	.type	_ZTVN2kc40impl_ac_storage_class_specifier_AcStaticE,@object # @_ZTVN2kc40impl_ac_storage_class_specifier_AcStaticE
	.globl	_ZTVN2kc40impl_ac_storage_class_specifier_AcStaticE
	.p2align	3
_ZTVN2kc40impl_ac_storage_class_specifier_AcStaticE:
	.quad	0
	.quad	_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE
	.quad	_ZNK2kc40impl_ac_storage_class_specifier_AcStatic8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcStatic7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcStaticD0Ev
	.quad	_ZN2kc40impl_ac_storage_class_specifier_AcStatic10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc40impl_ac_storage_class_specifier_AcStaticE, 104

	.type	_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE,@object # @_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE
	.globl	_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE
	.p2align	4
_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE:
	.asciz	"N2kc40impl_ac_storage_class_specifier_AcStaticE"
	.size	_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE, 48

	.type	_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE,@object # @_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE
	.globl	_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE
	.p2align	4
_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc40impl_ac_storage_class_specifier_AcStaticE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc40impl_ac_storage_class_specifier_AcStaticE, 24

	.type	_ZTVN2kc42impl_ac_storage_class_specifier_AcRegisterE,@object # @_ZTVN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.globl	_ZTVN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.p2align	3
_ZTVN2kc42impl_ac_storage_class_specifier_AcRegisterE:
	.quad	0
	.quad	_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.quad	_ZNK2kc42impl_ac_storage_class_specifier_AcRegister8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc42impl_ac_storage_class_specifier_AcRegister7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc42impl_ac_storage_class_specifier_AcRegisterD0Ev
	.quad	_ZN2kc42impl_ac_storage_class_specifier_AcRegister10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc42impl_ac_storage_class_specifier_AcRegisterE, 104

	.type	_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE,@object # @_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.globl	_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.p2align	4
_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE:
	.asciz	"N2kc42impl_ac_storage_class_specifier_AcRegisterE"
	.size	_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE, 50

	.type	_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE,@object # @_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.globl	_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.p2align	4
_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc42impl_ac_storage_class_specifier_AcRegisterE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc42impl_ac_storage_class_specifier_AcRegisterE, 24

	.type	_ZTVN2kc38impl_ac_storage_class_specifier_AcAutoE,@object # @_ZTVN2kc38impl_ac_storage_class_specifier_AcAutoE
	.globl	_ZTVN2kc38impl_ac_storage_class_specifier_AcAutoE
	.p2align	3
_ZTVN2kc38impl_ac_storage_class_specifier_AcAutoE:
	.quad	0
	.quad	_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE
	.quad	_ZNK2kc38impl_ac_storage_class_specifier_AcAuto8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc38impl_ac_storage_class_specifier_AcAuto7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc38impl_ac_storage_class_specifier_AcAutoD0Ev
	.quad	_ZN2kc38impl_ac_storage_class_specifier_AcAuto10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc38impl_ac_storage_class_specifier_AcAutoE, 104

	.type	_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE,@object # @_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE
	.globl	_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE
	.p2align	4
_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE:
	.asciz	"N2kc38impl_ac_storage_class_specifier_AcAutoE"
	.size	_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE, 46

	.type	_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE,@object # @_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE
	.globl	_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE
	.p2align	4
_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc38impl_ac_storage_class_specifier_AcAutoE
	.quad	_ZTIN2kc31impl_ac_storage_class_specifierE
	.size	_ZTIN2kc38impl_ac_storage_class_specifier_AcAutoE, 24

	.type	_ZTVN2kc36impl_ac_type_qualifier_AcNoQualifierE,@object # @_ZTVN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.globl	_ZTVN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.p2align	3
_ZTVN2kc36impl_ac_type_qualifier_AcNoQualifierE:
	.quad	0
	.quad	_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.quad	_ZNK2kc36impl_ac_type_qualifier_AcNoQualifier8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc36impl_ac_type_qualifier_AcNoQualifier7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc36impl_ac_type_qualifier_AcNoQualifierD0Ev
	.quad	_ZN2kc36impl_ac_type_qualifier_AcNoQualifier10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc36impl_ac_type_qualifier_AcNoQualifierE, 104

	.type	_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE,@object # @_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.globl	_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.p2align	4
_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE:
	.asciz	"N2kc36impl_ac_type_qualifier_AcNoQualifierE"
	.size	_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE, 44

	.type	_ZTSN2kc22impl_ac_type_qualifierE,@object # @_ZTSN2kc22impl_ac_type_qualifierE
	.section	.rodata._ZTSN2kc22impl_ac_type_qualifierE,"aG",@progbits,_ZTSN2kc22impl_ac_type_qualifierE,comdat
	.weak	_ZTSN2kc22impl_ac_type_qualifierE
	.p2align	4
_ZTSN2kc22impl_ac_type_qualifierE:
	.asciz	"N2kc22impl_ac_type_qualifierE"
	.size	_ZTSN2kc22impl_ac_type_qualifierE, 30

	.type	_ZTIN2kc22impl_ac_type_qualifierE,@object # @_ZTIN2kc22impl_ac_type_qualifierE
	.section	.rodata._ZTIN2kc22impl_ac_type_qualifierE,"aG",@progbits,_ZTIN2kc22impl_ac_type_qualifierE,comdat
	.weak	_ZTIN2kc22impl_ac_type_qualifierE
	.p2align	4
_ZTIN2kc22impl_ac_type_qualifierE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_ac_type_qualifierE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc22impl_ac_type_qualifierE, 24

	.type	_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE,@object # @_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.p2align	4
_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc36impl_ac_type_qualifier_AcNoQualifierE
	.quad	_ZTIN2kc22impl_ac_type_qualifierE
	.size	_ZTIN2kc36impl_ac_type_qualifier_AcNoQualifierE, 24

	.type	_ZTVN2kc33impl_ac_type_qualifier_AcUnsignedE,@object # @_ZTVN2kc33impl_ac_type_qualifier_AcUnsignedE
	.globl	_ZTVN2kc33impl_ac_type_qualifier_AcUnsignedE
	.p2align	3
_ZTVN2kc33impl_ac_type_qualifier_AcUnsignedE:
	.quad	0
	.quad	_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE
	.quad	_ZNK2kc33impl_ac_type_qualifier_AcUnsigned8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc33impl_ac_type_qualifier_AcUnsigned7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc33impl_ac_type_qualifier_AcUnsignedD0Ev
	.quad	_ZN2kc33impl_ac_type_qualifier_AcUnsigned10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc33impl_ac_type_qualifier_AcUnsignedE, 104

	.type	_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE,@object # @_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE
	.globl	_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE
	.p2align	4
_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE:
	.asciz	"N2kc33impl_ac_type_qualifier_AcUnsignedE"
	.size	_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE, 41

	.type	_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE,@object # @_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE
	.globl	_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE
	.p2align	4
_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc33impl_ac_type_qualifier_AcUnsignedE
	.quad	_ZTIN2kc22impl_ac_type_qualifierE
	.size	_ZTIN2kc33impl_ac_type_qualifier_AcUnsignedE, 24

	.type	_ZTVN2kc33impl_ac_type_qualifier_AcVolatileE,@object # @_ZTVN2kc33impl_ac_type_qualifier_AcVolatileE
	.globl	_ZTVN2kc33impl_ac_type_qualifier_AcVolatileE
	.p2align	3
_ZTVN2kc33impl_ac_type_qualifier_AcVolatileE:
	.quad	0
	.quad	_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE
	.quad	_ZNK2kc33impl_ac_type_qualifier_AcVolatile8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc33impl_ac_type_qualifier_AcVolatile7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc33impl_ac_type_qualifier_AcVolatileD0Ev
	.quad	_ZN2kc33impl_ac_type_qualifier_AcVolatile10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc33impl_ac_type_qualifier_AcVolatileE, 104

	.type	_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE,@object # @_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE
	.globl	_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE
	.p2align	4
_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE:
	.asciz	"N2kc33impl_ac_type_qualifier_AcVolatileE"
	.size	_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE, 41

	.type	_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE,@object # @_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE
	.globl	_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE
	.p2align	4
_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc33impl_ac_type_qualifier_AcVolatileE
	.quad	_ZTIN2kc22impl_ac_type_qualifierE
	.size	_ZTIN2kc33impl_ac_type_qualifier_AcVolatileE, 24

	.type	_ZTVN2kc30impl_ac_type_qualifier_AcConstE,@object # @_ZTVN2kc30impl_ac_type_qualifier_AcConstE
	.globl	_ZTVN2kc30impl_ac_type_qualifier_AcConstE
	.p2align	3
_ZTVN2kc30impl_ac_type_qualifier_AcConstE:
	.quad	0
	.quad	_ZTIN2kc30impl_ac_type_qualifier_AcConstE
	.quad	_ZNK2kc30impl_ac_type_qualifier_AcConst8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc30impl_ac_type_qualifier_AcConst7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc30impl_ac_type_qualifier_AcConstD0Ev
	.quad	_ZN2kc30impl_ac_type_qualifier_AcConst10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc30impl_ac_type_qualifier_AcConstE, 104

	.type	_ZTSN2kc30impl_ac_type_qualifier_AcConstE,@object # @_ZTSN2kc30impl_ac_type_qualifier_AcConstE
	.globl	_ZTSN2kc30impl_ac_type_qualifier_AcConstE
	.p2align	4
_ZTSN2kc30impl_ac_type_qualifier_AcConstE:
	.asciz	"N2kc30impl_ac_type_qualifier_AcConstE"
	.size	_ZTSN2kc30impl_ac_type_qualifier_AcConstE, 38

	.type	_ZTIN2kc30impl_ac_type_qualifier_AcConstE,@object # @_ZTIN2kc30impl_ac_type_qualifier_AcConstE
	.globl	_ZTIN2kc30impl_ac_type_qualifier_AcConstE
	.p2align	4
_ZTIN2kc30impl_ac_type_qualifier_AcConstE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc30impl_ac_type_qualifier_AcConstE
	.quad	_ZTIN2kc22impl_ac_type_qualifierE
	.size	_ZTIN2kc30impl_ac_type_qualifier_AcConstE, 24

	.type	_ZTVN2kc32impl_ac_pointer_option_NopointerE,@object # @_ZTVN2kc32impl_ac_pointer_option_NopointerE
	.globl	_ZTVN2kc32impl_ac_pointer_option_NopointerE
	.p2align	3
_ZTVN2kc32impl_ac_pointer_option_NopointerE:
	.quad	0
	.quad	_ZTIN2kc32impl_ac_pointer_option_NopointerE
	.quad	_ZNK2kc32impl_ac_pointer_option_Nopointer8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc32impl_ac_pointer_option_Nopointer7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc32impl_ac_pointer_option_NopointerD0Ev
	.quad	_ZN2kc32impl_ac_pointer_option_Nopointer10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc32impl_ac_pointer_option_NopointerE, 104

	.type	_ZTSN2kc32impl_ac_pointer_option_NopointerE,@object # @_ZTSN2kc32impl_ac_pointer_option_NopointerE
	.globl	_ZTSN2kc32impl_ac_pointer_option_NopointerE
	.p2align	4
_ZTSN2kc32impl_ac_pointer_option_NopointerE:
	.asciz	"N2kc32impl_ac_pointer_option_NopointerE"
	.size	_ZTSN2kc32impl_ac_pointer_option_NopointerE, 40

	.type	_ZTSN2kc22impl_ac_pointer_optionE,@object # @_ZTSN2kc22impl_ac_pointer_optionE
	.section	.rodata._ZTSN2kc22impl_ac_pointer_optionE,"aG",@progbits,_ZTSN2kc22impl_ac_pointer_optionE,comdat
	.weak	_ZTSN2kc22impl_ac_pointer_optionE
	.p2align	4
_ZTSN2kc22impl_ac_pointer_optionE:
	.asciz	"N2kc22impl_ac_pointer_optionE"
	.size	_ZTSN2kc22impl_ac_pointer_optionE, 30

	.type	_ZTIN2kc22impl_ac_pointer_optionE,@object # @_ZTIN2kc22impl_ac_pointer_optionE
	.section	.rodata._ZTIN2kc22impl_ac_pointer_optionE,"aG",@progbits,_ZTIN2kc22impl_ac_pointer_optionE,comdat
	.weak	_ZTIN2kc22impl_ac_pointer_optionE
	.p2align	4
_ZTIN2kc22impl_ac_pointer_optionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_ac_pointer_optionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc22impl_ac_pointer_optionE, 24

	.type	_ZTIN2kc32impl_ac_pointer_option_NopointerE,@object # @_ZTIN2kc32impl_ac_pointer_option_NopointerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc32impl_ac_pointer_option_NopointerE
	.p2align	4
_ZTIN2kc32impl_ac_pointer_option_NopointerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc32impl_ac_pointer_option_NopointerE
	.quad	_ZTIN2kc22impl_ac_pointer_optionE
	.size	_ZTIN2kc32impl_ac_pointer_option_NopointerE, 24

	.type	_ZTVN2kc24impl_ac_ref_option_AcRefE,@object # @_ZTVN2kc24impl_ac_ref_option_AcRefE
	.globl	_ZTVN2kc24impl_ac_ref_option_AcRefE
	.p2align	3
_ZTVN2kc24impl_ac_ref_option_AcRefE:
	.quad	0
	.quad	_ZTIN2kc24impl_ac_ref_option_AcRefE
	.quad	_ZNK2kc24impl_ac_ref_option_AcRef8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc24impl_ac_ref_option_AcRef7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc24impl_ac_ref_option_AcRefD0Ev
	.quad	_ZN2kc24impl_ac_ref_option_AcRef10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc24impl_ac_ref_option_AcRefE, 104

	.type	_ZTSN2kc24impl_ac_ref_option_AcRefE,@object # @_ZTSN2kc24impl_ac_ref_option_AcRefE
	.globl	_ZTSN2kc24impl_ac_ref_option_AcRefE
	.p2align	4
_ZTSN2kc24impl_ac_ref_option_AcRefE:
	.asciz	"N2kc24impl_ac_ref_option_AcRefE"
	.size	_ZTSN2kc24impl_ac_ref_option_AcRefE, 32

	.type	_ZTSN2kc18impl_ac_ref_optionE,@object # @_ZTSN2kc18impl_ac_ref_optionE
	.section	.rodata._ZTSN2kc18impl_ac_ref_optionE,"aG",@progbits,_ZTSN2kc18impl_ac_ref_optionE,comdat
	.weak	_ZTSN2kc18impl_ac_ref_optionE
	.p2align	4
_ZTSN2kc18impl_ac_ref_optionE:
	.asciz	"N2kc18impl_ac_ref_optionE"
	.size	_ZTSN2kc18impl_ac_ref_optionE, 26

	.type	_ZTIN2kc18impl_ac_ref_optionE,@object # @_ZTIN2kc18impl_ac_ref_optionE
	.section	.rodata._ZTIN2kc18impl_ac_ref_optionE,"aG",@progbits,_ZTIN2kc18impl_ac_ref_optionE,comdat
	.weak	_ZTIN2kc18impl_ac_ref_optionE
	.p2align	4
_ZTIN2kc18impl_ac_ref_optionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc18impl_ac_ref_optionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc18impl_ac_ref_optionE, 24

	.type	_ZTIN2kc24impl_ac_ref_option_AcRefE,@object # @_ZTIN2kc24impl_ac_ref_option_AcRefE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc24impl_ac_ref_option_AcRefE
	.p2align	4
_ZTIN2kc24impl_ac_ref_option_AcRefE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc24impl_ac_ref_option_AcRefE
	.quad	_ZTIN2kc18impl_ac_ref_optionE
	.size	_ZTIN2kc24impl_ac_ref_option_AcRefE, 24

	.type	_ZTVN2kc26impl_ac_ref_option_AcNoRefE,@object # @_ZTVN2kc26impl_ac_ref_option_AcNoRefE
	.globl	_ZTVN2kc26impl_ac_ref_option_AcNoRefE
	.p2align	3
_ZTVN2kc26impl_ac_ref_option_AcNoRefE:
	.quad	0
	.quad	_ZTIN2kc26impl_ac_ref_option_AcNoRefE
	.quad	_ZNK2kc26impl_ac_ref_option_AcNoRef8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc26impl_ac_ref_option_AcNoRef7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc26impl_ac_ref_option_AcNoRefD0Ev
	.quad	_ZN2kc26impl_ac_ref_option_AcNoRef10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc26impl_ac_ref_option_AcNoRefE, 104

	.type	_ZTSN2kc26impl_ac_ref_option_AcNoRefE,@object # @_ZTSN2kc26impl_ac_ref_option_AcNoRefE
	.globl	_ZTSN2kc26impl_ac_ref_option_AcNoRefE
	.p2align	4
_ZTSN2kc26impl_ac_ref_option_AcNoRefE:
	.asciz	"N2kc26impl_ac_ref_option_AcNoRefE"
	.size	_ZTSN2kc26impl_ac_ref_option_AcNoRefE, 34

	.type	_ZTIN2kc26impl_ac_ref_option_AcNoRefE,@object # @_ZTIN2kc26impl_ac_ref_option_AcNoRefE
	.globl	_ZTIN2kc26impl_ac_ref_option_AcNoRefE
	.p2align	4
_ZTIN2kc26impl_ac_ref_option_AcNoRefE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc26impl_ac_ref_option_AcNoRefE
	.quad	_ZTIN2kc18impl_ac_ref_optionE
	.size	_ZTIN2kc26impl_ac_ref_option_AcNoRefE, 24

	.type	_ZTVN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE,@object # @_ZTVN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.globl	_ZTVN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.p2align	3
_ZTVN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE:
	.quad	0
	.quad	_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.quad	_ZNK2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorD0Ev
	.quad	_ZN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declarator10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE, 104

	.type	_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE,@object # @_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.globl	_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.p2align	4
_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE:
	.asciz	"N2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE"
	.size	_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE, 81

	.type	_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE,@object # @_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE
	.section	.rodata._ZTSN2kc41impl_ac_direct_abstract_declarator_optionE,"aG",@progbits,_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE,comdat
	.weak	_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE
	.p2align	4
_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE:
	.asciz	"N2kc41impl_ac_direct_abstract_declarator_optionE"
	.size	_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE, 49

	.type	_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE,@object # @_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE
	.section	.rodata._ZTIN2kc41impl_ac_direct_abstract_declarator_optionE,"aG",@progbits,_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE,comdat
	.weak	_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE
	.p2align	4
_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc41impl_ac_direct_abstract_declarator_optionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE, 24

	.type	_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE,@object # @_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.p2align	4
_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE
	.quad	_ZTIN2kc41impl_ac_direct_abstract_declarator_optionE
	.size	_ZTIN2kc73impl_ac_direct_abstract_declarator_option_Noac_direct_abstract_declaratorE, 24

	.type	_ZTVN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE,@object # @_ZTVN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.globl	_ZTVN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.p2align	3
_ZTVN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE:
	.quad	0
	.quad	_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.quad	_ZNK2kc59impl_ac_constant_expression_option_Noac_constant_expression8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expressionD0Ev
	.quad	_ZN2kc59impl_ac_constant_expression_option_Noac_constant_expression10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE, 104

	.type	_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE,@object # @_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.globl	_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.p2align	4
_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE:
	.asciz	"N2kc59impl_ac_constant_expression_option_Noac_constant_expressionE"
	.size	_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE, 67

	.type	_ZTSN2kc34impl_ac_constant_expression_optionE,@object # @_ZTSN2kc34impl_ac_constant_expression_optionE
	.section	.rodata._ZTSN2kc34impl_ac_constant_expression_optionE,"aG",@progbits,_ZTSN2kc34impl_ac_constant_expression_optionE,comdat
	.weak	_ZTSN2kc34impl_ac_constant_expression_optionE
	.p2align	4
_ZTSN2kc34impl_ac_constant_expression_optionE:
	.asciz	"N2kc34impl_ac_constant_expression_optionE"
	.size	_ZTSN2kc34impl_ac_constant_expression_optionE, 42

	.type	_ZTIN2kc34impl_ac_constant_expression_optionE,@object # @_ZTIN2kc34impl_ac_constant_expression_optionE
	.section	.rodata._ZTIN2kc34impl_ac_constant_expression_optionE,"aG",@progbits,_ZTIN2kc34impl_ac_constant_expression_optionE,comdat
	.weak	_ZTIN2kc34impl_ac_constant_expression_optionE
	.p2align	4
_ZTIN2kc34impl_ac_constant_expression_optionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc34impl_ac_constant_expression_optionE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc34impl_ac_constant_expression_optionE, 24

	.type	_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE,@object # @_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.p2align	4
_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE
	.quad	_ZTIN2kc34impl_ac_constant_expression_optionE
	.size	_ZTIN2kc59impl_ac_constant_expression_option_Noac_constant_expressionE, 24

	.type	_ZTVN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE,@object # @_ZTVN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.globl	_ZTVN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.p2align	3
_ZTVN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE:
	.quad	0
	.quad	_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.quad	_ZNK2kc39impl_ac_opt_base_init_list_AcNoBaseInit8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInitD0Ev
	.quad	_ZN2kc39impl_ac_opt_base_init_list_AcNoBaseInit10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE, 104

	.type	_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE,@object # @_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.globl	_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.p2align	4
_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE:
	.asciz	"N2kc39impl_ac_opt_base_init_list_AcNoBaseInitE"
	.size	_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE, 47

	.type	_ZTSN2kc26impl_ac_opt_base_init_listE,@object # @_ZTSN2kc26impl_ac_opt_base_init_listE
	.section	.rodata._ZTSN2kc26impl_ac_opt_base_init_listE,"aG",@progbits,_ZTSN2kc26impl_ac_opt_base_init_listE,comdat
	.weak	_ZTSN2kc26impl_ac_opt_base_init_listE
	.p2align	4
_ZTSN2kc26impl_ac_opt_base_init_listE:
	.asciz	"N2kc26impl_ac_opt_base_init_listE"
	.size	_ZTSN2kc26impl_ac_opt_base_init_listE, 34

	.type	_ZTIN2kc26impl_ac_opt_base_init_listE,@object # @_ZTIN2kc26impl_ac_opt_base_init_listE
	.section	.rodata._ZTIN2kc26impl_ac_opt_base_init_listE,"aG",@progbits,_ZTIN2kc26impl_ac_opt_base_init_listE,comdat
	.weak	_ZTIN2kc26impl_ac_opt_base_init_listE
	.p2align	4
_ZTIN2kc26impl_ac_opt_base_init_listE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc26impl_ac_opt_base_init_listE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc26impl_ac_opt_base_init_listE, 24

	.type	_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE,@object # @_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.p2align	4
_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE
	.quad	_ZTIN2kc26impl_ac_opt_base_init_listE
	.size	_ZTIN2kc39impl_ac_opt_base_init_list_AcNoBaseInitE, 24

	.type	_ZTVN2kc19impl_charruns_StarsE,@object # @_ZTVN2kc19impl_charruns_StarsE
	.globl	_ZTVN2kc19impl_charruns_StarsE
	.p2align	3
_ZTVN2kc19impl_charruns_StarsE:
	.quad	0
	.quad	_ZTIN2kc19impl_charruns_StarsE
	.quad	_ZNK2kc19impl_charruns_Stars8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc19impl_charruns_Stars7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc19impl_charruns_StarsD0Ev
	.quad	_ZN2kc19impl_charruns_Stars10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc19impl_charruns_StarsE, 104

	.type	_ZTSN2kc19impl_charruns_StarsE,@object # @_ZTSN2kc19impl_charruns_StarsE
	.globl	_ZTSN2kc19impl_charruns_StarsE
	.p2align	4
_ZTSN2kc19impl_charruns_StarsE:
	.asciz	"N2kc19impl_charruns_StarsE"
	.size	_ZTSN2kc19impl_charruns_StarsE, 27

	.type	_ZTSN2kc13impl_charrunsE,@object # @_ZTSN2kc13impl_charrunsE
	.section	.rodata._ZTSN2kc13impl_charrunsE,"aG",@progbits,_ZTSN2kc13impl_charrunsE,comdat
	.weak	_ZTSN2kc13impl_charrunsE
	.p2align	4
_ZTSN2kc13impl_charrunsE:
	.asciz	"N2kc13impl_charrunsE"
	.size	_ZTSN2kc13impl_charrunsE, 21

	.type	_ZTIN2kc13impl_charrunsE,@object # @_ZTIN2kc13impl_charrunsE
	.section	.rodata._ZTIN2kc13impl_charrunsE,"aG",@progbits,_ZTIN2kc13impl_charrunsE,comdat
	.weak	_ZTIN2kc13impl_charrunsE
	.p2align	4
_ZTIN2kc13impl_charrunsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc13impl_charrunsE
	.quad	_ZTIN2kc20impl_abstract_phylumE
	.size	_ZTIN2kc13impl_charrunsE, 24

	.type	_ZTIN2kc19impl_charruns_StarsE,@object # @_ZTIN2kc19impl_charruns_StarsE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN2kc19impl_charruns_StarsE
	.p2align	4
_ZTIN2kc19impl_charruns_StarsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc19impl_charruns_StarsE
	.quad	_ZTIN2kc13impl_charrunsE
	.size	_ZTIN2kc19impl_charruns_StarsE, 24

	.type	_ZTVN2kc28impl_charruns_QuotedNewlinesE,@object # @_ZTVN2kc28impl_charruns_QuotedNewlinesE
	.globl	_ZTVN2kc28impl_charruns_QuotedNewlinesE
	.p2align	3
_ZTVN2kc28impl_charruns_QuotedNewlinesE:
	.quad	0
	.quad	_ZTIN2kc28impl_charruns_QuotedNewlinesE
	.quad	_ZNK2kc28impl_charruns_QuotedNewlines8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc28impl_charruns_QuotedNewlines7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc28impl_charruns_QuotedNewlinesD0Ev
	.quad	_ZN2kc28impl_charruns_QuotedNewlines10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc28impl_charruns_QuotedNewlinesE, 104

	.type	_ZTSN2kc28impl_charruns_QuotedNewlinesE,@object # @_ZTSN2kc28impl_charruns_QuotedNewlinesE
	.globl	_ZTSN2kc28impl_charruns_QuotedNewlinesE
	.p2align	4
_ZTSN2kc28impl_charruns_QuotedNewlinesE:
	.asciz	"N2kc28impl_charruns_QuotedNewlinesE"
	.size	_ZTSN2kc28impl_charruns_QuotedNewlinesE, 36

	.type	_ZTIN2kc28impl_charruns_QuotedNewlinesE,@object # @_ZTIN2kc28impl_charruns_QuotedNewlinesE
	.globl	_ZTIN2kc28impl_charruns_QuotedNewlinesE
	.p2align	4
_ZTIN2kc28impl_charruns_QuotedNewlinesE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc28impl_charruns_QuotedNewlinesE
	.quad	_ZTIN2kc13impl_charrunsE
	.size	_ZTIN2kc28impl_charruns_QuotedNewlinesE, 24

	.type	_ZTVN2kc22impl_charruns_NewlinesE,@object # @_ZTVN2kc22impl_charruns_NewlinesE
	.globl	_ZTVN2kc22impl_charruns_NewlinesE
	.p2align	3
_ZTVN2kc22impl_charruns_NewlinesE:
	.quad	0
	.quad	_ZTIN2kc22impl_charruns_NewlinesE
	.quad	_ZNK2kc22impl_charruns_Newlines8prod_selEv
	.quad	_ZNK2kc20impl_abstract_phylum9subphylumEi
	.quad	_ZN2kc20impl_abstract_phylum13set_subphylumEiPS0_
	.quad	_ZN2kc22impl_charruns_Newlines7rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum10do_rewriteERNS_11rview_classE
	.quad	_ZN2kc20impl_abstract_phylum15rewrite_membersEPS0_
	.quad	_ZN2kc20impl_abstract_phylum11post_createEv
	.quad	_ZN2kc20impl_abstract_phylumD2Ev
	.quad	_ZN2kc22impl_charruns_NewlinesD0Ev
	.quad	_ZN2kc22impl_charruns_Newlines10do_unparseERNS_21printer_functor_classERNS_11uview_classE
	.quad	_ZN2kc20impl_abstract_phylum15default_unparseERNS_21printer_functor_classERNS_11uview_classE
	.size	_ZTVN2kc22impl_charruns_NewlinesE, 104

	.type	_ZTSN2kc22impl_charruns_NewlinesE,@object # @_ZTSN2kc22impl_charruns_NewlinesE
	.globl	_ZTSN2kc22impl_charruns_NewlinesE
	.p2align	4
_ZTSN2kc22impl_charruns_NewlinesE:
	.asciz	"N2kc22impl_charruns_NewlinesE"
	.size	_ZTSN2kc22impl_charruns_NewlinesE, 30

	.type	_ZTIN2kc22impl_charruns_NewlinesE,@object # @_ZTIN2kc22impl_charruns_NewlinesE
	.globl	_ZTIN2kc22impl_charruns_NewlinesE
	.p2align	4
_ZTIN2kc22impl_charruns_NewlinesE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN2kc22impl_charruns_NewlinesE
	.quad	_ZTIN2kc13impl_charrunsE
	.size	_ZTIN2kc22impl_charruns_NewlinesE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
