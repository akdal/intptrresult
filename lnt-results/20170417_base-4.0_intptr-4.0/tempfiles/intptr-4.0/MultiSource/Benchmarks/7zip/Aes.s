	.text
	.file	"Aes.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI0_1:
	.long	27                      # 0x1b
	.long	27                      # 0x1b
	.long	27                      # 0x1b
	.long	27                      # 0x1b
.LCPI0_2:
	.long	254                     # 0xfe
	.long	254                     # 0xfe
	.long	254                     # 0xfe
	.long	254                     # 0xfe
	.text
	.globl	AesGenTables
	.p2align	4, 0x90
	.type	AesGenTables,@function
AesGenTables:                           # @AesGenTables
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	Sbox(%rax), %ecx
	movb	%al, InvS(%rcx)
	leal	1(%rax), %ecx
	movzbl	Sbox+1(%rax), %edx
	movb	%cl, InvS(%rdx)
	leal	2(%rax), %ecx
	movzbl	Sbox+2(%rax), %edx
	movb	%cl, InvS(%rdx)
	leal	3(%rax), %ecx
	movzbl	Sbox+3(%rax), %edx
	movb	%cl, InvS(%rdx)
	addq	$4, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_1
# BB#2:                                 # %vector.body.preheader
	movq	$-256, %rax
	movdqa	.LCPI0_0(%rip), %xmm8   # xmm8 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	.LCPI0_1(%rip), %xmm9   # xmm9 = [27,27,27,27]
	movdqa	.LCPI0_2(%rip), %xmm10  # xmm10 = [254,254,254,254]
	.p2align	4, 0x90
.LBB0_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	Sbox+256(%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	psrad	$24, %xmm1
	pxor	%xmm0, %xmm0
	pcmpgtd	%xmm1, %xmm0
	pand	%xmm8, %xmm1
	movdqa	%xmm1, %xmm3
	paddd	%xmm3, %xmm3
	pand	%xmm9, %xmm0
	pand	%xmm10, %xmm3
	pxor	%xmm0, %xmm3
	movdqa	%xmm3, %xmm5
	pxor	%xmm1, %xmm5
	movdqa	%xmm1, %xmm6
	pslld	$8, %xmm6
	movdqa	%xmm1, %xmm4
	pslld	$16, %xmm4
	movdqa	%xmm5, %xmm7
	pslld	$24, %xmm7
	movdqa	%xmm4, %xmm0
	por	%xmm6, %xmm0
	por	%xmm3, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm0, T+1024(,%rax,4)
	movdqa	%xmm3, %xmm0
	pslld	$8, %xmm0
	movdqa	%xmm1, %xmm7
	pslld	$24, %xmm7
	por	%xmm7, %xmm4
	por	%xmm0, %xmm4
	por	%xmm5, %xmm4
	movdqa	%xmm4, T+2048(,%rax,4)
	movdqa	%xmm5, %xmm0
	pslld	$8, %xmm0
	movdqa	%xmm3, %xmm4
	pslld	$16, %xmm4
	por	%xmm1, %xmm7
	por	%xmm4, %xmm7
	por	%xmm0, %xmm7
	movdqa	%xmm7, T+3072(,%rax,4)
	por	%xmm1, %xmm6
	pslld	$16, %xmm5
	pslld	$24, %xmm3
	por	%xmm6, %xmm3
	por	%xmm5, %xmm3
	movdqa	%xmm3, T+4096(,%rax,4)
	movd	InvS+256(%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	punpcklwd	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1],xmm7[2],xmm0[2],xmm7[3],xmm0[3]
	psrad	$24, %xmm7
	pxor	%xmm0, %xmm0
	pcmpgtd	%xmm7, %xmm0
	pand	%xmm8, %xmm7
	movdqa	%xmm7, %xmm1
	paddd	%xmm1, %xmm1
	pand	%xmm9, %xmm0
	movdqa	%xmm1, %xmm3
	pand	%xmm10, %xmm3
	pxor	%xmm0, %xmm3
	movdqa	%xmm3, %xmm0
	paddd	%xmm0, %xmm0
	pslld	$24, %xmm1
	psrad	$24, %xmm1
	pxor	%xmm4, %xmm4
	pcmpgtd	%xmm1, %xmm4
	pand	%xmm9, %xmm4
	movdqa	%xmm0, %xmm1
	pand	%xmm10, %xmm1
	pxor	%xmm4, %xmm1
	movdqa	%xmm1, %xmm4
	paddd	%xmm4, %xmm4
	pslld	$24, %xmm0
	psrad	$24, %xmm0
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm0, %xmm5
	pand	%xmm9, %xmm5
	pand	%xmm10, %xmm4
	pxor	%xmm5, %xmm4
	movdqa	%xmm4, %xmm2
	pxor	%xmm7, %xmm2
	movdqa	%xmm3, %xmm5
	pxor	%xmm7, %xmm5
	pxor	%xmm4, %xmm5
	pxor	%xmm1, %xmm4
	pxor	%xmm4, %xmm7
	pxor	%xmm3, %xmm4
	movdqa	%xmm5, %xmm1
	movdqa	%xmm2, %xmm6
	pslld	$16, %xmm6
	por	%xmm5, %xmm6
	movdqa	%xmm5, %xmm11
	movdqa	%xmm2, %xmm0
	pslld	$16, %xmm5
	por	%xmm2, %xmm5
	pslld	$8, %xmm2
	por	%xmm4, %xmm2
	movdqa	%xmm7, %xmm3
	pslld	$16, %xmm3
	por	%xmm2, %xmm3
	pslld	$24, %xmm1
	por	%xmm3, %xmm1
	movdqa	%xmm1, D+1024(,%rax,4)
	movdqa	%xmm4, %xmm1
	pslld	$8, %xmm1
	por	%xmm1, %xmm6
	movdqa	%xmm7, %xmm1
	pslld	$24, %xmm1
	por	%xmm1, %xmm6
	movdqa	%xmm6, D+2048(,%rax,4)
	pslld	$8, %xmm11
	movdqa	%xmm4, %xmm1
	pslld	$16, %xmm1
	pslld	$24, %xmm0
	por	%xmm7, %xmm0
	por	%xmm11, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, D+3072(,%rax,4)
	pslld	$8, %xmm7
	por	%xmm7, %xmm5
	pslld	$24, %xmm4
	por	%xmm5, %xmm4
	movdqa	%xmm4, D+4096(,%rax,4)
	addq	$4, %rax
	jne	.LBB0_3
# BB#4:                                 # %middle.block
	movq	$AesCbc_Encode, g_AesCbc_Encode(%rip)
	movq	$AesCbc_Decode, g_AesCbc_Decode(%rip)
	movq	$AesCtr_Code, g_AesCtr_Code(%rip)
	retq
.Lfunc_end0:
	.size	AesGenTables, .Lfunc_end0-AesGenTables
	.cfi_endproc

	.globl	AesCbc_Encode
	.p2align	4, 0x90
	.type	AesCbc_Encode,@function
AesCbc_Encode:                          # @AesCbc_Encode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB1_3
# BB#1:                                 # %.lr.ph
	leaq	16(%r15), %r12
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	xorl	%eax, (%r15)
	movl	4(%rbx), %eax
	xorl	%eax, 4(%r15)
	movl	8(%rbx), %eax
	xorl	%eax, 8(%r15)
	movl	12(%rbx), %eax
	xorl	%eax, 12(%r15)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r15, %rdx
	callq	Aes_Encode
	movl	(%r15), %eax
	movl	%eax, (%rbx)
	movl	4(%r15), %eax
	movl	%eax, 4(%rbx)
	movl	8(%r15), %eax
	movl	%eax, 8(%rbx)
	movl	12(%r15), %eax
	movl	%eax, 12(%rbx)
	addq	$16, %rbx
	decq	%r14
	jne	.LBB1_2
.LBB1_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	AesCbc_Encode, .Lfunc_end1-AesCbc_Encode
	.cfi_endproc

	.globl	AesCbc_Decode
	.p2align	4, 0x90
	.type	AesCbc_Decode,@function
AesCbc_Decode:                          # @AesCbc_Decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	testq	%rdx, %rdx
	je	.LBB2_6
# BB#1:                                 # %.lr.ph
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	leaq	28(%rax), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movdqu	(%rsi), %xmm0
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %ecx
	leal	4(,%rcx,8), %esi
	movd	%xmm0, %eax
	xorl	(%rdx,%rsi,4), %eax
	movl	%eax, -104(%rsp)
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, %eax
	xorl	4(%rdx,%rsi,4), %eax
	movl	%eax, -100(%rsp)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %ebx
	xorl	8(%rdx,%rsi,4), %ebx
	movl	%ebx, -96(%rsp)
	leaq	12(%rdx,%rsi,4), %rax
	leal	-1(%rcx), %edx
	shlq	$3, %rdx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	leaq	-8(%rsi), %rdx
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, %r13d
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	leaq	-16(%rdx,%rsi,4), %r11
	movl	$1, %r15d
	subl	%ecx, %r15d
	movl	$3, %edx
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=2
	movzbl	%ch, %esi  # NOREX
	movq	%rsi, %r14
	movl	%ecx, %r9d
	movzbl	%cl, %r10d
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	movl	%ebx, %esi
	movzbl	%bl, %r8d
	movzbl	%al, %ebp
	movzbl	%dh, %edi  # NOREX
	movl	D+1024(,%rdi,4), %r13d
	xorl	D(,%rbp,4), %r13d
	movzbl	%bh, %ebp  # NOREX
	xorl	D+2048(,%rcx,4), %r13d
	shrl	$24, %ebx
	xorl	D+3072(,%rbx,4), %r13d
	movl	%eax, %ecx
	movzbl	%ah, %edi  # NOREX
	movl	D+1024(,%r14,4), %ebx
	xorl	D(,%r12,4), %ebx
	shrl	$16, %esi
	movzbl	%sil, %esi
	xorl	D+2048(,%rsi,4), %ebx
	shrl	$24, %ecx
	xorl	D+3072(,%rcx,4), %ebx
	xorl	-28(%r11), %ebx
	movl	%ebx, -104(%rsp)
	movl	%edx, %ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	movl	D+1024(,%rdi,4), %ebx
	xorl	D(,%r8,4), %ebx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	D+2048(,%rcx,4), %ebx
	shrl	$24, %r9d
	xorl	D+3072(,%r9,4), %ebx
	xorl	-20(%r11), %ebx
	movl	%ebx, -96(%rsp)
	movl	D+1024(,%rbp,4), %ecx
	xorl	D(,%r10,4), %ecx
	shrl	$16, %eax
	movzbl	%al, %eax
	xorl	D+2048(,%rax,4), %ecx
	shrl	$24, %edx
	xorl	D+3072(,%rdx,4), %ecx
	xorl	-16(%r11), %ecx
	movl	%ecx, -92(%rsp)
	movq	%r11, %rax
	leaq	-32(%r11), %r11
	addq	$-24, %rax
	incl	%r15d
	movl	$1, %edx
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	(%rax), %r13d
	movl	%r13d, -104(%rsp,%rdx,4)
	movl	-104(%rsp), %edx
	movl	-92(%rsp), %eax
	movzbl	%ah, %ecx  # NOREX
	movq	%rcx, %r14
	movl	%eax, %r10d
	movzbl	%al, %r9d
	shrl	$16, %eax
	movzbl	%al, %r12d
	movl	-100(%rsp), %ecx
	movl	%ebx, %edi
	movzbl	%bl, %r13d
	movzbl	%bh, %ebp  # NOREX
	movzbl	%cl, %r8d
	movzbl	%dh, %eax  # NOREX
	movl	D+1024(,%rax,4), %eax
	xorl	D(,%r8,4), %eax
	xorl	D+2048(,%r12,4), %eax
	shrl	$24, %ebx
	xorl	D+3072(,%rbx,4), %eax
	movzbl	%dl, %ebx
	movq	%rdx, %r12
	movzbl	%ch, %esi  # NOREX
	movl	%ecx, %r8d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movl	D+1024(,%r14,4), %edx
	xorl	D(,%rbx,4), %edx
	shrl	$16, %edi
	movzbl	%dil, %edi
	xorl	D+2048(,%rdi,4), %edx
	shrl	$24, %ecx
	xorl	D+3072(,%rcx,4), %edx
	movq	%r12, %rcx
	movl	%ecx, %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	D+1024(,%rsi,4), %ebx
	xorl	D(,%r13,4), %ebx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	D+2048(,%rcx,4), %ebx
	xorl	-12(%r11), %edx
	shrl	$24, %r10d
	xorl	D+3072(,%r10,4), %ebx
	xorl	-4(%r11), %ebx
	movl	D+1024(,%rbp,4), %ecx
	xorl	D(,%r9,4), %ecx
	shrl	$16, %r8d
	movzbl	%r8b, %esi
	xorl	D+2048(,%rsi,4), %ecx
	shrl	$24, %edi
	xorl	D+3072(,%rdi,4), %ecx
	xorl	(%r11), %ecx
	xorl	-8(%r11), %eax
	testl	%r15d, %r15d
	movzbl	%dl, %r12d
	jne	.LBB2_4
# BB#5:                                 # %Aes_Decode.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movzbl	%ch, %ebp  # NOREX
	movl	%ecx, %r9d
	movl	%ecx, %r11d
	movzbl	%cl, %ecx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movl	%ebx, %esi
	movl	%ebx, %r15d
	movzbl	%bl, %r10d
	movzbl	%bh, %ecx  # NOREX
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	-40(%rsp), %r8          # 8-byte Reload
	subq	%rcx, %r8
	movq	-24(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movzbl	InvS(%r12), %ecx
	movzbl	InvS(%rbp), %ebp
	shll	$8, %ebp
	orl	%ecx, %ebp
	movl	%eax, %ecx
	movzbl	%al, %r12d
	movzbl	%ah, %ebx  # NOREX
	shrl	$16, %eax
	movzbl	%al, %r13d
	shrl	$16, %esi
	movzbl	%sil, %esi
	movzbl	InvS(%rsi), %esi
	shll	$16, %esi
	orl	%ebp, %esi
	shrl	$24, %ecx
	movzbl	InvS(%rcx), %ecx
	shll	$24, %ecx
	orl	%esi, %ecx
	movzbl	%dh, %esi  # NOREX
	movl	%edx, %ebp
	shrl	$24, %edx
	movzbl	InvS(%rdx), %edx
	movq	-80(%rsp), %r14         # 8-byte Reload
	xorl	16(%r14,%rdi,4), %ecx
	movzbl	InvS(%r12), %eax
	movzbl	InvS(%rsi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	shrl	$16, %r9d
	movzbl	%r9b, %eax
	movzbl	InvS(%rax), %eax
	shll	$16, %eax
	orl	%esi, %eax
	shrl	$24, %r15d
	movzbl	InvS(%r15), %r9d
	shll	$24, %r9d
	orl	%eax, %r9d
	movzbl	InvS(%r10), %eax
	movzbl	InvS(%rbx), %edi
	shll	$8, %edi
	orl	%eax, %edi
	shrl	$16, %ebp
	movzbl	%bpl, %eax
	movzbl	InvS(%rax), %eax
	shll	$16, %eax
	orl	%edi, %eax
	xorl	-12(%r14,%r8,4), %r9d
	shrl	$24, %r11d
	movzbl	InvS(%r11), %edi
	shll	$24, %edi
	orl	%eax, %edi
	xorl	-8(%r14,%r8,4), %edi
	movq	-48(%rsp), %rax         # 8-byte Reload
	movzbl	InvS(%rax), %eax
	movq	-56(%rsp), %rsi         # 8-byte Reload
	movzbl	InvS(%rsi), %ebp
	shll	$8, %ebp
	orl	%eax, %ebp
	movzbl	InvS(%r13), %eax
	shll	$16, %eax
	orl	%ebp, %eax
	shll	$24, %edx
	orl	%eax, %edx
	xorl	-4(%r14,%r8,4), %edx
	xorl	(%r14), %ecx
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movl	%ecx, (%rsi)
	xorl	4(%r14), %r9d
	movl	%r9d, 4(%rsi)
	xorl	8(%r14), %edi
	movl	%edi, 8(%rsi)
	xorl	12(%r14), %edx
	movl	%edx, 12(%rsi)
	movdqu	%xmm0, (%r14)
	addq	$16, %rsi
	movq	-8(%rsp), %rdx          # 8-byte Reload
	decq	%rdx
	jne	.LBB2_2
.LBB2_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	AesCbc_Decode, .Lfunc_end2-AesCbc_Decode
	.cfi_endproc

	.globl	AesCtr_Code
	.p2align	4, 0x90
	.type	AesCtr_Code,@function
AesCtr_Code:                            # @AesCtr_Code
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 96
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r13, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB3_5
# BB#1:                                 # %.lr.ph
	leaq	16(%r15), %r12
	leaq	32(%rsp), %r13
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	incl	(%r15)
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	incl	4(%r15)
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	Aes_Encode
	movaps	32(%rsp), %xmm0
	movaps	%xmm0, (%rsp)
	movaps	%xmm0, 16(%rsp)
	movzbl	16(%rsp), %eax
	xorb	%al, (%rbx)
	movzbl	1(%rsp), %eax
	xorb	%al, 1(%rbx)
	movzbl	2(%rsp), %eax
	xorb	%al, 2(%rbx)
	movzbl	3(%rsp), %eax
	xorb	%al, 3(%rbx)
	movzbl	4(%rsp), %eax
	xorb	%al, 4(%rbx)
	movzbl	5(%rsp), %eax
	xorb	%al, 5(%rbx)
	movzbl	6(%rsp), %eax
	xorb	%al, 6(%rbx)
	movzbl	7(%rsp), %eax
	xorb	%al, 7(%rbx)
	movzbl	8(%rsp), %eax
	xorb	%al, 8(%rbx)
	movzbl	9(%rsp), %eax
	xorb	%al, 9(%rbx)
	movzbl	10(%rsp), %eax
	xorb	%al, 10(%rbx)
	movzbl	11(%rsp), %eax
	xorb	%al, 11(%rbx)
	movzbl	12(%rsp), %eax
	xorb	%al, 12(%rbx)
	movzbl	13(%rsp), %eax
	xorb	%al, 13(%rbx)
	movzbl	14(%rsp), %eax
	xorb	%al, 14(%rbx)
	movzbl	15(%rsp), %eax
	xorb	%al, 15(%rbx)
	addq	$16, %rbx
	decq	%r14
	jne	.LBB3_2
.LBB3_5:                                # %._crit_edge
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	AesCtr_Code, .Lfunc_end3-AesCtr_Code
	.cfi_endproc

	.globl	Aes_SetKey_Enc
	.p2align	4, 0x90
	.type	Aes_SetKey_Enc,@function
Aes_SetKey_Enc:                         # @Aes_SetKey_Enc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 40
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leal	28(%rdx), %r10d
	movl	%edx, %r8d
	shrl	$2, %r8d
	movl	%edx, %eax
	shrl	$3, %eax
	addl	$3, %eax
	movl	%eax, (%rdi)
	leaq	16(%rdi), %r9
	testl	%r8d, %r8d
	je	.LBB4_1
# BB#2:                                 # %.lr.ph57.preheader
	movl	%r8d, %r11d
	cmpl	$31, %edx
	jbe	.LBB4_3
# BB#11:                                # %min.iters.checked
	movl	%r8d, %r14d
	andl	$7, %r14d
	movq	%r11, %rcx
	subq	%r14, %rcx
	je	.LBB4_3
# BB#12:                                # %vector.memcheck
	leal	(,%r8,4), %eax
	leaq	(%rsi,%rax), %rbx
	cmpq	%rbx, %r9
	jae	.LBB4_14
# BB#13:                                # %vector.memcheck
	leaq	16(%rdi,%r11,4), %rbx
	cmpq	%rsi, %rbx
	jbe	.LBB4_14
.LBB4_3:
	xorl	%ecx, %ecx
	movq	%rsi, %rax
.LBB4_4:                                # %.lr.ph57.preheader76
	movl	%r11d, %ebx
	subl	%ecx, %ebx
	leaq	-1(%r11), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbx
	je	.LBB4_7
# BB#5:                                 # %.lr.ph57.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph57.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebp
	movl	%ebp, (%r9,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	incq	%rbx
	jne	.LBB4_6
.LBB4_7:                                # %.lr.ph57.prol.loopexit
	cmpq	$7, %rsi
	movl	%r8d, %esi
	jb	.LBB4_17
# BB#8:                                 # %.lr.ph57.preheader76.new
	subq	%rcx, %r11
	leaq	44(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph57
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	movl	%esi, -28(%rcx)
	movl	4(%rax), %esi
	movl	%esi, -24(%rcx)
	movl	8(%rax), %esi
	movl	%esi, -20(%rcx)
	movl	12(%rax), %esi
	movl	%esi, -16(%rcx)
	movl	16(%rax), %esi
	movl	%esi, -12(%rcx)
	movl	20(%rax), %esi
	movl	%esi, -8(%rcx)
	movl	24(%rax), %esi
	movl	%esi, -4(%rcx)
	movl	28(%rax), %esi
	movl	%esi, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %r11
	jne	.LBB4_9
# BB#10:
	movl	%r8d, %esi
	cmpl	%r10d, %esi
	jb	.LBB4_18
	jmp	.LBB4_30
.LBB4_1:
	xorl	%esi, %esi
.LBB4_17:                               # %.preheader
	cmpl	%r10d, %esi
	jae	.LBB4_30
.LBB4_18:                               # %.lr.ph
	movl	$4294967295, %r11d      # imm = 0xFFFFFFFF
	cmpl	$27, %edx
	movl	%esi, %ecx
	movl	%r10d, %edi
	movl	%r8d, %esi
	jbe	.LBB4_19
# BB#23:                                # %.lr.ph.split.us.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%r11), %eax
	movl	(%r9,%rax,4), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r8d
	andl	$1073741823, %edx       # imm = 0x3FFFFFFF
	je	.LBB4_27
# BB#25:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB4_24 Depth=1
	cmpl	$4, %edx
	jne	.LBB4_29
# BB#26:                                #   in Loop: Header=BB4_24 Depth=1
	movzbl	%bl, %eax
	movzbl	Sbox(%rax), %eax
	movzbl	%bh, %edx  # NOREX
	movzbl	Sbox(%rdx), %edx
	shll	$8, %edx
	orl	%eax, %edx
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %eax
	movzbl	Sbox(%rax), %eax
	shll	$16, %eax
	orl	%edx, %eax
	shrl	$24, %ebx
	jmp	.LBB4_28
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_24 Depth=1
	movzbl	%bh, %ebp  # NOREX
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movzbl	Rcon(%rax), %eax
	xorb	Sbox(%rbp), %al
	movzbl	%al, %eax
	movl	%ebx, %edx
	shrl	$16, %edx
	movzbl	%dl, %edx
	movzbl	Sbox(%rdx), %edx
	shll	$8, %edx
	orl	%eax, %edx
	movl	%ebx, %eax
	shrl	$24, %eax
	movzbl	Sbox(%rax), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movzbl	%bl, %ebx
.LBB4_28:                               # %.sink.split.us
                                        #   in Loop: Header=BB4_24 Depth=1
	movzbl	Sbox(%rbx), %ebx
	shll	$24, %ebx
	orl	%eax, %ebx
.LBB4_29:                               #   in Loop: Header=BB4_24 Depth=1
	leal	(%rsi,%rcx), %eax
	xorl	(%r9,%rax,4), %ebx
	movl	%ebx, (%r9,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB4_24
	jmp	.LBB4_30
.LBB4_19:                               # %.lr.ph.split.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB4_20:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%r11), %eax
	movl	(%r9,%rax,4), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r8d
	testl	%edx, %edx
	jne	.LBB4_22
# BB#21:                                # %.sink.split
                                        #   in Loop: Header=BB4_20 Depth=1
	movzbl	%bh, %ebp  # NOREX
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movzbl	Rcon(%rax), %eax
	xorb	Sbox(%rbp), %al
	movzbl	%al, %eax
	movl	%ebx, %edx
	movzbl	%bl, %ebp
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	movzbl	Sbox(%rbx), %ebx
	shll	$8, %ebx
	orl	%eax, %ebx
	shrl	$24, %edx
	movzbl	Sbox(%rdx), %eax
	shll	$16, %eax
	orl	%ebx, %eax
	movzbl	Sbox(%rbp), %ebx
	shll	$24, %ebx
	orl	%eax, %ebx
.LBB4_22:                               #   in Loop: Header=BB4_20 Depth=1
	leal	(%rsi,%rcx), %eax
	xorl	(%r9,%rax,4), %ebx
	movl	%ebx, (%r9,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB4_20
.LBB4_30:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_14:                               # %vector.body.preheader
	leal	(,%r14,4), %ebx
	subq	%rbx, %rax
	addq	%rsi, %rax
	addq	$16, %rsi
	leaq	32(%rdi), %rbx
	movq	%rcx, %r15
	.p2align	4, 0x90
.LBB4_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rsi
	addq	$32, %rbx
	addq	$-8, %r15
	jne	.LBB4_15
# BB#16:                                # %middle.block
	testl	%r14d, %r14d
	movl	%r8d, %esi
	jne	.LBB4_4
	jmp	.LBB4_17
.Lfunc_end4:
	.size	Aes_SetKey_Enc, .Lfunc_end4-Aes_SetKey_Enc
	.cfi_endproc

	.globl	Aes_SetKey_Dec
	.p2align	4, 0x90
	.type	Aes_SetKey_Dec,@function
Aes_SetKey_Dec:                         # @Aes_SetKey_Dec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	callq	Aes_SetKey_Enc
	addl	$20, %ebp
	je	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$32, %rbx
	movl	%ebp, %eax
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	movzbl	%cl, %edx
	movzbl	Sbox(%rdx), %edx
	movzbl	%ch, %esi  # NOREX
	movzbl	Sbox(%rsi), %esi
	movl	D+1024(,%rsi,4), %esi
	xorl	D(,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$16, %edx
	movzbl	%dl, %edx
	movzbl	Sbox(%rdx), %edx
	xorl	D+2048(,%rdx,4), %esi
	shrl	$24, %ecx
	movzbl	Sbox(%rcx), %ecx
	xorl	D+3072(,%rcx,4), %esi
	movl	%esi, (%rbx)
	addq	$4, %rbx
	decq	%rax
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Aes_SetKey_Dec, .Lfunc_end5-Aes_SetKey_Dec
	.cfi_endproc

	.globl	AesCbc_Init
	.p2align	4, 0x90
	.type	AesCbc_Init,@function
AesCbc_Init:                            # @AesCbc_Init
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	retq
.Lfunc_end6:
	.size	AesCbc_Init, .Lfunc_end6-AesCbc_Init
	.cfi_endproc

	.p2align	4, 0x90
	.type	Aes_Encode,@function
Aes_Encode:                             # @Aes_Encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	(%rdi), %eax
	movdqu	(%rdx), %xmm1
	movdqu	16(%rdi), %xmm0
	pxor	%xmm1, %xmm0
	leal	-1(%rax), %ecx
	shlq	$3, %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	leaq	48(%rdi), %r9
	movl	$1, %r8d
	subl	%eax, %r8d
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_2:                                #   in Loop: Header=BB7_1 Depth=1
	movl	%ecx, %r15d
	movzbl	%ch, %esi  # NOREX
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	movl	%ecx, %r14d
	movzbl	%cl, %r13d
	movzbl	%dl, %ecx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movl	%edx, %esi
	movl	%edx, %r11d
	movzbl	%dh, %ebp  # NOREX
	movl	%ebx, %edx
	movzbl	%bl, %r12d
	movzbl	%bh, %edi  # NOREX
	shrl	$24, %ebx
	movd	T+3072(,%rbx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movzbl	%ah, %ebx  # NOREX
	movl	%r8d, %r10d
	movl	%eax, %r8d
	movq	%r9, %rcx
	movzbl	%al, %r9d
	shrl	$16, %eax
	movzbl	%al, %eax
	movd	T+1024(,%rdi,4), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movd	T+1024(,%rbp,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movd	T+1024(,%rbx,4), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movd	T+1024(,%rdi,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movd	T(,%r9,4), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movq	%rcx, %r9
	movd	T(,%r13,4), %xmm6       # xmm6 = mem[0],zero,zero,zero
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movd	T(,%rcx,4), %xmm11      # xmm11 = mem[0],zero,zero,zero
	movd	T(,%r12,4), %xmm1       # xmm1 = mem[0],zero,zero,zero
	shrl	$16, %r14d
	movzbl	%r14b, %edi
	movd	T+2048(,%rdi,4), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movd	T+2048(,%rax,4), %xmm5  # xmm5 = mem[0],zero,zero,zero
	shrl	$16, %edx
	movzbl	%dl, %eax
	movd	T+2048(,%rax,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	shrl	$16, %esi
	movzbl	%sil, %eax
	movd	T+2048(,%rax,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	shrl	$24, %r11d
	movd	T+3072(,%r11,4), %xmm14 # xmm14 = mem[0],zero,zero,zero
	shrl	$24, %r15d
	movd	T+3072(,%r15,4), %xmm15 # xmm15 = mem[0],zero,zero,zero
	shrl	$24, %r8d
	movd	T+3072(,%r8,4), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movl	%r10d, %r8d
	punpckldq	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	punpckldq	%xmm9, %xmm4    # xmm4 = xmm4[0],xmm9[0],xmm4[1],xmm9[1]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm10, %xmm6   # xmm6 = xmm6[0],xmm10[0],xmm6[1],xmm10[1]
	punpckldq	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	pxor	%xmm4, %xmm1
	punpckldq	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1]
	punpckldq	%xmm13, %xmm3   # xmm3 = xmm3[0],xmm13[0],xmm3[1],xmm13[1]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	pxor	%xmm1, %xmm3
	punpckldq	%xmm14, %xmm0   # xmm0 = xmm0[0],xmm14[0],xmm0[1],xmm14[1]
	punpckldq	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	movdqu	(%r9), %xmm0
	pxor	%xmm7, %xmm0
	pxor	%xmm3, %xmm0
	addq	$32, %r9
	incl	%r8d
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %eax
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, %ecx
	movzbl	%ch, %ebp  # NOREX
	movd	%xmm0, %edx
	movl	%edx, %r14d
	pshufd	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movl	%ecx, %r15d
	movzbl	%cl, %r11d
	shrl	$16, %ecx
	movzbl	%cl, %r13d
	movd	%xmm0, %ebx
	movzbl	%dl, %r12d
	movzbl	%dh, %edi  # NOREX
	movzbl	%bl, %esi
	movzbl	%ah, %ecx  # NOREX
	movl	T+1024(,%rcx,4), %ecx
	xorl	T(,%rsi,4), %ecx
	xorl	T+2048(,%r13,4), %ecx
	shrl	$24, %edx
	xorl	T+3072(,%rdx,4), %ecx
	movzbl	%al, %r13d
	movl	%eax, %r10d
	movzbl	%bh, %eax  # NOREX
	movl	%ebx, %esi
                                        # kill: %EBX<def> %EBX<kill> %RBX<def>
	movl	T+1024(,%rbp,4), %edx
	xorl	T(,%r13,4), %edx
	shrl	$16, %r14d
	movzbl	%r14b, %ebp
	xorl	T+2048(,%rbp,4), %edx
	shrl	$24, %ebx
	xorl	T+3072(,%rbx,4), %edx
	movl	%r10d, %ebp
	movl	%ebp, %r14d
	movl	T+1024(,%rax,4), %ebx
	xorl	T(,%r12,4), %ebx
	shrl	$16, %ebp
	movzbl	%bpl, %eax
	xorl	T+2048(,%rax,4), %ebx
	xorl	-8(%r9), %edx
	shrl	$24, %r15d
	xorl	T+3072(,%r15,4), %ebx
	xorl	-16(%r9), %ebx
	movl	T+1024(,%rdi,4), %eax
	xorl	T(,%r11,4), %eax
	shrl	$16, %esi
	movzbl	%sil, %esi
	xorl	T+2048(,%rsi,4), %eax
	shrl	$24, %r14d
	xorl	T+3072(,%r14,4), %eax
	xorl	-4(%r9), %eax
	xorl	-12(%r9), %ecx
	testl	%r8d, %r8d
	jne	.LBB7_2
# BB#3:
	movl	%eax, %r9d
	movl	%eax, %r8d
	movzbl	%ah, %esi  # NOREX
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	movzbl	%al, %r13d
	movzbl	%bl, %edi
	movl	%ebx, %eax
	movl	%ebx, %r15d
	movzbl	%bh, %esi  # NOREX
	movq	%rsi, %r10
	movzbl	Sbox(%rdi), %edi
	movzbl	%ch, %esi  # NOREX
	movzbl	Sbox(%rsi), %esi
	shll	$8, %esi
	orl	%edi, %esi
	movl	%edx, %edi
	shrl	$16, %edi
	movzbl	%dil, %edi
	movzbl	Sbox(%rdi), %edi
	shll	$16, %edi
	orl	%esi, %edi
	movzbl	%cl, %r11d
	movl	%ecx, %r12d
	shrl	$16, %ecx
	movzbl	%cl, %r14d
	shrl	$24, %r9d
	movzbl	Sbox(%r9), %ebx
	shll	$24, %ebx
	orl	%edi, %ebx
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	-32(%rsp), %r9          # 8-byte Reload
	xorl	48(%rsi,%r9,4), %ebx
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movl	%ebx, (%rbp)
	movzbl	%dh, %edi  # NOREX
	movzbl	%dl, %ebx
	shrl	$24, %edx
	movzbl	Sbox(%rdx), %edx
	movzbl	Sbox(%r11), %ecx
	movzbl	Sbox(%rdi), %edi
	shll	$8, %edi
	orl	%ecx, %edi
	shrl	$16, %r8d
	movzbl	%r8b, %ecx
	movzbl	Sbox(%rcx), %ecx
	shll	$16, %ecx
	orl	%edi, %ecx
	shrl	$24, %eax
	movzbl	Sbox(%rax), %edi
	shll	$24, %edi
	orl	%ecx, %edi
	xorl	52(%rsi,%r9,4), %edi
	movl	%edi, 4(%rbp)
	movzbl	Sbox(%rbx), %ecx
	movq	-40(%rsp), %rax         # 8-byte Reload
	movzbl	Sbox(%rax), %edi
	shll	$8, %edi
	orl	%ecx, %edi
	shrl	$16, %r15d
	movzbl	%r15b, %eax
	movzbl	Sbox(%rax), %eax
	shll	$16, %eax
	orl	%edi, %eax
	shrl	$24, %r12d
	movzbl	Sbox(%r12), %ecx
	shll	$24, %ecx
	orl	%eax, %ecx
	xorl	56(%rsi,%r9,4), %ecx
	movl	%ecx, 8(%rbp)
	movzbl	Sbox(%r13), %eax
	movzbl	Sbox(%r10), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	Sbox(%r14), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	shll	$24, %edx
	orl	%eax, %edx
	xorl	60(%rsi,%r9,4), %edx
	movl	%edx, 12(%rbp)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Aes_Encode, .Lfunc_end7-Aes_Encode
	.cfi_endproc

	.type	InvS,@object            # @InvS
	.local	InvS
	.comm	InvS,256,16
	.type	Sbox,@object            # @Sbox
	.section	.rodata,"a",@progbits
	.p2align	4
Sbox:
	.ascii	"c|w{\362ko\3050\001g+\376\327\253v\312\202\311}\372YG\360\255\324\242\257\234\244r\300\267\375\223&6?\367\3144\245\345\361q\3301\025\004\307#\303\030\226\005\232\007\022\200\342\353'\262u\t\203,\032\033nZ\240R;\326\263)\343/\204S\321\000\355 \374\261[j\313\2769JLX\317\320\357\252\373CM3\205E\371\002\177P<\237\250Q\243@\217\222\2358\365\274\266\332!\020\377\363\322\315\f\023\354_\227D\027\304\247~=d]\031s`\201O\334\"*\220\210F\356\270\024\336^\013\333\3402:\nI\006$\\\302\323\254b\221\225\344y\347\3107m\215\325N\251lV\364\352ez\256\b\272x%.\034\246\264\306\350\335t\037K\275\213\212p>\265fH\003\366\016a5W\271\206\301\035\236\341\370\230\021i\331\216\224\233\036\207\351\316U(\337\214\241\211\r\277\346BhA\231-\017\260T\273\026"
	.size	Sbox, 256

	.type	T,@object               # @T
	.local	T
	.comm	T,4096,16
	.type	D,@object               # @D
	.local	D
	.comm	D,4096,16
	.type	g_AesCbc_Encode,@object # @g_AesCbc_Encode
	.comm	g_AesCbc_Encode,8,8
	.type	g_AesCbc_Decode,@object # @g_AesCbc_Decode
	.comm	g_AesCbc_Decode,8,8
	.type	g_AesCtr_Code,@object   # @g_AesCtr_Code
	.comm	g_AesCtr_Code,8,8
	.type	Rcon,@object            # @Rcon
Rcon:
	.ascii	"\000\001\002\004\b\020 @\200\0336"
	.size	Rcon, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
