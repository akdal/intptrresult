	.text
	.file	"rna.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1120403456              # float 100
.LCPI0_1:
	.long	0                       # float 0
	.text
	.globl	rnaalifoldcall
	.p2align	4, 0x90
	.type	rnaalifoldcall,@function
rnaalifoldcall:                         # @rnaalifoldcall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi6:
	.cfi_def_cfa_offset 1104
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	(%r15), %rdi
	callq	strlen
	movq	%rax, %rbp
	cmpq	$0, rnaalifoldcall.order(%rip)
	je	.LBB0_1
.LBB0_5:                                # %.loopexit
	movslq	%ebp, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, rnaalifoldcall.pairnum(%rip)
	testl	%ebp, %ebp
	jle	.LBB0_7
# BB#6:                                 # %.lr.ph50
	movabsq	$17179869180, %rcx      # imm = 0x3FFFFFFFC
	leaq	(%rcx,%rbp,4), %rdx
	andq	%rcx, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB0_7:                                # %._crit_edge51
	movl	$rnaalifoldcall.fnamein, %edi
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_29
# BB#8:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	rnaalifoldcall.name(%rip), %r8
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%ebp, %edx
	movq	%r15, %rcx
	pushq	rnaalifoldcall.order(%rip)
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	clustalout_pointer
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rbx, %rdi
	callq	fclose
	movl	$rnaalifoldcall.cmd, %edi
	movl	$.L.str.4, %esi
	movl	$rnaalifoldcall.fnamein, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$rnaalifoldcall.cmd, %edi
	callq	system
	movl	$.L.str.5, %edi
	movl	$.L.str.6, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_29
# BB#9:
	leaq	48(%rsp), %r13
	leaq	16(%rsp), %rbx
	leaq	12(%rsp), %rbp
	leaq	44(%rsp), %r15
	leaq	20(%rsp), %r12
	jmp	.LBB0_10
.LBB0_17:                               #   in Loop: Header=BB0_10 Depth=1
	movq	%rbp, %rbx
	movq	%r15, %rbp
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	-8(%r15,%rax,8), %rdi
	movq	rnaalifoldcall.pairnum(%rip), %rcx
	movslq	-4(%rcx,%rax,4), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rsi
	callq	realloc
	movslq	16(%rsp), %rcx
	movq	%rax, (%r15,%rcx,8)
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movss	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm1
	movq	rnaalifoldcall.pairnum(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movss	%xmm1, 20(%rax,%rdx,8)
	movl	12(%rsp), %edi
	movl	%edi, 16(%rax,%rdx,8)
	incl	(%rsi,%rcx,4)
	cvtss2sd	%xmm0, %xmm0
	movslq	16(%rsp), %rdx
	movq	(%r15,%rdx,8), %rax
	movslq	(%rsi,%rdx,4), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movabsq	$-4647714811151384577, %rsi # imm = 0xBF800000FFFFFFFF
	movq	%rsi, 16(%rax,%rcx,8)
	movq	stderr(%rip), %rdi
	movl	12(%rsp), %ecx
	movl	$.L.str.8, %esi
	movb	$1, %al
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movslq	12(%rsp), %rax
	movq	(%r15,%rax,8), %rdi
	movq	rnaalifoldcall.pairnum(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rsi
	callq	realloc
	movslq	12(%rsp), %rcx
	movq	%rax, (%r15,%rcx,8)
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	divss	.LCPI0_0(%rip), %xmm1
	movq	rnaalifoldcall.pairnum(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movss	%xmm1, 20(%rax,%rsi,8)
	movl	16(%rsp), %edi
	movl	%edi, 16(%rax,%rsi,8)
	incl	(%rdx,%rcx,4)
	cvtss2sd	%xmm0, %xmm0
	movslq	12(%rsp), %rcx
	movq	(%r15,%rcx,8), %rax
	movq	%rbp, %r15
	movq	%rbx, %rbp
	leaq	16(%rsp), %rbx
	movslq	(%rdx,%rcx,4), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movabsq	$-4647714811151384577, %rsi # imm = 0xBF800000FFFFFFFF
	movq	%rsi, 16(%rax,%rdx,8)
	movq	stderr(%rip), %rdi
	movl	16(%rsp), %edx
	movl	$.L.str.8, %esi
	movb	$1, %al
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	movzbl	48(%rsp), %eax
	movl	%eax, %ecx
	addb	$-32, %cl
	cmpb	$14, %cl
	ja	.LBB0_11
# BB#15:                                # %.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	movzbl	%cl, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_16:                               #   in Loop: Header=BB0_10 Depth=1
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movq	%r15, %r8
	movq	%r12, %r9
	callq	sscanf
	movslq	16(%rsp), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rsp)
	decl	12(%rsp)
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI0_1, %xmm0
	jbe	.LBB0_10
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$123, %al
	jne	.LBB0_10
.LBB0_12:
	movq	%r14, %rdi
	callq	fclose
	movl	$rnaalifoldcall.cmd, %edi
	movl	$.L.str.9, %esi
	movl	$rnaalifoldcall.fnamein, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$rnaalifoldcall.cmd, %edi
	callq	system
	movq	32(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB0_28
# BB#13:                                # %.lr.ph.preheader
	movl	%ecx, %eax
	testb	$1, %cl
	jne	.LBB0_18
# BB#14:
	xorl	%ecx, %ecx
                                        # implicit-def: %EDX
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpq	$1, %rax
	jne	.LBB0_22
	jmp	.LBB0_27
.LBB0_1:
	callq	getpid
	movl	%eax, %ecx
	movl	$rnaalifoldcall.fnamein, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	movl	njob(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, rnaalifoldcall.order(%rip)
	movl	njob(%rip), %edi
	movl	$10, %esi
	callq	AllocateCharMtx
	movq	%rax, rnaalifoldcall.name(%rip)
	cmpl	$0, njob(%rip)
	jle	.LBB0_5
# BB#2:                                 # %.lr.ph54.preheader
	movq	rnaalifoldcall.order(%rip), %rcx
	movl	$0, (%rcx)
	movq	(%rax), %rdi
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	cmpl	$2, njob(%rip)
	jl	.LBB0_5
# BB#3:                                 # %.lr.ph54..lr.ph54_crit_edge.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph54..lr.ph54_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	rnaalifoldcall.name(%rip), %rax
	movq	rnaalifoldcall.order(%rip), %rcx
	movl	%ebx, (%rcx,%rbx,4)
	movq	(%rax,%rbx,8), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	sprintf
	incq	%rbx
	movslq	njob(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_4
	jmp	.LBB0_5
.LBB0_18:                               # %.lr.ph.prol
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rcx
	movslq	16(%rcx), %rdx
	testq	%rdx, %rdx
	js	.LBB0_20
# BB#19:
	movq	(%rbx,%rdx,8), %rsi
	movl	$0, 16(%rsi)
	movl	20(%rcx), %ecx
	movl	%ecx, 20(%rsi)
.LBB0_20:                               # %.lr.ph.prol.loopexit
	movl	$1, %ecx
	cmpq	$1, %rax
	je	.LBB0_27
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rcx,8), %rdx
	movslq	16(%rdx), %rsi
	testq	%rsi, %rsi
	js	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_22 Depth=1
	movq	(%rbx,%rsi,8), %rsi
	movl	%ecx, 16(%rsi)
	movl	20(%rdx), %edx
	movl	%edx, 20(%rsi)
.LBB0_24:                               # %.lr.ph.162
                                        #   in Loop: Header=BB0_22 Depth=1
	movq	8(%rbx,%rcx,8), %rsi
	movslq	16(%rsi), %rdx
	testq	%rdx, %rdx
	js	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_22 Depth=1
	movq	(%rbx,%rdx,8), %rdi
	leal	1(%rcx), %ebp
	movl	%ebp, 16(%rdi)
	movl	20(%rsi), %esi
	movl	%esi, 20(%rdi)
.LBB0_26:                               #   in Loop: Header=BB0_22 Depth=1
	addq	$2, %rcx
	cmpq	%rcx, %rax
	jne	.LBB0_22
.LBB0_27:                               # %._crit_edge
	movl	%edx, 12(%rsp)
.LBB0_28:
	movq	rnaalifoldcall.pairnum(%rip), %rdi
	callq	free
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_29:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	rnaalifoldcall, .Lfunc_end0-rnaalifoldcall
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_16
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_10
	.quad	.LBB0_12

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1142292480              # float 600
	.text
	.globl	foldrna
	.p2align	4, 0x90
	.type	foldrna,@function
foldrna:                                # @foldrna
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 224
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%esi, %r13d
	movl	%edi, %r15d
	movq	240(%rsp), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	224(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	(%rdx), %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rbp
	leal	10(%r14), %r12d
	movl	%r15d, %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.useq1(%rip)
	leal	10(%rbp), %ebx
	movl	%r13d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.useq2(%rip)
	movl	%r15d, %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.oseq1(%rip)
	movl	%r13d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.oseq2(%rip)
	movl	%r15d, %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.oseq1r(%rip)
	movl	%r13d, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, foldrna.oseq2r(%rip)
	movl	%r12d, %edi
	callq	AllocateCharVec
	movq	%rax, foldrna.odir1(%rip)
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, foldrna.odir2(%rip)
	leal	1(%r14), %esi
	movl	%r15d, %edi
	callq	AllocateIntMtx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leal	1(%rbp), %esi
	movl	%r13d, %edi
	callq	AllocateIntMtx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	%ebp, %r14d
	movl	%ebp, %edi
	cmovgel	%r14d, %edi
	callq	AllocateCharVec
	movslq	%r14d, %r12
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, foldrna.pairprob1(%rip)
	movq	%rbp, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	%ebp, %rbx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, foldrna.pairprob2(%rip)
	movl	%r12d, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, foldrna.map(%rip)
	movl	%r12d, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, foldrna.impmtx2(%rip)
	movl	%r12d, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	movq	32(%rsp), %r15          # 8-byte Reload
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph435.preheader
	movl	4(%rsp), %ebx           # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph435
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.useq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r15,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_2
.LBB1_3:                                # %.preheader358
	testl	%r13d, %r13d
	movq	96(%rsp), %r12          # 8-byte Reload
	jle	.LBB1_6
# BB#4:                                 # %.lr.ph432.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph432
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.useq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r12,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_5
.LBB1_6:                                # %.preheader357
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_9
# BB#7:                                 # %.lr.ph430.preheader
	movl	4(%rsp), %ebx           # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph430
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.oseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r15,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_8
.LBB1_9:                                # %.preheader356
	testl	%r13d, %r13d
	jle	.LBB1_12
# BB#10:                                # %.lr.ph428.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph428
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.oseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%r12,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_11
.LBB1_12:                               # %.preheader355
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	jle	.LBB1_15
# BB#13:                                # %.lr.ph426.preheader
	movl	4(%rsp), %ebx           # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph426
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.useq1(%rip), %rsi
	addq	%rbp, %rsi
	movq	(%r15,%rbp), %rdx
	movl	$1, %edi
	callq	commongappick_record
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB1_14
.LBB1_15:                               # %.preheader354
	testl	%r13d, %r13d
	movq	104(%rsp), %r15         # 8-byte Reload
	jle	.LBB1_18
# BB#16:                                # %.lr.ph424.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph424
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.useq2(%rip), %rsi
	addq	%rbp, %rsi
	movq	(%r15,%rbp), %rdx
	movl	$1, %edi
	callq	commongappick_record
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB1_17
.LBB1_18:                               # %.preheader353
	movq	%r14, 16(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB1_21
# BB#19:                                # %.lr.ph422
	movq	foldrna.pairprob1(%rip), %rbx
	movl	16(%rsp), %ebp          # 4-byte Reload
	movabsq	$-4647714811151384577, %r14 # imm = 0xBF800000FFFFFFFF
	.p2align	4, 0x90
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$24, %esi
	callq	calloc
	movq	%rax, (%rbx)
	movq	%r14, 16(%rax)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB1_20
.LBB1_21:                               # %.preheader352
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_24
# BB#22:                                # %.lr.ph419
	movq	foldrna.pairprob2(%rip), %rbx
	movl	8(%rsp), %ebp           # 4-byte Reload
	movabsq	$-4647714811151384577, %r14 # imm = 0xBF800000FFFFFFFF
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$24, %esi
	callq	calloc
	movq	%rax, (%rbx)
	movq	%r14, 16(%rax)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB1_23
.LBB1_24:                               # %._crit_edge420
	movq	foldrna.oseq1(%rip), %rdi
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_48
# BB#25:                                # %._crit_edge420
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_48
# BB#26:                                # %.preheader.us.preheader.i
	movl	4(%rsp), %r9d           # 4-byte Reload
	movl	16(%rsp), %r10d         # 4-byte Reload
	movl	%r9d, %r11d
	andl	$1, %r11d
	leaq	8(%rdi), %r8
	xorl	%r14d, %r14d
	jmp	.LBB1_29
.LBB1_27:                               #   in Loop: Header=BB1_29 Depth=1
	cmpb	$45, %bl
	je	.LBB1_33
# BB#28:                                #   in Loop: Header=BB1_29 Depth=1
	movb	$110, %bl
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_29:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_40 Depth 2
	testq	%r11, %r11
	jne	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB1_47
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_29 Depth=1
	movq	(%rdi), %rax
	movb	(%rax,%r14), %bl
	movl	%ebx, %edx
	addb	$-97, %dl
	cmpb	$20, %dl
	ja	.LBB1_27
# BB#32:                                #   in Loop: Header=BB1_29 Depth=1
	movb	$116, %cl
	movzbl	%dl, %edx
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_33:                               #   in Loop: Header=BB1_29 Depth=1
	movl	%ebx, %ecx
.LBB1_34:                               #   in Loop: Header=BB1_29 Depth=1
	movb	%cl, (%rax,%r14)
	movl	$1, %ecx
	cmpl	$1, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB1_47
.LBB1_35:                               # %.preheader.us.i.new
                                        #   in Loop: Header=BB1_29 Depth=1
	movq	%r9, %rax
	subq	%rcx, %rax
	leaq	(%r8,%rcx,8), %rsi
	jmp	.LBB1_40
.LBB1_36:                               #   in Loop: Header=BB1_40 Depth=2
	cmpb	$45, %bl
	je	.LBB1_42
# BB#37:                                #   in Loop: Header=BB1_40 Depth=2
	movb	$110, %bl
	jmp	.LBB1_42
.LBB1_38:                               #   in Loop: Header=BB1_40 Depth=2
	cmpb	$45, %bl
	je	.LBB1_45
# BB#39:                                #   in Loop: Header=BB1_40 Depth=2
	movb	$110, %bl
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_40:                               #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi), %rbp
	movzbl	(%rbp,%r14), %ebx
	movl	%ebx, %edx
	addb	$-97, %dl
	cmpb	$20, %dl
	ja	.LBB1_36
# BB#41:                                #   in Loop: Header=BB1_40 Depth=2
	movb	$116, %cl
	movzbl	%dl, %edx
	jmpq	*.LJTI1_1(,%rdx,8)
.LBB1_42:                               #   in Loop: Header=BB1_40 Depth=2
	movl	%ebx, %ecx
.LBB1_43:                               #   in Loop: Header=BB1_40 Depth=2
	movb	%cl, (%rbp,%r14)
	movq	(%rsi), %rbp
	movzbl	(%rbp,%r14), %ebx
	movl	%ebx, %ecx
	addb	$-97, %cl
	cmpb	$20, %cl
	ja	.LBB1_38
# BB#44:                                #   in Loop: Header=BB1_40 Depth=2
	movb	$116, %dl
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_2(,%rcx,8)
.LBB1_45:                               #   in Loop: Header=BB1_40 Depth=2
	movl	%ebx, %edx
.LBB1_46:                               #   in Loop: Header=BB1_40 Depth=2
	movb	%dl, (%rbp,%r14)
	addq	$16, %rsi
	addq	$-2, %rax
	jne	.LBB1_40
.LBB1_47:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_29 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB1_29
.LBB1_48:                               # %utot.exit
	testl	%r13d, %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	jle	.LBB1_72
# BB#49:                                # %utot.exit
	testl	%eax, %eax
	jle	.LBB1_72
# BB#50:                                # %.preheader.us.preheader.i322
	movq	foldrna.oseq2(%rip), %r9
	movl	%r13d, %r10d
	movl	%eax, %r11d
	movl	%r10d, %r14d
	andl	$1, %r14d
	leaq	8(%r9), %r8
	xorl	%r15d, %r15d
	jmp	.LBB1_53
.LBB1_51:                               #   in Loop: Header=BB1_53 Depth=1
	cmpb	$45, %cl
	je	.LBB1_57
# BB#52:                                #   in Loop: Header=BB1_53 Depth=1
	movb	$110, %cl
	jmp	.LBB1_57
	.p2align	4, 0x90
.LBB1_53:                               # %.preheader.us.i324
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_64 Depth 2
	testq	%r14, %r14
	jne	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_53 Depth=1
	xorl	%eax, %eax
	cmpl	$1, %r13d
	je	.LBB1_71
	jmp	.LBB1_59
	.p2align	4, 0x90
.LBB1_55:                               #   in Loop: Header=BB1_53 Depth=1
	movq	(%r9), %rax
	movb	(%rax,%r15), %cl
	movl	%ecx, %ebx
	addb	$-97, %bl
	cmpb	$20, %bl
	ja	.LBB1_51
# BB#56:                                #   in Loop: Header=BB1_53 Depth=1
	movb	$116, %dl
	movzbl	%bl, %esi
	jmpq	*.LJTI1_3(,%rsi,8)
.LBB1_57:                               #   in Loop: Header=BB1_53 Depth=1
	movl	%ecx, %edx
.LBB1_58:                               #   in Loop: Header=BB1_53 Depth=1
	movb	%dl, (%rax,%r15)
	movl	$1, %eax
	cmpl	$1, %r13d
	je	.LBB1_71
.LBB1_59:                               # %.preheader.us.i324.new
                                        #   in Loop: Header=BB1_53 Depth=1
	movq	%r10, %rsi
	subq	%rax, %rsi
	leaq	(%r8,%rax,8), %rax
	jmp	.LBB1_64
.LBB1_60:                               #   in Loop: Header=BB1_64 Depth=2
	cmpb	$45, %cl
	je	.LBB1_66
# BB#61:                                #   in Loop: Header=BB1_64 Depth=2
	movb	$110, %cl
	jmp	.LBB1_66
.LBB1_62:                               #   in Loop: Header=BB1_64 Depth=2
	cmpb	$45, %dl
	je	.LBB1_69
# BB#63:                                #   in Loop: Header=BB1_64 Depth=2
	movb	$110, %dl
	jmp	.LBB1_69
	.p2align	4, 0x90
.LBB1_64:                               #   Parent Loop BB1_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rax), %rbp
	movzbl	(%rbp,%r15), %ecx
	movl	%ecx, %ebx
	addb	$-97, %bl
	cmpb	$20, %bl
	ja	.LBB1_60
# BB#65:                                #   in Loop: Header=BB1_64 Depth=2
	movb	$116, %dl
	movzbl	%bl, %ebx
	jmpq	*.LJTI1_4(,%rbx,8)
.LBB1_66:                               #   in Loop: Header=BB1_64 Depth=2
	movl	%ecx, %edx
.LBB1_67:                               #   in Loop: Header=BB1_64 Depth=2
	movb	%dl, (%rbp,%r15)
	movq	(%rax), %rbp
	movzbl	(%rbp,%r15), %edx
	movl	%edx, %ecx
	addb	$-97, %cl
	cmpb	$20, %cl
	ja	.LBB1_62
# BB#68:                                #   in Loop: Header=BB1_64 Depth=2
	movb	$116, %bl
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_5(,%rcx,8)
.LBB1_69:                               #   in Loop: Header=BB1_64 Depth=2
	movl	%edx, %ebx
.LBB1_70:                               #   in Loop: Header=BB1_64 Depth=2
	movb	%bl, (%rbp,%r15)
	addq	$16, %rax
	addq	$-2, %rsi
	jne	.LBB1_64
.LBB1_71:                               # %._crit_edge.us.i332
                                        #   in Loop: Header=BB1_53 Depth=1
	incq	%r15
	cmpq	%r11, %r15
	jne	.LBB1_53
.LBB1_72:                               # %utot.exit333
	cmpb	$114, rnaprediction(%rip)
	jne	.LBB1_74
# BB#73:
	movq	foldrna.pairprob1(%rip), %rdx
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	rnaalifoldcall
	jmp	.LBB1_75
.LBB1_74:
	movq	foldrna.useq1(%rip), %rsi
	movq	foldrna.pairprob1(%rip), %rcx
	movq	(%rdi), %rdi
	subq	$8, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	mccaskillextract
	addq	$16, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -16
.LBB1_75:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	104(%rsp), %r9          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	foldrna.oseq2(%rip), %rdi
	cmpb	$114, rnaprediction(%rip)
	jne	.LBB1_77
# BB#76:
	movq	foldrna.pairprob2(%rip), %rdx
	movl	%r13d, %esi
	callq	rnaalifoldcall
	jmp	.LBB1_78
.LBB1_77:
	movq	foldrna.useq2(%rip), %rsi
	movq	foldrna.pairprob2(%rip), %rcx
	movq	(%rdi), %rdi
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movl	%r13d, %edx
	movq	32(%rsp), %r8           # 8-byte Reload
	pushq	%rbp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	mccaskillextract
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
.LBB1_78:
	movq	foldrna.oseq1(%rip), %rdi
	movq	foldrna.oseq2(%rip), %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	%r13d, %r9d
	pushq	foldrna.map(%rip)
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$10000                  # imm = 0x2710
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	Lalignmm_hmout
	addq	$48, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -48
	movb	RNAscoremtx(%rip), %al
	cmpb	$110, %al
	je	.LBB1_171
# BB#79:
	cmpb	$114, %al
	jne	.LBB1_178
# BB#80:                                # %.preheader351
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_220
# BB#81:                                # %.preheader350.lr.ph
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_101
# BB#82:                                # %.preheader350.us.preheader
	movq	foldrna.impmtx2(%rip), %r11
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	movl	%ecx, %eax
	movl	16(%rsp), %r14d         # 4-byte Reload
	leaq	-1(%rax), %r10
	movl	%ecx, %r8d
	andl	$7, %r8d
	movq	%rax, %r9
	subq	%r8, %r9
	xorl	%ebx, %ebx
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_83:                               # %.preheader350.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_91 Depth 2
                                        #     Child Loop BB1_95 Depth 2
                                        #     Child Loop BB1_98 Depth 2
	cmpq	$8, %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rbx,8), %rdi
	movq	(%r11,%rbx,8), %rbp
	jae	.LBB1_85
# BB#84:                                #   in Loop: Header=BB1_83 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_93
	.p2align	4, 0x90
.LBB1_85:                               # %min.iters.checked
                                        #   in Loop: Header=BB1_83 Depth=1
	testq	%r9, %r9
	je	.LBB1_89
# BB#86:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_83 Depth=1
	leaq	(%rbp,%rax,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB1_90
# BB#87:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_83 Depth=1
	leaq	(%rdi,%rax,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB1_90
.LBB1_89:                               #   in Loop: Header=BB1_83 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_93
.LBB1_90:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_83 Depth=1
	leaq	16(%rdi), %rcx
	leaq	16(%rbp), %rdx
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB1_91:                               # %vector.body
                                        #   Parent Loop BB1_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	movups	%xmm1, -16(%rdx)
	movups	%xmm1, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB1_91
# BB#92:                                # %middle.block
                                        #   in Loop: Header=BB1_83 Depth=1
	testq	%r8, %r8
	movq	%r9, %rsi
	je	.LBB1_99
	.p2align	4, 0x90
.LBB1_93:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_83 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%esi, %edx
	movq	%r10, %rcx
	subq	%rsi, %rcx
	andq	$7, %rdx
	je	.LBB1_96
# BB#94:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB1_83 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB1_95:                               # %scalar.ph.prol
                                        #   Parent Loop BB1_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1065353216, (%rdi,%rsi,4) # imm = 0x3F800000
	movl	$0, (%rbp,%rsi,4)
	incq	%rsi
	incq	%rdx
	jne	.LBB1_95
.LBB1_96:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_83 Depth=1
	cmpq	$7, %rcx
	jb	.LBB1_99
# BB#97:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB1_83 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	28(%rbp,%rsi,4), %rbp
	leaq	28(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_98:                               # %scalar.ph
                                        #   Parent Loop BB1_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1065353216, -28(%rsi)  # imm = 0x3F800000
	movl	$0, -28(%rbp)
	movl	$1065353216, -24(%rsi)  # imm = 0x3F800000
	movl	$0, -24(%rbp)
	movl	$1065353216, -20(%rsi)  # imm = 0x3F800000
	movl	$0, -20(%rbp)
	movl	$1065353216, -16(%rsi)  # imm = 0x3F800000
	movl	$0, -16(%rbp)
	movl	$1065353216, -12(%rsi)  # imm = 0x3F800000
	movl	$0, -12(%rbp)
	movl	$1065353216, -8(%rsi)   # imm = 0x3F800000
	movl	$0, -8(%rbp)
	movl	$1065353216, -4(%rsi)   # imm = 0x3F800000
	movl	$0, -4(%rbp)
	movl	$1065353216, (%rsi)     # imm = 0x3F800000
	movl	$0, (%rbp)
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rcx
	jne	.LBB1_98
.LBB1_99:                               # %._crit_edge416.us
                                        #   in Loop: Header=BB1_83 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB1_83
# BB#100:                               # %.preheader349
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_220
.LBB1_101:                              # %.lr.ph411
	movq	foldrna.pairprob1(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	foldrna.pairprob2(%rip), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	foldrna.oseq1(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	foldrna.oseq2(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	4(%rsp), %eax           # 4-byte Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	foldrna.impmtx2(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movl	$36, %r10d
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	%r13d, %r14d
	.p2align	4, 0x90
.LBB1_102:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_103 Depth 2
                                        #       Child Loop BB1_105 Depth 3
                                        #         Child Loop BB1_107 Depth 4
                                        #           Child Loop BB1_134 Depth 5
                                        #             Child Loop BB1_146 Depth 6
                                        #             Child Loop BB1_143 Depth 6
                                        #             Child Loop BB1_140 Depth 6
                                        #             Child Loop BB1_137 Depth 6
                                        #           Child Loop BB1_112 Depth 5
                                        #             Child Loop BB1_124 Depth 6
                                        #             Child Loop BB1_121 Depth 6
                                        #             Child Loop BB1_118 Depth 6
                                        #             Child Loop BB1_115 Depth 6
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	cmpl	$-1, 16(%rcx)
	je	.LBB1_159
	.p2align	4, 0x90
.LBB1_103:                              # %.preheader348
                                        #   Parent Loop BB1_102 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_105 Depth 3
                                        #         Child Loop BB1_107 Depth 4
                                        #           Child Loop BB1_134 Depth 5
                                        #             Child Loop BB1_146 Depth 6
                                        #             Child Loop BB1_143 Depth 6
                                        #             Child Loop BB1_140 Depth 6
                                        #             Child Loop BB1_137 Depth 6
                                        #           Child Loop BB1_112 Depth 5
                                        #             Child Loop BB1_124 Depth 6
                                        #             Child Loop BB1_121 Depth 6
                                        #             Child Loop BB1_118 Depth 6
                                        #             Child Loop BB1_115 Depth 6
	testl	%eax, %eax
	jle	.LBB1_158
# BB#104:                               # %.lr.ph404
                                        #   in Loop: Header=BB1_103 Depth=2
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB1_105:                              #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_107 Depth 4
                                        #           Child Loop BB1_134 Depth 5
                                        #             Child Loop BB1_146 Depth 6
                                        #             Child Loop BB1_143 Depth 6
                                        #             Child Loop BB1_140 Depth 6
                                        #             Child Loop BB1_137 Depth 6
                                        #           Child Loop BB1_112 Depth 5
                                        #             Child Loop BB1_124 Depth 6
                                        #             Child Loop BB1_121 Depth 6
                                        #             Child Loop BB1_118 Depth 6
                                        #             Child Loop BB1_115 Depth 6
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r9,8), %r11
	movl	16(%r11), %eax
	cmpl	$-1, %eax
	je	.LBB1_156
# BB#106:                               # %.lr.ph399.preheader
                                        #   in Loop: Header=BB1_105 Depth=3
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	16(%rcx), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_107:                              # %.lr.ph399
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_134 Depth 5
                                        #             Child Loop BB1_146 Depth 6
                                        #             Child Loop BB1_143 Depth 6
                                        #             Child Loop BB1_140 Depth 6
                                        #             Child Loop BB1_137 Depth 6
                                        #           Child Loop BB1_112 Depth 5
                                        #             Child Loop BB1_124 Depth 6
                                        #             Child Loop BB1_121 Depth 6
                                        #             Child Loop BB1_118 Depth 6
                                        #             Child Loop BB1_115 Depth 6
	movq	80(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	orl	%eax, %ecx
	js	.LBB1_155
# BB#108:                               #   in Loop: Header=BB1_107 Depth=4
	movq	64(%rsp), %rcx          # 8-byte Reload
	movss	20(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	20(%r11), %xmm0
	movslq	%eax, %r15
	cmpq	%r15, %r9
	jle	.LBB1_130
# BB#109:                               #   in Loop: Header=BB1_107 Depth=4
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jle	.LBB1_130
# BB#110:                               #   in Loop: Header=BB1_107 Depth=4
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_153
# BB#111:                               # %.preheader.i286.preheader
                                        #   in Loop: Header=BB1_107 Depth=4
	xorps	%xmm1, %xmm1
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_112:                              # %.preheader.i286
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB1_124 Depth 6
                                        #             Child Loop BB1_121 Depth 6
                                        #             Child Loop BB1_118 Depth 6
                                        #             Child Loop BB1_115 Depth 6
	testl	%r13d, %r13d
	jle	.LBB1_129
# BB#113:                               # %.lr.ph.i287
                                        #   in Loop: Header=BB1_112 Depth=5
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rax,%rcx), %rcx
	movslq	amino_n(,%rcx,4), %rbp
	cmpq	$4, %rbp
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r8,8), %xmm2     # xmm2 = mem[0],zero
	jl	.LBB1_116
# BB#114:                               # %.lr.ph.split.us.i294.preheader
                                        #   in Loop: Header=BB1_112 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_115:                              # %.lr.ph.split.us.i294
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_112 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis+3744(,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_115
	jmp	.LBB1_129
	.p2align	4, 0x90
.LBB1_116:                              # %.lr.ph.split.i295
                                        #   in Loop: Header=BB1_112 Depth=5
	movq	80(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rax,%rcx), %rax
	movl	amino_n(,%rax,4), %eax
	cmpl	$4, %eax
	jl	.LBB1_119
# BB#117:                               #   in Loop: Header=BB1_112 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_118:                              # %.lr.ph.split.split.us.i303
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_112 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$3, %rcx
	movq	%rbp, %rsi
	cmovgq	%r10, %rsi
	imulq	$104, %rsi, %rsi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis(%rsi,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_118
	jmp	.LBB1_129
.LBB1_119:                              # %.lr.ph.split.split.preheader.i296
                                        #   in Loop: Header=BB1_112 Depth=5
	leal	4(%rax,%rbp,4), %eax
	cmpl	$36, %eax
	jne	.LBB1_122
# BB#120:                               # %.lr.ph.split.split.i306.us.preheader
                                        #   in Loop: Header=BB1_112 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_121:                              # %.lr.ph.split.split.i306.us
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_112 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$3, %rcx
	movq	%rbp, %rsi
	cmovgq	%r10, %rsi
	imulq	$104, %rsi, %rsi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis(%rsi,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_121
	jmp	.LBB1_129
.LBB1_122:                              #   in Loop: Header=BB1_112 Depth=5
	movslq	%eax, %r13
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_124
.LBB1_123:                              #   in Loop: Header=BB1_124 Depth=6
	movslq	%esi, %rax
	imulq	$148, %r13, %rcx
	leaq	ribosumdis(%rcx,%rax,4), %rax
	jmp	.LBB1_128
	.p2align	4, 0x90
.LBB1_124:                              # %.lr.ph.split.split.i306
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_112 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rdx), %rsi
	movsbq	(%rsi,%r9), %rax
	movslq	amino_n(,%rax,4), %rcx
	cmpq	$3, %rcx
	movl	$36, %eax
	jg	.LBB1_127
# BB#125:                               #   in Loop: Header=BB1_124 Depth=6
	movsbq	(%rsi,%r15), %rax
	movl	amino_n(,%rax,4), %esi
	cmpl	$3, %esi
	movl	%ebp, %eax
	jg	.LBB1_127
# BB#126:                               #   in Loop: Header=BB1_124 Depth=6
	leal	4(%rsi,%rcx,4), %esi
	cmpl	$36, %esi
	movl	%ebp, %eax
	jne	.LBB1_123
	.p2align	4, 0x90
.LBB1_127:                              # %.thread.i309
                                        #   in Loop: Header=BB1_124 Depth=6
	cltq
	imulq	$104, %rax, %rax
	leaq	n_dis(%rax,%rcx,4), %rax
.LBB1_128:                              #   in Loop: Header=BB1_124 Depth=6
	cvtsi2ssl	(%rax), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rbx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rbx
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB1_124
	.p2align	4, 0x90
.LBB1_129:                              # %._crit_edge.i317
                                        #   in Loop: Header=BB1_112 Depth=5
	incq	%r8
	cmpq	96(%rsp), %r8           # 8-byte Folded Reload
	movl	%r14d, %r13d
	jne	.LBB1_112
	jmp	.LBB1_154
	.p2align	4, 0x90
.LBB1_130:                              #   in Loop: Header=BB1_107 Depth=4
	cmpq	%r15, %r9
	jge	.LBB1_155
# BB#131:                               #   in Loop: Header=BB1_107 Depth=4
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jle	.LBB1_155
# BB#132:                               #   in Loop: Header=BB1_107 Depth=4
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_153
# BB#133:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_107 Depth=4
	xorps	%xmm1, %xmm1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_134:                              # %.preheader.i
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB1_146 Depth 6
                                        #             Child Loop BB1_143 Depth 6
                                        #             Child Loop BB1_140 Depth 6
                                        #             Child Loop BB1_137 Depth 6
	testl	%r13d, %r13d
	jle	.LBB1_151
# BB#135:                               # %.lr.ph.i
                                        #   in Loop: Header=BB1_134 Depth=5
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rax,%rcx), %rcx
	movslq	amino_n(,%rcx,4), %r13
	cmpq	$4, %r13
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	jl	.LBB1_138
# BB#136:                               # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB1_134 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_137:                              # %.lr.ph.split.us.i
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_134 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis+3744(,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_137
	jmp	.LBB1_151
	.p2align	4, 0x90
.LBB1_138:                              # %.lr.ph.split.i
                                        #   in Loop: Header=BB1_134 Depth=5
	movq	80(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rax,%rcx), %rax
	movl	amino_n(,%rax,4), %eax
	cmpl	$4, %eax
	jl	.LBB1_141
# BB#139:                               #   in Loop: Header=BB1_134 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_140:                              # %.lr.ph.split.split.us.i
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_134 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$3, %rcx
	movq	%r13, %rbp
	cmovgq	%r10, %rbp
	imulq	$104, %rbp, %rbp
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis(%rbp,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_140
	jmp	.LBB1_151
.LBB1_141:                              # %.lr.ph.split.split.preheader.i
                                        #   in Loop: Header=BB1_134 Depth=5
	leal	4(%r13,%rax,4), %eax
	cmpl	$36, %eax
	jne	.LBB1_144
# BB#142:                               # %.lr.ph.split.split.i.us.preheader
                                        #   in Loop: Header=BB1_134 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_143:                              # %.lr.ph.split.split.i.us
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_134 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rax), %rcx
	movsbq	(%rcx,%r9), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$3, %rcx
	movq	%r13, %rbp
	cmovgq	%r10, %rbp
	imulq	$104, %rbp, %rbp
	xorps	%xmm3, %xmm3
	cvtsi2ssl	n_dis(%rbp,%rcx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rdx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rdi
	jne	.LBB1_143
	jmp	.LBB1_151
.LBB1_144:                              #   in Loop: Header=BB1_134 Depth=5
	movslq	%eax, %r8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_146
.LBB1_145:                              #   in Loop: Header=BB1_146 Depth=6
	cltq
	imulq	$148, %r8, %rcx
	leaq	ribosumdis(%rcx,%rax,4), %rax
	jmp	.LBB1_150
	.p2align	4, 0x90
.LBB1_146:                              # %.lr.ph.split.split.i
                                        #   Parent Loop BB1_102 Depth=1
                                        #     Parent Loop BB1_103 Depth=2
                                        #       Parent Loop BB1_105 Depth=3
                                        #         Parent Loop BB1_107 Depth=4
                                        #           Parent Loop BB1_134 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rdx), %r12
	movsbq	(%r12,%r9), %rax
	movslq	amino_n(,%rax,4), %rbp
	cmpq	$3, %rbp
	movl	$36, %ecx
	jg	.LBB1_149
# BB#147:                               #   in Loop: Header=BB1_146 Depth=6
	movsbq	(%r12,%r15), %rax
	movl	amino_n(,%rax,4), %eax
	cmpl	$3, %eax
	movl	%r13d, %ecx
	jg	.LBB1_149
# BB#148:                               #   in Loop: Header=BB1_146 Depth=6
	leal	4(%rbp,%rax,4), %eax
	cmpl	$36, %eax
	movl	%r13d, %ecx
	jne	.LBB1_145
	.p2align	4, 0x90
.LBB1_149:                              # %.thread.i
                                        #   in Loop: Header=BB1_146 Depth=6
	movslq	%ecx, %rax
	imulq	$104, %rax, %rax
	leaq	n_dis(%rax,%rbp,4), %rax
.LBB1_150:                              #   in Loop: Header=BB1_146 Depth=6
	cvtsi2ssl	(%rax), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rbx), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rbx
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB1_146
	.p2align	4, 0x90
.LBB1_151:                              # %._crit_edge.i
                                        #   in Loop: Header=BB1_134 Depth=5
	incq	%rsi
	cmpq	96(%rsp), %rsi          # 8-byte Folded Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	%r14d, %r13d
	jne	.LBB1_134
	jmp	.LBB1_154
.LBB1_153:                              #   in Loop: Header=BB1_107 Depth=4
	xorps	%xmm1, %xmm1
.LBB1_154:                              # %pairedribosumscore35.exit
                                        #   in Loop: Header=BB1_107 Depth=4
	mulss	%xmm0, %xmm1
	mulss	consweight_multi(%rip), %xmm1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	addss	(%rax,%r9,4), %xmm1
	movss	%xmm1, (%rax,%r9,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movss	(%rax,%r9,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rax,%r9,4)
.LBB1_155:                              #   in Loop: Header=BB1_107 Depth=4
	movl	40(%r11), %eax
	addq	$24, %r11
	cmpl	$-1, %eax
	jne	.LBB1_107
.LBB1_156:                              # %._crit_edge400
                                        #   in Loop: Header=BB1_105 Depth=3
	incq	%r9
	cmpq	152(%rsp), %r9          # 8-byte Folded Reload
	jne	.LBB1_105
# BB#157:                               # %._crit_edge405
                                        #   in Loop: Header=BB1_103 Depth=2
	movq	%r11, foldrna.pairpt2(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB1_158:                              #   in Loop: Header=BB1_103 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	cmpl	$-1, 40(%rcx)
	leaq	24(%rcx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	jne	.LBB1_103
.LBB1_159:                              # %._crit_edge408
                                        #   in Loop: Header=BB1_102 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	128(%rsp), %rdx         # 8-byte Folded Reload
	jne	.LBB1_102
# BB#160:                               # %.preheader342
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, foldrna.pairpt1(%rip)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_220
# BB#161:                               # %.preheader341.lr.ph
	testl	%r13d, %r13d
	setg	%al
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	setg	%r14b
	movq	foldrna.oseq1(%rip), %r12
	movq	foldrna.oseq2(%rip), %r13
	andb	%al, %r14b
	movq	foldrna.impmtx2(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movl	$36, %r10d
	.p2align	4, 0x90
.LBB1_162:                              # %.preheader341
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_164 Depth 2
                                        #       Child Loop BB1_166 Depth 3
                                        #         Child Loop BB1_167 Depth 4
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_170
# BB#163:                               # %.lr.ph394
                                        #   in Loop: Header=BB1_162 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r11,8), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_164:                              #   Parent Loop BB1_162 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_166 Depth 3
                                        #         Child Loop BB1_167 Depth 4
	movss	(%r15,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	testb	%r14b, %r14b
	je	.LBB1_169
# BB#165:                               # %.preheader.us.i268.preheader
                                        #   in Loop: Header=BB1_164 Depth=2
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_166:                              # %.preheader.us.i268
                                        #   Parent Loop BB1_162 Depth=1
                                        #     Parent Loop BB1_164 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_167 Depth 4
	movq	(%r12,%rax,8), %rsi
	movsbq	(%rsi,%r11), %rsi
	movslq	amino_n(,%rsi,4), %rbp
	cmpq	$3, %rbp
	cmovgq	%r10, %rbp
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movq	%r13, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_167:                              #   Parent Loop BB1_162 Depth=1
                                        #     Parent Loop BB1_164 Depth=2
                                        #       Parent Loop BB1_166 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rdi), %r9
	movsbq	(%r9,%rbx), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	cmpq	$3, %rcx
	movl	$36, %edx
	cmovleq	%rcx, %rdx
	imulq	$148, %rbp, %rcx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	ribosumdis(%rcx,%rdx,4), %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	(%rsi), %xmm3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addq	$8, %rsi
	addq	$8, %rdi
	decq	%r8
	jne	.LBB1_167
# BB#168:                               # %._crit_edge.us.i272
                                        #   in Loop: Header=BB1_166 Depth=3
	incq	%rax
	cmpq	96(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB1_166
.LBB1_169:                              # %singleribosumscore.exit
                                        #   in Loop: Header=BB1_164 Depth=2
	mulss	%xmm1, %xmm0
	mulss	consweight_multi(%rip), %xmm0
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r11,8), %rax
	addss	(%rax,%rbx,4), %xmm0
	movss	%xmm0, (%rax,%rbx,4)
	incq	%rbx
	cmpq	48(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB1_164
.LBB1_170:                              # %._crit_edge395
                                        #   in Loop: Header=BB1_162 Depth=1
	incq	%r11
	cmpq	40(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB1_162
	jmp	.LBB1_178
.LBB1_171:                              # %.preheader340
	testl	%ebx, %ebx
	jle	.LBB1_220
# BB#172:                               # %.preheader339.lr.ph
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_179
# BB#173:                               # %.preheader339.us.preheader
	movq	foldrna.impmtx2(%rip), %r12
	movabsq	$17179869180, %rax      # imm = 0x3FFFFFFFC
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rax,%rcx,4), %rbp
	andq	%rax, %rbp
	addq	$4, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r14d
	leaq	-1(%r14), %r15
	movq	%rax, %r13
	xorl	%ebx, %ebx
	andq	$7, %r13
	je	.LBB1_175
	.p2align	4, 0x90
.LBB1_174:                              # %.preheader339.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB1_174
.LBB1_175:                              # %.preheader339.us.prol.loopexit
	cmpq	$7, %r15
	jb	.LBB1_178
# BB#176:                               # %.preheader339.us.preheader.new
	subq	%rbx, %r14
	leaq	56(%r12,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_177:                              # %.preheader339.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r14
	jne	.LBB1_177
.LBB1_178:                              # %.preheader338
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_220
.LBB1_179:                              # %.lr.ph386
	movq	foldrna.pairprob1(%rip), %r8
	movq	foldrna.pairprob2(%rip), %r11
	movq	foldrna.map(%rip), %r14
	movq	foldrna.impmtx2(%rip), %r15
	movl	16(%rsp), %r9d          # 4-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %r10d
	xorl	%edi, %edi
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_180:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_181 Depth 2
                                        #       Child Loop BB1_183 Depth 3
                                        #         Child Loop BB1_195 Depth 4
                                        #         Child Loop BB1_191 Depth 4
                                        #         Child Loop BB1_185 Depth 4
	movq	(%r8,%rdi,8), %r12
	cmpl	$-1, 16(%r12)
	je	.LBB1_199
	.p2align	4, 0x90
.LBB1_181:                              # %.preheader337
                                        #   Parent Loop BB1_180 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_183 Depth 3
                                        #         Child Loop BB1_195 Depth 4
                                        #         Child Loop BB1_191 Depth 4
                                        #         Child Loop BB1_185 Depth 4
	testl	%eax, %eax
	jle	.LBB1_198
# BB#182:                               # %.lr.ph379
                                        #   in Loop: Header=BB1_181 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_183:                              #   Parent Loop BB1_180 Depth=1
                                        #     Parent Loop BB1_181 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_195 Depth 4
                                        #         Child Loop BB1_191 Depth 4
                                        #         Child Loop BB1_185 Depth 4
	movq	(%r11,%rbp,8), %rax
	movl	16(%rax), %esi
	cmpl	$-1, %esi
	je	.LBB1_196
# BB#184:                               # %.lr.ph370
                                        #   in Loop: Header=BB1_183 Depth=3
	movslq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	jle	.LBB1_190
	.p2align	4, 0x90
.LBB1_185:                              # %.lr.ph370.split.us
                                        #   Parent Loop BB1_180 Depth=1
                                        #     Parent Loop BB1_181 Depth=2
                                        #       Parent Loop BB1_183 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%edx, %ecx
	orl	%esi, %ecx
	js	.LBB1_189
# BB#186:                               #   in Loop: Header=BB1_185 Depth=4
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	20(%rax), %xmm1
	cmpq	%rdx, %rdi
	setl	%cl
	movslq	%esi, %rsi
	cmpq	%rsi, %rbp
	setl	%bl
	cmpq	%rsi, %rbp
	jg	.LBB1_188
# BB#187:                               #   in Loop: Header=BB1_185 Depth=4
	andb	%cl, %bl
	je	.LBB1_189
.LBB1_188:                              #   in Loop: Header=BB1_185 Depth=4
	movq	(%r14,%rdx,8), %rcx
	xorpd	%xmm2, %xmm2
	maxss	(%rcx,%rsi,4), %xmm2
	mulss	consweight_rna(%rip), %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm2, %xmm1
	movq	(%r15,%rdi,8), %rcx
	addss	(%rcx,%rbp,4), %xmm1
	movss	%xmm1, (%rcx,%rbp,4)
.LBB1_189:                              #   in Loop: Header=BB1_185 Depth=4
	movl	40(%rax), %esi
	addq	$24, %rax
	cmpl	$-1, %esi
	jne	.LBB1_185
	jmp	.LBB1_196
	.p2align	4, 0x90
.LBB1_190:                              # %.lr.ph370.split
                                        #   in Loop: Header=BB1_183 Depth=3
	jge	.LBB1_195
	.p2align	4, 0x90
.LBB1_191:                              # %.lr.ph370.split.split.us
                                        #   Parent Loop BB1_180 Depth=1
                                        #     Parent Loop BB1_181 Depth=2
                                        #       Parent Loop BB1_183 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%edx, %ecx
	orl	%esi, %ecx
	js	.LBB1_194
# BB#192:                               # %.lr.ph370.split.split.us
                                        #   in Loop: Header=BB1_191 Depth=4
	movslq	%esi, %rsi
	cmpq	%rsi, %rbp
	jge	.LBB1_194
# BB#193:                               #   in Loop: Header=BB1_191 Depth=4
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	20(%rax), %xmm1
	movq	(%r14,%rdx,8), %rcx
	xorpd	%xmm2, %xmm2
	maxss	(%rcx,%rsi,4), %xmm2
	mulss	consweight_rna(%rip), %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	movq	(%r15,%rdi,8), %rcx
	addss	(%rcx,%rbp,4), %xmm2
	movss	%xmm2, (%rcx,%rbp,4)
.LBB1_194:                              #   in Loop: Header=BB1_191 Depth=4
	movl	40(%rax), %esi
	addq	$24, %rax
	cmpl	$-1, %esi
	jne	.LBB1_191
	jmp	.LBB1_196
	.p2align	4, 0x90
.LBB1_195:                              # %.lr.ph370.split.split
                                        #   Parent Loop BB1_180 Depth=1
                                        #     Parent Loop BB1_181 Depth=2
                                        #       Parent Loop BB1_183 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$-1, 40(%rax)
	leaq	24(%rax), %rax
	jne	.LBB1_195
	.p2align	4, 0x90
.LBB1_196:                              # %._crit_edge371
                                        #   in Loop: Header=BB1_183 Depth=3
	incq	%rbp
	cmpq	%r10, %rbp
	jne	.LBB1_183
# BB#197:                               # %._crit_edge380
                                        #   in Loop: Header=BB1_181 Depth=2
	movq	%rax, foldrna.pairpt2(%rip)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB1_198:                              #   in Loop: Header=BB1_181 Depth=2
	cmpl	$-1, 40(%r12)
	leaq	24(%r12), %r12
	jne	.LBB1_181
.LBB1_199:                              # %._crit_edge383
                                        #   in Loop: Header=BB1_180 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jne	.LBB1_180
# BB#200:                               # %.preheader336
	movq	%r12, foldrna.pairpt1(%rip)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_220
# BB#201:                               # %.preheader335.lr.ph
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_220
# BB#202:                               # %.preheader335.us.preheader
	movq	foldrna.impmtx2(%rip), %r14
	movl	16(%rsp), %edx          # 4-byte Reload
	leaq	-1(%r10), %r11
	movq	8(%rsp), %r8            # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$7, %r8d
	movq	%r10, %r9
	subq	%r8, %r9
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_203:                              # %.preheader335.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_211 Depth 2
                                        #     Child Loop BB1_215 Depth 2
                                        #     Child Loop BB1_218 Depth 2
	cmpq	$8, %r10
	movq	(%r14,%rbx,8), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rbx,8), %rdi
	jae	.LBB1_205
# BB#204:                               #   in Loop: Header=BB1_203 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB1_213
	.p2align	4, 0x90
.LBB1_205:                              # %min.iters.checked585
                                        #   in Loop: Header=BB1_203 Depth=1
	testq	%r9, %r9
	je	.LBB1_209
# BB#206:                               # %vector.memcheck598
                                        #   in Loop: Header=BB1_203 Depth=1
	leaq	(%rax,%r10,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB1_210
# BB#207:                               # %vector.memcheck598
                                        #   in Loop: Header=BB1_203 Depth=1
	leaq	(%rdi,%r10,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_210
.LBB1_209:                              #   in Loop: Header=BB1_203 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB1_213
.LBB1_210:                              # %vector.body581.preheader
                                        #   in Loop: Header=BB1_203 Depth=1
	leaq	16(%rax), %rsi
	leaq	16(%rdi), %rcx
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB1_211:                              # %vector.body581
                                        #   Parent Loop BB1_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rcx), %xmm2
	movups	(%rcx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rcx)
	movups	%xmm3, (%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB1_211
# BB#212:                               # %middle.block582
                                        #   in Loop: Header=BB1_203 Depth=1
	testq	%r8, %r8
	movq	%r9, %rbp
	je	.LBB1_219
	.p2align	4, 0x90
.LBB1_213:                              # %scalar.ph583.preheader
                                        #   in Loop: Header=BB1_203 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%ebp, %ecx
	movq	%r11, %rsi
	subq	%rbp, %rsi
	andq	$3, %rcx
	je	.LBB1_216
# BB#214:                               # %scalar.ph583.prol.preheader
                                        #   in Loop: Header=BB1_203 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB1_215:                              # %scalar.ph583.prol
                                        #   Parent Loop BB1_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi,%rbp,4), %xmm0
	movss	%xmm0, (%rdi,%rbp,4)
	incq	%rbp
	incq	%rcx
	jne	.LBB1_215
.LBB1_216:                              # %scalar.ph583.prol.loopexit
                                        #   in Loop: Header=BB1_203 Depth=1
	cmpq	$3, %rsi
	jb	.LBB1_219
# BB#217:                               # %scalar.ph583.preheader.new
                                        #   in Loop: Header=BB1_203 Depth=1
	movq	%r10, %rsi
	subq	%rbp, %rsi
	leaq	12(%rdi,%rbp,4), %rdi
	leaq	12(%rax,%rbp,4), %rax
	.p2align	4, 0x90
.LBB1_218:                              # %scalar.ph583
                                        #   Parent Loop BB1_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	-12(%rdi), %xmm0
	movss	%xmm0, -12(%rdi)
	movss	-8(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	-8(%rdi), %xmm0
	movss	%xmm0, -8(%rdi)
	movss	-4(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdi), %xmm0
	movss	%xmm0, -4(%rdi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi), %xmm0
	movss	%xmm0, (%rdi)
	addq	$16, %rdi
	addq	$16, %rax
	addq	$-4, %rsi
	jne	.LBB1_218
.LBB1_219:                              # %._crit_edge365.us
                                        #   in Loop: Header=BB1_203 Depth=1
	incq	%rbx
	cmpq	%rdx, %rbx
	jne	.LBB1_203
.LBB1_220:                              # %._crit_edge367
	movq	foldrna.useq1(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.useq2(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.oseq1(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.oseq2(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.oseq1r(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.oseq2r(%rip), %rdi
	callq	FreeCharMtx
	movq	foldrna.odir1(%rip), %rdi
	callq	free
	movq	foldrna.odir2(%rip), %rdi
	callq	free
	movq	foldrna.impmtx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	foldrna.map(%rip), %rdi
	callq	FreeFloatMtx
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatMtx
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB1_223
# BB#221:                               # %.lr.ph362.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_222:                              # %.lr.ph362
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.pairprob1(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	free
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_222
.LBB1_223:                              # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB1_226
# BB#224:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_225:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	foldrna.pairprob2(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	free
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_225
.LBB1_226:                              # %._crit_edge
	movq	foldrna.pairprob1(%rip), %rdi
	callq	free
	movq	foldrna.pairprob2(%rip), %rdi
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	foldrna, .Lfunc_end1-foldrna
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_33
	.quad	.LBB1_27
	.quad	.LBB1_33
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_33
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_27
	.quad	.LBB1_34
	.quad	.LBB1_34
.LJTI1_1:
	.quad	.LBB1_42
	.quad	.LBB1_36
	.quad	.LBB1_42
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_42
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_36
	.quad	.LBB1_43
	.quad	.LBB1_43
.LJTI1_2:
	.quad	.LBB1_45
	.quad	.LBB1_38
	.quad	.LBB1_45
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_45
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_38
	.quad	.LBB1_46
	.quad	.LBB1_46
.LJTI1_3:
	.quad	.LBB1_57
	.quad	.LBB1_51
	.quad	.LBB1_57
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_57
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_51
	.quad	.LBB1_58
	.quad	.LBB1_58
.LJTI1_4:
	.quad	.LBB1_66
	.quad	.LBB1_60
	.quad	.LBB1_66
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_66
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_60
	.quad	.LBB1_67
	.quad	.LBB1_67
.LJTI1_5:
	.quad	.LBB1_69
	.quad	.LBB1_62
	.quad	.LBB1_69
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_69
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_62
	.quad	.LBB1_70
	.quad	.LBB1_70

	.text
	.p2align	4, 0x90
	.type	mccaskillextract,@function
mccaskillextract:                       # @mccaskillextract
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 128
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%edx, %ebx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	strlen
	movq	%rax, %rbp
	movslq	%ebp, %r13
	movl	$4, %esi
	movq	%r13, %rdi
	callq	calloc
	movq	%rax, mccaskillextract.pairnum(%rip)
	testl	%r13d, %r13d
	jle	.LBB2_2
# BB#1:                                 # %.lr.ph15
	movabsq	$17179869180, %rcx      # imm = 0x3FFFFFFFC
	leaq	(%rcx,%rbp,4), %rdx
	andq	%rcx, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB2_2:                                # %.preheader2
	testl	%ebx, %ebx
	jle	.LBB2_18
# BB#3:                                 # %.lr.ph12.preheader
	movslq	%ebx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph12
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
                                        #       Child Loop BB2_9 Depth 3
                                        #         Child Loop BB2_10 Depth 4
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rdi
	callq	strlen
	testl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jle	.LBB2_17
# BB#5:                                 # %.lr.ph9
                                        #   in Loop: Header=BB2_4 Depth=1
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_6:                                #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_9 Depth 3
                                        #         Child Loop BB2_10 Depth 4
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r14,8), %r13
	movl	16(%r13), %eax
	cmpl	$-1, %eax
	jne	.LBB2_9
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_9 Depth=3
	cmpl	$-1, %eax
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_9 Depth=3
	movq	mccaskillextract.pairnum(%rip), %rax
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rsi
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	callq	realloc
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%rax, (%rbx,%rbp,8)
	movq	mccaskillextract.pairnum(%rip), %rcx
	movslq	(%rcx,%rbp,4), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rcx,%rbp,4)
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rcx
	movl	$0, 20(%rax,%rdx,8)
	movl	%r15d, 16(%rax,%rdx,8)
	movabsq	$-4647714811151384577, %rsi # imm = 0xBF800000FFFFFFFF
	movq	%rsi, 40(%rax,%rdx,8)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%r15d, %eax
.LBB2_14:                               #   in Loop: Header=BB2_9 Depth=3
	cvtss2sd	%xmm0, %xmm0
	mulsd	(%rdx,%r12,8), %xmm0
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 20(%rcx)
	cmpl	%r15d, %eax
	jne	.LBB2_15
# BB#7:                                 #   in Loop: Header=BB2_9 Depth=3
	movl	40(%r13), %eax
	addq	$24, %r13
	cmpl	$-1, %eax
	je	.LBB2_16
.LBB2_9:                                # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_10 Depth 4
	movq	(%r8,%r12,8), %rcx
	cltq
	movl	(%rcx,%rax,4), %r15d
	movss	20(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movslq	(%rcx,%r14,4), %rbp
	movq	(%rbx,%rbp,8), %rdi
	leaq	-24(%rdi), %rcx
	.p2align	4, 0x90
.LBB2_10:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        #       Parent Loop BB2_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	40(%rcx), %eax
	addq	$24, %rcx
	cmpl	$-1, %eax
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=4
	cmpl	%r15d, %eax
	jne	.LBB2_10
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_16:                               # %._crit_edge6
                                        #   in Loop: Header=BB2_6 Depth=2
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB2_6
.LBB2_17:                               # %._crit_edge10
                                        #   in Loop: Header=BB2_4 Depth=1
	incq	%r12
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB2_4
.LBB2_18:                               # %._crit_edge
	movq	mccaskillextract.pairnum(%rip), %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB2_15:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	mccaskillextract, .Lfunc_end2-mccaskillextract
	.cfi_endproc

	.type	rnaalifoldcall.order,@object # @rnaalifoldcall.order
	.local	rnaalifoldcall.order
	.comm	rnaalifoldcall.order,8,8
	.type	rnaalifoldcall.name,@object # @rnaalifoldcall.name
	.local	rnaalifoldcall.name
	.comm	rnaalifoldcall.name,8,8
	.type	rnaalifoldcall.fnamein,@object # @rnaalifoldcall.fnamein
	.local	rnaalifoldcall.fnamein
	.comm	rnaalifoldcall.fnamein,100,16
	.type	rnaalifoldcall.cmd,@object # @rnaalifoldcall.cmd
	.local	rnaalifoldcall.cmd
	.comm	rnaalifoldcall.cmd,1000,16
	.type	rnaalifoldcall.pairnum,@object # @rnaalifoldcall.pairnum
	.local	rnaalifoldcall.pairnum
	.comm	rnaalifoldcall.pairnum,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/tmp/_rnaalifoldin.%d"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"seq%d"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"w"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Cannot open /tmp/_rnaalifoldin\n"
	.size	.L.str.3, 32

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"RNAalifold -p %s"
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"alifold.out"
	.size	.L.str.5, 12

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d %d %d %f"
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%d-%d, %f\n"
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"rm -f %s"
	.size	.L.str.9, 9

	.type	foldrna.useq1,@object   # @foldrna.useq1
	.local	foldrna.useq1
	.comm	foldrna.useq1,8,8
	.type	foldrna.useq2,@object   # @foldrna.useq2
	.local	foldrna.useq2
	.comm	foldrna.useq2,8,8
	.type	foldrna.oseq1,@object   # @foldrna.oseq1
	.local	foldrna.oseq1
	.comm	foldrna.oseq1,8,8
	.type	foldrna.oseq2,@object   # @foldrna.oseq2
	.local	foldrna.oseq2
	.comm	foldrna.oseq2,8,8
	.type	foldrna.oseq1r,@object  # @foldrna.oseq1r
	.local	foldrna.oseq1r
	.comm	foldrna.oseq1r,8,8
	.type	foldrna.oseq2r,@object  # @foldrna.oseq2r
	.local	foldrna.oseq2r
	.comm	foldrna.oseq2r,8,8
	.type	foldrna.odir1,@object   # @foldrna.odir1
	.local	foldrna.odir1
	.comm	foldrna.odir1,8,8
	.type	foldrna.odir2,@object   # @foldrna.odir2
	.local	foldrna.odir2
	.comm	foldrna.odir2,8,8
	.type	foldrna.pairprob1,@object # @foldrna.pairprob1
	.local	foldrna.pairprob1
	.comm	foldrna.pairprob1,8,8
	.type	foldrna.pairprob2,@object # @foldrna.pairprob2
	.local	foldrna.pairprob2
	.comm	foldrna.pairprob2,8,8
	.type	foldrna.pairpt1,@object # @foldrna.pairpt1
	.local	foldrna.pairpt1
	.comm	foldrna.pairpt1,8,8
	.type	foldrna.pairpt2,@object # @foldrna.pairpt2
	.local	foldrna.pairpt2
	.comm	foldrna.pairpt2,8,8
	.type	foldrna.impmtx2,@object # @foldrna.impmtx2
	.local	foldrna.impmtx2
	.comm	foldrna.impmtx2,8,8
	.type	foldrna.map,@object     # @foldrna.map
	.local	foldrna.map
	.comm	foldrna.map,8,8
	.type	mccaskillextract.pairnum,@object # @mccaskillextract.pairnum
	.local	mccaskillextract.pairnum
	.comm	mccaskillextract.pairnum,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"okashii!\n"
	.size	.L.str.10, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
