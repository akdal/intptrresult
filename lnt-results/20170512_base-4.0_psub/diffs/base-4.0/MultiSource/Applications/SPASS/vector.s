	.text
	.file	"vector.bc"
	.globl	vec_Swap
	.p2align	4, 0x90
	.type	vec_Swap,@function
vec_Swap:                               # @vec_Swap
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	vec_VECTOR(,%rax,8), %rcx
	movl	%esi, %edx
	movq	vec_VECTOR(,%rdx,8), %rsi
	movq	%rsi, vec_VECTOR(,%rax,8)
	movq	%rcx, vec_VECTOR(,%rdx,8)
	retq
.Lfunc_end0:
	.size	vec_Swap, .Lfunc_end0-vec_Swap
	.cfi_endproc

	.globl	vec_PrintSel
	.p2align	4, 0x90
	.type	vec_PrintSel,@function
vec_PrintSel:                           # @vec_PrintSel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movl	%edi, %ebx
	cmpl	$0, vec_MAX(%rip)
	jle	.LBB1_4
# BB#1:                                 # %.preheader
	cmpl	%ebp, %ebx
	jge	.LBB1_3
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	%ebx, %eax
	movq	vec_VECTOR(,%rax,8), %rdi
	callq	*%r14
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incl	%ebx
	cmpl	%ebx, %ebp
	jne	.LBB1_2
.LBB1_3:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB1_4:
	movl	$.L.str.1, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end1:
	.size	vec_PrintSel, .Lfunc_end1-vec_PrintSel
	.cfi_endproc

	.globl	vec_PrintAll
	.p2align	4, 0x90
	.type	vec_PrintAll,@function
vec_PrintAll:                           # @vec_PrintAll
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, vec_MAX(%rip)
	jle	.LBB2_4
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	vec_VECTOR(,%rbx,8), %rdi
	callq	*%r14
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incq	%rbx
	cmpl	vec_MAX(%rip), %ebx
	jl	.LBB2_2
# BB#3:                                 # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_4:
	movl	$.L.str.1, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	puts                    # TAILCALL
.Lfunc_end2:
	.size	vec_PrintAll, .Lfunc_end2-vec_PrintAll
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Entry %d:\t"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Vector is empty"
	.size	.L.str.1, 16

	.type	vec_VECTOR,@object      # @vec_VECTOR
	.comm	vec_VECTOR,80000,16
	.type	vec_MAX,@object         # @vec_MAX
	.comm	vec_MAX,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
