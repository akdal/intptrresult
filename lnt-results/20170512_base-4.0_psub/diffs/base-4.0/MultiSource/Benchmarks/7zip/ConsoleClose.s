	.text
	.file	"ConsoleClose.bc"
	.globl	_ZN13NConsoleClose15TestBreakSignalEv
	.p2align	4, 0x90
	.type	_ZN13NConsoleClose15TestBreakSignalEv,@function
_ZN13NConsoleClose15TestBreakSignalEv:  # @_ZN13NConsoleClose15TestBreakSignalEv
	.cfi_startproc
# BB#0:
	cmpl	$0, _ZL14g_BreakCounter(%rip)
	setg	%al
	retq
.Lfunc_end0:
	.size	_ZN13NConsoleClose15TestBreakSignalEv, .Lfunc_end0-_ZN13NConsoleClose15TestBreakSignalEv
	.cfi_endproc

	.globl	_ZN13NConsoleClose14CheckCtrlBreakEv
	.p2align	4, 0x90
	.type	_ZN13NConsoleClose14CheckCtrlBreakEv,@function
_ZN13NConsoleClose14CheckCtrlBreakEv:   # @_ZN13NConsoleClose14CheckCtrlBreakEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	cmpl	$0, _ZL14g_BreakCounter(%rip)
	jg	.LBB1_2
# BB#1:
	popq	%rax
	retq
.LBB1_2:
	movl	$1, %edi
	callq	__cxa_allocate_exception
	movl	$_ZTIN13NConsoleClose19CCtrlBreakExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end1:
	.size	_ZN13NConsoleClose14CheckCtrlBreakEv, .Lfunc_end1-_ZN13NConsoleClose14CheckCtrlBreakEv
	.cfi_endproc

	.globl	_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev
	.p2align	4, 0x90
	.type	_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev,@function
_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev: # @_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTVN13NConsoleClose18CCtrlHandlerSetterE+16, (%rbx)
	movl	$2, %edi
	movl	$_ZN13NConsoleCloseL14HandlerRoutineEi, %esi
	callq	signal
	movq	%rax, 8(%rbx)
	cmpq	$-1, %rax
	je	.LBB2_1
# BB#3:
	movl	$15, %edi
	movl	$_ZN13NConsoleCloseL14HandlerRoutineEi, %esi
	callq	signal
	movq	%rax, 16(%rbx)
	cmpq	$-1, %rax
	je	.LBB2_4
# BB#5:
	popq	%rbx
	retq
.LBB2_1:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	jmp	.LBB2_2
.LBB2_4:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.1, (%rax)
.LBB2_2:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end2:
	.size	_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev, .Lfunc_end2-_ZN13NConsoleClose18CCtrlHandlerSetterC2Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN13NConsoleCloseL14HandlerRoutineEi,@function
_ZN13NConsoleCloseL14HandlerRoutineEi:  # @_ZN13NConsoleCloseL14HandlerRoutineEi
	.cfi_startproc
# BB#0:
	movl	_ZL14g_BreakCounter(%rip), %eax
	incl	%eax
	movl	%eax, _ZL14g_BreakCounter(%rip)
	cmpl	$1, %eax
	jg	.LBB3_2
# BB#1:
	retq
.LBB3_2:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	_ZN13NConsoleCloseL14HandlerRoutineEi, .Lfunc_end3-_ZN13NConsoleCloseL14HandlerRoutineEi
	.cfi_endproc

	.globl	_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev
	.p2align	4, 0x90
	.type	_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev,@function
_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev: # @_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTVN13NConsoleClose18CCtrlHandlerSetterE+16, (%rbx)
	movq	8(%rbx), %rsi
	movl	$2, %edi
	callq	signal
	movq	16(%rbx), %rsi
	movl	$15, %edi
	popq	%rbx
	jmp	signal                  # TAILCALL
.Lfunc_end4:
	.size	_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev, .Lfunc_end4-_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev
	.cfi_endproc

	.globl	_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev
	.p2align	4, 0x90
	.type	_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev,@function
_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev: # @_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTVN13NConsoleClose18CCtrlHandlerSetterE+16, (%rbx)
	movq	8(%rbx), %rsi
	movl	$2, %edi
	callq	signal
	movq	16(%rbx), %rsi
	movl	$15, %edi
	callq	signal
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end5:
	.size	_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev, .Lfunc_end5-_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev
	.cfi_endproc

	.type	_ZL14g_BreakCounter,@object # @_ZL14g_BreakCounter
	.local	_ZL14g_BreakCounter
	.comm	_ZL14g_BreakCounter,4,4
	.type	_ZTSN13NConsoleClose19CCtrlBreakExceptionE,@object # @_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.section	.rodata._ZTSN13NConsoleClose19CCtrlBreakExceptionE,"aG",@progbits,_ZTSN13NConsoleClose19CCtrlBreakExceptionE,comdat
	.weak	_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.p2align	4
_ZTSN13NConsoleClose19CCtrlBreakExceptionE:
	.asciz	"N13NConsoleClose19CCtrlBreakExceptionE"
	.size	_ZTSN13NConsoleClose19CCtrlBreakExceptionE, 39

	.type	_ZTIN13NConsoleClose19CCtrlBreakExceptionE,@object # @_ZTIN13NConsoleClose19CCtrlBreakExceptionE
	.section	.rodata._ZTIN13NConsoleClose19CCtrlBreakExceptionE,"aG",@progbits,_ZTIN13NConsoleClose19CCtrlBreakExceptionE,comdat
	.weak	_ZTIN13NConsoleClose19CCtrlBreakExceptionE
	.p2align	3
_ZTIN13NConsoleClose19CCtrlBreakExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN13NConsoleClose19CCtrlBreakExceptionE
	.size	_ZTIN13NConsoleClose19CCtrlBreakExceptionE, 16

	.type	_ZTVN13NConsoleClose18CCtrlHandlerSetterE,@object # @_ZTVN13NConsoleClose18CCtrlHandlerSetterE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN13NConsoleClose18CCtrlHandlerSetterE
	.p2align	3
_ZTVN13NConsoleClose18CCtrlHandlerSetterE:
	.quad	0
	.quad	_ZTIN13NConsoleClose18CCtrlHandlerSetterE
	.quad	_ZN13NConsoleClose18CCtrlHandlerSetterD2Ev
	.quad	_ZN13NConsoleClose18CCtrlHandlerSetterD0Ev
	.size	_ZTVN13NConsoleClose18CCtrlHandlerSetterE, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SetConsoleCtrlHandler fails (SIGINT)"
	.size	.L.str, 37

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SetConsoleCtrlHandler fails (SIGTERM)"
	.size	.L.str.1, 38

	.type	_ZTSN13NConsoleClose18CCtrlHandlerSetterE,@object # @_ZTSN13NConsoleClose18CCtrlHandlerSetterE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN13NConsoleClose18CCtrlHandlerSetterE
	.p2align	4
_ZTSN13NConsoleClose18CCtrlHandlerSetterE:
	.asciz	"N13NConsoleClose18CCtrlHandlerSetterE"
	.size	_ZTSN13NConsoleClose18CCtrlHandlerSetterE, 38

	.type	_ZTIN13NConsoleClose18CCtrlHandlerSetterE,@object # @_ZTIN13NConsoleClose18CCtrlHandlerSetterE
	.globl	_ZTIN13NConsoleClose18CCtrlHandlerSetterE
	.p2align	3
_ZTIN13NConsoleClose18CCtrlHandlerSetterE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN13NConsoleClose18CCtrlHandlerSetterE
	.size	_ZTIN13NConsoleClose18CCtrlHandlerSetterE, 16


	.globl	_ZN13NConsoleClose18CCtrlHandlerSetterC1Ev
	.type	_ZN13NConsoleClose18CCtrlHandlerSetterC1Ev,@function
_ZN13NConsoleClose18CCtrlHandlerSetterC1Ev = _ZN13NConsoleClose18CCtrlHandlerSetterC2Ev
	.globl	_ZN13NConsoleClose18CCtrlHandlerSetterD1Ev
	.type	_ZN13NConsoleClose18CCtrlHandlerSetterD1Ev,@function
_ZN13NConsoleClose18CCtrlHandlerSetterD1Ev = _ZN13NConsoleClose18CCtrlHandlerSetterD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
