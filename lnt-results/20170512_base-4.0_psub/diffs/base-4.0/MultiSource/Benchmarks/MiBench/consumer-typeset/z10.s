	.text
	.file	"z10.bc"
	.globl	CrossInit
	.p2align	4, 0x90
	.type	CrossInit,@function
CrossInit:                              # @CrossInit
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movzbl	zz_lengths+140(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_3:
	movb	$-116, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movb	$0, 33(%rbx)
	movl	$0, 48(%rbx)
	movl	$0, 60(%rbx)
	movq	$0, 72(%rbx)
	movw	$0, 80(%rbx)
	movq	%r14, 64(%rbx)
	movq	%rbx, 88(%r14)
	cmpq	$0, RootCross(%rip)
	jne	.LBB0_8
# BB#4:
	movzbl	zz_lengths+141(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_5
# BB#6:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_7
.LBB0_5:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_7:
	movb	$-115, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, RootCross(%rip)
.LBB0_8:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_11
.LBB0_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	RootCross(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_14
# BB#12:
	testq	%rcx, %rcx
	je	.LBB0_14
# BB#13:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_14:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_17
# BB#15:
	testq	%rax, %rax
	je	.LBB0_17
# BB#16:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	CrossInit, .Lfunc_end0-CrossInit
	.cfi_endproc

	.globl	CrossMake
	.p2align	4, 0x90
	.type	CrossMake,@function
CrossMake:                              # @CrossMake
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r12
	movzbl	zz_lengths+6(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB1_1
# BB#2:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_3
.LBB1_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB1_3:
	movb	$6, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movb	%bl, 41(%r15)
	andb	$-5, 42(%r15)
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
# BB#5:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_6
.LBB1_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_6:
	movb	$2, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%r12, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_7
# BB#8:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_9
.LBB1_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_9:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_12
# BB#10:
	testq	%rax, %rax
	je	.LBB1_12
# BB#11:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_12:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_15
# BB#13:
	testq	%rax, %rax
	je	.LBB1_15
# BB#14:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_15:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_16
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_18
.LBB1_16:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_18:
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB1_21
# BB#19:
	testq	%rax, %rax
	je	.LBB1_21
# BB#20:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_21:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_24
# BB#22:
	testq	%rax, %rax
	je	.LBB1_24
# BB#23:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_24:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	CrossMake, .Lfunc_end1-CrossMake
	.cfi_endproc

	.globl	GallTargEval
	.p2align	4, 0x90
	.type	GallTargEval,@function
GallTargEval:                           # @GallTargEval
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi20:
	.cfi_def_cfa_offset 592
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	88(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movq	%r15, %rdi
	callq	CrossInit
	movq	88(%r15), %rbx
.LBB2_2:
	movzwl	2(%r14), %eax
	cmpw	80(%rbx), %ax
	je	.LBB2_4
# BB#3:
	movw	%ax, 80(%rbx)
	movl	$0, 60(%rbx)
.LBB2_4:
	addq	$60, %rbx
	movzwl	%ax, %edi
	callq	FileName
	movq	%rax, %r13
	movl	(%rbx), %ebp
	incl	%ebp
	movl	%ebp, (%rbx)
	movq	%r13, %rdi
	callq	strlen
	addq	$6, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB2_6
# BB#5:
	movl	%ebp, (%rsp)
	movl	$10, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r13, %r9
	callq	Error
.LBB2_6:
	leaq	16(%rsp), %r12
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	%r12, %rdi
	callq	strlen
	movw	$38, 16(%rsp,%rax)
	movl	(%rbx), %edi
	callq	StringInt
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcat
	movl	$11, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	MakeWord
	movl	$132, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	CrossMake
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	GallTargEval, .Lfunc_end2-GallTargEval
	.cfi_endproc

	.globl	CrossAddTag
	.p2align	4, 0x90
	.type	CrossAddTag,@function
CrossAddTag:                            # @CrossAddTag
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi33:
	.cfi_def_cfa_offset 608
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB3_18
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader128
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB3_3
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpb	$10, %dl
	jne	.LBB3_1
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	80(%rcx), %rdx
	movzwl	41(%rdx), %edx
	testb	$1, %dl
	jne	.LBB3_6
.LBB3_1:                                # %.loopexit129
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.LBB3_2
.LBB3_18:                               # %.thread
	movq	80(%rbx), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	je	.LBB3_68
	.p2align	4, 0x90
.LBB3_20:                               # %.preheader126
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_21 Depth 2
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB3_21:                               #   Parent Loop BB3_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %r15
	movzbl	32(%r15), %ecx
	testb	%cl, %cl
	je	.LBB3_21
# BB#22:                                #   in Loop: Header=BB3_20 Depth=1
	addb	$112, %cl
	cmpb	$2, %cl
	ja	.LBB3_19
# BB#23:                                #   in Loop: Header=BB3_20 Depth=1
	movzwl	41(%r15), %ecx
	testb	$1, %cl
	jne	.LBB3_24
.LBB3_19:                               #   in Loop: Header=BB3_20 Depth=1
	movq	8(%r14), %r14
	cmpq	%rax, %r14
	jne	.LBB3_20
.LBB3_68:                               # %.thread117
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_6:
	movq	8(%rcx), %rcx
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB3_7
# BB#8:
	addb	$-11, %dl
	cmpb	$1, %dl
	ja	.LBB3_10
# BB#9:
	cmpb	$0, 64(%rcx)
	je	.LBB3_11
.LBB3_10:                               # %.loopexit127
	cmpq	%rbx, %rax
	jne	.LBB3_68
	jmp	.LBB3_18
.LBB3_24:
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_26
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_27
.LBB3_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_27:
	movb	$10, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r15, 80(%rax)
	movq	80(%rbx), %r12
	cmpq	$0, 88(%r12)
	jne	.LBB3_29
# BB#28:
	movq	%r12, %rdi
	callq	CrossInit
.LBB3_29:
	leaq	32(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzwl	34(%rbx), %ebp
	movl	%ebp, %edi
	callq	FileName
	movq	%rax, %r13
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	crtab_getnext
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r13, %rdi
	callq	strlen
	addq	$20, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB3_31
# BB#30:
	movl	$10, %edi
	movl	$3, %esi
	movl	$.L.str.57, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	callq	Error
.LBB3_31:                               # %CrossGenTag.exit
	movzwl	34(%r12), %edx
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	36(%r12), %ecx
	leaq	32(%rsp), %rbp
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbp, %rdi
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	sprintf
	movl	$12, %edi
	movq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_33
# BB#32:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_34
.LBB3_33:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_34:
	movq	16(%rsp), %r12          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB3_37
# BB#35:
	testq	%rax, %rax
	je	.LBB3_37
# BB#36:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_37:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_40
# BB#38:
	testq	%rax, %rax
	je	.LBB3_40
# BB#39:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_40:
	movb	32(%r15), %al
	cmpb	$-112, %al
	je	.LBB3_44
# BB#41:
	cmpb	$-111, %al
	je	.LBB3_45
# BB#42:
	cmpb	$-110, %al
	jne	.LBB3_59
# BB#43:                                # %.preheader124
	movq	8(%rbx), %r14
	cmpq	%rbx, %r14
	je	.LBB3_56
.LBB3_52:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_54 Depth 2
	leaq	16(%r14), %rax
	jmp	.LBB3_54
	.p2align	4, 0x90
.LBB3_53:                               #   in Loop: Header=BB3_54 Depth=2
	addq	$16, %rax
.LBB3_54:                               #   Parent Loop BB3_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB3_53
# BB#55:                                #   in Loop: Header=BB3_52 Depth=1
	cmpb	$10, %cl
	jne	.LBB3_59
# BB#51:                                #   in Loop: Header=BB3_52 Depth=1
	movq	8(%r14), %r14
	cmpq	%rbx, %r14
	jne	.LBB3_52
.LBB3_56:
	movq	%rbx, %r14
	jmp	.LBB3_59
.LBB3_11:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_13
# BB#12:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_14
.LBB3_44:
	movq	8(%rbx), %r14
	jmp	.LBB3_59
.LBB3_45:
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	movq	%rbx, %r14
	je	.LBB3_59
# BB#46:                                # %.preheader.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB3_47:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB3_47
# BB#48:                                # %.preheader
	cmpb	$10, %dl
	jne	.LBB3_58
# BB#49:
	movq	80(%rcx), %rcx
	cmpb	$-112, 32(%rcx)
	jne	.LBB3_58
# BB#50:
	movq	8(%rax), %r14
	jmp	.LBB3_59
.LBB3_58:
	movq	%rax, %r14
.LBB3_59:                               # %.loopexit
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_61
# BB#60:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_62
.LBB3_61:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_62:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB3_65
# BB#63:
	testq	%rax, %rax
	je	.LBB3_65
# BB#64:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_65:
	testq	%r12, %r12
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB3_68
# BB#66:
	testq	%rax, %rax
	je	.LBB3_68
# BB#67:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB3_68
.LBB3_13:
	xorl	%ecx, %ecx
.LBB3_14:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_16
# BB#15:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_16:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_18
# BB#17:
	callq	DisposeObject
	jmp	.LBB3_18
.Lfunc_end3:
	.size	CrossAddTag, .Lfunc_end3-CrossAddTag
	.cfi_endproc

	.globl	CrossExpand
	.p2align	4, 0x90
	.type	CrossExpand,@function
CrossExpand:                            # @CrossExpand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$1112, %rsp             # imm = 0x458
.Lcfi46:
	.cfi_def_cfa_offset 1168
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %r12
	movb	32(%r12), %al
	andb	$-2, %al
	cmpb	$6, %al
	je	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movq	(%r12), %rax
	movq	8(%r12), %rcx
	cmpq	%rax, 8(%rcx)
	je	.LBB4_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r12), %rax
.LBB4_4:
	leaq	32(%r12), %r14
	addq	$16, %rax
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB4_5
# BB#6:
	subq	$8, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%r13, %rsi
	movq	%rbx, %rdx
	pushq	$0
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	pushq	%rbp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	movq	8(%r12), %rbx
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB4_7
# BB#8:
	cmpb	$2, %al
	je	.LBB4_10
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_10:                               # %.loopexit344
	movq	80(%rbx), %rbx
	movb	32(%rbp), %al
	addb	$-11, %al
	movl	$1, %ecx
	cmpb	$1, %al
	movq	%r15, 16(%rsp)          # 8-byte Spill
	ja	.LBB4_19
# BB#11:
	cmpb	$0, 64(%rbp)
	je	.LBB4_15
# BB#12:
	movq	%r14, %r15
	movq	%r13, %r14
	movq	%rbx, %r13
	leaq	64(%rbp), %rbx
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_16
# BB#13:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_17
# BB#14:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	orl	$126, %ecx
	jmp	.LBB4_18
.LBB4_15:
	movl	$2, %ecx
	jmp	.LBB4_19
.LBB4_16:
	movl	$134, %ecx
	jmp	.LBB4_18
.LBB4_17:
	movl	$128, %ecx
.LBB4_18:
	movq	%r13, %rbx
	movq	%r14, %r13
	movq	%r15, %r14
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB4_19:
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leaq	32(%rbp), %r13
	cmpb	$1, %cl
	movq	%rcx, %rbp
	jg	.LBB4_24
# BB#20:
	cmpb	$-128, %bpl
	je	.LBB4_27
# BB#21:
	cmpb	$-122, %bpl
	je	.LBB4_27
# BB#22:
	cmpb	$1, %bpl
	jne	.LBB4_58
# BB#23:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$10, %edi
	movl	$4, %esi
	movl	$.L.str.11, %edx
	jmp	.LBB4_38
.LBB4_24:
	cmpb	$2, %bpl
	je	.LBB4_37
# BB#25:
	cmpb	$126, %bpl
	je	.LBB4_39
# BB#26:
	cmpb	$127, %bpl
	jne	.LBB4_58
.LBB4_27:
	movzwl	41(%rbx), %eax
	testb	$2, %al
	jne	.LBB4_29
# BB#28:                                # %.thread335.thread
	movq	%rbx, %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$8, %esi
	movl	$.L.str.15, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	pushq	$.L.str.16
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB4_63
.LBB4_29:
	movq	88(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_31
# BB#30:
	movq	%rbx, %rdi
	callq	CrossInit
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.LBB4_135
.LBB4_31:                               # %.thread
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	cmpb	$-116, 32(%rax)
	je	.LBB4_33
# BB#32:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_33:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movzwl	34(%rax), %ebx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	callq	crtab_getnext
	movl	%eax, %r15d
	movl	%ebx, %edi
	callq	FileName
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	addq	$5, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB4_35
# BB#34:
	subq	$8, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$7, %esi
	movl	$.L.str.20, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	pushq	%r15
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -16
.LBB4_35:
	leaq	80(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%rbp, %rdi
	callq	strlen
	movw	$95, 80(%rsp,%rax)
	movl	%r15d, %edi
	callq	StringInt
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	strcat
	movl	$11, %edi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	MakeWord
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edx
	callq	CrossMake
	movq	%rax, %rbx
	movzbl	zz_lengths(%rbp), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_96
# BB#36:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_97
.LBB4_37:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$10, %edi
	movl	$5, %esi
	movl	$.L.str.13, %edx
.LBB4_38:                               # %.thread335
	movl	$2, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r13, %r8
	jmp	.LBB4_59
.LBB4_39:
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	88(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB4_41
# BB#40:
	movq	%rbx, %rdi
	callq	CrossInit
	movq	88(%rbx), %r14
.LBB4_41:
	cmpq	MomentSym(%rip), %rbx
	movq	56(%rsp), %rax          # 8-byte Reload
	jne	.LBB4_46
# BB#42:
	cmpb	$110, 64(%rax)
	jne	.LBB4_46
# BB#43:
	cmpb	$111, 65(%rax)
	jne	.LBB4_46
# BB#44:
	cmpb	$119, 66(%rax)
	jne	.LBB4_46
# BB#45:
	cmpb	$0, 67(%rax)
	je	.LBB4_134
.LBB4_46:                               # %.thread381
	movzwl	41(%rbx), %eax
	xorl	%ecx, %ecx
	testb	$2, %al
	jne	.LBB4_48
# BB#47:
	movq	%rbx, %rdi
	callq	SymName
	movq	%rbp, %r10
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$6, %esi
	movl	$.L.str.15, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	movq	%r10, %rbp
	pushq	$.L.str.16
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset -16
	movl	$1, %ecx
.LBB4_48:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	24(%r14), %rax
	movq	24(%rax), %r13
	cmpq	%r14, %r13
	je	.LBB4_109
# BB#49:                                # %.preheader.lr.ph
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	64(%rax), %rbp
	.p2align	4, 0x90
.LBB4_50:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_51 Depth 2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB4_51:                               #   Parent Loop BB4_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB4_51
# BB#52:                                #   in Loop: Header=BB4_50 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB4_54
# BB#53:                                #   in Loop: Header=BB4_50 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_54:                               # %.loopexit
                                        #   in Loop: Header=BB4_50 Depth=1
	subq	$8, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	leaq	600(%rsp), %r8
	leaq	14(%rsp), %r9
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	pushq	%rax
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	DbRetrieve
	addq	$32, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB4_110
# BB#55:                                #   in Loop: Header=BB4_50 Depth=1
	movq	24(%r13), %r13
	cmpq	%r14, %r13
	jne	.LBB4_50
# BB#56:
	movq	%r15, %rbx
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_57
.LBB4_58:
	movq	no_fpos(%rip), %r8
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
.LBB4_59:                               # %.thread335
	callq	Error
.LBB4_60:                               # %.thread335
	cmpl	$2, %ebp
	jb	.LBB4_63
.LBB4_61:                               # %.thread335
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB4_63
# BB#62:
	movq	%rbx, %rdi
	callq	SymName
	movq	%rax, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	$64, %rbp
	movl	$10, %edi
	movl	$9, %esi
	movl	$.L.str.23, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r14, %r8
	pushq	%rbp
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset -16
.LBB4_63:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB4_65
# BB#64:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_66
.LBB4_65:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB4_66:
	movb	$2, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	%rbx, 80(%r13)
	movq	%rbx, %r15
	movq	48(%rbx), %rax
	cmpq	StartSym(%rip), %rax
	movq	%r13, %rbx
	je	.LBB4_72
# BB#67:                                # %.lr.ph.preheader
	leaq	80(%r13), %r14
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB4_68:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_70
# BB#69:                                #   in Loop: Header=BB4_68 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_71
	.p2align	4, 0x90
.LBB4_70:                               #   in Loop: Header=BB4_68 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_71:                               #   in Loop: Header=BB4_68 Depth=1
	movb	$2, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	(%r14), %rax
	movq	48(%rax), %rax
	leaq	80(%rbx), %r14
	movq	%rax, 80(%rbx)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	SetEnv
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	AttachEnv
	movq	80(%rbx), %rax
	movq	48(%rax), %rax
	cmpq	StartSym(%rip), %rax
	movq	%rbx, %rbp
	jne	.LBB4_68
.LBB4_72:                               # %._crit_edge
	movzbl	zz_lengths+82(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_74
# BB#73:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_75
.LBB4_74:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB4_75:
	movb	$82, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_77
# BB#76:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_78
.LBB4_77:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_78:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB4_81
# BB#79:
	testq	%rax, %rax
	je	.LBB4_81
# BB#80:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_81:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB4_84
# BB#82:
	testq	%rax, %rax
	movq	%r15, %rbx
	je	.LBB4_85
# BB#83:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB4_85
.LBB4_84:
	movq	%r15, %rbx
.LBB4_85:
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB4_86:
	movq	%r13, %rdi
	callq	DetachEnv
	movq	%rax, (%r15)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB4_90
# BB#87:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB4_91
# BB#88:
	testq	%rax, %rax
	je	.LBB4_91
# BB#89:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r13), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r13)
	movq	%r13, 24(%rcx)
	jmp	.LBB4_91
.LBB4_90:                               # %.thread382
	movq	$0, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB4_91:
	movq	%r12, %rdi
	callq	DisposeObject
	cmpb	$2, 32(%r13)
	je	.LBB4_93
# BB#92:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_93:
	cmpq	%rbx, 80(%r13)
	je	.LBB4_95
# BB#94:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_95:
	movq	%r13, %rax
	addq	$1112, %rsp             # imm = 0x458
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_96:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB4_97:
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	%al, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rbx, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_99
# BB#98:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_100
.LBB4_99:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_100:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB4_103
# BB#101:
	testq	%rax, %rax
	je	.LBB4_103
# BB#102:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_103:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB4_106
# BB#104:
	testq	%rax, %rax
	je	.LBB4_106
# BB#105:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_106:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	$0, (%rax)
	jne	.LBB4_120
# BB#107:
	movzbl	zz_lengths+148(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_118
# BB#108:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_119
.LBB4_109:
	movq	48(%rsp), %r14          # 8-byte Reload
	cmpl	$2, %ebp
	jae	.LBB4_61
	jmp	.LBB4_63
.LBB4_110:
	xorl	%edi, %edi
	callq	SwitchScope
	movl	$0, 44(%rsp)
	cmpq	OldCrossDb(%rip), %rbx
	je	.LBB4_112
# BB#111:
	leaq	44(%rsp), %rsi
	xorl	%edx, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	SetScope
.LBB4_112:
	movq	64(%rsp), %rsi
	movl	40(%rsp), %edx
	movzwl	6(%rsp), %edi
	callq	ReadFromFile
	movq	%rax, %r13
	cmpl	$0, 44(%rsp)
	movq	16(%rsp), %r14          # 8-byte Reload
	jle	.LBB4_115
# BB#113:                               # %.lr.ph352.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_114:                              # %.lr.ph352
                                        # =>This Inner Loop Header: Depth=1
	callq	PopScope
	incl	%ebp
	cmpl	44(%rsp), %ebp
	jl	.LBB4_114
.LBB4_115:                              # %._crit_edge353
	xorl	%edi, %edi
	callq	UnSwitchScope
	cmpq	OldCrossDb(%rip), %rbx
	je	.LBB4_117
# BB#116:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	AttachEnv
.LBB4_117:
	movq	%r15, %rbx
	movq	%r14, %r15
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_132
.LBB4_118:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_119:
	movb	$-108, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
.LBB4_120:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_122
# BB#121:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_123
.LBB4_122:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_123:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_126
# BB#124:
	testq	%rcx, %rcx
	je	.LBB4_126
# BB#125:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_126:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB4_129
# BB#127:
	testq	%rax, %rax
	je	.LBB4_129
# BB#128:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_129:
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB4_133
# BB#130:
	movq	OldCrossDb(%rip), %rdi
	subq	$8, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rax
	leaq	48(%rsp), %rbp
	leaq	72(%rsp), %rbx
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	88(%rsp), %rcx
	leaq	600(%rsp), %r8
	leaq	14(%rsp), %r9
	movl	$0, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	pushq	%rax
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	movq	%rdx, %rbx
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	callq	DbRetrieve
	addq	$32, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB4_60
# BB#131:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edi, %edi
	callq	SwitchScope
	movq	64(%rsp), %rsi
	movl	40(%rsp), %edx
	movzwl	6(%rsp), %edi
	callq	ReadFromFile
	movq	%rax, %r13
	xorl	%edi, %edi
	callq	UnSwitchScope
.LBB4_132:
	testq	%r13, %r13
	jne	.LBB4_86
	jmp	.LBB4_60
.LBB4_133:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB4_57:
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpl	$2, %ebp
	jae	.LBB4_61
	jmp	.LBB4_63
.LBB4_134:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	callq	StartMoment
	movq	%rax, %r13
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_132
.LBB4_135:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
.Lfunc_end4:
	.size	CrossExpand, .Lfunc_end4-CrossExpand
	.cfi_endproc

	.p2align	4, 0x90
	.type	crtab_getnext,@function
crtab_getnext:                          # @crtab_getnext
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 80
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r12
	movq	crossref_tab(%rip), %r14
	testq	%r14, %r14
	jne	.LBB5_4
# BB#1:
	movl	$808, %edi              # imm = 0x328
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB5_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$10, %edi
	movl	$1, %esi
	movl	$.L.str.59, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_3:                                # %crtab_new.exit
	movl	$100, (%r14)
	movq	%r14, %rdi
	addq	$4, %rdi
	xorl	%esi, %esi
	movl	$804, %edx              # imm = 0x324
	callq	memset
	movq	%r14, crossref_tab(%rip)
.LBB5_4:
	movzwl	%bx, %esi
	addq	%r12, %rsi
	movslq	(%r14), %rcx
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	movq	%rdx, %r13
	movq	8(%r14,%r13,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_6
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_6 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_10
.LBB5_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r12, 8(%rcx)
	jne	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=1
	cmpw	%bx, 16(%rcx)
	jne	.LBB5_9
# BB#8:
	movl	20(%rcx), %eax
	incl	%eax
	movl	%eax, 20(%rcx)
	jmp	.LBB5_25
.LBB5_10:                               # %._crit_edge
	movslq	4(%r14), %r15
	cmpl	(%r14), %r15d
	jne	.LBB5_21
# BB#11:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	leal	(%r15,%r15), %r13d
	movq	%r15, %rdi
	shlq	$4, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$10, %edi
	movl	$1, %esi
	movl	$.L.str.59, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_13:
	movl	%r13d, (%rbx)
	movl	$0, 4(%rbx)
	testl	%r15d, %r15d
	jle	.LBB5_15
# BB#14:                                # %.lr.ph.i.i
	movq	%rbx, %rdi
	addq	$8, %rdi
	decl	%r13d
	leaq	8(,%r13,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB5_15:                               # %crtab_new.exit.preheader.i
	movslq	(%r14), %r8
	testq	%r8, %r8
	jle	.LBB5_20
# BB#16:                                # %.lr.ph31.preheader.i
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph31.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_18 Depth 2
	movq	8(%r14,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB5_19
	.p2align	4, 0x90
.LBB5_18:                               # %.lr.ph.i
                                        #   Parent Loop BB5_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbp
	movzwl	16(%rdi), %eax
	addq	8(%rdi), %rax
	movslq	(%rbx), %rcx
	xorl	%edx, %edx
	divq	%rcx
	movq	8(%rbx,%rdx,8), %rax
	movq	%rax, (%rdi)
	movq	%rdi, 8(%rbx,%rdx,8)
	incl	4(%rbx)
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.LBB5_18
.LBB5_19:                               # %crtab_new.exit.i
                                        #   in Loop: Header=BB5_17 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB5_17
.LBB5_20:                               # %crtab_rehash.exit
	movq	%r14, %rdi
	callq	free
	movq	%rbx, crossref_tab(%rip)
	movslq	(%rbx), %rcx
	xorl	%edx, %edx
	movq	16(%rsp), %rax          # 8-byte Reload
	divq	%rcx
	movq	%rdx, %r13
	movq	%rbx, %r14
	movl	12(%rsp), %ebx          # 4-byte Reload
.LBB5_21:
	movl	$24, zz_size(%rip)
	movq	zz_free+192(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_22
# BB#23:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free+192(%rip)
	jmp	.LBB5_24
.LBB5_22:
	movq	no_fpos(%rip), %rsi
	movl	$24, %edi
	callq	GetMemory
	movq	crossref_tab(%rip), %r14
.LBB5_24:
	movq	%r12, 8(%rax)
	movw	%bx, 16(%rax)
	movq	8(%r14,%r13,8), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 8(%r14,%r13,8)
	incl	4(%r14)
	movl	$1, 20(%rax)
	movl	$1, %eax
.LBB5_25:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	crtab_getnext, .Lfunc_end5-crtab_getnext
	.cfi_endproc

	.globl	CrossSequence
	.p2align	4, 0x90
	.type	CrossSequence,@function
CrossSequence:                          # @CrossSequence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi101:
	.cfi_def_cfa_offset 656
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB6_1
# BB#3:
	movb	32(%rbp), %al
	andb	$-2, %al
	cmpb	$6, %al
	je	.LBB6_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_5:
	movb	41(%rbp), %al
	movb	%al, 7(%rsp)            # 1-byte Spill
	movzbl	%al, %r14d
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_6
# BB#7:
	cmpb	$2, %al
	je	.LBB6_9
# BB#8:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_9:                                # %.loopexit620
	movq	80(%rbx), %r13
	movq	88(%r13), %r12
	testq	%r12, %r12
	jne	.LBB6_11
# BB#10:
	movq	%r13, %rdi
	callq	CrossInit
	movq	88(%r13), %r12
.LBB6_11:
	cmpb	$-116, 32(%r12)
	je	.LBB6_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_13:
	movq	8(%rbp), %rax
	movq	8(%rax), %rax
	leaq	16(%rax), %rcx
	.p2align	4, 0x90
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbx
	leaq	16(%rbx), %rcx
	cmpb	$0, 32(%rbx)
	je	.LBB6_14
# BB#15:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_17
# BB#16:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_17:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_19
# BB#18:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_19:
	movq	%rax, %rcx
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	addq	$33, %rax
	cmpb	$2, %dl
	cmovbq	%rax, %rsi
	movzbl	(%rsi), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rdx
	movq	%rdx, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	cmpq	%rbp, 24(%rbp)
	jne	.LBB6_21
# BB#20:
	movq	%rbp, %rdi
	callq	DisposeObject
.LBB6_21:
	leaq	32(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	7(%rsp), %bpl           # 1-byte Reload
	movl	%ebp, %eax
	addb	$-128, %al
	cmpb	$6, %al
	ja	.LBB6_22
# BB#25:
	movzbl	%al, %eax
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_26:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$2, (%rax)
	je	.LBB6_28
# BB#27:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_28:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %ecx
	testb	$64, %ch
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	je	.LBB6_29
# BB#30:
	movq	8(%rax), %r13
	cmpq	%rax, %r13
	je	.LBB6_29
# BB#31:                                # %.preheader.lr.ph
	leaq	64(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_32:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_33 Depth 2
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB6_33:                               #   Parent Loop BB6_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB6_33
# BB#34:                                #   in Loop: Header=BB6_32 Depth=1
	movzwl	41(%rbp), %ecx
	testb	$32, %ch
	je	.LBB6_78
# BB#35:                                #   in Loop: Header=BB6_32 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 80(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 16(%rsp)
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB6_36
# BB#37:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_38
.LBB6_36:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB6_38:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$2, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movq	%rbp, 80(%r12)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB6_39
# BB#40:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_41
.LBB6_39:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB6_41:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_42
# BB#43:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_44
.LBB6_42:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_44:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB6_47
# BB#45:                                #   in Loop: Header=BB6_32 Depth=1
	testq	%rax, %rax
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_32 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_47:                               #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB6_50
# BB#48:                                #   in Loop: Header=BB6_32 Depth=1
	testq	%rax, %rax
	je	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_32 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_50:                               #   in Loop: Header=BB6_32 Depth=1
	movzbl	zz_lengths+82(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB6_51
# BB#52:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_53
.LBB6_51:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB6_53:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$82, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_54
# BB#55:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_56
.LBB6_54:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_56:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB6_59
# BB#57:                                #   in Loop: Header=BB6_32 Depth=1
	testq	%rax, %rax
	je	.LBB6_59
# BB#58:                                #   in Loop: Header=BB6_32 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_59:                               #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_61
# BB#60:                                #   in Loop: Header=BB6_32 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_61:                               #   in Loop: Header=BB6_32 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB6_62
# BB#63:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_64
.LBB6_62:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB6_64:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_65
# BB#66:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_67
.LBB6_65:                               #   in Loop: Header=BB6_32 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_67:                               #   in Loop: Header=BB6_32 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB6_70
# BB#68:                                #   in Loop: Header=BB6_32 Depth=1
	testq	%rax, %rax
	je	.LBB6_70
# BB#69:                                #   in Loop: Header=BB6_32 Depth=1
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_70:                               #   in Loop: Header=BB6_32 Depth=1
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB6_73
# BB#71:                                #   in Loop: Header=BB6_32 Depth=1
	testq	%rax, %rax
	je	.LBB6_73
# BB#72:                                #   in Loop: Header=BB6_32 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_73:                               #   in Loop: Header=BB6_32 Depth=1
	subq	$8, %rsp
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	88(%rsp), %rcx
	leaq	56(%rsp), %r8
	leaq	80(%rsp), %r9
	pushq	$0
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	pushq	%rax
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi114:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r12
	movq	8(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_75
# BB#74:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_75:                               #   in Loop: Header=BB6_32 Depth=1
	movq	%rbp, %r14
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_77
# BB#76:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_77:                               #   in Loop: Header=BB6_32 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%r15, %rdi
	callq	DisposeObject
	movq	80(%rbx), %rax
.LBB6_78:                               #   in Loop: Header=BB6_32 Depth=1
	movq	8(%r13), %r13
	cmpq	%rax, %r13
	jne	.LBB6_32
	jmp	.LBB6_79
.LBB6_1:
	cmpq	%rbp, 24(%rbp)
	jne	.LBB6_211
# BB#2:
	movq	%rbp, %rdi
	callq	DisposeObject
	jmp	.LBB6_211
.LBB6_29:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
.LBB6_79:                               # %.loopexit
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	DatabaseFileNum
	movl	%eax, %r15d
	movzwl	%r15w, %esi
	leaq	48(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	AppendToFile
	testq	%r12, %r12
	je	.LBB6_80
# BB#81:
	leaq	32(%r12), %r8
	movb	32(%r12), %al
	addb	$-11, %al
	cmpb	$2, %al
	movq	40(%rsp), %r13          # 8-byte Reload
	movb	7(%rsp), %bpl           # 1-byte Reload
	jae	.LBB6_82
# BB#83:
	cmpb	$0, 64(%r12)
	je	.LBB6_85
# BB#84:
	addq	$64, %r12
	cmpb	$-123, %bpl
	je	.LBB6_88
	jmp	.LBB6_94
.LBB6_22:
	cmpb	$127, %bpl
	jne	.LBB6_210
.LBB6_23:
	movq	8(%rsp), %r14           # 8-byte Reload
	movb	(%r14), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB6_24
# BB#141:
	cmpb	$0, 64(%rbx)
	je	.LBB6_211
# BB#142:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_143
# BB#144:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_145
.LBB6_80:
	leaq	80(%rsp), %r12
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	60(%r13), %edi
	incl	%edi
	movl	%edi, 60(%r13)
	callq	StringFiveInt
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movb	7(%rsp), %bpl           # 1-byte Reload
	cmpb	$-123, %bpl
	je	.LBB6_88
	jmp	.LBB6_94
.LBB6_151:
	cmpb	$1, 33(%r12)
	jne	.LBB6_153
# BB#152:
	movq	40(%r12), %rdi
	callq	DisposeObject
.LBB6_153:                              # %._crit_edge683
	movq	%rbx, 40(%r12)
	cmpq	%rbx, 24(%rbx)
	je	.LBB6_155
# BB#154:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.43, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_155:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	DatabaseFileNum
	movw	%ax, 34(%r12)
	movb	$1, 33(%r12)
	movq	40(%r12), %rax
	cmpb	$2, 32(%rax)
	je	.LBB6_157
# BB#156:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.44, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	40(%r12), %rax
.LBB6_157:
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	jne	.LBB6_158
	jmp	.LBB6_187
.LBB6_105:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB6_107
# BB#106:
	callq	DisposeObject
.LBB6_107:
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	(%rax), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB6_109
# BB#108:
	cmpb	$0, 64(%rbx)
	jne	.LBB6_110
.LBB6_109:
	movq	%rbx, %rdi
	callq	DisposeObject
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.36, %esi
	callq	MakeWord
	movq	%rax, %rbx
.LBB6_110:
	movq	%rbx, 72(%r12)
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	je	.LBB6_211
# BB#111:                               # %.preheader613.lr.ph
	movl	$38, %r15d
	movl	$65, %r14d
	.p2align	4, 0x90
.LBB6_112:                              # %.preheader613
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_113 Depth 2
                                        #     Child Loop BB6_124 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB6_113:                              #   Parent Loop BB6_112 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_113
# BB#114:                               #   in Loop: Header=BB6_112 Depth=1
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB6_116
# BB#115:                               #   in Loop: Header=BB6_112 Depth=1
	cmpb	$0, 64(%rbx)
	jne	.LBB6_117
.LBB6_116:                              # %.loopexit614
                                        #   in Loop: Header=BB6_112 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.38, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_117:                              #   in Loop: Header=BB6_112 Depth=1
	movb	48(%rbx), %al
	movl	%eax, %ecx
	addb	$-128, %cl
	cmpb	$6, %cl
	ja	.LBB6_118
# BB#120:                               #   in Loop: Header=BB6_112 Depth=1
	movzbl	%cl, %ecx
	btl	%ecx, %r15d
	jae	.LBB6_121
# BB#122:                               #   in Loop: Header=BB6_112 Depth=1
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB6_125
# BB#123:                               #   in Loop: Header=BB6_112 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB6_124:                              #   Parent Loop BB6_112 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	leaq	16(%rax), %rax
	je	.LBB6_124
.LBB6_125:                              # %.loopexit612
                                        #   in Loop: Header=BB6_112 Depth=1
	movq	NewCrossDb(%rip), %rdi
	movq	72(%r12), %rcx
	addq	$64, %rcx
	movq	no_fpos(%rip), %r8
	movslq	52(%rbx), %r10
	movl	56(%rbx), %eax
	movzwl	50(%rbx), %r11d
	leaq	64(%rbx), %r9
	movl	$1, %esi
	movq	%r13, %rdx
	pushq	$0
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi119:
	.cfi_adjust_cfa_offset -32
	movq	(%rbp), %rbp
	movq	8(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_126
# BB#127:                               #   in Loop: Header=BB6_112 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB6_128
	.p2align	4, 0x90
.LBB6_121:                              #   in Loop: Header=BB6_112 Depth=1
	btl	%ecx, %r14d
	jb	.LBB6_132
	.p2align	4, 0x90
.LBB6_118:                              #   in Loop: Header=BB6_112 Depth=1
	addb	$-126, %al
	cmpb	$2, %al
	jb	.LBB6_132
# BB#119:                               #   in Loop: Header=BB6_112 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.39, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB6_132
.LBB6_126:                              #   in Loop: Header=BB6_112 Depth=1
	xorl	%ecx, %ecx
.LBB6_128:                              #   in Loop: Header=BB6_112 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_130
# BB#129:                               #   in Loop: Header=BB6_112 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_130:                              #   in Loop: Header=BB6_112 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB6_132
# BB#131:                               #   in Loop: Header=BB6_112 Depth=1
	callq	DisposeObject
	.p2align	4, 0x90
.LBB6_132:                              # %.backedge
                                        #   in Loop: Header=BB6_112 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB6_112
	jmp	.LBB6_211
.LBB6_133:
	movb	33(%r12), %al
	cmpb	$1, %al
	je	.LBB6_136
# BB#134:
	testb	%al, %al
	jne	.LBB6_137
# BB#135:
	movq	%r13, %rdi
	callq	SymName
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$13, %esi
	movl	$.L.str.40, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	pushq	$.L.str.8
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB6_211
.LBB6_210:
	movq	no_fpos(%rip), %rbx
	movl	%r14d, %edi
	callq	Image
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.48, %edx
	movl	$0, %ecx
	movl	$.L.str.49, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	pushq	%rbp
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi127:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB6_211
.LBB6_24:
	movq	64(%r12), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$10, %edi
	movl	$14, %esi
	movl	$.L.str.42, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	callq	Error
	jmp	.LBB6_211
.LBB6_82:
	movl	$10, %edi
	movl	$10, %esi
	movl	$.L.str.30, %edx
	jmp	.LBB6_86
.LBB6_85:
	movl	$10, %edi
	movl	$11, %esi
	movl	$.L.str.33, %edx
.LBB6_86:
	movl	$2, %ecx
	movl	$.L.str.31, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	$.L.str.32, %r12d
	cmpb	$-123, %bpl
	jne	.LBB6_94
.LBB6_88:
	movq	72(%r13), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_90
# BB#89:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	SymName
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	SymName
	movq	%rax, %r10
	subq	$8, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$12, %esi
	movl	$.L.str.34, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r8
	movq	%r15, %r9
	pushq	$.L.str.8
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset -32
	movl	$11, %edi
	movl	$.L.str.36, %esi
	movq	%rbp, %rdx
	callq	MakeWord
	movq	%rax, %rcx
	movq	%rcx, 72(%r13)
.LBB6_90:
	movb	32(%rcx), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB6_92
# BB#91:
	cmpb	$0, 64(%rcx)
	jne	.LBB6_93
.LBB6_92:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.37, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	72(%r13), %rcx
.LBB6_93:
	movq	NewCrossDb(%rip), %rdi
	addq	$64, %rcx
	movq	no_fpos(%rip), %r8
	movslq	48(%rsp), %rax
	movl	16(%rsp), %ebp
	movl	$1, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r9
	pushq	$0
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi137:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB6_103
.LBB6_94:
	movl	$11, %edi
	movq	%r12, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	MakeWord
	movl	%ebp, %ecx
	movq	%rax, %rbp
	movb	%cl, 48(%rbp)
	movw	%r15w, 50(%rbp)
	movl	48(%rsp), %eax
	movl	%eax, 52(%rbp)
	movl	16(%rsp), %eax
	movl	%eax, 56(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_95
# BB#96:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_97
.LBB6_95:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_97:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB6_100
# BB#98:
	testq	%rax, %rax
	je	.LBB6_100
# BB#99:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_100:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB6_103
# BB#101:
	testq	%rax, %rax
	je	.LBB6_103
# BB#102:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_103:
	movq	%rbx, %rdi
	callq	DisposeObject
	testq	%r14, %r14
	je	.LBB6_211
# BB#104:
	movq	%r14, %rdi
	callq	DisposeObject
	jmp	.LBB6_211
.LBB6_136:
	movq	40(%r12), %rdi
	leaq	52(%r12), %rdx
	leaq	56(%r12), %rcx
	movzwl	34(%r12), %esi
	callq	AppendToFile
	movq	40(%r12), %rdi
	callq	DisposeObject
	movq	$0, 40(%r12)
	movb	$2, 33(%r12)
.LBB6_137:
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	(%rax), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB6_139
# BB#138:
	cmpb	$0, 64(%rbx)
	jne	.LBB6_140
.LBB6_139:
	movq	%rbx, %rdi
	callq	DisposeObject
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.36, %esi
	callq	MakeWord
	movq	%rax, %rbx
.LBB6_140:
	movq	NewCrossDb(%rip), %rdi
	leaq	64(%rbx), %rcx
	leaq	32(%rbx), %r8
	movslq	52(%r12), %rax
	movl	56(%r12), %ebp
	movzwl	34(%r12), %r10d
	xorl	%esi, %esi
	movl	$.L.str.41, %r9d
	movq	%r13, %rdx
	pushq	$1
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi142:
	.cfi_adjust_cfa_offset -32
	movq	%rbx, %rdi
	callq	DisposeObject
	jmp	.LBB6_211
.LBB6_143:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_145:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB6_148
# BB#146:
	testq	%rax, %rax
	je	.LBB6_148
# BB#147:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_148:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_150
# BB#149:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_150:
	movzwl	34(%rbx), %eax
	movw	%ax, 50(%rbx)
	movb	%bpl, 48(%rbx)
	jmp	.LBB6_211
.LBB6_181:                              # %.thread
                                        #   in Loop: Header=BB6_158 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB6_185:                              #   in Loop: Header=BB6_158 Depth=1
	movq	%rbp, %rdi
	callq	DisposeObject
	jmp	.LBB6_186
	.p2align	4, 0x90
.LBB6_158:                              # %.preheader618
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_159 Depth 2
                                        #     Child Loop BB6_165 Depth 2
                                        #     Child Loop BB6_178 Depth 2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB6_159:                              #   Parent Loop BB6_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_159
# BB#160:                               #   in Loop: Header=BB6_158 Depth=1
	cmpb	$10, %al
	jne	.LBB6_186
# BB#161:                               #   in Loop: Header=BB6_158 Depth=1
	cmpq	%rbx, 8(%rbx)
	jne	.LBB6_163
# BB#162:                               #   in Loop: Header=BB6_158 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.45, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_163:                              #   in Loop: Header=BB6_158 Depth=1
	movq	80(%rbx), %rax
	movzwl	41(%rax), %ecx
	testb	$1, %cl
	jne	.LBB6_164
# BB#176:                               #   in Loop: Header=BB6_158 Depth=1
	cmpb	$-110, 32(%rax)
	jne	.LBB6_186
# BB#177:                               #   in Loop: Header=BB6_158 Depth=1
	movq	8(%rbx), %rbp
	.p2align	4, 0x90
.LBB6_178:                              #   Parent Loop BB6_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	cmpq	$93, %rax
	ja	.LBB6_180
# BB#179:                               #   in Loop: Header=BB6_178 Depth=2
	jmpq	*.LJTI6_1(,%rax,8)
.LBB6_180:                              #   in Loop: Header=BB6_158 Depth=1
	leaq	32(%rbp), %rdx
	movl	$11, %edi
	movl	$.L.str.46, %esi
	callq	MakeWord
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rcx
	cmpq	%rbp, %rcx
	je	.LBB6_181
# BB#182:                               #   in Loop: Header=BB6_158 Depth=1
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbp), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_185
# BB#183:                               #   in Loop: Header=BB6_158 Depth=1
	testq	%rcx, %rcx
	je	.LBB6_185
# BB#184:                               #   in Loop: Header=BB6_158 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB6_185
	.p2align	4, 0x90
.LBB6_164:                              #   in Loop: Header=BB6_158 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB6_165:                              #   Parent Loop BB6_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB6_165
# BB#166:                               #   in Loop: Header=BB6_158 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB6_167
# BB#168:                               #   in Loop: Header=BB6_158 Depth=1
	cmpb	$0, 64(%rbp)
	je	.LBB6_186
# BB#169:                               #   in Loop: Header=BB6_158 Depth=1
	movzwl	34(%rbp), %eax
	movw	%ax, 50(%rbp)
	movb	$126, 48(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_170
# BB#171:                               #   in Loop: Header=BB6_158 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_172
.LBB6_167:                              #   in Loop: Header=BB6_158 Depth=1
	addq	$32, %rbp
	movq	40(%r12), %rax
	movq	80(%rax), %rdi
	callq	SymName
	movq	%rax, %r9
	movl	$10, %edi
	movl	$15, %esi
	movl	$.L.str.42, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB6_186
.LBB6_170:                              #   in Loop: Header=BB6_158 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_172:                              #   in Loop: Header=BB6_158 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_174
# BB#173:                               #   in Loop: Header=BB6_158 Depth=1
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_174:                              #   in Loop: Header=BB6_158 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_186
# BB#175:                               #   in Loop: Header=BB6_158 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	.p2align	4, 0x90
.LBB6_186:                              # %.loopexit617
                                        #   in Loop: Header=BB6_158 Depth=1
	movq	8(%r14), %r14
	cmpq	40(%r12), %r14
	jne	.LBB6_158
.LBB6_187:                              # %._crit_edge650
	cmpq	%r12, 8(%r12)
	je	.LBB6_211
# BB#188:
	leaq	52(%r12), %rdx
	leaq	56(%r12), %rcx
	movzwl	34(%r12), %esi
	movq	%r14, %rdi
	callq	AppendToFile
	movq	40(%r12), %rdi
	callq	DisposeObject
	movq	$0, 40(%r12)
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	jne	.LBB6_190
	jmp	.LBB6_209
	.p2align	4, 0x90
.LBB6_208:                              #   in Loop: Header=BB6_190 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	je	.LBB6_209
.LBB6_190:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_191 Depth 2
	leaq	16(%rbp), %rbx
	jmp	.LBB6_191
	.p2align	4, 0x90
.LBB6_212:                              #   in Loop: Header=BB6_191 Depth=2
	addq	$16, %rbx
.LBB6_191:                              #   Parent Loop BB6_190 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_212
# BB#192:                               #   in Loop: Header=BB6_190 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB6_194
# BB#193:                               #   in Loop: Header=BB6_190 Depth=1
	cmpb	$0, 64(%rbx)
	jne	.LBB6_195
.LBB6_194:                              # %.loopexit616
                                        #   in Loop: Header=BB6_190 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.47, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_195:                              #   in Loop: Header=BB6_190 Depth=1
	movb	48(%rbx), %al
	movl	%eax, %ecx
	cmpb	$-124, %al
	jg	.LBB6_200
# BB#196:                               #   in Loop: Header=BB6_190 Depth=1
	addb	$127, %cl
	cmpb	$2, %cl
	jb	.LBB6_208
# BB#197:                               #   in Loop: Header=BB6_190 Depth=1
	cmpb	$-128, %al
	je	.LBB6_198
	jmp	.LBB6_202
	.p2align	4, 0x90
.LBB6_200:                              #   in Loop: Header=BB6_190 Depth=1
	addb	$-126, %cl
	cmpb	$2, %cl
	jae	.LBB6_201
.LBB6_198:                              #   in Loop: Header=BB6_190 Depth=1
	movq	NewCrossDb(%rip), %rdi
	leaq	32(%rbx), %r8
	addq	$64, %rbx
	movslq	52(%r12), %r10
	movl	56(%r12), %eax
	movzwl	34(%r12), %r11d
	xorl	%esi, %esi
	movl	$.L.str.41, %r9d
	movq	%r13, %rdx
	movq	%rbx, %rcx
	pushq	$1
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi147:
	.cfi_adjust_cfa_offset -32
	movq	(%rbp), %rbp
	movq	8(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_199
# BB#203:                               #   in Loop: Header=BB6_190 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB6_204
.LBB6_201:                              #   in Loop: Header=BB6_190 Depth=1
	cmpb	$-123, %al
	je	.LBB6_208
.LBB6_202:                              #   in Loop: Header=BB6_190 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.39, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB6_190
	jmp	.LBB6_209
.LBB6_199:                              #   in Loop: Header=BB6_190 Depth=1
	xorl	%ecx, %ecx
.LBB6_204:                              #   in Loop: Header=BB6_190 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_206
# BB#205:                               #   in Loop: Header=BB6_190 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_206:                              #   in Loop: Header=BB6_190 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB6_208
# BB#207:                               #   in Loop: Header=BB6_190 Depth=1
	callq	DisposeObject
	jmp	.LBB6_208
.LBB6_209:                              # %._crit_edge
	movb	$2, 33(%r12)
.LBB6_211:                              # %.loopexit615
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	CrossSequence, .Lfunc_end6-CrossSequence
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_23
	.quad	.LBB6_26
	.quad	.LBB6_26
	.quad	.LBB6_151
	.quad	.LBB6_105
	.quad	.LBB6_26
	.quad	.LBB6_133
.LJTI6_1:
	.quad	.LBB6_178
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_186
	.quad	.LBB6_186
	.quad	.LBB6_186
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_186
	.quad	.LBB6_186
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_186
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_186
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_180
	.quad	.LBB6_186
	.quad	.LBB6_186

	.text
	.globl	CrossClose
	.p2align	4, 0x90
	.type	CrossClose,@function
CrossClose:                             # @CrossClose
	.cfi_startproc
# BB#0:
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB7_53
# BB#1:
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 56
	subq	$1624, %rsp             # imm = 0x658
.Lcfi154:
	.cfi_def_cfa_offset 1680
.Lcfi155:
	.cfi_offset %rbx, -56
.Lcfi156:
	.cfi_offset %r12, -48
.Lcfi157:
	.cfi_offset %r13, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
	movq	RootCross(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_46
# BB#2:                                 # %.preheader133
	movq	8(%rax), %r15
	cmpq	%rax, %r15
	je	.LBB7_46
	.p2align	4, 0x90
.LBB7_3:                                # %.preheader132
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #     Child Loop BB7_11 Depth 2
                                        #       Child Loop BB7_12 Depth 3
                                        #         Child Loop BB7_13 Depth 4
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB7_4
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	64(%r15), %r14
	cmpb	$-116, %al
	je	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.50, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_7:                                # %.preheader129
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	8(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB7_44
# BB#8:                                 # %.preheader.lr.ph.lr.ph
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	52(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	56(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jmp	.LBB7_11
.LBB7_9:                                #   in Loop: Header=BB7_11 Depth=2
	cmpl	$5, %ebp
	jne	.LBB7_43
# BB#10:                                #   in Loop: Header=BB7_11 Depth=2
	movq	no_fpos(%rip), %rbx
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$10, %edi
	movl	$17, %esi
	movl	$.L.str.53, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	movl	4(%rsp), %ebp           # 4-byte Reload
	pushq	$.L.str.10
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi163:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB7_43
	.p2align	4, 0x90
.LBB7_11:                               # %.preheader.lr.ph
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_12 Depth 3
                                        #         Child Loop BB7_13 Depth 4
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB7_12:                               # %.preheader
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_13 Depth 4
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB7_13:                               #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_11 Depth=2
                                        #       Parent Loop BB7_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB7_13
# BB#14:                                #   in Loop: Header=BB7_12 Depth=3
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB7_16
# BB#15:                                #   in Loop: Header=BB7_12 Depth=3
	cmpb	$0, 64(%rbx)
	jne	.LBB7_17
.LBB7_16:                               # %.loopexit
                                        #   in Loop: Header=BB7_12 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_17:                               #   in Loop: Header=BB7_12 Depth=3
	leaq	32(%rbx), %r13
	movb	48(%rbx), %al
	cmpb	$-127, %al
	jg	.LBB7_22
# BB#18:                                #   in Loop: Header=BB7_12 Depth=3
	cmpb	$-128, %al
	jne	.LBB7_26
# BB#19:                                #   in Loop: Header=BB7_12 Depth=3
	movb	33(%r15), %al
	cmpb	$1, %al
	je	.LBB7_29
# BB#20:                                #   in Loop: Header=BB7_12 Depth=3
	testb	%al, %al
	jne	.LBB7_30
# BB#21:                                #   in Loop: Header=BB7_12 Depth=3
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$18, %esi
	movl	$.L.str.54, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	pushq	$.L.str.9
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movl	36(%rsp), %ebp          # 4-byte Reload
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi168:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_35
	.p2align	4, 0x90
.LBB7_22:                               #   in Loop: Header=BB7_12 Depth=3
	cmpb	$-126, %al
	jne	.LBB7_27
# BB#23:                                #   in Loop: Header=BB7_12 Depth=3
	movq	72(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_12 Depth=3
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %r14
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$21, %esi
	movl	$.L.str.55, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	movq	16(%rsp), %r14          # 8-byte Reload
	pushq	$.L.str.9
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi173:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.36, %esi
	callq	MakeWord
	movq	%rax, %rcx
	movq	%rcx, 72(%r15)
.LBB7_25:                               #   in Loop: Header=BB7_12 Depth=3
	movq	NewCrossDb(%rip), %rdi
	addq	$64, %rcx
	movq	no_fpos(%rip), %r8
	movslq	52(%rbx), %rax
	movl	56(%rbx), %ebp
	movzwl	50(%rbx), %r10d
	leaq	64(%rbx), %r9
	movl	$1, %esi
	movq	%r14, %rdx
	pushq	$0
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi178:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_34
.LBB7_26:                               #   in Loop: Header=BB7_12 Depth=3
	cmpb	$-127, %al
	jne	.LBB7_28
	jmp	.LBB7_36
.LBB7_27:                               #   in Loop: Header=BB7_12 Depth=3
	cmpb	$127, %al
	je	.LBB7_38
.LBB7_28:                               #   in Loop: Header=BB7_12 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB7_35
.LBB7_29:                               #   in Loop: Header=BB7_12 Depth=3
	movq	40(%r15), %rdi
	movzwl	34(%r15), %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	AppendToFile
	movq	40(%r15), %rdi
	callq	DisposeObject
	movq	$0, 40(%r15)
	movb	$2, 33(%r15)
.LBB7_30:                               #   in Loop: Header=BB7_12 Depth=3
	movb	(%r13), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB7_32
# BB#31:                                #   in Loop: Header=BB7_12 Depth=3
	cmpb	$0, 64(%rbx)
	jne	.LBB7_33
.LBB7_32:                               #   in Loop: Header=BB7_12 Depth=3
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.36, %esi
	callq	MakeWord
	movq	%rax, %rbx
.LBB7_33:                               #   in Loop: Header=BB7_12 Depth=3
	movq	NewCrossDb(%rip), %rdi
	leaq	64(%rbx), %rcx
	addq	$32, %rbx
	movslq	52(%r15), %rax
	movl	56(%r15), %ebp
	movzwl	34(%r15), %r10d
	xorl	%esi, %esi
	movl	$.L.str.41, %r9d
	movq	%r14, %rdx
	movq	%rbx, %r8
	pushq	$1
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi180:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi183:
	.cfi_adjust_cfa_offset -32
.LBB7_34:                               # %.backedge131
                                        #   in Loop: Header=BB7_12 Depth=3
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB7_35:                               # %.backedge131
                                        #   in Loop: Header=BB7_12 Depth=3
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB7_12
	jmp	.LBB7_44
	.p2align	4, 0x90
.LBB7_36:                               #   in Loop: Header=BB7_11 Depth=2
	cmpl	$4, %ebp
	jg	.LBB7_40
# BB#37:                                #   in Loop: Header=BB7_11 Depth=2
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %r14
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$19, %esi
	movl	$.L.str.52, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	movq	16(%rsp), %r14          # 8-byte Reload
	pushq	$.L.str.10
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi188:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_42
	.p2align	4, 0x90
.LBB7_38:                               #   in Loop: Header=BB7_11 Depth=2
	cmpl	$4, %ebp
	jg	.LBB7_9
# BB#39:                                #   in Loop: Header=BB7_11 Depth=2
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	movl	$10, %edi
	movl	$16, %esi
	movl	$.L.str.52, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	pushq	$.L.str.10
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movl	36(%rsp), %ebp          # 4-byte Reload
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi193:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_43
.LBB7_40:                               #   in Loop: Header=BB7_11 Depth=2
	cmpl	$5, %ebp
	jne	.LBB7_42
# BB#41:                                #   in Loop: Header=BB7_11 Depth=2
	movq	no_fpos(%rip), %r14
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SymName
	movq	%rax, %rbp
	movl	$10, %edi
	movl	$20, %esi
	movl	$.L.str.53, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r14, %r8
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%rbp, %r9
	pushq	$.L.str.10
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.12
.Lcfi195:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi196:
	.cfi_adjust_cfa_offset -16
.LBB7_42:                               #   in Loop: Header=BB7_11 Depth=2
	movq	NewCrossDb(%rip), %rdi
	movq	no_fpos(%rip), %r8
	leaq	64(%rbx), %r9
	movslq	52(%rbx), %rax
	movl	56(%rbx), %ebp
	movzwl	50(%rbx), %ebx
	movl	$1, %esi
	movl	$.L.str.36, %ecx
	movq	%r14, %rdx
	pushq	$0
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi198:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi201:
	.cfi_adjust_cfa_offset -32
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB7_43:                               # %.outer.backedge
                                        #   in Loop: Header=BB7_11 Depth=2
	incl	%ebp
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB7_11
	.p2align	4, 0x90
.LBB7_44:                               # %.loopexit130
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	8(%r15), %r15
	cmpq	RootCross(%rip), %r15
	jne	.LBB7_3
# BB#45:                                # %.loopexit134.loopexit
	movq	%r14, 24(%rsp)
.LBB7_46:                               # %.loopexit134
	movq	$0, 72(%rsp)
	movq	OldCrossDb(%rip), %rdi
	subq	$8, %rsp
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rbp
	leaq	44(%rsp), %rbx
	leaq	48(%rsp), %rax
	leaq	28(%rsp), %rsi
	leaq	32(%rsp), %rdx
	leaq	88(%rsp), %rcx
	leaq	600(%rsp), %r8
	leaq	10(%rsp), %r9
	pushq	%rbp
.Lcfi203:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi205:
	.cfi_adjust_cfa_offset 8
	callq	DbRetrieveNext
	addq	$32, %rsp
.Lcfi206:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	je	.LBB7_52
# BB#47:                                # %.lr.ph.preheader
	leaq	1104(%rsp), %r14
	leaq	80(%rsp), %r15
	leaq	592(%rsp), %r13
	leaq	2(%rsp), %r12
	.p2align	4, 0x90
.LBB7_48:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 20(%rsp)
	jne	.LBB7_51
# BB#49:                                #   in Loop: Header=BB7_48 Depth=1
	movzwl	2(%rsp), %edi
	callq	FileName
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movb	$0, 1101(%rsp,%rax)
	movl	$.L.str.3, %esi
	movq	%r14, %rdi
	callq	FileNum
	testw	%ax, %ax
	jne	.LBB7_51
# BB#50:                                #   in Loop: Header=BB7_48 Depth=1
	movq	NewCrossDb(%rip), %rdi
	movq	24(%rsp), %rdx
	movq	no_fpos(%rip), %r8
	movl	36(%rsp), %eax
	movq	%rbx, %r10
	movq	%rbp, %rbx
	movzwl	2(%rsp), %ebp
	xorl	%esi, %esi
	movq	%r15, %rcx
	movq	%r13, %r9
	pushq	$0
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movq	%rbx, %rbp
	movq	%r10, %rbx
.Lcfi210:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi211:
	.cfi_adjust_cfa_offset -32
	.p2align	4, 0x90
.LBB7_51:                               # %.backedge
                                        #   in Loop: Header=BB7_48 Depth=1
	movq	OldCrossDb(%rip), %rdi
	subq	$8, %rsp
.Lcfi212:
	.cfi_adjust_cfa_offset 8
	leaq	28(%rsp), %rsi
	leaq	32(%rsp), %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	movq	%r12, %r9
	pushq	%rbp
.Lcfi213:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	leaq	64(%rsp), %rax
	pushq	%rax
.Lcfi215:
	.cfi_adjust_cfa_offset 8
	callq	DbRetrieveNext
	addq	$32, %rsp
.Lcfi216:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB7_48
.LBB7_52:                               # %._crit_edge
	movq	OldCrossDb(%rip), %rdi
	callq	DbClose
	movq	NewCrossDb(%rip), %rdi
	movl	$1, %esi
	callq	DbConvert
	addq	$1624, %rsp             # imm = 0x658
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
.LBB7_53:
	retq
.Lfunc_end7:
	.size	CrossClose, .Lfunc_end7-CrossClose
	.cfi_endproc

	.type	RootCross,@object       # @RootCross
	.local	RootCross
	.comm	RootCross,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"automatically generated tag %s&%d is too long"
	.size	.L.str.1, 46

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"assert failed in %s"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"CrossExpand: x!"
	.size	.L.str.5, 16

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"CrossExpand: #args!"
	.size	.L.str.6, 20

	.type	nbt,@object             # @nbt
	.local	nbt
	.comm	nbt,16,16
	.type	nft,@object             # @nft
	.local	nft
	.comm	nft,16,16
	.type	ntarget,@object         # @ntarget
	.local	ntarget
	.comm	ntarget,8,8
	.type	nenclose,@object        # @nenclose
	.local	nenclose
	.comm	nenclose,8,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ClosureExpand: type(y) != CLOSURE!"
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"preceding"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"foll_or_prec"
	.size	.L.str.9, 13

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"following"
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"value of right parameter of %s is not a simple word"
	.size	.L.str.11, 52

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"&&"
	.size	.L.str.12, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"value of right parameter of %s is an empty word"
	.size	.L.str.13, 48

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"symbol %s used in cross reference has no %s parameter"
	.size	.L.str.15, 54

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"@Tag"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"CrossExpand: db!"
	.size	.L.str.17, 17

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"CrossExpand/CROSS_FOLL: cs == nilobj!"
	.size	.L.str.18, 38

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"CrossExpand/CROSS_FOLL: type(cs)!"
	.size	.L.str.19, 34

	.type	crossref_tab,@object    # @crossref_tab
	.local	crossref_tab
	.comm	crossref_tab,8,8
	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"automatically generated tag %s_%d is too long"
	.size	.L.str.20, 46

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"CrossExpand ctype"
	.size	.L.str.22, 18

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"unresolved cross reference %s%s%s"
	.size	.L.str.23, 34

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"CrossExpand: type(res) != CLOSURE!"
	.size	.L.str.24, 35

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"CrossExpand: actual(res) != sym!"
	.size	.L.str.25, 33

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"CrossSequence: type(x)!"
	.size	.L.str.26, 24

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"CrossSequence: type(tmp)!"
	.size	.L.str.27, 26

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"CrossSequence: cs!"
	.size	.L.str.28, 19

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"CrossSequence/GALL_FOLL: type(val)!"
	.size	.L.str.29, 36

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s parameter is not a word"
	.size	.L.str.30, 27

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"@Key"
	.size	.L.str.31, 5

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"badkey"
	.size	.L.str.32, 7

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s parameter is an empty word"
	.size	.L.str.33, 30

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"no %s galley target precedes this %s%s%s"
	.size	.L.str.34, 41

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"none"
	.size	.L.str.36, 5

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"CrossSequence: gall_tag!"
	.size	.L.str.37, 25

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"CrossSequence: GALL_TARG y!"
	.size	.L.str.38, 28

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"CrossSequence: cs_type!"
	.size	.L.str.39, 24

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"no %s precedes this %s%s%s"
	.size	.L.str.40, 27

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"0"
	.size	.L.str.41, 2

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"tag of %s is not a simple word"
	.size	.L.str.42, 31

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"CrossSeq: Up(tag)!"
	.size	.L.str.43, 19

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"CrossSequence: target_val!"
	.size	.L.str.44, 27

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"CrossSequence: Down(PAR)!"
	.size	.L.str.45, 26

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"???"
	.size	.L.str.46, 4

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"CrossSeq: non-WORD or empty tag!"
	.size	.L.str.47, 33

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"assert failed in %s %s"
	.size	.L.str.48, 23

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"CrossSequence:"
	.size	.L.str.49, 15

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"CrossClose: type(cs)!"
	.size	.L.str.50, 22

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"CrossClose: GALL_TARG y!"
	.size	.L.str.51, 25

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"no %s follows this %s%s%s"
	.size	.L.str.52, 26

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"and more undefined %s%s%s"
	.size	.L.str.53, 26

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"no %s follows or precedes this %s%s%s"
	.size	.L.str.54, 38

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"no %s precedes or follows this %s%s%s"
	.size	.L.str.55, 38

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"CrossClose: unknown cs_type!"
	.size	.L.str.56, 29

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"automatically generated tag is too long (contains %s)"
	.size	.L.str.57, 54

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%d.%d.%s.%d"
	.size	.L.str.58, 12

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"run out of memory enlarging crossref table"
	.size	.L.str.59, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
