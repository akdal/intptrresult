	.text
	.file	"richards_benchmark.bc"
	.globl	createtask
	.p2align	4, 0x90
	.type	createtask,@function
createtask:                             # @createtask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r12d
	movq	%rdx, %r13
	movl	%esi, %ebp
	movl	%edi, %ebx
	movl	$56, %edi
	callq	malloc
	movslq	%ebx, %rcx
	movq	%rax, tasktab(,%rcx,8)
	movq	tasklist(%rip), %rdx
	movq	%rdx, (%rax)
	movl	%ecx, 8(%rax)
	movl	%ebp, 12(%rax)
	movq	%r13, 16(%rax)
	movl	%r12d, 24(%rax)
	movq	%r15, 32(%rax)
	movq	%r14, 40(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rax, tasklist(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	createtask, .Lfunc_end0-createtask
	.cfi_endproc

	.globl	pkt
	.p2align	4, 0x90
	.type	pkt,@function
pkt:                                    # @pkt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$24, %edi
	callq	malloc
	movl	$0, 20(%rax)
	movq	%rbx, (%rax)
	movl	%ebp, 8(%rax)
	movl	%r14d, 12(%rax)
	movl	$0, 16(%rax)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	pkt, .Lfunc_end1-pkt
	.cfi_endproc

	.globl	trace
	.p2align	4, 0x90
	.type	trace,@function
trace:                                  # @trace
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	layout(%rip), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, layout(%rip)
	cmpl	$1, %eax
	jg	.LBB2_2
# BB#1:
	movl	$10, %edi
	callq	putchar
	movl	$50, layout(%rip)
.LBB2_2:
	movsbl	%bl, %edi
	popq	%rbx
	jmp	putchar                 # TAILCALL
.Lfunc_end2:
	.size	trace, .Lfunc_end2-trace
	.cfi_endproc

	.globl	schedule
	.p2align	4, 0x90
	.type	schedule,@function
schedule:                               # @schedule
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	tcb(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB3_14
# BB#1:                                 # %.preheader.preheader
	movl	$244, %ebp
	movl	$3, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	24(%rcx), %eax
	cmpl	$7, %eax
	ja	.LBB3_14
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	btl	%eax, %ebp
	jae	.LBB3_4
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%rcx), %rcx
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	btl	%eax, %r15d
	jae	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB3_7
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rcx), %r14
	movq	(%r14), %rax
	movq	%rax, 16(%rcx)
	xorl	%edx, %edx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, 24(%rcx)
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	movslq	8(%rcx), %rbx
	movq	%rbx, taskid(%rip)
	movq	40(%rcx), %rax
	movq	%rax, v1(%rip)
	movq	48(%rcx), %rax
	movq	%rax, v2(%rip)
	cmpl	$1, tracing(%rip)
	jne	.LBB3_11
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	layout(%rip), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, layout(%rip)
	cmpl	$1, %eax
	jg	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$10, %edi
	callq	putchar
	movl	$50, layout(%rip)
.LBB3_10:                               # %trace.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	shll	$24, %ebx
	addl	$805306368, %ebx        # imm = 0x30000000
	sarl	$24, %ebx
	movl	%ebx, %edi
	callq	putchar
	movq	tcb(%rip), %rcx
.LBB3_11:                               #   in Loop: Header=BB3_2 Depth=1
	movq	%r14, %rdi
	callq	*32(%rcx)
	movq	%rax, %rcx
	movq	v1(%rip), %rax
	movq	tcb(%rip), %rdx
	movq	%rax, 40(%rdx)
	movq	v2(%rip), %rax
	movq	%rax, 48(%rdx)
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movq	%rcx, tcb(%rip)
	testq	%rcx, %rcx
	jne	.LBB3_2
.LBB3_14:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	schedule, .Lfunc_end3-schedule
	.cfi_endproc

	.globl	Wait
	.p2align	4, 0x90
	.type	Wait,@function
Wait:                                   # @Wait
	.cfi_startproc
# BB#0:
	movq	tcb(%rip), %rax
	orl	$2, 24(%rax)
	retq
.Lfunc_end4:
	.size	Wait, .Lfunc_end4-Wait
	.cfi_endproc

	.globl	holdself
	.p2align	4, 0x90
	.type	holdself,@function
holdself:                               # @holdself
	.cfi_startproc
# BB#0:
	incl	holdcount(%rip)
	movq	tcb(%rip), %rax
	orl	$4, 24(%rax)
	movq	(%rax), %rax
	retq
.Lfunc_end5:
	.size	holdself, .Lfunc_end5-holdself
	.cfi_endproc

	.globl	findtcb
	.p2align	4, 0x90
	.type	findtcb,@function
findtcb:                                # @findtcb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	leal	-1(%rcx), %eax
	cmpl	$9, %eax
	ja	.LBB6_2
# BB#1:
	movslq	%ecx, %rax
	movq	tasktab(,%rax,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_3
.LBB6_2:                                # %.thread
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
.LBB6_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	findtcb, .Lfunc_end6-findtcb
	.cfi_endproc

	.globl	release
	.p2align	4, 0x90
	.type	release,@function
release:                                # @release
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	leal	-1(%rcx), %eax
	cmpl	$9, %eax
	ja	.LBB7_2
# BB#1:
	movslq	%ecx, %rax
	movq	tasktab(,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB7_2
# BB#3:                                 # %findtcb.exit
	andl	$65531, 24(%rax)        # imm = 0xFFFB
	movl	12(%rax), %ecx
	movq	tcb(%rip), %rbx
	cmpl	12(%rbx), %ecx
	cmovgq	%rax, %rbx
	jmp	.LBB7_4
.LBB7_2:                                # %findtcb.exit.thread
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
.LBB7_4:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	release, .Lfunc_end7-release
	.cfi_endproc

	.globl	qpkt
	.p2align	4, 0x90
	.type	qpkt,@function
qpkt:                                   # @qpkt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movslq	8(%rdi), %rsi
	leal	-1(%rsi), %eax
	cmpl	$9, %eax
	ja	.LBB8_2
# BB#1:
	movq	tasktab(,%rsi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB8_2
# BB#3:                                 # %findtcb.exit
	incl	qpktcount(%rip)
	movq	$0, (%rdi)
	movl	taskid(%rip), %eax
	movl	%eax, 8(%rdi)
	cmpq	$0, 16(%rbx)
	je	.LBB8_4
# BB#5:
	addq	$16, %rbx
	movq	$0, (%rdi)
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_6
# BB#7:                                 # %append.exit
	movq	%rdi, (%rax)
	movq	tcb(%rip), %rax
	jmp	.LBB8_8
.LBB8_2:                                # %findtcb.exit.thread
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB8_9
.LBB8_4:
	movq	%rdi, 16(%rbx)
	orb	$1, 24(%rbx)
	movl	12(%rbx), %ecx
	movq	tcb(%rip), %rax
	cmpl	12(%rax), %ecx
	jg	.LBB8_9
.LBB8_8:
	movq	%rax, %rbx
.LBB8_9:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	qpkt, .Lfunc_end8-qpkt
	.cfi_endproc

	.globl	append
	.p2align	4, 0x90
	.type	append,@function
append:                                 # @append
	.cfi_startproc
# BB#0:
	movq	$0, (%rdi)
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.LBB9_1
# BB#2:
	movq	%rdi, (%rax)
	retq
.Lfunc_end9:
	.size	append, .Lfunc_end9-append
	.cfi_endproc

	.globl	idlefn
	.p2align	4, 0x90
	.type	idlefn,@function
idlefn:                                 # @idlefn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	decq	v2(%rip)
	je	.LBB10_1
# BB#2:
	movl	v1(%rip), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	testb	$1, %cl
	jne	.LBB10_6
# BB#3:
	movq	%rax, v1(%rip)
	movq	tasktab+40(%rip), %rax
	testq	%rax, %rax
	jne	.LBB10_8
# BB#4:                                 # %findtcb.exit.thread.i1
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	movl	$5, %esi
	jmp	.LBB10_5
.LBB10_1:
	incl	holdcount(%rip)
	movq	tcb(%rip), %rax
	orl	$4, 24(%rax)
	movq	(%rax), %rbx
	jmp	.LBB10_9
.LBB10_6:
	xorq	$53256, %rax            # imm = 0xD008
	movq	%rax, v1(%rip)
	movq	tasktab+48(%rip), %rax
	testq	%rax, %rax
	je	.LBB10_7
.LBB10_8:                               # %findtcb.exit.i
	andl	$65531, 24(%rax)        # imm = 0xFFFB
	movl	12(%rax), %ecx
	movq	tcb(%rip), %rbx
	cmpl	12(%rbx), %ecx
	cmovgq	%rax, %rbx
	jmp	.LBB10_9
.LBB10_7:                               # %findtcb.exit.thread.i
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	movl	$6, %esi
.LBB10_5:                               # %release.exit5
	xorl	%eax, %eax
	callq	printf
.LBB10_9:                               # %release.exit5
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	idlefn, .Lfunc_end10-idlefn
	.cfi_endproc

	.globl	workfn
	.p2align	4, 0x90
	.type	workfn,@function
workfn:                                 # @workfn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB11_1
# BB#2:
	movl	$7, %esi
	subq	v1(%rip), %rsi
	movq	%rsi, v1(%rip)
	movl	%esi, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	v2(%rip), %rax
	cmpq	$25, %rax
	leaq	1(%rax), %rax
	movl	$1, %ecx
	cmovgq	%rcx, %rax
	movb	alphabet(%rax), %dl
	movb	%dl, 20(%rdi)
	cmpq	$25, %rax
	leaq	1(%rax), %rax
	cmovgq	%rcx, %rax
	movb	alphabet(%rax), %dl
	movb	%dl, 21(%rdi)
	cmpq	$25, %rax
	leaq	1(%rax), %rax
	cmovgq	%rcx, %rax
	movb	alphabet(%rax), %dl
	movb	%dl, 22(%rdi)
	cmpq	$25, %rax
	leaq	1(%rax), %rax
	cmovgq	%rcx, %rax
	movb	alphabet(%rax), %cl
	movb	%cl, 23(%rdi)
	movq	%rax, v2(%rip)
	leal	-1(%rsi), %eax
	cmpl	$9, %eax
	ja	.LBB11_4
# BB#3:
	movslq	%esi, %rax
	movq	tasktab(,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB11_4
# BB#5:                                 # %findtcb.exit.i
	incl	qpktcount(%rip)
	movq	$0, (%rdi)
	movl	taskid(%rip), %eax
	movl	%eax, 8(%rdi)
	cmpq	$0, 16(%rbx)
	je	.LBB11_6
# BB#7:
	addq	$16, %rbx
	movq	$0, (%rdi)
	.p2align	4, 0x90
.LBB11_8:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_8
# BB#9:                                 # %append.exit.i
	movq	%rdi, (%rax)
	movq	tcb(%rip), %rax
	jmp	.LBB11_10
.LBB11_4:                               # %findtcb.exit.thread.i
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB11_11
.LBB11_1:
	movq	tcb(%rip), %rbx
	orl	$2, 24(%rbx)
	jmp	.LBB11_11
.LBB11_6:
	movq	%rdi, 16(%rbx)
	orb	$1, 24(%rbx)
	movl	12(%rbx), %ecx
	movq	tcb(%rip), %rax
	cmpl	12(%rax), %ecx
	jg	.LBB11_11
.LBB11_10:
	movq	%rax, %rbx
.LBB11_11:                              # %qpkt.exit
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	workfn, .Lfunc_end11-workfn
	.cfi_endproc

	.globl	handlerfn
	.p2align	4, 0x90
	.type	handlerfn,@function
handlerfn:                              # @handlerfn
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#1:
	cmpl	$1001, 12(%rdi)         # imm = 0x3E9
	movl	$v1, %ecx
	movl	$v2, %eax
	cmoveq	%rcx, %rax
	movq	$0, (%rdi)
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB12_2
# BB#3:                                 # %append.exit
	movq	%rdi, (%rcx)
.LBB12_4:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	v1(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_18
# BB#5:
	movslq	16(%rax), %rcx
	cmpq	$4, %rcx
	jl	.LBB12_7
# BB#6:
	movq	(%rax), %rcx
	movq	%rcx, v1(%rip)
	jmp	.LBB12_9
.LBB12_7:
	movq	v2(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB12_18
# BB#8:
	movq	(%rdx), %rsi
	movq	%rsi, v2(%rip)
	movsbl	20(%rax,%rcx), %esi
	movl	%esi, 16(%rdx)
	leal	1(%rcx), %ecx
	movl	%ecx, 16(%rax)
	movq	%rdx, %rax
.LBB12_9:                               # %.sink.split
	movslq	8(%rax), %rsi
	leal	-1(%rsi), %ecx
	cmpl	$9, %ecx
	ja	.LBB12_11
# BB#10:
	movq	tasktab(,%rsi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB12_11
# BB#12:                                # %findtcb.exit.i
	incl	qpktcount(%rip)
	movq	$0, (%rax)
	movl	taskid(%rip), %ecx
	movl	%ecx, 8(%rax)
	cmpq	$0, 16(%rbx)
	je	.LBB12_13
# BB#14:
	addq	$16, %rbx
	movq	$0, (%rax)
	.p2align	4, 0x90
.LBB12_15:                              # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	movq	(%rcx), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_15
# BB#16:                                # %append.exit.i
	movq	%rax, (%rcx)
	movq	tcb(%rip), %rax
	jmp	.LBB12_17
.LBB12_18:                              # %qpkt.exit
	movq	tcb(%rip), %rbx
	orl	$2, 24(%rbx)
	jmp	.LBB12_19
.LBB12_11:                              # %findtcb.exit.thread.i
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB12_19
.LBB12_13:
	movq	%rax, 16(%rbx)
	orb	$1, 24(%rbx)
	movl	12(%rbx), %ecx
	movq	tcb(%rip), %rax
	cmpl	12(%rax), %ecx
	jg	.LBB12_19
.LBB12_17:
	movq	%rax, %rbx
.LBB12_19:                              # %qpkt.exit.thread
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	handlerfn, .Lfunc_end12-handlerfn
	.cfi_endproc

	.globl	devfn
	.p2align	4, 0x90
	.type	devfn,@function
devfn:                                  # @devfn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB13_1
# BB#12:
	movq	%rdi, v1(%rip)
	cmpl	$1, tracing(%rip)
	jne	.LBB13_16
# BB#13:
	movl	16(%rdi), %ebx
	movl	layout(%rip), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, layout(%rip)
	cmpl	$1, %eax
	jg	.LBB13_15
# BB#14:
	movl	$10, %edi
	callq	putchar
	movl	$50, layout(%rip)
.LBB13_15:                              # %trace.exit
	movsbl	%bl, %edi
	callq	putchar
.LBB13_16:
	incl	holdcount(%rip)
	movq	tcb(%rip), %rax
	orl	$4, 24(%rax)
	movq	(%rax), %rbx
	jmp	.LBB13_17
.LBB13_1:
	movq	v1(%rip), %rax
	testq	%rax, %rax
	je	.LBB13_2
# BB#3:
	movq	$0, v1(%rip)
	movslq	8(%rax), %rsi
	leal	-1(%rsi), %ecx
	cmpl	$9, %ecx
	ja	.LBB13_5
# BB#4:
	movq	tasktab(,%rsi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#6:                                 # %findtcb.exit.i
	incl	qpktcount(%rip)
	movq	$0, (%rax)
	movl	taskid(%rip), %ecx
	movl	%ecx, 8(%rax)
	cmpq	$0, 16(%rbx)
	je	.LBB13_7
# BB#8:
	addq	$16, %rbx
	movq	$0, (%rax)
	.p2align	4, 0x90
.LBB13_9:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	movq	(%rcx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_9
# BB#10:                                # %append.exit.i
	movq	%rax, (%rcx)
	movq	tcb(%rip), %rax
	jmp	.LBB13_11
.LBB13_5:                               # %findtcb.exit.thread.i
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	jmp	.LBB13_17
.LBB13_2:
	movq	tcb(%rip), %rbx
	orl	$2, 24(%rbx)
	jmp	.LBB13_17
.LBB13_7:
	movq	%rax, 16(%rbx)
	orb	$1, 24(%rbx)
	movl	12(%rbx), %ecx
	movq	tcb(%rip), %rax
	cmpl	12(%rax), %ecx
	jg	.LBB13_17
.LBB13_11:
	movq	%rax, %rbx
.LBB13_17:                              # %qpkt.exit
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	devfn, .Lfunc_end13-devfn
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.quad	1                       # 0x1
	.quad	10000000                # 0x989680
.LCPI14_1:
	.long	0                       # 0x0
	.long	1001                    # 0x3e9
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI14_2:
	.long	5                       # 0x5
	.long	1000                    # 0x3e8
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI14_3:
	.long	6                       # 0x6
	.long	1000                    # 0x3e8
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI14_4:
	.zero	16
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -40
.Lcfi50:
	.cfi_offset %r12, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movl	$.Lstr, %edi
	callq	puts
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, tasktab+8(%rip)
	movq	tasklist(%rip), %rax
	movq	%rax, (%rbx)
	movl	$1, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%rbx)
	movq	$idlefn, 32(%rbx)
	movaps	.LCPI14_0(%rip), %xmm0  # xmm0 = [1,10000000]
	movups	%xmm0, 40(%rbx)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movq	$0, (%r14)
	movaps	.LCPI14_1(%rip), %xmm0  # xmm0 = [0,1001,0,0]
	movups	%xmm0, 8(%r14)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r14, (%r15)
	movaps	.LCPI14_1(%rip), %xmm0  # xmm0 = [0,1001,0,0]
	movups	%xmm0, 8(%r15)
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, tasktab+16(%rip)
	movq	%rbx, (%r14)
	movl	$2, 8(%r14)
	movl	$1000, 12(%r14)         # imm = 0x3E8
	movq	%r15, 16(%r14)
	movl	$3, 24(%r14)
	movq	$workfn, 32(%r14)
	movl	$3, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 40(%r14)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	$0, (%r15)
	movaps	.LCPI14_2(%rip), %xmm0  # xmm0 = [5,1000,0,0]
	movups	%xmm0, 8(%r15)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r15, (%rbx)
	movaps	.LCPI14_2(%rip), %xmm0  # xmm0 = [5,1000,0,0]
	movups	%xmm0, 8(%rbx)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%rbx, (%r15)
	movaps	.LCPI14_2(%rip), %xmm0  # xmm0 = [5,1000,0,0]
	movups	%xmm0, 8(%r15)
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, tasktab+24(%rip)
	movq	%r14, (%r12)
	movl	$3, 8(%r12)
	movl	$2000, 12(%r12)         # imm = 0x7D0
	movq	%r15, 16(%r12)
	movl	$3, 24(%r12)
	movq	$handlerfn, 32(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movq	$0, (%r14)
	movaps	.LCPI14_3(%rip), %xmm0  # xmm0 = [6,1000,0,0]
	movups	%xmm0, 8(%r14)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r14, (%r15)
	movaps	.LCPI14_3(%rip), %xmm0  # xmm0 = [6,1000,0,0]
	movups	%xmm0, 8(%r15)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%r15, (%r14)
	movaps	.LCPI14_3(%rip), %xmm0  # xmm0 = [6,1000,0,0]
	movups	%xmm0, 8(%r14)
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, tasktab+32(%rip)
	movq	%r12, (%rbx)
	movl	$4, 8(%rbx)
	movl	$3000, 12(%rbx)         # imm = 0xBB8
	movq	%r14, 16(%rbx)
	movl	$3, 24(%rbx)
	movq	$handlerfn, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, tasktab+40(%rip)
	movq	%rbx, (%r14)
	movl	$5, 8(%r14)
	movl	$4000, 12(%r14)         # imm = 0xFA0
	movq	$0, 16(%r14)
	movl	$2, 24(%r14)
	movq	$devfn, 32(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r14)
	movl	$56, %edi
	callq	malloc
	movq	%rax, tasktab+48(%rip)
	movq	%r14, (%rax)
	movl	$6, 8(%rax)
	movl	$5000, 12(%rax)         # imm = 0x1388
	movq	$0, 16(%rax)
	movl	$2, 24(%rax)
	movq	$devfn, 32(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rax)
	movq	%rax, tasklist(%rip)
	movq	%rax, tcb(%rip)
	movl	$0, holdcount(%rip)
	movl	$0, qpktcount(%rip)
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$0, tracing(%rip)
	movl	$0, layout(%rip)
	callq	schedule
	movl	$.Lstr.2, %edi
	callq	puts
	movl	qpktcount(%rip), %esi
	movl	holdcount(%rip), %edx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$23263894, qpktcount(%rip) # imm = 0x162FA96
	jne	.LBB14_3
# BB#1:
	cmpl	$9305557, holdcount(%rip) # imm = 0x8DFDD5
	jne	.LBB14_3
# BB#2:
	xorl	%ebx, %ebx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB14_4
.LBB14_3:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %ebx
.LBB14_4:
	movl	$.Lstr.3, %edi
	callq	puts
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	main, .Lfunc_end14-main
	.cfi_endproc

	.type	alphabet,@object        # @alphabet
	.data
	.globl	alphabet
	.p2align	4
alphabet:
	.asciz	"0ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	.size	alphabet, 28

	.type	tasktab,@object         # @tasktab
	.bss
	.globl	tasktab
	.p2align	4
tasktab:
	.zero	88
	.size	tasktab, 88

	.type	tasklist,@object        # @tasklist
	.globl	tasklist
	.p2align	3
tasklist:
	.quad	0
	.size	tasklist, 8

	.type	qpktcount,@object       # @qpktcount
	.globl	qpktcount
	.p2align	2
qpktcount:
	.long	0                       # 0x0
	.size	qpktcount, 4

	.type	holdcount,@object       # @holdcount
	.globl	holdcount
	.p2align	2
holdcount:
	.long	0                       # 0x0
	.size	holdcount, 4

	.type	tracing,@object         # @tracing
	.data
	.globl	tracing
	.p2align	2
tracing:
	.long	1                       # 0x1
	.size	tracing, 4

	.type	layout,@object          # @layout
	.bss
	.globl	layout
	.p2align	2
layout:
	.long	0                       # 0x0
	.size	layout, 4

	.type	tcb,@object             # @tcb
	.comm	tcb,8,8
	.type	taskid,@object          # @taskid
	.comm	taskid,8,8
	.type	v1,@object              # @v1
	.comm	v1,8,8
	.type	v2,@object              # @v2
	.comm	v2,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"\nBad task id %d\n"
	.size	.L.str.2, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"qpkt count = %d  holdcount = %d\n"
	.size	.L.str.6, 33

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"These results are "
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"correct"
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"incorrect"
	.size	.L.str.9, 10

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Bench mark starting"
	.size	.Lstr, 20

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"Starting"
	.size	.Lstr.1, 9

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"finished"
	.size	.Lstr.2, 9

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"\nend of run"
	.size	.Lstr.3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
