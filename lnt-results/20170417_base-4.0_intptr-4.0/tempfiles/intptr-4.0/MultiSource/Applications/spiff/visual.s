	.text
	.file	"visual.bc"
	.globl	V_visual
	.p2align	4, 0x90
	.type	V_visual,@function
V_visual:                               # @V_visual
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	negl	%ebx
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	V_visual, .Lfunc_end0-V_visual
	.cfi_endproc

	.globl	V_cleanup
	.p2align	4, 0x90
	.type	V_cleanup,@function
V_cleanup:                              # @V_cleanup
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	V_cleanup, .Lfunc_end1-V_cleanup
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"visual mode is not available on this machine\n"
	.size	.L.str, 46


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
