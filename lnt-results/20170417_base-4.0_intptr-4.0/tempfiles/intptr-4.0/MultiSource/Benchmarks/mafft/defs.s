	.text
	.file	"defs.bc"
	.type	commonAlloc1,@object    # @commonAlloc1
	.bss
	.globl	commonAlloc1
	.p2align	2
commonAlloc1:
	.long	0                       # 0x0
	.size	commonAlloc1, 4

	.type	commonAlloc2,@object    # @commonAlloc2
	.globl	commonAlloc2
	.p2align	2
commonAlloc2:
	.long	0                       # 0x0
	.size	commonAlloc2, 4

	.type	commonIP,@object        # @commonIP
	.globl	commonIP
	.p2align	3
commonIP:
	.quad	0
	.size	commonIP, 8

	.type	commonJP,@object        # @commonJP
	.globl	commonJP
	.p2align	3
commonJP:
	.quad	0
	.size	commonJP, 8

	.type	consweight_multi,@object # @consweight_multi
	.data
	.globl	consweight_multi
	.p2align	2
consweight_multi:
	.long	1065353216              # float 1
	.size	consweight_multi, 4

	.type	consweight_rna,@object  # @consweight_rna
	.bss
	.globl	consweight_rna
	.p2align	2
consweight_rna:
	.long	0                       # float 0
	.size	consweight_rna, 4

	.type	RNAscoremtx,@object     # @RNAscoremtx
	.data
	.globl	RNAscoremtx
RNAscoremtx:
	.byte	110                     # 0x6e
	.size	RNAscoremtx, 1

	.type	modelname,@object       # @modelname
	.comm	modelname,100,16
	.type	njob,@object            # @njob
	.comm	njob,4,4
	.type	nlenmax,@object         # @nlenmax
	.comm	nlenmax,4,4
	.type	amino_n,@object         # @amino_n
	.comm	amino_n,512,16
	.type	amino_grp,@object       # @amino_grp
	.comm	amino_grp,128,16
	.type	amino_dis,@object       # @amino_dis
	.comm	amino_dis,65536,16
	.type	amino_disLN,@object     # @amino_disLN
	.comm	amino_disLN,65536,16
	.type	amino_dis_consweight_multi,@object # @amino_dis_consweight_multi
	.comm	amino_dis_consweight_multi,131072,16
	.type	n_dis,@object           # @n_dis
	.comm	n_dis,2704,16
	.type	n_disFFT,@object        # @n_disFFT
	.comm	n_disFFT,2704,16
	.type	n_dis_consweight_multi,@object # @n_dis_consweight_multi
	.comm	n_dis_consweight_multi,2704,16
	.type	amino,@object           # @amino
	.comm	amino,26,16
	.type	polarity,@object        # @polarity
	.comm	polarity,160,16
	.type	volume,@object          # @volume
	.comm	volume,160,16
	.type	ribosumdis,@object      # @ribosumdis
	.comm	ribosumdis,5476,16
	.type	ppid,@object            # @ppid
	.comm	ppid,4,4
	.type	thrinter,@object        # @thrinter
	.comm	thrinter,8,8
	.type	fastathreshold,@object  # @fastathreshold
	.comm	fastathreshold,8,8
	.type	pslocal,@object         # @pslocal
	.comm	pslocal,4,4
	.type	ppslocal,@object        # @ppslocal
	.comm	ppslocal,4,4
	.type	constraint,@object      # @constraint
	.comm	constraint,4,4
	.type	divpairscore,@object    # @divpairscore
	.comm	divpairscore,4,4
	.type	fmodel,@object          # @fmodel
	.comm	fmodel,4,4
	.type	nblosum,@object         # @nblosum
	.comm	nblosum,4,4
	.type	kobetsubunkatsu,@object # @kobetsubunkatsu
	.comm	kobetsubunkatsu,4,4
	.type	bunkatsu,@object        # @bunkatsu
	.comm	bunkatsu,4,4
	.type	dorp,@object            # @dorp
	.comm	dorp,4,4
	.type	niter,@object           # @niter
	.comm	niter,4,4
	.type	contin,@object          # @contin
	.comm	contin,4,4
	.type	calledByXced,@object    # @calledByXced
	.comm	calledByXced,4,4
	.type	devide,@object          # @devide
	.comm	devide,4,4
	.type	scmtd,@object           # @scmtd
	.comm	scmtd,4,4
	.type	weight,@object          # @weight
	.comm	weight,4,4
	.type	utree,@object           # @utree
	.comm	utree,4,4
	.type	tbutree,@object         # @tbutree
	.comm	tbutree,4,4
	.type	refine,@object          # @refine
	.comm	refine,4,4
	.type	check,@object           # @check
	.comm	check,4,4
	.type	cut,@object             # @cut
	.comm	cut,8,8
	.type	cooling,@object         # @cooling
	.comm	cooling,4,4
	.type	penalty,@object         # @penalty
	.comm	penalty,4,4
	.type	ppenalty,@object        # @ppenalty
	.comm	ppenalty,4,4
	.type	penaltyLN,@object       # @penaltyLN
	.comm	penaltyLN,4,4
	.type	RNApenalty,@object      # @RNApenalty
	.comm	RNApenalty,4,4
	.type	RNAppenalty,@object     # @RNAppenalty
	.comm	RNAppenalty,4,4
	.type	RNApenalty_ex,@object   # @RNApenalty_ex
	.comm	RNApenalty_ex,4,4
	.type	RNAppenalty_ex,@object  # @RNAppenalty_ex
	.comm	RNAppenalty_ex,4,4
	.type	penalty_ex,@object      # @penalty_ex
	.comm	penalty_ex,4,4
	.type	ppenalty_ex,@object     # @ppenalty_ex
	.comm	ppenalty_ex,4,4
	.type	penalty_exLN,@object    # @penalty_exLN
	.comm	penalty_exLN,4,4
	.type	penalty_EX,@object      # @penalty_EX
	.comm	penalty_EX,4,4
	.type	ppenalty_EX,@object     # @ppenalty_EX
	.comm	ppenalty_EX,4,4
	.type	penalty_OP,@object      # @penalty_OP
	.comm	penalty_OP,4,4
	.type	ppenalty_OP,@object     # @ppenalty_OP
	.comm	ppenalty_OP,4,4
	.type	RNAthr,@object          # @RNAthr
	.comm	RNAthr,4,4
	.type	RNApthr,@object         # @RNApthr
	.comm	RNApthr,4,4
	.type	offset,@object          # @offset
	.comm	offset,4,4
	.type	poffset,@object         # @poffset
	.comm	poffset,4,4
	.type	offsetLN,@object        # @offsetLN
	.comm	offsetLN,4,4
	.type	offsetFFT,@object       # @offsetFFT
	.comm	offsetFFT,4,4
	.type	scoremtx,@object        # @scoremtx
	.comm	scoremtx,4,4
	.type	TMorJTT,@object         # @TMorJTT
	.comm	TMorJTT,4,4
	.type	use_fft,@object         # @use_fft
	.comm	use_fft,1,1
	.type	force_fft,@object       # @force_fft
	.comm	force_fft,1,1
	.type	nevermemsave,@object    # @nevermemsave
	.comm	nevermemsave,4,4
	.type	fftscore,@object        # @fftscore
	.comm	fftscore,4,4
	.type	fftWinSize,@object      # @fftWinSize
	.comm	fftWinSize,4,4
	.type	fftThreshold,@object    # @fftThreshold
	.comm	fftThreshold,4,4
	.type	fftRepeatStop,@object   # @fftRepeatStop
	.comm	fftRepeatStop,4,4
	.type	fftNoAnchStop,@object   # @fftNoAnchStop
	.comm	fftNoAnchStop,4,4
	.type	divWinSize,@object      # @divWinSize
	.comm	divWinSize,4,4
	.type	divThreshold,@object    # @divThreshold
	.comm	divThreshold,4,4
	.type	disp,@object            # @disp
	.comm	disp,4,4
	.type	outgap,@object          # @outgap
	.comm	outgap,4,4
	.type	alg,@object             # @alg
	.comm	alg,1,1
	.type	cnst,@object            # @cnst
	.comm	cnst,4,4
	.type	mix,@object             # @mix
	.comm	mix,4,4
	.type	tbitr,@object           # @tbitr
	.comm	tbitr,4,4
	.type	tbweight,@object        # @tbweight
	.comm	tbweight,4,4
	.type	tbrweight,@object       # @tbrweight
	.comm	tbrweight,4,4
	.type	disopt,@object          # @disopt
	.comm	disopt,4,4
	.type	pamN,@object            # @pamN
	.comm	pamN,4,4
	.type	checkC,@object          # @checkC
	.comm	checkC,4,4
	.type	geta2,@object           # @geta2
	.comm	geta2,4,4
	.type	treemethod,@object      # @treemethod
	.comm	treemethod,4,4
	.type	kimuraR,@object         # @kimuraR
	.comm	kimuraR,4,4
	.type	swopt,@object           # @swopt
	.comm	swopt,8,8
	.type	fftkeika,@object        # @fftkeika
	.comm	fftkeika,4,4
	.type	score_check,@object     # @score_check
	.comm	score_check,4,4
	.type	makedistmtx,@object     # @makedistmtx
	.comm	makedistmtx,4,4
	.type	inputfile,@object       # @inputfile
	.comm	inputfile,8,8
	.type	rnakozo,@object         # @rnakozo
	.comm	rnakozo,4,4
	.type	rnaprediction,@object   # @rnaprediction
	.comm	rnaprediction,1,1
	.type	signalSM,@object        # @signalSM
	.comm	signalSM,8,8
	.type	prep_g,@object          # @prep_g
	.comm	prep_g,8,8
	.type	trap_g,@object          # @trap_g
	.comm	trap_g,8,8
	.type	seq_g,@object           # @seq_g
	.comm	seq_g,8,8
	.type	res_g,@object           # @res_g
	.comm	res_g,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
