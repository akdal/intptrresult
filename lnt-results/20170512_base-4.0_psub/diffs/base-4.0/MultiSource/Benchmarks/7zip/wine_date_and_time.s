	.text
	.file	"wine_date_and_time.bc"
	.globl	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	.p2align	4, 0x90
	.type	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME,@function
_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME: # @_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	imulq	$10000000, %rax, %rax   # imm = 0x989680
	movabsq	$116444736000000000, %rcx # imm = 0x19DB1DED53E8000
	addq	%rax, %rcx
	movl	%ecx, (%rsi)
	shrq	$32, %rcx
	movl	%ecx, 4(%rsi)
	retq
.Lfunc_end0:
	.size	_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME, .Lfunc_end0-_Z29RtlSecondsSince1970ToFileTimejP9_FILETIME
	.cfi_endproc

	.globl	DosDateTimeToFileTime
	.p2align	4, 0x90
	.type	DosDateTimeToFileTime,@function
DosDateTimeToFileTime:                  # @DosDateTimeToFileTime
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 80
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	(%rsi,%rsi), %eax
	andl	$62, %eax
	movl	%eax, 8(%rsp)
	movl	%esi, %eax
	shrl	$5, %eax
	andl	$63, %eax
	movl	%eax, 12(%rsp)
	shrl	$11, %esi
	movl	%esi, 16(%rsp)
	movl	%edi, %eax
	andl	$31, %eax
	movl	%eax, 20(%rsp)
	movl	%edi, %eax
	shrl	$5, %eax
	andl	$15, %eax
	decl	%eax
	movl	%eax, 24(%rsp)
	shrl	$9, %edi
	addl	$80, %edi
	movl	%edi, 28(%rsp)
	movl	$-1, 40(%rsp)
	leaq	8(%rsp), %rdi
	callq	timegm
	movl	%eax, %eax
	imulq	$10000000, %rax, %rax   # imm = 0x989680
	movabsq	$116444736000000000, %rcx # imm = 0x19DB1DED53E8000
	addq	%rax, %rcx
	movl	%ecx, (%rbx)
	shrq	$32, %rcx
	movl	%ecx, 4(%rbx)
	movl	$1, %eax
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	DosDateTimeToFileTime, .Lfunc_end1-DosDateTimeToFileTime
	.cfi_endproc

	.globl	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	.p2align	4, 0x90
	.type	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj,@function
_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj: # @_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	.cfi_startproc
# BB#0:
	movabsq	$-2972493582642298179, %rax # imm = 0xD6BF94D5E57A42BD
	mulq	(%rdi)
	shrq	$23, %rdx
	movabsq	$-11644473600, %rax     # imm = 0xFFFFFFFD49EF6F00
	addq	%rdx, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	je	.LBB2_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB2_2:
	movl	%eax, (%rsi)
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj, .Lfunc_end2-_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	.cfi_endproc

	.globl	FileTimeToDosDateTime
	.p2align	4, 0x90
	.type	FileTimeToDosDateTime,@function
FileTimeToDosDateTime:                  # @FileTimeToDosDateTime
	.cfi_startproc
# BB#0:                                 # %_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj.exit
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	(%rdi), %eax
	movl	4(%rdi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movabsq	$-2972493582642298179, %rcx # imm = 0xD6BF94D5E57A42BD
	mulq	%rcx
	shrq	$23, %rdx
	addl	$1240428288, %edx       # imm = 0x49EF6F00
	movq	%rdx, (%rsp)
	movq	%rsp, %rdi
	callq	gmtime
	movl	20(%rax), %esi
	movl	12(%rax), %r8d
	movl	16(%rax), %edx
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#1:
	movl	8(%rax), %edi
	shll	$11, %edi
	movl	(%rax), %ecx
	movl	4(%rax), %eax
	shll	$5, %eax
	addl	%edi, %eax
	movl	%ecx, %edi
	shrl	$31, %edi
	addl	%ecx, %edi
	shrl	%edi
	addl	%eax, %edi
	movw	%di, (%rbx)
.LBB3_2:
	testq	%r14, %r14
	je	.LBB3_4
# BB#3:
	shll	$9, %esi
	shll	$5, %edx
	addl	%esi, %edx
	leal	24608(%r8,%rdx), %eax
	movw	%ax, (%r14)
.LBB3_4:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	FileTimeToDosDateTime, .Lfunc_end3-FileTimeToDosDateTime
	.cfi_endproc

	.globl	FileTimeToLocalFileTime
	.p2align	4, 0x90
	.type	FileTimeToLocalFileTime,@function
FileTimeToLocalFileTime:                # @FileTimeToLocalFileTime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	(%rdi), %ebx
	movl	4(%rdi), %eax
	shlq	$32, %rax
	orq	%rax, %rbx
	xorl	%edi, %edi
	callq	time
	movq	%rax, (%rsp)
	movq	%rsp, %r15
	movq	%r15, %rdi
	callq	localtime
	movl	32(%rax), %ebp
	movq	%r15, %rdi
	callq	gmtime
	movl	%ebp, 32(%rax)
	movq	%rax, %rdi
	callq	mktime
	subl	(%rsp), %eax
	cltq
	imulq	$-10000000, %rax, %rax  # imm = 0xFF676980
	addq	%rbx, %rax
	movl	%eax, (%r14)
	shrq	$32, %rax
	movl	%eax, 4(%r14)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	FileTimeToLocalFileTime, .Lfunc_end4-FileTimeToLocalFileTime
	.cfi_endproc

	.globl	FileTimeToSystemTime
	.p2align	4, 0x90
	.type	FileTimeToSystemTime,@function
FileTimeToSystemTime:                   # @FileTimeToSystemTime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	(%rdi), %r10d
	movl	4(%rdi), %eax
	shlq	$32, %rax
	orq	%rax, %r10
	movabsq	$-2972493582642298179, %rcx # imm = 0xD6BF94D5E57A42BD
	movq	%r10, %rax
	imulq	%rcx
	movq	%rdx, %rcx
	addq	%r10, %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	sarq	$23, %rcx
	addq	%rax, %rcx
	imulq	$10000000, %rcx, %rdx   # imm = 0x989680
	movq	%r10, %rax
	subq	%rdx, %rax
	movabsq	$3777893186295716171, %rdx # imm = 0x346DC5D63886594B
	imulq	%rdx
	movq	%rdx, %r8
	movq	%r8, %rax
	shrq	$63, %rax
	shrl	$11, %r8d
	addl	%eax, %r8d
	movabsq	$-6709238516040760861, %rdx # imm = 0xA2E3FF1DE20581E3
	movq	%r10, %rax
	imulq	%rdx
	movq	%rdx, %r9
	addq	%r10, %r9
	movq	%r9, %rbx
	shrq	$63, %rbx
	sarq	$39, %r9
	leaq	(%r9,%rbx), %r14
	movabsq	$1749024623285053783, %rdx # imm = 0x1845C8A0CE512957
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$13, %rdx
	addq	%rax, %rdx
	imulq	$86400, %rdx, %rax      # imm = 0x15180
	subq	%rax, %rcx
	movslq	%ecx, %rax
	imulq	$-1851608123, %rax, %r11 # imm = 0x91A2B3C5
	shrq	$32, %r11
	addl	%eax, %r11d
	movl	%r11d, %ecx
	shrl	$31, %ecx
	sarl	$11, %r11d
	addl	%ecx, %r11d
	imull	$3600, %r11d, %ecx      # imm = 0xE10
	subl	%ecx, %eax
	movslq	%eax, %r10
	imulq	$-2004318071, %r10, %r15 # imm = 0x88888889
	shrq	$32, %r15
	addl	%r10d, %r15d
	movl	%r15d, %eax
	shrl	$31, %eax
	sarl	$5, %r15d
	addl	%eax, %r15d
	imull	$60, %r15d, %eax
	subl	%eax, %r10d
	leaq	1(%r9,%rbx), %r9
	movabsq	$5270498306774157605, %rcx # imm = 0x4924924924924925
	movq	%r9, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrl	%edx
	addl	%eax, %edx
	leal	(,%rdx,8), %eax
	subl	%edx, %eax
	subl	%eax, %r9d
	leaq	1227(,%r14,4), %rax
	movabsq	$4137408090565272301, %rcx # imm = 0x396B06BCC8F862ED
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	3(%rdx,%rdx,2), %rcx
	sarq	$63, %rcx
	shrq	$62, %rcx
	leaq	3(%rcx,%rax), %rax
	sarq	$2, %rax
	leaq	28188(%rax,%r14), %rbx
	leaq	(%rbx,%rbx,4), %rcx
	leaq	-2442(,%rcx,4), %rax
	movabsq	$-8103436239908822879, %rdx # imm = 0x8F8ACFAC743520A1
	imulq	%rdx
	leaq	-2442(%rdx,%rcx,4), %rdi
	movq	%rdi, %rax
	shrq	$63, %rax
	sarq	$12, %rdi
	addq	%rax, %rdi
	imulq	$1461, %rdi, %rax       # imm = 0x5B5
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$62, %rcx
	addq	%rax, %rcx
	sarq	$2, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	shlq	$6, %rcx
	movabsq	$4821201105533073215, %rdx # imm = 0x42E858DC9604F73F
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$9, %rdx
	addq	%rax, %rdx
	cmpq	$27426, %rcx            # imm = 0x6B22
	movl	$65535, %eax            # imm = 0xFFFF
	movl	$65523, %ebp            # imm = 0xFFF3
	cmovll	%eax, %ebp
	addl	%edx, %ebp
	xorl	%eax, %eax
	cmpq	$27425, %rcx            # imm = 0x6B21
	setg	%al
	leal	1524(%rax,%rdi), %eax
	imulq	$1959, %rdx, %rcx       # imm = 0x7A7
	movq	%rcx, %rdx
	sarq	$63, %rdx
	shrq	$58, %rdx
	addl	%ecx, %edx
	shrl	$6, %edx
	subl	%edx, %ebx
	movw	%ax, (%rsi)
	movw	%bp, 2(%rsi)
	movw	%bx, 6(%rsi)
	movw	%r11w, 8(%rsi)
	movw	%r15w, 10(%rsi)
	movw	%r10w, 12(%rsi)
	movw	%r8w, 14(%rsi)
	movw	%r9w, 4(%rsi)
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	FileTimeToSystemTime, .Lfunc_end5-FileTimeToSystemTime
	.cfi_endproc

	.globl	LocalFileTimeToFileTime
	.p2align	4, 0x90
	.type	LocalFileTimeToFileTime,@function
LocalFileTimeToFileTime:                # @LocalFileTimeToFileTime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	(%rdi), %ebx
	movl	4(%rdi), %eax
	shlq	$32, %rax
	orq	%rax, %rbx
	xorl	%edi, %edi
	callq	time
	movq	%rax, (%rsp)
	movq	%rsp, %r15
	movq	%r15, %rdi
	callq	localtime
	movl	32(%rax), %ebp
	movq	%r15, %rdi
	callq	gmtime
	movl	%ebp, 32(%rax)
	movq	%rax, %rdi
	callq	mktime
	subl	(%rsp), %eax
	cltq
	imulq	$10000000, %rax, %rax   # imm = 0x989680
	addq	%rbx, %rax
	movl	%eax, (%r14)
	shrq	$32, %rax
	movl	%eax, 4(%r14)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	LocalFileTimeToFileTime, .Lfunc_end6-LocalFileTimeToFileTime
	.cfi_endproc

	.globl	GetSystemTime
	.p2align	4, 0x90
	.type	GetSystemTime,@function
GetSystemTime:                          # @GetSystemTime
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r12, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	imulq	$10000000, 8(%rsp), %rax # imm = 0x989680
	movq	16(%rsp), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,2), %rax
	movabsq	$116444736000000000, %rcx # imm = 0x19DB1DED53E8000
	addq	%rax, %rcx
	movabsq	$-2972493582642298179, %rdx # imm = 0xD6BF94D5E57A42BD
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rdi
	addq	%rcx, %rdi
	movq	%rdi, %rax
	shrq	$63, %rax
	sarq	$23, %rdi
	addq	%rax, %rdi
	imulq	$10000000, %rdi, %rdx   # imm = 0x989680
	movq	%rcx, %rax
	subq	%rdx, %rax
	movabsq	$3777893186295716171, %rdx # imm = 0x346DC5D63886594B
	imulq	%rdx
	movq	%rdx, %r8
	movq	%r8, %rax
	shrq	$63, %rax
	shrl	$11, %r8d
	addl	%eax, %r8d
	movabsq	$-6709238516040760861, %rdx # imm = 0xA2E3FF1DE20581E3
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rsi
	addq	%rcx, %rsi
	movq	%rsi, %rcx
	shrq	$63, %rcx
	sarq	$39, %rsi
	leaq	(%rsi,%rcx), %r11
	movabsq	$1749024623285053783, %rdx # imm = 0x1845C8A0CE512957
	movq	%rdi, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$13, %rdx
	addq	%rax, %rdx
	imulq	$86400, %rdx, %rax      # imm = 0x15180
	subq	%rax, %rdi
	movslq	%edi, %rax
	imulq	$-1851608123, %rax, %r15 # imm = 0x91A2B3C5
	shrq	$32, %r15
	addl	%eax, %r15d
	movl	%r15d, %edx
	shrl	$31, %edx
	sarl	$11, %r15d
	addl	%edx, %r15d
	imull	$3600, %r15d, %edx      # imm = 0xE10
	subl	%edx, %eax
	movslq	%eax, %r9
	imulq	$-2004318071, %r9, %r12 # imm = 0x88888889
	shrq	$32, %r12
	addl	%r9d, %r12d
	movl	%r12d, %eax
	shrl	$31, %eax
	sarl	$5, %r12d
	addl	%eax, %r12d
	imull	$60, %r12d, %eax
	subl	%eax, %r9d
	leaq	1(%rsi,%rcx), %r10
	movabsq	$5270498306774157605, %rcx # imm = 0x4924924924924925
	movq	%r10, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	shrl	%edx
	addl	%eax, %edx
	leal	(,%rdx,8), %eax
	subl	%edx, %eax
	subl	%eax, %r10d
	leaq	1227(,%r11,4), %rax
	movabsq	$4137408090565272301, %rcx # imm = 0x396B06BCC8F862ED
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	3(%rdx,%rdx,2), %rcx
	sarq	$63, %rcx
	shrq	$62, %rcx
	leaq	3(%rcx,%rax), %rax
	sarq	$2, %rax
	leaq	28188(%rax,%r11), %rsi
	leaq	(%rsi,%rsi,4), %rcx
	leaq	-2442(,%rcx,4), %rax
	movabsq	$-8103436239908822879, %rdx # imm = 0x8F8ACFAC743520A1
	imulq	%rdx
	leaq	-2442(%rdx,%rcx,4), %rdi
	movq	%rdi, %rax
	shrq	$63, %rax
	sarq	$12, %rdi
	addq	%rax, %rdi
	imulq	$1461, %rdi, %rax       # imm = 0x5B5
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$62, %rcx
	addq	%rax, %rcx
	sarq	$2, %rcx
	subq	%rcx, %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	movabsq	$4821201105533073215, %rdx # imm = 0x42E858DC9604F73F
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$9, %rdx
	addq	%rax, %rdx
	cmpq	$27426, %rcx            # imm = 0x6B22
	movl	$65535, %r11d           # imm = 0xFFFF
	movl	$65523, %eax            # imm = 0xFFF3
	cmovll	%r11d, %eax
	addl	%edx, %eax
	xorl	%ebx, %ebx
	cmpq	$27425, %rcx            # imm = 0x6B21
	setg	%bl
	leal	1524(%rbx,%rdi), %ecx
	imulq	$1959, %rdx, %rdx       # imm = 0x7A7
	movq	%rdx, %rdi
	sarq	$63, %rdi
	shrq	$58, %rdi
	addl	%edx, %edi
	shrl	$6, %edi
	subl	%edi, %esi
	movw	%cx, (%r14)
	movw	%ax, 2(%r14)
	movw	%si, 6(%r14)
	movw	%r15w, 8(%r14)
	movw	%r12w, 10(%r14)
	movw	%r9w, 12(%r14)
	movw	%r8w, 14(%r14)
	movw	%r10w, 4(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	GetSystemTime, .Lfunc_end7-GetSystemTime
	.cfi_endproc

	.globl	SystemTimeToFileTime
	.p2align	4, 0x90
	.type	SystemTimeToFileTime,@function
SystemTimeToFileTime:                   # @SystemTimeToFileTime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 40
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movswl	2(%rdi), %ecx
	leal	-1(%rcx), %eax
	movzwl	%ax, %eax
	cmpl	$11, %eax
                                        # implicit-def: %RBX
	ja	.LBB8_15
# BB#1:
	movzwl	8(%rdi), %edx
	movzwl	%dx, %eax
	cmpl	$23, %eax
                                        # implicit-def: %RBX
	ja	.LBB8_15
# BB#2:
	movzwl	10(%rdi), %r8d
	movzwl	%r8w, %eax
	cmpl	$59, %eax
                                        # implicit-def: %RBX
	ja	.LBB8_15
# BB#3:
	movzwl	12(%rdi), %r9d
	movzwl	%r9w, %eax
	cmpl	$59, %eax
                                        # implicit-def: %RBX
	ja	.LBB8_15
# BB#4:
	movzwl	14(%rdi), %r10d
	movzwl	%r10w, %eax
	cmpl	$999, %eax              # imm = 0x3E7
                                        # implicit-def: %RBX
	ja	.LBB8_15
# BB#5:
	movswl	6(%rdi), %eax
	testw	%ax, %ax
	jle	.LBB8_6
# BB#7:
	movswl	(%rdi), %ebp
	movzwl	%cx, %ebx
	movb	$1, %r11b
	cmpl	$2, %ebx
	je	.LBB8_12
# BB#8:
	testb	$3, %bpl
	je	.LBB8_10
# BB#9:
	xorl	%r11d, %r11d
	jmp	.LBB8_12
.LBB8_6:
                                        # implicit-def: %RBX
	jmp	.LBB8_15
.LBB8_10:
	movslq	%ebp, %rbx
	imulq	$1374389535, %rbx, %r15 # imm = 0x51EB851F
	movq	%r15, %r14
	shrq	$63, %r14
	movq	%r15, %rbx
	sarq	$37, %rbx
	addl	%r14d, %ebx
	imull	$100, %ebx, %ebx
	cmpl	%ebx, %ebp
	jne	.LBB8_12
# BB#11:
	sarq	$39, %r15
	addl	%r14d, %r15d
	imull	$400, %r15d, %ebx       # imm = 0x190
	cmpl	%ebx, %ebp
	sete	%r11b
.LBB8_12:                               # %_ZL10IsLeapYeari.exit.i
	cmpl	$1601, %ebp             # imm = 0x641
                                        # implicit-def: %RBX
	jl	.LBB8_15
# BB#13:                                # %_ZL10IsLeapYeari.exit.i
	movslq	%ecx, %rbx
	movzbl	%r11b, %edi
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$4, %rdi
	cmpl	_ZL12MonthLengths-4(%rdi,%rbx,4), %eax
                                        # implicit-def: %RBX
	jg	.LBB8_15
# BB#14:
	movswq	%dx, %r14
	movswq	%r8w, %r11
	movswq	%r9w, %r9
	movswq	%r10w, %r8
	xorl	%edi, %edi
	cmpl	$3, %ecx
	setl	%dil
	movl	$13, %ebx
	movl	$1, %edx
	cmovll	%ebx, %edx
	subl	%edi, %ebp
	addl	%ecx, %edx
	movslq	%ebp, %rcx
	imulq	$1374389535, %rcx, %rdi # imm = 0x51EB851F
	movq	%rdi, %rbp
	shrq	$63, %rbp
	sarq	$37, %rdi
	addl	%ebp, %edi
	leal	(%rdi,%rdi,2), %ebp
	leal	3(%rdi,%rdi,2), %edi
	imull	$36525, %ecx, %ecx      # imm = 0x8EAD
	movslq	%ecx, %rcx
	imulq	$1374389535, %rcx, %rcx # imm = 0x51EB851F
	movq	%rcx, %rbx
	shrq	$63, %rbx
	sarq	$37, %rcx
	addl	%ebx, %ecx
	sarl	$31, %edi
	shrl	$30, %edi
	leal	3(%rdi,%rbp), %edi
	sarl	$2, %edi
	imull	$1959, %edx, %ebp       # imm = 0x7A7
	movl	%ebp, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%ebp, %edx
	sarl	$6, %edx
	addl	%edx, %eax
	addl	%ecx, %eax
	subl	%edi, %eax
	addl	$-584817, %eax          # imm = 0xFFF7138F
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rax
	imulq	$60, %rax, %rax
	addq	%r11, %rax
	imulq	$60, %rax, %rax
	addq	%r9, %rax
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	addq	%r8, %rax
	imulq	$10000, %rax, %rbx      # imm = 0x2710
.LBB8_15:                               # %_ZL19RtlTimeFieldsToTimeP12_TIME_FIELDSP13LARGE_INTEGER.exit
	movl	%ebx, (%rsi)
	shrq	$32, %rbx
	movl	%ebx, 4(%rsi)
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	SystemTimeToFileTime, .Lfunc_end8-SystemTimeToFileTime
	.cfi_endproc

	.type	_ZL12MonthLengths,@object # @_ZL12MonthLengths
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL12MonthLengths:
	.long	31                      # 0x1f
	.long	28                      # 0x1c
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.size	_ZL12MonthLengths, 96


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
