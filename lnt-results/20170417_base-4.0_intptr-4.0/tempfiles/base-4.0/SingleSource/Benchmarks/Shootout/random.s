	.text
	.file	"random.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4636737291354636288     # double 100
.LCPI0_1:
	.quad	4684049276697837568     # double 139968
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$399999999, %r8d        # imm = 0x17D783FF
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r8
	decl	%r8d
.LBB0_2:                                # %.preheader
	movq	gen_random.last(%rip), %rcx
	testb	$1, %r8b
	jne	.LBB0_3
# BB#4:
	leal	-1(%r8), %esi
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movabsq	$4318579316753219217, %rdx # imm = 0x3BEEAD01FD6CBE91
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	testl	%r8d, %r8d
	jne	.LBB0_6
	jmp	.LBB0_8
.LBB0_3:
	movl	%r8d, %esi
	testl	%r8d, %r8d
	je	.LBB0_8
.LBB0_6:                                # %.preheader.new
	notl	%esi
	movabsq	$4318579316753219217, %rdi # imm = 0x3BEEAD01FD6CBE91
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	addl	$2, %esi
	jne	.LBB0_7
.LBB0_8:
	movq	%rcx, gen_random.last(%rip)
	cvtsi2sdq	%rcx, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	divsd	.LCPI0_1(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.9f\n"
	.size	.L.str, 6

	.type	gen_random.last,@object # @gen_random.last
	.data
	.p2align	3
gen_random.last:
	.quad	42                      # 0x2a
	.size	gen_random.last, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
