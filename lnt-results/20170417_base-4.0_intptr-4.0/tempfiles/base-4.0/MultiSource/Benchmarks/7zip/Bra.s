	.text
	.file	"Bra.bc"
	.globl	ARM_Convert
	.p2align	4, 0x90
	.type	ARM_Convert,@function
ARM_Convert:                            # @ARM_Convert
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpq	$4, %rsi
	jae	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	retq
.LBB0_2:
	addq	$-4, %rsi
	testl	%ecx, %ecx
	je	.LBB0_7
# BB#3:                                 # %.split.preheader
	addl	$8, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$-21, 3(%rdi,%rax)
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movzbl	2(%rdi,%rax), %r8d
	shll	$16, %r8d
	movzbl	1(%rdi,%rax), %ecx
	shll	$8, %ecx
	orl	%r8d, %ecx
	movzbl	(%rdi,%rax), %r8d
	orl	%ecx, %r8d
	leal	(%rdx,%rax), %ecx
	leal	(%rcx,%r8,4), %r9d
	movl	%r9d, %r8d
	shrl	$2, %r8d
	movl	%r9d, %ecx
	shrl	$18, %ecx
	movb	%cl, 2(%rdi,%rax)
	shrl	$10, %r9d
	movb	%r9b, 1(%rdi,%rax)
	movb	%r8b, (%rdi,%rax)
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	addq	$4, %rax
	cmpq	%rsi, %rax
	jbe	.LBB0_4
	jmp	.LBB0_11
.LBB0_7:                                # %.split.us.preheader
	movl	$-8, %r9d
	subl	%edx, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_8:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$-21, 3(%rdi,%rax)
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movzbl	2(%rdi,%rax), %r8d
	shll	$16, %r8d
	movzbl	1(%rdi,%rax), %edx
	shll	$8, %edx
	orl	%r8d, %edx
	movzbl	(%rdi,%rax), %ecx
	orl	%edx, %ecx
	leal	(%r9,%rcx,4), %edx
	movl	%edx, %r8d
	shrl	$2, %r8d
	movl	%edx, %ecx
	shrl	$18, %ecx
	movb	%cl, 2(%rdi,%rax)
	shrl	$10, %edx
	movb	%dl, 1(%rdi,%rax)
	movb	%r8b, (%rdi,%rax)
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	addq	$4, %rax
	addl	$-4, %r9d
	cmpq	%rsi, %rax
	jbe	.LBB0_8
.LBB0_11:                               # %.loopexit
	retq
.Lfunc_end0:
	.size	ARM_Convert, .Lfunc_end0-ARM_Convert
	.cfi_endproc

	.globl	ARMT_Convert
	.p2align	4, 0x90
	.type	ARMT_Convert,@function
ARMT_Convert:                           # @ARMT_Convert
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpq	$4, %rsi
	jae	.LBB1_2
# BB#1:
	xorl	%eax, %eax
	retq
.LBB1_2:
	addq	$-4, %rsi
	addl	$4, %edx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB1_7
	.p2align	4, 0x90
.LBB1_3:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rdi,%rax), %r8d
	movl	%r8d, %ecx
	andl	$248, %ecx
	cmpl	$240, %ecx
	jne	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	3(%rdi,%rax), %r9d
	movl	%r9d, %ecx
	andl	$248, %ecx
	cmpl	$248, %ecx
	jne	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	shll	$19, %r8d
	movzbl	(%rdi,%rax), %ecx
	shll	$11, %ecx
	shll	$8, %r9d
	orl	%r8d, %r9d
	movzbl	2(%rdi,%rax), %r8d
	andl	$3671808, %r9d          # imm = 0x380700
	orl	%ecx, %r9d
	orl	%r8d, %r9d
	leal	(%rdx,%rax), %ecx
	leal	(%rcx,%r9,2), %r9d
	movl	%r9d, %r8d
	shrl	%r8d
	movl	%r9d, %ecx
	shrl	$20, %ecx
	andb	$7, %cl
	orb	$-16, %cl
	movb	%cl, 1(%rdi,%rax)
	movl	%r9d, %ecx
	shrl	$12, %ecx
	movb	%cl, (%rdi,%rax)
	shrl	$9, %r9d
	orb	$-8, %r9b
	movb	%r9b, 3(%rdi,%rax)
	movb	%r8b, 2(%rdi,%rax)
	leaq	2(%rax), %rax
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=1
	addq	$2, %rax
	cmpq	%rsi, %rax
	jbe	.LBB1_3
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_7:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rdi,%rax), %r8d
	movl	%r8d, %ecx
	andl	$248, %ecx
	cmpl	$240, %ecx
	jne	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	movzbl	3(%rdi,%rax), %r9d
	movl	%r9d, %ecx
	andl	$248, %ecx
	cmpl	$248, %ecx
	jne	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=1
	shll	$19, %r8d
	movzbl	(%rdi,%rax), %ecx
	shll	$11, %ecx
	shll	$8, %r9d
	orl	%r8d, %r9d
	movzbl	2(%rdi,%rax), %r8d
	andl	$3671808, %r9d          # imm = 0x380700
	orl	%ecx, %r9d
	orl	%r8d, %r9d
	addl	%r9d, %r9d
	leal	(%rdx,%rax), %ecx
	subl	%ecx, %r9d
	movl	%r9d, %r8d
	shrl	%r8d
	movl	%r9d, %ecx
	shrl	$20, %ecx
	andb	$7, %cl
	orb	$-16, %cl
	movb	%cl, 1(%rdi,%rax)
	movl	%r9d, %ecx
	shrl	$12, %ecx
	movb	%cl, (%rdi,%rax)
	shrl	$9, %r9d
	orb	$-8, %r9b
	movb	%r9b, 3(%rdi,%rax)
	movb	%r8b, 2(%rdi,%rax)
	leaq	2(%rax), %rax
.LBB1_10:                               #   in Loop: Header=BB1_7 Depth=1
	addq	$2, %rax
	cmpq	%rsi, %rax
	jbe	.LBB1_7
.LBB1_11:                               # %.loopexit
	retq
.Lfunc_end1:
	.size	ARMT_Convert, .Lfunc_end1-ARMT_Convert
	.cfi_endproc

	.globl	PPC_Convert
	.p2align	4, 0x90
	.type	PPC_Convert,@function
PPC_Convert:                            # @PPC_Convert
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	cmpq	$4, %rsi
	jae	.LBB2_2
# BB#1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB2_2:
	addq	$-4, %rsi
	testl	%ecx, %ecx
	je	.LBB2_8
# BB#3:                                 # %.split.preheader
	movl	%edx, %r8d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_4:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$72, %edx
	jne	.LBB2_7
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	movzbl	3(%rdi,%rax), %r9d
	movl	%r9d, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	shll	$24, %ecx
	movzbl	1(%rdi,%rax), %edx
	shll	$16, %edx
	movzbl	2(%rdi,%rax), %ebx
	shll	$8, %ebx
	orl	%r9d, %ecx
	andl	$50331900, %ecx         # imm = 0x30000FC
	orl	%edx, %ecx
	orl	%ebx, %ecx
	leal	(%r8,%rax), %ebx
	addl	%ecx, %ebx
	movl	%ebx, %ecx
	shrl	$24, %ecx
	andb	$3, %cl
	orb	$72, %cl
	movb	%cl, (%rdi,%rax)
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rdi,%rax)
	movb	%bh, 2(%rdi,%rax)  # NOREX
	andb	$3, %r9b
	movzbl	%r9b, %ecx
	orl	%ebx, %ecx
	movb	%cl, 3(%rdi,%rax)
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=1
	addq	$4, %rax
	cmpq	%rsi, %rax
	jbe	.LBB2_4
	jmp	.LBB2_13
.LBB2_8:                                # %.split.us.preheader
	negl	%edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_9:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %ebx
	movl	%ebx, %ecx
	andl	$252, %ecx
	cmpl	$72, %ecx
	jne	.LBB2_12
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	movzbl	3(%rdi,%rax), %r8d
	movl	%r8d, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	shll	$24, %ebx
	movzbl	1(%rdi,%rax), %r9d
	shll	$16, %r9d
	movzbl	2(%rdi,%rax), %ecx
	shll	$8, %ecx
	orl	%r8d, %ebx
	andl	$50331900, %ebx         # imm = 0x30000FC
	orl	%r9d, %ebx
	orl	%ecx, %ebx
	addl	%edx, %ebx
	movl	%ebx, %ecx
	shrl	$24, %ecx
	andb	$3, %cl
	orb	$72, %cl
	movb	%cl, (%rdi,%rax)
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rdi,%rax)
	movb	%bh, 2(%rdi,%rax)  # NOREX
	andb	$3, %r8b
	movzbl	%r8b, %ecx
	orl	%ebx, %ecx
	movb	%cl, 3(%rdi,%rax)
.LBB2_12:                               #   in Loop: Header=BB2_9 Depth=1
	addq	$4, %rax
	addl	$-4, %edx
	cmpq	%rsi, %rax
	jbe	.LBB2_9
.LBB2_13:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	PPC_Convert, .Lfunc_end2-PPC_Convert
	.cfi_endproc

	.globl	SPARC_Convert
	.p2align	4, 0x90
	.type	SPARC_Convert,@function
SPARC_Convert:                          # @SPARC_Convert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpq	$4, %rsi
	jae	.LBB3_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB3_17
.LBB3_2:
	addq	$-4, %rsi
	testl	%ecx, %ecx
	je	.LBB3_7
# BB#3:                                 # %.split.preheader
	movl	$4, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_4:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %r9d
	cmpl	$127, %r9d
	je	.LBB3_14
# BB#5:                                 # %.split
                                        #   in Loop: Header=BB3_4 Depth=1
	cmpb	$64, %r9b
	jne	.LBB3_16
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	leal	-3(%rcx), %r8d
	movzbl	(%rdi,%r8), %r10d
	cmpb	$64, %r10b
	jb	.LBB3_15
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_4 Depth=1
	leal	-3(%rcx), %r8d
	movzbl	(%rdi,%r8), %r10d
	cmpb	$-64, %r10b
	jb	.LBB3_16
.LBB3_15:                               #   in Loop: Header=BB3_4 Depth=1
	shll	$24, %r9d
	movzbl	%r10b, %ebp
	shll	$16, %ebp
	orl	%r9d, %ebp
	leal	-2(%rcx), %r9d
	movzbl	(%rdi,%r9), %ebx
	shll	$8, %ebx
	orl	%ebp, %ebx
	leal	-1(%rcx), %r10d
	movzbl	(%rdi,%r10), %ebp
	orl	%ebx, %ebp
	leal	(%rdx,%rcx), %ebx
	leal	-4(%rbx,%rbp,4), %r14d
	movl	%r14d, %r11d
	shrl	$2, %r11d
	movl	%r14d, %ebp
	shrl	$24, %ebp
	andl	$1, %ebp
	negl	%ebp
	movl	%ebp, %r15d
	shll	$22, %r15d
	movl	%r11d, %ebx
	andl	$4128768, %ebx          # imm = 0x3F0000
	orl	%r15d, %ebx
	shrb	$2, %bpl
	orb	$64, %bpl
	movb	%bpl, (%rdi,%rax)
	shrl	$16, %ebx
	movb	%bl, (%rdi,%r8)
	shrl	$10, %r14d
	movb	%r14b, (%rdi,%r9)
	movb	%r11b, (%rdi,%r10)
.LBB3_16:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	%ecx, %eax
	addl	$4, %ecx
	cmpq	%rsi, %rax
	jbe	.LBB3_4
	jmp	.LBB3_17
.LBB3_7:                                # %.split.us.preheader
	negl	%edx
	movl	$4, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_8:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %r9d
	cmpl	$127, %r9d
	je	.LBB3_11
# BB#9:                                 # %.split.us
                                        #   in Loop: Header=BB3_8 Depth=1
	cmpb	$64, %r9b
	jne	.LBB3_13
# BB#10:                                #   in Loop: Header=BB3_8 Depth=1
	leal	-3(%rcx), %r8d
	movzbl	(%rdi,%r8), %r10d
	cmpb	$64, %r10b
	jb	.LBB3_12
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_8 Depth=1
	leal	-3(%rcx), %r8d
	movzbl	(%rdi,%r8), %r10d
	cmpb	$-64, %r10b
	jb	.LBB3_13
.LBB3_12:                               #   in Loop: Header=BB3_8 Depth=1
	shll	$24, %r9d
	movzbl	%r10b, %r10d
	shll	$16, %r10d
	orl	%r9d, %r10d
	leal	-2(%rcx), %r9d
	movzbl	(%rdi,%r9), %r11d
	shll	$8, %r11d
	orl	%r10d, %r11d
	leal	-1(%rcx), %r10d
	movzbl	(%rdi,%r10), %ebx
	orl	%r11d, %ebx
	leal	(%rdx,%rbx,4), %r14d
	movl	%r14d, %r11d
	shrl	$2, %r11d
	movl	%r14d, %ebp
	shrl	$24, %ebp
	andl	$1, %ebp
	negl	%ebp
	movl	%ebp, %r15d
	shll	$22, %r15d
	movl	%r11d, %ebx
	andl	$4128768, %ebx          # imm = 0x3F0000
	orl	%r15d, %ebx
	shrb	$2, %bpl
	orb	$64, %bpl
	movb	%bpl, (%rdi,%rax)
	shrl	$16, %ebx
	movb	%bl, (%rdi,%r8)
	shrl	$10, %r14d
	movb	%r14b, (%rdi,%r9)
	movb	%r11b, (%rdi,%r10)
.LBB3_13:                               # %thread-pre-split.us.thread
                                        #   in Loop: Header=BB3_8 Depth=1
	movl	%ecx, %eax
	addl	$4, %ecx
	addl	$-4, %edx
	cmpq	%rsi, %rax
	jbe	.LBB3_8
.LBB3_17:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	SPARC_Convert, .Lfunc_end3-SPARC_Convert
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
