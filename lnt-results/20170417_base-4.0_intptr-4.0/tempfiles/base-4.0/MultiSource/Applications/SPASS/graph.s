	.text
	.file	"graph.bc"
	.globl	graph_NodePrint
	.p2align	4, 0x90
	.type	graph_NodePrint,@function
graph_NodePrint:                        # @graph_NodePrint
	.cfi_startproc
# BB#0:
	movl	(%rdi), %esi
	movl	4(%rdi), %edx
	movl	8(%rdi), %ecx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	graph_NodePrint, .Lfunc_end0-graph_NodePrint
	.cfi_endproc

	.globl	graph_Create
	.p2align	4, 0x90
	.type	graph_Create,@function
graph_Create:                           # @graph_Create
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$24, %edi
	callq	memory_Malloc
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rcx
	retq
.Lfunc_end1:
	.size	graph_Create, .Lfunc_end1-graph_Create
	.cfi_endproc

	.globl	graph_Delete
	.p2align	4, 0x90
	.type	graph_Delete,@function
graph_Delete:                           # @graph_Delete
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB2_2
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_5:                                # %list_Delete.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rax), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movq	8(%rdi), %rcx
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, 8(%rdi)
	testq	%rax, %rax
	je	.LBB2_6
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	8(%rax), %rcx
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB2_3
# BB#4:                                 # %list_Delete.exit.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rdi), %rax
	jmp	.LBB2_5
.LBB2_6:                                # %._crit_edge
	movq	memory_ARRAY+192(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+192(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end2:
	.size	graph_Delete, .Lfunc_end2-graph_Delete
	.cfi_endproc

	.globl	graph_GetNode
	.p2align	4, 0x90
	.type	graph_GetNode,@function
graph_GetNode:                          # @graph_GetNode
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB3_1
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rax
	cmpl	%esi, (%rax)
	je	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_4
# BB#3:
	xorl	%eax, %eax
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.LBB3_5:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	graph_GetNode, .Lfunc_end3-graph_GetNode
	.cfi_endproc

	.globl	graph_AddNode
	.p2align	4, 0x90
	.type	graph_AddNode,@function
graph_AddNode:                          # @graph_AddNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movq	8(%r14), %rax
	testq	%rax, %rax
	jne	.LBB4_3
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_1:                                #   in Loop: Header=BB4_3 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB4_5
.LBB4_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rbx
	cmpl	%ebp, (%rbx)
	jne	.LBB4_1
# BB#4:                                 # %graph_GetNode.exit
	testq	%rbx, %rbx
	jne	.LBB4_6
.LBB4_5:                                # %graph_GetNode.exit.thread
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	8(%r14), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 8(%r14)
	movl	%ebp, (%rbx)
	movl	$-1, 4(%rbx)
	movl	$-1, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
.LBB4_6:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	graph_AddNode, .Lfunc_end4-graph_AddNode
	.cfi_endproc

	.globl	graph_AddEdge
	.p2align	4, 0x90
	.type	graph_AddEdge,@function
graph_AddEdge:                          # @graph_AddEdge
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	24(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	graph_AddEdge, .Lfunc_end5-graph_AddEdge
	.cfi_endproc

	.globl	graph_DeleteEdge
	.p2align	4, 0x90
	.type	graph_DeleteEdge,@function
graph_DeleteEdge:                       # @graph_DeleteEdge
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	graph_DeleteEdge, .Lfunc_end6-graph_DeleteEdge
	.cfi_endproc

	.globl	graph_DeleteDuplicateEdges
	.p2align	4, 0x90
	.type	graph_DeleteDuplicateEdges,@function
graph_DeleteDuplicateEdges:             # @graph_DeleteDuplicateEdges
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	movq	24(%r14), %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, 24(%r14)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_1
.LBB7_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	graph_DeleteDuplicateEdges, .Lfunc_end7-graph_DeleteDuplicateEdges
	.cfi_endproc

	.globl	graph_SortNodes
	.p2align	4, 0x90
	.type	graph_SortNodes,@function
graph_SortNodes:                        # @graph_SortNodes
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	list_Sort
	movq	%rax, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	graph_SortNodes, .Lfunc_end8-graph_SortNodes
	.cfi_endproc

	.globl	graph_StronglyConnectedComponents
	.p2align	4, 0x90
	.type	graph_StronglyConnectedComponents,@function
graph_StronglyConnectedComponents:      # @graph_StronglyConnectedComponents
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, 16(%r14)
	je	.LBB9_1
# BB#2:
	movq	$0, 16(%r14)
	leaq	8(%r14), %rax
	movq	8(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB9_5
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movl	$-1, 4(%rdx)
	movq	8(%rcx), %rdx
	movl	$-1, 8(%rdx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_3
	jmp	.LBB9_5
.LBB9_1:                                # %.graph_ReinitDFS.exit_crit_edge
	leaq	8(%r14), %rax
.LBB9_5:                                # %graph_ReinitDFS.exit
	movq	$0, graph_ROOTS(%rip)
	movq	$0, graph_UNFINISHED(%rip)
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_7
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_9:                                #   in Loop: Header=BB9_7 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB9_10
.LBB9_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	cmpl	$0, 4(%rsi)
	jns	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=1
	movq	%r14, %rdi
	callq	graph_InternSCC
	jmp	.LBB9_9
.LBB9_10:                               # %._crit_edge
	movl	20(%r14), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	graph_StronglyConnectedComponents, .Lfunc_end9-graph_StronglyConnectedComponents
	.cfi_endproc

	.p2align	4, 0x90
	.type	graph_InternSCC,@function
graph_InternSCC:                        # @graph_InternSCC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r12, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	16(%r12), %r15d
	leal	1(%r15), %eax
	movl	%eax, 16(%r12)
	movl	%r15d, 4(%r14)
	movq	graph_UNFINISHED(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, graph_UNFINISHED(%rip)
	movq	graph_ROOTS(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, graph_ROOTS(%rip)
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB10_9
	.p2align	4, 0x90
.LBB10_1:                               # %.lr.ph56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
	movq	8(%rbx), %rsi
	movl	4(%rsi), %eax
	testl	%eax, %eax
	js	.LBB10_2
# BB#3:                                 #   in Loop: Header=BB10_1 Depth=1
	cmpl	$0, 8(%rsi)
	jns	.LBB10_7
# BB#4:                                 # %.preheader50
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	graph_ROOTS(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB10_7
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph52
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	cmpl	%eax, 4(%rdx)
	jbe	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	movq	%rdx, graph_ROOTS(%rip)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB10_5
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_2:                               #   in Loop: Header=BB10_1 Depth=1
	movq	%r12, %rdi
	callq	graph_InternSCC
.LBB10_7:                               # %.critedge
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB10_1
# BB#8:                                 # %._crit_edge.loopexit
	movq	graph_ROOTS(%rip), %rax
.LBB10_9:                               # %._crit_edge
	cmpq	%r14, 8(%rax)
	jne	.LBB10_15
# BB#10:                                # %.preheader
	movq	graph_UNFINISHED(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB10_14
	.p2align	4, 0x90
.LBB10_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rax
	cmpl	%r15d, 4(%rax)
	jb	.LBB10_13
# BB#12:                                #   in Loop: Header=BB10_11 Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	movq	%rdx, graph_UNFINISHED(%rip)
	movl	20(%r12), %ecx
	movl	%ecx, 8(%rax)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB10_11
.LBB10_13:                              # %.critedge1.loopexit
	movq	graph_ROOTS(%rip), %rax
.LBB10_14:                              # %.critedge1
	incl	20(%r12)
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, graph_ROOTS(%rip)
.LBB10_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	graph_InternSCC, .Lfunc_end10-graph_InternSCC
	.cfi_endproc

	.globl	graph_Print
	.p2align	4, 0x90
	.type	graph_Print,@function
graph_Print:                            # @graph_Print
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	8(%rdi), %r14
	testq	%r14, %r14
	jne	.LBB11_2
	jmp	.LBB11_6
	.p2align	4, 0x90
.LBB11_5:                               # %._crit_edge
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB11_6
.LBB11_2:                               # %.lr.ph26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_3 Depth 2
	movq	8(%r14), %rax
	movl	(%rax), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%r14), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB11_5
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_3
	jmp	.LBB11_5
.LBB11_6:                               # %._crit_edge27
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	graph_Print, .Lfunc_end11-graph_Print
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(%d,%d,%d) "
	.size	.L.str, 12

	.type	graph_ROOTS,@object     # @graph_ROOTS
	.local	graph_ROOTS
	.comm	graph_ROOTS,8,8
	.type	graph_UNFINISHED,@object # @graph_UNFINISHED
	.local	graph_UNFINISHED
	.comm	graph_UNFINISHED,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n%u -> "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%u,"
	.size	.L.str.2, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
