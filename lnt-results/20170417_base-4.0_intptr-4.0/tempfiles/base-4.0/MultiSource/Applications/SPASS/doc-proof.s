	.text
	.file	"doc-proof.bc"
	.globl	dp_Init
	.p2align	4, 0x90
	.type	dp_Init,@function
dp_Init:                                # @dp_Init
	.cfi_startproc
# BB#0:
	movl	$0, dp_DEPTH(%rip)
	retq
.Lfunc_end0:
	.size	dp_Init, .Lfunc_end0-dp_Init
	.cfi_endproc

	.globl	dp_PrintProof
	.p2align	4, 0x90
	.type	dp_PrintProof,@function
dp_PrintProof:                          # @dp_PrintProof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	112(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	callq	pcheck_ConvertParentsInSPASSProof
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_4
# BB#1:                                 # %.lr.ph153
	movl	$.L.str, %edi
	callq	puts
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	8(%rbp), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB1_2
# BB#3:                                 # %._crit_edge154
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB1_4:
	movq	%r13, %rdi
	callq	list_Copy
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	96(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %r15
	movq	56(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	movq	40(%rbx), %rdi
	callq	list_Copy
	testq	%rbp, %rbp
	je	.LBB1_9
# BB#5:
	testq	%rax, %rax
	je	.LBB1_10
# BB#6:                                 # %.preheader.i.preheader
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_7
# BB#8:
	movq	%rax, (%rcx)
	jmp	.LBB1_10
.LBB1_9:
	movq	%rax, %rbp
.LBB1_10:                               # %list_Nconc.exit
	testq	%r15, %r15
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB1_15
# BB#11:
	testq	%rbp, %rbp
	je	.LBB1_16
# BB#12:                                # %.preheader.i124.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader.i124
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_13
# BB#14:
	movq	%rbp, (%rax)
	testq	%r15, %r15
	jne	.LBB1_16
	jmp	.LBB1_21
.LBB1_15:
	movq	%rbp, %r15
	testq	%r15, %r15
	je	.LBB1_21
.LBB1_16:                               # %.lr.ph148.preheader
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	cmpl	$15, 76(%rbp)
	jne	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_17 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB1_19:                               #   in Loop: Header=BB1_17 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_17
# BB#20:
	xorl	%ebp, %ebp
	jmp	.LBB1_22
.LBB1_21:
	movb	$1, %bpl
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB1_22:                               # %._crit_edge149
	movl	$64, %esi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	pcheck_ClauseListRemoveFlag
	movl	$64, %esi
	movq	%r15, %rdi
	callq	pcheck_ClauseListRemoveFlag
	movq	%rbx, %rdi
	callq	pcheck_MarkRecursive
	movq	%r12, %rdi
	callq	pcheck_MarkRecursive
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	testb	%bpl, %bpl
	movq	%r15, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	je	.LBB1_24
# BB#23:                                # %._crit_edge144.thread
	movq	%rbx, %rdi
	callq	list_Copy
	jmp	.LBB1_33
.LBB1_24:                               # %.lr.ph143.preheader
	xorl	%r15d, %r15d
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_25:                               # %.lr.ph143
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	testb	$64, 48(%rbp)
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_25 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB1_27:                               #   in Loop: Header=BB1_25 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_25
# BB#28:                                # %._crit_edge144
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	list_Copy
	testq	%r15, %r15
	je	.LBB1_33
# BB#29:
	testq	%rax, %rax
	je	.LBB1_34
# BB#30:                                # %.preheader.i130.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB1_31:                               # %.preheader.i130
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_31
# BB#32:
	movq	%rax, (%rcx)
	jmp	.LBB1_34
.LBB1_33:
	movq	%rax, %r15
.LBB1_34:                               # %list_Nconc.exit132
	movq	%r15, %rdi
	callq	pcheck_ClauseNumberMergeSort
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	pcheck_ReduceSPASSProof
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	pcheck_SeqProofDepth
	movl	%eax, dp_DEPTH(%rip)
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	callq	pcheck_ParentPointersToParentNumbers
	movq	%r13, %rdi
	movq	%rbx, %r13
	callq	pcheck_ParentPointersToParentNumbers
	testq	%r13, %r13
	je	.LBB1_43
# BB#35:                                # %.lr.ph138.preheader
	movb	$1, 7(%rsp)             # 1-byte Folded Spill
	xorl	%r14d, %r14d
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph138
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	cmpb	$0, 48(%rbp)
	jns	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_36 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB1_38:                               #   in Loop: Header=BB1_36 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_36
# BB#39:                                # %._crit_edge139
	movq	%r13, %rbp
	testq	%r14, %r14
	sete	%r13b
	je	.LBB1_44
# BB#40:                                # %.lr.ph
	movl	$.L.str.2, %edi
	callq	puts
	movq	%r14, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_41:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rax
	movl	(%rax), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB1_41
# BB#42:                                # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movb	%r13b, 7(%rsp)          # 1-byte Spill
	jmp	.LBB1_45
.LBB1_43:
	movb	$1, 7(%rsp)             # 1-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_46
.LBB1_44:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB1_45:                               # %._crit_edge139.thread
	movq	%rbp, %r13
.LBB1_46:                               # %._crit_edge139.thread
	movl	dp_DEPTH(%rip), %ebx
	movq	%r13, %rdi
	callq	list_Length
	movl	%eax, %ecx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ecx, %edx
	callq	printf
	movq	%r13, %rdi
	callq	clause_ListPrint
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 124(%rax)
	je	.LBB1_73
# BB#47:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	104(%rax), %r14
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strlen
	leal	5(%rax), %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.5, %esi
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	misc_OpenFile
	movq	%rax, %rbp
	movl	$.L.str.6, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.7, %edi
	movl	$22, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.8, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	fputs
	movl	$.L.str.9, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.10, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.11, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.9, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.12, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.13, %edi
	movl	$62, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.14, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.15, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movq	%rbp, %rdi
	callq	fol_FPrintDFGSignature
	movl	$.L.str.14, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	testq	%r13, %r13
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r14, 72(%rsp)          # 8-byte Spill
	je	.LBB1_56
# BB#48:                                # %.lr.ph91.i.preheader
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r14
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph91.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %r13
	cmpl	$16, 76(%r13)
	jne	.LBB1_52
# BB#50:                                #   in Loop: Header=BB1_49 Depth=1
	movq	%rbp, %rbx
	movl	48(%r13), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	testb	$8, %bpl
	movq	%r13, 8(%rax)
	jne	.LBB1_53
# BB#51:                                #   in Loop: Header=BB1_49 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB1_54
	.p2align	4, 0x90
.LBB1_52:                               #   in Loop: Header=BB1_49 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_49 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB1_54:                               #   in Loop: Header=BB1_49 Depth=1
	movq	%rbx, %rbp
.LBB1_55:                               #   in Loop: Header=BB1_49 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB1_49
	jmp	.LBB1_57
.LBB1_56:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB1_57:                               # %._crit_edge92.i
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_NReverse
	movq	%rax, %r14
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	list_NReverse
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	list_NReverse
	movq	%rax, %r15
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	callq	clause_FPrintCnfDFG
	movl	$.L.str.16, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	testq	%r15, %r15
	je	.LBB1_60
# BB#58:                                # %.lr.ph.i118.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_59:                               # %.lr.ph.i118
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	$1, %edx
	movq	%rbp, %rdi
	callq	clause_FPrintDFGStep
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_59
.LBB1_60:                               # %._crit_edge.i
	movl	$.L.str.14, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.17, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movq	%rbp, %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	misc_CloseFile
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	puts
	testq	%r14, %r14
	je	.LBB1_62
	.p2align	4, 0x90
.LBB1_61:                               # %.lr.ph.i82.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB1_61
.LBB1_62:                               # %list_Delete.exit83.i
	testq	%r13, %r13
	je	.LBB1_64
	.p2align	4, 0x90
.LBB1_63:                               # %.lr.ph.i77.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_63
.LBB1_64:                               # %list_Delete.exit78.i
	testq	%r15, %r15
	je	.LBB1_66
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB1_65
.LBB1_66:                               # %list_Delete.exit.i
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %rcx
	addq	$5, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	movq	88(%rsp), %r13          # 8-byte Reload
	jae	.LBB1_68
# BB#67:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_73
.LBB1_68:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB1_70
# BB#69:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_70:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_72
# BB#71:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_72:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB1_73:                               # %dp_FPrintDFGProof.exit
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	40(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movl	52(%rsp), %edi          # 4-byte Reload
	je	.LBB1_75
	.p2align	4, 0x90
.LBB1_74:                               # %.lr.ph.i116
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB1_74
.LBB1_75:                               # %list_Delete.exit117
	testb	%dil, %dil
	jne	.LBB1_77
	.p2align	4, 0x90
.LBB1_76:                               # %.lr.ph.i111
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB1_76
.LBB1_77:                               # %list_Delete.exit112
	testq	%r12, %r12
	je	.LBB1_79
	.p2align	4, 0x90
.LBB1_78:                               # %.lr.ph.i106
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB1_78
.LBB1_79:                               # %list_Delete.exit107
	movq	96(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB1_81
	.p2align	4, 0x90
.LBB1_80:                               # %.lr.ph.i101
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB1_80
.LBB1_81:                               # %list_Delete.exit102
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB1_83
	.p2align	4, 0x90
.LBB1_82:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB1_82
.LBB1_83:                               # %list_Delete.exit
	movq	%r13, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	dp_PrintProof, .Lfunc_end1-dp_PrintProof
	.cfi_endproc

	.type	dp_DEPTH,@object        # @dp_DEPTH
	.comm	dp_DEPTH,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nNOTE: clauses with following numbers have not been found:"
	.size	.L.str, 59

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d "
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"NOTE: Following clauses in reduced proof have incomplete parent sets:"
	.size	.L.str.2, 70

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n\nHere is a proof with depth %d, length %d :\n"
	.size	.L.str.3, 46

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s.prf"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"w"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"begin_problem(Unknown).\n\n"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"list_of_descriptions.\n"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"name({*"
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"*}).\n"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"author({*SPASS "
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"V 2.1"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"status(unsatisfiable).\n"
	.size	.L.str.12, 24

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"description({*File generated by SPASS containing a proof.*}).\n"
	.size	.L.str.13, 63

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"end_of_list.\n\n"
	.size	.L.str.14, 15

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"list_of_symbols.\n"
	.size	.L.str.15, 18

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\nlist_of_proof(SPASS).\n"
	.size	.L.str.16, 24

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"end_problem.\n\n"
	.size	.L.str.17, 15

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\nDFG Proof printed to: "
	.size	.L.str.18, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
