	.text
	.file	"write_ctables.bc"
	.p2align	4, 0x90
	.type	scanner_block_hash_fn,@function
scanner_block_hash_fn:                  # @scanner_block_hash_fn
	.cfi_startproc
# BB#0:
	movl	16(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	movq	16(%rdi), %rdx
	testb	$1, %r8b
	jne	.LBB0_4
# BB#3:
	xorl	%esi, %esi
	xorl	%eax, %eax
	cmpq	$1, %r8
	jne	.LBB0_9
	jmp	.LBB0_15
.LBB0_1:
	xorl	%eax, %eax
	retq
.LBB0_4:                                # %.lr.ph.prol
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.LBB0_5
# BB#6:
	movl	(%rax), %eax
	addl	$2, %eax
	jmp	.LBB0_7
.LBB0_5:
	movl	$1, %eax
.LBB0_7:                                # %.lr.ph.prol.loopexit
	movl	$1, %esi
	cmpq	$1, %r8
	je	.LBB0_15
.LBB0_9:                                # %.lr.ph.preheader.new
	subq	%rsi, %r8
	leaq	8(%rdx,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	shll	$4, %esi
	addl	%eax, %esi
	movq	-8(%rdx), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	movl	$1, %edi
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_10 Depth=1
	movl	(%rcx), %edi
	addl	$2, %edi
.LBB0_12:                               # %.lr.ph.120
                                        #   in Loop: Header=BB0_10 Depth=1
	addl	%esi, %edi
	movl	%edi, %esi
	shll	$4, %esi
	addl	%edi, %esi
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_10 Depth=1
	movl	(%rcx), %eax
	addl	$2, %eax
.LBB0_14:                               #   in Loop: Header=BB0_10 Depth=1
	addl	%esi, %eax
	addq	$16, %rdx
	addq	$-2, %r8
	jne	.LBB0_10
.LBB0_15:                               # %._crit_edge
	retq
.Lfunc_end0:
	.size	scanner_block_hash_fn, .Lfunc_end0-scanner_block_hash_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	scanner_block_cmp_fn,@function
scanner_block_cmp_fn:                   # @scanner_block_cmp_fn
	.cfi_startproc
# BB#0:
	movq	16(%rdx), %rax
	testl	%eax, %eax
	jle	.LBB1_7
# BB#1:                                 # %.lr.ph.preheader
	movq	16(%rdi), %r8
	movq	16(%rsi), %rdx
	movslq	%eax, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rdi,8), %rcx
	movq	(%rdx,%rdi,8), %rsi
	cmpq	%rsi, %rcx
	je	.LBB1_6
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.LBB1_8
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	testq	%rsi, %rsi
	je	.LBB1_8
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	(%rcx), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB1_8
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB1_2
.LBB1_7:
	xorl	%eax, %eax
.LBB1_8:                                # %._crit_edge
	retq
.Lfunc_end1:
	.size	scanner_block_cmp_fn, .Lfunc_end1-scanner_block_cmp_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	trans_scanner_block_hash_fn,@function
trans_scanner_block_hash_fn:            # @trans_scanner_block_hash_fn
	.cfi_startproc
# BB#0:
	movl	16(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%rdi), %rdx
	testb	$1, %cl
	jne	.LBB2_4
# BB#2:
	xorl	%esi, %esi
                                        # implicit-def: %EAX
	jmp	.LBB2_7
.LBB2_3:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB2_4:                                # %.lr.ph.prol
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.LBB2_6
# BB#5:
	movl	(%rax), %eax
	incl	%eax
	movl	$1, %esi
	movl	%eax, %edi
	cmpq	$1, %rcx
	jne	.LBB2_8
	jmp	.LBB2_14
.LBB2_6:
	xorl	%eax, %eax
	movl	$1, %esi
.LBB2_7:
	xorl	%edi, %edi
	cmpq	$1, %rcx
	je	.LBB2_14
.LBB2_8:                                # %.lr.ph.preheader.new
	subq	%rsi, %rcx
	leaq	8(%rdx,%rsi,8), %rdx
	movl	%edi, %eax
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rax,2), %r8d
	movq	-8(%rdx), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	movl	$0, %edi
	je	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	movl	(%rsi), %edi
	incl	%edi
.LBB2_11:                               # %.lr.ph.120
                                        #   in Loop: Header=BB2_9 Depth=1
	addl	%r8d, %edi
	leal	(%rdi,%rdi,2), %esi
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_9 Depth=1
	movl	(%rdi), %eax
	incl	%eax
.LBB2_13:                               #   in Loop: Header=BB2_9 Depth=1
	addl	%esi, %eax
	addq	$16, %rdx
	addq	$-2, %rcx
	jne	.LBB2_9
.LBB2_14:                               # %._crit_edge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end2:
	.size	trans_scanner_block_hash_fn, .Lfunc_end2-trans_scanner_block_hash_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	trans_scanner_block_cmp_fn,@function
trans_scanner_block_cmp_fn:             # @trans_scanner_block_cmp_fn
	.cfi_startproc
# BB#0:
	movq	16(%rdx), %rax
	testl	%eax, %eax
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%rdi), %r8
	movq	24(%rsi), %rdx
	movslq	%eax, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rdi,8), %rcx
	movq	(%rdx,%rdi,8), %rsi
	cmpq	%rsi, %rcx
	je	.LBB3_6
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.LBB3_8
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	testq	%rsi, %rsi
	je	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	(%rcx), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB3_8
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB3_2
.LBB3_7:
	xorl	%eax, %eax
.LBB3_8:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	trans_scanner_block_cmp_fn, .Lfunc_end3-trans_scanner_block_cmp_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	shift_hash_fn,@function
shift_hash_fn:                          # @shift_hash_fn
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	4(%rax), %eax
	retq
.Lfunc_end4:
	.size	shift_hash_fn, .Lfunc_end4-shift_hash_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	shift_cmp_fn,@function
shift_cmp_fn:                           # @shift_cmp_fn
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	4(%rax), %ecx
	movq	8(%rsi), %rdx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	setne	%al
	retq
.Lfunc_end5:
	.size	shift_cmp_fn, .Lfunc_end5-shift_cmp_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	er_hint_hash_fn,@function
er_hint_hash_fn:                        # @er_hint_hash_fn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 256(%r14)
	je	.LBB6_1
# BB#2:                                 # %.lr.ph
	movq	264(%r14), %rax
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %r12d
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rcx
	movq	40(%rcx), %rdx
	movl	32(%rcx), %ecx
	decl	%ecx
	movq	(%rdx,%rcx,8), %rcx
	movq	16(%rcx), %rcx
	imull	$13, (%rax), %ebp
	movq	24(%rcx), %rdi
	movl	32(%rcx), %esi
	callq	strhashl
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%r15d, %ebp
	leal	13(%rax,%rbp), %r15d
	movq	264(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	8(%rcx), %rcx
	imull	$10007, 56(%rcx), %ecx  # imm = 0x2717
	addl	%ecx, %r15d
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=1
	incl	%ebx
	cmpl	256(%r14), %ebx
	jb	.LBB6_3
	jmp	.LBB6_6
.LBB6_1:
	xorl	%r15d, %r15d
.LBB6_6:                                # %._crit_edge
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	er_hint_hash_fn, .Lfunc_end6-er_hint_hash_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	er_hint_cmp_fn,@function
er_hint_cmp_fn:                         # @er_hint_cmp_fn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	256(%rdi), %r13d
	movl	$1, %eax
	cmpl	256(%rsi), %r13d
	jne	.LBB7_10
# BB#1:                                 # %.preheader
	testl	%r13d, %r13d
	je	.LBB7_9
# BB#2:                                 # %.lr.ph
	movq	264(%rdi), %r15
	movq	264(%rsi), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rax
	movq	(%r12,%rbx,8), %rcx
	movl	(%rax), %edx
	cmpl	(%rcx), %edx
	jne	.LBB7_8
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	16(%rax), %r14
	movq	16(%rcx), %rbp
	movq	40(%rbp), %rax
	movl	32(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %rax
	movq	40(%r14), %rcx
	movl	32(%r14), %edx
	decl	%edx
	movq	(%rcx,%rdx,8), %rcx
	movq	16(%rcx), %rcx
	movq	24(%rcx), %rdi
	movq	24(%rax), %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB7_8
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	8(%r14), %rax
	movl	56(%rax), %eax
	movq	8(%rbp), %rcx
	cmpl	56(%rcx), %eax
	jne	.LBB7_8
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=1
	incq	%rbx
	cmpl	%r13d, %ebx
	jb	.LBB7_3
.LBB7_9:
	xorl	%eax, %eax
	jmp	.LBB7_10
.LBB7_8:
	movl	$1, %eax
.LBB7_10:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	er_hint_cmp_fn, .Lfunc_end7-er_hint_cmp_fn
	.cfi_endproc

	.globl	write_parser_tables_as_C
	.p2align	4, 0x90
	.type	write_parser_tables_as_C,@function
write_parser_tables_as_C:               # @write_parser_tables_as_C
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$9080, %rsp             # imm = 0x2378
.Lcfi29:
	.cfi_def_cfa_offset 9136
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rdi, %r13
	movl	$0, 384(%rsp)
	movq	$0, 392(%rsp)
	leaq	4976(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movabsq	$7310312382975140910, %r14 # imm = 0x65737261705F642E
	movq	%r14, 4976(%rsp,%rax)
	movl	$6499954, 4984(%rsp,%rax) # imm = 0x632E72
	movq	stdout(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
# BB#1:
	leaq	4976(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	d_fail
.LBB8_2:
	movl	48(%r13), %eax
	testq	%rax, %rax
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	je	.LBB8_3
# BB#4:                                 # %.lr.ph103.i
	movq	56(%r13), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB8_5
# BB#6:                                 # %.prol.preheader235
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$3, (%rsi)
	cmovel	%ebx, %ebp
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB8_7
	jmp	.LBB8_8
.LBB8_3:
	xorl	%ebp, %ebp
	jmp	.LBB8_11
.LBB8_5:
	xorl	%ebp, %ebp
.LBB8_8:                                # %.prol.loopexit236
	cmpq	$3, %r8
	movq	72(%rsp), %r13          # 8-byte Reload
	jb	.LBB8_11
# BB#9:                                 # %.lr.ph103.i.new
	subq	%rdx, %rax
	leaq	24(%rcx,%rdx,8), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB8_10:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rcx), %rsi
	cmpl	$3, (%rsi)
	movq	-16(%rcx), %rsi
	cmovel	%edx, %ebp
	cmpl	$3, (%rsi)
	movq	-8(%rcx), %rsi
	cmovel	%edx, %ebp
	cmpl	$3, (%rsi)
	movq	(%rcx), %rsi
	cmovel	%edx, %ebp
	cmpl	$3, (%rsi)
	cmovel	%edx, %ebp
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB8_10
.LBB8_11:                               # %._crit_edge104.i
	movl	$1, %r15d
	cmpl	$0, 260(%r13)
	jne	.LBB8_16
# BB#12:                                # %.preheader89.i
	cmpl	$0, 8(%r13)
	je	.LBB8_13
# BB#14:                                # %.lr.ph99.i.preheader
	xorl	%r15d, %r15d
	movl	$1, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph99.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	state_for_declaration
	testl	%eax, %eax
	cmovnel	%r12d, %r15d
	incl	%ebx
	cmpl	8(%r13), %ebx
	jb	.LBB8_15
	jmp	.LBB8_16
.LBB8_13:
	xorl	%r15d, %r15d
.LBB8_16:                               # %.loopexit90.i
	movl	544(%r13), %eax
	testl	%eax, %eax
	movq	%r13, %r12
	jg	.LBB8_19
# BB#17:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB8_44
# BB#18:
	movl	%r15d, %eax
	orl	%ebp, %eax
	je	.LBB8_44
.LBB8_19:
	leaq	880(%rsp), %rbx
	movq	%rbx, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, 880(%rsp,%rax)
	movl	$6827634, 888(%rsp,%rax) # imm = 0x682E72
	movq	stdout(%rip), %r12
	testq	%r12, %r12
	jne	.LBB8_21
# BB#20:
	leaq	880(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	d_fail
.LBB8_21:
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	fprintf
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	testl	%ebp, %ebp
	je	.LBB8_36
# BB#22:
	movq	72(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 548(%rbx)
	je	.LBB8_23
# BB#28:
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	fprintf
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.LBB8_35
# BB#29:                                # %.lr.ph96.i
	movl	$.L.str.23, %r14d
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_30:                               # =>This Inner Loop Header: Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rsi
	cmpl	$3, (%rsi)
	jne	.LBB8_34
# BB#31:                                #   in Loop: Header=BB8_30 Depth=1
	movl	32(%rsi), %ecx
	leal	7(%r13,%rcx), %r13d
	cmpl	$71, %r13d
	jl	.LBB8_33
# BB#32:                                #   in Loop: Header=BB8_30 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	48(%rcx), %eax
	xorl	%r13d, %r13d
.LBB8_33:                               #   in Loop: Header=BB8_30 Depth=1
	movq	24(%rsi), %rdx
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	8(%rbx), %ecx
	addl	4(%rsi), %ecx
	decl	%eax
	cmpq	%rax, %rbp
	movl	$.L.str.24, %r8d
	cmoveq	%r14, %r8
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	48(%rbx), %eax
.LBB8_34:                               #   in Loop: Header=BB8_30 Depth=1
	incq	%rbp
	cmpl	%eax, %ebp
	jb	.LBB8_30
.LBB8_35:                               # %._crit_edge.i
	movl	$.L.str.25, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	testl	%r15d, %r15d
	jne	.LBB8_37
	jmp	.LBB8_43
.LBB8_23:                               # %.preheader87.i
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.LBB8_36
# BB#24:                                # %.lr.ph93.i
	xorl	%ebx, %ebx
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_25:                               # =>This Inner Loop Header: Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	cmpl	$3, (%rsi)
	jne	.LBB8_27
# BB#26:                                #   in Loop: Header=BB8_25 Depth=1
	movq	24(%rsi), %rdx
	movl	8(%rbp), %ecx
	addl	4(%rsi), %ecx
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	48(%rbp), %eax
.LBB8_27:                               #   in Loop: Header=BB8_25 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB8_25
.LBB8_36:                               # %.loopexit88.i
	testl	%r15d, %r15d
	je	.LBB8_43
.LBB8_37:                               # %.preheader.i
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.LBB8_43
# BB#38:                                # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_39:                               # =>This Inner Loop Header: Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rcx
	testb	$28, 60(%rcx)
	jne	.LBB8_42
# BB#40:                                #   in Loop: Header=BB8_39 Depth=1
	cmpq	$0, 208(%rcx)
	je	.LBB8_42
# BB#41:                                #   in Loop: Header=BB8_39 Depth=1
	movq	(%rcx), %rdx
	movq	200(%rcx), %rax
	movl	(%rax), %ecx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %eax
.LBB8_42:                               #   in Loop: Header=BB8_39 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB8_39
.LBB8_43:                               # %.loopexit.i
	movl	$.L.str.27, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	$1, %ecx
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB8_44:                               # %write_header_as_C.exit
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	cmpl	$0, 152(%r12)
	movq	80(%rsp), %r14          # 8-byte Reload
	jle	.LBB8_80
# BB#45:                                # %.lr.ph.i77
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_46:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_49 Depth 2
                                        #       Child Loop BB8_54 Depth 3
                                        #       Child Loop BB8_59 Depth 3
                                        #       Child Loop BB8_68 Depth 3
	cmpl	$0, 540(%r12)
	je	.LBB8_48
# BB#47:                                #   in Loop: Header=BB8_46 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	144(%rax), %rax
	movq	%rbx, %rcx
	shlq	$4, %rcx
	movl	8(%rax,%rcx), %r15d
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbp, %rcx
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %edx
	callq	fprintf
.LBB8_48:                               #   in Loop: Header=BB8_46 Depth=1
	movq	144(%r12), %rax
	movq	%rbx, %rcx
	shlq	$4, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx), %rbp
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	jmp	.LBB8_49
	.p2align	4, 0x90
.LBB8_78:                               #   in Loop: Header=BB8_49 Depth=2
	incq	%rbp
	movq	144(%r12), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	8(%rax,%rcx), %esi
	addl	%ebx, %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	d_fail
	jmp	.LBB8_49
.LBB8_75:                               #   in Loop: Header=BB8_49 Depth=2
	movl	$.L.str.31, %esi
	movl	$6, %edx
	movq	%r15, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB8_76
.LBB8_77:                               #   in Loop: Header=BB8_49 Depth=2
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	144(%r12), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	8(%rax,%rcx), %esi
	movq	96(%rsp), %rbx          # 8-byte Reload
	addl	%ebx, %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	d_fail
	incq	%rbp
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB8_49
.LBB8_76:                               #   in Loop: Header=BB8_49 Depth=2
	movl	$3, %ecx
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	find_symbol
	movl	%eax, %ecx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, %edx
	callq	fprintf
	incq	%rbp
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB8_49
.LBB8_66:                               #   in Loop: Header=BB8_49 Depth=2
	movq	80(%rsp), %r14          # 8-byte Reload
.LBB8_70:                               # %.critedge.i.i
                                        #   in Loop: Header=BB8_49 Depth=2
	movl	$-1, %r15d
	cmpq	%rbp, %rbx
	jae	.LBB8_71
# BB#72:                                #   in Loop: Header=BB8_49 Depth=2
	movl	%ebp, %edx
	subl	%ebx, %edx
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	lookup_production
	testq	%rax, %rax
	movq	96(%rsp), %rbx          # 8-byte Reload
	je	.LBB8_74
# BB#73:                                #   in Loop: Header=BB8_49 Depth=2
	movl	56(%rax), %r15d
	jmp	.LBB8_74
.LBB8_71:                               #   in Loop: Header=BB8_49 Depth=2
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
.LBB8_74:                               # %find_symbol.exit.i
                                        #   in Loop: Header=BB8_49 Depth=2
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %edx
	callq	fprintf
	incq	%rbp
	jmp	.LBB8_49
	.p2align	4, 0x90
.LBB8_51:                               #   in Loop: Header=BB8_49 Depth=2
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fputc
	incq	%rbp
.LBB8_49:                               # %.backedge.i
                                        #   Parent Loop BB8_46 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_54 Depth 3
                                        #       Child Loop BB8_59 Depth 3
                                        #       Child Loop BB8_68 Depth 3
	movsbl	(%rbp), %edi
	cmpl	$36, %edi
	je	.LBB8_52
# BB#50:                                # %.backedge.i
                                        #   in Loop: Header=BB8_49 Depth=2
	testb	%dil, %dil
	jne	.LBB8_51
	jmp	.LBB8_79
	.p2align	4, 0x90
.LBB8_52:                               #   in Loop: Header=BB8_49 Depth=2
	cmpb	$123, 1(%rbp)
	jne	.LBB8_78
# BB#53:                                #   in Loop: Header=BB8_49 Depth=2
	leaq	2(%rbp), %r15
	addq	$3, %rbp
	movq	%rbp, %r13
	movq	%r15, %rbx
	jmp	.LBB8_54
	.p2align	4, 0x90
.LBB8_57:                               #   in Loop: Header=BB8_54 Depth=3
	incq	%rbx
	incq	%r13
.LBB8_54:                               #   Parent Loop BB8_46 Depth=1
                                        #     Parent Loop BB8_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbx), %r14
	testq	%r14, %r14
	je	.LBB8_58
# BB#55:                                #   in Loop: Header=BB8_54 Depth=3
	cmpb	$125, %r14b
	je	.LBB8_58
# BB#56:                                #   in Loop: Header=BB8_54 Depth=3
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$32, 1(%rax,%r14,2)
	je	.LBB8_57
.LBB8_58:                               # %.critedge1.i
                                        #   in Loop: Header=BB8_49 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	leaq	1(%rbx), %rbp
	movzwl	(%rcx,%r14,2), %r12d
	movw	$8192, %ax              # imm = 0x2000
	andw	%ax, %r12w
	cmoveq	%rbx, %rbp
	jmp	.LBB8_59
	.p2align	4, 0x90
.LBB8_61:                               #   in Loop: Header=BB8_59 Depth=3
	incq	%rbp
.LBB8_59:                               #   Parent Loop BB8_46 Depth=1
                                        #     Parent Loop BB8_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp), %eax
	testb	%al, %al
	je	.LBB8_62
# BB#60:                                #   in Loop: Header=BB8_59 Depth=3
	cmpb	$125, %al
	jne	.LBB8_61
.LBB8_62:                               # %.critedge.i
                                        #   in Loop: Header=BB8_49 Depth=2
	movq	%rbx, %rax
	subq	%r15, %rax
	cmpq	$6, %rax
	je	.LBB8_75
# BB#63:                                # %.critedge.i
                                        #   in Loop: Header=BB8_49 Depth=2
	cmpq	$5, %rax
	jne	.LBB8_77
# BB#64:                                #   in Loop: Header=BB8_49 Depth=2
	movl	$.L.str.29, %esi
	movl	$5, %edx
	movq	%r15, %rdi
	movq	%rcx, %r15
	callq	strncasecmp
	movq	%r15, %rcx
	testl	%eax, %eax
	jne	.LBB8_77
# BB#65:                                #   in Loop: Header=BB8_49 Depth=2
	testb	%r14b, %r14b
	je	.LBB8_66
# BB#67:                                #   in Loop: Header=BB8_49 Depth=2
	testw	%r12w, %r12w
	movq	80(%rsp), %r14          # 8-byte Reload
	je	.LBB8_70
	.p2align	4, 0x90
.LBB8_68:                               # %.lr.ph
                                        #   Parent Loop BB8_46 Depth=1
                                        #     Parent Loop BB8_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rbx
	movsbq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_70
# BB#69:                                # %._crit_edge4.i
                                        #   in Loop: Header=BB8_68 Depth=3
	leaq	1(%rbx), %r13
	testb	$32, 1(%rcx,%rax,2)
	jne	.LBB8_68
	jmp	.LBB8_70
	.p2align	4, 0x90
.LBB8_79:                               #   in Loop: Header=BB8_46 Depth=1
	movl	$10, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fputc
	incq	%rbx
	movslq	152(%r12), %rax
	cmpq	%rax, %rbx
	jl	.LBB8_46
.LBB8_80:                               # %write_global_code_as_C.exit
	movl	$.L.str.2, %edi
	movl	$20, %esi
	movl	$1, %edx
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	fwrite
	cmpl	$0, 112(%rsp)           # 4-byte Folded Reload
	je	.LBB8_82
# BB#81:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	fprintf
.LBB8_82:
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	fputc
	movl	$.L.str.34, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	lookup_production
	testq	%rax, %rax
	je	.LBB8_83
# BB#84:
	movq	24(%rax), %rax
	movq	(%rax), %r13
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	movl	$.L.str.35, %esi
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %r8
	callq	fprintf
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	movl	$.L.str.36, %esi
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %r8
	callq	fprintf
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %r8
	callq	fprintf
	cmpl	$0, 112(%r13)
	je	.LBB8_88
# BB#85:                                # %.lr.ph262.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_86:                               # %.lr.ph262.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rax
	movl	56(%rax), %ecx
	movl	(%r13), %r8d
	movq	$.L.str.52, (%rsp)
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ebx, %edx
	movq	%r14, %r9
	callq	fprintf
	incl	%ebx
	cmpl	112(%r13), %ebx
	jb	.LBB8_86
# BB#87:
	movq	72(%rsp), %r12          # 8-byte Reload
	jmp	.LBB8_88
.LBB8_83:
	xorl	%r13d, %r13d
.LBB8_88:                               # %.loopexit233.i
	movl	8(%r12), %r8d
	testl	%r8d, %r8d
	je	.LBB8_172
# BB#89:                                # %.lr.ph258.i
	xorl	%ecx, %ecx
	movq	%r13, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_90:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_92 Depth 2
                                        #       Child Loop BB8_94 Depth 3
                                        #         Child Loop BB8_105 Depth 4
                                        #     Child Loop BB8_110 Depth 2
                                        #       Child Loop BB8_117 Depth 3
                                        #       Child Loop BB8_149 Depth 3
                                        #       Child Loop BB8_153 Depth 3
	movq	16(%r12), %rax
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	16(%rax), %r14d
	movl	%r14d, %eax
	decl	%eax
	js	.LBB8_108
# BB#91:                                # %.lr.ph243.i
                                        #   in Loop: Header=BB8_90 Depth=1
	movslq	%eax, %rdx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rsi
	jmp	.LBB8_92
.LBB8_126:                              # %.preheader._crit_edge.i
                                        #   in Loop: Header=BB8_92 Depth=2
	movq	%rax, 160(%rdi)
	jmp	.LBB8_127
	.p2align	4, 0x90
.LBB8_92:                               #   Parent Loop BB8_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_94 Depth 3
                                        #         Child Loop BB8_105 Depth 4
	testq	%rdx, %rdx
	jle	.LBB8_127
# BB#93:                                # %.lr.ph239.i
                                        #   in Loop: Header=BB8_92 Depth=2
	movq	(%rsi,%rdx,8), %rdi
	movl	32(%rdi), %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_94:                               #   Parent Loop BB8_90 Depth=1
                                        #     Parent Loop BB8_92 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_105 Depth 4
	movq	(%rsi,%rbx,8), %rax
	cmpl	32(%rax), %ebp
	jne	.LBB8_107
# BB#95:                                #   in Loop: Header=BB8_94 Depth=3
	movq	80(%rdi), %rcx
	cmpq	80(%rax), %rcx
	jne	.LBB8_107
# BB#96:                                #   in Loop: Header=BB8_94 Depth=3
	movq	96(%rdi), %rcx
	cmpq	96(%rax), %rcx
	jne	.LBB8_107
# BB#97:                                #   in Loop: Header=BB8_94 Depth=3
	movl	16(%rdi), %ecx
	cmpl	16(%rax), %ecx
	jne	.LBB8_107
# BB#98:                                #   in Loop: Header=BB8_94 Depth=3
	movl	20(%rdi), %ecx
	cmpl	20(%rax), %ecx
	jne	.LBB8_107
# BB#99:                                #   in Loop: Header=BB8_94 Depth=3
	movl	24(%rdi), %ecx
	cmpl	24(%rax), %ecx
	jne	.LBB8_107
# BB#100:                               #   in Loop: Header=BB8_94 Depth=3
	movl	28(%rdi), %ecx
	cmpl	28(%rax), %ecx
	jne	.LBB8_107
# BB#101:                               #   in Loop: Header=BB8_94 Depth=3
	movl	152(%rdi), %ecx
	cmpl	152(%rax), %ecx
	jne	.LBB8_107
# BB#102:                               #   in Loop: Header=BB8_94 Depth=3
	movl	112(%rdi), %r9d
	cmpl	112(%rax), %r9d
	jne	.LBB8_107
# BB#103:                               # %.preheader.i86
                                        #   in Loop: Header=BB8_94 Depth=3
	testl	%r9d, %r9d
	je	.LBB8_126
# BB#104:                               # %.lr.ph.i87
                                        #   in Loop: Header=BB8_94 Depth=3
	movq	120(%rdi), %r10
	movq	120(%rax), %r11
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_105:                              #   Parent Loop BB8_90 Depth=1
                                        #     Parent Loop BB8_92 Depth=2
                                        #       Parent Loop BB8_94 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r10,%r13,8), %r12
	movq	(%r11,%r13,8), %r15
	testq	%r12, %r12
	je	.LBB8_106
# BB#123:                               #   in Loop: Header=BB8_105 Depth=4
	testq	%r15, %r15
	je	.LBB8_107
# BB#124:                               #   in Loop: Header=BB8_105 Depth=4
	movq	(%r12), %rcx
	cmpq	(%r15), %rcx
	je	.LBB8_125
	jmp	.LBB8_107
	.p2align	4, 0x90
.LBB8_106:                              #   in Loop: Header=BB8_105 Depth=4
	testq	%r15, %r15
	jne	.LBB8_107
.LBB8_125:                              #   in Loop: Header=BB8_105 Depth=4
	incq	%r13
	cmpl	%r9d, %r13d
	jb	.LBB8_105
	jmp	.LBB8_126
	.p2align	4, 0x90
.LBB8_107:                              # %.loopexit.i91
                                        #   in Loop: Header=BB8_94 Depth=3
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB8_94
.LBB8_127:                              # %.backedge.i90
                                        #   in Loop: Header=BB8_92 Depth=2
	decq	%rdx
	testl	%edx, %edx
	jns	.LBB8_92
.LBB8_108:                              # %.preheader232.i
                                        #   in Loop: Header=BB8_90 Depth=1
	testl	%r14d, %r14d
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	104(%rsp), %r13         # 8-byte Reload
	je	.LBB8_171
# BB#109:                               # %.lr.ph254.i
                                        #   in Loop: Header=BB8_90 Depth=1
	movq	%r12, %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_110:                              #   Parent Loop BB8_90 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_117 Depth 3
                                        #       Child Loop BB8_149 Depth 3
                                        #       Child Loop BB8_153 Depth 3
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	(%rax,%r15,8), %r12
	cmpq	$0, 160(%r12)
	jne	.LBB8_169
# BB#111:                               #   in Loop: Header=BB8_110 Depth=2
	cmpq	$0, 80(%r12)
	je	.LBB8_113
# BB#112:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r12), %rax
	movl	56(%rax), %edx
	movl	(%r12), %ecx
	movl	$.L.str.39, %esi
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	fprintf
	movq	80(%r12), %rcx
	movl	88(%r12), %r8d
	movq	%rbx, %rsi
	movq	(%rsi), %r9
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	write_code_as_C
.LBB8_113:                              #   in Loop: Header=BB8_110 Depth=2
	cmpq	$0, 96(%r12)
	je	.LBB8_115
# BB#114:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r12), %rax
	movl	56(%rax), %edx
	movl	(%r12), %ecx
	movl	$.L.str.40, %esi
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	fprintf
	movq	96(%r12), %rcx
	movl	104(%r12), %r8d
	movq	%rbx, %rsi
	movq	(%rsi), %r9
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	write_code_as_C
.LBB8_115:                              # %.preheader230.i
                                        #   in Loop: Header=BB8_110 Depth=2
	movl	112(%r12), %eax
	testl	%eax, %eax
	je	.LBB8_120
# BB#116:                               # %.lr.ph245.i
                                        #   in Loop: Header=BB8_110 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_117:                              #   Parent Loop BB8_90 Depth=1
                                        #     Parent Loop BB8_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	120(%r12), %rcx
	cmpq	$0, (%rcx,%rbx,8)
	je	.LBB8_119
# BB#118:                               #   in Loop: Header=BB8_117 Depth=3
	movq	8(%r12), %rax
	movl	56(%rax), %ecx
	movl	(%r12), %r8d
	movq	$.L.str.52, (%rsp)
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ebx, %edx
	movq	80(%rsp), %r9           # 8-byte Reload
	callq	fprintf
	movq	120(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rcx
	movl	8(%rax), %r8d
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %r9
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	write_code_as_C
	movl	112(%r12), %eax
.LBB8_119:                              #   in Loop: Header=BB8_117 Depth=3
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB8_117
.LBB8_120:                              # %._crit_edge246.i
                                        #   in Loop: Header=BB8_110 Depth=2
	cmpq	$0, 80(%r12)
	je	.LBB8_128
# BB#121:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r12), %rax
	movl	56(%rax), %edx
	movl	(%r12), %ecx
.LBB8_122:                              #   in Loop: Header=BB8_110 Depth=2
	movl	$.L.str.42, %esi
	xorl	%eax, %eax
	leaq	464(%rsp), %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	sprintf
	cmpq	$0, 96(%r12)
	jne	.LBB8_133
	jmp	.LBB8_135
.LBB8_128:                              #   in Loop: Header=BB8_110 Depth=2
	testq	%r13, %r13
	je	.LBB8_131
# BB#129:                               #   in Loop: Header=BB8_110 Depth=2
	cmpq	$0, 80(%r13)
	je	.LBB8_131
# BB#130:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	jmp	.LBB8_122
.LBB8_131:                              #   in Loop: Header=BB8_110 Depth=2
	movb	$0, 468(%rsp)
	movl	$1280070990, 464(%rsp)  # imm = 0x4C4C554E
	cmpq	$0, 96(%r12)
	je	.LBB8_135
.LBB8_133:                              #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r12), %rax
	movl	56(%rax), %edx
	movl	(%r12), %ecx
.LBB8_134:                              #   in Loop: Header=BB8_110 Depth=2
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	leaq	880(%rsp), %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	sprintf
	jmp	.LBB8_139
.LBB8_135:                              #   in Loop: Header=BB8_110 Depth=2
	testq	%r13, %r13
	je	.LBB8_138
# BB#136:                               #   in Loop: Header=BB8_110 Depth=2
	cmpq	$0, 96(%r13)
	je	.LBB8_138
# BB#137:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	jmp	.LBB8_134
.LBB8_138:                              #   in Loop: Header=BB8_110 Depth=2
	movb	$0, 884(%rsp)
	movl	$1280070990, 880(%rsp)  # imm = 0x4C4C554E
.LBB8_139:                              #   in Loop: Header=BB8_110 Depth=2
	movl	112(%r12), %r14d
	testl	%r14d, %r14d
	je	.LBB8_140
# BB#143:                               #   in Loop: Header=BB8_110 Depth=2
	testq	%r13, %r13
	je	.LBB8_144
# BB#145:                               #   in Loop: Header=BB8_110 Depth=2
	movl	112(%r13), %eax
	cmpl	%r14d, %eax
	cmoval	%eax, %r14d
	movb	$1, %al
	movl	%eax, 112(%rsp)         # 4-byte Spill
	jmp	.LBB8_146
.LBB8_140:                              #   in Loop: Header=BB8_110 Depth=2
	testq	%r13, %r13
	je	.LBB8_165
# BB#141:                               #   in Loop: Header=BB8_110 Depth=2
	movl	112(%r13), %r14d
	testl	%r14d, %r14d
	je	.LBB8_165
# BB#142:                               #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r13), %rax
	movl	56(%rax), %edx
	movl	(%r13), %ecx
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	leaq	128(%rsp), %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	sprintf
	jmp	.LBB8_166
.LBB8_144:                              #   in Loop: Header=BB8_110 Depth=2
	movl	$0, 112(%rsp)           # 4-byte Folded Spill
.LBB8_146:                              # %.thread287.i
                                        #   in Loop: Header=BB8_110 Depth=2
	movq	8(%r12), %rax
	movl	56(%rax), %edx
	movl	(%r12), %ecx
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	leaq	128(%rsp), %rbx
	movq	%rbx, %rdi
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	sprintf
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	movq	%r14, 96(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB8_164
# BB#147:                               # %.lr.ph251.i
                                        #   in Loop: Header=BB8_110 Depth=2
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	movslq	%eax, %r14
	movl	%ecx, %ebp
	cmpb	$0, 112(%rsp)           # 1-byte Folded Reload
	je	.LBB8_148
# BB#152:                               # %.lr.ph251.split.us.i.preheader
                                        #   in Loop: Header=BB8_110 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_153:                              # %.lr.ph251.split.us.i
                                        #   Parent Loop BB8_90 Depth=1
                                        #     Parent Loop BB8_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	112(%r12), %eax
	cmpq	%rbx, %rax
	jbe	.LBB8_157
# BB#154:                               #   in Loop: Header=BB8_153 Depth=3
	movq	120(%r12), %rax
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB8_157
# BB#155:                               #   in Loop: Header=BB8_153 Depth=3
	movq	8(%r12), %rax
	movl	56(%rax), %ecx
	movl	(%r12), %r8d
	jmp	.LBB8_156
	.p2align	4, 0x90
.LBB8_157:                              #   in Loop: Header=BB8_153 Depth=3
	movl	112(%r13), %eax
	cmpq	%rbx, %rax
	jbe	.LBB8_160
# BB#158:                               #   in Loop: Header=BB8_153 Depth=3
	movq	120(%r13), %rax
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB8_160
# BB#159:                               #   in Loop: Header=BB8_153 Depth=3
	movq	8(%r13), %rax
	movl	56(%rax), %ecx
	movl	(%r13), %r8d
.LBB8_156:                              #   in Loop: Header=BB8_153 Depth=3
	cmpq	%r14, %rbx
	movl	$.L.str.23, %eax
	movl	$.L.str.24, %edx
	cmovlq	%rdx, %rax
	movq	%rax, (%rsp)
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %edx
	movq	80(%rsp), %r9           # 8-byte Reload
	callq	fprintf
	jmp	.LBB8_161
	.p2align	4, 0x90
.LBB8_160:                              #   in Loop: Header=BB8_153 Depth=3
	cmpq	%r14, %rbx
	movl	$.L.str.23, %edx
	movl	$.L.str.24, %eax
	cmovlq	%rax, %rdx
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	fprintf
.LBB8_161:                              #   in Loop: Header=BB8_153 Depth=3
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB8_153
	jmp	.LBB8_164
.LBB8_165:                              #   in Loop: Header=BB8_110 Depth=2
	movb	$0, 132(%rsp)
	movl	$1280070990, 128(%rsp)  # imm = 0x4C4C554E
	xorl	%r14d, %r14d
	jmp	.LBB8_166
.LBB8_148:                              # %.lr.ph251.split.i.preheader
                                        #   in Loop: Header=BB8_110 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_149:                              # %.lr.ph251.split.i
                                        #   Parent Loop BB8_90 Depth=1
                                        #     Parent Loop BB8_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	112(%r12), %eax
	cmpq	%rbx, %rax
	jbe	.LBB8_162
# BB#150:                               #   in Loop: Header=BB8_149 Depth=3
	movq	120(%r12), %rax
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB8_162
# BB#151:                               #   in Loop: Header=BB8_149 Depth=3
	movq	8(%r12), %rax
	movl	56(%rax), %ecx
	movl	(%r12), %r8d
	cmpq	%r14, %rbx
	movl	$.L.str.23, %eax
	movl	$.L.str.24, %edx
	cmovlq	%rdx, %rax
	movq	%rax, (%rsp)
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %edx
	movq	80(%rsp), %r9           # 8-byte Reload
	callq	fprintf
	jmp	.LBB8_163
	.p2align	4, 0x90
.LBB8_162:                              #   in Loop: Header=BB8_149 Depth=3
	cmpq	%r14, %rbx
	movl	$.L.str.23, %edx
	movl	$.L.str.24, %eax
	cmovlq	%rax, %rdx
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	fprintf
.LBB8_163:                              #   in Loop: Header=BB8_149 Depth=3
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB8_149
.LBB8_164:                              # %._crit_edge252.i
                                        #   in Loop: Header=BB8_110 Depth=2
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	fwrite
	movq	96(%rsp), %r14          # 8-byte Reload
.LBB8_166:                              #   in Loop: Header=BB8_110 Depth=2
	movl	(%r12), %edx
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	fprintf
	movl	32(%r12), %edx
	movq	8(%r12), %rax
	movl	56(%rax), %ecx
	movl	20(%r12), %r8d
	movl	28(%r12), %esi
	movl	16(%r12), %edi
	movq	%rbp, %r13
	movl	24(%r12), %ebp
	movl	$-1, %ebx
	testb	$28, 60(%rax)
	jne	.LBB8_168
# BB#167:                               #   in Loop: Header=BB8_110 Depth=2
	movl	152(%r12), %ebx
.LBB8_168:                              #   in Loop: Header=BB8_110 Depth=2
	leaq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
	movl	%r14d, 40(%rsp)
	movl	%ebx, 32(%rsp)
	movl	%ebp, 24(%rsp)
	movl	%edi, 16(%rsp)
	movl	%esi, 8(%rsp)
	movl	%r8d, (%rsp)
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	464(%rsp), %r8
	leaq	880(%rsp), %r9
	callq	fprintf
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %r14d
	movq	%r13, %rbp
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	104(%rsp), %r13         # 8-byte Reload
.LBB8_169:                              #   in Loop: Header=BB8_110 Depth=2
	incq	%r15
	cmpl	%r14d, %r15d
	jb	.LBB8_110
# BB#170:                               # %._crit_edge255.i.loopexit
                                        #   in Loop: Header=BB8_90 Depth=1
	movq	%rbx, %r12
	movl	8(%r12), %r8d
.LBB8_171:                              # %._crit_edge255.i
                                        #   in Loop: Header=BB8_90 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpl	%r8d, %ecx
	jb	.LBB8_90
.LBB8_172:                              # %write_reductions_as_C.exit
	cmpl	$0, 48(%r12)
	movq	80(%rsp), %r15          # 8-byte Reload
	je	.LBB8_179
# BB#173:                               # %.lr.ph504.i
	leaq	880(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_174:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	40(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB8_177
# BB#175:                               #   in Loop: Header=BB8_174 Depth=1
	movq	24(%rcx), %rdx
	movq	(%rdx), %rsi
	cmpq	$0, 80(%rsi)
	je	.LBB8_177
# BB#176:                               #   in Loop: Header=BB8_174 Depth=1
	movl	56(%rcx), %edx
	movl	(%rsi), %ecx
	movl	$.L.str.42, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %r8
	callq	sprintf
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	56(%r12), %rax
	jmp	.LBB8_178
	.p2align	4, 0x90
.LBB8_177:                              #   in Loop: Header=BB8_174 Depth=1
	movb	$0, 884(%rsp)
	movl	$1280070990, 880(%rsp)  # imm = 0x4C4C554E
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB8_178:                              #   in Loop: Header=BB8_174 Depth=1
	movq	(%rax,%rbp,8), %rax
	movl	8(%r12), %r8d
	addl	4(%rax), %r8d
	movzbl	36(%rax), %r9d
	andl	$7, %r9d
	movl	8(%rax), %ecx
	movl	12(%rax), %edx
	movl	16(%rax), %eax
	movq	%r14, 24(%rsp)
	movl	%ecx, 16(%rsp)
	movl	%eax, 8(%rsp)
	movl	%edx, (%rsp)
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %edx
	movq	%r15, %rcx
	callq	fprintf
	incq	%rbp
	cmpl	48(%r12), %ebp
	jb	.LBB8_174
.LBB8_179:                              # %._crit_edge505.i
	movl	$10, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fputc
	movl	88(%r12), %ebp
	testq	%rbp, %rbp
	je	.LBB8_180
# BB#181:                               # %.lr.ph498.i
	movq	96(%r12), %rdx
	movl	532(%r12), %eax
	leaq	-1(%rbp), %r8
	movq	%rbp, %rbx
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	andq	$3, %rbx
	je	.LBB8_183
	.p2align	4, 0x90
.LBB8_182:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rsi
	movl	296(%rsi), %esi
	imull	%eax, %esi
	addl	%esi, %ecx
	incq	%rdi
	cmpq	%rdi, %rbx
	jne	.LBB8_182
.LBB8_183:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB8_186
# BB#184:                               # %.lr.ph498.i.new
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	leaq	24(%rdx,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB8_185:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rdi
	movq	-16(%rdx), %rbx
	movl	296(%rdi), %edi
	imull	%eax, %edi
	addl	%ecx, %edi
	movl	296(%rbx), %ecx
	imull	%eax, %ecx
	addl	%edi, %ecx
	movq	-8(%rdx), %rdi
	movl	296(%rdi), %edi
	imull	%eax, %edi
	addl	%ecx, %edi
	movq	(%rdx), %rcx
	movl	296(%rcx), %ecx
	imull	%eax, %ecx
	addl	%edi, %ecx
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB8_185
	jmp	.LBB8_186
.LBB8_180:
	xorl	%ecx, %ecx
.LBB8_186:                              # %._crit_edge499.i
	movslq	%ecx, %rax
	movq	%rax, %rcx
	shlq	$5, %rcx
	testl	%eax, %eax
	movl	$32, %edi
	cmovneq	%rcx, %rdi
	callq	malloc
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testl	%ebp, %ebp
	movl	$0, 128(%rsp)
	movq	$0, 136(%rsp)
	movl	$0, 720(%rsp)
	movq	$0, 728(%rsp)
	movl	$0, 168(%rsp)
	movq	$0, 176(%rsp)
	movl	$0, 760(%rsp)
	movq	$0, 768(%rsp)
	movl	$0, 208(%rsp)
	movq	$0, 216(%rsp)
	movl	$0, 800(%rsp)
	movq	$0, 808(%rsp)
	movl	$0, 248(%rsp)
	movq	$0, 256(%rsp)
	movl	$0, 840(%rsp)
	movq	$0, 848(%rsp)
	movq	72(%rsp), %rsi          # 8-byte Reload
	movslq	536(%rsi), %rax
	movq	%rax, scanner_block_fns+16(%rip)
	movq	%rsi, scanner_block_fns+24(%rip)
	movq	%rax, trans_scanner_block_fns+16(%rip)
	movq	%rsi, trans_scanner_block_fns+24(%rip)
	movl	$0, 424(%rsp)
	movq	$0, 432(%rsp)
	movq	64(%rsp), %r13          # 8-byte Reload
	je	.LBB8_346
# BB#187:                               # %.lr.ph493.i
	movl	$.L.str.76, %r15d
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_188:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_192 Depth 2
                                        #     Child Loop BB8_196 Depth 2
                                        #       Child Loop BB8_198 Depth 3
                                        #     Child Loop BB8_203 Depth 2
                                        #     Child Loop BB8_231 Depth 2
                                        #       Child Loop BB8_234 Depth 3
                                        #         Child Loop BB8_245 Depth 4
                                        #         Child Loop BB8_265 Depth 4
                                        #       Child Loop BB8_276 Depth 3
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	96(%rsi), %rax
	movq	(%rax,%rbp,8), %r14
	cmpl	$0, 136(%r14)
	je	.LBB8_194
# BB#189:                               #   in Loop: Header=BB8_188 Depth=1
	cmpq	$0, 400(%r14)
	jne	.LBB8_194
# BB#190:                               #   in Loop: Header=BB8_188 Depth=1
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebp, %edx
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
	movl	136(%r14), %eax
	testl	%eax, %eax
	je	.LBB8_193
# BB#191:                               # %.lr.ph451.i
                                        #   in Loop: Header=BB8_188 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_192:                              #   Parent Loop BB8_188 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%r14), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	movl	4(%rcx), %edx
	decl	%eax
	cmpq	%rax, %rbx
	movl	$.L.str.24, %r8d
	cmoveq	%r15, %r8
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rcx
	callq	fprintf
	incq	%rbx
	movl	136(%r14), %eax
	cmpl	%eax, %ebx
	jb	.LBB8_192
.LBB8_193:                              # %._crit_edge452.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB8_194:                              # %.preheader431.i
                                        #   in Loop: Header=BB8_188 Depth=1
	cmpl	$0, 336(%r14)
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	je	.LBB8_205
# BB#195:                               # %.lr.ph458.i
                                        #   in Loop: Header=BB8_188 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_196:                              #   Parent Loop BB8_188 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_198 Depth 3
	movq	344(%r14), %rax
	movq	(%rax,%r15,8), %rbp
	movl	$.L.str.77, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r15d, %ecx
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	%r12, %r8
	callq	fprintf
	cmpl	$0, 48(%rbp)
	je	.LBB8_199
# BB#197:                               # %.lr.ph455.i
                                        #   in Loop: Header=BB8_196 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_198:                              #   Parent Loop BB8_188 Depth=1
                                        #     Parent Loop BB8_196 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	56(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movl	4(%rax), %edx
	movl	$.L.str.78, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rcx
	callq	fprintf
	incq	%rbx
	cmpl	48(%rbp), %ebx
	jb	.LBB8_198
.LBB8_199:                              # %._crit_edge456.i
                                        #   in Loop: Header=BB8_196 Depth=2
	movl	$.L.str.79, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	incq	%r15
	movl	336(%r14), %eax
	cmpl	%eax, %r15d
	jb	.LBB8_196
# BB#200:                               # %._crit_edge459.i
                                        #   in Loop: Header=BB8_188 Depth=1
	testl	%eax, %eax
	movl	$.L.str.4, %r12d
	movq	96(%rsp), %rbp          # 8-byte Reload
	je	.LBB8_205
# BB#201:                               #   in Loop: Header=BB8_188 Depth=1
	movl	$.L.str.80, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebp, %edx
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	callq	fprintf
	movl	336(%r14), %eax
	testl	%eax, %eax
	je	.LBB8_204
# BB#202:                               # %.lr.ph463.i.preheader
                                        #   in Loop: Header=BB8_188 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_203:                              # %.lr.ph463.i
                                        #   Parent Loop BB8_188 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%eax
	cmpl	%eax, %ebx
	movl	$.L.str.82, %r9d
	cmoveq	%r12, %r9
	movl	$.L.str.81, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movq	%r15, %r8
	callq	fprintf
	incl	%ebx
	movl	336(%r14), %eax
	cmpl	%eax, %ebx
	jb	.LBB8_203
.LBB8_204:                              # %._crit_edge464.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB8_205:                              # %._crit_edge459.thread.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	296(%r14), %eax
	cmpl	$254, %eax
	ja	.LBB8_215
# BB#206:                               #   in Loop: Header=BB8_188 Depth=1
	movl	336(%r14), %ecx
	cmpl	$255, %ecx
	movq	72(%rsp), %rsi          # 8-byte Reload
	jae	.LBB8_217
# BB#207:                               #   in Loop: Header=BB8_188 Depth=1
	leaq	128(%rsp), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	leaq	464(%rsp), %r12
	testl	%eax, %eax
	jne	.LBB8_230
	jmp	.LBB8_229
	.p2align	4, 0x90
.LBB8_215:                              # %thread-pre-split.i.i
                                        #   in Loop: Header=BB8_188 Depth=1
	cmpl	$32383, %eax            # imm = 0x7E7F
	movq	72(%rsp), %rsi          # 8-byte Reload
	ja	.LBB8_219
# BB#216:                               # %thread-pre-split.thread.ithread-pre-split.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	336(%r14), %ecx
.LBB8_217:                              # %thread-pre-split.thread.i.i
                                        #   in Loop: Header=BB8_188 Depth=1
	cmpl	$32384, %ecx            # imm = 0x7E80
	jae	.LBB8_219
# BB#218:                               #   in Loop: Header=BB8_188 Depth=1
	movl	$1, %ecx
	jmp	.LBB8_220
	.p2align	4, 0x90
.LBB8_219:                              #   in Loop: Header=BB8_188 Depth=1
	movl	$3, %ecx
.LBB8_220:                              # %scanner_size.exit.i
                                        #   in Loop: Header=BB8_188 Depth=1
	leaq	(%rcx,%rcx,4), %rcx
	leaq	128(%rsp,%rcx,8), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	cmpl	$254, %eax
	leaq	464(%rsp), %r12
	ja	.LBB8_223
# BB#221:                               # %scanner_size.exit.thread.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	336(%r14), %ecx
	cmpl	$255, %ecx
	jae	.LBB8_225
# BB#222:                               #   in Loop: Header=BB8_188 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.LBB8_230
	jmp	.LBB8_229
	.p2align	4, 0x90
.LBB8_223:                              # %thread-pre-split.i422.i
                                        #   in Loop: Header=BB8_188 Depth=1
	cmpl	$32383, %eax            # imm = 0x7E7F
	ja	.LBB8_227
# BB#224:                               # %thread-pre-split.thread.i423thread-pre-split.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movl	336(%r14), %ecx
.LBB8_225:                              # %thread-pre-split.thread.i423.i
                                        #   in Loop: Header=BB8_188 Depth=1
	cmpl	$32384, %ecx            # imm = 0x7E80
	jae	.LBB8_227
# BB#226:                               #   in Loop: Header=BB8_188 Depth=1
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB8_230
	jmp	.LBB8_229
	.p2align	4, 0x90
.LBB8_227:                              #   in Loop: Header=BB8_188 Depth=1
	movl	$3, %ecx
	testl	%eax, %eax
	je	.LBB8_229
.LBB8_230:                              # %.lr.ph487.i
                                        #   in Loop: Header=BB8_188 Depth=1
	leaq	(%rcx,%rcx,4), %rax
	leaq	720(%rsp,%rax,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	movq	88(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_231:                              #   Parent Loop BB8_188 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_234 Depth 3
                                        #         Child Loop BB8_245 Depth 4
                                        #         Child Loop BB8_265 Depth 4
                                        #       Child Loop BB8_276 Depth 3
	cmpq	$0, 400(%r14)
	jne	.LBB8_283
# BB#232:                               # %.preheader430.i
                                        #   in Loop: Header=BB8_231 Depth=2
	cmpl	$0, 532(%rsi)
	jle	.LBB8_273
# BB#233:                               # %.lr.ph477.i
                                        #   in Loop: Header=BB8_231 Depth=2
	movslq	%ecx, %rcx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_234:                              #   Parent Loop BB8_188 Depth=1
                                        #     Parent Loop BB8_231 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_245 Depth 4
                                        #         Child Loop BB8_265 Depth 4
	movl	(%r14), %eax
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	shlq	$5, %rcx
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rcx), %rbp
	movl	%eax, (%rbx,%rcx)
	movl	%r15d, 4(%rbx,%rcx)
	movl	%r13d, 8(%rbx,%rcx)
	movq	304(%r14), %rax
	movq	(%rax,%r15,8), %rdx
	movq	%rsi, %r12
	movslq	536(%rsi), %rsi
	movslq	%r13d, %rdi
	imulq	%rsi, %rdi
	leaq	8(%rdx,%rdi,8), %rdx
	movq	%rdx, 16(%rbx,%rcx)
	movq	(%rax,%r15,8), %rax
	leaq	2136(%rax,%rdi,8), %rax
	movq	%rax, 24(%rbx,%rcx)
	movl	$scanner_block_fns, %edx
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %rsi
	callq	set_add_fn
	cmpq	%rax, %rbp
	jne	.LBB8_253
# BB#235:                               #   in Loop: Header=BB8_234 Depth=3
	movl	296(%r14), %eax
	cmpl	$254, %eax
	ja	.LBB8_238
# BB#236:                               #   in Loop: Header=BB8_234 Depth=3
	movl	336(%r14), %eax
	cmpl	$255, %eax
	jae	.LBB8_240
# BB#237:                               #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.100, %edx
	jmp	.LBB8_243
.LBB8_238:                              # %thread-pre-split.i.i414.i
                                        #   in Loop: Header=BB8_234 Depth=3
	cmpl	$32383, %eax            # imm = 0x7E7F
	ja	.LBB8_242
# BB#239:                               # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge.i417.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	336(%r14), %eax
.LBB8_240:                              # %thread-pre-split.thread.i.i418.i
                                        #   in Loop: Header=BB8_234 Depth=3
	cmpl	$32384, %eax            # imm = 0x7E80
	jae	.LBB8_242
# BB#241:                               #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.101, %edx
	jmp	.LBB8_243
.LBB8_242:                              # %scanner_size.exit.i419.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.102, %edx
.LBB8_243:                              # %scanner_type.exit421.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	$.L.str.83, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r15d, %r8d
	movl	%r13d, %r9d
	callq	fprintf
	movl	536(%r12), %eax
	testl	%eax, %eax
	jle	.LBB8_252
# BB#244:                               # %.lr.ph466.i.preheader
                                        #   in Loop: Header=BB8_234 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_245:                              # %.lr.ph466.i
                                        #   Parent Loop BB8_188 Depth=1
                                        #     Parent Loop BB8_231 Depth=2
                                        #       Parent Loop BB8_234 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	%r13d, %eax
	addl	%ebx, %eax
	movq	304(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	cltq
	movq	8(%rcx,%rax,8), %rax
	testq	%rax, %rax
	movl	$0, %edx
	je	.LBB8_247
# BB#246:                               #   in Loop: Header=BB8_245 Depth=4
	movl	(%rax), %edx
	incl	%edx
.LBB8_247:                              #   in Loop: Header=BB8_245 Depth=4
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	fprintf
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpl	536(%rax), %ebx
	je	.LBB8_249
# BB#248:                               #   in Loop: Header=BB8_245 Depth=4
	movl	$44, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fputc
.LBB8_249:                              #   in Loop: Header=BB8_245 Depth=4
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ebx, %eax
	andl	$-16, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$15, %ecx
	jne	.LBB8_251
# BB#250:                               #   in Loop: Header=BB8_245 Depth=4
	movl	$10, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fputc
.LBB8_251:                              #   in Loop: Header=BB8_245 Depth=4
	incl	%ebx
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	536(%rax), %eax
	cmpl	%eax, %ebx
	jl	.LBB8_245
.LBB8_252:                              # %._crit_edge467.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB8_253:                              #   in Loop: Header=BB8_234 Depth=3
	movb	376(%r14), %al
	andb	$24, %al
	cmpb	$8, %al
	je	.LBB8_271
# BB#254:                               #   in Loop: Header=BB8_234 Depth=3
	movl	$trans_scanner_block_fns, %edx
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %rsi
	callq	set_add_fn
	cmpq	%rax, %rbp
	jne	.LBB8_271
# BB#255:                               #   in Loop: Header=BB8_234 Depth=3
	movl	296(%r14), %eax
	cmpl	$254, %eax
	ja	.LBB8_258
# BB#256:                               #   in Loop: Header=BB8_234 Depth=3
	movl	336(%r14), %eax
	cmpl	$255, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	jae	.LBB8_260
# BB#257:                               #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.100, %edx
	jmp	.LBB8_263
.LBB8_258:                              # %thread-pre-split.i.i407.i
                                        #   in Loop: Header=BB8_234 Depth=3
	cmpl	$32383, %eax            # imm = 0x7E7F
	movq	64(%rsp), %rbp          # 8-byte Reload
	ja	.LBB8_262
# BB#259:                               # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge.i410.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	336(%r14), %eax
.LBB8_260:                              # %thread-pre-split.thread.i.i411.i
                                        #   in Loop: Header=BB8_234 Depth=3
	cmpl	$32384, %eax            # imm = 0x7E80
	jae	.LBB8_262
# BB#261:                               #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.101, %edx
	jmp	.LBB8_263
.LBB8_262:                              # %scanner_size.exit.i412.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.102, %edx
.LBB8_263:                              # %scanner_type.exit.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	96(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r15d, %r8d
	movl	%r13d, %r9d
	callq	fprintf
	movl	536(%r12), %eax
	testl	%eax, %eax
	jle	.LBB8_270
# BB#264:                               # %.lr.ph469.i.preheader
                                        #   in Loop: Header=BB8_234 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_265:                              # %.lr.ph469.i
                                        #   Parent Loop BB8_188 Depth=1
                                        #     Parent Loop BB8_231 Depth=2
                                        #       Parent Loop BB8_234 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	imull	%r13d, %eax
	addl	%ebx, %eax
	movq	304(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	cltq
	movq	2136(%rcx,%rax,8), %rax
	movl	(%rax), %edx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	cmpl	536(%r12), %ebx
	je	.LBB8_267
# BB#266:                               #   in Loop: Header=BB8_265 Depth=4
	movl	$44, %edi
	movq	%rbp, %rsi
	callq	fputc
.LBB8_267:                              #   in Loop: Header=BB8_265 Depth=4
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ebx, %eax
	andl	$-16, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$15, %ecx
	jne	.LBB8_269
# BB#268:                               #   in Loop: Header=BB8_265 Depth=4
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	fputc
.LBB8_269:                              #   in Loop: Header=BB8_265 Depth=4
	incl	%ebx
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	536(%r12), %eax
	cmpl	%eax, %ebx
	jl	.LBB8_265
.LBB8_270:                              # %._crit_edge470.i
                                        #   in Loop: Header=BB8_234 Depth=3
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	.p2align	4, 0x90
.LBB8_271:                              #   in Loop: Header=BB8_234 Depth=3
	movq	88(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	incl	%r13d
	movq	%r12, %rsi
	cmpl	532(%rsi), %r13d
	jl	.LBB8_234
# BB#272:                               # %._crit_edge478.loopexit.i
                                        #   in Loop: Header=BB8_231 Depth=2
	leaq	464(%rsp), %r12
.LBB8_273:                              # %._crit_edge478.i
                                        #   in Loop: Header=BB8_231 Depth=2
	leaq	424(%rsp), %r13
	movq	304(%r14), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	$0, 2056(%rax)
	je	.LBB8_274
# BB#275:                               # %.lr.ph482.preheader.i
                                        #   in Loop: Header=BB8_231 Depth=2
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_276:                              # %.lr.ph482.i
                                        #   Parent Loop BB8_188 Depth=1
                                        #     Parent Loop BB8_231 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	2064(%rax), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	$.L.str.86, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r15d, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	sprintf
	movq	304(%r14), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	$1, 2056(%rax)
	jne	.LBB8_278
# BB#277:                               #   in Loop: Header=BB8_276 Depth=3
	movq	%r12, %rdi
	callq	__strdup
	movq	%rax, 40(%rbx)
	movl	$shift_fns, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	set_add_fn
	cmpq	%rbx, %rax
	jne	.LBB8_281
.LBB8_278:                              #   in Loop: Header=BB8_276 Depth=3
	testq	%rbp, %rbp
	jne	.LBB8_280
# BB#279:                               #   in Loop: Header=BB8_276 Depth=3
	movl	$.L.str.87, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	fprintf
.LBB8_280:                              #   in Loop: Header=BB8_276 Depth=3
	movq	8(%rbx), %rax
	movl	4(%rax), %edx
	movq	304(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movl	2056(%rax), %eax
	decl	%eax
	cmpq	%rax, %rbp
	movl	$.L.str.24, %r8d
	movl	$.L.str.88, %eax
	cmoveq	%rax, %r8
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	fprintf
.LBB8_281:                              #   in Loop: Header=BB8_276 Depth=3
	incq	%rbp
	movq	304(%r14), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	2056(%rax), %ebp
	jb	.LBB8_276
# BB#282:                               #   in Loop: Header=BB8_231 Depth=2
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB8_283
.LBB8_274:                              #   in Loop: Header=BB8_231 Depth=2
	movq	64(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_283:                              # %.loopexit.i94
                                        #   in Loop: Header=BB8_231 Depth=2
	incq	%r15
	cmpl	296(%r14), %r15d
	jb	.LBB8_231
	jmp	.LBB8_284
	.p2align	4, 0x90
.LBB8_229:                              #   in Loop: Header=BB8_188 Depth=1
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB8_284:                              # %._crit_edge488.i
                                        #   in Loop: Header=BB8_188 Depth=1
	movq	96(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movl	88(%rsi), %eax
	cmpl	%eax, %ebp
	movl	$.L.str.76, %r15d
	jb	.LBB8_188
# BB#208:                               # %.preheader428.i
	testl	%eax, %eax
	je	.LBB8_346
# BB#209:                               # %.lr.ph448.i
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_210:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_300 Depth 2
                                        #       Child Loop BB8_307 Depth 3
                                        #     Child Loop BB8_334 Depth 2
                                        #       Child Loop BB8_336 Depth 3
	movq	96(%rsi), %rax
	movq	(%rax,%rcx,8), %r12
	movl	296(%r12), %r9d
	testl	%r9d, %r9d
	je	.LBB8_345
# BB#211:                               #   in Loop: Header=BB8_210 Depth=1
	cmpq	$0, 400(%r12)
	jne	.LBB8_345
# BB#212:                               #   in Loop: Header=BB8_210 Depth=1
	cmpl	$254, %r9d
	ja	.LBB8_285
# BB#213:                               #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %eax
	cmpl	$255, %eax
	jae	.LBB8_287
# BB#214:                               #   in Loop: Header=BB8_210 Depth=1
	movq	%rsi, %r14
	movl	$.L.str.104, %edx
	jmp	.LBB8_290
.LBB8_285:                              # %thread-pre-split.i.i399.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32383, %r9d            # imm = 0x7E7F
	ja	.LBB8_289
# BB#286:                               # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge.i402.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %eax
.LBB8_287:                              # %thread-pre-split.thread.i.i403.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32384, %eax            # imm = 0x7E80
	jae	.LBB8_289
# BB#288:                               #   in Loop: Header=BB8_210 Depth=1
	movq	%rsi, %r14
	movl	$.L.str.105, %edx
	jmp	.LBB8_290
.LBB8_289:                              # %scanner_size.exit.i404.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movq	%rsi, %r14
	movl	$.L.str.106, %edx
.LBB8_290:                              # %scanner_u_type.exit406.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.89, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, 96(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	fprintf
	movl	296(%r12), %eax
	cmpl	$254, %eax
	ja	.LBB8_293
# BB#291:                               #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %ecx
	cmpl	$255, %ecx
	jae	.LBB8_295
# BB#292:                               #   in Loop: Header=BB8_210 Depth=1
	movq	%r13, %rbx
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.LBB8_299
	jmp	.LBB8_315
.LBB8_293:                              # %thread-pre-split.i395.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32383, %eax            # imm = 0x7E7F
	ja	.LBB8_297
# BB#294:                               # %thread-pre-split.i395.thread-pre-split.thread.i396_crit_edge.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %ecx
.LBB8_295:                              # %thread-pre-split.thread.i396.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32384, %ecx            # imm = 0x7E80
	jae	.LBB8_297
# BB#296:                               #   in Loop: Header=BB8_210 Depth=1
	movq	%r13, %rbx
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB8_299
	jmp	.LBB8_315
.LBB8_297:                              #   in Loop: Header=BB8_210 Depth=1
	movq	%r13, %rbx
	movl	$3, %ecx
	testl	%eax, %eax
	je	.LBB8_315
.LBB8_299:                              # %.lr.ph437.i
                                        #   in Loop: Header=BB8_210 Depth=1
	leaq	(%rcx,%rcx,4), %rax
	leaq	128(%rsp,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_300:                              #   Parent Loop BB8_210 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_307 Depth 3
	movq	304(%r12), %rax
	movq	(%rax,%r13,8), %rax
	movl	2056(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB8_304
# BB#301:                               #   in Loop: Header=BB8_300 Depth=2
	cmpl	$1, %ecx
	jne	.LBB8_303
# BB#302:                               #   in Loop: Header=BB8_300 Depth=2
	movq	2064(%rax), %rax
	movq	(%rax), %rsi
	movl	$shift_fns, %edx
	leaq	424(%rsp), %rdi
	callq	set_add_fn
	movq	40(%rax), %rdx
	movl	$.L.str.90, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	cmpl	$0, 532(%r14)
	jg	.LBB8_306
	jmp	.LBB8_311
	.p2align	4, 0x90
.LBB8_304:                              #   in Loop: Header=BB8_300 Depth=2
	movl	$.L.str.92, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$0, 532(%r14)
	jg	.LBB8_306
	jmp	.LBB8_311
	.p2align	4, 0x90
.LBB8_303:                              #   in Loop: Header=BB8_300 Depth=2
	movl	$.L.str.91, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	fprintf
	cmpl	$0, 532(%r14)
	jle	.LBB8_311
.LBB8_306:                              # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB8_300 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_307:                              # %.lr.ph.i97
                                        #   Parent Loop BB8_210 Depth=1
                                        #     Parent Loop BB8_300 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12), %eax
	movl	%eax, 464(%rsp)
	movl	%r13d, 468(%rsp)
	movl	%ebp, 472(%rsp)
	movq	304(%r12), %rax
	movq	(%rax,%r13,8), %rcx
	movslq	536(%r14), %rdx
	movslq	%ebp, %rbx
	imulq	%rbx, %rdx
	leaq	8(%rcx,%rdx,8), %rcx
	movq	%rcx, 480(%rsp)
	movq	(%rax,%r13,8), %rax
	leaq	2136(%rax,%rdx,8), %rax
	movq	%rax, 488(%rsp)
	movl	$scanner_block_fns, %edx
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	464(%rsp), %rsi
	callq	set_add_fn
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	movl	8(%rax), %r8d
	movl	$.L.str.93, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	80(%rsp), %r9           # 8-byte Reload
	callq	fprintf
	movl	532(%r14), %eax
	decl	%eax
	cmpl	%eax, %ebx
	je	.LBB8_310
# BB#308:                               #   in Loop: Header=BB8_307 Depth=3
	movl	$.L.str.24, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	andl	$-2, %eax
	movl	%ebp, %ecx
	subl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB8_310
# BB#309:                               #   in Loop: Header=BB8_307 Depth=3
	movl	$.L.str.94, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB8_310:                              #   in Loop: Header=BB8_307 Depth=3
	incl	%ebp
	movq	72(%rsp), %r14          # 8-byte Reload
	cmpl	532(%r14), %ebp
	jl	.LBB8_307
.LBB8_311:                              # %._crit_edge.i98
                                        #   in Loop: Header=BB8_300 Depth=2
	movl	296(%r12), %eax
	decl	%eax
	cmpq	%rax, %r13
	jne	.LBB8_312
# BB#313:                               #   in Loop: Header=BB8_300 Depth=2
	movl	$.L.str.96, %edi
	movl	$3, %esi
	jmp	.LBB8_314
	.p2align	4, 0x90
.LBB8_312:                              #   in Loop: Header=BB8_300 Depth=2
	movl	$.L.str.95, %edi
	movl	$4, %esi
.LBB8_314:                              #   in Loop: Header=BB8_300 Depth=2
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rbx
	callq	fwrite
	incq	%r13
	cmpl	296(%r12), %r13d
	jb	.LBB8_300
.LBB8_315:                              # %._crit_edge438.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %r13
	movq	%r13, %rcx
	callq	fwrite
	movb	376(%r12), %al
	andb	$24, %al
	cmpb	$8, %al
	movq	%r14, %rsi
	movq	96(%rsp), %rcx          # 8-byte Reload
	je	.LBB8_345
# BB#316:                               #   in Loop: Header=BB8_210 Depth=1
	movl	296(%r12), %r9d
	cmpl	$254, %r9d
	ja	.LBB8_319
# BB#317:                               #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %eax
	cmpl	$255, %eax
	jae	.LBB8_321
# BB#318:                               #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.104, %edx
	jmp	.LBB8_324
.LBB8_319:                              # %thread-pre-split.i.i.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32383, %r9d            # imm = 0x7E7F
	ja	.LBB8_323
# BB#320:                               # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge.i.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %eax
.LBB8_321:                              # %thread-pre-split.thread.i.i.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32384, %eax            # imm = 0x7E80
	jae	.LBB8_323
# BB#322:                               #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.105, %edx
	jmp	.LBB8_324
.LBB8_323:                              # %scanner_size.exit.i.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.106, %edx
.LBB8_324:                              # %scanner_u_type.exit.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.97, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	fprintf
	movl	296(%r12), %eax
	cmpl	$254, %eax
	ja	.LBB8_327
# BB#325:                               #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %ecx
	cmpl	$255, %ecx
	jae	.LBB8_329
# BB#326:                               #   in Loop: Header=BB8_210 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.LBB8_333
	jmp	.LBB8_344
.LBB8_327:                              # %thread-pre-split.i391.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32383, %eax            # imm = 0x7E7F
	ja	.LBB8_331
# BB#328:                               # %thread-pre-split.i391.thread-pre-split.thread.i392_crit_edge.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	336(%r12), %ecx
.LBB8_329:                              # %thread-pre-split.thread.i392.i
                                        #   in Loop: Header=BB8_210 Depth=1
	cmpl	$32384, %ecx            # imm = 0x7E80
	jae	.LBB8_331
# BB#330:                               #   in Loop: Header=BB8_210 Depth=1
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB8_333
	jmp	.LBB8_344
.LBB8_331:                              #   in Loop: Header=BB8_210 Depth=1
	movl	$3, %ecx
	testl	%eax, %eax
	je	.LBB8_344
.LBB8_333:                              # %.lr.ph444.i
                                        #   in Loop: Header=BB8_210 Depth=1
	leaq	(%rcx,%rcx,4), %rax
	leaq	720(%rsp,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_334:                              #   Parent Loop BB8_210 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_336 Depth 3
	movl	$.L.str.98, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$0, 532(%r14)
	jle	.LBB8_340
# BB#335:                               # %.lr.ph441.preheader.i
                                        #   in Loop: Header=BB8_334 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_336:                              # %.lr.ph441.i
                                        #   Parent Loop BB8_210 Depth=1
                                        #     Parent Loop BB8_334 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12), %eax
	movl	%eax, 464(%rsp)
	movl	%r13d, 468(%rsp)
	movl	%ebx, 472(%rsp)
	movq	304(%r12), %rax
	movq	(%rax,%r13,8), %rcx
	movslq	536(%r14), %rdx
	movslq	%ebx, %rbp
	imulq	%rbp, %rdx
	leaq	8(%rcx,%rdx,8), %rcx
	movq	%rcx, 480(%rsp)
	movq	(%rax,%r13,8), %rax
	leaq	2136(%rax,%rdx,8), %rax
	movq	%rax, 488(%rsp)
	movl	$trans_scanner_block_fns, %edx
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	464(%rsp), %rsi
	callq	set_add_fn
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	movl	8(%rax), %r8d
	movl	$.L.str.99, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	80(%rsp), %r9           # 8-byte Reload
	callq	fprintf
	movl	532(%r14), %eax
	decl	%eax
	cmpl	%eax, %ebp
	je	.LBB8_339
# BB#337:                               #   in Loop: Header=BB8_336 Depth=3
	movl	$.L.str.24, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	andl	$-2, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB8_339
# BB#338:                               #   in Loop: Header=BB8_336 Depth=3
	movl	$.L.str.94, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB8_339:                              #   in Loop: Header=BB8_336 Depth=3
	incl	%ebx
	movq	72(%rsp), %r14          # 8-byte Reload
	cmpl	532(%r14), %ebx
	jl	.LBB8_336
.LBB8_340:                              # %._crit_edge442.i
                                        #   in Loop: Header=BB8_334 Depth=2
	movl	296(%r12), %eax
	decl	%eax
	cmpq	%rax, %r13
	jne	.LBB8_341
# BB#342:                               #   in Loop: Header=BB8_334 Depth=2
	movl	$.L.str.96, %edi
	movl	$3, %esi
	jmp	.LBB8_343
	.p2align	4, 0x90
.LBB8_341:                              #   in Loop: Header=BB8_334 Depth=2
	movl	$.L.str.95, %edi
	movl	$4, %esi
.LBB8_343:                              #   in Loop: Header=BB8_334 Depth=2
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rbx
	callq	fwrite
	movq	72(%rsp), %r14          # 8-byte Reload
	incq	%r13
	cmpl	296(%r12), %r13d
	jb	.LBB8_334
.LBB8_344:                              # %._crit_edge445.i
                                        #   in Loop: Header=BB8_210 Depth=1
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %r13
	movq	%r13, %rcx
	callq	fwrite
	movq	%r14, %rsi
	movq	96(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_345:                              #   in Loop: Header=BB8_210 Depth=1
	incq	%rcx
	cmpl	88(%rsi), %ecx
	jb	.LBB8_210
.LBB8_346:                              # %.preheader.preheader.i
	movq	%rsi, %rbx
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_349
# BB#347:                               # %.preheader.preheader.i
	leaq	144(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB8_349
# BB#348:
	callq	free
.LBB8_349:                              # %.preheader.1509.i
	movl	$0, 128(%rsp)
	movq	$0, 136(%rsp)
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_352
# BB#350:                               # %.preheader.1509.i
	leaq	184(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB8_352
# BB#351:
	callq	free
.LBB8_352:                              # %.preheader.2510.i
	movl	$0, 168(%rsp)
	movq	$0, 176(%rsp)
	movq	216(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_355
# BB#353:                               # %.preheader.2510.i
	leaq	224(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB8_355
# BB#354:
	callq	free
.LBB8_355:                              # %.preheader.3511.i
	movl	$0, 208(%rsp)
	movq	$0, 216(%rsp)
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	movq	80(%rsp), %r15          # 8-byte Reload
	je	.LBB8_358
# BB#356:                               # %.preheader.3511.i
	leaq	264(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB8_358
# BB#357:
	callq	free
.LBB8_358:                              # %write_scanner_data_as_C.exit
	movl	$0, 248(%rsp)
	movq	$0, 256(%rsp)
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	cmpl	$0, 88(%rbx)
	je	.LBB8_359
# BB#360:                               # %.lr.ph44.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_361:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_364 Depth 2
	movq	%r13, %rcx
	movq	96(%rbx), %rax
	movq	(%rax,%rbp,8), %r13
	movl	136(%r13), %eax
	testl	%eax, %eax
	je	.LBB8_362
# BB#363:                               # %.lr.ph.i101
                                        #   in Loop: Header=BB8_361 Depth=1
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	leaq	376(%r13), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_364:                              #   Parent Loop BB8_361 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%r13), %rcx
	movq	(%rcx,%rbp,8), %r14
	cmpl	$1, (%r14)
	jne	.LBB8_372
# BB#365:                               #   in Loop: Header=BB8_364 Depth=2
	movq	8(%r14), %rcx
	cmpl	$2, (%rcx)
	jne	.LBB8_372
# BB#366:                               #   in Loop: Header=BB8_364 Depth=2
	movzbl	(%r12), %eax
	testb	$2, %al
	jne	.LBB8_368
# BB#367:                               #   in Loop: Header=BB8_364 Depth=2
	orb	$2, %al
	movb	%al, (%r12)
	movl	$.L.str.107, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	fprintf
.LBB8_368:                              #   in Loop: Header=BB8_364 Depth=2
	movl	$.L.str.108, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	callq	fwrite
	movq	8(%r14), %rax
	movq	24(%rax), %rbx
	movq	%rbx, %rdi
	callq	strlen
	shlq	$32, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rax
	sarq	$32, %rax
	cmpb	$41, (%rbx,%rax)
	jne	.LBB8_370
# BB#369:                               #   in Loop: Header=BB8_364 Depth=2
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r15, %rcx
	callq	fwrite
	movl	$.L.str.24, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	jmp	.LBB8_371
.LBB8_370:                              #   in Loop: Header=BB8_364 Depth=2
	movl	$.L.str.109, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fprintf
.LBB8_371:                              #   in Loop: Header=BB8_364 Depth=2
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	8(%r14), %rax
	movl	8(%rbx), %edx
	addl	4(%rax), %edx
	movl	8(%rax), %ecx
	movl	$.L.str.110, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	136(%r13), %eax
.LBB8_372:                              #   in Loop: Header=BB8_364 Depth=2
	incq	%rbp
	cmpl	%eax, %ebp
	jb	.LBB8_364
# BB#373:                               #   in Loop: Header=BB8_361 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	testb	$2, (%r12)
	jne	.LBB8_375
	jmp	.LBB8_376
	.p2align	4, 0x90
.LBB8_362:                              # %.._crit_edge_crit_edge.i
                                        #   in Loop: Header=BB8_361 Depth=1
	addq	$376, %r13              # imm = 0x178
	movq	%r13, %r12
	movq	%rcx, %r13
	testb	$2, (%r12)
	je	.LBB8_376
.LBB8_375:                              #   in Loop: Header=BB8_361 Depth=1
	movl	$.L.str.111, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB8_376:                              #   in Loop: Header=BB8_361 Depth=1
	incq	%rbp
	movl	88(%rbx), %r14d
	cmpl	%r14d, %ebp
	movq	80(%rsp), %r15          # 8-byte Reload
	jb	.LBB8_361
	jmp	.LBB8_377
.LBB8_359:
	xorl	%r14d, %r14d
.LBB8_377:                              # %write_scanner_code_as_C.exit
	movl	8(%rbx), %eax
	movl	48(%rbx), %ecx
	leal	7(%rax,%rcx), %ebp
	shrl	$3, %ebp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$0, 880(%rsp)
	movq	$0, 888(%rsp)
	testl	%r14d, %r14d
	je	.LBB8_431
# BB#378:                               # %.lr.ph184.i
	leaq	896(%rsp), %r14
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	leal	-1(%rbp), %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$.L.str.23, %r15d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_379:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_381 Depth 2
                                        #     Child Loop BB8_387 Depth 2
                                        #     Child Loop BB8_404 Depth 2
                                        #       Child Loop BB8_389 Depth 3
                                        #         Child Loop BB8_390 Depth 4
                                        #       Child Loop BB8_402 Depth 3
                                        #     Child Loop BB8_408 Depth 2
                                        #     Child Loop BB8_414 Depth 2
                                        #     Child Loop BB8_419 Depth 2
	movq	96(%rbx), %rax
	movq	(%rax,%rcx,8), %r12
	movl	96(%r12), %eax
	testq	%rax, %rax
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	je	.LBB8_410
# BB#380:                               # %.lr.ph154.i
                                        #   in Loop: Header=BB8_379 Depth=1
	movq	104(%r12), %rcx
	xorl	%edx, %edx
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_381:                              #   Parent Loop BB8_379 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movq	(%rsi), %rsi
	cmpl	$1, (%rsi)
	jne	.LBB8_384
# BB#382:                               #   in Loop: Header=BB8_381 Depth=2
	movq	16(%rsi), %rsi
	cmpl	$3, (%rsi)
	jne	.LBB8_384
# BB#383:                               #   in Loop: Header=BB8_381 Depth=2
	orb	$4, 376(%r12)
.LBB8_384:                              #   in Loop: Header=BB8_381 Depth=2
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB8_381
# BB#385:                               # %._crit_edge155.i
                                        #   in Loop: Header=BB8_379 Depth=1
	movq	(%rcx), %rbx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
	callq	memset
	movq	(%rbx), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	elem_symbol
	movl	%eax, %ebp
	movl	%ebp, %ecx
	andb	$7, %cl
	movl	$1, %eax
	shll	%cl, %eax
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%ebp, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r13,%rcx), %edx
	orl	%eax, %edx
	movb	%dl, (%r13,%rcx)
	movl	96(%r12), %eax
	cmpl	$2, %eax
	jb	.LBB8_388
# BB#386:                               # %.lr.ph159.i.preheader
                                        #   in Loop: Header=BB8_379 Depth=1
	movl	$1, %ebx
	movq	72(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_387:                              # %.lr.ph159.i
                                        #   Parent Loop BB8_379 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	104(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	elem_symbol
	movl	%eax, %ecx
	andb	$7, %cl
	movl	$1, %edx
	shll	%cl, %edx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r13,%rcx), %esi
	orl	%edx, %esi
	movb	%sil, (%r13,%rcx)
	cmpl	%ebp, %eax
	cmovlel	%eax, %ebp
	incq	%rbx
	movl	96(%r12), %eax
	cmpl	%eax, %ebx
	jb	.LBB8_387
.LBB8_388:                              # %.outer.i.preheader
                                        #   in Loop: Header=BB8_379 Depth=1
	testl	%eax, %eax
	je	.LBB8_406
.LBB8_404:                              # %.lr.ph165.preheader.i
                                        #   Parent Loop BB8_379 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_389 Depth 3
                                        #         Child Loop BB8_390 Depth 4
                                        #       Child Loop BB8_402 Depth 3
	movq	104(%r12), %rcx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_389:                              # %.lr.ph165.i
                                        #   Parent Loop BB8_379 Depth=1
                                        #     Parent Loop BB8_404 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_390 Depth 4
	movq	(%rcx,%r15,8), %rax
	movq	(%rax), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	elem_symbol
	movl	%eax, %ebx
	subl	%ebp, %ebx
	movl	880(%rsp), %ecx
	cmpl	%ebx, %ecx
	movq	888(%rsp), %rax
	ja	.LBB8_399
	.p2align	4, 0x90
.LBB8_390:                              # %.lr.ph161.i
                                        #   Parent Loop BB8_379 Depth=1
                                        #     Parent Loop BB8_404 Depth=2
                                        #       Parent Loop BB8_389 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rax, %rax
	je	.LBB8_391
# BB#392:                               #   in Loop: Header=BB8_390 Depth=4
	cmpq	%r14, %rax
	je	.LBB8_393
# BB#395:                               #   in Loop: Header=BB8_390 Depth=4
	testb	$7, %cl
	je	.LBB8_397
# BB#396:                               #   in Loop: Header=BB8_390 Depth=4
	movl	%ecx, %edx
	leal	1(%rcx), %ecx
	movl	%ecx, 880(%rsp)
	movq	$0, (%rax,%rdx,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	cmpl	%ebx, %ecx
	jbe	.LBB8_390
	jmp	.LBB8_399
	.p2align	4, 0x90
.LBB8_391:                              #   in Loop: Header=BB8_390 Depth=4
	movq	%r14, 888(%rsp)
	jmp	.LBB8_394
	.p2align	4, 0x90
.LBB8_393:                              #   in Loop: Header=BB8_390 Depth=4
	cmpl	$2, %ecx
	ja	.LBB8_397
.LBB8_394:                              #   in Loop: Header=BB8_390 Depth=4
	movl	%ecx, %eax
	leal	1(%rcx), %ecx
	movl	%ecx, 880(%rsp)
	movq	$0, 896(%rsp,%rax,8)
	movq	%r14, %rax
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	cmpl	%ebx, %ecx
	jbe	.LBB8_390
	jmp	.LBB8_399
	.p2align	4, 0x90
.LBB8_397:                              #   in Loop: Header=BB8_390 Depth=4
	xorl	%esi, %esi
	leaq	880(%rsp), %rdi
	callq	vec_add_internal
	movl	880(%rsp), %ecx
	movq	888(%rsp), %rax
	cmpl	%ebx, %ecx
	jbe	.LBB8_390
	.p2align	4, 0x90
.LBB8_399:                              # %._crit_edge162.i
                                        #   in Loop: Header=BB8_389 Depth=3
	movslq	%ebx, %rdx
	cmpq	$0, (%rax,%rdx,8)
	jne	.LBB8_400
# BB#405:                               #   in Loop: Header=BB8_389 Depth=3
	movq	104(%r12), %rcx
	movq	(%rcx,%r15,8), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
	incl	%esi
	movq	%rsi, (%rax,%rdx,8)
	incq	%r15
	incl	%r13d
	cmpl	96(%r12), %r15d
	jb	.LBB8_389
	jmp	.LBB8_406
	.p2align	4, 0x90
.LBB8_400:                              # %.preheader.i108
                                        #   in Loop: Header=BB8_404 Depth=2
	testl	%r15d, %r15d
	movq	72(%rsp), %r15          # 8-byte Reload
	jle	.LBB8_403
# BB#401:                               # %.lr.ph168.preheader.i
                                        #   in Loop: Header=BB8_404 Depth=2
	movslq	%r13d, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB8_402:                              # %.lr.ph168.i
                                        #   Parent Loop BB8_379 Depth=1
                                        #     Parent Loop BB8_404 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	104(%r12), %rax
	movq	-16(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	elem_symbol
	subl	%ebp, %eax
	movq	888(%rsp), %rcx
	cltq
	movq	$0, (%rcx,%rax,8)
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB8_402
.LBB8_403:                              # %._crit_edge169.i
                                        #   in Loop: Header=BB8_404 Depth=2
	decl	%ebp
	cmpl	$0, 96(%r12)
	jne	.LBB8_404
	.p2align	4, 0x90
.LBB8_406:                              # %.loopexit.i110
                                        #   in Loop: Header=BB8_379 Depth=1
	movl	%ebp, 392(%r12)
	movl	$.L.str.112, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	movq	88(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	fprintf
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movl	$.L.str.23, %eax
	je	.LBB8_409
# BB#407:                               # %.lr.ph172.i.preheader
                                        #   in Loop: Header=BB8_379 Depth=1
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_408:                              # %.lr.ph172.i
                                        #   Parent Loop BB8_379 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %edx
	testq	%rbp, %rbp
	movl	$.L.str.24, %ecx
	cmoveq	%rax, %rcx
	movl	$.L.str.113, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$.L.str.23, %eax
	decq	%rbp
	incq	%rbx
	decq	%r15
	jne	.LBB8_408
.LBB8_409:                              # %._crit_edge173.i
                                        #   in Loop: Header=BB8_379 Depth=1
	movl	$.L.str.16, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	$.L.str.23, %r15d
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 176(%r12)
	jne	.LBB8_412
	jmp	.LBB8_416
	.p2align	4, 0x90
.LBB8_410:                              #   in Loop: Header=BB8_379 Depth=1
	movl	$-2147483647, 392(%r12) # imm = 0x80000001
	cmpl	$0, 176(%r12)
	je	.LBB8_416
.LBB8_412:                              #   in Loop: Header=BB8_379 Depth=1
	movl	$.L.str.114, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, %rdx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	fprintf
	movl	176(%r12), %eax
	testl	%eax, %eax
	je	.LBB8_415
# BB#413:                               # %.lr.ph176.i
                                        #   in Loop: Header=BB8_379 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_414:                              #   Parent Loop BB8_379 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%r12), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	16(%rcx), %rcx
	movq	160(%rcx), %rdx
	testq	%rdx, %rdx
	cmoveq	%rcx, %rdx
	movl	(%rdx), %edx
	decl	%eax
	cmpq	%rax, %rbx
	movl	$.L.str.24, %r8d
	cmoveq	%r15, %r8
	movl	$.L.str.115, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %rcx
	callq	fprintf
	incq	%rbx
	movl	176(%r12), %eax
	cmpl	%eax, %ebx
	jb	.LBB8_414
.LBB8_415:                              # %._crit_edge177.i
                                        #   in Loop: Header=BB8_379 Depth=1
	movl	$.L.str.16, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB8_416:                              #   in Loop: Header=BB8_379 Depth=1
	cmpl	$0, 216(%r12)
	je	.LBB8_421
# BB#417:                               #   in Loop: Header=BB8_379 Depth=1
	movl	$.L.str.116, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, %rdx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	fprintf
	movl	216(%r12), %eax
	testl	%eax, %eax
	je	.LBB8_420
# BB#418:                               # %.lr.ph180.i
                                        #   in Loop: Header=BB8_379 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_419:                              #   Parent Loop BB8_379 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	224(%r12), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movl	(%rsi), %edx
	movq	8(%rsi), %rcx
	movl	(%rcx), %ecx
	movq	16(%rsi), %rsi
	movq	160(%rsi), %rdi
	testq	%rdi, %rdi
	cmoveq	%rsi, %rdi
	movl	(%rdi), %r8d
	decl	%eax
	cmpq	%rax, %rbx
	movl	$.L.str.24, %eax
	cmoveq	%r15, %rax
	movq	%rax, (%rsp)
	movl	$.L.str.117, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbp, %r9
	callq	fprintf
	incq	%rbx
	movl	216(%r12), %eax
	cmpl	%eax, %ebx
	jb	.LBB8_419
.LBB8_420:                              # %._crit_edge181.i
                                        #   in Loop: Header=BB8_379 Depth=1
	movl	$.L.str.16, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB8_421:                              #   in Loop: Header=BB8_379 Depth=1
	incq	%rcx
	cmpl	88(%rbx), %ecx
	jb	.LBB8_379
# BB#422:                               # %._crit_edge185.i
	movl	880(%rsp), %ecx
	testl	%ecx, %ecx
	movq	80(%rsp), %r15          # 8-byte Reload
	je	.LBB8_431
# BB#423:
	movl	$.L.str.118, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	fprintf
	cmpl	$0, 880(%rsp)
	je	.LBB8_430
# BB#424:                               # %.lr.ph.i114.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_425:                              # %.lr.ph.i114
                                        # =>This Inner Loop Header: Depth=1
	movq	888(%rsp), %rax
	movq	(%rax,%rbx,8), %rdx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	880(%rsp), %eax
	decl	%eax
	cmpq	%rax, %rbx
	je	.LBB8_427
# BB#426:                               #   in Loop: Header=BB8_425 Depth=1
	movl	$44, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB8_427:                              #   in Loop: Header=BB8_425 Depth=1
	movl	%ebx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ebx, %eax
	andl	$-16, %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	$15, %ecx
	jne	.LBB8_429
# BB#428:                               #   in Loop: Header=BB8_425 Depth=1
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB8_429:                              #   in Loop: Header=BB8_425 Depth=1
	incq	%rbx
	cmpl	880(%rsp), %ebx
	jb	.LBB8_425
.LBB8_430:                              # %._crit_edge.i117
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %r14
	movq	%r13, %rcx
	callq	fwrite
	movq	72(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 88(%rbx)
	jne	.LBB8_433
	jmp	.LBB8_441
.LBB8_431:                              # %._crit_edge185.thread.i
	movl	$.L.str.119, %esi
	xorl	%eax, %eax
	movq	%r13, %r14
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	fprintf
	cmpl	$0, 88(%rbx)
	je	.LBB8_441
.LBB8_433:                              # %.lr.ph46.i
	leaq	384(%rsp), %rbp
	movl	$.L.str.23, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_434:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_438 Depth 2
	movq	96(%rbx), %rax
	movq	(%rax,%r12,8), %rbx
	cmpl	$0, 256(%rbx)
	je	.LBB8_440
# BB#435:                               #   in Loop: Header=BB8_434 Depth=1
	movl	$er_hint_hash_fns, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	set_add_fn
	cmpq	%rbx, %rax
	jne	.LBB8_440
# BB#436:                               #   in Loop: Header=BB8_434 Depth=1
	cmpl	$1, 256(%rbx)
	movl	$.L.str.23, %r8d
	movl	$.L.str.4, %eax
	cmovaq	%rax, %r8
	movl	$.L.str.120, %esi
	xorl	%eax, %eax
	movq	%r14, %r15
	movq	%r15, %rdi
	movl	%r12d, %edx
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	fprintf
	cmpl	$0, 256(%rbx)
	je	.LBB8_439
# BB#437:                               # %.lr.ph.i118
                                        #   in Loop: Header=BB8_434 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_438:                              #   Parent Loop BB8_434 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	264(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	16(%rax), %rax
	movq	40(%rax), %rcx
	movl	32(%rax), %eax
	decl	%eax
	movq	(%rcx,%rax,8), %rax
	movq	16(%rax), %rax
	movq	24(%rax), %rdi
	callq	escape_string
	movq	%rax, %rbp
	movq	264(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movl	(%rax), %edx
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	56(%rax), %ecx
	movl	256(%rbx), %eax
	decl	%eax
	cmpq	%rax, %r14
	movl	$.L.str.82, %r9d
	cmoveq	%r13, %r9
	movl	$.L.str.121, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %r8
	callq	fprintf
	movq	%rbp, %rdi
	callq	free
	incq	%r14
	cmpl	256(%rbx), %r14d
	jb	.LBB8_438
.LBB8_439:                              # %._crit_edge.i121
                                        #   in Loop: Header=BB8_434 Depth=1
	movl	$.L.str.16, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%r15, %r14
	leaq	384(%rsp), %rbp
.LBB8_440:                              #   in Loop: Header=BB8_434 Depth=1
	incq	%r12
	movq	72(%rsp), %rbx          # 8-byte Reload
	cmpl	88(%rbx), %r12d
	jb	.LBB8_434
.LBB8_441:                              # %write_error_data_as_C.exit
	movl	$.L.str.122, %esi
	xorl	%eax, %eax
	movq	%r14, %rbp
	movq	%rbp, %rdi
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	callq	fprintf
	cmpl	$0, 88(%rbx)
	je	.LBB8_506
# BB#442:
	movl	$.L.str.53, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	cmpl	$0, 88(%rbx)
	je	.LBB8_505
# BB#443:                               # %.lr.ph.i123
	movl	$.L.str.134, %r14d
	movl	$.L.str.23, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_444:                              # =>This Inner Loop Header: Depth=1
	movq	96(%rbx), %rax
	movq	%rbp, %rbx
	movq	(%rax,%r13,8), %rbp
	movl	$.L.str.123, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$0, 96(%rbp)
	je	.LBB8_446
# BB#445:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.124, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r13d, %edx
	movq	%r12, %rcx
	callq	fprintf
	jmp	.LBB8_447
	.p2align	4, 0x90
.LBB8_446:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB8_447:                              #   in Loop: Header=BB8_444 Depth=1
	movl	392(%rbp), %edx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	176(%rbp), %edx
	testl	%edx, %edx
	je	.LBB8_449
# BB#448:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.125, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r13d, %ecx
	movq	%r12, %r8
	callq	fprintf
	jmp	.LBB8_450
	.p2align	4, 0x90
.LBB8_449:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.126, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB8_450:                              #   in Loop: Header=BB8_444 Depth=1
	movl	216(%rbp), %edx
	testl	%edx, %edx
	je	.LBB8_452
# BB#451:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.127, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %ecx
	movq	%r12, %r8
	callq	fprintf
	cmpl	$0, 256(%rbp)
	jne	.LBB8_454
	jmp	.LBB8_455
	.p2align	4, 0x90
.LBB8_452:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.126, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpl	$0, 256(%rbp)
	je	.LBB8_455
.LBB8_454:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$er_hint_hash_fns, %edx
	leaq	384(%rsp), %rdi
	movq	%rbp, %rsi
	callq	set_add_fn
	movl	256(%rbp), %edx
	movl	(%rax), %ecx
	movl	$.L.str.128, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %r8
	callq	fprintf
	cmpl	$0, 136(%rbp)
	jne	.LBB8_457
	jmp	.LBB8_461
	.p2align	4, 0x90
.LBB8_455:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.126, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpl	$0, 136(%rbp)
	je	.LBB8_461
.LBB8_457:                              #   in Loop: Header=BB8_444 Depth=1
	movq	400(%rbp), %rax
	testq	%rax, %rax
	je	.LBB8_458
# BB#459:                               #   in Loop: Header=BB8_444 Depth=1
	movl	(%rax), %edx
	jmp	.LBB8_460
	.p2align	4, 0x90
.LBB8_461:                              #   in Loop: Header=BB8_444 Depth=1
	leaq	376(%rbp), %rbx
	movzbl	376(%rbp), %eax
	testb	$2, %al
	jne	.LBB8_464
# BB#462:                               #   in Loop: Header=BB8_444 Depth=1
	testb	$4, %al
	je	.LBB8_465
# BB#463:                               #   in Loop: Header=BB8_444 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.LBB8_465
.LBB8_464:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.130, %edi
	movl	$15, %esi
	jmp	.LBB8_466
	.p2align	4, 0x90
.LBB8_458:                              # %._crit_edge266
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	%r13d, %edx
.LBB8_460:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.129, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
	leaq	376(%rbp), %rbx
	jmp	.LBB8_467
.LBB8_465:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.12, %edi
	movl	$6, %esi
.LBB8_466:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
.LBB8_467:                              #   in Loop: Header=BB8_444 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	movzbl	(%rbx), %eax
	je	.LBB8_470
# BB#468:                               #   in Loop: Header=BB8_444 Depth=1
	testb	$4, %al
	je	.LBB8_472
# BB#469:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	fprintf
	cmpl	$0, 296(%rbp)
	jne	.LBB8_474
	jmp	.LBB8_478
	.p2align	4, 0x90
.LBB8_470:                              #   in Loop: Header=BB8_444 Depth=1
	testb	$2, %al
	jne	.LBB8_471
.LBB8_472:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpl	$0, 296(%rbp)
	jne	.LBB8_474
	jmp	.LBB8_478
.LBB8_471:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.131, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %edx
	movq	%r12, %rcx
	callq	fprintf
	cmpl	$0, 296(%rbp)
	je	.LBB8_478
	.p2align	4, 0x90
.LBB8_474:                              #   in Loop: Header=BB8_444 Depth=1
	movq	400(%rbp), %rax
	testq	%rax, %rax
	je	.LBB8_475
# BB#476:                               #   in Loop: Header=BB8_444 Depth=1
	movl	(%rax), %edx
	jmp	.LBB8_477
	.p2align	4, 0x90
.LBB8_478:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	jmp	.LBB8_479
	.p2align	4, 0x90
.LBB8_475:                              # %._crit_edge264
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	%r13d, %edx
.LBB8_477:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.132, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
.LBB8_479:                              #   in Loop: Header=BB8_444 Depth=1
	movl	296(%rbp), %eax
	cmpl	$254, %eax
	ja	.LBB8_482
# BB#480:                               #   in Loop: Header=BB8_444 Depth=1
	movl	336(%rbp), %eax
	cmpl	$255, %eax
	jae	.LBB8_484
# BB#481:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.100, %edx
	jmp	.LBB8_487
	.p2align	4, 0x90
.LBB8_482:                              # %thread-pre-split.i.i.i128
                                        #   in Loop: Header=BB8_444 Depth=1
	cmpl	$32383, %eax            # imm = 0x7E7F
	ja	.LBB8_486
# BB#483:                               # %thread-pre-split.i.thread-pre-split.thread.i_crit_edge.i.i131
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	336(%rbp), %eax
.LBB8_484:                              # %thread-pre-split.thread.i.i.i132
                                        #   in Loop: Header=BB8_444 Depth=1
	cmpl	$32384, %eax            # imm = 0x7E80
	jae	.LBB8_486
# BB#485:                               #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.101, %edx
	jmp	.LBB8_487
	.p2align	4, 0x90
.LBB8_486:                              # %scanner_size.exit.i.i133
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.102, %edx
.LBB8_487:                              # %scanner_type.exit.i135
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.133, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	callq	fprintf
	testb	$1, (%rbx)
	movl	$.L.str.135, %esi
	cmovneq	%r14, %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movzbl	(%rbx), %eax
	shrb	$3, %al
	andb	$3, %al
	movzbl	%al, %eax
	movq	scan_kind_strings(,%rax,8), %rdx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movzbl	(%rbx), %eax
	andb	$24, %al
	cmpb	$8, %al
	je	.LBB8_493
# BB#488:                               #   in Loop: Header=BB8_444 Depth=1
	cmpl	$0, 296(%rbp)
	je	.LBB8_493
# BB#489:                               #   in Loop: Header=BB8_444 Depth=1
	movq	400(%rbp), %rax
	testq	%rax, %rax
	movq	80(%rsp), %r12          # 8-byte Reload
	je	.LBB8_490
# BB#491:                               #   in Loop: Header=BB8_444 Depth=1
	movl	(%rax), %edx
	jmp	.LBB8_492
	.p2align	4, 0x90
.LBB8_493:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	movq	80(%rsp), %r12          # 8-byte Reload
	jmp	.LBB8_494
.LBB8_490:                              # %._crit_edge262
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	%r13d, %edx
.LBB8_492:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.136, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
.LBB8_494:                              #   in Loop: Header=BB8_444 Depth=1
	movzbl	(%rbx), %eax
	andb	$24, %al
	cmpb	$8, %al
	je	.LBB8_500
# BB#495:                               #   in Loop: Header=BB8_444 Depth=1
	cmpl	$0, 296(%rbp)
	je	.LBB8_500
# BB#496:                               #   in Loop: Header=BB8_444 Depth=1
	movq	400(%rbp), %rax
	testq	%rax, %rax
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB8_497
# BB#498:                               #   in Loop: Header=BB8_444 Depth=1
	movl	(%rax), %edx
	jmp	.LBB8_499
	.p2align	4, 0x90
.LBB8_500:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.138, %edi
	movl	$18, %esi
	movl	$1, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	movq	72(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB8_501
.LBB8_497:                              # %._crit_edge
                                        #   in Loop: Header=BB8_444 Depth=1
	movl	%r13d, %edx
.LBB8_499:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.137, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
.LBB8_501:                              #   in Loop: Header=BB8_444 Depth=1
	movq	408(%rbp), %rax
	testq	%rax, %rax
	je	.LBB8_503
# BB#502:                               #   in Loop: Header=BB8_444 Depth=1
	movl	(%rax), %edx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	fprintf
	jmp	.LBB8_504
	.p2align	4, 0x90
.LBB8_503:                              #   in Loop: Header=BB8_444 Depth=1
	movl	$.L.str.139, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	fwrite
.LBB8_504:                              #   in Loop: Header=BB8_444 Depth=1
	movl	88(%rbx), %eax
	decl	%eax
	cmpq	%rax, %r13
	movl	$.L.str.84, %edx
	cmoveq	%r15, %rdx
	movl	$.L.str.140, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	incq	%r13
	cmpl	88(%rbx), %r13d
	jb	.LBB8_444
.LBB8_505:                              # %._crit_edge.i137
	movl	$.L.str.49, %edi
	movl	$4, %esi
	jmp	.LBB8_507
.LBB8_506:
	movl	$.L.str.141, %edi
	movl	$28, %esi
.LBB8_507:                              # %write_state_data_as_C.exit
	movl	$1, %edx
	movq	%rbp, %rcx
	callq	fwrite
	movl	$.L.str.145, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	fprintf
	cmpl	$0, 8(%rbx)
	movq	%rbx, %r13
	je	.LBB8_510
# BB#508:                               # %.lr.ph33.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_509:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	xorl	%ecx, %ecx
	testb	$28, 60(%rax)
	setne	%cl
	movq	d_internal(,%rcx,8), %rdx
	movq	(%rax), %rcx
	movl	8(%rax), %r8d
	movl	$.L.str.146, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	incq	%rbx
	cmpl	8(%r13), %ebx
	jb	.LBB8_509
.LBB8_510:                              # %.preheader.i138
	cmpl	$0, 48(%r13)
	movq	%rbp, %r12
	je	.LBB8_513
# BB#511:                               # %.lr.ph.i139
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_512:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	escape_string
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	escape_string
	movq	%rax, %rbp
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movl	(%rax), %eax
	movq	d_symbol(,%rax,8), %r15
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r8
	movl	$.L.str.146, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	fprintf
	movq	%r14, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	incq	%rbx
	cmpl	48(%r13), %ebx
	jb	.LBB8_512
.LBB8_513:                              # %write_symbol_data_as_C.exit
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	cmpl	$0, 200(%r13)
	movq	%r12, %r15
	movq	%r13, %rbx
	movq	80(%rsp), %r12          # 8-byte Reload
	je	.LBB8_518
# BB#514:
	movl	$.L.str.153, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movl	200(%rbx), %eax
	testl	%eax, %eax
	je	.LBB8_517
# BB#515:                               # %.lr.ph.i143
	movl	$.L.str.24, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_516:                              # =>This Inner Loop Header: Depth=1
	movq	208(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	(%rsi), %rdx
	movl	8(%rsi), %ecx
	movl	12(%rsi), %r8d
	movl	16(%rsi), %r9d
	decl	%eax
	cmpq	%rax, %rbp
	movl	$.L.str.23, %eax
	cmovbq	%r14, %rax
	movq	%rax, (%rsp)
	movl	$.L.str.154, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	incq	%rbp
	movl	200(%rbx), %eax
	cmpl	%eax, %ebp
	jb	.LBB8_516
.LBB8_517:                              # %._crit_edge.i146
	movl	$.L.str.49, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB8_518:                              # %write_passes_as_C.exit
	movl	$.L.str.5, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	lookup_production
	testq	%rax, %rax
	je	.LBB8_519
# BB#520:
	movq	200(%rax), %rax
	movl	(%rax), %r14d
	jmp	.LBB8_521
.LBB8_519:
	xorl	%r14d, %r14d
.LBB8_521:
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	88(%rbx), %edx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r14d, %edx
	callq	fprintf
	movl	48(%rbx), %edx
	addl	8(%rbx), %edx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movq	240(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB8_523
# BB#522:
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	jmp	.LBB8_524
.LBB8_523:
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB8_524:
	movl	200(%rbx), %edx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	cmpl	$0, 200(%rbx)
	je	.LBB8_526
# BB#525:
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fprintf
	cmpl	$0, 272(%rbx)
	jne	.LBB8_528
	jmp	.LBB8_529
.LBB8_526:
	movl	$.L.str.12, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpl	$0, 272(%rbx)
	je	.LBB8_529
.LBB8_528:
	movl	$49, %edi
	jmp	.LBB8_530
.LBB8_529:
	movl	$48, %edi
.LBB8_530:
	movq	%r15, %rsi
	callq	fputc
	movl	$.L.str.16, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	addq	$9080, %rsp             # imm = 0x2378
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	write_parser_tables_as_C, .Lfunc_end8-write_parser_tables_as_C
	.cfi_endproc

	.globl	write_ctables
	.p2align	4, 0x90
	.type	write_ctables,@function
write_ctables:                          # @write_ctables
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 16
	movl	$256, %eax              # imm = 0x100
	xorl	%edx, %edx
	idivl	532(%rdi)
	movl	%eax, 536(%rdi)
	movq	(%rdi), %rsi
	leaq	276(%rdi), %rdx
	xorl	%eax, %eax
	cmpb	$0, 276(%rdi)
	cmoveq	%rax, %rdx
	callq	write_parser_tables_as_C
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	write_ctables, .Lfunc_end9-write_ctables
	.cfi_endproc

	.p2align	4, 0x90
	.type	find_symbol,@function
find_symbol:                            # @find_symbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 64
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB10_4
# BB#1:                                 # %.lr.ph52
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movzbl	1(%r14), %ebx
	incq	%r14
	testb	%bl, %bl
	jne	.LBB10_2
.LBB10_4:                               # %.critedge
	movl	$-1, %r13d
	movq	%rbp, %rbx
	cmpq	%rbx, %r14
	jae	.LBB10_22
# BB#5:
	cmpl	$1, %r12d
	je	.LBB10_14
# BB#6:
	cmpl	$3, %r12d
	jne	.LBB10_22
# BB#7:                                 # %.preheader
	movl	48(%r15), %ebp
	testl	%ebp, %ebp
	je	.LBB10_22
# BB#8:                                 # %.lr.ph
	subq	%r14, %rbx
	movl	$-1, %r13d
	xorl	%r12d, %r12d
	jmp	.LBB10_9
.LBB10_14:
	subl	%r14d, %ebx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	lookup_production
	testq	%rax, %rax
	je	.LBB10_22
# BB#15:
	movl	56(%rax), %r13d
	jmp	.LBB10_22
.LBB10_13:                              # %._crit_edge56
                                        #   in Loop: Header=BB10_9 Depth=1
	movl	%r12d, %r13d
	jmp	.LBB10_18
	.p2align	4, 0x90
.LBB10_9:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%r12,8), %rax
	cmpl	$0, (%rax)
	jne	.LBB10_18
# BB#10:                                #   in Loop: Header=BB10_9 Depth=1
	movslq	32(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.LBB10_18
# BB#11:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movq	24(%rax), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB10_17
# BB#12:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB10_13
# BB#16:                                #   in Loop: Header=BB10_9 Depth=1
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	d_fail
	movl	48(%r15), %ebp
.LBB10_17:                              #   in Loop: Header=BB10_9 Depth=1
	movl	4(%rsp), %r13d          # 4-byte Reload
	.p2align	4, 0x90
.LBB10_18:                              #   in Loop: Header=BB10_9 Depth=1
	incq	%r12
	cmpl	%ebp, %r12d
	jb	.LBB10_9
# BB#19:                                # %._crit_edge
	testl	%r13d, %r13d
	jle	.LBB10_20
# BB#21:
	addl	8(%r15), %r13d
	jmp	.LBB10_22
.LBB10_20:
	movl	$-1, %r13d
.LBB10_22:                              # %.thread
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	find_symbol, .Lfunc_end10-find_symbol
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_code_as_C,@function
write_code_as_C:                        # @write_code_as_C
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 96
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movl	%r8d, %r14d
	movq	%rcx, %rbp
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	$.L.str.53, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 540(%rbx)
	je	.LBB11_2
# BB#1:
	movl	$47, %esi
	movq	%r15, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%r15, %rcx
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r14d, %edx
	callq	fprintf
.LBB11_2:                               # %.preheader116
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	jmp	.LBB11_3
.LBB11_51:                              # %.critedge.i
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$-1, %r15d
	cmpq	%rbp, %rbx
	jae	.LBB11_52
# BB#53:                                #   in Loop: Header=BB11_3 Depth=1
	movl	%ebp, %edx
	subl	%ebx, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	lookup_production
	testq	%rax, %rax
	movl	4(%rsp), %r14d          # 4-byte Reload
	je	.LBB11_55
# BB#54:                                #   in Loop: Header=BB11_3 Depth=1
	movl	56(%rax), %r15d
	jmp	.LBB11_55
.LBB11_36:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.61, %esi
	movl	$6, %edx
	movq	%rdi, %r14
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_37
# BB#44:                                #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.67, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_45
# BB#57:                                # %.thread112
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.31, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB11_63
# BB#58:                                #   in Loop: Header=BB11_3 Depth=1
	movl	$3, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	find_symbol
	movl	%eax, %ecx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ecx, %edx
	callq	fprintf
	jmp	.LBB11_39
.LBB11_40:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.63, %esi
	movl	$10, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_41
.LBB11_63:                              # %.thread
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %esi
	callq	d_fail
	incq	%rbp
	jmp	.LBB11_3
.LBB11_43:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.66, %edi
	movl	$27, %esi
	jmp	.LBB11_38
.LBB11_37:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.62, %edi
	movl	$11, %esi
	jmp	.LBB11_38
.LBB11_41:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.64, %edi
	movl	$53, %esi
	jmp	.LBB11_38
.LBB11_45:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.68, %edi
	movl	$7, %esi
.LBB11_38:                              # %.backedge
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB11_39:                              # %.backedge
                                        #   in Loop: Header=BB11_3 Depth=1
	incq	%rbp
	movl	4(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB11_3
.LBB11_52:                              #   in Loop: Header=BB11_3 Depth=1
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB11_55:                              # %find_symbol.exit
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
.LBB11_56:                              # %.backedge
                                        #   in Loop: Header=BB11_3 Depth=1
	callq	fprintf
	incq	%rbp
	jmp	.LBB11_3
	.p2align	4, 0x90
.LBB11_5:                               #   in Loop: Header=BB11_3 Depth=1
	movq	%r13, %rsi
	callq	fputc
	incq	%rbp
.LBB11_3:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_16 Depth 2
                                        #     Child Loop BB11_23 Depth 2
                                        #     Child Loop BB11_26 Depth 2
                                        #     Child Loop BB11_31 Depth 2
                                        #     Child Loop BB11_49 Depth 2
	movsbl	(%rbp), %edi
	cmpl	$36, %edi
	je	.LBB11_6
# BB#4:                                 # %.backedge
                                        #   in Loop: Header=BB11_3 Depth=1
	testb	%dil, %dil
	jne	.LBB11_5
	jmp	.LBB11_65
	.p2align	4, 0x90
.LBB11_6:                               #   in Loop: Header=BB11_3 Depth=1
	movsbq	1(%rbp), %rbx
	cmpq	$102, %rbx
	jg	.LBB11_10
# BB#7:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpb	$35, %bl
	je	.LBB11_17
# BB#8:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpb	$36, %bl
	jne	.LBB11_21
# BB#9:                                 #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.59, %edi
	movl	$26, %esi
	jmp	.LBB11_18
	.p2align	4, 0x90
.LBB11_10:                              #   in Loop: Header=BB11_3 Depth=1
	cmpb	$103, %bl
	je	.LBB11_19
# BB#11:                                #   in Loop: Header=BB11_3 Depth=1
	cmpb	$110, %bl
	jne	.LBB11_21
# BB#12:                                #   in Loop: Header=BB11_3 Depth=1
	leaq	2(%rbp), %rbx
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movsbq	2(%rbp), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB11_13
# BB#20:                                #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.58, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	%rbx, %rbp
	jmp	.LBB11_3
.LBB11_17:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.54, %edi
	movl	$13, %esi
	jmp	.LBB11_18
.LBB11_19:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.55, %edi
	movl	$29, %esi
.LBB11_18:                              # %.backedge
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	addq	$2, %rbp
	jmp	.LBB11_3
.LBB11_21:                              #   in Loop: Header=BB11_3 Depth=1
	leaq	1(%rbp), %r15
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	(%r12), %r14
	testb	$8, 1(%r14,%rbx,2)
	jne	.LBB11_22
# BB#24:                                #   in Loop: Header=BB11_3 Depth=1
	cmpb	$123, %bl
	jne	.LBB11_64
# BB#25:                                #   in Loop: Header=BB11_3 Depth=1
	leaq	2(%rbp), %rdi
	addq	$3, %rbp
	movq	%rbp, %r12
	movq	%rdi, %rbx
	jmp	.LBB11_26
	.p2align	4, 0x90
.LBB11_29:                              #   in Loop: Header=BB11_26 Depth=2
	incq	%rbx
	incq	%r12
.LBB11_26:                              #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB11_30
# BB#27:                                #   in Loop: Header=BB11_26 Depth=2
	cmpb	$125, %r15b
	je	.LBB11_30
# BB#28:                                #   in Loop: Header=BB11_26 Depth=2
	testb	$32, 1(%r14,%r15,2)
	je	.LBB11_29
.LBB11_30:                              # %.critedge107
                                        #   in Loop: Header=BB11_3 Depth=1
	movzwl	(%r14,%r15,2), %ecx
	leaq	1(%rbx), %rbp
	testb	$32, %ch
	cmoveq	%rbx, %rbp
	jmp	.LBB11_31
	.p2align	4, 0x90
.LBB11_33:                              #   in Loop: Header=BB11_31 Depth=2
	incq	%rbp
.LBB11_31:                              #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %eax
	testb	%al, %al
	je	.LBB11_34
# BB#32:                                #   in Loop: Header=BB11_31 Depth=2
	cmpb	$125, %al
	jne	.LBB11_33
.LBB11_34:                              # %.critedge
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	%rbx, %rax
	subq	%rdi, %rax
	addq	$-4, %rax
	cmpq	$6, %rax
	ja	.LBB11_63
# BB#35:                                # %.critedge
                                        #   in Loop: Header=BB11_3 Depth=1
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_59:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.69, %esi
	movl	$4, %edx
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB11_63
# BB#60:                                #   in Loop: Header=BB11_3 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	find_pass
	movq	%rax, %r15
	testq	%r15, %r15
	movl	4(%rsp), %r14d          # 4-byte Reload
	jne	.LBB11_62
# BB#61:                                #   in Loop: Header=BB11_3 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	dup_str
	movq	%rax, %rcx
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movl	%r14d, %edx
	callq	d_fail
.LBB11_62:                              #   in Loop: Header=BB11_3 Depth=1
	movl	16(%r15), %edx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	jmp	.LBB11_56
.LBB11_13:                              #   in Loop: Header=BB11_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %rbx
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	32(%rax), %eax
	decl	%eax
	cmpl	%eax, %ebx
	jbe	.LBB11_15
# BB#14:                                #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	d_fail
.LBB11_15:                              # %.preheader
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	(%r15), %rax
	incq	%rbp
	.p2align	4, 0x90
.LBB11_16:                              #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rbp), %rcx
	incq	%rbp
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB11_16
	jmp	.LBB11_3
.LBB11_22:                              #   in Loop: Header=BB11_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movq	%rax, %rcx
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ecx, %edx
	callq	fprintf
	movq	(%r12), %rax
	movl	4(%rsp), %r14d          # 4-byte Reload
	.p2align	4, 0x90
.LBB11_23:                              #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rbp), %rcx
	incq	%rbp
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB11_23
	jmp	.LBB11_3
.LBB11_64:                              #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %esi
	callq	d_fail
	movq	%r15, %rbp
	jmp	.LBB11_3
.LBB11_42:                              #   in Loop: Header=BB11_3 Depth=1
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	$.L.str.65, %esi
	movl	$5, %edx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_43
# BB#46:                                # %.thread110.thread
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	$.L.str.29, %esi
	movl	$5, %edx
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	strncasecmp
	testl	%eax, %eax
	movl	20(%rsp), %eax          # 4-byte Reload
	jne	.LBB11_63
# BB#47:                                #   in Loop: Header=BB11_3 Depth=1
	testb	%r15b, %r15b
	je	.LBB11_51
# BB#48:                                #   in Loop: Header=BB11_3 Depth=1
	andl	$8192, %eax             # imm = 0x2000
	testw	%ax, %ax
	je	.LBB11_51
	.p2align	4, 0x90
.LBB11_49:                              # %.lr.ph
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rbx
	movsbq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB11_51
# BB#50:                                # %._crit_edge
                                        #   in Loop: Header=BB11_49 Depth=2
	leaq	1(%rbx), %r12
	testb	$32, 1(%r14,%rax,2)
	jne	.LBB11_49
	jmp	.LBB11_51
.LBB11_65:
	movl	$.L.str.71, %edi
	movl	$11, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movl	$.L.str.72, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end11:
	.size	write_code_as_C, .Lfunc_end11-write_code_as_C
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_59
	.quad	.LBB11_42
	.quad	.LBB11_36
	.quad	.LBB11_63
	.quad	.LBB11_63
	.quad	.LBB11_63
	.quad	.LBB11_40

	.type	scanner_block_fns,@object # @scanner_block_fns
	.data
	.globl	scanner_block_fns
	.p2align	3
scanner_block_fns:
	.quad	scanner_block_hash_fn
	.quad	scanner_block_cmp_fn
	.zero	16
	.size	scanner_block_fns, 32

	.type	trans_scanner_block_fns,@object # @trans_scanner_block_fns
	.globl	trans_scanner_block_fns
	.p2align	3
trans_scanner_block_fns:
	.quad	trans_scanner_block_hash_fn
	.quad	trans_scanner_block_cmp_fn
	.zero	16
	.size	trans_scanner_block_fns, 32

	.type	shift_fns,@object       # @shift_fns
	.globl	shift_fns
	.p2align	3
shift_fns:
	.quad	shift_hash_fn
	.quad	shift_cmp_fn
	.zero	16
	.size	shift_fns, 32

	.type	er_hint_hash_fns,@object # @er_hint_hash_fns
	.globl	er_hint_hash_fns
	.p2align	3
er_hint_hash_fns:
	.quad	er_hint_hash_fn
	.quad	er_hint_cmp_fn
	.zero	16
	.size	er_hint_hash_fns, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	".d_parser.c"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unable to open `%s` for write\n"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"#include \"dparse.h\"\n"
	.size	.L.str.2, 21

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"#include \"%s.d_parser.h\"\n"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"whitespace"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"D_ParserTables parser_tables_%s = {\n"
	.size	.L.str.6, 37

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d, "
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"d_states_%s, "
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"d_gotos_%s, "
	.size	.L.str.9, 13

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"d_symbols_%s, "
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s, "
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"NULL, "
	.size	.L.str.12, 7

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"d_passes_%s, "
	.size	.L.str.13, 14

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"};\n"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	".d_parser.h"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"#ifndef _%s_h\n"
	.size	.L.str.18, 15

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"#define _%s_h\n"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"#define %s \t%d\n"
	.size	.L.str.20, 16

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"enum D_Tokens_%s {\n"
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s = %d%s"
	.size	.L.str.22, 10

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.zero	1
	.size	.L.str.23, 1

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	", "
	.size	.L.str.24, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\n};\n"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"#define D_START_STATE_%s \t%d\n"
	.size	.L.str.26, 30

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"#endif\n"
	.size	.L.str.27, 8

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"#line %d \"%s\"\n"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"nterm"
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%d"
	.size	.L.str.30, 3

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"string"
	.size	.L.str.31, 7

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"bad $ escape in code line %u\n"
	.size	.L.str.32, 30

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"attempt to find symbol for non-unique string '%s'\n"
	.size	.L.str.33, 51

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"_"
	.size	.L.str.34, 2

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"int d_speculative_reduction_code_%d_%d_%s%s;\n"
	.size	.L.str.35, 46

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"int d_final_reduction_code_%d_%d_%s%s;\n"
	.size	.L.str.36, 40

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"extern D_ReductionCode d_pass_code_%d_%d_%s[];\n"
	.size	.L.str.37, 48

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"int d_pass_code_%d_%d_%d_%s%s;\n"
	.size	.L.str.38, 32

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"int d_speculative_reduction_code_%d_%d_%s%s "
	.size	.L.str.39, 45

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"int d_final_reduction_code_%d_%d_%s%s "
	.size	.L.str.40, 39

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"int d_pass_code_%d_%d_%d_%s%s "
	.size	.L.str.41, 31

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"d_speculative_reduction_code_%d_%d_%s"
	.size	.L.str.42, 38

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"NULL"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"d_final_reduction_code_%d_%d_%s"
	.size	.L.str.44, 32

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"d_pass_code_%d_%d_%s"
	.size	.L.str.45, 21

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"D_ReductionCode %s[] = {"
	.size	.L.str.46, 25

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"d_pass_code_%d_%d_%d_%s%s"
	.size	.L.str.47, 26

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"NULL%s"
	.size	.L.str.48, 7

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"};\n\n"
	.size	.L.str.49, 5

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"D_Reduction d_reduction_%d_%s = "
	.size	.L.str.50, 33

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"{%d, %d, %s, %s, %d, %d, %d, %d, %d, %d, %s};\n"
	.size	.L.str.51, 47

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"(void *_ps, void **_children, int _n_children, int _offset, D_Parser *_parser)"
	.size	.L.str.52, 79

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"{\n"
	.size	.L.str.53, 3

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"(_n_children)"
	.size	.L.str.54, 14

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"(D_PN(_ps, _offset)->globals)"
	.size	.L.str.55, 30

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"(*(D_PN(_children[%d], _offset)))"
	.size	.L.str.56, 34

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"$nXXXX greater than number of children at line %d"
	.size	.L.str.57, 50

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"(*(D_PN(_ps, _offset)))"
	.size	.L.str.58, 24

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"(D_PN(_ps, _offset)->user)"
	.size	.L.str.59, 27

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"(D_PN(_children[%d], _offset)->user)"
	.size	.L.str.60, 37

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"reject"
	.size	.L.str.61, 7

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	" return -1 "
	.size	.L.str.62, 12

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"free_below"
	.size	.L.str.63, 11

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	" free_D_ParseTreeBelow(_parser, (D_PN(_ps, _offset)))"
	.size	.L.str.64, 54

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"scope"
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"(D_PN(_ps, _offset)->scope)"
	.size	.L.str.66, 28

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"parser"
	.size	.L.str.67, 7

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"_parser"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"pass"
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"unknown pass '%s' line %d"
	.size	.L.str.70, 26

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"  return 0;"
	.size	.L.str.71, 12

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"}\n\n"
	.size	.L.str.72, 4

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"D_Shift d_shift_%d_%s = { %d, %d, %d, %d, %d, %s };\n"
	.size	.L.str.73, 53

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"D_Shift *d_shifts_%d_%s[] = {\n"
	.size	.L.str.74, 31

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"&d_shift_%d_%s%s"
	.size	.L.str.75, 17

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	", NULL"
	.size	.L.str.76, 7

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"D_Shift *d_accepts_diff_%d_%d_%s[] = {"
	.size	.L.str.77, 39

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"&d_shift_%d_%s,"
	.size	.L.str.78, 16

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"0};\n"
	.size	.L.str.79, 5

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"D_Shift **d_accepts_diff_%d_%s[] = {\n"
	.size	.L.str.80, 38

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"d_accepts_diff_%d_%d_%s%s"
	.size	.L.str.81, 26

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	",\n"
	.size	.L.str.82, 3

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"%s d_scanner_%d_%d_%d_%s[SCANNER_BLOCK_SIZE] = {\n"
	.size	.L.str.83, 50

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	","
	.size	.L.str.84, 2

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"%s d_accepts_diff_%d_%d_%d_%s[SCANNER_BLOCK_SIZE] = {\n"
	.size	.L.str.85, 55

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"d_shift_%d_%d_%s"
	.size	.L.str.86, 17

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"D_Shift *%s[] = { "
	.size	.L.str.87, 19

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	", NULL};\n\n"
	.size	.L.str.88, 11

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"SB_%s d_scanner_%d_%s[%d] = {\n"
	.size	.L.str.89, 31

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"{ %s, {"
	.size	.L.str.90, 8

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"{ d_shift_%d_%d_%s, {"
	.size	.L.str.91, 22

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"{ NULL, {"
	.size	.L.str.92, 10

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"d_scanner_%d_%d_%d_%s"
	.size	.L.str.93, 22

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"\n  "
	.size	.L.str.94, 4

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"}},\n"
	.size	.L.str.95, 5

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"}}\n"
	.size	.L.str.96, 4

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"SB_trans_%s d_transition_%d_%s[%d] = {\n"
	.size	.L.str.97, 40

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"{{ "
	.size	.L.str.98, 4

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"d_accepts_diff_%d_%d_%d_%s"
	.size	.L.str.99, 27

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"unsigned char"
	.size	.L.str.100, 14

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"unsigned short"
	.size	.L.str.101, 15

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"unsigned int"
	.size	.L.str.102, 13

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"uint8"
	.size	.L.str.104, 6

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"uint16"
	.size	.L.str.105, 7

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"uint32"
	.size	.L.str.106, 7

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"int d_scan_code_%d_%s(char **as, int *col, int *line,unsigned short *symbol, int *term_priority,unsigned char *op_assoc, int *op_priority) {\n  int res;\n"
	.size	.L.str.107, 153

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"  if ((res = "
	.size	.L.str.108, 14

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"%s("
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"as, col, line, op_assoc, op_priority))) {\n    *symbol = %d;\n    *term_priority = %d;\n    return res;\n  }\n"
	.size	.L.str.110, 106

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"  return 0;\n}\n\n"
	.size	.L.str.111, 16

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"unsigned char d_goto_valid_%d_%s[] = {\n"
	.size	.L.str.112, 40

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"0x%x%s"
	.size	.L.str.113, 7

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"D_Reduction *d_reductions_%d_%s[] = {"
	.size	.L.str.114, 38

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"&d_reduction_%d_%s%s"
	.size	.L.str.115, 21

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"D_RightEpsilonHint d_right_epsilon_hints_%d_%s[] = {"
	.size	.L.str.116, 53

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"{ %d, %d, &d_reduction_%d_%s}%s"
	.size	.L.str.117, 32

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"unsigned short d_gotos_%s[%d] = {\n"
	.size	.L.str.118, 35

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"unsigned short d_gotos_%s[1] = {0};\n"
	.size	.L.str.119, 37

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"D_ErrorRecoveryHint d_error_recovery_hints_%d_%s[] = {%s"
	.size	.L.str.120, 57

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"{ %d, %d, \"%s\"}%s"
	.size	.L.str.121, 18

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"\nD_State d_states_%s[] = "
	.size	.L.str.122, 26

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"{ "
	.size	.L.str.123, 3

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"d_goto_valid_%d_%s, "
	.size	.L.str.124, 21

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"{ %d, d_reductions_%d_%s}, "
	.size	.L.str.125, 28

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"{ 0, NULL}, "
	.size	.L.str.126, 13

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"{ %d, d_right_epsilon_hints_%d_%s}, "
	.size	.L.str.127, 37

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"{ %d, d_error_recovery_hints_%d_%s}, "
	.size	.L.str.128, 38

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"d_shifts_%d_%s, "
	.size	.L.str.129, 17

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"D_SHIFTS_CODE, "
	.size	.L.str.130, 16

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"d_scan_code_%d_%s, "
	.size	.L.str.131, 20

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"(void*)d_scanner_%d_%s, "
	.size	.L.str.132, 25

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"sizeof(%s), "
	.size	.L.str.133, 13

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"1, "
	.size	.L.str.134, 4

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"0, "
	.size	.L.str.135, 4

	.type	scan_kind_strings,@object # @scan_kind_strings
	.section	.rodata,"a",@progbits
	.p2align	4
scan_kind_strings:
	.quad	.L.str.142
	.quad	.L.str.143
	.quad	.L.str.144
	.quad	0
	.size	scan_kind_strings, 32

	.type	.L.str.136,@object      # @.str.136
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.136:
	.asciz	"(void*)d_transition_%d_%s, "
	.size	.L.str.136, 28

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"d_accepts_diff_%d_%s, "
	.size	.L.str.137, 23

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"(D_Shift***)NULL, "
	.size	.L.str.138, 19

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"-1"
	.size	.L.str.139, 3

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"}%s\n"
	.size	.L.str.140, 5

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"{{0, {0, NULL}, 0, NULL}};\n\n"
	.size	.L.str.141, 29

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"D_SCAN_ALL"
	.size	.L.str.142, 11

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"D_SCAN_LONGEST"
	.size	.L.str.143, 15

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"D_SCAN_MIXED"
	.size	.L.str.144, 13

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"D_Symbol d_symbols_%s[] = {\n"
	.size	.L.str.145, 29

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"{%s, \"%s\", %d},\n"
	.size	.L.str.146, 17

	.type	d_internal,@object      # @d_internal
	.section	.rodata,"a",@progbits
	.p2align	4
d_internal:
	.quad	.L.str.147
	.quad	.L.str.148
	.size	d_internal, 16

	.type	d_symbol,@object        # @d_symbol
	.p2align	4
d_symbol:
	.quad	.L.str.149
	.quad	.L.str.150
	.quad	.L.str.151
	.quad	.L.str.152
	.size	d_symbol, 32

	.type	.L.str.147,@object      # @.str.147
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.147:
	.asciz	"D_SYMBOL_NTERM"
	.size	.L.str.147, 15

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"D_SYMBOL_INTERNAL"
	.size	.L.str.148, 18

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"D_SYMBOL_STRING"
	.size	.L.str.149, 16

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"D_SYMBOL_REGEX"
	.size	.L.str.150, 15

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"D_SYMBOL_CODE"
	.size	.L.str.151, 14

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"D_SYMBOL_TOKEN"
	.size	.L.str.152, 15

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"D_Pass d_passes_%s[] = {\n"
	.size	.L.str.153, 26

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"{\"%s\", %d, 0x%X, %d}%s\n"
	.size	.L.str.154, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
