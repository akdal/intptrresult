	.text
	.file	"tol.bc"
	.globl	T_initdefault
	.p2align	4, 0x90
	.type	T_initdefault,@function
T_initdefault:                          # @T_initdefault
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	cmpb	$1, T_initdefault.called_before(%rip)
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_2:
	cmpq	$0, _T_gtol(%rip)
	jne	.LBB0_13
# BB#3:                                 # %.loopexit.i
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %rbx
	movq	%rbx, _T_gtol(%rip)
	movl	$0, (%rbx)
	movq	$0, 16(%rbx)
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rax, 8(%rbx)
	cmpl	$0, 4(%rax)
	je	.LBB0_5
# BB#4:
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_5:                                # %_T_addtol.exit
	movq	_T_gtol(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader.i2
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_7
# BB#8:                                 # %.loopexit.i4.loopexit
	addq	$16, %rbx
	jmp	.LBB0_9
.LBB0_6:
	movl	$_T_gtol, %ebx
.LBB0_9:                                # %.loopexit.i4
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %r14
	movq	%r14, (%rbx)
	movl	$1, (%r14)
	movq	$0, 16(%r14)
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rax, %rbx
	movq	%rbx, 8(%r14)
	cmpl	$0, 4(%rbx)
	je	.LBB0_11
# BB#10:
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%r14), %rbx
.LBB0_11:
	movl	$.L.str.4, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	F_floatcmp
	testl	%eax, %eax
	jle	.LBB0_13
# BB#12:
	movl	$Z_err_buf, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_13:                               # %_T_addtol.exit5
	movb	$1, T_initdefault.called_before(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	T_initdefault, .Lfunc_end0-T_initdefault
	.cfi_endproc

	.globl	T_clear_tols
	.p2align	4, 0x90
	.type	T_clear_tols,@function
T_clear_tols:                           # @T_clear_tols
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, _T_tols+64(%rip)
	movaps	%xmm0, _T_tols+48(%rip)
	movaps	%xmm0, _T_tols+32(%rip)
	movaps	%xmm0, _T_tols+16(%rip)
	movaps	%xmm0, _T_tols(%rip)
	retq
.Lfunc_end1:
	.size	T_clear_tols, .Lfunc_end1-T_clear_tols
	.cfi_endproc

	.globl	T_setdef
	.p2align	4, 0x90
	.type	T_setdef,@function
T_setdef:                               # @T_setdef
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	$0, _T_gtol(%rip)
	movl	$_T_gtol, %edi
	movq	%rax, %rsi
	jmp	_T_settol               # TAILCALL
.Lfunc_end2:
	.size	T_setdef, .Lfunc_end2-T_setdef
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	_T_settol,@function
_T_settol:                              # @_T_settol
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 64
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r13, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	%rsi, 8(%rsp)
	cmpb	$0, (%rsi)
	je	.LBB3_28
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
                                        #     Child Loop BB3_18 Depth 2
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_5 Depth 2
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_skipspace
	movq	8(%rsp), %rax
	movsbl	(%rax), %ebx
	incq	%rax
	movq	%rax, 8(%rsp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_skipspace
	leal	-97(%rbx), %eax
	cmpl	$17, %eax
	ja	.LBB3_25
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %r12
	movq	(%r15), %rax
	testq	%rax, %rax
	movq	%r15, %rbx
	je	.LBB3_7
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB3_5
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	addq	$16, %rbx
.LBB3_7:                                # %.loopexit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %r13
	movq	%r13, (%rbx)
	movl	$0, (%r13)
	movq	$0, 16(%r13)
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	F_atof
	movq	%rax, 8(%r13)
	cmpl	$0, 4(%rax)
	je	.LBB3_27
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$Z_err_buf, %edi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	sprintf
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_21:                               #   in Loop: Header=BB3_2 Depth=1
	movq	_T_gtol(%rip), %rax
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	movq	%r15, %rcx
	je	.LBB3_24
	.p2align	4, 0x90
.LBB3_22:                               # %.preheader.i16
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_22
# BB#23:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$16, %rcx
.LBB3_24:                               # %_T_appendtols.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_2 Depth=1
	movq	(%r15), %rax
	testq	%rax, %rax
	movq	%r15, %rbx
	je	.LBB3_20
	.p2align	4, 0x90
.LBB3_18:                               # %.preheader.i11
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB3_18
# BB#19:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$16, %rbx
.LBB3_20:                               # %_T_addtol.exit14
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, (%rbx)
	movl	$2, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %r12
	movq	(%r15), %rax
	testq	%rax, %rax
	movq	%r15, %rbx
	je	.LBB3_13
	.p2align	4, 0x90
.LBB3_11:                               # %.preheader.i6
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB3_11
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$16, %rbx
.LBB3_13:                               # %.loopexit.i8
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %r13
	movq	%r13, (%rbx)
	movl	$1, (%r13)
	movq	$0, 16(%r13)
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	F_atof
	movq	%rax, %rbx
	movq	%rbx, 8(%r13)
	cmpl	$0, 4(%rbx)
	je	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%r13), %rbx
.LBB3_15:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.4, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	F_floatcmp
	testl	%eax, %eax
	jle	.LBB3_27
# BB#16:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$Z_err_buf, %edi
	movl	$.L.str.5, %esi
.LBB3_9:                                # %_T_addtol.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	sprintf
.LBB3_26:                               # %_T_addtol.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB3_27:                               # %_T_addtol.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_nextword
	movq	8(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.LBB3_2
.LBB3_28:                               # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_T_settol, .Lfunc_end3-_T_settol
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_4
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_21
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_17
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_10

	.text
	.globl	T_tolline
	.p2align	4, 0x90
	.type	T_tolline,@function
T_tolline:                              # @T_tolline
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, _T_tols+64(%rip)
	movaps	%xmm0, _T_tols+48(%rip)
	movaps	%xmm0, _T_tols+32(%rip)
	movaps	%xmm0, _T_tols+16(%rip)
	movaps	%xmm0, _T_tols(%rip)
	cmpb	$0, (%rbx)
	je	.LBB4_13
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_9 Depth 2
	cmpq	$10, %r14
	jl	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	leaq	_T_tols(,%r14,8), %rdi
	xorl	%eax, %eax
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_5 Depth=2
	movb	%cl, _T_getspec.retval(%rax)
	incq	%rax
.LBB4_5:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx,%rax), %ecx
	testb	%cl, %cl
	je	.LBB4_8
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=2
	cmpb	$59, %cl
	jne	.LBB4_7
.LBB4_8:                                # %_T_getspec.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movb	$0, _T_getspec.retval(%rax)
	movl	$_T_getspec.retval, %esi
	callq	_T_settol
	incq	%r14
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_9 Depth=2
	incq	%rbx
.LBB4_9:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.LBB4_12
# BB#10:                                #   in Loop: Header=BB4_9 Depth=2
	cmpb	$59, %al
	jne	.LBB4_11
.LBB4_12:                               # %_T_nextspec.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rbx), %rcx
	cmpb	$59, %al
	cmoveq	%rcx, %rbx
	cmpb	$0, (%rbx)
	jne	.LBB4_2
.LBB4_13:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	T_tolline, .Lfunc_end4-T_tolline
	.cfi_endproc

	.globl	T_moretols
	.p2align	4, 0x90
	.type	T_moretols,@function
T_moretols:                             # @T_moretols
	.cfi_startproc
# BB#0:
	cmpl	$8, %edi
	ja	.LBB5_1
# BB#2:
	movslq	%edi, %rax
	cmpq	$0, _T_tols+8(,%rax,8)
	setne	%al
	movzbl	%al, %eax
	retq
.LBB5_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end5:
	.size	T_moretols, .Lfunc_end5-T_moretols
	.cfi_endproc

	.globl	T_gettol
	.p2align	4, 0x90
	.type	T_gettol,@function
T_gettol:                               # @T_gettol
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movq	_T_tols(,%rax,8), %rax
	retq
.Lfunc_end6:
	.size	T_gettol, .Lfunc_end6-T_gettol
	.cfi_endproc

	.globl	T_picktol
	.p2align	4, 0x90
	.type	T_picktol,@function
T_picktol:                              # @T_picktol
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#2:
	movq	%rdi, %rax
	retq
.LBB7_1:
	testq	%rsi, %rsi
	cmoveq	_T_gtol(%rip), %rsi
	movq	%rsi, %rax
	retq
.Lfunc_end7:
	.size	T_picktol, .Lfunc_end7-T_picktol
	.cfi_endproc

	.globl	_T_appendtols
	.p2align	4, 0x90
	.type	_T_appendtols,@function
_T_appendtols:                          # @_T_appendtols
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB8_1
# BB#2:
	addq	$16, %rdi
.LBB8_3:                                # %.loopexit
	movq	%rsi, (%rdi)
	retq
.Lfunc_end8:
	.size	_T_appendtols, .Lfunc_end8-_T_appendtols
	.cfi_endproc

	.globl	_T_addtol
	.p2align	4, 0x90
	.type	_T_addtol,@function
_T_addtol:                              # @_T_addtol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	movq	16(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB9_1
# BB#2:
	addq	$16, %rbp
.LBB9_3:                                # %.loopexit
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %rbx
	movq	%rbx, (%rbp)
	movl	%r15d, (%rbx)
	movq	$0, 16(%rbx)
	cmpl	$2, %r15d
	jne	.LBB9_5
# BB#4:
	movq	$0, 8(%rbx)
	jmp	.LBB9_9
.LBB9_5:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	F_atof
	movq	%rax, 8(%rbx)
	cmpl	$0, 4(%rax)
	je	.LBB9_7
# BB#6:
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB9_7:
	cmpl	$1, %r15d
	jne	.LBB9_9
# BB#8:
	movq	8(%rbx), %rbx
	movl	$.L.str.4, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	F_floatcmp
	testl	%eax, %eax
	jle	.LBB9_9
# BB#10:
	movl	$Z_err_buf, %edi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Z_fatal                 # TAILCALL
.LBB9_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_T_addtol, .Lfunc_end9-_T_addtol
	.cfi_endproc

	.type	_T_gtol,@object         # @_T_gtol
	.bss
	.globl	_T_gtol
	.p2align	3
_T_gtol:
	.quad	0
	.size	_T_gtol, 8

	.type	T_initdefault.called_before,@object # @T_initdefault.called_before
	.local	T_initdefault.called_before
	.comm	T_initdefault.called_before,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"T_initdefault called more than once\n"
	.size	.L.str, 37

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1e-10"
	.size	.L.str.1, 6

	.type	_T_tols,@object         # @_T_tols
	.local	_T_tols
	.comm	_T_tols,80,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"too many tolerances per line"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s : negative tolerances don't make any sense\n"
	.size	.L.str.3, 47

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2.0"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s : relative tolerances greater than 2 don't make any sense\n"
	.size	.L.str.5, 62

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"don't understand tolerance type '%c'\n"
	.size	.L.str.6, 38

	.type	_T_getspec.retval,@object # @_T_getspec.retval
	.local	_T_getspec.retval
	.comm	_T_getspec.retval,1024,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
