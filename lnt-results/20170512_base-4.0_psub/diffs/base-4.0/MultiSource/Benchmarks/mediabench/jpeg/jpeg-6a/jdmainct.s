	.text
	.file	"jdmainct.bc"
	.globl	jinit_d_main_controller
	.p2align	4, 0x90
	.type	jinit_d_main_controller,@function
jinit_d_main_controller:                # @jinit_d_main_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r13
	movq	8(%r13), %rax
	movl	$1, %esi
	movl	$136, %edx
	callq	*(%rax)
	movq	%rax, 536(%r13)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	$start_pass_main, (%rax)
	testl	%ebp, %ebp
	je	.LBB0_2
# BB#1:
	movq	(%r13), %rax
	movl	$4, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_2:
	movq	592(%r13), %rax
	movl	396(%r13), %r15d
	cmpl	$0, 16(%rax)
	je	.LBB0_3
# BB#4:
	cmpl	$1, %r15d
	jg	.LBB0_6
# BB#5:
	movq	(%r13), %rax
	movl	$46, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	396(%r13), %r15d
.LBB0_6:
	movq	536(%r13), %rbp
	movq	8(%r13), %rax
	movslq	48(%r13), %rdx
	shlq	$4, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*(%rax)
	movq	%rax, 104(%rbp)
	movslq	48(%r13), %rcx
	testq	%rcx, %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 112(%rbp)
	jle	.LBB0_9
# BB#7:                                 # %.lr.ph.i
	movq	296(%r13), %rbx
	addl	$4, %r15d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	addq	$36, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	imull	-24(%rbx), %eax
	cltd
	idivl	396(%r13)
	movq	%rbp, %r15
	movl	%eax, %ebp
	movq	8(%r13), %rax
	movl	%ebp, %r12d
	imull	8(%rsp), %r12d          # 4-byte Folded Reload
	leal	(%r12,%r12), %ecx
	movslq	%ecx, %rdx
	shlq	$3, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*(%rax)
	movslq	%ebp, %rcx
	movq	%r15, %rbp
	leaq	(%rax,%rcx,8), %rax
	movq	104(%rbp), %rcx
	movq	%rax, (%rcx,%r14,8)
	movslq	%r12d, %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	112(%rbp), %rcx
	movq	%rax, (%rcx,%r14,8)
	incq	%r14
	movslq	48(%r13), %rcx
	addq	$96, %rbx
	cmpq	%rcx, %r14
	jl	.LBB0_8
.LBB0_9:                                # %alloc_funny_pointers.exit
	movl	396(%r13), %r15d
	leal	2(%r15), %r14d
	testl	%ecx, %ecx
	jg	.LBB0_11
	jmp	.LBB0_14
.LBB0_3:                                # %._crit_edge44
	movl	48(%r13), %ecx
	movl	%r15d, %r14d
	testl	%ecx, %ecx
	jle	.LBB0_14
.LBB0_11:                               # %.lr.ph
	movq	296(%r13), %rbx
	addq	$36, %rbx
	movl	$1, %r12d
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_13:                               # %._crit_edge46
                                        #   in Loop: Header=BB0_12 Depth=1
	movl	396(%r13), %r15d
	incq	%r12
	addq	$96, %rbx
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	movl	-24(%rbx), %eax
	imull	%ecx, %eax
	cltd
	idivl	%r15d
	movq	8(%r13), %rbp
	imull	-8(%rbx), %ecx
	imull	%r14d, %eax
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%ecx, %edx
	movl	%eax, %ecx
	callq	*16(%rbp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx,%r12,8)
	movslq	48(%r13), %rax
	cmpq	%rax, %r12
	jl	.LBB0_13
.LBB0_14:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_d_main_controller, .Lfunc_end0-jinit_d_main_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_main,@function
start_pass_main:                        # @start_pass_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	536(%rdi), %r13
	cmpl	$2, %esi
	je	.LBB1_54
# BB#1:
	testl	%esi, %esi
	jne	.LBB1_55
# BB#2:
	movq	592(%rdi), %rax
	cmpl	$0, 16(%rax)
	je	.LBB1_56
# BB#3:
	movq	$process_data_context_main, 8(%r13)
	movslq	48(%rdi), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB1_53
# BB#4:                                 # %.lr.ph79.i
	movl	396(%rdi), %r14d
	movq	296(%rdi), %r12
	leal	2(%r14), %eax
	movl	%eax, -60(%rsp)         # 4-byte Spill
	leal	-2(%r14), %eax
	movl	%eax, -64(%rsp)         # 4-byte Spill
	xorl	%r9d, %r9d
	movq	%r13, -88(%rsp)         # 8-byte Spill
	movq	%r14, -80(%rsp)         # 8-byte Spill
	jmp	.LBB1_5
.LBB1_54:
	movq	$process_data_crank_post, 8(%r13)
	jmp	.LBB1_58
.LBB1_55:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB1_39:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB1_44
.LBB1_42:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	48(%r11,%rax,8), %rdx
	leaq	48(%r8,%rax,8), %rsi
	leaq	48(%r8,%rbp,8), %rbx
	leaq	48(%r11,%rbp,8), %r13
	.p2align	4, 0x90
.LBB1_43:                               # %vector.body
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rsi,%rcx,8), %xmm0
	movups	-32(%rsi,%rcx,8), %xmm1
	movups	%xmm0, -48(%r13,%rcx,8)
	movups	%xmm1, -32(%r13,%rcx,8)
	movups	-48(%rbx,%rcx,8), %xmm0
	movups	-32(%rbx,%rcx,8), %xmm1
	movups	%xmm0, -48(%rdx,%rcx,8)
	movups	%xmm1, -32(%rdx,%rcx,8)
	movups	-16(%rsi,%rcx,8), %xmm0
	movups	(%rsi,%rcx,8), %xmm1
	movups	%xmm0, -16(%r13,%rcx,8)
	movups	%xmm1, (%r13,%rcx,8)
	movups	-16(%rbx,%rcx,8), %xmm0
	movups	(%rbx,%rcx,8), %xmm1
	movups	%xmm0, -16(%rdx,%rcx,8)
	movups	%xmm1, (%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %r14
	jne	.LBB1_43
.LBB1_44:                               # %middle.block
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	%r14, %r10
	movq	%r14, %rcx
	movq	-88(%rsp), %r13         # 8-byte Reload
	movq	-104(%rsp), %rbx        # 8-byte Reload
	jne	.LBB1_27
	jmp	.LBB1_45
.LBB1_16:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	movq	-88(%rsp), %r13         # 8-byte Reload
	jmp	.LBB1_19
.LBB1_18:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
.LBB1_19:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	-80(%rsp), %r14         # 8-byte Reload
	movq	-96(%rsp), %rax         # 8-byte Reload
	jmp	.LBB1_8
.LBB1_36:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
	movq	%rsi, %r13
	jmp	.LBB1_27
.LBB1_33:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movq	%rsi, %r13
	jmp	.LBB1_27
.LBB1_31:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
	movq	-88(%rsp), %r13         # 8-byte Reload
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movq	-48(%rsp), %rbp         # 8-byte Reload
	movq	-56(%rsp), %r12         # 8-byte Reload
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
                                        #     Child Loop BB1_10 Depth 2
                                        #     Child Loop BB1_13 Depth 2
                                        #     Child Loop BB1_43 Depth 2
                                        #     Child Loop BB1_28 Depth 2
                                        #     Child Loop BB1_48 Depth 2
                                        #     Child Loop BB1_51 Depth 2
	movl	36(%r12), %eax
	imull	12(%r12), %eax
	cltd
	idivl	%r14d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	104(%r13), %rcx
	movq	112(%r13), %rdx
	movq	(%rcx,%r9,8), %r15
	movq	(%rdx,%r9,8), %r11
	movq	16(%r13,%r9,8), %r8
	movl	%eax, %ebp
	imull	-60(%rsp), %ebp         # 4-byte Folded Reload
	testl	%ebp, %ebp
	jle	.LBB1_24
# BB#6:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	%ebp, %edx
	cmpl	$3, %ebp
	jbe	.LBB1_7
# BB#14:                                # %min.iters.checked55
                                        #   in Loop: Header=BB1_5 Depth=1
	andl	$3, %ebp
	movq	%rdx, %r10
	subq	%rbp, %r10
	je	.LBB1_7
# BB#15:                                # %vector.memcheck78
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	(%r11,%rdx,8), %rsi
	leaq	(%r15,%rdx,8), %r13
	leaq	(%r8,%rdx,8), %rdi
	cmpq	%r13, %r11
	sbbb	%bl, %bl
	cmpq	%rsi, %r15
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rdi, %r11
	sbbb	%bl, %bl
	cmpq	%rsi, %r8
	sbbb	%r14b, %r14b
	cmpq	%rdi, %r15
	sbbb	%dil, %dil
	cmpq	%r13, %r8
	sbbb	%sil, %sil
	testb	$1, %cl
	jne	.LBB1_16
# BB#17:                                # %vector.memcheck78
                                        #   in Loop: Header=BB1_5 Depth=1
	andb	%r14b, %bl
	andb	$1, %bl
	movq	-88(%rsp), %r13         # 8-byte Reload
	jne	.LBB1_18
# BB#20:                                # %vector.memcheck78
                                        #   in Loop: Header=BB1_5 Depth=1
	andb	%sil, %dil
	andb	$1, %dil
	movl	$0, %esi
	movq	-80(%rsp), %r14         # 8-byte Reload
	movq	-96(%rsp), %rax         # 8-byte Reload
	jne	.LBB1_8
# BB#21:                                # %vector.body51.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	leaq	16(%r8), %rcx
	leaq	16(%r11), %rsi
	leaq	16(%r15), %rbx
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB1_22:                               # %vector.body51
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB1_22
# BB#23:                                # %middle.block52
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%ebp, %ebp
	movq	%r10, %rsi
	movq	-96(%rsp), %rax         # 8-byte Reload
	jne	.LBB1_8
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
.LBB1_8:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	%edx, %edi
	subl	%esi, %edi
	leaq	-1(%rdx), %rcx
	subq	%rsi, %rcx
	andq	$3, %rdi
	je	.LBB1_11
# BB#9:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rsi,8), %rbp
	movq	%rbp, (%r11,%rsi,8)
	movq	%rbp, (%r15,%rsi,8)
	incq	%rsi
	incq	%rdi
	jne	.LBB1_10
.LBB1_11:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$3, %rcx
	jb	.LBB1_24
# BB#12:                                # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	subq	%rsi, %rdx
	leaq	24(%r15,%rsi,8), %rbp
	leaq	24(%r11,%rsi,8), %rcx
	leaq	24(%r8,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.i
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rsi), %rdi
	movq	%rdi, -24(%rcx)
	movq	%rdi, -24(%rbp)
	movq	-16(%rsi), %rdi
	movq	%rdi, -16(%rcx)
	movq	%rdi, -16(%rbp)
	movq	-8(%rsi), %rdi
	movq	%rdi, -8(%rcx)
	movq	%rdi, -8(%rbp)
	movq	(%rsi), %rdi
	movq	%rdi, (%rcx)
	movq	%rdi, (%rbp)
	addq	$32, %rbp
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB1_13
.LBB1_24:                               # %.preheader70.i
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%eax, %eax
	jle	.LBB1_52
# BB#25:                                # %.lr.ph73.i
                                        #   in Loop: Header=BB1_5 Depth=1
	leal	(%rax,%rax), %ecx
	movl	%eax, %edx
	imull	%r14d, %edx
	movl	%eax, %esi
	imull	-64(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rbp
	movslq	%edx, %rbx
	movslq	%ecx, %rdi
	testq	%rdi, %rdi
	movl	$1, %r10d
	cmovgq	%rdi, %r10
	cmpq	$3, %r10
	movq	%rax, -96(%rsp)         # 8-byte Spill
	jbe	.LBB1_26
# BB#29:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r10, %r14
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	andq	%rax, %r14
	je	.LBB1_26
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r12, -56(%rsp)         # 8-byte Spill
	leaq	(%r11,%rbp,8), %r13
	testq	%rdi, %rdi
	movl	$1, %ecx
	cmovgq	%rdi, %rcx
	leaq	(%rcx,%rbp), %rsi
	movq	%rbp, %rax
	leaq	(%r11,%rsi,8), %rbp
	leaq	(%r11,%rbx,8), %r12
	addq	%rbx, %rcx
	leaq	(%r11,%rcx,8), %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%rbx, -104(%rsp)        # 8-byte Spill
	leaq	(%r8,%rbx,8), %rbx
	movq	%rbx, -72(%rsp)         # 8-byte Spill
	leaq	(%r8,%rcx,8), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	(%r8,%rsi,8), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	cmpq	%rdx, %r13
	sbbb	%dl, %dl
	cmpq	%rbp, %r12
	sbbb	%sil, %sil
	andb	%dl, %sil
	cmpq	%rcx, %r13
	sbbb	%cl, %cl
	cmpq	%rbp, %rbx
	sbbb	%dl, %dl
	movb	%dl, -105(%rsp)         # 1-byte Spill
	cmpq	%rax, %r13
	sbbb	%bl, %bl
	movq	-24(%rsp), %rdx         # 8-byte Reload
	cmpq	%rbp, %rdx
	sbbb	%al, %al
	movb	%al, -106(%rsp)         # 1-byte Spill
	cmpq	-40(%rsp), %r12         # 8-byte Folded Reload
	sbbb	%r13b, %r13b
	movq	-16(%rsp), %rbp         # 8-byte Reload
	cmpq	%rbp, -72(%rsp)         # 8-byte Folded Reload
	sbbb	%al, %al
	movb	%al, -72(%rsp)          # 1-byte Spill
	cmpq	-32(%rsp), %r12         # 8-byte Folded Reload
	sbbb	%r12b, %r12b
	cmpq	%rbp, %rdx
	sbbb	%bpl, %bpl
	testb	$1, %sil
	jne	.LBB1_31
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	%r12d, %edx
	movl	%ebp, %eax
	andb	-105(%rsp), %cl         # 1-byte Folded Reload
	andb	$1, %cl
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movq	-48(%rsp), %rbp         # 8-byte Reload
	movq	-56(%rsp), %r12         # 8-byte Reload
	jne	.LBB1_33
# BB#34:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	andb	-106(%rsp), %bl         # 1-byte Folded Reload
	andb	$1, %bl
	jne	.LBB1_33
# BB#35:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	andb	-72(%rsp), %r13b        # 1-byte Folded Reload
	andb	$1, %r13b
	movq	-104(%rsp), %rbx        # 8-byte Reload
	jne	.LBB1_36
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_5 Depth=1
	andb	%al, %dl
	andb	$1, %dl
	movl	$0, %ecx
	movq	%rsi, %r13
	jne	.LBB1_27
# BB#38:                                # %vector.body.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	leaq	-4(%r14), %rax
	movq	%rax, %rdx
	shrq	$2, %rdx
	btl	$2, %eax
	jb	.LBB1_39
# BB#40:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_5 Depth=1
	movups	(%r8,%rbx,8), %xmm0
	movups	16(%r8,%rbx,8), %xmm1
	movups	%xmm0, (%r11,%rbp,8)
	movups	%xmm1, 16(%r11,%rbp,8)
	movups	(%r8,%rbp,8), %xmm0
	movups	16(%r8,%rbp,8), %xmm1
	movups	%xmm0, (%r11,%rbx,8)
	movups	%xmm1, 16(%r11,%rbx,8)
	movl	$4, %ecx
	testq	%rdx, %rdx
	jne	.LBB1_42
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
.LBB1_27:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	leaq	(%r8,%rbp,8), %rdx
	leaq	(%r11,%rbp,8), %rsi
	leaq	(%r11,%rbx,8), %rbp
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_28:                               # %scalar.ph
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rcx,8), %rax
	movq	%rax, (%rsi,%rcx,8)
	movq	(%rdx,%rcx,8), %rax
	movq	%rax, (%rbp,%rcx,8)
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB1_28
.LBB1_45:                               # %.lr.ph75.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	-96(%rsp), %rax         # 8-byte Reload
	movslq	%eax, %rcx
	movl	%eax, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_46
# BB#47:                                # %.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	leaq	(,%rcx,8), %rsi
	movq	%r15, %rbp
	subq	%rsi, %rbp
	xorl	%esi, %esi
	movq	-80(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_48:                               #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rbx
	movq	%rbx, (%rbp,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB1_48
	jmp	.LBB1_49
	.p2align	4, 0x90
.LBB1_46:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	movq	-80(%rsp), %r14         # 8-byte Reload
.LBB1_49:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$3, %rdx
	jb	.LBB1_52
# BB#50:                                # %.lr.ph75.i.new
                                        #   in Loop: Header=BB1_5 Depth=1
	subq	%rsi, %rax
	subq	%rcx, %rsi
	leaq	24(%r15,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB1_51:                               #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rdx
	movq	%rdx, -24(%rcx)
	movq	(%r15), %rdx
	movq	%rdx, -16(%rcx)
	movq	(%r15), %rdx
	movq	%rdx, -8(%rcx)
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB1_51
.LBB1_52:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_5 Depth=1
	incq	%r9
	addq	$96, %r12
	cmpq	-8(%rsp), %r9           # 8-byte Folded Reload
	jne	.LBB1_5
.LBB1_53:                               # %make_funny_pointers.exit
	movq	$0, 120(%r13)
	movl	$0, 132(%r13)
	jmp	.LBB1_57
.LBB1_56:
	movq	$process_data_simple_main, 8(%r13)
.LBB1_57:
	movq	$0, 96(%r13)
.LBB1_58:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass_main, .Lfunc_end1-start_pass_main
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_data_context_main,@function
process_data_context_main:              # @process_data_context_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 112
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	536(%r12), %r15
	cmpl	$0, 96(%r15)
	jne	.LBB2_3
# BB#1:
	movq	544(%r12), %rax
	movslq	120(%r15), %rcx
	movq	104(%r15,%rcx,8), %rsi
	movq	%r12, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB2_35
# BB#2:
	movl	$1, 96(%r15)
	incl	132(%r15)
.LBB2_3:
	movl	124(%r15), %eax
	cmpl	$2, %eax
	je	.LBB2_8
# BB#4:
	cmpl	$1, %eax
	je	.LBB2_7
# BB#5:
	testl	%eax, %eax
	jne	.LBB2_35
# BB#6:                                 # %._crit_edge59
	leaq	100(%r15), %rbp
.LBB2_10:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$0, 100(%r15)
	movl	396(%r12), %r9d
	leal	-1(%r9), %eax
	movl	%eax, 128(%r15)
	movl	132(%r15), %eax
	cmpl	400(%r12), %eax
	jne	.LBB2_19
# BB#11:
	movslq	48(%r12), %r8
	testq	%r8, %r8
	jle	.LBB2_19
# BB#12:                                # %.lr.ph50.i
	movq	536(%r12), %r10
	movq	296(%r12), %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
	movl	36(%rbp), %ecx
	movl	44(%rbp), %edi
	imull	12(%rbp), %ecx
	movl	%ecx, %eax
	cltd
	idivl	%r9d
	movl	%eax, %esi
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%ecx
	testl	%edx, %edx
	cmovnel	%edx, %ecx
	testq	%r11, %r11
	jne	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
	leal	-1(%rcx), %eax
	cltd
	idivl	%esi
	incl	%eax
	movl	%eax, 128(%r10)
.LBB2_15:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%esi, %esi
	jle	.LBB2_18
# BB#16:                                # %.lr.ph.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movslq	120(%r10), %rax
	movq	104(%r10,%rax,8), %rax
	movq	(%rax,%r11,8), %rax
	addl	%esi, %esi
	leal	-1(%rcx), %edx
	movslq	%edx, %rdx
	movslq	%ecx, %rdi
	movslq	%esi, %rcx
	leaq	(%rax,%rdi,8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rdx,8), %rbx
	movq	%rbx, (%rsi,%rdi,8)
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_17
.LBB2_18:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_13 Depth=1
	incq	%r11
	addq	$96, %rbp
	cmpq	%r8, %r11
	jne	.LBB2_13
.LBB2_19:                               # %set_bottom_pointers.exit
	movl	$1, 124(%r15)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB2_20
.LBB2_8:
	movq	552(%r12), %rax
	movslq	120(%r15), %rcx
	movq	104(%r15,%rcx,8), %rsi
	leaq	100(%r15), %rbp
	movl	128(%r15), %ecx
	movl	%r13d, (%rsp)
	movq	%r12, %rdi
	movq	%rbp, %rdx
	movq	%rbx, %r8
	movq	%r14, %r9
	callq	*8(%rax)
	movl	100(%r15), %eax
	cmpl	128(%r15), %eax
	jb	.LBB2_35
# BB#9:
	movl	$0, 124(%r15)
	cmpl	%r13d, (%r14)
	jb	.LBB2_10
	jmp	.LBB2_35
.LBB2_7:                                # %._crit_edge
	leaq	100(%r15), %rdx
.LBB2_20:
	movq	552(%r12), %rax
	movslq	120(%r15), %rcx
	movq	104(%r15,%rcx,8), %rsi
	movl	128(%r15), %ecx
	movl	%r13d, (%rsp)
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r14, %r9
	callq	*8(%rax)
	movl	100(%r15), %eax
	cmpl	128(%r15), %eax
	jb	.LBB2_35
# BB#21:
	cmpl	$1, 132(%r15)
	jne	.LBB2_22
# BB#23:
	movl	396(%r12), %ecx
	movslq	48(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB2_34
# BB#24:                                # %.lr.ph63.i
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	536(%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	296(%r12), %r11
	leal	1(%rcx), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	leal	2(%rcx), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_31 Depth 2
	movl	36(%r11), %eax
	imull	12(%r11), %eax
	cltd
	idivl	%ecx
	testl	%eax, %eax
	jle	.LBB2_32
# BB#26:                                # %.lr.ph.i50
                                        #   in Loop: Header=BB2_25 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	104(%rdx), %rcx
	movq	112(%rdx), %rdx
	movq	(%rcx,%r15,8), %r10
	movq	(%rdx,%r15,8), %rbx
	movl	%eax, %edx
	imull	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%eax, %esi
	imull	32(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %r8
	movslq	%edx, %rdx
	movslq	%eax, %r12
	movl	%eax, %r13d
	testb	$1, %r13b
	jne	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_25 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %eax
	jne	.LBB2_30
	jmp	.LBB2_32
	.p2align	4, 0x90
.LBB2_28:                               #   in Loop: Header=BB2_25 Depth=1
	movq	(%r10,%rdx,8), %rsi
	leaq	(,%r12,8), %rdi
	movq	%r10, %rbp
	subq	%rdi, %rbp
	movq	%rsi, (%rbp)
	movq	(%rbx,%rdx,8), %rsi
	movq	%rbx, %rcx
	subq	%rdi, %rcx
	movq	%rsi, (%rcx)
	movq	(%r10), %rcx
	movq	%rcx, (%r10,%r8,8)
	movq	(%rbx), %rcx
	movq	%rcx, (%rbx,%r8,8)
	movl	$1, %ebp
	cmpl	$1, %eax
	je	.LBB2_32
.LBB2_30:                               # %.lr.ph.i50.new
                                        #   in Loop: Header=BB2_25 Depth=1
	leaq	8(%rbx,%r8,8), %rax
	leaq	8(%r10,%r8,8), %r8
	leaq	8(%rbx,%rdx,8), %r9
	leaq	8(%rbx), %r14
	leaq	8(%r10,%rdx,8), %rdi
	leaq	8(%r10), %rsi
	shlq	$3, %r12
	movq	%r14, %rcx
	subq	%r12, %rcx
	movq	%rsi, %rbx
	subq	%r12, %rbx
	.p2align	4, 0x90
.LBB2_31:                               #   Parent Loop BB2_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdi,%rbp,8), %rdx
	movq	%rdx, -8(%rbx,%rbp,8)
	movq	-8(%r9,%rbp,8), %rdx
	movq	%rdx, -8(%rcx,%rbp,8)
	movq	-8(%rsi,%rbp,8), %rdx
	movq	%rdx, -8(%r8,%rbp,8)
	movq	-8(%r14,%rbp,8), %rdx
	movq	%rdx, -8(%rax,%rbp,8)
	movq	(%rdi,%rbp,8), %rdx
	movq	%rdx, (%rbx,%rbp,8)
	movq	(%r9,%rbp,8), %rdx
	movq	%rdx, (%rcx,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movq	%rdx, (%r8,%rbp,8)
	movq	(%r14,%rbp,8), %rdx
	movq	%rdx, (%rax,%rbp,8)
	addq	$2, %rbp
	cmpq	%rbp, %r13
	jne	.LBB2_31
.LBB2_32:                               # %._crit_edge.i53
                                        #   in Loop: Header=BB2_25 Depth=1
	incq	%r15
	addq	$96, %r11
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB2_25
# BB#33:
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB2_34
.LBB2_22:                               # %.set_wraparound_pointers.exit_crit_edge
	movl	396(%r12), %ecx
.LBB2_34:                               # %set_wraparound_pointers.exit
	xorb	$1, 120(%r15)
	movl	$0, 96(%r15)
	leal	1(%rcx), %eax
	movl	%eax, 100(%r15)
	addl	$2, %ecx
	movl	%ecx, 128(%r15)
	movl	$2, 124(%r15)
.LBB2_35:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	process_data_context_main, .Lfunc_end2-process_data_context_main
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_data_simple_main,@function
process_data_simple_main:               # @process_data_simple_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rax
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	536(%rbx), %r14
	cmpl	$0, 96(%r14)
	je	.LBB3_2
# BB#1:                                 # %._crit_edge
	leaq	16(%r14), %r13
	jmp	.LBB3_4
.LBB3_2:
	movq	%rax, %rbp
	movq	544(%rbx), %rax
	leaq	16(%r14), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB3_6
# BB#3:
	movl	$1, 96(%r14)
	movq	%rbp, %rax
.LBB3_4:
	movl	396(%rbx), %ebp
	movq	552(%rbx), %r10
	leaq	100(%r14), %rdx
	movl	%r12d, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%ebp, %ecx
	movq	%r15, %r8
	movq	%rax, %r9
	callq	*8(%r10)
	cmpl	%ebp, 100(%r14)
	jb	.LBB3_6
# BB#5:
	movq	$0, 96(%r14)
.LBB3_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	process_data_simple_main, .Lfunc_end3-process_data_simple_main
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_data_crank_post,@function
process_data_crank_post:                # @process_data_crank_post
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movq	%rdx, %r9
	movq	%rsi, %rax
	movq	552(%rdi), %r10
	movl	%ecx, (%rsp)
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %r8
	callq	*8(%r10)
	popq	%rax
	retq
.Lfunc_end4:
	.size	process_data_crank_post, .Lfunc_end4-process_data_crank_post
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
