	.text
	.file	"Threads.bc"
	.globl	Thread_Create
	.p2align	4, 0x90
	.type	Thread_Create,@function
Thread_Create:                          # @Thread_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 112
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$0, 8(%rbx)
	leaq	8(%rsp), %rdi
	callq	pthread_attr_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_4
# BB#1:
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	pthread_attr_setdetachstate
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB0_4
# BB#2:
	leaq	8(%rsp), %r12
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	pthread_create
	movl	%eax, %ebp
	movq	%r12, %rdi
	callq	pthread_attr_destroy
	testl	%ebp, %ebp
	jne	.LBB0_4
# BB#3:
	movl	$1, 8(%rbx)
	xorl	%ebp, %ebp
.LBB0_4:
	movl	%ebp, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Thread_Create, .Lfunc_end0-Thread_Create
	.cfi_endproc

	.globl	Thread_Wait
	.p2align	4, 0x90
	.type	Thread_Wait,@function
Thread_Wait:                            # @Thread_Wait
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 8(%rbx)
	je	.LBB1_1
# BB#2:
	movq	(%rbx), %rdi
	leaq	8(%rsp), %rsi
	callq	pthread_join
	movl	$0, 8(%rbx)
	jmp	.LBB1_3
.LBB1_1:
	movl	$22, %eax
.LBB1_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Thread_Wait, .Lfunc_end1-Thread_Wait
	.cfi_endproc

	.globl	Thread_Close
	.p2align	4, 0x90
	.type	Thread_Close,@function
Thread_Close:                           # @Thread_Close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 8(%rbx)
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rdi
	callq	pthread_detach
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
.LBB2_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	Thread_Close, .Lfunc_end2-Thread_Close
	.cfi_endproc

	.globl	Event_Create
	.p2align	4, 0x90
	.type	Event_Create,@function
Event_Create:                           # @Event_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	movl	%ebp, 4(%rbx)
	xorl	%eax, %eax
	testl	%r14d, %r14d
	setne	%al
	movl	%eax, 8(%rbx)
	movl	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Event_Create, .Lfunc_end3-Event_Create
	.cfi_endproc

	.globl	Event_Set
	.p2align	4, 0x90
	.type	Event_Set,@function
Event_Set:                              # @Event_Set
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movl	$1, 8(%rbx)
	addq	$56, %rbx
	movq	%rbx, %rdi
	callq	pthread_cond_broadcast
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	Event_Set, .Lfunc_end4-Event_Set
	.cfi_endproc

	.globl	Event_Reset
	.p2align	4, 0x90
	.type	Event_Reset,@function
Event_Reset:                            # @Event_Reset
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movl	$0, 8(%rbx)
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	Event_Reset, .Lfunc_end5-Event_Reset
	.cfi_endproc

	.globl	Event_Wait
	.p2align	4, 0x90
	.type	Event_Wait,@function
Event_Wait:                             # @Event_Wait
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	16(%r15), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	cmpl	$0, 8(%r15)
	jne	.LBB6_3
# BB#1:                                 # %.lr.ph
	leaq	56(%r15), %rbx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	pthread_cond_wait
	cmpl	$0, 8(%r15)
	je	.LBB6_2
.LBB6_3:                                # %._crit_edge
	cmpl	$0, 4(%r15)
	jne	.LBB6_5
# BB#4:
	movl	$0, 8(%r15)
.LBB6_5:
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	Event_Wait, .Lfunc_end6-Event_Wait
	.cfi_endproc

	.globl	Event_Close
	.p2align	4, 0x90
	.type	Event_Close,@function
Event_Close:                            # @Event_Close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, (%rbx)
	je	.LBB7_2
# BB#1:
	movl	$0, (%rbx)
	leaq	16(%rbx), %rdi
	callq	pthread_mutex_destroy
	addq	$56, %rbx
	movq	%rbx, %rdi
	callq	pthread_cond_destroy
.LBB7_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	Event_Close, .Lfunc_end7-Event_Close
	.cfi_endproc

	.globl	Semaphore_Create
	.p2align	4, 0x90
	.type	Semaphore_Create,@function
Semaphore_Create:                       # @Semaphore_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	movl	%ebp, 4(%rbx)
	movl	%r14d, 8(%rbx)
	movl	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Semaphore_Create, .Lfunc_end8-Semaphore_Create
	.cfi_endproc

	.globl	Semaphore_ReleaseN
	.p2align	4, 0x90
	.type	Semaphore_ReleaseN,@function
Semaphore_ReleaseN:                     # @Semaphore_ReleaseN
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$22, %r15d
	testl	%ebp, %ebp
	je	.LBB9_4
# BB#1:
	leaq	16(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	addl	4(%rbx), %ebp
	cmpl	8(%rbx), %ebp
	jbe	.LBB9_3
# BB#2:
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	jmp	.LBB9_4
.LBB9_3:
	movl	%ebp, 4(%rbx)
	addq	$56, %rbx
	movq	%rbx, %rdi
	callq	pthread_cond_broadcast
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	xorl	%r15d, %r15d
.LBB9_4:
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	Semaphore_ReleaseN, .Lfunc_end9-Semaphore_ReleaseN
	.cfi_endproc

	.globl	Semaphore_Wait
	.p2align	4, 0x90
	.type	Semaphore_Wait,@function
Semaphore_Wait:                         # @Semaphore_Wait
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	16(%r15), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movl	4(%r15), %eax
	testl	%eax, %eax
	jne	.LBB10_3
# BB#1:                                 # %.lr.ph
	leaq	56(%r15), %rbx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	pthread_cond_wait
	movl	4(%r15), %eax
	testl	%eax, %eax
	je	.LBB10_2
.LBB10_3:                               # %._crit_edge
	decl	%eax
	movl	%eax, 4(%r15)
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	Semaphore_Wait, .Lfunc_end10-Semaphore_Wait
	.cfi_endproc

	.globl	Semaphore_Close
	.p2align	4, 0x90
	.type	Semaphore_Close,@function
Semaphore_Close:                        # @Semaphore_Close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
.Lcfi62:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, (%rbx)
	je	.LBB11_2
# BB#1:
	movl	$0, (%rbx)
	leaq	16(%rbx), %rdi
	callq	pthread_mutex_destroy
	addq	$56, %rbx
	movq	%rbx, %rdi
	callq	pthread_cond_destroy
.LBB11_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	Semaphore_Close, .Lfunc_end11-Semaphore_Close
	.cfi_endproc

	.globl	CriticalSection_Init
	.p2align	4, 0x90
	.type	CriticalSection_Init,@function
CriticalSection_Init:                   # @CriticalSection_Init
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	pthread_mutex_init      # TAILCALL
.Lfunc_end12:
	.size	CriticalSection_Init, .Lfunc_end12-CriticalSection_Init
	.cfi_endproc

	.globl	ManualResetEvent_Create
	.p2align	4, 0x90
	.type	ManualResetEvent_Create,@function
ManualResetEvent_Create:                # @ManualResetEvent_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	xorl	%eax, %eax
	testl	%ebp, %ebp
	setne	%al
	movl	%eax, 8(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	ManualResetEvent_Create, .Lfunc_end13-ManualResetEvent_Create
	.cfi_endproc

	.globl	ManualResetEvent_CreateNotSignaled
	.p2align	4, 0x90
	.type	ManualResetEvent_CreateNotSignaled,@function
ManualResetEvent_CreateNotSignaled:     # @ManualResetEvent_CreateNotSignaled
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 16
.Lcfi69:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	movl	$0, 8(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end14:
	.size	ManualResetEvent_CreateNotSignaled, .Lfunc_end14-ManualResetEvent_CreateNotSignaled
	.cfi_endproc

	.globl	AutoResetEvent_Create
	.p2align	4, 0x90
	.type	AutoResetEvent_Create,@function
AutoResetEvent_Create:                  # @AutoResetEvent_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	xorl	%eax, %eax
	testl	%ebp, %ebp
	setne	%al
	movl	%eax, 8(%rbx)
	movq	$1, (%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	AutoResetEvent_Create, .Lfunc_end15-AutoResetEvent_Create
	.cfi_endproc

	.globl	AutoResetEvent_CreateNotSignaled
	.p2align	4, 0x90
	.type	AutoResetEvent_CreateNotSignaled,@function
AutoResetEvent_CreateNotSignaled:       # @AutoResetEvent_CreateNotSignaled
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 16
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_mutex_init
	leaq	56(%rbx), %rdi
	xorl	%esi, %esi
	callq	pthread_cond_init
	movl	$0, 8(%rbx)
	movq	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end16:
	.size	AutoResetEvent_CreateNotSignaled, .Lfunc_end16-AutoResetEvent_CreateNotSignaled
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
