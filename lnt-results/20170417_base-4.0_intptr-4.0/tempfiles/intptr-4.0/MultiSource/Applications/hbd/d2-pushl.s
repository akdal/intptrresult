	.text
	.file	"d2-pushl.bc"
	.globl	_Z9pushlocalP9Classfile
	.p2align	4, 0x90
	.type	_Z9pushlocalP9Classfile,@function
_Z9pushlocalP9Classfile:                # @_Z9pushlocalP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r12d
	movl	ch(%rip), %eax
	cmpl	$25, %eax
	jg	.LBB0_9
# BB#1:
	leal	1(%r12), %ecx
	movl	%ecx, currpc(%rip)
	decl	bufflength(%rip)
	movq	inbuff(%rip), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, inbuff(%rip)
	movsbl	(%rcx), %ebp
	addl	$-21, %eax
	cmpl	$4, %eax
	ja	.LBB0_2
# BB#3:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_4:
	movl	$4, %r13d
	jmp	.LBB0_19
.LBB0_9:
	cmpl	$29, %eax
	jg	.LBB0_11
# BB#10:
	addl	$-26, %eax
	movl	$4, %r13d
	jmp	.LBB0_18
.LBB0_11:
	cmpl	$33, %eax
	jg	.LBB0_13
# BB#12:
	addl	$-30, %eax
	movl	$5, %r13d
	jmp	.LBB0_18
.LBB0_13:
	cmpl	$37, %eax
	jg	.LBB0_15
# BB#14:
	addl	$-34, %eax
	movl	$6, %r13d
	jmp	.LBB0_18
.LBB0_2:
	xorl	%r13d, %r13d
	jmp	.LBB0_19
.LBB0_5:
	movl	$5, %r13d
	jmp	.LBB0_19
.LBB0_6:
	movl	$6, %r13d
	jmp	.LBB0_19
.LBB0_7:
	movl	$7, %r13d
	jmp	.LBB0_19
.LBB0_8:
	movl	$8, %r13d
	jmp	.LBB0_19
.LBB0_15:
	cmpl	$41, %eax
	jg	.LBB0_17
# BB#16:
	addl	$-38, %eax
	movl	$7, %r13d
	jmp	.LBB0_18
.LBB0_17:
	addl	$-42, %eax
	movl	$8, %r13d
.LBB0_18:
	movl	%eax, %ebp
.LBB0_19:
	movq	miptr(%rip), %rax
	movq	88(%rax), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_24
# BB#20:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#21:                                # %.noexc
	decl	%r12d
	movq	%r15, (%rbx)
	movl	%r13d, 8(%rbx)
	movl	$3, 12(%rbx)
	movl	%ebp, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%r12d, 16(%r14)
	movl	%r12d, 12(%r14)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
.Ltmp3:
# BB#22:
	movl	$0, (%rax)
	movl	$1, 4(%rax)
	movl	%r13d, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movq	stkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	%r14, (%rax)
	xorl	%ecx, %ecx
.LBB0_25:
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %ecx
	jmp	.LBB0_25
.LBB0_23:
.Ltmp4:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z9pushlocalP9Classfile, .Lfunc_end0-_Z9pushlocalP9Classfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_6
	.quad	.LBB0_7
	.quad	.LBB0_8
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp3      #   Call between .Ltmp3 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error in code: local used before defined.\n"
	.size	.L.str, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
