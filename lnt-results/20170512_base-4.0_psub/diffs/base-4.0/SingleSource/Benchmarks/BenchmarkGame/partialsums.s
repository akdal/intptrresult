	.text
	.file	"partialsums.bc"
	.globl	make_vec
	.p2align	4, 0x90
	.type	make_vec,@function
make_vec:                               # @make_vec
	.cfi_startproc
# BB#0:
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	retq
.Lfunc_end0:
	.size	make_vec, .Lfunc_end0-make_vec
	.cfi_endproc

	.globl	sum_vec
	.p2align	4, 0x90
	.type	sum_vec,@function
sum_vec:                                # @sum_vec
	.cfi_startproc
# BB#0:
	movaps	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	retq
.Lfunc_end1:
	.size	sum_vec, .Lfunc_end1-sum_vec
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
.LCPI2_1:
	.quad	-4616189618054758400    # double -1
.LCPI2_2:
	.quad	4604180019048437077     # double 0.66666666666666663
.LCPI2_8:
	.quad	4702623120467427328     # double 2.5E+6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_3:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI2_4:
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
.LCPI2_5:
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
.LCPI2_6:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
.LCPI2_7:
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 128
.Lcfi2:
	.cfi_offset %rbx, -16
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movl	$2500000, %ebx          # imm = 0x2625A0
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI2_1(%rip), %xmm1
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_3
# BB#2:                                 # %call.sqrt
                                        #   in Loop: Header=BB2_1 Depth=1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB2_3:                                # %.split
                                        #   in Loop: Header=BB2_1 Depth=1
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 96(%rsp)         # 8-byte Spill
	callq	sin
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movsd	96(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm1
	movsd	80(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm1
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	.LCPI2_3(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00]
	movapd	%xmm3, %xmm1
	divpd	%xmm2, %xmm1
	movapd	64(%rsp), %xmm2         # 16-byte Reload
	addpd	%xmm1, %xmm2
	movapd	%xmm2, 64(%rsp)         # 16-byte Spill
	movapd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm0
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	decl	%ebx
	jne	.LBB2_1
# BB#4:                                 # %.preheader.preheader
	movapd	.LCPI2_4(%rip), %xmm0   # xmm0 = [1.000000e+00,2.000000e+00]
	xorpd	%xmm12, %xmm12
	movapd	.LCPI2_5(%rip), %xmm11  # xmm11 = [1.000000e+00,-1.000000e+00]
	movapd	.LCPI2_6(%rip), %xmm8   # xmm8 = [-1.000000e+00,-1.000000e+00]
	movapd	.LCPI2_7(%rip), %xmm9   # xmm9 = [2.000000e+00,2.000000e+00]
	movsd	.LCPI2_8(%rip), %xmm10  # xmm10 = mem[0],zero
	xorpd	%xmm4, %xmm4
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	xorpd	%xmm7, %xmm7
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm5
	addpd	%xmm3, %xmm5
	mulpd	%xmm0, %xmm5
	movapd	%xmm3, %xmm6
	divpd	%xmm5, %xmm6
	addpd	%xmm6, %xmm1
	movapd	%xmm3, %xmm5
	divpd	%xmm0, %xmm5
	addpd	%xmm5, %xmm12
	movapd	%xmm0, %xmm5
	mulpd	%xmm5, %xmm5
	movapd	%xmm3, %xmm6
	divpd	%xmm5, %xmm6
	addpd	%xmm6, %xmm4
	movapd	%xmm11, %xmm5
	divpd	%xmm0, %xmm5
	addpd	%xmm5, %xmm2
	movapd	%xmm0, %xmm5
	addpd	%xmm5, %xmm5
	addpd	%xmm8, %xmm5
	movapd	%xmm11, %xmm6
	divpd	%xmm5, %xmm6
	addpd	%xmm6, %xmm7
	addpd	%xmm9, %xmm0
	ucomisd	%xmm0, %xmm10
	jae	.LBB2_5
# BB#6:
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	movb	$1, %al
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm7, (%rsp)           # 16-byte Spill
	movapd	%xmm2, 48(%rsp)         # 16-byte Spill
	movapd	%xmm4, 32(%rsp)         # 16-byte Spill
	movapd	%xmm12, 96(%rsp)        # 16-byte Spill
	movapd	%xmm1, 80(%rsp)         # 16-byte Spill
	callq	printf
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	movb	$1, %al
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	callq	printf
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	printf
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movl	$.L.str, %edi
	movl	$.L.str.4, %esi
	movb	$1, %al
	callq	printf
	movl	$.L.str, %edi
	movl	$.L.str.5, %esi
	movb	$1, %al
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movl	$.L.str.6, %esi
	movb	$1, %al
	callq	printf
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movl	$.L.str.7, %esi
	movb	$1, %al
	callq	printf
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movl	$.L.str.8, %esi
	movb	$1, %al
	callq	printf
	movaps	(%rsp), %xmm1           # 16-byte Reload
	movaps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movl	$.L.str.9, %esi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.9f\t%s\n"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"(2/3)^k"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"k^-0.5"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1/k(k+1)"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Flint Hills"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Cookson Hills"
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Harmonic"
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Riemann Zeta"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Alternating Harmonic"
	.size	.L.str.8, 21

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Gregory"
	.size	.L.str.9, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
