	.text
	.file	"intersection.bc"
	.globl	_ZN16intersection_4x410connectNinEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x410connectNinEP7roadletS1_,@function
_ZN16intersection_4x410connectNinEP7roadletS1_: # @_ZN16intersection_4x410connectNinEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	S(%rip), %eax
	movq	16(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z11green_lightP20intersection_roadletP7vehicle9direction, %r8d
	movq	%rsi, %rdi
	movl	%eax, %esi
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	8(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction, %r8d
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end0:
	.size	_ZN16intersection_4x410connectNinEP7roadletS1_, .Lfunc_end0-_ZN16intersection_4x410connectNinEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x410connectEinEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x410connectEinEP7roadletS1_,@function
_ZN16intersection_4x410connectEinEP7roadletS1_: # @_ZN16intersection_4x410connectEinEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	W(%rip), %eax
	movq	64(%rbx), %rdx
	movl	E(%rip), %ecx
	movl	$_Z11green_lightP20intersection_roadletP7vehicle9direction, %r8d
	movq	%rsi, %rdi
	movl	%eax, %esi
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	W(%rip), %esi
	movq	32(%rbx), %rdx
	movl	E(%rip), %ecx
	movl	$_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction, %r8d
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end1:
	.size	_ZN16intersection_4x410connectEinEP7roadletS1_, .Lfunc_end1-_ZN16intersection_4x410connectEinEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x410connectSinEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x410connectSinEP7roadletS1_,@function
_ZN16intersection_4x410connectSinEP7roadletS1_: # @_ZN16intersection_4x410connectSinEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	N(%rip), %eax
	movq	120(%rbx), %rdx
	movl	S(%rip), %ecx
	movl	$_Z11green_lightP20intersection_roadletP7vehicle9direction, %r8d
	movq	%rsi, %rdi
	movl	%eax, %esi
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	N(%rip), %esi
	movq	128(%rbx), %rdx
	movl	S(%rip), %ecx
	movl	$_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction, %r8d
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end2:
	.size	_ZN16intersection_4x410connectSinEP7roadletS1_, .Lfunc_end2-_ZN16intersection_4x410connectSinEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x410connectWinEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x410connectWinEP7roadletS1_,@function
_ZN16intersection_4x410connectWinEP7roadletS1_: # @_ZN16intersection_4x410connectWinEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	E(%rip), %eax
	movq	72(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z11green_lightP20intersection_roadletP7vehicle9direction, %r8d
	movq	%rsi, %rdi
	movl	%eax, %esi
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	104(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z24green_OR_plan_rightONredP20intersection_roadletP7vehicle9direction, %r8d
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end3:
	.size	_ZN16intersection_4x410connectWinEP7roadletS1_, .Lfunc_end3-_ZN16intersection_4x410connectWinEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x411connectNoutEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x411connectNoutEP7roadletS1_,@function
_ZN16intersection_4x411connectNoutEP7roadletS1_: # @_ZN16intersection_4x411connectNoutEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	movq	%rax, %rdx
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	32(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z15strait_or_rightP7roadletP7vehicle9direction, %r8d
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end4:
	.size	_ZN16intersection_4x411connectNoutEP7roadletS1_, .Lfunc_end4-_ZN16intersection_4x411connectNoutEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x411connectEoutEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x411connectEoutEP7roadletS1_,@function
_ZN16intersection_4x411connectEoutEP7roadletS1_: # @_ZN16intersection_4x411connectEoutEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	96(%rbx), %rdi
	movl	E(%rip), %esi
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	movq	%rax, %rdx
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	128(%rbx), %rdi
	movl	E(%rip), %esi
	movl	W(%rip), %ecx
	movl	$_Z15strait_or_rightP7roadletP7vehicle9direction, %r8d
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end5:
	.size	_ZN16intersection_4x411connectEoutEP7roadletS1_, .Lfunc_end5-_ZN16intersection_4x411connectEoutEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x411connectSoutEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x411connectSoutEP7roadletS1_,@function
_ZN16intersection_4x411connectSoutEP7roadletS1_: # @_ZN16intersection_4x411connectSoutEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	112(%rbx), %rdi
	movl	S(%rip), %esi
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	movq	%rax, %rdx
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	104(%rbx), %rdi
	movl	S(%rip), %esi
	movl	N(%rip), %ecx
	movl	$_Z15strait_or_rightP7roadletP7vehicle9direction, %r8d
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end6:
	.size	_ZN16intersection_4x411connectSoutEP7roadletS1_, .Lfunc_end6-_ZN16intersection_4x411connectSoutEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x411connectWoutEP7roadletS1_
	.p2align	4, 0x90
	.type	_ZN16intersection_4x411connectWoutEP7roadletS1_,@function
_ZN16intersection_4x411connectWoutEP7roadletS1_: # @_ZN16intersection_4x411connectWoutEP7roadletS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	movq	%rax, %rdx
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	8(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z15strait_or_rightP7roadletP7vehicle9direction, %r8d
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E # TAILCALL
.Lfunc_end7:
	.size	_ZN16intersection_4x411connectWoutEP7roadletS1_, .Lfunc_end7-_ZN16intersection_4x411connectWoutEP7roadletS1_
	.cfi_endproc

	.globl	_ZN16intersection_4x4C2EPKc
	.p2align	4, 0x90
	.type	_ZN16intersection_4x4C2EPKc,@function
_ZN16intersection_4x4C2EPKc:            # @_ZN16intersection_4x4C2EPKc
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 144
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	$_ZTV5light+16, (%r15)
.Ltmp0:
	movl	$4, %esi
	movl	$2, %edx
	movl	$5, %ecx
	movl	$2, %r8d
	movq	%r15, %rdi
	callq	_ZN5light4initEiiii
.Ltmp1:
# BB#1:                                 # %_ZN5lightC2Ev.exit
	movq	%r15, (%rbx)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	leaq	32(%rbx), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader82
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	strlen
	leaq	7(%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	sprintf
	movl	$152, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp4:
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, 144(%rbx)
	movq	%rbx, -24(%r14)
	movq	%r12, %rdi
	callq	strlen
	leaq	7(%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str, %esi
	movl	$1, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	sprintf
	movl	$152, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp5:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp6:
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, 144(%rbx)
	movq	%rbx, -16(%r14)
	movq	%r12, %rdi
	callq	strlen
	leaq	7(%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str, %esi
	movl	$2, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	sprintf
	movl	$152, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp7:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp8:
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, 144(%rbx)
	movq	%rbx, -8(%r14)
	movq	%r12, %rdi
	callq	strlen
	leaq	7(%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str, %esi
	movl	$3, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	sprintf
	movl	$152, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp9:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN7roadlet4initEPKc
.Ltmp10:
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, 144(%rbx)
	movq	%rbx, (%r14)
	incq	%r13
	addq	$32, %r14
	cmpq	$4, %r13
	jl	.LBB8_2
# BB#7:                                 # %.preheader81.preheader
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	96(%rbx), %rdx
	movq	128(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	88(%rbx), %rdx
	movq	120(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	64(%rbx), %rdx
	movq	96(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	56(%rbx), %rdx
	movq	88(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	32(%rbx), %rdx
	movq	64(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	24(%rbx), %rdx
	movq	56(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	8(%rbx), %rdi
	movq	40(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	16(%rbx), %rdi
	movq	48(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	40(%rbx), %rdi
	movq	72(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	48(%rbx), %rdi
	movq	80(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	72(%rbx), %rdi
	movq	104(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	80(%rbx), %rdi
	movq	112(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	72(%rbx), %rdi
	movq	80(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	104(%rbx), %rdi
	movq	112(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	80(%rbx), %rdi
	movq	88(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	112(%rbx), %rdi
	movq	120(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	88(%rbx), %rdi
	movq	96(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	120(%rbx), %rdi
	movq	128(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	56(%rbx), %rdx
	movq	64(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z6straitP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	56(%rbx), %rdx
	movq	88(%rbx), %rdi
	movl	N(%rip), %esi
	movl	S(%rip), %ecx
	movl	$_Z14strait_or_leftP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rdi
	movl	W(%rip), %esi
	movl	E(%rip), %ecx
	movl	$_Z14strait_or_leftP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	S(%rip), %esi
	movq	48(%rbx), %rdi
	movq	80(%rbx), %rdx
	movl	N(%rip), %ecx
	movl	$_Z14strait_or_leftP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	E(%rip), %esi
	movq	80(%rbx), %rdi
	movq	88(%rbx), %rdx
	movl	W(%rip), %ecx
	movl	$_Z14strait_or_leftP7roadletP7vehicle9direction, %r8d
	callq	_Z7connectP7roadlet9directionS0_S1_PFS0_S0_P7vehicleS1_E
	movl	$-1, 84(%rsp)
	movq	$.L.str.1, 64(%rsp)
	movq	$100, 72(%rsp)
	movl	N(%rip), %eax
	movl	%eax, 80(%rsp)
	movq	24(%rbx), %rax
	movq	%rax, 56(%rsp)
	leaq	56(%rsp), %rcx
	movq	%rcx, 8(%rax)
	movups	56(%rsp), %xmm0
	movups	72(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$_ZSt4cout, %edi
	callq	_ZlsRSo3car
	movb	$10, 47(%rsp)
	leaq	47(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_9:
.Ltmp2:
	movq	%rax, %r14
	movq	%r15, %rdi
	jmp	.LBB8_10
.LBB8_8:
.Ltmp11:
	movq	%rax, %r14
	movq	%rbx, %rdi
.LBB8_10:
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN16intersection_4x4C2EPKc, .Lfunc_end8-_ZN16intersection_4x4C2EPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 10 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end8-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5light10next_stateEv,"axG",@progbits,_ZN5light10next_stateEv,comdat
	.weak	_ZN5light10next_stateEv
	.p2align	4, 0x90
	.type	_ZN5light10next_stateEv,@function
_ZN5light10next_stateEv:                # @_ZN5light10next_stateEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	leal	1(%rcx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	leal	1(%rcx,%rdx), %ecx
	andl	$-4, %ecx
	subl	%ecx, %eax
	retq
.Lfunc_end9:
	.size	_ZN5light10next_stateEv, .Lfunc_end9-_ZN5light10next_stateEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_intersection.ii,@function
_GLOBAL__sub_I_intersection.ii:         # @_GLOBAL__sub_I_intersection.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end10:
	.size	_GLOBAL__sub_I_intersection.ii, .Lfunc_end10-_GLOBAL__sub_I_intersection.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s %d %d"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"blocker"
	.size	.L.str.1, 8

	.type	_ZTS5light,@object      # @_ZTS5light
	.section	.rodata._ZTS5light,"aG",@progbits,_ZTS5light,comdat
	.weak	_ZTS5light
_ZTS5light:
	.asciz	"5light"
	.size	_ZTS5light, 7

	.type	_ZTI5light,@object      # @_ZTI5light
	.section	.rodata._ZTI5light,"aG",@progbits,_ZTI5light,comdat
	.weak	_ZTI5light
	.p2align	3
_ZTI5light:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS5light
	.size	_ZTI5light, 16

	.type	_ZTV5light,@object      # @_ZTV5light
	.section	.rodata._ZTV5light,"aG",@progbits,_ZTV5light,comdat
	.weak	_ZTV5light
	.p2align	3
_ZTV5light:
	.quad	0
	.quad	_ZTI5light
	.quad	_ZN5light10next_stateEv
	.size	_ZTV5light, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_intersection.ii

	.globl	_ZN16intersection_4x4C1EPKc
	.type	_ZN16intersection_4x4C1EPKc,@function
_ZN16intersection_4x4C1EPKc = _ZN16intersection_4x4C2EPKc
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
