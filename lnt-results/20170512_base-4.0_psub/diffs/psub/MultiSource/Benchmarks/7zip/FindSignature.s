	.text
	.file	"FindSignature.bc"
	.globl	_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy
	.p2align	4, 0x90
	.type	_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy,@function
_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy: # @_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %r14
	movl	%edx, %ebx
	movq	%rsi, %r12
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	$0, (%r13)
	movl	%ebx, %ebp
	xorl	%r15d, %r15d
	testl	%ebx, %ebx
	je	.LBB0_3
# BB#1:
.Ltmp0:
	movq	%rbp, %rdi
	callq	_Znam
.Ltmp1:
# BB#2:
	movq	%rax, %r15
.LBB0_3:                                # %_ZN7CBufferIhE11SetCapacityEm.exit
.Ltmp2:
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
.Ltmp3:
# BB#4:
	testl	%eax, %eax
	jne	.LBB0_46
# BB#5:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_6
# BB#7:
.Ltmp5:
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	$65536, %edi            # imm = 0x10000
	callq	_Znam
	movq	%rax, %r12
.Ltmp6:
# BB#8:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit107
	leal	-1(%rbx), %edx
	leaq	1(%r15), %rsi
	movq	%r12, %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	memcpy
	movq	$1, (%r13)
	testq	%r14, %r14
	je	.LBB0_9
# BB#15:                                # %_ZN7CBufferIhE11SetCapacityEm.exit107.split.preheader
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	cmpq	$0, (%r14)
	je	.LBB0_45
# BB#16:                                # %.preheader.preheader.preheader
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	negl	%eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
.LBB0_17:                               # %.preheader.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
                                        #     Child Loop BB0_38 Depth 2
                                        #       Child Loop BB0_39 Depth 3
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_18:                               # %.preheader
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %r14d
	movl	$65536, %edx            # imm = 0x10000
	subl	%r14d, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%r14d, %esi
	addq	%r12, %rsi
.Ltmp8:
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
.Ltmp9:
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	testl	%eax, %eax
	jne	.LBB0_36
# BB#20:                                #   in Loop: Header=BB0_18 Depth=2
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_45
# BB#21:                                # %.thread
                                        #   in Loop: Header=BB0_18 Depth=2
	leal	(%rax,%r14), %edx
	movl	%edx, %ebp
	subl	%ebx, %ebp
	jb	.LBB0_18
# BB#22:                                #   in Loop: Header=BB0_17 Depth=1
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	incl	%ebp
	je	.LBB0_44
# BB#23:                                # %.lr.ph
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movb	(%rcx), %r15b
	addl	76(%rsp), %r14d         # 4-byte Folded Reload
	addl	%eax, %r14d
	xorl	%ebx, %ebx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_38:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_39 Depth 3
	addl	$-2, %ebx
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	2(%rbx), %r13d
	incl	%ebx
	cmpl	%ebp, %r13d
	jae	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_39 Depth=3
	cmpb	%r15b, (%r12,%r13)
	jne	.LBB0_39
.LBB0_41:                               # %.critedge
                                        #   in Loop: Header=BB0_38 Depth=2
	cmpl	%ebx, %r14d
	je	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_38 Depth=2
	leaq	(%r12,%r13), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_43
# BB#37:                                #   in Loop: Header=BB0_38 Depth=2
	addl	$2, %ebx
	cmpl	%ebp, %ebx
	movq	16(%rsp), %rdx          # 8-byte Reload
	jb	.LBB0_38
.LBB0_44:                               # %.thread147
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	%ebp, %ebx
	leaq	(%r12,%rbx), %rsi
	movq	24(%rsp), %r13          # 8-byte Reload
	addq	(%r13), %rbx
	movq	%rbx, (%r13)
	subl	%ebp, %edx
	movq	%r12, %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	memmove
	movq	80(%rsp), %r14          # 8-byte Reload
	cmpq	(%r14), %rbx
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	jbe	.LBB0_17
	jmp	.LBB0_45
.LBB0_6:
	xorl	%eax, %eax
	testq	%r15, %r15
	jne	.LBB0_47
	jmp	.LBB0_48
.LBB0_9:                                # %_ZN7CBufferIhE11SetCapacityEm.exit107.split.us.preheader
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	negl	%eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_10
.LBB0_26:                               # %.thread147.us
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	%ebx, %esi
	movq	24(%rsp), %r13          # 8-byte Reload
	addq	%rsi, (%r13)
	subl	%ebx, %edx
	addq	%r12, %rsi
	movq	%r12, %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	memmove
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB0_10:                               # %_ZN7CBufferIhE11SetCapacityEm.exit107.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_28 Depth 2
                                        #       Child Loop BB0_29 Depth 3
	leaq	12(%rsp), %rbp
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %r14d
	movl	$65536, %edx            # imm = 0x10000
	subl	%r14d, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%r14d, %esi
	addq	%r12, %rsi
.Ltmp11:
	movq	%rbp, %rcx
	callq	*40(%rax)
.Ltmp12:
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	testl	%eax, %eax
	jne	.LBB0_36
# BB#13:                                #   in Loop: Header=BB0_11 Depth=2
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_14
# BB#24:                                # %.thread.us
                                        #   in Loop: Header=BB0_11 Depth=2
	leal	(%rax,%r14), %edx
	movl	%edx, %ebx
	subl	56(%rsp), %ebx          # 4-byte Folded Reload
	jb	.LBB0_11
# BB#25:                                #   in Loop: Header=BB0_10 Depth=1
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	incl	%ebx
	je	.LBB0_26
# BB#33:                                # %.lr.ph.us
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movb	(%rcx), %bpl
	addl	8(%rsp), %r14d          # 4-byte Folded Reload
	addl	%eax, %r14d
	xorl	%r15d, %r15d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_28:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_29 Depth 3
	addl	$-2, %r15d
	.p2align	4, 0x90
.LBB0_29:                               #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	2(%r15), %r13d
	incl	%r15d
	cmpl	%ebx, %r13d
	jae	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_29 Depth=3
	cmpb	%bpl, (%r12,%r13)
	jne	.LBB0_29
.LBB0_31:                               # %.critedge.us
                                        #   in Loop: Header=BB0_28 Depth=2
	cmpl	%r15d, %r14d
	je	.LBB0_26
# BB#32:                                #   in Loop: Header=BB0_28 Depth=2
	leaq	(%r12,%r13), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_43
# BB#27:                                #   in Loop: Header=BB0_28 Depth=2
	addl	$2, %r15d
	cmpl	%ebx, %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jb	.LBB0_28
	jmp	.LBB0_26
.LBB0_36:                               # %.us-lcssa165.us
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_45
.LBB0_14:
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB0_45
.LBB0_43:                               # %.us-lcssa166.us
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	%r13, (%rax)
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB0_45:                               # %_ZN7CBufferIhED2Ev.exit108
	movq	%r12, %rdi
	callq	_ZdaPv
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB0_46:
	testq	%r15, %r15
	je	.LBB0_48
.LBB0_47:
	movq	%r15, %rdi
	movl	%eax, %ebx
	callq	_ZdaPv
	movl	%ebx, %eax
.LBB0_48:                               # %_ZN7CBufferIhED2Ev.exit103
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_35:                               # %.thread149
.Ltmp7:
	jmp	.LBB0_52
.LBB0_34:                               # %.us-lcssa.us
.Ltmp13:
	jmp	.LBB0_50
.LBB0_49:                               # %.us-lcssa
.Ltmp10:
.LBB0_50:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdaPv
	testq	%r15, %r15
	jne	.LBB0_54
	jmp	.LBB0_55
.LBB0_51:
.Ltmp4:
.LBB0_52:                               # %_ZN7CBufferIhED2Ev.exit104
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB0_55
.LBB0_54:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_55:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy, .Lfunc_end0-_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
