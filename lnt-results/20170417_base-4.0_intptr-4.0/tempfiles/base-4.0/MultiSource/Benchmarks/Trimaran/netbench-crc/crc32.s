	.text
	.file	"crc32.bc"
	.globl	gen_crc_table
	.p2align	4, 0x90
	.type	gen_crc_table,@function
gen_crc_table:                          # @gen_crc_table
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	xorq	$79764919, %rdx         # imm = 0x4C11DB7
	testb	%cl, %cl
	cmovnsq	%rax, %rdx
	movq	%rdx, %rsi
	addq	%rsi, %rsi
	movq	%rsi, %rdi
	xorq	$79764919, %rdi         # imm = 0x4C11DB7
	testl	%edx, %edx
	cmovnsq	%rsi, %rdi
	movq	%rdi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edi, %edi
	cmovnsq	%rdx, %rsi
	movq	%rsi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rdi
	xorq	$79764919, %rdi         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rdx, %rdi
	movq	%rdi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edi, %edi
	cmovnsq	%rdx, %rsi
	movq	%rsi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rdi
	xorq	$79764919, %rdi         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rdx, %rdi
	movq	%rdi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edi, %edi
	cmovnsq	%rdx, %rsi
	movq	%rsi, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rdi
	xorq	$79764919, %rdi         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rdx, %rdi
	movq	%rdi, crc_table(,%rcx,8)
	incq	%rcx
	addq	$33554432, %rax         # imm = 0x2000000
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB0_1
# BB#2:
	retq
.Lfunc_end0:
	.size	gen_crc_table, .Lfunc_end0-gen_crc_table
	.cfi_endproc

	.globl	update_crc
	.p2align	4, 0x90
	.type	update_crc,@function
update_crc:                             # @update_crc
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	jle	.LBB1_7
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %dl
	jne	.LBB1_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %edx
	jne	.LBB1_5
	jmp	.LBB1_7
.LBB1_3:                                # %.lr.ph.prol
	movl	%edi, %eax
	shrl	$24, %eax
	movzbl	(%rsi), %ecx
	incq	%rsi
	xorq	%rax, %rcx
	shlq	$8, %rdi
	xorq	crc_table(,%rcx,8), %rdi
	movl	$1, %eax
	cmpl	$1, %edx
	je	.LBB1_7
.LBB1_5:                                # %.lr.ph.preheader.new
	subl	%eax, %edx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %eax
	shrl	$24, %eax
	movzbl	(%rsi), %ecx
	xorq	%rax, %rcx
	shlq	$8, %rdi
	xorq	crc_table(,%rcx,8), %rdi
	movl	%edi, %eax
	shrl	$24, %eax
	movzbl	1(%rsi), %ecx
	xorq	%rax, %rcx
	shlq	$8, %rdi
	xorq	crc_table(,%rcx,8), %rdi
	addq	$2, %rsi
	addl	$-2, %edx
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	update_crc, .Lfunc_end1-update_crc
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB2_15
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	xorq	$79764919, %rcx         # imm = 0x4C11DB7
	testb	%al, %al
	cmovnsq	%rbx, %rcx
	movq	%rcx, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%ecx, %ecx
	cmovnsq	%rdx, %rsi
	movq	%rsi, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rdx
	xorq	$79764919, %rdx         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rcx, %rdx
	movq	%rdx, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edx, %edx
	cmovnsq	%rcx, %rsi
	movq	%rsi, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rdx
	xorq	$79764919, %rdx         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rcx, %rdx
	movq	%rdx, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edx, %edx
	cmovnsq	%rcx, %rsi
	movq	%rsi, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rdx
	xorq	$79764919, %rdx         # imm = 0x4C11DB7
	testl	%esi, %esi
	cmovnsq	%rcx, %rdx
	movq	%rdx, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %rsi
	xorq	$79764919, %rsi         # imm = 0x4C11DB7
	testl	%edx, %edx
	cmovnsq	%rcx, %rsi
	movq	%rsi, crc_table(,%rax,8)
	incq	%rax
	addq	$33554432, %rbx         # imm = 0x2000000
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB2_2
# BB#3:                                 # %gen_crc_table.exit.preheader
	testl	%r14d, %r14d
	jle	.LBB2_4
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	xorl	%eax, %eax
	movl	%r15d, %edi
	callq	get_next_packet
	movq	%rax, %rbp
	movl	%r15d, %edi
	callq	packet_size
	testl	%eax, %eax
	movl	$0, %ebx
	jle	.LBB2_13
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_6 Depth=1
	testb	$1, %al
	jne	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_6 Depth=1
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	jne	.LBB2_11
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph.i.prol
                                        #   in Loop: Header=BB2_6 Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movq	crc_table(,%rcx,8), %rbx
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB2_13
.LBB2_11:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB2_6 Depth=1
	subl	%ecx, %eax
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph.i
                                        #   Parent Loop BB2_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	shrl	$24, %ecx
	movzbl	(%rbp), %edx
	xorq	%rcx, %rdx
	shlq	$8, %rbx
	xorq	crc_table(,%rdx,8), %rbx
	movl	%ebx, %ecx
	shrl	$24, %ecx
	movzbl	1(%rbp), %edx
	xorq	%rcx, %rdx
	shlq	$8, %rbx
	xorq	crc_table(,%rdx,8), %rbx
	addq	$2, %rbp
	addl	$-2, %eax
	jne	.LBB2_12
.LBB2_13:                               # %update_crc.exit
                                        #   in Loop: Header=BB2_6 Depth=1
	incl	%r15d
	cmpl	%r14d, %r15d
	jne	.LBB2_6
	jmp	.LBB2_14
.LBB2_4:
                                        # implicit-def: %EBX
.LBB2_14:                               # %gen_crc_table.exit._crit_edge
	movq	stdout(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_15:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%edi, %edi
	callq	exit
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	crc_table,@object       # @crc_table
	.local	crc_table
	.comm	crc_table,2048,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Usage: crc #numpackets"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"CRC completed for %d packets \n"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"crc_accum is %u\n"
	.size	.L.str.2, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
