	.text
	.file	"ExtractCallbackConsole.bc"
	.globl	_ZN23CExtractCallbackConsole8SetTotalEy
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole8SetTotalEy,@function
_ZN23CExtractCallbackConsole8SetTotalEy: # @_ZN23CExtractCallbackConsole8SetTotalEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZN23CExtractCallbackConsole8SetTotalEy, .Lfunc_end0-_ZN23CExtractCallbackConsole8SetTotalEy
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole12SetCompletedEPKy
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole12SetCompletedEPKy,@function
_ZN23CExtractCallbackConsole12SetCompletedEPKy: # @_ZN23CExtractCallbackConsole12SetCompletedEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_ZN23CExtractCallbackConsole12SetCompletedEPKy, .Lfunc_end1-_ZN23CExtractCallbackConsole12SetCompletedEPKy
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi,@function
_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi: # @_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	movl	$.L.str, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	movl	$.L.str.1, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movq	72(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	movq	72(%rbx), %rdi
	callq	_Z20ScanUserYesNoAllQuitP13CStdOutStream
	cmpl	$5, %eax
	ja	.LBB2_4
# BB#1:
	movl	$3, %edx
	movl	$-2147467260, %ecx      # imm = 0x80004004
	movl	%eax, %esi
	jmpq	*.LJTI2_0(,%rsi,8)
.LBB2_3:
	movl	%eax, %edx
	jmp	.LBB2_6
.LBB2_4:
	movl	$-2147467259, %ecx      # imm = 0x80004005
	jmp	.LBB2_7
.LBB2_5:
	movl	$2, %edx
	jmp	.LBB2_6
.LBB2_2:
	movl	$1, %edx
.LBB2_6:
	movq	40(%rsp), %rax
	movl	%edx, (%rax)
	xorl	%ecx, %ecx
.LBB2_7:
	movl	%ecx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi, .Lfunc_end2-_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_3
	.quad	.LBB2_5
	.quad	.LBB2_2
	.quad	.LBB2_6
	.quad	.LBB2_3
	.quad	.LBB2_7

	.text
	.globl	_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy,@function
_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy: # @_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	je	.LBB3_4
# BB#1:
	cmpl	$1, %ecx
	je	.LBB3_5
# BB#2:
	cmpl	$2, %ecx
	jne	.LBB3_3
# BB#6:
	movq	72(%rbx), %rdi
	addq	$72, %rbx
	movl	$.L.str.12, %esi
	jmp	.LBB3_7
.LBB3_4:
	movq	72(%rbx), %rdi
	addq	$72, %rbx
	movl	$.L.str.10, %esi
	jmp	.LBB3_7
.LBB3_5:
	movq	72(%rbx), %rdi
	addq	$72, %rbx
	movl	$.L.str.11, %esi
.LBB3_7:
	callq	_ZN13CStdOutStreamlsEPKc
	jmp	.LBB3_8
.LBB3_3:                                # %._crit_edge
	addq	$72, %rbx
.LBB3_8:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	testq	%r14, %r14
	je	.LBB3_10
# BB#9:
	movq	(%rbx), %rdi
	movl	$.L.str.2, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
	movl	$.L.str.3, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.LBB3_10:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy, .Lfunc_end3-_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.text
	.globl	_ZN23CExtractCallbackConsole12MessageErrorEPKw
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole12MessageErrorEPKw,@function
_ZN23CExtractCallbackConsole12MessageErrorEPKw: # @_ZN23CExtractCallbackConsole12MessageErrorEPKw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	callq	_ZN13CStdOutStreamlsEPKw
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movdqu	56(%rbx), %xmm0
	paddq	.LCPI4_0(%rip), %xmm0
	movdqu	%xmm0, 56(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN23CExtractCallbackConsole12MessageErrorEPKw, .Lfunc_end4-_ZN23CExtractCallbackConsole12MessageErrorEPKw
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.text
	.globl	_ZN23CExtractCallbackConsole18SetOperationResultEib
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole18SetOperationResultEib,@function
_ZN23CExtractCallbackConsole18SetOperationResultEib: # @_ZN23CExtractCallbackConsole18SetOperationResultEib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB5_1
# BB#2:
	movdqu	56(%rbx), %xmm0
	paddq	.LCPI5_0(%rip), %xmm0
	movdqu	%xmm0, 56(%rbx)
	movq	72(%rbx), %rdi
	addq	$72, %rbx
	movl	$.L.str.4, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	cmpl	$3, %ebp
	je	.LBB5_6
# BB#3:
	cmpl	$2, %ebp
	je	.LBB5_8
# BB#4:
	cmpl	$1, %ebp
	jne	.LBB5_9
# BB#5:
	movq	(%rbx), %rdi
	movl	$.L.str.13, %esi
	jmp	.LBB5_10
.LBB5_1:                                # %._crit_edge
	addq	$72, %rbx
	jmp	.LBB5_11
.LBB5_6:
	movq	(%rbx), %rdi
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %esi
	jmp	.LBB5_7
.LBB5_8:
	movq	(%rbx), %rdi
	movl	$.L.str.16, %eax
	movl	$.L.str.17, %esi
.LBB5_7:
	testb	%r14b, %r14b
	cmovneq	%rax, %rsi
	jmp	.LBB5_10
.LBB5_9:
	movq	(%rbx), %rdi
	movl	$.L.str.18, %esi
.LBB5_10:
	callq	_ZN13CStdOutStreamlsEPKc
.LBB5_11:
	movq	(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN23CExtractCallbackConsole18SetOperationResultEib, .Lfunc_end5-_ZN23CExtractCallbackConsole18SetOperationResultEib
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE,@function
_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE: # @_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	$1, 20(%r15)
	leaq	24(%r15), %rax
	cmpq	%r14, %rax
	je	.LBB6_9
# BB#1:
	movl	$0, 32(%r15)
	movq	24(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %r12
	incq	%r12
	movl	36(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB6_6
# BB#2:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_5
# BB#3:
	testl	%ebp, %ebp
	jle	.LBB6_5
# BB#4:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r15), %rax
.LBB6_5:                                # %._crit_edge16.i.i
	movq	%r13, 24(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 36(%r15)
	movq	%r13, %rbx
.LBB6_6:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_7
# BB#8:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r14), %eax
	movl	%eax, 32(%r15)
.LBB6_9:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE, .Lfunc_end6-_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw,@function
_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw: # @_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 80
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpb	$0, 20(%r15)
	jne	.LBB7_15
# BB#1:
	movq	72(%r15), %rsi
	leaq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z11GetPasswordP13CStdOutStreamb
	leaq	24(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB7_2
# BB#3:
	movl	$0, 32(%r15)
	movq	24(%r15), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r12
	incq	%r12
	movl	36(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB7_9
# BB#4:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r13
.Ltmp1:
# BB#5:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB7_8
# BB#6:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB7_8
# BB#7:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r15), %rax
.LBB7_8:                                # %._crit_edge16.i.i
	movq	%r13, 24(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 36(%r15)
	movq	%r13, %rbx
.LBB7_9:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_10:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_10
# BB#11:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 32(%r15)
	testq	%rdi, %rdi
	jne	.LBB7_13
	jmp	.LBB7_14
.LBB7_2:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_14
.LBB7_13:
	callq	_ZdaPv
.LBB7_14:                               # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, 20(%r15)
.LBB7_15:
	movq	24(%r15), %rdi
	callq	SysAllocString
	movq	%rax, (%r14)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_16:
.Ltmp2:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_18
# BB#17:
	callq	_ZdaPv
.LBB7_18:                               # %_ZN11CStringBaseIwED2Ev.exit3
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw, .Lfunc_end7-_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw,@function
_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw: # @_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw # TAILCALL
.Lfunc_end8:
	.size	_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw, .Lfunc_end8-_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole10BeforeOpenEPKw
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole10BeforeOpenEPKw,@function
_ZN23CExtractCallbackConsole10BeforeOpenEPKw: # @_ZN23CExtractCallbackConsole10BeforeOpenEPKw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 16
.Lcfi49:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	incq	40(%rdi)
	movq	$0, 64(%rdi)
	movq	72(%rdi), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$.L.str.19, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN23CExtractCallbackConsole10BeforeOpenEPKw, .Lfunc_end9-_ZN23CExtractCallbackConsole10BeforeOpenEPKw
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole10OpenResultEPKwib
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole10OpenResultEPKwib,@function
_ZN23CExtractCallbackConsole10OpenResultEPKwib: # @_ZN23CExtractCallbackConsole10OpenResultEPKwib
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	testl	%ebp, %ebp
	je	.LBB10_11
# BB#1:
	movq	72(%rbx), %rdi
	movl	$.L.str.5, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	cmpl	$1, %ebp
	jne	.LBB10_3
# BB#2:
	movq	72(%rbx), %rdi
	movl	$.L.str.6, %eax
	movl	$.L.str.7, %esi
	testb	%r14b, %r14b
	cmovneq	%rax, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	jmp	.LBB10_10
.LBB10_3:
	movq	72(%rbx), %r14
	cmpl	$-2147024882, %ebp      # imm = 0x8007000E
	jne	.LBB10_6
# BB#4:
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	jmp	.LBB10_10
.LBB10_6:
	movq	$0, 8(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	$4, 12(%rsp)
.Ltmp3:
	movq	%rsp, %rsi
	movl	%ebp, %edi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp4:
# BB#7:                                 # %_ZN8NWindows6NError15MyFormatMessageEj.exit
	movq	(%rsp), %rsi
.Ltmp6:
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp7:
# BB#8:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_10
# BB#9:
	callq	_ZdaPv
.LBB10_10:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	72(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	incq	48(%rbx)
.LBB10_11:
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB10_12:
.Ltmp8:
	jmp	.LBB10_13
.LBB10_5:
.Ltmp5:
.LBB10_13:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_15
# BB#14:
	callq	_ZdaPv
.LBB10_15:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN23CExtractCallbackConsole10OpenResultEPKwib, .Lfunc_end10-_ZN23CExtractCallbackConsole10OpenResultEPKwib
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp7     #   Call between .Ltmp7 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN23CExtractCallbackConsole15ThereAreNoFilesEv
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole15ThereAreNoFilesEv,@function
_ZN23CExtractCallbackConsole15ThereAreNoFilesEv: # @_ZN23CExtractCallbackConsole15ThereAreNoFilesEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 16
	movq	72(%rdi), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$.L.str.20, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN23CExtractCallbackConsole15ThereAreNoFilesEv, .Lfunc_end11-_ZN23CExtractCallbackConsole15ThereAreNoFilesEv
	.cfi_endproc

	.globl	_ZN23CExtractCallbackConsole13ExtractResultEi
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole13ExtractResultEi,@function
_ZN23CExtractCallbackConsole13ExtractResultEi: # @_ZN23CExtractCallbackConsole13ExtractResultEi
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testl	%ebp, %ebp
	je	.LBB12_1
# BB#5:                                 # %.critedge
	incq	48(%rbx)
	cmpl	$-2147467260, %ebp      # imm = 0x80004004
	je	.LBB12_11
# BB#6:                                 # %.critedge
	cmpl	$28, %ebp
	je	.LBB12_11
# BB#7:
	movq	72(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	movl	$.L.str.22, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	cmpl	$-2147024882, %ebp      # imm = 0x8007000E
	jne	.LBB12_12
# BB#8:
	movq	72(%rbx), %rdi
	movl	$.L.str.23, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	jmp	.LBB12_9
.LBB12_1:
	movq	72(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	cmpq	$0, 64(%rbx)
	je	.LBB12_2
# BB#4:
	incq	48(%rbx)
	movq	72(%rbx), %rdi
	movl	$.L.str.9, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	64(%rbx), %rsi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
	jmp	.LBB12_3
.LBB12_2:
	movq	72(%rbx), %rdi
	movl	$.L.str.21, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.LBB12_3:
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	jmp	.LBB12_10
.LBB12_12:
	movq	$0, 16(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movl	$0, (%rax)
	movl	$4, 20(%rsp)
.Ltmp9:
	leaq	8(%rsp), %rsi
	movl	%ebp, %edi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp10:
# BB#13:
	movq	72(%rbx), %rdi
	movq	8(%rsp), %rsi
.Ltmp11:
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp12:
# BB#14:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#15:
	callq	_ZdaPv
.LBB12_9:
	movq	72(%rbx), %rdi
	movl	$_Z4endlR13CStdOutStream, %esi
.LBB12_10:
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
	xorl	%ebp, %ebp
.LBB12_11:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB12_16:
.Ltmp13:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_18
# BB#17:
	callq	_ZdaPv
.LBB12_18:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN23CExtractCallbackConsole13ExtractResultEi, .Lfunc_end12-_ZN23CExtractCallbackConsole13ExtractResultEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin2   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end12-.Ltmp12    #   Call between .Ltmp12 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,@function
_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv: # @_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB13_1
# BB#2:
	movl	$IID_IFolderArchiveExtractCallback, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB13_3
.LBB13_1:
	movq	%rbx, (%r14)
.LBB13_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB13_3:
	movl	$IID_ICryptoGetTextPassword, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB13_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB13_6
.LBB13_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB13_7
.Lfunc_end13:
	.size	_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN23CExtractCallbackConsole6AddRefEv,"axG",@progbits,_ZN23CExtractCallbackConsole6AddRefEv,comdat
	.weak	_ZN23CExtractCallbackConsole6AddRefEv
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole6AddRefEv,@function
_ZN23CExtractCallbackConsole6AddRefEv:  # @_ZN23CExtractCallbackConsole6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN23CExtractCallbackConsole6AddRefEv, .Lfunc_end14-_ZN23CExtractCallbackConsole6AddRefEv
	.cfi_endproc

	.section	.text._ZN23CExtractCallbackConsole7ReleaseEv,"axG",@progbits,_ZN23CExtractCallbackConsole7ReleaseEv,comdat
	.weak	_ZN23CExtractCallbackConsole7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsole7ReleaseEv,@function
_ZN23CExtractCallbackConsole7ReleaseEv: # @_ZN23CExtractCallbackConsole7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN23CExtractCallbackConsole7ReleaseEv, .Lfunc_end15-_ZN23CExtractCallbackConsole7ReleaseEv
	.cfi_endproc

	.section	.text._ZN23CExtractCallbackConsoleD2Ev,"axG",@progbits,_ZN23CExtractCallbackConsoleD2Ev,comdat
	.weak	_ZN23CExtractCallbackConsoleD2Ev
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsoleD2Ev,@function
_ZN23CExtractCallbackConsoleD2Ev:       # @_ZN23CExtractCallbackConsoleD2Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTV23CExtractCallbackConsole+168, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CExtractCallbackConsole+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB16_1:                               # %_ZN11CStringBaseIwED2Ev.exit
	retq
.Lfunc_end16:
	.size	_ZN23CExtractCallbackConsoleD2Ev, .Lfunc_end16-_ZN23CExtractCallbackConsoleD2Ev
	.cfi_endproc

	.section	.text._ZN23CExtractCallbackConsoleD0Ev,"axG",@progbits,_ZN23CExtractCallbackConsoleD0Ev,comdat
	.weak	_ZN23CExtractCallbackConsoleD0Ev
	.p2align	4, 0x90
	.type	_ZN23CExtractCallbackConsoleD0Ev,@function
_ZN23CExtractCallbackConsoleD0Ev:       # @_ZN23CExtractCallbackConsoleD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTV23CExtractCallbackConsole+168, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CExtractCallbackConsole+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	callq	_ZdaPv
.LBB17_2:                               # %_ZN23CExtractCallbackConsoleD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZN23CExtractCallbackConsoleD0Ev, .Lfunc_end17-_ZN23CExtractCallbackConsoleD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB18_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB18_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB18_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB18_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB18_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB18_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB18_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB18_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB18_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB18_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB18_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB18_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB18_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB18_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB18_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB18_16
.LBB18_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IFolderArchiveExtractCallback(%rip), %cl
	jne	.LBB18_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+1(%rip), %al
	jne	.LBB18_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+2(%rip), %al
	jne	.LBB18_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+3(%rip), %al
	jne	.LBB18_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+4(%rip), %al
	jne	.LBB18_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+5(%rip), %al
	jne	.LBB18_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+6(%rip), %al
	jne	.LBB18_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+7(%rip), %al
	jne	.LBB18_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+8(%rip), %al
	jne	.LBB18_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+9(%rip), %al
	jne	.LBB18_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+10(%rip), %al
	jne	.LBB18_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+11(%rip), %al
	jne	.LBB18_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+12(%rip), %al
	jne	.LBB18_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+13(%rip), %al
	jne	.LBB18_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+14(%rip), %al
	jne	.LBB18_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IFolderArchiveExtractCallback+15(%rip), %al
	jne	.LBB18_33
.LBB18_16:
	movq	%rdi, (%rdx)
	jmp	.LBB18_50
.LBB18_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoGetTextPassword(%rip), %cl
	jne	.LBB18_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+1(%rip), %cl
	jne	.LBB18_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+2(%rip), %cl
	jne	.LBB18_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+3(%rip), %cl
	jne	.LBB18_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+4(%rip), %cl
	jne	.LBB18_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+5(%rip), %cl
	jne	.LBB18_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+6(%rip), %cl
	jne	.LBB18_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+7(%rip), %cl
	jne	.LBB18_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+8(%rip), %cl
	jne	.LBB18_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+9(%rip), %cl
	jne	.LBB18_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+10(%rip), %cl
	jne	.LBB18_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+11(%rip), %cl
	jne	.LBB18_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+12(%rip), %cl
	jne	.LBB18_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+13(%rip), %cl
	jne	.LBB18_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+14(%rip), %cl
	jne	.LBB18_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoGetTextPassword+15(%rip), %cl
	jne	.LBB18_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB18_50:                              # %_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB18_51:                              # %_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv, .Lfunc_end18-_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N23CExtractCallbackConsole6AddRefEv,"axG",@progbits,_ZThn8_N23CExtractCallbackConsole6AddRefEv,comdat
	.weak	_ZThn8_N23CExtractCallbackConsole6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsole6AddRefEv,@function
_ZThn8_N23CExtractCallbackConsole6AddRefEv: # @_ZThn8_N23CExtractCallbackConsole6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end19:
	.size	_ZThn8_N23CExtractCallbackConsole6AddRefEv, .Lfunc_end19-_ZThn8_N23CExtractCallbackConsole6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N23CExtractCallbackConsole7ReleaseEv,"axG",@progbits,_ZThn8_N23CExtractCallbackConsole7ReleaseEv,comdat
	.weak	_ZThn8_N23CExtractCallbackConsole7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsole7ReleaseEv,@function
_ZThn8_N23CExtractCallbackConsole7ReleaseEv: # @_ZThn8_N23CExtractCallbackConsole7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB20_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB20_2:                               # %_ZN23CExtractCallbackConsole7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZThn8_N23CExtractCallbackConsole7ReleaseEv, .Lfunc_end20-_ZThn8_N23CExtractCallbackConsole7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N23CExtractCallbackConsoleD1Ev,"axG",@progbits,_ZThn8_N23CExtractCallbackConsoleD1Ev,comdat
	.weak	_ZThn8_N23CExtractCallbackConsoleD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsoleD1Ev,@function
_ZThn8_N23CExtractCallbackConsoleD1Ev:  # @_ZThn8_N23CExtractCallbackConsoleD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTV23CExtractCallbackConsole+168, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CExtractCallbackConsole+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB21_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB21_1:                               # %_ZN23CExtractCallbackConsoleD2Ev.exit
	retq
.Lfunc_end21:
	.size	_ZThn8_N23CExtractCallbackConsoleD1Ev, .Lfunc_end21-_ZThn8_N23CExtractCallbackConsoleD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N23CExtractCallbackConsoleD0Ev,"axG",@progbits,_ZThn8_N23CExtractCallbackConsoleD0Ev,comdat
	.weak	_ZThn8_N23CExtractCallbackConsoleD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N23CExtractCallbackConsoleD0Ev,@function
_ZThn8_N23CExtractCallbackConsoleD0Ev:  # @_ZThn8_N23CExtractCallbackConsoleD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
.Lcfi75:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTV23CExtractCallbackConsole+168, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV23CExtractCallbackConsole+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	16(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	callq	_ZdaPv
.LBB22_2:                               # %_ZN23CExtractCallbackConsoleD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZThn8_N23CExtractCallbackConsoleD0Ev, .Lfunc_end22-_ZThn8_N23CExtractCallbackConsoleD0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB23_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB23_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB23_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB23_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB23_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB23_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB23_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB23_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB23_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB23_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB23_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB23_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB23_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB23_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB23_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB23_16:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end23-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"file "
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nalready exists. Overwrite with "
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" <"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	">"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"     "
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Error: "
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Can not open encrypted archive. Wrong password?"
	.size	.L.str.6, 48

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Can not open file as archive"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Can't allocate required memory"
	.size	.L.str.8, 31

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Sub items Errors: "
	.size	.L.str.9, 19

	.type	_ZTV23CExtractCallbackConsole,@object # @_ZTV23CExtractCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTV23CExtractCallbackConsole
	.p2align	3
_ZTV23CExtractCallbackConsole:
	.quad	0
	.quad	_ZTI23CExtractCallbackConsole
	.quad	_ZN23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.quad	_ZN23CExtractCallbackConsole6AddRefEv
	.quad	_ZN23CExtractCallbackConsole7ReleaseEv
	.quad	_ZN23CExtractCallbackConsoleD2Ev
	.quad	_ZN23CExtractCallbackConsoleD0Ev
	.quad	_ZN23CExtractCallbackConsole8SetTotalEy
	.quad	_ZN23CExtractCallbackConsole12SetCompletedEPKy
	.quad	_ZN23CExtractCallbackConsole12AskOverwriteEPKwPK9_FILETIMEPKyS1_S4_S6_Pi
	.quad	_ZN23CExtractCallbackConsole16PrepareOperationEPKwbiPKy
	.quad	_ZN23CExtractCallbackConsole12MessageErrorEPKw
	.quad	_ZN23CExtractCallbackConsole18SetOperationResultEib
	.quad	_ZN23CExtractCallbackConsole10BeforeOpenEPKw
	.quad	_ZN23CExtractCallbackConsole10OpenResultEPKwib
	.quad	_ZN23CExtractCallbackConsole15ThereAreNoFilesEv
	.quad	_ZN23CExtractCallbackConsole13ExtractResultEi
	.quad	_ZN23CExtractCallbackConsole11SetPasswordERK11CStringBaseIwE
	.quad	_ZN23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.quad	-8
	.quad	_ZTI23CExtractCallbackConsole
	.quad	_ZThn8_N23CExtractCallbackConsole14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N23CExtractCallbackConsole6AddRefEv
	.quad	_ZThn8_N23CExtractCallbackConsole7ReleaseEv
	.quad	_ZThn8_N23CExtractCallbackConsoleD1Ev
	.quad	_ZThn8_N23CExtractCallbackConsoleD0Ev
	.quad	_ZThn8_N23CExtractCallbackConsole21CryptoGetTextPasswordEPPw
	.size	_ZTV23CExtractCallbackConsole, 216

	.type	_ZTS23CExtractCallbackConsole,@object # @_ZTS23CExtractCallbackConsole
	.globl	_ZTS23CExtractCallbackConsole
	.p2align	4
_ZTS23CExtractCallbackConsole:
	.asciz	"23CExtractCallbackConsole"
	.size	_ZTS23CExtractCallbackConsole, 26

	.type	_ZTS18IExtractCallbackUI,@object # @_ZTS18IExtractCallbackUI
	.section	.rodata._ZTS18IExtractCallbackUI,"aG",@progbits,_ZTS18IExtractCallbackUI,comdat
	.weak	_ZTS18IExtractCallbackUI
	.p2align	4
_ZTS18IExtractCallbackUI:
	.asciz	"18IExtractCallbackUI"
	.size	_ZTS18IExtractCallbackUI, 21

	.type	_ZTS29IFolderArchiveExtractCallback,@object # @_ZTS29IFolderArchiveExtractCallback
	.section	.rodata._ZTS29IFolderArchiveExtractCallback,"aG",@progbits,_ZTS29IFolderArchiveExtractCallback,comdat
	.weak	_ZTS29IFolderArchiveExtractCallback
	.p2align	4
_ZTS29IFolderArchiveExtractCallback:
	.asciz	"29IFolderArchiveExtractCallback"
	.size	_ZTS29IFolderArchiveExtractCallback, 32

	.type	_ZTS9IProgress,@object  # @_ZTS9IProgress
	.section	.rodata._ZTS9IProgress,"aG",@progbits,_ZTS9IProgress,comdat
	.weak	_ZTS9IProgress
_ZTS9IProgress:
	.asciz	"9IProgress"
	.size	_ZTS9IProgress, 11

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI9IProgress,@object  # @_ZTI9IProgress
	.section	.rodata._ZTI9IProgress,"aG",@progbits,_ZTI9IProgress,comdat
	.weak	_ZTI9IProgress
	.p2align	4
_ZTI9IProgress:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IProgress
	.quad	_ZTI8IUnknown
	.size	_ZTI9IProgress, 24

	.type	_ZTI29IFolderArchiveExtractCallback,@object # @_ZTI29IFolderArchiveExtractCallback
	.section	.rodata._ZTI29IFolderArchiveExtractCallback,"aG",@progbits,_ZTI29IFolderArchiveExtractCallback,comdat
	.weak	_ZTI29IFolderArchiveExtractCallback
	.p2align	4
_ZTI29IFolderArchiveExtractCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29IFolderArchiveExtractCallback
	.quad	_ZTI9IProgress
	.size	_ZTI29IFolderArchiveExtractCallback, 24

	.type	_ZTI18IExtractCallbackUI,@object # @_ZTI18IExtractCallbackUI
	.section	.rodata._ZTI18IExtractCallbackUI,"aG",@progbits,_ZTI18IExtractCallbackUI,comdat
	.weak	_ZTI18IExtractCallbackUI
	.p2align	4
_ZTI18IExtractCallbackUI:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18IExtractCallbackUI
	.quad	_ZTI29IFolderArchiveExtractCallback
	.size	_ZTI18IExtractCallbackUI, 24

	.type	_ZTS22ICryptoGetTextPassword,@object # @_ZTS22ICryptoGetTextPassword
	.section	.rodata._ZTS22ICryptoGetTextPassword,"aG",@progbits,_ZTS22ICryptoGetTextPassword,comdat
	.weak	_ZTS22ICryptoGetTextPassword
	.p2align	4
_ZTS22ICryptoGetTextPassword:
	.asciz	"22ICryptoGetTextPassword"
	.size	_ZTS22ICryptoGetTextPassword, 25

	.type	_ZTI22ICryptoGetTextPassword,@object # @_ZTI22ICryptoGetTextPassword
	.section	.rodata._ZTI22ICryptoGetTextPassword,"aG",@progbits,_ZTI22ICryptoGetTextPassword,comdat
	.weak	_ZTI22ICryptoGetTextPassword
	.p2align	4
_ZTI22ICryptoGetTextPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoGetTextPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoGetTextPassword, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI23CExtractCallbackConsole,@object # @_ZTI23CExtractCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTI23CExtractCallbackConsole
	.p2align	4
_ZTI23CExtractCallbackConsole:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS23CExtractCallbackConsole
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI18IExtractCallbackUI
	.quad	2                       # 0x2
	.quad	_ZTI22ICryptoGetTextPassword
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTI23CExtractCallbackConsole, 72

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.10:
	.asciz	"Extracting  "
	.size	.L.str.10, 13

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Testing     "
	.size	.L.str.11, 13

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Skipping    "
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Unsupported Method"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"CRC Failed in encrypted file. Wrong password?"
	.size	.L.str.14, 46

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"CRC Failed"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Data Error in encrypted file. Wrong password?"
	.size	.L.str.16, 46

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Data Error"
	.size	.L.str.17, 11

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Unknown Error"
	.size	.L.str.18, 14

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Processing archive: "
	.size	.L.str.19, 21

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"No files to process"
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Everything is Ok"
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ERROR: "
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Can't allocate required memory!"
	.size	.L.str.23, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
