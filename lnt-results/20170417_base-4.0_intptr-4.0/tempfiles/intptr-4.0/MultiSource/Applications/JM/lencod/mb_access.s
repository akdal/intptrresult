	.text
	.file	"mb_access.bc"
	.globl	mb_is_available
	.p2align	4, 0x90
	.type	mb_is_available,@function
mb_is_available:                        # @mb_is_available
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.LBB0_5
# BB#1:
	movq	img(%rip), %rcx
	cmpl	%edi, 15348(%rcx)
	jle	.LBB0_5
# BB#2:
	cmpl	$0, 15420(%rcx)
	jne	.LBB0_4
# BB#3:
	movq	14224(%rcx), %rcx
	movslq	%edi, %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movl	(%rcx,%rdx), %edx
	movslq	%esi, %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	cmpl	(%rcx,%rsi), %edx
	jne	.LBB0_5
.LBB0_4:
	movl	$1, %eax
.LBB0_5:
	retq
.Lfunc_end0:
	.size	mb_is_available, .Lfunc_end0-mb_is_available
	.cfi_endproc

	.globl	CheckAvailabilityOfNeighbors
	.p2align	4, 0x90
	.type	CheckAvailabilityOfNeighbors,@function
CheckAvailabilityOfNeighbors:           # @CheckAvailabilityOfNeighbors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rdi
	movq	14224(%rdi), %rax
	movslq	12(%rdi), %rsi
	imulq	$536, %rsi, %rcx        # imm = 0x218
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rax,%rcx)
	cmpl	$0, 15268(%rdi)
	je	.LBB1_6
# BB#1:
	movl	%esi, %edx
	sarl	%edx
	andl	$-2, %esi
	leal	-2(%rsi), %ebp
	movl	%ebp, 436(%rax,%rcx)
	movl	%edx, %ebp
	subl	15336(%rdi), %ebp
	leal	(%rbp,%rbp), %r11d
	xorl	%r8d, %r8d
	movl	%esi, %r10d
	addl	$-2, %r10d
	movl	%r11d, 440(%rax,%rcx)
	leal	2(%rbp,%rbp), %ebx
	movl	%ebx, 444(%rax,%rcx)
	leal	-2(%rbp,%rbp), %esi
	movl	%esi, 448(%rax,%rcx)
	js	.LBB1_12
# BB#2:
	cmpl	%r10d, 15348(%rdi)
	jle	.LBB1_12
# BB#3:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_5
# BB#4:
	movslq	%r10d, %rbp
	imulq	$536, %rbp, %rbp        # imm = 0x218
	movl	(%rax,%rbp), %ebp
	cmpl	(%rax,%rcx), %ebp
	jne	.LBB1_12
.LBB1_5:                                # %mb_is_available.exit
	movq	PicPos(%rip), %r9
	movslq	%edx, %rbp
	movq	(%r9,%rbp,8), %rbp
	cmpl	$0, (%rbp)
	setne	%bpl
	jmp	.LBB1_13
.LBB1_12:
	xorl	%ebp, %ebp
.LBB1_13:                               # %mb_is_available.exit.thread
	movzbl	%bpl, %r9d
	movl	%r9d, 452(%rax,%rcx)
	testl	%r11d, %r11d
	js	.LBB1_18
# BB#14:
	cmpl	%r11d, 15348(%rdi)
	jle	.LBB1_18
# BB#15:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_17
# BB#16:
	movslq	%r11d, %rbp
	imulq	$536, %rbp, %rbp        # imm = 0x218
	movl	(%rax,%rbp), %ebp
	cmpl	(%rax,%rcx), %ebp
	jne	.LBB1_18
.LBB1_17:
	movl	$1, %r8d
.LBB1_18:                               # %mb_is_available.exit75
	movl	%r8d, 456(%rax,%rcx)
	testl	%ebx, %ebx
	js	.LBB1_24
# BB#19:
	cmpl	%ebx, 15348(%rdi)
	jle	.LBB1_24
# BB#20:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_22
# BB#21:
	movslq	%ebx, %rbx
	imulq	$536, %rbx, %rbx        # imm = 0x218
	movl	(%rax,%rbx), %ebx
	cmpl	(%rax,%rcx), %ebx
	jne	.LBB1_24
.LBB1_22:                               # %mb_is_available.exit73
	movq	PicPos(%rip), %rbx
	movslq	%edx, %rbp
	movq	8(%rbx,%rbp,8), %rbx
	cmpl	$0, (%rbx)
	setne	%bl
	jmp	.LBB1_25
.LBB1_24:
	xorl	%ebx, %ebx
.LBB1_25:                               # %mb_is_available.exit73.thread
	movzbl	%bl, %ebx
	movl	%ebx, 460(%rax,%rcx)
	testl	%esi, %esi
	js	.LBB1_59
# BB#26:
	cmpl	%esi, 15348(%rdi)
	jle	.LBB1_59
# BB#27:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_29
# BB#28:
	movslq	%esi, %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movl	(%rax,%rsi), %esi
	cmpl	(%rax,%rcx), %esi
	jne	.LBB1_59
.LBB1_29:                               # %mb_is_available.exit71
	movq	PicPos(%rip), %rsi
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rdx
	jmp	.LBB1_51
.LBB1_6:
	leal	-1(%rsi), %r10d
	movl	%r10d, 436(%rax,%rcx)
	movl	15336(%rdi), %ebx
	movl	%esi, %r11d
	subl	%ebx, %r11d
	movl	%r11d, 440(%rax,%rcx)
	leal	1(%rsi), %r14d
	movl	%r14d, %ebp
	subl	%ebx, %ebp
	movl	%ebp, 444(%rax,%rcx)
	movl	%r10d, %edx
	subl	%ebx, %edx
	movl	%edx, 448(%rax,%rcx)
	xorl	%r8d, %r8d
	testl	%r10d, %r10d
	js	.LBB1_33
# BB#7:
	cmpl	%r10d, 15348(%rdi)
	jle	.LBB1_33
# BB#8:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_10
# BB#9:
	movslq	%r10d, %rbx
	imulq	$536, %rbx, %rbx        # imm = 0x218
	movl	(%rax,%rbx), %ebx
	cmpl	(%rax,%rcx), %ebx
	jne	.LBB1_33
.LBB1_10:                               # %mb_is_available.exit69
	movq	PicPos(%rip), %rbx
	movq	(%rbx,%rsi,8), %rbx
	cmpl	$0, (%rbx)
	setne	%bl
	jmp	.LBB1_34
.LBB1_33:
	xorl	%ebx, %ebx
.LBB1_34:                               # %mb_is_available.exit69.thread
	movzbl	%bl, %r9d
	movl	%r9d, 452(%rax,%rcx)
	testl	%r11d, %r11d
	js	.LBB1_39
# BB#35:
	cmpl	%r11d, 15348(%rdi)
	jle	.LBB1_39
# BB#36:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_38
# BB#37:
	movslq	%r11d, %rbx
	imulq	$536, %rbx, %rbx        # imm = 0x218
	movl	(%rax,%rbx), %ebx
	cmpl	(%rax,%rcx), %ebx
	jne	.LBB1_39
.LBB1_38:
	movl	$1, %r8d
.LBB1_39:                               # %mb_is_available.exit67
	movl	%r8d, 456(%rax,%rcx)
	testl	%ebp, %ebp
	js	.LBB1_45
# BB#40:
	cmpl	%ebp, 15348(%rdi)
	jle	.LBB1_45
# BB#41:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_43
# BB#42:
	movslq	%ebp, %rbx
	imulq	$536, %rbx, %rbx        # imm = 0x218
	movl	(%rax,%rbx), %ebx
	cmpl	(%rax,%rcx), %ebx
	jne	.LBB1_45
.LBB1_43:                               # %mb_is_available.exit65
	movq	PicPos(%rip), %rbx
	movslq	%r14d, %rbp
	movq	(%rbx,%rbp,8), %rbx
	cmpl	$0, (%rbx)
	setne	%bl
	jmp	.LBB1_46
.LBB1_45:
	xorl	%ebx, %ebx
.LBB1_46:                               # %mb_is_available.exit65.thread
	movzbl	%bl, %ebx
	movl	%ebx, 460(%rax,%rcx)
	testl	%edx, %edx
	js	.LBB1_59
# BB#47:
	cmpl	%edx, 15348(%rdi)
	jle	.LBB1_59
# BB#48:
	cmpl	$0, 15420(%rdi)
	jne	.LBB1_50
# BB#49:
	movslq	%edx, %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movl	(%rax,%rdx), %edx
	cmpl	(%rax,%rcx), %edx
	jne	.LBB1_59
.LBB1_50:                               # %mb_is_available.exit63
	movq	PicPos(%rip), %rdx
	movq	(%rdx,%rsi,8), %rdx
.LBB1_51:                               # %mb_is_available.exit71.thread
	cmpl	$0, (%rdx)
	setne	%dl
	jmp	.LBB1_60
.LBB1_59:
	xorl	%edx, %edx
.LBB1_60:                               # %mb_is_available.exit71.thread
	movzbl	%dl, %edx
	movl	%edx, 464(%rax,%rcx)
	testl	%r9d, %r9d
	je	.LBB1_62
# BB#61:
	leaq	64(%rax,%rcx), %rdx
	movslq	%r10d, %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	addq	%rax, %rsi
	movq	%rsi, (%rdx)
.LBB1_62:
	testl	%r8d, %r8d
	je	.LBB1_64
# BB#63:
	leaq	56(%rax,%rcx), %rcx
	movslq	%r11d, %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	addq	%rax, %rdx
	movq	%rdx, (%rcx)
.LBB1_64:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	CheckAvailabilityOfNeighbors, .Lfunc_end1-CheckAvailabilityOfNeighbors
	.cfi_endproc

	.globl	get_mb_block_pos_normal
	.p2align	4, 0x90
	.type	get_mb_block_pos_normal,@function
get_mb_block_pos_normal:                # @get_mb_block_pos_normal
	.cfi_startproc
# BB#0:
	movq	PicPos(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rsi)
	movl	4(%rax), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end2:
	.size	get_mb_block_pos_normal, .Lfunc_end2-get_mb_block_pos_normal
	.cfi_endproc

	.globl	get_mb_block_pos_mbaff
	.p2align	4, 0x90
	.type	get_mb_block_pos_mbaff,@function
get_mb_block_pos_mbaff:                 # @get_mb_block_pos_mbaff
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	PicPos(%rip), %rax
	movl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	movl	%ecx, (%rsi)
	movl	4(%rax), %eax
	andl	$1, %edi
	leal	(%rdi,%rax,2), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end3:
	.size	get_mb_block_pos_mbaff, .Lfunc_end3-get_mb_block_pos_mbaff
	.cfi_endproc

	.globl	get_mb_pos
	.p2align	4, 0x90
	.type	get_mb_pos,@function
get_mb_pos:                             # @get_mb_pos
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	callq	*get_mb_block_pos(%rip)
	movq	img(%rip), %rax
	movslq	%r14d, %rcx
	movl	(%rbp), %edx
	imull	15552(%rax,%rcx,8), %edx
	movl	%edx, (%rbp)
	movl	(%rbx), %edx
	imull	15556(%rax,%rcx,8), %edx
	movl	%edx, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_mb_pos, .Lfunc_end4-get_mb_pos
	.cfi_endproc

	.globl	getNonAffNeighbour
	.p2align	4, 0x90
	.type	getNonAffNeighbour,@function
getNonAffNeighbour:                     # @getNonAffNeighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	img(%rip), %r9
	movq	14224(%r9), %r10
	movl	%edi, %r11d
	movslq	%ecx, %rax
	movl	15552(%r9,%rax,8), %ecx
	movl	15556(%r9,%rax,8), %eax
	testl	%esi, %edx
	js	.LBB5_1
# BB#2:
	testl	%esi, %esi
	jns	.LBB5_6
# BB#3:
	testl	%edx, %edx
	js	.LBB5_6
# BB#4:
	cmpl	%edx, %eax
	jle	.LBB5_6
# BB#5:
	imulq	$536, %r11, %r11        # imm = 0x218
	movl	436(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	452(%r10,%r11), %r10
	jmp	.LBB5_18
.LBB5_1:
	imulq	$536, %r11, %r11        # imm = 0x218
	movl	448(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	464(%r10,%r11), %r10
	jmp	.LBB5_18
.LBB5_6:
	testl	%esi, %esi
	js	.LBB5_10
# BB#7:
	testl	%edx, %edx
	jns	.LBB5_10
# BB#8:
	cmpl	%esi, %ecx
	jle	.LBB5_10
# BB#9:
	imulq	$536, %r11, %r11        # imm = 0x218
	movl	440(%r10,%r11), %edi
	movl	%edi, 4(%r8)
	leaq	456(%r10,%r11), %r10
	jmp	.LBB5_18
.LBB5_10:
	movl	%edx, %ebx
	orl	%esi, %ebx
	js	.LBB5_14
# BB#11:
	cmpl	%esi, %ecx
	jle	.LBB5_14
# BB#12:
	cmpl	%edx, %eax
	jle	.LBB5_14
# BB#13:                                # %.thread82
	movl	%edi, 4(%r8)
	movl	$1, (%r8)
	jmp	.LBB5_21
.LBB5_14:
	testl	%edx, %edx
	jns	.LBB5_16
# BB#15:
	cmpl	%esi, %ecx
	jg	.LBB5_16
# BB#17:
	imulq	$536, %r11, %rbx        # imm = 0x218
	movl	444(%r10,%rbx), %edi
	movl	%edi, 4(%r8)
	leaq	460(%r10,%rbx), %r10
.LBB5_18:
	movl	(%r10), %ebx
	movl	%ebx, (%r8)
	testl	%ebx, %ebx
	jne	.LBB5_21
# BB#19:
	cmpl	$0, 15420(%r9)
	je	.LBB5_22
.LBB5_20:                               # %._crit_edge
	movl	4(%r8), %edi
.LBB5_21:                               # %._crit_edge83
	movq	PicPos(%rip), %rbx
	movslq	%edi, %rdi
	movq	(%rbx,%rdi,8), %rdi
	leal	-1(%rcx), %ebx
	andl	%esi, %ebx
	movl	%ebx, 8(%r8)
	leal	-1(%rax), %esi
	andl	%edx, %esi
	movl	%esi, 12(%r8)
	imull	(%rdi), %ecx
	addl	%ebx, %ecx
	movl	%ecx, 16(%r8)
	imull	4(%rdi), %eax
	addl	%esi, %eax
	movl	%eax, 20(%r8)
.LBB5_22:
	popq	%rbx
	retq
.LBB5_16:                               # %.thread
	movl	$0, (%r8)
	cmpl	$0, 15420(%r9)
	jne	.LBB5_20
	jmp	.LBB5_22
.Lfunc_end5:
	.size	getNonAffNeighbour, .Lfunc_end5-getNonAffNeighbour
	.cfi_endproc

	.globl	getAffNeighbour
	.p2align	4, 0x90
	.type	getAffNeighbour,@function
getAffNeighbour:                        # @getAffNeighbour
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	img(%rip), %rax
	movq	14224(%rax), %r9
	movslq	%ecx, %r14
	movl	15552(%rax,%r14,8), %r8d
	movl	15556(%rax,%r14,8), %r15d
	movl	$0, (%rbx)
	cmpl	%edx, %r15d
	jle	.LBB6_74
# BB#1:
	testl	%edx, %edx
	js	.LBB6_3
# BB#2:
	cmpl	%esi, %r8d
	jle	.LBB6_74
.LBB6_3:
	movl	%edi, %ecx
	testl	%esi, %esi
	js	.LBB6_9
# BB#4:
	cmpl	%esi, %r8d
	jle	.LBB6_16
# BB#5:
	testl	%edx, %edx
	js	.LBB6_27
# BB#6:
	jne	.LBB6_63
# BB#7:
	cmpl	$2, 15420(%rax)
	jne	.LBB6_63
# BB#8:
	imulq	$536, %rcx, %rax        # imm = 0x218
	movl	440(%r9,%rax), %eax
	incl	%eax
	movl	%eax, 4(%rbx)
	movl	$1, (%rbx)
	movl	$-1, %edx
	jmp	.LBB6_73
.LBB6_9:
	imulq	$536, %rcx, %r11        # imm = 0x218
	movl	424(%r9,%r11), %ebp
	andl	$1, %edi
	testl	%edx, %edx
	js	.LBB6_22
# BB#10:
	movslq	436(%r9,%r11), %r10
	movl	%r10d, 4(%rbx)
	movl	452(%r9,%r11), %ecx
	movl	%ecx, (%rbx)
	testl	%ebp, %ebp
	je	.LBB6_32
# BB#11:
	testl	%edi, %edi
	jne	.LBB6_36
# BB#12:
	testl	%ecx, %ecx
	je	.LBB6_71
# BB#13:
	imulq	$536, %r10, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	jne	.LBB6_73
# BB#14:
	movl	%r15d, %eax
	sarl	%eax
	cmpl	%edx, %eax
	jle	.LBB6_75
# BB#15:
	addl	%edx, %edx
	jmp	.LBB6_73
.LBB6_16:
	movl	$-1, %r10d
	testl	%edx, %edx
	jns	.LBB6_72
# BB#17:
	imulq	$536, %rcx, %rcx        # imm = 0x218
	andl	$1, %edi
	cmpl	$0, 424(%r9,%rcx)
	je	.LBB6_42
# BB#18:
	movl	444(%r9,%rcx), %r11d
	testl	%edi, %edi
	jne	.LBB6_52
# BB#19:
	movl	%r11d, 4(%rbx)
	movl	460(%r9,%rcx), %ecx
	movl	%ecx, (%rbx)
	testl	%ecx, %ecx
	je	.LBB6_72
# BB#20:
	movslq	%r11d, %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	jne	.LBB6_73
# BB#21:
	incl	%r11d
	movl	%r11d, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_73
.LBB6_22:
	testl	%ebp, %ebp
	je	.LBB6_39
# BB#23:
	movl	448(%r9,%r11), %ecx
	testl	%edi, %edi
	jne	.LBB6_41
# BB#24:
	movl	%ecx, 4(%rbx)
	movl	464(%r9,%r11), %edi
	movl	%edi, (%rbx)
	testl	%edi, %edi
	je	.LBB6_71
# BB#25:
	movslq	%ecx, %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	jne	.LBB6_73
# BB#26:
	incl	%ecx
	movl	%ecx, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_73
.LBB6_27:
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	%edi, %ebp
	andl	$1, %ebp
	cmpl	$0, 424(%r9,%rcx)
	je	.LBB6_44
# BB#28:
	movl	440(%r9,%rcx), %edi
	testl	%ebp, %ebp
	jne	.LBB6_54
# BB#29:
	movl	%edi, 4(%rbx)
	movl	456(%r9,%rcx), %ecx
	movl	%ecx, (%rbx)
	testl	%ecx, %ecx
	je	.LBB6_71
# BB#30:
	movslq	%edi, %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	jne	.LBB6_73
# BB#31:
	incl	%edi
	movl	%edi, 4(%rbx)
	addl	%edx, %edx
	jmp	.LBB6_73
.LBB6_32:
	testl	%edi, %edi
	jne	.LBB6_49
# BB#33:
	testl	%ecx, %ecx
	je	.LBB6_71
# BB#34:
	imulq	$536, %r10, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	je	.LBB6_73
# BB#35:
	movl	%edx, %eax
	andl	$1, %eax
	addl	%r10d, %eax
	movl	%eax, 4(%rbx)
	shrl	%edx
	jmp	.LBB6_73
.LBB6_36:
	testl	%ecx, %ecx
	je	.LBB6_71
# BB#37:
	imulq	$536, %r10, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	je	.LBB6_66
# BB#38:
	leal	1(%r10), %eax
	movl	%eax, 4(%rbx)
	jmp	.LBB6_73
.LBB6_39:
	testl	%edi, %edi
	jne	.LBB6_58
# BB#40:
	movl	448(%r9,%r11), %ecx
.LBB6_41:
	incl	%ecx
	movl	%ecx, 4(%rbx)
	movl	464(%r9,%r11), %edi
	jmp	.LBB6_55
.LBB6_42:
	testl	%edi, %edi
	jne	.LBB6_61
# BB#43:
	movl	444(%r9,%rcx), %edi
	incl	%edi
	movl	%edi, 4(%rbx)
	jmp	.LBB6_53
.LBB6_44:
	testl	%ebp, %ebp
	jne	.LBB6_62
# BB#45:
	movslq	440(%r9,%rcx), %r10
	movl	%r10d, 4(%rbx)
	movl	456(%r9,%rcx), %edi
	testl	%edi, %edi
	je	.LBB6_55
# BB#46:
	cmpl	$1, 15420(%rax)
	jne	.LBB6_48
# BB#47:
	imulq	$536, %r10, %rcx        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	jne	.LBB6_55
.LBB6_48:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	jmp	.LBB6_55
.LBB6_49:
	testl	%ecx, %ecx
	je	.LBB6_71
# BB#50:
	imulq	$536, %r10, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	je	.LBB6_70
# BB#51:
	leal	(%r15,%rdx), %eax
	andl	$1, %edx
	addl	%r10d, %edx
	movl	%edx, 4(%rbx)
	sarl	%eax
	movl	%eax, %edx
	jmp	.LBB6_73
.LBB6_52:
	incl	%r11d
	movl	%r11d, 4(%rbx)
.LBB6_53:                               # %thread-pre-split
	movl	460(%r9,%rcx), %edi
	jmp	.LBB6_55
.LBB6_54:
	incl	%edi
	movl	%edi, 4(%rbx)
	movl	456(%r9,%rcx), %edi
.LBB6_55:                               # %thread-pre-split
	movl	%edi, (%rbx)
	testl	%edi, %edi
	movl	%edx, %r10d
	jne	.LBB6_73
	jmp	.LBB6_72
.LBB6_58:
	movslq	436(%r9,%r11), %rcx
	movl	%ecx, 4(%rbx)
	movl	452(%r9,%r11), %edi
	movl	%edi, (%rbx)
	testl	%edi, %edi
	je	.LBB6_71
# BB#59:
	imulq	$536, %rcx, %rax        # imm = 0x218
	cmpl	$0, 424(%r9,%rax)
	je	.LBB6_73
# BB#60:
	incl	%ecx
	movl	%ecx, 4(%rbx)
	addl	%r15d, %edx
	sarl	%edx
	jmp	.LBB6_73
.LBB6_71:
	movl	$-1, %r10d
.LBB6_72:                               # %thread-pre-split.thread173
	cmpl	$0, 15420(%rax)
	movl	%r10d, %edx
	jne	.LBB6_73
	jmp	.LBB6_74
.LBB6_61:                               # %.thread169
	movl	$0, (%rbx)
	jmp	.LBB6_72
.LBB6_62:
	decl	%edi
.LBB6_63:
	movl	%edi, 4(%rbx)
	movl	$1, (%rbx)
.LBB6_73:                               # %.thread
	decl	%r15d
	decl	%r8d
	andl	%esi, %r8d
	movl	%r8d, 8(%rbx)
	andl	%edx, %r15d
	movl	%r15d, 12(%rbx)
	movl	4(%rbx), %edi
	leaq	16(%rbx), %rsi
	leaq	20(%rbx), %rdx
	callq	*get_mb_block_pos(%rip)
	movq	img(%rip), %rax
	movl	16(%rbx), %ecx
	imull	15552(%rax,%r14,8), %ecx
	movl	%ecx, 16(%rbx)
	movl	20(%rbx), %edx
	imull	15556(%rax,%r14,8), %edx
	addl	8(%rbx), %ecx
	movl	%ecx, 16(%rbx)
	addl	12(%rbx), %edx
	movl	%edx, 20(%rbx)
.LBB6_74:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_66:
	movl	%r15d, %eax
	sarl	%eax
	cmpl	%edx, %eax
	jle	.LBB6_76
# BB#67:
	leal	1(%rdx,%rdx), %edx
	jmp	.LBB6_73
.LBB6_70:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	jmp	.LBB6_73
.LBB6_75:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	addl	%edx, %edx
	subl	%r15d, %edx
	jmp	.LBB6_73
.LBB6_76:
	incl	%r10d
	movl	%r10d, 4(%rbx)
	leal	1(%rdx,%rdx), %edx
	subl	%r15d, %edx
	jmp	.LBB6_73
.Lfunc_end6:
	.size	getAffNeighbour, .Lfunc_end6-getAffNeighbour
	.cfi_endproc

	.globl	getLuma4x4Neighbour
	.p2align	4, 0x90
	.type	getLuma4x4Neighbour,@function
getLuma4x4Neighbour:                    # @getLuma4x4Neighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, (%rbx)
	je	.LBB7_2
# BB#1:
	movdqu	8(%rbx), %xmm0
	psrad	$2, %xmm0
	movdqu	%xmm0, 8(%rbx)
.LBB7_2:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	getLuma4x4Neighbour, .Lfunc_end7-getLuma4x4Neighbour
	.cfi_endproc

	.globl	getChroma4x4Neighbour
	.p2align	4, 0x90
	.type	getChroma4x4Neighbour,@function
getChroma4x4Neighbour:                  # @getChroma4x4Neighbour
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$1, %ecx
	movq	%rbx, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, (%rbx)
	je	.LBB8_2
# BB#1:
	movdqu	8(%rbx), %xmm0
	psrad	$2, %xmm0
	movdqu	%xmm0, 8(%rbx)
.LBB8_2:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	getChroma4x4Neighbour, .Lfunc_end8-getChroma4x4Neighbour
	.cfi_endproc

	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
