	.text
	.file	"struct_innerprod.bc"
	.globl	hypre_StructInnerProd
	.p2align	4, 0x90
	.type	hypre_StructInnerProd,@function
hypre_StructInnerProd:                  # @hypre_StructInnerProd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	$0, 96(%rsp)
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB0_22
# BB#1:                                 # %.lr.ph336
	xorpd	%xmm2, %xmm2
	xorl	%esi, %esi
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_13 Depth 4
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movq	(%rcx), %rcx
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rbp), %rdi
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	(%rax), %r15
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	(%rax), %r12
	movq	40(%rcx), %rax
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rdx), %rax
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	108(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r12,%rbp), %r9d
	movl	4(%r12,%rbp), %esi
	movl	12(%r12,%rbp), %eax
	movl	%eax, %ecx
	subl	%r9d, %ecx
	incl	%ecx
	cmpl	%r9d, %eax
	movl	16(%r12,%rbp), %eax
	movl	$0, %edx
	cmovsl	%edx, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%eax, %r13d
	subl	%esi, %r13d
	incl	%r13d
	cmpl	%esi, %eax
	movl	(%r15,%rbp), %edi
	movl	4(%r15,%rbp), %ebx
	movl	12(%r15,%rbp), %eax
	cmovsl	%edx, %r13d
	movl	%eax, %r14d
	subl	%edi, %r14d
	incl	%r14d
	cmpl	%edi, %eax
	movl	16(%r15,%rbp), %ecx
	cmovsl	%edx, %r14d
	movl	%ecx, %eax
	subl	%ebx, %eax
	incl	%eax
	cmpl	%ebx, %ecx
	cmovsl	%edx, %eax
	movl	108(%rsp), %ecx
	movl	112(%rsp), %r11d
	movl	116(%rsp), %r10d
	cmpl	%ecx, %r11d
	movl	%ecx, %edx
	cmovgel	%r11d, %edx
	cmpl	%edx, %r10d
	cmovgel	%r10d, %edx
	testl	%edx, %edx
	jle	.LBB0_20
# BB#3:                                 # %.lr.ph330
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r10d, %r10d
	jle	.LBB0_20
# BB#4:                                 # %.lr.ph330
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r11d, %r11d
	jle	.LBB0_20
# BB#5:                                 # %.preheader284.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	8(%rsi,%rbp), %r8d
	movl	%r8d, %edx
	subl	8(%r12,%rbp), %edx
	movl	%r9d, %ebx
	movl	%edx, %edi
	subl	8(%r15,%rbp), %r8d
	movl	%r10d, 44(%rsp)         # 4-byte Spill
	movl	(%rsi,%rbp), %r9d
	movl	4(%rsi,%rbp), %r12d
	movl	%r9d, %edx
	subl	%ebx, %edx
	movl	%r12d, %ebp
	subl	12(%rsp), %ebp          # 4-byte Folded Reload
	movl	%r14d, %r15d
	subl	%ecx, %r15d
	imull	%r13d, %edi
	addl	%ebp, %edi
	movq	%r11, %rbx
	movl	20(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	imull	%r10d, %edi
	addl	%edx, %edi
	movl	%eax, %edx
	subl	%ebx, %edx
	imull	%r14d, %edx
	movl	%edx, 64(%rsp)          # 4-byte Spill
	subl	%ebx, %r13d
	imull	%r10d, %r13d
	movl	%r13d, 56(%rsp)         # 4-byte Spill
	subl	16(%rsp), %r9d          # 4-byte Folded Reload
	leal	-1(%rbx), %edx
	subl	32(%rsp), %r12d         # 4-byte Folded Reload
	movl	%r15d, %esi
	imull	%edx, %esi
	addl	%r14d, %esi
	subl	%ecx, %esi
	movl	%esi, 84(%rsp)          # 4-byte Spill
	imull	%ebp, %edx
	addl	%r10d, %edx
	subl	%ecx, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	imull	%eax, %r8d
	leal	-1(%rcx), %r13d
	incq	%r13
	addl	%r12d, %r8d
	movl	%r14d, %eax
	imull	%ebx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	imull	%ebx, %r10d
	movl	%r10d, 20(%rsp)         # 4-byte Spill
	imull	%r14d, %r8d
	addl	%r9d, %r8d
	movl	%r8d, %ebx
	movl	44(%rsp), %r8d          # 4-byte Reload
	movl	%ecx, %r14d
	andl	$1, %r14d
	xorl	%esi, %esi
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader284.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_13 Depth 4
	testl	%ecx, %ecx
	jle	.LBB0_16
# BB#7:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%esi, 32(%rsp)          # 4-byte Spill
	xorl	%r8d, %r8d
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	%ebx, %r9d
	movl	%edi, 72(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	movq	144(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_13 Depth 4
	movslq	%r9d, %r9
	testl	%r14d, %r14d
	movslq	%eax, %r12
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=3
	movq	%r9, %rdi
	movapd	%xmm2, %xmm0
	xorl	%r10d, %r10d
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=3
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r9,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	(%rsi,%r12,8), %xmm0
	addsd	%xmm0, %xmm2
	leaq	1(%r9), %rdi
	incq	%r12
	movl	$1, %r10d
	movapd	%xmm2, %xmm0
.LBB0_11:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_8 Depth=3
	addq	%r13, %r9
	cmpl	$1, %ecx
	je	.LBB0_14
# BB#12:                                # %.preheader.us.us.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	8(%rsi,%r12,8), %rdx
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	8(%rbx,%rdi,8), %rdi
	movl	%ecx, %ebx
	subl	%r10d, %ebx
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rdi), %xmm2           # xmm2 = mem[0],zero
	mulsd	-8(%rdx), %xmm1
	addsd	%xmm0, %xmm1
	mulsd	(%rdx), %xmm2
	addsd	%xmm1, %xmm2
	addq	$16, %rdx
	addq	$16, %rdi
	addl	$-2, %ebx
	movapd	%xmm2, %xmm0
	jne	.LBB0_13
.LBB0_14:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_8 Depth=3
	addl	%r13d, %eax
	addl	%r15d, %r9d
	addl	%ebp, %eax
	incl	%r8d
	movq	%r11, %rdx
	cmpl	%edx, %r8d
	jne	.LBB0_8
# BB#15:                                #   in Loop: Header=BB0_6 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %ebx          # 4-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	movl	32(%rsp), %esi          # 4-byte Reload
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_6 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	84(%rsp), %edx          # 4-byte Reload
.LBB0_17:                               # %._crit_edge295.us
                                        #   in Loop: Header=BB0_6 Depth=2
	addl	%ebx, %edx
	addl	%edi, %eax
	addl	64(%rsp), %edx          # 4-byte Folded Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	incl	%esi
	cmpl	%r8d, %esi
	movl	%edx, %ebx
	movl	%eax, %edi
	jne	.LBB0_6
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_21:                               # %._crit_edge331
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	136(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movq	120(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_2
	jmp	.LBB0_23
.LBB0_22:
	xorpd	%xmm2, %xmm2
.LBB0_23:                               # %._crit_edge337
	movsd	%xmm2, 96(%rsp)
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %r9d
	leaq	96(%rsp), %rdi
	movl	$final_innerprod_result, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	hypre_MPI_Allreduce
	movl	72(%rbx), %edi
	addl	%edi, %edi
	callq	hypre_IncFLOPCount
	movsd	final_innerprod_result(%rip), %xmm0 # xmm0 = mem[0],zero
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructInnerProd, .Lfunc_end0-hypre_StructInnerProd
	.cfi_endproc

	.type	final_innerprod_result,@object # @final_innerprod_result
	.comm	final_innerprod_result,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
