	.text
	.file	"mtxutl.bc"
	.globl	MtxuntDouble
	.p2align	4, 0x90
	.type	MtxuntDouble,@function
MtxuntDouble:                           # @MtxuntDouble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r15
	testl	%esi, %esi
	jle	.LBB0_15
# BB#1:                                 # %.preheader18.us.preheader
	leal	-1(%rsi), %eax
	leaq	8(,%rax,8), %rbx
	movl	%esi, %r14d
	leaq	-1(%r14), %r13
	movq	%r14, %r12
	andq	$7, %r12
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB0_2
# BB#3:                                 # %.preheader18.us.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader18.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB0_4
	jmp	.LBB0_5
.LBB0_2:
	xorl	%ebp, %ebp
.LBB0_5:                                # %.preheader18.us.prol.loopexit
	cmpq	$7, %r13
	jb	.LBB0_8
# BB#6:                                 # %.preheader18.us.preheader.new
	movq	%r14, %r13
	subq	%rbp, %r13
	leaq	56(%r15,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader18.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-48(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-40(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-32(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-24(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addq	$64, %rbp
	addq	$-8, %r13
	jne	.LBB0_7
.LBB0_8:                                # %.preheader
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_15
# BB#9:                                 # %.lr.ph.preheader
	leaq	-1(%r14), %rcx
	movq	%r14, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB0_12
# BB#10:                                # %.lr.ph.prol.preheader
	movabsq	$4607182418800017408, %rsi # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rax,8), %rdi
	movq	%rsi, (%rdi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB0_11
.LBB0_12:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_15
# BB#13:                                # %.lr.ph.preheader.new
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rax,8), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	8(%r15,%rax,8), %rdx
	movq	%rcx, 8(%rdx,%rax,8)
	movq	16(%r15,%rax,8), %rdx
	movq	%rcx, 16(%rdx,%rax,8)
	movq	24(%r15,%rax,8), %rdx
	movq	%rcx, 24(%rdx,%rax,8)
	movq	32(%r15,%rax,8), %rdx
	movq	%rcx, 32(%rdx,%rax,8)
	movq	40(%r15,%rax,8), %rdx
	movq	%rcx, 40(%rdx,%rax,8)
	movq	48(%r15,%rax,8), %rdx
	movq	%rcx, 48(%rdx,%rax,8)
	movq	56(%r15,%rax,8), %rdx
	movq	%rcx, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r14
	jne	.LBB0_14
.LBB0_15:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MtxuntDouble, .Lfunc_end0-MtxuntDouble
	.cfi_endproc

	.globl	MtxmltDouble
	.p2align	4, 0x90
	.type	MtxmltDouble,@function
MtxmltDouble:                           # @MtxmltDouble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r15
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	%r13d, %r12
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, %rbx
	testl	%r12d, %r12d
	jle	.LBB1_9
# BB#1:                                 # %.preheader41.lr.ph
	leal	-1(%r13), %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r13d, %r12d
	leaq	-1(%r12), %r14
	movl	%r12d, %ebp
	andl	$3, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.lr.ph.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_5 Depth 3
                                        #       Child Loop BB1_8 Depth 3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	memcpy
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader.us.us
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_5 Depth 3
                                        #       Child Loop BB1_8 Depth 3
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbx,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r15,%rcx,8), %rdx
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm1, %xmm0
	incq	%rcx
	cmpq	%rcx, %rbp
	jne	.LBB1_5
.LBB1_6:                                # %.prol.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	$3, %r14
	jb	.LBB1_7
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbx,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r15,%rcx,8), %rdx
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	8(%rbx,%rcx,8), %xmm0   # xmm0 = mem[0],zero
	movq	8(%r15,%rcx,8), %rdx
	mulsd	(%rdx,%rax,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	16(%rbx,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	movq	16(%r15,%rcx,8), %rdx
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	24(%rbx,%rcx,8), %xmm0  # xmm0 = mem[0],zero
	movq	24(%r15,%rcx,8), %rdx
	mulsd	(%rdx,%rax,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$4, %rcx
	cmpq	%rcx, %r12
	jne	.LBB1_8
.LBB1_7:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB1_4 Depth=2
	movsd	%xmm0, (%r13,%rax,8)
	incq	%rax
	cmpq	%rcx, %rax
	jne	.LBB1_4
# BB#2:                                 # %._crit_edge47.us
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	%r12, %rcx
	jne	.LBB1_3
.LBB1_9:                                # %._crit_edge49
	movq	%rbx, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	MtxmltDouble, .Lfunc_end1-MtxmltDouble
	.cfi_endproc

	.globl	AllocateCharVec
	.p2align	4, 0x90
	.type	AllocateCharVec,@function
AllocateCharVec:                        # @AllocateCharVec
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	movl	$1, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	popq	%rbx
	retq
.LBB2_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	AllocateCharVec, .Lfunc_end2-AllocateCharVec
	.cfi_endproc

	.globl	ReallocateCharMtx
	.p2align	4, 0x90
	.type	ReallocateCharMtx,@function
ReallocateCharMtx:                      # @ReallocateCharMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -48
.Lcfi34:
	.cfi_offset %r12, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	testl	%r15d, %r15d
	jle	.LBB3_5
# BB#1:                                 # %.lr.ph
	leal	1(%r14), %eax
	movslq	%eax, %r12
	movl	%r15d, %ebp
	jmp	.LBB3_2
.LBB3_3:                                #   in Loop: Header=BB3_2 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	callq	realloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB3_3
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB3_2
.LBB3_5:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ReallocateCharMtx, .Lfunc_end3-ReallocateCharMtx
	.cfi_endproc

	.globl	AllocateCharMtx
	.p2align	4, 0x90
	.type	AllocateCharMtx,@function
AllocateCharMtx:                        # @AllocateCharMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	leal	1(%r15), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_8
# BB#1:
	testl	%r14d, %r14d
	je	.LBB4_3
# BB#2:
	testl	%r15d, %r15d
	jle	.LBB4_3
# BB#4:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB4_9
# BB#6:                                 # %AllocateCharVec.exit
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	%rax, (%r12,%rbx,8)
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB4_5
	jmp	.LBB4_7
.LBB4_3:                                # %..loopexit_crit_edge
	movslq	%r15d, %r15
.LBB4_7:                                # %.loopexit
	movq	$0, (%r12,%r15,8)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB4_8:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	AllocateCharMtx, .Lfunc_end4-AllocateCharMtx
	.cfi_endproc

	.globl	FreeCharMtx
	.p2align	4, 0x90
	.type	FreeCharMtx,@function
FreeCharMtx:                            # @FreeCharMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end5:
	.size	FreeCharMtx, .Lfunc_end5-FreeCharMtx
	.cfi_endproc

	.globl	AllocateFloatVec
	.p2align	4, 0x90
	.type	AllocateFloatVec,@function
AllocateFloatVec:                       # @AllocateFloatVec
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 16
.Lcfi54:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	%ebx, %edi
	movl	$4, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB6_2
# BB#1:
	popq	%rbx
	retq
.LBB6_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	AllocateFloatVec, .Lfunc_end6-AllocateFloatVec
	.cfi_endproc

	.globl	FreeFloatVec
	.p2align	4, 0x90
	.type	FreeFloatVec,@function
FreeFloatVec:                           # @FreeFloatVec
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	FreeFloatVec, .Lfunc_end7-FreeFloatVec
	.cfi_endproc

	.globl	AllocateFloatHalfMtx
	.p2align	4, 0x90
	.type	AllocateFloatHalfMtx,@function
AllocateFloatHalfMtx:                   # @AllocateFloatHalfMtx
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 48
.Lcfi60:
	.cfi_offset %rbx, -48
.Lcfi61:
	.cfi_offset %r12, -40
.Lcfi62:
	.cfi_offset %r13, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movl	%edi, %r14d
	leal	1(%r14), %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB8_3
# BB#1:                                 # %.preheader
	movslq	%r14d, %r12
	testl	%r14d, %r14d
	jle	.LBB8_8
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB8_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
	movq	%r13, %rdi
	callq	calloc
	movq	%rax, (%r15,%rbx,8)
	testq	%rax, %rax
	je	.LBB8_7
# BB#5:                                 #   in Loop: Header=BB8_6 Depth=1
	incq	%rbx
	decq	%r13
	cmpq	%r12, %rbx
	jl	.LBB8_6
.LBB8_8:                                # %._crit_edge
	movq	$0, (%r15,%r12,8)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_7:
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
.LBB8_4:
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB8_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	jmp	.LBB8_4
.Lfunc_end8:
	.size	AllocateFloatHalfMtx, .Lfunc_end8-AllocateFloatHalfMtx
	.cfi_endproc

	.globl	AllocateFloatMtx
	.p2align	4, 0x90
	.type	AllocateFloatMtx,@function
AllocateFloatMtx:                       # @AllocateFloatMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 64
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	leal	1(%r15), %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB9_1
# BB#3:
	testl	%r14d, %r14d
	je	.LBB9_5
# BB#4:
	testl	%r15d, %r15d
	jle	.LBB9_5
# BB#8:                                 # %.lr.ph
	movslq	%r14d, %rbx
	movslq	%r15d, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_9:                                # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, (%r12,%rbp,8)
	testq	%rax, %rax
	je	.LBB9_10
# BB#7:                                 #   in Loop: Header=BB9_9 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB9_9
	jmp	.LBB9_6
.LBB9_5:                                # %..loopexit_crit_edge
	movslq	%r15d, %r13
.LBB9_6:                                # %.loopexit
	movq	$0, (%r12,%r13,8)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
.LBB9_2:
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB9_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	jmp	.LBB9_2
.Lfunc_end9:
	.size	AllocateFloatMtx, .Lfunc_end9-AllocateFloatMtx
	.cfi_endproc

	.globl	FreeFloatHalfMtx
	.p2align	4, 0x90
	.type	FreeFloatHalfMtx,@function
FreeFloatHalfMtx:                       # @FreeFloatHalfMtx
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB10_5
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	free
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	addq	$8, %rbx
	decq	%r15
	jne	.LBB10_2
.LBB10_5:                               # %._crit_edge
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end10:
	.size	FreeFloatHalfMtx, .Lfunc_end10-FreeFloatHalfMtx
	.cfi_endproc

	.globl	FreeFloatMtx
	.p2align	4, 0x90
	.type	FreeFloatMtx,@function
FreeFloatMtx:                           # @FreeFloatMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB11_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB11_2
.LBB11_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end11:
	.size	FreeFloatMtx, .Lfunc_end11-FreeFloatMtx
	.cfi_endproc

	.globl	AllocateIntVec
	.p2align	4, 0x90
	.type	AllocateIntVec,@function
AllocateIntVec:                         # @AllocateIntVec
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	movl	$4, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB12_2
# BB#1:
	popq	%rbx
	retq
.LBB12_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end12:
	.size	AllocateIntVec, .Lfunc_end12-AllocateIntVec
	.cfi_endproc

	.globl	FreeIntVec
	.p2align	4, 0x90
	.type	FreeIntVec,@function
FreeIntVec:                             # @FreeIntVec
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	FreeIntVec, .Lfunc_end13-FreeIntVec
	.cfi_endproc

	.globl	AllocateFloatTri
	.p2align	4, 0x90
	.type	AllocateFloatTri,@function
AllocateFloatTri:                       # @AllocateFloatTri
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -32
.Lcfi95:
	.cfi_offset %r14, -24
.Lcfi96:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	leal	1(%rbx), %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB14_6
# BB#1:                                 # %.preheader
	movslq	%ebx, %r15
	testl	%ebx, %ebx
	jle	.LBB14_5
# BB#2:                                 # %.lr.ph.preheader
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB14_7
# BB#4:                                 # %AllocateFloatVec.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	%rax, -24(%r14,%rbx,8)
	leaq	1(%rbx), %rax
	addq	$-2, %rbx
	cmpq	%r15, %rbx
	movq	%rax, %rbx
	jl	.LBB14_3
.LBB14_5:                               # %._crit_edge
	movq	$0, (%r14,%r15,8)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_7:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB14_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end14:
	.size	AllocateFloatTri, .Lfunc_end14-AllocateFloatTri
	.cfi_endproc

	.globl	FreeFloatTri
	.p2align	4, 0x90
	.type	FreeFloatTri,@function
FreeFloatTri:                           # @FreeFloatTri
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB15_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB15_2
.LBB15_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end15:
	.size	FreeFloatTri, .Lfunc_end15-FreeFloatTri
	.cfi_endproc

	.globl	AllocateIntMtx
	.p2align	4, 0x90
	.type	AllocateIntMtx,@function
AllocateIntMtx:                         # @AllocateIntMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -48
.Lcfi108:
	.cfi_offset %r12, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	leal	1(%r15), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB16_8
# BB#1:
	testl	%r14d, %r14d
	je	.LBB16_3
# BB#2:
	testl	%r15d, %r15d
	jle	.LBB16_3
# BB#4:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_5:                               # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
	movq	%rbp, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB16_9
# BB#6:                                 # %AllocateIntVec.exit
                                        #   in Loop: Header=BB16_5 Depth=1
	movq	%rax, (%r12,%rbx,8)
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB16_5
	jmp	.LBB16_7
.LBB16_3:                               # %..loopexit_crit_edge
	movslq	%r15d, %r15
.LBB16_7:                               # %.loopexit
	movq	$0, (%r12,%r15,8)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB16_8:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end16:
	.size	AllocateIntMtx, .Lfunc_end16-AllocateIntMtx
	.cfi_endproc

	.globl	AllocateCharCub
	.p2align	4, 0x90
	.type	AllocateCharCub,@function
AllocateCharCub:                        # @AllocateCharCub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 64
.Lcfi119:
	.cfi_offset %rbx, -56
.Lcfi120:
	.cfi_offset %r12, -48
.Lcfi121:
	.cfi_offset %r13, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %r13d
	movl	%edi, %r14d
	leal	1(%r14), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB17_6
# BB#1:
	testl	%r13d, %r13d
	je	.LBB17_5
# BB#2:
	testl	%r14d, %r14d
	jle	.LBB17_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB17_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	AllocateCharMtx
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB17_4
.LBB17_5:                               # %.loopexit
	movslq	%r14d, %rax
	movq	$0, (%r15,%rax,8)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_6:
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end17:
	.size	AllocateCharCub, .Lfunc_end17-AllocateCharCub
	.cfi_endproc

	.globl	FreeCharCub
	.p2align	4, 0x90
	.type	FreeCharCub,@function
FreeCharCub:                            # @FreeCharCub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 48
.Lcfi130:
	.cfi_offset %rbx, -40
.Lcfi131:
	.cfi_offset %r12, -32
.Lcfi132:
	.cfi_offset %r14, -24
.Lcfi133:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.LBB18_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB18_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	leaq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph.i
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB18_4
.LBB18_5:                               # %FreeCharMtx.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	8(%r14,%r12,8), %r15
	incq	%r12
	testq	%r15, %r15
	jne	.LBB18_2
.LBB18_6:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end18:
	.size	FreeCharCub, .Lfunc_end18-FreeCharCub
	.cfi_endproc

	.globl	freeintmtx
	.p2align	4, 0x90
	.type	freeintmtx,@function
freeintmtx:                             # @freeintmtx
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 32
.Lcfi137:
	.cfi_offset %rbx, -32
.Lcfi138:
	.cfi_offset %r14, -24
.Lcfi139:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB19_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	free
	addq	$8, %rbx
	decq	%r15
	jne	.LBB19_2
.LBB19_3:                               # %._crit_edge
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end19:
	.size	freeintmtx, .Lfunc_end19-freeintmtx
	.cfi_endproc

	.globl	FreeIntMtx
	.p2align	4, 0x90
	.type	FreeIntMtx,@function
FreeIntMtx:                             # @FreeIntMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -24
.Lcfi144:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB20_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB20_2
.LBB20_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end20:
	.size	FreeIntMtx, .Lfunc_end20-FreeIntMtx
	.cfi_endproc

	.globl	AllocateCharHcu
	.p2align	4, 0x90
	.type	AllocateCharHcu,@function
AllocateCharHcu:                        # @AllocateCharHcu
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 96
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movl	%esi, %ebx
	movl	%edi, %r13d
	leal	1(%r13), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB21_13
# BB#1:                                 # %.preheader
	testl	%r13d, %r13d
	jle	.LBB21_2
# BB#3:                                 # %.lr.ph
	leal	1(%rbx), %eax
	cltq
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	%r13d, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jle	.LBB21_5
# BB#4:                                 # %.lr.ph
	testl	%ebp, %ebp
	je	.LBB21_5
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%ebx, %r15d
	movslq	%ebx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%r12, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_11 Depth 2
	movl	$8, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB21_7
# BB#10:                                # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB21_11:                              # %.lr.ph.i.us
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	AllocateCharMtx
	movq	%rax, (%rbx,%r12,8)
	incq	%r12
	cmpq	%r12, %r15
	jne	.LBB21_11
# BB#12:                                # %AllocateCharCub.exit.loopexit.us
                                        #   in Loop: Header=BB21_9 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rbx,%rax,8)
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%rbx, (%r12,%r13,8)
	incq	%r13
	movq	(%rsp), %rax            # 8-byte Reload
	cmpq	%rax, %r13
	jl	.LBB21_9
	jmp	.LBB21_15
.LBB21_2:                               # %.preheader.._crit_edge_crit_edge
	movslq	%r13d, %rax
	jmp	.LBB21_15
.LBB21_5:                               # %.lr.ph.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	calloc
	testq	%rax, %rax
	je	.LBB21_7
# BB#14:                                # %AllocateCharCub.exit
                                        #   in Loop: Header=BB21_6 Depth=1
	movq	%rax, (%r12,%rbx,8)
	incq	%rbx
	movq	(%rsp), %rax            # 8-byte Reload
	cmpq	%rax, %rbx
	jl	.LBB21_6
.LBB21_15:                              # %._crit_edge
	movq	$0, (%r12,%rax,8)
	movq	%r12, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_7:                               # %.us-lcssa.us
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB21_13:
	movl	$1, %edi
	callq	exit
.Lfunc_end21:
	.size	AllocateCharHcu, .Lfunc_end21-AllocateCharHcu
	.cfi_endproc

	.globl	FreeCharHcu
	.p2align	4, 0x90
	.type	FreeCharHcu,@function
FreeCharHcu:                            # @FreeCharHcu
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 64
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.LBB22_9
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB22_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_4 Depth 2
                                        #       Child Loop BB22_6 Depth 3
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.LBB22_8
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_4:                               # %.lr.ph.i
                                        #   Parent Loop BB22_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_6 Depth 3
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB22_7
# BB#5:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=2
	leaq	8(%r13), %rbp
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph.i.i
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	callq	free
	movq	(%rbp), %rdi
	addq	$8, %rbp
	testq	%rdi, %rdi
	jne	.LBB22_6
.LBB22_7:                               # %FreeCharMtx.exit.i
                                        #   in Loop: Header=BB22_4 Depth=2
	movq	%r13, %rdi
	callq	free
	movq	8(%r15,%rbx,8), %r13
	incq	%rbx
	testq	%r13, %r13
	jne	.LBB22_4
.LBB22_8:                               # %FreeCharCub.exit
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	8(%r14,%r12,8), %r15
	incq	%r12
	testq	%r15, %r15
	jne	.LBB22_2
.LBB22_9:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end22:
	.size	FreeCharHcu, .Lfunc_end22-FreeCharHcu
	.cfi_endproc

	.globl	AllocateDoubleVec
	.p2align	4, 0x90
	.type	AllocateDoubleVec,@function
AllocateDoubleVec:                      # @AllocateDoubleVec
	.cfi_startproc
# BB#0:
	movslq	%edi, %rdi
	movl	$8, %esi
	jmp	calloc                  # TAILCALL
.Lfunc_end23:
	.size	AllocateDoubleVec, .Lfunc_end23-AllocateDoubleVec
	.cfi_endproc

	.globl	FreeDoubleVec
	.p2align	4, 0x90
	.type	FreeDoubleVec,@function
FreeDoubleVec:                          # @FreeDoubleVec
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end24:
	.size	FreeDoubleVec, .Lfunc_end24-FreeDoubleVec
	.cfi_endproc

	.globl	AllocateIntCub
	.p2align	4, 0x90
	.type	AllocateIntCub,@function
AllocateIntCub:                         # @AllocateIntCub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 64
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %r13d
	movl	%edi, %r14d
	leal	1(%r14), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB25_5
# BB#1:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB25_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	AllocateIntMtx
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB25_3
.LBB25_4:                               # %._crit_edge
	movslq	%r14d, %rax
	movq	$0, (%r15,%rax,8)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end25:
	.size	AllocateIntCub, .Lfunc_end25-AllocateIntCub
	.cfi_endproc

	.globl	FreeIntCub
	.p2align	4, 0x90
	.type	FreeIntCub,@function
FreeIntCub:                             # @FreeIntCub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 48
.Lcfi189:
	.cfi_offset %rbx, -40
.Lcfi190:
	.cfi_offset %r12, -32
.Lcfi191:
	.cfi_offset %r14, -24
.Lcfi192:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.LBB26_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB26_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_4 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB26_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	leaq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph.i
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB26_4
.LBB26_5:                               # %FreeIntMtx.exit
                                        #   in Loop: Header=BB26_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	8(%r14,%r12,8), %r15
	incq	%r12
	testq	%r15, %r15
	jne	.LBB26_2
.LBB26_6:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end26:
	.size	FreeIntCub, .Lfunc_end26-FreeIntCub
	.cfi_endproc

	.globl	AllocateDoubleMtx
	.p2align	4, 0x90
	.type	AllocateDoubleMtx,@function
AllocateDoubleMtx:                      # @AllocateDoubleMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 48
.Lcfi198:
	.cfi_offset %rbx, -48
.Lcfi199:
	.cfi_offset %r12, -40
.Lcfi200:
	.cfi_offset %r14, -32
.Lcfi201:
	.cfi_offset %r15, -24
.Lcfi202:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, %r14d
	leal	1(%r14), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB27_6
# BB#1:
	testl	%ebp, %ebp
	je	.LBB27_5
# BB#2:
	testl	%r14d, %r14d
	jle	.LBB27_5
# BB#3:                                 # %.lr.ph
	movslq	%ebp, %r12
	movl	%r14d, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB27_4:                               # =>This Inner Loop Header: Depth=1
	movl	$8, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB27_4
.LBB27_5:                               # %.loopexit
	movslq	%r14d, %rax
	movq	$0, (%r15,%rax,8)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end27:
	.size	AllocateDoubleMtx, .Lfunc_end27-AllocateDoubleMtx
	.cfi_endproc

	.globl	FreeDoubleMtx
	.p2align	4, 0x90
	.type	FreeDoubleMtx,@function
FreeDoubleMtx:                          # @FreeDoubleMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi205:
	.cfi_def_cfa_offset 32
.Lcfi206:
	.cfi_offset %rbx, -24
.Lcfi207:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB28_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB28_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB28_2
.LBB28_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end28:
	.size	FreeDoubleMtx, .Lfunc_end28-FreeDoubleMtx
	.cfi_endproc

	.globl	AllocateFloatCub
	.p2align	4, 0x90
	.type	AllocateFloatCub,@function
AllocateFloatCub:                       # @AllocateFloatCub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi208:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi209:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi211:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi212:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi213:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi214:
	.cfi_def_cfa_offset 64
.Lcfi215:
	.cfi_offset %rbx, -56
.Lcfi216:
	.cfi_offset %r12, -48
.Lcfi217:
	.cfi_offset %r13, -40
.Lcfi218:
	.cfi_offset %r14, -32
.Lcfi219:
	.cfi_offset %r15, -24
.Lcfi220:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %r13d
	movl	%edi, %r14d
	leal	1(%r14), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB29_5
# BB#1:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB29_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB29_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	AllocateFloatMtx
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB29_3
.LBB29_4:                               # %._crit_edge
	movslq	%r14d, %rax
	movq	$0, (%r15,%rax,8)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end29:
	.size	AllocateFloatCub, .Lfunc_end29-AllocateFloatCub
	.cfi_endproc

	.globl	FreeFloatCub
	.p2align	4, 0x90
	.type	FreeFloatCub,@function
FreeFloatCub:                           # @FreeFloatCub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi221:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi223:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi224:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi225:
	.cfi_def_cfa_offset 48
.Lcfi226:
	.cfi_offset %rbx, -40
.Lcfi227:
	.cfi_offset %r12, -32
.Lcfi228:
	.cfi_offset %r14, -24
.Lcfi229:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.LBB30_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB30_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_4 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB30_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB30_2 Depth=1
	leaq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB30_4:                               # %.lr.ph.i
                                        #   Parent Loop BB30_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB30_4
.LBB30_5:                               # %FreeFloatMtx.exit
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	8(%r14,%r12,8), %r15
	incq	%r12
	testq	%r15, %r15
	jne	.LBB30_2
.LBB30_6:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end30:
	.size	FreeFloatCub, .Lfunc_end30-FreeFloatCub
	.cfi_endproc

	.globl	AllocateDoubleCub
	.p2align	4, 0x90
	.type	AllocateDoubleCub,@function
AllocateDoubleCub:                      # @AllocateDoubleCub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi230:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi233:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi234:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi236:
	.cfi_def_cfa_offset 80
.Lcfi237:
	.cfi_offset %rbx, -56
.Lcfi238:
	.cfi_offset %r12, -48
.Lcfi239:
	.cfi_offset %r13, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r14d
	movl	%edi, %ebx
	leal	1(%rbx), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB31_13
# BB#1:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB31_2
# BB#3:                                 # %.lr.ph
	leal	1(%r14), %eax
	cltq
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%ebx, %rbp
	testl	%r14d, %r14d
	jle	.LBB31_5
# BB#4:                                 # %.lr.ph
	testl	%r13d, %r13d
	je	.LBB31_5
# BB#8:                                 # %.lr.ph.split.us.preheader
	movslq	%r13d, %r13
	movl	%r14d, %r12d
	movslq	%r14d, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_11 Depth 2
	movl	$8, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB31_7
# BB#10:                                # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB31_9 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_11:                              # %.lr.ph.i.us
                                        #   Parent Loop BB31_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %esi
	movq	%r13, %rdi
	callq	calloc
	movq	%rax, (%rbx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB31_11
# BB#12:                                # %AllocateDoubleMtx.exit.loopexit.us
                                        #   in Loop: Header=BB31_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, (%rbx,%rax,8)
	movq	%rbx, (%r15,%r14,8)
	incq	%r14
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmpq	%rbp, %r14
	jl	.LBB31_9
	jmp	.LBB31_16
.LBB31_2:                               # %.preheader.._crit_edge_crit_edge
	movslq	%ebx, %rbp
	jmp	.LBB31_16
.LBB31_5:                               # %.lr.ph.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB31_6:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	calloc
	testq	%rax, %rax
	je	.LBB31_7
# BB#15:                                # %AllocateDoubleMtx.exit
                                        #   in Loop: Header=BB31_6 Depth=1
	movq	%rax, (%r15,%rbx,8)
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB31_6
.LBB31_16:                              # %._crit_edge
	movq	$0, (%r15,%rbp,8)
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_7:                               # %.us-lcssa.us
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$26, %esi
.LBB31_14:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB31_13:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$29, %esi
	jmp	.LBB31_14
.Lfunc_end31:
	.size	AllocateDoubleCub, .Lfunc_end31-AllocateDoubleCub
	.cfi_endproc

	.globl	FreeDoubleCub
	.p2align	4, 0x90
	.type	FreeDoubleCub,@function
FreeDoubleCub:                          # @FreeDoubleCub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi245:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi247:
	.cfi_def_cfa_offset 48
.Lcfi248:
	.cfi_offset %rbx, -40
.Lcfi249:
	.cfi_offset %r12, -32
.Lcfi250:
	.cfi_offset %r14, -24
.Lcfi251:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.LBB32_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_4 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB32_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	leaq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB32_4:                               # %.lr.ph.i
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB32_4
.LBB32_5:                               # %FreeDoubleMtx.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	%r15, %rdi
	callq	free
	movq	8(%r14,%r12,8), %r15
	incq	%r12
	testq	%r15, %r15
	jne	.LBB32_2
.LBB32_6:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end32:
	.size	FreeDoubleCub, .Lfunc_end32-FreeDoubleCub
	.cfi_endproc

	.globl	AllocateShortVec
	.p2align	4, 0x90
	.type	AllocateShortVec,@function
AllocateShortVec:                       # @AllocateShortVec
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi252:
	.cfi_def_cfa_offset 16
.Lcfi253:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	movl	$2, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB33_2
# BB#1:
	popq	%rbx
	retq
.LBB33_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end33:
	.size	AllocateShortVec, .Lfunc_end33-AllocateShortVec
	.cfi_endproc

	.globl	FreeShortVec
	.p2align	4, 0x90
	.type	FreeShortVec,@function
FreeShortVec:                           # @FreeShortVec
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end34:
	.size	FreeShortVec, .Lfunc_end34-FreeShortVec
	.cfi_endproc

	.globl	AllocateShortMtx
	.p2align	4, 0x90
	.type	AllocateShortMtx,@function
AllocateShortMtx:                       # @AllocateShortMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi254:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi255:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi256:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi257:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi258:
	.cfi_def_cfa_offset 48
.Lcfi259:
	.cfi_offset %rbx, -48
.Lcfi260:
	.cfi_offset %r12, -40
.Lcfi261:
	.cfi_offset %r14, -32
.Lcfi262:
	.cfi_offset %r15, -24
.Lcfi263:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	leal	1(%r15), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB35_3
# BB#1:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB35_2
# BB#4:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB35_5:                               # =>This Inner Loop Header: Depth=1
	movl	$2, %esi
	movq	%rbp, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB35_8
# BB#6:                                 # %AllocateShortVec.exit
                                        #   in Loop: Header=BB35_5 Depth=1
	movq	%rax, (%r12,%rbx,8)
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB35_5
	jmp	.LBB35_7
.LBB35_2:                               # %.preheader.._crit_edge_crit_edge
	movslq	%r15d, %r15
.LBB35_7:                               # %._crit_edge
	movq	$0, (%r12,%r15,8)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_8:
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB35_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end35:
	.size	AllocateShortMtx, .Lfunc_end35-AllocateShortMtx
	.cfi_endproc

	.globl	FreeShortMtx
	.p2align	4, 0x90
	.type	FreeShortMtx,@function
FreeShortMtx:                           # @FreeShortMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi266:
	.cfi_def_cfa_offset 32
.Lcfi267:
	.cfi_offset %rbx, -24
.Lcfi268:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB36_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB36_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB36_2
.LBB36_3:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end36:
	.size	FreeShortMtx, .Lfunc_end36-FreeShortMtx
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Cannot allocate %d character vector.\n"
	.size	.L.str, 38

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Cannot reallocate %d x %d character matrix.\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Cannot allocate %d x %d character matrix.\n"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Allocation error ( %d fload vec )\n"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Allocation error ( %d fload halfmtx )\n"
	.size	.L.str.4, 39

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Allocation error( %d floathalfmtx )\n"
	.size	.L.str.5, 37

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Allocation error ( %d x %d fload mtx )\n"
	.size	.L.str.6, 40

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Allocation error( %d x %d floatmtx )\n"
	.size	.L.str.7, 38

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Allocation error( %d int vec )\n"
	.size	.L.str.8, 32

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Allocation error ( float tri )\n"
	.size	.L.str.9, 32

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Allocation error( %d x %d int mtx )\n"
	.size	.L.str.10, 37

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Allocation error( %d x %d x %d char cube\n"
	.size	.L.str.11, 42

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"cannot allocate IntCub\n"
	.size	.L.str.12, 24

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cannot allocate DoubleMtx\n"
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"cannot allocate float cube.\n"
	.size	.L.str.14, 29

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cannot allocate double cube.\n"
	.size	.L.str.15, 30

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Allocation error( %d short vec )\n"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Allocation error( %d x %d short mtx ) \n"
	.size	.L.str.17, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
