	.text
	.file	"csr_matrix.bc"
	.globl	hypre_CSRMatrixCreate
	.p2align	4, 0x90
	.type	hypre_CSRMatrixCreate,@function
hypre_CSRMatrixCreate:                  # @hypre_CSRMatrixCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movl	%edi, %ebx
	movl	$1, %edi
	movl	$56, %esi
	callq	hypre_CAlloc
	movq	$0, 40(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movl	%ebx, 24(%rax)
	movl	%ebp, 28(%rax)
	movl	%r14d, 32(%rax)
	movl	$1, 52(%rax)
	movl	%ebx, 48(%rax)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CSRMatrixCreate, .Lfunc_end0-hypre_CSRMatrixCreate
	.cfi_endproc

	.globl	hypre_CSRMatrixDestroy
	.p2align	4, 0x90
	.type	hypre_CSRMatrixDestroy,@function
hypre_CSRMatrixDestroy:                 # @hypre_CSRMatrixDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
# BB#1:
	movq	8(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 8(%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:
	callq	hypre_Free
	movq	$0, 40(%rbx)
.LBB1_3:
	cmpl	$0, 52(%rbx)
	je	.LBB1_5
# BB#4:
	movq	(%rbx), %rdi
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	16(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 16(%rbx)
.LBB1_5:
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB1_6:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	hypre_CSRMatrixDestroy, .Lfunc_end1-hypre_CSRMatrixDestroy
	.cfi_endproc

	.globl	hypre_CSRMatrixInitialize
	.p2align	4, 0x90
	.type	hypre_CSRMatrixInitialize,@function
hypre_CSRMatrixInitialize:              # @hypre_CSRMatrixInitialize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %r14d
	movl	32(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB2_3
# BB#1:
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB2_3
# BB#2:
	movl	$8, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
.LBB2_3:
	cmpq	$0, 8(%rbx)
	jne	.LBB2_5
# BB#4:
	incl	%r14d
	movl	$4, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	%rax, 8(%rbx)
.LBB2_5:
	testl	%ebp, %ebp
	je	.LBB2_8
# BB#6:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB2_8
# BB#7:
	movl	$4, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, 16(%rbx)
.LBB2_8:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_CSRMatrixInitialize, .Lfunc_end2-hypre_CSRMatrixInitialize
	.cfi_endproc

	.globl	hypre_CSRMatrixSetDataOwner
	.p2align	4, 0x90
	.type	hypre_CSRMatrixSetDataOwner,@function
hypre_CSRMatrixSetDataOwner:            # @hypre_CSRMatrixSetDataOwner
	.cfi_startproc
# BB#0:
	movl	%esi, 52(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_CSRMatrixSetDataOwner, .Lfunc_end3-hypre_CSRMatrixSetDataOwner
	.cfi_endproc

	.globl	hypre_CSRMatrixSetRownnz
	.p2align	4, 0x90
	.type	hypre_CSRMatrixSetRownnz,@function
hypre_CSRMatrixSetRownnz:               # @hypre_CSRMatrixSetRownnz
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r12, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	24(%r14), %r15
	testq	%r15, %r15
	jle	.LBB4_1
# BB#3:                                 # %.lr.ph.preheader
	movl	%r15d, %eax
	movq	8(%r14), %r12
	movl	(%r12), %esi
	cmpl	$8, %r15d
	jb	.LBB4_4
# BB#5:                                 # %min.iters.checked
	movl	%eax, %edx
	andl	$7, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB4_4
# BB#6:                                 # %vector.ph
	movd	%esi, %xmm0
	pshufd	$36, %xmm0, %xmm1       # xmm1 = xmm0[0,1,2,0]
	leaq	20(%r12), %rsi
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB4_7:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm3
	shufps	$3, %xmm3, %xmm1        # xmm1 = xmm1[3,0],xmm3[0,0]
	shufps	$152, %xmm3, %xmm1      # xmm1 = xmm1[0,2],xmm3[1,2]
	movaps	%xmm3, %xmm4
	pcmpgtd	%xmm1, %xmm3
	movups	(%rsi), %xmm1
	shufps	$3, %xmm1, %xmm4        # xmm4 = xmm4[3,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm4      # xmm4 = xmm4[0,2],xmm1[1,2]
	psrld	$31, %xmm3
	movaps	%xmm1, %xmm5
	pcmpgtd	%xmm4, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm3, %xmm0
	paddd	%xmm5, %xmm2
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB4_7
# BB#8:                                 # %middle.block
	paddd	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm2
	movd	%xmm2, %edi
	testl	%edx, %edx
	je	.LBB4_12
# BB#9:
	pshufd	$231, %xmm1, %xmm0      # xmm0 = xmm1[3,1,2,3]
	movd	%xmm0, %esi
	jmp	.LBB4_10
.LBB4_4:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
.LBB4_10:                               # %.lr.ph.preheader67
	subq	%rcx, %rax
	leaq	4(%r12,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	xorl	%ebx, %ebx
	cmpl	%esi, %edx
	setg	%bl
	addl	%ebx, %edi
	addq	$4, %rcx
	decq	%rax
	movl	%edx, %esi
	jne	.LBB4_11
.LBB4_12:                               # %._crit_edge
	movl	%edi, 48(%r14)
	xorl	%eax, %eax
	testl	%edi, %edi
	je	.LBB4_2
# BB#13:                                # %._crit_edge
	cmpl	%r15d, %edi
	je	.LBB4_2
# BB#14:
	movl	$4, %esi
	callq	hypre_CAlloc
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB4_15
.LBB4_1:                                # %._crit_edge.thread
	movl	$0, 48(%r14)
	xorl	%eax, %eax
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_18:                               #   in Loop: Header=BB4_15 Depth=1
	movl	%esi, (%rax,%rcx,4)
	incq	%rcx
.LBB4_15:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
	movslq	%edx, %rdx
	leal	-1(%rdx), %esi
	.p2align	4, 0x90
.LBB4_16:                               #   Parent Loop BB4_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r15, %rdx
	jge	.LBB4_2
# BB#17:                                #   in Loop: Header=BB4_16 Depth=2
	movl	4(%r12,%rdx,4), %edi
	incl	%esi
	cmpl	(%r12,%rdx,4), %edi
	leaq	1(%rdx), %rdx
	jle	.LBB4_16
	jmp	.LBB4_18
.LBB4_2:                                # %.loopexit
	movq	%rax, 40(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	hypre_CSRMatrixSetRownnz, .Lfunc_end4-hypre_CSRMatrixSetRownnz
	.cfi_endproc

	.globl	hypre_CSRMatrixRead
	.p2align	4, 0x90
	.type	hypre_CSRMatrixRead,@function
hypre_CSRMatrixRead:                    # @hypre_CSRMatrixRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %r14
	leaq	4(%rsp), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	4(%rsp), %edi
	incl	%edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, %r13
	movl	4(%rsp), %r15d
	testl	%r15d, %r15d
	js	.LBB5_3
# BB#1:                                 # %.lr.ph67.preheader
	movq	$-1, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph67
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r13,%rbx), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	decl	4(%r13,%rbp,4)
	movslq	4(%rsp), %r15
	incq	%rbp
	addq	$4, %rbx
	cmpq	%r15, %rbp
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge68
	movslq	%r15d, %rbx
	movl	(%r13,%rbx,4), %r12d
	movl	$1, %edi
	movl	$56, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	testq	%r12, %r12
	movq	$0, 40(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	movl	%ebx, 24(%rbp)
	movl	%ebx, 28(%rbp)
	movl	%r12d, 32(%rbp)
	movl	$1, 52(%rbp)
	movl	%ebx, 48(%rbp)
	movq	%r13, 8(%rbp)
	je	.LBB5_4
# BB#5:
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rbp)
	movq	8(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB5_8
	jmp	.LBB5_7
.LBB5_4:
	movq	%r13, %rax
	testq	%rax, %rax
	jne	.LBB5_8
.LBB5_7:
	incl	%r15d
	movl	$4, %esi
	movl	%r15d, %edi
	callq	hypre_CAlloc
	movq	%rax, 8(%rbp)
.LBB5_8:
	movq	16(%rbp), %rbx
	testl	%r12d, %r12d
	je	.LBB5_11
# BB#9:
	testq	%rbx, %rbx
	jne	.LBB5_11
# BB#10:
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movq	%rbx, 16(%rbp)
.LBB5_11:                               # %hypre_CSRMatrixInitialize.exit
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	testl	%r12d, %r12d
	jle	.LBB5_12
# BB#13:                                # %.lr.ph63.preheader
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %r12d
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	movl	(%rbx), %ebp
	decl	%ebp
	movl	%ebp, (%rbx)
	cmpl	%r12d, %ebp
	cmovll	%r12d, %ebp
	addq	$4, %rbx
	decq	%r15
	jne	.LBB5_14
# BB#15:                                # %._crit_edge64.loopexit
	incl	%ebp
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB5_16
.LBB5_12:
	movl	$1, %ebp
.LBB5_16:                               # %._crit_edge64
	movslq	4(%rsp), %rax
	cmpl	$0, (%r13,%rax,4)
	jle	.LBB5_19
# BB#17:                                # %.lr.ph.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fscanf
	incq	%rbx
	movslq	4(%rsp), %rax
	movslq	(%r13,%rax,4), %rax
	addq	$8, %r15
	cmpq	%rax, %rbx
	jl	.LBB5_18
.LBB5_19:                               # %._crit_edge
	movq	%r14, %rdi
	callq	fclose
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r12d, 32(%rax)
	movl	%ebp, 28(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_CSRMatrixRead, .Lfunc_end5-hypre_CSRMatrixRead
	.cfi_endproc

	.globl	hypre_CSRMatrixPrint
	.p2align	4, 0x90
	.type	hypre_CSRMatrixPrint,@function
hypre_CSRMatrixPrint:                   # @hypre_CSRMatrixPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rsi, %rax
	movq	(%rdi), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %r15
	movq	16(%rdi), %r13
	movslq	24(%rdi), %r14
	movl	$.L.str.3, %esi
	movq	%rax, %rdi
	callq	fopen
	movq	%rax, %rbx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	fprintf
	testq	%r14, %r14
	js	.LBB6_3
# BB#1:                                 # %.lr.ph53.preheader
	leal	1(%r14), %ebp
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %edx
	incl	%edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	addq	$4, %r12
	decq	%rbp
	jne	.LBB6_2
.LBB6_3:                                # %.preheader47
	movl	(%r15,%r14,4), %eax
	testl	%eax, %eax
	movq	(%rsp), %r12            # 8-byte Reload
	jle	.LBB6_6
# BB#4:                                 # %.lr.ph50.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph50
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbp,4), %edx
	incl	%edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	incq	%rbp
	movslq	(%r15,%r14,4), %rax
	cmpq	%rax, %rbp
	jl	.LBB6_5
.LBB6_6:                                # %._crit_edge
	testq	%r12, %r12
	je	.LBB6_10
# BB#7:                                 # %.preheader
	testl	%eax, %eax
	jle	.LBB6_11
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r12,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.5, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	incq	%rbp
	movslq	(%r15,%r14,4), %rax
	cmpq	%rax, %rbp
	jl	.LBB6_9
	jmp	.LBB6_11
.LBB6_10:
	movl	$.L.str.6, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB6_11:                               # %.loopexit
	movq	%rbx, %rdi
	callq	fclose
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_CSRMatrixPrint, .Lfunc_end6-hypre_CSRMatrixPrint
	.cfi_endproc

	.globl	hypre_CSRMatrixCopy
	.p2align	4, 0x90
	.type	hypre_CSRMatrixCopy,@function
hypre_CSRMatrixCopy:                    # @hypre_CSRMatrixCopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movslq	24(%rdi), %r8
	testq	%r8, %r8
	movl	%r8d, %r9d
	movq	8(%rdi), %r13
	movq	8(%rsi), %r10
	jle	.LBB7_5
# BB#1:                                 # %.lr.ph72.preheader
	movq	16(%rdi), %r14
	movq	16(%rsi), %rcx
	movl	(%r13), %ebp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph72
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movq	%r11, %rax
	movl	%ebp, (%r10,%rax,4)
	movslq	(%r13,%rax,4), %rbx
	leaq	1(%rax), %r11
	movl	4(%r13,%rax,4), %ebp
	cmpl	%ebp, %ebx
	jge	.LBB7_2
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph69
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rbx,4), %ebp
	movl	%ebp, (%rcx,%rbx,4)
	incq	%rbx
	movslq	4(%r13,%rax,4), %rbp
	cmpq	%rbp, %rbx
	jl	.LBB7_4
.LBB7_2:                                # %.loopexit63
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	%r9, %r11
	jne	.LBB7_3
.LBB7_5:                                # %._crit_edge
	movl	(%r13,%r8,4), %eax
	movl	%eax, (%r10,%r8,4)
	testl	%edx, %edx
	je	.LBB7_24
# BB#6:
	testl	%r9d, %r9d
	jle	.LBB7_24
# BB#7:                                 # %.lr.ph67.preheader
	movq	(%rdi), %r9
	movq	(%rsi), %rdx
	movl	(%r13), %r10d
	movl	%r8d, %r14d
	leaq	16(%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	16(%r9), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	112(%rdx), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	112(%r9), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph67
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
                                        #     Child Loop BB7_20 Depth 2
                                        #     Child Loop BB7_23 Depth 2
	movl	%r10d, %eax
	movl	4(%r13,%r11,4), %r10d
	incq	%r11
	cmpl	%r10d, %eax
	jge	.LBB7_8
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB7_9 Depth=1
	movslq	%r10d, %rdi
	movslq	%eax, %rsi
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	$4, %r15
	jb	.LBB7_23
# BB#11:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	%r15, %r12
	andq	$-4, %r12
	je	.LBB7_23
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_9 Depth=1
	leaq	(%rdx,%rsi,8), %rax
	leaq	(%r9,%rdi,8), %rcx
	cmpq	%rcx, %rax
	jae	.LBB7_14
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_9 Depth=1
	leaq	(%rdx,%rdi,8), %rax
	leaq	(%r9,%rsi,8), %rcx
	cmpq	%rax, %rcx
	jb	.LBB7_23
.LBB7_14:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_9 Depth=1
	leaq	-4(%r12), %rax
	movl	%eax, %ecx
	shrl	$2, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB7_15
# BB#16:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	-8(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbx
	movq	-16(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rbp
	negq	%rcx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB7_17:                               # %vector.body.prol
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbp,%r8,8), %xmm0
	movups	(%rbp,%r8,8), %xmm1
	movups	%xmm0, -16(%rbx,%r8,8)
	movups	%xmm1, (%rbx,%r8,8)
	addq	$4, %r8
	incq	%rcx
	jne	.LBB7_17
	jmp	.LBB7_18
.LBB7_15:                               #   in Loop: Header=BB7_9 Depth=1
	xorl	%r8d, %r8d
.LBB7_18:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_9 Depth=1
	cmpq	$12, %rax
	jb	.LBB7_21
# BB#19:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	%r12, %rbp
	subq	%r8, %rbp
	addq	%rsi, %r8
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,8), %rbx
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,8), %rcx
	.p2align	4, 0x90
.LBB7_20:                               # %vector.body
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-16, %rbp
	jne	.LBB7_20
.LBB7_21:                               # %middle.block
                                        #   in Loop: Header=BB7_9 Depth=1
	cmpq	%r12, %r15
	je	.LBB7_8
# BB#22:                                #   in Loop: Header=BB7_9 Depth=1
	addq	%r12, %rsi
	.p2align	4, 0x90
.LBB7_23:                               # %scalar.ph
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r9,%rsi,8), %rax
	movq	%rax, (%rdx,%rsi,8)
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB7_23
.LBB7_8:                                # %.loopexit
                                        #   in Loop: Header=BB7_9 Depth=1
	cmpq	%r14, %r11
	jne	.LBB7_9
.LBB7_24:                               # %.loopexit62
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	hypre_CSRMatrixCopy, .Lfunc_end7-hypre_CSRMatrixCopy
	.cfi_endproc

	.globl	hypre_CSRMatrixClone
	.p2align	4, 0x90
	.type	hypre_CSRMatrixClone,@function
hypre_CSRMatrixClone:                   # @hypre_CSRMatrixClone
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 64
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	24(%r15), %ebp
	movl	28(%r15), %ebx
	movl	32(%r15), %r13d
	movl	$1, %edi
	movl	$56, %esi
	callq	hypre_CAlloc
	movq	%rax, %r12
	testq	%r13, %r13
	movq	%r12, %r14
	movq	$0, 40(%r12)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	movq	$0, 16(%r12)
	movl	%ebp, 24(%r12)
	movl	%ebx, 28(%r12)
	movl	%r13d, 32(%r12)
	movl	$1, 52(%r12)
	movl	%ebp, 48(%r12)
	je	.LBB8_1
# BB#2:
	movl	$8, %esi
	movl	%r13d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%r14)
	addq	$8, %r12
	cmpq	$0, 8(%r14)
	jne	.LBB8_4
	jmp	.LBB8_3
.LBB8_1:                                # %.thread
	addq	$8, %r12
.LBB8_3:
	leal	1(%rbp), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%r12)
.LBB8_4:
	movq	16(%r14), %rax
	testl	%r13d, %r13d
	je	.LBB8_7
# BB#5:
	testq	%rax, %rax
	jne	.LBB8_7
# BB#6:
	movl	$4, %esi
	movl	%r13d, %edi
	callq	hypre_CAlloc
	movq	%rax, 16(%r14)
.LBB8_7:                                # %hypre_CSRMatrixInitialize.exit
	movq	16(%r15), %r9
	movq	(%r12), %r12
	testl	%ebp, %ebp
	js	.LBB8_22
# BB#8:                                 # %.lr.ph53.preheader
	movq	8(%r15), %rdx
	incl	%ebp
	cmpl	$7, %ebp
	jbe	.LBB8_9
# BB#16:                                # %min.iters.checked
	movl	%ebp, %r8d
	andl	$7, %r8d
	movq	%rbp, %rsi
	subq	%r8, %rsi
	je	.LBB8_9
# BB#17:                                # %vector.memcheck
	leaq	(%rdx,%rbp,4), %rcx
	cmpq	%rcx, %r12
	jae	.LBB8_19
# BB#18:                                # %vector.memcheck
	leaq	(%r12,%rbp,4), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB8_19
.LBB8_9:
	xorl	%esi, %esi
.LBB8_10:                               # %.lr.ph53.preheader129
	movl	%ebp, %ebx
	subl	%esi, %ebx
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rbx
	je	.LBB8_13
# BB#11:                                # %.lr.ph53.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph53.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rsi,4), %ecx
	movl	%ecx, (%r12,%rsi,4)
	incq	%rsi
	incq	%rbx
	jne	.LBB8_12
.LBB8_13:                               # %.lr.ph53.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB8_22
# BB#14:                                # %.lr.ph53.preheader129.new
	subq	%rsi, %rbp
	leaq	28(%r12,%rsi,4), %rdi
	leaq	28(%rdx,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %ecx
	movl	%ecx, -28(%rdi)
	movl	-24(%rdx), %ecx
	movl	%ecx, -24(%rdi)
	movl	-20(%rdx), %ecx
	movl	%ecx, -20(%rdi)
	movl	-16(%rdx), %ecx
	movl	%ecx, -16(%rdi)
	movl	-12(%rdx), %ecx
	movl	%ecx, -12(%rdi)
	movl	-8(%rdx), %ecx
	movl	%ecx, -8(%rdi)
	movl	-4(%rdx), %ecx
	movl	%ecx, -4(%rdi)
	movl	(%rdx), %ecx
	movl	%ecx, (%rdi)
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-8, %rbp
	jne	.LBB8_15
.LBB8_22:                               # %.preheader
	testl	%r13d, %r13d
	jle	.LBB8_37
# BB#23:                                # %.lr.ph.preheader
	cmpl	$7, %r13d
	jbe	.LBB8_24
# BB#31:                                # %min.iters.checked78
	movl	%r13d, %esi
	andl	$7, %esi
	movq	%r13, %rdx
	subq	%rsi, %rdx
	je	.LBB8_24
# BB#32:                                # %vector.memcheck91
	leaq	(%r9,%r13,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB8_34
# BB#33:                                # %vector.memcheck91
	leaq	(%rax,%r13,4), %rcx
	cmpq	%rcx, %r9
	jae	.LBB8_34
.LBB8_24:
	xorl	%edx, %edx
.LBB8_25:                               # %.lr.ph.preheader128
	movl	%r13d, %edi
	subl	%edx, %edi
	leaq	-1(%r13), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB8_28
# BB#26:                                # %.lr.ph.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB8_27:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rdx,4), %ecx
	movl	%ecx, (%rax,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB8_27
.LBB8_28:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_37
# BB#29:                                # %.lr.ph.preheader128.new
	subq	%rdx, %r13
	leaq	28(%rax,%rdx,4), %rax
	leaq	28(%r9,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB8_30:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %edx
	movl	%edx, -28(%rax)
	movl	-24(%rcx), %edx
	movl	%edx, -24(%rax)
	movl	-20(%rcx), %edx
	movl	%edx, -20(%rax)
	movl	-16(%rcx), %edx
	movl	%edx, -16(%rax)
	movl	-12(%rcx), %edx
	movl	%edx, -12(%rax)
	movl	-8(%rcx), %edx
	movl	%edx, -8(%rax)
	movl	-4(%rcx), %edx
	movl	%edx, -4(%rax)
	movl	(%rcx), %edx
	movl	%edx, (%rax)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %r13
	jne	.LBB8_30
.LBB8_37:                               # %._crit_edge
	movl	48(%r15), %eax
	movl	%eax, 48(%r14)
	cmpq	$0, 40(%r15)
	je	.LBB8_41
# BB#38:
	movslq	24(%r14), %rbx
	testq	%rbx, %rbx
	jle	.LBB8_39
# BB#42:                                # %.lr.ph.preheader.i
	movl	%ebx, %eax
	movl	(%r12), %esi
	cmpl	$8, %ebx
	jb	.LBB8_43
# BB#44:                                # %min.iters.checked107
	movl	%eax, %edx
	andl	$7, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB8_43
# BB#45:                                # %vector.ph111
	movd	%esi, %xmm0
	pshufd	$36, %xmm0, %xmm1       # xmm1 = xmm0[0,1,2,0]
	leaq	20(%r12), %rsi
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB8_46:                               # %vector.body103
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm3
	shufps	$3, %xmm3, %xmm1        # xmm1 = xmm1[3,0],xmm3[0,0]
	shufps	$152, %xmm3, %xmm1      # xmm1 = xmm1[0,2],xmm3[1,2]
	movaps	%xmm3, %xmm4
	pcmpgtd	%xmm1, %xmm3
	movups	(%rsi), %xmm1
	shufps	$3, %xmm1, %xmm4        # xmm4 = xmm4[3,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm4      # xmm4 = xmm4[0,2],xmm1[1,2]
	psrld	$31, %xmm3
	movaps	%xmm1, %xmm5
	pcmpgtd	%xmm4, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm3, %xmm0
	paddd	%xmm5, %xmm2
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB8_46
# BB#47:                                # %middle.block104
	paddd	%xmm0, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm2
	movd	%xmm2, %edi
	testl	%edx, %edx
	je	.LBB8_51
# BB#48:
	pshufd	$231, %xmm1, %xmm0      # xmm0 = xmm1[3,1,2,3]
	movd	%xmm0, %esi
	jmp	.LBB8_49
.LBB8_43:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
.LBB8_49:                               # %.lr.ph.i.preheader
	subq	%rcx, %rax
	leaq	4(%r12,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB8_50:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	xorl	%ebp, %ebp
	cmpl	%esi, %edx
	setg	%bpl
	addl	%ebp, %edi
	addq	$4, %rcx
	decq	%rax
	movl	%edx, %esi
	jne	.LBB8_50
.LBB8_51:                               # %._crit_edge.i
	movl	%edi, 48(%r14)
	xorl	%eax, %eax
	testl	%edi, %edi
	je	.LBB8_40
# BB#52:                                # %._crit_edge.i
	cmpl	%ebx, %edi
	je	.LBB8_40
# BB#53:
	movl	$4, %esi
	callq	hypre_CAlloc
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB8_54
.LBB8_39:                               # %._crit_edge.thread.i
	movl	$0, 48(%r14)
	xorl	%eax, %eax
	jmp	.LBB8_40
.LBB8_57:                               #   in Loop: Header=BB8_54 Depth=1
	movl	%esi, (%rax,%rcx,4)
	incq	%rcx
.LBB8_54:                               # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_55 Depth 2
	movslq	%edx, %rdx
	leal	-1(%rdx), %esi
	.p2align	4, 0x90
.LBB8_55:                               #   Parent Loop BB8_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %rdx
	jge	.LBB8_40
# BB#56:                                #   in Loop: Header=BB8_55 Depth=2
	movl	4(%r12,%rdx,4), %edi
	incl	%esi
	cmpl	(%r12,%rdx,4), %edi
	leaq	1(%rdx), %rdx
	jle	.LBB8_55
	jmp	.LBB8_57
.LBB8_40:                               # %hypre_CSRMatrixSetRownnz.exit
	movq	%rax, 40(%r14)
.LBB8_41:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_19:                               # %vector.body.preheader
	leaq	16(%rdx), %rbx
	leaq	16(%r12), %rdi
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB8_20:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-8, %rcx
	jne	.LBB8_20
# BB#21:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB8_10
	jmp	.LBB8_22
.LBB8_34:                               # %vector.body74.preheader
	leaq	16(%r9), %rdi
	leaq	16(%rax), %rbp
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB8_35:                               # %vector.body74
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB8_35
# BB#36:                                # %middle.block75
	testl	%esi, %esi
	jne	.LBB8_25
	jmp	.LBB8_37
.Lfunc_end8:
	.size	hypre_CSRMatrixClone, .Lfunc_end8-hypre_CSRMatrixClone
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI9_1:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.text
	.globl	hypre_CSRMatrixUnion
	.p2align	4, 0x90
	.type	hypre_CSRMatrixUnion,@function
hypre_CSRMatrixUnion:                   # @hypre_CSRMatrixUnion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 176
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movl	24(%rdi), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	28(%rdi), %edx
	movl	28(%rsi), %ecx
	movq	8(%rdi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rdi), %r13
	movq	8(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%r14, %r14
	je	.LBB9_1
# BB#2:
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	$4, %esi
	movl	%ecx, %edi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	callq	hypre_CAlloc
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	movl	%r11d, %r12d
	jle	.LBB9_25
# BB#3:                                 # %.lr.ph313
	testl	%r11d, %r11d
	jle	.LBB9_4
# BB#14:                                # %.lr.ph313.split.us.preheader
	leaq	-1(%r11), %r9
	movl	%r11d, %ecx
	andl	$3, %ecx
	leaq	12(%r14), %r8
	xorl	%r10d, %r10d
	movl	$1, %edi
	movl	%r11d, %r12d
	.p2align	4, 0x90
.LBB9_15:                               # %.lr.ph313.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_18 Depth 2
                                        #     Child Loop BB9_21 Depth 2
	testq	%rcx, %rcx
	movl	(%r15,%r10,4), %ebp
	je	.LBB9_16
# BB#17:                                # %.prol.preheader559
                                        #   in Loop: Header=BB9_15 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_18:                               #   Parent Loop BB9_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, (%r14,%rax,4)
	cmovel	%edi, %edx
	incq	%rax
	cmpq	%rax, %rcx
	jne	.LBB9_18
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_16:                               #   in Loop: Header=BB9_15 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB9_19:                               # %.prol.loopexit560
                                        #   in Loop: Header=BB9_15 Depth=1
	cmpq	$3, %r9
	jb	.LBB9_22
# BB#20:                                # %.lr.ph313.split.us.new
                                        #   in Loop: Header=BB9_15 Depth=1
	movq	%r11, %rsi
	subq	%rax, %rsi
	leaq	(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB9_21:                               #   Parent Loop BB9_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, -12(%rax)
	cmovel	%edi, %edx
	cmpl	%ebp, -8(%rax)
	cmovel	%edi, %edx
	cmpl	%ebp, -4(%rax)
	cmovel	%edi, %edx
	cmpl	%ebp, (%rax)
	cmovel	%edi, %edx
	addq	$16, %rax
	addq	$-4, %rsi
	jne	.LBB9_21
.LBB9_22:                               # %._crit_edge308.us
                                        #   in Loop: Header=BB9_15 Depth=1
	testl	%edx, %edx
	jne	.LBB9_24
# BB#23:                                #   in Loop: Header=BB9_15 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%r12d, (%rax,%r10,4)
	incl	%r12d
.LBB9_24:                               #   in Loop: Header=BB9_15 Depth=1
	incq	%r10
	cmpq	%rbx, %r10
	jne	.LBB9_15
	jmp	.LBB9_25
.LBB9_1:                                # %.thread
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB9_61
.LBB9_4:                                # %.preheader425
	cmpl	$7, %ebx
	ja	.LBB9_8
# BB#5:
	xorl	%ecx, %ecx
	movl	%r11d, %eax
	movq	(%rsp), %r8             # 8-byte Reload
	jmp	.LBB9_6
.LBB9_8:                                # %min.iters.checked
	movl	%ebx, %edx
	andl	$7, %edx
	movq	%rbx, %rax
	subq	%rdx, %rax
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	movq	(%rsp), %r8             # 8-byte Reload
	je	.LBB9_9
# BB#10:                                # %vector.body.preheader
	addl	%r11d, %eax
	leaq	16(%r8), %rsi
	movdqa	.LCPI9_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI9_1(%rip), %xmm1   # xmm1 = [4,5,6,7]
	movq	%rcx, %rdi
	movl	%r11d, %ebp
	.p2align	4, 0x90
.LBB9_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebp, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movdqa	%xmm2, %xmm3
	paddd	%xmm0, %xmm3
	paddd	%xmm1, %xmm2
	movdqu	%xmm3, -16(%rsi)
	movdqu	%xmm2, (%rsi)
	addq	$32, %rsi
	addl	$8, %ebp
	addq	$-8, %rdi
	jne	.LBB9_11
# BB#12:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB9_6
	jmp	.LBB9_13
.LBB9_9:
	xorl	%ecx, %ecx
	movl	%r11d, %eax
.LBB9_6:                                # %scalar.ph.preheader
	leaq	(%r8,%rcx,4), %rdx
	movq	%rbx, %rsi
	subq	%rcx, %rsi
	.p2align	4, 0x90
.LBB9_7:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rdx)
	incl	%eax
	addq	$4, %rdx
	decq	%rsi
	jne	.LBB9_7
.LBB9_13:                               # %._crit_edge314.loopexit327
	leal	(%rbx,%r11), %r12d
.LBB9_25:                               # %._crit_edge314
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	testl	%ebx, %ebx
	jle	.LBB9_40
# BB#26:                                # %.lr.ph303.preheader
	cmpl	$7, %ebx
	jbe	.LBB9_27
# BB#34:                                # %min.iters.checked465
	movl	%ebx, %edx
	andl	$7, %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	je	.LBB9_27
# BB#35:                                # %vector.memcheck
	leaq	(%r14,%rbx,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB9_37
# BB#36:                                # %vector.memcheck
	leaq	(%rax,%rbx,4), %rsi
	cmpq	%rsi, %r14
	jae	.LBB9_37
.LBB9_27:
	xorl	%ecx, %ecx
.LBB9_28:                               # %.lr.ph303.preheader536
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB9_31
# BB#29:                                # %.lr.ph303.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB9_30:                               # %.lr.ph303.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB9_30
.LBB9_31:                               # %.lr.ph303.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB9_40
# BB#32:                                # %.lr.ph303.preheader536.new
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	leaq	28(%rax,%rcx,4), %rsi
	leaq	28(%r14,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB9_33:                               # %.lr.ph303
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %edi
	movl	%edi, -28(%rsi)
	movl	-24(%rcx), %edi
	movl	%edi, -24(%rsi)
	movl	-20(%rcx), %edi
	movl	%edi, -20(%rsi)
	movl	-16(%rcx), %edi
	movl	%edi, -16(%rsi)
	movl	-12(%rcx), %edi
	movl	%edi, -12(%rsi)
	movl	-8(%rcx), %edi
	movl	%edi, -8(%rsi)
	movl	-4(%rcx), %edi
	movl	%edi, -4(%rsi)
	movl	(%rcx), %edi
	movl	%edi, (%rsi)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %rdx
	jne	.LBB9_33
.LBB9_40:                               # %.preheader
	movb	$1, %cl
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%r8d, %r8d
	movl	%r12d, 56(%rsp)         # 4-byte Spill
	jle	.LBB9_60
# BB#41:                                # %.lr.ph300
	testl	%ebx, %ebx
	jle	.LBB9_42
# BB#49:                                # %.lr.ph300.split.us.preheader
	leaq	-1(%rbx), %r9
	movl	%ebx, %r11d
	andl	$3, %r11d
	leaq	12(%r14), %r8
	xorl	%r10d, %r10d
	movl	$1, %esi
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB9_50:                               # %.lr.ph300.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_53 Depth 2
                                        #     Child Loop BB9_56 Depth 2
	testq	%r11, %r11
	movl	(%r15,%r10,4), %ebp
	je	.LBB9_51
# BB#52:                                # %.prol.preheader
                                        #   in Loop: Header=BB9_50 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_53:                               #   Parent Loop BB9_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, (%r14,%rdi,4)
	cmovel	%esi, %ecx
	incq	%rdi
	cmpq	%rdi, %r11
	jne	.LBB9_53
	jmp	.LBB9_54
	.p2align	4, 0x90
.LBB9_51:                               #   in Loop: Header=BB9_50 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
.LBB9_54:                               # %.prol.loopexit
                                        #   in Loop: Header=BB9_50 Depth=1
	cmpq	$3, %r9
	jb	.LBB9_57
# BB#55:                                # %.lr.ph300.split.us.new
                                        #   in Loop: Header=BB9_50 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	subq	%rdi, %rdx
	leaq	(%r8,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB9_56:                               #   Parent Loop BB9_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, -12(%rdi)
	cmovel	%esi, %ecx
	cmpl	%ebp, -8(%rdi)
	cmovel	%esi, %ecx
	cmpl	%ebp, -4(%rdi)
	cmovel	%esi, %ecx
	cmpl	%ebp, (%rdi)
	cmovel	%esi, %ecx
	addq	$16, %rdi
	addq	$-4, %rdx
	jne	.LBB9_56
.LBB9_57:                               # %._crit_edge297.us
                                        #   in Loop: Header=BB9_50 Depth=1
	testl	%ecx, %ecx
	jne	.LBB9_59
# BB#58:                                #   in Loop: Header=BB9_50 Depth=1
	movslq	(%rbx,%r10,4), %rcx
	movl	%ebp, (%rax,%rcx,4)
.LBB9_59:                               #   in Loop: Header=BB9_50 Depth=1
	incq	%r10
	cmpq	40(%rsp), %r10          # 8-byte Folded Reload
	jne	.LBB9_50
	jmp	.LBB9_60
.LBB9_42:                               # %.preheader423
	leaq	-1(%r8), %rcx
	movq	%r8, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB9_43
# BB#44:                                # %.prol.preheader551
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB9_45:                               # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rdx,4), %edi
	movslq	(%rbx,%rdx,4), %rbp
	movl	%edi, (%rax,%rbp,4)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB9_45
	jmp	.LBB9_46
.LBB9_37:                               # %vector.body461.preheader
	leaq	16(%r14), %rsi
	leaq	16(%rax), %rdi
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB9_38:                               # %vector.body461
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB9_38
# BB#39:                                # %middle.block462
	testl	%edx, %edx
	jne	.LBB9_28
	jmp	.LBB9_40
.LBB9_43:
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB9_46:                               # %.prol.loopexit552
	cmpq	$3, %rcx
	jb	.LBB9_60
# BB#47:                                # %.preheader423.new
	subq	%rdx, %r8
	leaq	12(%rbx,%rdx,4), %rcx
	leaq	12(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB9_48:                               # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %esi
	movslq	-12(%rcx), %rdi
	movl	%esi, (%rax,%rdi,4)
	movl	-8(%rdx), %esi
	movslq	-8(%rcx), %rdi
	movl	%esi, (%rax,%rdi,4)
	movl	-4(%rdx), %esi
	movslq	-4(%rcx), %rdi
	movl	%esi, (%rax,%rdi,4)
	movl	(%rdx), %esi
	movslq	(%rcx), %rdi
	movl	%esi, (%rax,%rdi,4)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %r8
	jne	.LBB9_48
.LBB9_60:
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB9_61:                               # %.loopexit237
	movl	32(%rdi), %r12d
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB9_83
# BB#62:                                # %.lr.ph290
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r10d
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB9_64:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_75 Depth 2
                                        #       Child Loop BB9_79 Depth 3
                                        #       Child Loop BB9_77 Depth 3
                                        #     Child Loop BB9_66 Depth 2
                                        #       Child Loop BB9_68 Depth 3
                                        #       Child Loop BB9_73 Depth 3
	movl	%r10d, %esi
	movl	%r8d, %eax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%r9,4), %r10d
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%r9,4), %r8d
	incq	%r9
	cmpl	%r8d, %eax
	jge	.LBB9_63
# BB#65:                                # %.lr.ph279
                                        #   in Loop: Header=BB9_64 Depth=1
	movslq	%r10d, %rdi
	movslq	%r8d, %r11
	testq	%r15, %r15
	movslq	%eax, %rcx
	je	.LBB9_66
	.p2align	4, 0x90
.LBB9_75:                               # %.lr.ph279.split
                                        #   Parent Loop BB9_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_79 Depth 3
                                        #       Child Loop BB9_77 Depth 3
	cmpl	%r10d, %esi
	jge	.LBB9_81
# BB#76:                                # %.lr.ph269
                                        #   in Loop: Header=BB9_75 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movl	(%r15,%rax,4), %eax
	movslq	%esi, %rbp
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB9_79
	.p2align	4, 0x90
.LBB9_77:                               # %.lr.ph269.split.us
                                        #   Parent Loop BB9_64 Depth=1
                                        #     Parent Loop BB9_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%r13,%rbp,4), %rdx
	cmpl	(%r14,%rdx,4), %eax
	je	.LBB9_136
# BB#78:                                #   in Loop: Header=BB9_77 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB9_77
	jmp	.LBB9_81
	.p2align	4, 0x90
.LBB9_79:                               # %.lr.ph269.split
                                        #   Parent Loop BB9_64 Depth=1
                                        #     Parent Loop BB9_75 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r13,%rbp,4), %eax
	je	.LBB9_136
# BB#80:                                #   in Loop: Header=BB9_79 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB9_79
	.p2align	4, 0x90
.LBB9_81:                               # %._crit_edge270
                                        #   in Loop: Header=BB9_75 Depth=2
	incl	%r12d
	jmp	.LBB9_82
	.p2align	4, 0x90
.LBB9_136:                              # %.us-lcssa271.us
                                        #   in Loop: Header=BB9_75 Depth=2
	xorl	%eax, %eax
	cmpl	%esi, %ebp
	sete	%al
	addl	%eax, %esi
.LBB9_82:                               #   in Loop: Header=BB9_75 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB9_75
	jmp	.LBB9_63
	.p2align	4, 0x90
.LBB9_66:                               # %.lr.ph279.split.us
                                        #   Parent Loop BB9_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_68 Depth 3
                                        #       Child Loop BB9_73 Depth 3
	cmpl	%r10d, %esi
	jge	.LBB9_67
# BB#72:                                # %.lr.ph269.us
                                        #   in Loop: Header=BB9_66 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx,4), %eax
	movslq	%esi, %rbp
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB9_68
	.p2align	4, 0x90
.LBB9_73:                               # %.lr.ph269.split.us.us
                                        #   Parent Loop BB9_64 Depth=1
                                        #     Parent Loop BB9_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%r13,%rbp,4), %rdx
	cmpl	(%r14,%rdx,4), %eax
	je	.LBB9_70
# BB#74:                                #   in Loop: Header=BB9_73 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB9_73
	jmp	.LBB9_67
	.p2align	4, 0x90
.LBB9_68:                               # %.lr.ph269..lr.ph269.split_crit_edge.us
                                        #   Parent Loop BB9_64 Depth=1
                                        #     Parent Loop BB9_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r13,%rbp,4), %eax
	je	.LBB9_70
# BB#69:                                #   in Loop: Header=BB9_68 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB9_68
	.p2align	4, 0x90
.LBB9_67:                               # %._crit_edge270.us
                                        #   in Loop: Header=BB9_66 Depth=2
	incl	%r12d
	jmp	.LBB9_71
	.p2align	4, 0x90
.LBB9_70:                               # %.us-lcssa271.us284
                                        #   in Loop: Header=BB9_66 Depth=2
	xorl	%eax, %eax
	cmpl	%esi, %ebp
	sete	%al
	addl	%eax, %esi
.LBB9_71:                               #   in Loop: Header=BB9_66 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB9_66
	.p2align	4, 0x90
.LBB9_63:                               # %.loopexit
                                        #   in Loop: Header=BB9_64 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	jne	.LBB9_64
.LBB9_83:                               # %._crit_edge291
	movl	$1, %edi
	movl	$56, %esi
	callq	hypre_CAlloc
	movq	$0, 40(%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%edx, 24(%rax)
	movl	56(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 28(%rax)
	movl	%r12d, 32(%rax)
	movl	$1, 52(%rax)
	movl	%edx, 48(%rax)
	testl	%r12d, %r12d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB9_84
# BB#85:
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	movq	%rcx, %rbx
	addq	$8, %rbx
	cmpq	$0, 8(%rcx)
	jne	.LBB9_87
	jmp	.LBB9_86
.LBB9_84:                               # %.thread409
	movq	%rax, %rbx
	addq	$8, %rbx
.LBB9_86:
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %edi
	movl	$4, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB9_87:
	movq	16(%rcx), %rax
	testl	%r12d, %r12d
	je	.LBB9_90
# BB#88:
	testq	%rax, %rax
	jne	.LBB9_90
# BB#89:
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 16(%rcx)
.LBB9_90:                               # %hypre_CSRMatrixInitialize.exit
	movq	(%rbx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	$0, (%rcx)
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB9_133
# BB#91:                                # %.lr.ph265
	leaq	48(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	48(%r13), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leaq	12(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	12(%rax), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movq	%r15, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_92:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_102 Depth 2
                                        #     Child Loop BB9_108 Depth 2
                                        #     Child Loop BB9_111 Depth 2
                                        #     Child Loop BB9_124 Depth 2
                                        #       Child Loop BB9_128 Depth 3
                                        #       Child Loop BB9_126 Depth 3
                                        #     Child Loop BB9_115 Depth 2
                                        #       Child Loop BB9_117 Depth 3
                                        #       Child Loop BB9_122 Depth 3
	movq	%r11, %r8
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r8,4), %r12d
	movslq	4(%rcx,%r8,4), %r10
	cmpl	%r10d, %r12d
	jge	.LBB9_113
# BB#93:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_92 Depth=1
	movslq	%r12d, %rcx
	movslq	%edx, %rsi
	leal	-1(%r10), %ebx
	subl	%r12d, %ebx
	movq	%r10, %r15
	subq	%rcx, %r15
	cmpq	$8, %r15
	jb	.LBB9_105
# BB#94:                                # %min.iters.checked486
                                        #   in Loop: Header=BB9_92 Depth=1
	movq	%r15, %rdi
	andq	$-8, %rdi
	je	.LBB9_105
# BB#95:                                # %vector.memcheck503
                                        #   in Loop: Header=BB9_92 Depth=1
	leaq	(%rax,%rsi,4), %r9
	leaq	(%r13,%r10,4), %rbp
	cmpq	%rbp, %r9
	jae	.LBB9_97
# BB#96:                                # %vector.memcheck503
                                        #   in Loop: Header=BB9_92 Depth=1
	leaq	(%rsi,%r10), %rbp
	subq	%rcx, %rbp
	leaq	(%rax,%rbp,4), %r9
	leaq	(%r13,%rcx,4), %rbp
	cmpq	%r9, %rbp
	jb	.LBB9_105
.LBB9_97:                               # %vector.body482.preheader
                                        #   in Loop: Header=BB9_92 Depth=1
	leaq	-8(%rdi), %rbp
	movq	%rbp, %r9
	shrq	$3, %r9
	btl	$3, %ebp
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jb	.LBB9_98
# BB#99:                                # %vector.body482.prol
                                        #   in Loop: Header=BB9_92 Depth=1
	movdqu	(%r13,%rcx,4), %xmm0
	movdqu	16(%r13,%rcx,4), %xmm1
	movdqu	%xmm0, (%rax,%rsi,4)
	movdqu	%xmm1, 16(%rax,%rsi,4)
	movl	$8, %r11d
	testq	%r9, %r9
	jne	.LBB9_101
	jmp	.LBB9_103
.LBB9_98:                               #   in Loop: Header=BB9_92 Depth=1
	xorl	%r11d, %r11d
	testq	%r9, %r9
	je	.LBB9_103
.LBB9_101:                              # %vector.body482.preheader.new
                                        #   in Loop: Header=BB9_92 Depth=1
	movq	%rdi, %rbp
	subq	%r11, %rbp
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	leaq	(%r11,%rsi), %rdi
	movq	96(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,4), %r9
	addq	%rcx, %r11
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r11,4), %r11
	movq	104(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB9_102:                              # %vector.body482
                                        #   Parent Loop BB9_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%r11), %xmm0
	movups	-32(%r11), %xmm1
	movups	%xmm0, -48(%r9)
	movups	%xmm1, -32(%r9)
	movdqu	-16(%r11), %xmm0
	movdqu	(%r11), %xmm1
	movdqu	%xmm0, -16(%r9)
	movdqu	%xmm1, (%r9)
	addq	$64, %r9
	addq	$64, %r11
	addq	$-16, %rbp
	jne	.LBB9_102
.LBB9_103:                              # %middle.block483
                                        #   in Loop: Header=BB9_92 Depth=1
	cmpq	%rdi, %r15
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	je	.LBB9_112
# BB#104:                               #   in Loop: Header=BB9_92 Depth=1
	addq	%rdi, %rcx
	addq	%rdi, %rsi
	.p2align	4, 0x90
.LBB9_105:                              # %.lr.ph.preheader529
                                        #   in Loop: Header=BB9_92 Depth=1
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	leaq	-1(%r10), %r9
	subq	%rcx, %r9
	andq	$3, %rbp
	je	.LBB9_106
# BB#107:                               # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB9_92 Depth=1
	negq	%rbp
	movq	72(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_108:                              # %.lr.ph.prol
                                        #   Parent Loop BB9_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx,4), %edi
	movl	%edi, (%rax,%rsi,4)
	incq	%rsi
	incq	%rcx
	incq	%rbp
	jne	.LBB9_108
	jmp	.LBB9_109
.LBB9_106:                              #   in Loop: Header=BB9_92 Depth=1
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB9_109:                              # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB9_92 Depth=1
	cmpq	$3, %r9
	jb	.LBB9_112
# BB#110:                               # %.lr.ph.preheader529.new
                                        #   in Loop: Header=BB9_92 Depth=1
	movq	%r10, %rdi
	subq	%rcx, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rcx,4), %rcx
	movq	56(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB9_111:                              # %.lr.ph
                                        #   Parent Loop BB9_92 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rcx), %ebp
	movl	%ebp, -12(%rsi)
	movl	-8(%rcx), %ebp
	movl	%ebp, -8(%rsi)
	movl	-4(%rcx), %ebp
	movl	%ebp, -4(%rsi)
	movl	(%rcx), %ebp
	movl	%ebp, (%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	addq	$-4, %rdi
	jne	.LBB9_111
.LBB9_112:                              # %._crit_edge.loopexit
                                        #   in Loop: Header=BB9_92 Depth=1
	leal	1(%rdx,%rbx), %edx
.LBB9_113:                              # %._crit_edge
                                        #   in Loop: Header=BB9_92 Depth=1
	leaq	1(%r8), %r11
	movq	48(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%r8,4), %r9
	movl	4(%rcx,%r8,4), %ecx
	cmpl	%ecx, %r9d
	jge	.LBB9_132
# BB#114:                               # %.lr.ph254
                                        #   in Loop: Header=BB9_92 Depth=1
	testq	%r15, %r15
	je	.LBB9_115
	.p2align	4, 0x90
.LBB9_124:                              # %.lr.ph254.split
                                        #   Parent Loop BB9_92 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_128 Depth 3
                                        #       Child Loop BB9_126 Depth 3
	movq	64(%rsp), %rsi          # 8-byte Reload
	movslq	(%rsi,%r9,4), %rdi
	cmpl	%r10d, %r12d
	jge	.LBB9_130
# BB#125:                               # %.lr.ph244
                                        #   in Loop: Header=BB9_124 Depth=2
	movl	(%r15,%rdi,4), %ebp
	movslq	%r12d, %rsi
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB9_128
	.p2align	4, 0x90
.LBB9_126:                              # %.lr.ph244.split.us
                                        #   Parent Loop BB9_92 Depth=1
                                        #     Parent Loop BB9_124 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%r13,%rsi,4), %rbx
	cmpl	(%r14,%rbx,4), %ebp
	je	.LBB9_137
# BB#127:                               #   in Loop: Header=BB9_126 Depth=3
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB9_126
	jmp	.LBB9_130
	.p2align	4, 0x90
.LBB9_128:                              # %.lr.ph244.split
                                        #   Parent Loop BB9_92 Depth=1
                                        #     Parent Loop BB9_124 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r13,%rsi,4), %ebp
	je	.LBB9_137
# BB#129:                               #   in Loop: Header=BB9_128 Depth=3
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB9_128
	.p2align	4, 0x90
.LBB9_130:                              # %._crit_edge245
                                        #   in Loop: Header=BB9_124 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx,%rdi,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, (%rax,%rdx,4)
	incl	%edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%r8,4), %ecx
	jmp	.LBB9_131
	.p2align	4, 0x90
.LBB9_137:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB9_124 Depth=2
	xorl	%edi, %edi
	cmpl	%r12d, %esi
	sete	%dil
	addl	%edi, %r12d
.LBB9_131:                              #   in Loop: Header=BB9_124 Depth=2
	incq	%r9
	movslq	%ecx, %rsi
	cmpq	%rsi, %r9
	jl	.LBB9_124
	jmp	.LBB9_132
	.p2align	4, 0x90
.LBB9_115:                              # %.lr.ph254.split.us
                                        #   Parent Loop BB9_92 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_117 Depth 3
                                        #       Child Loop BB9_122 Depth 3
	movq	64(%rsp), %rsi          # 8-byte Reload
	movslq	(%rsi,%r9,4), %rdi
	cmpl	%r10d, %r12d
	jge	.LBB9_116
# BB#121:                               # %.lr.ph244.us
                                        #   in Loop: Header=BB9_115 Depth=2
	movslq	%r12d, %rsi
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB9_117
	.p2align	4, 0x90
.LBB9_122:                              # %.lr.ph244.split.us.us
                                        #   Parent Loop BB9_92 Depth=1
                                        #     Parent Loop BB9_115 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%r13,%rsi,4), %rbp
	cmpl	(%r14,%rbp,4), %edi
	je	.LBB9_119
# BB#123:                               #   in Loop: Header=BB9_122 Depth=3
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB9_122
	jmp	.LBB9_116
	.p2align	4, 0x90
.LBB9_117:                              # %.lr.ph244..lr.ph244.split_crit_edge.us
                                        #   Parent Loop BB9_92 Depth=1
                                        #     Parent Loop BB9_115 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r13,%rsi,4), %edi
	je	.LBB9_119
# BB#118:                               #   in Loop: Header=BB9_117 Depth=3
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB9_117
	.p2align	4, 0x90
.LBB9_116:                              # %._crit_edge245.us
                                        #   in Loop: Header=BB9_115 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx,%rdi,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, (%rax,%rdx,4)
	incl	%edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%r8,4), %ecx
	jmp	.LBB9_120
	.p2align	4, 0x90
.LBB9_119:                              # %.us-lcssa.us260
                                        #   in Loop: Header=BB9_115 Depth=2
	xorl	%edi, %edi
	cmpl	%r12d, %esi
	sete	%dil
	addl	%edi, %r12d
.LBB9_120:                              #   in Loop: Header=BB9_115 Depth=2
	incq	%r9
	movslq	%ecx, %rsi
	cmpq	%rsi, %r9
	jl	.LBB9_115
	.p2align	4, 0x90
.LBB9_132:                              # %._crit_edge255
                                        #   in Loop: Header=BB9_92 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%edx, 4(%rcx,%r8,4)
	cmpq	24(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB9_92
.LBB9_133:                              # %._crit_edge266
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB9_135
# BB#134:
	callq	hypre_Free
.LBB9_135:
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_CSRMatrixUnion, .Lfunc_end9-hypre_CSRMatrixUnion
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%le"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"w"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d\n"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%.14e\n"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Warning: No matrix data!\n"
	.size	.L.str.6, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
