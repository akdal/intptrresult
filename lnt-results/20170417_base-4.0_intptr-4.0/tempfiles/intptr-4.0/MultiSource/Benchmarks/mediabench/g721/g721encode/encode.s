	.text
	.file	"encode.bc"
	.globl	pack_output
	.p2align	4, 0x90
	.type	pack_output,@function
pack_output:                            # @pack_output
	.cfi_startproc
# BB#0:
	movl	pack_output.out_bits(%rip), %ecx
	shll	%cl, %edi
	orl	pack_output.out_buffer(%rip), %edi
	movl	%edi, pack_output.out_buffer(%rip)
	addl	%ecx, %esi
	movl	%esi, pack_output.out_bits(%rip)
	cmpl	$8, %esi
	jl	.LBB0_2
# BB#1:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	addl	$-8, %esi
	movl	%esi, pack_output.out_bits(%rip)
	movl	%edi, %eax
	shrl	$8, %eax
	movl	%eax, pack_output.out_buffer(%rip)
	movq	stdout(%rip), %rsi
	movsbl	%dil, %edi
	callq	fputc
	movl	pack_output.out_bits(%rip), %esi
	addq	$8, %rsp
.LBB0_2:
	xorl	%eax, %eax
	testl	%esi, %esi
	setg	%al
	retq
.Lfunc_end0:
	.size	pack_output, .Lfunc_end0-pack_output
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 128
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %ebx
	leaq	16(%rsp), %rdi
	callq	g72x_init_state
	cmpl	$2, %ebx
	jl	.LBB1_1
# BB#2:                                 # %.lr.ph57
	leaq	13(%rsp), %rax
	movl	$g721_encoder, %r12d
	addq	$8, %rbp
	movl	$1, %esi
	movl	$4, %r13d
	leaq	14(%rsp), %rcx
	movq	%rax, %r15
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	cmpb	$45, (%rdx)
	jne	.LBB1_4
# BB#15:                                #   in Loop: Header=BB1_3 Depth=1
	movsbl	1(%rdx), %edx
	cmpl	$96, %edx
	jg	.LBB1_21
# BB#16:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$51, %edx
	je	.LBB1_17
# BB#18:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$52, %edx
	je	.LBB1_28
# BB#19:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$53, %edx
	jne	.LBB1_27
# BB#20:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$5, %r13d
	movl	$g723_40_encoder, %r12d
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_3 Depth=1
	cmpl	$97, %edx
	je	.LBB1_25
# BB#22:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$108, %edx
	je	.LBB1_26
# BB#23:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$117, %edx
	jne	.LBB1_27
# BB#24:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$1, %r14d
	movl	$1, %esi
	movq	%rax, %r15
	jmp	.LBB1_29
.LBB1_17:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$3, %r13d
	movl	$g723_24_encoder, %r12d
	jmp	.LBB1_29
.LBB1_28:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$4, %r13d
	movl	$g721_encoder, %r12d
	jmp	.LBB1_29
.LBB1_25:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$1, %esi
	movl	$2, %r14d
	movq	%rax, %r15
	jmp	.LBB1_29
.LBB1_26:                               #   in Loop: Header=BB1_3 Depth=1
	movl	$2, %esi
	movl	$3, %r14d
	movq	%rcx, %r15
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_3 Depth=1
	decl	%ebx
	addq	$8, %rbp
	cmpl	$1, %ebx
	jg	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	leaq	13(%rsp), %r15
	movl	$g721_encoder, %r12d
	movl	$4, %r13d
	movl	$1, %r14d
	movl	$1, %esi
.LBB1_4:                                # %.critedge.preheader
	movslq	%esi, %rbp
	movq	stdin(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movl	%esi, %ebx
	movq	%rbp, %rsi
	callq	fread
	cmpq	$1, %rax
	jne	.LBB1_10
# BB#5:                                 # %.lr.ph47
	cmpl	$2, %ebx
	jne	.LBB1_30
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph47.split.us
                                        # =>This Inner Loop Header: Depth=1
	movswl	14(%rsp), %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	leaq	16(%rsp), %rdx
	callq	*%r12
	movzbl	%al, %eax
	movl	pack_output.out_bits(%rip), %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	pack_output.out_buffer(%rip), %eax
	movl	%eax, pack_output.out_buffer(%rip)
	addl	%r13d, %ebx
	movl	%ebx, pack_output.out_bits(%rip)
	cmpl	$8, %ebx
	jl	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	addl	$-8, %ebx
	movl	%ebx, pack_output.out_bits(%rip)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, pack_output.out_buffer(%rip)
	movq	stdout(%rip), %rsi
	movsbl	%al, %edi
	callq	fputc
	movl	pack_output.out_bits(%rip), %ebx
.LBB1_8:                                # %pack_output.exit.us
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	stdin(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	fread
	cmpq	$1, %rax
	je	.LBB1_6
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_30:                               # %.lr.ph47.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	13(%rsp), %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	leaq	16(%rsp), %rdx
	callq	*%r12
	movzbl	%al, %eax
	movl	pack_output.out_bits(%rip), %ebx
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	pack_output.out_buffer(%rip), %eax
	movl	%eax, pack_output.out_buffer(%rip)
	addl	%r13d, %ebx
	movl	%ebx, pack_output.out_bits(%rip)
	cmpl	$8, %ebx
	jl	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_30 Depth=1
	addl	$-8, %ebx
	movl	%ebx, pack_output.out_bits(%rip)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, pack_output.out_buffer(%rip)
	movq	stdout(%rip), %rsi
	movsbl	%al, %edi
	callq	fputc
	movl	pack_output.out_bits(%rip), %ebx
.LBB1_32:                               # %pack_output.exit
                                        #   in Loop: Header=BB1_30 Depth=1
	movq	stdin(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	fread
	cmpq	$1, %rax
	je	.LBB1_30
.LBB1_9:                                # %.preheader
	testl	%ebx, %ebx
	jle	.LBB1_14
.LBB1_10:                               # %.lr.ph.preheader
	movl	pack_output.out_bits(%rip), %eax
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	pack_output.out_buffer(%rip), %ecx
	addl	%r13d, %eax
	movl	%eax, pack_output.out_bits(%rip)
	cmpl	$8, %eax
	jl	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	addl	$-8, %eax
	movl	%eax, pack_output.out_bits(%rip)
	movl	%ecx, %eax
	shrl	$8, %eax
	movl	%eax, pack_output.out_buffer(%rip)
	movq	stdout(%rip), %rsi
	movsbl	%cl, %edi
	callq	fputc
	movl	pack_output.out_bits(%rip), %eax
.LBB1_13:                               # %pack_output.exit41
                                        #   in Loop: Header=BB1_11 Depth=1
	testl	%eax, %eax
	jg	.LBB1_11
.LBB1_14:                               # %._crit_edge
	movq	stdout(%rip), %rdi
	callq	fclose
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_27:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	pack_output.out_buffer,@object # @pack_output.out_buffer
	.local	pack_output.out_buffer
	.comm	pack_output.out_buffer,4,4
	.type	pack_output.out_bits,@object # @pack_output.out_bits
	.local	pack_output.out_bits
	.comm	pack_output.out_bits,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"CCITT ADPCM Encoder -- usage:\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\tencode [-3|4|5] [-a|u|l] < infile > outfile\n"
	.size	.L.str.1, 46

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"where:\n"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\t-3\tGenerate G.723 24kbps (3-bit) data\n"
	.size	.L.str.3, 40

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\t-4\tGenerate G.721 32kbps (4-bit) data [default]\n"
	.size	.L.str.4, 50

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\t-5\tGenerate G.723 40kbps (5-bit) data\n"
	.size	.L.str.5, 40

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\t-a\tProcess 8-bit A-law input data\n"
	.size	.L.str.6, 36

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\t-u\tProcess 8-bit u-law input data [default]\n"
	.size	.L.str.7, 46

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\t-l\tProcess 16-bit linear PCM input data\n"
	.size	.L.str.8, 42


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
