	.text
	.file	"pc1cod.bc"
	.globl	assemble
	.p2align	4, 0x90
	.type	assemble,@function
assemble:                               # @assemble
	.cfi_startproc
# BB#0:
	movl	cle(%rip), %eax
	shll	$8, %eax
	movzbl	cle+1(%rip), %ecx
	orl	%eax, %ecx
	movw	%cx, x1a0(%rip)
	movzwl	i(%rip), %r8d
	movw	x1a2(%rip), %cx
	addw	%r8w, %cx
	movw	x1a0(%r8,%r8), %ax
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	si(%rip), %dx
	movw	%ax, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%cx, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_1
# BB#2:
	imull	$20021, %ecx, %ecx      # imm = 0x4E35
	movw	%cx, ax(%rip)
	jmp	.LBB0_3
.LBB0_1:
	xorl	%ecx, %ecx
.LBB0_3:                                # %code.exit
	imull	$346, %eax, %esi        # imm = 0x15A
	addl	%esi, %ecx
	imull	$20021, %eax, %r10d     # imm = 0x4E35
	addl	%ecx, %edx
	incl	%r10d
	movw	%dx, x1a2(%rip)
	movw	%r10w, x1a0(%r8,%r8)
	xorl	%edx, %r10d
	movw	%r10w, res(%rip)
	leal	1(%r8), %ecx
	movw	%cx, i(%rip)
	movw	%r10w, inter(%rip)
	movzbl	cle+2(%rip), %edi
	shll	$8, %edi
	movzbl	cle+3(%rip), %eax
	orl	%edi, %eax
	xorw	x1a0(%rip), %ax
	movw	%ax, x1a0+2(%rip)
	movzwl	%cx, %r9d
	addw	%dx, %cx
	movw	x1a0(%r9,%r9), %di
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%di, si(%rip)
	movw	%si, tmp(%rip)
	movw	%cx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_4
# BB#5:
	imull	$20021, %ecx, %ecx      # imm = 0x4E35
	movw	%cx, ax(%rip)
	jmp	.LBB0_6
.LBB0_4:
	xorl	%ecx, %ecx
.LBB0_6:                                # %code.exit15
	imull	$346, %edi, %edx        # imm = 0x15A
	addl	%edx, %ecx
	imull	$20021, %edi, %eax      # imm = 0x4E35
	leal	(%rsi,%rcx), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	2(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+4(%rip), %eax
	shll	$8, %eax
	movzbl	cle+5(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+2(%rip), %di
	movw	%di, x1a0+4(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_7
# BB#8:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_9
.LBB0_7:
	xorl	%edi, %edi
.LBB0_9:                                # %code.exit14
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	3(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+6(%rip), %eax
	shll	$8, %eax
	movzbl	cle+7(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+4(%rip), %di
	movw	%di, x1a0+6(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_10
# BB#11:
	imull	$20021, %edx, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_12
.LBB0_10:
	xorl	%edi, %edi
.LBB0_12:                               # %code.exit13
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	4(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+8(%rip), %eax
	shll	$8, %eax
	movzbl	cle+9(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+6(%rip), %di
	movw	%di, x1a0+8(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_13
# BB#14:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_15
.LBB0_13:
	xorl	%edi, %edi
.LBB0_15:                               # %code.exit12
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	5(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+10(%rip), %eax
	shll	$8, %eax
	movzbl	cle+11(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+8(%rip), %di
	movw	%di, x1a0+10(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_16
# BB#17:
	imull	$20021, %edx, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_18
.LBB0_16:
	xorl	%edi, %edi
.LBB0_18:                               # %code.exit11
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	6(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+12(%rip), %eax
	shll	$8, %eax
	movzbl	cle+13(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+10(%rip), %di
	movw	%di, x1a0+12(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_19
# BB#20:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_21
.LBB0_19:
	xorl	%edi, %edi
.LBB0_21:                               # %code.exit10
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	7(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+14(%rip), %eax
	shll	$8, %eax
	movzbl	cle+15(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+12(%rip), %di
	movw	%di, x1a0+14(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_22
# BB#23:
	imull	$20021, %edx, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_24
.LBB0_22:
	xorl	%edi, %edi
.LBB0_24:                               # %code.exit9
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	8(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+16(%rip), %eax
	shll	$8, %eax
	movzbl	cle+17(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+14(%rip), %di
	movw	%di, x1a0+16(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_25
# BB#26:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_27
.LBB0_25:
	xorl	%edi, %edi
.LBB0_27:                               # %code.exit8
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	9(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+18(%rip), %eax
	shll	$8, %eax
	movzbl	cle+19(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+16(%rip), %di
	movw	%di, x1a0+18(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_28
# BB#29:
	imull	$20021, %edx, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_30
.LBB0_28:
	xorl	%edi, %edi
.LBB0_30:                               # %code.exit7
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	10(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+20(%rip), %eax
	shll	$8, %eax
	movzbl	cle+21(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+18(%rip), %di
	movw	%di, x1a0+20(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_31
# BB#32:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_33
.LBB0_31:
	xorl	%edi, %edi
.LBB0_33:                               # %code.exit6
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	11(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+22(%rip), %eax
	shll	$8, %eax
	movzbl	cle+23(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+20(%rip), %di
	movw	%di, x1a0+22(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_34
# BB#35:
	imull	$20021, %edx, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_36
.LBB0_34:
	xorl	%edi, %edi
.LBB0_36:                               # %code.exit5
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	12(%r8), %esi
	movw	%si, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+24(%rip), %eax
	shll	$8, %eax
	movzbl	cle+25(%rip), %edi
	orl	%eax, %edi
	xorw	x1a0+22(%rip), %di
	movw	%di, x1a0+24(%rip)
	movzwl	%si, %r9d
	addw	%cx, %si
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_37
# BB#38:
	imull	$20021, %esi, %edi      # imm = 0x4E35
	movw	%di, ax(%rip)
	jmp	.LBB0_39
.LBB0_37:
	xorl	%edi, %edi
.LBB0_39:                               # %code.exit4
	imull	$346, %ecx, %esi        # imm = 0x15A
	addl	%esi, %edi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdx,%rdi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	13(%r8), %edi
	movw	%di, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+26(%rip), %eax
	shll	$8, %eax
	movzbl	cle+27(%rip), %edx
	orl	%eax, %edx
	xorw	x1a0+24(%rip), %dx
	movw	%dx, x1a0+26(%rip)
	movzwl	%di, %r9d
	addw	%cx, %di
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%si, tmp(%rip)
	movw	%di, ax(%rip)
	movw	%si, dx(%rip)
	je	.LBB0_40
# BB#41:
	imull	$20021, %edi, %edx      # imm = 0x4E35
	movw	%dx, ax(%rip)
	jmp	.LBB0_42
.LBB0_40:
	xorl	%edx, %edx
.LBB0_42:                               # %code.exit3
	imull	$346, %ecx, %edi        # imm = 0x15A
	addl	%edi, %edx
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rsi,%rdx), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	leal	14(%r8), %edx
	movw	%dx, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movl	cle+28(%rip), %eax
	shll	$8, %eax
	movzbl	cle+29(%rip), %esi
	orl	%eax, %esi
	xorw	x1a0+26(%rip), %si
	movw	%si, x1a0+28(%rip)
	movzwl	%dx, %r9d
	addw	%cx, %dx
	movw	x1a0(%r9,%r9), %cx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%cx, si(%rip)
	movw	%di, tmp(%rip)
	movw	%dx, ax(%rip)
	movw	%di, dx(%rip)
	je	.LBB0_43
# BB#44:
	imull	$20021, %edx, %esi      # imm = 0x4E35
	movw	%si, ax(%rip)
	jmp	.LBB0_45
.LBB0_43:
	xorl	%esi, %esi
.LBB0_45:                               # %code.exit2
	imull	$346, %ecx, %edx        # imm = 0x15A
	addl	%edx, %esi
	imull	$20021, %ecx, %eax      # imm = 0x4E35
	leal	(%rdi,%rsi), %ecx
	incl	%eax
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%r9,%r9)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	addl	$15, %r8d
	movw	%r8w, i(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movzbl	cle+30(%rip), %eax
	shll	$8, %eax
	movzbl	cle+31(%rip), %esi
	orl	%eax, %esi
	xorw	x1a0+28(%rip), %si
	movw	%si, x1a0+30(%rip)
	movzwl	%r8w, %esi
	addw	%cx, %r8w
	movw	x1a0(%rsi,%rsi), %di
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	%di, si(%rip)
	movw	%dx, tmp(%rip)
	movw	%r8w, ax(%rip)
	movw	%dx, dx(%rip)
	je	.LBB0_46
# BB#47:
	imull	$20021, %r8d, %ecx      # imm = 0x4E35
	movw	%cx, ax(%rip)
	jmp	.LBB0_48
.LBB0_46:
	xorl	%ecx, %ecx
.LBB0_48:                               # %code.exit1
	imull	$346, %edi, %eax        # imm = 0x15A
	addl	%eax, %ecx
	movw	%cx, cx(%rip)
	movw	%ax, tmp(%rip)
	movw	%ax, si(%rip)
	imull	$20021, %edi, %eax      # imm = 0x4E35
	leal	(%rdx,%rcx), %ecx
	movw	%cx, dx(%rip)
	incl	%eax
	movw	%ax, ax(%rip)
	movw	%cx, x1a2(%rip)
	movw	%ax, x1a0(%rsi,%rsi)
	xorl	%ecx, %eax
	movw	%ax, res(%rip)
	xorl	%eax, %r10d
	movw	%r10w, inter(%rip)
	movw	$0, i(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	assemble, .Lfunc_end0-assemble
	.cfi_endproc

	.globl	code
	.p2align	4, 0x90
	.type	code,@function
code:                                   # @code
	.cfi_startproc
# BB#0:
	movzwl	i(%rip), %eax
	movw	x1a2(%rip), %si
	addw	%ax, %si
	movw	x1a0(%rax,%rax), %dx
	movw	$346, cx(%rip)          # imm = 0x15A
	movw	$20021, bx(%rip)        # imm = 0x4E35
	movw	si(%rip), %cx
	movw	%dx, si(%rip)
	movw	%cx, tmp(%rip)
	movw	%si, ax(%rip)
	movw	%cx, dx(%rip)
	je	.LBB1_1
# BB#2:
	imull	$20021, %esi, %esi      # imm = 0x4E35
	movw	%si, ax(%rip)
	jmp	.LBB1_3
.LBB1_1:
	xorl	%esi, %esi
.LBB1_3:
	imull	$346, %edx, %edi        # imm = 0x15A
	addl	%edi, %esi
	movw	%si, cx(%rip)
	movw	%di, tmp(%rip)
	movw	%di, si(%rip)
	imull	$20021, %edx, %edx      # imm = 0x4E35
	addl	%esi, %ecx
	movw	%cx, dx(%rip)
	incl	%edx
	movw	%dx, ax(%rip)
	movw	%cx, x1a2(%rip)
	movw	%dx, x1a0(%rax,%rax)
	xorl	%ecx, %edx
	movw	%dx, res(%rip)
	leal	1(%rax), %eax
	movw	%ax, i(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	code, .Lfunc_end1-code
	.cfi_endproc

	.globl	my_rand_r
	.p2align	4, 0x90
	.type	my_rand_r,@function
my_rand_r:                              # @my_rand_r
	.cfi_startproc
# BB#0:
	imull	$1664525, (%rdi), %eax  # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	movl	%eax, (%rdi)
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	retq
.Lfunc_end2:
	.size	my_rand_r, .Lfunc_end2-my_rand_r
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	$19999, %r14d           # imm = 0x4E1F
	cmpl	$2, %edi
	jne	.LBB3_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	decl	%r14d
.LBB3_2:
	movw	$0, si(%rip)
	movw	$0, x1a2(%rip)
	movw	$0, i(%rip)
	movups	.L.str+16(%rip), %xmm0
	movaps	%xmm0, cle+16(%rip)
	movdqu	.L.str(%rip), %xmm0
	movdqa	%xmm0, cle(%rip)
	movl	$.Lstr, %edi
	callq	puts
	movb	$0, buff+1(%rip)
	movl	$buff, %edi
	callq	strlen
	cmpq	$32, %rax
	movl	$32, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, count(%rip)
	movl	$0, c1(%rip)
	testl	%ecx, %ecx
	jle	.LBB3_15
# BB#3:                                 # %.lr.ph17.preheader
	xorl	%edx, %edx
	cmpl	$31, %ecx
	jbe	.LBB3_4
# BB#6:                                 # %min.iters.checked
	movl	%ecx, %eax
	andl	$32, %eax
	je	.LBB3_4
# BB#7:                                 # %vector.body.preheader
	leal	-32(%rax), %esi
	movl	%esi, %edi
	shrl	$5, %edi
	incl	%edi
	andl	$3, %edi
	je	.LBB3_8
# BB#9:                                 # %vector.body.prol.preheader
	negl	%edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_10:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqa	buff(%rdx), %xmm0
	movdqa	buff+16(%rdx), %xmm1
	movdqa	%xmm0, cle(%rdx)
	movdqa	%xmm1, cle+16(%rdx)
	addq	$32, %rdx
	incl	%edi
	jne	.LBB3_10
	jmp	.LBB3_11
.LBB3_8:
	xorl	%edx, %edx
.LBB3_11:                               # %vector.body.prol.loopexit
	cmpl	$96, %esi
	jb	.LBB3_13
	.p2align	4, 0x90
.LBB3_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movaps	buff(%rdx), %xmm0
	movaps	buff+16(%rdx), %xmm1
	movaps	%xmm0, cle(%rdx)
	movaps	%xmm1, cle+16(%rdx)
	leal	32(%rdx), %esi
	movslq	%esi, %rsi
	movaps	buff(%rsi), %xmm0
	movaps	buff+16(%rsi), %xmm1
	movaps	%xmm0, cle(%rsi)
	movaps	%xmm1, cle+16(%rsi)
	leal	64(%rdx), %esi
	movslq	%esi, %rsi
	movaps	buff(%rsi), %xmm0
	movaps	buff+16(%rsi), %xmm1
	movaps	%xmm0, cle(%rsi)
	movaps	%xmm1, cle+16(%rsi)
	leal	96(%rdx), %esi
	movslq	%esi, %rsi
	movdqa	buff(%rsi), %xmm0
	movdqa	buff+16(%rsi), %xmm1
	movdqa	%xmm0, cle(%rsi)
	movdqa	%xmm1, cle+16(%rsi)
	subl	$-128, %edx
	cmpl	%edx, %eax
	jne	.LBB3_12
.LBB3_13:                               # %middle.block
	cmpl	%eax, %ecx
	movl	%eax, %edx
	je	.LBB3_14
.LBB3_4:                                # %.lr.ph17.preheader22
	movslq	%edx, %rax
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph17
                                        # =>This Inner Loop Header: Depth=1
	movzbl	buff(%rax), %edx
	movb	%dl, cle(%rax)
	incq	%rax
	cmpl	%ecx, %eax
	jl	.LBB3_5
.LBB3_14:                               # %..preheader_crit_edge
	movl	%eax, c1(%rip)
.LBB3_15:                               # %.preheader
	testl	%r14d, %r14d
	je	.LBB3_20
# BB#16:                                # %.lr.ph.preheader
	movl	$1, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	$1664525, %ebp, %ebp    # imm = 0x19660D
	addl	$1013904223, %ebp       # imm = 0x3C6EF35F
	movl	%ebp, %eax
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	movw	%ax, c(%rip)
	callq	assemble
	movzwl	inter(%rip), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movw	%ax, cfc(%rip)
	movw	%cx, cfd(%rip)
	movzwl	c(%rip), %ecx
	movd	%ecx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	cle(%rip), %xmm1
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, cle(%rip)
	pxor	cle+16(%rip), %xmm0
	movdqa	%xmm0, cle+16(%rip)
	movw	$32, compte(%rip)
	movw	cfc(%rip), %ax
	xorw	%cx, %ax
	xorw	cfd(%rip), %ax
	movw	%ax, c(%rip)
	movl	%eax, %ecx
	sarw	$4, %cx
	movw	%cx, d(%rip)
	andl	$15, %eax
	movw	%ax, e(%rip)
	incl	%ebx
	testw	$2047, %bx              # imm = 0x7FF
	jne	.LBB3_19
# BB#18:                                #   in Loop: Header=BB3_17 Depth=1
	movswl	%cx, %esi
	movzwl	%ax, %edx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.LBB3_19:                               # %.backedge
                                        #   in Loop: Header=BB3_17 Depth=1
	cmpl	%ebx, %r14d
	jne	.LBB3_17
.LBB3_20:                               # %._crit_edge
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	cle,@object             # @cle
	.comm	cle,32,16
	.type	x1a0,@object            # @x1a0
	.comm	x1a0,32,16
	.type	res,@object             # @res
	.comm	res,2,2
	.type	inter,@object           # @inter
	.comm	inter,2,2
	.type	i,@object               # @i
	.comm	i,2,2
	.type	x1a2,@object            # @x1a2
	.comm	x1a2,2,2
	.type	dx,@object              # @dx
	.comm	dx,2,2
	.type	ax,@object              # @ax
	.comm	ax,2,2
	.type	cx,@object              # @cx
	.comm	cx,2,2
	.type	bx,@object              # @bx
	.comm	bx,2,2
	.type	tmp,@object             # @tmp
	.comm	tmp,2,2
	.type	si,@object              # @si
	.comm	si,2,2
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"abcdefghijklmnopqrstuvwxyz012345"
	.size	.L.str, 33

	.type	buff,@object            # @buff
	.comm	buff,32,16
	.type	count,@object           # @count
	.comm	count,4,4
	.type	c1,@object              # @c1
	.comm	c1,4,4
	.type	c,@object               # @c
	.comm	c,2,2
	.type	cfc,@object             # @cfc
	.comm	cfc,2,2
	.type	cfd,@object             # @cfd
	.comm	cfd,2,2
	.type	compte,@object          # @compte
	.comm	compte,2,2
	.type	d,@object               # @d
	.comm	d,2,2
	.type	e,@object               # @e
	.comm	e,2,2
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d %d "
	.size	.L.str.2, 7

	.type	in,@object              # @in
	.comm	in,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"PC1 Cipher 256 bits \nENCRYPT file IN.BIN to OUT.BIN"
	.size	.Lstr, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
