	.text
	.file	"parset.bc"
	.globl	GenerateParameterSets
	.p2align	4, 0x90
	.type	GenerateParameterSets,@function
GenerateParameterSets:                  # @GenerateParameterSets
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	callq	AllocSPS
	movq	%rax, %rbx
	movl	$PicParSet, %edi
	xorl	%esi, %esi
	movl	$2048, %edx             # imm = 0x800
	callq	memset
	movq	%rbx, %rdi
	callq	GenerateSequenceParameterSet
	movq	input(%rip), %rax
	movl	1576(%rax), %ebp
	callq	AllocPPS
	cmpl	$0, %ebp
	movq	%rax, PicParSet(%rip)
	je	.LBB0_5
# BB#1:
	callq	AllocPPS
	movq	%rax, PicParSet+8(%rip)
	callq	AllocPPS
	movq	%rax, PicParSet+16(%rip)
	movq	PicParSet(%rip), %rdi
	movq	input(%rip), %rax
	cmpl	$100, 4(%rbx)
	jb	.LBB0_3
# BB#2:
	movl	5276(%rax), %r9d
	movl	5280(%rax), %eax
	movl	%eax, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	callq	GeneratePictureParameterSet
	movq	PicParSet+8(%rip), %rdi
	movq	input(%rip), %rax
	movl	5276(%rax), %r9d
	movl	5280(%rax), %eax
	movl	%eax, (%rsp)
	movl	$1, %edx
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	callq	GeneratePictureParameterSet
	movq	PicParSet+16(%rip), %rdi
	movq	input(%rip), %rax
	movl	5276(%rax), %r9d
	movl	5280(%rax), %eax
	movl	%eax, (%rsp)
	jmp	.LBB0_4
.LBB0_5:
	movq	input(%rip), %rdx
	movl	2928(%rdx), %ecx
	movl	2932(%rdx), %r8d
	cmpl	$100, 4(%rbx)
	jb	.LBB0_7
# BB#6:
	movl	5276(%rdx), %r9d
	movl	5280(%rdx), %edx
	movl	%edx, (%rsp)
	jmp	.LBB0_8
.LBB0_3:
	movl	4136(%rax), %r9d
	movl	$0, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	callq	GeneratePictureParameterSet
	movq	PicParSet+8(%rip), %rdi
	movq	input(%rip), %rax
	movl	4136(%rax), %r9d
	movl	$0, (%rsp)
	movl	$1, %edx
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	callq	GeneratePictureParameterSet
	movq	PicParSet+16(%rip), %rdi
	movq	input(%rip), %rax
	movl	4136(%rax), %r9d
	movl	$0, (%rsp)
.LBB0_4:
	movl	$2, %edx
	movl	$1, %ecx
	movl	$2, %r8d
	jmp	.LBB0_9
.LBB0_7:
	movl	4136(%rdx), %r9d
	movl	$0, (%rsp)
.LBB0_8:
	xorl	%edx, %edx
	movq	%rax, %rdi
.LBB0_9:
	movq	%rbx, %rsi
	callq	GeneratePictureParameterSet
	movq	%rbx, active_sps(%rip)
	movq	PicParSet(%rip), %rax
	movq	%rax, active_pps(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	GenerateParameterSets, .Lfunc_end0-GenerateParameterSets
	.cfi_endproc

	.globl	GenerateSequenceParameterSet
	.p2align	4, 0x90
	.type	GenerateSequenceParameterSet,@function
GenerateSequenceParameterSet:           # @GenerateSequenceParameterSet
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	input(%rip), %rsi
	movl	(%rsi), %eax
	leal	-100(%rax), %ecx
	cmpl	$44, %ecx
	ja	.LBB1_1
# BB#2:                                 # %switch.lookup
	movabsq	$17592190239745, %r14   # imm = 0x100000400401
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %r14
	andl	$1, %r14d
	jmp	.LBB1_3
.LBB1_1:
	xorl	%r14d, %r14d
.LBB1_3:
	movl	%eax, 4(%rbx)
	movl	4(%rsi), %ecx
	movl	%ecx, 24(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	cmpl	$9, %ecx
	jne	.LBB1_6
# BB#4:
	cmpl	$99, %eax
	ja	.LBB1_6
# BB#5:
	movabsq	$47244640257, %rcx      # imm = 0xB00000001
	movq	%rcx, 20(%rbx)
	jmp	.LBB1_7
.LBB1_6:
	movl	$0, 20(%rbx)
.LBB1_7:
	movl	$0, 28(%rbx)
	movl	5256(%rsi), %ecx
	addl	$-8, %ecx
	movl	%ecx, 72(%rbx)
	movl	5260(%rsi), %ecx
	addl	$-8, %ecx
	movl	%ecx, 76(%rbx)
	xorl	%ecx, %ecx
	cmpl	$144, %eax
	sete	%cl
	andl	5284(%rsi), %ecx
	movq	img(%rip), %rax
	movl	%ecx, 15540(%rax)
	movl	log2_max_frame_num_minus4(%rip), %ecx
	movl	%ecx, 80(%rbx)
	movl	log2_max_pic_order_cnt_lsb_minus4(%rip), %ecx
	movl	%ecx, 88(%rbx)
	movl	5088(%rsi), %ecx
	movl	%ecx, 84(%rbx)
	movdqu	15276(%rax), %xmm0
	movdqu	%xmm0, 92(%rbx)
	pshufd	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	movd	%xmm0, %ecx
	testl	%ecx, %ecx
	je	.LBB1_10
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movl	15292(%rax,%rdx,4), %edi
	movl	%edi, 108(%rbx,%rdx,4)
	incl	%ecx
	cmpl	15288(%rax), %ecx
	jb	.LBB1_9
.LBB1_10:                               # %._crit_edge
	movl	32(%rsi), %ecx
	movl	%ecx, 1132(%rbx)
	movl	$0, 1136(%rbx)
	movl	4708(%rsi), %r8d
	xorl	%edx, %edx
	movl	4704(%rsi), %ecx
	orl	%r8d, %ecx
	sete	%dl
	movl	%edx, 1148(%rbx)
	movl	15584(%rax), %ecx
	addl	56(%rsi), %ecx
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$28, %edi
	addl	%ecx, %edi
	sarl	$4, %edi
	decl	%edi
	movl	%edi, 1140(%rbx)
	movl	15588(%rax), %ecx
	addl	60(%rsi), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ecx, %eax
	sarl	$4, %eax
	movl	$2, %ecx
	subl	%edx, %ecx
	xorl	%edx, %edx
	divl	%ecx
	decl	%eax
	movl	%eax, 1144(%rbx)
	xorl	%edx, %edx
	testl	%r8d, %r8d
	setne	%dl
	movl	%edx, 1152(%rbx)
	movl	2116(%rsi), %ebp
	movl	%ebp, 1156(%rbx)
	movl	64(%rsi), %ebp
	cmpl	$0, 5272(%rsi)
	je	.LBB1_13
# BB#11:                                # %._crit_edge
	cmpl	$3, %ebp
	jne	.LBB1_13
# BB#12:
	movb	$1, %sil
	movl	$3, %ebp
	jmp	.LBB1_14
.LBB1_13:                               # %._crit_edge120
	cmpl	$0, 1580(%rsi)
	setne	%sil
.LBB1_14:
	movzbl	%sil, %esi
	movl	%esi, 1180(%rbx)
	movl	%ebp, 32(%rbx)
	shll	$4, %edi
	addl	$16, %edi
	shll	$4, %eax
	addl	$16, %eax
	imull	%ecx, %eax
	movl	%eax, %esi
	callq	alloc_colocated
	movq	%rax, Co_located(%rip)
	testb	%r14b, %r14b
	je	.LBB1_15
# BB#23:
	movq	input(%rip), %rax
	movl	5208(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 36(%rbx)
	movl	5212(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 40(%rbx)
	movl	5216(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 44(%rbx)
	movl	5220(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 48(%rbx)
	movl	5224(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 52(%rbx)
	movl	5228(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 56(%rbx)
	movl	5232(%rax), %ecx
	andl	$1, %ecx
	movl	%ecx, 60(%rbx)
	xorl	%ecx, %ecx
	cmpl	$0, 5100(%rax)
	movl	$0, %edx
	je	.LBB1_25
# BB#24:                                # %.sink.split.6
	movl	5236(%rax), %edx
	andl	$1, %edx
.LBB1_25:
	movl	%edx, 64(%rbx)
	cmpl	$0, 5100(%rax)
	je	.LBB1_27
# BB#26:                                # %.sink.split.7
	movl	5240(%rax), %ecx
	andl	$1, %ecx
.LBB1_27:                               # %.loopexit.loopexit100
	movl	%ecx, 68(%rbx)
	jmp	.LBB1_16
.LBB1_15:                               # %.loopexit.loopexit99
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 52(%rbx)
	movdqu	%xmm0, 36(%rbx)
	movl	$0, 68(%rbx)
.LBB1_16:                               # %.loopexit
	movq	img(%rip), %rcx
	movl	15584(%rcx), %eax
	movl	15588(%rcx), %ecx
	movl	%eax, %edx
	orl	%ecx, %edx
	je	.LBB1_21
# BB#17:                                # %.loopexit._crit_edge
	movq	$1, 1160(%rbx)
	movl	$0, 1172(%rbx)
	movl	32(%rbx), %esi
	cltd
	idivl	.LGenerateSequenceParameterSet.SubWidthC(,%rsi,4)
	movl	%edx, %r8d
	movl	%eax, 1168(%rbx)
	movl	.LGenerateSequenceParameterSet.SubHeightC(,%rsi,4), %r9d
	movl	1148(%rbx), %esi
	movl	$2, %ebp
	movl	$2, %edi
	subl	%esi, %edi
	imull	%r9d, %edi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%edi
	movl	%eax, 1176(%rbx)
	testl	%r8d, %r8d
	je	.LBB1_19
# BB#18:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	img(%rip), %rax
	movl	15588(%rax), %ecx
	movl	32(%rbx), %eax
	movl	.LGenerateSequenceParameterSet.SubHeightC(,%rax,4), %r9d
	movl	1148(%rbx), %esi
.LBB1_19:
	subl	%esi, %ebp
	imull	%r9d, %ebp
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebp
	testl	%edx, %edx
	je	.LBB1_22
# BB#20:
	movl	$.L.str.1, %edi
	movl	$500, %esi              # imm = 0x1F4
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB1_21:
	movl	$0, 1160(%rbx)
.LBB1_22:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	GenerateSequenceParameterSet, .Lfunc_end1-GenerateSequenceParameterSet
	.cfi_endproc

	.globl	GeneratePictureParameterSet
	.p2align	4, 0x90
	.type	GeneratePictureParameterSet,@function
GeneratePictureParameterSet:            # @GeneratePictureParameterSet
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%ecx, %r11d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	input(%rip), %rsi
	movl	(%rsi), %ecx
	addl	$-100, %ecx
	cmpl	$44, %ecx
	ja	.LBB2_2
# BB#1:                                 # %switch.lookup
	movabsq	$17592190239745, %r13   # imm = 0x100000400401
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %r13
	andl	$1, %r13d
	jmp	.LBB2_3
.LBB2_2:
	xorl	%r13d, %r13d
.LBB2_3:
	movl	28(%rbp), %eax
	movl	%eax, 8(%rbx)
	movl	%edx, 4(%rbx)
	xorl	%eax, %eax
	cmpl	$0, 4008(%rsi)
	setne	%al
	movl	%eax, 12(%rbx)
	testb	%r13b, %r13b
	je	.LBB2_6
# BB#4:
	movl	5100(%rsi), %eax
	xorl	%ecx, %ecx
	cmpl	$0, %eax
	setne	%cl
	movl	%ecx, 16(%rbx)
	movl	5208(%rsi), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 20(%rbx)
	movq	input(%rip), %r15
	movl	5212(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 24(%rbx)
	movl	5216(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 28(%rbx)
	movl	5220(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 32(%rbx)
	movl	5224(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 36(%rbx)
	movl	5228(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	movl	%ecx, 40(%rbx)
	movl	5232(%r15), %ecx
	shrl	%ecx
	andl	$1, %ecx
	cmpl	$0, %eax
	movl	%ecx, 44(%rbx)
	je	.LBB2_7
# BB#5:                                 # %.sink.split.7
	movl	5236(%r15), %eax
	shrl	%eax
	andl	$1, %eax
	movl	%eax, 48(%rbx)
	movl	5240(%r15), %eax
	shrl	%eax
	andl	$1, %eax
	jmp	.LBB2_8
.LBB2_6:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	$0, 48(%rbx)
	movq	input(%rip), %r15
	movl	$0, 5100(%r15)
	jmp	.LBB2_9
.LBB2_7:
	movl	$0, 48(%rbx)
	xorl	%eax, %eax
.LBB2_8:                                # %.loopexit103
	movl	%eax, 52(%rbx)
.LBB2_9:                                # %.loopexit103
	movl	64(%rsp), %r14d
	movq	img(%rip), %rax
	movl	15356(%rax), %eax
	movl	%eax, 56(%rbx)
	movl	5032(%r15), %eax
	movl	%eax, 60(%rbx)
	testl	%eax, %eax
	je	.LBB2_20
# BB#10:
	movl	%r11d, (%rsp)           # 4-byte Spill
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movl	1140(%rbp), %edi
	movl	1144(%rbp), %eax
	incl	%eax
	incl	%edi
	imull	%eax, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 176(%rbx)
	testq	%rax, %rax
	jne	.LBB2_12
# BB#11:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movq	input(%rip), %r15
.LBB2_12:
	movl	5036(%r15), %eax
	cmpq	$6, %rax
	ja	.LBB2_15
# BB#13:
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_14:
	movl	%eax, 64(%rbx)
	movl	5072(%r15), %eax
	movl	%eax, 164(%rbx)
	movl	5076(%r15), %eax
	movl	%eax, 168(%rbx)
	jmp	.LBB2_19
.LBB2_15:
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB2_19
.LBB2_16:
	movl	$0, 64(%rbx)
	movq	5064(%r15), %rax
	movl	60(%rbx), %ecx
	xorl	%edx, %edx
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	movl	(%rax,%rsi,4), %edi
	movl	%edi, 68(%rbx,%rsi,4)
	incl	%edx
	cmpl	%ecx, %edx
	jbe	.LBB2_17
	jmp	.LBB2_20
.LBB2_18:
	movl	$1, 64(%rbx)
.LBB2_19:                               # %.loopexit
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
.LBB2_20:                               # %.loopexit
	movl	1132(%rbp), %eax
	cmpl	$0, 1148(%rbp)
	sete	%cl
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 184(%rbx)
	movl	%eax, 188(%rbx)
	movl	%r11d, 192(%rbx)
	movl	%r8d, 196(%rbx)
	movl	$0, 200(%rbx)
	movl	$0, 204(%rbx)
	movl	%r12d, 208(%rbx)
	testb	%r13b, %r13b
	cmovel	%r12d, %r14d
	movl	%r14d, 216(%rbx)
	movl	%r12d, 212(%rbx)
	movq	input(%rip), %rax
	movl	4748(%rax), %ecx
	movl	%ecx, 220(%rbx)
	movl	272(%rax), %ecx
	movl	%ecx, 224(%rbx)
	movl	5084(%rax), %eax
	movl	%eax, 228(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_21:
	movl	$2, 64(%rbx)
	movl	60(%rbx), %eax
	testl	%eax, %eax
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	je	.LBB2_20
# BB#22:                                # %.lr.ph
	movq	5040(%r15), %rcx
	movq	5048(%r15), %rdx
	xorl	%edi, %edi
	cmpl	$4, %eax
	jb	.LBB2_37
# BB#23:                                # %min.iters.checked
	movl	%eax, %r9d
	andl	$-4, %r9d
	je	.LBB2_37
# BB#24:                                # %vector.memcheck
	leaq	100(%rbx), %r8
	leal	-1(%rax), %esi
	leaq	136(%rbx,%rsi,4), %r10
	leaq	4(%rcx,%rsi,4), %r11
	leaq	4(%rdx,%rsi,4), %rsi
	cmpq	%r11, %r8
	sbbb	%r11b, %r11b
	cmpq	%r10, %rcx
	sbbb	%r15b, %r15b
	andb	%r11b, %r15b
	cmpq	%rsi, %r8
	sbbb	%sil, %sil
	cmpq	%r10, %rdx
	sbbb	%r8b, %r8b
	testb	$1, %r15b
	jne	.LBB2_32
# BB#25:                                # %vector.memcheck
	andb	%r8b, %sil
	andb	$1, %sil
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	jne	.LBB2_37
# BB#26:                                # %vector.body.preheader
	leal	-4(%r9), %r10d
	movl	%r10d, %esi
	shrl	$2, %esi
	incl	%esi
	andl	$3, %esi
	je	.LBB2_33
# BB#27:                                # %vector.body.prol.preheader
	negl	%esi
	xorl	%edi, %edi
.LBB2_28:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	%xmm0, 100(%rbx,%rdi,4)
	movups	(%rdx,%rdi,4), %xmm0
	movups	%xmm0, 132(%rbx,%rdi,4)
	addq	$4, %rdi
	incl	%esi
	jne	.LBB2_28
	jmp	.LBB2_34
.LBB2_29:
	movl	$6, 64(%rbx)
	movq	img(%rip), %rax
	movl	15584(%rax), %ecx
	movl	15588(%rax), %edx
	addl	60(%r15), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%edx, %eax
	sarl	$4, %eax
	movl	$2, %esi
	subl	1148(%rbp), %esi
	xorl	%edx, %edx
	divl	%esi
	addl	56(%r15), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	imull	%eax, %edx
	decl	%edx
	movl	%edx, 172(%rbx)
	movq	5056(%r15), %rax
	movb	(%rax), %al
	movq	176(%rbx), %rcx
	movb	%al, (%rcx)
	cmpl	$0, 172(%rbx)
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	je	.LBB2_20
# BB#30:                                # %._crit_edge119.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB2_31:                               # %._crit_edge119
                                        # =>This Inner Loop Header: Depth=1
	movq	input(%rip), %rcx
	movq	5056(%rcx), %rcx
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %ecx
	movq	176(%rbx), %rsi
	movb	%cl, (%rsi,%rdx)
	incl	%eax
	cmpl	172(%rbx), %eax
	jbe	.LBB2_31
	jmp	.LBB2_20
.LBB2_32:
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	jmp	.LBB2_37
.LBB2_33:
	xorl	%edi, %edi
.LBB2_34:                               # %vector.body.prol.loopexit
	cmpl	$12, %r10d
	jb	.LBB2_36
.LBB2_35:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %esi
	movups	(%rcx,%rsi,4), %xmm0
	movups	%xmm0, 100(%rbx,%rsi,4)
	movups	(%rdx,%rsi,4), %xmm0
	movups	%xmm0, 132(%rbx,%rsi,4)
	leal	4(%rdi), %esi
	movups	(%rcx,%rsi,4), %xmm0
	movups	%xmm0, 100(%rbx,%rsi,4)
	movups	(%rdx,%rsi,4), %xmm0
	movups	%xmm0, 132(%rbx,%rsi,4)
	leal	8(%rdi), %esi
	movups	(%rcx,%rsi,4), %xmm0
	movups	%xmm0, 100(%rbx,%rsi,4)
	movups	(%rdx,%rsi,4), %xmm0
	movups	%xmm0, 132(%rbx,%rsi,4)
	leal	12(%rdi), %esi
	movups	(%rcx,%rsi,4), %xmm0
	movups	%xmm0, 100(%rbx,%rsi,4)
	movups	(%rdx,%rsi,4), %xmm0
	movups	%xmm0, 132(%rbx,%rsi,4)
	addl	$16, %edi
	cmpl	%edi, %r9d
	jne	.LBB2_35
.LBB2_36:                               # %middle.block
	cmpl	%r9d, %eax
	movl	%r9d, %edi
	je	.LBB2_20
.LBB2_37:                               # %scalar.ph.preheader
	movl	%edi, %esi
	.p2align	4, 0x90
.LBB2_38:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edi
	movl	%edi, 100(%rbx,%rsi,4)
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, 132(%rbx,%rsi,4)
	incq	%rsi
	cmpl	%eax, %esi
	jb	.LBB2_38
	jmp	.LBB2_20
.Lfunc_end2:
	.size	GeneratePictureParameterSet, .Lfunc_end2-GeneratePictureParameterSet
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_16
	.quad	.LBB2_18
	.quad	.LBB2_21
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_29

	.text
	.globl	FreeParameterSets
	.p2align	4, 0x90
	.type	FreeParameterSets,@function
FreeParameterSets:                      # @FreeParameterSets
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	$-2048, %rbx            # imm = 0xF800
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	PicParSet+2048(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	callq	FreePPS
	movq	$0, PicParSet+2048(%rbx)
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	addq	$8, %rbx
	jne	.LBB3_1
# BB#4:
	movq	active_sps(%rip), %rdi
	popq	%rbx
	jmp	FreeSPS                 # TAILCALL
.Lfunc_end3:
	.size	FreeParameterSets, .Lfunc_end3-FreeParameterSets
	.cfi_endproc

	.globl	GenerateSeq_parameter_set_NALU
	.p2align	4, 0x90
	.type	GenerateSeq_parameter_set_NALU,@function
GenerateSeq_parameter_set_NALU:         # @GenerateSeq_parameter_set_NALU
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	subq	$64024, %rsp            # imm = 0xFA18
.Lcfi28:
	.cfi_def_cfa_offset 64048
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movl	$64000, %edi            # imm = 0xFA00
	callq	AllocNALU
	movq	%rax, %rbx
	movq	active_sps(%rip), %rdi
	leaq	16(%rsp), %r14
	movq	%r14, %rsi
	callq	GenerateSeq_parameter_set_rbsp
	movl	$1, (%rsp)
	movl	$7, %ecx
	movl	$3, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	RBSPtoNALU
	movl	$4, (%rbx)
	movq	%rbx, %rax
	addq	$64024, %rsp            # imm = 0xFA18
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	GenerateSeq_parameter_set_NALU, .Lfunc_end4-GenerateSeq_parameter_set_NALU
	.cfi_endproc

	.globl	GenerateSeq_parameter_set_rbsp
	.p2align	4, 0x90
	.type	GenerateSeq_parameter_set_rbsp,@function
GenerateSeq_parameter_set_rbsp:         # @GenerateSeq_parameter_set_rbsp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$1, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB5_2:
	movq	%r15, 32(%r14)
	movl	$8, 4(%r14)
	movl	4(%rbp), %edx
	movl	$8, %edi
	movl	$.L.str.6, %esi
	movq	%r14, %rcx
	callq	u_v
	movl	8(%rbp), %esi
	movl	$.L.str.7, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	12(%rbp), %esi
	movl	$.L.str.8, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	16(%rbp), %esi
	movl	$.L.str.9, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	20(%rbp), %esi
	movl	$.L.str.10, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	$4, %edi
	movl	$.L.str.11, %esi
	xorl	%edx, %edx
	movq	%r14, %rcx
	callq	u_v
	movl	24(%rbp), %edx
	movl	$8, %edi
	movl	$.L.str.12, %esi
	movq	%r14, %rcx
	callq	u_v
	movl	28(%rbp), %esi
	movl	$.L.str.13, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	4(%rbp), %eax
	addl	$-100, %eax
	cmpl	$44, %eax
	ja	.LBB5_27
# BB#3:
	movabsq	$17592190239745, %rcx   # imm = 0x100000400401
	btq	%rax, %rcx
	jae	.LBB5_27
# BB#4:
	movl	32(%rbp), %esi
	movl	$.L.str.14, %edi
	movq	%r14, %rdx
	callq	ue_v
	movq	img(%rip), %rax
	cmpl	$3, 15536(%rax)
	jne	.LBB5_6
# BB#5:
	movl	$.L.str.15, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
.LBB5_6:
	movl	72(%rbp), %esi
	movl	$.L.str.16, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	76(%rbp), %esi
	movl	$.L.str.17, %edi
	movq	%r14, %rdx
	callq	ue_v
	movq	img(%rip), %rax
	movl	15540(%rax), %esi
	movl	$.L.str.18, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	36(%rbp), %esi
	movl	$.L.str.19, %edi
	movq	%r14, %rdx
	callq	u_1
	cmpl	$0, 36(%rbp)
	je	.LBB5_27
# BB#7:                                 # %.preheader.preheader
	xorl	%r15d, %r15d
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_11 Depth 2
	movl	40(%rbp,%r15,4), %esi
	movl	$.L.str.20, %edi
	movq	%r14, %rdx
	callq	u_1
	cmpl	$0, 40(%rbp,%r15,4)
	je	.LBB5_26
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	cmpq	$5, %r15
	ja	.LBB5_18
# BB#10:                                #   in Loop: Header=BB5_8 Depth=1
	movl	$8, %eax
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movzbl	ZZ_SCAN(%rbx), %r14d
	je	.LBB5_12
# BB#13:                                #   in Loop: Header=BB5_11 Depth=2
	movq	%r15, %rax
	shlq	$5, %rax
	leaq	ScalingList4x4input(%rax,%r14,2), %r12
	movswl	ScalingList4x4input(%rax,%r14,2), %eax
	subl	%ebp, %eax
	cmpl	$127, %eax
	jle	.LBB5_14
# BB#15:                                #   in Loop: Header=BB5_11 Depth=2
	addl	$-256, %eax
	movl	%eax, %esi
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_12:                               #   in Loop: Header=BB5_11 Depth=2
	xorl	%eax, %eax
	jmp	.LBB5_17
	.p2align	4, 0x90
.LBB5_14:                               #   in Loop: Header=BB5_11 Depth=2
	leal	256(%rax), %esi
	cmpl	$-128, %eax
	cmovgel	%eax, %esi
.LBB5_16:                               #   in Loop: Header=BB5_11 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	$.L.str.4, %edi
	callq	se_v
	movswl	(%r12), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbx, %rbx
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	orw	%cx, UseDefaultScalingMatrix4x4Flag(%r15,%r15)
.LBB5_17:                               # %.lr.ph.split.us._crit_edge.i
                                        #   in Loop: Header=BB5_11 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movq	%r15, %rcx
	shlq	$5, %rcx
	movw	%bp, ScalingList4x4(%rcx,%r14,2)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB5_11
	jmp	.LBB5_26
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_8 Depth=1
	leal	-6(%r15), %r13d
	movl	$8, %eax
	xorl	%ebp, %ebp
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph.split.i
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movzbl	ZZ_SCAN8(%rbp), %r14d
	je	.LBB5_20
# BB#21:                                #   in Loop: Header=BB5_19 Depth=2
	movq	%r13, %rax
	shlq	$7, %rax
	leaq	ScalingList8x8input(%rax,%r14,2), %r12
	movswl	ScalingList8x8input(%rax,%r14,2), %esi
	subl	%ebx, %esi
	cmpl	$128, %esi
	jl	.LBB5_23
# BB#22:                                #   in Loop: Header=BB5_19 Depth=2
	addl	$-256, %esi
	jmp	.LBB5_24
	.p2align	4, 0x90
.LBB5_20:                               #   in Loop: Header=BB5_19 Depth=2
	xorl	%eax, %eax
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_19 Depth=2
	leal	256(%rsi), %eax
	cmpl	$-128, %esi
	cmovgel	%esi, %eax
	movl	%eax, %esi
.LBB5_24:                               #   in Loop: Header=BB5_19 Depth=2
	movl	$.L.str.4, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	se_v
	movswl	(%r12), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbp, %rbp
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	orw	%cx, UseDefaultScalingMatrix8x8Flag(%r13,%r13)
.LBB5_25:                               # %.lr.ph.split._crit_edge.i
                                        #   in Loop: Header=BB5_19 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebx
	movq	%r13, %rcx
	shlq	$7, %rcx
	movw	%bx, ScalingList8x8(%rcx,%r14,2)
	incq	%rbp
	cmpq	$64, %rbp
	jne	.LBB5_19
.LBB5_26:                               # %Scaling_List.exit
                                        #   in Loop: Header=BB5_8 Depth=1
	incq	%r15
	cmpq	$8, %r15
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB5_8
.LBB5_27:                               # %.loopexit151
	movl	80(%rbp), %esi
	movl	$.L.str.21, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	84(%rbp), %esi
	movl	$.L.str.22, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	84(%rbp), %eax
	cmpl	$1, %eax
	je	.LBB5_30
# BB#28:                                # %.loopexit151
	testl	%eax, %eax
	jne	.LBB5_33
# BB#29:
	movl	88(%rbp), %esi
	movl	$.L.str.23, %edi
	movq	%r14, %rdx
	callq	ue_v
	jmp	.LBB5_33
.LBB5_30:
	movl	92(%rbp), %esi
	movl	$.L.str.24, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	96(%rbp), %esi
	movl	$.L.str.25, %edi
	movq	%r14, %rdx
	callq	se_v
	movl	100(%rbp), %esi
	movl	$.L.str.26, %edi
	movq	%r14, %rdx
	callq	se_v
	movl	104(%rbp), %esi
	movl	$.L.str.27, %edi
	movq	%r14, %rdx
	callq	ue_v
	cmpl	$0, 104(%rbp)
	je	.LBB5_33
# BB#31:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movl	108(%rbp,%rax,4), %esi
	movl	$.L.str.28, %edi
	movq	%r14, %rdx
	callq	se_v
	incl	%ebx
	cmpl	104(%rbp), %ebx
	jb	.LBB5_32
.LBB5_33:                               # %.loopexit
	movl	1132(%rbp), %esi
	movl	$.L.str.29, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1136(%rbp), %esi
	movl	$.L.str.30, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	1140(%rbp), %esi
	movl	$.L.str.31, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1144(%rbp), %esi
	movl	$.L.str.32, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1148(%rbp), %esi
	movl	$.L.str.33, %edi
	movq	%r14, %rdx
	callq	u_1
	cmpl	$0, 1148(%rbp)
	jne	.LBB5_35
# BB#34:
	movl	1152(%rbp), %esi
	movl	$.L.str.34, %edi
	movq	%r14, %rdx
	callq	u_1
.LBB5_35:
	movl	1156(%rbp), %esi
	movl	$.L.str.35, %edi
	movq	%r14, %rdx
	callq	u_1
	movl	1160(%rbp), %esi
	movl	$.L.str.36, %edi
	movq	%r14, %rdx
	callq	u_1
	cmpl	$0, 1160(%rbp)
	je	.LBB5_37
# BB#36:
	movl	1164(%rbp), %esi
	movl	$.L.str.37, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1168(%rbp), %esi
	movl	$.L.str.38, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1172(%rbp), %esi
	movl	$.L.str.39, %edi
	movq	%r14, %rdx
	callq	ue_v
	movl	1176(%rbp), %esi
	movl	$.L.str.40, %edi
	movq	%r14, %rdx
	callq	ue_v
.LBB5_37:
	movl	1180(%rbp), %esi
	movl	$.L.str.41, %edi
	movq	%r14, %rdx
	callq	u_1
	cmpl	$0, 1180(%rbp)
	je	.LBB5_44
# BB#38:
	movq	input(%rip), %rax
	cmpl	$0, 5272(%rax)
	je	.LBB5_41
# BB#39:
	cmpl	$3, 64(%rax)
	jne	.LBB5_41
# BB#40:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.78, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.79, %edi
	movl	$1, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$3, %edi
	movl	$.L.str.80, %esi
	movl	$2, %edx
	movq	%r14, %rcx
	callq	u_v
	movl	$.L.str.81, %edi
	movl	$1, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.82, %edi
	movl	$1, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$8, %edi
	movl	$.L.str.83, %esi
	movl	$2, %edx
	movq	%r14, %rcx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.84, %esi
	movl	$2, %edx
	movq	%r14, %rcx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.85, %esi
	xorl	%edx, %edx
	movq	%r14, %rcx
	callq	u_v
	jmp	.LBB5_43
.LBB5_41:
	cmpl	$0, 1580(%rax)
	je	.LBB5_45
# BB#42:
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.78, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.79, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
.LBB5_43:                               # %GenerateVUISequenceParameters.exit
	movl	$.L.str.86, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.87, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.88, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.89, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.90, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
	movl	$.L.str.91, %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	u_1
.LBB5_44:                               # %GenerateVUISequenceParameters.exit
	movq	%r14, %rdi
	callq	SODBtoRBSP
	movl	(%r14), %ebx
	movq	%r14, %rdi
	callq	free
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_45:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end5:
	.size	GenerateSeq_parameter_set_rbsp, .Lfunc_end5-GenerateSeq_parameter_set_rbsp
	.cfi_endproc

	.globl	GeneratePic_parameter_set_NALU
	.p2align	4, 0x90
	.type	GeneratePic_parameter_set_NALU,@function
GeneratePic_parameter_set_NALU:         # @GeneratePic_parameter_set_NALU
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	subq	$64024, %rsp            # imm = 0xFA18
.Lcfi46:
	.cfi_def_cfa_offset 64048
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$64000, %edi            # imm = 0xFA00
	callq	AllocNALU
	movq	%rax, %rbx
	movslq	%ebp, %rax
	movq	PicParSet(,%rax,8), %rdi
	leaq	16(%rsp), %rbp
	movq	%rbp, %rsi
	callq	GeneratePic_parameter_set_rbsp
	movl	$1, (%rsp)
	movl	$8, %ecx
	movl	$3, %r8d
	xorl	%r9d, %r9d
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	RBSPtoNALU
	movl	$4, (%rbx)
	movq	%rbx, %rax
	addq	$64024, %rsp            # imm = 0xFA18
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	GeneratePic_parameter_set_NALU, .Lfunc_end6-GeneratePic_parameter_set_NALU
	.cfi_endproc

	.globl	GeneratePic_parameter_set_rbsp
	.p2align	4, 0x90
	.type	GeneratePic_parameter_set_rbsp,@function
GeneratePic_parameter_set_rbsp:         # @GeneratePic_parameter_set_rbsp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 80
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	$1, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB7_2
# BB#1:
	movl	$.L.str.42, %edi
	callq	no_mem_exit
.LBB7_2:
	movq	%rbx, 32(%r15)
	movl	$8, 4(%r15)
	movq	img(%rip), %rax
	movl	15356(%rax), %eax
	movl	%eax, 56(%rbp)
	movl	4(%rbp), %esi
	movl	$.L.str.43, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	8(%rbp), %esi
	movl	$.L.str.44, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	12(%rbp), %esi
	movl	$.L.str.45, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	56(%rbp), %esi
	movl	$.L.str.46, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	60(%rbp), %esi
	movl	$.L.str.47, %edi
	movq	%r15, %rdx
	callq	ue_v
	cmpl	$0, 60(%rbp)
	je	.LBB7_16
# BB#3:
	movl	64(%rbp), %esi
	movl	$.L.str.48, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	64(%rbp), %eax
	cmpq	$6, %rax
	ja	.LBB7_16
# BB#4:
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_10:
	movl	164(%rbp), %esi
	movl	$.L.str.52, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	168(%rbp), %esi
	movl	$.L.str.53, %edi
	movq	%r15, %rdx
	callq	ue_v
	jmp	.LBB7_16
.LBB7_8:                                # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movl	68(%rbp,%rax,4), %esi
	movl	$.L.str.49, %edi
	movq	%r15, %rdx
	callq	ue_v
	incl	%ebx
	cmpl	60(%rbp), %ebx
	jbe	.LBB7_9
	jmp	.LBB7_16
.LBB7_5:                                # %.preheader168
	cmpl	$0, 60(%rbp)
	je	.LBB7_16
# BB#6:                                 # %.lr.ph177.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph177
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %r14d
	movl	100(%rbp,%r14,4), %esi
	movl	$.L.str.50, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	132(%rbp,%r14,4), %esi
	movl	$.L.str.51, %edi
	movq	%r15, %rdx
	callq	ue_v
	incl	%ebx
	cmpl	60(%rbp), %ebx
	jb	.LBB7_7
	jmp	.LBB7_16
.LBB7_11:
	movl	60(%rbp), %eax
	movl	$3, %r14d
	cmpl	$3, %eax
	ja	.LBB7_14
# BB#12:
	movl	$2, %r14d
	cmpl	$1, %eax
	ja	.LBB7_14
# BB#13:
	sete	%al
	movzbl	%al, %r14d
.LBB7_14:
	movl	172(%rbp), %esi
	movl	$.L.str.54, %edi
	movq	%r15, %rdx
	callq	ue_v
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_15:                               # =>This Inner Loop Header: Depth=1
	movq	176(%rbp), %rax
	movl	%ebx, %ecx
	movzbl	(%rax,%rcx), %edx
	movl	$.L.str.55, %esi
	movl	%r14d, %edi
	movq	%r15, %rcx
	callq	u_v
	incl	%ebx
	cmpl	172(%rbp), %ebx
	jbe	.LBB7_15
.LBB7_16:                               # %.loopexit167
	movl	184(%rbp), %esi
	movl	$.L.str.56, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	188(%rbp), %esi
	movl	$.L.str.57, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	192(%rbp), %esi
	movl	$.L.str.58, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	196(%rbp), %edx
	movl	$2, %edi
	movl	$.L.str.59, %esi
	movq	%r15, %rcx
	callq	u_v
	movl	200(%rbp), %esi
	movl	$.L.str.60, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	204(%rbp), %esi
	movl	$.L.str.61, %edi
	movq	%r15, %rdx
	callq	se_v
	movq	input(%rip), %rax
	movl	(%rax), %ebx
	addl	$-100, %ebx
	cmpl	$44, %ebx
	ja	.LBB7_19
# BB#17:                                # %.loopexit167
	movabsq	$17592190239745, %rax   # imm = 0x100000400401
	btq	%rbx, %rax
	jae	.LBB7_19
# BB#18:
	leaq	212(%rbp), %rax
.LBB7_20:
	movl	(%rax), %esi
	movl	$.L.str.62, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	220(%rbp), %esi
	movl	$.L.str.63, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	224(%rbp), %esi
	movl	$.L.str.64, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	228(%rbp), %esi
	movl	$.L.str.65, %edi
	movq	%r15, %rdx
	callq	u_1
	cmpl	$44, %ebx
	ja	.LBB7_45
# BB#21:
	movabsq	$17592190239745, %rax   # imm = 0x100000400401
	btq	%rbx, %rax
	jae	.LBB7_45
# BB#22:
	movl	16(%rbp), %esi
	movl	$.L.str.66, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	20(%rbp), %esi
	movl	$.L.str.67, %edi
	movq	%r15, %rdx
	callq	u_1
	cmpl	$0, 20(%rbp)
	je	.LBB7_44
# BB#23:
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	andl	16(%rbp), %eax
	cmpl	$2147483645, %eax       # imm = 0x7FFFFFFD
	je	.LBB7_44
# BB#24:                                # %.lr.ph.preheader
	xorl	%r12d, %r12d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_36 Depth 2
                                        #     Child Loop BB7_28 Depth 2
	movl	%r12d, %r12d
	movl	24(%rbp,%r12,4), %esi
	movl	$.L.str.68, %edi
	movq	%r15, %rdx
	callq	u_1
	cmpl	$0, 24(%rbp,%r12,4)
	je	.LBB7_43
# BB#26:                                #   in Loop: Header=BB7_25 Depth=1
	cmpl	$5, %r12d
	ja	.LBB7_35
# BB#27:                                #   in Loop: Header=BB7_25 Depth=1
	movl	$8, %eax
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB7_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movzbl	ZZ_SCAN(%rbx), %r14d
	je	.LBB7_29
# BB#30:                                #   in Loop: Header=BB7_28 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	ScalingList4x4input(%rax,%r14,2), %r15
	movswl	ScalingList4x4input(%rax,%r14,2), %eax
	subl	%ebp, %eax
	cmpl	$127, %eax
	jle	.LBB7_31
# BB#32:                                #   in Loop: Header=BB7_28 Depth=2
	addl	$-256, %eax
	movl	%eax, %esi
	jmp	.LBB7_33
	.p2align	4, 0x90
.LBB7_29:                               #   in Loop: Header=BB7_28 Depth=2
	xorl	%eax, %eax
	jmp	.LBB7_34
	.p2align	4, 0x90
.LBB7_31:                               #   in Loop: Header=BB7_28 Depth=2
	leal	256(%rax), %esi
	cmpl	$-128, %eax
	cmovgel	%eax, %esi
.LBB7_33:                               #   in Loop: Header=BB7_28 Depth=2
	movl	$.L.str.4, %edi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	se_v
	movswl	(%r15), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbx, %rbx
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	orw	%cx, UseDefaultScalingMatrix4x4Flag(%r12,%r12)
.LBB7_34:                               # %.lr.ph.split.us._crit_edge.i
                                        #   in Loop: Header=BB7_28 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movq	%r12, %rcx
	shlq	$5, %rcx
	movw	%bp, ScalingList4x4(%rcx,%r14,2)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB7_28
	jmp	.LBB7_43
	.p2align	4, 0x90
.LBB7_35:                               #   in Loop: Header=BB7_25 Depth=1
	leal	-6(%r12), %r13d
	movl	$8, %eax
	xorl	%ebp, %ebp
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB7_36:                               # %.lr.ph.split.i
                                        #   Parent Loop BB7_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movzbl	ZZ_SCAN8(%rbp), %r14d
	je	.LBB7_37
# BB#38:                                #   in Loop: Header=BB7_36 Depth=2
	movq	%r13, %rax
	shlq	$7, %rax
	leaq	ScalingList8x8input(%rax,%r14,2), %r15
	movswl	ScalingList8x8input(%rax,%r14,2), %esi
	subl	%ebx, %esi
	cmpl	$128, %esi
	jl	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_36 Depth=2
	addl	$-256, %esi
	jmp	.LBB7_41
	.p2align	4, 0x90
.LBB7_37:                               #   in Loop: Header=BB7_36 Depth=2
	xorl	%eax, %eax
	jmp	.LBB7_42
	.p2align	4, 0x90
.LBB7_40:                               #   in Loop: Header=BB7_36 Depth=2
	leal	256(%rsi), %eax
	cmpl	$-128, %esi
	cmovgel	%esi, %eax
	movl	%eax, %esi
.LBB7_41:                               #   in Loop: Header=BB7_36 Depth=2
	movl	$.L.str.4, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	se_v
	movswl	(%r15), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbp, %rbp
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	orw	%cx, UseDefaultScalingMatrix8x8Flag(%r13,%r13)
.LBB7_42:                               # %.lr.ph.split._crit_edge.i
                                        #   in Loop: Header=BB7_36 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %ebx
	movq	%r13, %rcx
	shlq	$7, %rcx
	movw	%bx, ScalingList8x8(%rcx,%r14,2)
	incq	%rbp
	cmpq	$64, %rbp
	jne	.LBB7_36
.LBB7_43:                               # %Scaling_List.exit166
                                        #   in Loop: Header=BB7_25 Depth=1
	incl	%r12d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	16(%rbp), %eax
	leal	6(%rax,%rax), %eax
	cmpl	%eax, %r12d
	movq	8(%rsp), %r15           # 8-byte Reload
	jb	.LBB7_25
.LBB7_44:                               # %.loopexit
	movl	216(%rbp), %esi
	movl	$.L.str.69, %edi
	movq	%r15, %rdx
	callq	se_v
.LBB7_45:
	movq	%r15, %rdi
	callq	SODBtoRBSP
	movl	(%r15), %ebx
	movq	%r15, %rdi
	callq	free
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_19:
	leaq	208(%rbp), %rax
	jmp	.LBB7_20
.Lfunc_end7:
	.size	GeneratePic_parameter_set_rbsp, .Lfunc_end7-GeneratePic_parameter_set_rbsp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_8
	.quad	.LBB7_16
	.quad	.LBB7_5
	.quad	.LBB7_10
	.quad	.LBB7_10
	.quad	.LBB7_10
	.quad	.LBB7_11

	.text
	.globl	Scaling_List
	.p2align	4, 0x90
	.type	Scaling_List,@function
Scaling_List:                           # @Scaling_List
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 80
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r14
	testl	%edx, %edx
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph
	cmpl	$16, %edx
	jne	.LBB8_11
# BB#3:                                 # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	movl	$8, %eax
	movl	$8, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	movzbl	ZZ_SCAN(%rbx), %r13d
	je	.LBB8_5
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=1
	movswl	(%r14,%r13,2), %eax
	subl	%ebp, %eax
	cmpl	$127, %eax
	jle	.LBB8_7
# BB#8:                                 #   in Loop: Header=BB8_4 Depth=1
	addl	$-256, %eax
	movl	%eax, %esi
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_4 Depth=1
	xorl	%eax, %eax
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_4 Depth=1
	leal	256(%rax), %esi
	cmpl	$-128, %eax
	cmovgel	%eax, %esi
.LBB8_9:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$.L.str.4, %edi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	se_v
	addl	%eax, %r15d
	movswl	(%r14,%r13,2), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbx, %rbx
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	orw	%cx, (%rdx)
.LBB8_10:                               # %.lr.ph.split.us._crit_edge
                                        #   in Loop: Header=BB8_4 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movw	%bp, (%r12,%r13,2)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB8_4
	jmp	.LBB8_19
.LBB8_1:
	xorl	%r15d, %r15d
	jmp	.LBB8_19
.LBB8_11:                               # %.lr.ph.split.preheader
	movl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$8, %eax
	movl	$8, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	movzbl	ZZ_SCAN8(%rbx), %r13d
	je	.LBB8_13
# BB#14:                                #   in Loop: Header=BB8_12 Depth=1
	movswl	(%r14,%r13,2), %esi
	subl	%ebp, %esi
	cmpl	$128, %esi
	jl	.LBB8_16
# BB#15:                                #   in Loop: Header=BB8_12 Depth=1
	addl	$-256, %esi
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_13:                               #   in Loop: Header=BB8_12 Depth=1
	xorl	%eax, %eax
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_16:                               #   in Loop: Header=BB8_12 Depth=1
	leal	256(%rsi), %eax
	cmpl	$-128, %esi
	cmovgel	%esi, %eax
	movl	%eax, %esi
.LBB8_17:                               #   in Loop: Header=BB8_12 Depth=1
	movl	$.L.str.4, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	se_v
	addl	%eax, %r15d
	movswl	(%r14,%r13,2), %eax
	testl	%eax, %eax
	sete	%cl
	testq	%rbx, %rbx
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	orw	%cx, (%rdx)
.LBB8_18:                               # %.lr.ph.split._crit_edge
                                        #   in Loop: Header=BB8_12 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movw	%bp, (%r12,%r13,2)
	incq	%rbx
	cmpq	%rbx, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB8_12
.LBB8_19:                               # %._crit_edge
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Scaling_List, .Lfunc_end8-Scaling_List
	.cfi_endproc

	.globl	GenerateSEImessage_NALU
	.p2align	4, 0x90
	.type	GenerateSEImessage_NALU,@function
GenerateSEImessage_NALU:                # @GenerateSEImessage_NALU
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 24
	subq	$64024, %rsp            # imm = 0xFA18
.Lcfi77:
	.cfi_def_cfa_offset 64048
.Lcfi78:
	.cfi_offset %rbx, -24
.Lcfi79:
	.cfi_offset %r14, -16
	movl	$64000, %edi            # imm = 0xFA00
	callq	AllocNALU
	movq	%rax, %rbx
	leaq	16(%rsp), %r14
	movq	%r14, %rsi
	callq	GenerateSEImessage_rbsp
	movl	$1, (%rsp)
	movl	$6, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	RBSPtoNALU
	movl	$4, (%rbx)
	movq	%rbx, %rax
	addq	$64024, %rsp            # imm = 0xFA18
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	GenerateSEImessage_NALU, .Lfunc_end9-GenerateSEImessage_NALU
	.cfi_endproc

	.globl	GenerateSEImessage_rbsp
	.p2align	4, 0x90
	.type	GenerateSEImessage_rbsp,@function
GenerateSEImessage_rbsp:                # @GenerateSEImessage_rbsp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 40
	subq	$520, %rsp              # imm = 0x208
.Lcfi84:
	.cfi_def_cfa_offset 560
.Lcfi85:
	.cfi_offset %rbx, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	$1, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB10_2
# BB#1:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB10_2:
	movq	%rbp, 32(%r14)
	movl	$8, 4(%r14)
	movq	input(%rip), %rdi
	addq	$1584, %rdi             # imm = 0x630
	callq	strlen
	movq	%rax, %rbp
	movq	%rsp, %rdi
	callq	ftime
	testl	%ebp, %ebp
	je	.LBB10_3
# BB#4:
	movq	input(%rip), %rsi
	addq	$1584, %rsi             # imm = 0x630
	movl	%ebp, %edx
	leaq	16(%rsp), %rdi
	callq	strncpy
	leal	17(%rbp), %ebp
	movl	$8, %edi
	movl	$.L.str.71, %esi
	movl	$5, %edx
	movq	%r14, %rcx
	callq	u_v
	cmpl	$255, %ebp
	jb	.LBB10_6
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %edi
	movl	$.L.str.72, %esi
	movl	$255, %edx
	movq	%r14, %rcx
	callq	u_v
	addl	$-255, %ebp
	cmpl	$254, %ebp
	ja	.LBB10_5
	jmp	.LBB10_6
.LBB10_3:                               # %.thread
	movabsq	$7306916068917071136, %rax # imm = 0x6567617373654D20
	movq	%rax, 21(%rsp)
	movabsq	$7299526277054098757, %rax # imm = 0x654D207974706D45
	movq	%rax, 16(%rsp)
	movl	$8, %edi
	movl	$.L.str.71, %esi
	movl	$5, %edx
	movq	%r14, %rcx
	callq	u_v
	movl	$30, %ebp
.LBB10_6:                               # %._crit_edge50
	movl	$8, %edi
	movl	$.L.str.73, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	u_v
	movswl	10(%rsp), %edx
	movl	$32, %edi
	movl	$.L.str.74, %esi
	movq	%r14, %rcx
	callq	u_v
	imull	$1000, (%rsp), %eax     # imm = 0x3E8
	movzwl	8(%rsp), %edx
	addl	%eax, %edx
	movl	$32, %edi
	movl	$.L.str.74, %esi
	movq	%r14, %rcx
	callq	u_v
	movl	$32, %edi
	movl	$.L.str.74, %esi
	movl	$1382116964, %edx       # imm = 0x52616E64
	movq	%r14, %rcx
	callq	u_v
	movl	$32, %edi
	movl	$.L.str.74, %esi
	movl	$1869434195, %edx       # imm = 0x6F6D4D53
	movq	%r14, %rcx
	callq	u_v
	leaq	16(%rsp), %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB10_9
# BB#7:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	16(%rsp,%rbp), %edx
	movl	$8, %edi
	movl	$.L.str.75, %esi
	movq	%r14, %rcx
	callq	u_v
	movl	%ebx, %ebp
	movq	%r15, %rdi
	callq	strlen
	incl	%ebx
	cmpq	%rax, %rbp
	jb	.LBB10_8
.LBB10_9:                               # %._crit_edge
	movl	$8, %edi
	movl	$.L.str.75, %esi
	xorl	%edx, %edx
	movq	%r14, %rcx
	callq	u_v
	movq	%r14, %rdi
	callq	SODBtoRBSP
	movl	(%r14), %ebp
	movq	%r14, %rdi
	callq	free
	movl	%ebp, %eax
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	GenerateSEImessage_rbsp, .Lfunc_end10-GenerateSEImessage_rbsp
	.cfi_endproc

	.type	PicParSet,@object       # @PicParSet
	.comm	PicParSet,2048,16
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.LGenerateSequenceParameterSet.SubWidthC,@object # @GenerateSequenceParameterSet.SubWidthC
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LGenerateSequenceParameterSet.SubWidthC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	.LGenerateSequenceParameterSet.SubWidthC, 16

	.type	.LGenerateSequenceParameterSet.SubHeightC,@object # @GenerateSequenceParameterSet.SubHeightC
	.p2align	4
.LGenerateSequenceParameterSet.SubHeightC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.LGenerateSequenceParameterSet.SubHeightC, 16

	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"automatic frame cropping (width) not possible"
	.size	.L.str, 46

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"automatic frame cropping (height) not possible"
	.size	.L.str.1, 47

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"GeneratePictureParameterSet: slice_group_id"
	.size	.L.str.2, 44

	.type	ZZ_SCAN,@object         # @ZZ_SCAN
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
ZZ_SCAN:
	.ascii	"\000\001\004\b\005\002\003\006\t\f\r\n\007\013\016\017"
	.size	ZZ_SCAN, 16

	.type	ZZ_SCAN8,@object        # @ZZ_SCAN8
	.section	.rodata,"a",@progbits
	.p2align	4
ZZ_SCAN8:
	.ascii	"\000\001\b\020\t\002\003\n\021\030 \031\022\013\004\005\f\023\032!(0)\"\033\024\r\006\007\016\025\034#*1892+$\035\026\017\027\036%,3:;4-&\037'.5<=6/7>?"
	.size	ZZ_SCAN8, 64

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"   : delta_sl   "
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SeqParameterSet:bitstream"
	.size	.L.str.5, 26

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SPS: profile_idc"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SPS: constrained_set0_flag"
	.size	.L.str.7, 27

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SPS: constrained_set1_flag"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SPS: constrained_set2_flag"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SPS: constrained_set3_flag"
	.size	.L.str.10, 27

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SPS: reserved_zero_4bits"
	.size	.L.str.11, 25

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SPS: level_idc"
	.size	.L.str.12, 15

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SPS: seq_parameter_set_id"
	.size	.L.str.13, 26

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SPS: chroma_format_idc"
	.size	.L.str.14, 23

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SPS: residue_transform_flag"
	.size	.L.str.15, 28

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SPS: bit_depth_luma_minus8"
	.size	.L.str.16, 27

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SPS: bit_depth_chroma_minus8"
	.size	.L.str.17, 29

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SPS: lossless_qpprime_y_zero_flag"
	.size	.L.str.18, 34

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SPS: seq_scaling_matrix_present_flag"
	.size	.L.str.19, 37

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SPS: seq_scaling_list_present_flag"
	.size	.L.str.20, 35

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SPS: log2_max_frame_num_minus4"
	.size	.L.str.21, 31

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SPS: pic_order_cnt_type"
	.size	.L.str.22, 24

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SPS: log2_max_pic_order_cnt_lsb_minus4"
	.size	.L.str.23, 39

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SPS: delta_pic_order_always_zero_flag"
	.size	.L.str.24, 38

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SPS: offset_for_non_ref_pic"
	.size	.L.str.25, 28

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SPS: offset_for_top_to_bottom_field"
	.size	.L.str.26, 36

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SPS: num_ref_frames_in_pic_order_cnt_cycle"
	.size	.L.str.27, 43

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SPS: offset_for_ref_frame"
	.size	.L.str.28, 26

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SPS: num_ref_frames"
	.size	.L.str.29, 20

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SPS: gaps_in_frame_num_value_allowed_flag"
	.size	.L.str.30, 42

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SPS: pic_width_in_mbs_minus1"
	.size	.L.str.31, 29

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SPS: pic_height_in_map_units_minus1"
	.size	.L.str.32, 36

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SPS: frame_mbs_only_flag"
	.size	.L.str.33, 25

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"SPS: mb_adaptive_frame_field_flag"
	.size	.L.str.34, 34

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"SPS: direct_8x8_inference_flag"
	.size	.L.str.35, 31

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SPS: frame_cropping_flag"
	.size	.L.str.36, 25

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"SPS: frame_cropping_rect_left_offset"
	.size	.L.str.37, 37

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"SPS: frame_cropping_rect_right_offset"
	.size	.L.str.38, 38

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"SPS: frame_cropping_rect_top_offset"
	.size	.L.str.39, 36

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"SPS: frame_cropping_rect_bottom_offset"
	.size	.L.str.40, 39

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"SPS: vui_parameters_present_flag"
	.size	.L.str.41, 33

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"PicParameterSet:bitstream"
	.size	.L.str.42, 26

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"PPS: pic_parameter_set_id"
	.size	.L.str.43, 26

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"PPS: seq_parameter_set_id"
	.size	.L.str.44, 26

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"PPS: entropy_coding_mode_flag"
	.size	.L.str.45, 30

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"PPS: pic_order_present_flag"
	.size	.L.str.46, 28

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"PPS: num_slice_groups_minus1"
	.size	.L.str.47, 29

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"PPS: slice_group_map_type"
	.size	.L.str.48, 26

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"PPS: run_length_minus1[i]"
	.size	.L.str.49, 26

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"PPS: top_left[i]"
	.size	.L.str.50, 17

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"PPS: bottom_right[i]"
	.size	.L.str.51, 21

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"PPS: slice_group_change_direction_flag"
	.size	.L.str.52, 39

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"PPS: slice_group_change_rate_minus1"
	.size	.L.str.53, 36

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"PPS: pic_size_in_map_units_minus1"
	.size	.L.str.54, 34

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"PPS: >slice_group_id[i]"
	.size	.L.str.55, 24

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"PPS: num_ref_idx_l0_active_minus1"
	.size	.L.str.56, 34

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"PPS: num_ref_idx_l1_active_minus1"
	.size	.L.str.57, 34

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"PPS: weighted_pred_flag"
	.size	.L.str.58, 24

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"PPS: weighted_bipred_idc"
	.size	.L.str.59, 25

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"PPS: pic_init_qp_minus26"
	.size	.L.str.60, 25

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"PPS: pic_init_qs_minus26"
	.size	.L.str.61, 25

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"PPS: chroma_qp_index_offset"
	.size	.L.str.62, 28

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"PPS: deblocking_filter_control_present_flag"
	.size	.L.str.63, 44

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"PPS: constrained_intra_pred_flag"
	.size	.L.str.64, 33

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"PPS: redundant_pic_cnt_present_flag"
	.size	.L.str.65, 36

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"PPS: transform_8x8_mode_flag"
	.size	.L.str.66, 29

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"PPS: pic_scaling_matrix_present_flag"
	.size	.L.str.67, 37

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"PPS: pic_scaling_list_present_flag"
	.size	.L.str.68, 35

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"PPS: second_chroma_qp_index_offset"
	.size	.L.str.69, 35

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"Empty Message"
	.size	.L.str.70, 14

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"SEI: last_payload_type_byte"
	.size	.L.str.71, 28

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"SEI: ff_byte"
	.size	.L.str.72, 13

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"SEI: last_payload_size_byte"
	.size	.L.str.73, 28

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"SEI: uuid_iso_iec_11578"
	.size	.L.str.74, 24

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"SEI: user_data_payload_byte"
	.size	.L.str.75, 28

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	seiHasBufferingPeriod_info,@object # @seiHasBufferingPeriod_info
	.comm	seiHasBufferingPeriod_info,4,4
	.type	seiBufferingPeriod,@object # @seiBufferingPeriod
	.comm	seiBufferingPeriod,280,8
	.type	seiHasPicTiming_info,@object # @seiHasPicTiming_info
	.comm	seiHasPicTiming_info,4,4
	.type	seiPicTiming,@object    # @seiPicTiming
	.comm	seiPicTiming,152,8
	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"VUI: aspect_ratio_info_present_flag"
	.size	.L.str.77, 36

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"VUI: overscan_info_present_flag"
	.size	.L.str.78, 32

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"VUI: video_signal_type_present_flag"
	.size	.L.str.79, 36

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"VUI: video format"
	.size	.L.str.80, 18

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"VUI: video_full_range_flag"
	.size	.L.str.81, 27

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"VUI: color_description_present_flag"
	.size	.L.str.82, 36

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"VUI: colour primaries"
	.size	.L.str.83, 22

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"VUI: transfer characteristics"
	.size	.L.str.84, 30

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"VUI: matrix coefficients"
	.size	.L.str.85, 25

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"VUI: chroma_loc_info_present_flag"
	.size	.L.str.86, 34

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"VUI: timing_info_present_flag"
	.size	.L.str.87, 30

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"VUI: nal_hrd_parameters_present_flag"
	.size	.L.str.88, 37

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"VUI: vcl_hrd_parameters_present_flag"
	.size	.L.str.89, 37

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"VUI: pic_struc_present_flag"
	.size	.L.str.90, 28

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"VUI: bitstream_restriction_flag"
	.size	.L.str.91, 32

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Parset.c: slice_group_map_type invalid, default"
	.size	.Lstr, 48

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Sequence Parameter VUI not yet implemented, this should never happen, exit"
	.size	.Lstr.1, 75

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"VUI: writing Sequence Parameter VUI to signal RGB format"
	.size	.Lstr.2, 57


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
