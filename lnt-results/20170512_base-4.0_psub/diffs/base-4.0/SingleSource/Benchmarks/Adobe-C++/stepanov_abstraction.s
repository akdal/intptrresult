	.text
	.file	"stepanov_abstraction.bc"
	.globl	_Z13record_resultdPKc
	.p2align	4, 0x90
	.type	_Z13record_resultdPKc,@function
_Z13record_resultdPKc:                  # @_Z13record_resultdPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	results(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movl	current_test(%rip), %ecx
	movl	allocated_results(%rip), %edx
	cmpl	%edx, %ecx
	jge	.LBB0_3
	jmp	.LBB0_5
.LBB0_1:                                # %._crit_edge
	movl	allocated_results(%rip), %edx
.LBB0_3:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	addl	$10, %ecx
	movl	%ecx, allocated_results(%rip)
	addq	$160, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, results(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#4:                                 # %._crit_edge2
	movl	current_test(%rip), %ecx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_5:
	movslq	%ecx, %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movsd	%xmm0, (%rax,%rdx)
	movq	%rbx, 8(%rax,%rdx)
	incl	%ecx
	movl	%ecx, current_test(%rip)
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB0_6:
	movl	allocated_results(%rip), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	_Z13record_resultdPKc, .Lfunc_end0-_Z13record_resultdPKc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z9summarizePKciiii
	.p2align	4, 0x90
	.type	_Z9summarizePKciiii,@function
_Z9summarizePKciiii:                    # @_Z9summarizePKciiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 80
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	current_test(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph63
	movq	results(%rip), %r14
	addq	$8, %r14
	movl	$12, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	callq	strlen
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	incq	%r15
	addq	$16, %r14
	cmpq	%rbx, %r15
	jl	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movl	$12, %ebp
.LBB1_4:                                # %._crit_edge64
	leal	-12(%rbp), %esi
	movl	$.L.str.1, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.3, %edi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB1_5
# BB#20:                                # %.lr.ph59
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	.LCPI1_0(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movl	%ebp, %r12d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r13
	movq	%r13, %rdi
	callq	strlen
	movl	%r12d, %edx
	subl	%eax, %edx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	divsd	(%r14), %xmm2
	movl	$.L.str.4, %edi
	movl	$.L.str.5, %ecx
	movb	$3, %al
	movq	%r13, %r8
	movl	%ebx, %esi
	callq	printf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_21
# BB#6:                                 # %.preheader48
	testl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB1_14
# BB#7:                                 # %.lr.ph54
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_8
# BB#9:                                 # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB1_10
	jmp	.LBB1_11
.LBB1_5:
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_14
.LBB1_8:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB1_11:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB1_14
# BB#12:                                # %.lr.ph54.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB1_13
.LBB1_14:                               # %._crit_edge55
	movl	$.L.str.6, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_19
# BB#15:                                # %._crit_edge55
	cmpl	$2, current_test(%rip)
	jl	.LBB1_19
# BB#16:                                # %.lr.ph.preheader
	xorpd	%xmm1, %xmm1
	movl	$1, %ebx
	movl	$16, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	results(%rip), %rax
	movsd	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rax), %xmm0
	callq	log
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB1_17
# BB#18:                                # %._crit_edge
	decl	%eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp
	movl	$.L.str.7, %edi
	movb	$1, %al
	movq	%r14, %rsi
	callq	printf
.LBB1_19:
	movl	$0, current_test(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z9summarizePKciiii, .Lfunc_end1-_Z9summarizePKciiii
	.cfi_endproc

	.globl	_Z17summarize_simplefP8_IO_FILEPKc
	.p2align	4, 0x90
	.type	_Z17summarize_simplefP8_IO_FILEPKc,@function
_Z17summarize_simplefP8_IO_FILEPKc:     # @_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	current_test(%rip), %r12
	testq	%r12, %r12
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph42
	movq	results(%rip), %rbp
	addq	$8, %rbp
	movl	$12, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	strlen
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	incq	%rbx
	addq	$16, %rbp
	cmpq	%r12, %rbx
	jl	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	movl	$12, %r13d
.LBB2_4:                                # %._crit_edge43
	leal	-12(%r13), %edx
	movl	$.L.str.8, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.9, %esi
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r13d, %edx
	callq	fprintf
	xorpd	%xmm0, %xmm0
	cmpl	$0, current_test(%rip)
	jle	.LBB2_15
# BB#5:                                 # %.lr.ph38
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	results(%rip), %r14
	movq	(%r14,%rbp), %r12
	movq	%r12, %rdi
	callq	strlen
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movsd	-8(%r14,%rbp), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.10, %esi
	movl	$.L.str.5, %r8d
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r12, %r9
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	movslq	current_test(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB2_6
# BB#7:                                 # %.preheader
	testl	%eax, %eax
	movq	(%rsp), %r14            # 8-byte Reload
	xorpd	%xmm0, %xmm0
	jle	.LBB2_15
# BB#8:                                 # %.lr.ph
	movq	results(%rip), %rbp
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB2_9
# BB#10:                                # %.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	addsd	(%rdx), %xmm0
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB2_11
	jmp	.LBB2_12
.LBB2_9:
	xorl	%ecx, %ecx
	xorpd	%xmm0, %xmm0
.LBB2_12:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB2_15
# BB#13:                                # %.lr.ph.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	addsd	-48(%rdx), %xmm0
	addsd	-32(%rdx), %xmm0
	addsd	-16(%rdx), %xmm0
	addsd	(%rdx), %xmm0
	addq	$4, %rcx
	addq	$64, %rdx
	cmpq	%rax, %rcx
	jl	.LBB2_14
.LBB2_15:                               # %._crit_edge
	movl	$.L.str.6, %esi
	movb	$1, %al
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fprintf
	movl	$0, current_test(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z17summarize_simplefP8_IO_FILEPKc, .Lfunc_end2-_Z17summarize_simplefP8_IO_FILEPKc
	.cfi_endproc

	.globl	_Z11start_timerv
	.p2align	4, 0x90
	.type	_Z11start_timerv,@function
_Z11start_timerv:                       # @_Z11start_timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, start_time(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	_Z11start_timerv, .Lfunc_end3-_Z11start_timerv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	_Z5timerv
	.p2align	4, 0x90
	.type	_Z5timerv,@function
_Z5timerv:                              # @_Z5timerv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI4_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end4:
	.size	_Z5timerv, .Lfunc_end4-_Z5timerv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4656510908468559872     # double 2000
.LCPI5_1:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$2, %ebp
	jl	.LBB5_3
# BB#1:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, iterations(%rip)
	cmpl	$2, %ebp
	je	.LBB5_3
# BB#2:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	callq	strtod
	movsd	%xmm0, init_value(%rip)
.LBB5_3:                                # %.thread
	movabsq	$4611686018427387900, %r14 # imm = 0x3FFFFFFFFFFFFFFC
	cvttsd2si	init_value(%rip), %edi
	addl	$123, %edi
	callq	srand
	movq	dpb(%rip), %rcx
	movq	dpe(%rip), %rdx
	movq	init_value(%rip), %rax
	cmpq	%rdx, %rcx
	je	.LBB5_17
# BB#4:                                 # %.lr.ph.i.preheader
	leaq	-8(%rdx), %rsi
	subq	%rcx, %rsi
	shrq	$3, %rsi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB5_15
# BB#5:                                 # %min.iters.checked
	movq	%rsi, %rbx
	andq	%r14, %rbx
	je	.LBB5_15
# BB#6:                                 # %vector.ph
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rbx), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB5_9
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rcx,%rbp,8)
	movdqu	%xmm0, 16(%rcx,%rbp,8)
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB5_8
	jmp	.LBB5_10
.LBB5_9:
	xorl	%ebp, %ebp
.LBB5_10:                               # %vector.body.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB5_13
# BB#11:                                # %vector.ph.new
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	leaq	240(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB5_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rbp)
	movdqu	%xmm0, -224(%rbp)
	movdqu	%xmm0, -208(%rbp)
	movdqu	%xmm0, -192(%rbp)
	movdqu	%xmm0, -176(%rbp)
	movdqu	%xmm0, -160(%rbp)
	movdqu	%xmm0, -144(%rbp)
	movdqu	%xmm0, -128(%rbp)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-32, %rdi
	jne	.LBB5_12
.LBB5_13:                               # %middle.block
	cmpq	%rbx, %rsi
	je	.LBB5_16
# BB#14:
	leaq	(%rcx,%rbx,8), %rcx
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB5_15
.LBB5_16:                               # %_ZN9benchmark4fillIPddEEvT_S2_T0_.exit.loopexit
	movq	init_value(%rip), %rax
.LBB5_17:                               # %_ZN9benchmark4fillIPddEEvT_S2_T0_.exit
	movq	DVpb(%rip), %rcx
	movq	DVpe(%rip), %rdx
	cmpq	%rdx, %rcx
	je	.LBB5_31
# BB#18:                                # %.lr.ph.i69.preheader
	leaq	-8(%rdx), %rsi
	subq	%rcx, %rsi
	shrq	$3, %rsi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB5_29
# BB#19:                                # %min.iters.checked243
	movq	%rsi, %rbx
	andq	%r14, %rbx
	je	.LBB5_29
# BB#20:                                # %vector.ph247
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rbx), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB5_23
# BB#21:                                # %vector.body235.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_22:                               # %vector.body235.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rcx,%rbp,8)
	movdqu	%xmm0, 16(%rcx,%rbp,8)
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB5_22
	jmp	.LBB5_24
.LBB5_23:
	xorl	%ebp, %ebp
.LBB5_24:                               # %vector.body235.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB5_27
# BB#25:                                # %vector.ph247.new
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	leaq	240(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB5_26:                               # %vector.body235
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rbp)
	movdqu	%xmm0, -224(%rbp)
	movdqu	%xmm0, -208(%rbp)
	movdqu	%xmm0, -192(%rbp)
	movdqu	%xmm0, -176(%rbp)
	movdqu	%xmm0, -160(%rbp)
	movdqu	%xmm0, -144(%rbp)
	movdqu	%xmm0, -128(%rbp)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-32, %rdi
	jne	.LBB5_26
.LBB5_27:                               # %middle.block236
	cmpq	%rbx, %rsi
	je	.LBB5_30
# BB#28:
	leaq	(%rcx,%rbx,8), %rcx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph.i69
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB5_29
.LBB5_30:                               # %_ZN9benchmark4fillIP12ValueWrapperIdES2_EEvT_S4_T0_.exit.loopexit
	movq	init_value(%rip), %rax
.LBB5_31:                               # %_ZN9benchmark4fillIP12ValueWrapperIdES2_EEvT_S4_T0_.exit
	movq	DV10pb(%rip), %rcx
	movq	DV10pe(%rip), %rdx
	cmpq	%rdx, %rcx
	je	.LBB5_44
# BB#32:                                # %.lr.ph.i72.preheader
	leaq	-8(%rdx), %rsi
	subq	%rcx, %rsi
	shrq	$3, %rsi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB5_43
# BB#33:                                # %min.iters.checked265
	movq	%rsi, %rbx
	andq	%r14, %rbx
	je	.LBB5_43
# BB#34:                                # %vector.ph269
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rbx), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB5_37
# BB#35:                                # %vector.body257.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_36:                               # %vector.body257.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rcx,%rbp,8)
	movdqu	%xmm0, 16(%rcx,%rbp,8)
	addq	$4, %rbp
	incq	%rdi
	jne	.LBB5_36
	jmp	.LBB5_38
.LBB5_37:
	xorl	%ebp, %ebp
.LBB5_38:                               # %vector.body257.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB5_41
# BB#39:                                # %vector.ph269.new
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	leaq	240(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB5_40:                               # %vector.body257
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rbp)
	movdqu	%xmm0, -224(%rbp)
	movdqu	%xmm0, -208(%rbp)
	movdqu	%xmm0, -192(%rbp)
	movdqu	%xmm0, -176(%rbp)
	movdqu	%xmm0, -160(%rbp)
	movdqu	%xmm0, -144(%rbp)
	movdqu	%xmm0, -128(%rbp)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-32, %rdi
	jne	.LBB5_40
.LBB5_41:                               # %middle.block258
	cmpq	%rbx, %rsi
	je	.LBB5_44
# BB#42:
	leaq	(%rcx,%rbx,8), %rcx
	.p2align	4, 0x90
.LBB5_43:                               # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB5_43
.LBB5_44:                               # %_ZN9benchmark4fillIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_T0_.exit
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_140
# BB#45:                                # %.lr.ph.i73
	movq	dpb(%rip), %r15
	movq	dpe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_56
# BB#46:                                # %.lr.ph.split.i.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andl	$7, %ebx
	movq	%rbx, %r13
	negq	%r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_47:                               # %.lr.ph.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_49 Depth 2
                                        #     Child Loop BB5_52 Depth 2
	pxor	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB5_50
# BB#48:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_49:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_49
	jmp	.LBB5_51
	.p2align	4, 0x90
.LBB5_50:                               #   in Loop: Header=BB5_47 Depth=1
	movq	%r15, %rcx
.LBB5_51:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB5_47 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_53
	.p2align	4, 0x90
.LBB5_52:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_52
.LBB5_53:                               # %_ZN9benchmark10accumulateIPddEET0_T_S3_S2_.exit.i
                                        #   in Loop: Header=BB5_47 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_54
	jnp	.LBB5_55
.LBB5_54:                               #   in Loop: Header=BB5_47 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_55:                               # %_Z9check_sumd.exit.i
                                        #   in Loop: Header=BB5_47 Depth=1
	incl	%r14d
	cmpl	%eax, %r14d
	jl	.LBB5_47
	jmp	.LBB5_60
.LBB5_56:                               # %.lr.ph.split.us.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_57:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_58
	jnp	.LBB5_59
.LBB5_58:                               #   in Loop: Header=BB5_57 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_59:                               # %_Z9check_sumd.exit.us.i
                                        #   in Loop: Header=BB5_57 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_57
.LBB5_60:                               # %_Z15test_accumulateIPddEvT_S1_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_140
# BB#61:                                # %.lr.ph.i74
	movq	dPb(%rip), %r15
	movq	dPe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_72
# BB#62:                                # %.lr.ph.split.i77.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	andl	$7, %r14d
	movq	%r14, %r13
	negq	%r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_63:                               # %.lr.ph.split.i77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_65 Depth 2
                                        #     Child Loop BB5_68 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%r14, %r14
	je	.LBB5_66
# BB#64:                                # %.lr.ph.i.i78.prol.preheader
                                        #   in Loop: Header=BB5_63 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_65:                               # %.lr.ph.i.i78.prol
                                        #   Parent Loop BB5_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_65
	jmp	.LBB5_67
	.p2align	4, 0x90
.LBB5_66:                               #   in Loop: Header=BB5_63 Depth=1
	movq	%r15, %rcx
.LBB5_67:                               # %.lr.ph.i.i78.prol.loopexit
                                        #   in Loop: Header=BB5_63 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_69
	.p2align	4, 0x90
.LBB5_68:                               # %.lr.ph.i.i78
                                        #   Parent Loop BB5_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_68
.LBB5_69:                               # %_ZN9benchmark10accumulateI14PointerWrapperIdEdEET0_T_S4_S3_.exit.i
                                        #   in Loop: Header=BB5_63 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_70
	jnp	.LBB5_71
.LBB5_70:                               #   in Loop: Header=BB5_63 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_71:                               # %_Z9check_sumd.exit.i80
                                        #   in Loop: Header=BB5_63 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_63
	jmp	.LBB5_76
.LBB5_72:                               # %.lr.ph.split.us.i75.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_73:                               # %.lr.ph.split.us.i75
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_74
	jnp	.LBB5_75
.LBB5_74:                               #   in Loop: Header=BB5_73 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_75:                               # %_Z9check_sumd.exit.us.i76
                                        #   in Loop: Header=BB5_73 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_73
.LBB5_76:                               # %_Z15test_accumulateI14PointerWrapperIdEdEvT_S2_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_140
# BB#77:                                # %.lr.ph.i81
	movq	DVpb(%rip), %r15
	movq	DVpe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_88
# BB#78:                                # %.lr.ph.split.i87.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	andl	$7, %r14d
	movq	%r14, %r13
	negq	%r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_79:                               # %.lr.ph.split.i87
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_81 Depth 2
                                        #     Child Loop BB5_84 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%r14, %r14
	je	.LBB5_82
# BB#80:                                # %.lr.ph.i.i89.prol.preheader
                                        #   in Loop: Header=BB5_79 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_81:                               # %.lr.ph.i.i89.prol
                                        #   Parent Loop BB5_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_81
	jmp	.LBB5_83
	.p2align	4, 0x90
.LBB5_82:                               #   in Loop: Header=BB5_79 Depth=1
	movq	%r15, %rcx
.LBB5_83:                               # %.lr.ph.i.i89.prol.loopexit
                                        #   in Loop: Header=BB5_79 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_85
	.p2align	4, 0x90
.LBB5_84:                               # %.lr.ph.i.i89
                                        #   Parent Loop BB5_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_84
.LBB5_85:                               # %_ZN9benchmark10accumulateIP12ValueWrapperIdES2_EET0_T_S5_S4_.exit.i
                                        #   in Loop: Header=BB5_79 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_86
	jnp	.LBB5_87
.LBB5_86:                               #   in Loop: Header=BB5_79 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_87:                               # %_Z9check_sumd.exit.i91
                                        #   in Loop: Header=BB5_79 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_79
	jmp	.LBB5_92
.LBB5_88:                               # %.lr.ph.split.us.i83.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_89:                               # %.lr.ph.split.us.i83
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_90
	jnp	.LBB5_91
.LBB5_90:                               #   in Loop: Header=BB5_89 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_91:                               # %_Z9check_sumd.exit.us.i85
                                        #   in Loop: Header=BB5_89 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_89
.LBB5_92:                               # %_Z15test_accumulateIP12ValueWrapperIdES1_EvT_S3_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_140
# BB#93:                                # %.lr.ph.i92
	movq	DVPb(%rip), %r15
	movq	DVPe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_104
# BB#94:                                # %.lr.ph.split.i95.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	andl	$7, %r14d
	movq	%r14, %r13
	negq	%r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_95:                               # %.lr.ph.split.i95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_97 Depth 2
                                        #     Child Loop BB5_100 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%r14, %r14
	je	.LBB5_98
# BB#96:                                # %.lr.ph.i.i96.prol.preheader
                                        #   in Loop: Header=BB5_95 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_97:                               # %.lr.ph.i.i96.prol
                                        #   Parent Loop BB5_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_97
	jmp	.LBB5_99
	.p2align	4, 0x90
.LBB5_98:                               #   in Loop: Header=BB5_95 Depth=1
	movq	%r15, %rcx
.LBB5_99:                               # %.lr.ph.i.i96.prol.loopexit
                                        #   in Loop: Header=BB5_95 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_101
	.p2align	4, 0x90
.LBB5_100:                              # %.lr.ph.i.i96
                                        #   Parent Loop BB5_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_100
.LBB5_101:                              # %_ZN9benchmark10accumulateI14PointerWrapperI12ValueWrapperIdEES3_EET0_T_S6_S5_.exit.i
                                        #   in Loop: Header=BB5_95 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_102
	jnp	.LBB5_103
.LBB5_102:                              #   in Loop: Header=BB5_95 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_103:                              # %_Z9check_sumd.exit.i98
                                        #   in Loop: Header=BB5_95 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_95
	jmp	.LBB5_108
.LBB5_104:                              # %.lr.ph.split.us.i93.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_105:                              # %.lr.ph.split.us.i93
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_106
	jnp	.LBB5_107
.LBB5_106:                              #   in Loop: Header=BB5_105 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_107:                              # %_Z9check_sumd.exit.us.i94
                                        #   in Loop: Header=BB5_105 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_105
.LBB5_108:                              # %_Z15test_accumulateI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_140
# BB#109:                               # %.lr.ph.i99
	movq	DV10pb(%rip), %r15
	movq	DV10pe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_120
# BB#110:                               # %.lr.ph.split.i105.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	andl	$7, %r14d
	movq	%r14, %r13
	negq	%r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_111:                              # %.lr.ph.split.i105
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_113 Depth 2
                                        #     Child Loop BB5_116 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%r14, %r14
	je	.LBB5_114
# BB#112:                               # %.lr.ph.i.i108.prol.preheader
                                        #   in Loop: Header=BB5_111 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_113:                              # %.lr.ph.i.i108.prol
                                        #   Parent Loop BB5_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_113
	jmp	.LBB5_115
	.p2align	4, 0x90
.LBB5_114:                              #   in Loop: Header=BB5_111 Depth=1
	movq	%r15, %rcx
.LBB5_115:                              # %.lr.ph.i.i108.prol.loopexit
                                        #   in Loop: Header=BB5_111 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_117
	.p2align	4, 0x90
.LBB5_116:                              # %.lr.ph.i.i108
                                        #   Parent Loop BB5_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_116
.LBB5_117:                              # %_ZN9benchmark10accumulateIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EET0_T_SE_SD_.exit.i
                                        #   in Loop: Header=BB5_111 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_118
	jnp	.LBB5_119
.LBB5_118:                              #   in Loop: Header=BB5_111 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_119:                              # %_Z9check_sumd.exit.i110
                                        #   in Loop: Header=BB5_111 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_111
	jmp	.LBB5_124
.LBB5_120:                              # %.lr.ph.split.us.i101.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_121:                              # %.lr.ph.split.us.i101
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_122
	jnp	.LBB5_123
.LBB5_122:                              #   in Loop: Header=BB5_121 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_123:                              # %_Z9check_sumd.exit.us.i103
                                        #   in Loop: Header=BB5_121 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_121
.LBB5_124:                              # %_Z15test_accumulateIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_T0_PKc.exit
	testl	%eax, %eax
	jle	.LBB5_140
# BB#125:                               # %.lr.ph.i111
	movq	DV10Pb(%rip), %r15
	movq	DV10Pe(%rip), %rbp
	cmpq	%rbp, %r15
	je	.LBB5_136
# BB#126:                               # %.lr.ph.split.i117.preheader
	leaq	-8(%rbp), %r12
	subq	%r15, %r12
	movl	%r12d, %r14d
	shrl	$3, %r14d
	incl	%r14d
	andl	$7, %r14d
	movq	%r14, %r13
	negq	%r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_127:                              # %.lr.ph.split.i117
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_129 Depth 2
                                        #     Child Loop BB5_132 Depth 2
	xorpd	%xmm0, %xmm0
	testq	%r14, %r14
	je	.LBB5_130
# BB#128:                               # %.lr.ph.i.i120.prol.preheader
                                        #   in Loop: Header=BB5_127 Depth=1
	movq	%r13, %rdx
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_129:                              # %.lr.ph.i.i120.prol
                                        #   Parent Loop BB5_127 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_129
	jmp	.LBB5_131
	.p2align	4, 0x90
.LBB5_130:                              #   in Loop: Header=BB5_127 Depth=1
	movq	%r15, %rcx
.LBB5_131:                              # %.lr.ph.i.i120.prol.loopexit
                                        #   in Loop: Header=BB5_127 Depth=1
	cmpq	$56, %r12
	jb	.LBB5_133
	.p2align	4, 0x90
.LBB5_132:                              # %.lr.ph.i.i120
                                        #   Parent Loop BB5_127 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB5_132
.LBB5_133:                              # %_ZN9benchmark10accumulateI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EET0_T_SF_SE_.exit.i
                                        #   in Loop: Header=BB5_127 Depth=1
	movsd	init_value(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_134
	jnp	.LBB5_135
.LBB5_134:                              #   in Loop: Header=BB5_127 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_135:                              # %_Z9check_sumd.exit.i122
                                        #   in Loop: Header=BB5_127 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_127
	jmp	.LBB5_140
.LBB5_136:                              # %.lr.ph.split.us.i113.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_137:                              # %.lr.ph.split.us.i113
                                        # =>This Inner Loop Header: Depth=1
	movsd	init_value(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	ucomisd	.LCPI5_1, %xmm0
	jne	.LBB5_138
	jnp	.LBB5_139
.LBB5_138:                              #   in Loop: Header=BB5_137 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB5_139:                              # %_Z9check_sumd.exit.us.i115
                                        #   in Loop: Header=BB5_137 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_137
.LBB5_140:                              # %_Z15test_accumulateI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_T0_PKc.exit
	cltq
	imulq	$274877907, %rax, %rax  # imm = 0x10624DD3
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$39, %rax
	addl	%ecx, %eax
	movl	%eax, iterations(%rip)
	movq	dMpb(%rip), %rbp
	movq	dMpe(%rip), %rbx
	cmpq	%rbx, %rbp
	je	.LBB5_145
	.p2align	4, 0x90
.LBB5_141:                              # %.lr.ph.i124
                                        # =>This Inner Loop Header: Depth=1
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rbp)
	addq	$8, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB5_141
# BB#142:                               # %_ZN9benchmark11fill_randomIPddEEvT_S2_.exit
	movq	dMpb(%rip), %rbp
	movq	dMpe(%rip), %rsi
	cmpq	%rsi, %rbp
	movabsq	$4611686018427387900, %r14 # imm = 0x3FFFFFFFFFFFFFFC
	je	.LBB5_187
# BB#143:                               # %.lr.ph.i125.preheader
	movq	DVMpb(%rip), %rdi
	leaq	-8(%rsi), %r8
	movq	%r8, %r10
	subq	%rbp, %r10
	movq	%r10, %r11
	shrq	$3, %r11
	incq	%r11
	cmpq	$4, %r11
	jae	.LBB5_146
# BB#144:
	movq	%rbp, %rbx
	jmp	.LBB5_160
.LBB5_145:
	movq	%rbp, %rsi
	jmp	.LBB5_187
.LBB5_146:                              # %min.iters.checked288
	movq	%r11, %r9
	andq	%r14, %r9
	je	.LBB5_150
# BB#147:                               # %vector.memcheck
	movq	%r10, %rax
	andq	$-8, %rax
	leaq	8(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_151
# BB#148:                               # %vector.memcheck
	leaq	8(%rdi,%rax), %rax
	cmpq	%rax, %rbp
	jae	.LBB5_151
.LBB5_150:
	movq	%rbp, %rbx
.LBB5_160:                              # %.lr.ph.i125.preheader356
	movq	%r8, %rax
	subq	%rbx, %rax
	movl	%eax, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB5_163
# BB#161:                               # %.lr.ph.i125.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB5_162:                              # %.lr.ph.i125.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	addq	$8, %rbx
	movq	%rcx, (%rdi)
	addq	$8, %rdi
	incq	%rdx
	jne	.LBB5_162
.LBB5_163:                              # %.lr.ph.i125.prol.loopexit
	cmpq	$56, %rax
	jb	.LBB5_165
	.p2align	4, 0x90
.LBB5_164:                              # %.lr.ph.i125
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rax, (%rdi)
	movq	8(%rbx), %rax
	movq	%rax, 8(%rdi)
	movq	16(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rax
	movq	%rax, 24(%rdi)
	movq	32(%rbx), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rbx), %rax
	movq	%rax, 40(%rdi)
	movq	48(%rbx), %rax
	movq	%rax, 48(%rdi)
	movq	56(%rbx), %rax
	movq	%rax, 56(%rdi)
	addq	$64, %rbx
	addq	$64, %rdi
	cmpq	%rsi, %rbx
	jne	.LBB5_164
.LBB5_165:                              # %_ZN9benchmark4copyIPdP12ValueWrapperIdEEEvT_S5_T0_.exit
	cmpq	%rsi, %rbp
	je	.LBB5_187
# BB#166:                               # %.lr.ph.i128.preheader
	movq	DV10Mpb(%rip), %rdi
	cmpq	$4, %r11
	jae	.LBB5_168
# BB#167:
	movq	%rbp, %rcx
	jmp	.LBB5_182
.LBB5_168:                              # %min.iters.checked323
	andq	%r11, %r14
	je	.LBB5_172
# BB#169:                               # %vector.memcheck340
	andq	$-8, %r10
	leaq	8(%rbp,%r10), %rax
	cmpq	%rax, %rdi
	jae	.LBB5_173
# BB#170:                               # %vector.memcheck340
	leaq	8(%rdi,%r10), %rax
	cmpq	%rax, %rbp
	jae	.LBB5_173
.LBB5_172:
	movq	%rbp, %rcx
.LBB5_182:                              # %.lr.ph.i128.preheader355
	subq	%rcx, %r8
	movl	%r8d, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$7, %rax
	je	.LBB5_185
# BB#183:                               # %.lr.ph.i128.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB5_184:                              # %.lr.ph.i128.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	addq	$8, %rcx
	movq	%rdx, (%rdi)
	addq	$8, %rdi
	incq	%rax
	jne	.LBB5_184
.LBB5_185:                              # %.lr.ph.i128.prol.loopexit
	cmpq	$56, %r8
	jb	.LBB5_187
	.p2align	4, 0x90
.LBB5_186:                              # %.lr.ph.i128
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
	movq	8(%rcx), %rax
	movq	%rax, 8(%rdi)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rcx), %rax
	movq	%rax, 24(%rdi)
	movq	32(%rcx), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rcx), %rax
	movq	%rax, 40(%rdi)
	movq	48(%rcx), %rax
	movq	%rax, 48(%rdi)
	movq	56(%rcx), %rax
	movq	%rax, 56(%rdi)
	addq	$64, %rcx
	addq	$64, %rdi
	cmpq	%rsi, %rcx
	jne	.LBB5_186
.LBB5_187:                              # %_ZN9benchmark4copyIPdP12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEvT_SE_T0_.exit
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.32, %r8d
	movq	%rbp, %rdi
	callq	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	movq	dMPb(%rip), %rdi
	movq	dMPe(%rip), %rsi
	movq	dPb(%rip), %rdx
	movq	dPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.33, %r8d
	callq	_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	movq	DVMpb(%rip), %rdi
	movq	DVMpe(%rip), %rsi
	movq	DVpb(%rip), %rdx
	movq	DVpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.34, %r8d
	callq	_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	movq	DVMPb(%rip), %rdi
	movq	DVMPe(%rip), %rsi
	movq	DVPb(%rip), %rdx
	movq	DVPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.35, %r8d
	callq	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	movq	DV10Mpb(%rip), %rdi
	movq	DV10Mpe(%rip), %rsi
	movq	DV10pb(%rip), %rdx
	movq	DV10pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.36, %r8d
	callq	_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	movq	DV10MPb(%rip), %rdi
	movq	DV10MPe(%rip), %rsi
	movq	DV10Pb(%rip), %rdx
	movq	DV10Pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.37, %r8d
	callq	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	shll	$3, iterations(%rip)
	movq	dMpb(%rip), %rdi
	movq	dMpe(%rip), %rsi
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.38, %r8d
	callq	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	movq	dMPb(%rip), %rdi
	movq	dMPe(%rip), %rsi
	movq	dPb(%rip), %rdx
	movq	dPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.39, %r8d
	callq	_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	movq	DVMpb(%rip), %rdi
	movq	DVMpe(%rip), %rsi
	movq	DVpb(%rip), %rdx
	movq	DVpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.40, %r8d
	callq	_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	movq	DVMPb(%rip), %rdi
	movq	DVMPe(%rip), %rsi
	movq	DVPb(%rip), %rdx
	movq	DVPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.41, %r8d
	callq	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	movq	DV10Mpb(%rip), %rdi
	movq	DV10Mpe(%rip), %rsi
	movq	DV10pb(%rip), %rdx
	movq	DV10pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.42, %r8d
	callq	_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	movq	DV10MPb(%rip), %rdi
	movq	DV10MPe(%rip), %rsi
	movq	DV10Pb(%rip), %rdx
	movq	DV10Pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.43, %r8d
	callq	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	movq	dMpb(%rip), %rdi
	movq	dMpe(%rip), %rsi
	movq	dpb(%rip), %rdx
	movq	dpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.44, %r8d
	callq	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	movq	dMPb(%rip), %rdi
	movq	dMPe(%rip), %rsi
	movq	dPb(%rip), %rdx
	movq	dPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.45, %r8d
	callq	_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	movq	DVMpb(%rip), %rdi
	movq	DVMpe(%rip), %rsi
	movq	DVpb(%rip), %rdx
	movq	DVpe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.46, %r8d
	callq	_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	movq	DVMPb(%rip), %rdi
	movq	DVMPe(%rip), %rsi
	movq	DVPb(%rip), %rdx
	movq	DVPe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.47, %r8d
	callq	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	movq	DV10Mpb(%rip), %rdi
	movq	DV10Mpe(%rip), %rsi
	movq	DV10pb(%rip), %rdx
	movq	DV10pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.48, %r8d
	callq	_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	movq	DV10MPb(%rip), %rdi
	movq	DV10MPe(%rip), %rsi
	movq	DV10Pb(%rip), %rdx
	movq	DV10Pe(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movl	$.L.str.49, %r8d
	callq	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_151:                              # %vector.body279.preheader
	leaq	-4(%r9), %rbx
	movl	%ebx, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB5_154
# BB#152:                               # %vector.body279.prol.preheader
	negq	%rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_153:                              # %vector.body279.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp,%rdx,8), %xmm0
	movupd	16(%rbp,%rdx,8), %xmm1
	movupd	%xmm0, (%rdi,%rdx,8)
	movupd	%xmm1, 16(%rdi,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB5_153
	jmp	.LBB5_155
.LBB5_173:                              # %vector.body315.preheader
	leaq	-4(%r14), %rax
	movl	%eax, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_176
# BB#174:                               # %vector.body315.prol.preheader
	negq	%rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_175:                              # %vector.body315.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp,%rbx,8), %xmm0
	movupd	16(%rbp,%rbx,8), %xmm1
	movupd	%xmm0, (%rdi,%rbx,8)
	movupd	%xmm1, 16(%rdi,%rbx,8)
	addq	$4, %rbx
	incq	%rdx
	jne	.LBB5_175
	jmp	.LBB5_177
.LBB5_154:
	xorl	%edx, %edx
.LBB5_155:                              # %vector.body279.prol.loopexit
	cmpq	$12, %rbx
	jb	.LBB5_158
# BB#156:                               # %vector.body279.preheader.new
	movq	%r9, %rax
	subq	%rdx, %rax
	leaq	112(%rbp,%rdx,8), %rbx
	leaq	112(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB5_157:                              # %vector.body279
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rbx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB5_157
.LBB5_158:                              # %middle.block280
	cmpq	%r9, %r11
	je	.LBB5_165
# BB#159:
	leaq	(%rdi,%r9,8), %rdi
	leaq	(%rbp,%r9,8), %rbx
	jmp	.LBB5_160
.LBB5_176:
	xorl	%ebx, %ebx
.LBB5_177:                              # %vector.body315.prol.loopexit
	cmpq	$12, %rax
	jb	.LBB5_180
# BB#178:                               # %vector.body315.preheader.new
	movq	%r14, %rax
	subq	%rbx, %rax
	leaq	112(%rbp,%rbx,8), %rdx
	leaq	112(%rdi,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB5_179:                              # %vector.body315
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movupd	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	subq	$-128, %rdx
	subq	$-128, %rbx
	addq	$-16, %rax
	jne	.LBB5_179
.LBB5_180:                              # %middle.block316
	cmpq	%r14, %r11
	je	.LBB5_187
# BB#181:
	leaq	(%rdi,%r14,8), %rdi
	leaq	(%rbp,%r14,8), %rcx
	jmp	.LBB5_182
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.section	.text._Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc: # @_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 176
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB6_64
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB6_2
# BB#25:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%rbx,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
                                        #     Child Loop BB6_35 Depth 2
                                        #     Child Loop BB6_39 Depth 2
                                        #     Child Loop BB6_41 Depth 2
                                        #     Child Loop BB6_45 Depth 2
                                        #     Child Loop BB6_51 Depth 2
                                        #       Child Loop BB6_53 Depth 3
                                        #       Child Loop BB6_57 Depth 3
                                        #     Child Loop BB6_60 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jb	.LBB6_37
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	je	.LBB6_37
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jne	.LBB6_37
# BB#29:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB6_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_32:                               # %vector.body.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movupd	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB6_32
	jmp	.LBB6_33
.LBB6_30:                               #   in Loop: Header=BB6_26 Depth=1
	xorl	%esi, %esi
.LBB6_33:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB6_36
# BB#34:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB6_35:                               # %vector.body
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movupd	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB6_35
.LBB6_36:                               # %middle.block
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB6_42
	.p2align	4, 0x90
.LBB6_37:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rdx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB6_40
# BB#38:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB6_39:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	incq	%rdi
	jne	.LBB6_39
.LBB6_40:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$56, %rsi
	jb	.LBB6_42
	.p2align	4, 0x90
.LBB6_41:                               # %.lr.ph.i
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rcx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rcx)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rcx)
	movq	48(%rdx), %rax
	movq	%rax, 48(%rcx)
	movq	56(%rdx), %rax
	movq	%rax, 56(%rcx)
	addq	$64, %rdx
	addq	$64, %rcx
	cmpq	%r15, %rdx
	jne	.LBB6_41
.LBB6_42:                               # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	%r14, %r13
	je	.LBB6_59
# BB#43:                                # %.lr.ph32.i.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB6_50
# BB#44:                                # %.lr.ph.i9.preheader.prol
                                        #   in Loop: Header=BB6_26 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB6_45:                               # %.lr.ph.i9.prol
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_48
# BB#46:                                #   in Loop: Header=BB6_45 Depth=2
	movsd	%xmm1, (%rbx,%rcx)
	addq	$-8, %rcx
	jne	.LBB6_45
# BB#47:                                #   in Loop: Header=BB6_26 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB6_49
.LBB6_48:                               # %.lr.ph.i9.prol..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB6_26 Depth=1
	addq	%rbx, %rcx
.LBB6_49:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB6_26 Depth=1
	movsd	%xmm0, (%rcx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB6_50:                               # %.lr.ph32.i.prol.loopexit
                                        #   in Loop: Header=BB6_26 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB6_59
	.p2align	4, 0x90
.LBB6_51:                               # %.lr.ph32.i
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_53 Depth 3
                                        #       Child Loop BB6_57 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB6_56
# BB#52:                                # %.lr.ph.i9.preheader
                                        #   in Loop: Header=BB6_51 Depth=2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_53:                               # %.lr.ph.i9
                                        #   Parent Loop BB6_26 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_56
# BB#54:                                #   in Loop: Header=BB6_53 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_53
# BB#55:                                #   in Loop: Header=BB6_51 Depth=2
	movq	%rbx, %rdx
.LBB6_56:                               # %.critedge.i
                                        #   in Loop: Header=BB6_51 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB6_70
	.p2align	4, 0x90
.LBB6_57:                               # %.lr.ph.i9.1
                                        #   Parent Loop BB6_26 Depth=1
                                        #     Parent Loop BB6_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_58
# BB#68:                                #   in Loop: Header=BB6_57 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_57
# BB#69:                                #   in Loop: Header=BB6_51 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB6_70
	.p2align	4, 0x90
.LBB6_58:                               #   in Loop: Header=BB6_51 Depth=2
	movq	%rdx, %rsi
.LBB6_70:                               # %.critedge.i.1
                                        #   in Loop: Header=BB6_51 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB6_51
.LBB6_59:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.preheader
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_60:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit
                                        #   Parent Loop BB6_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB6_63
# BB#61:                                #   in Loop: Header=BB6_60 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB6_60
# BB#62:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB6_26 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB6_63:                               # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB6_26 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB6_26
	jmp	.LBB6_64
.LBB6_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
                                        #     Child Loop BB6_12 Depth 2
                                        #       Child Loop BB6_14 Depth 3
                                        #       Child Loop BB6_18 Depth 3
                                        #     Child Loop BB6_21 Depth 2
	cmpq	%r14, %r13
	je	.LBB6_20
# BB#4:                                 # %.lr.ph32.i.us.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB6_11
# BB#5:                                 # %.lr.ph.i9.us.preheader.prol
                                        #   in Loop: Header=BB6_3 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph.i9.us.prol
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=2
	movsd	%xmm1, (%rbx,%rcx)
	addq	$-8, %rcx
	jne	.LBB6_6
# BB#8:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB6_10
.LBB6_9:                                # %.lr.ph.i9.us.prol..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB6_3 Depth=1
	addq	%rbx, %rcx
.LBB6_10:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB6_3 Depth=1
	movsd	%xmm0, (%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB6_11:                               # %.lr.ph32.i.us.prol.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB6_20
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph32.i.us
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
                                        #       Child Loop BB6_18 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB6_17
# BB#13:                                # %.lr.ph.i9.us.preheader
                                        #   in Loop: Header=BB6_12 Depth=2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_14:                               # %.lr.ph.i9.us
                                        #   Parent Loop BB6_3 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_14 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_14
# BB#16:                                #   in Loop: Header=BB6_12 Depth=2
	movq	%rbx, %rdx
.LBB6_17:                               # %.critedge.i.us
                                        #   in Loop: Header=BB6_12 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB6_67
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph.i9.us.1
                                        #   Parent Loop BB6_3 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB6_19
# BB#65:                                #   in Loop: Header=BB6_18 Depth=3
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB6_18
# BB#66:                                #   in Loop: Header=BB6_12 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB6_67
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_12 Depth=2
	movq	%rdx, %rsi
.LBB6_67:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB6_12 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB6_12
.LBB6_20:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.us.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_21:                               # %_ZN9benchmark13insertionSortIPddEEvT_S2_.exit.us
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB6_24
# BB#22:                                #   in Loop: Header=BB6_21 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB6_21
# BB#23:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB6_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB6_24:                               # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB6_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB6_3
.LBB6_64:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end6-_Z19test_insertion_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,"axG",@progbits,_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,comdat
	.weak	_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,@function
_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc: # @_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 176
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB7_66
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB7_2
# BB#26:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%r12,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_33 Depth 2
                                        #     Child Loop BB7_36 Depth 2
                                        #     Child Loop BB7_40 Depth 2
                                        #     Child Loop BB7_42 Depth 2
                                        #     Child Loop BB7_46 Depth 2
                                        #     Child Loop BB7_52 Depth 2
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_59 Depth 3
                                        #     Child Loop BB7_62 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jb	.LBB7_38
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	je	.LBB7_38
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jne	.LBB7_38
# BB#30:                                # %vector.body.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB7_31
# BB#32:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB7_33:                               # %vector.body.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movupd	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB7_33
	jmp	.LBB7_34
.LBB7_31:                               #   in Loop: Header=BB7_27 Depth=1
	xorl	%esi, %esi
.LBB7_34:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB7_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB7_36:                               # %vector.body
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB7_36
.LBB7_37:                               # %middle.block
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB7_43
	.p2align	4, 0x90
.LBB7_38:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rcx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB7_41
# BB#39:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB7_40:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	addq	$8, %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB7_40
.LBB7_41:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$56, %rsi
	jb	.LBB7_43
	.p2align	4, 0x90
.LBB7_42:                               # %.lr.ph.i
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	movq	24(%rcx), %rax
	movq	%rax, 24(%rdx)
	movq	32(%rcx), %rax
	movq	%rax, 32(%rdx)
	movq	40(%rcx), %rax
	movq	%rax, 40(%rdx)
	movq	48(%rcx), %rax
	movq	%rax, 48(%rdx)
	movq	56(%rcx), %rax
	movq	%rax, 56(%rdx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%r15, %rcx
	jne	.LBB7_42
.LBB7_43:                               # %_ZN9benchmark4copyI14PointerWrapperIdES2_EEvT_S3_T0_.exit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	%r14, %r13
	je	.LBB7_61
# BB#44:                                # %.lr.ph24.i.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB7_51
# BB#45:                                # %.lr.ph.i13.preheader.prol
                                        #   in Loop: Header=BB7_27 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB7_46:                               # %.lr.ph.i13.prol
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_49
# BB#47:                                #   in Loop: Header=BB7_46 Depth=2
	addq	$-8, %rdx
	movsd	%xmm1, (%rsi)
	leaq	(%rbx,%rcx), %rsi
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB7_46
# BB#48:                                #   in Loop: Header=BB7_27 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB7_50
.LBB7_49:                               # %.lr.ph.i13.prol..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB7_27 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB7_50:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB7_27 Depth=1
	movsd	%xmm0, (%rcx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB7_51:                               # %.lr.ph24.i.prol.loopexit
                                        #   in Loop: Header=BB7_27 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB7_61
	.p2align	4, 0x90
.LBB7_52:                               # %.lr.ph24.i
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_59 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB7_57
# BB#53:                                # %.lr.ph.i13.preheader
                                        #   in Loop: Header=BB7_52 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB7_54:                               # %.lr.ph.i13
                                        #   Parent Loop BB7_27 Depth=1
                                        #     Parent Loop BB7_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_57
# BB#55:                                #   in Loop: Header=BB7_54 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_54
# BB#56:                                #   in Loop: Header=BB7_52 Depth=2
	movq	%rbx, %rdx
.LBB7_57:                               # %.critedge.i
                                        #   in Loop: Header=BB7_52 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB7_72
# BB#58:                                # %.lr.ph.i13.preheader.1
                                        #   in Loop: Header=BB7_52 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB7_59:                               # %.lr.ph.i13.1
                                        #   Parent Loop BB7_27 Depth=1
                                        #     Parent Loop BB7_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_60
# BB#70:                                #   in Loop: Header=BB7_59 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_59
# BB#71:                                #   in Loop: Header=BB7_52 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB7_72
	.p2align	4, 0x90
.LBB7_60:                               #   in Loop: Header=BB7_52 Depth=2
	movq	%rdx, %rsi
.LBB7_72:                               # %.critedge.i.1
                                        #   in Loop: Header=BB7_52 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB7_52
.LBB7_61:                               # %_ZN9benchmark13insertionSortI14PointerWrapperIdEdEEvT_S3_.exit.preheader
                                        #   in Loop: Header=BB7_27 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB7_62:                               # %_ZN9benchmark13insertionSortI14PointerWrapperIdEdEEvT_S3_.exit
                                        #   Parent Loop BB7_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB7_65
# BB#63:                                #   in Loop: Header=BB7_62 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB7_62
# BB#64:                                # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i
                                        #   in Loop: Header=BB7_27 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB7_65:                               # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit
                                        #   in Loop: Header=BB7_27 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB7_27
	jmp	.LBB7_66
.LBB7_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_12 Depth 2
                                        #       Child Loop BB7_14 Depth 3
                                        #       Child Loop BB7_19 Depth 3
                                        #     Child Loop BB7_22 Depth 2
	cmpq	%r14, %r13
	je	.LBB7_21
# BB#4:                                 # %.lr.ph24.i.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB7_11
# BB#5:                                 # %.lr.ph.i13.us.preheader.prol
                                        #   in Loop: Header=BB7_3 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.i13.us.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=2
	addq	$-8, %rdx
	movsd	%xmm1, (%rsi)
	leaq	(%rbx,%rcx), %rsi
	addq	$-8, %rcx
	cmpq	$-8, %rcx
	jne	.LBB7_6
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB7_10
.LBB7_9:                                # %.lr.ph.i13.us.prol..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	8(%rbx,%rcx), %rcx
.LBB7_10:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB7_3 Depth=1
	movsd	%xmm0, (%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB7_11:                               # %.lr.ph24.i.us.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB7_21
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph24.i.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_14 Depth 3
                                        #       Child Loop BB7_19 Depth 3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	movq	%rbx, %rdx
	je	.LBB7_17
# BB#13:                                # %.lr.ph.i13.us.preheader
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph.i13.us
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_17
# BB#15:                                #   in Loop: Header=BB7_14 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_14
# BB#16:                                #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, %rdx
.LBB7_17:                               # %.critedge.i.us
                                        #   in Loop: Header=BB7_12 Depth=2
	movsd	%xmm0, (%rdx)
	leaq	8(%rcx), %rdx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	cmpq	%rbx, %rdx
	movq	%rbx, %rsi
	je	.LBB7_69
# BB#18:                                # %.lr.ph.i13.us.preheader.1
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i13.us.1
                                        #   Parent Loop BB7_3 Depth=1
                                        #     Parent Loop BB7_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB7_20
# BB#67:                                #   in Loop: Header=BB7_19 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rdx)
	addq	$-8, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB7_19
# BB#68:                                #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, %rsi
	jmp	.LBB7_69
	.p2align	4, 0x90
.LBB7_20:                               #   in Loop: Header=BB7_12 Depth=2
	movq	%rdx, %rsi
.LBB7_69:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB7_12 Depth=2
	movsd	%xmm0, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB7_12
.LBB7_21:                               # %_ZN9benchmark13insertionSortI14PointerWrapperIdEdEEvT_S3_.exit.us.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB7_22:                               # %_ZN9benchmark13insertionSortI14PointerWrapperIdEdEEvT_S3_.exit.us
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB7_25
# BB#23:                                #   in Loop: Header=BB7_22 Depth=2
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB7_22
# BB#24:                                # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i.us
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB7_25:                               # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit.us
                                        #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB7_3
.LBB7_66:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc, .Lfunc_end7-_Z19test_insertion_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,"axG",@progbits,_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,comdat
	.weak	_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,@function
_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc: # @_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 176
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB8_66
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB8_2
# BB#26:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%rbx,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_27:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_33 Depth 2
                                        #     Child Loop BB8_36 Depth 2
                                        #     Child Loop BB8_40 Depth 2
                                        #     Child Loop BB8_42 Depth 2
                                        #     Child Loop BB8_46 Depth 2
                                        #     Child Loop BB8_52 Depth 2
                                        #       Child Loop BB8_54 Depth 3
                                        #       Child Loop BB8_59 Depth 3
                                        #     Child Loop BB8_62 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jb	.LBB8_38
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	je	.LBB8_38
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jne	.LBB8_38
# BB#30:                                # %vector.body.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB8_31
# BB#32:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_33:                               # %vector.body.prol
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movdqu	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB8_33
	jmp	.LBB8_34
.LBB8_31:                               #   in Loop: Header=BB8_27 Depth=1
	xorl	%esi, %esi
.LBB8_34:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB8_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB8_36:                               # %vector.body
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB8_36
.LBB8_37:                               # %middle.block
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB8_43
	.p2align	4, 0x90
.LBB8_38:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rdx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB8_41
# BB#39:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB8_40:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	incq	%rdi
	jne	.LBB8_40
.LBB8_41:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$56, %rsi
	jb	.LBB8_43
	.p2align	4, 0x90
.LBB8_42:                               # %.lr.ph.i
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rcx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rcx)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rcx)
	movq	48(%rdx), %rax
	movq	%rax, 48(%rcx)
	movq	56(%rdx), %rax
	movq	%rax, 56(%rcx)
	addq	$64, %rdx
	addq	$64, %rcx
	cmpq	%r15, %rdx
	jne	.LBB8_42
.LBB8_43:                               # %_ZN9benchmark4copyIP12ValueWrapperIdES3_EEvT_S4_T0_.exit
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	%r14, %r13
	je	.LBB8_61
# BB#44:                                # %.lr.ph29.i.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB8_51
# BB#45:                                # %.lr.ph.i9.prol
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	(%r13), %rcx
	movd	%rcx, %xmm0
	movl	$8, %edx
	.p2align	4, 0x90
.LBB8_46:                               #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rdx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_49
# BB#47:                                #   in Loop: Header=BB8_46 Depth=2
	movsd	%xmm1, (%rbx,%rdx)
	addq	$-8, %rdx
	jne	.LBB8_46
# BB#48:                                #   in Loop: Header=BB8_27 Depth=1
	movq	%rbx, %rdx
	jmp	.LBB8_50
.LBB8_49:                               # %..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB8_27 Depth=1
	addq	%rbx, %rdx
.LBB8_50:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	%rcx, (%rdx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB8_51:                               # %.lr.ph29.i.prol.loopexit
                                        #   in Loop: Header=BB8_27 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB8_61
	.p2align	4, 0x90
.LBB8_52:                               # %.lr.ph29.i
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_54 Depth 3
                                        #       Child Loop BB8_59 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	movq	%rbx, %rsi
	je	.LBB8_57
# BB#53:                                # %.lr.ph.i9
                                        #   in Loop: Header=BB8_52 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB8_54:                               #   Parent Loop BB8_27 Depth=1
                                        #     Parent Loop BB8_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_57
# BB#55:                                #   in Loop: Header=BB8_54 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB8_54
# BB#56:                                #   in Loop: Header=BB8_52 Depth=2
	movq	%rbx, %rsi
.LBB8_57:                               # %.critedge.i
                                        #   in Loop: Header=BB8_52 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	movq	%rbx, %rdi
	je	.LBB8_72
# BB#58:                                # %.lr.ph.i9.1
                                        #   in Loop: Header=BB8_52 Depth=2
	movd	%rdx, %xmm0
	.p2align	4, 0x90
.LBB8_59:                               #   Parent Loop BB8_27 Depth=1
                                        #     Parent Loop BB8_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_60
# BB#70:                                #   in Loop: Header=BB8_59 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB8_59
# BB#71:                                #   in Loop: Header=BB8_52 Depth=2
	movq	%rbx, %rdi
	jmp	.LBB8_72
	.p2align	4, 0x90
.LBB8_60:                               #   in Loop: Header=BB8_52 Depth=2
	movq	%rsi, %rdi
.LBB8_72:                               # %.critedge.i.1
                                        #   in Loop: Header=BB8_52 Depth=2
	movq	%rdx, (%rdi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB8_52
.LBB8_61:                               # %_ZN9benchmark13insertionSortIP12ValueWrapperIdES2_EEvT_S4_.exit.preheader
                                        #   in Loop: Header=BB8_27 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB8_62:                               # %_ZN9benchmark13insertionSortIP12ValueWrapperIdES2_EEvT_S4_.exit
                                        #   Parent Loop BB8_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB8_65
# BB#63:                                #   in Loop: Header=BB8_62 Depth=2
	movq	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB8_62
# BB#64:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i
                                        #   in Loop: Header=BB8_27 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB8_65:                               # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit
                                        #   in Loop: Header=BB8_27 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB8_27
	jmp	.LBB8_66
.LBB8_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_6 Depth 2
                                        #     Child Loop BB8_12 Depth 2
                                        #       Child Loop BB8_14 Depth 3
                                        #       Child Loop BB8_19 Depth 3
                                        #     Child Loop BB8_22 Depth 2
	cmpq	%r14, %r13
	je	.LBB8_21
# BB#4:                                 # %.lr.ph29.i.us.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB8_11
# BB#5:                                 # %.lr.ph.i9.us.prol
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	(%r13), %rcx
	movd	%rcx, %xmm0
	movl	$8, %edx
	.p2align	4, 0x90
.LBB8_6:                                #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rdx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_9
# BB#7:                                 #   in Loop: Header=BB8_6 Depth=2
	movsd	%xmm1, (%rbx,%rdx)
	addq	$-8, %rdx
	jne	.LBB8_6
# BB#8:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	%rbx, %rdx
	jmp	.LBB8_10
.LBB8_9:                                # %..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB8_3 Depth=1
	addq	%rbx, %rdx
.LBB8_10:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rcx, (%rdx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB8_11:                               # %.lr.ph29.i.us.prol.loopexit
                                        #   in Loop: Header=BB8_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_21
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph29.i.us
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_14 Depth 3
                                        #       Child Loop BB8_19 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	movq	%rbx, %rsi
	je	.LBB8_17
# BB#13:                                # %.lr.ph.i9.us
                                        #   in Loop: Header=BB8_12 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB8_14:                               #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_17
# BB#15:                                #   in Loop: Header=BB8_14 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB8_14
# BB#16:                                #   in Loop: Header=BB8_12 Depth=2
	movq	%rbx, %rsi
.LBB8_17:                               # %.critedge.i.us
                                        #   in Loop: Header=BB8_12 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	movq	%rbx, %rdi
	je	.LBB8_69
# BB#18:                                # %.lr.ph.i9.us.1
                                        #   in Loop: Header=BB8_12 Depth=2
	movd	%rdx, %xmm0
	.p2align	4, 0x90
.LBB8_19:                               #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB8_20
# BB#67:                                #   in Loop: Header=BB8_19 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB8_19
# BB#68:                                #   in Loop: Header=BB8_12 Depth=2
	movq	%rbx, %rdi
	jmp	.LBB8_69
	.p2align	4, 0x90
.LBB8_20:                               #   in Loop: Header=BB8_12 Depth=2
	movq	%rsi, %rdi
.LBB8_69:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB8_12 Depth=2
	movq	%rdx, (%rdi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB8_12
.LBB8_21:                               # %_ZN9benchmark13insertionSortIP12ValueWrapperIdES2_EEvT_S4_.exit.us.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB8_22:                               # %_ZN9benchmark13insertionSortIP12ValueWrapperIdES2_EEvT_S4_.exit.us
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB8_25
# BB#23:                                #   in Loop: Header=BB8_22 Depth=2
	movq	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB8_22
# BB#24:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i.us
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB8_25:                               # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit.us
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB8_3
.LBB8_66:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc, .Lfunc_end8-_Z19test_insertion_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,"axG",@progbits,_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,comdat
	.weak	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,@function
_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc: # @_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 176
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB9_64
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r9
	cmpq	%r15, %r12
	movq	%r9, 24(%rsp)           # 8-byte Spill
	je	.LBB9_2
# BB#25:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %r13
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r13, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rbx
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rsi,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-8(%rbx), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_26:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_32 Depth 2
                                        #     Child Loop BB9_35 Depth 2
                                        #     Child Loop BB9_39 Depth 2
                                        #     Child Loop BB9_41 Depth 2
                                        #     Child Loop BB9_45 Depth 2
                                        #     Child Loop BB9_49 Depth 2
                                        #       Child Loop BB9_52 Depth 3
                                        #       Child Loop BB9_56 Depth 3
                                        #     Child Loop BB9_60 Depth 2
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpq	$4, %r13
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jb	.LBB9_37
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	je	.LBB9_37
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jne	.LBB9_37
# BB#29:                                # %vector.body.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB9_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_32:                               # %vector.body.prol
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movdqu	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rax
	jne	.LBB9_32
	jmp	.LBB9_33
.LBB9_30:                               #   in Loop: Header=BB9_26 Depth=1
	xorl	%esi, %esi
.LBB9_33:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB9_36
# BB#34:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB9_35:                               # %vector.body
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB9_35
.LBB9_36:                               # %middle.block
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB9_42
	.p2align	4, 0x90
.LBB9_37:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rcx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB9_40
# BB#38:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB9_39:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	addq	$8, %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB9_39
.LBB9_40:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$56, %rsi
	jb	.LBB9_42
	.p2align	4, 0x90
.LBB9_41:                               # %.lr.ph.i
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	movq	24(%rcx), %rax
	movq	%rax, 24(%rdx)
	movq	32(%rcx), %rax
	movq	%rax, 32(%rdx)
	movq	40(%rcx), %rax
	movq	%rax, 40(%rdx)
	movq	48(%rcx), %rax
	movq	%rax, 48(%rdx)
	movq	56(%rcx), %rax
	movq	%rax, 56(%rdx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%r15, %rcx
	jne	.LBB9_41
.LBB9_42:                               # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIdEES4_EEvT_S5_T0_.exit
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	%r14, %r9
	je	.LBB9_59
# BB#43:                                # %.lr.ph24.i.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB9_48
# BB#44:                                # %.lr.ph.i14.prol
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	(%r9), %rcx
	movd	%rcx, %xmm0
	xorl	%esi, %esi
	movq	%r9, %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB9_45:                               #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_47
# BB#46:                                #   in Loop: Header=BB9_45 Depth=2
	addq	$-8, %rdi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rsi), %rdx
	addq	$-8, %rsi
	cmpq	$-8, %rsi
	jne	.LBB9_45
.LBB9_47:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	%rcx, (%rdx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB9_48:                               # %.lr.ph24.i.prol.loopexit
                                        #   in Loop: Header=BB9_26 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB9_59
	.p2align	4, 0x90
.LBB9_49:                               # %.lr.ph24.i
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_52 Depth 3
                                        #       Child Loop BB9_56 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	je	.LBB9_50
# BB#51:                                # %.lr.ph.i14
                                        #   in Loop: Header=BB9_49 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB9_52:                               #   Parent Loop BB9_26 Depth=1
                                        #     Parent Loop BB9_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_54
# BB#53:                                #   in Loop: Header=BB9_52 Depth=3
	addq	$-8, %rdi
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB9_52
	jmp	.LBB9_54
	.p2align	4, 0x90
.LBB9_50:                               #   in Loop: Header=BB9_49 Depth=2
	movq	%rcx, %rsi
.LBB9_54:                               # %.critedge.i
                                        #   in Loop: Header=BB9_49 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	je	.LBB9_58
# BB#55:                                # %.lr.ph.i14.1
                                        #   in Loop: Header=BB9_49 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rdi
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB9_56:                               #   Parent Loop BB9_26 Depth=1
                                        #     Parent Loop BB9_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_58
# BB#57:                                #   in Loop: Header=BB9_56 Depth=3
	addq	$-8, %rax
	movsd	%xmm1, (%rsi)
	movq	%rdi, %rsi
	addq	$-8, %rdi
	cmpq	%rdi, %rbp
	jne	.LBB9_56
.LBB9_58:                               # %.critedge.i.1
                                        #   in Loop: Header=BB9_49 Depth=2
	movq	%rdx, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB9_49
.LBB9_59:                               # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_.exit.preheader
                                        #   in Loop: Header=BB9_26 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB9_60:                               # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_.exit
                                        #   Parent Loop BB9_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB9_63
# BB#61:                                #   in Loop: Header=BB9_60 Depth=2
	movq	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB9_60
# BB#62:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i
                                        #   in Loop: Header=BB9_26 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB9_63:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit
                                        #   in Loop: Header=BB9_26 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB9_26
	jmp	.LBB9_64
.LBB9_2:                                # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %r13
	subq	%rbx, %r13
	shrq	$3, %r13
	movl	%r13d, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rbx), %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_6 Depth 2
                                        #     Child Loop BB9_10 Depth 2
                                        #       Child Loop BB9_13 Depth 3
                                        #       Child Loop BB9_17 Depth 3
                                        #     Child Loop BB9_21 Depth 2
	cmpq	%r14, %r9
	je	.LBB9_20
# BB#4:                                 # %.lr.ph24.i.us.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	testq	%r15, %r15
	movq	%r9, %rcx
	jne	.LBB9_9
# BB#5:                                 # %.lr.ph.i14.us.prol
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	(%r9), %rcx
	movd	%rcx, %xmm0
	xorl	%esi, %esi
	movq	%r9, %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB9_6:                                #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_6 Depth=2
	addq	$-8, %rdi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rsi), %rdx
	addq	$-8, %rsi
	cmpq	$-8, %rsi
	jne	.LBB9_6
.LBB9_8:                                # %.critedge.i.us.prol
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%rcx, (%rdx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB9_9:                                # %.lr.ph24.i.us.prol.loopexit
                                        #   in Loop: Header=BB9_3 Depth=1
	testq	%r13, %r13
	je	.LBB9_20
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph24.i.us
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_13 Depth 3
                                        #       Child Loop BB9_17 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	je	.LBB9_11
# BB#12:                                # %.lr.ph.i14.us
                                        #   in Loop: Header=BB9_10 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB9_13:                               #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_15
# BB#14:                                #   in Loop: Header=BB9_13 Depth=3
	addq	$-8, %rdi
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB9_13
	jmp	.LBB9_15
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_10 Depth=2
	movq	%rcx, %rsi
.LBB9_15:                               # %.critedge.i.us
                                        #   in Loop: Header=BB9_10 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rax
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rax
	je	.LBB9_19
# BB#16:                                # %.lr.ph.i14.us.1
                                        #   in Loop: Header=BB9_10 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rdi
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB9_17:                               #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_17 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rax)
	movq	%rdi, %rax
	addq	$-8, %rdi
	cmpq	%rdi, %rbp
	jne	.LBB9_17
.LBB9_19:                               # %.critedge.i.us.1
                                        #   in Loop: Header=BB9_10 Depth=2
	movq	%rdx, (%rax)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB9_10
.LBB9_20:                               # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_.exit.us.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB9_21:                               # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_.exit.us
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB9_24
# BB#22:                                #   in Loop: Header=BB9_21 Depth=2
	movq	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB9_21
# BB#23:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i.us
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB9_24:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit.us
                                        #   in Loop: Header=BB9_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB9_3
.LBB9_64:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc, .Lfunc_end9-_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,"axG",@progbits,_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,comdat
	.weak	_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,@function
_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc: # @_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 176
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB10_66
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r13
	cmpq	%r15, %r12
	je	.LBB10_2
# BB#26:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rcx
	shrq	$3, %rcx
	leaq	8(%rbx,%rcx,8), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	leaq	1(%rcx), %r9
	movabsq	$4611686018427387900, %rax # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r9, %rax
	leaq	-4(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rcx
	subq	%rbx, %rcx
	shrq	$3, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rbx
	sbbb	%cl, %cl
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	andb	%cl, %dl
	andb	$1, %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	leaq	(%rbx,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_27:                              # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_33 Depth 2
                                        #     Child Loop BB10_36 Depth 2
                                        #     Child Loop BB10_40 Depth 2
                                        #     Child Loop BB10_42 Depth 2
                                        #     Child Loop BB10_46 Depth 2
                                        #     Child Loop BB10_52 Depth 2
                                        #       Child Loop BB10_54 Depth 3
                                        #       Child Loop BB10_59 Depth 3
                                        #     Child Loop BB10_62 Depth 2
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpq	$4, %r9
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jb	.LBB10_38
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	je	.LBB10_38
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %rcx
	movq	%r12, %rdx
	jne	.LBB10_38
# BB#30:                                # %vector.body.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB10_31
# BB#32:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_33:                              # %vector.body.prol
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movdqu	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rcx
	jne	.LBB10_33
	jmp	.LBB10_34
.LBB10_31:                              #   in Loop: Header=BB10_27 Depth=1
	xorl	%esi, %esi
.LBB10_34:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB10_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB10_36:                              # %vector.body
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movupd	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB10_36
.LBB10_37:                              # %middle.block
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	24(%rsp), %r9           # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB10_43
	.p2align	4, 0x90
.LBB10_38:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rdx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB10_41
# BB#39:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB10_40:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	incq	%rdi
	jne	.LBB10_40
.LBB10_41:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$56, %rsi
	jb	.LBB10_43
	.p2align	4, 0x90
.LBB10_42:                              # %.lr.ph.i
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rcx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rcx)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rcx)
	movq	48(%rdx), %rax
	movq	%rax, 48(%rcx)
	movq	56(%rdx), %rax
	movq	%rax, 56(%rcx)
	addq	$64, %rdx
	addq	$64, %rcx
	cmpq	%r15, %rdx
	jne	.LBB10_42
.LBB10_43:                              # %_ZN9benchmark4copyIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESC_EEvT_SD_T0_.exit
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	%r14, %r13
	je	.LBB10_61
# BB#44:                                # %.lr.ph29.i.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r13, %rcx
	jne	.LBB10_51
# BB#45:                                # %.lr.ph.i9.prol
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	(%r13), %rcx
	movd	%rcx, %xmm0
	movl	$8, %edx
	.p2align	4, 0x90
.LBB10_46:                              #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rdx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_49
# BB#47:                                #   in Loop: Header=BB10_46 Depth=2
	movsd	%xmm1, (%rbx,%rdx)
	addq	$-8, %rdx
	jne	.LBB10_46
# BB#48:                                #   in Loop: Header=BB10_27 Depth=1
	movq	%rbx, %rdx
	jmp	.LBB10_50
.LBB10_49:                              # %..critedge.i.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB10_27 Depth=1
	addq	%rbx, %rdx
.LBB10_50:                              # %.critedge.i.prol
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	%rcx, (%rdx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB10_51:                              # %.lr.ph29.i.prol.loopexit
                                        #   in Loop: Header=BB10_27 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB10_61
	.p2align	4, 0x90
.LBB10_52:                              # %.lr.ph29.i
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_54 Depth 3
                                        #       Child Loop BB10_59 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	movq	%rbx, %rsi
	je	.LBB10_57
# BB#53:                                # %.lr.ph.i9
                                        #   in Loop: Header=BB10_52 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB10_54:                              #   Parent Loop BB10_27 Depth=1
                                        #     Parent Loop BB10_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_57
# BB#55:                                #   in Loop: Header=BB10_54 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB10_54
# BB#56:                                #   in Loop: Header=BB10_52 Depth=2
	movq	%rbx, %rsi
.LBB10_57:                              # %.critedge.i
                                        #   in Loop: Header=BB10_52 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	movq	%rbx, %rdi
	je	.LBB10_72
# BB#58:                                # %.lr.ph.i9.1
                                        #   in Loop: Header=BB10_52 Depth=2
	movd	%rdx, %xmm0
	.p2align	4, 0x90
.LBB10_59:                              #   Parent Loop BB10_27 Depth=1
                                        #     Parent Loop BB10_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_60
# BB#70:                                #   in Loop: Header=BB10_59 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB10_59
# BB#71:                                #   in Loop: Header=BB10_52 Depth=2
	movq	%rbx, %rdi
	jmp	.LBB10_72
	.p2align	4, 0x90
.LBB10_60:                              #   in Loop: Header=BB10_52 Depth=2
	movq	%rsi, %rdi
.LBB10_72:                              # %.critedge.i.1
                                        #   in Loop: Header=BB10_52 Depth=2
	movq	%rdx, (%rdi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB10_52
.LBB10_61:                              # %_ZN9benchmark13insertionSortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_.exit.preheader
                                        #   in Loop: Header=BB10_27 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_62:                              # %_ZN9benchmark13insertionSortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_.exit
                                        #   Parent Loop BB10_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB10_65
# BB#63:                                #   in Loop: Header=BB10_62 Depth=2
	movq	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_62
# BB#64:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i
                                        #   in Loop: Header=BB10_27 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movq	%r13, %rbp
	movq	%r9, %r13
	callq	printf
	movq	%r13, %r9
	movq	%rbp, %r13
	movl	iterations(%rip), %r8d
.LBB10_65:                              # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit
                                        #   in Loop: Header=BB10_27 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB10_27
	jmp	.LBB10_66
.LBB10_2:                               # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %rbp
	subq	%rbx, %rbp
	shrq	$3, %rbp
	movl	%ebp, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_6 Depth 2
                                        #     Child Loop BB10_12 Depth 2
                                        #       Child Loop BB10_14 Depth 3
                                        #       Child Loop BB10_19 Depth 3
                                        #     Child Loop BB10_22 Depth 2
	cmpq	%r14, %r13
	je	.LBB10_21
# BB#4:                                 # %.lr.ph29.i.us.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	testq	%r15, %r15
	movq	%r13, %rcx
	jne	.LBB10_11
# BB#5:                                 # %.lr.ph.i9.us.prol
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	(%r13), %rcx
	movd	%rcx, %xmm0
	movl	$8, %edx
	.p2align	4, 0x90
.LBB10_6:                               #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rdx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_9
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=2
	movsd	%xmm1, (%rbx,%rdx)
	addq	$-8, %rdx
	jne	.LBB10_6
# BB#8:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	%rbx, %rdx
	jmp	.LBB10_10
.LBB10_9:                               # %..critedge.i.us.loopexit.prol_crit_edge
                                        #   in Loop: Header=BB10_3 Depth=1
	addq	%rbx, %rdx
.LBB10_10:                              # %.critedge.i.us.prol
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%rcx, (%rdx)
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB10_11:                              # %.lr.ph29.i.us.prol.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB10_21
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph29.i.us
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_14 Depth 3
                                        #       Child Loop BB10_19 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	movq	%rbx, %rsi
	je	.LBB10_17
# BB#13:                                # %.lr.ph.i9.us
                                        #   in Loop: Header=BB10_12 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB10_14:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_17
# BB#15:                                #   in Loop: Header=BB10_14 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB10_14
# BB#16:                                #   in Loop: Header=BB10_12 Depth=2
	movq	%rbx, %rsi
.LBB10_17:                              # %.critedge.i.us
                                        #   in Loop: Header=BB10_12 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	movq	%rbx, %rdi
	je	.LBB10_69
# BB#18:                                # %.lr.ph.i9.us.1
                                        #   in Loop: Header=BB10_12 Depth=2
	movd	%rdx, %xmm0
	.p2align	4, 0x90
.LBB10_19:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_20
# BB#67:                                #   in Loop: Header=BB10_19 Depth=3
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB10_19
# BB#68:                                #   in Loop: Header=BB10_12 Depth=2
	movq	%rbx, %rdi
	jmp	.LBB10_69
	.p2align	4, 0x90
.LBB10_20:                              #   in Loop: Header=BB10_12 Depth=2
	movq	%rsi, %rdi
.LBB10_69:                              # %.critedge.i.us.1
                                        #   in Loop: Header=BB10_12 Depth=2
	movq	%rdx, (%rdi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB10_12
.LBB10_21:                              # %_ZN9benchmark13insertionSortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_.exit.us.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB10_22:                              # %_ZN9benchmark13insertionSortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_.exit.us
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r14
	je	.LBB10_25
# BB#23:                                #   in Loop: Header=BB10_22 Depth=2
	movq	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	leaq	8(%rcx), %rcx
	jbe	.LBB10_22
# BB#24:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i.us
                                        #   in Loop: Header=BB10_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %r8d
.LBB10_25:                              # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit.us
                                        #   in Loop: Header=BB10_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB10_3
.LBB10_66:                              # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc, .Lfunc_end10-_Z19test_insertion_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_endproc

	.section	.text._Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,"axG",@progbits,_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,comdat
	.weak	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.p2align	4, 0x90
	.type	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,@function
_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc: # @_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 176
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	iterations(%rip), %r8d
	testl	%r8d, %r8d
	jle	.LBB11_64
# BB#1:                                 # %.lr.ph
	leaq	8(%rbx), %r9
	cmpq	%r15, %r12
	movq	%r9, 24(%rsp)           # 8-byte Spill
	je	.LBB11_2
# BB#25:                                # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %r13
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r13, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	leaq	-16(%r14), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rbx
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rsi,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-8(%rbx), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_26:                              # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_32 Depth 2
                                        #     Child Loop BB11_35 Depth 2
                                        #     Child Loop BB11_39 Depth 2
                                        #     Child Loop BB11_41 Depth 2
                                        #     Child Loop BB11_45 Depth 2
                                        #     Child Loop BB11_49 Depth 2
                                        #       Child Loop BB11_52 Depth 3
                                        #       Child Loop BB11_56 Depth 3
                                        #     Child Loop BB11_60 Depth 2
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpq	$4, %r13
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jb	.LBB11_37
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	je	.LBB11_37
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rcx
	movq	%rbx, %rdx
	jne	.LBB11_37
# BB#29:                                # %vector.body.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB11_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_32:                              # %vector.body.prol
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12,%rsi,8), %xmm0
	movupd	16(%r12,%rsi,8), %xmm1
	movdqu	%xmm0, (%rbx,%rsi,8)
	movupd	%xmm1, 16(%rbx,%rsi,8)
	addq	$4, %rsi
	incq	%rax
	jne	.LBB11_32
	jmp	.LBB11_33
.LBB11_30:                              #   in Loop: Header=BB11_26 Depth=1
	xorl	%esi, %esi
.LBB11_33:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$12, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB11_36
# BB#34:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB11_35:                              # %vector.body
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB11_35
.LBB11_36:                              # %middle.block
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB11_42
	.p2align	4, 0x90
.LBB11_37:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	subq	%rcx, %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB11_40
# BB#38:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB11_39:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	addq	$8, %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB11_39
.LBB11_40:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$56, %rsi
	jb	.LBB11_42
	.p2align	4, 0x90
.LBB11_41:                              # %.lr.ph.i
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	movq	24(%rcx), %rax
	movq	%rax, 24(%rdx)
	movq	32(%rcx), %rax
	movq	%rax, 32(%rdx)
	movq	40(%rcx), %rax
	movq	%rax, 40(%rdx)
	movq	48(%rcx), %rax
	movq	%rax, 48(%rdx)
	movq	56(%rcx), %rax
	movq	%rax, 56(%rdx)
	addq	$64, %rcx
	addq	$64, %rdx
	cmpq	%r15, %rcx
	jne	.LBB11_41
.LBB11_42:                              # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESD_EEvT_SE_T0_.exit
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	%r14, %r9
	je	.LBB11_59
# BB#43:                                # %.lr.ph24.i.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	movq	%r9, %rcx
	jne	.LBB11_48
# BB#44:                                # %.lr.ph.i14.prol
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	(%r9), %rcx
	movd	%rcx, %xmm0
	xorl	%esi, %esi
	movq	%r9, %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB11_45:                              #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_47
# BB#46:                                #   in Loop: Header=BB11_45 Depth=2
	addq	$-8, %rdi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rsi), %rdx
	addq	$-8, %rsi
	cmpq	$-8, %rsi
	jne	.LBB11_45
.LBB11_47:                              # %.critedge.i.prol
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	%rcx, (%rdx)
	movq	88(%rsp), %rcx          # 8-byte Reload
.LBB11_48:                              # %.lr.ph24.i.prol.loopexit
                                        #   in Loop: Header=BB11_26 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB11_59
	.p2align	4, 0x90
.LBB11_49:                              # %.lr.ph24.i
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_52 Depth 3
                                        #       Child Loop BB11_56 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	je	.LBB11_50
# BB#51:                                # %.lr.ph.i14
                                        #   in Loop: Header=BB11_49 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB11_52:                              #   Parent Loop BB11_26 Depth=1
                                        #     Parent Loop BB11_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_54
# BB#53:                                #   in Loop: Header=BB11_52 Depth=3
	addq	$-8, %rdi
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB11_52
	jmp	.LBB11_54
	.p2align	4, 0x90
.LBB11_50:                              #   in Loop: Header=BB11_49 Depth=2
	movq	%rcx, %rsi
.LBB11_54:                              # %.critedge.i
                                        #   in Loop: Header=BB11_49 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rsi
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rsi
	je	.LBB11_58
# BB#55:                                # %.lr.ph.i14.1
                                        #   in Loop: Header=BB11_49 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rdi
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB11_56:                              #   Parent Loop BB11_26 Depth=1
                                        #     Parent Loop BB11_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_58
# BB#57:                                #   in Loop: Header=BB11_56 Depth=3
	addq	$-8, %rax
	movsd	%xmm1, (%rsi)
	movq	%rdi, %rsi
	addq	$-8, %rdi
	cmpq	%rdi, %rbp
	jne	.LBB11_56
.LBB11_58:                              # %.critedge.i.1
                                        #   in Loop: Header=BB11_49 Depth=2
	movq	%rdx, (%rsi)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB11_49
.LBB11_59:                              # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_.exit.preheader
                                        #   in Loop: Header=BB11_26 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB11_60:                              # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_.exit
                                        #   Parent Loop BB11_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB11_63
# BB#61:                                #   in Loop: Header=BB11_60 Depth=2
	movq	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB11_60
# BB#62:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i
                                        #   in Loop: Header=BB11_26 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB11_63:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit
                                        #   in Loop: Header=BB11_26 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
	jl	.LBB11_26
	jmp	.LBB11_64
.LBB11_2:                               # %.lr.ph.split.us.preheader
	leaq	-16(%r14), %r13
	subq	%rbx, %r13
	shrq	$3, %r13
	movl	%r13d, %r15d
	andl	$1, %r15d
	leaq	16(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rbx), %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_6 Depth 2
                                        #     Child Loop BB11_10 Depth 2
                                        #       Child Loop BB11_13 Depth 3
                                        #       Child Loop BB11_17 Depth 3
                                        #     Child Loop BB11_21 Depth 2
	cmpq	%r14, %r9
	je	.LBB11_20
# BB#4:                                 # %.lr.ph24.i.us.preheader
                                        #   in Loop: Header=BB11_3 Depth=1
	testq	%r15, %r15
	movq	%r9, %rcx
	jne	.LBB11_9
# BB#5:                                 # %.lr.ph.i14.us.prol
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	(%r9), %rcx
	movd	%rcx, %xmm0
	xorl	%esi, %esi
	movq	%r9, %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB11_6:                               #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=2
	addq	$-8, %rdi
	movsd	%xmm1, (%rdx)
	leaq	(%rbx,%rsi), %rdx
	addq	$-8, %rsi
	cmpq	$-8, %rsi
	jne	.LBB11_6
.LBB11_8:                               # %.critedge.i.us.prol
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	%rcx, (%rdx)
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB11_9:                               # %.lr.ph24.i.us.prol.loopexit
                                        #   in Loop: Header=BB11_3 Depth=1
	testq	%r13, %r13
	je	.LBB11_20
	.p2align	4, 0x90
.LBB11_10:                              # %.lr.ph24.i.us
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_13 Depth 3
                                        #       Child Loop BB11_17 Depth 3
	movq	(%rcx), %rdx
	cmpq	%rbx, %rcx
	je	.LBB11_11
# BB#12:                                # %.lr.ph.i14.us
                                        #   in Loop: Header=BB11_10 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB11_13:                              #   Parent Loop BB11_3 Depth=1
                                        #     Parent Loop BB11_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_15
# BB#14:                                #   in Loop: Header=BB11_13 Depth=3
	addq	$-8, %rdi
	movsd	%xmm1, (%rsi)
	addq	$-8, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB11_13
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_11:                              #   in Loop: Header=BB11_10 Depth=2
	movq	%rcx, %rsi
.LBB11_15:                              # %.critedge.i.us
                                        #   in Loop: Header=BB11_10 Depth=2
	movq	%rdx, (%rsi)
	leaq	8(%rcx), %rax
	movq	8(%rcx), %rdx
	cmpq	%rbx, %rax
	je	.LBB11_19
# BB#16:                                # %.lr.ph.i14.us.1
                                        #   in Loop: Header=BB11_10 Depth=2
	movd	%rdx, %xmm0
	movq	%rcx, %rdi
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB11_17:                              #   Parent Loop BB11_3 Depth=1
                                        #     Parent Loop BB11_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB11_19
# BB#18:                                #   in Loop: Header=BB11_17 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rax)
	movq	%rdi, %rax
	addq	$-8, %rdi
	cmpq	%rdi, %rbp
	jne	.LBB11_17
.LBB11_19:                              # %.critedge.i.us.1
                                        #   in Loop: Header=BB11_10 Depth=2
	movq	%rdx, (%rax)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.LBB11_10
.LBB11_20:                              # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_.exit.us.preheader
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB11_21:                              # %_ZN9benchmark13insertionSortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_.exit.us
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r14
	je	.LBB11_24
# BB#22:                                #   in Loop: Header=BB11_21 Depth=2
	movq	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB11_21
# BB#23:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i.us
                                        #   in Loop: Header=BB11_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	iterations(%rip), %r8d
.LBB11_24:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit.us
                                        #   in Loop: Header=BB11_3 Depth=1
	incl	%r12d
	cmpl	%r8d, %r12d
	jl	.LBB11_3
.LBB11_64:                              # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc, .Lfunc_end11-_Z19test_insertion_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc: # @_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 144
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB12_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB12_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_15 Depth 2
                                        #     Child Loop BB12_18 Depth 2
                                        #     Child Loop BB12_22 Depth 2
                                        #     Child Loop BB12_24 Depth 2
                                        #     Child Loop BB12_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB12_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB12_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB12_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB12_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB12_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_15:                              # %vector.body.prol
                                        #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB12_15
	jmp	.LBB12_16
.LBB12_13:                              #   in Loop: Header=BB12_9 Depth=1
	xorl	%edx, %edx
.LBB12_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB12_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB12_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB12_18:                              # %vector.body
                                        #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB12_18
.LBB12_19:                              # %middle.block
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB12_25
	.p2align	4, 0x90
.LBB12_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB12_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB12_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB12_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB12_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB12_22
.LBB12_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB12_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB12_25
	.p2align	4, 0x90
.LBB12_24:                              # %.lr.ph.i
                                        #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB12_24
.LBB12_25:                              # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB12_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_26:                              #   Parent Loop BB12_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB12_29
# BB#27:                                #   in Loop: Header=BB12_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB12_26
# BB#28:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB12_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB12_29:                              # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB12_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB12_9
	jmp	.LBB12_30
.LBB12_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB12_4:                               #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB12_7
# BB#5:                                 #   in Loop: Header=BB12_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB12_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB12_7:                               # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB12_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB12_3
.LBB12_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end12-_Z14test_quicksortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,"axG",@progbits,_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,comdat
	.weak	_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,@function
_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc: # @_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi139:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 144
.Lcfi142:
	.cfi_offset %rbx, -56
.Lcfi143:
	.cfi_offset %r12, -48
.Lcfi144:
	.cfi_offset %r13, -40
.Lcfi145:
	.cfi_offset %r14, -32
.Lcfi146:
	.cfi_offset %r15, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB13_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB13_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB13_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_15 Depth 2
                                        #     Child Loop BB13_18 Depth 2
                                        #     Child Loop BB13_22 Depth 2
                                        #     Child Loop BB13_24 Depth 2
                                        #     Child Loop BB13_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB13_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB13_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB13_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB13_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_15:                              # %vector.body.prol
                                        #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB13_15
	jmp	.LBB13_16
.LBB13_13:                              #   in Loop: Header=BB13_9 Depth=1
	xorl	%edx, %edx
.LBB13_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB13_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB13_18:                              # %vector.body
                                        #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB13_18
.LBB13_19:                              # %middle.block
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB13_25
	.p2align	4, 0x90
.LBB13_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB13_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB13_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB13_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB13_22
.LBB13_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB13_25
	.p2align	4, 0x90
.LBB13_24:                              # %.lr.ph.i
                                        #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB13_24
.LBB13_25:                              # %_ZN9benchmark4copyI14PointerWrapperIdES2_EEvT_S3_T0_.exit
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_26:                              #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB13_29
# BB#27:                                #   in Loop: Header=BB13_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB13_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i
                                        #   in Loop: Header=BB13_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB13_29:                              # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit
                                        #   in Loop: Header=BB13_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB13_9
	jmp	.LBB13_30
.LBB13_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB13_4:                               #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB13_7
# BB#5:                                 #   in Loop: Header=BB13_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB13_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i.us
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB13_7:                               # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit.us
                                        #   in Loop: Header=BB13_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB13_3
.LBB13_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc, .Lfunc_end13-_Z14test_quicksortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,"axG",@progbits,_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,comdat
	.weak	_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,@function
_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc: # @_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi154:
	.cfi_def_cfa_offset 144
.Lcfi155:
	.cfi_offset %rbx, -56
.Lcfi156:
	.cfi_offset %r12, -48
.Lcfi157:
	.cfi_offset %r13, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB14_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB14_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB14_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_15 Depth 2
                                        #     Child Loop BB14_18 Depth 2
                                        #     Child Loop BB14_22 Depth 2
                                        #     Child Loop BB14_24 Depth 2
                                        #     Child Loop BB14_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB14_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB14_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB14_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB14_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_15:                              # %vector.body.prol
                                        #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB14_15
	jmp	.LBB14_16
.LBB14_13:                              #   in Loop: Header=BB14_9 Depth=1
	xorl	%edx, %edx
.LBB14_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB14_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB14_18:                              # %vector.body
                                        #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB14_18
.LBB14_19:                              # %middle.block
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB14_25
	.p2align	4, 0x90
.LBB14_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB14_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB14_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB14_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB14_22
.LBB14_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB14_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB14_25
	.p2align	4, 0x90
.LBB14_24:                              # %.lr.ph.i
                                        #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB14_24
.LBB14_25:                              # %_ZN9benchmark4copyIP12ValueWrapperIdES3_EEvT_S4_T0_.exit
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB14_26:                              #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB14_29
# BB#27:                                #   in Loop: Header=BB14_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB14_26
# BB#28:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i
                                        #   in Loop: Header=BB14_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB14_29:                              # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit
                                        #   in Loop: Header=BB14_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB14_9
	jmp	.LBB14_30
.LBB14_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB14_4:                               #   Parent Loop BB14_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB14_7
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB14_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i.us
                                        #   in Loop: Header=BB14_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB14_7:                               # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit.us
                                        #   in Loop: Header=BB14_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB14_3
.LBB14_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc, .Lfunc_end14-_Z14test_quicksortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,"axG",@progbits,_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,comdat
	.weak	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,@function
_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc: # @_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi165:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi167:
	.cfi_def_cfa_offset 144
.Lcfi168:
	.cfi_offset %rbx, -56
.Lcfi169:
	.cfi_offset %r12, -48
.Lcfi170:
	.cfi_offset %r13, -40
.Lcfi171:
	.cfi_offset %r14, -32
.Lcfi172:
	.cfi_offset %r15, -24
.Lcfi173:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB15_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB15_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB15_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_15 Depth 2
                                        #     Child Loop BB15_18 Depth 2
                                        #     Child Loop BB15_22 Depth 2
                                        #     Child Loop BB15_24 Depth 2
                                        #     Child Loop BB15_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB15_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB15_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB15_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB15_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_15:                              # %vector.body.prol
                                        #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB15_15
	jmp	.LBB15_16
.LBB15_13:                              #   in Loop: Header=BB15_9 Depth=1
	xorl	%edx, %edx
.LBB15_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB15_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB15_18:                              # %vector.body
                                        #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB15_18
.LBB15_19:                              # %middle.block
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB15_25
	.p2align	4, 0x90
.LBB15_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB15_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB15_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB15_22
.LBB15_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB15_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB15_25
	.p2align	4, 0x90
.LBB15_24:                              # %.lr.ph.i
                                        #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB15_24
.LBB15_25:                              # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIdEES4_EEvT_S5_T0_.exit
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_26:                              #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB15_29
# BB#27:                                #   in Loop: Header=BB15_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB15_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i
                                        #   in Loop: Header=BB15_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB15_29:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit
                                        #   in Loop: Header=BB15_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB15_9
	jmp	.LBB15_30
.LBB15_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB15_7
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB15_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i.us
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB15_7:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit.us
                                        #   in Loop: Header=BB15_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB15_3
.LBB15_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc, .Lfunc_end15-_Z14test_quicksortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,"axG",@progbits,_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,comdat
	.weak	_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,@function
_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc: # @_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi180:
	.cfi_def_cfa_offset 144
.Lcfi181:
	.cfi_offset %rbx, -56
.Lcfi182:
	.cfi_offset %r12, -48
.Lcfi183:
	.cfi_offset %r13, -40
.Lcfi184:
	.cfi_offset %r14, -32
.Lcfi185:
	.cfi_offset %r15, -24
.Lcfi186:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB16_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB16_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_15 Depth 2
                                        #     Child Loop BB16_18 Depth 2
                                        #     Child Loop BB16_22 Depth 2
                                        #     Child Loop BB16_24 Depth 2
                                        #     Child Loop BB16_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB16_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB16_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB16_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB16_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB16_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_15:                              # %vector.body.prol
                                        #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB16_15
	jmp	.LBB16_16
.LBB16_13:                              #   in Loop: Header=BB16_9 Depth=1
	xorl	%edx, %edx
.LBB16_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB16_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB16_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB16_18:                              # %vector.body
                                        #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB16_18
.LBB16_19:                              # %middle.block
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB16_25
	.p2align	4, 0x90
.LBB16_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB16_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB16_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB16_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB16_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB16_22
.LBB16_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB16_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB16_25
	.p2align	4, 0x90
.LBB16_24:                              # %.lr.ph.i
                                        #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB16_24
.LBB16_25:                              # %_ZN9benchmark4copyIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESC_EEvT_SD_T0_.exit
                                        #   in Loop: Header=BB16_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_26:                              #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB16_29
# BB#27:                                #   in Loop: Header=BB16_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB16_26
# BB#28:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i
                                        #   in Loop: Header=BB16_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB16_29:                              # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit
                                        #   in Loop: Header=BB16_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB16_9
	jmp	.LBB16_30
.LBB16_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB16_4:                               #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB16_7
# BB#5:                                 #   in Loop: Header=BB16_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB16_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i.us
                                        #   in Loop: Header=BB16_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB16_7:                               # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit.us
                                        #   in Loop: Header=BB16_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB16_3
.LBB16_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc, .Lfunc_end16-_Z14test_quicksortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,"axG",@progbits,_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,comdat
	.weak	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,@function
_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc: # @_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi189:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi190:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi193:
	.cfi_def_cfa_offset 144
.Lcfi194:
	.cfi_offset %rbx, -56
.Lcfi195:
	.cfi_offset %r12, -48
.Lcfi196:
	.cfi_offset %r13, -40
.Lcfi197:
	.cfi_offset %r14, -32
.Lcfi198:
	.cfi_offset %r15, -24
.Lcfi199:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB17_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB17_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_15 Depth 2
                                        #     Child Loop BB17_18 Depth 2
                                        #     Child Loop BB17_22 Depth 2
                                        #     Child Loop BB17_24 Depth 2
                                        #     Child Loop BB17_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB17_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB17_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB17_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB17_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB17_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_15:                              # %vector.body.prol
                                        #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB17_15
	jmp	.LBB17_16
.LBB17_13:                              #   in Loop: Header=BB17_9 Depth=1
	xorl	%edx, %edx
.LBB17_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB17_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB17_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB17_18:                              # %vector.body
                                        #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB17_18
.LBB17_19:                              # %middle.block
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB17_25
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB17_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB17_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB17_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB17_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB17_22
.LBB17_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB17_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB17_25
	.p2align	4, 0x90
.LBB17_24:                              # %.lr.ph.i
                                        #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB17_24
.LBB17_25:                              # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESD_EEvT_SE_T0_.exit
                                        #   in Loop: Header=BB17_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_26:                              #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB17_29
# BB#27:                                #   in Loop: Header=BB17_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB17_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i
                                        #   in Loop: Header=BB17_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB17_29:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit
                                        #   in Loop: Header=BB17_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB17_9
	jmp	.LBB17_30
.LBB17_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB17_4:                               #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB17_7
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB17_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i.us
                                        #   in Loop: Header=BB17_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB17_7:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit.us
                                        #   in Loop: Header=BB17_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB17_3
.LBB17_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc, .Lfunc_end17-_Z14test_quicksortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,"axG",@progbits,_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,comdat
	.weak	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc,@function
_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc: # @_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi200:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi201:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi203:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi204:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi206:
	.cfi_def_cfa_offset 144
.Lcfi207:
	.cfi_offset %rbx, -56
.Lcfi208:
	.cfi_offset %r12, -48
.Lcfi209:
	.cfi_offset %r13, -40
.Lcfi210:
	.cfi_offset %r14, -32
.Lcfi211:
	.cfi_offset %r15, -24
.Lcfi212:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB18_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB18_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_15 Depth 2
                                        #     Child Loop BB18_18 Depth 2
                                        #     Child Loop BB18_22 Depth 2
                                        #     Child Loop BB18_24 Depth 2
                                        #     Child Loop BB18_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB18_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB18_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB18_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB18_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB18_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_15:                              # %vector.body.prol
                                        #   Parent Loop BB18_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB18_15
	jmp	.LBB18_16
.LBB18_13:                              #   in Loop: Header=BB18_9 Depth=1
	xorl	%edx, %edx
.LBB18_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB18_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB18_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB18_18:                              # %vector.body
                                        #   Parent Loop BB18_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB18_18
.LBB18_19:                              # %middle.block
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB18_25
	.p2align	4, 0x90
.LBB18_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB18_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB18_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB18_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB18_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB18_22
.LBB18_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB18_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB18_25
	.p2align	4, 0x90
.LBB18_24:                              # %.lr.ph.i
                                        #   Parent Loop BB18_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB18_24
.LBB18_25:                              # %_ZN9benchmark4copyIPdS1_EEvT_S2_T0_.exit
                                        #   in Loop: Header=BB18_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIPddEEvT_S2_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB18_26:                              #   Parent Loop BB18_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB18_29
# BB#27:                                #   in Loop: Header=BB18_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB18_26
# BB#28:                                # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i
                                        #   in Loop: Header=BB18_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB18_29:                              # %_Z13verify_sortedIPdEvT_S1_.exit
                                        #   in Loop: Header=BB18_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB18_9
	jmp	.LBB18_30
.LBB18_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIPddEEvT_S2_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB18_4:                               #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB18_7
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB18_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIPdEEbT_S2_.exit.i.us
                                        #   in Loop: Header=BB18_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB18_7:                               # %_Z13verify_sortedIPdEvT_S1_.exit.us
                                        #   in Loop: Header=BB18_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB18_3
.LBB18_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc, .Lfunc_end18-_Z14test_heap_sortIPddEvT_S1_S1_S1_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,"axG",@progbits,_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,comdat
	.weak	_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc,@function
_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc: # @_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi216:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi217:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi219:
	.cfi_def_cfa_offset 144
.Lcfi220:
	.cfi_offset %rbx, -56
.Lcfi221:
	.cfi_offset %r12, -48
.Lcfi222:
	.cfi_offset %r13, -40
.Lcfi223:
	.cfi_offset %r14, -32
.Lcfi224:
	.cfi_offset %r15, -24
.Lcfi225:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB19_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB19_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_15 Depth 2
                                        #     Child Loop BB19_18 Depth 2
                                        #     Child Loop BB19_22 Depth 2
                                        #     Child Loop BB19_24 Depth 2
                                        #     Child Loop BB19_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB19_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB19_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB19_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB19_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB19_15:                              # %vector.body.prol
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB19_15
	jmp	.LBB19_16
.LBB19_13:                              #   in Loop: Header=BB19_9 Depth=1
	xorl	%edx, %edx
.LBB19_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB19_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB19_18:                              # %vector.body
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB19_18
.LBB19_19:                              # %middle.block
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB19_25
	.p2align	4, 0x90
.LBB19_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB19_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB19_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB19_22
.LBB19_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB19_25
	.p2align	4, 0x90
.LBB19_24:                              # %.lr.ph.i
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB19_24
.LBB19_25:                              # %_ZN9benchmark4copyI14PointerWrapperIdES2_EEvT_S3_T0_.exit
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB19_26:                              #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB19_29
# BB#27:                                #   in Loop: Header=BB19_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB19_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i
                                        #   in Loop: Header=BB19_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB19_29:                              # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit
                                        #   in Loop: Header=BB19_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB19_9
	jmp	.LBB19_30
.LBB19_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB19_4:                               #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB19_7
# BB#5:                                 #   in Loop: Header=BB19_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB19_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperIdEEEbT_S3_.exit.i.us
                                        #   in Loop: Header=BB19_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB19_7:                               # %_Z13verify_sortedI14PointerWrapperIdEEvT_S2_.exit.us
                                        #   in Loop: Header=BB19_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB19_3
.LBB19_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc, .Lfunc_end19-_Z14test_heap_sortI14PointerWrapperIdEdEvT_S2_S2_S2_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,"axG",@progbits,_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,comdat
	.weak	_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc,@function
_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc: # @_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi230:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi232:
	.cfi_def_cfa_offset 144
.Lcfi233:
	.cfi_offset %rbx, -56
.Lcfi234:
	.cfi_offset %r12, -48
.Lcfi235:
	.cfi_offset %r13, -40
.Lcfi236:
	.cfi_offset %r14, -32
.Lcfi237:
	.cfi_offset %r15, -24
.Lcfi238:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB20_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB20_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB20_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_15 Depth 2
                                        #     Child Loop BB20_18 Depth 2
                                        #     Child Loop BB20_22 Depth 2
                                        #     Child Loop BB20_24 Depth 2
                                        #     Child Loop BB20_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB20_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB20_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB20_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB20_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB20_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_15:                              # %vector.body.prol
                                        #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB20_15
	jmp	.LBB20_16
.LBB20_13:                              #   in Loop: Header=BB20_9 Depth=1
	xorl	%edx, %edx
.LBB20_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB20_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB20_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB20_18:                              # %vector.body
                                        #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB20_18
.LBB20_19:                              # %middle.block
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB20_25
	.p2align	4, 0x90
.LBB20_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB20_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB20_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB20_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB20_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB20_22
.LBB20_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB20_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB20_25
	.p2align	4, 0x90
.LBB20_24:                              # %.lr.ph.i
                                        #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB20_24
.LBB20_25:                              # %_ZN9benchmark4copyIP12ValueWrapperIdES3_EEvT_S4_T0_.exit
                                        #   in Loop: Header=BB20_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB20_26:                              #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB20_29
# BB#27:                                #   in Loop: Header=BB20_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB20_26
# BB#28:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i
                                        #   in Loop: Header=BB20_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB20_29:                              # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit
                                        #   in Loop: Header=BB20_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB20_9
	jmp	.LBB20_30
.LBB20_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB20_4:                               #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB20_7
# BB#5:                                 #   in Loop: Header=BB20_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB20_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIP12ValueWrapperIdEEEbT_S4_.exit.i.us
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB20_7:                               # %_Z13verify_sortedIP12ValueWrapperIdEEvT_S3_.exit.us
                                        #   in Loop: Header=BB20_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB20_3
.LBB20_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc, .Lfunc_end20-_Z14test_heap_sortIP12ValueWrapperIdES1_EvT_S3_S3_S3_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,"axG",@progbits,_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,comdat
	.weak	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc,@function
_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc: # @_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi245:
	.cfi_def_cfa_offset 144
.Lcfi246:
	.cfi_offset %rbx, -56
.Lcfi247:
	.cfi_offset %r12, -48
.Lcfi248:
	.cfi_offset %r13, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB21_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB21_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_15 Depth 2
                                        #     Child Loop BB21_18 Depth 2
                                        #     Child Loop BB21_22 Depth 2
                                        #     Child Loop BB21_24 Depth 2
                                        #     Child Loop BB21_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB21_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB21_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB21_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB21_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_15:                              # %vector.body.prol
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB21_15
	jmp	.LBB21_16
.LBB21_13:                              #   in Loop: Header=BB21_9 Depth=1
	xorl	%edx, %edx
.LBB21_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB21_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB21_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB21_18:                              # %vector.body
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB21_18
.LBB21_19:                              # %middle.block
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB21_25
	.p2align	4, 0x90
.LBB21_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB21_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB21_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB21_22
.LBB21_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB21_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB21_25
	.p2align	4, 0x90
.LBB21_24:                              # %.lr.ph.i
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB21_24
.LBB21_25:                              # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIdEES4_EEvT_S5_T0_.exit
                                        #   in Loop: Header=BB21_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB21_26:                              #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB21_29
# BB#27:                                #   in Loop: Header=BB21_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB21_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i
                                        #   in Loop: Header=BB21_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB21_29:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit
                                        #   in Loop: Header=BB21_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB21_9
	jmp	.LBB21_30
.LBB21_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB21_4:                               #   Parent Loop BB21_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB21_7
# BB#5:                                 #   in Loop: Header=BB21_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB21_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIdEEEEbT_S5_.exit.i.us
                                        #   in Loop: Header=BB21_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB21_7:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIdEEEvT_S4_.exit.us
                                        #   in Loop: Header=BB21_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB21_3
.LBB21_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc, .Lfunc_end21-_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIdEES2_EvT_S4_S4_S4_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,"axG",@progbits,_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,comdat
	.weak	_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc,@function
_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc: # @_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi256:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi258:
	.cfi_def_cfa_offset 144
.Lcfi259:
	.cfi_offset %rbx, -56
.Lcfi260:
	.cfi_offset %r12, -48
.Lcfi261:
	.cfi_offset %r13, -40
.Lcfi262:
	.cfi_offset %r14, -32
.Lcfi263:
	.cfi_offset %r15, -24
.Lcfi264:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB22_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB22_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB22_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_15 Depth 2
                                        #     Child Loop BB22_18 Depth 2
                                        #     Child Loop BB22_22 Depth 2
                                        #     Child Loop BB22_24 Depth 2
                                        #     Child Loop BB22_26 Depth 2
	cmpq	$4, %rbp
	movq	%r14, %rax
	movq	%r12, %rcx
	jb	.LBB22_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB22_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r14, %rax
	movq	%r12, %rcx
	jne	.LBB22_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB22_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB22_15:                              # %vector.body.prol
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB22_15
	jmp	.LBB22_16
.LBB22_13:                              #   in Loop: Header=BB22_9 Depth=1
	xorl	%edx, %edx
.LBB22_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB22_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB22_18:                              # %vector.body
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB22_18
.LBB22_19:                              # %middle.block
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB22_25
	.p2align	4, 0x90
.LBB22_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB22_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB22_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB22_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	addq	$8, %rcx
	movq	%rdi, (%rax)
	addq	$8, %rax
	incq	%rsi
	jne	.LBB22_22
.LBB22_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB22_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB22_25
	.p2align	4, 0x90
.LBB22_24:                              # %.lr.ph.i
                                        #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rcx), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rcx), %rdx
	movq	%rdx, 40(%rax)
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movq	56(%rcx), %rdx
	movq	%rdx, 56(%rax)
	addq	$64, %rcx
	addq	$64, %rax
	cmpq	%r15, %rcx
	jne	.LBB22_24
.LBB22_25:                              # %_ZN9benchmark4copyIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESC_EEvT_SD_T0_.exit
                                        #   in Loop: Header=BB22_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB22_26:                              #   Parent Loop BB22_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB22_29
# BB#27:                                #   in Loop: Header=BB22_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB22_26
# BB#28:                                # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i
                                        #   in Loop: Header=BB22_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB22_29:                              # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit
                                        #   in Loop: Header=BB22_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB22_9
	jmp	.LBB22_30
.LBB22_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB22_4:                               #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB22_7
# BB#5:                                 #   in Loop: Header=BB22_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB22_4
# BB#6:                                 # %_ZN9benchmark9is_sortedIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEbT_SD_.exit.i.us
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB22_7:                               # %_Z13verify_sortedIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEEEvT_SC_.exit.us
                                        #   in Loop: Header=BB22_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB22_3
.LBB22_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc, .Lfunc_end22-_Z14test_heap_sortIP12ValueWrapperIS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IS0_IdEEEEEEEEEESA_EvT_SC_SC_SC_T0_PKc
	.cfi_endproc

	.section	.text._Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,"axG",@progbits,_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,comdat
	.weak	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.p2align	4, 0x90
	.type	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc,@function
_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc: # @_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi269:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi270:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi271:
	.cfi_def_cfa_offset 144
.Lcfi272:
	.cfi_offset %rbx, -56
.Lcfi273:
	.cfi_offset %r12, -48
.Lcfi274:
	.cfi_offset %r13, -40
.Lcfi275:
	.cfi_offset %r14, -32
.Lcfi276:
	.cfi_offset %r15, -24
.Lcfi277:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, iterations(%rip)
	jle	.LBB23_30
# BB#1:                                 # %.lr.ph
	cmpq	%r15, %r12
	je	.LBB23_2
# BB#8:                                 # %.lr.ph.split.preheader
	leaq	-8(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	8(%r14,%rax,8), %rcx
	leaq	8(%r12,%rax,8), %rdx
	leaq	1(%rax), %rbp
	movabsq	$4611686018427387900, %rsi # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rsi
	leaq	-4(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	cmpq	%rdx, %r14
	sbbb	%al, %al
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	leaq	(%r12,%rsi,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	negq	%rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	112(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB23_9:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_15 Depth 2
                                        #     Child Loop BB23_18 Depth 2
                                        #     Child Loop BB23_22 Depth 2
                                        #     Child Loop BB23_24 Depth 2
                                        #     Child Loop BB23_26 Depth 2
	cmpq	$4, %rbp
	movq	%r12, %rax
	movq	%r14, %rcx
	jb	.LBB23_20
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	je	.LBB23_20
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	movq	%r12, %rax
	movq	%r14, %rcx
	jne	.LBB23_20
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB23_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB23_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB23_15:                              # %vector.body.prol
                                        #   Parent Loop BB23_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdx,8), %xmm0
	movups	16(%r12,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB23_15
	jmp	.LBB23_16
.LBB23_13:                              #   in Loop: Header=BB23_9 Depth=1
	xorl	%edx, %edx
.LBB23_16:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB23_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB23_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB23_18:                              # %vector.body
                                        #   Parent Loop BB23_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rdx
	addq	$-16, %rax
	jne	.LBB23_18
.LBB23_19:                              # %middle.block
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	je	.LBB23_25
	.p2align	4, 0x90
.LBB23_20:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB23_9 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB23_23
# BB#21:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB23_9 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB23_22:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB23_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	addq	$8, %rax
	movq	%rdi, (%rcx)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB23_22
.LBB23_23:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB23_9 Depth=1
	cmpq	$56, %rdx
	jb	.LBB23_25
	.p2align	4, 0x90
.LBB23_24:                              # %.lr.ph.i
                                        #   Parent Loop BB23_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rcx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rcx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rcx)
	addq	$64, %rax
	addq	$64, %rcx
	cmpq	%r15, %rax
	jne	.LBB23_24
.LBB23_25:                              # %_ZN9benchmark4copyI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESD_EEvT_SE_T0_.exit
                                        #   in Loop: Header=BB23_9 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB23_26:                              #   Parent Loop BB23_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB23_29
# BB#27:                                #   in Loop: Header=BB23_26 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB23_26
# BB#28:                                # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i
                                        #   in Loop: Header=BB23_9 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB23_29:                              # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit
                                        #   in Loop: Header=BB23_9 Depth=1
	incl	%r13d
	cmpl	iterations(%rip), %r13d
	jl	.LBB23_9
	jmp	.LBB23_30
.LBB23_2:                               # %.lr.ph.split.us.preheader
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_4 Depth 2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB23_4:                               #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rbx
	je	.LBB23_7
# BB#5:                                 #   in Loop: Header=BB23_4 Depth=2
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	leaq	8(%rax), %rax
	jbe	.LBB23_4
# BB#6:                                 # %_ZN9benchmark9is_sortedI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEEEEbT_SE_.exit.i.us
                                        #   in Loop: Header=BB23_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	printf
.LBB23_7:                               # %_Z13verify_sortedI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEEEvT_SD_.exit.us
                                        #   in Loop: Header=BB23_3 Depth=1
	incl	%ebp
	cmpl	iterations(%rip), %ebp
	jl	.LBB23_3
.LBB23_30:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc, .Lfunc_end23-_Z14test_heap_sortI14PointerWrapperI12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEEESB_EvT_SD_SD_SD_T0_PKc
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortIPddEEvT_S2_,"axG",@progbits,_ZN9benchmark9quicksortIPddEEvT_S2_,comdat
	.weak	_ZN9benchmark9quicksortIPddEEvT_S2_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortIPddEEvT_S2_,@function
_ZN9benchmark9quicksortIPddEEvT_S2_:    # @_ZN9benchmark9quicksortIPddEEvT_S2_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 32
.Lcfi281:
	.cfi_offset %rbx, -24
.Lcfi282:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB24_5
	.p2align	4, 0x90
.LBB24_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_2 Depth 2
                                        #       Child Loop BB24_7 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movq	%r14, %rbx
	movq	%rdi, %rax
	jmp	.LBB24_2
.LBB24_9:                               #   in Loop: Header=BB24_2 Depth=2
	movsd	%xmm1, (%rbx)
	movsd	%xmm2, (%rax)
	movapd	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB24_2:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_7 Depth 3
	movsd	-8(%rbx), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm2
	ja	.LBB24_2
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB24_7
	jmp	.LBB24_4
	.p2align	4, 0x90
.LBB24_6:                               # %.preheader..preheader_crit_edge
                                        #   in Loop: Header=BB24_7 Depth=3
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rax
.LBB24_7:                               # %.preheader..preheader_crit_edge
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	%xmm1, %xmm0
	ja	.LBB24_6
# BB#8:                                 # %.preheader._crit_edge
                                        #   in Loop: Header=BB24_2 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB24_9
.LBB24_4:                               # %tailrecurse
                                        #   in Loop: Header=BB24_1 Depth=1
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIPddEEvT_S2_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB24_1
.LBB24_5:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	_ZN9benchmark9quicksortIPddEEvT_S2_, .Lfunc_end24-_ZN9benchmark9quicksortIPddEEvT_S2_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_,"axG",@progbits,_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_,comdat
	.weak	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_,@function
_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_: # @_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi283:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi284:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi285:
	.cfi_def_cfa_offset 32
.Lcfi286:
	.cfi_offset %rbx, -24
.Lcfi287:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB25_10
# BB#1:                                 # %.lr.ph35.preheader
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB25_2:                               # %.lr.ph35
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_3 Depth 2
                                        #       Child Loop BB25_6 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movq	%rbx, %rax
	movq	%r14, %rcx
	jmp	.LBB25_3
.LBB25_8:                               #   in Loop: Header=BB25_3 Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movsd	%xmm1, (%rax)
	.p2align	4, 0x90
.LBB25_3:                               #   Parent Loop BB25_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_6 Depth 3
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB25_3
# BB#4:                                 #   in Loop: Header=BB25_3 Depth=2
	cmpq	%rcx, %rax
	jae	.LBB25_9
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB25_3 Depth=2
	ucomisd	(%rax), %xmm0
	jbe	.LBB25_7
	.p2align	4, 0x90
.LBB25_6:                               # %.lr.ph
                                        #   Parent Loop BB25_2 Depth=1
                                        #     Parent Loop BB25_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB25_6
.LBB25_7:                               # %._crit_edge
                                        #   in Loop: Header=BB25_3 Depth=2
	cmpq	%rcx, %rax
	jb	.LBB25_8
.LBB25_9:                               # %tailrecurse
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB25_2
.LBB25_10:                              # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_, .Lfunc_end25-_ZN9benchmark9quicksortI14PointerWrapperIdEdEEvT_S3_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_,"axG",@progbits,_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_,comdat
	.weak	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_,@function
_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_: # @_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi288:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi289:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi290:
	.cfi_def_cfa_offset 32
.Lcfi291:
	.cfi_offset %rbx, -24
.Lcfi292:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB26_10
# BB#1:
	movq	%rsp, %rcx
	movq	(%rdi), %rax
	movq	%r14, %rbx
	movq	%rdi, %rdx
	jmp	.LBB26_2
	.p2align	4, 0x90
.LBB26_8:                               #   in Loop: Header=BB26_2 Depth=1
	movsd	%xmm1, (%rbx)
	movq	%rcx, %rdx
.LBB26_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_3 Depth 2
                                        #     Child Loop BB26_6 Depth 2
	movq	%rax, (%rcx)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB26_3:                               #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm1
	ja	.LBB26_3
# BB#4:                                 #   in Loop: Header=BB26_2 Depth=1
	cmpq	%rbx, %rdx
	jae	.LBB26_9
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	movd	%xmm1, %rax
	addq	$-8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB26_6:                               # %.preheader
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm1, %xmm0
	ja	.LBB26_6
# BB#7:                                 #   in Loop: Header=BB26_2 Depth=1
	cmpq	%rbx, %rcx
	jb	.LBB26_8
.LBB26_9:
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
.LBB26_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_, .Lfunc_end26-_ZN9benchmark9quicksortIP12ValueWrapperIdES2_EEvT_S4_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,"axG",@progbits,_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,comdat
	.weak	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,@function
_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_: # @_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi295:
	.cfi_def_cfa_offset 32
.Lcfi296:
	.cfi_offset %rbx, -24
.Lcfi297:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB27_10
# BB#1:                                 # %.lr.ph38.preheader
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB27_2:                               # %.lr.ph38
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_3 Depth 2
                                        #       Child Loop BB27_6 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movq	%rbx, %rax
	movq	%r14, %rcx
	jmp	.LBB27_3
.LBB27_8:                               #   in Loop: Header=BB27_3 Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movsd	%xmm1, (%rax)
	.p2align	4, 0x90
.LBB27_3:                               #   Parent Loop BB27_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_6 Depth 3
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB27_3
# BB#4:                                 #   in Loop: Header=BB27_3 Depth=2
	cmpq	%rcx, %rax
	jae	.LBB27_9
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB27_3 Depth=2
	ucomisd	(%rax), %xmm0
	jbe	.LBB27_7
	.p2align	4, 0x90
.LBB27_6:                               # %.lr.ph
                                        #   Parent Loop BB27_2 Depth=1
                                        #     Parent Loop BB27_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB27_6
.LBB27_7:                               # %._crit_edge
                                        #   in Loop: Header=BB27_3 Depth=2
	cmpq	%rcx, %rax
	jb	.LBB27_8
.LBB27_9:                               # %tailrecurse
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB27_2
.LBB27_10:                              # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end27:
	.size	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_, .Lfunc_end27-_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,"axG",@progbits,_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,comdat
	.weak	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,@function
_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_: # @_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi298:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi300:
	.cfi_def_cfa_offset 32
.Lcfi301:
	.cfi_offset %rbx, -24
.Lcfi302:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB28_10
# BB#1:
	movq	%rsp, %rcx
	movq	(%rdi), %rax
	movq	%r14, %rbx
	movq	%rdi, %rdx
	jmp	.LBB28_2
	.p2align	4, 0x90
.LBB28_8:                               #   in Loop: Header=BB28_2 Depth=1
	movsd	%xmm1, (%rbx)
	movq	%rcx, %rdx
.LBB28_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_3 Depth 2
                                        #     Child Loop BB28_6 Depth 2
	movq	%rax, (%rcx)
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB28_3:                               #   Parent Loop BB28_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm0, %xmm1
	ja	.LBB28_3
# BB#4:                                 #   in Loop: Header=BB28_2 Depth=1
	cmpq	%rbx, %rdx
	jae	.LBB28_9
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB28_2 Depth=1
	movd	%xmm1, %rax
	addq	$-8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB28_6:                               # %.preheader
                                        #   Parent Loop BB28_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm1, %xmm0
	ja	.LBB28_6
# BB#7:                                 #   in Loop: Header=BB28_2 Depth=1
	cmpq	%rbx, %rcx
	jb	.LBB28_8
.LBB28_9:
	addq	$8, %rbx
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
.LBB28_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_, .Lfunc_end28-_ZN9benchmark9quicksortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.cfi_endproc

	.section	.text._ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,"axG",@progbits,_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,comdat
	.weak	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.p2align	4, 0x90
	.type	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,@function
_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_: # @_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi303:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi304:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi305:
	.cfi_def_cfa_offset 32
.Lcfi306:
	.cfi_offset %rbx, -24
.Lcfi307:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%r14, %rax
	subq	%rdi, %rax
	cmpq	$9, %rax
	jl	.LBB29_10
# BB#1:                                 # %.lr.ph38.preheader
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB29_2:                               # %.lr.ph38
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_3 Depth 2
                                        #       Child Loop BB29_6 Depth 3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movq	%rbx, %rax
	movq	%r14, %rcx
	jmp	.LBB29_3
.LBB29_8:                               #   in Loop: Header=BB29_3 Depth=2
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movsd	%xmm1, (%rax)
	.p2align	4, 0x90
.LBB29_3:                               #   Parent Loop BB29_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_6 Depth 3
	movq	%rcx, %rbx
	leaq	-8(%rbx), %rcx
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB29_3
# BB#4:                                 #   in Loop: Header=BB29_3 Depth=2
	cmpq	%rcx, %rax
	jae	.LBB29_9
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB29_3 Depth=2
	ucomisd	(%rax), %xmm0
	jbe	.LBB29_7
	.p2align	4, 0x90
.LBB29_6:                               # %.lr.ph
                                        #   Parent Loop BB29_2 Depth=1
                                        #     Parent Loop BB29_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	8(%rax), %xmm0
	leaq	8(%rax), %rax
	ja	.LBB29_6
.LBB29_7:                               # %._crit_edge
                                        #   in Loop: Header=BB29_3 Depth=2
	cmpq	%rcx, %rax
	jb	.LBB29_8
.LBB29_9:                               # %tailrecurse
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rbx, %rsi
	callq	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$8, %rax
	movq	%rbx, %rdi
	jg	.LBB29_2
.LBB29_10:                              # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_, .Lfunc_end29-_ZN9benchmark9quicksortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortIPddEEvT_S2_,"axG",@progbits,_ZN9benchmark8heapsortIPddEEvT_S2_,comdat
	.weak	_ZN9benchmark8heapsortIPddEEvT_S2_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortIPddEEvT_S2_,@function
_ZN9benchmark8heapsortIPddEEvT_S2_:     # @_ZN9benchmark8heapsortIPddEEvT_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi308:
	.cfi_def_cfa_offset 16
.Lcfi309:
	.cfi_offset %rbx, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB30_27
# BB#1:                                 # %.lr.ph54
	movq	%rsi, %r11
	sarq	$3, %r11
	movq	%r11, %r9
	shrq	$63, %r9
	addq	%r11, %r9
	sarq	%r9
	leaq	-1(%r11), %r8
	.p2align	4, 0x90
.LBB30_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_4 Depth 2
                                        #     Child Loop BB30_10 Depth 2
	movq	%r9, %r10
	leaq	-1(%r10), %r9
	movsd	-8(%rdi,%r10,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r10,%r10), %rcx
	addq	$2, %rcx
	cmpq	%r11, %rcx
	jge	.LBB30_5
# BB#3:                                 # %.lr.ph51.i31.preheader
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB30_4:                               # %.lr.ph51.i31
                                        #   Parent Loop BB30_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%edx, %edx
	ucomisd	-8(%rdi,%rcx,8), %xmm1
	seta	%dl
	leaq	(%rdx,%rcx), %rbx
	leaq	-1(%rdx,%rcx), %rdx
	movq	-8(%rdi,%rbx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r11, %rbx
	movq	%rdx, %rax
	movq	%rbx, %rcx
	jl	.LBB30_4
	jmp	.LBB30_6
	.p2align	4, 0x90
.LBB30_5:                               #   in Loop: Header=BB30_2 Depth=1
	movq	%rcx, %rbx
	movq	%r9, %rdx
.LBB30_6:                               # %._crit_edge.i34
                                        #   in Loop: Header=BB30_2 Depth=1
	cmpq	%r11, %rbx
	jne	.LBB30_8
# BB#7:                                 #   in Loop: Header=BB30_2 Depth=1
	movq	-8(%rdi,%r11,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	movq	%r8, %rdx
.LBB30_8:                               # %.preheader.i36
                                        #   in Loop: Header=BB30_2 Depth=1
	cmpq	%r10, %rdx
	jl	.LBB30_12
	.p2align	4, 0x90
.LBB30_10:                              # %.lr.ph.i40
                                        #   Parent Loop BB30_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB30_12
# BB#11:                                #   in Loop: Header=BB30_10 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jge	.LBB30_10
	jmp	.LBB30_13
	.p2align	4, 0x90
.LBB30_12:                              #   in Loop: Header=BB30_2 Depth=1
	movq	%rdx, %rax
.LBB30_13:                              # %_ZN9benchmark7sift_inIPddEEvlT_lT0_.exit42
                                        #   in Loop: Header=BB30_2 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r10
	jg	.LBB30_2
# BB#14:                                # %.preheader
	cmpq	$9, %rsi
	jl	.LBB30_27
	.p2align	4, 0x90
.LBB30_15:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_17 Depth 2
                                        #     Child Loop BB30_22 Depth 2
	movq	%r11, %rdx
	leaq	-1(%rdx), %r11
	movsd	-8(%rdi,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%rdx,8)
	cmpq	$3, %r11
	jl	.LBB30_18
# BB#16:                                # %.lr.ph51.i.preheader
                                        #   in Loop: Header=BB30_15 Depth=1
	xorl	%ecx, %ecx
	movl	$2, %eax
	.p2align	4, 0x90
.LBB30_17:                              # %.lr.ph51.i
                                        #   Parent Loop BB30_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdi,%rax,8), %xmm1
	seta	%sil
	orq	%rax, %rsi
	movq	-8(%rdi,%rsi,8), %rax
	movq	%rax, (%rdi,%rcx,8)
	leaq	-1(%rsi), %rcx
	addq	%rsi, %rsi
	cmpq	%r11, %rsi
	movq	%rsi, %rax
	jl	.LBB30_17
	jmp	.LBB30_19
	.p2align	4, 0x90
.LBB30_18:                              #   in Loop: Header=BB30_15 Depth=1
	movl	$2, %esi
	xorl	%ecx, %ecx
.LBB30_19:                              # %._crit_edge.i
                                        #   in Loop: Header=BB30_15 Depth=1
	cmpq	%r11, %rsi
	jne	.LBB30_21
# BB#20:                                #   in Loop: Header=BB30_15 Depth=1
	movq	-16(%rdi,%rdx,8), %rax
	addq	$-2, %rdx
	movq	%rax, (%rdi,%rcx,8)
	movq	%rdx, %rcx
.LBB30_21:                              # %.preheader.i
                                        #   in Loop: Header=BB30_15 Depth=1
	testq	%rcx, %rcx
	jle	.LBB30_25
	.p2align	4, 0x90
.LBB30_22:                              # %.lr.ph.i
                                        #   Parent Loop BB30_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB30_25
# BB#23:                                #   in Loop: Header=BB30_22 Depth=2
	movsd	%xmm1, (%rdi,%rcx,8)
	cmpq	$1, %rdx
	movq	%rax, %rcx
	jg	.LBB30_22
	jmp	.LBB30_26
	.p2align	4, 0x90
.LBB30_25:                              #   in Loop: Header=BB30_15 Depth=1
	movq	%rcx, %rax
.LBB30_26:                              # %_ZN9benchmark7sift_inIPddEEvlT_lT0_.exit
                                        #   in Loop: Header=BB30_15 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r11
	jg	.LBB30_15
.LBB30_27:                              # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end30:
	.size	_ZN9benchmark8heapsortIPddEEvT_S2_, .Lfunc_end30-_ZN9benchmark8heapsortIPddEEvT_S2_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_,"axG",@progbits,_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_,comdat
	.weak	_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_,@function
_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_: # @_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi312:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi313:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi314:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 56
.Lcfi316:
	.cfi_offset %rbx, -56
.Lcfi317:
	.cfi_offset %r12, -48
.Lcfi318:
	.cfi_offset %r13, -40
.Lcfi319:
	.cfi_offset %r14, -32
.Lcfi320:
	.cfi_offset %r15, -24
.Lcfi321:
	.cfi_offset %rbp, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB31_25
# BB#1:                                 # %.lr.ph100
	movq	%rsi, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r14
	leaq	-16(%rsp), %r8
	leaq	-8(%rsp), %r9
	leaq	-32(%rsp), %r10
	leaq	-24(%rsp), %r11
	jmp	.LBB31_2
	.p2align	4, 0x90
.LBB31_12:                              # %.sink.split.i38
                                        #   in Loop: Header=BB31_2 Depth=1
	leaq	(%rdi,%rbx,8), %rbp
	movq	%rbp, (%rax)
	movq	(%rdi,%rbx,8), %rax
	leaq	(%rdi,%rcx,8), %rbp
	movq	%rbp, (%rdx)
	movq	%rax, (%rdi,%rcx,8)
	movq	%rbx, %rcx
.LBB31_13:                              #   in Loop: Header=BB31_2 Depth=1
	cmpq	%r12, %rcx
	jl	.LBB31_4
# BB#14:                                #   in Loop: Header=BB31_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rbx
	sarq	%rbx
	ucomisd	(%rdi,%rbx,8), %xmm0
	jbe	.LBB31_4
# BB#15:                                #   in Loop: Header=BB31_2 Depth=1
	movq	%r11, %rax
	movq	%r10, %rdx
	jmp	.LBB31_12
	.p2align	4, 0x90
.LBB31_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdi,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rdx
	addq	$2, %rdx
	cmpq	%r13, %rdx
	jge	.LBB31_3
# BB#9:                                 # %.lr.ph.i30.preheader
                                        #   in Loop: Header=BB31_2 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB31_10:                              # %.lr.ph.i30
                                        #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	leaq	(%rcx,%rdx), %rbx
	leaq	-1(%rcx,%rdx), %rcx
	movq	-8(%rdi,%rbx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r13, %rbx
	movq	%rcx, %rax
	movq	%rbx, %rdx
	jl	.LBB31_10
	jmp	.LBB31_11
	.p2align	4, 0x90
.LBB31_3:                               #   in Loop: Header=BB31_2 Depth=1
	movq	%rdx, %rbx
	movq	%r15, %rcx
.LBB31_11:                              # %._crit_edge.i33
                                        #   in Loop: Header=BB31_2 Depth=1
	cmpq	%r13, %rbx
	movq	%r14, %rbx
	movq	%r9, %rax
	movq	%r8, %rdx
	je	.LBB31_12
	jmp	.LBB31_13
	.p2align	4, 0x90
.LBB31_4:                               # %_ZN9benchmark7sift_inI14PointerWrapperIdEdEEvlT_lT0_.exit42
                                        #   in Loop: Header=BB31_2 Depth=1
	movsd	%xmm0, (%rdi,%rcx,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB31_2
# BB#5:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB31_25
# BB#6:
	leaq	-16(%rsp), %r10
	leaq	-8(%rsp), %rbx
	leaq	-32(%rsp), %r8
	leaq	-24(%rsp), %r9
	.p2align	4, 0x90
.LBB31_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_17 Depth 2
	leaq	-1(%r13), %rsi
	movsd	-8(%rdi,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r13,8)
	cmpq	$3, %rsi
	jl	.LBB31_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB31_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %edx
	.p2align	4, 0x90
.LBB31_17:                              # %.lr.ph.i
                                        #   Parent Loop BB31_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	orq	%rdx, %rcx
	movq	-8(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	leaq	-1(%rcx), %rax
	addq	%rcx, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, %rdx
	jl	.LBB31_17
	jmp	.LBB31_18
	.p2align	4, 0x90
.LBB31_8:                               #   in Loop: Header=BB31_7 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
.LBB31_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB31_7 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB31_21
# BB#19:                                #   in Loop: Header=BB31_7 Depth=1
	addq	$-2, %r13
	movq	%rbx, %rcx
	movq	%r10, %rdx
.LBB31_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB31_7 Depth=1
	leaq	(%rdi,%r13,8), %rbp
	movq	%rbp, (%rcx)
	movq	(%rdi,%r13,8), %rcx
	leaq	(%rdi,%rax,8), %rbp
	movq	%rbp, (%rdx)
	movq	%rcx, (%rdi,%rax,8)
	movq	%r13, %rax
.LBB31_21:                              #   in Loop: Header=BB31_7 Depth=1
	testq	%rax, %rax
	jle	.LBB31_24
# BB#22:                                #   in Loop: Header=BB31_7 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %r13
	sarq	%r13
	ucomisd	(%rdi,%r13,8), %xmm0
	jbe	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_7 Depth=1
	movq	%r9, %rcx
	movq	%r8, %rdx
	jmp	.LBB31_20
	.p2align	4, 0x90
.LBB31_24:                              # %_ZN9benchmark7sift_inI14PointerWrapperIdEdEEvlT_lT0_.exit
                                        #   in Loop: Header=BB31_7 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %rsi
	movq	%rsi, %r13
	jg	.LBB31_7
.LBB31_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_, .Lfunc_end31-_ZN9benchmark8heapsortI14PointerWrapperIdEdEEvT_S3_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_,"axG",@progbits,_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_,comdat
	.weak	_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_,@function
_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_: # @_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi322:
	.cfi_def_cfa_offset 16
.Lcfi323:
	.cfi_offset %rbx, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB32_22
# BB#1:                                 # %.lr.ph52
	movq	%rsi, %r11
	sarq	$3, %r11
	movq	%r11, %r10
	shrq	$63, %r10
	addq	%r11, %r10
	sarq	%r10
	leaq	-1(%r11), %r8
	.p2align	4, 0x90
.LBB32_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_8 Depth 2
	leaq	-1(%r10), %r9
	movsd	-8(%rdi,%r10,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r10,%r10), %rcx
	addq	$2, %rcx
	cmpq	%r11, %rcx
	jge	.LBB32_3
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB32_8:                               # %.lr.ph.i
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	-8(%rdi,%rcx,8), %xmm1
	seta	%al
	leaq	(%rax,%rcx), %rbx
	leaq	-1(%rax,%rcx), %rax
	movq	-8(%rdi,%rbx,8), %rcx
	movq	%rcx, (%rdi,%rdx,8)
	addq	%rbx, %rbx
	cmpq	%r11, %rbx
	movq	%rax, %rdx
	movq	%rbx, %rcx
	jl	.LBB32_8
	jmp	.LBB32_9
	.p2align	4, 0x90
.LBB32_3:                               #   in Loop: Header=BB32_2 Depth=1
	movq	%rcx, %rbx
	movq	%r9, %rax
.LBB32_9:                               # %._crit_edge.i
                                        #   in Loop: Header=BB32_2 Depth=1
	cmpq	%r11, %rbx
	movq	%r8, %rcx
	jne	.LBB32_11
.LBB32_10:                              # %.sink.split.i
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movq	%rcx, %rax
.LBB32_11:                              #   in Loop: Header=BB32_2 Depth=1
	cmpq	%r10, %rax
	jl	.LBB32_13
# BB#12:                                #   in Loop: Header=BB32_2 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %rcx
	sarq	%rcx
	ucomisd	(%rdi,%rcx,8), %xmm0
	ja	.LBB32_10
.LBB32_13:                              # %_ZN9benchmark7sift_inIP12ValueWrapperIdES2_EEvlT_lT0_.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r10
	movq	%r9, %r10
	jg	.LBB32_2
# BB#4:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB32_22
	.p2align	4, 0x90
.LBB32_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_15 Depth 2
	leaq	-1(%r11), %rcx
	movsd	-8(%rdi,%r11,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r11,8)
	cmpq	$3, %rcx
	jl	.LBB32_6
# BB#14:                                # %.lr.ph.i30.preheader
                                        #   in Loop: Header=BB32_5 Depth=1
	xorl	%edx, %edx
	movl	$2, %eax
	.p2align	4, 0x90
.LBB32_15:                              # %.lr.ph.i30
                                        #   Parent Loop BB32_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdi,%rax,8), %xmm1
	seta	%sil
	orq	%rax, %rsi
	movq	-8(%rdi,%rsi,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	leaq	-1(%rsi), %rdx
	addq	%rsi, %rsi
	cmpq	%rcx, %rsi
	movq	%rsi, %rax
	jl	.LBB32_15
	jmp	.LBB32_16
	.p2align	4, 0x90
.LBB32_6:                               #   in Loop: Header=BB32_5 Depth=1
	movl	$2, %esi
	xorl	%edx, %edx
.LBB32_16:                              # %._crit_edge.i33
                                        #   in Loop: Header=BB32_5 Depth=1
	cmpq	%rcx, %rsi
	jne	.LBB32_19
# BB#17:                                #   in Loop: Header=BB32_5 Depth=1
	addq	$-2, %r11
.LBB32_18:                              # %.sink.split.i36
                                        #   in Loop: Header=BB32_5 Depth=1
	movq	(%rdi,%r11,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	movq	%r11, %rdx
.LBB32_19:                              #   in Loop: Header=BB32_5 Depth=1
	testq	%rdx, %rdx
	jle	.LBB32_21
# BB#20:                                #   in Loop: Header=BB32_5 Depth=1
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %r11
	sarq	%r11
	ucomisd	(%rdi,%r11,8), %xmm0
	ja	.LBB32_18
.LBB32_21:                              # %_ZN9benchmark7sift_inIP12ValueWrapperIdES2_EEvlT_lT0_.exit40
                                        #   in Loop: Header=BB32_5 Depth=1
	movsd	%xmm0, (%rdi,%rdx,8)
	cmpq	$1, %rcx
	movq	%rcx, %r11
	jg	.LBB32_5
.LBB32_22:                              # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end32:
	.size	_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_, .Lfunc_end32-_ZN9benchmark8heapsortIP12ValueWrapperIdES2_EEvT_S4_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,"axG",@progbits,_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,comdat
	.weak	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_,@function
_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_: # @_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi327:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi328:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi329:
	.cfi_def_cfa_offset 56
.Lcfi330:
	.cfi_offset %rbx, -56
.Lcfi331:
	.cfi_offset %r12, -48
.Lcfi332:
	.cfi_offset %r13, -40
.Lcfi333:
	.cfi_offset %r14, -32
.Lcfi334:
	.cfi_offset %r15, -24
.Lcfi335:
	.cfi_offset %rbp, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB33_25
# BB#1:                                 # %.lr.ph101
	movq	%rsi, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r14
	leaq	-16(%rsp), %r8
	leaq	-8(%rsp), %r9
	leaq	-32(%rsp), %r10
	leaq	-24(%rsp), %r11
	jmp	.LBB33_2
	.p2align	4, 0x90
.LBB33_12:                              # %.sink.split.i39
                                        #   in Loop: Header=BB33_2 Depth=1
	leaq	(%rdi,%rbx,8), %rbp
	movq	%rbp, (%rax)
	leaq	(%rdi,%rcx,8), %rax
	movq	%rax, (%rdx)
	movq	(%rdi,%rbx,8), %rax
	movq	%rax, (%rdi,%rcx,8)
	movq	%rbx, %rcx
.LBB33_13:                              #   in Loop: Header=BB33_2 Depth=1
	cmpq	%r12, %rcx
	jl	.LBB33_4
# BB#14:                                #   in Loop: Header=BB33_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rbx
	sarq	%rbx
	ucomisd	(%rdi,%rbx,8), %xmm0
	jbe	.LBB33_4
# BB#15:                                #   in Loop: Header=BB33_2 Depth=1
	movq	%r11, %rax
	movq	%r10, %rdx
	jmp	.LBB33_12
	.p2align	4, 0x90
.LBB33_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdi,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rdx
	addq	$2, %rdx
	cmpq	%r13, %rdx
	jge	.LBB33_3
# BB#9:                                 # %.lr.ph.i31.preheader
                                        #   in Loop: Header=BB33_2 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB33_10:                              # %.lr.ph.i31
                                        #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	leaq	(%rcx,%rdx), %rbx
	leaq	-1(%rcx,%rdx), %rcx
	movq	-8(%rdi,%rbx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r13, %rbx
	movq	%rcx, %rax
	movq	%rbx, %rdx
	jl	.LBB33_10
	jmp	.LBB33_11
	.p2align	4, 0x90
.LBB33_3:                               #   in Loop: Header=BB33_2 Depth=1
	movq	%rdx, %rbx
	movq	%r15, %rcx
.LBB33_11:                              # %._crit_edge.i34
                                        #   in Loop: Header=BB33_2 Depth=1
	cmpq	%r13, %rbx
	movq	%r14, %rbx
	movq	%r9, %rax
	movq	%r8, %rdx
	je	.LBB33_12
	jmp	.LBB33_13
	.p2align	4, 0x90
.LBB33_4:                               # %_ZN9benchmark7sift_inI14PointerWrapperI12ValueWrapperIdEES3_EEvlT_lT0_.exit43
                                        #   in Loop: Header=BB33_2 Depth=1
	movsd	%xmm0, (%rdi,%rcx,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB33_2
# BB#5:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB33_25
# BB#6:
	leaq	-16(%rsp), %r10
	leaq	-8(%rsp), %rbx
	leaq	-32(%rsp), %r8
	leaq	-24(%rsp), %r9
	.p2align	4, 0x90
.LBB33_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_17 Depth 2
	leaq	-1(%r13), %rsi
	movsd	-8(%rdi,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r13,8)
	cmpq	$3, %rsi
	jl	.LBB33_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %edx
	.p2align	4, 0x90
.LBB33_17:                              # %.lr.ph.i
                                        #   Parent Loop BB33_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	orq	%rdx, %rcx
	movq	-8(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	leaq	-1(%rcx), %rax
	addq	%rcx, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, %rdx
	jl	.LBB33_17
	jmp	.LBB33_18
	.p2align	4, 0x90
.LBB33_8:                               #   in Loop: Header=BB33_7 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
.LBB33_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB33_7 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB33_21
# BB#19:                                #   in Loop: Header=BB33_7 Depth=1
	addq	$-2, %r13
	movq	%rbx, %rcx
	movq	%r10, %rdx
.LBB33_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB33_7 Depth=1
	leaq	(%rdi,%r13,8), %rbp
	movq	%rbp, (%rcx)
	leaq	(%rdi,%rax,8), %rcx
	movq	%rcx, (%rdx)
	movq	(%rdi,%r13,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	movq	%r13, %rax
.LBB33_21:                              #   in Loop: Header=BB33_7 Depth=1
	testq	%rax, %rax
	jle	.LBB33_24
# BB#22:                                #   in Loop: Header=BB33_7 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %r13
	sarq	%r13
	ucomisd	(%rdi,%r13,8), %xmm0
	jbe	.LBB33_24
# BB#23:                                #   in Loop: Header=BB33_7 Depth=1
	movq	%r9, %rcx
	movq	%r8, %rdx
	jmp	.LBB33_20
	.p2align	4, 0x90
.LBB33_24:                              # %_ZN9benchmark7sift_inI14PointerWrapperI12ValueWrapperIdEES3_EEvlT_lT0_.exit
                                        #   in Loop: Header=BB33_7 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %rsi
	movq	%rsi, %r13
	jg	.LBB33_7
.LBB33_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_, .Lfunc_end33-_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIdEES3_EEvT_S5_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,"axG",@progbits,_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,comdat
	.weak	_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_,@function
_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_: # @_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi336:
	.cfi_def_cfa_offset 16
.Lcfi337:
	.cfi_offset %rbx, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB34_22
# BB#1:                                 # %.lr.ph52
	movq	%rsi, %r11
	sarq	$3, %r11
	movq	%r11, %r10
	shrq	$63, %r10
	addq	%r11, %r10
	sarq	%r10
	leaq	-1(%r11), %r8
	.p2align	4, 0x90
.LBB34_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_8 Depth 2
	leaq	-1(%r10), %r9
	movsd	-8(%rdi,%r10,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r10,%r10), %rcx
	addq	$2, %rcx
	cmpq	%r11, %rcx
	jge	.LBB34_3
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB34_8:                               # %.lr.ph.i
                                        #   Parent Loop BB34_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	-8(%rdi,%rcx,8), %xmm1
	seta	%al
	leaq	(%rax,%rcx), %rbx
	leaq	-1(%rax,%rcx), %rax
	movq	-8(%rdi,%rbx,8), %rcx
	movq	%rcx, (%rdi,%rdx,8)
	addq	%rbx, %rbx
	cmpq	%r11, %rbx
	movq	%rax, %rdx
	movq	%rbx, %rcx
	jl	.LBB34_8
	jmp	.LBB34_9
	.p2align	4, 0x90
.LBB34_3:                               #   in Loop: Header=BB34_2 Depth=1
	movq	%rcx, %rbx
	movq	%r9, %rax
.LBB34_9:                               # %._crit_edge.i
                                        #   in Loop: Header=BB34_2 Depth=1
	cmpq	%r11, %rbx
	movq	%r8, %rcx
	jne	.LBB34_11
.LBB34_10:                              # %.sink.split.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movq	%rcx, %rax
.LBB34_11:                              #   in Loop: Header=BB34_2 Depth=1
	cmpq	%r10, %rax
	jl	.LBB34_13
# BB#12:                                #   in Loop: Header=BB34_2 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %rcx
	sarq	%rcx
	ucomisd	(%rdi,%rcx,8), %xmm0
	ja	.LBB34_10
.LBB34_13:                              # %_ZN9benchmark7sift_inIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvlT_lT0_.exit
                                        #   in Loop: Header=BB34_2 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %r10
	movq	%r9, %r10
	jg	.LBB34_2
# BB#4:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB34_22
	.p2align	4, 0x90
.LBB34_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_15 Depth 2
	leaq	-1(%r11), %rcx
	movsd	-8(%rdi,%r11,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r11,8)
	cmpq	$3, %rcx
	jl	.LBB34_6
# BB#14:                                # %.lr.ph.i30.preheader
                                        #   in Loop: Header=BB34_5 Depth=1
	xorl	%edx, %edx
	movl	$2, %eax
	.p2align	4, 0x90
.LBB34_15:                              # %.lr.ph.i30
                                        #   Parent Loop BB34_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%esi, %esi
	ucomisd	-8(%rdi,%rax,8), %xmm1
	seta	%sil
	orq	%rax, %rsi
	movq	-8(%rdi,%rsi,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	leaq	-1(%rsi), %rdx
	addq	%rsi, %rsi
	cmpq	%rcx, %rsi
	movq	%rsi, %rax
	jl	.LBB34_15
	jmp	.LBB34_16
	.p2align	4, 0x90
.LBB34_6:                               #   in Loop: Header=BB34_5 Depth=1
	movl	$2, %esi
	xorl	%edx, %edx
.LBB34_16:                              # %._crit_edge.i33
                                        #   in Loop: Header=BB34_5 Depth=1
	cmpq	%rcx, %rsi
	jne	.LBB34_19
# BB#17:                                #   in Loop: Header=BB34_5 Depth=1
	addq	$-2, %r11
.LBB34_18:                              # %.sink.split.i36
                                        #   in Loop: Header=BB34_5 Depth=1
	movq	(%rdi,%r11,8), %rax
	movq	%rax, (%rdi,%rdx,8)
	movq	%r11, %rdx
.LBB34_19:                              #   in Loop: Header=BB34_5 Depth=1
	testq	%rdx, %rdx
	jle	.LBB34_21
# BB#20:                                #   in Loop: Header=BB34_5 Depth=1
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %r11
	sarq	%r11
	ucomisd	(%rdi,%r11,8), %xmm0
	ja	.LBB34_18
.LBB34_21:                              # %_ZN9benchmark7sift_inIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvlT_lT0_.exit40
                                        #   in Loop: Header=BB34_5 Depth=1
	movsd	%xmm0, (%rdi,%rdx,8)
	cmpq	$1, %rcx
	movq	%rcx, %r11
	jg	.LBB34_5
.LBB34_22:                              # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end34:
	.size	_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_, .Lfunc_end34-_ZN9benchmark8heapsortIP12ValueWrapperIS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IS1_IdEEEEEEEEEESB_EEvT_SD_
	.cfi_endproc

	.section	.text._ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,"axG",@progbits,_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,comdat
	.weak	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.p2align	4, 0x90
	.type	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_,@function
_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_: # @_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi338:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi339:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi340:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi341:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi342:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi343:
	.cfi_def_cfa_offset 56
.Lcfi344:
	.cfi_offset %rbx, -56
.Lcfi345:
	.cfi_offset %r12, -48
.Lcfi346:
	.cfi_offset %r13, -40
.Lcfi347:
	.cfi_offset %r14, -32
.Lcfi348:
	.cfi_offset %r15, -24
.Lcfi349:
	.cfi_offset %rbp, -16
	subq	%rdi, %rsi
	cmpq	$9, %rsi
	jl	.LBB35_25
# BB#1:                                 # %.lr.ph101
	movq	%rsi, %r13
	sarq	$3, %r13
	movq	%r13, %r12
	shrq	$63, %r12
	addq	%r13, %r12
	sarq	%r12
	leaq	-1(%r13), %r14
	leaq	-16(%rsp), %r8
	leaq	-8(%rsp), %r9
	leaq	-32(%rsp), %r10
	leaq	-24(%rsp), %r11
	jmp	.LBB35_2
	.p2align	4, 0x90
.LBB35_12:                              # %.sink.split.i39
                                        #   in Loop: Header=BB35_2 Depth=1
	leaq	(%rdi,%rbx,8), %rbp
	movq	%rbp, (%rax)
	leaq	(%rdi,%rcx,8), %rax
	movq	%rax, (%rdx)
	movq	(%rdi,%rbx,8), %rax
	movq	%rax, (%rdi,%rcx,8)
	movq	%rbx, %rcx
.LBB35_13:                              #   in Loop: Header=BB35_2 Depth=1
	cmpq	%r12, %rcx
	jl	.LBB35_4
# BB#14:                                #   in Loop: Header=BB35_2 Depth=1
	leaq	-1(%rcx), %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rbx
	sarq	%rbx
	ucomisd	(%rdi,%rbx,8), %xmm0
	jbe	.LBB35_4
# BB#15:                                #   in Loop: Header=BB35_2 Depth=1
	movq	%r11, %rax
	movq	%r10, %rdx
	jmp	.LBB35_12
	.p2align	4, 0x90
.LBB35_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_10 Depth 2
	leaq	-1(%r12), %r15
	movsd	-8(%rdi,%r12,8), %xmm0  # xmm0 = mem[0],zero
	leaq	-2(%r12,%r12), %rdx
	addq	$2, %rdx
	cmpq	%r13, %rdx
	jge	.LBB35_3
# BB#9:                                 # %.lr.ph.i31.preheader
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB35_10:                              # %.lr.ph.i31
                                        #   Parent Loop BB35_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	leaq	(%rcx,%rdx), %rbx
	leaq	-1(%rcx,%rdx), %rcx
	movq	-8(%rdi,%rbx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	%rbx, %rbx
	cmpq	%r13, %rbx
	movq	%rcx, %rax
	movq	%rbx, %rdx
	jl	.LBB35_10
	jmp	.LBB35_11
	.p2align	4, 0x90
.LBB35_3:                               #   in Loop: Header=BB35_2 Depth=1
	movq	%rdx, %rbx
	movq	%r15, %rcx
.LBB35_11:                              # %._crit_edge.i34
                                        #   in Loop: Header=BB35_2 Depth=1
	cmpq	%r13, %rbx
	movq	%r14, %rbx
	movq	%r9, %rax
	movq	%r8, %rdx
	je	.LBB35_12
	jmp	.LBB35_13
	.p2align	4, 0x90
.LBB35_4:                               # %_ZN9benchmark7sift_inI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvlT_lT0_.exit43
                                        #   in Loop: Header=BB35_2 Depth=1
	movsd	%xmm0, (%rdi,%rcx,8)
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB35_2
# BB#5:                                 # %.preheader
	cmpq	$9, %rsi
	jl	.LBB35_25
# BB#6:
	leaq	-16(%rsp), %r10
	leaq	-8(%rsp), %rbx
	leaq	-32(%rsp), %r8
	leaq	-24(%rsp), %r9
	.p2align	4, 0x90
.LBB35_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_17 Depth 2
	leaq	-1(%r13), %rsi
	movsd	-8(%rdi,%r13,8), %xmm0  # xmm0 = mem[0],zero
	movq	(%rdi), %rax
	movq	%rax, -8(%rdi,%r13,8)
	cmpq	$3, %rsi
	jl	.LBB35_8
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB35_7 Depth=1
	xorl	%eax, %eax
	movl	$2, %edx
	.p2align	4, 0x90
.LBB35_17:                              # %.lr.ph.i
                                        #   Parent Loop BB35_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	-8(%rdi,%rdx,8), %xmm1
	seta	%cl
	orq	%rdx, %rcx
	movq	-8(%rdi,%rcx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	leaq	-1(%rcx), %rax
	addq	%rcx, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, %rdx
	jl	.LBB35_17
	jmp	.LBB35_18
	.p2align	4, 0x90
.LBB35_8:                               #   in Loop: Header=BB35_7 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
.LBB35_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB35_7 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB35_21
# BB#19:                                #   in Loop: Header=BB35_7 Depth=1
	addq	$-2, %r13
	movq	%rbx, %rcx
	movq	%r10, %rdx
.LBB35_20:                              # %.sink.split.i
                                        #   in Loop: Header=BB35_7 Depth=1
	leaq	(%rdi,%r13,8), %rbp
	movq	%rbp, (%rcx)
	leaq	(%rdi,%rax,8), %rcx
	movq	%rcx, (%rdx)
	movq	(%rdi,%r13,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	movq	%r13, %rax
.LBB35_21:                              #   in Loop: Header=BB35_7 Depth=1
	testq	%rax, %rax
	jle	.LBB35_24
# BB#22:                                #   in Loop: Header=BB35_7 Depth=1
	leaq	-1(%rax), %rcx
	shrq	$63, %rcx
	leaq	-1(%rax,%rcx), %r13
	sarq	%r13
	ucomisd	(%rdi,%r13,8), %xmm0
	jbe	.LBB35_24
# BB#23:                                #   in Loop: Header=BB35_7 Depth=1
	movq	%r9, %rcx
	movq	%r8, %rdx
	jmp	.LBB35_20
	.p2align	4, 0x90
.LBB35_24:                              # %_ZN9benchmark7sift_inI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvlT_lT0_.exit
                                        #   in Loop: Header=BB35_7 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	cmpq	$1, %rsi
	movq	%rsi, %r13
	jg	.LBB35_7
.LBB35_25:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_, .Lfunc_end35-_ZN9benchmark8heapsortI14PointerWrapperI12ValueWrapperIS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IS2_IdEEEEEEEEEEESC_EEvT_SE_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_stepanov_abstraction.ii,@function
_GLOBAL__sub_I_stepanov_abstraction.ii: # @_GLOBAL__sub_I_stepanov_abstraction.ii
	.cfi_startproc
# BB#0:
	movq	dpb(%rip), %rax
	movq	%rax, dPb(%rip)
	movq	dpe(%rip), %rax
	movq	%rax, dPe(%rip)
	movq	dMpb(%rip), %rax
	movq	%rax, dMPb(%rip)
	movq	dMpe(%rip), %rax
	movq	%rax, dMPe(%rip)
	movq	DVpb(%rip), %rax
	movq	%rax, DVPb(%rip)
	movq	DVpe(%rip), %rax
	movq	%rax, DVPe(%rip)
	movq	DVMpb(%rip), %rax
	movq	%rax, DVMPb(%rip)
	movq	DVMpe(%rip), %rax
	movq	%rax, DVMPe(%rip)
	movq	DV10pb(%rip), %rax
	movq	%rax, DV10Pb(%rip)
	movq	DV10pe(%rip), %rax
	movq	%rax, DV10Pe(%rip)
	movq	DV10Mpb(%rip), %rax
	movq	%rax, DV10MPb(%rip)
	movq	DV10Mpe(%rip), %rax
	movq	%rax, DV10MPe(%rip)
	retq
.Lfunc_end36:
	.size	_GLOBAL__sub_I_stepanov_abstraction.ii, .Lfunc_end36-_GLOBAL__sub_I_stepanov_abstraction.ii
	.cfi_endproc

	.type	results,@object         # @results
	.bss
	.globl	results
	.p2align	3
results:
	.quad	0
	.size	results, 8

	.type	current_test,@object    # @current_test
	.globl	current_test
	.p2align	2
current_test:
	.long	0                       # 0x0
	.size	current_test, 4

	.type	allocated_results,@object # @allocated_results
	.globl	allocated_results
	.p2align	2
allocated_results:
	.long	0                       # 0x0
	.size	allocated_results, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Could not allocate %d results\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\ntest %*s description   absolute   operations   ratio with\n"
	.size	.L.str.1, 60

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"number %*s time       per second   test0\n\n"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%2i %*s\"%s\"  %5.2f sec   %5.2f M     %.2f\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.zero	1
	.size	.L.str.5, 1

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nTotal absolute time for %s: %.2f sec\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n%s Penalty: %.2f\n\n"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\ntest %*s description   absolute\n"
	.size	.L.str.8, 34

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"number %*s time\n\n"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%2i %*s\"%s\"  %5.2f sec\n"
	.size	.L.str.10, 24

	.type	start_time,@object      # @start_time
	.bss
	.globl	start_time
	.p2align	3
start_time:
	.quad	0                       # 0x0
	.size	start_time, 8

	.type	end_time,@object        # @end_time
	.globl	end_time
	.p2align	3
end_time:
	.quad	0                       # 0x0
	.size	end_time, 8

	.type	iterations,@object      # @iterations
	.data
	.globl	iterations
	.p2align	2
iterations:
	.long	200000                  # 0x30d40
	.size	iterations, 4

	.type	init_value,@object      # @init_value
	.globl	init_value
	.p2align	3
init_value:
	.quad	4613937818241073152     # double 3
	.size	init_value, 8

	.type	data,@object            # @data
	.bss
	.globl	data
	.p2align	4
data:
	.zero	16000
	.size	data, 16000

	.type	VData,@object           # @VData
	.globl	VData
	.p2align	4
VData:
	.zero	16000
	.size	VData, 16000

	.type	V10Data,@object         # @V10Data
	.globl	V10Data
	.p2align	4
V10Data:
	.zero	16000
	.size	V10Data, 16000

	.type	dataMaster,@object      # @dataMaster
	.globl	dataMaster
	.p2align	4
dataMaster:
	.zero	16000
	.size	dataMaster, 16000

	.type	VDataMaster,@object     # @VDataMaster
	.globl	VDataMaster
	.p2align	4
VDataMaster:
	.zero	16000
	.size	VDataMaster, 16000

	.type	V10DataMaster,@object   # @V10DataMaster
	.globl	V10DataMaster
	.p2align	4
V10DataMaster:
	.zero	16000
	.size	V10DataMaster, 16000

	.type	dpb,@object             # @dpb
	.data
	.globl	dpb
	.p2align	3
dpb:
	.quad	data
	.size	dpb, 8

	.type	dpe,@object             # @dpe
	.globl	dpe
	.p2align	3
dpe:
	.quad	data+16000
	.size	dpe, 8

	.type	dMpb,@object            # @dMpb
	.globl	dMpb
	.p2align	3
dMpb:
	.quad	dataMaster
	.size	dMpb, 8

	.type	dMpe,@object            # @dMpe
	.globl	dMpe
	.p2align	3
dMpe:
	.quad	dataMaster+16000
	.size	dMpe, 8

	.type	DVpb,@object            # @DVpb
	.globl	DVpb
	.p2align	3
DVpb:
	.quad	VData
	.size	DVpb, 8

	.type	DVpe,@object            # @DVpe
	.globl	DVpe
	.p2align	3
DVpe:
	.quad	VData+16000
	.size	DVpe, 8

	.type	DVMpb,@object           # @DVMpb
	.globl	DVMpb
	.p2align	3
DVMpb:
	.quad	VDataMaster
	.size	DVMpb, 8

	.type	DVMpe,@object           # @DVMpe
	.globl	DVMpe
	.p2align	3
DVMpe:
	.quad	VDataMaster+16000
	.size	DVMpe, 8

	.type	DV10pb,@object          # @DV10pb
	.globl	DV10pb
	.p2align	3
DV10pb:
	.quad	V10Data
	.size	DV10pb, 8

	.type	DV10pe,@object          # @DV10pe
	.globl	DV10pe
	.p2align	3
DV10pe:
	.quad	V10Data+16000
	.size	DV10pe, 8

	.type	DV10Mpb,@object         # @DV10Mpb
	.globl	DV10Mpb
	.p2align	3
DV10Mpb:
	.quad	V10DataMaster
	.size	DV10Mpb, 8

	.type	DV10Mpe,@object         # @DV10Mpe
	.globl	DV10Mpe
	.p2align	3
DV10Mpe:
	.quad	V10DataMaster+16000
	.size	DV10Mpe, 8

	.type	dPb,@object             # @dPb
	.bss
	.globl	dPb
	.p2align	3
dPb:
	.zero	8
	.size	dPb, 8

	.type	dPe,@object             # @dPe
	.globl	dPe
	.p2align	3
dPe:
	.zero	8
	.size	dPe, 8

	.type	dMPb,@object            # @dMPb
	.globl	dMPb
	.p2align	3
dMPb:
	.zero	8
	.size	dMPb, 8

	.type	dMPe,@object            # @dMPe
	.globl	dMPe
	.p2align	3
dMPe:
	.zero	8
	.size	dMPe, 8

	.type	DVPb,@object            # @DVPb
	.globl	DVPb
	.p2align	3
DVPb:
	.zero	8
	.size	DVPb, 8

	.type	DVPe,@object            # @DVPe
	.globl	DVPe
	.p2align	3
DVPe:
	.zero	8
	.size	DVPe, 8

	.type	DVMPb,@object           # @DVMPb
	.globl	DVMPb
	.p2align	3
DVMPb:
	.zero	8
	.size	DVMPb, 8

	.type	DVMPe,@object           # @DVMPe
	.globl	DVMPe
	.p2align	3
DVMPe:
	.zero	8
	.size	DVMPe, 8

	.type	DV10Pb,@object          # @DV10Pb
	.globl	DV10Pb
	.p2align	3
DV10Pb:
	.zero	8
	.size	DV10Pb, 8

	.type	DV10Pe,@object          # @DV10Pe
	.globl	DV10Pe
	.p2align	3
DV10Pe:
	.zero	8
	.size	DV10Pe, 8

	.type	DV10MPb,@object         # @DV10MPb
	.globl	DV10MPb
	.p2align	3
DV10MPb:
	.zero	8
	.size	DV10MPb, 8

	.type	DV10MPe,@object         # @DV10MPe
	.globl	DV10MPe
	.p2align	3
DV10MPe:
	.zero	8
	.size	DV10MPe, 8

	.type	.L.str.32,@object       # @.str.32
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.32:
	.asciz	"insertion_sort double pointer"
	.size	.L.str.32, 30

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"insertion_sort double pointer_class"
	.size	.L.str.33, 36

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"insertion_sort DoubleValueWrapper pointer"
	.size	.L.str.34, 42

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"insertion_sort DoubleValueWrapper pointer_class"
	.size	.L.str.35, 48

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"insertion_sort DoubleValueWrapper10 pointer"
	.size	.L.str.36, 44

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"insertion_sort DoubleValueWrapper10 pointer_class"
	.size	.L.str.37, 50

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"quicksort double pointer"
	.size	.L.str.38, 25

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"quicksort double pointer_class"
	.size	.L.str.39, 31

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"quicksort DoubleValueWrapper pointer"
	.size	.L.str.40, 37

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"quicksort DoubleValueWrapper pointer_class"
	.size	.L.str.41, 43

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"quicksort DoubleValueWrapper10 pointer"
	.size	.L.str.42, 39

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"quicksort DoubleValueWrapper10 pointer_class"
	.size	.L.str.43, 45

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"heap_sort double pointer"
	.size	.L.str.44, 25

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"heap_sort double pointer_class"
	.size	.L.str.45, 31

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"heap_sort DoubleValueWrapper pointer"
	.size	.L.str.46, 37

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"heap_sort DoubleValueWrapper pointer_class"
	.size	.L.str.47, 43

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"heap_sort DoubleValueWrapper10 pointer"
	.size	.L.str.48, 39

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"heap_sort DoubleValueWrapper10 pointer_class"
	.size	.L.str.49, 45

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"test %i failed\n"
	.size	.L.str.50, 16

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"sort test %i failed\n"
	.size	.L.str.51, 21

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_stepanov_abstraction.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
