	.text
	.file	"libclamav_regex_regcomp.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	cli_regcomp
	.p2align	4, 0x90
	.type	cli_regcomp,@function
cli_regcomp:                            # @cli_regcomp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi6:
	.cfi_def_cfa_offset 320
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movl	%ebp, %r12d
	andl	$1, %r12d
	sete	%al
	movl	%ebp, %r14d
	andl	$16, %r14d
	movl	$16, %r13d
	je	.LBB0_2
# BB#1:
	testb	%al, %al
	je	.LBB0_96
.LBB0_2:
	testb	$32, %bpl
	jne	.LBB0_3
# BB#5:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_6
.LBB0_3:
	movq	16(%rdi), %rax
	subq	%rbx, %rax
	jb	.LBB0_96
# BB#4:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
.LBB0_6:
	movl	$399, %edi              # imm = 0x18F
	callq	cli_malloc
	movq	%rax, %r15
	movl	$12, %r13d
	testq	%r15, %r15
	je	.LBB0_96
# BB#7:
	movq	8(%rsp), %rax           # 8-byte Reload
	shrq	%rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	1(%rax,%rax,2), %rdi
	movq	%rdi, 64(%rsp)
	movl	$8, %esi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	cli_calloc
	movq	%rax, 56(%rsp)
	movq	$0, 72(%rsp)
	testq	%rax, %rax
	je	.LBB0_8
# BB#9:
	andl	$-129, %ebp
	movq	%r15, 88(%rsp)
	movq	%rbx, 32(%rsp)
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	%rbx, %rdx
	movq	%rdx, 40(%rsp)
	movl	$0, 48(%rsp)
	movl	$0, 80(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movl	$256, 16(%r15)          # imm = 0x100
	movdqu	%xmm0, 20(%r15)
	movl	$0, 36(%r15)
	movl	%ebp, 40(%r15)
	movq	$0, 96(%r15)
	movl	$0, 104(%r15)
	movq	$0, 112(%r15)
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [0,0,0,1]
	movups	%xmm1, 72(%r15)
	leaq	264(%r15), %rcx
	movq	%rcx, 88(%r15)
	movdqu	%xmm0, 376(%r15)
	movdqu	%xmm0, 360(%r15)
	movdqu	%xmm0, 344(%r15)
	movdqu	%xmm0, 328(%r15)
	movdqu	%xmm0, 312(%r15)
	movdqu	%xmm0, 296(%r15)
	movdqu	%xmm0, 280(%r15)
	movdqu	%xmm0, 264(%r15)
	movdqu	%xmm0, 248(%r15)
	movdqu	%xmm0, 232(%r15)
	movdqu	%xmm0, 216(%r15)
	movdqu	%xmm0, 200(%r15)
	movdqu	%xmm0, 184(%r15)
	movdqu	%xmm0, 168(%r15)
	movdqu	%xmm0, 152(%r15)
	movdqu	%xmm0, 136(%r15)
	movl	$0, 120(%r15)
	xorl	%r13d, %r13d
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	jg	.LBB0_14
# BB#10:
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rcx,2), %rsi
	leaq	2(%rsi), %rcx
	shrq	$63, %rcx
	leaq	2(%rsi,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbp
	cmpq	%rbp, %rdi
	jge	.LBB0_14
# BB#11:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	movq	%rax, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB0_12
# BB#13:
	movq	%rax, 56(%rsp)
	movq	%rbp, 64(%rsp)
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB0_14
.LBB0_8:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB0_96
.LBB0_12:                               # %seterr.exit.i.i
	movl	$12, 48(%rsp)
	movl	$nuls, %edx
	movd	%rdx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)
	movl	$12, %r13d
	movl	$nuls, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB0_14:                               # %doemit.exit
	movq	$1, 72(%rsp)
	movq	$134217728, (%rax)      # imm = 0x8000000
	movq	72(%rsp), %rax
	decq	%rax
	movq	%rax, 56(%r15)
	testl	%r12d, %r12d
	je	.LBB0_16
# BB#15:
	leaq	32(%rsp), %rdi
	movl	$128, %esi
	callq	p_ere
	jmp	.LBB0_24
.LBB0_16:
	testl	%r14d, %r14d
	jne	.LBB0_17
# BB#23:
	leaq	32(%rsp), %rdi
	movl	$128, %esi
	movl	$128, %edx
	callq	p_bre
	jmp	.LBB0_24
.LBB0_17:
	cmpq	%rdx, %rbx
	jae	.LBB0_20
# BB#18:
	leaq	32(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rbx), %rax
	movq	%rax, 32(%rsp)
	movsbl	(%rbx), %esi
	movq	%rbp, %rdi
	callq	ordinary
	movq	32(%rsp), %rbx
	cmpq	40(%rsp), %rbx
	jb	.LBB0_19
	jmp	.LBB0_24
.LBB0_20:
	testl	%r13d, %r13d
	jne	.LBB0_22
# BB#21:
	movl	$14, 48(%rsp)
.LBB0_22:                               # %.preheader.thread.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)
.LBB0_24:                               # %p_str.exit
	cmpl	$0, 48(%rsp)
	movq	72(%rsp), %rsi
	je	.LBB0_30
# BB#25:                                # %doemit.exit92.thread
	leaq	-1(%rsi), %rax
	movq	%rax, 64(%r15)
	jmp	.LBB0_26
.LBB0_30:
	movq	64(%rsp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	jl	.LBB0_37
# BB#31:
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB0_37
# BB#32:
	movq	56(%rsp), %rdi
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB0_33
# BB#36:
	movq	%rax, 56(%rsp)
	movq	%rbx, 64(%rsp)
	movl	48(%rsp), %eax
	jmp	.LBB0_37
.LBB0_33:
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jne	.LBB0_35
# BB#34:
	movl	$12, 48(%rsp)
	movl	$12, %eax
.LBB0_35:                               # %seterr.exit.i.i90
	movl	$nuls, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)
.LBB0_37:                               # %doemit.exit92
	movq	56(%rsp), %rcx
	movq	72(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 72(%rsp)
	movq	$134217728, (%rcx,%rdx,8) # imm = 0x8000000
	movq	72(%rsp), %rsi
	leaq	-1(%rsi), %rcx
	movq	%rcx, 64(%r15)
	testl	%eax, %eax
	jne	.LBB0_26
# BB#38:                                # %.preheader.i.preheader
	movq	%r15, %r8
	addq	$20, %r8
	movq	88(%r15), %r10
	movq	$-127, %r11
	movq	$-128, %r14
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_39 Depth=1
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.LBB0_48
# BB#41:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_39 Depth=1
	leal	7(%rax), %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	leal	7(%rax,%rcx), %eax
	sarl	$3, %eax
	movzbl	%r14b, %edi
	movq	32(%r15), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_42:                               #   Parent Loop BB0_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rcx,%rdi)
	jne	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_42 Depth=2
	incl	%edx
	movslq	16(%r15), %rbp
	addq	%rbp, %rcx
	cmpl	%eax, %edx
	jl	.LBB0_42
	jmp	.LBB0_48
.LBB0_44:                               # %isinsets.exit.i
                                        #   in Loop: Header=BB0_39 Depth=1
	movl	84(%r15), %r9d
	leal	1(%r9), %eax
	movl	%eax, 84(%r15)
	movb	%r9b, (%r10,%r14)
	leaq	1(%r14), %rax
	cmpq	$127, %rax
	jg	.LBB0_48
# BB#45:                                # %.lr.ph.i93.preheader
                                        #   in Loop: Header=BB0_39 Depth=1
	movq	%r11, %rdx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph.i93
                                        #   Parent Loop BB0_39 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_52 Depth 3
	cmpb	$0, (%r10,%rdx)
	jne	.LBB0_47
# BB#50:                                #   in Loop: Header=BB0_46 Depth=2
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.LBB0_54
# BB#51:                                # %.lr.ph.i28.i
                                        #   in Loop: Header=BB0_46 Depth=2
	leal	7(%rax), %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	leal	7(%rax,%rcx), %esi
	sarl	$3, %esi
	movq	32(%r15), %rbp
	movzbl	%dl, %ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_52:                               #   Parent Loop BB0_39 Depth=1
                                        #     Parent Loop BB0_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp,%rdi), %eax
	cmpb	(%rbp,%rcx), %al
	jne	.LBB0_47
# BB#53:                                #   in Loop: Header=BB0_52 Depth=3
	incl	%ebx
	movslq	16(%r15), %rax
	addq	%rax, %rbp
	cmpl	%esi, %ebx
	jl	.LBB0_52
.LBB0_54:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_46 Depth=2
	movb	%r9b, (%r10,%rdx)
.LBB0_47:                               # %samesets.exit.backedge.i
                                        #   in Loop: Header=BB0_46 Depth=2
	incq	%rdx
	cmpq	$128, %rdx
	jne	.LBB0_46
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_39:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
                                        #     Child Loop BB0_46 Depth 2
                                        #       Child Loop BB0_52 Depth 3
	cmpb	$0, (%r10,%r14)
	je	.LBB0_40
.LBB0_48:                               # %isinsets.exit.thread.i
                                        #   in Loop: Header=BB0_39 Depth=1
	incq	%r14
	incq	%r11
	cmpq	$128, %r14
	jne	.LBB0_39
# BB#49:                                # %categorize.exit.loopexit
	movq	72(%rsp), %rsi
.LBB0_26:                               # %categorize.exit
	movq	%rsi, 48(%r15)
	movq	56(%rsp), %rdi
	shlq	$3, %rsi
	callq	cli_realloc
	movq	%rax, 8(%r15)
	movl	48(%rsp), %ecx
	testq	%rax, %rax
	je	.LBB0_27
# BB#55:                                # %stripsnug.exit
	testl	%ecx, %ecx
	movq	24(%rsp), %r10          # 8-byte Reload
	je	.LBB0_57
# BB#56:
	xorl	%eax, %eax
	jmp	.LBB0_89
.LBB0_27:
	testl	%ecx, %ecx
	movq	24(%rsp), %r10          # 8-byte Reload
	jne	.LBB0_29
# BB#28:
	movl	$12, 48(%rsp)
	movl	$12, %ecx
.LBB0_29:                               # %stripsnug.exit.thread
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)
	movq	56(%rsp), %rax
	movq	%rax, 8(%r15)
	xorl	%eax, %eax
	jmp	.LBB0_89
.LBB0_57:
	addq	$8, %rax
	xorl	%edi, %edi
	movl	$6272, %ecx             # imm = 0x1880
	movl	$8704, %r8d             # imm = 0x2200
                                        # implicit-def: %RBX
                                        # implicit-def: %R9
	.p2align	4, 0x90
.LBB0_58:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_62 Depth 2
	leaq	8(%rax), %rdx
	movq	(%rax), %rbp
	movl	%ebp, %esi
	andl	$-134217728, %esi       # imm = 0xF8000000
	addq	$-268435456, %rsi       # imm = 0xF0000000
	shrq	$27, %rsi
	cmpq	$13, %rsi
	ja	.LBB0_68
# BB#59:                                #   in Loop: Header=BB0_58 Depth=1
	btq	%rsi, %rcx
	jb	.LBB0_71
# BB#60:                                #   in Loop: Header=BB0_58 Depth=1
	btq	%rsi, %r8
	jae	.LBB0_66
# BB#61:                                #   in Loop: Header=BB0_58 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_62:                               # %.preheader3.i
                                        #   Parent Loop BB0_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	andl	$134217727, %ebp        # imm = 0x7FFFFFF
	leaq	(%rax,%rbp,8), %rdx
	movq	(%rax,%rbp,8), %rbp
	movl	%ebp, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	cmpq	$1610612736, %rax       # imm = 0x60000000
	je	.LBB0_68
# BB#63:                                # %switch.early.test.i
                                        #   in Loop: Header=BB0_62 Depth=2
	cmpl	$-2013265920, %eax      # imm = 0x88000000
	je	.LBB0_62
# BB#64:                                # %switch.early.test.i
                                        #   in Loop: Header=BB0_58 Depth=1
	cmpl	$-1879048192, %eax      # imm = 0x90000000
	jne	.LBB0_65
	jmp	.LBB0_68
.LBB0_66:                               #   in Loop: Header=BB0_58 Depth=1
	testq	%rsi, %rsi
	jne	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_58 Depth=1
	testq	%rdi, %rdi
	cmoveq	%rax, %r9
	incq	%rdi
	jmp	.LBB0_71
.LBB0_68:                               # %.critedge.i
                                        #   in Loop: Header=BB0_58 Depth=1
	movslq	104(%r15), %rax
	cmpq	%rax, %rdi
	jle	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_58 Depth=1
	movl	%edi, 104(%r15)
	movq	%r9, %rbx
.LBB0_70:                               #   in Loop: Header=BB0_58 Depth=1
	xorl	%edi, %edi
.LBB0_71:                               #   in Loop: Header=BB0_58 Depth=1
	andl	$-134217728, %ebp       # imm = 0xF8000000
	cmpq	$134217728, %rbp        # imm = 0x8000000
	movq	%rdx, %rax
	jne	.LBB0_58
# BB#72:
	movslq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_79
# BB#73:
	movq	%r10, %rbp
	incq	%rdi
	callq	cli_malloc
	movq	%rax, 96(%r15)
	testq	%rax, %rax
	je	.LBB0_74
# BB#75:
	movslq	104(%r15), %rcx
	movq	%rbp, %r10
	testq	%rcx, %rcx
	jle	.LBB0_78
	.p2align	4, 0x90
.LBB0_76:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	addq	$8, %rbx
	movl	%edx, %esi
	andl	$-134217728, %esi       # imm = 0xF8000000
	cmpq	$268435456, %rsi        # imm = 0x10000000
	jne	.LBB0_76
# BB#77:                                #   in Loop: Header=BB0_76 Depth=1
	movb	%dl, (%rax)
	incq	%rax
	cmpq	$1, %rcx
	leaq	-1(%rcx), %rcx
	jg	.LBB0_76
.LBB0_78:                               # %._crit_edge.i
	movb	$0, (%rax)
	jmp	.LBB0_79
.LBB0_65:
	orb	$4, 72(%r15)
.LBB0_79:                               # %findmust.exit
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB0_81
# BB#80:
	xorl	%eax, %eax
	jmp	.LBB0_89
.LBB0_81:
	movq	8(%r15), %rcx
	addq	$8, %rcx
	xorl	%edx, %edx
	movl	$4160749568, %esi       # imm = 0xF8000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_82:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdi
	andq	%rsi, %rdi
	cmpl	$1342177280, %edi       # imm = 0x50000000
	je	.LBB0_85
# BB#83:                                #   in Loop: Header=BB0_82 Depth=1
	cmpl	$1207959552, %edi       # imm = 0x48000000
	jne	.LBB0_86
# BB#84:                                #   in Loop: Header=BB0_82 Depth=1
	incq	%rdx
	jmp	.LBB0_86
.LBB0_85:                               #   in Loop: Header=BB0_82 Depth=1
	cmpq	%rax, %rdx
	cmovgeq	%rdx, %rax
	decq	%rdx
.LBB0_86:                               #   in Loop: Header=BB0_82 Depth=1
	addq	$8, %rcx
	cmpq	$134217728, %rdi        # imm = 0x8000000
	jne	.LBB0_82
# BB#87:
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB0_89
# BB#88:
	orb	$4, 72(%r15)
.LBB0_89:                               # %pluscount.exit
	movq	%rax, 128(%r15)
	movl	$53829, (%r15)          # imm = 0xD245
	movq	112(%r15), %rax
	movq	%rax, 8(%r10)
	movq	%r15, 24(%r10)
	movl	$62053, (%r10)          # imm = 0xF265
	testb	$4, 72(%r15)
	jne	.LBB0_90
# BB#94:
	testl	%ecx, %ecx
	jne	.LBB0_93
# BB#95:
	xorl	%r13d, %r13d
	jmp	.LBB0_96
.LBB0_90:
	testl	%ecx, %ecx
	jne	.LBB0_92
# BB#91:
	movl	$15, 48(%rsp)
.LBB0_92:                               # %.thread
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)
.LBB0_93:
	movq	%r10, %rdi
	callq	cli_regfree
	movl	48(%rsp), %r13d
.LBB0_96:
	movl	%r13d, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_74:
	movl	$0, 104(%r15)
	movq	%rbp, %r10
	jmp	.LBB0_79
.Lfunc_end0:
	.size	cli_regcomp, .Lfunc_end0-cli_regcomp
	.cfi_endproc

	.p2align	4, 0x90
	.type	doemit,@function
doemit:                                 # @doemit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cmpl	$0, 16(%rbx)
	jne	.LBB1_9
# BB#1:
	movq	32(%rbx), %rax
	cmpq	%rax, 40(%rbx)
	jl	.LBB1_8
# BB#2:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %r12
	cmpq	%r12, %rax
	jge	.LBB1_8
# BB#3:
	movq	24(%rbx), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB1_4
# BB#7:
	movq	%rax, 24(%rbx)
	movq	%r12, 32(%rbx)
	jmp	.LBB1_8
.LBB1_4:
	cmpl	$0, 16(%rbx)
	jne	.LBB1_6
# BB#5:
	movl	$12, 16(%rbx)
.LBB1_6:                                # %seterr.exit.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rbx)
.LBB1_8:                                # %enlarge.exit
	orq	%r15, %r14
	movq	24(%rbx), %rax
	movq	40(%rbx), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r14, (%rax,%rcx,8)
.LBB1_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	doemit, .Lfunc_end1-doemit
	.cfi_endproc

	.p2align	4, 0x90
	.type	p_ere,@function
p_ere:                                  # @p_ere
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 128
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	movq	40(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %r14
	movq	8(%r12), %r13
	cmpq	%r13, %r14
	jae	.LBB2_1
# BB#2:                                 # %.lr.ph68.preheader.preheader
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r15d, (%rsp)           # 4-byte Spill
	jmp	.LBB2_3
.LBB2_227:                              #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %r14
	jae	.LBB2_228
# BB#237:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$124, (%r14)
	jne	.LBB2_228
# BB#238:                               #   in Loop: Header=BB2_3 Depth=1
	incq	%r14
	movq	%r14, (%r12)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB2_239
# BB#240:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$1, %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	subq	%rbx, %rdx
	addq	%rbp, %rdx
	movl	$2013265920, %esi       # imm = 0x78000000
	movq	%r12, %rdi
	movq	%rbx, %rcx
	callq	doinsert
	movq	40(%r12), %rbp
	cmpl	$0, 16(%r12)
	jne	.LBB2_251
	jmp	.LBB2_242
.LBB2_239:                              #   in Loop: Header=BB2_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 16(%r12)
	je	.LBB2_242
.LBB2_251:                              # %dofwd.exit53
                                        #   in Loop: Header=BB2_3 Depth=1
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB2_250
.LBB2_242:                              #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %r14
	subq	%rbx, %r14
	movq	32(%r12), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rbp
	jl	.LBB2_249
# BB#243:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB2_249
# BB#244:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rdi
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_245
# BB#248:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	movl	16(%r12), %eax
	jmp	.LBB2_249
.LBB2_219:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_221
# BB#220:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$13, 16(%r12)
.LBB2_221:                              # %p_ere_exp.exit.backedge.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movl	$nuls, %r13d
	movl	$nuls, %r14d
	jmp	.LBB2_222
.LBB2_245:                              #   in Loop: Header=BB2_3 Depth=1
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.LBB2_247
# BB#246:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
	movl	$12, %eax
.LBB2_247:                              # %seterr.exit.i.i50
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_249:                              # %doemit.exit52
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$2281701376, %ecx       # imm = 0x88000000
	leaq	-134217728(%rcx), %rcx
	orq	%rcx, %r14
	movq	24(%r12), %rcx
	movq	40(%r12), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%r12)
	movq	%r14, (%rcx,%rdx,8)
	movq	40(%r12), %rbp
	leaq	-1(%rbp), %rdx
	testl	%eax, %eax
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jne	.LBB2_250
# BB#252:                               #   in Loop: Header=BB2_3 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%rdx, %rbp
	movq	(%rcx,%rdx,8), %rax
	movl	$4160749568, %esi       # imm = 0xF8000000
	andq	%rsi, %rax
	orq	%rbp, %rax
	movq	%rax, (%rcx,%rdx,8)
	movq	32(%r12), %rax
	movq	40(%r12), %rbp
	cmpq	%rax, %rbp
	jl	.LBB2_259
# BB#253:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_259
# BB#254:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_255
# BB#258:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_259
.LBB2_185:                              #   in Loop: Header=BB2_3 Depth=1
	xorl	%ecx, %ecx
	cmpq	%r13, %r15
	jae	.LBB2_194
# BB#186:                               # %.lr.ph.i190.i.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%ecx, %ecx
	testb	$8, %dh
	movl	$0, %edx
	je	.LBB2_192
# BB#187:                               # %.lr.ph116.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%ecx, %ecx
	movl	$2, %esi
	.p2align	4, 0x90
.LBB2_188:                              # %.lr.ph116
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	leaq	(%r14,%rdx), %rsi
	leal	(%rcx,%rcx,4), %ecx
	movq	%rsi, (%r12)
	movsbl	-1(%r14,%rdx), %edi
	leal	-48(%rdi,%rcx,2), %ecx
	cmpq	%r13, %rsi
	jae	.LBB2_191
# BB#189:                               # %..lr.ph.i190.i_crit_edge
                                        #   in Loop: Header=BB2_188 Depth=2
	cmpl	$255, %ecx
	jg	.LBB2_191
# BB#190:                               # %..lr.ph.i190.i_crit_edge
                                        #   in Loop: Header=BB2_188 Depth=2
	movq	(%rax), %rdi
	movzbl	(%rsi), %esi
	movzwl	(%rdi,%rsi,2), %edi
	andl	$2048, %edi             # imm = 0x800
	leaq	1(%rdx), %rsi
	testw	%di, %di
	jne	.LBB2_188
.LBB2_191:                              # %.critedge.i197.i.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	decl	%edx
.LBB2_192:                              # %.critedge.i197.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$255, %ecx
	jg	.LBB2_194
# BB#193:                               # %.critedge.i197.i
                                        #   in Loop: Header=BB2_3 Depth=1
	testl	%edx, %edx
	jg	.LBB2_197
.LBB2_194:                              # %.critedge.thread.i199.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_196
# BB#195:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$10, 16(%r12)
.LBB2_196:                              # %seterr.exit.i200.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_197:                              # %p_count.exit202.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	%ecx, %ebp
	movl	(%rsp), %r15d           # 4-byte Reload
	movq	%rbx, %r8
	jle	.LBB2_201
# BB#198:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_200
# BB#199:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$10, 16(%r12)
.LBB2_200:                              # %seterr.exit203.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	jmp	.LBB2_201
.LBB2_75:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_77:                               # %seterr.exit.i.i165.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_79:                               # %enlarge.exit.i166.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r14, %r8
.LBB2_80:                               # %enlarge.exit.i166.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	$671088640, (%rax,%rcx,8) # imm = 0x28000000
	jmp	.LBB2_96
.LBB2_167:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_169
# BB#168:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_169:                              # %doemit.exit185.i.thread91
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
	jmp	.LBB2_213
.LBB2_255:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_257
# BB#256:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_257:                              # %seterr.exit.i.i54
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_259:                              # %enlarge.exit.i55
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
.LBB2_250:                              # %doemit.exit56.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %r14
	movq	8(%r12), %r13
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	cmpq	%r13, %r14
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jb	.LBB2_3
	jmp	.LBB2_224
.LBB2_175:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_177
# BB#176:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_177:                              # %seterr.exit.i.i187.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movq	24(%r12), %rax
.LBB2_179:                              # %enlarge.exit.i188.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movl	$2281701376, %edx       # imm = 0x88000000
	leaq	134217730(%rdx), %rdx
	movq	%rdx, (%rax,%rcx,8)
	jmp	.LBB2_213
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph68
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_122 Depth 2
                                        #     Child Loop BB2_188 Depth 2
                                        #     Child Loop BB2_204 Depth 2
	movsbl	(%r14), %eax
	cmpl	$124, %eax
	je	.LBB2_222
# BB#4:                                 # %.lr.ph68
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	%r15d, %eax
	je	.LBB2_222
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%r14), %rbx
	movq	%rbx, (%r12)
	movq	40(%r12), %r8
	movsbl	(%r14), %ebp
	leal	-36(%rbp), %eax
	cmpl	$88, %eax
	ja	.LBB2_92
# BB#6:                                 #   in Loop: Header=BB2_3 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_67:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_42
# BB#68:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$13, 16(%r12)
	jmp	.LBB2_42
.LBB2_54:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_64
# BB#55:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%r12), %rax
	cmpq	%rax, %r8
	jl	.LBB2_63
# BB#56:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_63
# BB#57:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %rbp
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_58
# BB#61:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_62
.LBB2_7:                                #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %rbx
	jb	.LBB2_11
# BB#8:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movl	$8, 16(%r12)
.LBB2_10:                               # %seterr.exit.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_11:                               #   in Loop: Header=BB2_3 Depth=1
	movq	56(%r12), %rax
	movq	112(%rax), %r14
	leaq	1(%r14), %rbp
	movq	%rbp, 112(%rax)
	cmpq	$9, %rbp
	jg	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, 72(%r12,%r14,8)
.LBB2_13:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_23
# BB#14:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%r12), %rax
	cmpq	%rax, 40(%r12)
	jl	.LBB2_22
# BB#15:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_22
# BB#16:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %r13
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_17
# BB#20:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_21
.LBB2_69:                               #   in Loop: Header=BB2_3 Depth=1
	movq	56(%r12), %rax
	testb	$8, 40(%rax)
	jne	.LBB2_70
# BB#71:                                #   in Loop: Header=BB2_3 Depth=1
	xorl	%ebp, %ebp
	cmpl	$0, 16(%r12)
	jne	.LBB2_96
# BB#72:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%r12), %rax
	cmpq	%rax, %r8
	jl	.LBB2_80
# BB#73:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_80
# BB#74:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %r14
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_75
# BB#78:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_79
.LBB2_81:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	movq	%r8, %rbx
	callq	p_bracket
	jmp	.LBB2_94
.LBB2_82:                               #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %rbx
	jb	.LBB2_86
# BB#83:                                #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$5, 16(%r12)
.LBB2_85:                               # %seterr.exit168.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movl	$nuls, %ebx
.LBB2_86:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rbx), %rax
	movq	%rax, (%r12)
	movsbl	(%rbx), %esi
	movq	%r12, %rdi
	jmp	.LBB2_93
.LBB2_43:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_53
# BB#44:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%r12), %rax
	cmpq	%rax, %r8
	jl	.LBB2_52
# BB#45:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_52
# BB#46:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %rbp
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_47
# BB#50:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_51
.LBB2_87:                               #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %rbx
	jae	.LBB2_92
# BB#88:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %r14
	callq	__ctype_b_loc
	movq	%r14, %r8
	movq	(%rax), %rax
	movzbl	(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB2_92
# BB#89:                                #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$13, 16(%r12)
.LBB2_91:                               # %seterr.exit169.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_92:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	movl	%ebp, %esi
.LBB2_93:                               # %doemit.exit167.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %rbx
	callq	ordinary
.LBB2_94:                               # %doemit.exit167.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, %r8
	jmp	.LBB2_95
.LBB2_65:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_42
# BB#66:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$14, 16(%r12)
	jmp	.LBB2_42
.LBB2_70:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	28(%rsp), %rax
	movq	%rax, (%r12)
	leaq	31(%rsp), %rax
	movq	%rax, 8(%r12)
	movl	$6097502, 28(%rsp)      # imm = 0x5D0A5E
	movq	%r12, %rdi
	movq	%r8, %rbp
	callq	p_bracket
	movq	%rbp, %r8
	movq	%rbx, (%r12)
	movq	%r13, 8(%r12)
	jmp	.LBB2_95
.LBB2_58:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_60:                               # %seterr.exit.i.i160.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_62:                               # %enlarge.exit.i161.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %r8
.LBB2_63:                               # %enlarge.exit.i161.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	$536870912, (%rax,%rcx,8) # imm = 0x20000000
.LBB2_64:                               # %doemit.exit162.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	56(%r12), %rax
	orl	$2, 72(%rax)
	incl	80(%rax)
	jmp	.LBB2_95
.LBB2_17:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_19:                               # %seterr.exit.i.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_21:                               # %enlarge.exit.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %r8
.LBB2_22:                               # %enlarge.exit.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %rax
	orq	$1744830464, %rax       # imm = 0x68000000
	movq	24(%r12), %rcx
	movq	40(%r12), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%r12)
	movq	%rax, (%rcx,%rdx,8)
.LBB2_23:                               # %doemit.exit.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_3 Depth=1
	cmpb	$41, (%rax)
	je	.LBB2_26
.LBB2_25:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$41, %esi
	movq	%r12, %rdi
	movq	%r8, %rbx
	callq	p_ere
	movq	%rbx, %r8
.LBB2_26:                               #   in Loop: Header=BB2_3 Depth=1
	cmpq	$9, %rbp
	jg	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rax
	movq	%rax, 152(%r12,%r14,8)
.LBB2_28:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_38
# BB#29:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%r12), %rax
	cmpq	%rax, 40(%r12)
	jl	.LBB2_37
# BB#30:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_37
# BB#31:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %r14
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_32
# BB#35:                                #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_36
.LBB2_47:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_49:                               # %seterr.exit.i.i157.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_51:                               # %enlarge.exit.i158.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %r8
.LBB2_52:                               # %enlarge.exit.i158.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	$402653184, (%rax,%rcx,8) # imm = 0x18000000
.LBB2_53:                               # %doemit.exit159.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	56(%r12), %rax
	orl	$1, 72(%rax)
	incl	76(%rax)
	movl	$1, %ebp
	jmp	.LBB2_96
.LBB2_32:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_34:                               # %seterr.exit.i.i152.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_36:                               # %enlarge.exit.i153.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r14, %r8
.LBB2_37:                               # %enlarge.exit.i153.i
                                        #   in Loop: Header=BB2_3 Depth=1
	orq	$1879048192, %rbp       # imm = 0x70000000
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	%rbp, (%rax,%rcx,8)
.LBB2_38:                               # %doemit.exit154.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	xorl	%ebp, %ebp
	cmpb	$41, (%rax)
	je	.LBB2_96
.LBB2_40:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$8, 16(%r12)
	.p2align	4, 0x90
.LBB2_42:                               # %seterr.exit155.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_95:                               # %doemit.exit167.i
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%ebp, %ebp
.LBB2_96:                               # %doemit.exit167.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%r12), %r14
	movq	8(%r12), %r13
	cmpq	%r13, %r14
	jae	.LBB2_97
# BB#98:                                #   in Loop: Header=BB2_3 Depth=1
	movb	(%r14), %bl
	movl	%ebx, %eax
	addb	$-42, %al
	cmpb	$2, %al
	jb	.LBB2_104
# BB#99:                                #   in Loop: Header=BB2_3 Depth=1
	cmpb	$63, %bl
	je	.LBB2_104
# BB#100:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$123, %bl
	jne	.LBB2_97
# BB#101:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%r14), %r15
	cmpq	%r13, %r15
	jae	.LBB2_102
# BB#103:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, 40(%rsp)           # 8-byte Spill
	callq	__ctype_b_loc
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%r15), %ecx
	testb	$8, 1(%rax,%rcx,2)
	movl	(%rsp), %r15d           # 4-byte Reload
	je	.LBB2_97
	.p2align	4, 0x90
.LBB2_104:                              #   in Loop: Header=BB2_3 Depth=1
	movsbl	%bl, %eax
	incq	%r14
	movq	%r14, (%r12)
	testl	%ebp, %ebp
	je	.LBB2_108
# BB#105:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_107
# BB#106:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$13, 16(%r12)
.LBB2_107:                              # %seterr.exit170.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movl	$nuls, %r13d
	movl	$nuls, %r14d
.LBB2_108:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$62, %eax
	jg	.LBB2_118
# BB#109:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$42, %eax
	je	.LBB2_131
# BB#110:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$43, %eax
	jne	.LBB2_213
# BB#111:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$1, %edx
	subq	%r8, %rdx
	addq	40(%r12), %rdx
	movl	$1207959552, %esi       # imm = 0x48000000
	movq	%r12, %rdi
	movq	%r8, %rcx
	movq	%r8, %rbx
	callq	doinsert
	movq	%rbx, %rax
	cmpl	$0, 16(%r12)
	jne	.LBB2_213
# BB#112:                               #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	movq	%rcx, %rbp
	subq	%rax, %rbp
	movq	32(%r12), %rax
	cmpq	%rax, %rcx
	jl	.LBB2_153
# BB#113:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_153
# BB#114:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_115
# BB#152:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
.LBB2_153:                              # %enlarge.exit.i178.i
                                        #   in Loop: Header=BB2_3 Depth=1
	orq	$1342177280, %rbp       # imm = 0x50000000
	jmp	.LBB2_151
.LBB2_118:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$63, %eax
	je	.LBB2_154
# BB#119:                               #   in Loop: Header=BB2_3 Depth=1
	cmpl	$123, %eax
	jne	.LBB2_213
# BB#120:                               #   in Loop: Header=BB2_3 Depth=1
	xorl	%ebp, %ebp
	cmpq	%r13, %r14
	jae	.LBB2_128
# BB#121:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r8, %rbx
	xorl	%r15d, %r15d
	callq	__ctype_b_loc
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_122:                              #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$255, %ebp
	jg	.LBB2_125
# BB#123:                               #   in Loop: Header=BB2_122 Depth=2
	movq	(%rax), %rcx
	movzbl	(%r14,%r15), %edx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	je	.LBB2_125
# BB#124:                               #   in Loop: Header=BB2_122 Depth=2
	leal	(%rbp,%rbp,4), %ecx
	leaq	1(%r14,%r15), %rdx
	movq	%rdx, (%r12)
	movsbl	(%r14,%r15), %esi
	leal	-48(%rsi,%rcx,2), %ebp
	incq	%r15
	cmpq	%r13, %rdx
	jb	.LBB2_122
.LBB2_125:                              # %..critedge.i.i_crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	%r15, %r14
	movq	%rbx, %r8
	cmpl	$255, %ebp
	jg	.LBB2_127
# BB#126:                               # %.critedge.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB2_127
# BB#180:                               # %p_count.exit.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %r14
	jae	.LBB2_181
# BB#182:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$44, (%r14)
	jne	.LBB2_181
# BB#183:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%r14), %r15
	movq	%r15, (%r12)
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	1(%r14), %edx
	movzwl	(%rcx,%rdx,2), %edx
	movl	$256, %ecx              # imm = 0x100
	testb	$8, %dh
	jne	.LBB2_185
# BB#184:                               #   in Loop: Header=BB2_3 Depth=1
	movl	(%rsp), %r15d           # 4-byte Reload
	movq	%rbx, %r8
	jmp	.LBB2_201
.LBB2_131:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$1, %ebp
	subq	%r8, %rbp
	movq	40(%r12), %rdx
	addq	%rbp, %rdx
	movl	$1207959552, %esi       # imm = 0x48000000
	movq	%r12, %rdi
	movq	%r8, %rcx
	movq	%r8, %rbx
	callq	doinsert
	movq	40(%r12), %rax
	cmpl	$0, 16(%r12)
	je	.LBB2_133
# BB#132:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, %rcx
	jmp	.LBB2_142
.LBB2_154:                              #   in Loop: Header=BB2_3 Depth=1
	movl	$1, %edx
	subq	%r8, %rdx
	addq	40(%r12), %rdx
	movl	$2013265920, %esi       # imm = 0x78000000
	movq	%r12, %rdi
	movq	%r8, %rcx
	movq	%r8, %rbx
	callq	doinsert
	movq	%rbx, %rdi
	cmpl	$0, 16(%r12)
	jne	.LBB2_213
# BB#155:                               #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rdx
	movq	%rdx, %rbp
	subq	%rdi, %rbp
	movq	32(%r12), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, %rdx
	jl	.LBB2_163
# BB#156:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rdx
	shrq	$63, %rdx
	leaq	1(%rax,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %r14
	cmpq	%r14, %rax
	jge	.LBB2_163
# BB#157:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rdi
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_158
# BB#161:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%r14, 32(%r12)
	movl	16(%r12), %ecx
	jmp	.LBB2_162
.LBB2_102:                              #   in Loop: Header=BB2_3 Depth=1
	movl	(%rsp), %r15d           # 4-byte Reload
	jmp	.LBB2_97
.LBB2_133:                              #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, %r14
	subq	%rbx, %r14
	movq	32(%r12), %rdx
	cmpq	%rdx, %rax
	movq	%rbx, %rcx
	jl	.LBB2_141
# BB#134:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rdx), %rax
	shrq	$63, %rax
	leaq	1(%rdx,%rax), %rax
	sarq	%rax
	leaq	(%rax,%rax,2), %rbx
	cmpq	%rbx, %rdx
	jge	.LBB2_141
# BB#135:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rcx, %r13
	movq	24(%r12), %rdi
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_136
# BB#139:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_140
.LBB2_127:                              #   in Loop: Header=BB2_3 Depth=1
	movl	(%rsp), %r15d           # 4-byte Reload
.LBB2_128:                              # %.critedge.thread.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_130
# BB#129:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$10, 16(%r12)
.LBB2_130:                              # %p_count.exit.i.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movl	%ebp, %ecx
.LBB2_201:                              #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rdi
	movq	%r8, %rsi
	movl	%ebp, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	repeat
	movq	(%r12), %rax
	movq	8(%r12), %rcx
	cmpq	%rcx, %rax
	jae	.LBB2_206
# BB#202:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$125, (%rax)
	jne	.LBB2_204
# BB#203:                               #   in Loop: Header=BB2_3 Depth=1
	incq	%rax
	movq	%rax, (%r12)
	jmp	.LBB2_213
	.p2align	4, 0x90
.LBB2_204:                              # %.lr.ph
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$125, (%rax)
	je	.LBB2_206
# BB#205:                               #   in Loop: Header=BB2_204 Depth=2
	incq	%rax
	movq	%rax, (%r12)
	cmpq	%rcx, %rax
	jb	.LBB2_204
.LBB2_206:                              # %.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	%rcx, %rax
	movl	16(%r12), %eax
	jae	.LBB2_207
# BB#210:                               #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	jne	.LBB2_212
# BB#211:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$10, 16(%r12)
	jmp	.LBB2_212
.LBB2_207:                              #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	jne	.LBB2_209
# BB#208:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$9, 16(%r12)
.LBB2_209:                              # %.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_212:                              # %seterr.exit205.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	jmp	.LBB2_213
.LBB2_181:                              #   in Loop: Header=BB2_3 Depth=1
	movl	%ebp, %ecx
	movl	(%rsp), %r15d           # 4-byte Reload
	jmp	.LBB2_201
.LBB2_115:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_117
# BB#116:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_117:                              # %seterr.exit.i.i177.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	orq	$1342177280, %rbp       # imm = 0x50000000
	jmp	.LBB2_151
.LBB2_158:                              #   in Loop: Header=BB2_3 Depth=1
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jne	.LBB2_160
# BB#159:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
	movl	$12, %ecx
.LBB2_160:                              # %seterr.exit.i.i180.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_162:                              # %doemit.exit182.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbx, %rdi
.LBB2_163:                              # %doemit.exit182.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$2281701376, %eax       # imm = 0x88000000
	leaq	-134217728(%rax), %rax
	orq	%rax, %rbp
	movq	24(%r12), %rax
	movq	40(%r12), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%r12)
	movq	%rbp, (%rax,%rdx,8)
	testl	%ecx, %ecx
	jne	.LBB2_213
# BB#164:                               #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	subq	%rdi, %rcx
	movq	(%rax,%rdi,8), %rdx
	movl	$4160749568, %esi       # imm = 0xF8000000
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, (%rax,%rdi,8)
	movq	32(%r12), %rcx
	cmpq	%rcx, 40(%r12)
	jl	.LBB2_170
# BB#165:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB2_170
# BB#166:                               #   in Loop: Header=BB2_3 Depth=1
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_167
# BB#171:                               # %doemit.exit185.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	cmpl	$0, 16(%r12)
	movq	%rdx, 40(%r12)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
	jne	.LBB2_213
	jmp	.LBB2_172
.LBB2_170:                              # %doemit.exit185.i.thread
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
.LBB2_172:                              #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	movq	-8(%rax,%rcx,8), %rdx
	movl	$4160749568, %esi       # imm = 0xF8000000
	andq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rdx, -8(%rax,%rcx,8)
	movq	32(%r12), %rcx
	cmpq	%rcx, 40(%r12)
	jl	.LBB2_179
# BB#173:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB2_179
# BB#174:                               #   in Loop: Header=BB2_3 Depth=1
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_175
# BB#178:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_179
.LBB2_136:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_138
# BB#137:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_138:                              # %seterr.exit.i.i171.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movdqa	48(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%r12)
.LBB2_140:                              # %enlarge.exit.i172.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r13, %rcx
.LBB2_141:                              # %enlarge.exit.i172.i
                                        #   in Loop: Header=BB2_3 Depth=1
	orq	$1342177280, %r14       # imm = 0x50000000
	movq	24(%r12), %rax
	movq	40(%r12), %rsi
	leaq	1(%rsi), %rdx
	movq	%rdx, 40(%r12)
	movq	%r14, (%rax,%rsi,8)
	movq	40(%r12), %rax
.LBB2_142:                              # %doemit.exit173.i
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	%rax, %rbp
	movl	$1476395008, %esi       # imm = 0x58000000
	movq	%r12, %rdi
	movq	%rbp, %rdx
	movq	%rcx, %rbx
	callq	doinsert
	movq	%rbx, %rax
	cmpl	$0, 16(%r12)
	jne	.LBB2_213
# BB#143:                               #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rcx
	movq	%rcx, %rbp
	subq	%rax, %rbp
	movq	32(%r12), %rax
	cmpq	%rax, %rcx
	jl	.LBB2_150
# BB#144:                               #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB2_150
# BB#145:                               #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_146
# BB#149:                               #   in Loop: Header=BB2_3 Depth=1
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_150
.LBB2_146:                              #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 16(%r12)
	jne	.LBB2_148
# BB#147:                               #   in Loop: Header=BB2_3 Depth=1
	movl	$12, 16(%r12)
.LBB2_148:                              # %seterr.exit.i.i174.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB2_150:                              # %enlarge.exit.i175.i
                                        #   in Loop: Header=BB2_3 Depth=1
	orq	$1610612736, %rbp       # imm = 0x60000000
.LBB2_151:                              # %doemit.exit176.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	%rbp, (%rax,%rcx,8)
	.p2align	4, 0x90
.LBB2_213:                              # %doemit.exit176.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%r12), %r14
	movq	8(%r12), %r13
	cmpq	%r13, %r14
	jae	.LBB2_97
# BB#214:                               #   in Loop: Header=BB2_3 Depth=1
	movb	(%r14), %al
	cmpb	$123, %al
	je	.LBB2_217
# BB#215:                               #   in Loop: Header=BB2_3 Depth=1
	movl	%eax, %ecx
	addb	$-42, %cl
	cmpb	$2, %cl
	jb	.LBB2_219
# BB#216:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$63, %al
	jne	.LBB2_97
	jmp	.LBB2_219
.LBB2_217:                              #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%r14), %rbx
	cmpq	%r13, %rbx
	jae	.LBB2_97
# BB#218:                               #   in Loop: Header=BB2_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB2_219
	.p2align	4, 0x90
.LBB2_97:                               # %p_ere_exp.exit.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	%r13, %r14
	jb	.LBB2_3
.LBB2_222:                              # %.critedge
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB2_227
# BB#223:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 16(%r12)
	jne	.LBB2_226
	jmp	.LBB2_225
.LBB2_1:
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
                                        # implicit-def: %RAX
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB2_224:                              # %.critedge.thread
	cmpl	$0, 16(%r12)
	jne	.LBB2_226
.LBB2_225:
	movl	$14, 16(%r12)
.LBB2_226:                              # %.thread94
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	jmp	.LBB2_229
.LBB2_228:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
.LBB2_229:                              # %.loopexit98
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB2_262
# BB#230:
	cmpl	$0, 16(%r12)
	jne	.LBB2_262
# BB#231:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	subq	%rdi, %rdx
	movq	24(%r12), %rax
	movl	$4160749568, %ecx       # imm = 0xF8000000
	andq	(%rax,%rdi,8), %rcx
	orq	%rdx, %rcx
	movq	%rcx, (%rax,%rdi,8)
	movq	32(%r12), %rcx
	movq	40(%r12), %rdx
	movq	%rdx, %rbp
	subq	%rsi, %rbp
	cmpq	%rcx, %rdx
	jl	.LBB2_261
# BB#232:
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB2_261
# BB#233:
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB2_234
# BB#260:
	movq	%rax, 24(%r12)
	movq	%rbx, 32(%r12)
	jmp	.LBB2_261
.LBB2_234:
	cmpl	$0, 16(%r12)
	jne	.LBB2_236
# BB#235:
	movl	$12, 16(%r12)
.LBB2_236:                              # %seterr.exit.i.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movq	24(%r12), %rax
.LBB2_261:                              # %enlarge.exit.i
	movl	$2281701376, %ecx       # imm = 0x88000000
	addq	$134217728, %rcx        # imm = 0x8000000
	orq	%rcx, %rbp
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	%rbp, (%rax,%rcx,8)
.LBB2_262:                              # %doemit.exit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	p_ere, .Lfunc_end2-p_ere
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_54
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_7
	.quad	.LBB2_40
	.quad	.LBB2_67
	.quad	.LBB2_67
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_69
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_67
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_81
	.quad	.LBB2_82
	.quad	.LBB2_92
	.quad	.LBB2_43
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_92
	.quad	.LBB2_87
	.quad	.LBB2_65

	.text
	.p2align	4, 0x90
	.type	p_bre,@function
p_bre:                                  # @p_bre
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 96
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movq	%rdi, %r13
	movq	40(%r13), %rbx
	movq	(%r13), %rbp
	movq	8(%r13), %r14
	cmpq	%r14, %rbp
	jae	.LBB3_12
# BB#1:
	cmpb	$94, (%rbp)
	jne	.LBB3_12
# BB#2:
	incq	%rbp
	movq	%rbp, (%r13)
	cmpl	$0, 16(%r13)
	jne	.LBB3_11
# BB#3:
	movq	32(%r13), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_10
# BB#4:
	leaq	1(%rcx), %rax
	shrq	$63, %rax
	leaq	1(%rcx,%rax), %rax
	sarq	%rax
	leaq	(%rax,%rax,2), %r15
	cmpq	%r15, %rcx
	jge	.LBB3_10
# BB#5:
	movq	24(%r13), %rdi
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_7
# BB#6:
	movq	%rax, 24(%r13)
	movq	%r15, 32(%r13)
	movq	(%r13), %rbp
	movq	8(%r13), %r14
	jmp	.LBB3_10
.LBB3_7:
	cmpl	$0, 16(%r13)
	jne	.LBB3_9
# BB#8:
	movl	$12, 16(%r13)
.LBB3_9:                                # %seterr.exit.i.i
	movl	$nuls, %r14d
	movd	%r14, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
	movl	$nuls, %ebp
.LBB3_10:                               # %enlarge.exit.i
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	$402653184, (%rax,%rcx,8) # imm = 0x18000000
.LBB3_11:                               # %doemit.exit
	movq	56(%r13), %rax
	orl	$1, 72(%rax)
	incl	76(%rax)
.LBB3_12:                               # %p_simp_re.exit.preheader
	cmpq	%r14, %rbp
	jae	.LBB3_207
# BB#13:                                # %.lr.ph42
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
	xorl	%ecx, %ecx
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB3_36
.LBB3_16:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_18:                               # %seterr.exit.i.i.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_19:                               # %enlarge.exit.i.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	$671088640, (%rax,%rcx,8) # imm = 0x28000000
	jmp	.LBB3_135
.LBB3_20:                               #   in Loop: Header=BB3_36 Depth=1
	movl	12(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB3_162
.LBB3_21:                               # %.lr.ph.i161.i.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	xorl	%ebx, %ebx
	testb	$8, %ch
	movl	$0, %ecx
	je	.LBB3_27
# BB#22:                                # %.lr.ph72.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	xorl	%ebx, %ebx
	movl	$2, %edx
.LBB3_23:                               # %.lr.ph72
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	leaq	(%rbp,%rcx), %rdx
	leal	(%rbx,%rbx,4), %esi
	movq	%rdx, (%r13)
	movsbl	-1(%rbp,%rcx), %edi
	leal	-48(%rdi,%rsi,2), %ebx
	cmpq	%r14, %rdx
	jae	.LBB3_26
# BB#24:                                # %..lr.ph.i161.i_crit_edge
                                        #   in Loop: Header=BB3_23 Depth=2
	cmpl	$255, %ebx
	jg	.LBB3_26
# BB#25:                                # %..lr.ph.i161.i_crit_edge
                                        #   in Loop: Header=BB3_23 Depth=2
	movq	(%rax), %rsi
	movzbl	(%rdx), %edx
	movzwl	(%rsi,%rdx,2), %esi
	andl	$2048, %esi             # imm = 0x800
	leaq	1(%rcx), %rdx
	testw	%si, %si
	jne	.LBB3_23
.LBB3_26:                               # %.critedge.i168.i.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	decl	%ecx
.LBB3_27:                               # %.critedge.i168.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	$255, %ebx
	movl	12(%rsp), %r12d         # 4-byte Reload
	jg	.LBB3_29
# BB#28:                                # %.critedge.i168.i
                                        #   in Loop: Header=BB3_36 Depth=1
	testl	%ecx, %ecx
	jg	.LBB3_32
.LBB3_29:                               # %.critedge.thread.i170.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$10, 16(%r13)
.LBB3_31:                               # %seterr.exit.i171.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_32:                               # %p_count.exit173.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	%ebx, %r15d
	jle	.LBB3_162
# BB#33:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$10, 16(%r13)
.LBB3_35:                               # %seterr.exit174.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
	jmp	.LBB3_162
	.p2align	4, 0x90
.LBB3_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_147 Depth 2
                                        #     Child Loop BB3_23 Depth 2
                                        #     Child Loop BB3_168 Depth 2
	leaq	1(%rbp), %rbx
	cmpq	%r14, %rbx
	jae	.LBB3_39
# BB#37:                                #   in Loop: Header=BB3_36 Depth=1
	movsbl	(%rbp), %edx
	cmpl	28(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_36 Depth=1
	movsbl	(%rbx), %edx
	cmpl	%r12d, %edx
	je	.LBB3_196
.LBB3_39:                               # %.critedge33
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	40(%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, (%r13)
	movsbl	(%rbp), %r15d
	cmpl	$92, %r15d
	jne	.LBB3_45
# BB#40:                                #   in Loop: Header=BB3_36 Depth=1
	cmpq	%r14, %rbx
	jb	.LBB3_44
# BB#41:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$5, 16(%r13)
.LBB3_43:                               # %seterr.exit.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
	movl	$nuls, %r14d
	movl	$nuls, %ebx
.LBB3_44:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rbx), %rcx
	movq	%rcx, (%r13)
	movsbl	(%rbx), %r15d
	orl	$256, %r15d             # imm = 0x100
	movq	%rcx, %rbx
.LBB3_45:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$295, %r15d             # imm = 0x127
	jle	.LBB3_54
# BB#46:                                #   in Loop: Header=BB3_36 Depth=1
	leal	-296(%r15), %eax
	cmpl	$85, %eax
	ja	.LBB3_76
# BB#47:                                #   in Loop: Header=BB3_36 Depth=1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_48:                               #   in Loop: Header=BB3_36 Depth=1
	movl	%r15d, %eax
	andl	$-257, %eax             # imm = 0xFEFF
	movslq	%eax, %r14
	movq	-240(%r13,%r14,8), %rbx
	testq	%rbx, %rbx
	movl	16(%r13), %eax
	je	.LBB3_58
# BB#49:                                #   in Loop: Header=BB3_36 Depth=1
	addq	$-48, %r14
	testl	%eax, %eax
	jne	.LBB3_88
# BB#50:                                #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %rax
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	jl	.LBB3_87
# BB#51:                                #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_87
# BB#52:                                #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_84
# BB#53:                                #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_87
	.p2align	4, 0x90
.LBB3_54:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$42, %r15d
	je	.LBB3_72
# BB#55:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$46, %r15d
	je	.LBB3_77
# BB#56:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$91, %r15d
	jne	.LBB3_76
# BB#57:                                #   in Loop: Header=BB3_36 Depth=1
	movq	%r13, %rdi
	callq	p_bracket
	jmp	.LBB3_135
.LBB3_58:                               #   in Loop: Header=BB3_36 Depth=1
	testl	%eax, %eax
	jne	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$6, 16(%r13)
.LBB3_60:                               # %seterr.exit153.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
	jmp	.LBB3_106
.LBB3_61:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_134
	jmp	.LBB3_133
.LBB3_63:                               #   in Loop: Header=BB3_36 Depth=1
	movq	56(%r13), %rax
	movq	112(%rax), %r14
	leaq	1(%r14), %rbp
	movq	%rbp, 112(%rax)
	cmpq	$9, %rbp
	jg	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_36 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%r13,%r14,8)
.LBB3_65:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_111
# BB#66:                                #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %rax
	cmpq	%rax, 40(%r13)
	jl	.LBB3_110
# BB#67:                                #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_110
# BB#68:                                #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_107
# BB#69:                                #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_110
.LBB3_70:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_134
# BB#71:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$13, 16(%r13)
	jmp	.LBB3_134
.LBB3_72:                               #   in Loop: Header=BB3_36 Depth=1
	testl	%eax, %eax
	jne	.LBB3_76
# BB#73:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_75
# BB#74:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$13, 16(%r13)
.LBB3_75:                               # %seterr.exit154.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_76:                               #   in Loop: Header=BB3_36 Depth=1
	movsbl	%r15b, %esi
	movq	%r13, %rdi
	callq	ordinary
	jmp	.LBB3_135
.LBB3_77:                               #   in Loop: Header=BB3_36 Depth=1
	movq	56(%r13), %rax
	testb	$8, 40(%rax)
	jne	.LBB3_83
# BB#78:                                #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_135
# BB#79:                                #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %rax
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	jl	.LBB3_19
# BB#80:                                #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_19
# BB#81:                                #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_16
# BB#82:                                #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_19
.LBB3_83:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	24(%rsp), %rax
	movq	%rax, (%r13)
	leaq	27(%rsp), %rax
	movq	%rax, 8(%r13)
	movl	$6097502, 24(%rsp)      # imm = 0x5D0A5E
	movq	%r13, %rdi
	callq	p_bracket
	movq	%rbx, (%r13)
	movq	%r14, 8(%r13)
	jmp	.LBB3_135
.LBB3_84:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_86
# BB#85:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_86:                               # %seterr.exit.i.i145.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_87:                               # %enlarge.exit.i146.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%r14, %rax
	orq	$939524096, %rax        # imm = 0x38000000
	movq	24(%r13), %rcx
	movq	40(%r13), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%r13)
	movq	%rax, (%rcx,%rdx,8)
	movq	144(%r13,%r14,8), %rbx
.LBB3_88:                               # %doemit.exit147.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	64(%r13,%r14,8), %rbp
	incq	%rbp
	subq	%rbp, %rbx
	je	.LBB3_97
# BB#89:                                #   in Loop: Header=BB3_36 Depth=1
	testq	%rbx, %rbx
	jle	.LBB3_96
# BB#90:                                #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %r12
	addq	%rbx, %r12
	movq	24(%r13), %rdi
	leaq	(,%r12,8), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_92
# BB#91:                                #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%r12, 32(%r13)
	jmp	.LBB3_95
.LBB3_92:                               #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_94:                               # %seterr.exit.i.i148.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_95:                               # %enlarge.exit.i149.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	12(%rsp), %r12d         # 4-byte Reload
.LBB3_96:                               # %enlarge.exit.i149.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	(%rax,%rcx,8), %rdi
	leaq	(%rax,%rbp,8), %rsi
	leaq	(,%rbx,8), %rdx
	callq	memmove
	addq	%rbx, 40(%r13)
.LBB3_97:                               # %dupl.exit.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_106
# BB#98:                                #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %rax
	cmpq	%rax, 40(%r13)
	jl	.LBB3_105
# BB#99:                                #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_105
# BB#100:                               #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_102
# BB#101:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_105
.LBB3_102:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_104
# BB#103:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_104:                              # %seterr.exit.i.i150.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_105:                              # %enlarge.exit.i151.i
                                        #   in Loop: Header=BB3_36 Depth=1
	orq	$1073741824, %r14       # imm = 0x40000000
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	%r14, (%rax,%rcx,8)
.LBB3_106:                              # %doemit.exit152.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	56(%r13), %rax
	movl	$1, 120(%rax)
	jmp	.LBB3_135
.LBB3_107:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_109
# BB#108:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_109:                              # %seterr.exit.i.i137.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_110:                              # %enlarge.exit.i138.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%rbp, %rax
	orq	$1744830464, %rax       # imm = 0x68000000
	movq	24(%r13), %rcx
	movq	40(%r13), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%r13)
	movq	%rax, (%rcx,%rdx,8)
.LBB3_111:                              # %doemit.exit139.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	jae	.LBB3_116
# BB#112:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB3_115
# BB#113:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$92, (%rax)
	jne	.LBB3_115
# BB#114:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$41, (%rcx)
	je	.LBB3_116
.LBB3_115:                              #   in Loop: Header=BB3_36 Depth=1
	movl	$92, %esi
	movl	$41, %edx
	movq	%r13, %rdi
	callq	p_bre
.LBB3_116:                              #   in Loop: Header=BB3_36 Depth=1
	cmpq	$9, %rbp
	jg	.LBB3_118
# BB#117:                               #   in Loop: Header=BB3_36 Depth=1
	movq	40(%r13), %rax
	movq	%rax, 152(%r13,%r14,8)
.LBB3_118:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_127
# BB#119:                               #   in Loop: Header=BB3_36 Depth=1
	movq	32(%r13), %rax
	cmpq	%rax, 40(%r13)
	jl	.LBB3_126
# BB#120:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_126
# BB#121:                               #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_123
# BB#122:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_126
.LBB3_123:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_125
# BB#124:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_125:                              # %seterr.exit.i.i140.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_126:                              # %enlarge.exit.i141.i
                                        #   in Loop: Header=BB3_36 Depth=1
	orq	$1879048192, %rbp       # imm = 0x70000000
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	%rbp, (%rax,%rcx,8)
.LBB3_127:                              # %doemit.exit142.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	jae	.LBB3_132
# BB#128:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jae	.LBB3_132
# BB#129:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$92, (%rax)
	jne	.LBB3_132
# BB#130:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$41, (%rcx)
	jne	.LBB3_132
# BB#131:                               #   in Loop: Header=BB3_36 Depth=1
	addq	$2, %rax
	movq	%rax, (%r13)
	jmp	.LBB3_135
.LBB3_132:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_134
.LBB3_133:                              #   in Loop: Header=BB3_36 Depth=1
	movl	$8, 16(%r13)
.LBB3_134:                              # %seterr.exit136.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_135:                              # %doemit.exit.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	(%r13), %rbp
	movq	8(%r13), %r14
	cmpq	%r14, %rbp
	jae	.LBB3_157
# BB#136:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rbp), %rax
	cmpb	$42, (%rbp)
	jne	.LBB3_142
# BB#137:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, (%r13)
	movl	$1, %ebp
	movq	16(%rsp), %rbx          # 8-byte Reload
	subq	%rbx, %rbp
	movq	40(%r13), %rdx
	addq	%rbp, %rdx
	movl	$1207959552, %esi       # imm = 0x48000000
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	doinsert
	movq	40(%r13), %rax
	cmpl	$0, 16(%r13)
	jne	.LBB3_185
# BB#138:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, %r15
	subq	%rbx, %r15
	movq	32(%r13), %rcx
	cmpq	%rcx, %rax
	jl	.LBB3_184
# BB#139:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rcx), %rax
	shrq	$63, %rax
	leaq	1(%rcx,%rax), %rax
	sarq	%rax
	leaq	(%rax,%rax,2), %r14
	cmpq	%r14, %rcx
	jge	.LBB3_184
# BB#140:                               #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_181
# BB#141:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%r14, 32(%r13)
	jmp	.LBB3_184
	.p2align	4, 0x90
.LBB3_142:                              #   in Loop: Header=BB3_36 Depth=1
	cmpq	%r14, %rax
	jae	.LBB3_157
# BB#143:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$92, (%rbp)
	jne	.LBB3_157
# BB#144:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$123, (%rax)
	jne	.LBB3_157
# BB#145:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	2(%rbp), %rax
	movq	%rax, (%r13)
	xorl	%r15d, %r15d
	cmpq	%r14, %rax
	jae	.LBB3_158
# BB#146:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB3_36 Depth=1
	xorl	%ebx, %ebx
	callq	__ctype_b_loc
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_147:                              #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$255, %r15d
	jg	.LBB3_150
# BB#148:                               #   in Loop: Header=BB3_147 Depth=2
	movq	(%rax), %rcx
	movzbl	2(%rbp,%rbx), %edx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	je	.LBB3_150
# BB#149:                               #   in Loop: Header=BB3_147 Depth=2
	leal	(%r15,%r15,4), %ecx
	leaq	3(%rbp,%rbx), %rdx
	movq	%rdx, (%r13)
	movsbl	2(%rbp,%rbx), %esi
	leal	-48(%rsi,%rcx,2), %r15d
	incq	%rbx
	cmpq	%r14, %rdx
	jb	.LBB3_147
.LBB3_150:                              # %..critedge.i.i_crit_edge
                                        #   in Loop: Header=BB3_36 Depth=1
	leaq	2(%rbp,%rbx), %rbp
	cmpl	$255, %r15d
	jg	.LBB3_158
# BB#151:                               # %.critedge.i.i
                                        #   in Loop: Header=BB3_36 Depth=1
	testl	%ebx, %ebx
	jle	.LBB3_158
# BB#152:                               # %p_count.exit.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpq	%r14, %rbp
	jae	.LBB3_161
# BB#153:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$44, (%rbp)
	jne	.LBB3_161
# BB#154:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rbp), %r12
	movq	%r12, (%r13)
	movl	$256, %ebx              # imm = 0x100
	cmpq	%r14, %r12
	jae	.LBB3_20
# BB#155:                               #   in Loop: Header=BB3_36 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	(%r12), %edx
	movzwl	(%rcx,%rdx,2), %ecx
	testb	$8, %ch
	jne	.LBB3_21
# BB#156:                               #   in Loop: Header=BB3_36 Depth=1
	movl	12(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB3_162
	.p2align	4, 0x90
.LBB3_157:                              # %.thread59
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$1, %ecx
	cmpl	$36, %r15d
	jne	.LBB3_194
	jmp	.LBB3_195
.LBB3_158:                              # %.critedge.thread.i.i
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_160
# BB#159:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$10, 16(%r13)
.LBB3_160:                              # %p_count.exit.i.thread
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_161:                              #   in Loop: Header=BB3_36 Depth=1
	movl	%r15d, %ebx
.LBB3_162:                              #   in Loop: Header=BB3_36 Depth=1
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %edx
	movl	%ebx, %ecx
	callq	repeat
	movq	(%r13), %rdx
	movq	8(%r13), %rax
	cmpq	%rax, %rdx
	jae	.LBB3_174
# BB#163:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rdx), %rcx
	cmpq	%rax, %rcx
	jae	.LBB3_168
# BB#164:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$92, (%rdx)
	jne	.LBB3_168
# BB#165:                               #   in Loop: Header=BB3_36 Depth=1
	cmpb	$125, (%rcx)
	jne	.LBB3_168
# BB#166:                               #   in Loop: Header=BB3_36 Depth=1
	addq	$2, %rdx
	movq	%rdx, (%r13)
	jmp	.LBB3_194
	.p2align	4, 0x90
.LBB3_167:                              # %.thread39.thread
                                        #   in Loop: Header=BB3_168 Depth=2
	movq	%rcx, (%r13)
	incq	%rcx
.LBB3_168:                              # %.lr.ph.preheader
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %rcx
	jae	.LBB3_172
# BB#169:                               # %.lr.ph76
                                        #   in Loop: Header=BB3_168 Depth=2
	cmpb	$92, -1(%rcx)
	jne	.LBB3_167
# BB#170:                               #   in Loop: Header=BB3_168 Depth=2
	cmpb	$125, (%rcx)
	jne	.LBB3_167
# BB#171:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	decq	%rcx
	jmp	.LBB3_173
.LBB3_172:                              # %.thread39.thread62
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%rcx, (%r13)
.LBB3_173:                              # %.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	%rcx, %rdx
.LBB3_174:                              # %.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpq	%rax, %rdx
	movl	16(%r13), %eax
	jae	.LBB3_177
# BB#175:                               #   in Loop: Header=BB3_36 Depth=1
	testl	%eax, %eax
	jne	.LBB3_180
# BB#176:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$10, 16(%r13)
	jmp	.LBB3_180
.LBB3_177:                              #   in Loop: Header=BB3_36 Depth=1
	testl	%eax, %eax
	jne	.LBB3_179
# BB#178:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$9, 16(%r13)
.LBB3_179:                              # %.thread
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_180:                              # %seterr.exit176.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
	jmp	.LBB3_194
.LBB3_181:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_183
# BB#182:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_183:                              # %seterr.exit.i.i155.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_184:                              # %enlarge.exit.i156.i
                                        #   in Loop: Header=BB3_36 Depth=1
	orq	$1342177280, %r15       # imm = 0x50000000
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	%r15, (%rax,%rcx,8)
	movq	40(%r13), %rax
.LBB3_185:                              # %doemit.exit157.i
                                        #   in Loop: Header=BB3_36 Depth=1
	addq	%rax, %rbp
	movl	$1476395008, %esi       # imm = 0x58000000
	movq	%r13, %rdi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	doinsert
	cmpl	$0, 16(%r13)
	jne	.LBB3_194
# BB#186:                               #   in Loop: Header=BB3_36 Depth=1
	movq	40(%r13), %rcx
	movq	%rcx, %rbp
	subq	%rbx, %rbp
	movq	32(%r13), %rax
	cmpq	%rax, %rcx
	jl	.LBB3_193
# BB#187:                               #   in Loop: Header=BB3_36 Depth=1
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbx
	cmpq	%rbx, %rax
	jge	.LBB3_193
# BB#188:                               #   in Loop: Header=BB3_36 Depth=1
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_190
# BB#189:                               #   in Loop: Header=BB3_36 Depth=1
	movq	%rax, 24(%r13)
	movq	%rbx, 32(%r13)
	jmp	.LBB3_193
.LBB3_190:                              #   in Loop: Header=BB3_36 Depth=1
	cmpl	$0, 16(%r13)
	jne	.LBB3_192
# BB#191:                               #   in Loop: Header=BB3_36 Depth=1
	movl	$12, 16(%r13)
.LBB3_192:                              # %seterr.exit.i.i158.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_193:                              # %enlarge.exit.i159.i
                                        #   in Loop: Header=BB3_36 Depth=1
	orq	$1610612736, %rbp       # imm = 0x60000000
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	%rbp, (%rax,%rcx,8)
	.p2align	4, 0x90
.LBB3_194:                              # %doemit.exit160.i
                                        #   in Loop: Header=BB3_36 Depth=1
	movq	(%r13), %rbp
	movq	8(%r13), %r14
	xorl	%ecx, %ecx
.LBB3_195:                              # %p_simp_re.exit.backedge
                                        #   in Loop: Header=BB3_36 Depth=1
	xorl	%eax, %eax
	cmpq	%r14, %rbp
	jb	.LBB3_36
.LBB3_196:                              # %.critedge
	testl	%ecx, %ecx
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_207
# BB#197:
	movq	40(%r13), %rcx
	leaq	-1(%rcx), %rax
	movq	%rax, 40(%r13)
	cmpl	$0, 16(%r13)
	jne	.LBB3_206
# BB#198:
	movq	32(%r13), %rax
	cmpq	%rax, %rcx
	jle	.LBB3_205
# BB#199:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbp
	cmpq	%rbp, %rax
	jge	.LBB3_205
# BB#200:
	movq	24(%r13), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB3_202
# BB#201:
	movq	%rax, 24(%r13)
	movq	%rbp, 32(%r13)
	jmp	.LBB3_205
.LBB3_202:
	cmpl	$0, 16(%r13)
	jne	.LBB3_204
# BB#203:
	movl	$12, 16(%r13)
.LBB3_204:                              # %seterr.exit.i.i35
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_205:                              # %enlarge.exit.i36
	movq	24(%r13), %rax
	movq	40(%r13), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r13)
	movq	$536870912, (%rax,%rcx,8) # imm = 0x20000000
.LBB3_206:                              # %doemit.exit37
	movq	56(%r13), %rax
	orl	$2, 72(%rax)
	incl	80(%rax)
.LBB3_207:                              # %.critedge.thread
	cmpq	%rbx, 40(%r13)
	jne	.LBB3_211
# BB#208:
	cmpl	$0, 16(%r13)
	jne	.LBB3_210
# BB#209:
	movl	$14, 16(%r13)
.LBB3_210:                              # %seterr.exit
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r13)
.LBB3_211:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	p_bre, .Lfunc_end3-p_bre
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_63
	.quad	.LBB3_61
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_70
	.quad	.LBB3_76
	.quad	.LBB3_61

	.text
	.p2align	4, 0x90
	.type	doinsert,@function
doinsert:                               # @doinsert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	cmpl	$0, 16(%rbx)
	jne	.LBB4_38
# BB#1:
	movq	40(%rbx), %r15
	movq	%rbx, %rdi
	callq	doemit
	movq	24(%rbx), %rax
	movq	72(%rbx), %rcx
	movq	(%rax,%r15,8), %r15
	cmpq	%r14, %rcx
	jl	.LBB4_3
# BB#2:
	incq	%rcx
	movq	%rcx, 72(%rbx)
.LBB4_3:
	movq	152(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_5
# BB#4:
	incq	%rcx
	movq	%rcx, 152(%rbx)
.LBB4_5:
	movq	80(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_7
# BB#6:
	incq	%rcx
	movq	%rcx, 80(%rbx)
.LBB4_7:
	movq	160(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_9
# BB#8:
	incq	%rcx
	movq	%rcx, 160(%rbx)
.LBB4_9:
	movq	88(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_11
# BB#10:
	incq	%rcx
	movq	%rcx, 88(%rbx)
.LBB4_11:
	movq	168(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_13
# BB#12:
	incq	%rcx
	movq	%rcx, 168(%rbx)
.LBB4_13:
	movq	96(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_15
# BB#14:
	incq	%rcx
	movq	%rcx, 96(%rbx)
.LBB4_15:
	movq	176(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_17
# BB#16:
	incq	%rcx
	movq	%rcx, 176(%rbx)
.LBB4_17:
	movq	104(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_19
# BB#18:
	incq	%rcx
	movq	%rcx, 104(%rbx)
.LBB4_19:
	movq	184(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_21
# BB#20:
	incq	%rcx
	movq	%rcx, 184(%rbx)
.LBB4_21:
	movq	112(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_23
# BB#22:
	incq	%rcx
	movq	%rcx, 112(%rbx)
.LBB4_23:
	movq	192(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_25
# BB#24:
	incq	%rcx
	movq	%rcx, 192(%rbx)
.LBB4_25:
	movq	120(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_27
# BB#26:
	incq	%rcx
	movq	%rcx, 120(%rbx)
.LBB4_27:
	movq	200(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_29
# BB#28:
	incq	%rcx
	movq	%rcx, 200(%rbx)
.LBB4_29:
	movq	128(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_31
# BB#30:
	incq	%rcx
	movq	%rcx, 128(%rbx)
.LBB4_31:
	movq	208(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_33
# BB#32:
	incq	%rcx
	movq	%rcx, 208(%rbx)
.LBB4_33:
	movq	136(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_35
# BB#34:
	incq	%rcx
	movq	%rcx, 136(%rbx)
.LBB4_35:
	movq	216(%rbx), %rcx
	cmpq	%r14, %rcx
	jl	.LBB4_37
# BB#36:
	incq	%rcx
	movq	%rcx, 216(%rbx)
.LBB4_37:
	leaq	8(%rax,%r14,8), %rdi
	leaq	(%rax,%r14,8), %rsi
	movq	40(%rbx), %rax
	subq	%r14, %rax
	leaq	-8(,%rax,8), %rdx
	callq	memmove
	movq	24(%rbx), %rax
	movq	%r15, (%rax,%r14,8)
.LBB4_38:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	doinsert, .Lfunc_end4-doinsert
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI5_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.p2align	4, 0x90
	.type	p_bracket,@function
p_bracket:                              # @p_bracket
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 144
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	(%rbp), %rbx
	leaq	5(%rbx), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB5_3
# BB#1:
	movl	$2550136832, %r15d      # imm = 0x98000000
	movl	$.L.str, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_13
# BB#2:
	movl	$.L.str.1, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_18
.LBB5_3:                                # %.thread189
	movq	56(%rbp), %rcx
	movslq	20(%rcx), %r12
	leal	1(%r12), %eax
	movl	%eax, 20(%rcx)
	movslq	16(%rcx), %r15
	movslq	48(%rbp), %rbx
	cmpl	%ebx, %r12d
	jge	.LBB5_5
# BB#4:
	movq	%rbp, %r14
	jmp	.LBB5_27
.LBB5_5:
	addq	$8, %rbx
	movl	%ebx, 48(%rbp)
	movq	24(%rcx), %rdi
	movq	%rbx, %rsi
	shlq	$5, %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_10
# BB#6:
	shrq	$3, %rbx
	imulq	%r15, %rbx
	movq	56(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	32(%rcx), %rdi
	movq	%rbx, %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_10
# BB#7:
	movl	%r12d, %ecx
	movq	56(%rbp), %rdx
	movq	%rax, 32(%rdx)
	testl	%ecx, %ecx
	jle	.LBB5_26
# BB#8:                                 # %.lr.ph.i
	movq	24(%rdx), %rsi
	testb	$1, %cl
	jne	.LBB5_23
# BB#9:
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB5_24
	jmp	.LBB5_26
.LBB5_10:
	movq	56(%rbp), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	56(%rbp), %rax
	movq	$0, 24(%rax)
	movq	32(%rax), %rdi
	callq	free
	movq	56(%rbp), %rax
	movq	$0, 32(%rax)
	cmpl	$0, 16(%rbp)
	jne	.LBB5_12
# BB#11:
	movl	$12, 16(%rbp)
.LBB5_12:                               # %allocset.exit.thread
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rbp)
	jmp	.LBB5_254
.LBB5_13:
	cmpl	$0, 16(%rbp)
	jne	.LBB5_253
# BB#14:
	movq	32(%rbp), %rcx
	movq	%rbp, %r12
	cmpq	%rcx, 40(%rbp)
	jl	.LBB5_246
# BB#15:
	leaq	1(%rcx), %rax
	shrq	$63, %rax
	leaq	1(%rcx,%rax), %rax
	sarq	%rax
	leaq	(%rax,%rax,2), %r14
	cmpq	%r14, %rcx
	jge	.LBB5_246
# BB#16:
	movq	%r12, %rbx
	movq	24(%rbx), %rdi
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_243
# BB#17:
	movq	%rax, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	(%rbx), %rbx
	jmp	.LBB5_246
.LBB5_18:
	cmpl	$0, 16(%rbp)
	jne	.LBB5_253
# BB#19:
	movq	32(%rbp), %rcx
	movq	%rbp, %r12
	cmpq	%rcx, 40(%rbp)
	jl	.LBB5_251
# BB#20:
	leaq	1(%rcx), %rax
	shrq	$63, %rax
	leaq	1(%rcx,%rax), %rax
	sarq	%rax
	leaq	(%rax,%rax,2), %r14
	cmpq	%r14, %rcx
	jge	.LBB5_251
# BB#21:
	movq	%r12, %rbx
	movq	24(%rbx), %rdi
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_248
# BB#22:
	movq	%rax, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	(%rbx), %rbx
	jmp	.LBB5_251
.LBB5_23:
	movq	%rax, (%rsi)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB5_26
.LBB5_24:                               # %.lr.ph.i.new
	movq	%rdx, %rdi
	shlq	$5, %rdi
	leaq	32(%rsi,%rdi), %rsi
	.p2align	4, 0x90
.LBB5_25:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %edi
	sarl	$31, %edi
	shrl	$29, %edi
	addl	%edx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	imulq	%r15, %rdi
	addq	%rax, %rdi
	movq	%rdi, -32(%rsi)
	leal	1(%rdx), %edi
	sarl	$31, %edi
	shrl	$29, %edi
	leal	1(%rdx,%rdi), %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	imulq	%r15, %rdi
	addq	%rax, %rdi
	movq	%rdi, (%rsi)
	addq	$2, %rdx
	addq	$64, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB5_25
.LBB5_26:                               # %._crit_edge.i
	subq	%r15, %rbx
	addq	%rbx, %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	%rbp, %r14
	movq	56(%rbp), %rcx
.LBB5_27:                               # %allocset.exit
	movq	24(%rcx), %rax
	movq	%r12, %rdx
	shlq	$5, %rdx
	leaq	(%rax,%rdx), %rbp
	movl	%r12d, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%r12d, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %r15
	addq	32(%rcx), %r15
	movq	%r15, (%rax,%rdx)
	andb	$7, %r12b
	movl	$1, %esi
	movl	%r12d, %ecx
	shll	%cl, %esi
	movb	%sil, 8(%rax,%rdx)
	movb	$0, 9(%rax,%rdx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rax,%rdx)
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB5_254
# BB#28:
	movq	(%r14), %rcx
	movq	8(%r14), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB5_32
# BB#29:
	cmpb	$94, (%rcx)
	jne	.LBB5_32
# BB#30:
	incq	%rcx
	movq	%rcx, (%r14)
	movl	$1, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB5_33
.LBB5_32:
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
.LBB5_33:
	leaq	9(%rax,%rdx), %r8
	cmpq	%rdi, %rcx
	jae	.LBB5_38
# BB#34:
	cmpb	$93, (%rcx)
	jne	.LBB5_36
# BB#35:
	incq	%rcx
	movq	%rcx, (%r14)
	orb	%sil, 93(%r15)
	addb	$93, (%r8)
	jmp	.LBB5_38
.LBB5_36:
	cmpb	$45, (%rcx)
	jne	.LBB5_38
# BB#37:
	incq	%rcx
	movq	%rcx, (%r14)
	orb	%sil, 45(%r15)
	addb	$45, (%r8)
.LBB5_38:                               # %p_b_term.exit.preheader
	leaq	8(%rax,%rdx), %r11
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	jae	.LBB5_165
# BB#39:                                # %.lr.ph161
	leaq	16(%rax,%rdx), %r13
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 64(%rsp)         # 16-byte Spill
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB5_164
	.p2align	4, 0x90
.LBB5_40:                               #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%r12), %rcx
	cmpq	%rbx, %rcx
	jae	.LBB5_43
# BB#41:                                #   in Loop: Header=BB5_164 Depth=1
	cmpb	$45, %al
	jne	.LBB5_43
# BB#42:                                #   in Loop: Header=BB5_164 Depth=1
	cmpb	$93, (%rcx)
	je	.LBB5_166
.LBB5_43:                               # %.thread
                                        #   in Loop: Header=BB5_164 Depth=1
	cmpq	%rbx, %r12
	jae	.LBB5_47
# BB#44:                                #   in Loop: Header=BB5_164 Depth=1
	cmpl	$91, %eax
	je	.LBB5_119
# BB#45:                                #   in Loop: Header=BB5_164 Depth=1
	cmpl	$45, %eax
	jne	.LBB5_47
	jmp	.LBB5_208
	.p2align	4, 0x90
.LBB5_119:                              #   in Loop: Header=BB5_164 Depth=1
	cmpq	%rbx, %rcx
	jae	.LBB5_47
# BB#120:                               #   in Loop: Header=BB5_164 Depth=1
	movb	(%rcx), %al
	jmp	.LBB5_48
	.p2align	4, 0x90
.LBB5_47:                               #   in Loop: Header=BB5_164 Depth=1
	xorl	%eax, %eax
.LBB5_48:                               # %.thread.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movsbl	%al, %eax
	cmpl	$61, %eax
	je	.LBB5_98
# BB#49:                                # %.thread.i
                                        #   in Loop: Header=BB5_164 Depth=1
	cmpl	$58, %eax
	jne	.LBB5_113
# BB#50:                                #   in Loop: Header=BB5_164 Depth=1
	addq	$2, %r12
	movq	%r12, (%r14)
	cmpq	%rbx, %r12
	jb	.LBB5_54
# BB#51:                                #   in Loop: Header=BB5_164 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB5_53
# BB#52:                                #   in Loop: Header=BB5_164 Depth=1
	movl	$7, 16(%r14)
.LBB5_53:                               # %seterr.exit79.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movl	$nuls, %ebx
	movl	$nuls, %r12d
.LBB5_54:                               #   in Loop: Header=BB5_164 Depth=1
	movsbq	(%r12), %rbp
	cmpq	$93, %rbp
	je	.LBB5_56
# BB#55:                                #   in Loop: Header=BB5_164 Depth=1
	cmpb	$45, %bpl
	jne	.LBB5_130
.LBB5_56:                               #   in Loop: Header=BB5_164 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB5_58
# BB#57:                                #   in Loop: Header=BB5_164 Depth=1
	movl	$4, 16(%r14)
.LBB5_58:                               # %.thread98.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movl	$nuls, %r12d
	movl	$nuls, %r15d
.LBB5_59:                               # %.loopexit37.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	subq	%r12, %r15
	movl	$.L.str.2, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_61
# BB#60:                                # %.loopexit37.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses, %ecx
	movl	$cclasses, %eax
	je	.LBB5_83
.LBB5_61:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.5, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_63
# BB#62:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+24, %ecx
	movl	$cclasses+24, %eax
	je	.LBB5_83
.LBB5_63:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.7, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_65
# BB#64:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+48, %ecx
	movl	$cclasses+48, %eax
	je	.LBB5_83
.LBB5_65:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.9, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_67
# BB#66:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+72, %ecx
	movl	$cclasses+72, %eax
	je	.LBB5_83
.LBB5_67:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.11, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_69
# BB#68:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+96, %ecx
	movl	$cclasses+96, %eax
	je	.LBB5_83
.LBB5_69:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.13, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_71
# BB#70:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+120, %ecx
	movl	$cclasses+120, %eax
	je	.LBB5_83
.LBB5_71:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.15, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_73
# BB#72:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+144, %ecx
	movl	$cclasses+144, %eax
	je	.LBB5_83
.LBB5_73:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.17, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_75
# BB#74:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+168, %ecx
	movl	$cclasses+168, %eax
	je	.LBB5_83
.LBB5_75:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.19, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_77
# BB#76:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+192, %ecx
	movl	$cclasses+192, %eax
	je	.LBB5_83
.LBB5_77:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.21, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_79
# BB#78:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+216, %ecx
	movl	$cclasses+216, %eax
	je	.LBB5_83
.LBB5_79:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.23, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$5, %r15
	jne	.LBB5_81
# BB#80:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+240, %ecx
	movl	$cclasses+240, %eax
	je	.LBB5_83
.LBB5_81:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$.L.str.25, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	cmpq	$6, %r15
	jne	.LBB5_121
# BB#82:                                #   in Loop: Header=BB5_164 Depth=1
	testl	%eax, %eax
	movl	$cclasses+264, %ecx
	movl	$cclasses+264, %eax
	jne	.LBB5_121
	.p2align	4, 0x90
.LBB5_83:                               #   in Loop: Header=BB5_164 Depth=1
	cmpq	$0, (%rcx)
	je	.LBB5_121
# BB#84:                                #   in Loop: Header=BB5_164 Depth=1
	movq	8(%rax), %rcx
	movb	(%rcx), %dl
	testb	%dl, %dl
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB5_87
# BB#85:                                # %.lr.ph44.i.i.preheader
                                        #   in Loop: Header=BB5_164 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB5_86:                               # %.lr.ph44.i.i
                                        #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %ebx
	movq	(%r15), %rsi
	movzbl	%dl, %edx
	orb	%bl, (%rsi,%rdx)
	addb	%dl, (%rdi)
	movzbl	(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB5_86
.LBB5_87:                               # %._crit_edge.i.i123
                                        #   in Loop: Header=BB5_164 Depth=1
	movq	16(%rax), %rbx
	cmpb	$0, (%rbx)
	movl	$nuls, %r12d
	je	.LBB5_124
	.p2align	4, 0x90
.LBB5_88:                               # %.lr.ph.i84.i
                                        #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rbp
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rbp,%rax), %rsi
	movq	%rsi, (%r13)
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_90
# BB#89:                                #   in Loop: Header=BB5_88 Depth=2
	callq	cli_realloc
	testq	%rax, %rax
	jne	.LBB5_91
	jmp	.LBB5_92
	.p2align	4, 0x90
.LBB5_90:                               #   in Loop: Header=BB5_88 Depth=2
	movq	%rsi, %rdi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB5_92
.LBB5_91:                               #   in Loop: Header=BB5_88 Depth=2
	movq	%rax, 8(%r13)
	leaq	-1(%rax,%rbp), %rdi
	movl	$1, %edx
	subq	%rbp, %rdx
	addq	(%r13), %rdx
	movq	%rbx, %rsi
	callq	cli_strlcpy
	jmp	.LBB5_97
	.p2align	4, 0x90
.LBB5_92:                               #   in Loop: Header=BB5_88 Depth=2
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_94
# BB#93:                                #   in Loop: Header=BB5_88 Depth=2
	callq	free
.LBB5_94:                               #   in Loop: Header=BB5_88 Depth=2
	movq	$0, 8(%r13)
	cmpl	$0, 16(%r14)
	jne	.LBB5_96
# BB#95:                                #   in Loop: Header=BB5_88 Depth=2
	movl	$12, 16(%r14)
.LBB5_96:                               # %seterr.exit.i.i86.i
                                        #   in Loop: Header=BB5_88 Depth=2
	movd	%r12, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
.LBB5_97:                               # %mcadd.exit.i.i
                                        #   in Loop: Header=BB5_88 Depth=2
	movq	%rbx, %rdi
	callq	strlen
	cmpb	$0, 1(%rbx,%rax)
	leaq	1(%rbx,%rax), %rbx
	jne	.LBB5_88
	jmp	.LBB5_124
	.p2align	4, 0x90
.LBB5_98:                               #   in Loop: Header=BB5_164 Depth=1
	addq	$2, %r12
	movq	%r12, (%r14)
	cmpq	%rbx, %r12
	jb	.LBB5_102
# BB#99:                                #   in Loop: Header=BB5_164 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB5_101
# BB#100:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$7, 16(%r14)
.LBB5_101:                              # %seterr.exit89.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movl	$nuls, %ebx
	movl	$nuls, %r12d
.LBB5_102:                              #   in Loop: Header=BB5_164 Depth=1
	movb	(%r12), %al
	cmpb	$93, %al
	je	.LBB5_104
# BB#103:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$45, %al
	jne	.LBB5_133
.LBB5_104:                              #   in Loop: Header=BB5_164 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB5_106
# BB#105:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$3, 16(%r14)
.LBB5_106:                              # %.thread192
                                        #   in Loop: Header=BB5_164 Depth=1
	movdqa	64(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%r14)
.LBB5_107:                              # %seterr.exit31.i.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movl	$nuls, %eax
.LBB5_108:                              # %p_b_eclass.exit.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	xorl	%eax, %eax
.LBB5_109:                              # %p_b_eclass.exit.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movb	(%r11), %cl
	movq	(%r15), %rdx
	movzbl	%al, %eax
	orb	%cl, (%rdx,%rax)
	addb	%al, (%r8)
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	jae	.LBB5_224
# BB#110:                               #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%r12), %rax
	cmpq	%rbx, %rax
	jae	.LBB5_227
# BB#111:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$61, (%r12)
	jne	.LBB5_227
# BB#112:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$93, (%rax)
	je	.LBB5_128
	jmp	.LBB5_227
	.p2align	4, 0x90
.LBB5_113:                              #   in Loop: Header=BB5_164 Depth=1
	movq	%r14, %rdi
	callq	p_b_symbol
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	%eax, %ebx
	movq	(%r14), %rcx
	movq	8(%r14), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB5_157
# BB#114:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$45, (%rcx)
	movl	%ebx, %eax
	jne	.LBB5_157
# BB#115:                               #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%rcx), %rsi
	cmpq	%rdx, %rsi
	movl	%ebx, %eax
	jae	.LBB5_157
# BB#116:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$93, (%rsi)
	movl	%ebx, %eax
	je	.LBB5_157
# BB#117:                               #   in Loop: Header=BB5_164 Depth=1
	movq	%rsi, (%r14)
	cmpb	$45, (%rsi)
	jne	.LBB5_156
# BB#118:                               #   in Loop: Header=BB5_164 Depth=1
	addq	$2, %rcx
	movq	%rcx, (%r14)
	movb	$45, %al
	cmpb	%al, %bl
	jle	.LBB5_158
	jmp	.LBB5_208
.LBB5_121:                              # %.thread36.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB5_123
# BB#122:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$4, 16(%r14)
.LBB5_123:                              # %seterr.exit.i.i122
                                        #   in Loop: Header=BB5_164 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB5_124:                              # %p_b_cclass.exit.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	jae	.LBB5_229
# BB#125:                               #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%r12), %rax
	cmpq	%rbx, %rax
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jae	.LBB5_232
# BB#126:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$58, (%r12)
	jne	.LBB5_232
# BB#127:                               #   in Loop: Header=BB5_164 Depth=1
	cmpb	$93, (%rax)
	jne	.LBB5_232
.LBB5_128:                              #   in Loop: Header=BB5_164 Depth=1
	addq	$2, %r12
	movq	%r12, (%r14)
	cmpq	%rbx, %r12
	jb	.LBB5_164
	jmp	.LBB5_166
.LBB5_130:                              #   in Loop: Header=BB5_164 Depth=1
	cmpq	%rbx, %r12
	jae	.LBB5_148
# BB#131:                               # %.lr.ph47.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	testb	$4, 1(%rcx,%rbp,2)
	jne	.LBB5_149
# BB#132:                               #   in Loop: Header=BB5_164 Depth=1
	movq	%r12, %r15
	jmp	.LBB5_59
.LBB5_133:                              #   in Loop: Header=BB5_164 Depth=1
	movq	%r14, 48(%rsp)          # 8-byte Spill
	cmpq	%rbx, %r12
	movq	%r12, %r14
	jae	.LBB5_141
# BB#134:                               # %.lr.ph.i.preheader.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%r12), %r14
	cmpq	%rbx, %r14
	jb	.LBB5_136
	jmp	.LBB5_140
	.p2align	4, 0x90
.LBB5_135:                              # %.thread32.thread.i.i.i..lr.ph.i.i124_crit_edge
                                        #   in Loop: Header=BB5_136 Depth=2
	movzbl	(%r14), %eax
	movq	%rcx, %r14
.LBB5_136:                              # %.lr.ph.i.i124
                                        #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$61, %al
	jne	.LBB5_138
# BB#137:                               #   in Loop: Header=BB5_136 Depth=2
	cmpb	$93, (%r14)
	je	.LBB5_161
.LBB5_138:                              # %.thread32.thread.i.i.i
                                        #   in Loop: Header=BB5_136 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
	leaq	1(%r14), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB5_135
# BB#139:                               # %.thread32.thread38.i.i.i.loopexit
                                        #   in Loop: Header=BB5_164 Depth=1
	incq	%r14
.LBB5_140:                              # %.thread32.thread38.i.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
.LBB5_141:                              # %.loopexit.i.i.i
                                        #   in Loop: Header=BB5_164 Depth=1
	cmpq	%rbx, %r14
	jae	.LBB5_162
.LBB5_142:                              #   in Loop: Header=BB5_164 Depth=1
	subq	%r12, %r14
	movslq	%r14d, %r15
	movl	$cnames+16, %ebp
	movl	$.L.str.27, %ebx
	.p2align	4, 0x90
.LBB5_143:                              #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB5_145
# BB#144:                               #   in Loop: Header=BB5_143 Depth=2
	cmpb	$0, (%rbx,%r15)
	je	.LBB5_154
.LBB5_145:                              #   in Loop: Header=BB5_143 Depth=2
	movq	(%rbp), %rbx
	addq	$16, %rbp
	testq	%rbx, %rbx
	jne	.LBB5_143
# BB#146:                               #   in Loop: Header=BB5_164 Depth=1
	cmpl	$1, %r14d
	jne	.LBB5_152
# BB#147:                               #   in Loop: Header=BB5_164 Depth=1
	movb	(%r12), %al
	jmp	.LBB5_155
.LBB5_148:                              #   in Loop: Header=BB5_164 Depth=1
	movq	%r12, %r15
	jmp	.LBB5_59
.LBB5_149:                              # %.lr.ph217.preheader
                                        #   in Loop: Header=BB5_164 Depth=1
	leaq	1(%r12), %rcx
	.p2align	4, 0x90
.LBB5_150:                              # %.lr.ph217
                                        #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r15
	movq	%r15, (%r14)
	cmpq	%rbx, %r15
	jae	.LBB5_59
# BB#151:                               # %._crit_edge172
                                        #   in Loop: Header=BB5_150 Depth=2
	movq	(%rax), %rdx
	movsbq	(%r15), %rsi
	leaq	1(%r15), %rcx
	testb	$4, 1(%rdx,%rsi,2)
	jne	.LBB5_150
	jmp	.LBB5_59
.LBB5_152:                              #   in Loop: Header=BB5_164 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 16(%r14)
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jne	.LBB5_107
# BB#153:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$3, 16(%r14)
	jmp	.LBB5_107
.LBB5_154:                              #   in Loop: Header=BB5_164 Depth=1
	movb	-8(%rbp), %al
.LBB5_155:                              # %p_b_eclass.exit.i
                                        #   in Loop: Header=BB5_164 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB5_109
.LBB5_156:                              #   in Loop: Header=BB5_164 Depth=1
	movq	%r14, %rdi
	callq	p_b_symbol
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	.p2align	4, 0x90
.LBB5_157:                              #   in Loop: Header=BB5_164 Depth=1
	cmpb	%al, %bl
	jg	.LBB5_208
.LBB5_158:                              # %.lr.ph.i125
                                        #   in Loop: Header=BB5_164 Depth=1
	movsbl	%al, %eax
	movsbl	%bl, %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB5_159:                              #   Parent Loop BB5_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ecx
	movzbl	(%r11), %edx
	movq	(%r15), %rsi
	movzbl	%cl, %edi
	orb	%dl, (%rsi,%rdi)
	addb	%bl, (%r8)
	incb	%bl
	cmpl	%eax, %ecx
	jl	.LBB5_159
# BB#160:                               # %p_b_term.exit.backedge.loopexit
                                        #   in Loop: Header=BB5_164 Depth=1
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	jb	.LBB5_164
	jmp	.LBB5_166
.LBB5_161:                              # %.loopexit.i.i.i.loopexit
                                        #   in Loop: Header=BB5_164 Depth=1
	decq	%r14
	cmpq	%rbx, %r14
	jb	.LBB5_142
.LBB5_162:                              #   in Loop: Header=BB5_164 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 16(%r14)
	movl	$nuls, %eax
	jne	.LBB5_108
# BB#163:                               #   in Loop: Header=BB5_164 Depth=1
	movl	$7, 16(%r14)
	jmp	.LBB5_108
	.p2align	4, 0x90
.LBB5_164:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_159 Depth 2
                                        #     Child Loop BB5_150 Depth 2
                                        #     Child Loop BB5_86 Depth 2
                                        #     Child Loop BB5_88 Depth 2
                                        #     Child Loop BB5_136 Depth 2
                                        #     Child Loop BB5_143 Depth 2
	movsbl	(%r12), %eax
	cmpl	$93, %eax
	jne	.LBB5_40
	jmp	.LBB5_166
.LBB5_165:
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB5_166:                              # %.critedge
	cmpq	%rbx, %r12
	jae	.LBB5_169
# BB#167:
	cmpb	$45, (%r12)
	jne	.LBB5_169
# BB#168:
	incq	%r12
	movq	%r12, (%r14)
	movb	(%r11), %al
	movq	(%r15), %rcx
	orb	%al, 45(%rcx)
	addb	$45, (%r8)
	movq	(%r14), %r12
	movq	8(%r14), %rbx
.LBB5_169:
	cmpq	%rbx, %r12
	jae	.LBB5_235
# BB#170:
	leaq	1(%r12), %rax
	movq	%rax, (%r14)
	cmpb	$93, (%r12)
	jne	.LBB5_235
# BB#171:
	cmpl	$0, 16(%r14)
	movq	56(%r14), %rcx
	jne	.LBB5_238
# BB#172:
	movq	%r14, %r12
	testb	$2, 40(%rcx)
	je	.LBB5_187
# BB#173:
	movslq	16(%rcx), %rbx
	testq	%rbx, %rbx
	jle	.LBB5_187
# BB#174:                               # %.lr.ph160
	decq	%rbx
	.p2align	4, 0x90
.LBB5_175:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	movzbl	%bl, %ebp
	movzbl	(%r11), %r14d
	testb	(%r15,%rbp), %r14b
	je	.LBB5_186
# BB#176:                               #   in Loop: Header=BB5_175 Depth=1
	callq	__ctype_b_loc
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	je	.LBB5_186
# BB#177:                               #   in Loop: Header=BB5_175 Depth=1
	movzwl	(%rax,%rbp,2), %eax
	testb	$1, %ah
	jne	.LBB5_181
# BB#178:                               #   in Loop: Header=BB5_175 Depth=1
	testb	$2, %ah
	movl	%ebx, %eax
	je	.LBB5_184
# BB#179:                               #   in Loop: Header=BB5_175 Depth=1
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebp, %eax
	ja	.LBB5_184
# BB#180:                               #   in Loop: Header=BB5_175 Depth=1
	callq	__ctype_toupper_loc
	jmp	.LBB5_183
.LBB5_181:                              #   in Loop: Header=BB5_175 Depth=1
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebp, %eax
	ja	.LBB5_184
# BB#182:                               #   in Loop: Header=BB5_175 Depth=1
	callq	__ctype_tolower_loc
.LBB5_183:                              # %othercase.exit
                                        #   in Loop: Header=BB5_175 Depth=1
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%rax), %rax
	movzbl	(%rax,%rbp,4), %eax
.LBB5_184:                              # %othercase.exit
                                        #   in Loop: Header=BB5_175 Depth=1
	movsbl	%al, %ecx
	cmpq	%rcx, %rbx
	je	.LBB5_186
# BB#185:                               #   in Loop: Header=BB5_175 Depth=1
	movzbl	%al, %eax
	orb	%r14b, (%r15,%rax)
	addb	%al, (%r8)
	.p2align	4, 0x90
.LBB5_186:                              # %.backedge
                                        #   in Loop: Header=BB5_175 Depth=1
	leaq	-1(%rbx), %rax
	incq	%rbx
	cmpq	$1, %rbx
	movq	%rax, %rbx
	movq	32(%rsp), %r15          # 8-byte Reload
	jg	.LBB5_175
.LBB5_187:                              # %.loopexit
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	movq	%r12, %r14
	je	.LBB5_197
# BB#188:
	movq	56(%r14), %rax
	movl	16(%rax), %edi
	testl	%edi, %edi
	jle	.LBB5_195
# BB#189:                               # %.lr.ph.preheader
	leal	-1(%rdi), %ecx
	.p2align	4, 0x90
.LBB5_190:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	decl	%edi
	movq	(%r15), %rdx
	movzbl	%cl, %esi
	movzbl	(%rdx,%rsi), %ebx
	movzbl	(%r11), %eax
	testb	%bl, %al
	je	.LBB5_192
# BB#191:                               #   in Loop: Header=BB5_190 Depth=1
	notb	%al
	andb	%al, %bl
	movb	%bl, (%rdx,%rsi)
	movzbl	(%r8), %edx
	subl	%edi, %edx
	jmp	.LBB5_193
.LBB5_192:                              #   in Loop: Header=BB5_190 Depth=1
	orb	%bl, %al
	movb	%al, (%rdx,%rsi)
	movzbl	(%r8), %edx
	addl	%ecx, %edx
.LBB5_193:                              #   in Loop: Header=BB5_190 Depth=1
	movb	%dl, (%r8)
	leal	-1(%rcx), %eax
	incl	%ecx
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB5_190
# BB#194:                               # %._crit_edge.loopexit
	movq	56(%r14), %rax
.LBB5_195:                              # %._crit_edge
	testb	$8, 40(%rax)
	je	.LBB5_197
# BB#196:
	movb	(%r11), %al
	notb	%al
	movq	(%r15), %rcx
	andb	%al, 10(%rcx)
	addb	$-10, (%r8)
.LBB5_197:
	movq	56(%r14), %r9
	movslq	16(%r9), %r10
	testq	%r10, %r10
	je	.LBB5_210
# BB#198:                               # %.lr.ph.i129
	movq	(%r15), %rdx
	movb	(%r11), %bl
	cmpq	$1, %r10
	movl	$1, %edi
	cmovaq	%r10, %rdi
	cmpq	$8, %rdi
	jb	.LBB5_201
# BB#199:                               # %min.iters.checked
	movq	%rdi, %rsi
	andq	$-8, %rsi
	je	.LBB5_201
# BB#200:                               # %vector.scevcheck
	cmpq	$1, %r10
	movl	$1, %eax
	cmovaq	%r10, %rax
	decq	%rax
	cmpq	$255, %rax
	jbe	.LBB5_271
.LBB5_201:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_202:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%sil, %eax
	movzbl	(%rdx,%rax), %eax
	andb	%bl, %al
	cmpb	$1, %al
	sbbl	$-1, %ebp
	incq	%rsi
	cmpq	%r10, %rsi
	jb	.LBB5_202
.LBB5_203:                              # %nch.exit
	cmpl	$1, %ebp
	jne	.LBB5_210
# BB#204:                               # %.lr.ph.i131.preheader
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.LBB5_205:                              # %.lr.ph.i131
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %ecx
	testb	(%rdx,%rcx), %bl
	jne	.LBB5_266
# BB#206:                               #   in Loop: Header=BB5_205 Depth=1
	incq	%rax
	addl	$16777216, %ebp         # imm = 0x1000000
	cmpq	%r10, %rax
	jb	.LBB5_205
# BB#207:
	xorl	%ebp, %ebp
	jmp	.LBB5_267
.LBB5_208:
	cmpl	$0, 16(%r14)
	jne	.LBB5_234
# BB#209:
	movl	$11, 16(%r14)
	jmp	.LBB5_234
.LBB5_210:                              # %nch.exit.thread
	movslq	20(%r9), %r11
	testq	%r11, %r11
	jle	.LBB5_247
# BB#211:                               # %.lr.ph45.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	(%rax), %r8b
	movq	24(%r9), %r14
	shlq	$5, %r11
	addq	%r14, %r11
	testl	%r10d, %r10d
	je	.LBB5_220
.LBB5_212:                              # %.lr.ph45.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_215 Depth 2
	cmpq	%r15, %r14
	je	.LBB5_218
# BB#213:                               # %.lr.ph45.split.us.i
                                        #   in Loop: Header=BB5_212 Depth=1
	cmpb	%r8b, 9(%r14)
	jne	.LBB5_218
# BB#214:                               # %.lr.ph.us.i
                                        #   in Loop: Header=BB5_212 Depth=1
	movq	(%r14), %rbp
	movq	%r15, %rax
	movb	8(%r14), %r15b
	movq	(%rax), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	(%rax), %cl
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_215:                              #   Parent Loop BB5_212 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%dil, %esi
	testb	(%rbp,%rsi), %r15b
	setne	%dl
	testb	(%rbx,%rsi), %cl
	setne	%al
	xorb	%dl, %al
	jne	.LBB5_217
# BB#216:                               #   in Loop: Header=BB5_215 Depth=2
	incq	%rdi
	cmpq	%r10, %rdi
	jb	.LBB5_215
.LBB5_217:                              # %._crit_edge.us.i
                                        #   in Loop: Header=BB5_212 Depth=1
	cmpq	%r10, %rdi
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB5_255
.LBB5_218:                              #   in Loop: Header=BB5_212 Depth=1
	addq	$32, %r14
	cmpq	%r11, %r14
	jb	.LBB5_212
	jmp	.LBB5_247
.LBB5_220:                              # %.lr.ph45.split.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %r15
	je	.LBB5_222
# BB#221:                               # %.lr.ph45.split.i
                                        #   in Loop: Header=BB5_220 Depth=1
	cmpb	%r8b, 9(%r14)
	je	.LBB5_255
.LBB5_222:                              #   in Loop: Header=BB5_220 Depth=1
	addq	$32, %r14
	cmpq	%r11, %r14
	jb	.LBB5_220
.LBB5_247:
	movq	%r15, %r14
	cmpl	$0, 16(%r12)
	jne	.LBB5_254
	jmp	.LBB5_262
.LBB5_224:
	cmpl	$0, 16(%r14)
	jne	.LBB5_226
# BB#225:
	movl	$7, 16(%r14)
.LBB5_226:                              # %.thread101.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
.LBB5_227:                              # %.loopexit199
	cmpl	$0, 16(%r14)
	jne	.LBB5_234
# BB#228:
	movl	$3, 16(%r14)
	jmp	.LBB5_234
.LBB5_229:
	cmpl	$0, 16(%r14)
	jne	.LBB5_231
# BB#230:
	movl	$7, 16(%r14)
.LBB5_231:                              # %.thread99.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB5_232:                              # %.loopexit200
	cmpl	$0, 16(%r14)
	jne	.LBB5_234
# BB#233:
	movl	$4, 16(%r14)
.LBB5_234:                              # %seterr.exit.i121
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
.LBB5_235:                              # %.thread193
	cmpl	$0, 16(%r14)
	jne	.LBB5_237
# BB#236:
	movl	$7, 16(%r14)
.LBB5_237:                              # %.thread194
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r14)
	movq	56(%r14), %rcx
.LBB5_238:
	movslq	20(%rcx), %rax
	shlq	$5, %rax
	addq	24(%rcx), %rax
	movslq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_241
# BB#239:                               # %.lr.ph.i126.preheader
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_240:                              # %.lr.ph.i126
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r11), %ebx
	notb	%bl
	movq	(%r15), %rdi
	movzbl	%sil, %ebp
	andb	%bl, (%rdi,%rbp)
	addb	%dl, (%r8)
	incq	%rsi
	decb	%dl
	cmpq	%rcx, %rsi
	jb	.LBB5_240
.LBB5_241:                              # %._crit_edge.i127
	addq	$-32, %rax
	cmpq	%r15, %rax
	jne	.LBB5_254
# BB#242:
	movq	56(%r14), %rax
	decl	20(%rax)
	jmp	.LBB5_254
.LBB5_243:
	cmpl	$0, 16(%rbx)
	movq	%rbx, %rax
	jne	.LBB5_245
# BB#244:
	movl	$12, 16(%rax)
.LBB5_245:                              # %seterr.exit.i.i115
	movl	$nuls, %ebx
	movd	%rbx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rax)
.LBB5_246:                              # %enlarge.exit.i116
	movq	%r12, %rbp
	movq	24(%rbp), %rax
	movq	40(%rbp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbp)
	jmp	.LBB5_252
.LBB5_248:
	cmpl	$0, 16(%rbx)
	movq	%rbx, %rax
	jne	.LBB5_250
# BB#249:
	movl	$12, 16(%rax)
.LBB5_250:                              # %seterr.exit.i.i118
	movl	$nuls, %ebx
	movd	%rbx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rax)
.LBB5_251:                              # %enlarge.exit.i119
	movq	%r12, %rbp
	movq	24(%rbp), %rax
	movq	40(%rbp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbp)
	addq	$134217728, %r15        # imm = 0x8000000
.LBB5_252:                              # %doemit.exit117
	movq	%r15, (%rax,%rcx,8)
.LBB5_253:                              # %doemit.exit117
	addq	$6, %rbx
	movq	%rbx, (%rbp)
.LBB5_254:                              # %freeset.exit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_255:                              # %.us-lcssa.us.i
	testl	%r10d, %r10d
	je	.LBB5_259
# BB#256:                               # %.lr.ph.i.i.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB5_257:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %edx
	notb	%dl
	movq	(%r15), %rsi
	movzbl	%cl, %edi
	andb	%dl, (%rsi,%rdi)
	addb	%al, (%rbp)
	incq	%rcx
	decb	%al
	cmpq	%r10, %rcx
	jb	.LBB5_257
# BB#258:                               # %._crit_edge.i.i.loopexit
	movq	56(%r12), %r9
.LBB5_259:                              # %._crit_edge.i.i
	addq	$-32, %r11
	cmpq	%r15, %r11
	jne	.LBB5_261
# BB#260:
	decl	20(%r9)
.LBB5_261:                              # %freezeset.exit
	cmpl	$0, 16(%r12)
	jne	.LBB5_254
.LBB5_262:
	subq	24(%r9), %r14
	shrq	$5, %r14
	movslq	%r14d, %rbx
	movq	32(%r12), %rax
	cmpq	%rax, 40(%r12)
	jl	.LBB5_294
# BB#263:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %rbp
	cmpq	%rbp, %rax
	jge	.LBB5_294
# BB#264:
	movq	24(%r12), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_291
# BB#265:
	movq	%rax, 24(%r12)
	movq	%rbp, 32(%r12)
	jmp	.LBB5_294
.LBB5_266:
	sarl	$24, %ebp
.LBB5_267:                              # %firstch.exit
	movq	88(%r9), %r15
	testb	$2, 40(%r9)
	jne	.LBB5_269
# BB#268:                               # %firstch.exit._crit_edge
	movzbl	%bpl, %r13d
	movq	8(%rsp), %r9            # 8-byte Reload
	cmpl	$0, 16(%r14)
	jne	.LBB5_300
	jmp	.LBB5_286
.LBB5_269:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%bpl, %r13d
	movzwl	(%rax,%r13,2), %eax
	testb	$4, %ah
	jne	.LBB5_273
# BB#270:
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	cmpl	$0, 16(%r14)
	jne	.LBB5_300
	jmp	.LBB5_286
.LBB5_271:                              # %vector.ph
	movzbl	%bl, %eax
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%rsi), %rax
	movq	%rax, %r8
	shrq	$3, %r8
	btl	$3, %eax
	jb	.LBB5_277
# BB#272:                               # %vector.body.prol
	movd	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movd	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pand	%xmm0, %xmm1
	movdqa	.LCPI5_0(%rip), %xmm3   # xmm3 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pand	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	pxor	%xmm3, %xmm3
	pcmpeqd	%xmm3, %xmm1
	movdqa	.LCPI5_1(%rip), %xmm4   # xmm4 = [1,1,1,1]
	pandn	%xmm4, %xmm1
	pcmpeqd	%xmm3, %xmm2
	pandn	%xmm4, %xmm2
	movl	$8, %ebp
	testq	%r8, %r8
	jne	.LBB5_278
	jmp	.LBB5_280
.LBB5_273:
	testb	$1, %ah
	movq	8(%rsp), %r9            # 8-byte Reload
	jne	.LBB5_281
# BB#274:
	testb	$2, %ah
	movl	%ebp, %eax
	movq	16(%rsp), %r11          # 8-byte Reload
	je	.LBB5_284
# BB#275:
	movl	%r13d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%r13d, %eax
	ja	.LBB5_284
# BB#276:
	callq	__ctype_toupper_loc
	jmp	.LBB5_283
.LBB5_277:
	pxor	%xmm1, %xmm1
	xorl	%ebp, %ebp
	pxor	%xmm2, %xmm2
	testq	%r8, %r8
	je	.LBB5_280
.LBB5_278:                              # %vector.ph.new
	movdqa	.LCPI5_0(%rip), %xmm3   # xmm3 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pxor	%xmm4, %xmm4
	movdqa	.LCPI5_1(%rip), %xmm5   # xmm5 = [1,1,1,1]
.LBB5_279:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	andl	$248, %eax
	movd	(%rdx,%rax), %xmm6      # xmm6 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1],xmm6[2],xmm0[2],xmm6[3],xmm0[3],xmm6[4],xmm0[4],xmm6[5],xmm0[5],xmm6[6],xmm0[6],xmm6[7],xmm0[7]
	punpcklwd	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1],xmm6[2],xmm0[2],xmm6[3],xmm0[3]
	movd	4(%rdx,%rax), %xmm7     # xmm7 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1],xmm7[2],xmm0[2],xmm7[3],xmm0[3],xmm7[4],xmm0[4],xmm7[5],xmm0[5],xmm7[6],xmm0[6],xmm7[7],xmm0[7]
	punpcklwd	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1],xmm7[2],xmm0[2],xmm7[3],xmm0[3]
	pand	%xmm0, %xmm6
	pand	%xmm3, %xmm6
	pand	%xmm0, %xmm7
	pand	%xmm3, %xmm7
	pcmpeqd	%xmm4, %xmm6
	pandn	%xmm5, %xmm6
	pcmpeqd	%xmm4, %xmm7
	pandn	%xmm5, %xmm7
	paddd	%xmm1, %xmm6
	paddd	%xmm2, %xmm7
	leal	8(%rbp), %eax
	andl	$248, %eax
	movd	(%rdx,%rax), %xmm1      # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movd	4(%rdx,%rax), %xmm2     # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pand	%xmm0, %xmm1
	pand	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	pcmpeqd	%xmm4, %xmm1
	pandn	%xmm5, %xmm1
	pcmpeqd	%xmm4, %xmm2
	pandn	%xmm5, %xmm2
	paddd	%xmm6, %xmm1
	paddd	%xmm7, %xmm2
	addq	$16, %rbp
	cmpq	%rbp, %rsi
	jne	.LBB5_279
.LBB5_280:                              # %middle.block
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ebp
	cmpq	%rsi, %rdi
	jne	.LBB5_202
	jmp	.LBB5_203
.LBB5_281:
	movl	%r13d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%r13d, %eax
	movq	16(%rsp), %r11          # 8-byte Reload
	ja	.LBB5_284
# BB#282:
	callq	__ctype_tolower_loc
.LBB5_283:                              # %othercase.exit141
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r13,4), %eax
.LBB5_284:                              # %othercase.exit141
	movsbl	%al, %eax
	cmpl	%ebp, %eax
	jne	.LBB5_290
# BB#285:
	cmpl	$0, 16(%r14)
	jne	.LBB5_300
.LBB5_286:
	movq	32(%r14), %rax
	cmpq	%rax, 40(%r14)
	movq	%r14, %rbx
	jl	.LBB5_299
# BB#287:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %r14
	cmpq	%r14, %rax
	jge	.LBB5_299
# BB#288:
	movq	24(%rbx), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB5_295
# BB#289:
	movq	%rax, 24(%rbx)
	movq	%r14, 32(%rbx)
	jmp	.LBB5_298
.LBB5_290:
	movups	(%r14), %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	leaq	29(%rsp), %rax
	movq	%rax, (%r14)
	leaq	31(%rsp), %rax
	movq	%rax, 8(%r14)
	movb	%bpl, 29(%rsp)
	movb	$93, 30(%rsp)
	movb	$0, 31(%rsp)
	movq	%r14, %rdi
	callq	p_bracket
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movdqa	48(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%r14)
	jmp	.LBB5_302
.LBB5_291:
	cmpl	$0, 16(%r12)
	jne	.LBB5_293
# BB#292:
	movl	$12, 16(%r12)
.LBB5_293:                              # %seterr.exit.i.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
.LBB5_294:                              # %enlarge.exit.i
	orq	$805306368, %rbx        # imm = 0x30000000
	movq	24(%r12), %rax
	movq	40(%r12), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%r12)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB5_254
.LBB5_295:
	cmpl	$0, 16(%rbx)
	jne	.LBB5_297
# BB#296:
	movl	$12, 16(%rbx)
.LBB5_297:                              # %seterr.exit.i.i142
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rbx)
.LBB5_298:                              # %enlarge.exit.i143
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB5_299:                              # %enlarge.exit.i143
	orl	$268435456, %r13d       # imm = 0x10000000
	movq	24(%rbx), %rax
	movq	40(%rbx), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r13, (%rax,%rcx,8)
	movq	%rbx, %r14
.LBB5_300:                              # %doemit.exit144
	movslq	%ebp, %rax
	cmpb	$0, (%r15,%rax)
	jne	.LBB5_302
# BB#301:
	movq	56(%r14), %rcx
	movl	84(%rcx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 84(%rcx)
	movb	%dl, (%r15,%rax)
.LBB5_302:                              # %ordinary.exit
	movq	56(%r14), %rcx
	movslq	20(%rcx), %r8
	shlq	$5, %r8
	addq	24(%rcx), %r8
	movslq	16(%rcx), %rcx
	testq	%rcx, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB5_305
# BB#303:                               # %.lr.ph.i133.preheader
	xorl	%edx, %edx
	xorl	%esi, %esi
.LBB5_304:                              # %.lr.ph.i133
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r11), %ebx
	notb	%bl
	movq	(%rax), %rdi
	movzbl	%sil, %ebp
	andb	%bl, (%rdi,%rbp)
	addb	%dl, (%r9)
	incq	%rsi
	decb	%dl
	cmpq	%rcx, %rsi
	jb	.LBB5_304
.LBB5_305:                              # %._crit_edge.i135
	addq	$-32, %r8
	cmpq	%rax, %r8
	jne	.LBB5_254
# BB#306:
	movq	56(%r12), %rax
	decl	20(%rax)
	jmp	.LBB5_254
.Lfunc_end5:
	.size	p_bracket, .Lfunc_end5-p_bracket
	.cfi_endproc

	.p2align	4, 0x90
	.type	ordinary,@function
ordinary:                               # @ordinary
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 80
.Lcfi73:
	.cfi_offset %rbx, -48
.Lcfi74:
	.cfi_offset %r12, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	56(%rbx), %rax
	movq	88(%rax), %r14
	testb	$2, 40(%rax)
	jne	.LBB6_9
# BB#1:                                 # %._crit_edge
	movzbl	%r15b, %ebp
	cmpl	$0, 16(%rbx)
	jne	.LBB6_21
	jmp	.LBB6_3
.LBB6_9:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%r15b, %ebp
	movzwl	(%rax,%rbp,2), %eax
	testb	$4, %ah
	je	.LBB6_2
# BB#10:
	testb	$1, %ah
	jne	.LBB6_11
# BB#13:
	testb	$2, %ah
	movl	%r15d, %eax
	je	.LBB6_17
# BB#14:
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebp, %eax
	ja	.LBB6_17
# BB#15:
	callq	__ctype_toupper_loc
	jmp	.LBB6_16
.LBB6_11:
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	movl	%ebp, %eax
	ja	.LBB6_17
# BB#12:
	callq	__ctype_tolower_loc
.LBB6_16:                               # %othercase.exit
	movq	(%rax), %rax
	movl	(%rax,%rbp,4), %eax
.LBB6_17:                               # %othercase.exit
	movsbl	%al, %eax
	cmpl	%r15d, %eax
	jne	.LBB6_18
.LBB6_2:
	cmpl	$0, 16(%rbx)
	jne	.LBB6_21
.LBB6_3:
	movq	32(%rbx), %rax
	cmpq	%rax, 40(%rbx)
	jl	.LBB6_20
# BB#4:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %r12
	cmpq	%r12, %rax
	jge	.LBB6_20
# BB#5:
	movq	24(%rbx), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB6_6
# BB#19:
	movq	%rax, 24(%rbx)
	movq	%r12, 32(%rbx)
	jmp	.LBB6_20
.LBB6_18:
	movups	(%rbx), %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	leaq	13(%rsp), %rax
	movq	%rax, (%rbx)
	leaq	15(%rsp), %rax
	movq	%rax, 8(%rbx)
	movb	%r15b, 13(%rsp)
	movb	$93, 14(%rsp)
	movb	$0, 15(%rsp)
	movq	%rbx, %rdi
	callq	p_bracket
	movdqa	16(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%rbx)
	jmp	.LBB6_23
.LBB6_6:
	cmpl	$0, 16(%rbx)
	jne	.LBB6_8
# BB#7:
	movl	$12, 16(%rbx)
.LBB6_8:                                # %seterr.exit.i.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rbx)
.LBB6_20:                               # %enlarge.exit.i
	orl	$268435456, %ebp        # imm = 0x10000000
	movq	24(%rbx), %rax
	movq	40(%rbx), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbx)
	movq	%rbp, (%rax,%rcx,8)
.LBB6_21:                               # %doemit.exit
	movslq	%r15d, %rax
	cmpb	$0, (%r14,%rax)
	jne	.LBB6_23
# BB#22:
	movq	56(%rbx), %rcx
	movl	84(%rcx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 84(%rcx)
	movb	%dl, (%r14,%rax)
.LBB6_23:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	ordinary, .Lfunc_end6-ordinary
	.cfi_endproc

	.p2align	4, 0x90
	.type	repeat,@function
repeat:                                 # @repeat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 112
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r12
	cmpl	$0, 16(%rdi)
	je	.LBB7_1
.LBB7_98:                               # %doemit.exit86
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_1:                                # %.lr.ph.lr.ph
	movq	40(%rdi), %r13
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
.LBB7_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
                                        #       Child Loop BB7_5 Depth 3
	xorl	%r15d, %r15d
	cmpl	$256, %ecx              # imm = 0x100
	sete	%r15b
	orl	$2, %r15d
	cmpl	$2, %ecx
	cmovll	%ecx, %r15d
	movq	%r12, %r14
	movl	%ecx, 20(%rsp)          # 4-byte Spill
.LBB7_3:                                # %.outer
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_5 Depth 3
	movq	%r14, %rbp
	movq	%r13, %r14
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_4:                                # %dupl.exit105.thread
                                        #   in Loop: Header=BB7_5 Depth=3
	decl	%ebx
	movq	%r14, %rbp
.LBB7_5:                                #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%eax, %eax
	cmpl	$256, %ebx              # imm = 0x100
	sete	%al
	orl	$2, %eax
	cmpl	$2, %ebx
	cmovll	%ebx, %eax
	leal	(%r15,%rax,8), %eax
	cmpl	$19, %eax
	ja	.LBB7_67
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=3
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_7:                                #   in Loop: Header=BB7_5 Depth=3
	movq	%r14, %r13
	subq	%rbp, %r13
	je	.LBB7_4
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=2
	testq	%r13, %r13
	jle	.LBB7_14
# BB#9:                                 #   in Loop: Header=BB7_3 Depth=2
	movq	32(%rdi), %rax
	addq	%r13, %rax
	movq	%rdi, %r12
	movq	24(%r12), %rdi
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(,%rax,8), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%rax, 24(%r12)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r12)
	movq	%r12, %rdi
	jmp	.LBB7_14
.LBB7_11:                               #   in Loop: Header=BB7_3 Depth=2
	cmpl	$0, 16(%r12)
	movq	%r12, %rdi
	jne	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_3 Depth=2
	movl	$12, 16(%rdi)
.LBB7_13:                               # %seterr.exit.i.i103
                                        #   in Loop: Header=BB7_3 Depth=2
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
.LBB7_14:                               # %dupl.exit105
                                        #   in Loop: Header=BB7_3 Depth=2
	movq	24(%rdi), %rax
	movq	40(%rdi), %rcx
	leaq	(%rax,%rcx,8), %rdi
	leaq	(%rax,%rbp,8), %rsi
	leaq	(,%r13,8), %rdx
	callq	memmove
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	40(%rdi), %r13
	movq	%r13, 40(%rdi)
	decl	%ebx
	cmpl	$0, 16(%rdi)
	movl	20(%rsp), %ecx          # 4-byte Reload
	je	.LBB7_3
	jmp	.LBB7_98
.LBB7_15:                               #   in Loop: Header=BB7_2 Depth=1
	movl	$1, %edx
	subq	%rbp, %rdx
	addq	%r14, %rdx
	movl	$2013265920, %esi       # imm = 0x78000000
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, %rcx
	callq	doinsert
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 16(%rdi)
	jne	.LBB7_50
# BB#16:                                #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rdx
	movq	%rdx, %rbx
	subq	%rbp, %rbx
	movq	32(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, %rdx
	jl	.LBB7_32
# BB#17:                                #   in Loop: Header=BB7_2 Depth=1
	leaq	1(%rax), %rdx
	shrq	$63, %rdx
	leaq	1(%rax,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %r15
	cmpq	%r15, %rax
	jge	.LBB7_32
# BB#18:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%rdi, %r12
	movq	24(%r12), %rdi
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_29
# BB#19:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%rax, 24(%r12)
	movq	%r15, 32(%r12)
	movl	16(%r12), %ecx
	movq	%r12, %rdi
	jmp	.LBB7_32
.LBB7_20:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, %r15
	subq	%rbp, %r15
	movq	%r14, %r13
	je	.LBB7_28
# BB#21:                                #   in Loop: Header=BB7_2 Depth=1
	testq	%r15, %r15
	jle	.LBB7_27
# BB#22:                                #   in Loop: Header=BB7_2 Depth=1
	movq	32(%rdi), %r12
	addq	%r15, %r12
	movq	%rdi, %r13
	movq	24(%r13), %rdi
	leaq	(,%r12,8), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%rax, 24(%r13)
	movq	%r12, 32(%r13)
	movq	%r13, %rdi
	jmp	.LBB7_27
.LBB7_24:                               #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, 16(%r13)
	movq	%r13, %rdi
	jne	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$12, 16(%rdi)
.LBB7_26:                               # %seterr.exit.i.i95
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
.LBB7_27:                               # %enlarge.exit.i96
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rdi), %rax
	movq	40(%rdi), %rcx
	leaq	(%rax,%rcx,8), %rdi
	leaq	(%rax,%rbp,8), %rsi
	leaq	(,%r15,8), %rdx
	callq	memmove
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	40(%rdi), %r15
	movq	%r15, 40(%rdi)
	movq	%r15, %r13
	movl	20(%rsp), %ecx          # 4-byte Reload
.LBB7_28:                               # %dupl.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	decl	%ebx
	movq	%r14, %r12
	jmp	.LBB7_60
.LBB7_29:                               #   in Loop: Header=BB7_2 Depth=1
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	movq	%r12, %rdi
	jne	.LBB7_31
# BB#30:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$12, 16(%rdi)
	movl	$12, %ecx
.LBB7_31:                               # %seterr.exit.i.i87
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
.LBB7_32:                               # %doemit.exit89
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$2281701376, %eax       # imm = 0x88000000
	leaq	-134217728(%rax), %rax
	orq	%rax, %rbx
	movq	24(%rdi), %rax
	movq	40(%rdi), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movq	%rbx, (%rax,%rdx,8)
	testl	%ecx, %ecx
	jne	.LBB7_50
# BB#33:                                #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rcx
	subq	%rbp, %rcx
	movq	(%rax,%rbp,8), %rdx
	movl	$4160749568, %esi       # imm = 0xF8000000
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, (%rax,%rbp,8)
	movq	32(%rdi), %rcx
	cmpq	%rcx, 40(%rdi)
	jl	.LBB7_37
# BB#34:                                #   in Loop: Header=BB7_2 Depth=1
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB7_37
# BB#35:                                #   in Loop: Header=BB7_2 Depth=1
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_42
# BB#36:                                # %doemit.exit93
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	cmpl	$0, 16(%rdi)
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
	jne	.LBB7_50
	jmp	.LBB7_38
.LBB7_37:                               # %doemit.exit93.thread
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
.LBB7_38:                               #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rcx
	movq	-8(%rax,%rcx,8), %rdx
	movl	$4160749568, %esi       # imm = 0xF8000000
	andq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rdx, -8(%rax,%rcx,8)
	movq	32(%rdi), %rcx
	cmpq	%rcx, 40(%rdi)
	jl	.LBB7_48
# BB#39:                                #   in Loop: Header=BB7_2 Depth=1
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB7_48
# BB#40:                                #   in Loop: Header=BB7_2 Depth=1
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_45
# BB#41:                                #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	jmp	.LBB7_48
.LBB7_42:                               #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 16(%rdi)
	jne	.LBB7_44
# BB#43:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$12, 16(%rdi)
.LBB7_44:                               # %doemit.exit93.thread218
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
	movq	24(%rdi), %rax
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	jmp	.LBB7_49
.LBB7_45:                               #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 16(%rdi)
	jne	.LBB7_47
# BB#46:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$12, 16(%rdi)
.LBB7_47:                               # %seterr.exit.i.i97
                                        #   in Loop: Header=BB7_2 Depth=1
	movdqa	32(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%rdi)
	movq	24(%rdi), %rax
.LBB7_48:                               # %enlarge.exit.i98
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	leaq	134217730(%rdx), %rdx
.LBB7_49:                               # %doemit.exit99
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rdx, (%rax,%rcx,8)
.LBB7_50:                               # %doemit.exit99
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %r12
	subq	%rbp, %r14
	movl	$1, %ebx
	je	.LBB7_54
# BB#51:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rbp
	testq	%r14, %r14
	jle	.LBB7_58
# BB#52:                                #   in Loop: Header=BB7_2 Depth=1
	movq	32(%rdi), %r15
	addq	%r14, %r15
	movq	%rdi, %r13
	movq	24(%r13), %rdi
	leaq	(,%r15,8), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_55
# BB#53:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%rax, 24(%r13)
	movq	%r15, 32(%r13)
	movq	%r13, %rdi
	jmp	.LBB7_58
.LBB7_54:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%r12, %r13
	jmp	.LBB7_59
.LBB7_55:                               #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, 16(%r13)
	movq	%r13, %rdi
	jne	.LBB7_57
# BB#56:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$12, 16(%rdi)
.LBB7_57:                               # %seterr.exit.i.i100
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
.LBB7_58:                               # %enlarge.exit.i101
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rdi), %rax
	movq	40(%rdi), %rcx
	leaq	(%rax,%rcx,8), %rdi
	leaq	(%rax,%rbp,8), %rsi
	leaq	(,%r14,8), %rdx
	callq	memmove
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	40(%rdi), %r14
	movq	%r14, 40(%rdi)
	movq	%r14, %r13
.LBB7_59:                               # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	20(%rsp), %ecx          # 4-byte Reload
.LBB7_60:                               # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB7_2 Depth=1
	decl	%ecx
	cmpl	$0, 16(%rdi)
	je	.LBB7_2
	jmp	.LBB7_98
.LBB7_61:
	movl	$1, %edx
	subq	%rbp, %rdx
	addq	%r14, %rdx
	movl	$2013265920, %esi       # imm = 0x78000000
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ecx, %ebx
	movq	%rbp, %rcx
	callq	doinsert
	leaq	1(%rbp), %rsi
	movl	$1, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ebx, %ecx
	callq	repeat
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 16(%rdi)
	jne	.LBB7_98
# BB#62:
	movq	40(%rdi), %rdx
	movq	%rdx, %rbx
	subq	%rbp, %rbx
	movq	32(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, %rdx
	jl	.LBB7_76
# BB#63:
	leaq	1(%rax), %rdx
	shrq	$63, %rdx
	leaq	1(%rax,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %r14
	cmpq	%r14, %rax
	jge	.LBB7_76
# BB#64:
	movq	%rdi, %r15
	movq	24(%r15), %rdi
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_73
# BB#65:
	movq	%rax, 24(%r15)
	movq	%r14, 32(%r15)
	movl	16(%r15), %ecx
	movq	%r15, %rdi
	jmp	.LBB7_76
.LBB7_66:
	movq	%rbp, 40(%rdi)
	jmp	.LBB7_98
.LBB7_67:                               # %seterr.exit
	movl	$15, 16(%rdi)
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
	jmp	.LBB7_98
.LBB7_68:
	movl	$1, %edx
	subq	%rbp, %rdx
	addq	%r14, %rdx
	movl	$1207959552, %esi       # imm = 0x48000000
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, %rcx
	callq	doinsert
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	$0, 16(%rsi)
	jne	.LBB7_98
# BB#69:
	movq	40(%rsi), %rcx
	movq	%rcx, %rbx
	subq	%rbp, %rbx
	movq	32(%rsi), %rax
	cmpq	%rax, %rcx
	jl	.LBB7_92
# BB#70:
	leaq	1(%rax), %rcx
	shrq	$63, %rcx
	leaq	1(%rax,%rcx), %rcx
	sarq	%rcx
	leaq	(%rcx,%rcx,2), %r14
	cmpq	%r14, %rax
	jge	.LBB7_92
# BB#71:
	movq	%rsi, %rbp
	movq	24(%rbp), %rdi
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_89
# BB#72:
	movq	%rax, 24(%rbp)
	movq	%r14, 32(%rbp)
	movq	%rbp, %rsi
	jmp	.LBB7_92
.LBB7_73:
	movl	16(%r15), %ecx
	testl	%ecx, %ecx
	movq	%r15, %rdi
	jne	.LBB7_75
# BB#74:
	movl	$12, 16(%rdi)
	movl	$12, %ecx
.LBB7_75:                               # %seterr.exit.i.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
.LBB7_76:                               # %doemit.exit
	movl	$2281701376, %eax       # imm = 0x88000000
	leaq	-134217728(%rax), %rax
	orq	%rax, %rbx
	movq	24(%rdi), %rax
	movq	40(%rdi), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movq	%rbx, (%rax,%rdx,8)
	testl	%ecx, %ecx
	jne	.LBB7_98
# BB#77:
	movq	40(%rdi), %rcx
	subq	%rbp, %rcx
	movl	$4160749568, %edx       # imm = 0xF8000000
	andq	(%rax,%rbp,8), %rdx
	orq	%rcx, %rdx
	movq	%rdx, (%rax,%rbp,8)
	movq	32(%rdi), %rcx
	cmpq	%rcx, 40(%rdi)
	jl	.LBB7_81
# BB#78:
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB7_81
# BB#79:
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_86
# BB#80:                                # %doemit.exit82
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	cmpl	$0, 16(%rdi)
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
	jne	.LBB7_98
	jmp	.LBB7_82
.LBB7_81:                               # %doemit.exit82.thread
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	movq	%rdx, (%rax,%rcx,8)
.LBB7_82:
	movq	40(%rdi), %rcx
	movl	$4160749568, %edx       # imm = 0xF8000000
	andq	-8(%rax,%rcx,8), %rdx
	orq	$1, %rdx
	movq	%rdx, -8(%rax,%rcx,8)
	movq	32(%rdi), %rcx
	cmpq	%rcx, 40(%rdi)
	jl	.LBB7_96
# BB#83:
	leaq	1(%rcx), %rdx
	shrq	$63, %rdx
	leaq	1(%rcx,%rdx), %rdx
	sarq	%rdx
	leaq	(%rdx,%rdx,2), %rbx
	cmpq	%rbx, %rcx
	jge	.LBB7_96
# BB#84:
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rax, %rdi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB7_93
# BB#85:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	jmp	.LBB7_96
.LBB7_86:
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	$0, 16(%rsi)
	jne	.LBB7_88
# BB#87:
	movl	$12, 16(%rsi)
.LBB7_88:                               # %doemit.exit82.thread217
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rsi)
	movq	24(%rsi), %rax
	movq	40(%rsi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rsi)
	movl	$2281701376, %edx       # imm = 0x88000000
	jmp	.LBB7_97
.LBB7_89:
	cmpl	$0, 16(%rbp)
	movq	%rbp, %rsi
	jne	.LBB7_91
# BB#90:
	movl	$12, 16(%rsi)
.LBB7_91:                               # %seterr.exit.i.i106
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rsi)
.LBB7_92:                               # %enlarge.exit.i107
	orq	$1342177280, %rbx       # imm = 0x50000000
	movq	24(%rsi), %rax
	movq	40(%rsi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rsi)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB7_98
.LBB7_93:
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 16(%rdi)
	jne	.LBB7_95
# BB#94:
	movl	$12, 16(%rdi)
.LBB7_95:                               # %seterr.exit.i.i84
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%rdi)
	movq	24(%rdi), %rax
.LBB7_96:                               # %enlarge.exit.i85
	movq	40(%rdi), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rdi)
	movl	$2281701376, %edx       # imm = 0x88000000
	addq	$134217730, %rdx        # imm = 0x8000002
.LBB7_97:                               # %doemit.exit86
	movq	%rdx, (%rax,%rcx,8)
	jmp	.LBB7_98
.Lfunc_end7:
	.size	repeat, .Lfunc_end7-repeat
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_66
	.quad	.LBB7_61
	.quad	.LBB7_61
	.quad	.LBB7_61
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_98
	.quad	.LBB7_15
	.quad	.LBB7_68
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_67
	.quad	.LBB7_20
	.quad	.LBB7_7

	.text
	.p2align	4, 0x90
	.type	p_b_symbol,@function
p_b_symbol:                             # @p_b_symbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 80
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %rax
	movq	8(%r12), %r13
	cmpq	%r13, %rax
	jae	.LBB8_1
# BB#6:
	leaq	1(%rax), %rcx
	cmpq	%r13, %rcx
	jae	.LBB8_4
# BB#7:
	cmpb	$91, (%rax)
	jne	.LBB8_4
# BB#8:
	cmpb	$46, (%rcx)
	jne	.LBB8_4
# BB#9:
	leaq	2(%rax), %rbp
	movq	%rbp, (%r12)
	cmpq	%r13, %rbp
	movq	%rbp, %rcx
	jae	.LBB8_18
# BB#10:                                # %.lr.ph.i.preheader
	addq	$3, %rax
	cmpq	%r13, %rax
	jb	.LBB8_13
	jmp	.LBB8_12
.LBB8_1:
	cmpl	$0, 16(%r12)
	jne	.LBB8_3
# BB#2:
	movl	$7, 16(%r12)
.LBB8_3:                                # %.thread
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	movl	$nuls+1, %ecx
.LBB8_4:
	movq	%rcx, (%r12)
	movb	(%rax), %al
.LBB8_5:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.p2align	4, 0x90
.LBB8_15:                               # %.thread32.thread.i
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rax, (%r12)
	incq	%rax
	cmpq	%r13, %rax
	jae	.LBB8_12
.LBB8_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$46, -1(%rax)
	jne	.LBB8_15
# BB#14:                                #   in Loop: Header=BB8_13 Depth=1
	cmpb	$93, (%rax)
	jne	.LBB8_15
# BB#16:                                # %.loopexit.i.loopexit
	decq	%rax
	jmp	.LBB8_17
.LBB8_12:                               # %.thread32.thread38.i
	movq	%rax, (%r12)
.LBB8_17:                               # %.loopexit.i
	movq	%rax, %rcx
.LBB8_18:                               # %.loopexit.i
	cmpq	%r13, %rcx
	jae	.LBB8_19
# BB#25:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	subq	%rbp, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	%ecx, %r15
	movl	$.L.str.27, %ebx
	movl	$cnames+8, %r14d
	.p2align	4, 0x90
.LBB8_26:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB8_34
# BB#27:                                #   in Loop: Header=BB8_26 Depth=1
	cmpb	$0, (%rbx,%r15)
	je	.LBB8_28
.LBB8_34:                               #   in Loop: Header=BB8_26 Depth=1
	movq	8(%r14), %rbx
	addq	$16, %r14
	testq	%rbx, %rbx
	jne	.LBB8_26
# BB#35:
	cmpl	$1, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB8_29
# BB#36:
	cmpl	$0, 16(%r12)
	jne	.LBB8_21
# BB#37:
	movl	$3, 16(%r12)
	jmp	.LBB8_21
.LBB8_19:
	cmpl	$0, 16(%r12)
	jne	.LBB8_21
# BB#20:
	movl	$7, 16(%r12)
.LBB8_21:                               # %seterr.exit.i
	movl	$nuls, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	xorl	%eax, %eax
.LBB8_22:                               # %p_b_coll_elem.exit.thread
	cmpl	$0, 16(%r12)
	jne	.LBB8_24
# BB#23:
	movl	$3, 16(%r12)
.LBB8_24:                               # %seterr.exit21
	movl	$nuls, %ecx
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, (%r12)
	jmp	.LBB8_5
.LBB8_28:
	movq	%r14, %rbp
.LBB8_29:                               # %p_b_coll_elem.exit
	movb	(%rbp), %al
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%r13, %rdx
	jae	.LBB8_22
# BB#30:
	leaq	1(%rdx), %rcx
	cmpq	%r13, %rcx
	jae	.LBB8_22
# BB#31:
	cmpb	$46, (%rdx)
	jne	.LBB8_22
# BB#32:
	cmpb	$93, (%rcx)
	jne	.LBB8_22
# BB#33:
	addq	$2, %rdx
	movq	%rdx, (%r12)
	jmp	.LBB8_5
.Lfunc_end8:
	.size	p_b_symbol, .Lfunc_end8-p_b_symbol
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"[:<:]]"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[:>:]]"
	.size	.L.str.1, 7

	.type	cclasses,@object        # @cclasses
	.section	.rodata,"a",@progbits
	.p2align	4
cclasses:
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.4
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.4
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.4
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.4
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.4
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.4
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.4
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.4
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.4
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.4
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.4
	.quad	0
	.quad	0
	.quad	.L.str.4
	.size	cclasses, 312

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"alnum"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	.size	.L.str.3, 63

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.zero	1
	.size	.L.str.4, 1

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"alpha"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	.size	.L.str.6, 53

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"blank"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" \t"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cntrl"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\007\b\t\n\013\f\r\001\002\003\004\005\006\016\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037\177"
	.size	.L.str.10, 33

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"digit"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"0123456789"
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"graph"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
	.size	.L.str.14, 95

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"lower"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"abcdefghijklmnopqrstuvwxyz"
	.size	.L.str.16, 27

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"print"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ "
	.size	.L.str.18, 96

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"punct"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"space"
	.size	.L.str.21, 6

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\t\n\013\f\r "
	.size	.L.str.22, 7

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"upper"
	.size	.L.str.23, 6

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	.size	.L.str.24, 27

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"xdigit"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"0123456789ABCDEFabcdef"
	.size	.L.str.26, 23

	.type	cnames,@object          # @cnames
	.section	.rodata,"a",@progbits
	.p2align	4
cnames:
	.quad	.L.str.27
	.byte	0                       # 0x0
	.zero	7
	.quad	.L.str.28
	.byte	1                       # 0x1
	.zero	7
	.quad	.L.str.29
	.byte	2                       # 0x2
	.zero	7
	.quad	.L.str.30
	.byte	3                       # 0x3
	.zero	7
	.quad	.L.str.31
	.byte	4                       # 0x4
	.zero	7
	.quad	.L.str.32
	.byte	5                       # 0x5
	.zero	7
	.quad	.L.str.33
	.byte	6                       # 0x6
	.zero	7
	.quad	.L.str.34
	.byte	7                       # 0x7
	.zero	7
	.quad	.L.str.35
	.byte	7                       # 0x7
	.zero	7
	.quad	.L.str.36
	.byte	8                       # 0x8
	.zero	7
	.quad	.L.str.37
	.byte	8                       # 0x8
	.zero	7
	.quad	.L.str.38
	.byte	9                       # 0x9
	.zero	7
	.quad	.L.str.39
	.byte	9                       # 0x9
	.zero	7
	.quad	.L.str.40
	.byte	10                      # 0xa
	.zero	7
	.quad	.L.str.41
	.byte	10                      # 0xa
	.zero	7
	.quad	.L.str.42
	.byte	11                      # 0xb
	.zero	7
	.quad	.L.str.43
	.byte	11                      # 0xb
	.zero	7
	.quad	.L.str.44
	.byte	12                      # 0xc
	.zero	7
	.quad	.L.str.45
	.byte	12                      # 0xc
	.zero	7
	.quad	.L.str.46
	.byte	13                      # 0xd
	.zero	7
	.quad	.L.str.47
	.byte	13                      # 0xd
	.zero	7
	.quad	.L.str.48
	.byte	14                      # 0xe
	.zero	7
	.quad	.L.str.49
	.byte	15                      # 0xf
	.zero	7
	.quad	.L.str.50
	.byte	16                      # 0x10
	.zero	7
	.quad	.L.str.51
	.byte	17                      # 0x11
	.zero	7
	.quad	.L.str.52
	.byte	18                      # 0x12
	.zero	7
	.quad	.L.str.53
	.byte	19                      # 0x13
	.zero	7
	.quad	.L.str.54
	.byte	20                      # 0x14
	.zero	7
	.quad	.L.str.55
	.byte	21                      # 0x15
	.zero	7
	.quad	.L.str.56
	.byte	22                      # 0x16
	.zero	7
	.quad	.L.str.57
	.byte	23                      # 0x17
	.zero	7
	.quad	.L.str.58
	.byte	24                      # 0x18
	.zero	7
	.quad	.L.str.59
	.byte	25                      # 0x19
	.zero	7
	.quad	.L.str.60
	.byte	26                      # 0x1a
	.zero	7
	.quad	.L.str.61
	.byte	27                      # 0x1b
	.zero	7
	.quad	.L.str.62
	.byte	28                      # 0x1c
	.zero	7
	.quad	.L.str.63
	.byte	28                      # 0x1c
	.zero	7
	.quad	.L.str.64
	.byte	29                      # 0x1d
	.zero	7
	.quad	.L.str.65
	.byte	29                      # 0x1d
	.zero	7
	.quad	.L.str.66
	.byte	30                      # 0x1e
	.zero	7
	.quad	.L.str.67
	.byte	30                      # 0x1e
	.zero	7
	.quad	.L.str.68
	.byte	31                      # 0x1f
	.zero	7
	.quad	.L.str.69
	.byte	31                      # 0x1f
	.zero	7
	.quad	.L.str.21
	.byte	32                      # 0x20
	.zero	7
	.quad	.L.str.70
	.byte	33                      # 0x21
	.zero	7
	.quad	.L.str.71
	.byte	34                      # 0x22
	.zero	7
	.quad	.L.str.72
	.byte	35                      # 0x23
	.zero	7
	.quad	.L.str.73
	.byte	36                      # 0x24
	.zero	7
	.quad	.L.str.74
	.byte	37                      # 0x25
	.zero	7
	.quad	.L.str.75
	.byte	38                      # 0x26
	.zero	7
	.quad	.L.str.76
	.byte	39                      # 0x27
	.zero	7
	.quad	.L.str.77
	.byte	40                      # 0x28
	.zero	7
	.quad	.L.str.78
	.byte	41                      # 0x29
	.zero	7
	.quad	.L.str.79
	.byte	42                      # 0x2a
	.zero	7
	.quad	.L.str.80
	.byte	43                      # 0x2b
	.zero	7
	.quad	.L.str.81
	.byte	44                      # 0x2c
	.zero	7
	.quad	.L.str.82
	.byte	45                      # 0x2d
	.zero	7
	.quad	.L.str.83
	.byte	45                      # 0x2d
	.zero	7
	.quad	.L.str.84
	.byte	46                      # 0x2e
	.zero	7
	.quad	.L.str.85
	.byte	46                      # 0x2e
	.zero	7
	.quad	.L.str.86
	.byte	47                      # 0x2f
	.zero	7
	.quad	.L.str.87
	.byte	47                      # 0x2f
	.zero	7
	.quad	.L.str.88
	.byte	48                      # 0x30
	.zero	7
	.quad	.L.str.89
	.byte	49                      # 0x31
	.zero	7
	.quad	.L.str.90
	.byte	50                      # 0x32
	.zero	7
	.quad	.L.str.91
	.byte	51                      # 0x33
	.zero	7
	.quad	.L.str.92
	.byte	52                      # 0x34
	.zero	7
	.quad	.L.str.93
	.byte	53                      # 0x35
	.zero	7
	.quad	.L.str.94
	.byte	54                      # 0x36
	.zero	7
	.quad	.L.str.95
	.byte	55                      # 0x37
	.zero	7
	.quad	.L.str.96
	.byte	56                      # 0x38
	.zero	7
	.quad	.L.str.97
	.byte	57                      # 0x39
	.zero	7
	.quad	.L.str.98
	.byte	58                      # 0x3a
	.zero	7
	.quad	.L.str.99
	.byte	59                      # 0x3b
	.zero	7
	.quad	.L.str.100
	.byte	60                      # 0x3c
	.zero	7
	.quad	.L.str.101
	.byte	61                      # 0x3d
	.zero	7
	.quad	.L.str.102
	.byte	62                      # 0x3e
	.zero	7
	.quad	.L.str.103
	.byte	63                      # 0x3f
	.zero	7
	.quad	.L.str.104
	.byte	64                      # 0x40
	.zero	7
	.quad	.L.str.105
	.byte	91                      # 0x5b
	.zero	7
	.quad	.L.str.106
	.byte	92                      # 0x5c
	.zero	7
	.quad	.L.str.107
	.byte	92                      # 0x5c
	.zero	7
	.quad	.L.str.108
	.byte	93                      # 0x5d
	.zero	7
	.quad	.L.str.109
	.byte	94                      # 0x5e
	.zero	7
	.quad	.L.str.110
	.byte	94                      # 0x5e
	.zero	7
	.quad	.L.str.111
	.byte	95                      # 0x5f
	.zero	7
	.quad	.L.str.112
	.byte	95                      # 0x5f
	.zero	7
	.quad	.L.str.113
	.byte	96                      # 0x60
	.zero	7
	.quad	.L.str.114
	.byte	123                     # 0x7b
	.zero	7
	.quad	.L.str.115
	.byte	123                     # 0x7b
	.zero	7
	.quad	.L.str.116
	.byte	124                     # 0x7c
	.zero	7
	.quad	.L.str.117
	.byte	125                     # 0x7d
	.zero	7
	.quad	.L.str.118
	.byte	125                     # 0x7d
	.zero	7
	.quad	.L.str.119
	.byte	126                     # 0x7e
	.zero	7
	.quad	.L.str.120
	.byte	127                     # 0x7f
	.zero	7
	.zero	16
	.size	cnames, 1536

	.type	.L.str.27,@object       # @.str.27
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.27:
	.asciz	"NUL"
	.size	.L.str.27, 4

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SOH"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"STX"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ETX"
	.size	.L.str.30, 4

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"EOT"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"ENQ"
	.size	.L.str.32, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"ACK"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"BEL"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"alert"
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"BS"
	.size	.L.str.36, 3

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"backspace"
	.size	.L.str.37, 10

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"HT"
	.size	.L.str.38, 3

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"tab"
	.size	.L.str.39, 4

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"LF"
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"newline"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"VT"
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"vertical-tab"
	.size	.L.str.43, 13

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"FF"
	.size	.L.str.44, 3

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"form-feed"
	.size	.L.str.45, 10

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"CR"
	.size	.L.str.46, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"carriage-return"
	.size	.L.str.47, 16

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SO"
	.size	.L.str.48, 3

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SI"
	.size	.L.str.49, 3

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"DLE"
	.size	.L.str.50, 4

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"DC1"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"DC2"
	.size	.L.str.52, 4

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"DC3"
	.size	.L.str.53, 4

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"DC4"
	.size	.L.str.54, 4

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"NAK"
	.size	.L.str.55, 4

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"SYN"
	.size	.L.str.56, 4

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ETB"
	.size	.L.str.57, 4

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"CAN"
	.size	.L.str.58, 4

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"EM"
	.size	.L.str.59, 3

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SUB"
	.size	.L.str.60, 4

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"ESC"
	.size	.L.str.61, 4

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"IS4"
	.size	.L.str.62, 4

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"FS"
	.size	.L.str.63, 3

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"IS3"
	.size	.L.str.64, 4

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"GS"
	.size	.L.str.65, 3

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"IS2"
	.size	.L.str.66, 4

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"RS"
	.size	.L.str.67, 3

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"IS1"
	.size	.L.str.68, 4

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"US"
	.size	.L.str.69, 3

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"exclamation-mark"
	.size	.L.str.70, 17

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"quotation-mark"
	.size	.L.str.71, 15

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"number-sign"
	.size	.L.str.72, 12

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"dollar-sign"
	.size	.L.str.73, 12

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"percent-sign"
	.size	.L.str.74, 13

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"ampersand"
	.size	.L.str.75, 10

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"apostrophe"
	.size	.L.str.76, 11

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"left-parenthesis"
	.size	.L.str.77, 17

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"right-parenthesis"
	.size	.L.str.78, 18

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"asterisk"
	.size	.L.str.79, 9

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"plus-sign"
	.size	.L.str.80, 10

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"comma"
	.size	.L.str.81, 6

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"hyphen"
	.size	.L.str.82, 7

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"hyphen-minus"
	.size	.L.str.83, 13

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"period"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"full-stop"
	.size	.L.str.85, 10

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"slash"
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"solidus"
	.size	.L.str.87, 8

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"zero"
	.size	.L.str.88, 5

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"one"
	.size	.L.str.89, 4

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"two"
	.size	.L.str.90, 4

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"three"
	.size	.L.str.91, 6

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"four"
	.size	.L.str.92, 5

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"five"
	.size	.L.str.93, 5

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"six"
	.size	.L.str.94, 4

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"seven"
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"eight"
	.size	.L.str.96, 6

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"nine"
	.size	.L.str.97, 5

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"colon"
	.size	.L.str.98, 6

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"semicolon"
	.size	.L.str.99, 10

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"less-than-sign"
	.size	.L.str.100, 15

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"equals-sign"
	.size	.L.str.101, 12

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"greater-than-sign"
	.size	.L.str.102, 18

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"question-mark"
	.size	.L.str.103, 14

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"commercial-at"
	.size	.L.str.104, 14

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"left-square-bracket"
	.size	.L.str.105, 20

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"backslash"
	.size	.L.str.106, 10

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"reverse-solidus"
	.size	.L.str.107, 16

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"right-square-bracket"
	.size	.L.str.108, 21

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"circumflex"
	.size	.L.str.109, 11

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"circumflex-accent"
	.size	.L.str.110, 18

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"underscore"
	.size	.L.str.111, 11

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"low-line"
	.size	.L.str.112, 9

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"grave-accent"
	.size	.L.str.113, 13

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"left-brace"
	.size	.L.str.114, 11

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"left-curly-bracket"
	.size	.L.str.115, 19

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"vertical-line"
	.size	.L.str.116, 14

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"right-brace"
	.size	.L.str.117, 12

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"right-curly-bracket"
	.size	.L.str.118, 20

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"tilde"
	.size	.L.str.119, 6

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"DEL"
	.size	.L.str.120, 4

	.type	nuls,@object            # @nuls
	.local	nuls
	.comm	nuls,10,1

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
