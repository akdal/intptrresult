	.text
	.file	"main.bc"
	.globl	DisplayUsage
	.p2align	4, 0x90
	.type	DisplayUsage,@function
DisplayUsage:                           # @DisplayUsage
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	DisplayUsage, .Lfunc_end0-DisplayUsage
	.cfi_endproc

	.globl	ParseInputFile
	.p2align	4, 0x90
	.type	ParseInputFile,@function
ParseInputFile:                         # @ParseInputFile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$712, %rsp              # imm = 0x2C8
.Lcfi6:
	.cfi_def_cfa_offset 768
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	192(%rsp), %rdi
	movl	$.LParseInputFile.parms, %esi
	movl	$520, %edx              # imm = 0x208
	callq	memcpy
	movl	$IMAGE, %edi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$IMAGE, %edi
	movl	$47, %esi
	callq	strrchr
	incq	%rax
	movq	%rax, 224(%rsp)
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_17
# BB#1:                                 # %.critedge21.thread.preheader
	movq	%rsp, %rdi
	movl	$80, %esi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_10
# BB#2:                                 # %.critedge21.thread.preheader
	cmpb	$10, (%rsp)
	je	.LBB1_10
# BB#3:
	movq	%rsp, %r15
	leaq	80(%rsp), %rbx
	leaq	112(%rsp), %r13
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movl	$.L.str.3, %esi
	movq	%r15, %rdi
	callq	strtok
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	xorl	%edi, %edi
	movl	$.L.str.3, %esi
	callq	strtok
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	strcpy
	leaq	192(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_7
# BB#5:                                 #   in Loop: Header=BB1_6 Depth=2
	cmpq	$0, 72(%rbp)
	leaq	40(%rbp), %rbp
	jne	.LBB1_6
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                # %.critedge21
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	32(%rbp), %r14
	testq	%r14, %r14
	je	.LBB1_8
# BB#11:                                #   in Loop: Header=BB1_4 Depth=1
	movsbl	25(%rbp), %eax
	cmpl	$2, %eax
	je	.LBB1_15
# BB#12:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$1, %eax
	je	.LBB1_16
# BB#13:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%eax, %eax
	jne	.LBB1_8
# BB#14:                                #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movl	%eax, (%r14)
	jmp	.LBB1_8
.LBB1_15:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	strtod
	movq	32(%rbp), %rax
	movsd	%xmm0, (%rax)
	jmp	.LBB1_8
.LBB1_16:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	32(%rbp), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movb	$0, -1(%rbp,%rax)
	.p2align	4, 0x90
.LBB1_8:                                # %.critedge21.thread.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$80, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_10
# BB#9:                                 # %.critedge21.thread.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpb	$10, (%rsp)
	jne	.LBB1_4
.LBB1_10:                               # %.critedge
	movq	%r12, %rdi
	callq	fclose
	addq	$712, %rsp              # imm = 0x2C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_17:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	ParseInputFile, .Lfunc_end1-ParseInputFile
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$240, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 272
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	8(%rsi), %rdi
	callq	ParseInputFile
	leaq	16(%rsp), %rbx
	movl	$IMAGE, %esi
	movq	%rbx, %rdi
	callq	PGM_InitImage
	movsd	SIGMA(%rip), %xmm0      # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	VAR_THRESHOLD(%rip), %ebp
	movq	%rbx, %rdi
	callq	PGM_LoadImage
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	$3, %eax
	cmpl	$3, %eax
	ja	.LBB2_6
# BB#1:
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_4:
	movq	24(%rsp), %rsi
	movl	$.L.str.6, %edi
	jmp	.LBB2_5
.LBB2_3:
	movq	24(%rsp), %rsi
	movl	$.L.str.5, %edi
	jmp	.LBB2_5
.LBB2_2:
	movq	24(%rsp), %rsi
	movl	$.L.str.4, %edi
.LBB2_5:
	xorl	%eax, %eax
	callq	printf
.LBB2_6:
	movq	24(%rsp), %rdi
	callq	free
	movq	32(%rsp), %rdi
	callq	free
	movl	$-2, %ebx
.LBB2_10:
	movl	%ebx, %eax
	addq	$240, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_7:
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	callq	PGM_PrintInfo
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	HorzVariance
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	88(%rsp), %rdi
	movl	48(%rsp), %edx
	movl	52(%rsp), %esi
	leaq	96(%rsp), %rcx
	leaq	160(%rsp), %r8
	callq	L_canny
	testl	%eax, %eax
	je	.LBB2_9
# BB#8:
	leaq	160(%rsp), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-3, %ebx
	jmp	.LBB2_10
.LBB2_9:
	leaq	16(%rsp), %r14
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	BuildConnectedComponents
	movq	%rax, (%rsp)
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	EliminateLargeSpreadComponents
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	MergeRowComponents
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	PairComponents
	movq	(%rsp), %rdi
	movq	%r14, %rsi
	callq	ComputeBoundingBoxes
	movq	(%rsp), %rdi
	callq	FreeConnectedComponents
	movq	%r14, %rdi
	callq	PGM_FreeImage
	jmp	.LBB2_10
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_4
	.quad	.LBB2_3
	.quad	.LBB2_2
	.quad	.LBB2_7

	.type	VAR_THRESHOLD,@object   # @VAR_THRESHOLD
	.data
	.globl	VAR_THRESHOLD
	.p2align	2
VAR_THRESHOLD:
	.long	4294967295              # 0xffffffff
	.size	VAR_THRESHOLD, 4

	.type	VSPREAD_THRESHOLD,@object # @VSPREAD_THRESHOLD
	.globl	VSPREAD_THRESHOLD
	.p2align	3
VSPREAD_THRESHOLD:
	.quad	4581421828931458171     # double 0.02
	.size	VSPREAD_THRESHOLD, 8

	.type	SAME_ROW_THRESHOLD,@object # @SAME_ROW_THRESHOLD
	.globl	SAME_ROW_THRESHOLD
	.p2align	2
SAME_ROW_THRESHOLD:
	.long	3                       # 0x3
	.size	SAME_ROW_THRESHOLD, 4

	.type	SAME_ROW_V,@object      # @SAME_ROW_V
	.globl	SAME_ROW_V
	.p2align	2
SAME_ROW_V:
	.long	3                       # 0x3
	.size	SAME_ROW_V, 4

	.type	SAME_ROW_H,@object      # @SAME_ROW_H
	.globl	SAME_ROW_H
	.p2align	2
SAME_ROW_H:
	.long	30                      # 0x1e
	.size	SAME_ROW_H, 4

	.type	MAX_CHAR_SIZE,@object   # @MAX_CHAR_SIZE
	.globl	MAX_CHAR_SIZE
	.p2align	2
MAX_CHAR_SIZE:
	.long	50                      # 0x32
	.size	MAX_CHAR_SIZE, 4

	.type	MIN_CHAR_SIZE,@object   # @MIN_CHAR_SIZE
	.globl	MIN_CHAR_SIZE
	.p2align	2
MIN_CHAR_SIZE:
	.long	5                       # 0x5
	.size	MIN_CHAR_SIZE, 4

	.type	KILL_SMALL_COMP,@object # @KILL_SMALL_COMP
	.globl	KILL_SMALL_COMP
	.p2align	2
KILL_SMALL_COMP:
	.long	1                       # 0x1
	.size	KILL_SMALL_COMP, 4

	.type	SMALL_THRESHOLD,@object # @SMALL_THRESHOLD
	.globl	SMALL_THRESHOLD
	.p2align	2
SMALL_THRESHOLD:
	.long	10                      # 0xa
	.size	SMALL_THRESHOLD, 4

	.type	HVAR_WINDOW,@object     # @HVAR_WINDOW
	.globl	HVAR_WINDOW
	.p2align	2
HVAR_WINDOW:
	.long	10                      # 0xa
	.size	HVAR_WINDOW, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Usage: %s <PGM image file> <sigma> <variance threshold>\n"
	.size	.L.str, 57

	.type	IMAGE,@object           # @IMAGE
	.comm	IMAGE,800,16
	.type	SIGMA,@object           # @SIGMA
	.comm	SIGMA,8,8
	.type	.LParseInputFile.parms,@object # @ParseInputFile.parms
	.section	.rodata,"a",@progbits
	.p2align	4
.LParseInputFile.parms:
	.asciz	"image\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	1                       # 0x1
	.zero	6
	.quad	IMAGE
	.asciz	"sigma\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	2                       # 0x2
	.zero	6
	.quad	SIGMA
	.asciz	"var_thresh\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	VAR_THRESHOLD
	.asciz	"vspread_thresh\000\000\000\000\000\000\000\000\000\000"
	.byte	2                       # 0x2
	.zero	6
	.quad	VSPREAD_THRESHOLD
	.asciz	"same_row_thresh\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	SAME_ROW_THRESHOLD
	.asciz	"same_row_v\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	SAME_ROW_V
	.asciz	"same_row_h\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	SAME_ROW_H
	.asciz	"max_char_size\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	MAX_CHAR_SIZE
	.asciz	"min_char_size\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	MIN_CHAR_SIZE
	.asciz	"kill_small\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	KILL_SMALL_COMP
	.asciz	"small_thresh\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	SMALL_THRESHOLD
	.asciz	"hvar_window\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.byte	0                       # 0x0
	.zero	6
	.quad	HVAR_WINDOW
	.zero	40
	.size	.LParseInputFile.parms, 520

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"r"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s not found. Exiting.\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" \t"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error: %s not found. Exiting.\n"
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Error: %s is not a PGM file. Exiting.\n"
	.size	.L.str.5, 39

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Error: %s has 0 length. Exiting.\n"
	.size	.L.str.6, 34

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Error: '%s' in L_canny(). Exiting.\n"
	.size	.L.str.7, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
