	.text
	.file	"sei.bc"
	.globl	InterpretSEIMessage
	.p2align	4, 0x90
	.type	InterpretSEIMessage,@function
InterpretSEIMessage:                    # @InterpretSEIMessage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdi, %r15
	movl	$1, %r12d
	movabsq	$4294967296, %r13       # imm = 0x100000000
	jmp	.LBB0_1
.LBB0_30:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_stereo_video_info_info
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_22 Depth 2
	movslq	%r12d, %rdx
	leal	1(%rdx), %r14d
	leaq	(%r15,%rdx), %rsi
	leal	2(%rdx), %ebp
	shlq	$32, %rdx
	movl	$-255, %edi
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ecx
	movzbl	(%rsi), %eax
	incl	%r14d
	incq	%rsi
	addq	%r13, %rdx
	addl	$255, %edi
	leal	1(%rcx), %ebp
	cmpl	$255, %eax
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	addl	%edi, %eax
	sarq	$32, %rdx
	movb	(%r15,%rdx), %bl
	xorl	%edx, %edx
	cmpb	$-1, %bl
	jne	.LBB0_6
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	movslq	%ecx, %rsi
	addq	%r15, %rsi
	xorl	%edx, %edx
	movl	%ecx, %r14d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	$255, %edx
	movzbl	(%rsi), %ebx
	incq	%rsi
	incl	%r14d
	cmpb	$-1, %bl
	je	.LBB0_5
.LBB0_6:                                # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movzbl	%bl, %r12d
	addl	%edx, %r12d
	cmpl	$21, %eax
	ja	.LBB0_31
# BB#7:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	%eax, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_25:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.41, %edi
	jmp	.LBB0_17
.LBB0_9:                                #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_buffering_period_info
	jmp	.LBB0_31
.LBB0_10:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_picture_timing_info
	jmp	.LBB0_31
.LBB0_11:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_pan_scan_rect_info
	jmp	.LBB0_31
.LBB0_12:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.36, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$2, %edi
	movl	$.L.str.37, %esi
	movq	%rbx, %rdx
	callq	u_v
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, 6088(%rax)
	movl	%ebp, 6096(%rax)
	jmp	.LBB0_18
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	interpret_dec_ref_pic_marking_repetition_info
	jmp	.LBB0_31
.LBB0_14:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	interpret_spare_pic
	jmp	.LBB0_31
.LBB0_15:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.24, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.25, %edi
	movq	%rbx, %rsi
	callq	ue_v
	cmpl	$4, %eax
	jl	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.25, %edi
	jmp	.LBB0_17
.LBB0_19:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_subsequence_info
	jmp	.LBB0_31
.LBB0_20:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.14, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_18
# BB#21:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.15, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$16, %edi
	movl	$.L.str.16, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$16, %edi
	movl	$.L.str.17, %esi
	movq	%rbx, %rdx
	callq	u_v
	decl	%ebp
	jne	.LBB0_22
	jmp	.LBB0_18
.LBB0_23:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rdi
	addq	%r15, %rdi
	movl	%r12d, %esi
	callq	interpret_subsequence_characteristics_info
	jmp	.LBB0_31
.LBB0_24:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.40, %edi
	jmp	.LBB0_17
.LBB0_26:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbx
	addq	%r15, %rbx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	interpret_motion_constrained_slice_group_set_info
	jmp	.LBB0_27
.LBB0_8:                                # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbx
	addq	%r15, %rbx
.LBB0_27:                               #   in Loop: Header=BB0_1 Depth=1
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	interpret_film_grain_characteristics_info
	jmp	.LBB0_31
.LBB0_28:                               #   in Loop: Header=BB0_1 Depth=1
	movslq	%r14d, %rbp
	addq	%r15, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r12d, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$.L.str.64, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	jne	.LBB0_18
# BB#29:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.65, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.66, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.67, %edi
	.p2align	4, 0x90
.LBB0_17:                               # %interpret_scene_information.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%rbx, %rsi
	callq	ue_v
.LBB0_18:                               # %interpret_scene_information.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB0_31:                               #   in Loop: Header=BB0_1 Depth=1
	addl	%r14d, %r12d
	movslq	%r12d, %rax
	cmpb	$-128, (%r15,%rax)
	jne	.LBB0_1
# BB#32:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	InterpretSEIMessage, .Lfunc_end0-InterpretSEIMessage
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_10
	.quad	.LBB0_11
	.quad	.LBB0_31
	.quad	.LBB0_31
	.quad	.LBB0_31
	.quad	.LBB0_12
	.quad	.LBB0_13
	.quad	.LBB0_14
	.quad	.LBB0_15
	.quad	.LBB0_19
	.quad	.LBB0_20
	.quad	.LBB0_23
	.quad	.LBB0_31
	.quad	.LBB0_31
	.quad	.LBB0_24
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_26
	.quad	.LBB0_8
	.quad	.LBB0_28
	.quad	.LBB0_30

	.text
	.globl	interpret_buffering_period_info
	.p2align	4, 0x90
	.type	interpret_buffering_period_info,@function
interpret_buffering_period_info:        # @interpret_buffering_period_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r15
	movl	%ebp, 12(%r15)
	movq	%r14, 16(%r15)
	movl	$0, 8(%r15)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.74, %edi
	movq	%r15, %rsi
	callq	ue_v
	cltq
	imulq	$3064, %rax, %r14       # imm = 0xBF8
	leaq	SeqParSet(%r14), %rdi
	callq	activate_sps
	cmpl	$0, SeqParSet+2108(%r14)
	je	.LBB1_9
# BB#1:
	cmpl	$0, SeqParSet+2192(%r14)
	je	.LBB1_5
# BB#2:                                 # %.preheader36
	cmpl	$-1, SeqParSet+2196(%r14)
	je	.LBB1_5
# BB#3:                                 # %.lr.ph40
	leaq	SeqParSet+2196(%r14), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movl	396(%rbp), %edi
	incl	%edi
	movl	$.L.str.75, %esi
	movq	%r15, %rdx
	callq	u_v
	movl	396(%rbp), %edi
	incl	%edi
	movl	$.L.str.76, %esi
	movq	%r15, %rdx
	callq	u_v
	incl	%ebx
	movl	(%rbp), %eax
	incl	%eax
	cmpl	%eax, %ebx
	jb	.LBB1_4
.LBB1_5:                                # %.loopexit37
	cmpl	$0, SeqParSet+2608(%r14)
	je	.LBB1_9
# BB#6:                                 # %.preheader
	cmpl	$-1, SeqParSet+2612(%r14)
	je	.LBB1_9
# BB#7:                                 # %.lr.ph
	leaq	SeqParSet+2612(%r14), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movl	396(%rbp), %edi
	incl	%edi
	movl	$.L.str.75, %esi
	movq	%r15, %rdx
	callq	u_v
	movl	396(%rbp), %edi
	incl	%edi
	movl	$.L.str.76, %esi
	movq	%r15, %rdx
	callq	u_v
	incl	%ebx
	movl	(%rbp), %eax
	incl	%eax
	cmpl	%eax, %ebx
	jb	.LBB1_8
.LBB1_9:                                # %.loopexit
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	interpret_buffering_period_info, .Lfunc_end1-interpret_buffering_period_info
	.cfi_endproc

	.globl	interpret_picture_timing_info
	.p2align	4, 0x90
	.type	interpret_picture_timing_info,@function
interpret_picture_timing_info:          # @interpret_picture_timing_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	active_sps(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB2_31
# BB#1:
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%r14d, 12(%rbx)
	movq	%r15, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	2108(%rbp), %eax
	testl	%eax, %eax
	je	.LBB2_30
# BB#2:
	cmpl	$0, 2192(%rbp)
	je	.LBB2_3
# BB#6:
	movl	2596(%rbp), %edi
	movl	2600(%rbp), %r14d
	incl	%edi
	incl	%r14d
.LBB2_9:
	movl	$.L.str.78, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.79, %esi
	movl	%r14d, %edi
	movq	%rbx, %rdx
	callq	u_v
	movq	active_sps(%rip), %rbp
	movl	2108(%rbp), %eax
.LBB2_10:                               # %.critedge
	testl	%eax, %eax
	jne	.LBB2_11
	jmp	.LBB2_30
.LBB2_31:
	movq	stderr(%rip), %rcx
	movl	$.L.str.77, %edi
	movl	$52, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB2_3:
	cmpl	$0, 2608(%rbp)
	je	.LBB2_11
# BB#4:
	cmpl	$0, 2608(%rbp)
	je	.LBB2_5
# BB#7:
	movl	3012(%rbp), %edi
	movl	3016(%rbp), %r14d
	incl	%edi
	incl	%r14d
	cmpl	$0, 2608(%rbp)
	jne	.LBB2_9
	jmp	.LBB2_10
.LBB2_11:                               # %.critedge.thread106
	cmpl	$0, 3028(%rbp)
	je	.LBB2_30
# BB#12:
	movl	$4, %edi
	movl	$.L.str.80, %esi
	movq	%rbx, %rdx
	callq	u_v
	cmpl	$9, %eax
	jae	.LBB2_13
# BB#14:                                # %.lr.ph.preheader
	cltq
	movl	.Lswitch.table(,%rax,4), %r14d
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.82, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_29
# BB#16:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$2, %edi
	movl	$.L.str.83, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.84, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$5, %edi
	movl	$.L.str.85, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.86, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	%eax, %ebp
	movl	$.L.str.87, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.88, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$8, %edi
	movl	$.L.str.89, %esi
	movq	%rbx, %rdx
	callq	u_v
	testl	%ebp, %ebp
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$6, %edi
	movl	$.L.str.90, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$6, %edi
	movl	$.L.str.91, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB2_21:                               #   in Loop: Header=BB2_15 Depth=1
	movl	$5, %edi
	movl	$.L.str.92, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB2_22:                               #   in Loop: Header=BB2_15 Depth=1
	movq	active_sps(%rip), %rax
	cmpl	$0, 2608(%rax)
	je	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_15 Depth=1
	addq	$3020, %rax             # imm = 0xBCC
	jmp	.LBB2_27
	.p2align	4, 0x90
.LBB2_18:                               #   in Loop: Header=BB2_15 Depth=1
	movl	$.L.str.93, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_22
# BB#19:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$6, %edi
	movl	$.L.str.90, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.94, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB2_22
# BB#20:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$6, %edi
	movl	$.L.str.91, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.95, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	jne	.LBB2_21
	jmp	.LBB2_22
	.p2align	4, 0x90
.LBB2_24:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$0, 2192(%rax)
	je	.LBB2_25
# BB#26:                                #   in Loop: Header=BB2_15 Depth=1
	addq	$2604, %rax             # imm = 0xA2C
.LBB2_27:                               #   in Loop: Header=BB2_15 Depth=1
	movl	(%rax), %edi
	testl	%edi, %edi
	jne	.LBB2_28
	jmp	.LBB2_29
.LBB2_25:                               #   in Loop: Header=BB2_15 Depth=1
	movl	$24, %edi
.LBB2_28:                               # %.thread
                                        #   in Loop: Header=BB2_15 Depth=1
	movl	$.L.str.96, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB2_29:                               #   in Loop: Header=BB2_15 Depth=1
	decl	%r14d
	jne	.LBB2_15
	jmp	.LBB2_30
.LBB2_13:                               # %.thread107
	movl	$.L.str.81, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB2_30:                               # %.critedge97
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB2_5:
	movl	$24, %edi
	movl	$24, %r14d
	cmpl	$0, 2608(%rbp)
	jne	.LBB2_9
	jmp	.LBB2_10
.Lfunc_end2:
	.size	interpret_picture_timing_info, .Lfunc_end2-interpret_picture_timing_info
	.cfi_endproc

	.globl	interpret_pan_scan_rect_info
	.p2align	4, 0x90
	.type	interpret_pan_scan_rect_info,@function
interpret_pan_scan_rect_info:           # @interpret_pan_scan_rect_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.26, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	jne	.LBB3_5
# BB#1:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB3_4
# BB#2:                                 # %.lr.ph.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.29, %edi
	movq	%rbx, %rsi
	callq	se_v
	movl	$.L.str.30, %edi
	movq	%rbx, %rsi
	callq	se_v
	movl	$.L.str.31, %edi
	movq	%rbx, %rsi
	callq	se_v
	movl	$.L.str.32, %edi
	movq	%rbx, %rsi
	callq	se_v
	decl	%ebp
	jne	.LBB3_3
.LBB3_4:                                # %._crit_edge
	movl	$.L.str.33, %edi
	movq	%rbx, %rsi
	callq	ue_v
.LBB3_5:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	interpret_pan_scan_rect_info, .Lfunc_end3-interpret_pan_scan_rect_info
	.cfi_endproc

	.globl	interpret_filler_payload_info
	.p2align	4, 0x90
	.type	interpret_filler_payload_info,@function
interpret_filler_payload_info:          # @interpret_filler_payload_info
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cltq
	xorl	%ecx, %ecx
	cmpb	$-1, (%rdi,%rax)
	sete	%cl
	addl	%ecx, %eax
	cmpl	%esi, %eax
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	interpret_filler_payload_info, .Lfunc_end4-interpret_filler_payload_info
	.cfi_endproc

	.globl	interpret_user_data_registered_itu_t_t35_info
	.p2align	4, 0x90
	.type	interpret_user_data_registered_itu_t_t35_info,@function
interpret_user_data_registered_itu_t_t35_info: # @interpret_user_data_registered_itu_t_t35_info
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	interpret_user_data_registered_itu_t_t35_info, .Lfunc_end5-interpret_user_data_registered_itu_t_t35_info
	.cfi_endproc

	.globl	interpret_user_data_unregistered_info
	.p2align	4, 0x90
	.type	interpret_user_data_unregistered_info,@function
interpret_user_data_unregistered_info:  # @interpret_user_data_unregistered_info
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader7
	retq
.Lfunc_end6:
	.size	interpret_user_data_unregistered_info, .Lfunc_end6-interpret_user_data_unregistered_info
	.cfi_endproc

	.globl	interpret_recovery_point_info
	.p2align	4, 0x90
	.type	interpret_recovery_point_info,@function
interpret_recovery_point_info:          # @interpret_recovery_point_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r15, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.36, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$2, %edi
	movl	$.L.str.37, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$1, 6088(%r14)
	movl	%ebp, 6096(%r14)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	interpret_recovery_point_info, .Lfunc_end7-interpret_recovery_point_info
	.cfi_endproc

	.globl	interpret_dec_ref_pic_marking_repetition_info
	.p2align	4, 0x90
	.type	interpret_dec_ref_pic_marking_repetition_info,@function
interpret_dec_ref_pic_marking_repetition_info: # @interpret_dec_ref_pic_marking_repetition_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r14
	movl	%ebp, 12(%r14)
	movq	%r15, 16(%r14)
	movl	$0, 8(%r14)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.38, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	%eax, %ebp
	movl	$.L.str.39, %edi
	movq	%r14, %rsi
	callq	ue_v
	movq	5632(%rbx), %r15
	movl	5804(%rbx), %r12d
	movl	5848(%rbx), %r13d
	movl	5852(%rbx), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	5856(%rbx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%ebp, 5804(%rbx)
	movq	$0, 5632(%rbx)
	movq	%r14, %rdi
	callq	dec_ref_pic_marking
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 5632(%rbx)
	callq	free
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	5632(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_1
# BB#3:                                 # %._crit_edge
	movq	%r15, 5632(%rbx)
	movl	%r12d, 5804(%rbx)
	movl	%r13d, 5848(%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 5852(%rbx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 5856(%rbx)
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end8:
	.size	interpret_dec_ref_pic_marking_repetition_info, .Lfunc_end8-interpret_dec_ref_pic_marking_repetition_info
	.cfi_endproc

	.globl	interpret_spare_pic
	.p2align	4, 0x90
	.type	interpret_spare_pic,@function
interpret_spare_pic:                    # @interpret_spare_pic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 144
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	$0, UsedBits(%rip)
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	%r14d, 12(%rbp)
	movq	%r15, 16(%rbp)
	movl	$0, 8(%rbp)
	movl	$.L.str, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	$.L.str.1, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, %r14d
	leal	1(%r14), %esi
	movl	48(%rbx), %eax
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	52(%rbx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%eax, %ecx
	sarl	$4, %ecx
	leaq	40(%rsp), %rdi
	movl	%esi, 60(%rsp)          # 4-byte Spill
	callq	get_mem3D
	testl	%r14d, %r14d
	js	.LBB9_57
# BB#1:                                 # %.lr.ph247
	movslq	%r14d, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_7 Depth 2
                                        #       Child Loop BB9_9 Depth 3
                                        #     Child Loop BB9_21 Depth 2
                                        #       Child Loop BB9_23 Depth 3
                                        #     Child Loop BB9_14 Depth 2
                                        #       Child Loop BB9_16 Depth 3
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	$.L.str.2, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	$.L.str.3, %edi
	movq	%rbp, %rsi
	callq	ue_v
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB9_12
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpl	$2, %ecx
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB9_19
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpl	$1, %ecx
	movq	32(%rsp), %r9           # 8-byte Reload
	jne	.LBB9_58
# BB#5:                                 # %.preheader205
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	52(%rbx), %ecx
	cmpl	$16, %ecx
	jl	.LBB9_56
# BB#6:                                 # %.preheader202.lr.ph
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	48(%rbx), %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_7:                                # %.preheader202
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_9 Depth 3
	cmpl	$16, %eax
	jl	.LBB9_11
# BB#8:                                 # %.lr.ph237.preheader
                                        #   in Loop: Header=BB9_7 Depth=2
	movq	%rbx, %r15
	xorl	%ebp, %ebp
	movq	%r9, %r12
	movq	48(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph237
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$.L.str.4, %edi
	movq	%rbx, %rsi
	callq	u_1
	movq	40(%rsp), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movb	%al, (%rcx,%rbp)
	incq	%rbp
	movl	48(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%eax, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB9_9
# BB#10:                                # %._crit_edge238.loopexit
                                        #   in Loop: Header=BB9_7 Depth=2
	movl	52(%r15), %ecx
	movq	%r15, %rbx
	movq	%r12, %r9
.LBB9_11:                               # %._crit_edge238
                                        #   in Loop: Header=BB9_7 Depth=2
	incq	%r14
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %r14
	jl	.LBB9_7
	jmp	.LBB9_56
	.p2align	4, 0x90
.LBB9_12:                               # %.preheader204
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	52(%rdi), %edx
	cmpl	$16, %edx
	movq	32(%rsp), %r9           # 8-byte Reload
	jl	.LBB9_56
# BB#13:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	48(%rdi), %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_14:                               # %.preheader
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_16 Depth 3
	cmpl	$16, %ecx
	jl	.LBB9_18
# BB#15:                                # %.lr.ph241.preheader
                                        #   in Loop: Header=BB9_14 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph241
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rsp), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$0, (%rcx,%rdx)
	incq	%rdx
	movl	48(%rdi), %ecx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%ecx, %esi
	sarl	$4, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rdx
	jl	.LBB9_16
# BB#17:                                # %._crit_edge242.loopexit
                                        #   in Loop: Header=BB9_14 Depth=2
	movl	52(%rdi), %edx
.LBB9_18:                               # %._crit_edge242
                                        #   in Loop: Header=BB9_14 Depth=2
	incq	%rax
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.LBB9_14
	jmp	.LBB9_56
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_2 Depth=1
	movl	52(%rbx), %edx
	cmpl	$16, %edx
	movq	32(%rsp), %r9           # 8-byte Reload
	jl	.LBB9_56
# BB#20:                                # %.preheader203.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	48(%rbx), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ecx, %eax
	sarl	$4, %eax
	leal	-1(%rax), %esi
	shrl	$31, %esi
	leal	-1(%rax,%rsi), %r8d
	sarl	%r8d
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%edx, %eax
	sarl	$4, %eax
	leal	-1(%rax), %esi
	shrl	$31, %esi
	leal	-1(%rax,%rsi), %r10d
	sarl	%r10d
	movl	$-1, %eax
	xorl	%r14d, %r14d
	movl	$1, %r13d
	movl	%r10d, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%r8d, %ebp
	xorl	%edi, %edi
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movl	%r8d, %r12d
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movl	%r10d, %r8d
	.p2align	4, 0x90
.LBB9_21:                               # %.preheader203
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_23 Depth 3
	cmpl	$16, %ecx
	jl	.LBB9_55
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_21 Depth=2
	movl	%edi, 64(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_23:                               # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%eax, %eax
	jns	.LBB9_25
# BB#24:                                #   in Loop: Header=BB9_23 Depth=3
	movl	$.L.str.5, %edi
	movl	%esi, 68(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r12d, %r15d
	movl	%r8d, %r12d
	callq	ue_v
	movl	68(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %r8d
	movl	%r15d, %r12d
	movq	32(%rsp), %r9           # 8-byte Reload
.LBB9_25:                               #   in Loop: Header=BB9_23 Depth=3
	movq	40(%rsp), %rcx
	movq	(%rcx,%r9,8), %rcx
	movslq	%r8d, %rdx
	testl	%eax, %eax
	movq	(%rcx,%rdx,8), %rcx
	movslq	%r12d, %rdx
	setle	(%rcx,%rdx)
	cmpl	$-1, %r14d
	jne	.LBB9_32
# BB#26:                                #   in Loop: Header=BB9_23 Depth=3
	testl	%r13d, %r13d
	jne	.LBB9_32
# BB#27:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	%ebp, %r12d
	jle	.LBB9_29
# BB#28:                                #   in Loop: Header=BB9_23 Depth=3
	decl	%r12d
	xorl	%r13d, %r13d
	movl	$-1, %r14d
	jmp	.LBB9_53
	.p2align	4, 0x90
.LBB9_32:                               #   in Loop: Header=BB9_23 Depth=3
	cmpl	$1, %r14d
	jne	.LBB9_39
# BB#33:                                #   in Loop: Header=BB9_23 Depth=3
	testl	%r13d, %r13d
	jne	.LBB9_39
# BB#34:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB9_36
# BB#35:                                #   in Loop: Header=BB9_23 Depth=3
	incl	%r12d
	xorl	%r13d, %r13d
	movl	$1, %r14d
	jmp	.LBB9_53
	.p2align	4, 0x90
.LBB9_29:                               #   in Loop: Header=BB9_23 Depth=3
	testl	%r12d, %r12d
	je	.LBB9_30
# BB#31:                                #   in Loop: Header=BB9_23 Depth=3
	leal	-1(%rbp), %ecx
	xorl	%r13d, %r13d
	cmpl	%ebp, %r12d
	sete	%r13b
	cmovel	%ecx, %r12d
	cmovel	%ecx, %ebp
	movl	$0, %r14d
	movl	$-1, %ecx
	cmovnel	%ecx, %r14d
	jmp	.LBB9_53
	.p2align	4, 0x90
.LBB9_39:                               #   in Loop: Header=BB9_23 Depth=3
	testl	%r14d, %r14d
	jne	.LBB9_46
# BB#40:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	$-1, %r13d
	jne	.LBB9_46
# BB#41:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	16(%rsp), %r8d          # 4-byte Folded Reload
	jle	.LBB9_43
# BB#42:                                #   in Loop: Header=BB9_23 Depth=3
	decl	%r8d
	movl	$-1, %r13d
	xorl	%r14d, %r14d
	jmp	.LBB9_53
.LBB9_36:                               #   in Loop: Header=BB9_23 Depth=3
	movl	48(%rbx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	decl	%edx
	cmpl	%edx, %r12d
	jne	.LBB9_38
# BB#37:                                #   in Loop: Header=BB9_23 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	decl	%ecx
	xorl	%r13d, %r13d
	movl	$-1, %r14d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, %r8d
	jmp	.LBB9_53
.LBB9_46:                               #   in Loop: Header=BB9_23 Depth=3
	testl	%r14d, %r14d
	jne	.LBB9_53
# BB#47:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	$1, %r13d
	jne	.LBB9_53
# BB#48:                                #   in Loop: Header=BB9_23 Depth=3
	cmpl	8(%rsp), %r8d           # 4-byte Folded Reload
	jge	.LBB9_50
# BB#49:                                #   in Loop: Header=BB9_23 Depth=3
	incl	%r8d
	movl	$1, %r13d
	xorl	%r14d, %r14d
	jmp	.LBB9_53
.LBB9_30:                               #   in Loop: Header=BB9_23 Depth=3
	movq	8(%rsp), %rcx           # 8-byte Reload
	incl	%ecx
	movl	$1, %r14d
	xorl	%r12d, %r12d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %r8d
	xorl	%r13d, %r13d
	jmp	.LBB9_53
.LBB9_43:                               #   in Loop: Header=BB9_23 Depth=3
	testl	%r8d, %r8d
	je	.LBB9_44
# BB#45:                                #   in Loop: Header=BB9_23 Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rdx), %ecx
	cmpl	%edx, %r8d
	cmovel	%ecx, %r8d
	cmovel	%ecx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	$0, %r14d
	movl	$-1, %ecx
	cmovel	%ecx, %r14d
	movl	$0, %r13d
	cmovnel	%ecx, %r13d
	jmp	.LBB9_53
.LBB9_38:                               #   in Loop: Header=BB9_23 Depth=3
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	1(%rdx), %ecx
	xorl	%r14d, %r14d
	cmpl	%edx, %r12d
	cmovel	%ecx, %r12d
	cmovel	%ecx, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	setne	%r14b
	movl	$0, %r13d
	movl	$-1, %ecx
	cmovel	%ecx, %r13d
	jmp	.LBB9_53
.LBB9_50:                               #   in Loop: Header=BB9_23 Depth=3
	movl	52(%rbx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	decl	%edx
	cmpl	%edx, %r8d
	jne	.LBB9_52
# BB#51:                                #   in Loop: Header=BB9_23 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	$-1, %r13d
	xorl	%r14d, %r14d
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %r12d
	jmp	.LBB9_53
.LBB9_44:                               #   in Loop: Header=BB9_23 Depth=3
	decl	%ebp
	movl	$1, %r13d
	xorl	%r8d, %r8d
	movl	%ebp, %r12d
	xorl	%r14d, %r14d
	jmp	.LBB9_53
.LBB9_52:                               #   in Loop: Header=BB9_23 Depth=3
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	1(%rdx), %ecx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	cmpl	%edx, %r8d
	sete	%r14b
	cmovel	%ecx, %r8d
	cmovel	%ecx, %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	setne	%r13b
	.p2align	4, 0x90
.LBB9_53:                               #   in Loop: Header=BB9_23 Depth=3
	decl	%eax
	incl	%esi
	movl	48(%rbx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	cmpl	%edx, %esi
	jl	.LBB9_23
# BB#54:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB9_21 Depth=2
	movl	52(%rbx), %edx
	movl	64(%rsp), %edi          # 4-byte Reload
.LBB9_55:                               # %._crit_edge
                                        #   in Loop: Header=BB9_21 Depth=2
	incl	%edi
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %edi
	jl	.LBB9_21
	.p2align	4, 0x90
.LBB9_56:                               # %.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpq	80(%rsp), %r9           # 8-byte Folded Reload
	leaq	1(%r9), %r9
	movq	48(%rsp), %rbp          # 8-byte Reload
	jl	.LBB9_2
.LBB9_57:                               # %._crit_edge248
	movq	40(%rsp), %rdi
	movl	60(%rsp), %esi          # 4-byte Reload
	callq	free_mem3D
	movq	%rbp, %rdi
	callq	free
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_58:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end9:
	.size	interpret_spare_pic, .Lfunc_end9-interpret_spare_pic
	.cfi_endproc

	.globl	interpret_scene_information
	.p2align	4, 0x90
	.type	interpret_scene_information,@function
interpret_scene_information:            # @interpret_scene_information
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.24, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.25, %edi
	movq	%rbx, %rsi
	callq	ue_v
	cmpl	$4, %eax
	jl	.LBB10_2
# BB#1:
	movl	$.L.str.25, %edi
	movq	%rbx, %rsi
	callq	ue_v
.LBB10_2:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end10:
	.size	interpret_scene_information, .Lfunc_end10-interpret_scene_information
	.cfi_endproc

	.globl	interpret_subsequence_info
	.p2align	4, 0x90
	.type	interpret_subsequence_info,@function
interpret_subsequence_info:             # @interpret_subsequence_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.7, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.8, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.9, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.10, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.11, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.12, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB11_2
# BB#1:
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	ue_v
.LBB11_2:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end11:
	.size	interpret_subsequence_info, .Lfunc_end11-interpret_subsequence_info
	.cfi_endproc

	.globl	interpret_subsequence_layer_characteristics_info
	.p2align	4, 0x90
	.type	interpret_subsequence_layer_characteristics_info,@function
interpret_subsequence_layer_characteristics_info: # @interpret_subsequence_layer_characteristics_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -32
.Lcfi88:
	.cfi_offset %r14, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.14, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB12_3
# BB#1:                                 # %.lr.ph.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.15, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$16, %edi
	movl	$.L.str.16, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$16, %edi
	movl	$.L.str.17, %esi
	movq	%rbx, %rdx
	callq	u_v
	decl	%ebp
	jne	.LBB12_2
.LBB12_3:                               # %._crit_edge
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end12:
	.size	interpret_subsequence_layer_characteristics_info, .Lfunc_end12-interpret_subsequence_layer_characteristics_info
	.cfi_endproc

	.globl	interpret_subsequence_characteristics_info
	.p2align	4, 0x90
	.type	interpret_subsequence_characteristics_info,@function
interpret_subsequence_characteristics_info: # @interpret_subsequence_characteristics_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.7, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.8, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.18, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB13_2
# BB#1:
	movl	$32, %edi
	movl	$.L.str.18, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB13_2:
	movl	$.L.str.19, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB13_4
# BB#3:
	movl	$.L.str.15, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$16, %edi
	movl	$.L.str.16, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$16, %edi
	movl	$.L.str.17, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB13_4:
	movl	$.L.str.20, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB13_6
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.21, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.22, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.23, %edi
	movq	%rbx, %rsi
	callq	u_1
	decl	%ebp
	jne	.LBB13_5
.LBB13_6:                               # %._crit_edge
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	interpret_subsequence_characteristics_info, .Lfunc_end13-interpret_subsequence_characteristics_info
	.cfi_endproc

	.globl	interpret_full_frame_freeze_info
	.p2align	4, 0x90
	.type	interpret_full_frame_freeze_info,@function
interpret_full_frame_freeze_info:       # @interpret_full_frame_freeze_info
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	interpret_full_frame_freeze_info, .Lfunc_end14-interpret_full_frame_freeze_info
	.cfi_endproc

	.globl	interpret_full_frame_freeze_release_info
	.p2align	4, 0x90
	.type	interpret_full_frame_freeze_release_info,@function
interpret_full_frame_freeze_release_info: # @interpret_full_frame_freeze_release_info
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	interpret_full_frame_freeze_release_info, .Lfunc_end15-interpret_full_frame_freeze_release_info
	.cfi_endproc

	.globl	interpret_full_frame_snapshot_info
	.p2align	4, 0x90
	.type	interpret_full_frame_snapshot_info,@function
interpret_full_frame_snapshot_info:     # @interpret_full_frame_snapshot_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.40, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end16:
	.size	interpret_full_frame_snapshot_info, .Lfunc_end16-interpret_full_frame_snapshot_info
	.cfi_endproc

	.globl	interpret_progressive_refinement_end_info
	.p2align	4, 0x90
	.type	interpret_progressive_refinement_end_info,@function
interpret_progressive_refinement_end_info: # @interpret_progressive_refinement_end_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.41, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end17:
	.size	interpret_progressive_refinement_end_info, .Lfunc_end17-interpret_progressive_refinement_end_info
	.cfi_endproc

	.globl	interpret_motion_constrained_slice_group_set_info
	.p2align	4, 0x90
	.type	interpret_motion_constrained_slice_group_set_info,@function
interpret_motion_constrained_slice_group_set_info: # @interpret_motion_constrained_slice_group_set_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 48
.Lcfi113:
	.cfi_offset %rbx, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r14
	movl	%ebx, 12(%r14)
	movq	%rbp, 16(%r14)
	movl	$0, 8(%r14)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.43, %edi
	movq	%r14, %rsi
	callq	ue_v
	movl	%eax, %r15d
	leal	1(%r15), %ebp
	movl	%ebp, %edi
	callq	CeilLog2
	movl	%eax, %ebx
	testl	%r15d, %r15d
	js	.LBB18_2
	.p2align	4, 0x90
.LBB18_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.44, %esi
	movl	%ebx, %edi
	movq	%r14, %rdx
	callq	u_v
	decl	%ebp
	jne	.LBB18_1
.LBB18_2:                               # %._crit_edge
	movl	$.L.str.35, %edi
	movq	%r14, %rsi
	callq	u_1
	movl	$.L.str.45, %edi
	movq	%r14, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB18_4
# BB#3:
	movl	$.L.str.26, %edi
	movq	%r14, %rsi
	callq	ue_v
.LBB18_4:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end18:
	.size	interpret_motion_constrained_slice_group_set_info, .Lfunc_end18-interpret_motion_constrained_slice_group_set_info
	.cfi_endproc

	.globl	interpret_film_grain_characteristics_info
	.p2align	4, 0x90
	.type	interpret_film_grain_characteristics_info,@function
interpret_film_grain_characteristics_info: # @interpret_film_grain_characteristics_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi123:
	.cfi_def_cfa_offset 80
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$.L.str.46, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	jne	.LBB19_16
# BB#1:
	movl	$2, %edi
	movl	$.L.str.47, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.48, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB19_3
# BB#2:
	movl	$3, %edi
	movl	$.L.str.49, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$3, %edi
	movl	$.L.str.50, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$1, %edi
	movl	$.L.str.51, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.52, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.53, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.54, %esi
	movq	%rbx, %rdx
	callq	u_v
.LBB19_3:                               # %.preheader.preheader93
	movl	$2, %edi
	movl	$.L.str.55, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$4, %edi
	movl	$.L.str.56, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$.L.str.57, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	%eax, %ebp
	movl	%ebp, 12(%rsp)
	movl	$.L.str.57, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	%eax, 16(%rsp)
	movl	$.L.str.57, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	%eax, 20(%rsp)
	xorl	%r15d, %r15d
	testl	%ebp, %ebp
	jne	.LBB19_5
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_14:                              # %.loopexit..preheader_crit_edge
                                        #   in Loop: Header=BB19_13 Depth=1
	movl	16(%rsp,%r15,4), %ebp
	movq	%rax, %r15
	testl	%ebp, %ebp
	je	.LBB19_13
.LBB19_5:
	movl	$8, %edi
	movl	$.L.str.58, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	%eax, %r12d
	movl	$3, %edi
	movl	$.L.str.59, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	%eax, %r14d
	testl	%r12d, %r12d
	js	.LBB19_13
# BB#6:                                 # %.lr.ph83
	testl	%r14d, %r14d
	js	.LBB19_11
# BB#7:                                 # %.lr.ph83.split.preheader
	incl	%r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB19_8:                               # %.lr.ph83.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_9 Depth 2
	movl	$8, %edi
	movl	$.L.str.60, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.61, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB19_9:                               #   Parent Loop BB19_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.62, %edi
	movq	%rbx, %rsi
	callq	se_v
	decl	%ebp
	jne	.LBB19_9
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB19_8 Depth=1
	cmpl	%r12d, %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jne	.LBB19_8
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_11:                              # %.lr.ph83.split.us.preheader
	incl	%r12d
	.p2align	4, 0x90
.LBB19_12:                              # %.lr.ph83.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$8, %edi
	movl	$.L.str.60, %esi
	movq	%rbx, %rdx
	callq	u_v
	movl	$8, %edi
	movl	$.L.str.61, %esi
	movq	%rbx, %rdx
	callq	u_v
	decl	%r12d
	jne	.LBB19_12
	.p2align	4, 0x90
.LBB19_13:                              # %.loopexit
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r15), %rax
	cmpq	$3, %rax
	jne	.LBB19_14
# BB#15:
	movl	$.L.str.63, %edi
	movq	%rbx, %rsi
	callq	ue_v
.LBB19_16:
	movq	%rbx, %rdi
	callq	free
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	interpret_film_grain_characteristics_info, .Lfunc_end19-interpret_film_grain_characteristics_info
	.cfi_endproc

	.globl	interpret_deblocking_filter_display_preference_info
	.p2align	4, 0x90
	.type	interpret_deblocking_filter_display_preference_info,@function
interpret_deblocking_filter_display_preference_info: # @interpret_deblocking_filter_display_preference_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 32
.Lcfi133:
	.cfi_offset %rbx, -32
.Lcfi134:
	.cfi_offset %r14, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$.L.str.64, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	jne	.LBB20_2
# BB#1:
	movl	$.L.str.65, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.66, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.67, %edi
	movq	%rbx, %rsi
	callq	ue_v
.LBB20_2:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end20:
	.size	interpret_deblocking_filter_display_preference_info, .Lfunc_end20-interpret_deblocking_filter_display_preference_info
	.cfi_endproc

	.globl	interpret_stereo_video_info_info
	.p2align	4, 0x90
	.type	interpret_stereo_video_info_info,@function
interpret_stereo_video_info_info:       # @interpret_stereo_video_info_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 32
.Lcfi139:
	.cfi_offset %rbx, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$.L.str.68, %edi
	movq	%rbx, %rsi
	callq	u_1
	testl	%eax, %eax
	je	.LBB21_2
# BB#1:
	movl	$.L.str.69, %edi
	jmp	.LBB21_3
.LBB21_2:
	movl	$.L.str.70, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.71, %edi
.LBB21_3:
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.72, %edi
	movq	%rbx, %rsi
	callq	u_1
	movl	$.L.str.73, %edi
	movq	%rbx, %rsi
	callq	u_1
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end21:
	.size	interpret_stereo_video_info_info, .Lfunc_end21-interpret_stereo_video_info_info
	.cfi_endproc

	.globl	interpret_reserved_info
	.p2align	4, 0x90
	.type	interpret_reserved_info,@function
interpret_reserved_info:                # @interpret_reserved_info
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	interpret_reserved_info, .Lfunc_end22-interpret_reserved_info
	.cfi_endproc

	.globl	interpret_progressive_refinement_start_info
	.p2align	4, 0x90
	.type	interpret_progressive_refinement_start_info,@function
interpret_progressive_refinement_start_info: # @interpret_progressive_refinement_start_info
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	%ebp, 12(%rbx)
	movq	%r14, 16(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, UsedBits(%rip)
	movl	$.L.str.41, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movl	$.L.str.42, %edi
	movq	%rbx, %rsi
	callq	ue_v
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end23:
	.size	interpret_progressive_refinement_start_info, .Lfunc_end23-interpret_progressive_refinement_start_info
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SEI: target_frame_num"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SEI: num_spare_pics_minus1"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SEI: delta_spare_frame_num"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SEI: ref_area_indicator"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SEI: ref_mb_indicator"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SEI: zero_run_length"
	.size	.L.str.5, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Wrong ref_area_indicator %d!\n"
	.size	.L.str.6, 30

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SEI: sub_seq_layer_num"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SEI: sub_seq_id"
	.size	.L.str.8, 16

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SEI: first_ref_pic_flag"
	.size	.L.str.9, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SEI: leading_non_ref_pic_flag"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SEI: last_pic_flag"
	.size	.L.str.11, 19

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SEI: sub_seq_frame_num_flag"
	.size	.L.str.12, 28

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SEI: sub_seq_frame_num"
	.size	.L.str.13, 23

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SEI: num_sub_layers_minus1"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SEI: accurate_statistics_flag"
	.size	.L.str.15, 30

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SEI: average_bit_rate"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SEI: average_frame_rate"
	.size	.L.str.17, 24

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SEI: duration_flag"
	.size	.L.str.18, 19

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SEI: average_rate_flag"
	.size	.L.str.19, 23

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SEI: num_referenced_subseqs"
	.size	.L.str.20, 28

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SEI: ref_sub_seq_layer_num"
	.size	.L.str.21, 27

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SEI: ref_sub_seq_id"
	.size	.L.str.22, 20

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SEI: ref_sub_seq_direction"
	.size	.L.str.23, 27

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SEI: scene_id"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"SEI: scene_transition_type"
	.size	.L.str.25, 27

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SEI: pan_scan_rect_id"
	.size	.L.str.26, 22

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SEI: pan_scan_rect_cancel_flag"
	.size	.L.str.27, 31

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SEI: pan_scan_cnt_minus1"
	.size	.L.str.28, 25

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SEI: pan_scan_rect_left_offset"
	.size	.L.str.29, 31

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SEI: pan_scan_rect_right_offset"
	.size	.L.str.30, 32

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SEI: pan_scan_rect_top_offset"
	.size	.L.str.31, 30

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SEI: pan_scan_rect_bottom_offset"
	.size	.L.str.32, 33

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SEI: pan_scan_rect_repetition_period"
	.size	.L.str.33, 37

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"SEI: recovery_frame_cnt"
	.size	.L.str.34, 24

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"SEI: exact_match_flag"
	.size	.L.str.35, 22

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SEI: broken_link_flag"
	.size	.L.str.36, 22

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"SEI: changing_slice_group_idc"
	.size	.L.str.37, 30

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"SEI: original_idr_flag"
	.size	.L.str.38, 23

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"SEI: original_frame_num"
	.size	.L.str.39, 24

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"SEI: snapshot_id"
	.size	.L.str.40, 17

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"SEI: progressive_refinement_id"
	.size	.L.str.41, 31

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"SEI: num_refinement_steps_minus1"
	.size	.L.str.42, 33

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"SEI: num_slice_groups_minus1"
	.size	.L.str.43, 29

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"SEI: slice_group_id"
	.size	.L.str.44, 20

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"SEI: pan_scan_rect_flag"
	.size	.L.str.45, 24

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"SEI: film_grain_characteristics_cancel_flag"
	.size	.L.str.46, 44

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SEI: model_id"
	.size	.L.str.47, 14

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SEI: separate_colour_description_present_flag"
	.size	.L.str.48, 46

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SEI: film_grain_bit_depth_luma_minus8"
	.size	.L.str.49, 38

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"SEI: film_grain_bit_depth_chroma_minus8"
	.size	.L.str.50, 40

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"SEI: film_grain_full_range_flag"
	.size	.L.str.51, 32

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"SEI: film_grain_colour_primaries"
	.size	.L.str.52, 33

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"SEI: film_grain_transfer_characteristics"
	.size	.L.str.53, 41

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"SEI: film_grain_matrix_coefficients"
	.size	.L.str.54, 36

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"SEI: blending_mode_id"
	.size	.L.str.55, 22

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"SEI: log2_scale_factor"
	.size	.L.str.56, 23

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"SEI: comp_model_present_flag"
	.size	.L.str.57, 29

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"SEI: num_intensity_intervals_minus1"
	.size	.L.str.58, 36

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"SEI: num_model_values_minus1"
	.size	.L.str.59, 29

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SEI: intensity_interval_lower_bound"
	.size	.L.str.60, 36

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"SEI: intensity_interval_upper_bound"
	.size	.L.str.61, 36

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"SEI: comp_model_value"
	.size	.L.str.62, 22

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"SEI: film_grain_characteristics_repetition_period"
	.size	.L.str.63, 50

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"SEI: deblocking_display_preference_cancel_flag"
	.size	.L.str.64, 47

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"SEI: display_prior_to_deblocking_preferred_flag"
	.size	.L.str.65, 48

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"SEI: dec_frame_buffering_constraint_flag"
	.size	.L.str.66, 41

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"SEI: deblocking_display_preference_repetition_period"
	.size	.L.str.67, 53

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"SEI: field_views_flags"
	.size	.L.str.68, 23

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"SEI: top_field_is_left_view_flag"
	.size	.L.str.69, 33

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"SEI: current_frame_is_left_view_flag"
	.size	.L.str.70, 37

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"SEI: next_frame_is_second_view_flag"
	.size	.L.str.71, 36

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"SEI: left_view_self_contained_flag"
	.size	.L.str.72, 35

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"SEI: right_view_self_contained_flag"
	.size	.L.str.73, 36

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"SEI: seq_parameter_set_id"
	.size	.L.str.74, 26

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"SEI: initial_cpb_removal_delay"
	.size	.L.str.75, 31

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"SEI: initial_cpb_removal_delay_offset"
	.size	.L.str.76, 38

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Warning: no active SPS, timing SEI cannot be parsed\n"
	.size	.L.str.77, 53

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"SEI: cpb_removal_delay"
	.size	.L.str.78, 23

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"SEI: dpb_output_delay"
	.size	.L.str.79, 22

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"SEI: pic_struct"
	.size	.L.str.80, 16

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"reserved picture_structure used (can't determine NumClockTs)"
	.size	.L.str.81, 61

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"SEI: clock_time_stamp_flag"
	.size	.L.str.82, 27

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"SEI: ct_type"
	.size	.L.str.83, 13

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"SEI: nuit_field_based_flag"
	.size	.L.str.84, 27

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"SEI: counting_type"
	.size	.L.str.85, 19

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"SEI: full_timestamp_flag"
	.size	.L.str.86, 25

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"SEI: discontinuity_flag"
	.size	.L.str.87, 24

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"SEI: cnt_dropped_flag"
	.size	.L.str.88, 22

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"SEI: nframes"
	.size	.L.str.89, 13

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"SEI: seconds_value"
	.size	.L.str.90, 19

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"SEI: minutes_value"
	.size	.L.str.91, 19

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"SEI: hours_value"
	.size	.L.str.92, 17

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"SEI: seconds_flag"
	.size	.L.str.93, 18

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"SEI: minutes_flag"
	.size	.L.str.94, 18

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"SEI: hours_flag"
	.size	.L.str.95, 16

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"SEI: time_offset"
	.size	.L.str.96, 17

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	.Lswitch.table, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
