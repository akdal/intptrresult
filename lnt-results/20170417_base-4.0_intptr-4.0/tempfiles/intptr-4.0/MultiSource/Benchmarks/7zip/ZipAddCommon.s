	.text
	.file	"ZipAddCommon.bc"
	.globl	_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	cmpq	$0, 24(%r13)
	je	.LBB0_2
# BB#1:                                 # %._crit_edge
	leaq	16(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB0_6
.LBB0_2:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp0:
	movq	%rbp, %rdi
	callq	_ZN9NCompress5NLzma8CEncoderC1Ev
.Ltmp1:
# BB#3:
	movq	%rbp, 16(%r13)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_5:                                # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
	leaq	16(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbp, 24(%r13)
.LBB0_6:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$0, 8(%rbp)
	movq	$_ZTV19CBufPtrSeqOutStream+16, (%rbp)
	movq	%rbp, %rdi
	callq	*_ZTV19CBufPtrSeqOutStream+24(%rip)
	leaq	36(%r13), %rax
	movq	%rax, 16(%rbp)
	movl	$5, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 24(%rbp)
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp3:
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp4:
# BB#7:
	testl	%ebx, %ebx
	jne	.LBB0_12
# BB#8:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp5:
	movq	%rbp, %rsi
	callq	*56(%rax)
	movl	%eax, %ebx
.Ltmp6:
# BB#9:
	testl	%ebx, %ebx
	jne	.LBB0_12
# BB#10:
	movl	$-2147467259, %ebx      # imm = 0x80004005
	cmpq	$5, 32(%rbp)
	jne	.LBB0_12
# BB#11:
	movl	$332809, 32(%r13)       # imm = 0x51409
	xorl	%ebx, %ebx
.LBB0_12:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*16(%rax)
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_16:
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_13:
.Ltmp7:
	movq	%rax, %rbx
	movq	(%rbp), %rax
.Ltmp8:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp9:
# BB#14:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_15:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end0-_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %rbp
	leaq	32(%rbp), %rsi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	je	.LBB2_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_2:
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end2:
	.size	_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end2-_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE,@function
_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE: # @_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	xorps	%xmm0, %xmm0
	movups	%xmm0, 112(%rbx)
	movq	$0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE, .Lfunc_end3-_ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_,@function
_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_: # @_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -48
.Lcfi34:
	.cfi_offset %r12, -40
.Lcfi35:
	.cfi_offset %r13, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZN13CRecordVectorIhEC2ERKS0_
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movslq	40(%rbx), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB4_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp11:
	callq	_Znam
	movq	%rax, %r15
.Ltmp12:
# BB#3:                                 # %.noexc
	movq	%r15, 32(%r14)
	movl	$0, (%r15)
	movl	%r12d, 44(%r14)
	jmp	.LBB4_4
.LBB4_1:
	xorl	%r15d, %r15d
.LBB4_4:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	32(%rbx), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB4_5
# BB#6:
	movl	%r13d, 40(%r14)
	movq	77(%rbx), %rax
	movq	%rax, 77(%r14)
	movups	48(%rbx), %xmm0
	movups	64(%rbx), %xmm1
	movups	%xmm1, 64(%r14)
	movups	%xmm0, 48(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%r14)
	movslq	96(%rbx), %rax
	leaq	1(%rax), %r12
	testl	%r12d, %r12d
	je	.LBB4_7
# BB#8:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp14:
	callq	_Znam
.Ltmp15:
# BB#9:                                 # %.noexc10
	movq	%rax, 88(%r14)
	movb	$0, (%rax)
	movl	%r12d, 100(%r14)
	jmp	.LBB4_10
.LBB4_7:
	xorl	%eax, %eax
.LBB4_10:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	88(%rbx), %rcx
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB4_11
# BB#12:
	movl	96(%rbx), %eax
	movl	%eax, 96(%r14)
	movzwl	104(%rbx), %eax
	movw	%ax, 104(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB4_14:
.Ltmp16:
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB4_16
# BB#15:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB4_16
.LBB4_13:
.Ltmp13:
	movq	%rax, %rbx
.LBB4_16:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp17:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp18:
# BB#17:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_18:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_, .Lfunc_end4-_ZN8NArchive4NZip22CCompressionMethodModeC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp11-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE,@function
_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE: # @_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$16920, %rsp            # imm = 0x4218
.Lcfi44:
	.cfi_def_cfa_offset 16976
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	$0, 72(%rsp)
	movq	(%r14), %rax
	xorl	%r15d, %r15d
.Ltmp20:
	leaq	72(%rsp), %rdx
	movl	$IID_IInStream, %esi
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%eax, %ebx
.Ltmp21:
# BB#1:
	testl	%ebx, %ebx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	je	.LBB5_3
# BB#2:
	movq	%r13, %r15
	movl	$1, %ebx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB5_18
.LBB5_3:
	cmpq	$0, 72(%rsp)
	je	.LBB5_11
# BB#4:
	xorl	%r15d, %r15d
.Ltmp22:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp23:
# BB#5:
	movl	$0, 8(%rbp)
	movq	$_ZTV16CInStreamWithCRC+16, (%rbp)
	movq	$0, 16(%rbp)
	xorl	%r15d, %r15d
.Ltmp24:
	movq	%rbp, %rdi
	callq	*_ZTV16CInStreamWithCRC+24(%rip)
.Ltmp25:
# BB#6:                                 # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	72(%rsp), %r12
	testq	%r12, %r12
	je	.LBB5_8
# BB#7:
	movq	%rbp, %r15
	movq	(%r12), %rax
.Ltmp26:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp27:
.LBB5_8:                                # %.noexc350
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_10
# BB#9:
	movq	%rbp, %r15
	movq	(%rdi), %rax
.Ltmp28:
	callq	*16(%rax)
.Ltmp29:
.LBB5_10:
	movq	%r13, %r15
	movq	%r12, 16(%rbp)
	movq	$0, 24(%rbp)
	xorl	%r13d, %r13d
	movq	%rbp, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB5_17
.LBB5_11:
	xorl	%r15d, %r15d
.Ltmp30:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp31:
# BB#12:
	movl	$0, 8(%rbp)
	movq	$_ZTV26CSequentialInStreamWithCRC+16, (%rbp)
	movq	$0, 16(%rbp)
	xorl	%r15d, %r15d
.Ltmp32:
	movq	%rbp, %rdi
	callq	*_ZTV26CSequentialInStreamWithCRC+24(%rip)
.Ltmp33:
# BB#13:                                # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit354
	movq	%rbp, %r15
	movq	(%r14), %rax
.Ltmp34:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp35:
# BB#14:                                # %.noexc355
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_16
# BB#15:
	movq	%rbp, %r15
	movq	(%rdi), %rax
.Ltmp36:
	callq	*16(%rax)
.Ltmp37:
.LBB5_16:
	movq	%r13, %r15
	movq	%r14, 16(%rbp)
	movq	$0, 24(%rbp)
	movb	$0, 36(%rbp)
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
.LBB5_17:                               # %.sink.split
	movl	$-1, 32(%rbp)
	xorl	%ebx, %ebx
	movq	%rbp, %r12
.LBB5_18:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
.LBB5_20:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	testl	%ebx, %ebx
	je	.LBB5_25
# BB#21:
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB5_22:                               # %_ZN18COutStreamReleaserD2Ev.exit347
	testq	%r12, %r12
	je	.LBB5_24
# BB#23:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
.LBB5_24:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit344
	movl	%ebp, %eax
	addq	$16920, %rsp            # imm = 0x4218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_25:
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %ecx
	cmpl	$1, %ecx
	jle	.LBB5_28
# BB#26:
	leaq	84(%rax), %rbx
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	je	.LBB5_177
.LBB5_27:                               # %.thread783
	leaq	22(%rsi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movb	$10, 22(%rsi)
	jmp	.LBB5_32
.LBB5_28:
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	jne	.LBB5_30
# BB#29:
	movq	48(%rsp), %rax          # 8-byte Reload
	movb	84(%rax), %al
	movl	$-2147467263, %ebp      # imm = 0x80004001
	testb	%al, %al
	jne	.LBB5_22
.LBB5_30:
	movb	$10, 22(%rsi)
	testl	%ecx, %ecx
	jle	.LBB5_179
# BB#31:
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	84(%rax), %rbx
	leaq	22(%rsi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %ecx
.LBB5_32:                               # %.lr.ph
	leaq	8(%rsi), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rsi
	leaq	32(%rsi), %rax
	leaq	24(%rsi), %rdx
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	104(%r8), %rbp
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	leaq	32(%r13), %rbp
	addq	$24, %r13
	movq	%r8, %rdi
	subq	$-128, %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	testq	%rsi, %rsi
	movslq	%ecx, %rcx
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	cmovneq	%rax, %rbp
	movq	%rbp, 408(%rsp)         # 8-byte Spill
	cmovneq	%rdx, %r13
	movq	%r13, 416(%rsp)         # 8-byte Spill
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 464(%rsp)        # 16-byte Spill
	movl	$_ZTVN7NCrypto4NZip8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto4NZip8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 448(%rsp)        # 16-byte Spill
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm2
	movl	$_ZTVN7NCrypto6NWzAes8CEncoderE+96, %eax
	movd	%rax, %xmm1
	movl	$_ZTVN7NCrypto6NWzAes8CEncoderE+16, %eax
	movd	%rax, %xmm3
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqa	%xmm2, 496(%rsp)        # 16-byte Spill
	punpcklqdq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0]
	movdqa	%xmm3, 480(%rsp)        # 16-byte Spill
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %r13
	movq	%r8, %r15
	movq	%r12, (%rsp)            # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
.LBB5_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_74 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	movb	$10, (%rax)
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB5_36
# BB#34:                                #   in Loop: Header=BB5_33 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp44:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp45:
# BB#35:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
.LBB5_36:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r13), %rax
.Ltmp46:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp47:
# BB#37:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	movl	%ebp, %r12d
	cmovel	20(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB5_61
# BB#38:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%r13), %rax
.Ltmp48:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp49:
# BB#39:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	cmovnel	%ebp, %r12d
	jne	.LBB5_61
# BB#40:                                #   in Loop: Header=BB5_33 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpb	$0, (%rbx)
	je	.LBB5_64
# BB#41:                                #   in Loop: Header=BB5_33 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movb	$20, (%rax)
	cmpq	$0, 152(%r15)
	jne	.LBB5_48
# BB#42:                                #   in Loop: Header=BB5_33 Depth=1
.Ltmp50:
	movl	$200, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp51:
# BB#43:                                #   in Loop: Header=BB5_33 Depth=1
.Ltmp52:
	movq	%rbp, %rdi
	callq	_ZN12CFilterCoderC1Ev
.Ltmp53:
# BB#44:                                #   in Loop: Header=BB5_33 Depth=1
	movq	%rbp, 144(%r15)
	movq	%rbp, %rbx
	addq	$32, %rbx
	movq	32(%rbp), %rax
.Ltmp55:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp56:
# BB#45:                                # %.noexc361
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_47
# BB#46:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp57:
	callq	*16(%rax)
.Ltmp58:
.LBB5_47:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	%rbx, 152(%r15)
.LBB5_48:                               #   in Loop: Header=BB5_33 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB5_65
# BB#49:                                #   in Loop: Header=BB5_33 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movb	$51, (%rax)
	movq	144(%r15), %rax
	cmpq	$0, 192(%rax)
	jne	.LBB5_59
# BB#50:                                #   in Loop: Header=BB5_33 Depth=1
.Ltmp59:
	movl	$592, %edi              # imm = 0x250
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp60:
# BB#51:                                #   in Loop: Header=BB5_33 Depth=1
	movl	$0, 16(%rbp)
	movdqa	496(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, (%rbp)
	movq	$_ZTV7CBufferIhE+16, 48(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%rbp)
	movl	$3, 24(%rbp)
	movq	%rbp, %rdi
	addq	$284, %rdi              # imm = 0x11C
.Ltmp61:
	callq	_ZN7NCrypto6NWzAes8CAesCtr2C1Ev
.Ltmp62:
# BB#52:                                #   in Loop: Header=BB5_33 Depth=1
	movdqa	480(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, (%rbp)
	movq	%rbp, 168(%r15)
	movq	144(%r15), %rbx
.Ltmp64:
	movq	%rbp, %rdi
	callq	*_ZTVN7NCrypto6NWzAes8CEncoderE+24(%rip)
.Ltmp65:
# BB#53:                                # %.noexc363
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_55
# BB#54:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp66:
	callq	*16(%rax)
.Ltmp67:
.LBB5_55:                               #   in Loop: Header=BB5_33 Depth=1
	movq	%rbp, 192(%rbx)
	movzbl	105(%r15), %eax
	leal	-1(%rax), %ecx
	cmpl	$2, %ecx
	movq	168(%r15), %rdi
	ja	.LBB5_57
# BB#56:                                #   in Loop: Header=BB5_33 Depth=1
	movl	%eax, 24(%rdi)
.LBB5_57:                               # %_ZN7NCrypto6NWzAes10CBaseCoder10SetKeyModeEj.exit
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
	movq	88(%r15), %rsi
	movl	96(%r15), %edx
.Ltmp68:
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp69:
# BB#58:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	jne	.LBB5_61
.LBB5_59:                               #   in Loop: Header=BB5_33 Depth=1
	movq	168(%r15), %rdi
.Ltmp70:
	movq	%r13, %rsi
	callq	_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream
	movl	%eax, %ebp
.Ltmp71:
# BB#60:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	je	.LBB5_82
	jmp	.LBB5_61
	.p2align	4, 0x90
.LBB5_64:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_85
.LBB5_65:                               #   in Loop: Header=BB5_33 Depth=1
	movq	144(%r15), %rbp
	cmpq	$0, 192(%rbp)
	jne	.LBB5_71
# BB#66:                                #   in Loop: Header=BB5_33 Depth=1
.Ltmp72:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp73:
# BB#67:                                #   in Loop: Header=BB5_33 Depth=1
	movl	$0, 16(%rbx)
	movdqa	448(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, (%rbx)
	movq	%rbx, 160(%r15)
.Ltmp74:
	movq	%rbx, %rdi
	callq	*_ZTVN7NCrypto4NZip8CEncoderE+24(%rip)
.Ltmp75:
# BB#68:                                # %.noexc368
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_70
# BB#69:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp76:
	callq	*16(%rax)
.Ltmp77:
.LBB5_70:                               #   in Loop: Header=BB5_33 Depth=1
	movq	%rbx, 192(%rbp)
	movq	160(%r15), %rdi
	movq	(%rdi), %rax
	movq	88(%r15), %rsi
	movl	96(%r15), %edx
.Ltmp78:
	callq	*56(%rax)
.Ltmp79:
.LBB5_71:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r14), %rax
.Ltmp80:
	movl	$16384, %edx            # imm = 0x4000
	movq	%r14, %rdi
	leaq	528(%rsp), %rsi
	leaq	84(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp81:
# BB#72:                                # %.noexc373
                                        #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
# BB#73:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_33 Depth=1
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB5_74:                               # %.lr.ph.i
                                        #   Parent Loop BB5_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	84(%rsp), %edx
	testq	%rdx, %rdx
	je	.LBB5_78
# BB#75:                                # %.thread.i
                                        #   in Loop: Header=BB5_74 Depth=2
.Ltmp82:
	movl	%ebx, %edi
	leaq	528(%rsp), %rbp
	movq	%rbp, %rsi
	callq	CrcUpdate
	movl	%eax, %ebx
.Ltmp83:
# BB#76:                                # %.noexc374
                                        #   in Loop: Header=BB5_74 Depth=2
	movq	(%r14), %rax
.Ltmp84:
	movl	$16384, %edx            # imm = 0x4000
	movq	%r14, %rdi
	movq	%rbp, %rsi
	leaq	84(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp85:
# BB#77:                                # %.noexc375
                                        #   in Loop: Header=BB5_74 Depth=2
	testl	%ebp, %ebp
	je	.LBB5_74
	jmp	.LBB5_61
.LBB5_78:                               #   in Loop: Header=BB5_33 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp87:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp88:
# BB#79:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
# BB#80:                                #   in Loop: Header=BB5_33 Depth=1
	notl	%ebx
	movq	160(%r15), %rdi
.Ltmp89:
	movq	%r13, %rsi
	movl	%ebx, %edx
	callq	_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj
	movl	%eax, %ebp
.Ltmp90:
# BB#81:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	jne	.LBB5_61
.LBB5_82:                               # %.thread
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	144(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp92:
	movq	%r13, %rsi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp93:
# BB#83:                                #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
# BB#84:                                #   in Loop: Header=BB5_33 Depth=1
	movq	144(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %r12            # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB5_85:                               #   in Loop: Header=BB5_33 Depth=1
	movq	16(%r15), %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movzbl	(%rax,%rcx), %ecx
	testq	%rcx, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	je	.LBB5_90
# BB#86:                                #   in Loop: Header=BB5_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	jne	.LBB5_87
# BB#99:                                #   in Loop: Header=BB5_33 Depth=1
	cmpb	$98, %cl
	je	.LBB5_121
# BB#100:                               #   in Loop: Header=BB5_33 Depth=1
	cmpb	$14, %cl
	jne	.LBB5_131
# BB#101:                               #   in Loop: Header=BB5_33 Depth=1
	movb	$63, 136(%r15)
.Ltmp124:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp125:
# BB#102:                               #   in Loop: Header=BB5_33 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rbx)
	movdqa	%xmm0, 16(%rbx)
	movdqa	%xmm0, (%rbx)
	movq	$_ZTVN8NArchive4NZip12CLzmaEncoderE+16, (%rbx)
	movq	$0, 24(%rbx)
	movl	$1, 8(%rbx)
	movl	80(%r15), %eax
	movl	48(%r15), %ecx
	movl	68(%r15), %edx
	movl	56(%r15), %edi
	movq	32(%r15), %rsi
	movq	%rbx, 128(%r15)
	movw	$19, 240(%rsp)
	movw	$0, 242(%rsp)
	movl	%eax, 248(%rsp)
	movw	$19, 256(%rsp)
	movw	$0, 258(%rsp)
	movl	%ecx, 264(%rsp)
	movw	$19, 272(%rsp)
	movw	$0, 274(%rsp)
	movl	%edx, 280(%rsp)
	movw	$19, 288(%rsp)
	movw	$0, 290(%rsp)
	movl	%edi, 296(%rsp)
.Ltmp126:
	leaq	304(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantC1EPw
.Ltmp127:
# BB#103:                               #   in Loop: Header=BB5_33 Depth=1
	movl	64(%r15), %eax
	movw	$19, 320(%rsp)
	movw	$0, 322(%rsp)
	movl	%eax, 328(%rsp)
	movq	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs+16(%rip), %rax
	movq	%rax, 544(%rsp)
	movdqa	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs(%rip), %xmm0
	movdqa	%xmm0, 528(%rsp)
	movzbl	60(%r15), %ecx
	addl	$5, %ecx
.Ltmp138:
	movq	%rbx, %rdi
	leaq	528(%rsp), %rsi
	leaq	240(%rsp), %rdx
	callq	_ZN8NArchive4NZip12CLzmaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	movl	%eax, %ebp
.Ltmp139:
# BB#104:                               #   in Loop: Header=BB5_33 Depth=1
.Ltmp154:
	leaq	320(%rsp), %rdi
	movq	%rdi, %rbx
	movq	(%rsp), %r12            # 8-byte Reload
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp155:
# BB#105:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp156:
	leaq	304(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp157:
# BB#106:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404.1
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp158:
	leaq	288(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp159:
# BB#107:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404.2
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp160:
	leaq	272(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp161:
# BB#108:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404.3
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp162:
	leaq	256(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp163:
# BB#109:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404.4
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp164:
	leaq	240(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp165:
	jmp	.LBB5_110
.LBB5_90:                               #   in Loop: Header=BB5_33 Depth=1
	cmpq	$0, 112(%r15)
	jne	.LBB5_96
# BB#91:                                #   in Loop: Header=BB5_33 Depth=1
.Ltmp244:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp245:
# BB#92:                                #   in Loop: Header=BB5_33 Depth=1
	movl	$0, 16(%rbx)
	movdqa	464(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movq	%rbx, 112(%r15)
.Ltmp246:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp247:
# BB#93:                                # %.noexc376
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_95
# BB#94:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp248:
	callq	*16(%rax)
.Ltmp249:
.LBB5_95:                               # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	%rbx, 120(%r15)
	movq	(%rsp), %r12            # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB5_96:                               #   in Loop: Header=BB5_33 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB5_113
# BB#97:                                #   in Loop: Header=BB5_33 Depth=1
	movq	152(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB5_133
# BB#98:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%rbx), %rax
.Ltmp250:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp251:
	jmp	.LBB5_134
.LBB5_113:                              #   in Loop: Header=BB5_33 Depth=1
	testq	%r13, %r13
	je	.LBB5_133
# BB#114:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r13), %rax
.Ltmp252:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp253:
	movq	%r13, %rbx
	jmp	.LBB5_134
.LBB5_133:                              #   in Loop: Header=BB5_33 Depth=1
	xorl	%ebx, %ebx
.LBB5_134:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSERKS1_.exit
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	120(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp255:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	232(%rsp), %r9          # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp256:
# BB#135:                               #   in Loop: Header=BB5_33 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_137
# BB#136:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rbx), %rax
.Ltmp260:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp261:
	jmp	.LBB5_137
.LBB5_121:                              #   in Loop: Header=BB5_33 Depth=1
	movb	$63, 136(%r15)
.Ltmp94:
	movl	$7496, %edi             # imm = 0x1D48
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp95:
# BB#122:                               #   in Loop: Header=BB5_33 Depth=1
.Ltmp96:
	movq	%rbp, %rdi
	callq	_ZN9NCompress8NPpmdZip8CEncoderC1Ev
.Ltmp97:
# BB#123:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rbp), %rax
.Ltmp99:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp100:
	leaq	528(%rsp), %rbx
# BB#124:                               # %.noexc409
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_126
# BB#125:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp101:
	callq	*16(%rax)
.Ltmp102:
.LBB5_126:                              #   in Loop: Header=BB5_33 Depth=1
	movq	%rbp, 128(%r15)
	movl	48(%r15), %eax
	movw	$19, 176(%rsp)
	movw	$0, 178(%rsp)
	movl	%eax, 184(%rsp)
	movl	72(%r15), %eax
	movw	$19, 192(%rsp)
	movw	$0, 194(%rsp)
	movl	%eax, 200(%rsp)
	movl	76(%r15), %eax
	movw	$19, 208(%rsp)
	movw	$0, 210(%rsp)
	movl	%eax, 216(%rsp)
	movl	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0+8(%rip), %eax
	movl	%eax, 536(%rsp)
	movq	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0(%rip), %rax
	movq	%rax, 528(%rsp)
.Ltmp104:
	movl	$3, %ecx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	leaq	176(%rsp), %rdx
	callq	_ZN9NCompress8NPpmdZip8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	movl	%eax, %ebp
.Ltmp105:
# BB#127:                               #   in Loop: Header=BB5_33 Depth=1
.Ltmp114:
	leaq	208(%rsp), %rdi
	movq	%rdi, %rbx
	movq	(%rsp), %r12            # 8-byte Reload
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp115:
# BB#128:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit416
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp116:
	leaq	192(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp117:
# BB#129:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit416.1
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp118:
	leaq	176(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp119:
.LBB5_110:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit404.5
                                        #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB5_87
	jmp	.LBB5_188
.LBB5_131:                              #   in Loop: Header=BB5_33 Depth=1
	cmpb	$12, %cl
	jne	.LBB5_146
# BB#132:                               #   in Loop: Header=BB5_33 Depth=1
	movb	$46, 136(%r15)
	movl	$262658, %edi           # imm = 0x40202
	jmp	.LBB5_147
.LBB5_146:                              #   in Loop: Header=BB5_33 Depth=1
	cmpb	$9, %cl
	sete	%al
	orb	$20, %al
	movb	%al, 136(%r15)
	leaq	262400(%rcx), %rdi
.LBB5_147:                              #   in Loop: Header=BB5_33 Depth=1
.Ltmp170:
	movl	$1, %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
	movl	%eax, %ebp
.Ltmp171:
# BB#148:                               #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_188
# BB#149:                               #   in Loop: Header=BB5_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_187
# BB#150:                               #   in Loop: Header=BB5_33 Depth=1
	movq	104(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	andb	$-2, %al
	cmpb	$8, %al
	jne	.LBB5_156
# BB#151:                               #   in Loop: Header=BB5_33 Depth=1
	movl	48(%r15), %eax
	movw	$19, 336(%rsp)
	movw	$0, 338(%rsp)
	movl	%eax, 344(%rsp)
	movl	52(%r15), %eax
	movw	$19, 352(%rsp)
	movw	$0, 354(%rsp)
	movl	%eax, 360(%rsp)
	movl	56(%r15), %eax
	movw	$19, 368(%rsp)
	movw	$0, 370(%rsp)
	movl	%eax, 376(%rsp)
	movl	64(%r15), %eax
	movw	$19, 384(%rsp)
	movw	$0, 386(%rsp)
	movl	%eax, 392(%rsp)
	movdqa	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_1(%rip), %xmm0
	movdqa	%xmm0, 512(%rsp)
	movzbl	60(%r15), %ebx
	movq	$0, 96(%rsp)
	movq	(%rdi), %rax
.Ltmp200:
	movl	$IID_ICompressSetCoderProperties, %esi
	leaq	96(%rsp), %rdx
	callq	*(%rax)
.Ltmp201:
# BB#152:                               # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI27ICompressSetCoderPropertiesEEiRK4GUIDPPT_.exit430
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_162
# BB#153:                               #   in Loop: Header=BB5_33 Depth=1
	addl	$3, %ebx
	movq	(%rdi), %rax
.Ltmp202:
	leaq	512(%rsp), %rsi
	leaq	336(%rsp), %rdx
	movl	%ebx, %ecx
	callq	*40(%rax)
.Ltmp203:
# BB#154:                               #   in Loop: Header=BB5_33 Depth=1
	testl	%eax, %eax
	je	.LBB5_162
# BB#155:                               #   in Loop: Header=BB5_33 Depth=1
	movl	$1, %ebp
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB5_163
.LBB5_156:                              #   in Loop: Header=BB5_33 Depth=1
	cmpb	$12, %cl
	jne	.LBB5_87
# BB#157:                               #   in Loop: Header=BB5_33 Depth=1
	movl	68(%r15), %eax
	movw	$19, 128(%rsp)
	movw	$0, 130(%rsp)
	movl	%eax, 136(%rsp)
	movl	52(%r15), %eax
	movw	$19, 144(%rsp)
	movw	$0, 146(%rsp)
	movl	%eax, 152(%rsp)
	movl	80(%r15), %eax
	movw	$19, 160(%rsp)
	movw	$0, 162(%rsp)
	movl	%eax, 168(%rsp)
	movl	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2+8(%rip), %eax
	movl	%eax, 440(%rsp)
	movq	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2(%rip), %rax
	movq	%rax, 432(%rsp)
	movq	$0, 88(%rsp)
	movq	(%rdi), %rax
.Ltmp173:
	movl	$IID_ICompressSetCoderProperties, %esi
	leaq	88(%rsp), %rdx
	callq	*(%rax)
.Ltmp174:
# BB#158:                               # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI27ICompressSetCoderPropertiesEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB5_33 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#159:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp175:
	movl	$3, %ecx
	leaq	432(%rsp), %rsi
	leaq	128(%rsp), %rdx
	callq	*40(%rax)
.Ltmp176:
# BB#160:                               #   in Loop: Header=BB5_33 Depth=1
	testl	%eax, %eax
	je	.LBB5_169
# BB#161:                               #   in Loop: Header=BB5_33 Depth=1
	movl	$1, %ebp
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB5_170
.LBB5_162:                              #   in Loop: Header=BB5_33 Depth=1
	xorl	%ebp, %ebp
.LBB5_163:                              #   in Loop: Header=BB5_33 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_165
# BB#164:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp207:
	callq	*16(%rax)
.Ltmp208:
.LBB5_165:                              # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit426
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp219:
	leaq	384(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp220:
# BB#166:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit424
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp221:
	leaq	368(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp222:
# BB#167:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit424.1
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp223:
	leaq	352(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp224:
# BB#168:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit424.2
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp225:
	leaq	336(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp226:
	jmp	.LBB5_175
.LBB5_169:                              #   in Loop: Header=BB5_33 Depth=1
	xorl	%ebp, %ebp
.LBB5_170:                              #   in Loop: Header=BB5_33 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_172
# BB#171:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%rdi), %rax
.Ltmp180:
	callq	*16(%rax)
.Ltmp181:
.LBB5_172:                              # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp190:
	leaq	160(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp191:
# BB#173:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit397
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp192:
	leaq	144(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp193:
# BB#174:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit397.1
                                        #   in Loop: Header=BB5_33 Depth=1
.Ltmp194:
	leaq	128(%rsp), %rdi
	movq	%rdi, %rbx
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp195:
.LBB5_175:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit397.2
                                        #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB5_176
	.p2align	4, 0x90
.LBB5_87:                               # %.thread471
                                        #   in Loop: Header=BB5_33 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB5_111
# BB#88:                                #   in Loop: Header=BB5_33 Depth=1
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.LBB5_115
# BB#89:                                #   in Loop: Header=BB5_33 Depth=1
	movq	(%r12), %rax
.Ltmp231:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp232:
	jmp	.LBB5_116
.LBB5_111:                              #   in Loop: Header=BB5_33 Depth=1
	testq	%r13, %r13
	je	.LBB5_115
# BB#112:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r13), %rax
.Ltmp233:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp234:
	movq	%r13, %r12
	jmp	.LBB5_116
.LBB5_115:                              #   in Loop: Header=BB5_33 Depth=1
	xorl	%r12d, %r12d
.LBB5_116:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSERKS1_.exit387
                                        #   in Loop: Header=BB5_33 Depth=1
	movb	136(%r15), %al
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpb	(%rcx), %al
	jbe	.LBB5_118
# BB#117:                               #   in Loop: Header=BB5_33 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx)
.LBB5_118:                              #   in Loop: Header=BB5_33 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp236:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r12, %rdx
	movq	232(%rsp), %r9          # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp237:
# BB#119:                               #   in Loop: Header=BB5_33 Depth=1
	testq	%r12, %r12
	je	.LBB5_137
# BB#120:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r12), %rax
.Ltmp241:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp242:
.LBB5_137:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit389
                                        #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
# BB#138:                               #   in Loop: Header=BB5_33 Depth=1
	movq	(%r13), %rax
.Ltmp263:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	424(%rsp), %rcx         # 8-byte Reload
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp264:
# BB#139:                               #   in Loop: Header=BB5_33 Depth=1
	testl	%ebp, %ebp
	jne	.LBB5_61
# BB#140:                               #   in Loop: Header=BB5_33 Depth=1
	movq	408(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	notl	%eax
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 16(%rsi)
	movq	416(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpb	$0, (%rbx)
	movq	8(%rsi), %rcx
	movq	(%rsp), %r12            # 8-byte Reload
	je	.LBB5_180
# BB#141:                               #   in Loop: Header=BB5_33 Depth=1
	movq	112(%rsp), %rdx         # 8-byte Reload
	cmpb	$0, (%rdx)
	je	.LBB5_143
# BB#142:                               #   in Loop: Header=BB5_33 Depth=1
	movq	168(%r15), %rdx
	movl	24(%rdx), %edx
	andl	$3, %edx
	leal	6(,%rdx,4), %edx
	jmp	.LBB5_144
.LBB5_180:                              #   in Loop: Header=BB5_33 Depth=1
	cmpq	%rax, %rcx
	jae	.LBB5_145
	jmp	.LBB5_181
.LBB5_143:                              #   in Loop: Header=BB5_33 Depth=1
	movl	$12, %edx
.LBB5_144:                              #   in Loop: Header=BB5_33 Depth=1
	movl	%edx, %edx
	addq	%rax, %rdx
	cmpq	%rdx, %rcx
	jb	.LBB5_181
.LBB5_145:                              #   in Loop: Header=BB5_33 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmpq	400(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB5_33
.LBB5_181:                              # %._crit_edge.loopexit
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpb	$0, (%rax)
	jne	.LBB5_182
	jmp	.LBB5_186
.LBB5_61:
	movq	(%rsp), %r12            # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB5_62:                               # %.thread478
	testq	%rbx, %rbx
	je	.LBB5_22
.LBB5_63:
	movq	(%rbx), %rax
.Ltmp273:
	movq	%rbx, %rdi
	callq	*80(%rax)
.Ltmp274:
	jmp	.LBB5_22
.LBB5_177:
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmpb	$0, (%rbx)
	jne	.LBB5_22
# BB#178:
	movl	$1, %ecx
	jmp	.LBB5_27
.LBB5_179:                              # %.._crit_edge_crit_edge
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	104(%rax), %rax
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%r15, %r13
	cmpb	$0, (%rax)
	je	.LBB5_186
.LBB5_182:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	168(%rax), %rdi
.Ltmp266:
	movq	%r13, %rsi
	callq	_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream
	movl	%eax, %ebp
.Ltmp267:
# BB#183:
	testl	%ebp, %ebp
	movq	64(%rsp), %rcx          # 8-byte Reload
	jne	.LBB5_62
# BB#184:
	movq	(%r13), %rax
	leaq	8(%rcx), %rcx
.Ltmp268:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp269:
# BB#185:
	testl	%ebp, %ebp
	movq	64(%rsp), %rsi          # 8-byte Reload
	jne	.LBB5_62
.LBB5_186:
	movq	104(%rsp), %rax         # 8-byte Reload
	movw	%ax, 20(%rsi)
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	jne	.LBB5_63
	jmp	.LBB5_22
.LBB5_187:
	movl	$-2147467263, %ebp      # imm = 0x80004001
	jmp	.LBB5_188
.LBB5_176:
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB5_188:
	movq	8(%rsp), %rbx           # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB5_63
	jmp	.LBB5_22
.LBB5_189:
.Ltmp182:
	movq	%rax, %r14
	jmp	.LBB5_194
.LBB5_190:
.Ltmp209:
	movq	%rax, %r14
	jmp	.LBB5_201
.LBB5_191:
.Ltmp177:
	movq	%rax, %r14
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_193
# BB#192:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_194
.LBB5_193:
	movq	(%rdi), %rax
.Ltmp178:
	callq	*16(%rax)
.Ltmp179:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB5_194:                              # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit400
.Ltmp183:
	leaq	160(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp184:
# BB#195:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
.Ltmp185:
	leaq	144(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp186:
# BB#196:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.1
.Ltmp187:
	leaq	128(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp188:
	jmp	.LBB5_270
.LBB5_197:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp189:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_198:
.Ltmp204:
	movq	%rax, %r14
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_200
# BB#199:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_201
.LBB5_200:
	movq	(%rdi), %rax
.Ltmp205:
	callq	*16(%rax)
.Ltmp206:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB5_201:                              # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit428
.Ltmp210:
	leaq	384(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp211:
# BB#202:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit414
.Ltmp212:
	leaq	368(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp213:
# BB#203:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit414.1
.Ltmp214:
	leaq	352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp215:
# BB#204:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit414.2
.Ltmp216:
	leaq	336(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp217:
	jmp	.LBB5_270
.LBB5_205:                              # %.loopexit.split-lp.loopexit
.Ltmp218:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_206:
.Ltmp140:
	movq	%rax, %r14
.Ltmp141:
	leaq	320(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp142:
	movq	(%rsp), %r12            # 8-byte Reload
# BB#207:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit408
.Ltmp143:
	leaq	304(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp144:
# BB#208:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit408.1
.Ltmp145:
	leaq	288(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp146:
# BB#209:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit408.2
.Ltmp147:
	leaq	272(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp148:
# BB#210:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit408.3
.Ltmp149:
	leaq	256(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp150:
# BB#211:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit408.4
.Ltmp151:
	leaq	240(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp152:
	jmp	.LBB5_270
.LBB5_212:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp153:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_213:
.Ltmp128:
	movq	%rax, %r14
.Ltmp129:
	leaq	288(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp130:
	movq	(%rsp), %r12            # 8-byte Reload
# BB#214:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit402
.Ltmp131:
	leaq	272(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp132:
# BB#215:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit402.1
.Ltmp133:
	leaq	256(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp134:
# BB#216:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit402.2
.Ltmp135:
	leaq	240(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp136:
	jmp	.LBB5_270
.LBB5_217:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp137:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_218:
.Ltmp106:
	movq	%rax, %r14
.Ltmp107:
	leaq	208(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp108:
	movq	(%rsp), %r12            # 8-byte Reload
# BB#219:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit420
.Ltmp109:
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp110:
# BB#220:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit420.1
.Ltmp111:
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp112:
	jmp	.LBB5_270
.LBB5_221:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp113:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_222:
.Ltmp98:
	jmp	.LBB5_232
.LBB5_223:
.Ltmp172:
	jmp	.LBB5_230
.LBB5_224:
.Ltmp270:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB5_230
.LBB5_225:
.Ltmp103:
	jmp	.LBB5_250
.LBB5_226:
.Ltmp63:
	movq	%rax, %r14
	movq	$_ZTV7CBufferIhE+16, 48(%rbp)
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_233
# BB#227:
	callq	_ZdaPv
	jmp	.LBB5_233
.LBB5_228:
.Ltmp262:
	jmp	.LBB5_250
.LBB5_229:                              # %.thread467
.Ltmp254:
.LBB5_230:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit381
	movq	%rax, %r14
	jmp	.LBB5_270
.LBB5_231:
.Ltmp54:
.LBB5_232:                              # %.body
	movq	%rax, %r14
.LBB5_233:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_234:
.Ltmp275:
	jmp	.LBB5_245
.LBB5_235:
.Ltmp243:
	jmp	.LBB5_250
.LBB5_236:                              # %.thread476
.Ltmp235:
	jmp	.LBB5_250
.LBB5_237:
.Ltmp257:
	movq	%rax, %r14
	testq	%rbx, %rbx
	jne	.LBB5_239
# BB#238:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_239:
	movq	(%rbx), %rax
.Ltmp258:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp259:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_240:                              # %.loopexit.split-lp515
.Ltmp91:
	jmp	.LBB5_250
.LBB5_241:
.Ltmp238:
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.LBB5_243
# BB#242:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_243:
	movq	(%r12), %rax
.Ltmp239:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp240:
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_244:
.Ltmp43:
.LBB5_245:                              # %_ZN18COutStreamReleaserD2Ev.exit
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.LBB5_273
	jmp	.LBB5_274
.LBB5_246:
.Ltmp38:
	movq	%r15, %r12
	movq	%rax, %r14
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_272
# BB#247:
	movq	(%rdi), %rax
.Ltmp39:
	callq	*16(%rax)
.Ltmp40:
	jmp	.LBB5_272
.LBB5_248:
.Ltmp265:
	jmp	.LBB5_250
.LBB5_249:                              # %.loopexit514
.Ltmp86:
.LBB5_250:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit381
	movq	%rax, %r14
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB5_270
.LBB5_251:
.Ltmp196:
	movq	%rax, %r14
	leaq	128(%rsp), %rax
	cmpq	%rbx, %rax
	je	.LBB5_270
# BB#252:                               # %.preheader488.preheader
	leaq	128(%rsp), %rbp
	.p2align	4, 0x90
.LBB5_253:                              # %.preheader488
                                        # =>This Inner Loop Header: Depth=1
	addq	$-16, %rbx
.Ltmp197:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp198:
# BB#254:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit395
                                        #   in Loop: Header=BB5_253 Depth=1
	cmpq	%rbx, %rbp
	jne	.LBB5_253
	jmp	.LBB5_270
.LBB5_255:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp199:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_256:
.Ltmp227:
	movq	%rax, %r14
	leaq	336(%rsp), %rax
	cmpq	%rbx, %rax
	je	.LBB5_270
# BB#257:                               # %.preheader.preheader
	leaq	336(%rsp), %rbp
	.p2align	4, 0x90
.LBB5_258:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	$-16, %rbx
.Ltmp228:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp229:
# BB#259:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit422
                                        #   in Loop: Header=BB5_258 Depth=1
	cmpq	%rbx, %rbp
	jne	.LBB5_258
	jmp	.LBB5_270
.LBB5_260:                              # %.loopexit
.Ltmp230:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_261:
.Ltmp120:
	movq	%rax, %r14
	leaq	176(%rsp), %rax
	cmpq	%rbx, %rax
	je	.LBB5_270
# BB#262:                               # %.preheader506.preheader
	leaq	176(%rsp), %rbp
	.p2align	4, 0x90
.LBB5_263:                              # %.preheader506
                                        # =>This Inner Loop Header: Depth=1
	addq	$-16, %rbx
.Ltmp121:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp122:
# BB#264:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit418
                                        #   in Loop: Header=BB5_263 Depth=1
	cmpq	%rbx, %rbp
	jne	.LBB5_263
	jmp	.LBB5_270
.LBB5_265:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp123:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_266:
.Ltmp166:
	movq	%rax, %r14
	leaq	240(%rsp), %rax
	cmpq	%rbx, %rax
	je	.LBB5_270
# BB#267:                               # %.preheader496.preheader
	leaq	240(%rsp), %rbp
	.p2align	4, 0x90
.LBB5_268:                              # %.preheader496
                                        # =>This Inner Loop Header: Depth=1
	addq	$-16, %rbx
.Ltmp167:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp168:
# BB#269:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit406
                                        #   in Loop: Header=BB5_268 Depth=1
	cmpq	%rbx, %rbp
	jne	.LBB5_268
.LBB5_270:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit381
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB5_272
# BB#271:
	movq	(%rdi), %rax
.Ltmp271:
	callq	*80(%rax)
.Ltmp272:
.LBB5_272:                              # %_ZN18COutStreamReleaserD2Ev.exit
	testq	%r12, %r12
	je	.LBB5_274
.LBB5_273:
	movq	(%r12), %rax
.Ltmp276:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp277:
.LBB5_274:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_275:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp278:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_276:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp169:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE, .Lfunc_end5-_ZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\240\005"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\227\005"              # Call site table length
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp37-.Ltmp20         #   Call between .Ltmp20 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin2   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp44-.Ltmp42         #   Call between .Ltmp42 and .Ltmp44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp51-.Ltmp44         #   Call between .Ltmp44 and .Ltmp51
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp60-.Ltmp55         #   Call between .Ltmp55 and .Ltmp60
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp79-.Ltmp64         #   Call between .Ltmp64 and .Ltmp79
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp91-.Lfunc_begin2   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp85-.Ltmp82         #   Call between .Ltmp82 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin2   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp90-.Ltmp87         #   Call between .Ltmp87 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin2   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp125-.Ltmp92        #   Call between .Ltmp92 and .Ltmp125
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin2  # >> Call Site 13 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin2  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin2  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp165-.Ltmp154       #   Call between .Ltmp154 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin2  #     jumps to .Ltmp166
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Ltmp249-.Ltmp244       #   Call between .Ltmp244 and .Ltmp249
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin2  # >> Call Site 17 <<
	.long	.Ltmp253-.Ltmp250       #   Call between .Ltmp250 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin2  #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin2  # >> Call Site 18 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin2  #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin2  # >> Call Site 19 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin2  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin2   # >> Call Site 20 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin2   # >> Call Site 21 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin2   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin2   # >> Call Site 22 <<
	.long	.Ltmp102-.Ltmp99        #   Call between .Ltmp99 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin2  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin2  # >> Call Site 23 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin2  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin2  # >> Call Site 24 <<
	.long	.Ltmp119-.Ltmp114       #   Call between .Ltmp114 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin2  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin2  # >> Call Site 25 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin2  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin2  # >> Call Site 26 <<
	.long	.Ltmp203-.Ltmp200       #   Call between .Ltmp200 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin2  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin2  # >> Call Site 27 <<
	.long	.Ltmp176-.Ltmp173       #   Call between .Ltmp173 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin2  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp207-.Lfunc_begin2  # >> Call Site 28 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin2  #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin2  # >> Call Site 29 <<
	.long	.Ltmp226-.Ltmp219       #   Call between .Ltmp219 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin2  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin2  # >> Call Site 30 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin2  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin2  # >> Call Site 31 <<
	.long	.Ltmp195-.Ltmp190       #   Call between .Ltmp190 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin2  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin2  # >> Call Site 32 <<
	.long	.Ltmp234-.Ltmp231       #   Call between .Ltmp231 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin2  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin2  # >> Call Site 33 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin2  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin2  # >> Call Site 34 <<
	.long	.Ltmp242-.Ltmp241       #   Call between .Ltmp241 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin2  #     jumps to .Ltmp243
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin2  # >> Call Site 35 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin2  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp273-.Lfunc_begin2  # >> Call Site 36 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin2  #     jumps to .Ltmp275
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin2  # >> Call Site 37 <<
	.long	.Ltmp269-.Ltmp266       #   Call between .Ltmp266 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin2  #     jumps to .Ltmp270
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin2  # >> Call Site 38 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp278-.Lfunc_begin2  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.long	.Ltmp183-.Lfunc_begin2  # >> Call Site 39 <<
	.long	.Ltmp188-.Ltmp183       #   Call between .Ltmp183 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin2  #     jumps to .Ltmp189
	.byte	1                       #   On action: 1
	.long	.Ltmp205-.Lfunc_begin2  # >> Call Site 40 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp278-.Lfunc_begin2  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin2  # >> Call Site 41 <<
	.long	.Ltmp217-.Ltmp210       #   Call between .Ltmp210 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin2  #     jumps to .Ltmp218
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin2  # >> Call Site 42 <<
	.long	.Ltmp152-.Ltmp141       #   Call between .Ltmp141 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin2  #     jumps to .Ltmp153
	.byte	1                       #   On action: 1
	.long	.Ltmp129-.Lfunc_begin2  # >> Call Site 43 <<
	.long	.Ltmp136-.Ltmp129       #   Call between .Ltmp129 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin2  #     jumps to .Ltmp137
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin2  # >> Call Site 44 <<
	.long	.Ltmp112-.Ltmp107       #   Call between .Ltmp107 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin2  #     jumps to .Ltmp113
	.byte	1                       #   On action: 1
	.long	.Ltmp258-.Lfunc_begin2  # >> Call Site 45 <<
	.long	.Ltmp40-.Ltmp258        #   Call between .Ltmp258 and .Ltmp40
	.long	.Ltmp278-.Lfunc_begin2  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.long	.Ltmp197-.Lfunc_begin2  # >> Call Site 46 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin2  #     jumps to .Ltmp199
	.byte	1                       #   On action: 1
	.long	.Ltmp228-.Lfunc_begin2  # >> Call Site 47 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin2  #     jumps to .Ltmp230
	.byte	1                       #   On action: 1
	.long	.Ltmp121-.Lfunc_begin2  # >> Call Site 48 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin2  #     jumps to .Ltmp123
	.byte	1                       #   On action: 1
	.long	.Ltmp167-.Lfunc_begin2  # >> Call Site 49 <<
	.long	.Ltmp168-.Ltmp167       #   Call between .Ltmp167 and .Ltmp168
	.long	.Ltmp169-.Lfunc_begin2  #     jumps to .Ltmp169
	.byte	1                       #   On action: 1
	.long	.Ltmp271-.Lfunc_begin2  # >> Call Site 50 <<
	.long	.Ltmp277-.Ltmp271       #   Call between .Ltmp271 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin2  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.long	.Ltmp277-.Lfunc_begin2  # >> Call Site 51 <<
	.long	.Lfunc_end5-.Ltmp277    #   Call between .Ltmp277 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB6_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB6_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB6_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB6_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB6_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB6_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB6_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB6_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB6_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB6_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB6_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB6_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB6_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB6_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB6_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB6_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB6_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end6-_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaEncoder6AddRefEv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv,@function
_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv: # @_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv, .Lfunc_end7-_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv,@function
_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv: # @_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB8_2:
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv, .Lfunc_end8-_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaEncoderD2Ev,"axG",@progbits,_ZN8NArchive4NZip12CLzmaEncoderD2Ev,comdat
	.weak	_ZN8NArchive4NZip12CLzmaEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoderD2Ev,@function
_ZN8NArchive4NZip12CLzmaEncoderD2Ev:    # @_ZN8NArchive4NZip12CLzmaEncoderD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive4NZip12CLzmaEncoderE+16, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB9_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB9_1:                                # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NZip12CLzmaEncoderD2Ev, .Lfunc_end9-_ZN8NArchive4NZip12CLzmaEncoderD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaEncoderD0Ev,"axG",@progbits,_ZN8NArchive4NZip12CLzmaEncoderD0Ev,comdat
	.weak	_ZN8NArchive4NZip12CLzmaEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaEncoderD0Ev,@function
_ZN8NArchive4NZip12CLzmaEncoderD0Ev:    # @_ZN8NArchive4NZip12CLzmaEncoderD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip12CLzmaEncoderE+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp279:
	callq	*16(%rax)
.Ltmp280:
.LBB10_2:                               # %_ZN8NArchive4NZip12CLzmaEncoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_3:
.Ltmp281:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NArchive4NZip12CLzmaEncoderD0Ev, .Lfunc_end10-_ZN8NArchive4NZip12CLzmaEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp279-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin3  #     jumps to .Ltmp281
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp280   #   Call between .Ltmp280 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIhEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIhEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIhEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhEC2ERKS0_,@function
_ZN13CRecordVectorIhEC2ERKS0_:          # @_ZN13CRecordVectorIhEC2ERKS0_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$1, 24(%r12)
	movq	$_ZTV13CRecordVectorIhE+16, (%r12)
.Ltmp282:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp283:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp284:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp285:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB11_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movzbl	(%rax,%rbx), %ebp
.Ltmp287:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp288:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movb	%bpl, (%rax,%rcx)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB11_4
.LBB11_6:                               # %_ZN13CRecordVectorIhEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_8:                               # %.loopexit.split-lp
.Ltmp286:
	jmp	.LBB11_9
.LBB11_7:                               # %.loopexit
.Ltmp289:
.LBB11_9:
	movq	%rax, %r14
.Ltmp290:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp291:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_11:
.Ltmp292:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN13CRecordVectorIhEC2ERKS0_, .Lfunc_end11-_ZN13CRecordVectorIhEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp282-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp285-.Ltmp282       #   Call between .Ltmp282 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin4  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin4  #     jumps to .Ltmp289
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin4  #     jumps to .Ltmp292
	.byte	1                       #   On action: 1
	.long	.Ltmp291-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp291   #   Call between .Ltmp291 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIhED0Ev,"axG",@progbits,_ZN13CRecordVectorIhED0Ev,comdat
	.weak	_ZN13CRecordVectorIhED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhED0Ev,@function
_ZN13CRecordVectorIhED0Ev:              # @_ZN13CRecordVectorIhED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp293:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp294:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp295:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN13CRecordVectorIhED0Ev, .Lfunc_end12-_ZN13CRecordVectorIhED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp293-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin5  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp294   #   Call between .Ltmp294 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB13_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB13_1:
	retq
.Lfunc_end13:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end13-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 16
.Lcfi74:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#1:
	callq	_ZdaPv
.LBB14_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end14:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end14-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.type	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs,@object # @_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs
	.section	.rodata,"a",@progbits
	.p2align	4
.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs:
	.long	13                      # 0xd
	.long	12                      # 0xc
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.size	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs, 24

	.type	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0,@object # @_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0
	.p2align	2
.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0:
	.long	12                      # 0xc
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_0, 12

	.type	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_1,@object # @_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_1:
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	10                      # 0xa
	.size	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_1, 16

	.type	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2,@object # @_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2
	.section	.rodata,"a",@progbits
	.p2align	2
.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2:
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	13                      # 0xd
	.size	.L_ZZN8NArchive4NZip10CAddCommon8CompressEP19ISequentialInStreamP10IOutStreamP21ICompressProgressInfoRNS0_18CCompressingResultEE7propIDs_2, 12

	.type	_ZTVN8NArchive4NZip12CLzmaEncoderE,@object # @_ZTVN8NArchive4NZip12CLzmaEncoderE
	.globl	_ZTVN8NArchive4NZip12CLzmaEncoderE
	.p2align	3
_ZTVN8NArchive4NZip12CLzmaEncoderE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip12CLzmaEncoderE
	.quad	_ZN8NArchive4NZip12CLzmaEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip12CLzmaEncoder6AddRefEv
	.quad	_ZN8NArchive4NZip12CLzmaEncoder7ReleaseEv
	.quad	_ZN8NArchive4NZip12CLzmaEncoderD2Ev
	.quad	_ZN8NArchive4NZip12CLzmaEncoderD0Ev
	.quad	_ZN8NArchive4NZip12CLzmaEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.size	_ZTVN8NArchive4NZip12CLzmaEncoderE, 64

	.type	_ZTSN8NArchive4NZip12CLzmaEncoderE,@object # @_ZTSN8NArchive4NZip12CLzmaEncoderE
	.globl	_ZTSN8NArchive4NZip12CLzmaEncoderE
	.p2align	4
_ZTSN8NArchive4NZip12CLzmaEncoderE:
	.asciz	"N8NArchive4NZip12CLzmaEncoderE"
	.size	_ZTSN8NArchive4NZip12CLzmaEncoderE, 31

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NZip12CLzmaEncoderE,@object # @_ZTIN8NArchive4NZip12CLzmaEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip12CLzmaEncoderE
	.p2align	4
_ZTIN8NArchive4NZip12CLzmaEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip12CLzmaEncoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NZip12CLzmaEncoderE, 56

	.type	_ZTV13CRecordVectorIhE,@object # @_ZTV13CRecordVectorIhE
	.section	.rodata._ZTV13CRecordVectorIhE,"aG",@progbits,_ZTV13CRecordVectorIhE,comdat
	.weak	_ZTV13CRecordVectorIhE
	.p2align	3
_ZTV13CRecordVectorIhE:
	.quad	0
	.quad	_ZTI13CRecordVectorIhE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIhED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIhE, 40

	.type	_ZTS13CRecordVectorIhE,@object # @_ZTS13CRecordVectorIhE
	.section	.rodata._ZTS13CRecordVectorIhE,"aG",@progbits,_ZTS13CRecordVectorIhE,comdat
	.weak	_ZTS13CRecordVectorIhE
	.p2align	4
_ZTS13CRecordVectorIhE:
	.asciz	"13CRecordVectorIhE"
	.size	_ZTS13CRecordVectorIhE, 19

	.type	_ZTI13CRecordVectorIhE,@object # @_ZTI13CRecordVectorIhE
	.section	.rodata._ZTI13CRecordVectorIhE,"aG",@progbits,_ZTI13CRecordVectorIhE,comdat
	.weak	_ZTI13CRecordVectorIhE
	.p2align	4
_ZTI13CRecordVectorIhE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIhE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIhE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16


	.globl	_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE
	.type	_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE,@function
_ZN8NArchive4NZip10CAddCommonC1ERKNS0_22CCompressionMethodModeE = _ZN8NArchive4NZip10CAddCommonC2ERKNS0_22CCompressionMethodModeE
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
