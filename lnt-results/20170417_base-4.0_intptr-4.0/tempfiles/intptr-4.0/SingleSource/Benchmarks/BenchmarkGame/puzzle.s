	.text
	.file	"puzzle.bc"
	.globl	rand
	.p2align	4, 0x90
	.type	rand,@function
rand:                                   # @rand
	.cfi_startproc
# BB#0:
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	movl	%edx, %ecx
	shll	$15, %ecx
	subl	%edx, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	retq
.Lfunc_end0:
	.size	rand, .Lfunc_end0-rand
	.cfi_endproc

	.globl	srand
	.p2align	4, 0x90
	.type	srand,@function
srand:                                  # @srand
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	%rax, next(%rip)
	retq
.Lfunc_end1:
	.size	srand, .Lfunc_end1-srand
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4539628424389459968     # double 3.0517578125E-5
	.text
	.globl	randInt
	.p2align	4, 0x90
	.type	randInt,@function
randInt:                                # @randInt
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	subl	%edi, %esi
	incl	%esi
	cvtsi2sdl	%esi, %xmm0
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	movl	%edx, %ecx
	shll	$15, %ecx
	subl	%edx, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	.LCPI2_0(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	leal	(%rdi,%rax), %ecx
	xorl	%edx, %edx
	cmpl	%esi, %eax
	movl	$-1, %eax
	cmovnel	%edx, %eax
	addl	%ecx, %eax
	retq
.Lfunc_end2:
	.size	randInt, .Lfunc_end2-randInt
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI3_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_2:
	.quad	4539628424389459968     # double 3.0517578125E-5
	.text
	.globl	shuffle
	.p2align	4, 0x90
	.type	shuffle,@function
shuffle:                                # @shuffle
	.cfi_startproc
# BB#0:
	decl	%esi
	je	.LBB3_4
# BB#1:                                 # %.lr.ph
	movslq	%esi, %r8
	movq	next(%rip), %rax
	incq	%r8
	movdqa	.LCPI3_0(%rip), %xmm0   # xmm0 = [1127219200,1160773632,0,0]
	movapd	.LCPI3_1(%rip), %xmm1   # xmm1 = [4.503600e+15,1.934281e+25]
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movd	%r8, %xmm4
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	subpd	%xmm1, %xmm4
	pshufd	$78, %xmm4, %xmm3       # xmm3 = xmm4[2,3,0,1]
	addpd	%xmm4, %xmm3
	imulq	$1103515245, %rax, %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, %rdx
	shrq	$16, %rdx
	movl	%edx, %esi
	imulq	$131077, %rsi, %rsi     # imm = 0x20005
	shrq	$32, %rsi
	movl	%edx, %ecx
	subl	%esi, %ecx
	shrl	%ecx
	addl	%esi, %ecx
	shrl	$14, %ecx
	movl	%ecx, %esi
	shll	$15, %esi
	subl	%ecx, %esi
	negl	%esi
	leal	1(%rdx,%rsi), %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm4
	cvttsd2si	%xmm4, %ecx
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	cmpq	%rcx, %r8
	sete	%dl
	subq	%rdx, %rcx
	movl	-4(%rdi,%r8,4), %edx
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, -4(%rdi,%r8,4)
	movl	%edx, (%rdi,%rcx,4)
	decq	%r8
	cmpq	$1, %r8
	jne	.LBB3_2
# BB#3:                                 # %._crit_edge
	movq	%rax, next(%rip)
.LBB3_4:
	retq
.Lfunc_end3:
	.size	shuffle, .Lfunc_end3-shuffle
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI4_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI4_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI4_4:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI4_5:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_3:
	.quad	4539628424389459968     # double 3.0517578125E-5
	.text
	.globl	createRandomArray
	.p2align	4, 0x90
	.type	createRandomArray,@function
createRandomArray:                      # @createRandomArray
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	%edi, %r14d
	leal	1(%r14), %ebx
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	testl	%r14d, %r14d
	js	.LBB4_9
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebx, %ecx
	cmpl	$8, %ebx
	jae	.LBB4_3
# BB#2:
	xorl	%edx, %edx
	jmp	.LBB4_8
.LBB4_3:                                # %min.iters.checked
	andl	$7, %ebx
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	je	.LBB4_4
# BB#5:                                 # %vector.body.preheader
	movq	%rax, %rsi
	addq	$16, %rsi
	movdqa	.LCPI4_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI4_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI4_2(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB4_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	paddd	%xmm2, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB4_6
# BB#7:                                 # %middle.block
	testl	%ebx, %ebx
	jne	.LBB4_8
	jmp	.LBB4_9
.LBB4_4:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rax,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %rcx
	jne	.LBB4_8
.LBB4_9:                                # %._crit_edge
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	imulq	$1103515245, next(%rip), %rcx # imm = 0x41C64E6D
	addq	$12345, %rcx            # imm = 0x3039
	movq	%rcx, next(%rip)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	movl	%edx, %esi
	imulq	$131077, %rsi, %rsi     # imm = 0x20005
	shrq	$32, %rsi
	movl	%edx, %edi
	subl	%esi, %edi
	shrl	%edi
	addl	%esi, %edi
	shrl	$14, %edi
	movl	%edi, %esi
	shll	$15, %esi
	subl	%edi, %esi
	negl	%esi
	leal	1(%rdx,%rsi), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	.LCPI4_3(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %edx
	xorl	%esi, %esi
	cmpl	%r14d, %edx
	movl	$-1, %edi
	cmovnel	%esi, %edi
	leal	1(%rdx,%rdi), %edx
	movl	%edx, (%rax)
	testl	%r14d, %r14d
	je	.LBB4_13
# BB#10:                                # %.lr.ph.i
	movslq	%r14d, %rdx
	incq	%rdx
	movdqa	.LCPI4_4(%rip), %xmm0   # xmm0 = [1127219200,1160773632,0,0]
	movapd	.LCPI4_5(%rip), %xmm1   # xmm1 = [4.503600e+15,1.934281e+25]
	movsd	.LCPI4_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movd	%rdx, %xmm4
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	subpd	%xmm1, %xmm4
	pshufd	$78, %xmm4, %xmm3       # xmm3 = xmm4[2,3,0,1]
	addpd	%xmm4, %xmm3
	imulq	$1103515245, %rcx, %rcx # imm = 0x41C64E6D
	addq	$12345, %rcx            # imm = 0x3039
	movq	%rcx, %rsi
	shrq	$16, %rsi
	movl	%esi, %edi
	imulq	$131077, %rdi, %rdi     # imm = 0x20005
	shrq	$32, %rdi
	movl	%esi, %ebx
	subl	%edi, %ebx
	shrl	%ebx
	addl	%edi, %ebx
	shrl	$14, %ebx
	movl	%ebx, %edi
	shll	$15, %edi
	subl	%ebx, %edi
	negl	%edi
	leal	1(%rsi,%rdi), %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm4
	cvttsd2si	%xmm4, %esi
	movslq	%esi, %rsi
	xorl	%edi, %edi
	cmpq	%rsi, %rdx
	sete	%dil
	subq	%rdi, %rsi
	movl	-4(%rax,%rdx,4), %edi
	movl	(%rax,%rsi,4), %ebx
	movl	%ebx, -4(%rax,%rdx,4)
	movl	%edi, (%rax,%rsi,4)
	decq	%rdx
	cmpq	$1, %rdx
	jne	.LBB4_11
# BB#12:                                # %._crit_edge.i
	movq	%rcx, next(%rip)
.LBB4_13:                               # %shuffle.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	createRandomArray, .Lfunc_end4-createRandomArray
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI5_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI5_2:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI5_3:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.text
	.globl	findDuplicate
	.p2align	4, 0x90
	.type	findDuplicate,@function
findDuplicate:                          # @findDuplicate
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB5_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ecx
	cmpl	$8, %esi
	jb	.LBB5_3
# BB#4:                                 # %min.iters.checked
	movl	%esi, %r8d
	andl	$7, %r8d
	movq	%rcx, %rdx
	subq	%r8, %rdx
	je	.LBB5_3
# BB#5:                                 # %vector.body.preheader
	leaq	16(%rdi), %r10
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [2,3]
	pxor	%xmm1, %xmm1
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI5_2(%rip), %xmm9   # xmm9 = [5,5,5,5]
	movdqa	.LCPI5_3(%rip), %xmm10  # xmm10 = [8,8]
	movq	%rdx, %r9
	pxor	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB5_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm7
	shufps	$136, %xmm2, %xmm7      # xmm7 = xmm7[0,2],xmm2[0,2]
	movaps	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	paddd	%xmm9, %xmm7
	movdqu	-16(%r10), %xmm4
	movdqu	(%r10), %xmm5
	pxor	%xmm4, %xmm1
	pxor	%xmm3, %xmm1
	pxor	%xmm5, %xmm6
	pxor	%xmm7, %xmm6
	paddq	%xmm10, %xmm0
	paddq	%xmm10, %xmm2
	addq	$32, %r10
	addq	$-8, %r9
	jne	.LBB5_6
# BB#7:                                 # %middle.block
	pxor	%xmm1, %xmm6
	pshufd	$78, %xmm6, %xmm0       # xmm0 = xmm6[2,3,0,1]
	pxor	%xmm6, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pxor	%xmm0, %xmm1
	movd	%xmm1, %eax
	testl	%r8d, %r8d
	jne	.LBB5_8
	jmp	.LBB5_10
.LBB5_3:
	xorl	%edx, %edx
	xorl	%eax, %eax
.LBB5_8:                                # %.lr.ph.preheader21
	leaq	(%rdi,%rdx,4), %rdi
	subq	%rdx, %rcx
	incl	%edx
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %eax
	xorl	(%rdi), %eax
	addq	$4, %rdi
	incl	%edx
	decq	%rcx
	jne	.LBB5_9
.LBB5_10:                               # %._crit_edge
	xorl	%esi, %eax
	retq
.LBB5_1:
	xorl	%eax, %eax
	xorl	%esi, %eax
	retq
.Lfunc_end5:
	.size	findDuplicate, .Lfunc_end5-findDuplicate
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI6_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI6_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI6_3:
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI6_4:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
.LCPI6_5:
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
.LCPI6_6:
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
.LCPI6_7:
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
.LCPI6_8:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
.LCPI6_9:
	.long	36                      # 0x24
	.long	36                      # 0x24
	.long	36                      # 0x24
	.long	36                      # 0x24
.LCPI6_10:
	.long	40                      # 0x28
	.long	40                      # 0x28
	.long	40                      # 0x28
	.long	40                      # 0x28
.LCPI6_13:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI6_14:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
.LCPI6_15:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI6_16:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI6_17:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI6_18:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
.LCPI6_19:
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
.LCPI6_20:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_11:
	.quad	4539628424389459968     # double 3.0517578125E-5
.LCPI6_12:
	.quad	4692333547057315840     # double 5.0E+5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$1, next(%rip)
	movl	$-1, %r14d
	movl	$1, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_1:                                # %min.iters.checked34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
                                        #     Child Loop BB6_4 Depth 2
                                        #     Child Loop BB6_6 Depth 2
                                        #       Child Loop BB6_7 Depth 3
	movl	$2000004, %edi          # imm = 0x1E8484
	callq	malloc
	movl	$36, %ecx
	movdqa	.LCPI6_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI6_1(%rip), %xmm3   # xmm3 = [4,4,4,4]
	movdqa	%xmm3, %xmm8
	movdqa	.LCPI6_2(%rip), %xmm4   # xmm4 = [8,8,8,8]
	movdqa	%xmm4, %xmm9
	movdqa	.LCPI6_3(%rip), %xmm5   # xmm5 = [12,12,12,12]
	movdqa	%xmm5, %xmm10
	movdqa	.LCPI6_4(%rip), %xmm6   # xmm6 = [16,16,16,16]
	movdqa	%xmm6, %xmm11
	movdqa	.LCPI6_5(%rip), %xmm7   # xmm7 = [20,20,20,20]
	movdqa	%xmm7, %xmm12
	movdqa	.LCPI6_6(%rip), %xmm3   # xmm3 = [24,24,24,24]
	movdqa	.LCPI6_7(%rip), %xmm4   # xmm4 = [28,28,28,28]
	movdqa	.LCPI6_8(%rip), %xmm5   # xmm5 = [32,32,32,32]
	movdqa	.LCPI6_9(%rip), %xmm6   # xmm6 = [36,36,36,36]
	movdqa	.LCPI6_10(%rip), %xmm7  # xmm7 = [40,40,40,40]
	.p2align	4, 0x90
.LBB6_2:                                # %vector.body31
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm0, %xmm1
	paddd	%xmm8, %xmm1
	movdqu	%xmm0, -144(%rax,%rcx,4)
	movdqu	%xmm1, -128(%rax,%rcx,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm9, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm10, %xmm2
	movdqu	%xmm1, -112(%rax,%rcx,4)
	movdqu	%xmm2, -96(%rax,%rcx,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm11, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm12, %xmm2
	movdqu	%xmm1, -80(%rax,%rcx,4)
	movdqu	%xmm2, -64(%rax,%rcx,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm4, %xmm2
	movdqu	%xmm1, -48(%rax,%rcx,4)
	movdqu	%xmm2, -32(%rax,%rcx,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm6, %xmm2
	movdqu	%xmm1, -16(%rax,%rcx,4)
	movdqu	%xmm2, (%rax,%rcx,4)
	paddd	%xmm7, %xmm0
	addq	$40, %rcx
	cmpq	$500036, %rcx           # imm = 0x7A144
	jne	.LBB6_2
# BB#3:                                 # %.lr.ph.i17
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	$500000, 2000000(%rax)  # imm = 0x7A120
	imulq	$1103515245, next(%rip), %rcx # imm = 0x41C64E6D
	addq	$12345, %rcx            # imm = 0x3039
	movq	%rcx, next(%rip)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	movl	%edx, %esi
	imulq	$131077, %rsi, %rsi     # imm = 0x20005
	shrq	$32, %rsi
	movl	%edx, %edi
	subl	%esi, %edi
	shrl	%edi
	addl	%esi, %edi
	shrl	$14, %edi
	movl	%edi, %esi
	shll	$15, %esi
	subl	%edi, %esi
	negl	%esi
	leal	1(%rdx,%rsi), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	.LCPI6_11(%rip), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	mulsd	.LCPI6_12(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	cmpl	$500000, %edx           # imm = 0x7A120
	movl	$0, %esi
	cmovel	%r14d, %esi
	leal	1(%rdx,%rsi), %edx
	movl	%edx, (%rax)
	movl	$500001, %edx           # imm = 0x7A121
	movdqa	.LCPI6_13(%rip), %xmm3  # xmm3 = [1127219200,1160773632,0,0]
	movapd	.LCPI6_14(%rip), %xmm4  # xmm4 = [4.503600e+15,1.934281e+25]
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%rdx, %xmm1
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	subpd	%xmm4, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	imulq	$1103515245, %rcx, %rcx # imm = 0x41C64E6D
	addq	$12345, %rcx            # imm = 0x3039
	movq	%rcx, %rsi
	shrq	$16, %rsi
	movl	%esi, %edi
	imulq	$131077, %rdi, %rdi     # imm = 0x20005
	shrq	$32, %rdi
	movl	%esi, %ebx
	subl	%edi, %ebx
	shrl	%ebx
	addl	%edi, %ebx
	shrl	$14, %ebx
	movl	%ebx, %edi
	shll	$15, %edi
	subl	%ebx, %edi
	negl	%edi
	leal	1(%rsi,%rdi), %esi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %esi
	movslq	%esi, %rsi
	xorl	%edi, %edi
	cmpq	%rsi, %rdx
	sete	%dil
	subq	%rdi, %rsi
	movl	-4(%rax,%rdx,4), %edi
	movl	(%rax,%rsi,4), %ebx
	movl	%ebx, -4(%rax,%rdx,4)
	movl	%edi, (%rax,%rsi,4)
	decq	%rdx
	cmpq	$1, %rdx
	jne	.LBB6_4
# BB#5:                                 # %createRandomArray.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	%rcx, next(%rip)
	xorl	%ecx, %ecx
	movaps	.LCPI6_15(%rip), %xmm9  # xmm9 = [2,3]
	movdqa	.LCPI6_16(%rip), %xmm10 # xmm10 = [1,1,1,1]
	movdqa	.LCPI6_17(%rip), %xmm11 # xmm11 = [5,5,5,5]
	movdqa	.LCPI6_18(%rip), %xmm12 # xmm12 = [9,9,9,9]
	movdqa	.LCPI6_19(%rip), %xmm13 # xmm13 = [13,13,13,13]
	movdqa	.LCPI6_20(%rip), %xmm14 # xmm14 = [16,16]
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph.i.preheader
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_7 Depth 3
	movd	%rbp, %xmm8
	pslldq	$8, %xmm8               # xmm8 = zero,zero,zero,zero,zero,zero,zero,zero,xmm8[0,1,2,3,4,5,6,7]
	xorpd	%xmm1, %xmm1
	movl	$12, %edx
	movaps	%xmm9, %xmm3
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB6_7:                                # %vector.body
                                        #   Parent Loop BB6_1 Depth=1
                                        #     Parent Loop BB6_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm8, %xmm4
	shufps	$136, %xmm3, %xmm4      # xmm4 = xmm4[0,2],xmm3[0,2]
	movaps	%xmm4, %xmm5
	paddd	%xmm10, %xmm5
	movaps	%xmm4, %xmm6
	paddd	%xmm11, %xmm6
	movupd	-48(%rax,%rdx,4), %xmm7
	movupd	-32(%rax,%rdx,4), %xmm0
	xorpd	%xmm1, %xmm7
	xorpd	%xmm5, %xmm7
	xorpd	%xmm2, %xmm0
	xorpd	%xmm6, %xmm0
	movaps	%xmm4, %xmm5
	paddd	%xmm12, %xmm5
	paddd	%xmm13, %xmm4
	movdqu	-16(%rax,%rdx,4), %xmm1
	movdqu	(%rax,%rdx,4), %xmm2
	pxor	%xmm5, %xmm1
	pxor	%xmm7, %xmm1
	pxor	%xmm4, %xmm2
	pxor	%xmm0, %xmm2
	paddq	%xmm14, %xmm8
	paddq	%xmm14, %xmm3
	addq	$16, %rdx
	cmpq	$500012, %rdx           # imm = 0x7A12C
	jne	.LBB6_7
# BB#8:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB6_6 Depth=2
	incl	%ecx
	cmpl	$200, %ecx
	jne	.LBB6_6
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	pxor	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	pxor	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pxor	%xmm0, %xmm1
	movd	%xmm1, %ebx
	xorl	2000000(%rax), %ebx
	movq	%rax, %rdi
	callq	free
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	incl	%r15d
	cmpl	$5, %r15d
	jne	.LBB6_1
# BB#10:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	next,@object            # @next
	.data
	.p2align	3
next:
	.quad	1                       # 0x1
	.size	next, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Found duplicate: %d\n"
	.size	.L.str, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
