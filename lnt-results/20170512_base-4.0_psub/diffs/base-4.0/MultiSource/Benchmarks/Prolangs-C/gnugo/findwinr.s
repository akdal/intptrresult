	.text
	.file	"findwinr.bc"
	.globl	findwinner
	.p2align	4, 0x90
	.type	findwinner,@function
findwinner:                             # @findwinner
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	$-1, (%rdi)
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movl	$-1, (%rsi)
	movl	$-1, (%rbx)
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_13 Depth 4
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_13 Depth 4
	imulq	$19, 16(%rsp), %rax     # 8-byte Folded Reload
	movzbl	p(%rax,%r12), %ecx
	cmpl	umove(%rip), %ecx
	jne	.LBB0_21
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=2
	cmpb	$3, l(%rax,%r12)
	ja	.LBB0_21
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=2
	leaq	l(%rax,%r12), %rbp
	movl	$0, 52(%rsp)
	callq	initmark
	movl	umove(%rip), %r8d
	movzbl	(%rbp), %r9d
	leaq	52(%rsp), %rax
	movq	%rax, (%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r12d, %esi
	leaq	40(%rsp), %rdx
	leaq	28(%rsp), %rcx
	callq	findopen
	testl	%eax, %eax
	je	.LBB0_21
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=2
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB0_21
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=2
	cmpb	$1, %al
	jne	.LBB0_7
# BB#10:                                #   in Loop: Header=BB0_2 Depth=2
	cmpl	$119, (%rbx)
	jg	.LBB0_21
# BB#11:                                #   in Loop: Header=BB0_2 Depth=2
	movl	$120, (%rbx)
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	28(%rsp), %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	jmp	.LBB0_21
.LBB0_7:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_2 Depth=2
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_13 Depth 4
	testb	%al, %al
	je	.LBB0_9
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB0_8 Depth=3
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%r15, %r14
	je	.LBB0_19
# BB#14:                                #   in Loop: Header=BB0_13 Depth=4
	movl	$0, lib(%rip)
	movl	40(%rsp,%r14,4), %edi
	movl	28(%rsp,%r14,4), %esi
	movl	mymove(%rip), %edx
	callq	countlib
	movl	lib(%rip), %r13d
	testl	%r13d, %r13d
	jle	.LBB0_19
# BB#15:                                #   in Loop: Header=BB0_13 Depth=4
	movzbl	mymove(%rip), %eax
	movslq	40(%rsp,%r14,4), %rcx
	movslq	28(%rsp,%r14,4), %rdx
	imulq	$19, %rcx, %rcx
	movb	%al, p(%rcx,%rdx)
	movl	$0, lib(%rip)
	movl	40(%rsp,%r15,4), %edi
	movl	28(%rsp,%r15,4), %esi
	movl	umove(%rip), %edx
	callq	countlib
	movl	lib(%rip), %eax
	imull	$-20, %eax, %ecx
	addl	$120, %ecx
	testl	%eax, %eax
	movl	%ecx, %eax
	movl	$0, %edx
	cmovgl	%edx, %eax
	cmpl	$1, %r13d
	cmovnel	%ecx, %eax
	cmpl	%eax, (%rbx)
	jge	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_13 Depth=4
	movl	%eax, (%rbx)
	movl	40(%rsp,%r14,4), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	28(%rsp,%r14,4), %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx)
	jmp	.LBB0_18
.LBB0_16:                               # %._crit_edge77
                                        #   in Loop: Header=BB0_13 Depth=4
	movl	40(%rsp,%r14,4), %eax
	movl	28(%rsp,%r14,4), %ecx
.LBB0_18:                               #   in Loop: Header=BB0_13 Depth=4
	cltq
	movslq	%ecx, %rcx
	imulq	$19, %rax, %rax
	movb	$0, p(%rax,%rcx)
.LBB0_19:                               #   in Loop: Header=BB0_13 Depth=4
	incq	%r15
	movzbl	(%rbp), %eax
	cmpq	%rax, %r15
	jl	.LBB0_13
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=3
	xorl	%eax, %eax
.LBB0_20:                               # %._crit_edge
                                        #   in Loop: Header=BB0_8 Depth=3
	incq	%r14
	movzbl	%al, %ecx
	cmpq	%rcx, %r14
	jl	.LBB0_8
	.p2align	4, 0x90
.LBB0_21:                               # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=2
	incq	%r12
	cmpq	$19, %r12
	jne	.LBB0_2
# BB#22:                                #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$19, %rcx
	jne	.LBB0_1
# BB#23:
	xorl	%eax, %eax
	cmpl	$0, (%rbx)
	setg	%al
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	findwinner, .Lfunc_end0-findwinner
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
