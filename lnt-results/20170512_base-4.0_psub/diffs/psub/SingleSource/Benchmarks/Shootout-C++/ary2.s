	.text
	.file	"ary2.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	cmpl	$2, %edi
	jne	.LBB0_1
# BB#11:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	testl	%eax, %eax
	js	.LBB0_13
# BB#12:
	movslq	%eax, %r12
	jmp	.LBB0_2
.LBB0_1:
	movl	$9000000, %r12d         # imm = 0x895440
.LBB0_2:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i
	leaq	(,%r12,4), %r15
.Ltmp0:
	movq	%r15, %rdi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i84
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
.Ltmp2:
	movq	%r15, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp3:
# BB#4:                                 # %.lr.ph166.preheader
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memset
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph166
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, (%r13,%rbx,4)
	leal	1(%rbx), %eax
	movl	%eax, 4(%r13,%rbx,4)
	leal	2(%rbx), %eax
	movl	%eax, 8(%r13,%rbx,4)
	leal	3(%rbx), %eax
	movl	%eax, 12(%r13,%rbx,4)
	leal	4(%rbx), %eax
	movl	%eax, 16(%r13,%rbx,4)
	leal	5(%rbx), %eax
	movl	%eax, 20(%r13,%rbx,4)
	leal	6(%rbx), %eax
	movl	%eax, 24(%r13,%rbx,4)
	leal	7(%rbx), %eax
	movl	%eax, 28(%r13,%rbx,4)
	leal	8(%rbx), %eax
	movl	%eax, 32(%r13,%rbx,4)
	leal	9(%rbx), %eax
	movl	%eax, 36(%r13,%rbx,4)
	addq	$10, %rbx
	cmpq	%r12, %rbx
	jl	.LBB0_5
# BB#6:                                 # %.lr.ph.preheader
	leaq	10(%r12), %rax
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-44(%r13,%rax,4), %ecx
	movl	%ecx, -44(%r14,%rax,4)
	movl	-48(%r13,%rax,4), %ecx
	movl	%ecx, -48(%r14,%rax,4)
	movups	-64(%r13,%rax,4), %xmm0
	movups	%xmm0, -64(%r14,%rax,4)
	movups	-80(%r13,%rax,4), %xmm0
	movups	%xmm0, -80(%r14,%rax,4)
	addq	$-10, %rax
	cmpq	$10, %rax
	jg	.LBB0_7
# BB#8:                                 # %._crit_edge
	movl	-4(%r14,%r12,4), %esi
.Ltmp5:
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp6:
# BB#9:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.LBB0_10
# BB#18:                                # %.noexc100
	cmpb	$0, 56(%r12)
	je	.LBB0_20
# BB#19:
	movb	67(%r12), %al
	jmp	.LBB0_22
.LBB0_20:
.Ltmp7:
	movq	%r12, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp8:
# BB#21:                                # %.noexc102
	movq	(%r12), %rax
.Ltmp9:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp10:
.LBB0_22:                               # %.noexc96
.Ltmp11:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp12:
# BB#23:                                # %.noexc97
.Ltmp13:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp14:
# BB#24:                                # %_ZNSolsEPFRSoS_E.exit
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%r13, %rdi
	callq	_ZdlPv
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_10:
.Ltmp15:
	callq	_ZSt16__throw_bad_castv
.Ltmp16:
# BB#17:                                # %.noexc104
.LBB0_13:                               # %.noexc.i.i
.Ltmp18:
	callq	_ZSt17__throw_bad_allocv
.Ltmp19:
# BB#14:                                # %.noexc
.LBB0_25:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit105.thread
.Ltmp4:
	movq	%rax, %r15
	jmp	.LBB0_26
.LBB0_15:
.Ltmp20:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_16:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit105
.Ltmp17:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB0_26:
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp5          #   Call between .Ltmp5 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ary2.ii,@function
_GLOBAL__sub_I_ary2.ii:                 # @_GLOBAL__sub_I_ary2.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_ary2.ii, .Lfunc_end1-_GLOBAL__sub_I_ary2.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ary2.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
