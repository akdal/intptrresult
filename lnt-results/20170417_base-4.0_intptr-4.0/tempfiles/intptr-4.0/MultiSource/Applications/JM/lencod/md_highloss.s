	.text
	.file	"md_highloss.bc"
	.globl	encode_one_macroblock_highloss
	.p2align	4, 0x90
	.type	encode_one_macroblock_highloss,@function
encode_one_macroblock_highloss:         # @encode_one_macroblock_highloss
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi6:
	.cfi_def_cfa_offset 496
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	$0, 320(%rsp)
	movw	$-256, 140(%rsp)
	movl	.Lencode_one_macroblock_highloss.bmcost+16(%rip), %eax
	movl	%eax, 384(%rsp)
	movapd	.Lencode_one_macroblock_highloss.bmcost(%rip), %xmm0
	movapd	%xmm0, 368(%rsp)
	movl	$0, 148(%rsp)
	movl	$0, 156(%rsp)
	movl	$0, 308(%rsp)
	movl	$0, 152(%rsp)
	movq	img(%rip), %rsi
	movl	20(%rsi), %ecx
	movl	$0, 172(%rsp)           # 4-byte Folded Spill
	cmpl	$1, %ecx
	sete	%r14b
	testl	%ecx, %ecx
	sete	%dl
	cmpl	$3, %ecx
	sete	%bl
	movl	%edx, %eax
	orb	%bl, %al
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	cmpl	$2, %ecx
	sete	%r12b
	je	.LBB0_5
# BB#1:
	xorb	%bl, %dl
	xorb	$1, %dl
	jne	.LBB0_5
# BB#2:
	movl	164(%rsi), %edx
	cmpl	112(%rsi), %edx
	jne	.LBB0_3
# BB#4:
	cmpl	116(%rsi), %edx
	setne	%r12b
	jmp	.LBB0_5
.LBB0_3:
	xorl	%r12d, %r12d
.LBB0_5:
	movq	input(%rip), %rdx
	cmpl	$1, 4732(%rdx)
	setne	%dl
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	setne	%bl
	movl	%eax, %ecx
	orb	%dl, %cl
	xorb	%dl, %cl
	movb	%cl, 143(%rsp)          # 1-byte Spill
	orb	%dl, %al
	movb	%bl, 138(%rsp)          # 1-byte Spill
	jne	.LBB0_8
# BB#6:
	testb	%bl, %bl
	jne	.LBB0_8
# BB#7:
	cmpl	$0, 15360(%rsi)
	setg	143(%rsp)               # 1-byte Folded Spill
.LBB0_8:
	movq	14224(%rsi), %r15
	movl	12(%rsi), %eax
	movslq	%eax, %rdi
	imulq	$536, %rdi, %rbx        # imm = 0x218
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	FmoGetPreviousMBNr
	testl	%eax, %eax
	js	.LBB0_9
# BB#10:
	movq	img(%rip), %rcx
	cltq
	imulq	$536, %rax, %rax        # imm = 0x218
	addq	14224(%rcx), %rax
	jmp	.LBB0_11
.LBB0_9:
	xorl	%eax, %eax
.LBB0_11:
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movl	172(%rsp), %eax         # 4-byte Reload
	movb	%r14b, %al
	movl	%eax, 172(%rsp)         # 4-byte Spill
	leaq	(%r15,%rbx), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movw	$0, 294(%rsp)
	movq	$0, 432(%rsp)
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_14
# BB#12:
	cmpl	$1, %eax
	jne	.LBB0_15
# BB#13:
	callq	UMHEX_decide_intrabk_SAD
	jmp	.LBB0_15
.LBB0_14:
	callq	smpUMHEX_decide_intrabk_SAD
.LBB0_15:
	xorl	%r13d, %r13d
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	sete	%r13b
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	RandomIntra
	movzbl	%r12b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 288(%rsp)         # 4-byte Spill
	movswl	%cx, %edx
	leaq	176(%rsp), %rsi
	movq	272(%rsp), %rdi         # 8-byte Reload
	movl	%edx, 332(%rsp)         # 4-byte Spill
	movl	%r13d, %ecx
	callq	init_enc_mb_params
	movb	143(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	incl	%eax
	movl	%eax, 336(%rsp)         # 4-byte Spill
	movq	%r15, 408(%rsp)         # 8-byte Spill
	movq	%rbx, 400(%rsp)         # 8-byte Spill
	leaq	416(%r15,%rbx), %r11
	xorl	%esi, %esi
	leaq	368(%rsp), %r12
	movl	$0, 304(%rsp)           # 4-byte Folded Spill
	movl	%r13d, 340(%rsp)        # 4-byte Spill
	movq	%r11, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
                                        #       Child Loop BB0_26 Depth 3
                                        #     Child Loop BB0_79 Depth 2
                                        #       Child Loop BB0_93 Depth 3
                                        #         Child Loop BB0_109 Depth 4
                                        #         Child Loop BB0_161 Depth 4
                                        #         Child Loop BB0_165 Depth 4
                                        #         Child Loop BB0_169 Depth 4
	testb	$1, 143(%rsp)           # 1-byte Folded Reload
	je	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_16 Depth=1
	xorl	%eax, %eax
	testl	%esi, %esi
	setne	%al
	leal	1(%rax,%rax), %eax
	movq	input(%rip), %rcx
	movl	%eax, 4168(%rcx)
.LBB0_18:                               #   in Loop: Header=BB0_16 Depth=1
	movq	%rsi, 416(%rsp)         # 8-byte Spill
	movl	$0, (%r11)
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	cmpw	$0, 288(%rsp)           # 2-byte Folded Reload
	jne	.LBB0_70
# BB#19:                                #   in Loop: Header=BB0_16 Depth=1
	movw	$1, best_mode(%rip)
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_16 Depth=1
	callq	Get_Direct_Motion_Vectors
.LBB0_21:                               #   in Loop: Header=BB0_16 Depth=1
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_16 Depth=1
	callq	get_initial_mb16x16_cost
.LBB0_23:                               # %.preheader349.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	$2147483647, 284(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader349
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_26 Depth 3
	movw	$0, bi_pred_me(%rip)
	movq	img(%rip), %rax
	movw	$0, 14408(%rax,%r14,2)
	cmpw	$0, 220(%rsp,%r14,2)
	je	.LBB0_58
# BB#25:                                #   in Loop: Header=BB0_24 Depth=2
	movl	$0, 148(%rsp)
	xorl	%ebp, %ebp
	cmpq	$1, %r14
	setne	%bpl
	incq	%rbp
	xorl	%ebx, %ebx
	movq	%rbp, 424(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_26:                               # %.preheader346
                                        #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	input(%rip), %rcx
	movl	208(%rsp), %eax
	cmpl	$0, 4172(%rcx)
	je	.LBB0_27
# BB#29:                                #   in Loop: Header=BB0_26 Depth=3
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_31
# BB#30:                                # %call.sqrt
                                        #   in Loop: Header=BB0_26 Depth=3
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 296(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	296(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_31:                               # %.split
                                        #   in Loop: Header=BB0_26 Depth=3
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 356(%rsp)
	movl	212(%rsp), %eax
	je	.LBB0_28
# BB#32:                                #   in Loop: Header=BB0_26 Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_34
# BB#33:                                # %call.sqrt446
                                        #   in Loop: Header=BB0_26 Depth=3
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 296(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	296(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_34:                               # %.split445
                                        #   in Loop: Header=BB0_26 Depth=3
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 360(%rsp)
	movl	216(%rsp), %eax
	je	.LBB0_38
# BB#35:                                #   in Loop: Header=BB0_26 Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_37
# BB#36:                                # %call.sqrt448
                                        #   in Loop: Header=BB0_26 Depth=3
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 296(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	296(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_37:                               # %.split447
                                        #   in Loop: Header=BB0_26 Depth=3
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_27:                               # %.thread
                                        #   in Loop: Header=BB0_26 Depth=3
	movl	%eax, 356(%rsp)
	movl	212(%rsp), %eax
.LBB0_28:                               # %.thread396
                                        #   in Loop: Header=BB0_26 Depth=3
	movl	%eax, 360(%rsp)
	movl	216(%rsp), %eax
.LBB0_38:                               #   in Loop: Header=BB0_26 Depth=3
	movl	%eax, 364(%rsp)
	movl	%r14d, %edi
	movl	%ebx, %esi
	leaq	356(%rsp), %rdx
	callq	PartitionMotionSearch
	movl	$2147483647, 368(%rsp)  # imm = 0x7FFFFFFF
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$0, %edi
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	%r12, %rcx
	movq	%r12, %r15
	leaq	140(%rsp), %r12
	movq	%r12, %r8
	callq	list_prediction_cost
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_44
# BB#39:                                #   in Loop: Header=BB0_26 Depth=3
	movl	%r13d, %ebp
	movl	$2147483647, 372(%rsp)  # imm = 0x7FFFFFFF
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edi
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	list_prediction_cost
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$2, %edi
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	%r15, %r13
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	list_prediction_cost
	cmpq	$1, %r14
	jne	.LBB0_42
# BB#40:                                #   in Loop: Header=BB0_26 Depth=3
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_26 Depth=3
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$3, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	movq	%r13, %r12
	movq	%r12, %rcx
	callq	list_prediction_cost
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$4, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	movq	%r12, %rcx
	callq	list_prediction_cost
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_26 Depth=3
	movb	$0, 139(%rsp)
	movl	368(%rsp), %eax
	addl	%eax, 148(%rsp)
	xorl	%eax, %eax
	movq	%r15, %r12
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_26 Depth=3
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, 380(%rsp)
	movq	%r13, %r12
.LBB0_43:                               #   in Loop: Header=BB0_26 Depth=3
	movl	$bi_pred_me, %r9d
	movl	%r14d, %edi
	movq	%r12, %rsi
	leaq	140(%rsp), %rdx
	leaq	139(%rsp), %rcx
	leaq	148(%rsp), %r8
	callq	determine_prediction_list
	movb	139(%rsp), %al
	movl	%ebp, %r13d
	movq	424(%rsp), %rbp         # 8-byte Reload
.LBB0_45:                               #   in Loop: Header=BB0_26 Depth=3
	movswl	250(%rsp), %ecx
	movsbl	140(%rsp), %r8d
	movsbl	141(%rsp), %r9d
	movl	%r13d, (%rsp)
	movsbl	%al, %esi
	movl	%r14d, %edi
	movl	%ebx, %edx
	callq	assign_enc_picture_params
	movzbl	140(%rsp), %eax
	cmpl	$2, %r14d
	je	.LBB0_48
# BB#46:                                #   in Loop: Header=BB0_26 Depth=3
	cmpl	$3, %r14d
	jne	.LBB0_49
# BB#47:                                #   in Loop: Header=BB0_26 Depth=3
	movb	%al, best8x8fwref+14(%rbx)
	movb	%al, best8x8fwref+12(%rbx)
	movb	139(%rsp), %dl
	movb	%dl, best8x8pdir+14(%rbx)
	movb	%dl, best8x8pdir+12(%rbx)
	movb	141(%rsp), %sil
	movb	%sil, best8x8bwref+14(%rbx)
	movb	%sil, best8x8bwref+12(%rbx)
	cmpq	$2, %r14
	jge	.LBB0_51
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_26 Depth=3
	movb	%al, best8x8fwref+9(%rbx,%rbx)
	movb	%al, best8x8fwref+8(%rbx,%rbx)
	movb	139(%rsp), %dl
	movb	%dl, best8x8pdir+9(%rbx,%rbx)
	movb	%dl, best8x8pdir+8(%rbx,%rbx)
	movb	141(%rsp), %sil
	movb	%sil, best8x8bwref+9(%rbx,%rbx)
	movb	%sil, best8x8bwref+8(%rbx,%rbx)
	cmpq	$2, %r14
	jge	.LBB0_51
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_26 Depth=3
	movzbl	%al, %ecx
	imull	$16843009, %ecx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8fwref+4(%rip)
	movzbl	141(%rsp), %esi
	imull	$16843009, %esi, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8bwref+4(%rip)
	movzbl	139(%rsp), %edx
	imull	$16843009, %edx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8pdir+4(%rip)
	cmpq	$2, %r14
	jl	.LBB0_53
.LBB0_51:                               #   in Loop: Header=BB0_26 Depth=3
	testq	%rbx, %rbx
	jne	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_26 Depth=3
	movsbl	%al, %ecx
	movsbl	%dl, %edx
	movsbl	%sil, %r8d
	xorl	%edi, %edi
	movl	%r14d, %esi
	callq	SetRefAndMotionVectors
.LBB0_53:                               #   in Loop: Header=BB0_26 Depth=3
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_26
# BB#54:                                #   in Loop: Header=BB0_24 Depth=2
	movl	148(%rsp), %ebx
	cmpl	284(%rsp), %ebx         # 4-byte Folded Reload
	jge	.LBB0_58
# BB#55:                                #   in Loop: Header=BB0_24 Depth=2
	movw	%r14w, best_mode(%rip)
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_24 Depth=2
	movl	%ebx, %edi
	callq	adjust_mb16x16_cost
.LBB0_57:                               #   in Loop: Header=BB0_24 Depth=2
	movl	%ebx, 284(%rsp)         # 4-byte Spill
.LBB0_58:                               #   in Loop: Header=BB0_24 Depth=2
	incq	%r14
	cmpq	$4, %r14
	jne	.LBB0_24
# BB#59:                                #   in Loop: Header=BB0_16 Depth=1
	cmpw	$0, 236(%rsp)
	je	.LBB0_66
# BB#60:                                #   in Loop: Header=BB0_16 Depth=1
	movl	$1, giRDOpt_B8OnlyFlag(%rip)
	movl	$2147483647, tr8x8(%rip) # imm = 0x7FFFFFFF
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movq	cs_mb(%rip), %rdi
	callq	store_coding_state
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	$-1, 52(%rax)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_16 Depth=1
	movl	$0, tr8x8(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 156(%rsp)
	movq	cofAC_8x8ts(%rip), %rax
	movq	(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	152(%rsp), %r14
	movq	%r14, 112(%rsp)
	movq	%r14, %r15
	leaq	148(%rsp), %r13
	movq	%r13, 104(%rsp)
	leaq	156(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movzwl	172(%rsp), %ebx         # 2-byte Folded Reload
	movl	$tr8x8, %edi
	xorl	%r9d, %r9d
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	leaq	308(%rsp), %r13
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr8x8+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr8x8+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr8x8+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	8(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	leaq	148(%rsp), %r14
	movq	%r14, 104(%rsp)
	leaq	156(%rsp), %r15
	movq	%r15, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr8x8+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr8x8+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr8x8+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	16(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	%r14, 104(%rsp)
	movq	%r15, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr8x8+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr8x8+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr8x8+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	24(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	%r14, 104(%rsp)
	movq	%r15, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movl	%ebx, %r8d
	movq	%r13, %rcx
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr8x8+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr8x8+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr8x8+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
	movl	cbp8x8(%rip), %eax
	movl	%eax, cbp8_8x8ts(%rip)
	movslq	cbp_blk8x8(%rip), %rax
	movq	%rax, cbp_blk8_8x8ts(%rip)
	movl	cnt_nonz_8x8(%rip), %eax
	movl	%eax, cnt_nonz8_8x8ts(%rip)
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	$0, 56(%rax)
	movq	input(%rip), %rax
	cmpl	$2, 5100(%rax)
	je	.LBB0_63
.LBB0_62:                               # %.loopexit348.loopexit371
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	$0, tr4x4(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 156(%rsp)
	movq	cofAC8x8(%rip), %rax
	movq	(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	152(%rsp), %r14
	movq	%r14, 112(%rsp)
	movq	%r14, %r15
	leaq	148(%rsp), %r13
	movq	%r13, 104(%rsp)
	leaq	156(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movzwl	172(%rsp), %ebx         # 2-byte Folded Reload
	movl	$tr4x4, %edi
	xorl	%r9d, %r9d
	movq	272(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	leaq	308(%rsp), %r13
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr4x4+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr4x4+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr4x4+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	8(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	leaq	148(%rsp), %r14
	movq	%r14, 104(%rsp)
	leaq	156(%rsp), %r15
	movq	%r15, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr4x4+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr4x4+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr4x4+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	16(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	%r14, 104(%rsp)
	movq	%r15, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r13, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr4x4+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr4x4+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr4x4+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	24(%rax), %rdx
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	%r14, 104(%rsp)
	movq	%r15, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movl	%ebx, %r8d
	movq	%r13, %rcx
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr4x4+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr4x4+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr4x4+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
.LBB0_63:                               # %.loopexit348
                                        #   in Loop: Header=BB0_16 Depth=1
	movq	cs_mb(%rip), %rdi
	callq	reset_coding_state
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_16 Depth=1
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
.LBB0_65:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$0, giRDOpt_B8OnlyFlag(%rip)
	cmpl	$3, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_68
	jmp	.LBB0_69
.LBB0_66:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	cmpl	$3, 144(%rsp)           # 4-byte Folded Reload
	je	.LBB0_69
.LBB0_68:                               #   in Loop: Header=BB0_16 Depth=1
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_70
.LBB0_69:                               #   in Loop: Header=BB0_16 Depth=1
	callq	FindSkipModeMotionVector
.LBB0_70:                               #   in Loop: Header=BB0_16 Depth=1
	movabsq	$5055640609639927018, %rax # imm = 0x46293E5939A08CEA
	movq	%rax, 320(%rsp)
	movq	input(%rip), %rax
	cmpl	$0, 2120(%rax)
	movq	img(%rip), %rax
	movb	138(%rsp), %r9b         # 1-byte Reload
	je	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_16 Depth=1
	movw	$0, 14410(%rax)
.LBB0_72:                               # %._crit_edge387
                                        #   in Loop: Header=BB0_16 Depth=1
	xorl	%ebx, %ebx
	cmpl	$0, 15536(%rax)
	je	.LBB0_73
# BB#74:                                #   in Loop: Header=BB0_16 Depth=1
	leaq	352(%rsp), %rdi
	leaq	348(%rsp), %rsi
	leaq	344(%rsp), %rdx
	callq	IntraChromaPrediction
	movq	input(%rip), %rax
	cmpl	$0, 4176(%rax)
	je	.LBB0_75
# BB#76:                                #   in Loop: Header=BB0_16 Depth=1
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	callq	IntraChromaRDDecision
	movq	160(%rsp), %r11         # 8-byte Reload
	movl	(%r11), %ebx
	movl	%ebx, %ecx
	jmp	.LBB0_77
	.p2align	4, 0x90
.LBB0_73:                               #   in Loop: Header=BB0_16 Depth=1
	xorl	%ecx, %ecx
	movq	160(%rsp), %r11         # 8-byte Reload
	jmp	.LBB0_78
	.p2align	4, 0x90
.LBB0_75:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$3, %ecx
	movq	160(%rsp), %r11         # 8-byte Reload
.LBB0_77:                               #   in Loop: Header=BB0_16 Depth=1
	movb	138(%rsp), %r9b         # 1-byte Reload
.LBB0_78:                               #   in Loop: Header=BB0_16 Depth=1
	movswl	%bx, %eax
	movl	%eax, (%r11)
	movswl	%cx, %r15d
	cmpl	%r15d, %eax
	jg	.LBB0_125
	.p2align	4, 0x90
.LBB0_79:                               # %.lr.ph
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_93 Depth 3
                                        #         Child Loop BB0_109 Depth 4
                                        #         Child Loop BB0_161 Depth 4
                                        #         Child Loop BB0_165 Depth 4
                                        #         Child Loop BB0_169 Depth 4
	movq	img(%rip), %rcx
	cmpl	$0, 15536(%rcx)
	je	.LBB0_92
# BB#80:                                #   in Loop: Header=BB0_79 Depth=2
	movq	input(%rip), %rcx
	cmpw	$0, 288(%rsp)           # 2-byte Folded Reload
	je	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_79 Depth=2
	cmpl	$0, 4048(%rcx)
	jne	.LBB0_84
.LBB0_82:                               # %._crit_edge389
                                        #   in Loop: Header=BB0_79 Depth=2
	cmpl	$1, 4072(%rcx)
	jne	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_79 Depth=2
	testl	%eax, %eax
	jne	.LBB0_124
	jmp	.LBB0_92
	.p2align	4, 0x90
.LBB0_84:                               #   in Loop: Header=BB0_79 Depth=2
	cmpl	$2, %eax
	movl	352(%rsp), %ecx
	jne	.LBB0_86
# BB#85:                                #   in Loop: Header=BB0_79 Depth=2
	testl	%ecx, %ecx
	je	.LBB0_124
.LBB0_86:                               #   in Loop: Header=BB0_79 Depth=2
	cmpl	$1, %eax
	movl	348(%rsp), %edx
	jne	.LBB0_88
# BB#87:                                #   in Loop: Header=BB0_79 Depth=2
	testl	%edx, %edx
	je	.LBB0_124
.LBB0_88:                               #   in Loop: Header=BB0_79 Depth=2
	cmpl	$3, %eax
	jne	.LBB0_92
# BB#89:                                #   in Loop: Header=BB0_79 Depth=2
	testl	%ecx, %ecx
	je	.LBB0_124
# BB#90:                                #   in Loop: Header=BB0_79 Depth=2
	testl	%edx, %edx
	je	.LBB0_124
# BB#91:                                #   in Loop: Header=BB0_79 Depth=2
	movl	344(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_124
	.p2align	4, 0x90
.LBB0_92:                               # %.thread342.preheader.preheader
                                        #   in Loop: Header=BB0_79 Depth=2
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB0_93
.LBB0_110:                              # %.thread337
                                        #   in Loop: Header=BB0_93 Depth=3
	cmpl	$2, %r13d
	jne	.LBB0_123
# BB#111:                               # %.thread337
                                        #   in Loop: Header=BB0_93 Depth=3
	movl	2120(%r10), %eax
	testl	%eax, %eax
	je	.LBB0_123
# BB#112:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$1, %ebx
	jne	.LBB0_123
# BB#113:                               #   in Loop: Header=BB0_93 Depth=3
	movq	img(%rip), %rax
	movw	14408(%rax,%rbx,2), %cx
	movswl	%cx, %edx
	cmpl	$1, %edx
	jle	.LBB0_122
	jmp	.LBB0_123
	.p2align	4, 0x90
.LBB0_93:                               # %.thread342.preheader
                                        #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_79 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_109 Depth 4
                                        #         Child Loop BB0_161 Depth 4
                                        #         Child Loop BB0_165 Depth 4
                                        #         Child Loop BB0_169 Depth 4
	movslq	%r14d, %rax
	movslq	mb_mode_table(,%rax,4), %rbx
	cmpq	$1, %rbx
	setne	%al
	orb	%r9b, %al
	cmpb	$1, %al
	jne	.LBB0_95
# BB#94:                                # %.thread342.preheader._crit_edge
                                        #   in Loop: Header=BB0_93 Depth=3
	movq	input(%rip), %r10
	jmp	.LBB0_99
	.p2align	4, 0x90
.LBB0_95:                               #   in Loop: Header=BB0_93 Depth=3
	movzbl	%r13b, %eax
	imull	$16843009, %eax, %eax   # imm = 0x1010101
	movl	%eax, best8x8pdir+4(%rip)
	movq	input(%rip), %r10
	cmpl	$2, %r13d
	jne	.LBB0_98
# BB#96:                                #   in Loop: Header=BB0_93 Depth=3
	movl	2120(%r10), %ecx
	testl	%ecx, %ecx
	je	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_93 Depth=3
	movq	img(%rip), %rcx
	movswl	14408(%rcx,%rbx,2), %ecx
	xorl	%edx, %edx
	cmpl	$2, %ecx
	setl	%dl
	movl	$2, %r13d
	subl	%edx, %r13d
.LBB0_98:                               #   in Loop: Header=BB0_93 Depth=3
	xorl	%ecx, %ecx
	cmpl	$2, %r13d
	setl	%cl
	subl	%ecx, %r14d
	incl	%r13d
.LBB0_99:                               #   in Loop: Header=BB0_93 Depth=3
	cmpw	$0, 288(%rsp)           # 2-byte Folded Reload
	sete	%cl
	cmpl	$0, 2960(%r10)
	setne	%dl
	movswl	best_mode(%rip), %esi
	cmpl	$3, %esi
	jg	.LBB0_103
# BB#100:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$10, %ebx
	jl	.LBB0_103
# BB#101:                               #   in Loop: Header=BB0_93 Depth=3
	andb	%dl, %cl
	je	.LBB0_103
# BB#102:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$0, -52(%r11)
	je	.LBB0_123
.LBB0_103:                              #   in Loop: Header=BB0_93 Depth=3
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_114
# BB#104:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$7, %ebx
	jg	.LBB0_114
# BB#105:                               #   in Loop: Header=BB0_93 Depth=3
	movq	active_pps(%rip), %rcx
	cmpl	$1, 196(%rcx)
	jne	.LBB0_114
# BB#106:                               # %.preheader344
                                        #   in Loop: Header=BB0_93 Depth=3
	movq	wbp_weight(%rip), %r8
	movq	active_sps(%rip), %rcx
	cmpb	$2, best8x8pdir(,%rbx,4)
	jne	.LBB0_159
# BB#107:                               # %.preheader
                                        #   in Loop: Header=BB0_93 Depth=3
	movsbq	best8x8fwref(,%rbx,4), %rdx
	movq	(%r8), %rsi
	movq	8(%r8), %r9
	movq	(%rsi,%rdx,8), %rsi
	movsbq	best8x8bwref(,%rbx,4), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%r9,%rdx,8), %rdx
	movb	138(%rsp), %r9b         # 1-byte Reload
	movq	(%rdx,%rdi,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_109:                              #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_79 Depth=2
                                        #       Parent Loop BB0_93 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rsi,%rdx,4), %ebp
	movl	(%rdi,%rdx,4), %eax
	leal	128(%rbp,%rax), %eax
	cmpl	$255, %eax
	ja	.LBB0_110
# BB#108:                               #   in Loop: Header=BB0_109 Depth=4
	incq	%rdx
	xorl	%ebp, %ebp
	cmpl	$0, 32(%rcx)
	setne	%bpl
	leaq	1(%rbp,%rbp), %rbp
	cmpq	%rbp, %rdx
	jl	.LBB0_109
.LBB0_159:                              # %.loopexit
                                        #   in Loop: Header=BB0_93 Depth=3
	cmpb	$2, best8x8pdir+1(,%rbx,4)
	jne	.LBB0_163
# BB#160:                               # %.preheader.1
                                        #   in Loop: Header=BB0_93 Depth=3
	movsbq	best8x8fwref+1(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rdi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+1(,%rbx,4), %rbp
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_161:                              #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_79 Depth=2
                                        #       Parent Loop BB0_93 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rsi,%rdx,4), %eax
	movl	(%rdi,%rdx,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_110
# BB#162:                               #   in Loop: Header=BB0_161 Depth=4
	incq	%rdx
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_161
.LBB0_163:                              # %.loopexit.1
                                        #   in Loop: Header=BB0_93 Depth=3
	cmpb	$2, best8x8pdir+2(,%rbx,4)
	jne	.LBB0_167
# BB#164:                               # %.preheader.2
                                        #   in Loop: Header=BB0_93 Depth=3
	movsbq	best8x8fwref+2(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rdi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+2(,%rbx,4), %rbp
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_165:                              #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_79 Depth=2
                                        #       Parent Loop BB0_93 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rsi,%rdx,4), %eax
	movl	(%rdi,%rdx,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_110
# BB#166:                               #   in Loop: Header=BB0_165 Depth=4
	incq	%rdx
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_165
.LBB0_167:                              # %.loopexit.2
                                        #   in Loop: Header=BB0_93 Depth=3
	cmpb	$2, best8x8pdir+3(,%rbx,4)
	jne	.LBB0_114
# BB#168:                               # %.preheader.3
                                        #   in Loop: Header=BB0_93 Depth=3
	movsbq	best8x8fwref+3(,%rbx,4), %rax
	movq	(%r8), %rdx
	movq	8(%r8), %rsi
	movq	(%rdx,%rax,8), %rdx
	movsbq	best8x8bwref+3(,%rbx,4), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	(%rax,%rdi,8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_169:                              #   Parent Loop BB0_16 Depth=1
                                        #     Parent Loop BB0_79 Depth=2
                                        #       Parent Loop BB0_93 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rdx,%rdi,4), %eax
	movl	(%rsi,%rdi,4), %ebp
	leal	128(%rax,%rbp), %eax
	cmpl	$255, %eax
	ja	.LBB0_110
# BB#170:                               #   in Loop: Header=BB0_169 Depth=4
	incq	%rdi
	xorl	%eax, %eax
	cmpl	$0, 32(%rcx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rdi
	jl	.LBB0_169
	.p2align	4, 0x90
.LBB0_114:                              # %.loopexit345
                                        #   in Loop: Header=BB0_93 Depth=3
	cmpw	$0, 220(%rsp,%rbx,2)
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_93 Depth=3
	movups	256(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	240(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	176(%rsp), %xmm0
	movups	192(%rsp), %xmm1
	movups	208(%rsp), %xmm2
	movups	224(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	leaq	294(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	172(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movl	%ebx, %edi
	movq	272(%rsp), %rsi         # 8-byte Reload
	leaq	320(%rsp), %rdx
	leaq	432(%rsp), %rcx
	callq	compute_mode_RD_cost
	movb	138(%rsp), %r9b         # 1-byte Reload
	movq	160(%rsp), %r11         # 8-byte Reload
	movq	input(%rip), %r10
.LBB0_116:                              #   in Loop: Header=BB0_93 Depth=3
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_123
# BB#117:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$2, %r13d
	jne	.LBB0_123
# BB#118:                               #   in Loop: Header=BB0_93 Depth=3
	movl	2120(%r10), %eax
	testl	%eax, %eax
	je	.LBB0_123
# BB#119:                               #   in Loop: Header=BB0_93 Depth=3
	cmpl	$1, %ebx
	jne	.LBB0_123
# BB#120:                               #   in Loop: Header=BB0_93 Depth=3
	movq	img(%rip), %rax
	movw	14408(%rax,%rbx,2), %cx
	movswl	%cx, %edx
	cmpl	$1, %edx
	jg	.LBB0_123
# BB#121:                               #   in Loop: Header=BB0_93 Depth=3
	cmpb	$2, best8x8pdir+4(%rip)
	jne	.LBB0_123
.LBB0_122:                              #   in Loop: Header=BB0_93 Depth=3
	incl	%ecx
	movw	%cx, 14408(%rax,%rbx,2)
.LBB0_123:                              # %.critedge319
                                        #   in Loop: Header=BB0_93 Depth=3
	incl	%r14d
	cmpl	$9, %r14d
	jl	.LBB0_93
.LBB0_124:                              # %.loopexit347
                                        #   in Loop: Header=BB0_79 Depth=2
	movl	(%r11), %ecx
	leal	1(%rcx), %eax
	movl	%eax, (%r11)
	cmpl	%r15d, %ecx
	jl	.LBB0_79
.LBB0_125:                              # %._crit_edge
                                        #   in Loop: Header=BB0_16 Depth=1
	movq	416(%rsp), %rsi         # 8-byte Reload
	testl	%esi, %esi
	jne	.LBB0_127
# BB#126:                               #   in Loop: Header=BB0_16 Depth=1
	movl	-344(%r11), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%al, %al
	movb	$51, %dl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %dl
	andb	%al, %dl
	andb	$1, %dl
	movzbl	%dl, %eax
	movl	%eax, 304(%rsp)         # 4-byte Spill
.LBB0_127:                              #   in Loop: Header=BB0_16 Depth=1
	incl	%esi
	cmpl	336(%rsp), %esi         # 4-byte Folded Reload
	movl	340(%rsp), %r13d        # 4-byte Reload
	jl	.LBB0_16
# BB#128:
	movl	cbp(%rip), %ecx
	movzwl	best_mode(%rip), %eax
	testl	%ecx, %ecx
	sete	%dl
	cmpl	$10, %eax
	setne	%bl
	cmpl	$14, %eax
	movq	408(%rsp), %r14         # 8-byte Reload
	movq	400(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_130
# BB#129:
	andb	%bl, %dl
	movl	$1, %edx
	je	.LBB0_135
.LBB0_130:
	movzwl	%ax, %eax
	testl	%ecx, %ecx
	je	.LBB0_131
# BB#133:
	cmpl	$14, %eax
	je	.LBB0_134
	jmp	.LBB0_136
.LBB0_131:
	cmpl	$14, %eax
	je	.LBB0_134
# BB#132:
	movq	input(%rip), %rax
	movl	5116(%rax), %eax
	testl	%eax, %eax
	jne	.LBB0_136
.LBB0_134:
	movl	$0, 4(%r14,%rbp)
	movl	496(%r14,%rbp), %eax
	movl	%eax, 8(%r14,%rbp)
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	set_chroma_qp
	movl	8(%r14,%rbp), %eax
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	xorl	%edx, %edx
.LBB0_135:                              # %.sink.split
	movl	%edx, 504(%r14,%rbp)
.LBB0_136:
	callq	set_stored_macroblock_parameters
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_138
# BB#137:
	movswl	best_mode(%rip), %esi
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	update_rc
.LBB0_138:
	movq	320(%rsp), %rax
	movq	rdopt(%rip), %rcx
	movq	%rax, (%rcx)
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB0_148
# BB#139:
	testb	$1, 12(%rax)
	je	.LBB0_148
# BB#140:
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -344(%rax)
	jne	.LBB0_148
# BB#141:
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_143
# BB#142:
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	jne	.LBB0_148
.LBB0_143:
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 72(%rax)
	je	.LBB0_144
.LBB0_148:
	movq	input(%rip), %rax
	cmpl	$0, 4732(%rax)
	je	.LBB0_150
# BB#149:
	movl	332(%rsp), %edi         # 4-byte Reload
	movl	304(%rsp), %esi         # 4-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	callq	update_refresh_map
	movq	input(%rip), %rax
.LBB0_150:
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_153
# BB#151:
	cmpl	$1, %eax
	movq	160(%rsp), %rcx         # 8-byte Reload
	jne	.LBB0_155
# BB#152:
	movswl	best_mode(%rip), %edi
	movswq	250(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	UMHEX_skip_intrabk_SAD
	jmp	.LBB0_154
.LBB0_153:
	movswl	best_mode(%rip), %edi
	movswq	250(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	smpUMHEX_skip_intrabk_SAD
.LBB0_154:
	movq	160(%rsp), %rcx         # 8-byte Reload
.LBB0_155:
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB0_158
# BB#156:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	ja	.LBB0_158
# BB#157:
	movl	-344(%rcx), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%dl, %dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	andb	%dl, %bl
	andb	$1, %bl
	movzbl	%bl, %ecx
	movq	14240(%rax), %rdx
	movslq	12(%rax), %rax
	movl	%ecx, (%rdx,%rax,4)
.LBB0_158:
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_144:
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_146
# BB#145:
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rax)
	jne	.LBB0_148
.LBB0_146:
	callq	field_flag_inference
	movswl	254(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_148
# BB#147:
	movq	rdopt(%rip), %rax
	movabsq	$5055640609639927018, %rcx # imm = 0x46293E5939A08CEA
	movq	%rcx, (%rax)
	jmp	.LBB0_148
.Lfunc_end0:
	.size	encode_one_macroblock_highloss, .Lfunc_end0-encode_one_macroblock_highloss
	.cfi_endproc

	.type	.Lencode_one_macroblock_highloss.bmcost,@object # @encode_one_macroblock_highloss.bmcost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lencode_one_macroblock_highloss.bmcost:
	.long	2147483647              # 0x7fffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lencode_one_macroblock_highloss.bmcost, 20

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
