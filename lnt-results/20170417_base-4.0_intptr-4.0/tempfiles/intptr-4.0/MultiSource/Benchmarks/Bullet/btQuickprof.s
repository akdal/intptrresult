	.text
	.file	"btQuickprof.bc"
	.globl	_ZN12CProfileNodeC2EPKcPS_
	.p2align	4, 0x90
	.type	_ZN12CProfileNodeC2EPKcPS_,@function
_ZN12CProfileNodeC2EPKcPS_:             # @_ZN12CProfileNodeC2EPKcPS_
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	movl	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movups	%xmm0, 40(%rdi)
	jmp	_ZN12CProfileNode5ResetEv # TAILCALL
.Lfunc_end0:
	.size	_ZN12CProfileNodeC2EPKcPS_, .Lfunc_end0-_ZN12CProfileNodeC2EPKcPS_
	.cfi_endproc

	.globl	_ZN12CProfileNode5ResetEv
	.p2align	4, 0x90
	.type	_ZN12CProfileNode5ResetEv,@function
_ZN12CProfileNode5ResetEv:              # @_ZN12CProfileNode5ResetEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB1_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 8(%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	_ZN12CProfileNode5ResetEv
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_1
# BB#4:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN12CProfileNode5ResetEv, .Lfunc_end1-_ZN12CProfileNode5ResetEv
	.cfi_endproc

	.globl	_ZN12CProfileNode13CleanupMemoryEv
	.p2align	4, 0x90
	.type	_ZN12CProfileNode13CleanupMemoryEv,@function
_ZN12CProfileNode13CleanupMemoryEv:     # @_ZN12CProfileNode13CleanupMemoryEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %r14
	testq	%r14, %r14
	je	.LBB2_3
# BB#1:
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN12CProfileNodeD2Ev
.Ltmp1:
# BB#2:
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB2_3:
	movq	$0, 40(%rbx)
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.LBB2_6
# BB#4:
.Ltmp3:
	movq	%r14, %rdi
	callq	_ZN12CProfileNodeD2Ev
.Ltmp4:
# BB#5:
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB2_6:
	movq	$0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_8:
.Ltmp5:
	jmp	.LBB2_9
.LBB2_7:
.Ltmp2:
.LBB2_9:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN12CProfileNode13CleanupMemoryEv, .Lfunc_end2-_ZN12CProfileNode13CleanupMemoryEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CProfileNodeD2Ev
	.p2align	4, 0x90
	.type	_ZN12CProfileNodeD2Ev,@function
_ZN12CProfileNodeD2Ev:                  # @_ZN12CProfileNodeD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB3_3
# BB#1:
.Ltmp6:
	movq	%rbx, %rdi
	callq	_ZN12CProfileNodeD2Ev
.Ltmp7:
# BB#2:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB3_3:
	movq	48(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB3_6
# BB#4:
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN12CProfileNodeD2Ev
.Ltmp10:
# BB#5:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_8:
.Ltmp11:
	jmp	.LBB3_9
.LBB3_7:
.Ltmp8:
.LBB3_9:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN12CProfileNodeD2Ev, .Lfunc_end3-_ZN12CProfileNodeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN12CProfileNode12Get_Sub_NodeEPKc
	.p2align	4, 0x90
	.type	_ZN12CProfileNode12Get_Sub_NodeEPKc,@function
_ZN12CProfileNode12Get_Sub_NodeEPKc:    # @_ZN12CProfileNode12Get_Sub_NodeEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB4_4
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, (%rbx)
	jne	.LBB4_3
	jmp	.LBB4_5
.LBB4_4:                                # %._crit_edge
	movl	$56, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	%r15, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movl	$0, 24(%rbx)
	movq	%r14, 32(%rbx)
	movups	%xmm0, 40(%rbx)
	movq	%rbx, %rdi
	callq	_ZN12CProfileNode5ResetEv
	movq	40(%r14), %rax
	movq	%rax, 48(%rbx)
	movq	%rbx, 40(%r14)
.LBB4_5:                                # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN12CProfileNode12Get_Sub_NodeEPKc, .Lfunc_end4-_ZN12CProfileNode12Get_Sub_NodeEPKc
	.cfi_endproc

	.globl	_ZN12CProfileNode4CallEv
	.p2align	4, 0x90
	.type	_ZN12CProfileNode4CallEv,@function
_ZN12CProfileNode4CallEv:               # @_ZN12CProfileNode4CallEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	incl	8(%rbx)
	movl	24(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 24(%rbx)
	testl	%eax, %eax
	jne	.LBB5_2
# BB#1:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	subq	_ZL13gProfileClock(%rip), %rax
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rcx
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
.LBB5_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN12CProfileNode4CallEv, .Lfunc_end5-_ZN12CProfileNode4CallEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1148846080              # float 1000
	.text
	.globl	_ZN12CProfileNode6ReturnEv
	.p2align	4, 0x90
	.type	_ZN12CProfileNode6ReturnEv,@function
_ZN12CProfileNode6ReturnEv:             # @_ZN12CProfileNode6ReturnEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %eax
	decl	%eax
	movl	%eax, 24(%rbx)
	jne	.LBB6_7
# BB#1:
	cmpl	$0, 8(%rbx)
	je	.LBB6_2
# BB#3:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	(%rsp), %rcx
	movq	8(%rsp), %rax
	subq	_ZL13gProfileClock(%rip), %rcx
	imulq	$1000000, %rcx, %rcx    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rax
	addq	%rcx, %rax
	movq	16(%rbx), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	subq	%rdx, %rax
	js	.LBB6_4
# BB#5:
	cvtsi2ssq	%rax, %xmm0
	jmp	.LBB6_6
.LBB6_2:
	xorl	%eax, %eax
	jmp	.LBB6_7
.LBB6_4:
	movq	%rcx, %rax
	shrq	%rax
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm0
	addss	%xmm0, %xmm0
.LBB6_6:
	divss	.LCPI6_0(%rip), %xmm0
	addss	12(%rbx), %xmm0
	movss	%xmm0, 12(%rbx)
	movl	24(%rbx), %eax
.LBB6_7:
	testl	%eax, %eax
	sete	%al
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN12CProfileNode6ReturnEv, .Lfunc_end6-_ZN12CProfileNode6ReturnEv
	.cfi_endproc

	.globl	_ZN16CProfileIteratorC2EP12CProfileNode
	.p2align	4, 0x90
	.type	_ZN16CProfileIteratorC2EP12CProfileNode,@function
_ZN16CProfileIteratorC2EP12CProfileNode: # @_ZN16CProfileIteratorC2EP12CProfileNode
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 8(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN16CProfileIteratorC2EP12CProfileNode, .Lfunc_end7-_ZN16CProfileIteratorC2EP12CProfileNode
	.cfi_endproc

	.globl	_ZN16CProfileIterator5FirstEv
	.p2align	4, 0x90
	.type	_ZN16CProfileIterator5FirstEv,@function
_ZN16CProfileIterator5FirstEv:          # @_ZN16CProfileIterator5FirstEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, 8(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN16CProfileIterator5FirstEv, .Lfunc_end8-_ZN16CProfileIterator5FirstEv
	.cfi_endproc

	.globl	_ZN16CProfileIterator4NextEv
	.p2align	4, 0x90
	.type	_ZN16CProfileIterator4NextEv,@function
_ZN16CProfileIterator4NextEv:           # @_ZN16CProfileIterator4NextEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	48(%rax), %rax
	movq	%rax, 8(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN16CProfileIterator4NextEv, .Lfunc_end9-_ZN16CProfileIterator4NextEv
	.cfi_endproc

	.globl	_ZN16CProfileIterator7Is_DoneEv
	.p2align	4, 0x90
	.type	_ZN16CProfileIterator7Is_DoneEv,@function
_ZN16CProfileIterator7Is_DoneEv:        # @_ZN16CProfileIterator7Is_DoneEv
	.cfi_startproc
# BB#0:
	cmpq	$0, 8(%rdi)
	sete	%al
	retq
.Lfunc_end10:
	.size	_ZN16CProfileIterator7Is_DoneEv, .Lfunc_end10-_ZN16CProfileIterator7Is_DoneEv
	.cfi_endproc

	.globl	_ZN16CProfileIterator11Enter_ChildEi
	.p2align	4, 0x90
	.type	_ZN16CProfileIterator11Enter_ChildEi,@function
_ZN16CProfileIterator11Enter_ChildEi:   # @_ZN16CProfileIterator11Enter_ChildEi
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, 8(%rdi)
	testq	%rax, %rax
	setne	%cl
	testl	%esi, %esi
	je	.LBB11_5
# BB#1:
	testq	%rax, %rax
	je	.LBB11_5
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %edx
	subl	%esi, %edx
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rax), %rax
	movq	%rax, 8(%rdi)
	testq	%rax, %rax
	setne	%cl
	testl	%edx, %edx
	je	.LBB11_5
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB11_3 Depth=1
	incl	%edx
	testq	%rax, %rax
	jne	.LBB11_3
.LBB11_5:                               # %._crit_edge
	testb	%cl, %cl
	je	.LBB11_7
# BB#6:
	movq	%rax, (%rdi)
	movq	40(%rax), %rax
	movq	%rax, 8(%rdi)
.LBB11_7:
	retq
.Lfunc_end11:
	.size	_ZN16CProfileIterator11Enter_ChildEi, .Lfunc_end11-_ZN16CProfileIterator11Enter_ChildEi
	.cfi_endproc

	.globl	_ZN16CProfileIterator12Enter_ParentEv
	.p2align	4, 0x90
	.type	_ZN16CProfileIterator12Enter_ParentEv,@function
_ZN16CProfileIterator12Enter_ParentEv:  # @_ZN16CProfileIterator12Enter_ParentEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB12_2
# BB#1:
	movq	%rcx, (%rdi)
	movq	%rcx, %rax
.LBB12_2:
	movq	40(%rax), %rax
	movq	%rax, 8(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN16CProfileIterator12Enter_ParentEv, .Lfunc_end12-_ZN16CProfileIterator12Enter_ParentEv
	.cfi_endproc

	.globl	_ZN15CProfileManager13Start_ProfileEPKc
	.p2align	4, 0x90
	.type	_ZN15CProfileManager13Start_ProfileEPKc,@function
_ZN15CProfileManager13Start_ProfileEPKc: # @_ZN15CProfileManager13Start_ProfileEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	_ZN15CProfileManager11CurrentNodeE(%rip), %r15
	cmpq	%r14, (%r15)
	je	.LBB13_7
# BB#1:
	movq	40(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_3
	jmp	.LBB13_5
	.p2align	4, 0x90
.LBB13_4:                               #   in Loop: Header=BB13_3 Depth=1
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB13_5
.LBB13_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, (%rbx)
	jne	.LBB13_4
	jmp	.LBB13_6
.LBB13_5:                               # %._crit_edge.i
	movl	$56, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	%r14, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movl	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movups	%xmm0, 40(%rbx)
	movq	%rbx, %rdi
	callq	_ZN12CProfileNode5ResetEv
	movq	40(%r15), %rax
	movq	%rax, 48(%rbx)
	movq	%rbx, 40(%r15)
.LBB13_6:                               # %_ZN12CProfileNode12Get_Sub_NodeEPKc.exit
	movq	%rbx, _ZN15CProfileManager11CurrentNodeE(%rip)
	movq	%rbx, %r15
.LBB13_7:
	incl	8(%r15)
	movl	24(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r15)
	testl	%eax, %eax
	jne	.LBB13_9
# BB#8:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	subq	_ZL13gProfileClock(%rip), %rax
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rcx
	addq	%rax, %rcx
	movq	%rcx, 16(%r15)
.LBB13_9:                               # %_ZN12CProfileNode4CallEv.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	_ZN15CProfileManager13Start_ProfileEPKc, .Lfunc_end13-_ZN15CProfileManager13Start_ProfileEPKc
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1148846080              # float 1000
	.text
	.globl	_ZN15CProfileManager12Stop_ProfileEv
	.p2align	4, 0x90
	.type	_ZN15CProfileManager12Stop_ProfileEv,@function
_ZN15CProfileManager12Stop_ProfileEv:   # @_ZN15CProfileManager12Stop_ProfileEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	_ZN15CProfileManager11CurrentNodeE(%rip), %rbx
	decl	24(%rbx)
	jne	.LBB14_8
# BB#1:
	cmpl	$0, 8(%rbx)
	je	.LBB14_7
# BB#2:                                 # %_ZN12CProfileNode6ReturnEv.exit
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	(%rsp), %rcx
	movq	8(%rsp), %rax
	subq	_ZL13gProfileClock(%rip), %rcx
	imulq	$1000000, %rcx, %rcx    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rax
	addq	%rcx, %rax
	movq	16(%rbx), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	subq	%rdx, %rax
	js	.LBB14_3
# BB#4:                                 # %_ZN12CProfileNode6ReturnEv.exit
	cvtsi2ssq	%rax, %xmm0
	jmp	.LBB14_5
.LBB14_3:
	movq	%rcx, %rax
	shrq	%rax
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm0
	addss	%xmm0, %xmm0
.LBB14_5:                               # %_ZN12CProfileNode6ReturnEv.exit
	divss	.LCPI14_0(%rip), %xmm0
	addss	12(%rbx), %xmm0
	movss	%xmm0, 12(%rbx)
	cmpl	$0, 24(%rbx)
	jne	.LBB14_8
# BB#6:                                 # %_ZN12CProfileNode6ReturnEv.exit._ZN12CProfileNode6ReturnEv.exit.thread_crit_edge
	movq	_ZN15CProfileManager11CurrentNodeE(%rip), %rbx
.LBB14_7:                               # %_ZN12CProfileNode6ReturnEv.exit.thread
	movq	32(%rbx), %rax
	movq	%rax, _ZN15CProfileManager11CurrentNodeE(%rip)
.LBB14_8:                               # %_ZN12CProfileNode6ReturnEv.exit.thread1
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end14:
	.size	_ZN15CProfileManager12Stop_ProfileEv, .Lfunc_end14-_ZN15CProfileManager12Stop_ProfileEv
	.cfi_endproc

	.globl	_ZN15CProfileManager5ResetEv
	.p2align	4, 0x90
	.type	_ZN15CProfileManager5ResetEv,@function
_ZN15CProfileManager5ResetEv:           # @_ZN15CProfileManager5ResetEv
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 32
	movl	$_ZL13gProfileClock, %edi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$_ZN15CProfileManager4RootE, %edi
	callq	_ZN12CProfileNode5ResetEv
	incl	_ZN15CProfileManager4RootE+8(%rip)
	movl	_ZN15CProfileManager4RootE+24(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, _ZN15CProfileManager4RootE+24(%rip)
	testl	%eax, %eax
	jne	.LBB15_2
# BB#1:
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	subq	_ZL13gProfileClock(%rip), %rax
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rcx
	addq	%rax, %rcx
	movq	%rcx, _ZN15CProfileManager4RootE+16(%rip)
.LBB15_2:                               # %_ZN12CProfileNode4CallEv.exit
	movl	$0, _ZN15CProfileManager12FrameCounterE(%rip)
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	subq	_ZL13gProfileClock(%rip), %rax
	imulq	$1000000, %rax, %rax    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rcx
	addq	%rax, %rcx
	movq	%rcx, _ZN15CProfileManager9ResetTimeE(%rip)
	addq	$24, %rsp
	retq
.Lfunc_end15:
	.size	_ZN15CProfileManager5ResetEv, .Lfunc_end15-_ZN15CProfileManager5ResetEv
	.cfi_endproc

	.globl	_ZN15CProfileManager23Increment_Frame_CounterEv
	.p2align	4, 0x90
	.type	_ZN15CProfileManager23Increment_Frame_CounterEv,@function
_ZN15CProfileManager23Increment_Frame_CounterEv: # @_ZN15CProfileManager23Increment_Frame_CounterEv
	.cfi_startproc
# BB#0:
	incl	_ZN15CProfileManager12FrameCounterE(%rip)
	retq
.Lfunc_end16:
	.size	_ZN15CProfileManager23Increment_Frame_CounterEv, .Lfunc_end16-_ZN15CProfileManager23Increment_Frame_CounterEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	1148846080              # float 1000
	.text
	.globl	_ZN15CProfileManager20Get_Time_Since_ResetEv
	.p2align	4, 0x90
	.type	_ZN15CProfileManager20Get_Time_Since_ResetEv,@function
_ZN15CProfileManager20Get_Time_Since_ResetEv: # @_ZN15CProfileManager20Get_Time_Since_ResetEv
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rax
	subq	_ZL13gProfileClock(%rip), %rcx
	imulq	$1000000, %rcx, %rcx    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rax
	addq	%rcx, %rax
	movq	_ZN15CProfileManager9ResetTimeE(%rip), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	subq	%rdx, %rax
	js	.LBB17_1
# BB#2:
	cvtsi2ssq	%rax, %xmm0
	jmp	.LBB17_3
.LBB17_1:
	movq	%rcx, %rax
	shrq	%rax
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm0
	addss	%xmm0, %xmm0
.LBB17_3:
	divss	.LCPI17_0(%rip), %xmm0
	addq	$24, %rsp
	retq
.Lfunc_end17:
	.size	_ZN15CProfileManager20Get_Time_Since_ResetEv, .Lfunc_end17-_ZN15CProfileManager20Get_Time_Since_ResetEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1148846080              # float 1000
.LCPI18_1:
	.long	872415232               # float 1.1920929E-7
.LCPI18_2:
	.long	1120403456              # float 100
	.text
	.globl	_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori
	.p2align	4, 0x90
	.type	_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori,@function
_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori: # @_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 96
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	40(%rax), %rcx
	movq	%rcx, 8(%rbx)
	testq	%rcx, %rcx
	je	.LBB18_48
# BB#1:
	cmpq	$0, 32(%rax)
	je	.LBB18_3
# BB#2:
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB18_7
.LBB18_3:
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	32(%rsp), %rcx
	movq	40(%rsp), %rax
	subq	_ZL13gProfileClock(%rip), %rcx
	imulq	$1000000, %rcx, %rcx    # imm = 0xF4240
	subq	_ZL13gProfileClock+8(%rip), %rax
	addq	%rcx, %rax
	movq	_ZN15CProfileManager9ResetTimeE(%rip), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	subq	%rdx, %rax
	js	.LBB18_5
# BB#4:
	cvtsi2ssq	%rax, %xmm0
	jmp	.LBB18_6
.LBB18_5:
	movq	%rcx, %rax
	shrq	%rax
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm0
	addss	%xmm0, %xmm0
.LBB18_6:
	divss	.LCPI18_0(%rip), %xmm0
.LBB18_7:
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	_ZN15CProfileManager12FrameCounterE(%rip), %r15d
	testl	%r14d, %r14d
	jle	.LBB18_13
# BB#8:
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_9:                               # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	movl	$46, %edi
	callq	putchar
	decl	%ebp
	jne	.LBB18_9
# BB#10:                                # %._crit_edge106
	movl	$.Lstr, %edi
	callq	puts
	testl	%r14d, %r14d
	jle	.LBB18_14
# BB#11:                                # %.lr.ph101.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_12:                              # %.lr.ph101
                                        # =>This Inner Loop Header: Depth=1
	movl	$46, %edi
	callq	putchar
	decl	%ebp
	jne	.LBB18_12
	jmp	.LBB18_14
.LBB18_13:                              # %._crit_edge106.thread
	movl	$.Lstr, %edi
	callq	puts
.LBB18_14:                              # %._crit_edge102
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.4, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB18_21
# BB#15:                                # %.lr.ph89
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI18_1(%rip), %xmm0
	jbe	.LBB18_22
# BB#16:                                # %.lr.ph89.split.us.preheader
	xorl	%r12d, %r12d
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB18_17:                              # %.lr.ph89.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_18 Depth 2
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	divss	8(%rsp), %xmm0          # 4-byte Folded Reload
	mulss	.LCPI18_2(%rip), %xmm0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	testl	%r14d, %r14d
	movl	%r14d, %ebp
	jle	.LBB18_20
	.p2align	4, 0x90
.LBB18_18:                              # %.lr.ph83.us
                                        #   Parent Loop BB18_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$46, %edi
	callq	putchar
	decl	%ebp
	jne	.LBB18_18
# BB#19:                                # %._crit_edge84.us.loopexit
                                        #   in Loop: Header=BB18_17 Depth=1
	movq	8(%rbx), %rax
.LBB18_20:                              # %._crit_edge84.us
                                        #   in Loop: Header=BB18_17 Depth=1
	leal	1(%r12), %r15d
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	(%rax), %rdx
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	divsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	movl	8(%rax), %ecx
	movl	$.L.str.5, %edi
	movb	$2, %al
	movl	%r12d, %esi
	callq	printf
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	movl	%r15d, %r12d
	jne	.LBB18_17
	jmp	.LBB18_29
.LBB18_21:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB18_29
.LBB18_22:                              # %.lr.ph89.split
	testl	%r14d, %r14d
	jle	.LBB18_27
# BB#23:                                # %.lr.ph89.split.split.us.preheader
	xorl	%r12d, %r12d
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB18_24:                              # %.lr.ph89.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_25 Depth 2
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_25:                              #   Parent Loop BB18_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$46, %edi
	callq	putchar
	decl	%ebp
	jne	.LBB18_25
# BB#26:                                # %._crit_edge84.us98
                                        #   in Loop: Header=BB18_24 Depth=1
	leal	1(%r12), %r15d
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	8(%rbx), %rax
	movq	(%rax), %rdx
	cvtss2sd	%xmm1, %xmm1
	divsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	movl	8(%rax), %ecx
	movl	$.L.str.5, %edi
	xorps	%xmm0, %xmm0
	movb	$2, %al
	movl	%r12d, %esi
	callq	printf
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	movl	%r15d, %r12d
	jne	.LBB18_24
	jmp	.LBB18_29
.LBB18_27:                              # %.lr.ph89.split.split.preheader
	xorl	%esi, %esi
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB18_28:                              # %.lr.ph89.split.split
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rsi), %r15d
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movq	(%rax), %rdx
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	divsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	movl	8(%rax), %ecx
	movl	$.L.str.5, %edi
	xorps	%xmm0, %xmm0
	movb	$2, %al
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	movl	%r15d, %esi
	jne	.LBB18_28
.LBB18_29:                              # %._crit_edge90
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rsp), %xmm0          # 4-byte Folded Reload
	jbe	.LBB18_31
# BB#30:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB18_31:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB18_34
# BB#32:                                # %.lr.ph80.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_33:                              # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	movl	$46, %edi
	callq	putchar
	decl	%ebp
	jne	.LBB18_33
.LBB18_34:                              # %._crit_edge
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	subss	4(%rsp), %xmm1          # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	ucomiss	.LCPI18_1(%rip), %xmm2
	jbe	.LBB18_36
# BB#35:
	movaps	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	mulss	.LCPI18_2(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
.LBB18_36:                              # %._crit_edge._crit_edge
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.7, %edi
	movl	$.L.str.8, %esi
	movb	$2, %al
	callq	printf
	testl	%r15d, %r15d
	jle	.LBB18_48
# BB#37:                                # %.lr.ph
	addl	$3, %r14d
	movq	(%rbx), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_38:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_41 Depth 2
	movq	40(%rax), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	setne	%cl
	testl	%ebp, %ebp
	je	.LBB18_43
# BB#39:                                #   in Loop: Header=BB18_38 Depth=1
	testq	%rax, %rax
	je	.LBB18_43
# BB#40:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_38 Depth=1
	movl	$1, %edx
	.p2align	4, 0x90
.LBB18_41:                              # %.lr.ph.i
                                        #   Parent Loop BB18_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rax), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	setne	%cl
	cmpl	%ebp, %edx
	je	.LBB18_43
# BB#42:                                # %.lr.ph.i
                                        #   in Loop: Header=BB18_41 Depth=2
	incl	%edx
	testq	%rax, %rax
	jne	.LBB18_41
.LBB18_43:                              # %._crit_edge.i
                                        #   in Loop: Header=BB18_38 Depth=1
	testb	%cl, %cl
	je	.LBB18_45
# BB#44:                                #   in Loop: Header=BB18_38 Depth=1
	movq	%rax, (%rbx)
	movq	40(%rax), %rax
	movq	%rax, 8(%rbx)
.LBB18_45:                              # %_ZN16CProfileIterator11Enter_ChildEi.exit
                                        #   in Loop: Header=BB18_38 Depth=1
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori
	movq	(%rbx), %rax
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB18_47
# BB#46:                                #   in Loop: Header=BB18_38 Depth=1
	movq	%rcx, (%rbx)
	movq	%rcx, %rax
.LBB18_47:                              # %_ZN16CProfileIterator12Enter_ParentEv.exit
                                        #   in Loop: Header=BB18_38 Depth=1
	movq	40(%rax), %rcx
	movq	%rcx, 8(%rbx)
	incl	%ebp
	cmpl	%r15d, %ebp
	jne	.LBB18_38
.LBB18_48:                              # %.loopexit
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori, .Lfunc_end18-_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori
	.cfi_endproc

	.globl	_ZN15CProfileManager7dumpAllEv
	.p2align	4, 0x90
	.type	_ZN15CProfileManager7dumpAllEv,@function
_ZN15CProfileManager7dumpAllEv:         # @_ZN15CProfileManager7dumpAllEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$_ZN15CProfileManager4RootE, (%rbx)
	movq	_ZN15CProfileManager4RootE+40(%rip), %rax
	movq	%rax, 8(%rbx)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZN15CProfileManager7dumpAllEv, .Lfunc_end19-_ZN15CProfileManager7dumpAllEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btQuickprof.ii,@function
_GLOBAL__sub_I_btQuickprof.ii:          # @_GLOBAL__sub_I_btQuickprof.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	movl	$_ZL13gProfileClock, %edi
	xorl	%esi, %esi
	callq	gettimeofday
	movq	$.L.str, _ZN15CProfileManager4RootE(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZN15CProfileManager4RootE+8(%rip)
	movl	$0, _ZN15CProfileManager4RootE+24(%rip)
	movups	%xmm0, _ZN15CProfileManager4RootE+32(%rip)
	movq	$0, _ZN15CProfileManager4RootE+48(%rip)
	movl	$_ZN15CProfileManager4RootE, %edi
	callq	_ZN12CProfileNode5ResetEv
	movl	$_ZN12CProfileNodeD2Ev, %edi
	movl	$_ZN15CProfileManager4RootE, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end20:
	.size	_GLOBAL__sub_I_btQuickprof.ii, .Lfunc_end20-_GLOBAL__sub_I_btQuickprof.ii
	.cfi_endproc

	.type	_ZL13gProfileClock,@object # @_ZL13gProfileClock
	.local	_ZL13gProfileClock
	.comm	_ZL13gProfileClock,16,8
	.type	_ZN15CProfileManager4RootE,@object # @_ZN15CProfileManager4RootE
	.bss
	.globl	_ZN15CProfileManager4RootE
	.p2align	3
_ZN15CProfileManager4RootE:
	.zero	56
	.size	_ZN15CProfileManager4RootE, 56

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Root"
	.size	.L.str, 5

	.type	_ZN15CProfileManager11CurrentNodeE,@object # @_ZN15CProfileManager11CurrentNodeE
	.data
	.globl	_ZN15CProfileManager11CurrentNodeE
	.p2align	3
_ZN15CProfileManager11CurrentNodeE:
	.quad	_ZN15CProfileManager4RootE
	.size	_ZN15CProfileManager11CurrentNodeE, 8

	.type	_ZN15CProfileManager12FrameCounterE,@object # @_ZN15CProfileManager12FrameCounterE
	.bss
	.globl	_ZN15CProfileManager12FrameCounterE
	.p2align	2
_ZN15CProfileManager12FrameCounterE:
	.long	0                       # 0x0
	.size	_ZN15CProfileManager12FrameCounterE, 4

	.type	_ZN15CProfileManager9ResetTimeE,@object # @_ZN15CProfileManager9ResetTimeE
	.globl	_ZN15CProfileManager9ResetTimeE
	.p2align	3
_ZN15CProfileManager9ResetTimeE:
	.quad	0                       # 0x0
	.size	_ZN15CProfileManager9ResetTimeE, 8

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"Profiling: %s (total running time: %.3f ms) ---\n"
	.size	.L.str.4, 49

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d -- %s (%.2f %%) :: %.3f ms / frame (%d calls)\n"
	.size	.L.str.5, 50

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s (%.3f %%) :: %.3f ms\n"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Unaccounted:"
	.size	.L.str.8, 13

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btQuickprof.ii
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"----------------------------------"
	.size	.Lstr, 35

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"what's wrong"
	.size	.Lstr.1, 13


	.globl	_ZN12CProfileNodeC1EPKcPS_
	.type	_ZN12CProfileNodeC1EPKcPS_,@function
_ZN12CProfileNodeC1EPKcPS_ = _ZN12CProfileNodeC2EPKcPS_
	.globl	_ZN12CProfileNodeD1Ev
	.type	_ZN12CProfileNodeD1Ev,@function
_ZN12CProfileNodeD1Ev = _ZN12CProfileNodeD2Ev
	.globl	_ZN16CProfileIteratorC1EP12CProfileNode
	.type	_ZN16CProfileIteratorC1EP12CProfileNode,@function
_ZN16CProfileIteratorC1EP12CProfileNode = _ZN16CProfileIteratorC2EP12CProfileNode
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
