	.text
	.file	"utrace.bc"
	.globl	strupr
	.p2align	4, 0x90
	.type	strupr,@function
strupr:                                 # @strupr
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end0:
	.size	strupr, .Lfunc_end0-strupr
	.cfi_endproc

	.globl	trace_open_map
	.p2align	4, 0x90
	.type	trace_open_map,@function
trace_open_map:                         # @trace_open_map
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	trace_open_map, .Lfunc_end1-trace_open_map
	.cfi_endproc

	.globl	trace_next_symbol
	.p2align	4, 0x90
	.type	trace_next_symbol,@function
trace_next_symbol:                      # @trace_next_symbol
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	trace_next_symbol, .Lfunc_end2-trace_next_symbol
	.cfi_endproc

	.globl	trace_find_symbol
	.p2align	4, 0x90
	.type	trace_find_symbol,@function
trace_find_symbol:                      # @trace_find_symbol
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	trace_find_symbol, .Lfunc_end3-trace_find_symbol
	.cfi_endproc

	.globl	trace_name
	.p2align	4, 0x90
	.type	trace_name,@function
trace_name:                             # @trace_name
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end4:
	.size	trace_name, .Lfunc_end4-trace_name
	.cfi_endproc

	.globl	trace
	.p2align	4, 0x90
	.type	trace,@function
trace:                                  # @trace
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end5:
	.size	trace, .Lfunc_end5-trace
	.cfi_endproc

	.globl	stack_top_frame
	.p2align	4, 0x90
	.type	stack_top_frame,@function
stack_top_frame:                        # @stack_top_frame
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 224
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	_setjmp
	movq	24(%rsp), %rcx
	xorl	%eax, %eax
	cmpq	%rbx, %rcx
	cmovaeq	%rcx, %rax
	addq	$208, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	stack_top_frame, .Lfunc_end6-stack_top_frame
	.cfi_endproc

	.globl	stack_next_frame
	.p2align	4, 0x90
	.type	stack_next_frame,@function
stack_next_frame:                       # @stack_next_frame
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rdi, %rcx
	cmovaeq	%rcx, %rax
	retq
.Lfunc_end7:
	.size	stack_next_frame, .Lfunc_end7-stack_next_frame
	.cfi_endproc

	.globl	stack_return
	.p2align	4, 0x90
	.type	stack_return,@function
stack_return:                           # @stack_return
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end8:
	.size	stack_return, .Lfunc_end8-stack_return
	.cfi_endproc

	.type	trace_flush_flag,@object # @trace_flush_flag
	.comm	trace_flush_flag,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
