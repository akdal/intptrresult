	.text
	.file	"memory.bc"
	.globl	memory_Init
	.p2align	4, 0x90
	.type	memory_Init,@function
memory_Init:                            # @memory_Init
	.cfi_startproc
# BB#0:
	movq	$0, memory_FREEDBYTES(%rip)
	movq	$0, memory_NEWBYTES(%rip)
	movl	$8192, memory_PAGESIZE(%rip) # imm = 0x2000
	movl	memory_MARKSIZE(%rip), %r8d
	leal	16(%r8), %eax
	leal	2064(%r8,%r8,2), %ecx
	movl	$8192, %r11d            # imm = 0x2000
	cmpl	$8193, %ecx             # imm = 0x2001
	jb	.LBB0_4
# BB#1:                                 # %.lr.ph.preheader
	movl	$8192, %r11d            # imm = 0x2000
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	$4096, %r11d            # imm = 0x1000
	cmpl	%ecx, %r11d
	jb	.LBB0_2
# BB#3:                                 # %._crit_edge
	movl	%r11d, memory_PAGESIZE(%rip)
.LBB0_4:
	testq	%rdi, %rdi
	movq	$-1, %rcx
	cmovgq	%rdi, %rcx
	movq	%rcx, memory_MAXMEM(%rip)
	subl	%eax, %r11d
	movl	memory_OFFSET(%rip), %r10d
	incl	%r8d
	movl	$memory_PAGES, %esi
	xorl	%r9d, %r9d
	movl	$memory__EOF, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	leaq	1(%rdi), %rcx
	movl	%ecx, %eax
	andl	$7, %eax
	movl	$8, %edx
	subl	%eax, %edx
	movl	%ecx, %eax
	andl	$7, %eax
	movq	%rsi, memory_ARRAY+8(,%rdi,8)
	movdqa	%xmm0, (%rsi)
	movdqa	%xmm0, 16(%rsi)
	cmovel	%r9d, %edx
	leal	1(%rdi,%rdx), %eax
	movl	%eax, 36(%rsi)
	addl	%r8d, %edi
	addl	%edx, %edi
	movl	%edi, 32(%rsi)
	xorl	%edx, %edx
	movl	%r11d, %eax
	divl	%edi
	movl	$8, %eax
	subl	%edx, %eax
	addl	%r11d, %eax
	addl	%r10d, %eax
	movl	%eax, 40(%rsi)
	addq	$48, %rsi
	cmpq	$1023, %rcx             # imm = 0x3FF
	movq	%rcx, %rdi
	jne	.LBB0_5
# BB#6:
	retq
.Lfunc_end0:
	.size	memory_Init, .Lfunc_end0-memory_Init
	.cfi_endproc

	.globl	memory_Restrict
	.p2align	4, 0x90
	.type	memory_Restrict,@function
memory_Restrict:                        # @memory_Restrict
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	movq	$-1, %rax
	cmovgq	%rdi, %rax
	movq	%rax, memory_MAXMEM(%rip)
	retq
.Lfunc_end1:
	.size	memory_Restrict, .Lfunc_end1-memory_Restrict
	.cfi_endproc

	.globl	memory_Malloc
	.p2align	4, 0x90
	.type	memory_Malloc,@function
memory_Malloc:                          # @memory_Malloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	cmpl	$1024, %edi             # imm = 0x400
	jb	.LBB2_10
# BB#1:
	movl	%edi, %eax
	andl	$7, %eax
	movl	$8, %ecx
	subl	%eax, %ecx
	xorl	%eax, %eax
	movl	%edi, %edx
	andl	$7, %edx
	movl	memory_MARKSIZE(%rip), %edx
	cmovnel	%ecx, %eax
	addl	%edi, %eax
	leal	16(%rdx,%rax), %ebp
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_2
# BB#3:
	cmpl	%ebp, %eax
	jb	.LBB2_24
# BB#4:
	movl	%ebp, %ebx
	subq	%rbx, %rax
	movq	%rax, memory_MAXMEM(%rip)
	jmp	.LBB2_5
.LBB2_10:
	movl	%edi, %eax
	movq	memory_ARRAY(,%rax,8), %rbx
	movq	(%rbx), %rax
	cmpl	$-1, (%rax)
	je	.LBB2_12
# BB#11:
	movq	(%rax), %rcx
	movq	%rcx, (%rbx)
	movslq	32(%rbx), %rcx
	subq	%rcx, memory_FREEDBYTES(%rip)
	jmp	.LBB2_23
.LBB2_2:                                # %._crit_edge47
	movl	%ebp, %ebx
.LBB2_5:
	movq	%rbx, %rdi
	callq	malloc
	testq	%rax, %rax
	je	.LBB2_9
# BB#6:
	movq	memory_BIGBLOCKS(%rip), %rcx
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	testq	%rcx, %rcx
	je	.LBB2_8
# BB#7:
	movq	%rax, (%rcx)
.LBB2_8:
	movq	%rax, memory_BIGBLOCKS(%rip)
	addq	$16, %rax
	addq	%rbx, memory_NEWBYTES(%rip)
.LBB2_23:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB2_12:
	movq	8(%rbx), %rax
	cmpq	24(%rbx), %rax
	je	.LBB2_14
# BB#13:
	movslq	32(%rbx), %rcx
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 8(%rbx)
	addq	%rcx, memory_NEWBYTES(%rip)
	jmp	.LBB2_23
.LBB2_14:
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB2_15
# BB#16:
	movl	memory_PAGESIZE(%rip), %eax
	cmpl	%eax, %ecx
	jb	.LBB2_17
# BB#19:
	subq	%rax, %rcx
	movq	%rcx, memory_MAXMEM(%rip)
	jmp	.LBB2_20
.LBB2_15:                               # %._crit_edge
	movl	memory_PAGESIZE(%rip), %eax
.LBB2_20:
	movl	%eax, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB2_21
# BB#22:
	movq	16(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 16(%rbx)
	movslq	32(%rbx), %rcx
	addq	%rcx, memory_NEWBYTES(%rip)
	movslq	40(%rbx), %rdx
	addq	%rax, %rdx
	movq	%rdx, 24(%rbx)
	leaq	8(%rax,%rcx), %rcx
	addq	$8, %rax
	movq	%rcx, 8(%rbx)
	jmp	.LBB2_23
.LBB2_9:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB2_24:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movq	memory_MAXMEM(%rip), %rsi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB2_21:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.10, %edi
	jmp	.LBB2_18
.LBB2_17:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.9, %edi
.LBB2_18:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end2:
	.size	memory_Malloc, .Lfunc_end2-memory_Malloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	misc_Error, .Lfunc_end3-misc_Error
	.cfi_endproc

	.globl	memory_Calloc
	.p2align	4, 0x90
	.type	memory_Calloc,@function
memory_Calloc:                          # @memory_Calloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	imull	%esi, %ebp
	movl	%ebp, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_2
# BB#1:
	movl	%ebp, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB4_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end4:
	.size	memory_Calloc, .Lfunc_end4-memory_Calloc
	.cfi_endproc

	.globl	memory_FreeAllMem
	.p2align	4, 0x90
	.type	memory_FreeAllMem,@function
memory_FreeAllMem:                      # @memory_FreeAllMem
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movl	$1, %r15d
	movl	$memory__EOF, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movq	memory_ARRAY(,%r15,8), %r14
	movq	16(%r14), %rdi
	cmpl	$-1, (%rdi)
	je	.LBB5_4
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader29
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbx
	callq	free
	cmpl	$-1, (%rbx)
	movq	%rbx, %rdi
	jne	.LBB5_2
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	movdqa	(%rsp), %xmm0           # 16-byte Reload
	movdqu	%xmm0, (%r14)
	movdqu	%xmm0, 16(%r14)
.LBB5_4:                                #   in Loop: Header=BB5_1 Depth=1
	incq	%r15
	cmpq	$1024, %r15             # imm = 0x400
	jne	.LBB5_1
# BB#5:
	movq	memory_BIGBLOCKS(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB5_6
# BB#7:                                 # %._crit_edge
	movq	$0, memory_BIGBLOCKS(%rip)
.LBB5_8:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	memory_FreeAllMem, .Lfunc_end5-memory_FreeAllMem
	.cfi_endproc

	.globl	memory_Print
	.p2align	4, 0x90
	.type	memory_Print,@function
memory_Print:                           # @memory_Print
	.cfi_startproc
# BB#0:
	movq	stdout(%rip), %rdi
	jmp	memory_FPrint           # TAILCALL
.Lfunc_end6:
	.size	memory_Print, .Lfunc_end6-memory_Print
	.cfi_endproc

	.globl	memory_FPrint
	.p2align	4, 0x90
	.type	memory_FPrint,@function
memory_FPrint:                          # @memory_FPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	movl	$1, %eax
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_8:                                # %._crit_edge.1
                                        #   in Loop: Header=BB7_1 Depth=1
	addq	$2, %rax
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #     Child Loop BB7_6 Depth 2
	movq	memory_ARRAY(,%rax,8), %rcx
	movq	16(%rcx), %rcx
	cmpl	$-1, (%rcx)
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	movq	(%rcx), %rcx
	cmpl	$-1, (%rcx)
	jne	.LBB7_2
.LBB7_4:                                # %._crit_edge
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	$1023, %rax             # imm = 0x3FF
	je	.LBB7_9
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	memory_ARRAY+8(,%rax,8), %rcx
	movq	16(%rcx), %rcx
	cmpl	$-1, (%rcx)
	je	.LBB7_8
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.1
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	movq	(%rcx), %rcx
	cmpl	$-1, (%rcx)
	jne	.LBB7_6
	jmp	.LBB7_8
.LBB7_9:
	movl	$.L.str.13, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	memory_PAGESIZE(%rip), %edx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	memory_MARKSIZE(%rip), %edx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	memory_NEWBYTES(%rip), %rdx
	shrq	$10, %rdx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	memory_FREEDBYTES(%rip), %rdx
	shrq	$10, %rdx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	memory_NEWBYTES(%rip), %rdx
	subq	memory_FREEDBYTES(%rip), %rdx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
	movl	$.L.str.20, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end7:
	.size	memory_FPrint, .Lfunc_end7-memory_FPrint
	.cfi_endproc

	.globl	memory_PrintAllocatedBlocks
	.p2align	4, 0x90
	.type	memory_PrintAllocatedBlocks,@function
memory_PrintAllocatedBlocks:            # @memory_PrintAllocatedBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	$1024, %ebx             # imm = 0x400
	jae	.LBB8_5
# BB#1:
	movl	%ebx, %eax
	movq	memory_ARRAY(,%rax,8), %rax
	movq	16(%rax), %rcx
	cmpl	$-1, (%rcx)
	je	.LBB8_2
# BB#4:
	popq	%rbx
	retq
.LBB8_2:
	movq	8(%rax), %rax
	cmpl	$-1, (%rax)
	jne	.LBB8_3
# BB#6:
	movl	$.L.str.24, %edi
	popq	%rbx
	jmp	puts                    # TAILCALL
.LBB8_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	misc_UserErrorReport
	movl	$.L.str.23, %edi
	movl	$1024, %esi             # imm = 0x400
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB8_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.25, %esi
	movl	$.L.str.26, %edx
	movl	$1192, %ecx             # imm = 0x4A8
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end8:
	.size	memory_PrintAllocatedBlocks, .Lfunc_end8-memory_PrintAllocatedBlocks
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.40, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end9:
	.size	misc_DumpCore, .Lfunc_end9-misc_DumpCore
	.cfi_endproc

	.globl	memory_PrintFreedBlocks
	.p2align	4, 0x90
	.type	memory_PrintFreedBlocks,@function
memory_PrintFreedBlocks:                # @memory_PrintFreedBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	$1024, %ebx             # imm = 0x400
	jae	.LBB10_6
# BB#1:
	movl	%ebx, %eax
	movq	memory_ARRAY(,%rax,8), %rax
	movq	(%rax), %rbx
	cmpl	$-1, (%rbx)
	je	.LBB10_7
# BB#2:
	movq	stdout(%rip), %rcx
	movl	$.L.str.32, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$-1, (%rbx)
	je	.LBB10_5
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	cmpl	$-1, (%rbx)
	jne	.LBB10_3
.LBB10_5:                               # %.loopexit
	popq	%rbx
	retq
.LBB10_7:
	movl	$.L.str.31, %edi
	popq	%rbx
	jmp	puts                    # TAILCALL
.LBB10_6:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	misc_UserErrorReport
	movl	$.L.str.23, %edi
	movl	$1024, %esi             # imm = 0x400
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end10:
	.size	memory_PrintFreedBlocks, .Lfunc_end10-memory_PrintFreedBlocks
	.cfi_endproc

	.globl	memory_PrintAllocatedBigBlocks
	.p2align	4, 0x90
	.type	memory_PrintAllocatedBigBlocks,@function
memory_PrintAllocatedBigBlocks:         # @memory_PrintAllocatedBigBlocks
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	memory_PrintAllocatedBigBlocks, .Lfunc_end11-memory_PrintAllocatedBigBlocks
	.cfi_endproc

	.globl	memory_PrintDetailed
	.p2align	4, 0x90
	.type	memory_PrintDetailed,@function
memory_PrintDetailed:                   # @memory_PrintDetailed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	$.L.str.33, %edi
	movl	$memory__EOF, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
                                        #       Child Loop BB12_9 Depth 3
                                        #         Child Loop BB12_10 Depth 4
	movq	memory_ARRAY(,%rbx,8), %rax
	movq	8(%rax), %r14
	movq	16(%rax), %r15
	movl	40(%rax), %ebp
	movl	32(%rax), %r12d
	movl	36(%rax), %edx
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r12d, %ecx
	callq	printf
	cmpl	$-1, (%r15)
	je	.LBB12_5
# BB#2:                                 # %.lr.ph52
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbp, %rax
	subq	%r12, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_3:                               #   Parent Loop BB12_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_9 Depth 3
                                        #         Child Loop BB12_10 Depth 4
	movq	(%r15), %rdx
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movl	memory_OFFSET(%rip), %eax
	leaq	8(%r15,%rax), %rbx
	leaq	(%r15,%rbp), %r14
	movq	stdout(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	cmpq	%r14, %rbx
	je	.LBB12_12
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_3 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%r15,%rax), %rbp
	.p2align	4, 0x90
.LBB12_9:                               # %.lr.ph
                                        #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_10 Depth 4
	movq	stdout(%rip), %rcx
	movl	$.L.str.38, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB12_10:                              #   Parent Loop BB12_1 Depth=1
                                        #     Parent Loop BB12_3 Depth=2
                                        #       Parent Loop BB12_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpq	%rbx, %rbp
	leaq	(%rbx,%r12), %rbx
	je	.LBB12_8
# BB#11:                                #   in Loop: Header=BB12_10 Depth=4
	cmpl	$6, %r13d
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jl	.LBB12_10
.LBB12_8:                               # %.loopexit
                                        #   in Loop: Header=BB12_9 Depth=3
	cmpq	%rbx, %r14
	jne	.LBB12_9
.LBB12_12:                              # %._crit_edge
                                        #   in Loop: Header=BB12_3 Depth=2
	movq	(%r15), %r15
	cmpl	$-1, (%r15)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB12_3
# BB#13:                                # %._crit_edge53
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%ebx, %edi
	callq	memory_PrintAllocatedBlocks
	movl	%ebx, %edi
	callq	memory_PrintFreedBlocks
	jmp	.LBB12_14
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_1 Depth=1
	cmpl	$-1, (%r14)
	jne	.LBB12_7
# BB#6:                                 #   in Loop: Header=BB12_1 Depth=1
	movl	$.L.str.24, %edi
	callq	puts
.LBB12_14:                              #   in Loop: Header=BB12_1 Depth=1
	incq	%rbx
	cmpq	$1024, %rbx             # imm = 0x400
	jb	.LBB12_1
# BB#15:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_7:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.25, %esi
	movl	$.L.str.26, %edx
	movl	$1443, %ecx             # imm = 0x5A3
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end12:
	.size	memory_PrintDetailed, .Lfunc_end12-memory_PrintDetailed
	.cfi_endproc

	.globl	memory_PrintLeaks
	.p2align	4, 0x90
	.type	memory_PrintLeaks,@function
memory_PrintLeaks:                      # @memory_PrintLeaks
	.cfi_startproc
# BB#0:                                 # %.loopexit
	retq
.Lfunc_end13:
	.size	memory_PrintLeaks, .Lfunc_end13-memory_PrintLeaks
	.cfi_endproc

	.type	memory_ALIGN,@object    # @memory_ALIGN
	.section	.rodata,"a",@progbits
	.globl	memory_ALIGN
	.p2align	2
memory_ALIGN:
	.long	8                       # 0x8
	.size	memory_ALIGN, 4

	.type	memory_MARKSIZE,@object # @memory_MARKSIZE
	.bss
	.globl	memory_MARKSIZE
	.p2align	2
memory_MARKSIZE:
	.long	0                       # 0x0
	.size	memory_MARKSIZE, 4

	.type	memory_OFFSET,@object   # @memory_OFFSET
	.globl	memory_OFFSET
	.p2align	2
memory_OFFSET:
	.long	0                       # 0x0
	.size	memory_OFFSET, 4

	.type	memory_MAGICMALLOC,@object # @memory_MAGICMALLOC
	.section	.rodata,"a",@progbits
	.globl	memory_MAGICMALLOC
	.p2align	2
memory_MAGICMALLOC:
	.long	1                       # 0x1
	.size	memory_MAGICMALLOC, 4

	.type	memory_MAGICFREE,@object # @memory_MAGICFREE
	.globl	memory_MAGICFREE
	.p2align	2
memory_MAGICFREE:
	.long	2                       # 0x2
	.size	memory_MAGICFREE, 4

	.type	memory_BIGBLOCKS,@object # @memory_BIGBLOCKS
	.bss
	.globl	memory_BIGBLOCKS
	.p2align	3
memory_BIGBLOCKS:
	.quad	0
	.size	memory_BIGBLOCKS, 8

	.type	memory_FREEDBYTES,@object # @memory_FREEDBYTES
	.comm	memory_FREEDBYTES,8,8
	.type	memory_NEWBYTES,@object # @memory_NEWBYTES
	.comm	memory_NEWBYTES,8,8
	.type	memory_PAGESIZE,@object # @memory_PAGESIZE
	.comm	memory_PAGESIZE,4,4
	.type	memory_MAXMEM,@object   # @memory_MAXMEM
	.comm	memory_MAXMEM,8,8
	.type	memory_PAGES,@object    # @memory_PAGES
	.local	memory_PAGES
	.comm	memory_PAGES,49200,16
	.type	memory_ARRAY,@object    # @memory_ARRAY
	.comm	memory_ARRAY,8192,16
	.type	memory__EOF,@object     # @memory__EOF
	.data
	.p2align	2
memory__EOF:
	.long	4294967295              # 0xffffffff
	.size	memory__EOF, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n In memory_Malloc:"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n Memory Error."
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" Terminated by user given memory restriction,\n"
	.size	.L.str.2, 47

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n while trying to allocate %lu bytes.\n"
	.size	.L.str.3, 39

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n Maximum amount of memory"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" left for allocation is %l bytes.\n"
	.size	.L.str.5, 35

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n In memory_MallocIntern:"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n Memory Error. Out of memory."
	.size	.L.str.7, 31

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n Failed to allocate %d bytes.\n"
	.size	.L.str.8, 32

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" Terminated by user given memory restriction.\n"
	.size	.L.str.9, 47

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" Terminated, ran out of system memory.\n"
	.size	.L.str.10, 40

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n In memory_Calloc:"
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n Memory Error. Out of memory.\n"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n###\n"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"### Pagesize: %d\n"
	.size	.L.str.14, 18

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"### Marksize: %d\n"
	.size	.L.str.15, 18

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"### Memory demanded:  %lu KBytes\n"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"### Memory freed:     %lu KBytes\n"
	.size	.L.str.17, 34

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"### Memory remaining: %lu Bytes\n"
	.size	.L.str.18, 33

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"### Pages allocated:  %d Pages\n"
	.size	.L.str.19, 32

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"###\n"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\n In memory_PrintAllocatedBlocks:"
	.size	.L.str.21, 34

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\n Parameter size is too big: %d."
	.size	.L.str.22, 33

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\n Maximal allowed value is: %d.\n"
	.size	.L.str.23, 33

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"   No request so far"
	.size	.L.str.24, 21

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.25, 31

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/memory.c"
	.size	.L.str.26, 78

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\n Memory Error. No Page entry but Next entry.\n"
	.size	.L.str.27, 47

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.28, 133

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\n In memory_PrintFreedBlocks."
	.size	.L.str.29, 30

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n Parameter Size is too big: %d."
	.size	.L.str.30, 33

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\n\n   No freed memory"
	.size	.L.str.31, 21

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\n\n   Free: "
	.size	.L.str.32, 12

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\n\nEOF Pointer: %p\n"
	.size	.L.str.33, 19

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\n\n Entry: %d aligned size: %d total size: %d\n"
	.size	.L.str.34, 46

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"\n In memory_PrintDetailed:"
	.size	.L.str.35, 27

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"\n\n   Page: %p Next Page: %p\n"
	.size	.L.str.36, 29

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"   Data: "
	.size	.L.str.37, 10

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\n\t\t"
	.size	.L.str.38, 4

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%p "
	.size	.L.str.39, 4

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\n\n"
	.size	.L.str.40, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
