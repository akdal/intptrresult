	.text
	.file	"nbench0.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_1:
	.quad	5                       # 0x5
	.quad	3000                    # 0xbb8
.LCPI0_2:
	.quad	5                       # 0x5
	.quad	4000                    # 0xfa0
.LCPI0_3:
	.quad	5                       # 0x5
	.quad	5000                    # 0x1388
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	$60, global_min_ticks(%rip)
	movq	$5, global_min_seconds(%rip)
	movl	$0, global_allstats(%rip)
	movl	$0, global_custrun(%rip)
	movl	$8, global_align(%rip)
	movl	$0, write_to_file(%rip)
	movl	$0, mem_array_ents(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, tests_to_do(%rip)
	movaps	%xmm0, tests_to_do+16(%rip)
	movl	$1, tests_to_do+32(%rip)
	movl	$1, tests_to_do+36(%rip)
	movq	$5, global_numsortstruct+8(%rip)
	movq	$5, global_strsortstruct+8(%rip)
	movq	$5, global_bitopstruct+8(%rip)
	movq	$5, global_fourierstruct+8(%rip)
	movq	$5, global_assignstruct+8(%rip)
	movq	$5, global_nnetstruct+8(%rip)
	movq	$5, global_lustruct+8(%rip)
	movl	$0, global_numsortstruct(%rip)
	movq	$8111, global_numsortstruct+32(%rip) # imm = 0x1FAF
	movl	$0, global_strsortstruct(%rip)
	movq	$8111, global_strsortstruct+32(%rip) # imm = 0x1FAF
	movl	$0, global_bitopstruct(%rip)
	movq	$32768, global_bitopstruct+32(%rip) # imm = 0x8000
	movl	$0, global_emfloatstruct(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [5,3000]
	movups	%xmm0, global_emfloatstruct+8(%rip)
	movl	$0, global_fourierstruct(%rip)
	movl	$0, global_assignstruct(%rip)
	movl	$0, global_ideastruct(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [5,4000]
	movups	%xmm0, global_ideastruct+8(%rip)
	movl	$0, global_huffstruct(%rip)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [5,5000]
	movdqu	%xmm0, global_huffstruct+8(%rip)
	movl	$0, global_nnetstruct(%rip)
	movl	$0, global_lustruct(%rip)
	cmpl	$2, %edi
	jl	.LBB0_100
# BB#1:                                 # %.lr.ph
	movslq	%edi, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r14d
	leaq	32(%rsp), %r12
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_87 Depth 2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_96 Depth 3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %r13
	cmpb	$45, (%r13)
	jne	.LBB0_190
# BB#3:                                 # %.preheader.i
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	1(%r13), %r15
	movq	%r15, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB0_8
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%r15,%rbp), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_7:                                # %toupper.exit.i
                                        #   in Loop: Header=BB0_5 Depth=2
	movb	%bl, (%r15,%rbp)
	incq	%rbp
	movq	%r15, %rdi
	callq	strlen
	cmpq	%rax, %rbp
	jb	.LBB0_5
.LBB0_8:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movsbl	1(%r13), %eax
	cmpl	$67, %eax
	je	.LBB0_11
# BB#9:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$86, %eax
	jne	.LBB0_190
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, global_allstats(%rip)
	jmp	.LBB0_99
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r13
	movl	$.L.str.79, %esi
	movq	%r13, %rdi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_87
	jmp	.LBB0_189
	.p2align	4, 0x90
.LBB0_12:                               # %strtoupper.exit.i.i._crit_edge
                                        #   in Loop: Header=BB0_87 Depth=2
	decl	%ebp
	testl	%ebp, %ebp
	js	.LBB0_17
# BB#13:                                #   in Loop: Header=BB0_87 Depth=2
	cmpl	$41, %ebp
	ja	.LBB0_87
# BB#14:                                #   in Loop: Header=BB0_87 Depth=2
	movl	%ebp, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_15:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_min_ticks(%rip)
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_87 Depth=2
	movl	$.L.str.81, %edi
	jmp	.LBB0_86
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_87 Depth=2
	movl	$.L.str.82, %edi
	jmp	.LBB0_86
.LBB0_18:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_min_seconds(%rip)
	movq	%rax, global_numsortstruct+8(%rip)
	movq	%rax, global_strsortstruct+8(%rip)
	movq	%rax, global_bitopstruct+8(%rip)
	movq	%rax, global_emfloatstruct+8(%rip)
	movq	%rax, global_fourierstruct+8(%rip)
	movq	%rax, global_assignstruct+8(%rip)
	movq	%rax, global_ideastruct+8(%rip)
	movq	%rax, global_huffstruct+8(%rip)
	movq	%rax, global_nnetstruct+8(%rip)
	jmp	.LBB0_84
.LBB0_19:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_21:                               # %getflag.exit.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, global_allstats(%rip)
	jmp	.LBB0_87
.LBB0_22:                               #   in Loop: Header=BB0_87 Depth=2
	movl	$global_ofile_name, %edi
	movq	%r13, %rsi
	callq	strcpy
	movl	$global_ofile_name, %edi
	movl	$.L.str.83, %esi
	callq	fopen
	movq	%rax, global_ofile(%rip)
	testq	%rax, %rax
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_87 Depth=2
	movl	$.L.str.84, %edi
	movl	$global_ofile_name, %esi
	xorl	%eax, %eax
	callq	printf
	callq	ErrorExit
.LBB0_24:                               #   in Loop: Header=BB0_87 Depth=2
	movl	$-1, write_to_file(%rip)
	jmp	.LBB0_87
.LBB0_25:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_27:                               # %getflag.exit74.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, global_custrun(%rip)
	movd	%eax, %xmm0
	xorl	$1, %eax
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pxor	.LCPI0_0(%rip), %xmm0
	movdqa	%xmm0, tests_to_do(%rip)
	movdqa	%xmm0, tests_to_do+16(%rip)
	movl	%eax, tests_to_do+32(%rip)
	movl	%eax, tests_to_do+36(%rip)
	jmp	.LBB0_87
.LBB0_28:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_30:                               # %getflag.exit78.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do(%rip)
	jmp	.LBB0_87
.LBB0_31:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movw	%ax, global_numsortstruct+24(%rip)
	movl	$1, global_numsortstruct(%rip)
	jmp	.LBB0_87
.LBB0_32:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_numsortstruct+32(%rip)
	jmp	.LBB0_87
.LBB0_33:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_numsortstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_34:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_36:                               # %getflag.exit82.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+4(%rip)
	jmp	.LBB0_87
.LBB0_37:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_strsortstruct+32(%rip)
	jmp	.LBB0_87
.LBB0_38:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movw	%ax, global_strsortstruct+24(%rip)
	movl	$1, global_strsortstruct(%rip)
	jmp	.LBB0_87
.LBB0_39:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_strsortstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_40:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_42:                               # %getflag.exit86.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+8(%rip)
	jmp	.LBB0_87
.LBB0_43:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_bitopstruct+24(%rip)
	movl	$1, global_bitopstruct(%rip)
	jmp	.LBB0_87
.LBB0_44:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_bitopstruct+32(%rip)
	jmp	.LBB0_87
.LBB0_45:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_bitopstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_46:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_48:                               # %getflag.exit90.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+12(%rip)
	jmp	.LBB0_87
.LBB0_49:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_emfloatstruct+16(%rip)
	jmp	.LBB0_87
.LBB0_50:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_emfloatstruct+24(%rip)
	jmp	.LBB0_87
.LBB0_51:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_emfloatstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_52:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_54:                               # %getflag.exit94.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+16(%rip)
	jmp	.LBB0_87
.LBB0_55:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_fourierstruct+16(%rip)
	movl	$1, global_fourierstruct(%rip)
	jmp	.LBB0_87
.LBB0_56:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_fourierstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_57:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_59:                               # %getflag.exit98.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+20(%rip)
	jmp	.LBB0_87
.LBB0_60:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_assignstruct+16(%rip)
	jmp	.LBB0_87
.LBB0_61:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_assignstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_62:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_64:                               # %getflag.exit102.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+24(%rip)
	jmp	.LBB0_87
.LBB0_65:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_ideastruct+16(%rip)
	jmp	.LBB0_87
.LBB0_66:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_ideastruct+24(%rip)
	jmp	.LBB0_87
.LBB0_67:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_ideastruct+8(%rip)
	jmp	.LBB0_87
.LBB0_68:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_70:                               # %getflag.exit106.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+28(%rip)
	jmp	.LBB0_87
.LBB0_71:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_huffstruct+16(%rip)
	jmp	.LBB0_87
.LBB0_72:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_huffstruct+24(%rip)
	movl	$1, global_huffstruct(%rip)
	jmp	.LBB0_87
.LBB0_73:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_huffstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_74:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_76:                               # %getflag.exit110.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+32(%rip)
	jmp	.LBB0_87
.LBB0_77:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_nnetstruct+16(%rip)
	movl	$1, global_nnetstruct(%rip)
	jmp	.LBB0_87
.LBB0_78:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_nnetstruct+8(%rip)
	jmp	.LBB0_87
.LBB0_79:                               #   in Loop: Header=BB0_87 Depth=2
	movsbl	(%r13), %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_87 Depth=2
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_81:                               # %getflag.exit114.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	cmpl	$84, %ebx
	sete	%al
	movl	%eax, tests_to_do+36(%rip)
	jmp	.LBB0_87
.LBB0_82:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, global_lustruct+16(%rip)
	movl	$1, global_lustruct(%rip)
	jmp	.LBB0_87
.LBB0_83:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
.LBB0_84:                               #   in Loop: Header=BB0_87 Depth=2
	movq	%rax, global_lustruct+8(%rip)
	jmp	.LBB0_87
.LBB0_85:                               #   in Loop: Header=BB0_87 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movl	%eax, global_align(%rip)
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_86:                               # %.backedge.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
.LBB0_87:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_96 Depth 3
	movl	$39, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_98
# BB#88:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	movq	%r12, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_87 Depth=2
	movb	$0, 31(%rsp,%rax)
.LBB0_90:                               #   in Loop: Header=BB0_87 Depth=2
	movl	$61, %esi
	movq	%r12, %rdi
	callq	strchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_16
# BB#91:                                #   in Loop: Header=BB0_87 Depth=2
	movb	$0, (%r13)
	movb	32(%rsp), %al
	leaq	33(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_92:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbl	%al, %ebx
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_92 Depth=3
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %ebx
.LBB0_94:                               # %toupper.exit.i.i.i
                                        #   in Loop: Header=BB0_92 Depth=3
	movb	%bl, -1(%rbp)
	movzbl	(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB0_92
# BB#95:                                # %strtoupper.exit.preheader.i.i
                                        #   in Loop: Header=BB0_87 Depth=2
	incq	%r13
	movl	$42, %ebp
	.p2align	4, 0x90
.LBB0_96:                               # %strtoupper.exit.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	paramnames-8(,%rbp,8), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_12
# BB#97:                                #   in Loop: Header=BB0_96 Depth=3
	decq	%rbp
	jg	.LBB0_96
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_98:                               # %read_comfile.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	fclose
.LBB0_99:                               # %parse_arg.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB0_2
.LBB0_100:                              # %.loopexit
	movl	$.L.str.88, %edi
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_102
# BB#101:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.55, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_102:                              # %output_string.exit82
	movl	$.L.str.88, %edi
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_104
# BB#103:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.56, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_104:                              # %output_string.exit84
	movl	$.L.str.88, %edi
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_106
# BB#105:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.56, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_106:                              # %output_string.exit86
	movl	$.L.str.88, %edi
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_108
# BB#107:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.57, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_108:                              # %output_string.exit88
	movl	$.L.str.88, %edi
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_110
# BB#109:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_110:                              # %output_string.exit90
	movl	$.L.str.88, %edi
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_112
# BB#111:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_112:                              # %output_string.exit92
	movl	$.L.str.88, %edi
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_114
# BB#113:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.59, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_114:                              # %output_string.exit94
	movl	$.L.str.88, %edi
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_116
# BB#115:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_116:                              # %output_string.exit96
	cmpl	$0, global_allstats(%rip)
	je	.LBB0_133
# BB#117:
	movl	$10, %edi
	callq	putchar
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_119
# BB#118:
	movq	global_ofile(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB0_119:                              # %output_string.exit98
	movl	$.L.str.88, %edi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_121
# BB#120:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.62, %edi
	movl	$78, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_121:                              # %output_string.exit100
	leaq	24(%rsp), %rbx
	movq	%rbx, %rdi
	callq	time
	movq	%rbx, %rdi
	callq	localtime
	movq	%rax, %rdi
	callq	asctime
	movq	%rax, %rcx
	movl	$buffer, %edi
	movl	$.L.str.63, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_123
# BB#122:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_123:                              # %output_string.exit102
	movl	$buffer, %edi
	movl	$.L.str.64, %esi
	movl	$1, %edx
	movl	$2, %ecx
	movl	$4, %r8d
	movl	$8, %r9d
	movl	$0, %eax
	pushq	$4
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	sprintf
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_125
# BB#124:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_125:                              # %output_string.exit104
	movq	sysname(%rip), %rdx
	movl	$buffer, %edi
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_127
# BB#126:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_127:                              # %output_string.exit106
	movq	compilername(%rip), %rdx
	movl	$buffer, %edi
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_129
# BB#128:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_129:                              # %output_string.exit108
	movq	compilerversion(%rip), %rdx
	movl	$buffer, %edi
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_131
# BB#130:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_131:                              # %output_string.exit110
	movl	$.L.str.88, %edi
	movl	$.L.str.66, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_133
# BB#132:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.66, %edi
	movl	$78, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_133:                              # %output_string.exit112
	movl	$.L.str.88, %edi
	movl	$.L.str.67, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_135
# BB#134:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.67, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_135:                              # %output_string.exit114.preheader
	leaq	32(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_136:                              # =>This Inner Loop Header: Depth=1
	cmpl	$0, tests_to_do(,%rbp,4)
	je	.LBB0_176
# BB#137:                               #   in Loop: Header=BB0_136 Depth=1
	movq	ftestnames(,%rbp,8), %rdx
	movl	$buffer, %edi
	movl	$.L.str.68, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_139
# BB#138:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_139:                              # %output_string.exit116.preheader
                                        #   in Loop: Header=BB0_136 Depth=1
	callq	*funcpointer(,%rbp,8)
	movl	$buffer, %edi
	movl	$.L.str.69, %esi
	pxor	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movb	$2, %al
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_141
# BB#140:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_141:                              # %output_string.exit118
                                        #   in Loop: Header=BB0_136 Depth=1
	cmpl	$0, global_allstats(%rip)
	je	.LBB0_176
# BB#142:                               #   in Loop: Header=BB0_136 Depth=1
	movl	$buffer, %edi
	movl	$.L.str.70, %esi
	movb	$1, %al
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_144
# BB#143:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_144:                              # %output_string.exit120
                                        #   in Loop: Header=BB0_136 Depth=1
	movl	$buffer, %edi
	movl	$.L.str.72, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_146
# BB#145:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_146:                              # %output_string.exit122
                                        #   in Loop: Header=BB0_136 Depth=1
	cmpl	$9, %ebp
	ja	.LBB0_174
# BB#147:                               # %output_string.exit122
                                        #   in Loop: Header=BB0_136 Depth=1
	movl	%ebp, %eax
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_148:                              #   in Loop: Header=BB0_136 Depth=1
	movzwl	global_numsortstruct+24(%rip), %edx
	movl	$.L.str.89, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_150
# BB#149:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_150:                              # %output_string.exit.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_numsortstruct+32(%rip), %rdx
	movl	$.L.str.90, %esi
	jmp	.LBB0_172
.LBB0_151:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_fourierstruct+16(%rip), %rdx
	movl	$.L.str.95, %esi
	jmp	.LBB0_172
.LBB0_152:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_lustruct+16(%rip), %rdx
	jmp	.LBB0_166
.LBB0_153:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_bitopstruct+24(%rip), %rdx
	movl	$.L.str.91, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_155:                              # %output_string.exit8.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_bitopstruct+32(%rip), %rdx
	movl	$.L.str.92, %esi
	jmp	.LBB0_172
.LBB0_156:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_emfloatstruct+24(%rip), %rdx
	movl	$.L.str.93, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_158
# BB#157:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_158:                              # %output_string.exit12.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_emfloatstruct+16(%rip), %rdx
	movl	$.L.str.94, %esi
	jmp	.LBB0_172
.LBB0_159:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_huffstruct+16(%rip), %rdx
	movl	$.L.str.94, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_161
# BB#160:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_161:                              # %output_string.exit24.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_huffstruct+24(%rip), %rdx
	jmp	.LBB0_171
.LBB0_162:                              #   in Loop: Header=BB0_136 Depth=1
	movzwl	global_strsortstruct+24(%rip), %edx
	movl	$.L.str.89, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_164
# BB#163:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_164:                              # %output_string.exit4.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_strsortstruct+32(%rip), %rdx
	movl	$.L.str.90, %esi
	jmp	.LBB0_172
.LBB0_165:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_assignstruct+16(%rip), %rdx
.LBB0_166:                              #   in Loop: Header=BB0_136 Depth=1
	movl	$.L.str.96, %esi
	jmp	.LBB0_172
.LBB0_167:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_ideastruct+16(%rip), %rdx
	movl	$.L.str.94, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_169
# BB#168:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_169:                              # %output_string.exit20.i
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	global_ideastruct+24(%rip), %rdx
	movl	$.L.str.97, %esi
	jmp	.LBB0_172
.LBB0_170:                              #   in Loop: Header=BB0_136 Depth=1
	movq	global_nnetstruct+16(%rip), %rdx
.LBB0_171:                              #   in Loop: Header=BB0_136 Depth=1
	movl	$.L.str.93, %esi
	.p2align	4, 0x90
.LBB0_172:                              #   in Loop: Header=BB0_136 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_174
# BB#173:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
.LBB0_174:                              # %show_stats.exit
                                        #   in Loop: Header=BB0_136 Depth=1
	movq	ftestnames(,%rbp,8), %rdx
	movl	$buffer, %edi
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_176
# BB#175:                               #   in Loop: Header=BB0_136 Depth=1
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_176:                              # %output_string.exit124
                                        #   in Loop: Header=BB0_136 Depth=1
	incq	%rbp
	cmpq	$10, %rbp
	jne	.LBB0_136
# BB#177:
	cmpl	$0, global_custrun(%rip)
	jne	.LBB0_188
# BB#178:
	movl	$.L.str.88, %edi
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_180
# BB#179:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$78, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_180:                              # %output_string.exit126
	movl	$buffer, %edi
	movl	$.L.str.75, %esi
	pxor	%xmm0, %xmm0
	movb	$1, %al
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_182
# BB#181:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_182:                              # %output_string.exit128
	movl	$buffer, %edi
	movl	$.L.str.76, %esi
	pxor	%xmm0, %xmm0
	movb	$1, %al
	callq	sprintf
	movl	$.L.str.88, %edi
	movl	$buffer, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_184
# BB#183:
	movq	global_ofile(%rip), %rsi
	movl	$buffer, %edi
	callq	fputs
.LBB0_184:                              # %output_string.exit130
	movl	$.L.str.88, %edi
	movl	$.L.str.77, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_186
# BB#185:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.77, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_186:                              # %output_string.exit132
	movl	$.L.str.88, %edi
	movl	$.L.str.78, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, write_to_file(%rip)
	je	.LBB0_188
# BB#187:
	movq	global_ofile(%rip), %rcx
	movl	$.L.str.78, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_188:                              # %output_string.exit
	xorl	%edi, %edi
	callq	exit
.LBB0_189:
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	printf
.LBB0_190:                              # %.loopexit133
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rsi
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_15
	.quad	.LBB0_18
	.quad	.LBB0_19
	.quad	.LBB0_22
	.quad	.LBB0_25
	.quad	.LBB0_28
	.quad	.LBB0_31
	.quad	.LBB0_32
	.quad	.LBB0_33
	.quad	.LBB0_34
	.quad	.LBB0_37
	.quad	.LBB0_38
	.quad	.LBB0_39
	.quad	.LBB0_40
	.quad	.LBB0_43
	.quad	.LBB0_44
	.quad	.LBB0_45
	.quad	.LBB0_46
	.quad	.LBB0_49
	.quad	.LBB0_50
	.quad	.LBB0_51
	.quad	.LBB0_52
	.quad	.LBB0_55
	.quad	.LBB0_56
	.quad	.LBB0_57
	.quad	.LBB0_60
	.quad	.LBB0_61
	.quad	.LBB0_62
	.quad	.LBB0_65
	.quad	.LBB0_66
	.quad	.LBB0_67
	.quad	.LBB0_68
	.quad	.LBB0_71
	.quad	.LBB0_72
	.quad	.LBB0_73
	.quad	.LBB0_74
	.quad	.LBB0_77
	.quad	.LBB0_78
	.quad	.LBB0_79
	.quad	.LBB0_82
	.quad	.LBB0_83
	.quad	.LBB0_85
.LJTI0_1:
	.quad	.LBB0_148
	.quad	.LBB0_162
	.quad	.LBB0_153
	.quad	.LBB0_156
	.quad	.LBB0_151
	.quad	.LBB0_165
	.quad	.LBB0_167
	.quad	.LBB0_159
	.quad	.LBB0_170
	.quad	.LBB0_152

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"You can enter your system description in nbench0.h"
	.size	.L.str, 51

	.type	sysname,@object         # @sysname
	.data
	.globl	sysname
	.p2align	3
sysname:
	.quad	.L.str
	.size	sysname, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"It then will be printed here after you recompile"
	.size	.L.str.1, 49

	.type	compilername,@object    # @compilername
	.data
	.globl	compilername
	.p2align	3
compilername:
	.quad	.L.str.1
	.size	compilername, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Have a nice day"
	.size	.L.str.2, 16

	.type	compilerversion,@object # @compilerversion
	.data
	.globl	compilerversion
	.p2align	3
compilerversion:
	.quad	.L.str.2
	.size	compilerversion, 8

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"NUMERIC SORT    "
	.size	.L.str.3, 17

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"STRING SORT     "
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"BITFIELD        "
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"FP EMULATION    "
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FOURIER         "
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ASSIGNMENT      "
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"IDEA            "
	.size	.L.str.9, 17

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"HUFFMAN         "
	.size	.L.str.10, 17

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"NEURAL NET      "
	.size	.L.str.11, 17

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"LU DECOMPOSITION"
	.size	.L.str.12, 17

	.type	ftestnames,@object      # @ftestnames
	.data
	.globl	ftestnames
	.p2align	4
ftestnames:
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.size	ftestnames, 80

	.type	bindex,@object          # @bindex
	.globl	bindex
	.p2align	4
bindex:
	.quad	4630684594192938697     # double 38.993000000000002
	.quad	4612221946783044993     # double 2.238
	.quad	4708017607981137920     # double 5829704
	.quad	4611875169611737465     # double 2.0840000000000001
	.quad	4650945426899362382     # double 879.27800000000002
	.quad	4598405803846197785     # double 0.26279999999999998
	.quad	4634301266168693916     # double 65.382000000000005
	.quad	4630272092614569230     # double 36.061999999999998
	.quad	4603782201081352684     # double 0.62250000000000005
	.quad	4626126557305072360     # double 19.303100000000001
	.size	bindex, 80

	.type	lx_bindex,@object       # @lx_bindex
	.globl	lx_bindex
	.p2align	4
lx_bindex:
	.quad	4638055297933083935     # double 118.73
	.quad	4624329311431277150     # double 14.459
	.quad	4718257243806498816     # double 2.791E+7
	.quad	4621273844264087637     # double 9.0313999999999997
	.quad	4654599957259485184     # double 1565.5
	.quad	4607241866315098699     # double 1.0132000000000001
	.quad	4641951967141922079     # double 220.21000000000001
	.quad	4637647159216853484     # double 112.93000000000001
	.quad	4609343696261192509     # double 1.4799
	.quad	4628217606759558152     # double 26.731999999999999
	.size	lx_bindex, 80

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"GLOBALMINTICKS"
	.size	.L.str.13, 15

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"MINSECONDS"
	.size	.L.str.14, 11

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"ALLSTATS"
	.size	.L.str.15, 9

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"OUTFILE"
	.size	.L.str.16, 8

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"CUSTOMRUN"
	.size	.L.str.17, 10

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"DONUMSORT"
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"NUMNUMARRAYS"
	.size	.L.str.19, 13

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"NUMARRAYSIZE"
	.size	.L.str.20, 13

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"NUMMINSECONDS"
	.size	.L.str.21, 14

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"DOSTRINGSORT"
	.size	.L.str.22, 13

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"STRARRAYSIZE"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"NUMSTRARRAYS"
	.size	.L.str.24, 13

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"STRMINSECONDS"
	.size	.L.str.25, 14

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"DOBITFIELD"
	.size	.L.str.26, 11

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"NUMBITOPS"
	.size	.L.str.27, 10

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"BITFIELDSIZE"
	.size	.L.str.28, 13

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"BITMINSECONDS"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"DOEMF"
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"EMFARRAYSIZE"
	.size	.L.str.31, 13

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"EMFLOOPS"
	.size	.L.str.32, 9

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"EMFMINSECONDS"
	.size	.L.str.33, 14

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"DOFOUR"
	.size	.L.str.34, 7

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"FOURSIZE"
	.size	.L.str.35, 9

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"FOURMINSECONDS"
	.size	.L.str.36, 15

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"DOASSIGN"
	.size	.L.str.37, 9

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"ASSIGNARRAYS"
	.size	.L.str.38, 13

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"ASSIGNMINSECONDS"
	.size	.L.str.39, 17

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"DOIDEA"
	.size	.L.str.40, 7

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"IDEARRAYSIZE"
	.size	.L.str.41, 13

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"IDEALOOPS"
	.size	.L.str.42, 10

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"IDEAMINSECONDS"
	.size	.L.str.43, 15

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"DOHUFF"
	.size	.L.str.44, 7

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"HUFARRAYSIZE"
	.size	.L.str.45, 13

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"HUFFLOOPS"
	.size	.L.str.46, 10

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"HUFFMINSECONDS"
	.size	.L.str.47, 15

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"DONNET"
	.size	.L.str.48, 7

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"NNETLOOPS"
	.size	.L.str.49, 10

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"NNETMINSECONDS"
	.size	.L.str.50, 15

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"DOLU"
	.size	.L.str.51, 5

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"LUNUMARRAYS"
	.size	.L.str.52, 12

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"LUMINSECONDS"
	.size	.L.str.53, 13

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"ALIGN"
	.size	.L.str.54, 6

	.type	paramnames,@object      # @paramnames
	.data
	.globl	paramnames
	.p2align	4
paramnames:
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.51
	.quad	.L.str.52
	.quad	.L.str.53
	.quad	.L.str.54
	.size	paramnames, 336

	.type	global_numsortstruct,@object # @global_numsortstruct
	.comm	global_numsortstruct,40,8
	.type	global_strsortstruct,@object # @global_strsortstruct
	.comm	global_strsortstruct,40,8
	.type	global_bitopstruct,@object # @global_bitopstruct
	.comm	global_bitopstruct,40,8
	.type	global_emfloatstruct,@object # @global_emfloatstruct
	.comm	global_emfloatstruct,40,8
	.type	global_fourierstruct,@object # @global_fourierstruct
	.comm	global_fourierstruct,32,8
	.type	global_assignstruct,@object # @global_assignstruct
	.comm	global_assignstruct,32,8
	.type	global_ideastruct,@object # @global_ideastruct
	.comm	global_ideastruct,40,8
	.type	global_huffstruct,@object # @global_huffstruct
	.comm	global_huffstruct,40,8
	.type	global_nnetstruct,@object # @global_nnetstruct
	.comm	global_nnetstruct,32,8
	.type	global_lustruct,@object # @global_lustruct
	.comm	global_lustruct,32,8
	.type	global_fstruct,@object  # @global_fstruct
	.globl	global_fstruct
	.p2align	4
global_fstruct:
	.quad	global_numsortstruct
	.quad	global_strsortstruct
	.quad	global_bitopstruct
	.quad	global_emfloatstruct
	.quad	global_fourierstruct
	.quad	global_assignstruct
	.quad	global_ideastruct
	.quad	global_huffstruct
	.quad	global_nnetstruct
	.quad	global_lustruct
	.size	global_fstruct, 80

	.type	funcpointer,@object     # @funcpointer
	.globl	funcpointer
	.p2align	4
funcpointer:
	.quad	DoNumSort
	.quad	DoStringSort
	.quad	DoBitops
	.quad	DoEmFloat
	.quad	DoFourier
	.quad	DoAssign
	.quad	DoIDEA
	.quad	DoHuffman
	.quad	DoNNET
	.quad	DoLU
	.size	funcpointer, 80

	.type	global_min_ticks,@object # @global_min_ticks
	.comm	global_min_ticks,8,8
	.type	global_min_seconds,@object # @global_min_seconds
	.comm	global_min_seconds,8,8
	.type	global_allstats,@object # @global_allstats
	.comm	global_allstats,4,4
	.type	global_custrun,@object  # @global_custrun
	.comm	global_custrun,4,4
	.type	global_align,@object    # @global_align
	.comm	global_align,4,4
	.type	write_to_file,@object   # @write_to_file
	.comm	write_to_file,4,4
	.type	mem_array_ents,@object  # @mem_array_ents
	.comm	mem_array_ents,4,4
	.type	tests_to_do,@object     # @tests_to_do
	.comm	tests_to_do,40,16
	.type	.L.str.55,@object       # @.str.55
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.55:
	.asciz	"BBBBBB   YYY   Y  TTTTTTT  EEEEEEE\n"
	.size	.L.str.55, 36

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"BBB   B  YYY   Y    TTT    EEE\n"
	.size	.L.str.56, 32

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"BBBBBB    YYY Y     TTT    EEEEEEE\n"
	.size	.L.str.57, 36

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"BBB   B    YYY      TTT    EEE\n"
	.size	.L.str.58, 32

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"BBBBBB     YYY      TTT    EEEEEEE\n\n"
	.size	.L.str.59, 37

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"\nBYTEmark (tm) Native Mode Benchmark ver. 2 (10/95)\n"
	.size	.L.str.60, 53

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"============================== ALL STATISTICS ===============================\n"
	.size	.L.str.62, 79

	.type	buffer,@object          # @buffer
	.comm	buffer,1024,16
	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"**Date and time of benchmark run: %s"
	.size	.L.str.63, 37

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"**Sizeof: char:%u short:%u int:%u long:%u u8:%u u16:%u u32:%u int32:%u\n"
	.size	.L.str.64, 72

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"**%s\n"
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"=============================================================================\n"
	.size	.L.str.66, 79

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"\nNOTE!!! Iteration display disabled to prevent diffs from failing!\n"
	.size	.L.str.67, 68

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%s    :"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"  Iterations/sec.: %13.2f  Index: %6.2f\n"
	.size	.L.str.69, 41

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"  Absolute standard deviation: %g\n"
	.size	.L.str.70, 35

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"  Number of runs: %lu\n"
	.size	.L.str.72, 23

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Done with %s\n\n"
	.size	.L.str.73, 15

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"==========================ORIGINAL BYTEMARK RESULTS==========================\n"
	.size	.L.str.74, 79

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"INTEGER INDEX       : %.3f\n"
	.size	.L.str.75, 28

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"FLOATING-POINT INDEX: %.3f\n"
	.size	.L.str.76, 28

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Baseline (MSDOS*)   : Pentium* 90, 256 KB L2-cache, Watcom* compiler 10.0\n"
	.size	.L.str.77, 75

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"* Trademarks are property of their respective holder.\n"
	.size	.L.str.78, 55

	.type	global_ofile_name,@object # @global_ofile_name
	.comm	global_ofile_name,1024,16
	.type	global_ofile,@object    # @global_ofile
	.comm	global_ofile,8,8
	.type	mem_array,@object       # @mem_array
	.comm	mem_array,320,16
	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"r"
	.size	.L.str.79, 2

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"**Error opening file: %s\n"
	.size	.L.str.80, 26

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"**COMMAND FILE ERROR at LINE:\n %s\n"
	.size	.L.str.81, 35

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"**COMMAND FILE ERROR -- UNKNOWN PARAM: %s"
	.size	.L.str.82, 42

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"a"
	.size	.L.str.83, 2

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"**Error opening output file: %s\n"
	.size	.L.str.84, 33

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"Usage: %s [-v] [-c<FILE>]\n"
	.size	.L.str.85, 27

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"%s"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"  Number of arrays: %d\n"
	.size	.L.str.89, 24

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"  Array size: %ld\n"
	.size	.L.str.90, 19

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"  Operations array size: %ld\n"
	.size	.L.str.91, 30

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"  Bitfield array size: %ld\n"
	.size	.L.str.92, 28

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"  Number of loops: %lu\n"
	.size	.L.str.93, 24

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"  Array size: %lu\n"
	.size	.L.str.94, 19

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"  Number of coefficients: %lu\n"
	.size	.L.str.95, 31

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"  Number of arrays: %lu\n"
	.size	.L.str.96, 25

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	" Number of loops: %lu\n"
	.size	.L.str.97, 23

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	" -v = verbose"
	.size	.Lstr, 14

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	" -c = input parameters thru command file <FILE>"
	.size	.Lstr.1, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
