	.text
	.file	"Materials.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	27                      # 0x1b
.LCPI0_1:
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	21                      # 0x15
	.text
	.globl	load_num_nucs
	.p2align	4, 0x90
	.type	load_num_nucs,@function
load_num_nucs:                          # @load_num_nucs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$48, %edi
	callq	malloc
	cmpq	$68, %rbx
	movl	$34, %ecx
	movl	$321, %edx              # imm = 0x141
	cmovel	%ecx, %edx
	movl	%edx, (%rax)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [5,4,4,27]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [21,21,21,21]
	movups	%xmm0, 20(%rax)
	movabsq	$38654705685, %rcx      # imm = 0x900000015
	movq	%rcx, 36(%rax)
	movl	$9, 44(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	load_num_nucs, .Lfunc_end0-load_num_nucs
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI1_1:
	.long	68                      # 0x44
	.long	68                      # 0x44
	.long	68                      # 0x44
	.long	68                      # 0x44
.LCPI1_2:
	.long	72                      # 0x48
	.long	72                      # 0x48
	.long	72                      # 0x48
	.long	72                      # 0x48
.LCPI1_3:
	.long	76                      # 0x4c
	.long	76                      # 0x4c
	.long	76                      # 0x4c
	.long	76                      # 0x4c
.LCPI1_4:
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	80                      # 0x50
.LCPI1_5:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
	.text
	.globl	load_mats
	.p2align	4, 0x90
	.type	load_mats,@function
load_mats:                              # @load_mats
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$1448, %rsp             # imm = 0x5A8
.Lcfi8:
	.cfi_def_cfa_offset 1504
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	movl	$96, %edi
	callq	malloc
	movq	%rax, %rbx
	movslq	(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, (%rbx)
	movslq	4(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	callq	malloc
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rax, 8(%rbx)
	movslq	8(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	callq	malloc
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, 16(%rbx)
	movslq	12(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	callq	malloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rbx)
	movslq	16(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	callq	malloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rbx)
	movslq	20(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rbx)
	movslq	24(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, 48(%rbx)
	movslq	28(%rbp), %rdi
	shlq	$2, %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rbx)
	movslq	32(%rbp), %r13
	shlq	$2, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 64(%rbx)
	movslq	36(%rbp), %r12
	shlq	$2, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rbx)
	movslq	40(%rbp), %r14
	shlq	$2, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rbx)
	movslq	44(%rbp), %r15
	shlq	$2, %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, 88(%rbx)
	leaq	160(%rsp), %rdi
	movl	$.Lload_mats.mats0_Lrg, %esi
	movl	$1284, %edx             # imm = 0x504
	callq	memcpy
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI1_0(%rip), %xmm1   # xmm1 = [2,3]
	movl	$46, %eax
	movdqa	.LCPI1_1(%rip), %xmm8   # xmm8 = [68,68,68,68]
	movdqa	.LCPI1_2(%rip), %xmm3   # xmm3 = [72,72,72,72]
	movdqa	.LCPI1_3(%rip), %xmm9   # xmm9 = [76,76,76,76]
	movdqa	.LCPI1_4(%rip), %xmm5   # xmm5 = [80,80,80,80]
	movdqa	.LCPI1_5(%rip), %xmm6   # xmm6 = [16,16]
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_6:                                # %vector.body.1
                                        #   in Loop: Header=BB1_1 Depth=1
	movaps	%xmm7, %xmm2
	paddd	%xmm9, %xmm2
	paddd	%xmm5, %xmm7
	movdqu	%xmm2, 144(%rsp,%rax,4)
	movdqu	%xmm7, 160(%rsp,%rax,4)
	paddq	%xmm6, %xmm0
	paddq	%xmm6, %xmm1
	addq	$16, %rax
.LBB1_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm7
	shufps	$136, %xmm1, %xmm7      # xmm7 = xmm7[0,2],xmm1[0,2]
	movaps	%xmm7, %xmm2
	paddd	%xmm8, %xmm2
	movaps	%xmm7, %xmm4
	paddd	%xmm3, %xmm4
	movdqu	%xmm2, 112(%rsp,%rax,4)
	movdqu	%xmm4, 128(%rsp,%rax,4)
	cmpq	$318, %rax              # imm = 0x13E
	jne	.LBB1_6
# BB#2:                                 # %scalar.ph
	movabsq	$1498943586652, %rax    # imm = 0x15D0000015C
	movq	%rax, 1416(%rsp)
	movabsq	$1507533521246, %rax    # imm = 0x15F0000015E
	movq	%rax, 1424(%rsp)
	movabsq	$1516123455840, %rax    # imm = 0x16100000160
	movq	%rax, 1432(%rsp)
	movl	$354, 1440(%rsp)        # imm = 0x162
	cmpq	$68, 128(%rsp)          # 8-byte Folded Reload
	jne	.LBB1_4
# BB#3:
	movl	$.Lload_mats.mats0_Sml, %esi
	jmp	.LBB1_5
.LBB1_4:
	leaq	160(%rsp), %rsi
.LBB1_5:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats1, %esi
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats3, %esi
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats3, %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats4, %esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats9, %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats9, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats9, %esi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movl	$.Lload_mats.mats9, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rdx
	callq	memcpy
	movl	$.Lload_mats.mats9, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	memcpy
	movl	$.Lload_mats.mats11, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	callq	memcpy
	movl	$.Lload_mats.mats11, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memcpy
	movq	%rbx, %rax
	addq	$1448, %rsp             # imm = 0x5A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	load_mats, .Lfunc_end1-load_mats
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	load_concs
	.p2align	4, 0x90
	.type	load_concs,@function
load_concs:                             # @load_concs
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader33
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r13, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$96, %edi
	callq	malloc
	movq	%rax, %r14
	movslq	(%r12), %r15
	movq	%r15, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, (%r14)
	movslq	4(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	movslq	8(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 16(%r14)
	movslq	12(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movslq	16(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 32(%r14)
	movslq	20(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 40(%r14)
	movslq	24(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 48(%r14)
	movslq	28(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 56(%r14)
	movslq	32(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 64(%r14)
	movslq	36(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 72(%r14)
	movslq	40(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 80(%r14)
	movslq	44(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 88(%r14)
	xorl	%r13d, %r13d
	testl	%r15d, %r15d
	jg	.LBB2_2
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_5:                                # %._crit_edge..preheader_crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	4(%r12,%r13,4), %r15d
	movq	%rax, %r13
	testl	%r15d, %r15d
	jle	.LBB2_4
.LBB2_2:                                # %.lr.ph
	movq	(%r14,%r13,8), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	callq	rand
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rbx,8)
	incq	%rbx
	movslq	(%r12,%r13,4), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_3
.LBB2_4:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r13), %rax
	cmpq	$12, %rax
	jne	.LBB2_5
# BB#6:
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	load_concs, .Lfunc_end2-load_concs
	.cfi_endproc

	.globl	load_concs_v
	.p2align	4, 0x90
	.type	load_concs_v,@function
load_concs_v:                           # @load_concs_v
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader33
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -48
.Lcfi31:
	.cfi_offset %r12, -40
.Lcfi32:
	.cfi_offset %r13, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$96, %edi
	callq	malloc
	movq	%rax, %r14
	movslq	(%r12), %r15
	movq	%r15, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, (%r14)
	movslq	4(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	movslq	8(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 16(%r14)
	movslq	12(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movslq	16(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 32(%r14)
	movslq	20(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 40(%r14)
	movslq	24(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 48(%r14)
	movslq	28(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 56(%r14)
	movslq	32(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 64(%r14)
	movslq	36(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 72(%r14)
	movslq	40(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 80(%r14)
	movslq	44(%r12), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 88(%r14)
	xorl	%r13d, %r13d
	testl	%r15d, %r15d
	jg	.LBB3_2
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_5:                                # %._crit_edge..preheader_crit_edge
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	4(%r12,%r13,4), %r15d
	movq	%rax, %r13
	testl	%r15d, %r15d
	jle	.LBB3_4
.LBB3_2:                                # %.lr.ph
	movq	(%r14,%r13,8), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	callq	rn_v
	movsd	%xmm0, (%r15,%rbx,8)
	incq	%rbx
	movslq	(%r12,%r13,4), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_3
.LBB3_4:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r13), %rax
	cmpq	$12, %rax
	jne	.LBB3_5
# BB#6:
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	load_concs_v, .Lfunc_end3-load_concs_v
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	4594212051873190380     # double 0.14000000000000001
	.quad	4587654810815738937     # double 0.051999999999999998
.LCPI4_1:
	.quad	4598625579508013466     # double 0.27500000000000002
	.quad	4593995879091076596     # double 0.13400000000000001
.LCPI4_2:
	.quad	4594716455031455875     # double 0.154
	.quad	4589276106681592316     # double 0.064000000000000001
.LCPI4_3:
	.quad	4589420221869668172     # double 0.066000000000000003
	.quad	4588087156379966505     # double 0.055
.LCPI4_4:
	.quad	4575765307799480828     # double 0.0080000000000000002
	.quad	4579800533065604792     # double 0.014999999999999999
.LCPI4_5:
	.quad	4582862980812216730     # double 0.025000000000000001
	.quad	4578647611560997945     # double 0.012999999999999999
	.text
	.globl	pick_mat
	.p2align	4, 0x90
	.type	pick_mat,@function
pick_mat:                               # @pick_mat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 128
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [1.400000e-01,5.200000e-02]
	movaps	%xmm0, (%rsp)
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [2.750000e-01,1.340000e-01]
	movaps	%xmm0, 16(%rsp)
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [1.540000e-01,6.400000e-02]
	movaps	%xmm0, 32(%rsp)
	movaps	.LCPI4_3(%rip), %xmm0   # xmm0 = [6.600000e-02,5.500000e-02]
	movaps	%xmm0, 48(%rsp)
	movaps	.LCPI4_4(%rip), %xmm0   # xmm0 = [8.000000e-03,1.500000e-02]
	movaps	%xmm0, 64(%rsp)
	movaps	.LCPI4_5(%rip), %xmm0   # xmm0 = [2.500000e-02,1.300000e-02]
	movaps	%xmm0, 80(%rsp)
	xorl	%ebx, %ebx
	movl	$1, %r14d
	movl	$2, %r15d
	callq	rn_v
	.p2align	4, 0x90
.LBB4_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_7 Depth 2
	xorpd	%xmm1, %xmm1
	testl	%ebx, %ebx
	jle	.LBB4_2
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rsp,%rax,8), %xmm1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB4_5
.LBB4_2:                                # %._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	ucomisd	%xmm0, %xmm1
	ja	.LBB4_3
# BB#6:                                 # %.lr.ph.preheader.1
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	%ebx, %eax
	orl	$1, %eax
	xorpd	%xmm1, %xmm1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.1
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rsp,%rcx,8), %xmm1
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB4_7
# BB#8:                                 # %._crit_edge.1
                                        #   in Loop: Header=BB4_1 Depth=1
	ucomisd	%xmm0, %xmm1
	ja	.LBB4_11
# BB#9:                                 #   in Loop: Header=BB4_1 Depth=1
	addl	$2, %ebx
	addq	$2, %r14
	addq	$2, %r15
	cmpl	$12, %ebx
	jl	.LBB4_1
# BB#10:
	xorl	%eax, %eax
	jmp	.LBB4_11
.LBB4_3:
	movl	%ebx, %eax
.LBB4_11:
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	pick_mat, .Lfunc_end4-pick_mat
	.cfi_endproc

	.type	.Lload_mats.mats0_Sml,@object # @load_mats.mats0_Sml
	.section	.rodata,"a",@progbits
	.p2align	4
.Lload_mats.mats0_Sml:
	.long	58                      # 0x3a
	.long	59                      # 0x3b
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	40                      # 0x28
	.long	42                      # 0x2a
	.long	43                      # 0x2b
	.long	44                      # 0x2c
	.long	45                      # 0x2d
	.long	46                      # 0x2e
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	29                      # 0x1d
	.long	57                      # 0x39
	.long	47                      # 0x2f
	.long	48                      # 0x30
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	15                      # 0xf
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	54                      # 0x36
	.long	55                      # 0x37
	.long	56                      # 0x38
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	41                      # 0x29
	.size	.Lload_mats.mats0_Sml, 136

	.type	.Lload_mats.mats0_Lrg,@object # @load_mats.mats0_Lrg
	.p2align	4
.Lload_mats.mats0_Lrg:
	.long	58                      # 0x3a
	.long	59                      # 0x3b
	.long	60                      # 0x3c
	.long	61                      # 0x3d
	.long	40                      # 0x28
	.long	42                      # 0x2a
	.long	43                      # 0x2b
	.long	44                      # 0x2c
	.long	45                      # 0x2d
	.long	46                      # 0x2e
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	29                      # 0x1d
	.long	57                      # 0x39
	.long	47                      # 0x2f
	.long	48                      # 0x30
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	15                      # 0xf
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	54                      # 0x36
	.long	55                      # 0x37
	.long	56                      # 0x38
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lload_mats.mats0_Lrg, 1284

	.type	.Lload_mats.mats1,@object # @load_mats.mats1
	.p2align	4
.Lload_mats.mats1:
	.long	63                      # 0x3f
	.long	64                      # 0x40
	.long	65                      # 0x41
	.long	66                      # 0x42
	.long	67                      # 0x43
	.size	.Lload_mats.mats1, 20

	.type	.Lload_mats.mats3,@object # @load_mats.mats3
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.Lload_mats.mats3:
	.long	24                      # 0x18
	.long	41                      # 0x29
	.long	4                       # 0x4
	.long	5                       # 0x5
	.size	.Lload_mats.mats3, 16

	.type	.Lload_mats.mats4,@object # @load_mats.mats4
	.section	.rodata,"a",@progbits
	.p2align	4
.Lload_mats.mats4:
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	35                      # 0x23
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	38                      # 0x26
	.long	39                      # 0x27
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	32                      # 0x20
	.long	26                      # 0x1a
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	51                      # 0x33
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	6                       # 0x6
	.long	16                      # 0x10
	.long	17                      # 0x11
	.size	.Lload_mats.mats4, 108

	.type	.Lload_mats.mats9,@object # @load_mats.mats9
	.p2align	4
.Lload_mats.mats9:
	.long	24                      # 0x18
	.long	41                      # 0x29
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	35                      # 0x23
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	38                      # 0x26
	.long	39                      # 0x27
	.long	25                      # 0x19
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	51                      # 0x33
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.size	.Lload_mats.mats9, 84

	.type	.Lload_mats.mats11,@object # @load_mats.mats11
	.p2align	4
.Lload_mats.mats11:
	.long	24                      # 0x18
	.long	41                      # 0x29
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	63                      # 0x3f
	.long	64                      # 0x40
	.long	65                      # 0x41
	.long	66                      # 0x42
	.long	67                      # 0x43
	.size	.Lload_mats.mats11, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
