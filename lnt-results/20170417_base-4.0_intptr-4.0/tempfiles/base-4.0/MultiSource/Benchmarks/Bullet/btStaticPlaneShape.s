	.text
	.file	"btStaticPlaneShape.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btStaticPlaneShapeC2ERK9btVector3f
	.p2align	4, 0x90
	.type	_ZN18btStaticPlaneShapeC2ERK9btVector3f,@function
_ZN18btStaticPlaneShapeC2ERK9btVector3f: # @_ZN18btStaticPlaneShapeC2ERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movq	$_ZTV18btStaticPlaneShape+16, (%rbx)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_2:                                # %.split
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	8(%r14), %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm2, 60(%rbx)
	movlps	%xmm3, 68(%rbx)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 76(%rbx)
	movups	%xmm1, 80(%rbx)
	movl	$28, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN18btStaticPlaneShapeC2ERK9btVector3f, .Lfunc_end0-_ZN18btStaticPlaneShapeC2ERK9btVector3f
	.cfi_endproc

	.globl	_ZN18btStaticPlaneShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN18btStaticPlaneShapeD2Ev,@function
_ZN18btStaticPlaneShapeD2Ev:            # @_ZN18btStaticPlaneShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.Lfunc_end1:
	.size	_ZN18btStaticPlaneShapeD2Ev, .Lfunc_end1-_ZN18btStaticPlaneShapeD2Ev
	.cfi_endproc

	.globl	_ZN18btStaticPlaneShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN18btStaticPlaneShapeD0Ev,@function
_ZN18btStaticPlaneShapeD0Ev:            # @_ZN18btStaticPlaneShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN18btStaticPlaneShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN18btStaticPlaneShapeD0Ev, .Lfunc_end2-_ZN18btStaticPlaneShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movl	$-581039253, (%rdx)     # imm = 0xDD5E0B6B
	movl	$-581039253, 4(%rdx)    # imm = 0xDD5E0B6B
	movl	$-581039253, 8(%rdx)    # imm = 0xDD5E0B6B
	movl	$0, 12(%rdx)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%rcx)
	movq	$1566444395, 8(%rcx)    # imm = 0x5D5E0B6B
	retq
.Lfunc_end3:
	.size	_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end3-_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1056964608              # float 0.5
.LCPI4_3:
	.long	1060439283              # float 0.707106769
.LCPI4_4:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI4_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI4_5:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	subq	$184, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 224
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	(%r12), %xmm1
	subss	4(%r12), %xmm2
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	8(%r12), %xmm0
	movss	.LCPI4_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm0
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	sqrtss	%xmm0, %xmm8
	ucomiss	%xmm8, %xmm8
	jnp	.LBB4_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm8
.LBB4_2:                                # %.split
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r12), %xmm6           # xmm6 = mem[0],zero
	addps	%xmm0, %xmm6
	movss	8(%rbx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	addss	8(%r12), %xmm9
	mulps	.LCPI4_1(%rip), %xmm6
	mulss	.LCPI4_0(%rip), %xmm9
	movss	68(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm0
	ucomiss	.LCPI4_3(%rip), %xmm0
	jbe	.LBB4_6
# BB#3:
	movss	64(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm7, %xmm7
	addss	%xmm0, %xmm7
	xorps	%xmm0, %xmm0
	sqrtss	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_5
# BB#4:                                 # %call.sqrt420
	movaps	%xmm7, %xmm0
	movaps	%xmm6, 48(%rsp)         # 16-byte Spill
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm6         # 16-byte Reload
.LBB4_5:                                # %.split419
	movss	.LCPI4_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm4
	movss	68(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm4, %xmm0
	movaps	.LCPI4_5(%rip), %xmm5   # xmm5 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm5, %xmm0
	movq	60(%r15), %xmm2         # xmm2 = mem[0],zero
	pshufd	$229, %xmm2, %xmm11     # xmm11 = xmm2[1,1,2,3]
	mulss	%xmm4, %xmm11
	mulss	%xmm4, %xmm7
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	xorps	%xmm5, %xmm4
	movaps	%xmm2, %xmm10
	mulss	%xmm0, %xmm10
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	xorps	%xmm4, %xmm4
	shufps	$0, %xmm4, %xmm0        # xmm0 = xmm0[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	jmp	.LBB4_9
.LBB4_6:
	movss	60(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r15), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm10, %xmm10
	addss	%xmm0, %xmm10
	xorps	%xmm0, %xmm0
	sqrtss	%xmm10, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_8
# BB#7:                                 # %call.sqrt422
	movaps	%xmm10, %xmm0
	movaps	%xmm6, 48(%rsp)         # 16-byte Spill
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm10        # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm6         # 16-byte Reload
.LBB4_8:                                # %.split421
	movss	.LCPI4_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movq	60(%r15), %xmm2         # xmm2 = mem[0],zero
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	mulss	%xmm1, %xmm0
	movaps	.LCPI4_5(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm0
	mulss	%xmm1, %xmm10
	mulss	%xmm2, %xmm1
	movss	68(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm7
	mulss	%xmm1, %xmm7
	xorps	%xmm4, %xmm7
	movaps	%xmm3, %xmm4
	mulss	%xmm0, %xmm4
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	xorps	%xmm11, %xmm11
.LBB4_9:                                # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	movaps	%xmm6, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm6, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm6, %xmm1
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm5, %xmm6
	addss	%xmm4, %xmm6
	movaps	%xmm9, %xmm4
	mulss	%xmm3, %xmm4
	addss	%xmm6, %xmm4
	subss	76(%r15), %xmm4
	mulss	%xmm4, %xmm3
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	subps	%xmm4, %xmm1
	subss	%xmm3, %xmm9
	movaps	%xmm8, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm0
	mulss	%xmm8, %xmm11
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm11, %xmm3
	addss	%xmm9, %xmm3
	mulps	%xmm7, %xmm4
	mulss	%xmm8, %xmm10
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm5
	movaps	%xmm4, %xmm7
	addps	%xmm2, %xmm5
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	xorps	%xmm6, %xmm6
	movss	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1,2,3]
	movaps	%xmm6, 160(%rsp)        # 16-byte Spill
	movlps	%xmm5, 64(%rsp)
	movlps	%xmm6, 72(%rsp)
	movaps	%xmm7, 144(%rsp)        # 16-byte Spill
	subps	%xmm7, %xmm2
	subss	%xmm10, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1,2,3]
	movlps	%xmm2, 80(%rsp)
	movlps	%xmm4, 88(%rsp)
	subps	%xmm0, %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	subss	%xmm11, %xmm9
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	subps	%xmm7, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm0
	subss	%xmm10, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movaps	%xmm2, 112(%rsp)        # 16-byte Spill
	movlps	%xmm1, 96(%rsp)
	movlps	%xmm2, 104(%rsp)
	movq	(%r14), %rax
	leaq	64(%rsp), %rbx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*16(%rax)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 64(%rsp)
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 72(%rsp)
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	144(%rsp), %xmm1        # 16-byte Folded Reload
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm2         # 16-byte Folded Reload
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm0, 88(%rsp)
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 96(%rsp)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 104(%rsp)
	movq	(%r14), %rax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*16(%rax)
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end4-_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc

	.globl	_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3: # @_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end5:
	.size	_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end5-_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3,@function
_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3: # @_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 80(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3, .Lfunc_end6-_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK18btStaticPlaneShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK18btStaticPlaneShape15getLocalScalingEv,@function
_ZNK18btStaticPlaneShape15getLocalScalingEv: # @_ZNK18btStaticPlaneShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	80(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZNK18btStaticPlaneShape15getLocalScalingEv, .Lfunc_end7-_ZNK18btStaticPlaneShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK18btStaticPlaneShape7getNameEv,"axG",@progbits,_ZNK18btStaticPlaneShape7getNameEv,comdat
	.weak	_ZNK18btStaticPlaneShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK18btStaticPlaneShape7getNameEv,@function
_ZNK18btStaticPlaneShape7getNameEv:     # @_ZNK18btStaticPlaneShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end8:
	.size	_ZNK18btStaticPlaneShape7getNameEv, .Lfunc_end8-_ZNK18btStaticPlaneShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end9-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end10:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end10-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.type	_ZTV18btStaticPlaneShape,@object # @_ZTV18btStaticPlaneShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV18btStaticPlaneShape
	.p2align	3
_ZTV18btStaticPlaneShape:
	.quad	0
	.quad	_ZTI18btStaticPlaneShape
	.quad	_ZN18btStaticPlaneShapeD2Ev
	.quad	_ZN18btStaticPlaneShapeD0Ev
	.quad	_ZNK18btStaticPlaneShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN18btStaticPlaneShape15setLocalScalingERK9btVector3
	.quad	_ZNK18btStaticPlaneShape15getLocalScalingEv
	.quad	_ZNK18btStaticPlaneShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK18btStaticPlaneShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK18btStaticPlaneShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.size	_ZTV18btStaticPlaneShape, 120

	.type	_ZTS18btStaticPlaneShape,@object # @_ZTS18btStaticPlaneShape
	.globl	_ZTS18btStaticPlaneShape
	.p2align	4
_ZTS18btStaticPlaneShape:
	.asciz	"18btStaticPlaneShape"
	.size	_ZTS18btStaticPlaneShape, 21

	.type	_ZTI18btStaticPlaneShape,@object # @_ZTI18btStaticPlaneShape
	.globl	_ZTI18btStaticPlaneShape
	.p2align	4
_ZTI18btStaticPlaneShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btStaticPlaneShape
	.quad	_ZTI14btConcaveShape
	.size	_ZTI18btStaticPlaneShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"STATICPLANE"
	.size	.L.str, 12


	.globl	_ZN18btStaticPlaneShapeC1ERK9btVector3f
	.type	_ZN18btStaticPlaneShapeC1ERK9btVector3f,@function
_ZN18btStaticPlaneShapeC1ERK9btVector3f = _ZN18btStaticPlaneShapeC2ERK9btVector3f
	.globl	_ZN18btStaticPlaneShapeD1Ev
	.type	_ZN18btStaticPlaneShapeD1Ev,@function
_ZN18btStaticPlaneShapeD1Ev = _ZN18btStaticPlaneShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
