	.text
	.file	"7zStream.bc"
	.globl	SeqInStream_Read2
	.p2align	4, 0x90
	.type	SeqInStream_Read2,@function
SeqInStream_Read2:                      # @SeqInStream_Read2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#1:                                 # %.lr.ph
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, 8(%rsp)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	*(%r15)
	testl	%eax, %eax
	jne	.LBB0_7
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_8
# BB#4:                                 # %.thread
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	%rax, %rbp
	subq	%rax, %rbx
	jne	.LBB0_2
.LBB0_6:
	xorl	%r14d, %r14d
	jmp	.LBB0_8
.LBB0_7:
	movl	%eax, %r14d
.LBB0_8:                                # %.loopexit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SeqInStream_Read2, .Lfunc_end0-SeqInStream_Read2
	.cfi_endproc

	.globl	SeqInStream_Read
	.p2align	4, 0x90
	.type	SeqInStream_Read,@function
SeqInStream_Read:                       # @SeqInStream_Read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB1_7
# BB#1:                                 # %.lr.ph.i
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, 8(%rsp)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	*(%r15)
	testl	%eax, %eax
	jne	.LBB1_6
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_4
# BB#5:                                 # %.thread.i
                                        #   in Loop: Header=BB1_2 Depth=1
	addq	%rax, %rbp
	subq	%rax, %rbx
	jne	.LBB1_2
	jmp	.LBB1_7
.LBB1_6:
	movl	%eax, %r14d
	jmp	.LBB1_7
.LBB1_4:                                # %.thread20.i
	movl	$6, %r14d
.LBB1_7:                                # %SeqInStream_Read2.exit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	SeqInStream_Read, .Lfunc_end1-SeqInStream_Read
	.cfi_endproc

	.globl	SeqInStream_ReadByte
	.p2align	4, 0x90
	.type	SeqInStream_ReadByte,@function
SeqInStream_ReadByte:                   # @SeqInStream_ReadByte
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movq	$1, (%rsp)
	movq	%rsp, %rdx
	callq	*(%rdi)
	testl	%eax, %eax
	jne	.LBB2_2
# BB#1:
	xorl	%ecx, %ecx
	cmpq	$1, (%rsp)
	movl	$6, %eax
	cmovel	%ecx, %eax
.LBB2_2:
	popq	%rcx
	retq
.Lfunc_end2:
	.size	SeqInStream_ReadByte, .Lfunc_end2-SeqInStream_ReadByte
	.cfi_endproc

	.globl	LookInStream_SeekTo
	.p2align	4, 0x90
	.type	LookInStream_SeekTo,@function
LookInStream_SeekTo:                    # @LookInStream_SeekTo
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movq	%rsi, (%rsp)
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	*24(%rdi)
	popq	%rcx
	retq
.Lfunc_end3:
	.size	LookInStream_SeekTo, .Lfunc_end3-LookInStream_SeekTo
	.cfi_endproc

	.globl	LookInStream_LookRead
	.p2align	4, 0x90
	.type	LookInStream_LookRead,@function
LookInStream_LookRead:                  # @LookInStream_LookRead
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpq	$0, (%rbx)
	je	.LBB4_1
# BB#2:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	*(%r14)
	testl	%eax, %eax
	jne	.LBB4_4
# BB#3:
	movq	8(%rsp), %rsi
	movq	(%rbx), %rdx
	movq	%r15, %rdi
	callq	memcpy
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	jmp	.LBB4_4
.LBB4_1:
	xorl	%eax, %eax
.LBB4_4:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	LookInStream_LookRead, .Lfunc_end4-LookInStream_LookRead
	.cfi_endproc

	.globl	LookInStream_Read2
	.p2align	4, 0x90
	.type	LookInStream_Read2,@function
LookInStream_Read2:                     # @LookInStream_Read2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB5_6
# BB#1:                                 # %.lr.ph
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, 8(%rsp)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	*16(%r15)
	testl	%eax, %eax
	jne	.LBB5_7
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_8
# BB#4:                                 # %.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	addq	%rax, %rbp
	subq	%rax, %rbx
	jne	.LBB5_2
.LBB5_6:
	xorl	%r14d, %r14d
	jmp	.LBB5_8
.LBB5_7:
	movl	%eax, %r14d
.LBB5_8:                                # %.loopexit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	LookInStream_Read2, .Lfunc_end5-LookInStream_Read2
	.cfi_endproc

	.globl	LookInStream_Read
	.p2align	4, 0x90
	.type	LookInStream_Read,@function
LookInStream_Read:                      # @LookInStream_Read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB6_7
# BB#1:                                 # %.lr.ph.i
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, 8(%rsp)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	*16(%r15)
	testl	%eax, %eax
	jne	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB6_4
# BB#5:                                 # %.thread.i
                                        #   in Loop: Header=BB6_2 Depth=1
	addq	%rax, %rbp
	subq	%rax, %rbx
	jne	.LBB6_2
	jmp	.LBB6_7
.LBB6_6:
	movl	%eax, %r14d
	jmp	.LBB6_7
.LBB6_4:                                # %.thread20.i
	movl	$6, %r14d
.LBB6_7:                                # %LookInStream_Read2.exit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	LookInStream_Read, .Lfunc_end6-LookInStream_Read
	.cfi_endproc

	.globl	LookToRead_CreateVTable
	.p2align	4, 0x90
	.type	LookToRead_CreateVTable,@function
LookToRead_CreateVTable:                # @LookToRead_CreateVTable
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$LookToRead_Look_Lookahead, %eax
	movl	$LookToRead_Look_Exact, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, (%rdi)
	movq	$LookToRead_Skip, 8(%rdi)
	movq	$LookToRead_Read, 16(%rdi)
	movq	$LookToRead_Seek, 24(%rdi)
	retq
.Lfunc_end7:
	.size	LookToRead_CreateVTable, .Lfunc_end7-LookToRead_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	LookToRead_Look_Lookahead,@function
LookToRead_Look_Lookahead:              # @LookToRead_Look_Lookahead
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	48(%rbx), %rcx
	xorl	%eax, %eax
	subq	40(%rbx), %rcx
	movq	%rcx, 8(%rsp)
	jne	.LBB8_4
# BB#1:
	cmpq	$0, (%r15)
	je	.LBB8_2
# BB#3:
	movq	$0, 40(%rbx)
	movq	$16384, 8(%rsp)         # imm = 0x4000
	movq	32(%rbx), %rdi
	leaq	56(%rbx), %rsi
	leaq	8(%rsp), %rdx
	callq	*(%rdi)
	movq	8(%rsp), %rcx
	movq	%rcx, 48(%rbx)
	cmpq	(%r15), %rcx
	jb	.LBB8_5
	jmp	.LBB8_6
.LBB8_2:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.LBB8_4:
	cmpq	(%r15), %rcx
	jae	.LBB8_6
.LBB8_5:
	movq	%rcx, (%r15)
.LBB8_6:
	movq	40(%rbx), %rcx
	leaq	56(%rbx,%rcx), %rcx
	movq	%rcx, (%r14)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	LookToRead_Look_Lookahead, .Lfunc_end8-LookToRead_Look_Lookahead
	.cfi_endproc

	.p2align	4, 0x90
	.type	LookToRead_Look_Exact,@function
LookToRead_Look_Exact:                  # @LookToRead_Look_Exact
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	48(%rbx), %rcx
	xorl	%eax, %eax
	subq	40(%rbx), %rcx
	jne	.LBB9_6
# BB#1:
	cmpq	$0, (%r15)
	je	.LBB9_2
# BB#3:
	movq	$0, 40(%rbx)
	cmpq	$16385, (%r15)          # imm = 0x4001
	jb	.LBB9_5
# BB#4:
	movq	$16384, (%r15)          # imm = 0x4000
.LBB9_5:
	movq	32(%rbx), %rdi
	leaq	56(%rbx), %rsi
	movq	%r15, %rdx
	callq	*(%rdi)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	(%r15), %rcx
	movq	%rcx, 48(%rbx)
	cmpq	(%r15), %rcx
	jb	.LBB9_7
	jmp	.LBB9_8
.LBB9_2:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB9_6:
	cmpq	(%r15), %rcx
	jae	.LBB9_8
.LBB9_7:
	movq	%rcx, (%r15)
.LBB9_8:
	movq	40(%rbx), %rcx
	leaq	56(%rbx,%rcx), %rcx
	movq	%rcx, (%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	LookToRead_Look_Exact, .Lfunc_end9-LookToRead_Look_Exact
	.cfi_endproc

	.p2align	4, 0x90
	.type	LookToRead_Skip,@function
LookToRead_Skip:                        # @LookToRead_Skip
	.cfi_startproc
# BB#0:
	addq	%rsi, 40(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	LookToRead_Skip, .Lfunc_end10-LookToRead_Skip
	.cfi_endproc

	.p2align	4, 0x90
	.type	LookToRead_Read,@function
LookToRead_Read:                        # @LookToRead_Read
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movq	40(%r15), %rax
	movq	48(%r15), %rbx
	subq	%rax, %rbx
	je	.LBB11_2
# BB#1:
	movq	(%r14), %rcx
	cmpq	%rcx, %rbx
	cmovaq	%rcx, %rbx
	leaq	56(%r15,%rax), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, 40(%r15)
	movq	%rbx, (%r14)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB11_2:
	movq	32(%r15), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end11:
	.size	LookToRead_Read, .Lfunc_end11-LookToRead_Read
	.cfi_endproc

	.p2align	4, 0x90
	.type	LookToRead_Seek,@function
LookToRead_Seek:                        # @LookToRead_Seek
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rdi)
	movq	32(%rdi), %rdi
	movq	8(%rdi), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end12:
	.size	LookToRead_Seek, .Lfunc_end12-LookToRead_Seek
	.cfi_endproc

	.globl	LookToRead_Init
	.p2align	4, 0x90
	.type	LookToRead_Init,@function
LookToRead_Init:                        # @LookToRead_Init
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rdi)
	retq
.Lfunc_end13:
	.size	LookToRead_Init, .Lfunc_end13-LookToRead_Init
	.cfi_endproc

	.globl	SecToLook_CreateVTable
	.p2align	4, 0x90
	.type	SecToLook_CreateVTable,@function
SecToLook_CreateVTable:                 # @SecToLook_CreateVTable
	.cfi_startproc
# BB#0:
	movq	$SecToLook_Read, (%rdi)
	retq
.Lfunc_end14:
	.size	SecToLook_CreateVTable, .Lfunc_end14-SecToLook_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	SecToLook_Read,@function
SecToLook_Read:                         # @SecToLook_Read
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 48
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	cmpq	$0, (%rbx)
	je	.LBB15_1
# BB#2:
	movq	8(%rdi), %r14
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	*(%r14)
	testl	%eax, %eax
	jne	.LBB15_4
# BB#3:
	movq	8(%rsp), %rsi
	movq	(%rbx), %rdx
	movq	%r15, %rdi
	callq	memcpy
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	jmp	.LBB15_4
.LBB15_1:
	xorl	%eax, %eax
.LBB15_4:                               # %LookInStream_LookRead.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	SecToLook_Read, .Lfunc_end15-SecToLook_Read
	.cfi_endproc

	.globl	SecToRead_CreateVTable
	.p2align	4, 0x90
	.type	SecToRead_CreateVTable,@function
SecToRead_CreateVTable:                 # @SecToRead_CreateVTable
	.cfi_startproc
# BB#0:
	movq	$SecToRead_Read, (%rdi)
	retq
.Lfunc_end16:
	.size	SecToRead_CreateVTable, .Lfunc_end16-SecToRead_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	SecToRead_Read,@function
SecToRead_Read:                         # @SecToRead_Read
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rdi
	movq	16(%rdi), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end17:
	.size	SecToRead_Read, .Lfunc_end17-SecToRead_Read
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
