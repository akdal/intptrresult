	.text
	.file	"libclamav_uuencode.bc"
	.globl	cli_uuencode
	.p2align	4, 0x90
	.type	cli_uuencode,@function
cli_uuencode:                           # @cli_uuencode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$1016, %rsp             # imm = 0x3F8
.Lcfi4:
	.cfi_def_cfa_offset 1056
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	dup
	movl	%eax, %ebx
	movl	$.L.str, %esi
	movl	%ebx, %edi
	callq	fdopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1
# BB#2:
	movq	%rsp, %rdi
	movl	$1000, %esi             # imm = 0x3E8
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_3
# BB#4:
	movq	%rsp, %rdi
	callq	isuuencodebegin
	testl	%eax, %eax
	je	.LBB0_5
# BB#6:
	callq	messageCreate
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_7
# BB#8:
	xorl	%ebp, %ebp
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	uudecodeFile
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	messageDestroy
	movq	%r15, %rdi
	callq	fclose
	testl	%r14d, %r14d
	jns	.LBB0_10
	jmp	.LBB0_9
.LBB0_1:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	movl	%ebx, %edi
	callq	close
	movl	$-115, %ebp
	jmp	.LBB0_10
.LBB0_3:
	movq	%r15, %rdi
	callq	fclose
	xorl	%ebp, %ebp
	jmp	.LBB0_10
.LBB0_5:
	movq	%r15, %rdi
	callq	fclose
.LBB0_9:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-124, %ebp
	jmp	.LBB0_10
.LBB0_7:
	movq	%r15, %rdi
	callq	fclose
	movl	$-114, %ebp
.LBB0_10:
	movl	%ebp, %eax
	addq	$1016, %rsp             # imm = 0x3F8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_uuencode, .Lfunc_end0-cli_uuencode
	.cfi_endproc

	.globl	uudecodeFile
	.p2align	4, 0x90
	.type	uudecodeFile,@function
uudecodeFile:                           # @uudecodeFile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$2040, %rsp             # imm = 0x7F8
.Lcfi15:
	.cfi_def_cfa_offset 2096
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rax
	movq	%rdi, %r14
	movl	$2, %esi
	movl	$.L.str.4, %edx
	movq	%rax, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	movl	$-1, %ebp
	testq	%rbx, %rbx
	je	.LBB1_11
# BB#1:
	callq	fileblobCreate
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_2
# BB#3:
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	fileblobSetFilename
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movq	%rsp, %rbx
	leaq	1008(%rsp), %r13
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movl	$1000, %esi             # imm = 0x3E8
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_10
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	%rbx, %rdi
	callq	cli_chomp
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB1_10
# BB#6:                                 #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, (%rsp)
	je	.LBB1_10
# BB#7:                                 #   in Loop: Header=BB1_4 Depth=1
	movl	$5, %esi
	movl	$1024, %r8d             # imm = 0x400
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	decodeLine
	testq	%rax, %rax
	je	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_4 Depth=1
	subq	%r13, %rax
	leaq	-1(%rax), %rcx
	cmpq	$61, %rcx
	ja	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	callq	fileblobAddData
	testl	%eax, %eax
	jns	.LBB1_4
.LBB1_10:                               # %.loopexit
	movq	%r12, %rdi
	callq	fileblobDestroy
	movl	$1, %ebp
	jmp	.LBB1_11
.LBB1_2:
	movq	%rbx, %rdi
	callq	free
.LBB1_11:
	movl	%ebp, %eax
	addq	$2040, %rsp             # imm = 0x7F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	uudecodeFile, .Lfunc_end1-uudecodeFile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rb"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Can't open descriptor %d\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Message is not in uuencoded format\n"
	.size	.L.str.2, 36

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"found uuencode file\n"
	.size	.L.str.3, 21

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" "
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"uudecode %s\n"
	.size	.L.str.5, 13

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"end"
	.size	.L.str.6, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
