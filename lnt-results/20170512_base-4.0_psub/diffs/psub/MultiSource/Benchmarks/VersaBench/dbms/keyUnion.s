	.text
	.file	"keyUnion.bc"
	.globl	keyUnion
	.p2align	4, 0x90
	.type	keyUnion,@function
keyUnion:                               # @keyUnion
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rdi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	(%rax), %eax
	movl	%eax, (%rdx)
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rdi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%rdx)
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rdi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	12(%rdi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	12(%rax), %eax
	movl	%eax, 12(%rdx)
	movss	16(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rsi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	16(%rax), %eax
	movl	%eax, 16(%rdx)
	movss	20(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rsi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	20(%rax), %eax
	movl	%eax, 20(%rdx)
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rsi), %xmm0
	movq	%rsi, %rax
	cmovaq	%rdi, %rax
	movl	24(%rax), %eax
	movl	%eax, 24(%rdx)
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rsi), %xmm0
	cmovaq	%rdi, %rsi
	movl	28(%rsi), %eax
	movl	%eax, 28(%rdx)
	retq
.Lfunc_end0:
	.size	keyUnion, .Lfunc_end0-keyUnion
	.cfi_endproc

	.globl	keysUnion
	.p2align	4, 0x90
	.type	keysUnion,@function
keysUnion:                              # @keysUnion
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movdqu	8(%rdi), %xmm0
	movdqu	24(%rdi), %xmm1
	movdqu	%xmm1, 16(%rsi)
	movdqu	%xmm0, (%rsi)
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_3
# BB#1:                                 # %.lr.ph
	movd	(%rsi), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movd	4(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movd	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movd	12(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movd	16(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	20(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	28(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	leaq	8(%rax), %rcx
	ucomiss	8(%rax), %xmm7
	movq	%rsi, %rdx
	cmovaq	%rcx, %rdx
	movl	(%rdx), %r8d
	movl	%r8d, (%rsi)
	ucomiss	12(%rax), %xmm6
	movq	%rsi, %rdx
	cmovaq	%rcx, %rdx
	movl	4(%rdx), %r9d
	movl	%r9d, 4(%rsi)
	ucomiss	16(%rax), %xmm5
	movq	%rsi, %rdx
	cmovaq	%rcx, %rdx
	movl	8(%rdx), %r10d
	movl	%r10d, 8(%rsi)
	ucomiss	20(%rax), %xmm4
	movq	%rsi, %rdi
	cmovaq	%rcx, %rdi
	movl	12(%rdi), %r11d
	movl	%r11d, 12(%rsi)
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	movq	%rsi, %rdx
	cmovaq	%rcx, %rdx
	movl	16(%rdx), %edx
	movl	%edx, 16(%rsi)
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movq	%rsi, %rdi
	cmovaq	%rcx, %rdi
	movl	20(%rdi), %edi
	movl	%edi, 20(%rsi)
	movss	32(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%rsi, %rbx
	cmovaq	%rcx, %rbx
	movl	24(%rbx), %ebx
	movl	%ebx, 24(%rsi)
	movss	36(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	cmovbeq	%rsi, %rcx
	movl	28(%rcx), %ecx
	movl	%ecx, 28(%rsi)
	movq	40(%rax), %rax
	movd	%r8d, %xmm7
	movd	%r9d, %xmm6
	movd	%r10d, %xmm5
	movd	%r11d, %xmm4
	movd	%edx, %xmm3
	movd	%edi, %xmm2
	movd	%ebx, %xmm1
	movd	%ecx, %xmm0
	testq	%rax, %rax
	jne	.LBB1_2
.LBB1_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	keysUnion, .Lfunc_end1-keysUnion
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
