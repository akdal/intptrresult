	.text
	.file	"SAalignmm.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607632778762754458     # double 1.1000000000000001
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_3:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_4:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	Aalign
	.p2align	4, 0x90
	.type	Aalign,@function
Aalign:                                 # @Aalign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi6:
	.cfi_def_cfa_offset 320
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	Aalign.orlgth1(%rip), %r15d
	testl	%r15d, %r15d
	jne	.LBB0_2
# BB#1:
	movl	njob(%rip), %edi
	movl	$1, %esi
	callq	AllocateCharMtx
	movq	%rax, Aalign.mseq1(%rip)
	movl	njob(%rip), %edi
	movl	$1, %esi
	callq	AllocateCharMtx
	movq	%rax, Aalign.mseq2(%rip)
	movl	Aalign.orlgth1(%rip), %r15d
.LBB0_2:
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	cmpl	%r15d, %ebp
	movl	Aalign.orlgth2(%rip), %r13d
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jg	.LBB0_4
# BB#3:
	cmpl	%r13d, %eax
	jg	.LBB0_4
.LBB0_9:                                # %.preheader193
	movl	12(%rsp), %r9d          # 4-byte Reload
	testl	%r9d, %r9d
	jle	.LBB0_24
# BB#10:                                # %.lr.ph221
	movq	Aalign.mseq(%rip), %rcx
	movq	Aalign.mseq1(%rip), %rdx
	movl	%r9d, %ebp
	cmpl	$3, %r9d
	jbe	.LBB0_11
# BB#18:                                # %min.iters.checked
	movl	%r9d, %r8d
	andl	$3, %r8d
	movq	%rbp, %rsi
	subq	%r8, %rsi
	je	.LBB0_11
# BB#19:                                # %vector.memcheck
	leaq	(%rcx,%rbp,8), %rax
	cmpq	%rax, %rdx
	jae	.LBB0_21
# BB#20:                                # %vector.memcheck
	leaq	(%rdx,%rbp,8), %rax
	cmpq	%rax, %rcx
	jae	.LBB0_21
.LBB0_11:
	xorl	%esi, %esi
.LBB0_12:                               # %scalar.ph.preheader
	movl	%ebp, %eax
	subl	%esi, %eax
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rax
	je	.LBB0_15
# BB#13:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbx
	movq	%rbx, (%rdx,%rsi,8)
	incq	%rsi
	incq	%rax
	jne	.LBB0_14
.LBB0_15:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB0_24
# BB#16:                                # %scalar.ph.preheader.new
	subq	%rsi, %rbp
	leaq	56(%rdx,%rsi,8), %rdx
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB0_17:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rax
	movq	%rax, -56(%rdx)
	movq	-48(%rcx), %rax
	movq	%rax, -48(%rdx)
	movq	-40(%rcx), %rax
	movq	%rax, -40(%rdx)
	movq	-32(%rcx), %rax
	movq	%rax, -32(%rdx)
	movq	-24(%rcx), %rax
	movq	%rax, -24(%rdx)
	movq	-16(%rcx), %rax
	movq	%rax, -16(%rdx)
	movq	-8(%rcx), %rax
	movq	%rax, -8(%rdx)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rbp
	jne	.LBB0_17
.LBB0_24:                               # %.preheader192
	testl	%r14d, %r14d
	jle	.LBB0_39
# BB#25:                                # %.lr.ph218
	movq	Aalign.mseq(%rip), %r8
	movq	Aalign.mseq2(%rip), %rsi
	movslq	%r9d, %r9
	movl	%r14d, %ebx
	cmpl	$3, %r14d
	jbe	.LBB0_26
# BB#33:                                # %min.iters.checked287
	movl	%r14d, %ebp
	andl	$3, %ebp
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	je	.LBB0_26
# BB#34:                                # %vector.memcheck302
	leaq	(%r9,%rbx), %rax
	leaq	(%r8,%rax,8), %rax
	cmpq	%rax, %rsi
	jae	.LBB0_36
# BB#35:                                # %vector.memcheck302
	leaq	(%rsi,%rbx,8), %rax
	leaq	(%r8,%r9,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB0_36
.LBB0_26:
	xorl	%edi, %edi
.LBB0_27:                               # %scalar.ph285.preheader
	movl	%ebx, %eax
	subl	%edi, %eax
	leaq	-1(%rbx), %rbp
	subq	%rdi, %rbp
	andq	$3, %rax
	je	.LBB0_30
# BB#28:                                # %scalar.ph285.prol.preheader
	leaq	(%r8,%r9,8), %rcx
	negq	%rax
	.p2align	4, 0x90
.LBB0_29:                               # %scalar.ph285.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rdx
	movq	%rdx, (%rsi,%rdi,8)
	incq	%rdi
	incq	%rax
	jne	.LBB0_29
.LBB0_30:                               # %scalar.ph285.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB0_39
# BB#31:                                # %scalar.ph285.preheader.new
	subq	%rdi, %rbx
	leaq	24(%rsi,%rdi,8), %rsi
	addq	%rdi, %r9
	leaq	24(%r8,%r9,8), %rax
	.p2align	4, 0x90
.LBB0_32:                               # %scalar.ph285
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rbx
	jne	.LBB0_32
.LBB0_39:                               # %._crit_edge219
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r15d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB0_41
# BB#40:                                # %._crit_edge219
	cmpl	%ebp, %r13d
	jg	.LBB0_41
.LBB0_45:
	movq	commonIP(%rip), %rax
	movq	%rax, Aalign.ijp(%rip)
	movq	Aalign.cpmx1(%rip), %r14
	movq	(%r12), %rdi
	callq	strlen
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%eax, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	callq	cpmx_calc
	movq	Aalign.cpmx2(%rip), %r14
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%eax, %ecx
	movl	8(%rsp), %r8d           # 4-byte Reload
	callq	cpmx_calc
	movq	Aalign.initverticalw(%rip), %rdi
	movq	Aalign.cpmx2(%rip), %rsi
	movq	Aalign.cpmx1(%rip), %rdx
	movq	Aalign.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %r8d
	pushq	$1
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	Aalign.intwork(%rip)
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	Aalign.currentw(%rip), %rdi
	movq	Aalign.cpmx1(%rip), %rsi
	movq	Aalign.cpmx2(%rip), %rdx
	movq	Aalign.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	pushq	$1
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	Aalign.intwork(%rip)
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movl	outgap(%rip), %edi
	cmpl	$1, %edi
	jne	.LBB0_56
# BB#46:                                # %.preheader191
	testl	%r14d, %r14d
	jle	.LBB0_55
# BB#47:                                # %.lr.ph216
	cvtsi2sdl	penalty(%rip), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movq	Aalign.initverticalw(%rip), %r8
	leal	1(%r14), %eax
	leaq	-1(%rax), %rsi
	cmpq	$7, %rsi
	jbe	.LBB0_48
# BB#51:                                # %min.iters.checked318
	movl	%r14d, %ecx
	andl	$7, %ecx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	subq	%rcx, %rsi
	je	.LBB0_48
# BB#52:                                # %vector.ph322
	incq	%rdx
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	20(%r8), %rbp
	.p2align	4, 0x90
.LBB0_53:                               # %vector.body314
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	-8(%rbp), %xmm2
	cvtps2pd	-16(%rbp), %xmm3
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	addpd	%xmm1, %xmm3
	addpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm5
	addpd	%xmm1, %xmm4
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm5, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	movupd	%xmm3, -16(%rbp)
	movupd	%xmm4, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_53
# BB#54:                                # %middle.block315
	testq	%rcx, %rcx
	jne	.LBB0_49
	jmp	.LBB0_55
.LBB0_48:
	movl	$1, %edx
.LBB0_49:                               # %scalar.ph316.preheader
	leaq	(%r8,%rdx,4), %rcx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_50:                               # %scalar.ph316
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rcx)
	addq	$4, %rcx
	decq	%rax
	jne	.LBB0_50
.LBB0_55:                               # %.preheader190
	testl	%ebx, %ebx
	jle	.LBB0_56
# BB#66:                                # %.lr.ph214
	xorps	%xmm0, %xmm0
	cvtsi2sdl	penalty(%rip), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movq	Aalign.currentw(%rip), %r8
	leal	1(%rbx), %eax
	leaq	-1(%rax), %rsi
	cmpq	$7, %rsi
	jbe	.LBB0_67
# BB#70:                                # %min.iters.checked339
	movl	%ebx, %ecx
	andl	$7, %ecx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	subq	%rcx, %rsi
	je	.LBB0_67
# BB#71:                                # %vector.ph343
	incq	%rdx
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	20(%r8), %rbp
	.p2align	4, 0x90
.LBB0_72:                               # %vector.body335
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	-8(%rbp), %xmm2
	cvtps2pd	-16(%rbp), %xmm3
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	addpd	%xmm1, %xmm3
	addpd	%xmm1, %xmm2
	addpd	%xmm1, %xmm5
	addpd	%xmm1, %xmm4
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm5, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	movupd	%xmm3, -16(%rbp)
	movupd	%xmm4, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_72
# BB#73:                                # %middle.block336
	testq	%rcx, %rcx
	jne	.LBB0_68
	jmp	.LBB0_56
.LBB0_67:
	movl	$1, %edx
.LBB0_68:                               # %scalar.ph337.preheader
	leaq	(%r8,%rdx,4), %rcx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_69:                               # %scalar.ph337
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rcx)
	addq	$4, %rcx
	decq	%rax
	jne	.LBB0_69
.LBB0_56:                               # %.preheader189
	leal	1(%rbx), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%ebx, %ebx
	movq	Aalign.currentw(%rip), %r13
	js	.LBB0_64
# BB#57:                                # %.lr.ph209
	movq	Aalign.m(%rip), %rdx
	movq	Aalign.mp(%rip), %rsi
	movl	32(%rsp), %eax          # 4-byte Reload
	testb	$1, %al
	jne	.LBB0_59
# BB#58:
	xorl	%edi, %edi
	testl	%ebx, %ebx
	jne	.LBB0_61
	jmp	.LBB0_63
.LBB0_59:
	movss	-4(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	penalty(%rip), %xmm1
	mulsd	.LCPI0_1(%rip), %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%rdx)
	movl	$0, (%rsi)
	movl	$1, %edi
	testl	%ebx, %ebx
	je	.LBB0_63
.LBB0_61:                               # %.lr.ph209.new
	subq	%rdi, %rax
	leaq	(%r13,%rdi,4), %rcx
	leaq	4(%rdx,%rdi,4), %rdx
	leaq	4(%rsi,%rdi,4), %rsi
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_62:                               # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	penalty(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rdx)
	movl	$0, -4(%rsi)
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	penalty(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rdx)
	movl	$0, (%rsi)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rax
	jne	.LBB0_62
.LBB0_63:                               # %._crit_edge210.loopexit
	movl	outgap(%rip), %edi
.LBB0_64:                               # %._crit_edge210
	movq	%rbx, %rax
	shlq	$32, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$30, %rax
	movl	(%r13,%rax), %eax
	movq	Aalign.lastverticalw(%rip), %rdx
	movl	%eax, (%rdx)
	cmpl	$1, %edi
	sbbl	$-1, %r14d
	cmpl	$2, %r14d
	jl	.LBB0_65
# BB#74:                                # %.lr.ph205
	sarq	$32, %rcx
	movl	%ebx, %r15d
	movq	Aalign.previousw(%rip), %rdi
	movl	%r14d, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	32(%rsp), %r14d         # 4-byte Reload
	decq	%r14
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movl	$1, %ebp
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_75:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_76 Depth 2
                                        #       Child Loop BB0_77 Depth 3
                                        #     Child Loop BB0_81 Depth 2
                                        #       Child Loop BB0_83 Depth 3
                                        #     Child Loop BB0_87 Depth 2
	movq	%r13, %rsi
	movl	32(%rsp), %edx          # 4-byte Reload
	callq	floatncpy
	movq	Aalign.initverticalw(%rip), %rcx
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	-4(%rcx,%rbp,4), %eax
	movq	Aalign.previousw(%rip), %rdi
	movl	%eax, (%rdi)
	movq	Aalign.currentw(%rip), %r13
	movq	Aalign.cpmx1(%rip), %rax
	movq	Aalign.floatwork(%rip), %r10
	movq	Aalign.intwork(%rip), %r11
	movl	$n_dis, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_76:                               #   Parent Loop BB0_75 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_77 Depth 3
	movl	$0, 160(%rsp,%rcx,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %edx
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB0_77:                               #   Parent Loop BB0_75 Depth=1
                                        #     Parent Loop BB0_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rsi), %xmm1
	movq	-8(%rax,%rdx,8), %r9
	mulss	(%r9,%rbp,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rsi), %xmm0
	movq	(%rax,%rdx,8), %rbx
	mulss	(%rbx,%rbp,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rsi
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB0_77
# BB#78:                                #   in Loop: Header=BB0_76 Depth=2
	movss	%xmm0, 160(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %r8
	cmpq	$26, %rcx
	jne	.LBB0_76
# BB#79:                                # %.preheader.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	56(%rsp), %r8           # 8-byte Reload
	testl	%r8d, %r8d
	movss	76(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movsd	.LCPI0_1(%rip), %xmm4   # xmm4 = mem[0],zero
	jle	.LBB0_85
# BB#80:                                # %.lr.ph74.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	(%r11), %rax
	addq	$8, %r11
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_81:                               #   Parent Loop BB0_75 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_83 Depth 3
	movl	$0, (%r13,%rcx,4)
	movl	(%rax,%rcx,4), %ebx
	testl	%ebx, %ebx
	js	.LBB0_84
# BB#82:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_81 Depth=2
	xorps	%xmm0, %xmm0
	movq	%r10, %rdx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB0_83:                               # %.lr.ph.i
                                        #   Parent Loop BB0_75 Depth=1
                                        #     Parent Loop BB0_81 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rbx
	movss	160(%rsp,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movq	(%rdx), %rbx
	mulss	(%rbx,%rcx,4), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%r13,%rcx,4)
	movq	(%rsi), %rbx
	movl	(%rbx,%rcx,4), %ebx
	addq	$8, %rsi
	addq	$8, %rdx
	testl	%ebx, %ebx
	jns	.LBB0_83
.LBB0_84:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_81 Depth=2
	incq	%rcx
	cmpq	%r15, %rcx
	jne	.LBB0_81
.LBB0_85:                               # %match_calc.exit
                                        #   in Loop: Header=BB0_75 Depth=1
	testl	%r8d, %r8d
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movl	%eax, (%r13)
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	penalty(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, Aalign.mi(%rip)
	movl	$0, Aalign.mpi(%rip)
	movq	16(%rsp), %r8           # 8-byte Reload
	jle	.LBB0_96
# BB#86:                                # %.lr.ph200
                                        #   in Loop: Header=BB0_75 Depth=1
	movq	Aalign.ijp(%rip), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	Aalign.m(%rip), %rdx
	movq	Aalign.mp(%rip), %r9
	xorl	%eax, %eax
	movl	$-1, %ebx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_87:                               #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movl	$0, 4(%rcx,%rax,4)
	movl	penalty(%rip), %esi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm4, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	ucomiss	%xmm1, %xmm2
	movaps	%xmm1, %xmm3
	jbe	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_87 Depth=2
	leal	(%r10,%rbx), %esi
	movl	%esi, 4(%rcx,%rax,4)
	movl	penalty(%rip), %esi
	movaps	%xmm2, %xmm3
.LBB0_89:                               #   in Loop: Header=BB0_87 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm4, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	addss	%xmm2, %xmm1
	ucomiss	%xmm0, %xmm1
	jb	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_87 Depth=2
	movss	%xmm1, Aalign.mi(%rip)
	movl	%eax, Aalign.mpi(%rip)
	movl	%eax, %r10d
	movaps	%xmm1, %xmm0
.LBB0_91:                               #   in Loop: Header=BB0_87 Depth=2
	movss	4(%rdx,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	ucomiss	%xmm3, %xmm2
	jbe	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_87 Depth=2
	movl	%ebp, %esi
	subl	4(%r9,%rax,4), %esi
	movl	%esi, 4(%rcx,%rax,4)
	movl	penalty(%rip), %esi
	movaps	%xmm2, %xmm3
.LBB0_93:                               #   in Loop: Header=BB0_87 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm4, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	addss	(%rdi,%rax,4), %xmm2
	ucomiss	%xmm1, %xmm2
	jb	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_87 Depth=2
	movss	%xmm2, 4(%rdx,%rax,4)
	movl	%r8d, 4(%r9,%rax,4)
.LBB0_95:                               #   in Loop: Header=BB0_87 Depth=2
	movss	4(%r13,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, 4(%r13,%rax,4)
	incq	%rax
	decl	%ebx
	cmpq	%rax, %r14
	jne	.LBB0_87
.LBB0_96:                               # %._crit_edge201
                                        #   in Loop: Header=BB0_75 Depth=1
	movss	%xmm3, 76(%rsp)         # 4-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	(%r13,%rcx,4), %eax
	movq	Aalign.lastverticalw(%rip), %rdx
	movl	%eax, (%rdx,%rbp,4)
	incq	%rbp
	cmpq	80(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB0_75
# BB#97:                                # %._crit_edge206.loopexit
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	outgap(%rip), %ebx
	jmp	.LBB0_98
.LBB0_65:
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movl	%edi, %ebx
.LBB0_98:                               # %._crit_edge206
	movq	Aalign.mseq1(%rip), %r15
	movq	Aalign.mseq2(%rip), %r14
	movq	Aalign.ijp(%rip), %rbp
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %r11
	cmpl	$1, %ebx
	movl	12(%rsp), %r10d         # 4-byte Reload
	je	.LBB0_99
# BB#110:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB0_111
# BB#125:                               # %.lr.ph73.i
	movslq	%edx, %rax
	movq	%r11, %r8
	movslq	%r8d, %rcx
	movq	%rdx, %rsi
	movl	%eax, %edx
	addq	$4, %rbx
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movapd	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB0_128
	jmp	.LBB0_127
	.p2align	4, 0x90
.LBB0_130:                              # %._crit_edge.i183
                                        #   in Loop: Header=BB0_128 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB0_128
.LBB0_127:
	movq	(%rbp,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movapd	%xmm0, %xmm1
.LBB0_128:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB0_130
# BB#129:
	movapd	%xmm1, %xmm0
	testl	%r8d, %r8d
	jg	.LBB0_113
	jmp	.LBB0_99
.LBB0_111:
	movq	%r11, %r8
	testl	%r8d, %r8d
	jle	.LBB0_99
.LBB0_113:                              # %.lr.ph69.i
	movslq	24(%rsp), %r8           # 4-byte Folded Reload
	movq	%r11, %rax
	movslq	%eax, %r9
	movl	%r9d, %edx
	testb	$1, %al
	jne	.LBB0_115
# BB#114:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB0_119
	jmp	.LBB0_99
.LBB0_115:
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB0_117
# BB#116:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB0_119
	jmp	.LBB0_99
.LBB0_21:                               # %vector.body.preheader
	leaq	16(%rcx), %rax
	leaq	16(%rdx), %rbx
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB0_22:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB0_22
# BB#23:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB0_12
	jmp	.LBB0_24
.LBB0_36:                               # %vector.body283.preheader
	leaq	16(%r8,%r9,8), %rax
	leaq	16(%rsi), %rcx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB0_37:                               # %vector.body283
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rcx)
	movupd	%xmm1, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB0_37
# BB#38:                                # %middle.block284
	testl	%ebp, %ebp
	jne	.LBB0_27
	jmp	.LBB0_39
.LBB0_117:
	movl	%r11d, %esi
	negl	%esi
	movq	(%rbp,%r8,8), %rdi
	movl	%esi, (%rdi,%r9,4)
	movl	$1, %esi
	movapd	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB0_99
.LBB0_119:                              # %.lr.ph69.i.new
	movl	%r11d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB0_120:                              # =>This Inner Loop Header: Depth=1
	movss	(%r13,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB0_122
# BB#121:                               #   in Loop: Header=BB0_120 Depth=1
	leal	(%rdi,%rsi), %eax
	movq	(%rbp,%r8,8), %rcx
	movl	%eax, (%rcx,%r9,4)
	movapd	%xmm1, %xmm0
.LBB0_122:                              #   in Loop: Header=BB0_120 Depth=1
	movss	4(%r13,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_120 Depth=1
	leal	1(%rdi,%rsi), %eax
	movq	(%rbp,%r8,8), %rcx
	movl	%eax, (%rcx,%r9,4)
	movapd	%xmm1, %xmm0
.LBB0_124:                              #   in Loop: Header=BB0_120 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB0_120
.LBB0_99:                               # %.preheader15.i
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	96(%rsp), %r13          # 8-byte Reload
	js	.LBB0_105
# BB#100:                               # %.lr.ph66.preheader.i
	movq	24(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB0_102
	.p2align	4, 0x90
.LBB0_101:                              # %.lr.ph66.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB0_101
.LBB0_102:                              # %.lr.ph66.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_105
# BB#103:                               # %.lr.ph66.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %r8
	leaq	56(%rbp,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_104:                              # %.lr.ph66.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %r9d
	leal	-3(%rdx,%rsi), %eax
	movl	%eax, (%rdi)
	movq	-48(%rcx,%rsi,8), %rax
	leal	-2(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-40(%rcx,%rsi,8), %rax
	leal	-1(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-32(%rcx,%rsi,8), %rax
	movl	%r9d, (%rax)
	movq	-24(%rcx,%rsi,8), %rax
	leal	1(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-16(%rcx,%rsi,8), %rax
	leal	2(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-8(%rcx,%rsi,8), %rax
	leal	3(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	(%rcx,%rsi,8), %rax
	leal	4(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	leaq	8(%r8,%rsi), %rax
	addq	$8, %rsi
	cmpq	$4, %rax
	jne	.LBB0_104
.LBB0_105:                              # %.preheader14.i
	testl	%r11d, %r11d
	js	.LBB0_135
# BB#106:                               # %.lr.ph64.i
	movq	(%rbp), %r8
	movq	%r11, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB0_107
# BB#131:                               # %min.iters.checked362
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB0_107
# BB#132:                               # %vector.body358.preheader
	leaq	16(%r8), %rdi
	movdqa	.LCPI0_2(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI0_3(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI0_4(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB0_133:                              # %vector.body358
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB0_133
# BB#134:                               # %middle.block359
	testq	%rsi, %rsi
	jne	.LBB0_108
	jmp	.LBB0_135
.LBB0_107:
	xorl	%ecx, %ecx
.LBB0_108:                              # %scalar.ph360.preheader
	movl	%ecx, %edx
	notl	%edx
	leaq	(%r8,%rcx,4), %rsi
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_109:                              # %scalar.ph360
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rsi)
	decl	%edx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_109
.LBB0_135:                              # %.preheader13.i
	testl	%r10d, %r10d
	jle	.LBB0_141
# BB#136:                               # %.lr.ph62.i
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%r11,%rax), %eax
	cltq
	movl	%r10d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	xorl	%esi, %esi
	andq	$3, %r9
	je	.LBB0_138
	.p2align	4, 0x90
.LBB0_137:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rdx
	leaq	(%rdx,%rax), %rdi
	movq	%rdi, (%r15,%rsi,8)
	movb	$0, (%rdx,%rax)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB0_137
.LBB0_138:                              # %.prol.loopexit429
	cmpq	$3, %r8
	jb	.LBB0_141
# BB#139:                               # %.lr.ph62.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB0_140:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB0_140
.LBB0_141:                              # %.preheader12.i
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r11, %rbx
	jle	.LBB0_147
# BB#142:                               # %.lr.ph60.i
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rbx,%rax), %eax
	cltq
	movl	8(%rsp), %ecx           # 4-byte Reload
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	xorl	%esi, %esi
	andq	$3, %r9
	je	.LBB0_144
	.p2align	4, 0x90
.LBB0_143:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rsi,8), %rdx
	leaq	(%rdx,%rax), %rdi
	movq	%rdi, (%r14,%rsi,8)
	movb	$0, (%rdx,%rax)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB0_143
.LBB0_144:                              # %.prol.loopexit424
	cmpq	$3, %r8
	movq	%r11, %rbx
	jb	.LBB0_147
# BB#145:                               # %.lr.ph60.i.new
	subq	%rsi, %rcx
	leaq	24(%r14,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB0_146:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB0_146
.LBB0_147:                              # %.preheader11.i
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movl	%ebx, %ecx
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	%eax, %ecx
	movl	%ecx, 140(%rsp)         # 4-byte Spill
	js	.LBB0_235
# BB#148:                               # %.lr.ph56.preheader.i
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	8(%rsp), %r10d          # 4-byte Reload
	leaq	-1(%r10), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-1(%r9), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$3, %r9d
	movq	%r10, 56(%rsp)          # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$3, %r10d
	leaq	24(%r14), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	24(%r15), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	leaq	24(%r13), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_149:                              # %.lr.ph56.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_178 Depth 2
                                        #       Child Loop BB0_179 Depth 3
                                        #       Child Loop BB0_182 Depth 3
                                        #     Child Loop BB0_159 Depth 2
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_163 Depth 3
                                        #     Child Loop BB0_165 Depth 2
                                        #       Child Loop BB0_166 Depth 3
                                        #       Child Loop BB0_169 Depth 3
                                        #       Child Loop BB0_171 Depth 3
                                        #       Child Loop BB0_174 Depth 3
                                        #     Child Loop BB0_209 Depth 2
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_213 Depth 3
                                        #     Child Loop BB0_200 Depth 2
                                        #       Child Loop BB0_201 Depth 3
                                        #       Child Loop BB0_204 Depth 3
                                        #     Child Loop BB0_189 Depth 2
                                        #       Child Loop BB0_190 Depth 3
                                        #       Child Loop BB0_193 Depth 3
                                        #       Child Loop BB0_195 Depth 3
                                        #       Child Loop BB0_198 Depth 3
                                        #     Child Loop BB0_222 Depth 2
                                        #     Child Loop BB0_225 Depth 2
                                        #     Child Loop BB0_230 Depth 2
                                        #     Child Loop BB0_233 Depth 2
	movl	%eax, %edx
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	movslq	%edx, %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rbx, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %eax
	testl	%eax, %eax
	js	.LBB0_150
# BB#151:                               #   in Loop: Header=BB0_149 Depth=1
	je	.LBB0_153
# BB#152:                               #   in Loop: Header=BB0_149 Depth=1
	movl	%edx, %ecx
	subl	%eax, %ecx
	jmp	.LBB0_154
	.p2align	4, 0x90
.LBB0_150:                              #   in Loop: Header=BB0_149 Depth=1
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	-1(%rdx), %ecx
	jmp	.LBB0_155
	.p2align	4, 0x90
.LBB0_153:                              #   in Loop: Header=BB0_149 Depth=1
	leal	-1(%rdx), %ecx
.LBB0_154:                              #   in Loop: Header=BB0_149 Depth=1
	movl	$-1, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_155:                              #   in Loop: Header=BB0_149 Depth=1
	movl	%edx, %eax
	subl	%ecx, %eax
	decl	%eax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	je	.LBB0_185
# BB#156:                               # %.preheader8.lr.ph.i
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_177
# BB#157:                               # %.preheader8.us.preheader.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	%r8, 88(%rsp)           # 8-byte Spill
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movslq	%eax, %r13
	movslq	%ecx, %rbp
	jle	.LBB0_158
	.p2align	4, 0x90
.LBB0_165:                              # %.preheader8.us.i.us
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_166 Depth 3
                                        #       Child Loop BB0_169 Depth 3
                                        #       Child Loop BB0_171 Depth 3
                                        #       Child Loop BB0_174 Depth 3
	leaq	(%r13,%rbp), %rdi
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.LBB0_167
	.p2align	4, 0x90
.LBB0_166:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_165 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rax,8), %rcx
	movzbl	(%rcx,%rdi), %ecx
	movq	(%r15,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r9
	jne	.LBB0_166
.LBB0_167:                              # %.prol.loopexit386
                                        #   in Loop: Header=BB0_165 Depth=2
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_170
# BB#168:                               # %.preheader8.us.i.us.new
                                        #   in Loop: Header=BB0_165 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r11
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	.p2align	4, 0x90
.LBB0_169:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_165 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rax), %rdx
	movzbl	(%rdx,%rdi), %edx
	movq	-24(%r11), %rsi
	leaq	-1(%rsi), %r8
	movq	%r8, -24(%r11)
	movb	%dl, -1(%rsi)
	movq	-16(%rax), %rdx
	movzbl	(%rdx,%rdi), %edx
	movq	-16(%r11), %rsi
	leaq	-1(%rsi), %rbx
	movq	%rbx, -16(%r11)
	movb	%dl, -1(%rsi)
	movq	-8(%rax), %rdx
	movzbl	(%rdx,%rdi), %edx
	movq	-8(%r11), %rsi
	leaq	-1(%rsi), %rbx
	movq	%rbx, -8(%r11)
	movb	%dl, -1(%rsi)
	movq	(%rax), %rdx
	movzbl	(%rdx,%rdi), %edx
	movq	(%r11), %rsi
	leaq	-1(%rsi), %rbx
	movq	%rbx, (%r11)
	movb	%dl, -1(%rsi)
	addq	$32, %r11
	addq	$32, %rax
	addq	$-4, %rcx
	jne	.LBB0_169
.LBB0_170:                              # %.lr.ph19.us.i.us.preheader
                                        #   in Loop: Header=BB0_165 Depth=2
	xorl	%ecx, %ecx
	testq	%r10, %r10
	je	.LBB0_172
	.p2align	4, 0x90
.LBB0_171:                              # %.lr.ph19.us.i.us.prol
                                        #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_165 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%rcx,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r14,%rcx,8)
	movb	$45, -1(%rax)
	incq	%rcx
	cmpq	%rcx, %r10
	jne	.LBB0_171
.LBB0_172:                              # %.lr.ph19.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB0_165 Depth=2
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_175
# BB#173:                               # %.lr.ph19.us.i.us.preheader.new
                                        #   in Loop: Header=BB0_165 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	subq	%rcx, %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB0_174:                              # %.lr.ph19.us.i.us
                                        #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_165 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movb	$45, -1(%rdx)
	movq	-16(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movb	$45, -1(%rdx)
	movq	-8(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movb	$45, -1(%rdx)
	movq	(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%rcx)
	movb	$45, -1(%rdx)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB0_174
.LBB0_175:                              # %._crit_edge.us.i.loopexit.us
                                        #   in Loop: Header=BB0_165 Depth=2
	decq	%r13
	testl	%r13d, %r13d
	jne	.LBB0_165
	jmp	.LBB0_176
	.p2align	4, 0x90
.LBB0_177:                              # %.preheader8.lr.ph.split.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	%rdx, %rbp
	leal	-1(%r8,%rdx), %r8d
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_184
	.p2align	4, 0x90
.LBB0_178:                              # %.preheader8.us22.i
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_179 Depth 3
                                        #       Child Loop BB0_182 Depth 3
	xorl	%edx, %edx
	testq	%r10, %r10
	je	.LBB0_180
	.p2align	4, 0x90
.LBB0_179:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_178 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%rdx,8), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r14,%rdx,8)
	movb	$45, -1(%rcx)
	incq	%rdx
	cmpq	%rdx, %r10
	jne	.LBB0_179
.LBB0_180:                              # %.prol.loopexit
                                        #   in Loop: Header=BB0_178 Depth=2
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_183
# BB#181:                               # %.preheader8.us22.i.new
                                        #   in Loop: Header=BB0_178 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_182:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_178 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, -24(%rdx)
	movb	$45, -1(%rsi)
	movq	-16(%rdx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, -16(%rdx)
	movb	$45, -1(%rsi)
	movq	-8(%rdx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, -8(%rdx)
	movb	$45, -1(%rsi)
	movq	(%rdx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%rdx)
	movb	$45, -1(%rsi)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB0_182
.LBB0_183:                              # %._crit_edge.us30.i
                                        #   in Loop: Header=BB0_178 Depth=2
	decl	%eax
	jne	.LBB0_178
.LBB0_184:                              # %._crit_edge21.loopexit77.i
                                        #   in Loop: Header=BB0_149 Depth=1
	subl	24(%rsp), %r8d          # 4-byte Folded Reload
	movq	%rbp, %rdx
	jmp	.LBB0_185
.LBB0_158:                              #   in Loop: Header=BB0_149 Depth=1
	movq	%rbp, %r8
	.p2align	4, 0x90
.LBB0_159:                              # %.preheader8.us.i
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_163 Depth 3
	leaq	(%r13,%rbp), %rdi
	xorl	%edx, %edx
	testq	%r9, %r9
	je	.LBB0_161
	.p2align	4, 0x90
.LBB0_160:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rdx,8), %rax
	movzbl	(%rax,%rdi), %eax
	movq	(%r15,%rdx,8), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movb	%al, -1(%rcx)
	incq	%rdx
	cmpq	%rdx, %r9
	jne	.LBB0_160
.LBB0_161:                              # %.prol.loopexit381
                                        #   in Loop: Header=BB0_159 Depth=2
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_164
# BB#162:                               # %.preheader8.us.i.new
                                        #   in Loop: Header=BB0_159 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	128(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_163:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movq	-24(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -24(%rax)
	movb	%bl, -1(%rsi)
	movq	-16(%rdx), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movq	-16(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -16(%rax)
	movb	%bl, -1(%rsi)
	movq	-8(%rdx), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movq	-8(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -8(%rax)
	movb	%bl, -1(%rsi)
	movq	(%rdx), %rsi
	movzbl	(%rsi,%rdi), %ebx
	movq	(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%rax)
	movb	%bl, -1(%rsi)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB0_163
.LBB0_164:                              # %..preheader7_crit_edge.us.i
                                        #   in Loop: Header=BB0_159 Depth=2
	decq	%r13
	testl	%r13d, %r13d
	movq	%r8, %rbp
	jne	.LBB0_159
.LBB0_176:                              # %._crit_edge21.loopexit.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	leal	-1(%r8,%rdx), %r8d
	subl	24(%rsp), %r8d          # 4-byte Folded Reload
	movq	96(%rsp), %r13          # 8-byte Reload
.LBB0_185:                              # %._crit_edge21.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rcx,%rax), %ebx
	cmpl	$-1, %ecx
	je	.LBB0_216
# BB#186:                               # %.preheader6.lr.ph.i
                                        #   in Loop: Header=BB0_149 Depth=1
	notl	%ecx
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_206
# BB#187:                               # %.preheader6.us.preheader.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	%r8, 88(%rsp)           # 8-byte Spill
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	%ecx, %r11
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	jle	.LBB0_200
# BB#188:                               # %.preheader6.us.i.us.preheader
                                        #   in Loop: Header=BB0_149 Depth=1
	movslq	%ebx, %r8
	.p2align	4, 0x90
.LBB0_189:                              # %.preheader6.us.i.us
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_190 Depth 3
                                        #       Child Loop BB0_193 Depth 3
                                        #       Child Loop BB0_195 Depth 3
                                        #       Child Loop BB0_198 Depth 3
	xorl	%ecx, %ecx
	testq	%r9, %r9
	je	.LBB0_191
	.p2align	4, 0x90
.LBB0_190:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_189 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rcx,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15,%rcx,8)
	movb	$45, -1(%rax)
	incq	%rcx
	cmpq	%rcx, %r9
	jne	.LBB0_190
.LBB0_191:                              # %.prol.loopexit404
                                        #   in Loop: Header=BB0_189 Depth=2
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_194
# BB#192:                               # %.preheader6.us.i.us.new
                                        #   in Loop: Header=BB0_189 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	subq	%rcx, %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB0_193:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_189 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -24(%rcx)
	movb	$45, -1(%rdx)
	movq	-16(%rcx), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -16(%rcx)
	movb	$45, -1(%rdx)
	movq	-8(%rcx), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	movb	$45, -1(%rdx)
	movq	(%rcx), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rcx)
	movb	$45, -1(%rdx)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB0_193
.LBB0_194:                              # %.lr.ph34.us.i.us
                                        #   in Loop: Header=BB0_189 Depth=2
	leaq	(%r11,%r8), %rcx
	xorl	%edx, %edx
	testq	%r10, %r10
	je	.LBB0_196
	.p2align	4, 0x90
.LBB0_195:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_189 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rdx,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	(%r14,%rdx,8), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%r14,%rdx,8)
	movb	%al, -1(%rdi)
	incq	%rdx
	cmpq	%rdx, %r10
	jne	.LBB0_195
.LBB0_196:                              # %.prol.loopexit409
                                        #   in Loop: Header=BB0_189 Depth=2
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_199
# BB#197:                               # %.lr.ph34.us.i.us.new
                                        #   in Loop: Header=BB0_189 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	subq	%rdx, %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_198:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_189 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rbp
	movzbl	(%rbp,%rcx), %ebx
	movq	-24(%rax), %rbp
	leaq	-1(%rbp), %rsi
	movq	%rsi, -24(%rax)
	movb	%bl, -1(%rbp)
	movq	-16(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	-16(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -16(%rax)
	movb	%bl, -1(%rsi)
	movq	-8(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	-8(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -8(%rax)
	movb	%bl, -1(%rsi)
	movq	(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%rax)
	movb	%bl, -1(%rsi)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rdi
	jne	.LBB0_198
.LBB0_199:                              # %._crit_edge35.us.i.loopexit.us
                                        #   in Loop: Header=BB0_189 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB0_189
	jmp	.LBB0_215
	.p2align	4, 0x90
.LBB0_200:                              # %.preheader6.us.i
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_201 Depth 3
                                        #       Child Loop BB0_204 Depth 3
	xorl	%ecx, %ecx
	testq	%r9, %r9
	je	.LBB0_202
	.p2align	4, 0x90
.LBB0_201:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_200 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rcx,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15,%rcx,8)
	movb	$45, -1(%rax)
	incq	%rcx
	cmpq	%rcx, %r9
	jne	.LBB0_201
.LBB0_202:                              # %.prol.loopexit399
                                        #   in Loop: Header=BB0_200 Depth=2
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_205
# BB#203:                               # %.preheader6.us.i.new
                                        #   in Loop: Header=BB0_200 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	subq	%rcx, %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB0_204:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_200 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movb	$45, -1(%rdx)
	movq	-16(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movb	$45, -1(%rdx)
	movq	-8(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movb	$45, -1(%rdx)
	movq	(%rcx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%rcx)
	movb	$45, -1(%rdx)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB0_204
.LBB0_205:                              # %..preheader_crit_edge.us.i
                                        #   in Loop: Header=BB0_200 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB0_200
	jmp	.LBB0_215
	.p2align	4, 0x90
.LBB0_206:                              # %.preheader6.lr.ph.split.i
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_207
# BB#208:                               # %.preheader6.us39.preheader.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	%ecx, %r11
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movslq	%ebx, %r8
	.p2align	4, 0x90
.LBB0_209:                              # %.preheader6.us39.i
                                        #   Parent Loop BB0_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_213 Depth 3
	leaq	(%r11,%r8), %rcx
	xorl	%edx, %edx
	testq	%r10, %r10
	je	.LBB0_211
	.p2align	4, 0x90
.LBB0_210:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_209 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rdx,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	(%r14,%rdx,8), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%r14,%rdx,8)
	movb	%al, -1(%rdi)
	incq	%rdx
	cmpq	%rdx, %r10
	jne	.LBB0_210
.LBB0_211:                              # %.prol.loopexit394
                                        #   in Loop: Header=BB0_209 Depth=2
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_214
# BB#212:                               # %.preheader6.us39.i.new
                                        #   in Loop: Header=BB0_209 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	subq	%rdx, %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_213:                              #   Parent Loop BB0_149 Depth=1
                                        #     Parent Loop BB0_209 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rbp
	movzbl	(%rbp,%rcx), %ebx
	movq	-24(%rax), %rbp
	leaq	-1(%rbp), %rsi
	movq	%rsi, -24(%rax)
	movb	%bl, -1(%rbp)
	movq	-16(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	-16(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -16(%rax)
	movb	%bl, -1(%rsi)
	movq	-8(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	-8(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -8(%rax)
	movb	%bl, -1(%rsi)
	movq	(%rdx), %rsi
	movzbl	(%rsi,%rcx), %ebx
	movq	(%rax), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%rax)
	movb	%bl, -1(%rsi)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rdi
	jne	.LBB0_213
.LBB0_214:                              # %._crit_edge35.us47.i
                                        #   in Loop: Header=BB0_209 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB0_209
.LBB0_215:                              # %._crit_edge37.loopexit75.i
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	88(%rsp), %r8           # 8-byte Reload
	addl	40(%rsp), %r8d          # 4-byte Folded Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
.LBB0_216:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jle	.LBB0_235
.LBB0_217:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB0_149 Depth=1
	testl	%edx, %edx
	jle	.LBB0_235
# BB#218:                               # %.preheader10.i
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_226
# BB#219:                               # %.lr.ph49.i
                                        #   in Loop: Header=BB0_149 Depth=1
	testq	%r9, %r9
	movslq	24(%rsp), %r11          # 4-byte Folded Reload
	je	.LBB0_220
# BB#221:                               # %.prol.preheader413
                                        #   in Loop: Header=BB0_149 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_222:                              #   Parent Loop BB0_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rax,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	movq	(%r15,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r9
	jne	.LBB0_222
	jmp	.LBB0_223
	.p2align	4, 0x90
.LBB0_220:                              #   in Loop: Header=BB0_149 Depth=1
	xorl	%eax, %eax
.LBB0_223:                              # %.prol.loopexit414
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_226
# BB#224:                               # %.lr.ph49.i.new
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdi
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	.p2align	4, 0x90
.LBB0_225:                              #   Parent Loop BB0_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-24(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movb	%dl, -1(%rsi)
	movq	-16(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-16(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movb	%dl, -1(%rsi)
	movq	-8(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-8(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movb	%dl, -1(%rsi)
	movq	(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%rdi)
	movb	%dl, -1(%rsi)
	addq	$32, %rdi
	addq	$32, %rax
	addq	$-4, %rcx
	jne	.LBB0_225
.LBB0_226:                              # %.preheader9.i
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_234
# BB#227:                               # %.lr.ph51.i
                                        #   in Loop: Header=BB0_149 Depth=1
	testq	%r10, %r10
	movslq	%ebx, %r11
	je	.LBB0_228
# BB#229:                               # %.prol.preheader418
                                        #   in Loop: Header=BB0_149 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_230:                              #   Parent Loop BB0_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rax,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	movq	(%r14,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r14,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r10
	jne	.LBB0_230
	jmp	.LBB0_231
	.p2align	4, 0x90
.LBB0_228:                              #   in Loop: Header=BB0_149 Depth=1
	xorl	%eax, %eax
.LBB0_231:                              # %.prol.loopexit419
                                        #   in Loop: Header=BB0_149 Depth=1
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_234
# BB#232:                               # %.lr.ph51.i.new
                                        #   in Loop: Header=BB0_149 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdi
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	.p2align	4, 0x90
.LBB0_233:                              #   Parent Loop BB0_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-24(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movb	%dl, -1(%rsi)
	movq	-16(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-16(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movb	%dl, -1(%rsi)
	movq	-8(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	-8(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movb	%dl, -1(%rsi)
	movq	(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movq	(%rdi), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%rdi)
	movb	%dl, -1(%rsi)
	addq	$32, %rdi
	addq	$32, %rax
	addq	$-4, %rcx
	jne	.LBB0_233
.LBB0_234:                              # %._crit_edge52.i
                                        #   in Loop: Header=BB0_149 Depth=1
	addl	$2, %r8d
	cmpl	140(%rsp), %r8d         # 4-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jle	.LBB0_149
	jmp	.LBB0_235
.LBB0_207:                              # %.preheader6.preheader.i
                                        #   in Loop: Header=BB0_149 Depth=1
	addl	%ecx, %r8d
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jg	.LBB0_217
.LBB0_235:                              # %Atracking.exit
	movq	(%r15), %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	320(%rsp), %edx
	cmpl	%edx, %ecx
	jg	.LBB0_237
# BB#236:                               # %Atracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_237
.LBB0_238:                              # %.preheader188
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movl	8(%rsp), %r14d          # 4-byte Reload
	jle	.LBB0_241
# BB#239:                               # %.lr.ph198.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_240:                              # %.lr.ph198
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	Aalign.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_240
.LBB0_241:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_244
# BB#242:                               # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_243:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	movq	Aalign.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_243
.LBB0_244:                              # %._crit_edge
	movss	76(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
	testl	%r15d, %r15d
	jle	.LBB0_5
# BB#6:
	testl	%r13d, %r13d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB0_8
# BB#7:
	movq	Aalign.currentw(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.previousw(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.match(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.m(%rip), %rdi
	callq	FreeFloatVec
	movq	Aalign.mp(%rip), %rdi
	callq	FreeIntVec
	movq	Aalign.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	Aalign.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	Aalign.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	Aalign.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	Aalign.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	Aalign.orlgth1(%rip), %r15d
	movl	Aalign.orlgth2(%rip), %r13d
	jmp	.LBB0_8
.LBB0_41:                               # %._crit_edge219._crit_edge
	testl	%ebx, %ebx
	je	.LBB0_44
# BB#42:                                # %._crit_edge219._crit_edge
	testl	%ebp, %ebp
	je	.LBB0_44
# BB#43:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	Aalign.orlgth1(%rip), %r15d
	movl	commonAlloc1(%rip), %ebx
	movl	Aalign.orlgth2(%rip), %r13d
	movl	commonAlloc2(%rip), %ebp
.LBB0_44:
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	cmpl	%ebp, %r13d
	cmovgel	%r13d, %ebp
	movq	stderr(%rip), %rdi
	leal	1(%rbx), %edx
	leal	1(%rbp), %ecx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	jmp	.LBB0_45
.LBB0_237:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.5, %edi
	callq	ErrorExit
	jmp	.LBB0_238
.LBB0_5:
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_8:
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %ecx
	cmpl	%r15d, %ecx
	cmovgel	%ecx, %r15d
	leal	100(%r15), %edx
	movl	%edx, 48(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	leal	100(%r13), %ebp
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %ecx
	callq	fprintf
	leal	102(%r13), %r14d
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.currentw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.previousw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.match(%rip)
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.initverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.lastverticalw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, Aalign.m(%rip)
	movl	%r14d, %edi
	callq	AllocateIntVec
	movq	%rax, Aalign.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r13,%r15), %esi
	callq	AllocateCharMtx
	movq	%rax, Aalign.mseq(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, Aalign.cpmx1(%rip)
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, Aalign.cpmx2(%rip)
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	%ebp, %eax
	movl	%ebp, %r14d
	cmovgel	%eax, %r14d
	movl	%eax, %ebx
	addl	$2, %r14d
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, Aalign.floatwork(%rip)
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	%rax, Aalign.intwork(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movl	%ebx, Aalign.orlgth1(%rip)
	movl	%ebp, Aalign.orlgth2(%rip)
	movl	%ebp, %r13d
	movl	%ebx, %r15d
	movl	8(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB0_9
.Lfunc_end0:
	.size	Aalign, .Lfunc_end0-Aalign
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB1_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB1_10
# BB#2:                                 # %.preheader70.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader70
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB1_5
	jnp	.LBB1_6
.LBB1_5:                                #   in Loop: Header=BB1_4 Depth=2
	movslq	%r15d, %r15
	movq	(%r9,%r15,8), %rbx
	movss	%xmm1, (%rbx,%r14,4)
	movq	(%r11,%r15,8), %rbx
	movl	%eax, (%rbx,%r14,4)
	incl	%r15d
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB1_7
	jnp	.LBB1_8
.LBB1_7:                                #   in Loop: Header=BB1_4 Depth=2
	movslq	%r15d, %r15
	movq	(%r9,%r15,8), %rbx
	movss	%xmm1, (%rbx,%r14,4)
	movq	(%r11,%r15,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r14,4)
	incl	%r15d
.LBB1_8:                                #   in Loop: Header=BB1_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB1_4
# BB#9:                                 #   in Loop: Header=BB1_3 Depth=1
	movslq	%r15d, %rax
	movq	(%r11,%rax,8), %rax
	movl	$-1, (%rax,%r14,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB1_3
.LBB1_10:                               # %.preheader69
	movl	$n_dis, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rax), %xmm1
	movq	-8(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rax), %xmm0
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB1_12
# BB#13:                                #   in Loop: Header=BB1_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB1_11
# BB#14:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB1_20
# BB#15:                                # %.lr.ph74
	movq	(%r11), %r10
	movl	%r8d, %ecx
	addq	$8, %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_18 Depth 2
	movl	$0, (%rdi,%rdx,4)
	movl	(%r10,%rdx,4), %eax
	testl	%eax, %eax
	js	.LBB1_19
# BB#17:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	xorps	%xmm0, %xmm0
	movq	%r11, %rsi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	movss	-104(%rsp,%rax,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movq	(%rbx), %rax
	mulss	(%rax,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi,%rdx,4)
	movq	(%rsi), %rax
	movl	(%rax,%rdx,4), %eax
	addq	$8, %rbx
	addq	$8, %rsi
	testl	%eax, %eax
	jns	.LBB1_18
.LBB1_19:                               # %._crit_edge
                                        #   in Loop: Header=BB1_16 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jne	.LBB1_16
.LBB1_20:                               # %._crit_edge75
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	match_calc, .Lfunc_end1-match_calc
	.cfi_endproc

	.type	Aalign.mi,@object       # @Aalign.mi
	.local	Aalign.mi
	.comm	Aalign.mi,4,4
	.type	Aalign.m,@object        # @Aalign.m
	.local	Aalign.m
	.comm	Aalign.m,8,8
	.type	Aalign.ijp,@object      # @Aalign.ijp
	.local	Aalign.ijp
	.comm	Aalign.ijp,8,8
	.type	Aalign.mpi,@object      # @Aalign.mpi
	.local	Aalign.mpi
	.comm	Aalign.mpi,4,4
	.type	Aalign.mp,@object       # @Aalign.mp
	.local	Aalign.mp
	.comm	Aalign.mp,8,8
	.type	Aalign.currentw,@object # @Aalign.currentw
	.local	Aalign.currentw
	.comm	Aalign.currentw,8,8
	.type	Aalign.previousw,@object # @Aalign.previousw
	.local	Aalign.previousw
	.comm	Aalign.previousw,8,8
	.type	Aalign.match,@object    # @Aalign.match
	.local	Aalign.match
	.comm	Aalign.match,8,8
	.type	Aalign.initverticalw,@object # @Aalign.initverticalw
	.local	Aalign.initverticalw
	.comm	Aalign.initverticalw,8,8
	.type	Aalign.lastverticalw,@object # @Aalign.lastverticalw
	.local	Aalign.lastverticalw
	.comm	Aalign.lastverticalw,8,8
	.type	Aalign.mseq1,@object    # @Aalign.mseq1
	.local	Aalign.mseq1
	.comm	Aalign.mseq1,8,8
	.type	Aalign.mseq2,@object    # @Aalign.mseq2
	.local	Aalign.mseq2
	.comm	Aalign.mseq2,8,8
	.type	Aalign.mseq,@object     # @Aalign.mseq
	.local	Aalign.mseq
	.comm	Aalign.mseq,8,8
	.type	Aalign.cpmx1,@object    # @Aalign.cpmx1
	.local	Aalign.cpmx1
	.comm	Aalign.cpmx1,8,8
	.type	Aalign.cpmx2,@object    # @Aalign.cpmx2
	.local	Aalign.cpmx2
	.comm	Aalign.cpmx2,8,8
	.type	Aalign.intwork,@object  # @Aalign.intwork
	.local	Aalign.intwork
	.comm	Aalign.intwork,8,8
	.type	Aalign.floatwork,@object # @Aalign.floatwork
	.local	Aalign.floatwork
	.comm	Aalign.floatwork,8,8
	.type	Aalign.orlgth1,@object  # @Aalign.orlgth1
	.local	Aalign.orlgth1
	.comm	Aalign.orlgth1,4,4
	.type	Aalign.orlgth2,@object  # @Aalign.orlgth2
	.local	Aalign.orlgth2
	.comm	Aalign.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\ntrying to allocate (%d+%d)xn matrices ... "
	.size	.L.str, 44

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"succeeded\n"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\ntrying to allocate %dx%d matrices ... "
	.size	.L.str.2, 41

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"succeeded\n\n"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.4, 33

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.5, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
