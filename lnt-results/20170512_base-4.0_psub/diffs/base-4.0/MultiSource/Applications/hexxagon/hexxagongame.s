	.text
	.file	"hexxagongame.bc"
	.globl	_ZN12HexxagonGameC2Ev
	.p2align	4, 0x90
	.type	_ZN12HexxagonGameC2Ev,@function
_ZN12HexxagonGameC2Ev:                  # @_ZN12HexxagonGameC2Ev
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpq	$0, clone_lookups(%rip)
	jne	.LBB0_2
# BB#1:
	callq	_Z16initCloneLookupsv
.LBB0_2:
	cmpq	$0, jump_lookups(%rip)
	jne	.LBB0_4
# BB#3:
	callq	_Z15initJumpLookupsv
.LBB0_4:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	%rbx, %rdi
	callq	_ZN13HexxagonBoard4initEv
	movq	%rbx, 8(%r14)
	movq	%rbx, 16(%r14)
	movl	$1, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN12HexxagonGameC2Ev, .Lfunc_end0-_ZN12HexxagonGameC2Ev
	.cfi_endproc

	.globl	_ZN12HexxagonGame4nextEv
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame4nextEv,@function
_ZN12HexxagonGame4nextEv:               # @_ZN12HexxagonGame4nextEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$0, (%rdi)
	sete	%cl
	movl	%ecx, (%rdi)
	movq	%rax, 16(%rdi)
	xorl	%eax, %eax
	retq
.LBB1_1:
	movl	$-1, %eax
	retq
.Lfunc_end1:
	.size	_ZN12HexxagonGame4nextEv, .Lfunc_end1-_ZN12HexxagonGame4nextEv
	.cfi_endproc

	.globl	_ZN12HexxagonGame4prevEv
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame4prevEv,@function
_ZN12HexxagonGame4prevEv:               # @_ZN12HexxagonGame4prevEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_1
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$0, (%rdi)
	sete	%cl
	movl	%ecx, (%rdi)
	movq	%rax, 16(%rdi)
	xorl	%eax, %eax
	retq
.LBB2_1:
	movl	$-1, %eax
	retq
.Lfunc_end2:
	.size	_ZN12HexxagonGame4prevEv, .Lfunc_end2-_ZN12HexxagonGame4prevEv
	.cfi_endproc

	.globl	_ZN12HexxagonGame5resetEv
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame5resetEv,@function
_ZN12HexxagonGame5resetEv:              # @_ZN12HexxagonGame5resetEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB3_4
# BB#1:                                 # %_ZN12HexxagonGame4prevEv.exit.thread.lr.ph
	movl	(%r14), %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %_ZN12HexxagonGame4prevEv.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	testl	%ecx, %ecx
	sete	%dl
	xorl	%ecx, %ecx
	movb	%dl, %cl
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_2
# BB#3:                                 # %._ZN12HexxagonGame4prevEv.exit_crit_edge
	movq	%rax, 16(%r14)
	movl	%ecx, (%r14)
.LBB3_4:                                # %_ZN12HexxagonGame4prevEv.exit
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB3_7
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_5
# BB#6:                                 # %._crit_edge.loopexit.i
	movq	16(%r14), %rax
.LBB3_7:                                # %_ZN12HexxagonGame11destroyRestEv.exit
	movq	$0, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN12HexxagonGame5resetEv, .Lfunc_end3-_ZN12HexxagonGame5resetEv
	.cfi_endproc

	.globl	_ZN12HexxagonGame11destroyRestEv
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame11destroyRestEv,@function
_ZN12HexxagonGame11destroyRestEv:       # @_ZN12HexxagonGame11destroyRestEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB4_3
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_1
# BB#2:                                 # %._crit_edge.loopexit
	movq	16(%r14), %rax
.LBB4_3:                                # %._crit_edge
	movq	$0, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN12HexxagonGame11destroyRestEv, .Lfunc_end4-_ZN12HexxagonGame11destroyRestEv
	.cfi_endproc

	.globl	_ZN12HexxagonGame9applyMoveER12HexxagonMove
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame9applyMoveER12HexxagonMove,@function
_ZN12HexxagonGame9applyMoveER12HexxagonMove: # @_ZN12HexxagonGame9applyMoveER12HexxagonMove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	16(%r14), %rdx
	movq	%rdx, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN13HexxagonBoardaSERKS_
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB5_1
# BB#2:
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB5_3
.LBB5_1:
	movq	16(%r14), %rax
	movq	%rbx, 16(%rax)
	xorl	%eax, %eax
	cmpl	$0, (%r14)
	sete	%al
	movl	%eax, (%r14)
	movq	%rbx, 16(%r14)
.LBB5_3:                                # %_ZN12HexxagonGame4nextEv.exit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN12HexxagonGame9applyMoveER12HexxagonMove, .Lfunc_end5-_ZN12HexxagonGame9applyMoveER12HexxagonMove
	.cfi_endproc

	.globl	_ZN12HexxagonGame12computerMoveEiPFvvEi
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame12computerMoveEiPFvvEi,@function
_ZN12HexxagonGame12computerMoveEiPFvvEi: # @_ZN12HexxagonGame12computerMoveEiPFvvEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	16(%r14), %rdx
	movq	%rdx, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN13HexxagonBoardaSERKS_
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movl	%r15d, %ecx
	callq	_ZN13HexxagonBoard12computerMoveEiPFvvEi
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB6_1
# BB#2:
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB6_3
.LBB6_1:
	movq	16(%r14), %rax
	movq	%rbx, 16(%rax)
	xorl	%eax, %eax
	cmpl	$0, (%r14)
	sete	%al
	movl	%eax, (%r14)
	movq	%rbx, 16(%r14)
.LBB6_3:                                # %_ZN12HexxagonGame4nextEv.exit
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN12HexxagonGame12computerMoveEiPFvvEi, .Lfunc_end6-_ZN12HexxagonGame12computerMoveEiPFvvEi
	.cfi_endproc

	.globl	_ZN12HexxagonGame8noBoardsEv
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame8noBoardsEv,@function
_ZN12HexxagonGame8noBoardsEv:           # @_ZN12HexxagonGame8noBoardsEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_1
.LBB7_3:                                # %._crit_edge
	retq
.Lfunc_end7:
	.size	_ZN12HexxagonGame8noBoardsEv, .Lfunc_end7-_ZN12HexxagonGame8noBoardsEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.zero	16
	.text
	.globl	_ZN12HexxagonGame8loadGameEPc
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame8loadGameEPc,@function
_ZN12HexxagonGame8loadGameEPc:          # @_ZN12HexxagonGame8loadGameEPc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	subq	$88, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 128
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %rax
	movq	%rdi, %r14
	movl	$.L.str, %esi
	movq	%rax, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB8_1
# BB#2:
	leaq	16(%rsp), %rdi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB8_3
# BB#4:
	leaq	16(%rsp), %rdi
	movl	$.L.str.1, %esi
	movl	$13, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB8_6
# BB#5:
	movq	%rbx, %rdi
	callq	fclose
	movl	$-3, %ebp
	jmp	.LBB8_28
.LBB8_1:
	movl	$-1, %ebp
	jmp	.LBB8_28
.LBB8_6:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB8_3
# BB#7:
	movl	8(%rsp), %r15d
	bswapl	%r15d
	movq	%r15, 8(%rsp)
	movq	8(%r14), %rdi
	movq	%rdi, 16(%r14)
	movq	16(%rdi), %rbp
	testq	%rbp, %rbp
	movq	%rdi, %rax
	je	.LBB8_10
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_8
# BB#9:                                 # %._crit_edge.loopexit.i
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
.LBB8_10:                               # %_ZN12HexxagonGame11destroyRestEv.exit
	movq	$0, 16(%rax)
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#11:
	callq	_ZdlPv
.LBB8_12:
	movq	$0, 8(%r14)
	movl	$1, (%r14)
	testl	%r15d, %r15d
	je	.LBB8_27
# BB#13:                                # %.lr.ph
	xorl	%ebp, %ebp
	jmp	.LBB8_14
	.p2align	4, 0x90
.LBB8_25:                               # %.thread._crit_edge
                                        #   in Loop: Header=BB8_14 Depth=1
	decq	%r15
	movq	8(%r14), %rbp
.LBB8_14:                               # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	callq	_Znwm
	testq	%rbp, %rbp
	je	.LBB8_15
# BB#16:                                #   in Loop: Header=BB8_14 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, 24(%rax)
	movq	$0, 16(%rax)
	movq	%rax, 16(%rcx)
	xorl	%ecx, %ecx
	cmpl	$0, (%r14)
	sete	%cl
	movl	%ecx, (%r14)
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_15:                               #   in Loop: Header=BB8_14 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%r14)
.LBB8_17:                               # %_ZN12HexxagonGame4nextEv.exit
                                        #   in Loop: Header=BB8_14 Depth=1
	movq	%rax, 16(%r14)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN13HexxagonBoard12readFromFileEP8_IO_FILE
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB8_18
# BB#24:                                # %.thread
                                        #   in Loop: Header=BB8_14 Depth=1
	cmpq	$1, %r15
	jne	.LBB8_25
# BB#26:                                # %._crit_edge
	movq	$0, 8(%rsp)
.LBB8_27:
	movq	%rbx, %rdi
	callq	fclose
	xorl	%ebp, %ebp
	jmp	.LBB8_28
.LBB8_3:
	movq	%rbx, %rdi
	callq	fclose
	movl	$-2, %ebp
.LBB8_28:
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_18:
	movq	%r15, 8(%rsp)
	movq	16(%r14), %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB8_20
# BB#19:
	xorl	%eax, %eax
	cmpl	$0, (%r14)
	sete	%al
	movl	%eax, (%r14)
	movq	%rcx, 16(%r14)
	movq	%rcx, %rax
.LBB8_20:                               # %_ZN12HexxagonGame4prevEv.exit
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB8_23
	.p2align	4, 0x90
.LBB8_21:                               # %.lr.ph.i42
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_21
# BB#22:                                # %._crit_edge.loopexit.i44
	movq	16(%r14), %rax
.LBB8_23:
	movq	$0, 16(%rax)
	jmp	.LBB8_28
.Lfunc_end8:
	.size	_ZN12HexxagonGame8loadGameEPc, .Lfunc_end8-_ZN12HexxagonGame8loadGameEPc
	.cfi_endproc

	.globl	_ZN12HexxagonGame8saveGameEPc
	.p2align	4, 0x90
	.type	_ZN12HexxagonGame8saveGameEPc,@function
_ZN12HexxagonGame8saveGameEPc:          # @_ZN12HexxagonGame8saveGameEPc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rsi, %rax
	movq	%rdi, %r14
	movl	$.L.str.2, %esi
	movq	%rax, %rdi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB9_1
# BB#2:
	movl	$.L.str.1, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB9_12
# BB#3:
	movq	8(%r14), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB9_6
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_4
.LBB9_6:                                # %_ZN12HexxagonGame8noBoardsEv.exit
	bswapl	%eax
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB9_12
# BB#7:                                 # %.preheader
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_11
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_10:                               #   in Loop: Header=BB9_11 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB9_9
.LBB9_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN13HexxagonBoard11writeToFileEP8_IO_FILE
	testl	%eax, %eax
	je	.LBB9_10
.LBB9_12:
	movq	%r15, %rdi
	callq	fclose
	movl	$-2, %eax
	jmp	.LBB9_13
.LBB9_1:
	movl	$-1, %eax
	jmp	.LBB9_13
.LBB9_9:                                # %._crit_edge
	movq	%r15, %rdi
	callq	fclose
	xorl	%eax, %eax
.LBB9_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN12HexxagonGame8saveGameEPc, .Lfunc_end9-_ZN12HexxagonGame8saveGameEPc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rb"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Hex2agon 1.1\n"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"wb"
	.size	.L.str.2, 3


	.globl	_ZN12HexxagonGameC1Ev
	.type	_ZN12HexxagonGameC1Ev,@function
_ZN12HexxagonGameC1Ev = _ZN12HexxagonGameC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
