	.text
	.file	"rules-sort.bc"
	.globl	inf_BackwardSortResolution
	.p2align	4, 0x90
	.type	inf_BackwardSortResolution,@function
inf_BackwardSortResolution:             # @inf_BackwardSortResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r8
	movl	68(%r8), %eax
	movl	72(%r8), %ecx
	addl	64(%r8), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_65
.LBB0_2:                                # %.lr.ph203
	movslq	%eax, %r13
	movslq	%ecx, %r15
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movq	%r15, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #       Child Loop BB0_19 Depth 3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_27 Depth 4
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_49 Depth 4
                                        #         Child Loop BB0_52 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_59 Depth 4
	movq	56(%r8), %rax
	movq	(%rax,%r13,8), %rdi
	movq	24(%rdi), %rcx
	movl	(%rcx), %edx
	movl	fol_NOT(%rip), %esi
	cmpl	%edx, %esi
	movq	%rcx, %rax
	jne	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
.LBB0_5:                                # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	testb	$2, (%rdi)
	je	.LBB0_64
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	%edx, %esi
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
.LBB0_8:                                # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%edx, %edx
	jns	.LBB0_64
# BB#9:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	negl	%edx
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%edx, %ecx
	cmpl	$2, %ecx
	jne	.LBB0_64
# BB#10:                                # %clause_LiteralIsSort.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB0_64
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r8, %rbx
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rcx
	callq	st_GetUnifier
	movq	%rax, %rbp
	movq	%rbx, %r8
	testq	%rbp, %rbp
	je	.LBB0_64
# BB#12:                                # %.lr.ph198.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph198
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_19 Depth 3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_27 Depth 4
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_49 Depth 4
                                        #         Child Loop BB0_52 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_59 Depth 4
	movq	8(%rbp), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB0_63
# BB#14:                                # %term_IsAtom.exit
                                        #   in Loop: Header=BB0_13 Depth=2
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB0_63
# BB#15:                                #   in Loop: Header=BB0_13 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jg	.LBB0_63
# BB#16:                                #   in Loop: Header=BB0_13 Depth=2
	movq	%r8, %rbx
	callq	sharing_NAtomDataList
	testq	%rax, %rax
	je	.LBB0_17
# BB#18:                                # %.lr.ph193.preheader
                                        #   in Loop: Header=BB0_13 Depth=2
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph193
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_20 Depth 4
                                        #         Child Loop BB0_27 Depth 4
                                        #         Child Loop BB0_31 Depth 4
                                        #         Child Loop BB0_44 Depth 4
                                        #         Child Loop BB0_49 Depth 4
                                        #         Child Loop BB0_52 Depth 4
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_59 Depth 4
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	56(%rbx), %rcx
	movl	$-1, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edx
	cmpq	%rdi, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_20
# BB#21:                                # %clause_LiteralGetIndex.exit178
                                        #   in Loop: Header=BB0_19 Depth=3
	movl	64(%rbx), %r12d
	cmpl	%r12d, %edx
	jge	.LBB0_62
# BB#22:                                #   in Loop: Header=BB0_19 Depth=3
	testb	$1, 48(%rbx)
	je	.LBB0_62
# BB#23:                                #   in Loop: Header=BB0_19 Depth=3
	movq	24(%rdi), %rsi
	movl	fol_NOT(%rip), %edx
	cmpl	(%rsi), %edx
	jne	.LBB0_24
# BB#25:                                #   in Loop: Header=BB0_19 Depth=3
	movq	16(%rsi), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_26
.LBB0_24:                               #   in Loop: Header=BB0_19 Depth=3
	movq	%rsi, 24(%rsp)          # 8-byte Spill
.LBB0_26:                               # %clause_LiteralAtom.exit171
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	$-1, %rdx
	xorl	%esi, %esi
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_27:                               #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%esi, %ebx
	addq	%r14, %rbp
	leal	1(%rbx), %esi
	cmpq	%rdi, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB0_27
# BB#28:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	sarq	$32, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	$0, (%r14)
	testl	%r12d, %r12d
	jle	.LBB0_29
# BB#30:                                # %.lr.ph
                                        #   in Loop: Header=BB0_19 Depth=3
	movl	%ebx, %r13d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%r15, %r13
	je	.LBB0_36
# BB#32:                                #   in Loop: Header=BB0_31 Depth=4
	movq	56(%rdx), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_31 Depth=4
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB0_34:                               # %clause_LiteralAtom.exit159
                                        #   in Loop: Header=BB0_31 Depth=4
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rsi), %rcx
	cmpq	8(%rcx), %rax
	jne	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_31 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	%r14, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rbp, %r14
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_36:                               #   in Loop: Header=BB0_31 Depth=4
	incq	%r15
	cmpq	%r15, %r12
	jne	.LBB0_31
	jmp	.LBB0_37
.LBB0_29:                               #   in Loop: Header=BB0_19 Depth=3
	xorl	%ebx, %ebx
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB0_37:                               # %._crit_edge
                                        #   in Loop: Header=BB0_19 Depth=3
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_40
# BB#38:                                #   in Loop: Header=BB0_19 Depth=3
	movq	%rdx, %rdi
	movq	%r14, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	callq	inf_SubsortPrecheck
	movq	16(%rsp), %rdx          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_39
.LBB0_40:                               #   in Loop: Header=BB0_19 Depth=3
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rdx, %r15
	callq	clause_Copy
	movq	%rax, %rbp
	movl	52(%r15), %r15d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r12)
	movq	$0, (%r12)
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	clause_RenameVarsBiggerThan
	movq	%rbp, %r15
	movq	56(%rbp), %rax
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_19 Depth=3
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB0_42:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rcx
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	88(%rsp), %rsi
	leaq	128(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB0_45
# BB#43:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_19 Depth=3
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph.i150
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB0_44
.LBB0_45:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	128(%rsp), %rdi
	callq	subst_Delete
	movq	88(%rsp), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movq	%r12, %r8
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	inf_ConstraintHyperResolvents
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_46
# BB#47:                                #   in Loop: Header=BB0_19 Depth=3
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB0_51
# BB#48:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_49:                               # %.preheader.i
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_49
# BB#50:                                #   in Loop: Header=BB0_19 Depth=3
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	jmp	.LBB0_51
.LBB0_46:                               #   in Loop: Header=BB0_19 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_51:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	88(%rsp), %rdi
	callq	subst_Delete
	testq	%r12, %r12
	je	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph.i148
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB0_52
.LBB0_53:                               # %list_Delete.exit149
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	%r15, %rdi
	callq	clause_Delete
	movq	40(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_54
.LBB0_39:                               #   in Loop: Header=BB0_19 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB0_54:                               #   in Loop: Header=BB0_19 Depth=3
	testq	%r14, %r14
	movq	120(%rsp), %rax         # 8-byte Reload
	je	.LBB0_56
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph.i143
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r14), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r14
	jne	.LBB0_55
.LBB0_56:                               # %list_Delete.exit144
                                        #   in Loop: Header=BB0_19 Depth=3
	testq	%rbx, %rbx
	je	.LBB0_57
# BB#58:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_19 Depth=3
	movabsq	$4294967296, %r14       # imm = 0x100000000
	.p2align	4, 0x90
.LBB0_59:                               # %.lr.ph.i
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        #       Parent Loop BB0_19 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rbx
	jne	.LBB0_59
# BB#60:                                #   in Loop: Header=BB0_19 Depth=3
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	64(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_61
.LBB0_57:                               #   in Loop: Header=BB0_19 Depth=3
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	64(%rsp), %r15          # 8-byte Reload
	movabsq	$4294967296, %r14       # imm = 0x100000000
.LBB0_61:                               # %list_Delete.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB0_62:                               # %list_Delete.exit
                                        #   in Loop: Header=BB0_19 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_19
	jmp	.LBB0_63
.LBB0_17:                               #   in Loop: Header=BB0_13 Depth=2
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB0_63:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB0_13 Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_13
	.p2align	4, 0x90
.LBB0_64:                               # %clause_LiteralIsSort.exit.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%r15, %r13
	leaq	1(%r13), %r13
	jl	.LBB0_3
.LBB0_65:                               # %._crit_edge204
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	inf_BackwardSortResolution, .Lfunc_end0-inf_BackwardSortResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_SubsortPrecheck,@function
inf_SubsortPrecheck:                    # @inf_SubsortPrecheck
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movslq	8(%rbx), %rax
	movq	56(%r13), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rdi
	movq	%rcx, %rsi
	callq	inf_GetForwardPartnerLits
	movq	%rax, %r15
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB1_15
# BB#1:                                 # %.lr.ph.i54.preheader
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph.i54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	movq	8(%rbp), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB1_4:                                # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r12, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB1_10
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	testq	%r14, %r14
	je	.LBB1_9
# BB#6:                                 # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB1_7:                                # %.preheader.i.i.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%r14, (%rcx)
.LBB1_9:                                # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %r14
.LBB1_10:                               # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_2
# BB#11:                                # %.lr.ph.i.preheader
	movq	%r14, %rdi
	callq	list_PointerDeleteDuplicates
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB1_12
# BB#13:                                # %list_Delete.exit
	testq	%rbx, %rbx
	jne	.LBB1_16
# BB#14:
	xorl	%ebp, %ebp
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB1_26
.LBB1_15:                               # %list_Delete.exit.thread
	xorl	%edi, %edi
	callq	list_PointerDeleteDuplicates
.LBB1_16:                               # %.lr.ph.preheader
	movq	(%rsp), %r15            # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
	movslq	8(%rbx), %rax
	movq	56(%r13), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_17 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB1_19:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%r12, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB1_25
# BB#20:                                #   in Loop: Header=BB1_17 Depth=1
	testq	%rbp, %rbp
	je	.LBB1_24
# BB#21:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB1_22:                               # %.preheader.i.i
                                        #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_22
# BB#23:                                #   in Loop: Header=BB1_17 Depth=1
	movq	%rbp, (%rcx)
.LBB1_24:                               # %sort_Intersect.exit
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%rax, %rbp
.LBB1_25:                               # %sort_Intersect.exit
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_17
.LBB1_26:                               # %._crit_edge
	movq	%rbp, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %r13
	testq	%r15, %r15
	je	.LBB1_27
# BB#28:
	movq	24(%r15), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB1_30
# BB#29:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB1_30:                               # %clause_LiteralPredicate.exit
	movq	%r12, %rdi
	callq	sort_TheorySortOfSymbol
	movq	%rax, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	sort_TheoryIsSubsortOfExtra
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	sort_Delete
	jmp	.LBB1_31
.LBB1_27:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	sort_TheoryIsSubsortOf
	movl	%eax, %ebp
.LBB1_31:
	movq	%r13, %rdi
	callq	sort_Delete
	movq	%r14, %rdi
	callq	sort_Delete
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	inf_SubsortPrecheck, .Lfunc_end1-inf_SubsortPrecheck
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ConstraintHyperResolvents,@function
inf_ConstraintHyperResolvents:          # @inf_ConstraintHyperResolvents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 192
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %rsi
	movabsq	$4294967296, %r12       # imm = 0x100000000
	testq	%rcx, %rcx
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	je	.LBB2_1
# BB#94:
	movslq	8(%rcx), %rax
	movq	56(%rsi), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_96
# BB#95:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_96:                               # %clause_LiteralAtom.exit
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	callq	st_GetUnifier
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_97
# BB#98:                                # %.lr.ph43.i
	movl	symbol_TYPEMASK(%rip), %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_99:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_103 Depth 2
                                        #       Child Loop BB2_109 Depth 3
	movq	8(%r15), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB2_113
# BB#100:                               # %term_IsAtom.exit.i
                                        #   in Loop: Header=BB2_99 Depth=1
	negl	%eax
	andl	%r14d, %eax
	cmpl	$2, %eax
	jne	.LBB2_113
# BB#101:                               #   in Loop: Header=BB2_99 Depth=1
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB2_103
	jmp	.LBB2_113
.LBB2_111:                              # %.loopexit.i84
                                        #   in Loop: Header=BB2_103 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB2_112:                              # %list_PointerMember.exit.i85
                                        #   in Loop: Header=BB2_103 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB2_113
.LBB2_103:                              # %.lr.ph.i75
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_109 Depth 3
	movq	8(%rbp), %rbx
	movq	24(%rbx), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	je	.LBB2_112
# BB#104:                               #   in Loop: Header=BB2_103 Depth=2
	testb	$2, (%rbx)
	je	.LBB2_112
# BB#105:                               #   in Loop: Header=BB2_103 Depth=2
	movq	16(%rbx), %rdi
	testb	$1, 48(%rdi)
	je	.LBB2_112
# BB#106:                               #   in Loop: Header=BB2_103 Depth=2
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB2_112
# BB#107:                               #   in Loop: Header=BB2_103 Depth=2
	testq	%r13, %r13
	je	.LBB2_111
# BB#108:                               # %.lr.ph.i.i81.preheader
                                        #   in Loop: Header=BB2_103 Depth=2
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB2_109:                              # %.lr.ph.i.i81
                                        #   Parent Loop BB2_99 Depth=1
                                        #     Parent Loop BB2_103 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB2_112
# BB#110:                               #   in Loop: Header=BB2_109 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_109
	jmp	.LBB2_111
	.p2align	4, 0x90
.LBB2_113:                              # %term_IsAtom.exit.thread.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB2_99
	jmp	.LBB2_114
.LBB2_1:
	movl	8(%rsi), %r13d
	testq	%r8, %r8
	je	.LBB2_4
# BB#2:                                 # %.lr.ph489.i.preheader
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph489.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	movl	8(%rax), %esi
	movl	%r13d, %edi
	callq	misc_Max
	movl	%eax, %r13d
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
.LBB2_4:                                # %._crit_edge490.i
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	clause_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	clause_SubstApply
	movq	72(%rsp), %rdx          # 8-byte Reload
	movslq	8(%rdx), %rax
	movq	56(%r14), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jne	.LBB2_6
# BB#5:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB2_6:                                # %clause_GetLiteralAtom.exit325.i
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	64(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.LBB2_7
# BB#8:                                 # %.lr.ph480.i
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph480.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.i386.i
                                        #   Parent Loop BB2_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 8(%rax)
	je	.LBB2_15
# BB#11:                                #   in Loop: Header=BB2_10 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_10
# BB#12:                                # %.loopexit.i
                                        #   in Loop: Header=BB2_9 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_9 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_14:                               # %clause_GetLiteralAtom.exit400.i
                                        #   in Loop: Header=BB2_9 Depth=1
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, %rcx
	movq	%rax, %r15
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_15:                               # %list_PointerMember.exit390.i
                                        #   in Loop: Header=BB2_9 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, %r14
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
.LBB2_16:                               #   in Loop: Header=BB2_9 Depth=1
	movq	%r14, 8(%rax)
	movq	%rcx, (%rax)
	incq	%rbx
	cmpq	%r12, %rbx
	movq	72(%rsp), %rdx          # 8-byte Reload
	jne	.LBB2_9
# BB#17:                                # %._crit_edge481.i.loopexit
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	64(%rcx), %eax
	jmp	.LBB2_18
.LBB2_97:
	xorl	%r13d, %r13d
.LBB2_114:                              # %inf_GetSortResolutionPartnerLits.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	52(%rax), %ebx
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_AtomMaxVar
	cmpl	%eax, %ebx
	cmovgel	%ebx, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	testq	%r13, %r13
	je	.LBB2_115
# BB#116:                               # %.lr.ph
	xorl	%ecx, %ecx
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB2_117:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_118 Depth 2
                                        #     Child Loop BB2_123 Depth 2
                                        #     Child Loop BB2_128 Depth 2
	movq	%rcx, %r15
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	8(%rax), %rbx
	movq	16(%rbx), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_118:                              #   Parent Loop BB2_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r12, %rbp
	cmpq	%rbx, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB2_118
# BB#119:                               # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB2_117 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	%r14, (%r13)
	movq	16(%rbx), %rdi
	callq	clause_Copy
	movq	%rax, %r14
	movq	%r14, %rdi
	movl	48(%rsp), %esi          # 4-byte Reload
	callq	clause_RenameVarsBiggerThan
	movq	56(%r14), %rax
	sarq	$29, %rbp
	movq	(%rax,%rbp), %rbx
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB2_121
# BB#120:                               #   in Loop: Header=BB2_117 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB2_121:                              # %clause_LiteralAtom.exit102
                                        #   in Loop: Header=BB2_117 Depth=1
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rbp
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	56(%rsp), %rsi
	leaq	64(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB2_124
# BB#122:                               # %.lr.ph.preheader.i104
                                        #   in Loop: Header=BB2_117 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB2_123:                              # %.lr.ph.i105
                                        #   Parent Loop BB2_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB2_123
.LBB2_124:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB2_117 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	64(%rsp), %rdi
	callq	subst_Delete
	movq	56(%rsp), %r12
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	subst_Copy
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Compose
	movq	%rax, 56(%rsp)
	movq	(%rbx), %rcx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rdx
	movq	%r13, %r8
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	200(%rsp)
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	inf_ConstraintHyperResolvents
	addq	$16, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_125
# BB#126:                               #   in Loop: Header=BB2_117 Depth=1
	testq	%rbp, %rbp
	je	.LBB2_130
# BB#127:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB2_117 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB2_128:                              # %.preheader.i
                                        #   Parent Loop BB2_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_128
# BB#129:                               #   in Loop: Header=BB2_117 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB2_130
	.p2align	4, 0x90
.LBB2_125:                              #   in Loop: Header=BB2_117 Depth=1
	movq	%rbp, %r15
.LBB2_130:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB2_117 Depth=1
	movq	56(%rsp), %rdi
	callq	subst_Delete
	movq	%r12, %rdi
	callq	subst_Delete
	movq	%r14, %rdi
	callq	clause_Delete
	movq	(%r13), %r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r13, (%rax)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%r15, %rcx
	movabsq	$4294967296, %r12       # imm = 0x100000000
	jne	.LBB2_117
	jmp	.LBB2_131
.LBB2_115:
	xorl	%r15d, %r15d
.LBB2_131:                              # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	jmp	.LBB2_132
.LBB2_7:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r12d, %eax
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB2_18:                               # %._crit_edge481.i
	movl	68(%rcx), %ecx
	leal	-1(%rax,%rcx), %edx
	cmpl	%edx, %r12d
	movl	%r13d, 92(%rsp)         # 4-byte Spill
	jle	.LBB2_20
# BB#19:
	xorl	%r14d, %r14d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB2_25
.LBB2_20:                               # %.lr.ph471.i
	movslq	%r12d, %r12
	movslq	%edx, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_21:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_23:                               # %clause_GetLiteralAtom.exit382.i
                                        #   in Loop: Header=BB2_21 Depth=1
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	%rbp, (%r14)
	cmpq	%r13, %r12
	leaq	1(%r12), %r12
	movq	%r14, %rbp
	jl	.LBB2_21
# BB#24:                                # %._crit_edge472.loopexit.i
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	64(%rdx), %eax
	movl	68(%rdx), %ecx
.LBB2_25:                               # %._crit_edge472.i
	movl	72(%rdx), %edx
	addl	%eax, %ecx
	leal	-1(%rdx,%rcx), %eax
	cmpl	%eax, %r12d
	jle	.LBB2_27
# BB#26:
	xorl	%r13d, %r13d
	jmp	.LBB2_31
.LBB2_27:                               # %.lr.ph465.i
	movslq	%r12d, %rbp
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	decq	%rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_28 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_30:                               # %clause_GetLiteralAtom.exit372.i
                                        #   in Loop: Header=BB2_28 Depth=1
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	%r12, (%r13)
	incq	%rbp
	cmpq	40(%rsp), %rbp          # 8-byte Folded Reload
	movq	%r13, %r12
	jl	.LBB2_28
.LBB2_31:                               # %._crit_edge466.i
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rdx
	testq	%rdx, %rdx
	jle	.LBB2_87
# BB#32:                                # %.lr.ph454.i
	xorl	%esi, %esi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_35 Depth 2
                                        #     Child Loop BB2_40 Depth 2
                                        #     Child Loop BB2_42 Depth 2
                                        #     Child Loop BB2_44 Depth 2
                                        #     Child Loop BB2_46 Depth 2
                                        #     Child Loop BB2_51 Depth 2
                                        #     Child Loop BB2_57 Depth 2
                                        #     Child Loop BB2_61 Depth 2
                                        #     Child Loop BB2_77 Depth 2
	testq	%rcx, %rcx
	je	.LBB2_86
# BB#34:                                # %.lr.ph.i361.i.preheader
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph.i361.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, 8(%rax)
	je	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_35 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_35
	jmp	.LBB2_86
	.p2align	4, 0x90
.LBB2_37:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	56(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	movq	24(%rax), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	jne	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_33 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB2_39:                               # %clause_GetLiteralAtom.exit360.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	clause_CreateUnnormalized
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	je	.LBB2_41
	.p2align	4, 0x90
.LBB2_40:                               # %.lr.ph.i349.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB2_40
.LBB2_41:                               # %list_Delete.exit350.i
                                        #   in Loop: Header=BB2_33 Depth=1
	testq	%r14, %r14
	je	.LBB2_43
	.p2align	4, 0x90
.LBB2_42:                               # %.lr.ph.i344.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB2_42
.LBB2_43:                               # %list_Delete.exit345.i
                                        #   in Loop: Header=BB2_33 Depth=1
	testq	%r13, %r13
	je	.LBB2_45
	.p2align	4, 0x90
.LBB2_44:                               # %.lr.ph.i339.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB2_44
.LBB2_45:                               # %list_Delete.exit340.preheader.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	(%rbx), %ecx
	movl	fol_NOT(%rip), %edx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movabsq	$4294967296, %r8        # imm = 0x100000000
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_49:                               #   in Loop: Header=BB2_46 Depth=2
	movq	(%rsi), %rsi
.LBB2_46:                               # %list_Delete.exit340.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rax
	movq	24(%rax), %rbp
	movl	(%rbp), %edi
	cmpl	%edi, %edx
	jne	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_46 Depth=2
	movq	16(%rbp), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %edi
.LBB2_48:                               # %clause_LiteralAtom.exit335.i
                                        #   in Loop: Header=BB2_46 Depth=2
	cmpl	%edi, %ecx
	jne	.LBB2_49
# BB#50:                                #   in Loop: Header=BB2_33 Depth=1
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	16(%rax), %rdi
	movq	56(%rdi), %rcx
	movq	$-1, %rdx
	xorl	%esi, %esi
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_51:                               #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ebp
	addq	%r8, %rbx
	leal	1(%rbp), %esi
	cmpq	%rax, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB2_51
# BB#52:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB2_33 Depth=1
	callq	clause_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r12, 8(%r15)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movslq	(%r12), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%r14, 8(%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%rbx, %r14
	sarq	$32, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%r14, 8(%rcx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rax, (%rcx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_SearchMaxVar
	movl	%eax, %r14d
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	clause_SearchMaxVar
	cmpl	%eax, %r14d
	cmovgel	%r14d, %eax
	movq	%r12, %rdi
	movl	%eax, %esi
	callq	clause_RenameVarsBiggerThan
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	56(%r12), %rax
	sarq	$29, %rbx
	movq	(%rax,%rbx), %rax
	movq	24(%rax), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB2_54
# BB#53:                                #   in Loop: Header=BB2_33 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB2_54:                               # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB2_33 Depth=1
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbx, %rsi
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB2_133
# BB#55:                                #   in Loop: Header=BB2_33 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	64(%rsp), %rsi
	leaq	56(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB2_58
# BB#56:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB2_57:                               # %.lr.ph.i315.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB2_57
.LBB2_58:                               # %cont_Reset.exit.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%r15, 120(%rsp)         # 8-byte Spill
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	56(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	clause_SubstApply
	movq	56(%rsp), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	clause_SubstApply
	movq	56(%rsp), %rdi
	callq	subst_Delete
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	64(%rsi), %eax
	movl	68(%rsi), %ecx
	leal	(%rcx,%rax), %edx
	addl	72(%rsi), %edx
	jle	.LBB2_59
# BB#60:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB2_33 Depth=1
	decl	%eax
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%edx, %r12d
	movl	%ebp, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_61:                               # %.lr.ph.i
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jle	.LBB2_62
# BB#65:                                #   in Loop: Header=BB2_61 Depth=2
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jle	.LBB2_66
# BB#69:                                #   in Loop: Header=BB2_61 Depth=2
	cmpq	%rbp, 48(%rsp)          # 8-byte Folded Reload
	je	.LBB2_74
# BB#70:                                #   in Loop: Header=BB2_61 Depth=2
	movq	64(%rsp), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_61 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_72:                               # %clause_GetLiteralAtom.exit294.i
                                        #   in Loop: Header=BB2_61 Depth=2
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, %rcx
	movq	%rax, %r13
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_62:                               #   in Loop: Header=BB2_61 Depth=2
	movq	64(%rsp), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_64
# BB#63:                                #   in Loop: Header=BB2_61 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_64:                               # %clause_GetLiteralAtom.exit314.i
                                        #   in Loop: Header=BB2_61 Depth=2
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, %rcx
	movq	%rax, %r15
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_66:                               #   in Loop: Header=BB2_61 Depth=2
	movq	64(%rsp), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_61 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_68:                               # %clause_GetLiteralAtom.exit304.i
                                        #   in Loop: Header=BB2_61 Depth=2
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, %rcx
	movq	%rax, %r14
.LBB2_73:                               # %.sink.split.i
                                        #   in Loop: Header=BB2_61 Depth=2
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
.LBB2_74:                               #   in Loop: Header=BB2_61 Depth=2
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB2_61
	jmp	.LBB2_75
.LBB2_59:                               #   in Loop: Header=BB2_33 Depth=1
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.LBB2_75:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	64(%rsp), %rdi
	callq	subst_Delete
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	64(%rsi), %eax
	movl	68(%rsi), %ecx
	leal	(%rcx,%rax), %edx
	addl	72(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB2_85
# BB#76:                                # %.lr.ph441.i
                                        #   in Loop: Header=BB2_33 Depth=1
	decl	%eax
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%edx, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_77:                               #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jle	.LBB2_78
# BB#81:                                #   in Loop: Header=BB2_77 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_83
# BB#82:                                #   in Loop: Header=BB2_77 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_83:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB2_77 Depth=2
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%r14, %rcx
	cmovgq	%r13, %rcx
	cmovgq	%rax, %r13
	cmovleq	%rax, %r14
	jmp	.LBB2_84
	.p2align	4, 0x90
.LBB2_78:                               #   in Loop: Header=BB2_77 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB2_80
# BB#79:                                #   in Loop: Header=BB2_77 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB2_80:                               # %clause_GetLiteralAtom.exit284.i
                                        #   in Loop: Header=BB2_77 Depth=2
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, %rcx
	movq	%rax, %r15
.LBB2_84:                               #   in Loop: Header=BB2_77 Depth=2
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB2_77
.LBB2_85:                               # %._crit_edge442.i
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	clause_Delete
	decl	clause_CLAUSECOUNTER(%rip)
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
.LBB2_86:                               # %list_PointerMember.exit.thread.i
                                        #   in Loop: Header=BB2_33 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB2_33
.LBB2_87:                               # %._crit_edge455.i
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	192(%rsp), %rcx
	movq	200(%rsp), %r8
	callq	clause_Create
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB2_89
	.p2align	4, 0x90
.LBB2_88:                               # %.lr.ph.i273.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB2_88
.LBB2_89:                               # %list_Delete.exit274.i
	testq	%r14, %r14
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB2_91
	.p2align	4, 0x90
.LBB2_90:                               # %.lr.ph.i268.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB2_90
.LBB2_91:                               # %list_Delete.exit269.i
	testq	%r13, %r13
	je	.LBB2_93
	.p2align	4, 0x90
.LBB2_92:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB2_92
.LBB2_93:                               # %inf_BuildConstraintHyperResolvent.exit
	xorl	%eax, %eax
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	setle	%al
	incl	%eax
	movl	%eax, 76(%rbx)
	movl	92(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 8(%rbx)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	clause_SetSplitDataFromList
	movq	%rbp, %rdi
	callq	clause_DeleteClauseList
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	list_NReverse
	movq	%rax, 32(%rbx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_NReverse
	movq	%rax, 40(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
.LBB2_132:
	movq	%r15, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_133:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$258, %ecx              # imm = 0x102
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end2:
	.size	inf_ConstraintHyperResolvents, .Lfunc_end2-inf_ConstraintHyperResolvents
	.cfi_endproc

	.globl	inf_ForwardSortResolution
	.p2align	4, 0x90
	.type	inf_ForwardSortResolution,@function
inf_ForwardSortResolution:              # @inf_ForwardSortResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 112
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movslq	64(%rdi), %r10
	xorl	%ebx, %ebx
	testq	%r10, %r10
	jle	.LBB3_24
# BB#1:                                 # %.lr.ph
	movq	%r10, %r13
	decq	%r13
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	56(%rdi), %r11
	movl	fol_NOT(%rip), %eax
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rdi
	movq	(%r11,%rdi,8), %rdi
	movq	24(%rdi), %rdi
	cmpl	(%rdi), %eax
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rdi), %rdi
	movq	8(%rdi), %rdi
.LBB3_4:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rdi), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %ebp
	xorl	%edi, %edi
	testl	%ebp, %ebp
	setg	%dil
	addl	%edi, %ebx
	cmpl	%r10d, %ebx
	jge	.LBB3_6
# BB#5:                                 # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	testb	%dil, %dil
	jne	.LBB3_2
.LBB3_6:                                # %.critedge
	testl	%ebp, %ebp
	jle	.LBB3_8
# BB#7:
	xorl	%ebx, %ebx
	jmp	.LBB3_24
.LBB3_8:
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	56(%rbp), %rax
	movslq	%ebx, %r14
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %r12
	movl	fol_NOT(%rip), %eax
	cmpl	(%r12), %eax
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	jne	.LBB3_10
# BB#9:
	movq	16(%r12), %rax
	movq	8(%rax), %r12
.LBB3_10:                               # %clause_GetLiteralAtom.exit87
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_11 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %ebx
.LBB3_11:                               # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r14, 8(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movl	fol_NOT(%rip), %eax
	movslq	%ebx, %r14
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r13, %r14
	jge	.LBB3_17
# BB#13:                                #   in Loop: Header=BB3_12 Depth=2
	movq	56(%rbp), %rcx
	movq	8(%rcx,%r14,8), %rcx
	movq	24(%rcx), %rcx
	cmpl	(%rcx), %eax
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_12 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
.LBB3_15:                               # %clause_GetLiteralAtom.exit77
                                        #   in Loop: Header=BB3_12 Depth=2
	incq	%r14
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	16(%r12), %rdx
	cmpq	8(%rdx), %rcx
	jne	.LBB3_12
	jmp	.LBB3_16
.LBB3_17:
	movq	%r15, %rdi
	callq	list_Copy
	movq	%rax, %rbp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB3_19
# BB#18:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	callq	inf_SubsortPrecheck
	testl	%eax, %eax
	je	.LBB3_20
.LBB3_19:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbp, %rcx
	movq	%r14, %r9
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	inf_ConstraintHyperResolvents
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
.LBB3_20:
	testq	%rbp, %rbp
	je	.LBB3_22
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph.i66
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB3_21
.LBB3_22:                               # %list_Delete.exit67
	testq	%r15, %r15
	je	.LBB3_24
	.p2align	4, 0x90
.LBB3_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB3_23
.LBB3_24:                               # %list_Delete.exit
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	inf_ForwardSortResolution, .Lfunc_end3-inf_ForwardSortResolution
	.cfi_endproc

	.globl	inf_BackwardEmptySort
	.p2align	4, 0x90
	.type	inf_BackwardEmptySort,@function
inf_BackwardEmptySort:                  # @inf_BackwardEmptySort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 192
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	68(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	64(%rbx), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB4_2
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB4_76
.LBB4_2:                                # %.lr.ph250
	movslq	%eax, %rbp
	movslq	%ecx, %r12
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_20 Depth 3
                                        #         Child Loop BB4_21 Depth 4
                                        #         Child Loop BB4_29 Depth 4
                                        #         Child Loop BB4_32 Depth 4
                                        #         Child Loop BB4_42 Depth 4
                                        #         Child Loop BB4_55 Depth 4
                                        #         Child Loop BB4_60 Depth 4
                                        #         Child Loop BB4_63 Depth 4
                                        #         Child Loop BB4_66 Depth 4
                                        #         Child Loop BB4_70 Depth 4
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	24(%rdi), %rcx
	movl	(%rcx), %edx
	movl	fol_NOT(%rip), %esi
	cmpl	%edx, %esi
	movq	%rcx, %rax
	jne	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
.LBB4_5:                                # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	testb	$2, (%rdi)
	je	.LBB4_75
# BB#6:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpl	%edx, %esi
	jne	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
.LBB4_8:                                # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB4_3 Depth=1
	testl	%edx, %edx
	jns	.LBB4_75
# BB#9:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB4_3 Depth=1
	negl	%edx
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%edx, %ecx
	cmpl	$2, %ecx
	jne	.LBB4_75
# BB#10:                                # %clause_LiteralIsSort.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB4_75
# BB#11:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%r8, %r15
	movq	%r9, %r14
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB4_12
# BB#13:                                # %.lr.ph245.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r14, %r9
	movq	%r15, %r8
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph245
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_20 Depth 3
                                        #         Child Loop BB4_21 Depth 4
                                        #         Child Loop BB4_29 Depth 4
                                        #         Child Loop BB4_32 Depth 4
                                        #         Child Loop BB4_42 Depth 4
                                        #         Child Loop BB4_55 Depth 4
                                        #         Child Loop BB4_60 Depth 4
                                        #         Child Loop BB4_63 Depth 4
                                        #         Child Loop BB4_66 Depth 4
                                        #         Child Loop BB4_70 Depth 4
	movq	8(%rax), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jns	.LBB4_74
# BB#15:                                # %term_IsAtom.exit
                                        #   in Loop: Header=BB4_14 Depth=2
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB4_74
# BB#16:                                #   in Loop: Header=BB4_14 Depth=2
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	cmpl	$0, (%rcx)
	jle	.LBB4_74
# BB#17:                                #   in Loop: Header=BB4_14 Depth=2
	movq	%r8, %r15
	movq	%r9, %r14
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rsi
	testq	%rsi, %rsi
	je	.LBB4_18
# BB#19:                                # %.lr.ph240.preheader
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r14, %r9
	movq	%r15, %r8
	.p2align	4, 0x90
.LBB4_20:                               # %.lr.ph240
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_21 Depth 4
                                        #         Child Loop BB4_29 Depth 4
                                        #         Child Loop BB4_32 Depth 4
                                        #         Child Loop BB4_42 Depth 4
                                        #         Child Loop BB4_55 Depth 4
                                        #         Child Loop BB4_60 Depth 4
                                        #         Child Loop BB4_63 Depth 4
                                        #         Child Loop BB4_66 Depth 4
                                        #         Child Loop BB4_70 Depth 4
	movq	8(%rsi), %r13
	movq	16(%r13), %r14
	movq	56(%r14), %rcx
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB4_21:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%edx
	cmpq	%r13, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB4_21
# BB#22:                                # %clause_LiteralGetIndex.exit218
                                        #   in Loop: Header=BB4_20 Depth=3
	cmpl	64(%r14), %edx
	jge	.LBB4_73
# BB#23:                                #   in Loop: Header=BB4_20 Depth=3
	testb	$1, 48(%r14)
	je	.LBB4_73
# BB#24:                                #   in Loop: Header=BB4_20 Depth=3
	movq	%r12, %r15
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%r8, %rsi
	movq	%r9, %rdx
	movq	%r8, %r12
	movq	%r9, %rbx
	callq	clause_HasOnlyVarsInConstraint
	testl	%eax, %eax
	je	.LBB4_25
# BB#26:                                #   in Loop: Header=BB4_20 Depth=3
	movq	24(%r13), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	jne	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_20 Depth=3
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
.LBB4_28:                               # %clause_LiteralAtom.exit211
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %r15d
	movslq	64(%r14), %r12
	movl	%r12d, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	16(%r13), %rcx
	movq	56(%rcx), %rcx
	movq	$-1, %rdx
	xorl	%esi, %esi
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB4_29:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%esi, %ebx
	addq	%rdi, %rbp
	leal	1(%rbx), %esi
	cmpq	%r13, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB4_29
# BB#30:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	movl	72(%r14), %ecx
	movl	68(%r14), %edx
	addl	%r12d, %edx
	leal	-1(%rcx,%rdx), %ecx
	cmpl	%ecx, 8(%rsp)           # 4-byte Folded Reload
	jg	.LBB4_39
# BB#31:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_20 Depth=3
	movslq	%ecx, %r13
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_36:                               # %clause_GetLiteralAtom.exit199..lr.ph_crit_edge
                                        #   in Loop: Header=BB4_32 Depth=4
	incq	%r12
	movl	fol_NOT(%rip), %eax
.LBB4_32:                               # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	56(%r14), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	24(%rcx), %rdi
	cmpl	(%rdi), %eax
	jne	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_32 Depth=4
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB4_34:                               # %clause_GetLiteralAtom.exit199
                                        #   in Loop: Header=BB4_32 Depth=4
	movl	%r15d, %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB4_37
# BB#35:                                # %clause_GetLiteralAtom.exit199
                                        #   in Loop: Header=BB4_32 Depth=4
	cmpq	%r13, %r12
	jl	.LBB4_36
.LBB4_37:                               # %._crit_edge
                                        #   in Loop: Header=BB4_20 Depth=3
	testl	%eax, %eax
	je	.LBB4_39
# BB#38:                                #   in Loop: Header=BB4_20 Depth=3
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB4_73
.LBB4_39:                               # %.critedge
                                        #   in Loop: Header=BB4_20 Depth=3
	sarq	$32, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbp, 8(%r12)
	movq	$0, (%r12)
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB4_40
# BB#41:                                # %.lr.ph233.preheader
                                        #   in Loop: Header=BB4_20 Depth=3
	movl	%ebx, %r13d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	88(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_42:                               # %.lr.ph233
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, %r13
	je	.LBB4_47
# BB#43:                                #   in Loop: Header=BB4_42 Depth=4
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB4_45
# BB#44:                                #   in Loop: Header=BB4_42 Depth=4
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB4_45:                               # %clause_LiteralAtom.exit189
                                        #   in Loop: Header=BB4_42 Depth=4
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rdx), %rcx
	cmpq	8(%rcx), %rax
	jne	.LBB4_47
# BB#46:                                #   in Loop: Header=BB4_42 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%r12, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%r15, %r12
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB4_47:                               #   in Loop: Header=BB4_42 Depth=4
	incq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB4_42
	jmp	.LBB4_48
.LBB4_25:                               #   in Loop: Header=BB4_20 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r15, %r12
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB4_73
.LBB4_40:                               #   in Loop: Header=BB4_20 Depth=3
	xorl	%ebx, %ebx
.LBB4_48:                               # %._crit_edge234
                                        #   in Loop: Header=BB4_20 Depth=3
	cmpl	$0, 100(%rsp)           # 4-byte Folded Reload
	je	.LBB4_51
# BB#49:                                #   in Loop: Header=BB4_20 Depth=3
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	callq	inf_SubsortPrecheck
	testl	%eax, %eax
	je	.LBB4_50
.LBB4_51:                               #   in Loop: Header=BB4_20 Depth=3
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	clause_Copy
	movq	%rax, %rbp
	movl	52(%r14), %r15d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 8(%r13)
	movq	$0, (%r13)
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	clause_RenameVarsBiggerThan
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	56(%rbp), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %r15
	movl	fol_NOT(%rip), %eax
	cmpl	(%r15), %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB4_53
# BB#52:                                #   in Loop: Header=BB4_20 Depth=3
	movq	16(%r15), %rax
	movq	8(%rax), %r15
.LBB4_53:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rcx
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	112(%rsp), %rsi
	leaq	128(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB4_56
# BB#54:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB4_20 Depth=3
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB4_55:                               # %.lr.ph.i180
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB4_55
.LBB4_56:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	128(%rsp), %rdi
	callq	subst_Delete
	movq	112(%rsp), %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rcx
	movq	%r13, %r8
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, %r15
	pushq	%rax
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	callq	inf_ConstraintHyperResolvents
	addq	$16, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_57
# BB#58:                                #   in Loop: Header=BB4_20 Depth=3
	testq	%rbp, %rbp
	je	.LBB4_62
# BB#59:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB4_60:                               # %.preheader.i
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_60
# BB#61:                                #   in Loop: Header=BB4_20 Depth=3
	movq	%rbp, (%rax)
	jmp	.LBB4_62
.LBB4_57:                               #   in Loop: Header=BB4_20 Depth=3
	movq	%rbp, %r14
.LBB4_62:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	testq	%r13, %r13
	je	.LBB4_64
	.p2align	4, 0x90
.LBB4_63:                               # %.lr.ph.i178
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB4_63
.LBB4_64:                               # %list_Delete.exit179
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	112(%rsp), %rdi
	callq	subst_Delete
	movq	%rbp, %rdi
	callq	clause_Delete
	movq	%r15, %r9
	jmp	.LBB4_65
.LBB4_50:                               #   in Loop: Header=BB4_20 Depth=3
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
.LBB4_65:                               #   in Loop: Header=BB4_20 Depth=3
	testq	%r12, %r12
	movq	80(%rsp), %rsi          # 8-byte Reload
	je	.LBB4_67
	.p2align	4, 0x90
.LBB4_66:                               # %.lr.ph.i173
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB4_66
.LBB4_67:                               # %list_Delete.exit174
                                        #   in Loop: Header=BB4_20 Depth=3
	testq	%rbx, %rbx
	je	.LBB4_68
# BB#69:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_70:                               # %.lr.ph.i
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB4_70
# BB#71:                                #   in Loop: Header=BB4_20 Depth=3
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB4_72
.LBB4_68:                               #   in Loop: Header=BB4_20 Depth=3
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB4_72:                               # %list_Delete.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_73:                               # %list_Delete.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB4_20
	jmp	.LBB4_74
.LBB4_18:                               #   in Loop: Header=BB4_14 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r14, %r9
	movq	%r15, %r8
	.p2align	4, 0x90
.LBB4_74:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rax
	jne	.LBB4_14
	jmp	.LBB4_75
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%r14, %r9
	movq	%r15, %r8
	.p2align	4, 0x90
.LBB4_75:                               # %clause_LiteralIsSort.exit.thread
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	%r12, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB4_3
.LBB4_76:                               # %._crit_edge251
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	inf_BackwardEmptySort, .Lfunc_end4-inf_BackwardEmptySort
	.cfi_endproc

	.globl	inf_ForwardEmptySort
	.p2align	4, 0x90
	.type	inf_ForwardEmptySort,@function
inf_ForwardEmptySort:                   # @inf_ForwardEmptySort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 112
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	64(%r14), %r15
	xorl	%r13d, %r13d
	testq	%r15, %r15
	jle	.LBB5_34
# BB#1:                                 # %.lr.ph145
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%r15, %rax
	decq	%rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
	movq	56(%r14), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	24(%rcx), %rdx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rdx), %ecx
	jne	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
.LBB5_4:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %r12d
	testl	%r12d, %r12d
	jle	.LBB5_5
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	movslq	64(%r14), %rbx
	movl	72(%r14), %edx
	movl	68(%r14), %esi
	addl	%ebx, %esi
	leal	-1(%rdx,%rsi), %edx
	cmpl	%edx, %ebx
	jg	.LBB5_18
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movslq	%edx, %rbp
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_12:                               # %clause_GetLiteralAtom.exit127..lr.ph_crit_edge
                                        #   in Loop: Header=BB5_8 Depth=2
	incq	%rbx
	movq	56(%r14), %rax
	movl	fol_NOT(%rip), %ecx
.LBB5_8:                                # %.lr.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	cmpl	(%rdi), %ecx
	jne	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB5_10:                               # %clause_GetLiteralAtom.exit127
                                        #   in Loop: Header=BB5_8 Depth=2
	movl	%r12d, %esi
	callq	term_ContainsSymbol
	cmpq	%rbp, %rbx
	jge	.LBB5_13
# BB#11:                                # %clause_GetLiteralAtom.exit127
                                        #   in Loop: Header=BB5_8 Depth=2
	testl	%eax, %eax
	je	.LBB5_12
.LBB5_13:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%ecx, %ecx
.LBB5_14:                               # %.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%ecx, %eax
	xorl	$1, %eax
	addl	%eax, %r13d
	cmpl	%r15d, %r13d
	jge	.LBB5_16
# BB#15:                                # %.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	testl	%ecx, %ecx
	je	.LBB5_2
.LBB5_16:                               # %.critedge
	testl	%ecx, %ecx
	je	.LBB5_17
.LBB5_18:                               # %.critedge.thread158
	movq	56(%r14), %rax
	movslq	%r13d, %rbp
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB5_20
# BB#19:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB5_20:                               # %clause_GetLiteralAtom.exit117
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r12d
	xorl	%ebx, %ebx
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_26:                               #   in Loop: Header=BB5_21 Depth=1
	movq	%r15, %rbx
	movl	%ebp, %r13d
.LBB5_21:                               # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_22 Depth 2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%rbx, (%r15)
	movl	fol_NOT(%rip), %eax
	movslq	%r13d, %rbp
	movq	48(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_22:                               #   Parent Loop BB5_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, %rbp
	jge	.LBB5_27
# BB#23:                                #   in Loop: Header=BB5_22 Depth=2
	movq	56(%r14), %rcx
	movq	8(%rcx,%rbp,8), %rcx
	movq	24(%rcx), %rcx
	cmpl	(%rcx), %eax
	jne	.LBB5_25
# BB#24:                                #   in Loop: Header=BB5_22 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
.LBB5_25:                               # %clause_GetLiteralAtom.exit107
                                        #   in Loop: Header=BB5_22 Depth=2
	incq	%rbp
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	cmpl	%r12d, (%rcx)
	jne	.LBB5_22
	jmp	.LBB5_26
.LBB5_27:
	movq	%r15, %rdi
	callq	list_Copy
	movq	%rax, %rbp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB5_29
# BB#28:
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	callq	inf_SubsortPrecheck
	testl	%eax, %eax
	je	.LBB5_30
.LBB5_29:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rcx
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	callq	inf_ConstraintHyperResolvents
	addq	$16, %rsp
.Lcfi92:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r13
.LBB5_30:
	testq	%rbp, %rbp
	je	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph.i96
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB5_31
.LBB5_32:                               # %list_Delete.exit97
	testq	%r15, %r15
	je	.LBB5_34
	.p2align	4, 0x90
.LBB5_33:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB5_33
	jmp	.LBB5_34
.LBB5_17:
	xorl	%r13d, %r13d
.LBB5_34:                               # %list_Delete.exit
	movq	%r13, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	inf_ForwardEmptySort, .Lfunc_end5-inf_ForwardEmptySort
	.cfi_endproc

	.globl	inf_ForwardWeakening
	.p2align	4, 0x90
	.type	inf_ForwardWeakening,@function
inf_ForwardWeakening:                   # @inf_ForwardWeakening
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 112
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdi, %r8
	movslq	64(%r8), %r14
	testq	%r14, %r14
	jle	.LBB6_1
# BB#2:                                 # %.lr.ph
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	56(%r8), %rcx
	movl	fol_NOT(%rip), %edx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rbx,8), %rdi
	movq	24(%rdi), %rdi
	cmpl	(%rdi), %edx
	movq	%rdi, %rbp
	jne	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	16(%rdi), %rbp
	movq	8(%rbp), %rbp
.LBB6_5:                                # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	16(%rbp), %rbp
	movq	8(%rbp), %rbp
	cmpl	$0, (%rbp)
	jle	.LBB6_6
# BB#41:                                # %list_Delete.exit
                                        #   in Loop: Header=BB6_3 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jl	.LBB6_3
	jmp	.LBB6_42
.LBB6_1:
	xorl	%eax, %eax
	jmp	.LBB6_42
.LBB6_6:
	movq	%r8, 16(%rsp)           # 8-byte Spill
	callq	inf_GetForwardPartnerLits
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB6_7
# BB#8:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsi), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %r15
	movl	fol_NOT(%rip), %eax
	cmpl	(%r15), %eax
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jne	.LBB6_10
# BB#9:
	movq	16(%r15), %rax
	movq	8(%rax), %r15
.LBB6_10:                               # %clause_GetLiteralAtom.exit125
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.LBB6_11
.LBB6_7:
	xorl	%eax, %eax
	jmp	.LBB6_42
.LBB6_21:                               #   in Loop: Header=BB6_11 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB6_11:                               # %sort_Intersect.exit105.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
                                        #       Child Loop BB6_13 Depth 3
                                        #     Child Loop BB6_20 Depth 2
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB6_12:                               # %sort_Intersect.exit105.outer
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_13 Depth 3
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	fol_NOT(%rip), %eax
	movslq	%r14d, %rbp
	decq	%rbp
	.p2align	4, 0x90
.LBB6_13:                               # %sort_Intersect.exit105
                                        #   Parent Loop BB6_11 Depth=1
                                        #     Parent Loop BB6_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, %rbp
	jle	.LBB6_22
# BB#14:                                #   in Loop: Header=BB6_13 Depth=3
	movq	56(%rsi), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	24(%rcx), %r12
	cmpl	(%r12), %eax
	jne	.LBB6_16
# BB#15:                                #   in Loop: Header=BB6_13 Depth=3
	movq	16(%r12), %rcx
	movq	8(%rcx), %r12
.LBB6_16:                               # %clause_GetLiteralAtom.exit115
                                        #   in Loop: Header=BB6_13 Depth=3
	movq	16(%r12), %rcx
	movq	8(%rcx), %rcx
	movq	16(%r15), %rdx
	decq	%rbp
	decl	%r14d
	cmpq	8(%rdx), %rcx
	jne	.LBB6_13
# BB#17:                                #   in Loop: Header=BB6_12 Depth=2
	incq	%rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%rbp, 8(%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	%rcx, %rbp
	movl	(%r12), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	movq	%rbp, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rax, %rax
	movq	%rdi, %rcx
	je	.LBB6_12
# BB#18:                                #   in Loop: Header=BB6_11 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	%rdi, %rcx
	je	.LBB6_11
# BB#19:                                # %.preheader.i.i103.preheader
                                        #   in Loop: Header=BB6_11 Depth=1
	movq	%rax, %rdx
.LBB6_20:                               # %.preheader.i.i103
                                        #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB6_20
	jmp	.LBB6_21
.LBB6_22:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movl	(%r15), %esi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB6_23
# BB#24:
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB6_28
# BB#25:                                # %.preheader.i.i.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB6_26:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_26
# BB#27:
	movq	%rdx, (%rsi)
	jmp	.LBB6_28
.LBB6_23:
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB6_28:                               # %.lr.ph.i98.preheader
	movq	%rax, %r12
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	xorl	%r15d, %r15d
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB6_29:                               # %.lr.ph.i98
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_34 Depth 2
	movq	8(%r14), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB6_31
# BB#30:                                #   in Loop: Header=BB6_29 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB6_31:                               # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB6_29 Depth=1
	movq	%rbx, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB6_37
# BB#32:                                #   in Loop: Header=BB6_29 Depth=1
	testq	%r15, %r15
	je	.LBB6_36
# BB#33:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB6_29 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB6_34:                               # %.preheader.i.i.i
                                        #   Parent Loop BB6_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB6_34
# BB#35:                                #   in Loop: Header=BB6_29 Depth=1
	movq	%r15, (%rcx)
.LBB6_36:                               # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB6_29 Depth=1
	movq	%rax, %r15
.LBB6_37:                               # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB6_29 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB6_29
# BB#38:                                # %inf_GetSortFromLits.exit
	movq	%r15, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	sort_TheoryComputeAllSubsortHits
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	sort_Delete
	movq	%r12, %rdi
	callq	sort_Delete
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	xorl	%ecx, %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%r14, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_InternWeakening
	testq	%rbp, %rbp
	je	.LBB6_40
	.p2align	4, 0x90
.LBB6_39:                               # %.lr.ph.i96
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbp, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbp
	jne	.LBB6_39
	.p2align	4, 0x90
.LBB6_40:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r13)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r13, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r13
	jne	.LBB6_40
.LBB6_42:                               # %.critedge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	inf_ForwardWeakening, .Lfunc_end6-inf_ForwardWeakening
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GetForwardPartnerLits,@function
inf_GetForwardPartnerLits:              # @inf_GetForwardPartnerLits
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB7_2
# BB#1:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB7_2:                                # %clause_LiteralAtom.exit
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 64
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	callq	st_GetUnifier
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB7_3
# BB#4:                                 # %.lr.ph15
	movl	symbol_TYPEMASK(%rip), %r12d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
                                        #       Child Loop BB7_14 Depth 3
	movq	8(%r13), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jns	.LBB7_7
# BB#6:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB7_5 Depth=1
	negl	%ecx
	andl	%r12d, %ecx
	cmpl	$2, %ecx
	je	.LBB7_23
.LBB7_7:                                # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB7_5 Depth=1
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_9
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_22:                               # %term_IsDeclaration.exit.thread
                                        #   in Loop: Header=BB7_9 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB7_23
.LBB7_9:                                # %.lr.ph11
                                        #   Parent Loop BB7_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_14 Depth 3
	movq	8(%rbx), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB7_22
# BB#10:                                # %term_IsAtom.exit.i
                                        #   in Loop: Header=BB7_9 Depth=2
	negl	%eax
	movl	%r12d, %ecx
	andl	%eax, %ecx
	cmpl	$2, %ecx
	jne	.LBB7_22
# BB#11:                                # %term_IsDeclaration.exit
                                        #   in Loop: Header=BB7_9 Depth=2
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB7_22
# BB#12:                                #   in Loop: Header=BB7_9 Depth=2
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB7_14
	jmp	.LBB7_22
	.p2align	4, 0x90
.LBB7_21:                               #   in Loop: Header=BB7_14 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB7_22
.LBB7_14:                               # %.lr.ph
                                        #   Parent Loop BB7_5 Depth=1
                                        #     Parent Loop BB7_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %r15
	movq	24(%r15), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	je	.LBB7_21
# BB#15:                                #   in Loop: Header=BB7_14 Depth=3
	testb	$2, (%r15)
	je	.LBB7_21
# BB#16:                                #   in Loop: Header=BB7_14 Depth=3
	movq	16(%r15), %rdi
	testb	$1, 48(%rdi)
	je	.LBB7_21
# BB#17:                                #   in Loop: Header=BB7_14 Depth=3
	movq	8(%r13), %rax
	cmpl	$0, (%rax)
	jle	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_14 Depth=3
	cmpl	$0, 64(%rdi)
	jne	.LBB7_21
.LBB7_19:                               #   in Loop: Header=BB7_14 Depth=3
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_14 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB7_21
	.p2align	4, 0x90
.LBB7_23:                               # %.loopexit
                                        #   in Loop: Header=BB7_5 Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB7_5
	jmp	.LBB7_24
.LBB7_3:
	xorl	%r14d, %r14d
.LBB7_24:                               # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	inf_GetForwardPartnerLits, .Lfunc_end7-inf_GetForwardPartnerLits
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_InternWeakening,@function
inf_InternWeakening:                    # @inf_InternWeakening
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 240
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	%rbp, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB8_3
# BB#1:                                 # %.lr.ph243
	movq	64(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movslq	8(%rbx), %rax
	movq	56(%rbp), %rcx
	movq	(%rcx,%rax,8), %rdi
	callq	clause_LiteralPrint
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
.LBB8_3:                                # %._crit_edge244
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	testq	%r15, %r15
	je	.LBB8_6
# BB#4:                                 # %.lr.ph239.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph239
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_LiteralPrint
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	testq	%rbx, %rbx
	jne	.LBB8_5
.LBB8_6:                                # %._crit_edge240
	testq	%r12, %r12
	je	.LBB8_162
# BB#7:
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	list_Copy
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_12
# BB#8:                                 # %.lr.ph.i149.preheader
	movabsq	$-4294967296, %r14      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	48(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_9:                                # %.lr.ph.i149
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	56(%rdi), %rcx
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB8_10:                               #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rbp, %rbx
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB8_10
# BB#11:                                # %clause_LiteralGetIndex.exit.i155
                                        #   in Loop: Header=BB8_9 Depth=1
	callq	clause_Copy
	movq	56(%rax), %rax
	sarq	$29, %rbx
	movq	(%rax,%rbx), %rax
	movq	%rax, 8(%r15)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB8_9
.LBB8_12:                               # %inf_CopyUnifierClauses.exit
	movq	112(%rsp), %r15         # 8-byte Reload
	movl	52(%r15), %ebx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rax
	movq	16(%rax), %r14
	movq	%r14, %rdi
	jmp	.LBB8_14
	.p2align	4, 0x90
.LBB8_13:                               #   in Loop: Header=BB8_14 Depth=1
	movq	%r14, %rdi
	callq	clause_UpdateMaxVar
	movl	52(%r14), %eax
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	movq	8(%rbp), %rax
	movq	16(%rax), %rdi
.LBB8_14:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %esi
	callq	clause_RenameVarsBiggerThan
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_13
# BB#15:                                # %inf_MakeClausesDisjoint.exit
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	movq	56(%r15), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB8_17
# BB#16:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB8_17:                               # %.lr.ph235
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	stack_POINTER(%rip), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_24 Depth 2
                                        #       Child Loop BB8_25 Depth 3
                                        #         Child Loop BB8_27 Depth 4
                                        #           Child Loop BB8_35 Depth 5
                                        #       Child Loop BB8_49 Depth 3
                                        #       Child Loop BB8_53 Depth 3
                                        #       Child Loop BB8_57 Depth 3
                                        #       Child Loop BB8_61 Depth 3
                                        #       Child Loop BB8_64 Depth 3
                                        #         Child Loop BB8_65 Depth 4
                                        #         Child Loop BB8_69 Depth 4
                                        #         Child Loop BB8_75 Depth 4
                                        #         Child Loop BB8_81 Depth 4
                                        #       Child Loop BB8_90 Depth 3
                                        #         Child Loop BB8_91 Depth 4
                                        #       Child Loop BB8_99 Depth 3
                                        #       Child Loop BB8_106 Depth 3
                                        #       Child Loop BB8_113 Depth 3
                                        #       Child Loop BB8_117 Depth 3
                                        #       Child Loop BB8_119 Depth 3
                                        #       Child Loop BB8_121 Depth 3
                                        #       Child Loop BB8_123 Depth 3
                                        #       Child Loop BB8_127 Depth 3
                                        #       Child Loop BB8_134 Depth 3
                                        #         Child Loop BB8_137 Depth 4
                                        #       Child Loop BB8_146 Depth 3
                                        #     Child Loop BB8_153 Depth 2
                                        #     Child Loop BB8_156 Depth 2
	movq	8(%r12), %rbx
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	sort_PairPrint
	movq	stdout(%rip), %rdi
	callq	fflush
	testq	%r14, %r14
	je	.LBB8_22
# BB#19:                                #   in Loop: Header=BB8_18 Depth=1
	movq	8(%rbx), %rdi
	movq	24(%r14), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB8_21
# BB#20:                                #   in Loop: Header=BB8_18 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB8_21:                               # %clause_LiteralPredicate.exit
                                        #   in Loop: Header=BB8_18 Depth=1
	callq	sort_ContainsSymbol
	testl	%eax, %eax
	je	.LBB8_155
.LBB8_22:                               # %._crit_edge264
                                        #   in Loop: Header=BB8_18 Depth=1
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	8(%rbx), %rdi
	callq	sort_GetSymbolsFromSort
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rax, %r14
	movq	48(%rsp), %rdi          # 8-byte Reload
	xorl	%r12d, %r12d
	jmp	.LBB8_24
.LBB8_23:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%edi, %edi
	movl	28(%rsp), %esi          # 4-byte Reload
	xorps	%xmm0, %xmm0
	jmp	.LBB8_132
	.p2align	4, 0x90
.LBB8_24:                               # %.critedge2
                                        #   Parent Loop BB8_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_25 Depth 3
                                        #         Child Loop BB8_27 Depth 4
                                        #           Child Loop BB8_35 Depth 5
                                        #       Child Loop BB8_49 Depth 3
                                        #       Child Loop BB8_53 Depth 3
                                        #       Child Loop BB8_57 Depth 3
                                        #       Child Loop BB8_61 Depth 3
                                        #       Child Loop BB8_64 Depth 3
                                        #         Child Loop BB8_65 Depth 4
                                        #         Child Loop BB8_69 Depth 4
                                        #         Child Loop BB8_75 Depth 4
                                        #         Child Loop BB8_81 Depth 4
                                        #       Child Loop BB8_90 Depth 3
                                        #         Child Loop BB8_91 Depth 4
                                        #       Child Loop BB8_99 Depth 3
                                        #       Child Loop BB8_106 Depth 3
                                        #       Child Loop BB8_113 Depth 3
                                        #       Child Loop BB8_117 Depth 3
                                        #       Child Loop BB8_119 Depth 3
                                        #       Child Loop BB8_121 Depth 3
                                        #       Child Loop BB8_123 Depth 3
                                        #       Child Loop BB8_127 Depth 3
                                        #       Child Loop BB8_134 Depth 3
                                        #         Child Loop BB8_137 Depth 4
                                        #       Child Loop BB8_146 Depth 3
	testq	%r14, %r14
	je	.LBB8_45
.LBB8_25:                               # %.lr.ph.outer
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_27 Depth 4
                                        #           Child Loop BB8_35 Depth 5
	testq	%rdi, %rdi
	je	.LBB8_23
# BB#26:                                # %.lr.ph331.preheader
                                        #   in Loop: Header=BB8_25 Depth=3
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_27:                               # %.lr.ph331
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_25 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB8_35 Depth 5
	movq	8(%rbp), %rbx
	movq	24(%rbx), %rdx
	movl	(%rdx), %ecx
	movl	fol_NOT(%rip), %eax
	cmpl	%ecx, %eax
	jne	.LBB8_29
# BB#28:                                #   in Loop: Header=BB8_27 Depth=4
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB8_29:                               # %clause_LiteralPredicate.exit177
                                        #   in Loop: Header=BB8_27 Depth=4
	cmpl	8(%r14), %ecx
	jne	.LBB8_39
# BB#30:                                #   in Loop: Header=BB8_27 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rbx), %rcx
	cmpl	(%rcx), %eax
	jne	.LBB8_32
# BB#31:                                #   in Loop: Header=BB8_27 Depth=4
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB8_32:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB8_27 Depth=4
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
	movq	168(%rsp), %rsi         # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	jne	.LBB8_41
# BB#33:                                #   in Loop: Header=BB8_27 Depth=4
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB8_36
# BB#34:                                # %.lr.ph.i182.preheader
                                        #   in Loop: Header=BB8_27 Depth=4
	incl	%eax
	.p2align	4, 0x90
.LBB8_35:                               # %.lr.ph.i182
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_25 Depth=3
                                        #         Parent Loop BB8_27 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB8_35
.LBB8_36:                               # %._crit_edge.i183
                                        #   in Loop: Header=BB8_27 Depth=4
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_38
# BB#37:                                #   in Loop: Header=BB8_27 Depth=4
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB8_38:                               # %.backedge
                                        #   in Loop: Header=BB8_27 Depth=4
	testq	%r14, %r14
	movq	(%rbp), %rbp
	jne	.LBB8_40
	jmp	.LBB8_44
	.p2align	4, 0x90
.LBB8_39:                               # %.backedge.thread273
                                        #   in Loop: Header=BB8_27 Depth=4
	movq	(%rbp), %rbp
.LBB8_40:                               # %.lr.ph.backedge
                                        #   in Loop: Header=BB8_27 Depth=4
	testq	%rbp, %rbp
	jne	.LBB8_27
	jmp	.LBB8_43
	.p2align	4, 0x90
.LBB8_41:                               #   in Loop: Header=BB8_25 Depth=3
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%r14, stack_STACK(,%rax,8)
	movq	(%rbp), %rdx
	leal	2(%rax), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	%rdx, stack_STACK(,%rcx,8)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	(%r14), %r14
	testq	%r14, %r14
	movq	%rax, %r12
	movq	48(%rsp), %rdi          # 8-byte Reload
	jne	.LBB8_25
# BB#42:                                # %.backedge.thread268
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	(%rbp), %rdi
	movq	%rax, %r12
	jmp	.LBB8_45
	.p2align	4, 0x90
.LBB8_43:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%edi, %edi
	movl	28(%rsp), %esi          # 4-byte Reload
	jmp	.LBB8_132
.LBB8_44:                               #   in Loop: Header=BB8_24 Depth=2
	movq	%rbp, %rdi
.LBB8_45:                               # %.critedge
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	8(%r15), %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	movq	56(%r15), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB8_47
# BB#46:                                #   in Loop: Header=BB8_24 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB8_47:                               # %clause_GetLiteralAtom.exit275.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	8(%r15), %rbp
	testq	%rbp, %rbp
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	je	.LBB8_50
# BB#48:                                # %.lr.ph421.i
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%r12d, %r12d
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_49:                               #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	(%r15), %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	term_ReplaceVariable
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	callq	cont_CopyAndApplyBindings
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, (%rbx)
	movq	%r14, %rdi
	callq	term_Delete
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rbx, %r12
	jne	.LBB8_49
	jmp	.LBB8_51
	.p2align	4, 0x90
.LBB8_50:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebx, %ebx
.LBB8_51:                               # %._crit_edge422.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	16(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB8_54
# BB#52:                                # %.lr.ph415.i
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%r12d, %r12d
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_53:                               #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	(%r15), %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	term_ReplaceVariable
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	callq	cont_CopyAndApplyBindings
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, (%rbx)
	movq	%r14, %rdi
	callq	term_Delete
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rbx, %r12
	jne	.LBB8_53
	jmp	.LBB8_55
	.p2align	4, 0x90
.LBB8_54:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebx, %ebx
.LBB8_55:                               # %._crit_edge416.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	24(%r15), %rbp
	testq	%rbp, %rbp
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB8_58
# BB#56:                                # %.lr.ph408.i
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%r12d, %r12d
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_57:                               #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	(%r15), %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	term_ReplaceVariable
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	callq	cont_CopyAndApplyBindings
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, (%rbx)
	movq	%r14, %rdi
	callq	term_Delete
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rbx, %r12
	jne	.LBB8_57
	jmp	.LBB8_59
	.p2align	4, 0x90
.LBB8_58:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebx, %ebx
.LBB8_59:                               # %._crit_edge409.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	32(%r15), %r13
	testq	%r13, %r13
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB8_62
# BB#60:                                # %.lr.ph399.i.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	12(%rsp), %eax          # 4-byte Reload
	.p2align	4, 0x90
.LBB8_61:                               # %.lr.ph399.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	8(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movslq	(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	%r15, (%r12)
	movslq	68(%r14), %rax
	movslq	64(%r14), %rbx
	addq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movl	8(%r14), %esi
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	misc_Max
	movq	%r12, %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%r13), %r13
	testq	%r13, %r13
	movq	%rdx, %r12
	movq	%rcx, %r15
	movq	%rbp, %rsi
	jne	.LBB8_61
	jmp	.LBB8_63
	.p2align	4, 0x90
.LBB8_62:                               #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB8_63:                               # %.preheader.i143
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB8_87
	.p2align	4, 0x90
.LBB8_64:                               # %.lr.ph383.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_65 Depth 4
                                        #         Child Loop BB8_69 Depth 4
                                        #         Child Loop BB8_75 Depth 4
                                        #         Child Loop BB8_81 Depth 4
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	16(%rax), %r14
	movq	56(%r14), %rcx
	xorl	%edx, %edx
	movq	72(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_65:                               #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %r12
	leaq	1(%r12), %rdx
	cmpq	%rax, (%rcx,%r12,8)
	jne	.LBB8_65
# BB#66:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	64(%r14), %r15d
	testl	%r15d, %r15d
	jle	.LBB8_73
# BB#67:                                # %.lr.ph360.preheader.i
                                        #   in Loop: Header=BB8_64 Depth=3
	decq	%r15
	xorl	%ebp, %ebp
	jmp	.LBB8_69
	.p2align	4, 0x90
.LBB8_68:                               # %clause_GetLiteralAtom.exit326..lr.ph360_crit_edge.i
                                        #   in Loop: Header=BB8_69 Depth=4
	incq	%rbp
	movq	56(%r14), %rcx
	movq	%rax, %r13
.LBB8_69:                               # %.lr.ph360.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_RIGHTCONTEXT(%rip), %rdi
	movq	(%rcx,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_71
# BB#70:                                #   in Loop: Header=BB8_69 Depth=4
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_71:                               # %clause_GetLiteralAtom.exit326.i
                                        #   in Loop: Header=BB8_69 Depth=4
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	cmpq	%rbp, %r15
	jne	.LBB8_68
# BB#72:                                # %._crit_edge361.loopexit.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	64(%r14), %r15d
	movq	%rax, %r13
.LBB8_73:                               # %._crit_edge361.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	68(%r14), %ecx
	leal	-1(%r15,%rcx), %eax
	cmpl	%eax, %r15d
	jg	.LBB8_79
# BB#74:                                # %.lr.ph366.preheader.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movslq	%r15d, %rbp
	movslq	%eax, %r15
	decq	%rbp
	.p2align	4, 0x90
.LBB8_75:                               # %.lr.ph366.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_RIGHTCONTEXT(%rip), %rdi
	movq	56(%r14), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_77
# BB#76:                                #   in Loop: Header=BB8_75 Depth=4
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_77:                               # %clause_GetLiteralAtom.exit316.i
                                        #   in Loop: Header=BB8_75 Depth=4
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	incq	%rbp
	cmpq	%r15, %rbp
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jl	.LBB8_75
# BB#78:                                # %._crit_edge367.loopexit.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	64(%r14), %r15d
	movl	68(%r14), %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB8_79:                               # %._crit_edge367.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	72(%r14), %eax
	addl	%r15d, %ecx
	leal	-1(%rax,%rcx), %eax
	cmpl	%eax, %ecx
	movq	%r13, 72(%rsp)          # 8-byte Spill
	jg	.LBB8_86
# BB#80:                                # %.lr.ph372.preheader.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movslq	%ecx, %rbp
	movslq	%eax, %r13
	decq	%rbp
	movl	%r12d, %r15d
	subl	%ecx, %r15d
	.p2align	4, 0x90
.LBB8_81:                               # %.lr.ph372.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%r15d, %r15d
	je	.LBB8_85
# BB#82:                                #   in Loop: Header=BB8_81 Depth=4
	movq	cont_RIGHTCONTEXT(%rip), %rdi
	movq	56(%r14), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_84
# BB#83:                                #   in Loop: Header=BB8_81 Depth=4
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_84:                               # %clause_GetLiteralAtom.exit306.i
                                        #   in Loop: Header=BB8_81 Depth=4
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB8_85:                               #   in Loop: Header=BB8_81 Depth=4
	incq	%rbp
	decl	%r15d
	cmpq	%r13, %rbp
	jl	.LBB8_81
.LBB8_86:                               # %._crit_edge373.i
                                        #   in Loop: Header=BB8_64 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%r14, 8(%r13)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movslq	(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movslq	%r12d, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movl	8(%r14), %esi
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	misc_Max
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %rcx
	movq	%rbx, %rbp
	jne	.LBB8_64
.LBB8_87:                               # %._crit_edge384.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	112(%rsp), %r15         # 8-byte Reload
	movl	64(%r15), %r14d
	testl	%r14d, %r14d
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	jle	.LBB8_103
# BB#88:                                # %.lr.ph353.i
                                        #   in Loop: Header=BB8_24 Depth=2
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB8_98
# BB#89:                                # %.lr.ph353.split.i.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_90:                               # %.lr.ph353.split.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_91 Depth 4
	movq	64(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_91:                               # %.lr.ph.i296.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_90 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbx, 8(%rax)
	je	.LBB8_96
# BB#92:                                #   in Loop: Header=BB8_91 Depth=4
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB8_91
# BB#93:                                # %.loopexit.i
                                        #   in Loop: Header=BB8_90 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_95
# BB#94:                                #   in Loop: Header=BB8_90 Depth=3
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_95:                               # %clause_GetLiteralAtom.exit295.i
                                        #   in Loop: Header=BB8_90 Depth=3
	callq	cont_CopyAndApplyBindings
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, %rcx
	movq	%rax, %r13
	jmp	.LBB8_97
	.p2align	4, 0x90
.LBB8_96:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB8_90 Depth=3
	movq	112(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, %r12
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
.LBB8_97:                               #   in Loop: Header=BB8_90 Depth=3
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB8_90
	jmp	.LBB8_103
.LBB8_98:                               # %.lr.ph353.split.us.i.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_99:                               # %.lr.ph353.split.us.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_101
# BB#100:                               #   in Loop: Header=BB8_99 Depth=3
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_101:                              # %clause_GetLiteralAtom.exit295.us.i
                                        #   in Loop: Header=BB8_99 Depth=3
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	incq	%rbp
	cmpq	%rbp, %r14
	movq	%rax, %r13
	jne	.LBB8_99
# BB#102:                               #   in Loop: Header=BB8_24 Depth=2
	movq	%rax, %r13
.LBB8_103:                              # %._crit_edge354.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movl	64(%r15), %edx
	movl	68(%r15), %ecx
	leal	-1(%rdx,%rcx), %eax
	cmpl	%eax, %edx
	jle	.LBB8_105
# BB#104:                               #   in Loop: Header=BB8_24 Depth=2
	movq	128(%rsp), %r12         # 8-byte Reload
	jmp	.LBB8_110
	.p2align	4, 0x90
.LBB8_105:                              # %.lr.ph344.preheader.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movslq	%edx, %rbp
	movslq	%eax, %r14
	decq	%rbp
	movq	128(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_106:                              # %.lr.ph344.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_108
# BB#107:                               #   in Loop: Header=BB8_106 Depth=3
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_108:                              # %clause_GetLiteralAtom.exit285.i
                                        #   in Loop: Header=BB8_106 Depth=3
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	incq	%rbp
	cmpq	%r14, %rbp
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jl	.LBB8_106
# BB#109:                               # %._crit_edge345.loopexit.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movl	64(%r15), %edx
	movl	68(%r15), %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB8_110:                              # %._crit_edge345.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	72(%r15), %eax
	addl	%edx, %ecx
	leal	-1(%rax,%rcx), %eax
	cmpl	%eax, %ecx
	movq	%r13, 72(%rsp)          # 8-byte Spill
	jle	.LBB8_112
# BB#111:                               #   in Loop: Header=BB8_24 Depth=2
	movq	%rsi, %r13
	jmp	.LBB8_116
	.p2align	4, 0x90
.LBB8_112:                              # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movslq	%ecx, %rbp
	movslq	%eax, %r14
	decq	%rbp
	.p2align	4, 0x90
.LBB8_113:                              # %.lr.ph.i145
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_115
# BB#114:                               #   in Loop: Header=BB8_113 Depth=3
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB8_115:                              # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB8_113 Depth=3
	callq	cont_CopyAndApplyBindings
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	incq	%rbp
	cmpq	%r14, %rbp
	movq	%r13, 16(%rsp)          # 8-byte Spill
	jl	.LBB8_113
.LBB8_116:                              # %._crit_edge.i146
                                        #   in Loop: Header=BB8_24 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	240(%rsp), %r8
	callq	clause_Create
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB8_118
	.p2align	4, 0x90
.LBB8_117:                              # %.lr.ph.i264.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB8_117
.LBB8_118:                              # %list_Delete.exit265.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB8_120
	.p2align	4, 0x90
.LBB8_119:                              # %.lr.ph.i259.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %rsi
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	jne	.LBB8_119
.LBB8_120:                              # %list_Delete.exit260.i
                                        #   in Loop: Header=BB8_24 Depth=2
	testq	%r13, %r13
	je	.LBB8_122
	.p2align	4, 0x90
.LBB8_121:                              # %.lr.ph.i254.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB8_121
.LBB8_122:                              # %list_Delete.exit255.i
                                        #   in Loop: Header=BB8_24 Depth=2
	xorl	%eax, %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, (%rcx)
	setle	%al
	incl	%eax
	movl	%eax, 76(%r14)
	movl	12(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 8(%r14)
	orb	$4, 48(%r14)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_SetSplitDataFromList
	testq	%rbx, %rbx
	je	.LBB8_124
	.p2align	4, 0x90
.LBB8_123:                              # %.lr.ph.i.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB8_123
.LBB8_124:                              # %inf_ApplyWeakening.exit
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r14)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 40(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	movq	160(%rsp), %r13         # 8-byte Reload
	je	.LBB8_129
# BB#125:                               #   in Loop: Header=BB8_24 Depth=2
	testq	%r13, %r13
	je	.LBB8_130
# BB#126:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	%rax, %rdx
	movl	28(%rsp), %esi          # 4-byte Reload
	xorps	%xmm0, %xmm0
	movq	120(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_127:                              # %.preheader.i
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB8_127
# BB#128:                               #   in Loop: Header=BB8_24 Depth=2
	movq	%r13, (%rcx)
	xorl	%r14d, %r14d
	movq	%rax, %r13
	jmp	.LBB8_132
	.p2align	4, 0x90
.LBB8_129:                              #   in Loop: Header=BB8_24 Depth=2
	xorl	%r14d, %r14d
	jmp	.LBB8_131
.LBB8_130:                              #   in Loop: Header=BB8_24 Depth=2
	xorl	%r14d, %r14d
	movq	%rax, %r13
.LBB8_131:                              # %list_Nconc.exit.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	movl	28(%rsp), %esi          # 4-byte Reload
	xorps	%xmm0, %xmm0
	movq	120(%rsp), %rdi         # 8-byte Reload
.LBB8_132:                              # %list_Nconc.exit.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	movl	stack_POINTER(%rip), %eax
	cmpl	%esi, %eax
	je	.LBB8_133
	.p2align	4, 0x90
.LBB8_134:                              # %.lr.ph229
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_137 Depth 4
	leal	-1(%rax), %ecx
	cmpq	$0, stack_STACK(,%rcx,8)
	jne	.LBB8_142
# BB#135:                               #   in Loop: Header=BB8_134 Depth=3
	addl	$-2, %eax
	movl	%eax, stack_POINTER(%rip)
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB8_138
# BB#136:                               # %.lr.ph.i139.preheader
                                        #   in Loop: Header=BB8_134 Depth=3
	incl	%eax
	.p2align	4, 0x90
.LBB8_137:                              # %.lr.ph.i139
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        #       Parent Loop BB8_134 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB8_137
.LBB8_138:                              # %._crit_edge.i140
                                        #   in Loop: Header=BB8_134 Depth=3
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_140
# BB#139:                               #   in Loop: Header=BB8_134 Depth=3
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB8_140:                              # %cont_BackTrack.exit141
                                        #   in Loop: Header=BB8_134 Depth=3
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	movl	stack_POINTER(%rip), %eax
	cmpl	%esi, %eax
	movq	%rcx, %r12
	jne	.LBB8_134
# BB#141:                               #   in Loop: Header=BB8_24 Depth=2
	movl	%esi, %eax
	jmp	.LBB8_150
	.p2align	4, 0x90
.LBB8_142:                              # %.critedge1
                                        #   in Loop: Header=BB8_24 Depth=2
	cmpl	%esi, %eax
	jne	.LBB8_144
.LBB8_133:                              #   in Loop: Header=BB8_24 Depth=2
	movl	%esi, %eax
	testq	%rdi, %rdi
	je	.LBB8_151
	jmp	.LBB8_24
.LBB8_144:                              #   in Loop: Header=BB8_24 Depth=2
	movq	stack_STACK(,%rcx,8), %rdi
	addl	$-2, %eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %r14
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB8_147
# BB#145:                               # %.lr.ph.i133.preheader
                                        #   in Loop: Header=BB8_24 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB8_146:                              # %.lr.ph.i133
                                        #   Parent Loop BB8_18 Depth=1
                                        #     Parent Loop BB8_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB8_146
.LBB8_147:                              # %._crit_edge.i
                                        #   in Loop: Header=BB8_24 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_149
# BB#148:                               #   in Loop: Header=BB8_24 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB8_149:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	movl	stack_POINTER(%rip), %eax
.LBB8_150:                              # %.critedge1.thread
                                        #   in Loop: Header=BB8_24 Depth=2
	movq	%rcx, %r12
	testq	%rdi, %rdi
	jne	.LBB8_24
.LBB8_151:                              # %.critedge1.thread
                                        #   in Loop: Header=BB8_24 Depth=2
	cmpl	%esi, %eax
	jne	.LBB8_24
# BB#152:                               #   in Loop: Header=BB8_18 Depth=1
	movq	144(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB8_154
	.p2align	4, 0x90
.LBB8_153:                              # %.lr.ph.i129
                                        #   Parent Loop BB8_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB8_153
.LBB8_154:                              #   in Loop: Header=BB8_18 Depth=1
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	136(%rsp), %r12         # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB8_155:                              # %list_Delete.exit130
                                        #   in Loop: Header=BB8_18 Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_157
	.p2align	4, 0x90
.LBB8_156:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB8_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB8_156
.LBB8_157:                              # %sort_PairDelete.exit
                                        #   in Loop: Header=BB8_18 Depth=1
	movq	(%rbx), %rdi
	callq	sort_ConditionDelete
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB8_18
# BB#158:                               # %._crit_edge
	movq	48(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB8_163
# BB#159:                               # %.lr.ph.i122.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB8_160:                              # %.lr.ph.i122
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	callq	clause_Delete
	movq	$0, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_160
	.p2align	4, 0x90
.LBB8_161:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB8_161
	jmp	.LBB8_163
.LBB8_162:
	xorl	%r13d, %r13d
.LBB8_163:                              # %list_Delete.exit
	movq	%r13, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	inf_InternWeakening, .Lfunc_end8-inf_InternWeakening
	.cfi_endproc

	.globl	inf_BackwardWeakening
	.p2align	4, 0x90
	.type	inf_BackwardWeakening,@function
inf_BackwardWeakening:                  # @inf_BackwardWeakening
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 208
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %r12
	movl	68(%rdi), %eax
	movl	72(%rdi), %ecx
	addl	64(%rdi), %eax
	leal	-1(%rcx,%rax), %edx
	cmpl	%edx, %eax
	jle	.LBB9_2
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_69
.LBB9_2:                                # %.lr.ph173
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movslq	%eax, %r9
	movslq	%edx, %r10
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	24(%rsp), %rdx
	movabsq	$4294967296, %r14       # imm = 0x100000000
	.p2align	4, 0x90
.LBB9_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_16 Depth 2
                                        #       Child Loop BB9_22 Depth 3
                                        #     Child Loop BB9_27 Depth 2
                                        #       Child Loop BB9_30 Depth 3
                                        #       Child Loop BB9_32 Depth 3
                                        #         Child Loop BB9_33 Depth 4
                                        #           Child Loop BB9_35 Depth 5
                                        #         Child Loop BB9_45 Depth 4
                                        #       Child Loop BB9_49 Depth 3
                                        #       Child Loop BB9_54 Depth 3
                                        #       Child Loop BB9_59 Depth 3
                                        #       Child Loop BB9_63 Depth 3
                                        #     Child Loop BB9_66 Depth 2
	movq	56(%rdi), %rax
	movq	(%rax,%r9,8), %rbp
	movq	24(%rbp), %rbx
	movl	(%rbx), %eax
	movl	fol_NOT(%rip), %r11d
	cmpl	%eax, %r11d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	16(%rbx), %rbp
	movq	8(%rbp), %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB9_5:                                # %clause_LiteralAtom.exit143
                                        #   in Loop: Header=BB9_3 Depth=1
	testb	$2, (%rbp)
	je	.LBB9_68
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	cmpl	%eax, %r11d
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB9_8:                                # %clause_LiteralPredicate.exit.i153
                                        #   in Loop: Header=BB9_3 Depth=1
	testl	%eax, %eax
	jns	.LBB9_68
# BB#9:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB9_3 Depth=1
	negl	%eax
	movl	%r8d, %ebp
	andl	%eax, %ebp
	cmpl	$2, %ebp
	jne	.LBB9_68
# BB#10:                                # %clause_LiteralIsSort.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rbp
	cltq
	movq	(%rbp,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB9_68
# BB#11:                                #   in Loop: Header=BB9_3 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jle	.LBB9_13
# BB#12:                                #   in Loop: Header=BB9_3 Depth=1
	cmpl	$0, 64(%rdi)
	jne	.LBB9_68
.LBB9_13:                               #   in Loop: Header=BB9_3 Depth=1
	movq	%r10, 112(%rsp)         # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	$0, 24(%rsp)
	movq	$0, 80(%rsp)
	movq	%r15, (%rsp)
	xorl	%r8d, %r8d
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	leaq	80(%rsp), %rcx
	movq	%r13, %r9
	callq	inf_GetBackwardPartnerLits
	movq	80(%rsp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 80(%rsp)
	testq	%rax, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	je	.LBB9_14
# BB#15:                                # %.lr.ph.i134.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph.i134
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_22 Depth 3
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB9_18
# BB#17:                                #   in Loop: Header=BB9_16 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB9_18:                               # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB9_16 Depth=2
	movq	%r12, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB9_19
# BB#20:                                #   in Loop: Header=BB9_16 Depth=2
	testq	%rbp, %rbp
	je	.LBB9_24
# BB#21:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB9_16 Depth=2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB9_22:                               # %.preheader.i.i.i
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_22
# BB#23:                                #   in Loop: Header=BB9_16 Depth=2
	movq	%rbp, (%rdx)
	jmp	.LBB9_24
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_16 Depth=2
	movq	%rbp, %rax
.LBB9_24:                               # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB9_16 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %rbp
	jne	.LBB9_16
	jmp	.LBB9_25
.LBB9_14:                               #   in Loop: Header=BB9_3 Depth=1
	xorl	%eax, %eax
.LBB9_25:                               # %inf_GetSortFromLits.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB9_65
# BB#26:                                # %.lr.ph168.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_27:                               # %.lr.ph168
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_30 Depth 3
                                        #       Child Loop BB9_32 Depth 3
                                        #         Child Loop BB9_33 Depth 4
                                        #           Child Loop BB9_35 Depth 5
                                        #         Child Loop BB9_45 Depth 4
                                        #       Child Loop BB9_49 Depth 3
                                        #       Child Loop BB9_54 Depth 3
                                        #       Child Loop BB9_59 Depth 3
                                        #       Child Loop BB9_63 Depth 3
	movq	8(%rdx), %rax
	movq	16(%rax), %r8
	movq	24(%rax), %rbx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbx), %ecx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jne	.LBB9_29
# BB#28:                                #   in Loop: Header=BB9_27 Depth=2
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rbx
.LBB9_29:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	56(%r8), %rcx
	movq	$-1, %rdx
	xorl	%esi, %esi
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB9_30:                               #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %edi
	addq	%r14, %rbp
	leal	1(%rdi), %esi
	cmpq	%rax, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB9_30
# BB#31:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	movl	64(%r8), %r15d
	movl	%edi, %r12d
	xorl	%edi, %edi
	xorl	%eax, %eax
	movq	%r8, 104(%rsp)          # 8-byte Spill
	jmp	.LBB9_32
.LBB9_46:                               #   in Loop: Header=BB9_32 Depth=3
	movq	%rsi, (%rcx)
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB9_32:                               # %sort_Intersect.exit125.outer.outer
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_33 Depth 4
                                        #           Child Loop BB9_35 Depth 5
                                        #         Child Loop BB9_45 Depth 4
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB9_33:                               # %sort_Intersect.exit125.outer
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        #       Parent Loop BB9_32 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB9_35 Depth 5
	testl	%r15d, %r15d
	jle	.LBB9_39
# BB#34:                                # %.lr.ph
                                        #   in Loop: Header=BB9_33 Depth=4
	movq	56(%r8), %rax
	movl	fol_NOT(%rip), %ecx
	movslq	%r15d, %r15
	decq	%r15
	.p2align	4, 0x90
.LBB9_35:                               #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        #       Parent Loop BB9_32 Depth=3
                                        #         Parent Loop BB9_33 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rax,%r15,8), %rdx
	movq	24(%rdx), %r13
	cmpl	(%r13), %ecx
	jne	.LBB9_37
# BB#36:                                #   in Loop: Header=BB9_35 Depth=5
	movq	16(%r13), %rdx
	movq	8(%rdx), %r13
.LBB9_37:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB9_35 Depth=5
	cmpq	%r15, %r12
	je	.LBB9_38
# BB#41:                                #   in Loop: Header=BB9_35 Depth=5
	movq	16(%r13), %rdx
	movq	8(%rdx), %rdx
	movq	16(%rbx), %rsi
	cmpq	8(%rsi), %rdx
	je	.LBB9_42
.LBB9_38:                               # %sort_Intersect.exit125.backedge
                                        #   in Loop: Header=BB9_35 Depth=5
	leaq	-1(%r15), %rdx
	incq	%r15
	cmpq	$1, %r15
	movq	%rdx, %r15
	jg	.LBB9_35
	jmp	.LBB9_39
	.p2align	4, 0x90
.LBB9_42:                               #   in Loop: Header=BB9_33 Depth=4
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r15, 8(%r14)
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%r14)
	movl	(%r13), %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	movq	104(%rsp), %r8          # 8-byte Reload
	testq	%rax, %rax
	movq	%r14, %rdi
	je	.LBB9_33
# BB#43:                                #   in Loop: Header=BB9_32 Depth=3
	movq	56(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movq	%r14, %rdi
	je	.LBB9_32
# BB#44:                                # %.preheader.i.i123.preheader
                                        #   in Loop: Header=BB9_32 Depth=3
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB9_45:                               # %.preheader.i.i123
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        #       Parent Loop BB9_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_45
	jmp	.LBB9_46
	.p2align	4, 0x90
.LBB9_39:                               # %sort_Intersect.exit125.outer._crit_edge
                                        #   in Loop: Header=BB9_27 Depth=2
	sarq	$32, %rbp
	movq	%rdi, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	%r15, (%r14)
	movl	(%rbx), %esi
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	callq	sort_TheorySortOfSymbol
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB9_40
# BB#47:                                #   in Loop: Header=BB9_27 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB9_51
# BB#48:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB9_49:                               # %.preheader.i.i
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_49
# BB#50:                                #   in Loop: Header=BB9_27 Depth=2
	movq	%rdx, (%rax)
	jmp	.LBB9_51
	.p2align	4, 0x90
.LBB9_40:                               #   in Loop: Header=BB9_27 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB9_51:                               # %sort_Intersect.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%rbp, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%r12, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	sort_TheoryComputeAllSubsortHits
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	sort_Delete
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbx, %rsi
	movq	72(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	%r13, %rbx
	movq	%r13, (%rsp)
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	%r14, %rsi
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	movq	%r15, %r9
	callq	inf_InternWeakening
	testq	%rax, %rax
	movq	%r15, %r13
	je	.LBB9_57
# BB#52:                                #   in Loop: Header=BB9_27 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB9_56
# BB#53:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB9_54:                               # %.preheader.i
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_54
# BB#55:                                #   in Loop: Header=BB9_27 Depth=2
	movq	%rsi, (%rcx)
.LBB9_56:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_57:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	16(%rsp), %r12          # 8-byte Reload
	xorps	%xmm0, %xmm0
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %r15
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB9_60
# BB#58:                                # %.lr.ph.i119.preheader
                                        #   in Loop: Header=BB9_27 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB9_59:                               # %.lr.ph.i119
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB9_59
.LBB9_60:                               # %._crit_edge.i
                                        #   in Loop: Header=BB9_27 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB9_62
# BB#61:                                #   in Loop: Header=BB9_27 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB9_62:                               # %cont_BackTrack.exit
                                        #   in Loop: Header=BB9_27 Depth=2
	testq	%r14, %r14
	je	.LBB9_64
	.p2align	4, 0x90
.LBB9_63:                               # %.lr.ph.i117
                                        #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB9_63
.LBB9_64:                               # %list_Delete.exit118
                                        #   in Loop: Header=BB9_27 Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	movq	%rax, %rdx
	movabsq	$4294967296, %r14       # imm = 0x100000000
	jne	.LBB9_27
.LBB9_65:                               # %._crit_edge
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	sort_Delete
	movq	96(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB9_67
	.p2align	4, 0x90
.LBB9_66:                               # %.lr.ph.i
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB9_66
.LBB9_67:                               # %list_Delete.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	movl	symbol_TYPEMASK(%rip), %ebx
	movl	%ebx, %r8d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %r10         # 8-byte Reload
	leaq	24(%rsp), %rdx
	.p2align	4, 0x90
.LBB9_68:                               # %clause_LiteralIsSort.exit.thread
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	%r10, %r9
	leaq	1(%r9), %r9
	jl	.LBB9_3
.LBB9_69:                               # %._crit_edge174
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	inf_BackwardWeakening, .Lfunc_end9-inf_BackwardWeakening
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GetBackwardPartnerLits,@function
inf_GetBackwardPartnerLits:             # @inf_GetBackwardPartnerLits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 96
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rdi, %rax
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB10_2
# BB#1:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB10_2:                               # %clause_LiteralAtom.exit
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	callq	st_GetUnifier
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB10_53
# BB#3:                                 # %.lr.ph101
	movq	%r12, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB10_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_8 Depth 2
                                        #       Child Loop BB10_13 Depth 3
                                        #         Child Loop BB10_26 Depth 4
                                        #       Child Loop BB10_33 Depth 3
                                        #         Child Loop BB10_46 Depth 4
	movq	8(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jns	.LBB10_6
# BB#5:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB10_4 Depth=1
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB10_52
.LBB10_6:                               # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	8(%rax), %r14
	testq	%r14, %r14
	jne	.LBB10_8
	jmp	.LBB10_52
	.p2align	4, 0x90
.LBB10_51:                              # %term_IsDeclaration.exit.thread
                                        #   in Loop: Header=BB10_8 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB10_52
.LBB10_8:                               # %.lr.ph99
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_13 Depth 3
                                        #         Child Loop BB10_26 Depth 4
                                        #       Child Loop BB10_33 Depth 3
                                        #         Child Loop BB10_46 Depth 4
	movq	8(%r14), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB10_51
# BB#9:                                 # %term_IsAtom.exit.i
                                        #   in Loop: Header=BB10_8 Depth=2
	negl	%eax
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%eax, %ecx
	cmpl	$2, %ecx
	jne	.LBB10_51
# BB#10:                                # %term_IsDeclaration.exit
                                        #   in Loop: Header=BB10_8 Depth=2
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB10_51
# BB#11:                                #   in Loop: Header=BB10_8 Depth=2
	callq	sharing_NAtomDataList
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB10_51
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB10_8 Depth=2
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB10_33
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph.split.us
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_26 Depth 4
	movq	8(%r15), %r13
	movq	16(%r13), %rdi
	testb	$1, 48(%rdi)
	je	.LBB10_32
# BB#14:                                #   in Loop: Header=BB10_13 Depth=3
	movq	24(%r13), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB10_15
# BB#25:                                #   in Loop: Header=BB10_13 Depth=3
	movq	56(%rdi), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB10_26:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_8 Depth=2
                                        #       Parent Loop BB10_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%eax
	cmpq	%r13, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB10_26
# BB#27:                                # %clause_LiteralGetIndex.exit.us
                                        #   in Loop: Header=BB10_13 Depth=3
	cmpl	64(%rdi), %eax
	jge	.LBB10_32
# BB#28:                                #   in Loop: Header=BB10_13 Depth=3
	movq	8(%rbp), %rax
	movl	(%rax), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	4(%rsp), %ecx           # 4-byte Folded Reload
	je	.LBB10_31
# BB#29:                                #   in Loop: Header=BB10_13 Depth=3
	testl	%eax, %eax
	jle	.LBB10_32
# BB#30:                                #   in Loop: Header=BB10_13 Depth=3
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rdx
	callq	clause_HasOnlyVarsInConstraint
	testl	%eax, %eax
	je	.LBB10_32
.LBB10_31:                              #   in Loop: Header=BB10_13 Depth=3
	movq	(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
	jmp	.LBB10_32
	.p2align	4, 0x90
.LBB10_15:                              #   in Loop: Header=BB10_13 Depth=3
	testb	$2, (%r13)
	je	.LBB10_32
# BB#16:                                #   in Loop: Header=BB10_13 Depth=3
	movq	8(%rbp), %rax
	cmpl	$0, (%rax)
	jle	.LBB10_18
# BB#17:                                #   in Loop: Header=BB10_13 Depth=3
	cmpl	$0, 64(%rdi)
	jne	.LBB10_32
.LBB10_18:                              #   in Loop: Header=BB10_13 Depth=3
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB10_32
# BB#19:                                #   in Loop: Header=BB10_13 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB10_21
# BB#20:                                #   in Loop: Header=BB10_13 Depth=3
	movq	16(%rdx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB10_21:                              # %clause_LiteralPredicate.exit89.us
                                        #   in Loop: Header=BB10_13 Depth=3
	movq	24(%r13), %rsi
	movl	(%rsi), %edx
	cmpl	%edx, %ecx
	jne	.LBB10_23
# BB#22:                                #   in Loop: Header=BB10_13 Depth=3
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
.LBB10_23:                              # %clause_LiteralPredicate.exit.us
                                        #   in Loop: Header=BB10_13 Depth=3
	cmpl	%edx, %eax
	je	.LBB10_32
# BB#24:                                #   in Loop: Header=BB10_13 Depth=3
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB10_32:                              #   in Loop: Header=BB10_13 Depth=3
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB10_13
	jmp	.LBB10_51
	.p2align	4, 0x90
.LBB10_33:                              # %.lr.ph.split
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_46 Depth 4
	movq	8(%r15), %rbx
	movq	16(%rbx), %rdi
	testb	$1, 48(%rdi)
	je	.LBB10_50
# BB#34:                                #   in Loop: Header=BB10_33 Depth=3
	movq	24(%rbx), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB10_35
# BB#45:                                #   in Loop: Header=BB10_33 Depth=3
	movq	56(%rdi), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB10_46:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_8 Depth=2
                                        #       Parent Loop BB10_33 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%eax
	cmpq	%rbx, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB10_46
# BB#47:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB10_33 Depth=3
	cmpl	64(%rdi), %eax
	jge	.LBB10_50
# BB#48:                                #   in Loop: Header=BB10_33 Depth=3
	movq	8(%rbp), %rax
	cmpl	$0, (%rax)
	jg	.LBB10_50
# BB#49:                                #   in Loop: Header=BB10_33 Depth=3
	movq	(%r12), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%r12)
	jmp	.LBB10_50
	.p2align	4, 0x90
.LBB10_35:                              #   in Loop: Header=BB10_33 Depth=3
	testb	$2, (%rbx)
	je	.LBB10_50
# BB#36:                                #   in Loop: Header=BB10_33 Depth=3
	movq	8(%rbp), %rax
	cmpl	$0, (%rax)
	jle	.LBB10_38
# BB#37:                                #   in Loop: Header=BB10_33 Depth=3
	cmpl	$0, 64(%rdi)
	jne	.LBB10_50
.LBB10_38:                              #   in Loop: Header=BB10_33 Depth=3
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB10_50
# BB#39:                                #   in Loop: Header=BB10_33 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_33 Depth=3
	movq	16(%rdx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB10_41:                              # %clause_LiteralPredicate.exit89
                                        #   in Loop: Header=BB10_33 Depth=3
	movq	24(%rbx), %rsi
	movl	(%rsi), %edx
	cmpl	%edx, %ecx
	jne	.LBB10_43
# BB#42:                                #   in Loop: Header=BB10_33 Depth=3
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
.LBB10_43:                              # %clause_LiteralPredicate.exit
                                        #   in Loop: Header=BB10_33 Depth=3
	cmpl	%edx, %eax
	je	.LBB10_50
# BB#44:                                #   in Loop: Header=BB10_33 Depth=3
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%r12)
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB10_50:                              #   in Loop: Header=BB10_33 Depth=3
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB10_33
	jmp	.LBB10_51
	.p2align	4, 0x90
.LBB10_52:                              # %.loopexit
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB10_4
.LBB10_53:                              # %._crit_edge
	movq	(%r12), %rdi
	movl	$inf_LiteralsHaveSameSubtermAndAreFromSameClause, %esi
	callq	list_DeleteDuplicates
	movq	%rax, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	inf_GetBackwardPartnerLits, .Lfunc_end10-inf_GetBackwardPartnerLits
	.cfi_endproc

	.globl	inf_ForwardEmptySortPlusPlus
	.p2align	4, 0x90
	.type	inf_ForwardEmptySortPlusPlus,@function
inf_ForwardEmptySortPlusPlus:           # @inf_ForwardEmptySortPlusPlus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi164:
	.cfi_def_cfa_offset 128
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movslq	64(%rbx), %r15
	testq	%r15, %r15
	jle	.LBB11_52
# BB#1:                                 # %.lr.ph177
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_9 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	24(%rcx), %rdi
	movl	(%rdi), %edx
	movl	fol_NOT(%rip), %ecx
	cmpl	%edx, %ecx
	movq	%rdi, %rsi
	jne	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rdi), %rsi
	movq	8(%rsi), %rsi
.LBB11_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rsi), %rsi
	movq	8(%rsi), %rsi
	cmpl	$0, (%rsi)
	jle	.LBB11_51
# BB#5:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpl	%edx, %ecx
	movq	%rdi, %rdx
	jne	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
.LBB11_7:                               # %clause_LiteralAtom.exit161
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %r14
	movslq	64(%rbx), %r13
	movl	72(%rbx), %edx
	movl	68(%rbx), %esi
	addl	%r13d, %esi
	leal	-1(%rdx,%rsi), %edx
	cmpl	%edx, %r13d
	jg	.LBB11_16
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB11_2 Depth=1
	movslq	%edx, %r12
	jmp	.LBB11_9
	.p2align	4, 0x90
.LBB11_13:                              # %clause_GetLiteralAtom.exit153._crit_edge
                                        #   in Loop: Header=BB11_9 Depth=2
	incq	%r13
	movq	56(%rbx), %rax
	movl	fol_NOT(%rip), %ecx
.LBB11_9:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	cmpl	(%rdi), %ecx
	jne	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_9 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB11_11:                              # %clause_GetLiteralAtom.exit153
                                        #   in Loop: Header=BB11_9 Depth=2
	movl	(%r14), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB11_14
# BB#12:                                # %clause_GetLiteralAtom.exit153
                                        #   in Loop: Header=BB11_9 Depth=2
	cmpq	%r12, %r13
	jl	.LBB11_13
.LBB11_14:                              # %._crit_edge
                                        #   in Loop: Header=BB11_2 Depth=1
	testl	%eax, %eax
	je	.LBB11_15
.LBB11_51:                              # %list_Delete.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jl	.LBB11_2
	jmp	.LBB11_52
.LBB11_15:                              # %._crit_edge..critedge179_crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdi
.LBB11_16:                              # %.critedge179
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	inf_GetForwardPartnerLits
	testq	%rax, %rax
	je	.LBB11_52
# BB#17:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rdx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rdx), %ecx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jne	.LBB11_18
# BB#19:
	movq	16(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB11_20
.LBB11_52:
	xorl	%eax, %eax
	jmp	.LBB11_53
.LBB11_18:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
.LBB11_20:                              # %clause_LiteralAtom.exit
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.LBB11_21
.LBB11_31:                              #   in Loop: Header=BB11_21 Depth=1
	movq	%rsi, (%rcx)
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB11_21:                              # %sort_Intersect.exit132.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_22 Depth 2
                                        #       Child Loop BB11_23 Depth 3
                                        #     Child Loop BB11_30 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB11_22:                              # %sort_Intersect.exit132.outer
                                        #   Parent Loop BB11_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_23 Depth 3
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	fol_NOT(%rip), %eax
	movslq	%r15d, %r12
	decq	%r12
	.p2align	4, 0x90
.LBB11_23:                              # %sort_Intersect.exit132
                                        #   Parent Loop BB11_21 Depth=1
                                        #     Parent Loop BB11_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %r12
	jle	.LBB11_32
# BB#24:                                #   in Loop: Header=BB11_23 Depth=3
	movq	56(%rbx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	24(%rcx), %r13
	cmpl	(%r13), %eax
	jne	.LBB11_26
# BB#25:                                #   in Loop: Header=BB11_23 Depth=3
	movq	16(%r13), %rcx
	movq	8(%rcx), %r13
.LBB11_26:                              # %clause_GetLiteralAtom.exit142
                                        #   in Loop: Header=BB11_23 Depth=3
	movq	16(%r13), %rcx
	decq	%r12
	decl	%r15d
	cmpq	%r14, 8(%rcx)
	jne	.LBB11_23
# BB#27:                                #   in Loop: Header=BB11_22 Depth=2
	incq	%r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%r12, 8(%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	%rcx, %r12
	movl	(%r13), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	movq	%r12, %rdi
	testq	%rax, %rax
	movq	%rdi, %rcx
	je	.LBB11_22
# BB#28:                                #   in Loop: Header=BB11_21 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movq	%rdi, %rcx
	je	.LBB11_21
# BB#29:                                # %.preheader.i.i130.preheader
                                        #   in Loop: Header=BB11_21 Depth=1
	movq	%rax, %rdx
.LBB11_30:                              # %.preheader.i.i130
                                        #   Parent Loop BB11_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB11_30
	jmp	.LBB11_31
.LBB11_32:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB11_33
# BB#34:
	movq	16(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB11_38
# BB#35:                                # %.preheader.i.i.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB11_36:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB11_36
# BB#37:
	movq	%rdx, (%rax)
	jmp	.LBB11_38
.LBB11_33:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB11_38:                              # %.lr.ph.i125.preheader
	movq	%r13, %rdi
	callq	list_PointerDeleteDuplicates
	xorl	%r15d, %r15d
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB11_39:                              # %.lr.ph.i125
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_44 Depth 2
	movq	8(%rbp), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB11_41
# BB#40:                                #   in Loop: Header=BB11_39 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB11_41:                              # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB11_39 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB11_47
# BB#42:                                #   in Loop: Header=BB11_39 Depth=1
	testq	%r15, %r15
	je	.LBB11_46
# BB#43:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB11_39 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB11_44:                              # %.preheader.i.i.i
                                        #   Parent Loop BB11_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB11_44
# BB#45:                                #   in Loop: Header=BB11_39 Depth=1
	movq	%r15, (%rcx)
.LBB11_46:                              # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB11_39 Depth=1
	movq	%rax, %r15
.LBB11_47:                              # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB11_39 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_39
# BB#48:                                # %inf_GetSortFromLits.exit
	movq	%r15, %rdi
	callq	list_PointerDeleteDuplicates
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	sort_TheoryComputeAllSubsortHits
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	sort_Delete
	movq	%r13, %rdi
	callq	sort_Delete
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rbp, %r8
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	inf_InternWeakening
	testq	%r14, %r14
	je	.LBB11_50
	.p2align	4, 0x90
.LBB11_49:                              # %.lr.ph.i123
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r14
	jne	.LBB11_49
	.p2align	4, 0x90
.LBB11_50:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r12
	jne	.LBB11_50
.LBB11_53:                              # %.critedge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	inf_ForwardEmptySortPlusPlus, .Lfunc_end11-inf_ForwardEmptySortPlusPlus
	.cfi_endproc

	.globl	inf_BackwardEmptySortPlusPlus
	.p2align	4, 0x90
	.type	inf_BackwardEmptySortPlusPlus,@function
inf_BackwardEmptySortPlusPlus:          # @inf_BackwardEmptySortPlusPlus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 224
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	movl	68(%rdi), %eax
	movl	72(%rdi), %ecx
	addl	64(%rdi), %eax
	leal	-1(%rcx,%rax), %edx
	cmpl	%edx, %eax
	jle	.LBB12_2
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB12_80
.LBB12_2:                               # %.lr.ph217
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movslq	%eax, %r9
	movslq	%edx, %r10
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %rdx
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movq	%r15, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_16 Depth 2
                                        #       Child Loop BB12_22 Depth 3
                                        #     Child Loop BB12_27 Depth 2
                                        #       Child Loop BB12_30 Depth 3
                                        #       Child Loop BB12_33 Depth 3
                                        #       Child Loop BB12_42 Depth 3
                                        #         Child Loop BB12_43 Depth 4
                                        #           Child Loop BB12_45 Depth 5
                                        #         Child Loop BB12_55 Depth 4
                                        #       Child Loop BB12_59 Depth 3
                                        #       Child Loop BB12_65 Depth 3
                                        #       Child Loop BB12_69 Depth 3
                                        #       Child Loop BB12_73 Depth 3
                                        #     Child Loop BB12_77 Depth 2
	movq	56(%rdi), %rax
	movq	(%rax,%r9,8), %r12
	movq	24(%r12), %rbx
	movl	(%rbx), %eax
	movl	fol_NOT(%rip), %r11d
	cmpl	%eax, %r11d
	movq	%rbx, %r13
	jne	.LBB12_5
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	16(%rbx), %rbp
	movq	8(%rbp), %r13
.LBB12_5:                               # %clause_LiteralAtom.exit181
                                        #   in Loop: Header=BB12_3 Depth=1
	testb	$2, (%r12)
	je	.LBB12_79
# BB#6:                                 #   in Loop: Header=BB12_3 Depth=1
	cmpl	%eax, %r11d
	jne	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB12_8:                               # %clause_LiteralPredicate.exit.i191
                                        #   in Loop: Header=BB12_3 Depth=1
	testl	%eax, %eax
	jns	.LBB12_79
# BB#9:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB12_3 Depth=1
	negl	%eax
	movl	%r8d, %ebp
	andl	%eax, %ebp
	cmpl	$2, %ebp
	jne	.LBB12_79
# BB#10:                                # %clause_LiteralIsSort.exit
                                        #   in Loop: Header=BB12_3 Depth=1
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rbp
	cltq
	movq	(%rbp,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB12_79
# BB#11:                                #   in Loop: Header=BB12_3 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jle	.LBB12_13
# BB#12:                                #   in Loop: Header=BB12_3 Depth=1
	cmpl	$0, 64(%rdi)
	jne	.LBB12_79
.LBB12_13:                              #   in Loop: Header=BB12_3 Depth=1
	movq	%r10, 128(%rsp)         # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	$0, 32(%rsp)
	movq	$0, 80(%rsp)
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	leaq	80(%rsp), %rcx
	movq	104(%rsp), %r9          # 8-byte Reload
	callq	inf_GetBackwardPartnerLits
	movq	80(%rsp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 80(%rsp)
	testq	%rax, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	je	.LBB12_14
# BB#15:                                # %.lr.ph.i172.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_16:                              # %.lr.ph.i172
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_22 Depth 3
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	movl	(%rax), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB12_18
# BB#17:                                #   in Loop: Header=BB12_16 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB12_18:                              # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB12_16 Depth=2
	movq	%r15, %rdi
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	je	.LBB12_19
# BB#20:                                #   in Loop: Header=BB12_16 Depth=2
	testq	%rbp, %rbp
	je	.LBB12_24
# BB#21:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB12_16 Depth=2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB12_22:                              # %.preheader.i.i.i
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB12_22
# BB#23:                                #   in Loop: Header=BB12_16 Depth=2
	movq	%rbp, (%rdx)
	jmp	.LBB12_24
	.p2align	4, 0x90
.LBB12_19:                              #   in Loop: Header=BB12_16 Depth=2
	movq	%rbp, %rax
.LBB12_24:                              # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB12_16 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %rbp
	jne	.LBB12_16
	jmp	.LBB12_25
.LBB12_14:                              #   in Loop: Header=BB12_3 Depth=1
	xorl	%eax, %eax
.LBB12_25:                              # %inf_GetSortFromLits.exit
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	32(%rsp), %r9
	testq	%r9, %r9
	je	.LBB12_76
# BB#26:                                # %.lr.ph211.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_27:                              # %.lr.ph211
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_30 Depth 3
                                        #       Child Loop BB12_33 Depth 3
                                        #       Child Loop BB12_42 Depth 3
                                        #         Child Loop BB12_43 Depth 4
                                        #           Child Loop BB12_45 Depth 5
                                        #         Child Loop BB12_55 Depth 4
                                        #       Child Loop BB12_59 Depth 3
                                        #       Child Loop BB12_65 Depth 3
                                        #       Child Loop BB12_69 Depth 3
                                        #       Child Loop BB12_73 Depth 3
	movq	8(%r9), %rdx
	movq	16(%rdx), %r8
	movq	24(%rdx), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB12_29
# BB#28:                                #   in Loop: Header=BB12_27 Depth=2
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rbx
.LBB12_29:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB12_27 Depth=2
	movq	56(%r8), %rcx
	movq	$-1, %rsi
	xorl	%edi, %edi
	movabsq	$-4294967296, %r13      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB12_30:                              #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edi, %ebp
	addq	%r14, %r13
	leal	1(%rbp), %edi
	cmpq	%rdx, 8(%rcx,%rsi,8)
	leaq	1(%rsi), %rsi
	jne	.LBB12_30
# BB#31:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB12_27 Depth=2
	movl	64(%r8), %r12d
	movl	72(%r8), %edx
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movl	68(%r8), %esi
	addl	%r12d, %esi
	leal	-1(%rdx,%rsi), %edx
	movq	16(%rbx), %rsi
	movq	8(%rsi), %r14
	cmpl	%edx, %r12d
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	jg	.LBB12_41
# BB#32:                                # %.lr.ph
                                        #   in Loop: Header=BB12_27 Depth=2
	movslq	%r12d, %rbx
	movslq	%edx, %r15
	movq	72(%rsp), %r12          # 8-byte Reload
	jmp	.LBB12_33
	.p2align	4, 0x90
.LBB12_37:                              # %clause_GetLiteralAtom.exit164._crit_edge
                                        #   in Loop: Header=BB12_33 Depth=3
	incq	%rbx
	movq	56(%r12), %rcx
	movl	fol_NOT(%rip), %eax
.LBB12_33:                              #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rbx,8), %rcx
	movq	24(%rcx), %rdi
	cmpl	(%rdi), %eax
	jne	.LBB12_35
# BB#34:                                #   in Loop: Header=BB12_33 Depth=3
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB12_35:                              # %clause_GetLiteralAtom.exit164
                                        #   in Loop: Header=BB12_33 Depth=3
	movl	(%r14), %esi
	callq	term_ContainsSymbol
	cmpq	%r15, %rbx
	jge	.LBB12_38
# BB#36:                                # %clause_GetLiteralAtom.exit164
                                        #   in Loop: Header=BB12_33 Depth=3
	testl	%eax, %eax
	je	.LBB12_37
.LBB12_38:                              # %._crit_edge
                                        #   in Loop: Header=BB12_27 Depth=2
	testl	%eax, %eax
	je	.LBB12_40
# BB#39:                                #   in Loop: Header=BB12_27 Depth=2
	movq	56(%rsp), %r15          # 8-byte Reload
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB12_75
.LBB12_40:                              # %._crit_edge..critedge_crit_edge
                                        #   in Loop: Header=BB12_27 Depth=2
	movl	64(%r12), %r12d
.LBB12_41:                              # %.critedge
                                        #   in Loop: Header=BB12_27 Depth=2
	movl	%ebp, %r15d
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.LBB12_42
.LBB12_56:                              #   in Loop: Header=BB12_42 Depth=3
	movq	%rsi, (%rcx)
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB12_42:                              # %sort_Intersect.exit153.outer.outer
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_43 Depth 4
                                        #           Child Loop BB12_45 Depth 5
                                        #         Child Loop BB12_55 Depth 4
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB12_43:                              # %sort_Intersect.exit153.outer
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        #       Parent Loop BB12_42 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB12_45 Depth 5
	testl	%r12d, %r12d
	jle	.LBB12_49
# BB#44:                                # %.lr.ph205
                                        #   in Loop: Header=BB12_43 Depth=4
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	movslq	%r12d, %r12
	decq	%r12
	.p2align	4, 0x90
.LBB12_45:                              #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        #       Parent Loop BB12_42 Depth=3
                                        #         Parent Loop BB12_43 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rax,%r12,8), %rdx
	movq	24(%rdx), %rbx
	cmpl	(%rbx), %ecx
	jne	.LBB12_47
# BB#46:                                #   in Loop: Header=BB12_45 Depth=5
	movq	16(%rbx), %rdx
	movq	8(%rdx), %rbx
.LBB12_47:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB12_45 Depth=5
	cmpq	%r12, %r15
	je	.LBB12_48
# BB#51:                                #   in Loop: Header=BB12_45 Depth=5
	movq	16(%rbx), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
	cmpl	(%r14), %edx
	je	.LBB12_52
.LBB12_48:                              # %sort_Intersect.exit153.backedge
                                        #   in Loop: Header=BB12_45 Depth=5
	leaq	-1(%r12), %rdx
	incq	%r12
	cmpq	$1, %r12
	movq	%rdx, %r12
	jg	.LBB12_45
	jmp	.LBB12_49
	.p2align	4, 0x90
.LBB12_52:                              #   in Loop: Header=BB12_43 Depth=4
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rbp)
	movl	(%rbx), %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	sort_TheorySortOfSymbol
	testq	%rax, %rax
	movq	%rbp, %rdi
	je	.LBB12_43
# BB#53:                                #   in Loop: Header=BB12_42 Depth=3
	movq	64(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movq	%rbp, %rdi
	je	.LBB12_42
# BB#54:                                # %.preheader.i.i151.preheader
                                        #   in Loop: Header=BB12_42 Depth=3
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB12_55:                              # %.preheader.i.i151
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        #       Parent Loop BB12_42 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB12_55
	jmp	.LBB12_56
	.p2align	4, 0x90
.LBB12_49:                              # %sort_Intersect.exit153.outer._crit_edge
                                        #   in Loop: Header=BB12_27 Depth=2
	sarq	$32, %r13
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r13, 8(%r14)
	movq	%rbx, (%r14)
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	callq	sort_TheorySortOfSymbol
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_50
# BB#57:                                #   in Loop: Header=BB12_27 Depth=2
	movq	64(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB12_61
# BB#58:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB12_27 Depth=2
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB12_59:                              # %.preheader.i.i
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB12_59
# BB#60:                                #   in Loop: Header=BB12_27 Depth=2
	movq	%rdx, (%rax)
	jmp	.LBB12_61
	.p2align	4, 0x90
.LBB12_50:                              #   in Loop: Header=BB12_27 Depth=2
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB12_61:                              # %sort_Intersect.exit
                                        #   in Loop: Header=BB12_27 Depth=2
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%r15, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	sort_TheoryComputeAllSubsortHits
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	sort_Delete
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	%rbx, %rcx
	callq	unify_UnifyNoOC
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	104(%rsp), %r9          # 8-byte Reload
	callq	inf_InternWeakening
	testq	%rax, %rax
	je	.LBB12_62
# BB#63:                                #   in Loop: Header=BB12_27 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB12_67
# BB#64:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB12_27 Depth=2
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB12_65:                              # %.preheader.i
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB12_65
# BB#66:                                #   in Loop: Header=BB12_27 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx)
	jmp	.LBB12_67
	.p2align	4, 0x90
.LBB12_62:                              #   in Loop: Header=BB12_27 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB12_67:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB12_27 Depth=2
	xorps	%xmm0, %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB12_70
# BB#68:                                # %.lr.ph.i147.preheader
                                        #   in Loop: Header=BB12_27 Depth=2
	incl	%ecx
	.p2align	4, 0x90
.LBB12_69:                              # %.lr.ph.i147
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	24(%rdx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rdx)
	movl	$0, 20(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 24(%rdx)
	leal	-2(%rcx), %edx
	movl	%edx, cont_BINDINGS(%rip)
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB12_69
.LBB12_70:                              # %._crit_edge.i
                                        #   in Loop: Header=BB12_27 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB12_72
# BB#71:                                #   in Loop: Header=BB12_27 Depth=2
	leaq	-1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB12_72:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB12_27 Depth=2
	testq	%r14, %r14
	je	.LBB12_74
	.p2align	4, 0x90
.LBB12_73:                              # %.lr.ph.i145
                                        #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r14, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB12_73
.LBB12_74:                              #   in Loop: Header=BB12_27 Depth=2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %r14       # imm = 0x100000000
.LBB12_75:                              # %list_Delete.exit146
                                        #   in Loop: Header=BB12_27 Depth=2
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	movq	%rax, 32(%rsp)
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.LBB12_27
.LBB12_76:                              # %._crit_edge212
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	sort_Delete
	movq	96(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB12_78
	.p2align	4, 0x90
.LBB12_77:                              # %.lr.ph.i
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB12_77
.LBB12_78:                              # %list_Delete.exit
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	symbol_TYPEMASK(%rip), %ebx
	movl	%ebx, %r8d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	136(%rsp), %r9          # 8-byte Reload
	movq	128(%rsp), %r10         # 8-byte Reload
	leaq	32(%rsp), %rdx
	.p2align	4, 0x90
.LBB12_79:                              # %clause_LiteralIsSort.exit.thread
                                        #   in Loop: Header=BB12_3 Depth=1
	cmpq	%r10, %r9
	leaq	1(%r9), %r9
	jl	.LBB12_3
.LBB12_80:                              # %._crit_edge218
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	inf_BackwardEmptySortPlusPlus, .Lfunc_end12-inf_BackwardEmptySortPlusPlus
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_SetSplitDataFromList,@function
clause_SetSplitDataFromList:            # @clause_SetSplitDataFromList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 32
.Lcfi187:
	.cfi_offset %rbx, -32
.Lcfi188:
	.cfi_offset %r14, -24
.Lcfi189:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	24(%rbx), %ecx
	testq	%r14, %r14
	je	.LBB13_15
# BB#1:                                 # %.lr.ph60
	movl	%ecx, %r15d
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	testb	$8, 48(%rdx)
	je	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	orb	$8, 48(%rbx)
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	movl	12(%rdx), %esi
	cmpl	12(%rbx), %esi
	movq	%rbx, %rsi
	cmovaq	%rdx, %rsi
	movl	12(%rsi), %esi
	movl	%esi, 12(%rbx)
	movl	24(%rdx), %edx
	cmpl	%edx, %r15d
	cmovbl	%edx, %r15d
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_2
# BB#5:                                 # %._crit_edge61
	cmpl	%ecx, %r15d
	jbe	.LBB13_15
# BB#6:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_14
# BB#7:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB13_8
# BB#13:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB13_14
.LBB13_15:                              # %.preheader51
	testl	%ecx, %ecx
	jne	.LBB13_16
	jmp	.LBB13_18
.LBB13_8:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_10
# BB#9:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB13_10:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB13_12
# BB#11:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB13_12:
	addq	$-16, %rdi
	callq	free
.LBB13_14:                              # %.preheader51.thread
	leal	(,%r15,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbx)
	movl	%r15d, 24(%rbx)
.LBB13_16:                              # %.lr.ph56
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_17:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movl	%eax, %edx
	movq	$0, (%rcx,%rdx,8)
	incl	%eax
	cmpl	24(%rbx), %eax
	jb	.LBB13_17
	jmp	.LBB13_18
	.p2align	4, 0x90
.LBB13_19:                              #   in Loop: Header=BB13_18 Depth=1
	movq	8(%r14), %rax
	movq	(%r14), %r14
	cmpl	$0, 24(%rax)
	je	.LBB13_18
# BB#20:                                # %.lr.ph
                                        #   in Loop: Header=BB13_18 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_21:                              #   Parent Loop BB13_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rdx
	movl	%ecx, %esi
	movq	16(%rax), %rdi
	movq	(%rdi,%rsi,8), %rdi
	orq	%rdi, (%rdx,%rsi,8)
	incl	%ecx
	cmpl	24(%rax), %ecx
	jb	.LBB13_21
.LBB13_18:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_21 Depth 2
	testq	%r14, %r14
	jne	.LBB13_19
# BB#22:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	clause_SetSplitDataFromList, .Lfunc_end13-clause_SetSplitDataFromList
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_LiteralsHaveSameSubtermAndAreFromSameClause,@function
inf_LiteralsHaveSameSubtermAndAreFromSameClause: # @inf_LiteralsHaveSameSubtermAndAreFromSameClause
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB14_2
# BB#1:
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
.LBB14_2:                               # %clause_LiteralAtom.exit
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	24(%rsi), %rdx
	cmpl	(%rdx), %eax
	jne	.LBB14_4
# BB#3:
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB14_4:                               # %clause_LiteralAtom.exit16
	movq	16(%rdx), %rax
	cmpq	8(%rax), %rcx
	je	.LBB14_6
# BB#5:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB14_6:
	movq	16(%rdi), %rax
	cmpq	16(%rsi), %rax
	sete	%al
	movzbl	%al, %eax
	retq
.Lfunc_end14:
	.size	inf_LiteralsHaveSameSubtermAndAreFromSameClause, .Lfunc_end14-inf_LiteralsHaveSameSubtermAndAreFromSameClause
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/rules-sort.c"
	.size	.L.str.1, 82

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n In inf_BuildConstraintHyperResolvent: Unification failed."
	.size	.L.str.2, 60

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\n"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nT_k = "
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nS_k ="
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" in "
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nSOJU: "
	.size	.L.str.8, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
