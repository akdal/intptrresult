	.text
	.file	"condensing.bc"
	.globl	cond_CondFast
	.p2align	4, 0x90
	.type	cond_CondFast,@function
cond_CondFast:                          # @cond_CondFast
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	vec_MAX(%rip), %r14d
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	addl	72(%r12), %eax
	jle	.LBB0_1
# BB#5:                                 # %.lr.ph68.preheader
	movslq	%r14d, %rax
	leaq	vec_VECTOR(,%rax,8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph68
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%r14,%rcx), %edx
	movl	%edx, vec_MAX(%rip)
	movq	%rcx, (%rax,%rcx,8)
	incq	%rcx
	movslq	68(%r12), %rdx
	movslq	72(%r12), %rsi
	movslq	64(%r12), %rbx
	addq	%rdx, %rbx
	addq	%rsi, %rbx
	movslq	%ebx, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB0_6
# BB#3:                                 # %.preheader51
	testl	%ebx, %ebx
	jle	.LBB0_1
# BB#4:                                 # %.preheader50.lr.ph
	addq	%r14, %rcx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
                                        #       Child Loop BB0_13 Depth 3
                                        #       Child Loop BB0_19 Depth 3
	movq	%rbx, %r13
	leaq	-1(%r13), %rbx
	cmpl	%ecx, %r14d
	jge	.LBB0_7
# BB#9:                                 # %.lr.ph56
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_13 Depth 3
                                        #       Child Loop BB0_19 Depth 3
	movl	%ebp, %eax
	movq	vec_VECTOR(,%rax,8), %rax
	movl	%eax, %edx
	cmpq	%rbx, %rdx
	je	.LBB0_26
# BB#11:                                #   in Loop: Header=BB0_10 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r12), %rcx
	movq	-8(%rcx,%r13,8), %rdx
	movq	24(%rdx), %rsi
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdx
	callq	unify_Match
	xorps	%xmm0, %xmm0
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_14
# BB#12:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_10 Depth=2
	incl	%ecx
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.i
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	24(%rdx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rdx)
	movl	$0, 20(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 24(%rdx)
	leal	-2(%rcx), %edx
	movl	%edx, cont_BINDINGS(%rip)
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB0_13
.LBB0_14:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_10 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_10 Depth=2
	leaq	-1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB0_16:                               # %cont_BackTrack.exit
                                        #   in Loop: Header=BB0_10 Depth=2
	testl	%eax, %eax
	movl	vec_MAX(%rip), %ecx
	je	.LBB0_26
# BB#17:                                # %.preheader
                                        #   in Loop: Header=BB0_10 Depth=2
	cmpl	%ecx, %r14d
	jge	.LBB0_22
# BB#18:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_10 Depth=2
	movl	%r14d, %esi
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %eax
	movl	vec_VECTOR(,%rax,8), %eax
	cmpq	%rax, %rbx
	jne	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_19 Depth=3
	decl	%ecx
	movl	%ecx, %edi
	callq	vec_Swap
	movl	vec_MAX(%rip), %ecx
	movl	%ecx, %esi
.LBB0_21:                               #   in Loop: Header=BB0_19 Depth=3
	incl	%esi
	cmpl	%ecx, %esi
	jl	.LBB0_19
.LBB0_22:                               # %._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=2
	decl	%ecx
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%ecx, %edx
	callq	subs_IdcRes
	testl	%eax, %eax
	je	.LBB0_23
# BB#24:                                #   in Loop: Header=BB0_10 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	vec_MAX(%rip), %ecx
	decl	%ecx
	movl	%ecx, vec_MAX(%rip)
	movq	%rax, %r15
	jmp	.LBB0_25
.LBB0_23:                               # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=2
	movl	vec_MAX(%rip), %ecx
.LBB0_25:                               #   in Loop: Header=BB0_10 Depth=2
	leal	1(%rcx), %ebp
.LBB0_26:                               # %cont_BackTrack.exit._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=2
	incl	%ebp
	cmpl	%ecx, %ebp
	jl	.LBB0_10
.LBB0_7:                                # %.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpq	$2, %r13
	jge	.LBB0_8
	jmp	.LBB0_2
.LBB0_1:
	xorl	%r15d, %r15d
.LBB0_2:                                # %._crit_edge61
	movl	%r14d, vec_MAX(%rip)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cond_CondFast, .Lfunc_end0-cond_CondFast
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
