	.text
	.file	"lgc.bc"
	.hidden	luaC_separateudata
	.globl	luaC_separateudata
	.p2align	4, 0x90
	.type	luaC_separateudata,@function
luaC_separateudata:                     # @luaC_separateudata
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	32(%r14), %r15
	movq	176(%r15), %r12
	movq	(%r12), %rax
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.LBB0_25
# BB#1:                                 # %.lr.ph.lr.ph
	testl	%esi, %esi
	je	.LBB0_13
.LBB0_2:                                # %.lr.ph.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	9(%rbx), %eax
	testb	$8, %al
	jne	.LBB0_9
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=2
	testb	$4, 10(%rdi)
	jne	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	32(%r14), %rax
	movq	312(%rax), %rdx
	movl	$2, %esi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB0_7
# BB#10:                                #   in Loop: Header=BB0_3 Depth=2
	movq	32(%rbx), %rax
	orb	$8, 9(%rbx)
	movq	(%rbx), %rcx
	movq	%rcx, (%r12)
	movq	80(%r15), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %rdx
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_3 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rbx)
	movq	%rcx, %rdx
.LBB0_12:                               #   in Loop: Header=BB0_3 Depth=2
	leaq	40(%r13,%rax), %r13
	movq	%rbx, (%rdx)
	movq	%rbx, 80(%r15)
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_3
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_7:                                # %..thread.us-lcssa.us.us_crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movb	9(%rbx), %al
.LBB0_8:                                # %.thread.us-lcssa.us.us
                                        #   in Loop: Header=BB0_2 Depth=1
	orb	$8, %al
	movb	%al, 9(%rbx)
.LBB0_9:                                # %.outer.backedge.us
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %r12
	jne	.LBB0_2
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	9(%rbx), %eax
	testb	$8, %al
	jne	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_14 Depth=2
	movl	%eax, %ecx
	andb	$3, %cl
	je	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_14 Depth=2
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_21
# BB#18:                                #   in Loop: Header=BB0_14 Depth=2
	testb	$4, 10(%rdi)
	jne	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_14 Depth=2
	movq	32(%r14), %rax
	movq	312(%rax), %rdx
	movl	$2, %esi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB0_20
# BB#22:                                #   in Loop: Header=BB0_14 Depth=2
	movq	32(%rbx), %rax
	orb	$8, 9(%rbx)
	movq	(%rbx), %rcx
	movq	%rcx, (%r12)
	movq	80(%r15), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %rdx
	je	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_14 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rbx)
	movq	%rcx, %rdx
.LBB0_24:                               #   in Loop: Header=BB0_14 Depth=2
	leaq	40(%r13,%rax), %r13
	movq	%rbx, (%rdx)
	movq	%rbx, 80(%r15)
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_14
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_20:                               # %..thread_crit_edge
                                        #   in Loop: Header=BB0_13 Depth=1
	movb	9(%rbx), %al
.LBB0_21:                               # %.thread
                                        #   in Loop: Header=BB0_13 Depth=1
	orb	$8, %al
	movb	%al, 9(%rbx)
.LBB0_16:                               # %.outer.backedge
                                        #   in Loop: Header=BB0_13 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %r12
	jne	.LBB0_13
.LBB0_25:                               # %.outer._crit_edge
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	luaC_separateudata, .Lfunc_end0-luaC_separateudata
	.cfi_endproc

	.hidden	luaC_callGCTM
	.globl	luaC_callGCTM
	.p2align	4, 0x90
	.type	luaC_callGCTM,@function
luaC_callGCTM:                          # @luaC_callGCTM
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	GCTM
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rbx), %rax
	cmpq	$0, 80(%rax)
	jne	.LBB1_1
# BB#3:                                 # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	luaC_callGCTM, .Lfunc_end1-luaC_callGCTM
	.cfi_endproc

	.p2align	4, 0x90
	.type	GCTM,@function
GCTM:                                   # @GCTM
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	32(%r14), %r15
	movq	80(%r15), %rax
	movq	(%rax), %rbx
	cmpq	%rax, %rbx
	je	.LBB2_1
# BB#2:
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	jmp	.LBB2_3
.LBB2_1:
	movq	$0, 80(%r15)
.LBB2_3:
	movq	176(%r15), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rbx)
	movq	%rbx, (%rax)
	movb	9(%rbx), %al
	andb	$-8, %al
	movb	32(%r15), %cl
	andb	$3, %cl
	orb	%al, %cl
	movb	%cl, 9(%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#4:
	testb	$4, 10(%rdi)
	jne	.LBB2_7
# BB#5:
	movq	312(%r15), %rdx
	movl	$2, %esi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB2_7
# BB#6:
	movb	101(%r14), %bpl
	movq	112(%r15), %r12
	movb	$0, 101(%r14)
	movq	120(%r15), %rcx
	addq	%rcx, %rcx
	movq	%rcx, 112(%r15)
	movq	16(%r14), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	movq	16(%r14), %rax
	movq	%rbx, 16(%rax)
	movl	$7, 24(%rax)
	movq	16(%r14), %rsi
	leaq	32(%rsi), %rax
	movq	%rax, 16(%r14)
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	luaD_call
	movb	%bpl, 101(%r14)
	movq	%r12, 112(%r15)
.LBB2_7:                                # %.thread
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	GCTM, .Lfunc_end2-GCTM
	.cfi_endproc

	.hidden	luaC_freeall
	.globl	luaC_freeall
	.p2align	4, 0x90
	.type	luaC_freeall,@function
luaC_freeall:                           # @luaC_freeall
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r12, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	32(%r14), %r15
	movb	$67, 32(%r15)
	leaq	40(%r15), %rsi
	movq	$-3, %rdx
	callq	sweeplist
	cmpl	$0, 12(%r15)
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rsi
	addq	%r12, %rsi
	movq	$-3, %rdx
	movq	%r14, %rdi
	callq	sweeplist
	incq	%rbx
	movslq	12(%r15), %rax
	addq	$8, %r12
	cmpq	%rax, %rbx
	jl	.LBB3_2
.LBB3_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	luaC_freeall, .Lfunc_end3-luaC_freeall
	.cfi_endproc

	.p2align	4, 0x90
	.type	sweeplist,@function
sweeplist:                              # @sweeplist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB4_8
# BB#1:                                 # %.lr.ph
	movq	32(%r14), %r13
	movb	32(%r13), %bpl
	xorb	$3, %bpl
	notq	%r12
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rax
	movq	%rax, (%r15)
	cmpq	40(%r13), %rbx
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rax
	movq	%rax, 40(%r13)
.LBB4_11:                               #   in Loop: Header=BB4_2 Depth=1
	movzbl	8(%rbx), %eax
	addb	$-4, %al
	cmpb	$6, %al
	ja	.LBB4_7
# BB#12:                                #   in Loop: Header=BB4_2 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_18:                               #   in Loop: Header=BB4_2 Depth=1
	movq	32(%r14), %rax
	decl	8(%rax)
	movq	16(%rbx), %rdx
	addq	$25, %rdx
	jmp	.LBB4_20
.LBB4_16:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaH_free
	jmp	.LBB4_7
.LBB4_14:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaF_freeclosure
	jmp	.LBB4_7
.LBB4_19:                               #   in Loop: Header=BB4_2 Depth=1
	movq	32(%rbx), %rdx
	addq	$40, %rdx
.LBB4_20:                               # %freeobj.exit.backedge
                                        #   in Loop: Header=BB4_2 Depth=1
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaM_realloc_
	jmp	.LBB4_7
.LBB4_17:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaE_freethread
	jmp	.LBB4_7
.LBB4_13:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaF_freeproto
	jmp	.LBB4_7
.LBB4_15:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaF_freeupval
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	incq	%r12
	je	.LBB4_8
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpb	$8, 8(%rbx)
	jne	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	leaq	152(%rbx), %rsi
	movq	$-3, %rdx
	movq	%r14, %rdi
	callq	sweeplist
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movzbl	9(%rbx), %eax
	movl	%eax, %ecx
	xorb	$3, %cl
	testb	%bpl, %cl
	je	.LBB4_9
# BB#6:                                 #   in Loop: Header=BB4_2 Depth=1
	andb	$-8, %al
	movzbl	32(%r13), %ecx
	andb	$3, %cl
	orb	%al, %cl
	movb	%cl, 9(%rbx)
	movq	%rbx, %r15
.LBB4_7:                                # %freeobj.exit.backedge
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
.LBB4_8:                                # %.critedge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	sweeplist, .Lfunc_end4-sweeplist
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_18
	.quad	.LBB4_16
	.quad	.LBB4_14
	.quad	.LBB4_19
	.quad	.LBB4_17
	.quad	.LBB4_13
	.quad	.LBB4_15

	.text
	.hidden	luaC_step
	.globl	luaC_step
	.p2align	4, 0x90
	.type	luaC_step,@function
luaC_step:                              # @luaC_step
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	32(%r15), %r14
	movl	148(%r14), %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	testl	%eax, %eax
	movabsq	$9223372036854775806, %rbx # imm = 0x7FFFFFFFFFFFFFFE
	cmovneq	%rax, %rbx
	movq	120(%r14), %rax
	subq	112(%r14), %rax
	addq	%rax, 136(%r14)
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	singlestep
	movzbl	33(%r14), %ecx
	testb	%cl, %cl
	je	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	subq	%rax, %rbx
	testq	%rbx, %rbx
	jg	.LBB5_1
.LBB5_3:
	testb	%cl, %cl
	je	.LBB5_7
# BB#4:
	movq	136(%r14), %rax
	cmpq	$1023, %rax             # imm = 0x3FF
	ja	.LBB5_6
# BB#5:
	movl	$1024, %eax             # imm = 0x400
	addq	120(%r14), %rax
	jmp	.LBB5_8
.LBB5_7:
	movq	128(%r14), %rax
	shrq	$2, %rax
	movabsq	$2951479051793528259, %rcx # imm = 0x28F5C28F5C28F5C3
	mulq	%rcx
	shrq	$2, %rdx
	movslq	144(%r14), %rax
	imulq	%rdx, %rax
	jmp	.LBB5_8
.LBB5_6:
	addq	$-1024, %rax            # imm = 0xFC00
	movq	%rax, 136(%r14)
	movq	120(%r14), %rax
.LBB5_8:
	movq	%rax, 112(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	luaC_step, .Lfunc_end5-luaC_step
	.cfi_endproc

	.p2align	4, 0x90
	.type	singlestep,@function
singlestep:                             # @singlestep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 64
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	32(%r14), %r12
	movzbl	33(%r12), %eax
	cmpq	$4, %rax
	ja	.LBB6_83
# BB#1:
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_2:
	movq	%r14, %rdi
	callq	markroot
	jmp	.LBB6_83
.LBB6_3:
	cmpq	$0, 56(%r12)
	je	.LBB6_19
# BB#4:
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	propagatemark           # TAILCALL
.LBB6_5:
	movq	120(%r12), %rbx
	movslq	36(%r12), %rsi
	leal	1(%rsi), %eax
	shlq	$3, %rsi
	addq	(%r12), %rsi
	movl	%eax, 36(%r12)
	movq	$-3, %rdx
	movq	%r14, %rdi
	callq	sweeplist
	movl	36(%r12), %eax
	cmpl	12(%r12), %eax
	jl	.LBB6_7
# BB#6:
	movb	$3, 33(%r12)
.LBB6_7:
	movq	120(%r12), %rax
	subq	%rbx, %rax
	addq	%rax, 128(%r12)
	movl	$10, %eax
	jmp	.LBB6_88
.LBB6_8:
	movq	48(%r12), %rsi
	movq	120(%r12), %rbx
	movl	$40, %edx
	movq	%r14, %rdi
	callq	sweeplist
	movq	%rax, 48(%r12)
	cmpq	$0, (%rax)
	jne	.LBB6_87
# BB#9:
	movq	32(%r14), %rbp
	movl	12(%rbp), %esi
	cmpl	$65, %esi
	jl	.LBB6_12
# BB#10:
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%esi, %eax
	sarl	$2, %eax
	cmpl	%eax, 8(%rbp)
	jae	.LBB6_12
# BB#11:
	shrl	%esi
	movq	%r14, %rdi
	callq	luaS_resize
.LBB6_12:
	movq	104(%rbp), %rdx
	cmpq	$65, %rdx
	jb	.LBB6_86
# BB#13:
	movq	%rdx, %r15
	shrq	%r15
	leaq	1(%r15), %rax
	cmpq	$-3, %rax
	ja	.LBB6_84
# BB#14:
	movq	88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r15, %rcx
	callq	luaM_realloc_
	jmp	.LBB6_85
.LBB6_15:
	cmpq	$0, 80(%r12)
	je	.LBB6_18
# BB#16:
	movq	%r14, %rdi
	callq	GCTM
	movq	128(%r12), %rax
	cmpq	$101, %rax
	jb	.LBB6_51
# BB#17:
	addq	$-100, %rax
	movq	%rax, 128(%r12)
	movl	$100, %eax
	jmp	.LBB6_88
.LBB6_18:
	movb	$0, 33(%r12)
	movq	$0, 136(%r12)
	jmp	.LBB6_83
.LBB6_19:
	leaq	184(%r12), %rbx
	movq	216(%r12), %rbp
	cmpq	%rbx, %rbp
	je	.LBB6_27
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	testb	$7, 9(%rbp)
	jne	.LBB6_24
# BB#21:                                #   in Loop: Header=BB6_20 Depth=1
	movq	16(%rbp), %rax
	cmpl	$4, 8(%rax)
	jl	.LBB6_24
# BB#22:                                #   in Loop: Header=BB6_20 Depth=1
	movq	(%rax), %rsi
	testb	$3, 9(%rsi)
	je	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_20 Depth=1
	movq	%r12, %rdi
	callq	reallymarkobject
	.p2align	4, 0x90
.LBB6_24:                               #   in Loop: Header=BB6_20 Depth=1
	movq	32(%rbp), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB6_20
	jmp	.LBB6_26
	.p2align	4, 0x90
.LBB6_25:                               # %.lr.ph.i31.i
                                        #   in Loop: Header=BB6_26 Depth=1
	movq	%r12, %rdi
	callq	propagatemark
.LBB6_26:                               # %.lr.ph.i31.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, 56(%r12)
	jne	.LBB6_25
.LBB6_27:                               # %propagateall.exit.i
	movq	72(%r12), %rax
	movq	%rax, 56(%r12)
	movq	$0, 72(%r12)
	testb	$3, 9(%r14)
	je	.LBB6_29
# BB#28:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	reallymarkobject
.LBB6_29:
	movq	%r12, %rdi
	callq	markmt
	cmpq	$0, 56(%r12)
	je	.LBB6_31
	.p2align	4, 0x90
.LBB6_30:                               # %.lr.ph.i33.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	propagatemark
	cmpq	$0, 56(%r12)
	jne	.LBB6_30
.LBB6_31:                               # %propagateall.exit35.i
	movq	64(%r12), %rax
	movq	%rax, 56(%r12)
	movq	$0, 64(%r12)
	testq	%rax, %rax
	je	.LBB6_33
	.p2align	4, 0x90
.LBB6_32:                               # %.lr.ph.i37.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	propagatemark
	cmpq	$0, 56(%r12)
	jne	.LBB6_32
.LBB6_33:                               # %propagateall.exit39.i
	movq	32(%r14), %r13
	movq	176(%r13), %rbx
	movq	(%rbx), %rax
	xorl	%r15d, %r15d
	testq	%rax, %rax
	je	.LBB6_46
.LBB6_34:                               # %.lr.ph.i40.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_35 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB6_35:                               #   Parent Loop BB6_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	9(%rbp), %eax
	testb	$8, %al
	jne	.LBB6_45
# BB#36:                                #   in Loop: Header=BB6_35 Depth=2
	movl	%eax, %ecx
	andb	$3, %cl
	je	.LBB6_45
# BB#37:                                #   in Loop: Header=BB6_35 Depth=2
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_44
# BB#38:                                #   in Loop: Header=BB6_35 Depth=2
	testb	$4, 10(%rdi)
	jne	.LBB6_44
# BB#39:                                #   in Loop: Header=BB6_35 Depth=2
	movq	32(%r14), %rax
	movq	312(%rax), %rdx
	movl	$2, %esi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB6_43
# BB#40:                                #   in Loop: Header=BB6_35 Depth=2
	movq	32(%rbp), %rax
	orb	$8, 9(%rbp)
	movq	(%rbp), %rcx
	movq	%rcx, (%rbx)
	movq	80(%r13), %rcx
	testq	%rcx, %rcx
	movq	%rbp, %rdx
	je	.LBB6_42
# BB#41:                                #   in Loop: Header=BB6_35 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, (%rbp)
	movq	%rcx, %rdx
.LBB6_42:                               #   in Loop: Header=BB6_35 Depth=2
	leaq	40(%r15,%rax), %r15
	movq	%rbp, (%rdx)
	movq	%rbp, 80(%r13)
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB6_35
	jmp	.LBB6_46
.LBB6_43:                               # %..thread_crit_edge.i.i
                                        #   in Loop: Header=BB6_34 Depth=1
	movb	9(%rbp), %al
.LBB6_44:                               # %.thread.i.i
                                        #   in Loop: Header=BB6_34 Depth=1
	orb	$8, %al
	movb	%al, 9(%rbp)
.LBB6_45:                               # %.outer.backedge.i.i
                                        #   in Loop: Header=BB6_34 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	movq	%rbp, %rbx
	jne	.LBB6_34
.LBB6_46:                               # %luaC_separateudata.exit.i
	movq	80(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB6_48
	.p2align	4, 0x90
.LBB6_47:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	9(%rbx), %eax
	andb	$-8, %al
	movzbl	32(%r12), %ecx
	andb	$3, %cl
	orb	%al, %cl
	movb	%cl, 9(%rbx)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	reallymarkobject
	cmpq	80(%r12), %rbx
	jne	.LBB6_47
.LBB6_48:                               # %marktmu.exit.i
	cmpq	$0, 56(%r12)
	je	.LBB6_52
# BB#49:                                # %.lr.ph.i44.i.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_50:                               # %.lr.ph.i44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	propagatemark
	addq	%rax, %r14
	cmpq	$0, 56(%r12)
	jne	.LBB6_50
	jmp	.LBB6_53
.LBB6_51:
	movl	$100, %eax
	jmp	.LBB6_88
.LBB6_52:
	xorl	%r14d, %r14d
.LBB6_53:                               # %propagateall.exit46.i
	movq	72(%r12), %rax
	testq	%rax, %rax
	je	.LBB6_82
	.p2align	4, 0x90
.LBB6_55:                               # %.lr.ph41.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_58 Depth 2
                                        #     Child Loop BB6_67 Depth 2
	movslq	56(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB6_66
# BB#56:                                # %.lr.ph41.i.i
                                        #   in Loop: Header=BB6_55 Depth=1
	movb	9(%rax), %dl
	andb	$16, %dl
	je	.LBB6_66
# BB#57:                                # %.lr.ph.i47.i
                                        #   in Loop: Header=BB6_55 Depth=1
	movq	24(%rax), %rdx
	movq	%rcx, %rsi
	shlq	$4, %rsi
	leaq	-8(%rdx,%rsi), %rdx
	.p2align	4, 0x90
.LBB6_58:                               #   Parent Loop BB6_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	cmpl	$4, %esi
	jl	.LBB6_65
# BB#59:                                #   in Loop: Header=BB6_58 Depth=2
	movq	-8(%rdx), %rdi
	movzbl	9(%rdi), %ebx
	jne	.LBB6_61
# BB#60:                                #   in Loop: Header=BB6_58 Depth=2
	andb	$-4, %bl
	movb	%bl, 9(%rdi)
	jmp	.LBB6_65
	.p2align	4, 0x90
.LBB6_61:                               #   in Loop: Header=BB6_58 Depth=2
	testb	$3, %bl
	jne	.LBB6_64
# BB#62:                                #   in Loop: Header=BB6_58 Depth=2
	cmpl	$7, %esi
	jne	.LBB6_65
# BB#63:                                #   in Loop: Header=BB6_58 Depth=2
	andb	$8, %bl
	je	.LBB6_65
.LBB6_64:                               # %iscleared.exit.thread31.i.i
                                        #   in Loop: Header=BB6_58 Depth=2
	movl	$0, (%rdx)
.LBB6_65:                               # %iscleared.exit.thread30.backedge.i.i
                                        #   in Loop: Header=BB6_58 Depth=2
	addq	$-16, %rdx
	decl	%ecx
	jne	.LBB6_58
.LBB6_66:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB6_55 Depth=1
	movb	11(%rax), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movslq	%edx, %rcx
	movq	32(%rax), %rsi
	leaq	(%rcx,%rcx,4), %rcx
	leaq	-16(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB6_67:                               #   Parent Loop BB6_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-16(%rcx), %edi
	testl	%edi, %edi
	je	.LBB6_81
# BB#68:                                #   in Loop: Header=BB6_67 Depth=2
	movl	(%rcx), %esi
	cmpl	$4, %esi
	jl	.LBB6_73
# BB#69:                                #   in Loop: Header=BB6_67 Depth=2
	movq	-8(%rcx), %rbp
	movzbl	9(%rbp), %ebx
	jne	.LBB6_71
# BB#70:                                #   in Loop: Header=BB6_67 Depth=2
	andb	$-4, %bl
	movb	%bl, 9(%rbp)
	cmpl	$4, %edi
	jge	.LBB6_74
	jmp	.LBB6_81
.LBB6_71:                               #   in Loop: Header=BB6_67 Depth=2
	testb	$3, %bl
	je	.LBB6_73
# BB#72:                                # %iscleared.exit29.thread33.thread.i.i
                                        #   in Loop: Header=BB6_67 Depth=2
	movl	$0, -16(%rcx)
	jmp	.LBB6_80
	.p2align	4, 0x90
.LBB6_73:                               # %iscleared.exit29.thread32.i.i
                                        #   in Loop: Header=BB6_67 Depth=2
	cmpl	$4, %edi
	jl	.LBB6_81
.LBB6_74:                               #   in Loop: Header=BB6_67 Depth=2
	movq	-24(%rcx), %rbp
	movzbl	9(%rbp), %ebx
	jne	.LBB6_76
# BB#75:                                #   in Loop: Header=BB6_67 Depth=2
	andb	$-4, %bl
	movb	%bl, 9(%rbp)
	jmp	.LBB6_81
.LBB6_76:                               #   in Loop: Header=BB6_67 Depth=2
	testb	$3, %bl
	jne	.LBB6_79
# BB#77:                                #   in Loop: Header=BB6_67 Depth=2
	cmpl	$7, %edi
	jne	.LBB6_81
# BB#78:                                #   in Loop: Header=BB6_67 Depth=2
	andb	$8, %bl
	je	.LBB6_81
.LBB6_79:                               # %iscleared.exit29.thread33.i.i
                                        #   in Loop: Header=BB6_67 Depth=2
	movl	$0, -16(%rcx)
	cmpl	$4, %esi
	jl	.LBB6_81
.LBB6_80:                               #   in Loop: Header=BB6_67 Depth=2
	movl	$11, (%rcx)
	.p2align	4, 0x90
.LBB6_81:                               # %removeentry.exit.backedge.i.i
                                        #   in Loop: Header=BB6_67 Depth=2
	addq	$-40, %rcx
	decl	%edx
	jne	.LBB6_67
# BB#54:                                #   in Loop: Header=BB6_55 Depth=1
	movq	48(%rax), %rax
	testq	%rax, %rax
	jne	.LBB6_55
.LBB6_82:                               # %atomic.exit
	xorb	$3, 32(%r12)
	movl	$0, 36(%r12)
	leaq	40(%r12), %rax
	movq	%rax, 48(%r12)
	movb	$2, 33(%r12)
	movq	120(%r12), %rax
	addq	%r15, %r14
	subq	%r14, %rax
	movq	%rax, 128(%r12)
.LBB6_83:
	xorl	%eax, %eax
	jmp	.LBB6_88
.LBB6_84:
	movq	%r14, %rdi
	callq	luaM_toobig
.LBB6_85:
	movq	%rax, 88(%rbp)
	movq	%r15, 104(%rbp)
.LBB6_86:                               # %checkSizes.exit
	movb	$4, 33(%r12)
.LBB6_87:
	movq	120(%r12), %rax
	subq	%rbx, %rax
	addq	%rax, 128(%r12)
	movl	$400, %eax              # imm = 0x190
.LBB6_88:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	singlestep, .Lfunc_end6-singlestep
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_2
	.quad	.LBB6_3
	.quad	.LBB6_5
	.quad	.LBB6_8
	.quad	.LBB6_15

	.text
	.hidden	luaC_fullgc
	.globl	luaC_fullgc
	.p2align	4, 0x90
	.type	luaC_fullgc,@function
luaC_fullgc:                            # @luaC_fullgc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %r14
	movb	33(%r14), %al
	cmpb	$1, %al
	ja	.LBB7_2
# BB#1:                                 # %.preheader.thread
	movl	$0, 36(%r14)
	leaq	40(%r14), %rax
	movq	%rax, 48(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%r14)
	movq	$0, 72(%r14)
	movb	$2, 33(%r14)
	jmp	.LBB7_3
.LBB7_2:                                # %.preheader
	cmpb	$4, %al
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph17
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	singlestep
	cmpb	$4, 33(%r14)
	jne	.LBB7_3
.LBB7_4:                                # %._crit_edge18
	movq	%rbx, %rdi
	callq	markroot
	cmpb	$0, 33(%r14)
	je	.LBB7_7
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	singlestep
	cmpb	$0, 33(%r14)
	jne	.LBB7_5
.LBB7_7:                                # %._crit_edge
	movq	128(%r14), %rax
	shrq	$2, %rax
	movabsq	$2951479051793528259, %rcx # imm = 0x28F5C28F5C28F5C3
	mulq	%rcx
	shrq	$2, %rdx
	movslq	144(%r14), %rax
	imulq	%rdx, %rax
	movq	%rax, 112(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	luaC_fullgc, .Lfunc_end7-luaC_fullgc
	.cfi_endproc

	.p2align	4, 0x90
	.type	markroot,@function
markroot:                               # @markroot
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	176(%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB8_2
# BB#1:
	movq	%rbx, %rdi
	callq	reallymarkobject
	movq	176(%rbx), %rsi
.LBB8_2:
	cmpl	$4, 128(%rsi)
	jl	.LBB8_5
# BB#3:
	movq	120(%rsi), %rsi
	testb	$3, 9(%rsi)
	je	.LBB8_5
# BB#4:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB8_5:
	movq	32(%r14), %rax
	cmpl	$4, 168(%rax)
	jl	.LBB8_8
# BB#6:
	movq	160(%rax), %rsi
	testb	$3, 9(%rsi)
	je	.LBB8_8
# BB#7:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB8_8:
	movq	%rbx, %rdi
	callq	markmt
	movb	$1, 33(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	markroot, .Lfunc_end8-markroot
	.cfi_endproc

	.hidden	luaC_barrierf
	.globl	luaC_barrierf
	.p2align	4, 0x90
	.type	luaC_barrierf,@function
luaC_barrierf:                          # @luaC_barrierf
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rdi
	cmpb	$1, 33(%rdi)
	jne	.LBB9_1
# BB#2:
	movq	%rdx, %rsi
	jmp	reallymarkobject        # TAILCALL
.LBB9_1:
	movb	9(%rsi), %al
	andb	$-8, %al
	movb	32(%rdi), %cl
	andb	$3, %cl
	orb	%al, %cl
	movb	%cl, 9(%rsi)
	retq
.Lfunc_end9:
	.size	luaC_barrierf, .Lfunc_end9-luaC_barrierf
	.cfi_endproc

	.p2align	4, 0x90
	.type	reallymarkobject,@function
reallymarkobject:                       # @reallymarkobject
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	9(%rbx), %al
	.p2align	4, 0x90
.LBB10_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	andb	$-4, %al
	movb	%al, 9(%rbx)
	movzbl	8(%rbx), %ecx
	addb	$-5, %cl
	cmpb	$5, %cl
	ja	.LBB10_17
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB10_1 Depth=1
	movzbl	%cl, %ecx
	jmpq	*.LJTI10_0(,%rcx,8)
.LBB10_3:                               #   in Loop: Header=BB10_1 Depth=1
	movq	16(%rbx), %rsi
	orb	$4, %al
	movb	%al, 9(%rbx)
	testq	%rsi, %rsi
	je	.LBB10_6
# BB#4:                                 #   in Loop: Header=BB10_1 Depth=1
	testb	$3, 9(%rsi)
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
.LBB10_6:                               #   in Loop: Header=BB10_1 Depth=1
	movq	24(%rbx), %rbx
	movzbl	9(%rbx), %eax
	testb	$3, %al
	jne	.LBB10_1
	jmp	.LBB10_17
.LBB10_13:
	movq	56(%r14), %rax
	movq	%rax, 48(%rbx)
	jmp	.LBB10_16
.LBB10_12:
	movq	56(%r14), %rax
	movq	%rax, 16(%rbx)
	jmp	.LBB10_16
.LBB10_14:
	movq	56(%r14), %rax
	movq	%rax, 160(%rbx)
	jmp	.LBB10_16
.LBB10_15:
	movq	56(%r14), %rax
	movq	%rax, 104(%rbx)
.LBB10_16:                              # %.loopexit
	movq	%rbx, 56(%r14)
.LBB10_17:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_7:
	movq	16(%rbx), %rax
	cmpl	$4, 8(%rax)
	jl	.LBB10_10
# BB#8:
	movq	(%rax), %rsi
	testb	$3, 9(%rsi)
	je	.LBB10_10
# BB#9:
	movq	%r14, %rdi
	callq	reallymarkobject
	movq	16(%rbx), %rax
.LBB10_10:
	leaq	24(%rbx), %rcx
	cmpq	%rcx, %rax
	jne	.LBB10_17
# BB#11:
	orb	$4, 9(%rbx)
	jmp	.LBB10_17
.Lfunc_end10:
	.size	reallymarkobject, .Lfunc_end10-reallymarkobject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_13
	.quad	.LBB10_12
	.quad	.LBB10_3
	.quad	.LBB10_14
	.quad	.LBB10_15
	.quad	.LBB10_7

	.text
	.hidden	luaC_barrierback
	.globl	luaC_barrierback
	.p2align	4, 0x90
	.type	luaC_barrierback,@function
luaC_barrierback:                       # @luaC_barrierback
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	andb	$-5, 9(%rsi)
	movq	64(%rax), %rcx
	movq	%rcx, 48(%rsi)
	movq	%rsi, 64(%rax)
	retq
.Lfunc_end11:
	.size	luaC_barrierback, .Lfunc_end11-luaC_barrierback
	.cfi_endproc

	.hidden	luaC_link
	.globl	luaC_link
	.p2align	4, 0x90
	.type	luaC_link,@function
luaC_link:                              # @luaC_link
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movq	40(%rax), %rcx
	movq	%rcx, (%rsi)
	movq	%rsi, 40(%rax)
	movb	32(%rax), %al
	andb	$3, %al
	movb	%al, 9(%rsi)
	movb	%dl, 8(%rsi)
	retq
.Lfunc_end12:
	.size	luaC_link, .Lfunc_end12-luaC_link
	.cfi_endproc

	.hidden	luaC_linkupval
	.globl	luaC_linkupval
	.p2align	4, 0x90
	.type	luaC_linkupval,@function
luaC_linkupval:                         # @luaC_linkupval
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, (%rsi)
	movq	%rsi, 40(%rdi)
	movb	9(%rsi), %al
	testb	$7, %al
	jne	.LBB13_6
# BB#1:
	cmpb	$1, 33(%rdi)
	jne	.LBB13_5
# BB#2:
	orb	$4, %al
	movb	%al, 9(%rsi)
	movq	16(%rsi), %rax
	cmpl	$4, 8(%rax)
	jl	.LBB13_6
# BB#3:
	movq	(%rax), %rsi
	testb	$3, 9(%rsi)
	je	.LBB13_6
# BB#4:
	jmp	reallymarkobject        # TAILCALL
.LBB13_5:
	andb	$-8, %al
	movb	32(%rdi), %cl
	andb	$3, %cl
	orb	%al, %cl
	movb	%cl, 9(%rsi)
.LBB13_6:                               # %luaC_barrierf.exit
	retq
.Lfunc_end13:
	.size	luaC_linkupval, .Lfunc_end13-luaC_linkupval
	.cfi_endproc

	.p2align	4, 0x90
	.type	propagatemark,@function
propagatemark:                          # @propagatemark
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 64
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	56(%r14), %r15
	movb	9(%r15), %cl
	movl	%ecx, %eax
	orb	$4, %al
	movb	%al, 9(%r15)
	movb	8(%r15), %dl
	xorl	%eax, %eax
	addb	$-5, %dl
	cmpb	$4, %dl
	ja	.LBB14_147
# BB#1:
	movzbl	%dl, %edx
	jmpq	*.LJTI14_0(,%rdx,8)
.LBB14_2:
	movq	48(%r15), %rax
	movq	%rax, 56(%r14)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB14_3
# BB#4:
	testb	$3, 9(%rax)
	je	.LBB14_6
# BB#5:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	reallymarkobject
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB14_3
.LBB14_6:                               # %.thread.i
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	testb	$8, 10(%rax)
	movl	$0, %r12d
	movl	$0, %r13d
	jne	.LBB14_15
# BB#7:
	movq	320(%r14), %rdx
	movl	$3, %esi
	movq	%rax, %rdi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB14_3
# BB#8:
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	$4, 8(%rax)
	movl	$0, %r12d
	movl	$0, %r13d
	jne	.LBB14_15
# BB#9:
	movq	(%rax), %r12
	addq	$24, %r12
	movl	$107, %esi
	movq	%r12, %rdi
	callq	strchr
	movq	%rax, %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	setne	%r13b
	movb	%r13b, %al
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$118, %esi
	movq	%r12, %rdi
	callq	strchr
	xorl	%r12d, %r12d
	testq	%rax, %rax
	setne	%r12b
	movq	%rbp, %rcx
	orq	%rax, %rcx
	je	.LBB14_11
# BB#10:
	movb	9(%r15), %cl
	andb	$-25, %cl
	movl	%r12d, %edx
	shll	$4, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	leal	(%rdx,%rsi,8), %edx
	movzbl	%cl, %ecx
	orl	%edx, %ecx
	movb	%cl, 9(%r15)
	movq	72(%r14), %rcx
	movq	%rcx, 48(%r15)
	movq	%r15, 72(%r14)
.LBB14_11:
	testq	%rbp, %rbp
	je	.LBB14_14
# BB#12:
	testq	%rax, %rax
	je	.LBB14_14
# BB#13:                                # %.traversetable.exit.thread_crit_edge
	leaq	11(%r15), %rax
	jmp	.LBB14_64
.LBB14_66:
	movq	16(%r15), %rax
	movq	%rax, 56(%r14)
	movq	24(%r15), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_68
# BB#67:
	movq	%r14, %rdi
	callq	reallymarkobject
.LBB14_68:
	cmpb	$0, 10(%r15)
	je	.LBB14_75
# BB#69:                                # %.preheader28.i
	movb	11(%r15), %al
	testb	%al, %al
	je	.LBB14_82
# BB#70:                                # %.lr.ph32.i.preheader
	leaq	48(%r15), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_71:                              # %.lr.ph32.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$4, (%rbx)
	jl	.LBB14_74
# BB#72:                                #   in Loop: Header=BB14_71 Depth=1
	movq	-8(%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_74
# BB#73:                                #   in Loop: Header=BB14_71 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movzbl	11(%r15), %eax
.LBB14_74:                              #   in Loop: Header=BB14_71 Depth=1
	incq	%rbp
	movzbl	%al, %ecx
	addq	$16, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB14_71
	jmp	.LBB14_82
.LBB14_83:
	movq	160(%r15), %rax
	movq	%rax, 56(%r14)
	movq	64(%r14), %rax
	movq	%rax, 160(%r15)
	movq	%r15, 64(%r14)
	andb	$-5, %cl
	movb	%cl, 9(%r15)
	cmpl	$4, 128(%r15)
	jl	.LBB14_86
# BB#84:
	movq	120(%r15), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_86
# BB#85:
	movq	%r14, %rdi
	callq	reallymarkobject
.LBB14_86:
	movq	16(%r15), %rax
	movq	40(%r15), %rcx
	movq	80(%r15), %rdx
	cmpq	%rcx, %rdx
	movq	%rax, %rbp
	ja	.LBB14_89
# BB#87:                                # %.lr.ph41.i.preheader
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB14_88:                              # %.lr.ph41.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdx), %rsi
	cmpq	%rsi, %rbp
	cmovbq	%rsi, %rbp
	addq	$40, %rdx
	cmpq	%rcx, %rdx
	jbe	.LBB14_88
.LBB14_89:                              # %._crit_edge42.i
	movq	64(%r15), %rbx
	cmpq	%rax, %rbx
	jb	.LBB14_91
	jmp	.LBB14_96
	.p2align	4, 0x90
.LBB14_94:                              #   in Loop: Header=BB14_91 Depth=1
	addq	$16, %rbx
	cmpq	%rax, %rbx
	jae	.LBB14_96
.LBB14_91:                              # %.lr.ph37.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$4, 8(%rbx)
	jl	.LBB14_94
# BB#92:                                #   in Loop: Header=BB14_91 Depth=1
	movq	(%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_94
# BB#93:                                #   in Loop: Header=BB14_91 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movq	16(%r15), %rax
	jmp	.LBB14_94
	.p2align	4, 0x90
.LBB14_95:                              # %.lr.ph.i64
                                        #   in Loop: Header=BB14_96 Depth=1
	movl	$0, 8(%rbx)
	addq	$16, %rbx
.LBB14_96:                              # %.lr.ph.i64
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	jbe	.LBB14_95
# BB#97:                                # %._crit_edge.i
	movl	92(%r15), %esi
	cmpl	$20001, %esi            # imm = 0x4E21
	jl	.LBB14_99
# BB#98:                                # %._crit_edge.i.traversestack.exit_crit_edge
	leaq	88(%r15), %rbx
	jmp	.LBB14_105
.LBB14_106:
	movq	104(%r15), %rax
	movq	%rax, 56(%r14)
	movq	64(%r15), %rax
	testq	%rax, %rax
	je	.LBB14_108
# BB#107:
	andb	$-4, 9(%rax)
.LBB14_108:                             # %.preheader38.i
	movl	76(%r15), %eax
	testl	%eax, %eax
	jle	.LBB14_114
# BB#109:                               # %.lr.ph45.i
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB14_110:                             # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	cmpl	$4, (%rcx,%rbp)
	jl	.LBB14_113
# BB#111:                               #   in Loop: Header=BB14_110 Depth=1
	movq	-8(%rcx,%rbp), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_113
# BB#112:                               #   in Loop: Header=BB14_110 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movl	76(%r15), %eax
.LBB14_113:                             #   in Loop: Header=BB14_110 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$16, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB14_110
.LBB14_114:                             # %.preheader37.i
	movslq	72(%r15), %rax
	testq	%rax, %rax
	jle	.LBB14_127
# BB#115:                               # %.lr.ph43.i
	movq	56(%r15), %rcx
	testb	$1, %al
	jne	.LBB14_117
# BB#116:
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB14_121
	jmp	.LBB14_127
.LBB14_3:
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB14_15:                              # %.thread73.i
	movslq	56(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB14_21
# BB#16:                                # %.lr.ph.i
	movq	%rbx, %rbp
	shlq	$4, %rbp
	addq	$-8, %rbp
	.p2align	4, 0x90
.LBB14_17:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	cmpl	$4, (%rax,%rbp)
	jl	.LBB14_20
# BB#18:                                #   in Loop: Header=BB14_17 Depth=1
	movq	-8(%rax,%rbp), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_20
# BB#19:                                #   in Loop: Header=BB14_17 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
.LBB14_20:                              # %.backedge.i
                                        #   in Loop: Header=BB14_17 Depth=1
	addq	$-16, %rbp
	decl	%ebx
	jne	.LBB14_17
.LBB14_21:
	xorl	%edx, %edx
.LBB14_22:                              # %.loopexit.i
	movb	11(%r15), %cl
	movl	$1, %ebp
	shll	%cl, %ebp
	leal	-1(%rbp), %esi
	movslq	%esi, %rax
	testb	%r13b, %r13b
	je	.LBB14_23
# BB#34:                                # %.split.us.i.preheader
	testb	%dl, %dl
	je	.LBB14_35
# BB#40:                                # %.split.us.i.preheader79
	movq	32(%r15), %rdx
	testb	%cl, %cl
	jne	.LBB14_45
# BB#41:                                # %.split.us.i.prol
	leaq	(%rax,%rax,4), %rcx
	cmpl	$0, 8(%rdx,%rcx,8)
	jne	.LBB14_44
# BB#42:
	cmpl	$4, 24(%rdx,%rcx,8)
	jl	.LBB14_44
# BB#43:
	leaq	24(%rdx,%rcx,8), %rcx
	movl	$11, (%rcx)
.LBB14_44:                              # %removeentry.exit.backedge.us.i.prol
	decq	%rax
.LBB14_45:                              # %.split.us.i.prol.loopexit
	testl	%esi, %esi
	je	.LBB14_63
# BB#46:                                # %.split.us.i.preheader79.new
	leaq	(%rax,%rax,4), %rcx
	leaq	24(%rdx,%rcx,8), %rcx
	notl	%eax
	.p2align	4, 0x90
.LBB14_47:                              # %.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, -16(%rcx)
	jne	.LBB14_50
# BB#48:                                #   in Loop: Header=BB14_47 Depth=1
	cmpl	$4, (%rcx)
	jl	.LBB14_50
# BB#49:                                #   in Loop: Header=BB14_47 Depth=1
	movl	$11, (%rcx)
	.p2align	4, 0x90
.LBB14_50:                              # %removeentry.exit.backedge.us.i
                                        #   in Loop: Header=BB14_47 Depth=1
	cmpl	$0, -56(%rcx)
	jne	.LBB14_53
# BB#51:                                #   in Loop: Header=BB14_47 Depth=1
	cmpl	$4, -40(%rcx)
	jl	.LBB14_53
# BB#52:                                #   in Loop: Header=BB14_47 Depth=1
	movl	$11, -40(%rcx)
	.p2align	4, 0x90
.LBB14_53:                              # %removeentry.exit.backedge.us.i.1
                                        #   in Loop: Header=BB14_47 Depth=1
	addq	$-80, %rcx
	addl	$2, %eax
	jne	.LBB14_47
	jmp	.LBB14_63
.LBB14_99:
	subq	64(%r15), %rbp
	cmpl	$17, %esi
	jl	.LBB14_102
# BB#100:
	movq	40(%r15), %rax
	subq	80(%r15), %rax
	shrq	$3, %rax
	imull	$858993460, %eax, %eax  # imm = 0x33333334
	cmpl	%esi, %eax
	jge	.LBB14_102
# BB#101:
	shrl	%esi
	movq	%r15, %rdi
	callq	luaD_reallocCI
.LBB14_102:
	shrq	$2, %rbp
	andl	$-4, %ebp
	leaq	88(%r15), %rbx
	movl	88(%r15), %esi
	cmpl	%esi, %ebp
	jge	.LBB14_105
# BB#103:
	cmpl	$91, %esi
	jl	.LBB14_105
# BB#104:
	shrl	%esi
	movq	%r15, %rdi
	callq	luaD_reallocstack
.LBB14_105:                             # %traversestack.exit
	movslq	(%rbx), %rax
	shlq	$4, %rax
	movslq	92(%r15), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	184(%rax,%rcx,8), %rax
	jmp	.LBB14_147
.LBB14_23:                              # %.split.i.preheader
	leaq	(%rax,%rax,4), %rax
	leaq	24(,%rax,8), %rbx
	testb	%dl, %dl
	je	.LBB14_24
	.p2align	4, 0x90
.LBB14_27:                              # %.split.i.us
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rax
	cmpl	$0, -16(%rax,%rbx)
	movl	(%rax,%rbx), %ecx
	je	.LBB14_31
# BB#28:                                #   in Loop: Header=BB14_27 Depth=1
	cmpl	$4, %ecx
	jl	.LBB14_33
# BB#29:                                #   in Loop: Header=BB14_27 Depth=1
	movq	-8(%rax,%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_33
# BB#30:                                #   in Loop: Header=BB14_27 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	jmp	.LBB14_33
	.p2align	4, 0x90
.LBB14_31:                              #   in Loop: Header=BB14_27 Depth=1
	cmpl	$4, %ecx
	jl	.LBB14_33
# BB#32:                                #   in Loop: Header=BB14_27 Depth=1
	movl	$11, (%rax,%rbx)
	.p2align	4, 0x90
.LBB14_33:                              # %removeentry.exit.backedge.i.us
                                        #   in Loop: Header=BB14_27 Depth=1
	addq	$-40, %rbx
	decl	%ebp
	jne	.LBB14_27
	jmp	.LBB14_63
	.p2align	4, 0x90
.LBB14_24:                              # %.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %r13
	movl	-16(%r13,%rbx), %eax
	movl	(%r13,%rbx), %ecx
	testl	%eax, %eax
	je	.LBB14_25
# BB#57:                                #   in Loop: Header=BB14_24 Depth=1
	cmpl	$4, %ecx
	jl	.LBB14_60
# BB#58:                                #   in Loop: Header=BB14_24 Depth=1
	movq	-8(%r13,%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_60
# BB#59:                                #   in Loop: Header=BB14_24 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movl	-16(%r13,%rbx), %eax
.LBB14_60:                              #   in Loop: Header=BB14_24 Depth=1
	cmpl	$4, %eax
	jl	.LBB14_62
# BB#61:                                #   in Loop: Header=BB14_24 Depth=1
	movq	-24(%r13,%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_62
# BB#148:                               #   in Loop: Header=BB14_24 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	jmp	.LBB14_62
	.p2align	4, 0x90
.LBB14_25:                              #   in Loop: Header=BB14_24 Depth=1
	cmpl	$4, %ecx
	jl	.LBB14_62
# BB#26:                                #   in Loop: Header=BB14_24 Depth=1
	movl	$11, (%r13,%rbx)
	.p2align	4, 0x90
.LBB14_62:                              # %removeentry.exit.backedge.i
                                        #   in Loop: Header=BB14_24 Depth=1
	addq	$-40, %rbx
	decl	%ebp
	jne	.LBB14_24
	jmp	.LBB14_63
.LBB14_75:
	movq	32(%r15), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_77
# BB#76:
	movq	%r14, %rdi
	callq	reallymarkobject
.LBB14_77:                              # %.preheader.i
	movb	11(%r15), %al
	testb	%al, %al
	je	.LBB14_82
# BB#78:                                # %.lr.ph.i58
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_79:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r15,%rbx,8), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_81
# BB#80:                                #   in Loop: Header=BB14_79 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movzbl	11(%r15), %eax
.LBB14_81:                              #   in Loop: Header=BB14_79 Depth=1
	incq	%rbx
	movzbl	%al, %ecx
	cmpq	%rcx, %rbx
	jl	.LBB14_79
.LBB14_82:                              # %traverseclosure.exit
	xorl	%eax, %eax
	cmpb	$0, 10(%r15)
	setne	%al
	movzbl	11(%r15), %ecx
	decl	%ecx
	leal	8(,%rax,8), %edx
	imull	%ecx, %edx
	leal	48(%rdx,%rax,8), %eax
	cltq
	jmp	.LBB14_147
.LBB14_117:
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB14_119
# BB#118:
	andb	$-4, 9(%rdx)
.LBB14_119:                             # %.prol.loopexit114
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB14_127
.LBB14_121:                             # %.lr.ph43.i.new
	subq	%rdx, %rax
	leaq	8(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB14_122:                             # =>This Inner Loop Header: Depth=1
	movq	-8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB14_124
# BB#123:                               #   in Loop: Header=BB14_122 Depth=1
	andb	$-4, 9(%rdx)
.LBB14_124:                             #   in Loop: Header=BB14_122 Depth=1
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB14_126
# BB#125:                               #   in Loop: Header=BB14_122 Depth=1
	andb	$-4, 9(%rdx)
.LBB14_126:                             #   in Loop: Header=BB14_122 Depth=1
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB14_122
.LBB14_127:                             # %.preheader36.i
	movl	88(%r15), %eax
	testl	%eax, %eax
	jle	.LBB14_133
# BB#128:                               # %.lr.ph41.i66
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_129:                             # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rcx
	movq	(%rcx,%rbx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB14_132
# BB#130:                               #   in Loop: Header=BB14_129 Depth=1
	testb	$3, 9(%rsi)
	je	.LBB14_132
# BB#131:                               #   in Loop: Header=BB14_129 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	movl	88(%r15), %eax
.LBB14_132:                             #   in Loop: Header=BB14_129 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB14_129
.LBB14_133:                             # %.preheader.i67
	movslq	92(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB14_146
# BB#134:                               # %.lr.ph.i68
	movq	48(%r15), %rdx
	testb	$1, %cl
	jne	.LBB14_136
# BB#135:
	xorl	%edi, %edi
	cmpl	$1, %ecx
	jne	.LBB14_140
	jmp	.LBB14_146
.LBB14_136:
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB14_138
# BB#137:
	andb	$-4, 9(%rsi)
.LBB14_138:                             # %.prol.loopexit
	movl	$1, %edi
	cmpl	$1, %ecx
	je	.LBB14_146
.LBB14_140:                             # %.lr.ph.i68.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	shlq	$4, %rdi
	leaq	16(%rdx,%rdi), %rdx
	.p2align	4, 0x90
.LBB14_141:                             # =>This Inner Loop Header: Depth=1
	movq	-16(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_143
# BB#142:                               #   in Loop: Header=BB14_141 Depth=1
	andb	$-4, 9(%rdi)
.LBB14_143:                             #   in Loop: Header=BB14_141 Depth=1
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_145
# BB#144:                               #   in Loop: Header=BB14_141 Depth=1
	andb	$-4, 9(%rdi)
.LBB14_145:                             #   in Loop: Header=BB14_141 Depth=1
	addq	$32, %rdx
	addq	$-2, %rsi
	jne	.LBB14_141
.LBB14_146:                             # %traverseproto.exit
	movslq	80(%r15), %rdx
	cltq
	movslq	76(%r15), %rsi
	movslq	84(%r15), %rdi
	movslq	72(%r15), %rbp
	addq	%rcx, %rsi
	shlq	$4, %rsi
	addq	%rax, %rbp
	addq	%rdx, %rdi
	leaq	(%rsi,%rdi,4), %rax
	leaq	120(%rax,%rbp,8), %rax
	jmp	.LBB14_147
.LBB14_35:                              # %.split.us.i.us.preheader
	leaq	(%rax,%rax,4), %rax
	leaq	24(,%rax,8), %rbx
	.p2align	4, 0x90
.LBB14_36:                              # %.split.us.i.us
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rax
	movl	-16(%rax,%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB14_54
# BB#37:                                #   in Loop: Header=BB14_36 Depth=1
	cmpl	$4, %ecx
	jl	.LBB14_56
# BB#38:                                #   in Loop: Header=BB14_36 Depth=1
	movq	-24(%rax,%rbx), %rsi
	testb	$3, 9(%rsi)
	je	.LBB14_56
# BB#39:                                #   in Loop: Header=BB14_36 Depth=1
	movq	%r14, %rdi
	callq	reallymarkobject
	jmp	.LBB14_56
	.p2align	4, 0x90
.LBB14_54:                              #   in Loop: Header=BB14_36 Depth=1
	cmpl	$4, (%rax,%rbx)
	jl	.LBB14_56
# BB#55:                                #   in Loop: Header=BB14_36 Depth=1
	movl	$11, (%rax,%rbx)
	.p2align	4, 0x90
.LBB14_56:                              # %removeentry.exit.backedge.us.i.us
                                        #   in Loop: Header=BB14_36 Depth=1
	addq	$-40, %rbx
	decl	%ebp
	jne	.LBB14_36
.LBB14_63:                              # %traversetable.exit
	leaq	11(%r15), %rax
	orl	(%rsp), %r12d           # 4-byte Folded Reload
	je	.LBB14_65
.LBB14_64:                              # %traversetable.exit.thread
	andb	$-5, 9(%r15)
.LBB14_65:
	movslq	56(%r15), %rdx
	shlq	$4, %rdx
	movb	(%rax), %cl
	movl	$1, %eax
	shll	%cl, %eax
	cltq
	leaq	(%rax,%rax,4), %rax
	leaq	64(%rdx,%rax,8), %rax
.LBB14_147:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_14:
	movb	$1, %dl
	testq	%rax, %rax
	jne	.LBB14_22
	jmp	.LBB14_15
.Lfunc_end14:
	.size	propagatemark, .Lfunc_end14-propagatemark
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI14_0:
	.quad	.LBB14_2
	.quad	.LBB14_66
	.quad	.LBB14_147
	.quad	.LBB14_83
	.quad	.LBB14_106

	.text
	.p2align	4, 0x90
	.type	markmt,@function
markmt:                                 # @markmt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 16
.Lcfi92:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	224(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_3
# BB#1:
	testb	$3, 9(%rsi)
	je	.LBB15_3
# BB#2:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_3:
	movq	232(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_6
# BB#4:
	testb	$3, 9(%rsi)
	je	.LBB15_6
# BB#5:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_6:
	movq	240(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_9
# BB#7:
	testb	$3, 9(%rsi)
	je	.LBB15_9
# BB#8:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_9:
	movq	248(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_12
# BB#10:
	testb	$3, 9(%rsi)
	je	.LBB15_12
# BB#11:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_12:
	movq	256(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_15
# BB#13:
	testb	$3, 9(%rsi)
	je	.LBB15_15
# BB#14:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_15:
	movq	264(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_18
# BB#16:
	testb	$3, 9(%rsi)
	je	.LBB15_18
# BB#17:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_18:
	movq	272(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_21
# BB#19:
	testb	$3, 9(%rsi)
	je	.LBB15_21
# BB#20:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_21:
	movq	280(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_24
# BB#22:
	testb	$3, 9(%rsi)
	je	.LBB15_24
# BB#23:
	movq	%rbx, %rdi
	callq	reallymarkobject
.LBB15_24:
	movq	288(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB15_26
# BB#25:
	testb	$3, 9(%rsi)
	je	.LBB15_26
# BB#27:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	reallymarkobject        # TAILCALL
.LBB15_26:
	popq	%rbx
	retq
.Lfunc_end15:
	.size	markmt, .Lfunc_end15-markmt
	.cfi_endproc

	.hidden	luaT_gettm
	.hidden	luaD_call
	.hidden	luaF_freeproto
	.hidden	luaF_freeclosure
	.hidden	luaF_freeupval
	.hidden	luaH_free
	.hidden	luaE_freethread
	.hidden	luaM_realloc_
	.hidden	luaD_reallocCI
	.hidden	luaD_reallocstack
	.hidden	luaS_resize
	.hidden	luaM_toobig

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
