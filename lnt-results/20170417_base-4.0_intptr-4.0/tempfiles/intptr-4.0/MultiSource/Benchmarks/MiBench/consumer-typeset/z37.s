	.text
	.file	"z37.bc"
	.globl	FontInit
	.p2align	4, 0x90
	.type	FontInit,@function
FontInit:                               # @FontInit
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$1, font_curr_page(%rip)
	movl	$0, font_count(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_3:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, font_root(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_6
.LBB0_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_6:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, font_used(%rip)
	movl	$0, font_seqnum(%rip)
	movl	$9600, %edi             # imm = 0x2580
	callq	malloc
	movq	%rax, finfo(%rip)
	movl	$100, finfo_size(%rip)
	movq	StartSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.1, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi6:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi9:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	%rbx, FontDefSym(%rip)
	movq	no_fpos(%rip), %r14
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%r14, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%r14, %rdx
	pushq	%rax
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi14:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%rbx)
	orb	$64, 126(%rax)
	movq	%rax, fd_tag(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.3, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%r14)
	orb	$64, 126(%rax)
	movq	%rax, fd_family(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.4, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%r14)
	orb	$64, 126(%rax)
	movq	%rax, fd_face(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.5, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%r14)
	orb	$64, 126(%rax)
	movq	%rax, fd_name(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.6, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%r14)
	orb	$64, 126(%rax)
	movq	%rax, fd_metrics(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.7, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	movq	%rax, fd_extra_metrics(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.8, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	incw	122(%r14)
	orb	$64, 126(%rax)
	movq	%rax, fd_mapping(%rip)
	movq	FontDefSym(%rip), %r14
	movq	no_fpos(%rip), %rbx
	movl	$11, %edi
	movl	$.L.str.14, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.9, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -32
	orb	$1, 43(%rax)
	movq	%rax, fd_recode(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	FontInit, .Lfunc_end0-FontInit
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16,1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1056964608              # float 0.5
	.text
	.globl	FontChange
	.p2align	4, 0x90
	.type	FontChange,@function
FontChange:                             # @FontChange
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$2328, %rsp             # imm = 0x918
.Lcfi56:
	.cfi_def_cfa_offset 2384
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$4095, %eax             # imm = 0xFFF
	andl	12(%rbx), %eax
	cmpl	font_count(%rip), %eax
	jbe	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	leaq	32(%r15), %r14
	movb	32(%r15), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB1_23
# BB#3:
	cmpb	$5, %al
	je	.LBB1_476
# BB#4:
	cmpb	$17, %al
	jne	.LBB1_47
# BB#5:                                 # %.preheader618
	movq	8(%r15), %r13
	cmpq	%r15, %r13
	je	.LBB1_476
# BB#6:                                 # %.preheader616.lr.ph.lr.ph
	xorl	%r12d, %r12d
	movq	%r14, 80(%rsp)          # 8-byte Spill
.LBB1_7:                                # %.preheader616
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	cmpq	$12, %rax
	ja	.LBB1_49
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_10:                               #   in Loop: Header=BB1_7 Depth=1
	leaq	64(%rbp), %r14
	movl	$.L.str.12, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_15
# BB#11:                                #   in Loop: Header=BB1_7 Depth=1
	movl	$.L.str.13, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_16
# BB#12:                                #   in Loop: Header=BB1_7 Depth=1
	cmpb	$0, (%r14)
	movq	80(%rsp), %r14          # 8-byte Reload
	je	.LBB1_18
# BB#13:                                #   in Loop: Header=BB1_7 Depth=1
	cmpq	$3, %r12
	jge	.LBB1_479
# BB#14:                                # %.outer
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	%rbp, 224(%rsp,%r12,8)
	incq	%r12
	jmp	.LBB1_18
.LBB1_15:                               #   in Loop: Header=BB1_7 Depth=1
	orb	$8, (%rbx)
	jmp	.LBB1_17
.LBB1_16:                               #   in Loop: Header=BB1_7 Depth=1
	andb	$-9, (%rbx)
.LBB1_17:                               # %.backedge
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	80(%rsp), %r14          # 8-byte Reload
.LBB1_18:                               # %.backedge
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	8(%r13), %r13
	cmpq	%r15, %r13
	jne	.LBB1_7
# BB#19:                                # %.outer._crit_edge
	testl	%r12d, %r12d
	je	.LBB1_476
# BB#20:
	leal	-1(%r12), %eax
	cmpl	$3, %eax
	jb	.LBB1_22
# BB#21:                                # %.preheader615
	movq	no_fpos(%rip), %r8
	xorl	%r15d, %r15d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
	testl	%r12d, %r12d
	jle	.LBB1_212
.LBB1_22:
	movq	%rbx, %r9
	jmp	.LBB1_27
.LBB1_23:
	leaq	64(%r15), %rbp
	movl	$.L.str.12, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_48
# BB#24:
	movl	$.L.str.13, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_51
# BB#25:
	cmpb	$0, (%rbp)
	je	.LBB1_476
# BB#26:                                # %.thread587
	movq	%rbx, %r9
	movq	%r15, 224(%rsp)
	movl	$1, %r12d
                                        # implicit-def: %RBP
.LBB1_27:                               # %.lr.ph704.preheader
	xorl	%edx, %edx
	movabsq	$8589934592, %r8        # imm = 0x200000000
	movabsq	$17179869184, %r10      # imm = 0x400000000
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	jmp	.LBB1_29
.LBB1_28:                               #   in Loop: Header=BB1_29 Depth=1
	movq	%rsi, %r15
	jmp	.LBB1_41
	.p2align	4, 0x90
.LBB1_29:                               # %.lr.ph704
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_35 Depth 2
                                        #     Child Loop BB1_38 Depth 2
	movslq	%edx, %rdi
	movq	224(%rsp,%rdi,8), %r15
	movb	64(%r15), %bl
	cmpb	$43, %bl
	je	.LBB1_32
# BB#30:                                # %.lr.ph704
                                        #   in Loop: Header=BB1_29 Depth=1
	cmpb	$45, %bl
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_29 Depth=1
	addb	$-48, %bl
	cmpb	$9, %bl
	ja	.LBB1_28
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_29 Depth=1
	leal	1(%rdx), %edi
	cmpl	%r12d, %edi
	jge	.LBB1_40
# BB#33:                                # %.lr.ph698.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	movslq	%edi, %rsi
	leal	3(%r12), %ebx
	subl	%edx, %ebx
	leal	-2(%r12), %edi
	subl	%edx, %edi
	andl	$3, %ebx
	je	.LBB1_36
# BB#34:                                # %.lr.ph698.prol.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB1_35:                               # %.lr.ph698.prol
                                        #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	224(%rsp,%rsi,8), %rax
	movslq	%edx, %rdx
	movq	%rax, 224(%rsp,%rdx,8)
	incq	%rsi
	incl	%edx
	incl	%ebx
	jne	.LBB1_35
.LBB1_36:                               # %.lr.ph698.prol.loopexit
                                        #   in Loop: Header=BB1_29 Depth=1
	cmpl	$3, %edi
	movl	%r12d, %edi
	jb	.LBB1_40
# BB#37:                                # %.lr.ph698.preheader.new
                                        #   in Loop: Header=BB1_29 Depth=1
	movl	%r12d, %edi
	movq	%rsi, %rbx
	shlq	$32, %rbx
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph698
                                        #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	224(%rsp,%rsi,8), %rax
	movslq	%edx, %rdx
	movq	%rax, 224(%rsp,%rdx,8)
	movq	232(%rsp,%rsi,8), %rax
	movq	%rbx, %rdx
	sarq	$29, %rdx
	movq	%rax, 224(%rsp,%rdx)
	movq	240(%rsp,%rsi,8), %rax
	leaq	(%rbx,%rcx), %rdx
	sarq	$29, %rdx
	movq	%rax, 224(%rsp,%rdx)
	movq	248(%rsp,%rsi,8), %rax
	leaq	(%rbx,%r8), %rdx
	sarq	$29, %rdx
	movq	%rax, 224(%rsp,%rdx)
	leal	3(%rsi), %edx
	leaq	4(%rsi), %rsi
	addq	%r10, %rbx
	cmpl	%esi, %edi
	jne	.LBB1_38
# BB#39:                                #   in Loop: Header=BB1_29 Depth=1
	movl	%r12d, %edi
.LBB1_40:                               # %._crit_edge699
                                        #   in Loop: Header=BB1_29 Depth=1
	decl	%r12d
	movq	%r15, %r13
	movl	%edi, %edx
.LBB1_41:                               #   in Loop: Header=BB1_29 Depth=1
	incl	%edx
	cmpl	%r12d, %edx
	movq	%r15, %rsi
	jl	.LBB1_29
# BB#42:
	movq	%r9, %rbx
.LBB1_43:                               # %._crit_edge705
	testl	%r12d, %r12d
	movq	%r14, 80(%rsp)          # 8-byte Spill
	je	.LBB1_52
# BB#44:                                # %._crit_edge705
	cmpl	$2, %r12d
	je	.LBB1_54
# BB#45:                                # %._crit_edge705
	cmpl	$1, %r12d
	jne	.LBB1_89
# BB#46:
	movq	224(%rsp), %rax
	jmp	.LBB1_53
.LBB1_47:
	movl	$37, %edi
	movl	$42, %esi
	jmp	.LBB1_50
.LBB1_48:
	orb	$8, (%rbx)
	jmp	.LBB1_476
.LBB1_49:
	movl	$37, %edi
	movl	$41, %esi
.LBB1_50:                               # %.thread
	movl	$.L.str.15, %edx
	movl	$2, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
.LBB1_475:                              # %.thread
	callq	Error
.LBB1_476:                              # %.thread
	addq	$2328, %rsp             # imm = 0x918
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_51:
	andb	$-9, (%rbx)
	jmp	.LBB1_476
.LBB1_52:
	xorl	%eax, %eax
.LBB1_53:
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.LBB1_55
.LBB1_54:
	movq	224(%rsp), %r14
	movq	232(%rsp), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	%r14, %r12
.LBB1_55:
	movzwl	12(%rbx), %eax
	testw	$4095, %ax              # imm = 0xFFF
	jne	.LBB1_60
# BB#56:
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	je	.LBB1_59
# BB#57:
	testq	%r13, %r13
	je	.LBB1_59
# BB#58:
	testq	%r12, %r12
	jne	.LBB1_60
.LBB1_59:
	movl	$37, %edi
	movl	$44, %esi
	movl	$.L.str.18, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_60:
	testq	%r12, %r12
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%r15, 320(%rsp)         # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	je	.LBB1_68
# BB#61:
	movq	font_root(%rip), %r12
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB1_67
# BB#62:                                # %.preheader612.preheader
	addq	$64, %r14
	.p2align	4, 0x90
.LBB1_63:                               # %.preheader612
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_64 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_64:                               #   Parent Loop BB1_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_64
# BB#65:                                #   in Loop: Header=BB1_63 Depth=1
	leaq	64(%rbp), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_63 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB1_63
	jmp	.LBB1_87
.LBB1_67:                               # %._crit_edge688
	cmpq	%r12, %rbx
	jne	.LBB1_79
	jmp	.LBB1_87
.LBB1_68:
	movq	finfo(%rip), %rax
	movl	12(%rbx), %ecx
	movl	%ecx, %edx
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	40(%rax,%rdx), %rdx
	cmpq	%rdx, 24(%rdx)
	jne	.LBB1_70
# BB#69:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	finfo(%rip), %rax
	movl	12(%rbx), %ecx
.LBB1_70:
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movq	24(%rax), %rbx
	.p2align	4, 0x90
.LBB1_71:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_71
# BB#72:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_74
# BB#73:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_74:                               # %.loopexit614
	movq	24(%rbx), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB1_76
# BB#75:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB1_76:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_76
# BB#77:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_79
# BB#78:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_79:                               # %.loopexit613
	testq	%rbp, %rbp
	je	.LBB1_87
# BB#80:
	movq	136(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_107
# BB#81:                                # %.preheader611
	movq	%r13, %r12
	leaq	64(%rax), %r13
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_82:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_84 Depth 2
	movq	8(%rbx), %rbx
	movb	$1, %r15b
	cmpq	%rbp, %rbx
	je	.LBB1_113
# BB#83:                                # %.preheader610.preheader
                                        #   in Loop: Header=BB1_82 Depth=1
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB1_84:                               # %.preheader610
                                        #   Parent Loop BB1_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_84
# BB#85:                                #   in Loop: Header=BB1_82 Depth=1
	leaq	64(%r14), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_82
# BB#86:
	testq	%r14, %r14
	movq	%r12, %r13
	movq	96(%rsp), %r12          # 8-byte Reload
	jne	.LBB1_392
	jmp	.LBB1_92
.LBB1_87:                               # %.thread595
	movq	96(%rsp), %r12          # 8-byte Reload
	testq	%r12, %r12
	je	.LBB1_90
# BB#88:
	xorl	%r15d, %r15d
	jmp	.LBB1_91
.LBB1_89:
	movl	$37, %edi
	movl	$43, %esi
	jmp	.LBB1_50
.LBB1_90:
	movq	no_fpos(%rip), %r8
	xorl	%r15d, %r15d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_91:                               # %.thread595.thread
	xorl	%ebp, %ebp
.LBB1_92:                               # %.thread595.thread
	movq	136(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB1_94
# BB#93:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_94:
	movq	%r13, 328(%rsp)         # 8-byte Spill
	testb	%r15b, %r15b
	cmovneq	%rbp, %r12
	addq	$64, %r12
	leaq	64(%rbx), %r15
	movq	FontDefSym(%rip), %rax
	movq	88(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_96
# BB#95:
	movq	no_fpos(%rip), %r8
	movq	%r15, (%rsp)
	movl	$37, %edi
	movl	$10, %esi
	movl	$.L.str.73, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r9
	callq	Error
.LBB1_96:
	xorl	%r14d, %r14d
	leaq	464(%rsp), %rdi
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	sprintf
	movq	24(%rbp), %rax
	movq	24(%rax), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_391
# BB#97:
	movq	%r15, 272(%rsp)         # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	leaq	336(%rsp), %r15
	xorl	%r14d, %r14d
	leaq	2224(%rsp), %r13
	leaq	166(%rsp), %r12
	.p2align	4, 0x90
.LBB1_98:                               # %.preheader1011.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_99 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_99:                               #   Parent Loop BB1_98 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_99
# BB#100:                               #   in Loop: Header=BB1_98 Depth=1
	movq	FontDefSym(%rip), %rdx
	leaq	344(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	308(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	%r15, (%rsp)
	movl	$0, %esi
	leaq	464(%rsp), %rcx
	movq	%r13, %r8
	movq	%r12, %r9
	callq	DbRetrieve
	testl	%eax, %eax
	jne	.LBB1_104
# BB#101:                               #   in Loop: Header=BB1_98 Depth=1
	movq	24(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB1_98
# BB#102:
	movq	96(%rsp), %r12          # 8-byte Reload
.LBB1_103:                              # %FontRead.exit
	movq	272(%rsp), %r15         # 8-byte Reload
	jmp	.LBB1_391
.LBB1_104:
	xorl	%edi, %edi
	callq	SwitchScope
	movq	336(%rsp), %rsi
	movl	308(%rsp), %edx
	movzwl	166(%rsp), %edi
	callq	ReadFromFile
	movq	%rax, %r14
	xorl	%edi, %edi
	callq	UnSwitchScope
	testq	%r14, %r14
	je	.LBB1_480
# BB#105:                               # %.preheader1008.i
	movq	8(%r14), %r12
	cmpq	%r14, %r12
	movq	272(%rsp), %r15         # 8-byte Reload
	movq	%r14, 40(%rsp)          # 8-byte Spill
	je	.LBB1_114
# BB#106:                               # %.preheader1006.lr.ph.i
	leaq	32(%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$0, %ebp
	movl	$0, %r13d
	movl	$0, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jmp	.LBB1_119
.LBB1_107:
	movq	finfo(%rip), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movl	12(%rcx), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movq	24(%rax), %r14
	.p2align	4, 0x90
.LBB1_108:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB1_108
# BB#109:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_111
# BB#110:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_111:                              # %.loopexit609
	cmpq	%r14, 24(%r14)
	jne	.LBB1_392
# BB#112:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_392
.LBB1_113:
	movq	%r12, %r13
	movq	96(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_92
.LBB1_114:
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB1_163
.LBB1_115:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
.LBB1_116:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_116
# BB#117:                               #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jb	.LBB1_162
# BB#118:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%rax, %r14
	addq	$32, %r14
	movq	fd_recode(%rip), %rdi
	callq	SymName
	movq	%rax, %r9
	movl	$37, %edi
	movl	$18, %esi
	movl	$.L.str.83, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	40(%rsp), %r14          # 8-byte Reload
	callq	Error
	jmp	.LBB1_162
	.p2align	4, 0x90
.LBB1_119:                              # %.preheader1006.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_120 Depth 2
                                        #     Child Loop BB1_116 Depth 2
                                        #     Child Loop BB1_158 Depth 2
                                        #     Child Loop BB1_154 Depth 2
                                        #     Child Loop BB1_149 Depth 2
                                        #     Child Loop BB1_144 Depth 2
                                        #     Child Loop BB1_138 Depth 2
                                        #     Child Loop BB1_133 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB1_120:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_120
# BB#121:                               #   in Loop: Header=BB1_119 Depth=1
	cmpb	$10, %al
	je	.LBB1_123
# BB#122:                               #   in Loop: Header=BB1_119 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.76, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_123:                              # %.loopexit1007.i
                                        #   in Loop: Header=BB1_119 Depth=1
	movq	80(%rbx), %rax
	cmpq	fd_tag(%rip), %rax
	je	.LBB1_162
# BB#124:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_family(%rip), %rax
	je	.LBB1_132
# BB#125:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_face(%rip), %rax
	je	.LBB1_137
# BB#126:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_name(%rip), %rax
	je	.LBB1_143
# BB#127:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_metrics(%rip), %rax
	je	.LBB1_148
# BB#128:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_extra_metrics(%rip), %rax
	je	.LBB1_153
# BB#129:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_mapping(%rip), %rax
	je	.LBB1_157
# BB#130:                               #   in Loop: Header=BB1_119 Depth=1
	cmpq	fd_recode(%rip), %rax
	je	.LBB1_115
# BB#131:                               #   in Loop: Header=BB1_119 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.84, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_162
.LBB1_132:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rbp
	.p2align	4, 0x90
.LBB1_133:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_133
# BB#134:                               #   in Loop: Header=BB1_119 Depth=1
	addb	$-11, %al
	leaq	64(%rbp), %r14
	cmpb	$2, %al
	jae	.LBB1_136
# BB#135:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%r14, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_147
.LBB1_136:                              #   in Loop: Header=BB1_119 Depth=1
	leaq	464(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	$.L.str.2, (%rsp)
	movl	$37, %edi
	movl	$12, %esi
	movl	$.L.str.77, %edx
	jmp	.LBB1_142
.LBB1_137:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %r13
	.p2align	4, 0x90
.LBB1_138:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %r13
	movq	(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB1_138
# BB#139:                               #   in Loop: Header=BB1_119 Depth=1
	addb	$-11, %al
	leaq	64(%r13), %r14
	cmpb	$2, %al
	jae	.LBB1_141
# BB#140:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_152
.LBB1_141:                              #   in Loop: Header=BB1_119 Depth=1
	leaq	464(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	$.L.str.2, (%rsp)
	movl	$37, %edi
	movl	$13, %esi
	movl	$.L.str.78, %edx
.LBB1_142:                              # %.backedge1009.i
                                        #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	%r14, %r9
	callq	Error
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_162
.LBB1_143:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_144:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_144
# BB#145:                               #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jb	.LBB1_162
# BB#146:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%rax, %r8
	addq	$32, %r8
	movl	$37, %edi
	movl	$14, %esi
	movl	$.L.str.79, %edx
	jmp	.LBB1_161
.LBB1_147:                              #   in Loop: Header=BB1_119 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_162
.LBB1_148:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_149:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_149
# BB#150:                               #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jb	.LBB1_162
# BB#151:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%rax, %r8
	addq	$32, %r8
	movl	$37, %edi
	movl	$15, %esi
	movl	$.L.str.80, %edx
	jmp	.LBB1_161
.LBB1_152:                              #   in Loop: Header=BB1_119 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_162
.LBB1_153:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_154:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_154
# BB#155:                               #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jb	.LBB1_162
# BB#156:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%rax, %r8
	addq	$32, %r8
	movl	$37, %edi
	movl	$16, %esi
	movl	$.L.str.81, %edx
	jmp	.LBB1_161
.LBB1_157:                              #   in Loop: Header=BB1_119 Depth=1
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_158:                              #   Parent Loop BB1_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_158
# BB#159:                               #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jb	.LBB1_162
# BB#160:                               #   in Loop: Header=BB1_119 Depth=1
	movq	%rax, %r8
	addq	$32, %r8
	movl	$37, %edi
	movl	$17, %esi
	movl	$.L.str.82, %edx
.LBB1_161:                              # %.backedge1009.i
                                        #   in Loop: Header=BB1_119 Depth=1
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB1_162:                              # %.backedge1009.i
                                        #   in Loop: Header=BB1_119 Depth=1
	movq	8(%r12), %r12
	cmpq	%r14, %r12
	jne	.LBB1_119
.LBB1_163:                              # %._crit_edge1144.i
	xorl	%r14d, %r14d
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB1_390
# BB#164:                               # %._crit_edge1144.i
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	96(%rsp), %r12          # 8-byte Reload
	je	.LBB1_391
# BB#165:                               # %._crit_edge1144.i
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB1_391
# BB#166:                               # %._crit_edge1144.i
	testq	%r13, %r13
	je	.LBB1_391
# BB#167:                               # %._crit_edge1144.i
	testq	%rbp, %rbp
	je	.LBB1_391
# BB#168:
	movq	font_root(%rip), %r14
	movq	8(%r14), %r15
	cmpq	%r14, %r15
	je	.LBB1_175
# BB#169:                               # %.preheader1003.lr.ph.i
	leaq	64(%rbp), %r12
.LBB1_170:                              # %.preheader1003.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_171 Depth 2
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_171:                              #   Parent Loop BB1_170 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_171
# BB#172:                               #   in Loop: Header=BB1_170 Depth=1
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_176
# BB#173:                               #   in Loop: Header=BB1_170 Depth=1
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB1_170
# BB#174:
	movq	%rbp, %rsi
	movq	96(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_178
.LBB1_175:
	movq	%rbp, %rbx
	jmp	.LBB1_177
.LBB1_176:                              # %._crit_edge1122.i
	movq	%rbx, %rbp
	movq	96(%rsp), %r12          # 8-byte Reload
.LBB1_177:                              # %._crit_edge1126.i
	movq	%rbp, %rsi
	cmpq	%r14, %r15
	movq	%rbx, %rbp
	jne	.LBB1_183
.LBB1_178:                              # %._crit_edge1126.thread.i
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_180
# BB#179:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
	movq	font_root(%rip), %r14
.LBB1_180:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_183
# BB#181:
	testq	%rax, %rax
	je	.LBB1_183
# BB#182:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_183:
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_189
# BB#184:                               # %.preheader1002.lr.ph.i
	leaq	64(%r13), %r15
.LBB1_185:                              # %.preheader1002.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_186 Depth 2
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB1_186:                              #   Parent Loop BB1_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_186
# BB#187:                               #   in Loop: Header=BB1_185 Depth=1
	leaq	64(%r14), %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_196
# BB#188:                               #   in Loop: Header=BB1_185 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB1_185
.LBB1_189:                              # %._crit_edge1119.i
	movq	24(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_191
# BB#190:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_191:
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	120(%rsp), %r15         # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rax, zz_res(%rip)
	movq	%rdx, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_194
# BB#192:
	testq	%rax, %rax
	je	.LBB1_194
# BB#193:
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_194:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	56(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_197
# BB#195:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_198
.LBB1_196:
	leaq	32(%r14), %rdi
	addq	$32, %r13
	movq	104(%rsp), %rbx         # 8-byte Reload
	addq	$64, %rbx
	callq	EchoFilePos
	movq	%rax, 8(%rsp)
	movq	%r15, (%rsp)
	movl	$37, %edi
	movl	$19, %esi
	movl	$.L.str.85, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	callq	Error
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	DisposeObject
	jmp	.LBB1_103
.LBB1_197:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_198:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_200
# BB#199:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_200:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_202
# BB#201:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_202:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_204
# BB#203:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_205
.LBB1_204:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_205:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_207
# BB#206:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_207:
	movq	%rax, zz_res(%rip)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_209
# BB#208:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_209:
	testq	%rbp, %rbp
	je	.LBB1_218
# BB#210:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_213
# BB#211:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_214
.LBB1_212:
	xorl	%r13d, %r13d
	jmp	.LBB1_43
.LBB1_213:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_214:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_216
# BB#215:
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_216:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_218
# BB#217:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_218:
	testq	%r15, %r15
	je	.LBB1_228
# BB#219:
	movb	64(%r15), %al
	cmpb	$78, %al
	jne	.LBB1_223
# BB#220:
	cmpb	$111, 65(%r15)
	jne	.LBB1_227
# BB#221:
	cmpb	$0, 66(%r15)
	jne	.LBB1_227
# BB#222:
	andb	$127, 60(%r13)
	xorl	%esi, %esi
	jmp	.LBB1_229
.LBB1_223:                              # %.thread1230.i
	cmpb	$89, %al
	jne	.LBB1_227
# BB#224:
	cmpb	$101, 65(%r15)
	jne	.LBB1_227
# BB#225:
	cmpb	$115, 66(%r15)
	jne	.LBB1_227
# BB#226:
	cmpb	$0, 67(%r15)
	je	.LBB1_228
.LBB1_227:                              # %.thread1232.i
	addq	$32, %r15
	movl	$37, %edi
	movl	$20, %esi
	movl	$.L.str.88, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	jmp	.LBB1_230
.LBB1_228:
	orb	$-128, 60(%r13)
	movl	$1, %esi
.LBB1_229:
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	MapLoad
	movb	60(%r13), %cl
	andb	$127, %al
	andb	$-128, %cl
	orb	%al, %cl
	movb	%cl, 60(%r13)
.LBB1_230:
	movw	$0, 42(%r13)
	movl	font_count(%rip), %ecx
	incl	%ecx
	movl	%ecx, font_count(%rip)
	movl	finfo_size(%rip), %eax
	cmpl	%eax, %ecx
	jb	.LBB1_235
# BB#231:
	cmpl	$4097, %ecx             # imm = 0x1001
	jb	.LBB1_233
# BB#232:
	movl	$37, %edi
	movl	$21, %esi
	movl	$.L.str.89, %edx
	movl	$1, %ecx
	movl	$4096, %r9d             # imm = 0x1000
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movl	finfo_size(%rip), %eax
.LBB1_233:
	addl	%eax, %eax
	movl	%eax, finfo_size(%rip)
	movq	finfo(%rip), %rdi
	cltq
	shlq	$5, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	realloc
	movq	%rax, finfo(%rip)
	testq	%rax, %rax
	jne	.LBB1_235
# BB#234:
	movl	$37, %edi
	movl	$22, %esi
	movl	$.L.str.37, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
.LBB1_235:
	movl	font_seqnum(%rip), %edi
	incl	%edi
	movl	%edi, font_seqnum(%rip)
	callq	StringInt
	movq	no_fpos(%rip), %rcx
	movl	$11, %edi
	movl	$.L.str.90, %esi
	movq	%rax, %rdx
	callq	MakeWordTwo
	movq	%rax, %rsi
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	je	.LBB1_237
# BB#236:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_238
.LBB1_237:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	264(%rsp), %rsi         # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB1_238:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_240
# BB#239:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_240:
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB1_243
# BB#241:
	testq	%rax, %rax
	je	.LBB1_243
# BB#242:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_243:
	movzwl	font_count(%rip), %eax
	movzwl	40(%rsi), %ecx
	andl	$4095, %eax             # imm = 0xFFF
	andl	$61440, %ecx            # imm = 0xF000
	orl	%eax, %ecx
	movw	%cx, 40(%rsi)
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 40(%rcx)
	movl	$1000, %ecx             # imm = 0x3E8
	cmovel	PlainCharHeight(%rip), %ecx
	movl	%ecx, 48(%rsi)
	movb	60(%r13), %cl
	andb	$-128, %cl
	movb	60(%rsi), %dl
	andb	$127, %dl
	orb	%cl, %dl
	movb	%dl, 60(%rsi)
	movb	60(%r13), %dl
	andb	$127, %dl
	orb	%cl, %dl
	movb	%dl, 60(%rsi)
	movzwl	40(%r13), %ecx
	andl	$61440, %ecx            # imm = 0xF000
	orl	%eax, %ecx
	movw	%cx, 40(%r13)
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	64(%rdx), %rdi
	addq	$32, %rdx
	movl	$.L.str.14, %esi
	movl	$5, %ecx
	movl	$5, %r8d
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	callq	DefineFile
	movzwl	%ax, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%edi, 40(%rsp)          # 4-byte Spill
	callq	OpenFile
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_245
# BB#244:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$23, %esi
	movl	$.L.str.91, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB1_245:
	leaq	1712(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_248
# BB#246:
	leaq	1712(%rsp), %rdi
	leaq	688(%rsp), %rdx
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_248
# BB#247:
	leaq	688(%rsp), %rdi
	movl	$.L.str.93, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_249
.LBB1_248:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$24, %esi
	movl	$.L.str.94, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB1_249:
	movl	$2560, %edi             # imm = 0xA00
	callq	malloc
	movq	%rax, 120(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB1_251
# BB#250:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$25, %esi
	movl	$.L.str.95, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB1_251:
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	testq	%rax, %rax
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jne	.LBB1_253
# BB#252:
	movl	40(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rbx
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$25, %esi
	movl	$.L.str.95, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
	movq	%rbx, %rax
	movq	112(%rsp), %rbx         # 8-byte Reload
.LBB1_253:                              # %.preheader1001.preheader.i
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movups	%xmm0, (%rax)
	movl	$258, 220(%rsp)         # imm = 0x102
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, 280(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB1_255
# BB#254:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$25, %esi
	movl	$.L.str.95, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB1_255:
	movl	$1536, %edi             # imm = 0x600
	callq	malloc
	movq	%rax, 104(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB1_257
# BB#256:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$25, %esi
	movl	$.L.str.95, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB1_257:                              # %.preheader1000.preheader.i
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	memset
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	testq	%rax, %rax
	jne	.LBB1_259
# BB#258:
	movl	40(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rbx
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$25, %esi
	movl	$.L.str.95, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
	movq	%rbx, %rax
	movq	112(%rsp), %rbx         # 8-byte Reload
.LBB1_259:                              # %.preheader.preheader.i
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	callq	memset
	movl	$1, 32(%rsp)
	leaq	1712(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%rdi, %r15
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_337
# BB#260:                               # %.lr.ph1106.i
	leaq	688(%rsp), %rcx
	movl	$0, 156(%rsp)           # 4-byte Folded Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movl	$0, 152(%rsp)           # 4-byte Folded Spill
	movl	$0, 148(%rsp)           # 4-byte Folded Spill
	movl	$0, 216(%rsp)           # 4-byte Folded Spill
	movl	$0, 212(%rsp)           # 4-byte Folded Spill
	movl	$0, 160(%rsp)           # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
.LBB1_261:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_285 Depth 2
                                        #       Child Loop BB1_286 Depth 3
                                        #       Child Loop BB1_302 Depth 3
                                        #       Child Loop BB1_311 Depth 3
                                        #     Child Loop BB1_271 Depth 2
	cmpb	$69, 1712(%rsp)
	jne	.LBB1_263
# BB#262:                               #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.96, %esi
	movq	%r15, %rdi
	callq	strcmp
	leaq	688(%rsp), %rcx
	testl	%eax, %eax
	je	.LBB1_339
.LBB1_263:                              # %.critedge968.i
                                        #   in Loop: Header=BB1_261 Depth=1
	incl	32(%rsp)
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	sscanf
	movb	688(%rsp), %al
	addb	$-70, %al
	cmpb	$18, %al
	ja	.LBB1_336
# BB#264:                               # %.critedge968.i
                                        #   in Loop: Header=BB1_261 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI1_1(,%rax,8)
.LBB1_265:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.106, %esi
	leaq	688(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_336
# BB#266:                               #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 156(%rsp)           # 4-byte Folded Reload
	je	.LBB1_268
# BB#267:                               #   in Loop: Header=BB1_261 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$29, %esi
	movl	$.L.str.107, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_268:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.108, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	688(%rsp), %rdx
	callq	sscanf
	cmpb	$0, 688(%rsp)
	jne	.LBB1_270
# BB#269:                               #   in Loop: Header=BB1_261 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$30, %esi
	movl	$.L.str.109, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_270:                              #   in Loop: Header=BB1_261 Depth=1
	movq	%r14, %rbp
	movq	8(%r13), %rbx
.LBB1_271:                              #   Parent Loop BB1_261 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_271
# BB#272:                               #   in Loop: Header=BB1_261 Depth=1
	addq	$64, %rbx
	leaq	688(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcmp
	movl	$1, 156(%rsp)           # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB1_274
# BB#273:                               #   in Loop: Header=BB1_261 Depth=1
	movq	%rbx, 8(%rsp)
	movq	$.L.str.1, (%rsp)
	movl	$37, %edi
	movl	$31, %esi
	movl	$.L.str.110, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	callq	Error
.LBB1_274:                              #   in Loop: Header=BB1_261 Depth=1
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbp, %r14
	jmp	.LBB1_336
.LBB1_275:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.111, %esi
	leaq	688(%rsp), %rbp
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_336
# BB#276:                               #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.112, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	sscanf
	movl	$.L.str.113, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	$1, %ecx
	cmovel	%ecx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB1_336
.LBB1_277:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.114, %esi
	leaq	688(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_328
# BB#278:                               #   in Loop: Header=BB1_261 Depth=1
	movq	BackEnd(%rip), %rax
	cmpl	$0, 40(%rax)
	je	.LBB1_336
# BB#279:                               #   in Loop: Header=BB1_261 Depth=1
	movl	Kern(%rip), %eax
	testl	%eax, %eax
	je	.LBB1_336
# BB#280:                               #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.116, %esi
	leaq	688(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_336
# BB#281:                               #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.117, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	576(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB1_283
# BB#282:                               #   in Loop: Header=BB1_261 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$33, %esi
	movl	$.L.str.118, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_283:                              #   in Loop: Header=BB1_261 Depth=1
	movslq	576(%rsp), %rbx
	addq	%rbx, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rbp
	movq	%rbp, %rdi
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	fgets
	movl	$1, %ecx
	cmpq	%rbp, %rax
	jne	.LBB1_335
# BB#284:                               # %.lr.ph1066.i.preheader
                                        #   in Loop: Header=BB1_261 Depth=1
	movl	$1, %ecx
	movl	$0, 144(%rsp)           # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
.LBB1_285:                              # %.lr.ph1066.i
                                        #   Parent Loop BB1_261 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_286 Depth 3
                                        #       Child Loop BB1_302 Depth 3
                                        #       Child Loop BB1_311 Depth 3
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	movq	%r14, %rbp
	movq	%r15, %r14
.LBB1_286:                              #   Parent Loop BB1_261 Depth=1
                                        #     Parent Loop BB1_285 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$.L.str.119, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB1_334
# BB#287:                               #   in Loop: Header=BB1_286 Depth=3
	movq	%rbp, %r12
	incl	32(%rsp)
	movl	$.L.str.120, %esi
	movq	%r14, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB1_296
# BB#288:                               #   in Loop: Header=BB1_286 Depth=3
	movl	$.L.str.121, %esi
	xorl	%eax, %eax
	movq	%r15, %rbx
	movq	%rbx, %rdi
	leaq	1200(%rsp), %rdx
	leaq	352(%rsp), %rcx
	leaq	132(%rsp), %r8
	callq	sscanf
	cmpl	$3, %eax
	je	.LBB1_290
# BB#289:                               #   in Loop: Header=BB1_286 Depth=3
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	32(%rsp), %eax
	movq	%rbx, 8(%rsp)
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$34, %esi
	movl	$.L.str.122, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_290:                              #   in Loop: Header=BB1_286 Depth=3
	cvttss2si	132(%rsp), %r14d
	testl	%r14d, %r14d
	je	.LBB1_296
# BB#291:                               #   in Loop: Header=BB1_286 Depth=3
	movzbl	60(%r13), %esi
	andl	$127, %esi
	leaq	1200(%rsp), %rdi
	callq	MapCharEncoding
	movl	%eax, %ebx
	testb	%bl, %bl
	je	.LBB1_296
# BB#292:                               #   in Loop: Header=BB1_286 Depth=3
	movzbl	60(%r13), %esi
	andl	$127, %esi
	leaq	352(%rsp), %rdi
	callq	MapCharEncoding
	testb	%al, %al
	je	.LBB1_296
# BB#293:                               #   in Loop: Header=BB1_286 Depth=3
	cmpb	144(%rsp), %bl          # 1-byte Folded Reload
	je	.LBB1_297
# BB#294:                               #   in Loop: Header=BB1_286 Depth=3
	movzbl	%bl, %edx
	movq	256(%rsp), %rcx         # 8-byte Reload
	cmpw	$0, (%rcx,%rdx,2)
	je	.LBB1_298
# BB#295:                               #   in Loop: Header=BB1_286 Depth=3
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movl	32(%rsp), %ecx
	movl	%ecx, 16(%rsp)
	movq	%rax, 8(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$37, %edi
	movl	$35, %esi
	movl	$.L.str.123, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	1200(%rsp), %r9
	callq	Error
.LBB1_296:                              # %.backedge998.i
                                        #   in Loop: Header=BB1_286 Depth=3
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %r14
	movq	%r14, %rdi
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	fgets
	cmpq	%r14, %rax
	movq	%r12, %rbp
	je	.LBB1_286
	jmp	.LBB1_334
.LBB1_297:                              #   in Loop: Header=BB1_285 Depth=2
	movb	%al, 39(%rsp)           # 1-byte Spill
	movzbl	144(%rsp), %eax         # 1-byte Folded Reload
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rbp
	movzwl	(%rcx,%rax,2), %eax
	testw	%ax, %ax
	jne	.LBB1_300
	jmp	.LBB1_299
.LBB1_298:                              #   in Loop: Header=BB1_285 Depth=2
	movb	%al, 39(%rsp)           # 1-byte Spill
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rbp
	movb	%bl, %al
	movl	%eax, 144(%rsp)         # 4-byte Spill
.LBB1_299:                              # %.thread1233.i
                                        #   in Loop: Header=BB1_285 Depth=2
	movq	200(%rsp), %rcx         # 8-byte Reload
	movw	%cx, (%rbp)
	movl	%ecx, %eax
	movslq	%ecx, %rcx
	movq	168(%rsp), %rdx         # 8-byte Reload
	movb	$0, (%rdx,%rcx)
	movq	176(%rsp), %rdx         # 8-byte Reload
	movb	$0, (%rdx,%rcx)
	incl	%ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rcx, 200(%rsp)         # 8-byte Spill
.LBB1_300:                              #   in Loop: Header=BB1_285 Depth=2
	movl	$1, %ebx
	movl	68(%rsp), %ecx          # 4-byte Reload
	cmpl	$2, %ecx
	jl	.LBB1_305
# BB#301:                               # %.lr.ph1073.preheader.i
                                        #   in Loop: Header=BB1_285 Depth=2
	movslq	%ecx, %rcx
	movl	$1, %ebx
.LBB1_302:                              # %.lr.ph1073.i
                                        #   Parent Loop BB1_261 Depth=1
                                        #     Parent Loop BB1_285 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	184(%rsp), %rdx         # 8-byte Reload
	movswl	(%rdx,%rbx,2), %edx
	cmpl	%r14d, %edx
	je	.LBB1_304
# BB#303:                               #   in Loop: Header=BB1_302 Depth=3
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB1_302
.LBB1_304:                              # %.lr.ph1073.i.._crit_edge1074.i.loopexit_crit_edge
                                        #   in Loop: Header=BB1_285 Depth=2
	movl	68(%rsp), %ecx          # 4-byte Reload
.LBB1_305:                              # %._crit_edge1074.i
                                        #   in Loop: Header=BB1_285 Depth=2
	cmpl	%ecx, %ebx
	jne	.LBB1_309
# BB#306:                               #   in Loop: Header=BB1_285 Depth=2
	cmpl	576(%rsp), %ecx
	jne	.LBB1_308
# BB#307:                               #   in Loop: Header=BB1_285 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %r9
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$36, %esi
	movl	$.L.str.124, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	Error
	movzwl	(%rbp), %eax
.LBB1_308:                              #   in Loop: Header=BB1_285 Depth=2
	movl	68(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %rcx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movw	%r14w, (%rdx,%rcx,2)
	incl	%esi
	movl	%esi, 68(%rsp)          # 4-byte Spill
.LBB1_309:                              #   in Loop: Header=BB1_285 Depth=2
	movq	200(%rsp), %rdx         # 8-byte Reload
	leal	-1(%rdx), %ecx
	movzwl	%ax, %esi
	cmpl	%esi, %edx
	jle	.LBB1_313
# BB#310:                               # %.lr.ph1082.preheader.i
                                        #   in Loop: Header=BB1_285 Depth=2
	movslq	%edx, %rdx
	movq	168(%rsp), %r14         # 8-byte Reload
	movb	39(%rsp), %r8b          # 1-byte Reload
.LBB1_311:                              # %.lr.ph1082.i
                                        #   Parent Loop BB1_261 Depth=1
                                        #     Parent Loop BB1_285 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-1(%r14,%rdx), %eax
	cmpb	%r8b, %al
	jae	.LBB1_314
# BB#312:                               #   in Loop: Header=BB1_311 Depth=3
	leaq	-1(%rdx), %rbp
	movb	%al, (%r14,%rdx)
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdi
	movzbl	-1(%rdi,%rdx), %eax
	movb	%al, (%rdi,%rdx)
	decl	%ecx
	cmpq	%rsi, %rbp
	movq	%rbp, %rdx
	jg	.LBB1_311
	jmp	.LBB1_315
.LBB1_313:                              #   in Loop: Header=BB1_285 Depth=2
	movl	%edx, %ebp
	movq	168(%rsp), %r14         # 8-byte Reload
	movb	39(%rsp), %r8b          # 1-byte Reload
	cmpl	%esi, %ebp
	jg	.LBB1_316
	jmp	.LBB1_318
.LBB1_314:                              # %.lr.ph1082.i..critedge89.i.loopexit_crit_edge
                                        #   in Loop: Header=BB1_285 Depth=2
	movl	%edx, %ebp
.LBB1_315:                              # %.critedge89.i
                                        #   in Loop: Header=BB1_285 Depth=2
	cmpl	%esi, %ebp
	jle	.LBB1_318
.LBB1_316:                              #   in Loop: Header=BB1_285 Depth=2
	movslq	%ecx, %rax
	cmpb	%r8b, (%r14,%rax)
	jne	.LBB1_318
# BB#317:                               #   in Loop: Header=BB1_285 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	movb	%r8b, 39(%rsp)          # 1-byte Spill
	callq	FileName
	movl	32(%rsp), %ecx
	movl	%ecx, 16(%rsp)
	movq	%rax, 8(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$37, %edi
	movl	$37, %esi
	movl	$.L.str.125, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	1200(%rsp), %r9
	callq	Error
	movb	39(%rsp), %r8b          # 1-byte Reload
.LBB1_318:                              # %.outer997.i
                                        #   in Loop: Header=BB1_285 Depth=2
	movslq	%ebp, %rax
	movb	%r8b, (%r14,%rax)
	movq	176(%rsp), %rcx         # 8-byte Reload
	movb	%bl, (%rcx,%rax)
	movq	200(%rsp), %rax         # 8-byte Reload
	incl	%eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rbp
	movq	%rbp, %rdi
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	fgets
	cmpq	%rbp, %rax
	movq	%r12, %r14
	movl	68(%rsp), %ecx          # 4-byte Reload
	je	.LBB1_285
	jmp	.LBB1_335
.LBB1_319:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.97, %esi
	leaq	688(%rsp), %rbp
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_331
# BB#320:                               #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.100, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_336
# BB#321:                               #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 212(%rsp)           # 4-byte Folded Reload
	je	.LBB1_323
# BB#322:                               #   in Loop: Header=BB1_261 Depth=1
	movl	32(%rsp), %r9d
	movl	$37, %edi
	movl	$27, %esi
	movl	$.L.str.101, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_323:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.102, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	296(%rsp), %rdx
	callq	sscanf
	movl	$1, 212(%rsp)           # 4-byte Folded Spill
	cvttss2si	296(%rsp), %eax
	movl	%eax, 148(%rsp)         # 4-byte Spill
	jmp	.LBB1_336
.LBB1_324:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.103, %esi
	leaq	688(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_336
# BB#325:                               #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	je	.LBB1_327
# BB#326:                               #   in Loop: Header=BB1_261 Depth=1
	movl	32(%rsp), %r9d
	movl	$37, %edi
	movl	$28, %esi
	movl	$.L.str.104, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_327:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.105, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	304(%rsp), %rdx
	callq	sscanf
	movss	304(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI1_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	$1, 160(%rsp)           # 4-byte Folded Spill
	jmp	.LBB1_336
.LBB1_328:                              #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 156(%rsp)           # 4-byte Folded Reload
	jne	.LBB1_330
# BB#329:                               #   in Loop: Header=BB1_261 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$32, %esi
	movl	$.L.str.115, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_330:                              #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	$250, %eax
	cmovel	%eax, %edx
	movq	%rbx, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movq	288(%rsp), %rcx         # 8-byte Reload
	leaq	220(%rsp), %r8
	movl	40(%rsp), %r9d          # 4-byte Reload
	callq	ReadCharMetrics
	jmp	.LBB1_336
.LBB1_331:                              #   in Loop: Header=BB1_261 Depth=1
	cmpl	$0, 216(%rsp)           # 4-byte Folded Reload
	je	.LBB1_333
# BB#332:                               #   in Loop: Header=BB1_261 Depth=1
	movl	32(%rsp), %r9d
	movl	$37, %edi
	movl	$26, %esi
	movl	$.L.str.98, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_333:                              #   in Loop: Header=BB1_261 Depth=1
	movl	$.L.str.99, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	300(%rsp), %rdx
	callq	sscanf
	movl	$1, 216(%rsp)           # 4-byte Folded Spill
	cvttss2si	300(%rsp), %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	jmp	.LBB1_336
.LBB1_334:                              #   in Loop: Header=BB1_261 Depth=1
	movq	%rbp, %r14
	movl	68(%rsp), %ecx          # 4-byte Reload
.LBB1_335:                              # %.critedge88.i
                                        #   in Loop: Header=BB1_261 Depth=1
	movq	184(%rsp), %rax         # 8-byte Reload
	movw	%cx, (%rax)
.LBB1_336:                              # %.backedge999.i
                                        #   in Loop: Header=BB1_261 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	leaq	688(%rsp), %rcx
	jne	.LBB1_261
	jmp	.LBB1_338
.LBB1_337:
	xorl	%eax, %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	$0, 148(%rsp)           # 4-byte Folded Spill
	movl	$0, 152(%rsp)           # 4-byte Folded Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
.LBB1_338:                              # %.loopexit.i
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$38, %esi
	movl	$.L.str.126, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	Error
	jmp	.LBB1_340
.LBB1_339:
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB1_340:                              # %.critedge.i
	movq	%rbx, %rdi
	callq	fclose
	movq	BackEnd(%rip), %rax
	cmpl	$0, 40(%rax)
	movl	72(%rsp), %eax          # 4-byte Reload
	jne	.LBB1_342
# BB#341:
	movl	PlainCharHeight(%rip), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
.LBB1_342:
	movq	264(%rsp), %rcx         # 8-byte Reload
	movl	%eax, 52(%rcx)
	movzbl	60(%rcx), %esi
	andl	$127, %esi
	movl	$.L.str.127, %edi
	callq	MapCharEncoding
	testb	%al, %al
	je	.LBB1_344
# BB#343:
	movzbl	%al, %eax
	leaq	(%rax,%rax,4), %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movswl	6(%rcx,%rax,2), %eax
	jmp	.LBB1_345
.LBB1_344:
	xorl	%eax, %eax
.LBB1_345:
	movq	264(%rsp), %rdx         # 8-byte Reload
	movl	%eax, 56(%rdx)
	testq	%rbp, %rbp
	je	.LBB1_350
# BB#346:
	leaq	64(%rbp), %rdi
	addq	$32, %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	$.L.str.14, %esi
	movl	$5, %ecx
	movl	$5, %r8d
	movq	%rbp, %rdx
	callq	DefineFile
	movzwl	%ax, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%edi, 40(%rsp)          # 4-byte Spill
	callq	OpenFile
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB1_348
# BB#347:
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$37, %edi
	movl	$39, %esi
	movl	$.L.str.128, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_348:
	movl	$0, 32(%rsp)
	leaq	1712(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_351
# BB#349:                               # %.lr.ph.lr.ph.i
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	1712(%rsp), %rbp
	leaq	688(%rsp), %rcx
	jmp	.LBB1_354
.LBB1_350:
	movl	$1, %esi
	jmp	.LBB1_389
.LBB1_351:
	movl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jmp	.LBB1_388
.LBB1_352:                              # %.critedge.i.i
                                        #   in Loop: Header=BB1_354 Depth=1
	movl	$.L.str.150, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB1_387
.LBB1_353:                              # %.loopexit72.i.i
                                        #   in Loop: Header=BB1_354 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %r9
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$9, %esi
	movl	$.L.str.151, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	callq	Error
	jmp	.LBB1_387
.LBB1_354:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_359 Depth 2
                                        #       Child Loop BB1_364 Depth 3
                                        #       Child Loop BB1_372 Depth 3
                                        #         Child Loop BB1_375 Depth 4
	incl	32(%rsp)
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	sscanf
	cmpb	$83, 688(%rsp)
	jne	.LBB1_387
# BB#355:                               #   in Loop: Header=BB1_354 Depth=1
	movl	$.L.str.129, %esi
	leaq	688(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_386
# BB#356:                               #   in Loop: Header=BB1_354 Depth=1
	movl	$.L.str.130, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	leaq	1200(%rsp), %rbx
	jne	.LBB1_387
# BB#357:                               #   in Loop: Header=BB1_354 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_353
# BB#358:                               # %.lr.ph83.i.i.preheader
                                        #   in Loop: Header=BB1_354 Depth=1
	movq	%r15, 112(%rsp)         # 8-byte Spill
.LBB1_359:                              # %.lr.ph83.i.i
                                        #   Parent Loop BB1_354 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_364 Depth 3
                                        #       Child Loop BB1_372 Depth 3
                                        #         Child Loop BB1_375 Depth 4
	movl	$.L.str.144, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB1_352
# BB#360:                               #   in Loop: Header=BB1_359 Depth=2
	incl	32(%rsp)
	movl	$.L.str.145, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	leaq	352(%rsp), %rdx
	leaq	132(%rsp), %rcx
	callq	sscanf
	cmpl	$2, %eax
	je	.LBB1_362
# BB#361:                               #   in Loop: Header=BB1_359 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$5, %esi
	movl	$.L.str.146, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	callq	Error
.LBB1_362:                              # %.preheader70.i.i.preheader
                                        #   in Loop: Header=BB1_359 Depth=2
	xorl	%r14d, %r14d
	jmp	.LBB1_364
	.p2align	4, 0x90
.LBB1_363:                              #   in Loop: Header=BB1_364 Depth=3
	incq	%r14
.LBB1_364:                              # %.preheader70.i.i
                                        #   Parent Loop BB1_354 Depth=1
                                        #     Parent Loop BB1_359 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	1200(%rsp,%r14), %eax
	testb	%al, %al
	je	.LBB1_367
# BB#365:                               # %.preheader70.i.i
                                        #   in Loop: Header=BB1_364 Depth=3
	cmpb	$10, %al
	je	.LBB1_367
# BB#366:                               # %.preheader70.i.i
                                        #   in Loop: Header=BB1_364 Depth=3
	cmpb	$59, %al
	jne	.LBB1_363
	jmp	.LBB1_368
.LBB1_367:                              #   in Loop: Header=BB1_359 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$5, %esi
	movl	$.L.str.146, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	callq	Error
.LBB1_368:                              # %.loopexit71.i.i
                                        #   in Loop: Header=BB1_359 Depth=2
	movzbl	60(%r13), %esi
	andl	$127, %esi
	leaq	352(%rsp), %rdi
	callq	MapCharEncoding
	movl	%eax, %ebx
	testb	%bl, %bl
	jne	.LBB1_370
# BB#369:                               #   in Loop: Header=BB1_359 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %r9
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$6, %esi
	movl	$.L.str.147, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_370:                              #   in Loop: Header=BB1_359 Depth=2
	movzbl	%bl, %eax
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movw	%bx, (%rcx,%rax,2)
	cmpl	$0, 132(%rsp)
	jle	.LBB1_383
# BB#371:                               # %.lr.ph.i.preheader.i
                                        #   in Loop: Header=BB1_359 Depth=2
	movslq	%ebx, %rbx
	xorl	%r12d, %r12d
	leaq	576(%rsp), %rbp
.LBB1_372:                              # %.lr.ph.i.i
                                        #   Parent Loop BB1_354 Depth=1
                                        #     Parent Loop BB1_359 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_375 Depth 4
	shlq	$32, %r14
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %r14
	sarq	$32, %r14
	leaq	1200(%rsp,%r14), %rdi
	movl	$.L.str.148, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	leaq	316(%rsp), %rcx
	leaq	312(%rsp), %r8
	callq	sscanf
	cmpl	$3, %eax
	movq	%rbx, %r15
	je	.LBB1_375
# BB#373:                               #   in Loop: Header=BB1_372 Depth=3
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$5, %esi
	movl	$.L.str.146, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	movq	%r15, %rbx
	callq	Error
	jmp	.LBB1_375
	.p2align	4, 0x90
.LBB1_374:                              #   in Loop: Header=BB1_375 Depth=4
	incq	%r14
.LBB1_375:                              # %.preheader.i.i
                                        #   Parent Loop BB1_354 Depth=1
                                        #     Parent Loop BB1_359 Depth=2
                                        #       Parent Loop BB1_372 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	1200(%rsp,%r14), %eax
	testb	%al, %al
	je	.LBB1_378
# BB#376:                               # %.preheader.i.i
                                        #   in Loop: Header=BB1_375 Depth=4
	cmpb	$10, %al
	je	.LBB1_378
# BB#377:                               # %.preheader.i.i
                                        #   in Loop: Header=BB1_375 Depth=4
	cmpb	$59, %al
	jne	.LBB1_374
	jmp	.LBB1_379
.LBB1_378:                              #   in Loop: Header=BB1_372 Depth=3
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$5, %esi
	movl	$.L.str.146, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	movq	%r15, %rbx
	callq	Error
.LBB1_379:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB1_372 Depth=3
	cmpq	$256, %rbx              # imm = 0x100
	jl	.LBB1_381
# BB#380:                               #   in Loop: Header=BB1_372 Depth=3
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$7, %esi
	movl	$.L.str.149, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	movq	%r15, %rbx
	callq	Error
.LBB1_381:                              #   in Loop: Header=BB1_372 Depth=3
	movzbl	60(%r13), %esi
	andl	$127, %esi
	leaq	576(%rsp), %rbp
	movq	%rbp, %rdi
	callq	MapCharEncoding
	leaq	(%rbx,%rbx,2), %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	movb	%al, (%rdx,%rcx,2)
	movzwl	316(%rsp), %eax
	movw	%ax, 2(%rdx,%rcx,2)
	movzwl	312(%rsp), %eax
	movw	%ax, 4(%rdx,%rcx,2)
	incq	%rbx
	incl	%r12d
	cmpl	132(%rsp), %r12d
	jl	.LBB1_372
# BB#382:                               # %._crit_edge.i.loopexit.i
                                        #   in Loop: Header=BB1_359 Depth=2
	movq	112(%rsp), %r15         # 8-byte Reload
	leaq	1712(%rsp), %rbp
.LBB1_383:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_359 Depth=2
	cmpl	$256, %ebx              # imm = 0x100
	jl	.LBB1_385
# BB#384:                               #   in Loop: Header=BB1_359 Depth=2
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rbx, %r10
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$8, %esi
	movl	$.L.str.149, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	movq	%r10, %rbx
	callq	Error
.LBB1_385:                              #   in Loop: Header=BB1_359 Depth=2
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movb	$0, (%rcx,%rax,2)
	incl	%ebx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	$512, %esi              # imm = 0x200
	leaq	1200(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB1_359
	jmp	.LBB1_353
.LBB1_386:                              #   in Loop: Header=BB1_354 Depth=1
	movq	%r15, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	72(%rsp), %edx          # 4-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	leaq	220(%rsp), %r8
	movl	40(%rsp), %r9d          # 4-byte Reload
	callq	ReadCharMetrics
.LBB1_387:                              # %.backedge.i
                                        #   in Loop: Header=BB1_354 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	leaq	688(%rsp), %rcx
	jne	.LBB1_354
.LBB1_388:                              # %.outer._crit_edge.i
	movq	%r15, %rdi
	callq	fclose
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB1_389:
	movl	font_count(%rip), %eax
	shlq	$5, %rax
	leaq	(%rax,%rax,2), %rax
	movq	finfo(%rip), %rcx
	movq	%rdx, 40(%rcx,%rax)
	movl	72(%rsp), %edx          # 4-byte Reload
	subl	152(%rsp), %edx         # 4-byte Folded Reload
	movq	%r13, 48(%rcx,%rax)
	movw	%dx, 56(%rcx,%rax)
	movl	148(%rsp), %edx         # 4-byte Reload
	movw	%dx, 58(%rcx,%rax)
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, (%rcx,%rax)
	movq	finfo(%rip), %rcx
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 8(%rcx,%rax)
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 16(%rcx,%rax)
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 24(%rcx,%rax)
	movl	%esi, 32(%rcx,%rax)
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 64(%rcx,%rax)
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 72(%rcx,%rax)
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 80(%rcx,%rax)
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 88(%rcx,%rax)
	movq	%r13, %r14
	movq	272(%rsp), %r15         # 8-byte Reload
.LBB1_390:                              # %FontRead.exit
	movq	96(%rsp), %r12          # 8-byte Reload
.LBB1_391:                              # %FontRead.exit
	testq	%r14, %r14
	movq	328(%rsp), %r13         # 8-byte Reload
	je	.LBB1_402
.LBB1_392:                              # %.thread598
	leaq	8(%r14), %r15
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB1_394
# BB#393:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r15), %rax
.LBB1_394:
	movq	320(%rsp), %rbx         # 8-byte Reload
	cmpq	%r14, 8(%rax)
	jne	.LBB1_396
# BB#395:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_396:
	movq	8(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%r14, 8(%rax)
	jne	.LBB1_398
# BB#397:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.31, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_398:
	testq	%r13, %r13
	je	.LBB1_409
# BB#399:
	leaq	1200(%rsp), %rdx
	leaq	1712(%rsp), %rcx
	movq	%r13, %rdi
	movq	192(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	callq	GetGap
	movzwl	1200(%rsp), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$9216, %eax             # imm = 0x2400
	jne	.LBB1_410
# BB#400:
	movl	1712(%rsp), %r9d
	cmpl	$158, %r9d
	jne	.LBB1_460
# BB#401:
	movw	1202(%rsp), %bp
	testw	%bp, %bp
	jg	.LBB1_412
	jmp	.LBB1_474
.LBB1_402:
	movq	font_root(%rip), %rbx
	movq	8(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB1_464
# BB#403:                               # %.lr.ph.preheader
	movq	136(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_404:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_405 Depth 2
	leaq	16(%rbp), %rax
	.p2align	4, 0x90
.LBB1_405:                              #   Parent Loop BB1_404 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_405
# BB#406:                               #   in Loop: Header=BB1_404 Depth=1
	addq	$64, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_465
# BB#407:                               #   in Loop: Header=BB1_404 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB1_404
# BB#408:                               # %._crit_edge.thread
	addq	$32, %r14
	jmp	.LBB1_467
.LBB1_409:
	movq	finfo(%rip), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movl	12(%rcx), %ecx
	jmp	.LBB1_411
.LBB1_410:
	leaq	32(%r13), %r8
	addq	$64, %r13
	movl	$37, %edi
	movl	$47, %esi
	movl	$.L.str.32, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r9
	callq	Error
	movq	finfo(%rip), %rax
	movl	12(%rbp), %ecx
.LBB1_411:
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movw	48(%rax), %bp
	testw	%bp, %bp
	jle	.LBB1_474
.LBB1_412:                              # %.thread600
	movq	BackEnd(%rip), %rax
	cmpl	$0, 40(%rax)
	cmovew	PlainCharHeight(%rip), %bp
	movq	8(%r14), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	je	.LBB1_418
# BB#413:                               # %.lr.ph684
	movswl	%bp, %ecx
	.p2align	4, 0x90
.LBB1_414:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_415 Depth 2
	leaq	16(%rax), %rsi
	.p2align	4, 0x90
.LBB1_415:                              #   Parent Loop BB1_414 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	leaq	16(%rdx), %rsi
	cmpb	$0, 32(%rdx)
	je	.LBB1_415
# BB#416:                               #   in Loop: Header=BB1_414 Depth=1
	cmpl	%ecx, 48(%rdx)
	je	.LBB1_429
# BB#417:                               #   in Loop: Header=BB1_414 Depth=1
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.LBB1_414
.LBB1_418:                              # %._crit_edge685
	movl	font_count(%rip), %ecx
	incl	%ecx
	movl	%ecx, font_count(%rip)
	movl	finfo_size(%rip), %eax
	cmpl	%eax, %ecx
	jb	.LBB1_423
# BB#419:
	cmpl	$4097, %ecx             # imm = 0x1001
	jb	.LBB1_421
# BB#420:
	movl	$37, %edi
	movl	$51, %esi
	movl	$.L.str.36, %edx
	movl	$1, %ecx
	movl	$4096, %r9d             # imm = 0x1000
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
	movl	finfo_size(%rip), %eax
.LBB1_421:
	addl	%eax, %eax
	movl	%eax, finfo_size(%rip)
	movq	finfo(%rip), %rdi
	cltq
	shlq	$5, %rax
	leaq	(%rax,%rax,2), %rsi
	callq	realloc
	movq	%rax, finfo(%rip)
	testq	%rax, %rax
	jne	.LBB1_423
# BB#422:
	movl	$37, %edi
	movl	$52, %esi
	movl	$.L.str.37, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_423:
	movq	(%r15), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %r12
	.p2align	4, 0x90
.LBB1_424:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB1_424
# BB#425:
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_427
# BB#426:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.38, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_427:                              # %.loopexit606
	leaq	64(%r12), %rsi
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %r13
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_430
# BB#428:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_431
.LBB1_429:
	movzwl	40(%rdx), %eax
	andl	$4095, %eax             # imm = 0xFFF
	movl	$-4096, %ecx            # imm = 0xF000
	movq	192(%rsp), %rsi         # 8-byte Reload
	andl	12(%rsi), %ecx
	orl	%eax, %ecx
	movl	%ecx, 12(%rsi)
	movb	$38, 5(%rsi)
	movzwl	56(%rdx), %eax
	movw	%ax, 6(%rsi)
	jmp	.LBB1_476
.LBB1_430:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_431:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_434
# BB#432:
	testq	%rax, %rax
	je	.LBB1_434
# BB#433:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_434:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB1_437
# BB#435:
	testq	%rax, %rax
	je	.LBB1_437
# BB#436:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_437:
	movl	font_count(%rip), %r8d
	movzwl	40(%r13), %eax
	movl	%r8d, %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	andl	$61440, %eax            # imm = 0xF000
	orl	%ecx, %eax
	movw	%ax, 40(%r13)
	movq	BackEnd(%rip), %rax
	cmpl	$0, 40(%rax)
	je	.LBB1_439
# BB#438:
	movswl	%bp, %ecx
	leaq	48(%r12), %r15
	jmp	.LBB1_440
.LBB1_439:
	leaq	48(%r12), %r15
	movl	48(%r12), %ecx
.LBB1_440:
	movl	%ecx, 48(%r13)
	movl	4(%r15), %eax
	imull	%ecx, %eax
	cltd
	idivl	48(%r12)
	movl	%eax, 52(%r13)
	movb	60(%r12), %al
	andb	$-128, %al
	movb	60(%r13), %dl
	andb	$127, %dl
	orb	%al, %dl
	movb	%dl, 60(%r13)
	movb	60(%r12), %dl
	andb	$127, %dl
	orb	%al, %dl
	movb	%dl, 60(%r13)
	movl	56(%r12), %eax
	imull	%ecx, %eax
	movl	48(%r12), %esi
	cltd
	idivl	%esi
	movl	%eax, 56(%r13)
	movq	finfo(%rip), %rbp
	movq	%r8, %rax
	shlq	$5, %rax
	leaq	(%rax,%rax,2), %rbx
	movq	%r13, 40(%rbp,%rbx)
	movq	%r14, 48(%rbp,%rbx)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movswl	56(%rbp,%rax), %eax
	imull	%ecx, %eax
	cltd
	idivl	%esi
	movw	%ax, 56(%rbp,%rbx)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movswl	58(%rbp,%rax), %eax
	imull	%ecx, %eax
	cltd
	idivl	%esi
	movw	%ax, 58(%rbp,%rbx)
	movl	$2560, %edi             # imm = 0xA00
	movq	%r8, %r14
	callq	malloc
	movq	%r14, %rcx
	movq	%rax, (%rbp,%rbx)
	movq	finfo(%rip), %r9
	cmpq	$0, (%r9,%rbx)
	movq	80(%rsp), %r14          # 8-byte Reload
	jne	.LBB1_442
# BB#441:
	movl	$37, %edi
	movl	$53, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movq	finfo(%rip), %r9
	movl	font_count(%rip), %ecx
.LBB1_442:
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	8(%r9,%rax), %rdi
	movl	%ecx, %eax
	leaq	(%rax,%rax,2), %r8
	shlq	$5, %r8
	movq	%rdi, 8(%r9,%r8)
	movzwl	40(%r13), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	(%r9,%rax), %r10
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	(%r9,%rax), %rbx
	movl	$8, %esi
	.p2align	4, 0x90
.LBB1_443:                              # =>This Inner Loop Header: Depth=1
	cmpb	$1, (%rdi)
	je	.LBB1_445
# BB#444:                               #   in Loop: Header=BB1_443 Depth=1
	movswl	-4(%rbx,%rsi), %eax
	movl	48(%r13), %ecx
	imull	%ecx, %eax
	movl	(%r15), %ebp
	cltd
	idivl	%ebp
	movw	%ax, -4(%r10,%rsi)
	movswl	-2(%rbx,%rsi), %eax
	imull	%ecx, %eax
	cltd
	idivl	%ebp
	movw	%ax, -2(%r10,%rsi)
	movswl	-6(%rbx,%rsi), %eax
	imull	%ecx, %eax
	cltd
	idivl	%ebp
	movw	%ax, -6(%r10,%rsi)
	movswl	-8(%rbx,%rsi), %eax
	imull	%ecx, %eax
	cltd
	idivl	%ebp
	movw	%ax, -8(%r10,%rsi)
	movswl	(%rbx,%rsi), %eax
	imull	%ecx, %eax
	cltd
	idivl	%ebp
	movw	%ax, (%r10,%rsi)
.LBB1_445:                              #   in Loop: Header=BB1_443 Depth=1
	incq	%rdi
	addq	$10, %rsi
	cmpq	$2568, %rsi             # imm = 0xA08
	jne	.LBB1_443
# BB#446:
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	16(%r9,%rax), %rax
	movq	%rax, 16(%r9,%r8)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movslq	32(%r9,%rax), %rax
	movl	%eax, %ebx
	movl	%eax, 32(%r9,%r8)
	movzwl	40(%r12), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	24(%r9,%rcx), %rbp
	addq	%rax, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB1_448
# BB#447:
	movl	$37, %edi
	movl	$54, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_448:                              # %.preheader
	cmpl	$2, %ebx
	jl	.LBB1_453
# BB#449:                               # %.lr.ph679.preheader
	addq	$10, %rbp
	movq	%r14, %rcx
	addq	$10, %rcx
	decq	%rbx
	.p2align	4, 0x90
.LBB1_450:                              # %.lr.ph679
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-4(%rbp), %eax
	movb	%al, -4(%rcx)
	testb	%al, %al
	je	.LBB1_452
# BB#451:                               #   in Loop: Header=BB1_450 Depth=1
	movswl	-2(%rbp), %eax
	movl	48(%r13), %esi
	imull	%esi, %eax
	movl	(%r15), %edi
	cltd
	idivl	%edi
	movw	%ax, -2(%rcx)
	movswl	(%rbp), %eax
	imull	%esi, %eax
	cltd
	idivl	%edi
	movw	%ax, (%rcx)
.LBB1_452:                              #   in Loop: Header=BB1_450 Depth=1
	addq	$6, %rbp
	addq	$6, %rcx
	decq	%rbx
	jne	.LBB1_450
.LBB1_453:                              # %._crit_edge680
	movq	finfo(%rip), %rbx
	movl	font_count(%rip), %eax
	leaq	(%rax,%rax,2), %rbp
	shlq	$5, %rbp
	movq	%r14, 24(%rbx,%rbp)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	64(%rbx,%rax), %rax
	movq	%rax, 64(%rbx,%rbp)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	72(%rbx,%rax), %rax
	movq	%rax, 72(%rbx,%rbp)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	80(%rbx,%rax), %rax
	movq	%rax, 80(%rbx,%rbp)
	movzwl	40(%r12), %eax
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	88(%rbx,%rax), %rax
	testq	%rax, %rax
	je	.LBB1_459
# BB#454:
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movzwl	(%rax), %r14d
	movswq	%r14w, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax), %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, 88(%rbx,%rbp)
	testq	%r12, %r12
	jne	.LBB1_456
# BB#455:
	movl	$37, %edi
	movl	$55, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_456:
	movw	%r14w, (%r12)
	movswl	%r14w, %eax
	cmpl	$2, %eax
	jl	.LBB1_471
# BB#457:                               # %.lr.ph677
	movl	48(%r13), %ecx
	movl	(%r15), %esi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edi
	testb	$1, %al
	jne	.LBB1_468
# BB#458:
	movq	56(%rsp), %rax          # 8-byte Reload
	movswl	2(%rax), %eax
	imull	%ecx, %eax
	cltd
	idivl	%esi
	movw	%ax, 2(%r12)
	movl	$2, %eax
	cmpq	$2, %rdi
	jne	.LBB1_469
	jmp	.LBB1_471
.LBB1_459:
	movq	$0, 88(%rbx,%rbp)
	jmp	.LBB1_471
.LBB1_460:
	movl	$4095, %eax             # imm = 0xFFF
	movq	192(%rsp), %rcx         # 8-byte Reload
	andl	12(%rcx), %eax
	je	.LBB1_472
# BB#461:
	cmpl	$160, %r9d
	je	.LBB1_473
# BB#462:
	cmpl	$159, %r9d
	jne	.LBB1_477
# BB#463:
	movq	finfo(%rip), %rcx
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	40(%rcx,%rax), %rax
	movzwl	1202(%rsp), %ebp
	addl	48(%rax), %ebp
	testw	%bp, %bp
	jg	.LBB1_412
	jmp	.LBB1_474
.LBB1_464:
	movq	136(%rsp), %r14         # 8-byte Reload
.LBB1_465:                              # %._crit_edge
	addq	$32, %r14
	cmpq	%rbx, %rbp
	je	.LBB1_467
# BB#466:
	movl	$37, %edi
	movl	$45, %esi
	movl	$.L.str.27, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	jmp	.LBB1_475
.LBB1_467:
	movq	%r15, (%rsp)
	movl	$37, %edi
	movl	$46, %esi
	movl	$.L.str.28, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r12, %r9
	jmp	.LBB1_475
.LBB1_468:
	movl	$1, %eax
	cmpq	$2, %rdi
	je	.LBB1_471
.LBB1_469:                              # %.lr.ph677.new
	subq	%rax, %rdi
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	2(%rdx,%rax,2), %rbp
	leaq	2(%r12,%rax,2), %rbx
.LBB1_470:                              # =>This Inner Loop Header: Depth=1
	movswl	-2(%rbp), %eax
	imull	%ecx, %eax
	cltd
	idivl	%esi
	movw	%ax, -2(%rbx)
	movswl	(%rbp), %eax
	imull	%ecx, %eax
	cltd
	idivl	%esi
	movw	%ax, (%rbx)
	addq	$4, %rbp
	addq	$4, %rbx
	addq	$-2, %rdi
	jne	.LBB1_470
.LBB1_471:                              # %.loopexit
	movl	$4095, %eax             # imm = 0xFFF
	andl	font_count(%rip), %eax
	movl	$-4096, %ecx            # imm = 0xF000
	movq	192(%rsp), %rdx         # 8-byte Reload
	andl	12(%rdx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 12(%rdx)
	movb	$38, 5(%rdx)
	movzwl	56(%r13), %eax
	movw	%ax, 6(%rdx)
	jmp	.LBB1_476
.LBB1_472:
	leaq	32(%r13), %r8
	addq	$64, %r13
	movl	$37, %edi
	movl	$48, %esi
	movl	$.L.str.33, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r13, %r9
	callq	Error
	jmp	.LBB1_478
.LBB1_473:
	movq	finfo(%rip), %rcx
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	40(%rcx,%rax), %rax
	movl	48(%rax), %ebp
	movzwl	1202(%rsp), %eax
	subl	%eax, %ebp
	testw	%bp, %bp
	jg	.LBB1_412
.LBB1_474:
	leaq	32(%rbx), %r8
	addq	$64, %rbx
	movq	$.L.str.16, (%rsp)
	movl	$37, %edi
	movl	$50, %esi
	movl	$.L.str.35, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	jmp	.LBB1_475
.LBB1_477:
	movl	$37, %edi
	movl	$49, %esi
	movl	$.L.str.34, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_478:                              # %.thread600
                                        # implicit-def: %BP
	jmp	.LBB1_412
.LBB1_479:
	movl	$37, %edi
	movl	$40, %esi
	jmp	.LBB1_50
.LBB1_480:
	movq	no_fpos(%rip), %r8
	leaq	464(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$37, %edi
	movl	$11, %esi
	movl	$.L.str.75, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	subq	$8, %rsp
.Lfunc_end1:
	.size	FontChange, .Lfunc_end1-FontChange
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_8
	.quad	.LBB1_18
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_18
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_49
	.quad	.LBB1_10
	.quad	.LBB1_10
.LJTI1_1:
	.quad	.LBB1_265
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_275
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_277
	.quad	.LBB1_336
	.quad	.LBB1_319
	.quad	.LBB1_336
	.quad	.LBB1_336
	.quad	.LBB1_324

	.text
	.globl	FontWordSize
	.p2align	4, 0x90
	.type	FontWordSize,@function
FontWordSize:                           # @FontWordSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	subq	$584, %rsp              # imm = 0x248
.Lcfi69:
	.cfi_def_cfa_offset 640
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.40, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:
	cmpb	$0, 64(%rbp)
	je	.LBB2_45
# BB#3:
	leaq	32(%rbp), %r14
	leaq	64(%rbp), %rbx
	movl	40(%rbp), %eax
	movl	%eax, %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	decl	%ecx
	cmpl	font_count(%rip), %ecx
	jb	.LBB2_5
# BB#4:
	movl	$37, %edi
	movl	$56, %esi
	movl	$.L.str.41, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
	movl	40(%rbp), %eax
.LBB2_5:
	testl	$4190208, %eax          # imm = 0x3FF000
	jne	.LBB2_8
# BB#6:
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 44(%rcx)
	je	.LBB2_8
# BB#7:
	movl	$37, %edi
	movl	$57, %esi
	movl	$.L.str.42, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
	movl	40(%rbp), %eax
.LBB2_8:
	testl	$528482304, %eax        # imm = 0x1F800000
	jne	.LBB2_10
# BB#9:
	movl	$37, %edi
	movl	$58, %esi
	movl	$.L.str.43, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
	movl	40(%rbp), %eax
.LBB2_10:                               # %._crit_edge219
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	64(%rsp), %r12
	movq	finfo(%rip), %rcx
	andl	$4095, %eax             # imm = 0xFFF
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	movq	(%rcx,%rax), %r9
	movq	8(%rcx,%rax), %r10
	movq	40(%rcx,%rax), %rax
	movzbl	60(%rax), %eax
	andl	$127, %eax
	movq	MapTable(,%rax,8), %r11
	movb	(%rbx), %al
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_21 Depth 2
                                        #       Child Loop BB2_24 Depth 3
                                        #       Child Loop BB2_30 Depth 3
	movl	%edx, %esi
	leaq	1(%rbx), %r13
	movb	%al, (%r12)
	movzbl	%al, %ecx
	movb	(%r10,%rcx), %cl
	testb	%cl, %cl
	je	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_11 Depth=1
	cmpb	$1, %cl
	jne	.LBB2_18
# BB#14:                                #   in Loop: Header=BB2_11 Depth=1
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	$12, %edi
	movl	$.L.str.44, %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%rax, %rbp
	movb	(%r12), %al
	movb	%al, 64(%rbp)
	movzbl	(%r12), %eax
	movzbl	2945(%r11,%rax), %ecx
	cmpb	%al, %cl
	jne	.LBB2_15
# BB#16:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rbp, %rdi
	callq	StringQuotedWord
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	40(%rax), %edi
	andl	$4095, %edi             # imm = 0xFFF
	callq	FontFamilyAndFace
	movq	$FontFamilyAndFace.buff, (%rsp)
	movl	$37, %edi
	movl	$60, %esi
	movl	$.L.str.45, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	Error
	movq	40(%rsp), %r11          # 8-byte Reload
	movb	$32, (%r12)
	movb	$32, %al
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_11 Depth=1
	movl	%eax, %edx
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_18:                               #   in Loop: Header=BB2_11 Depth=1
	movzbl	(%rbx), %r8d
	movzbl	(%r10,%r8), %edx
	cmpb	%r8b, 256(%r10,%rdx)
	jne	.LBB2_19
# BB#20:                                # %.preheader189.lr.ph
                                        #   in Loop: Header=BB2_11 Depth=1
	movl	%esi, 12(%rsp)          # 4-byte Spill
	leaq	256(%r10,%rdx), %rsi
	movb	(%r13), %bl
	.p2align	4, 0x90
.LBB2_21:                               # %.preheader189
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_24 Depth 3
                                        #       Child Loop BB2_30 Depth 3
	leaq	1(%rsi), %rbp
	movb	1(%rsi), %dl
	cmpb	%bl, %dl
	jne	.LBB2_22
# BB#23:                                # %.lr.ph200.preheader
                                        #   in Loop: Header=BB2_21 Depth=2
	movl	%ebx, %edx
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB2_24:                               # %.lr.ph200
                                        #   Parent Loop BB2_11 Depth=1
                                        #     Parent Loop BB2_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	%dl, %dl
	je	.LBB2_27
# BB#25:                                # %.lr.ph200
                                        #   in Loop: Header=BB2_24 Depth=3
	movzbl	1(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_24 Depth=3
	movzbl	1(%rbp), %edx
	incq	%rbp
	cmpb	1(%rdi), %dl
	leaq	1(%rdi), %rdi
	je	.LBB2_24
.LBB2_27:                               # %.lr.ph200..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB2_21 Depth=2
	leaq	-1(%rbp), %rsi
	cmpb	$0, 2(%rsi)
	jne	.LBB2_29
	jmp	.LBB2_47
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_21 Depth=2
	movq	%r13, %rdi
	cmpb	$0, 2(%rsi)
	je	.LBB2_47
.LBB2_29:                               # %.preheader.preheader
                                        #   in Loop: Header=BB2_21 Depth=2
	incq	%rbp
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB2_30:                               # %.preheader
                                        #   Parent Loop BB2_11 Depth=1
                                        #     Parent Loop BB2_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rsi
	jne	.LBB2_30
# BB#31:                                #   in Loop: Header=BB2_21 Depth=2
	cmpb	%r8b, (%rsi)
	je	.LBB2_21
# BB#32:                                #   in Loop: Header=BB2_11 Depth=1
	movl	%eax, %edx
	jmp	.LBB2_33
.LBB2_15:                               #   in Loop: Header=BB2_11 Depth=1
	leaq	(%rcx,%rcx,4), %rcx
	movq	32(%rsp), %r9           # 8-byte Reload
	movzwl	(%r9,%rcx,2), %ecx
	leaq	(%rax,%rax,4), %rax
	movw	%cx, (%r9,%rax,2)
	movzbl	(%r12), %eax
	movzbl	2945(%r11,%rax), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movzwl	2(%r9,%rcx,2), %ecx
	leaq	(%rax,%rax,4), %rax
	movw	%cx, 2(%r9,%rax,2)
	movzbl	(%r12), %eax
	movzbl	2945(%r11,%rax), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movzwl	4(%r9,%rcx,2), %ecx
	leaq	(%rax,%rax,4), %rax
	movw	%cx, 4(%r9,%rax,2)
	movzbl	(%r12), %eax
	movzbl	2945(%r11,%rax), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movzwl	6(%r9,%rcx,2), %ecx
	leaq	(%rax,%rax,4), %rax
	movw	%cx, 6(%r9,%rax,2)
	movzbl	(%r12), %eax
	movzbl	2945(%r11,%rax), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movzwl	8(%r9,%rcx,2), %ecx
	leaq	(%rax,%rax,4), %rax
	movw	%cx, 8(%r9,%rax,2)
	movzbl	(%r12), %ebx
	movq	24(%rsp), %r10          # 8-byte Reload
	addq	%r10, %rbx
	xorl	%eax, %eax
.LBB2_17:                               #   in Loop: Header=BB2_11 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	movb	%al, (%rbx)
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbp, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movb	(%r12), %dl
	jmp	.LBB2_34
.LBB2_19:                               #   in Loop: Header=BB2_11 Depth=1
	movl	%eax, %edx
	jmp	.LBB2_34
.LBB2_47:                               #   in Loop: Header=BB2_11 Depth=1
	movb	%dl, (%r12)
	movq	%rdi, %r13
.LBB2_33:                               # %.loopexit
                                        #   in Loop: Header=BB2_11 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	.p2align	4, 0x90
.LBB2_34:                               # %.loopexit
                                        #   in Loop: Header=BB2_11 Depth=1
	movzbl	%dl, %eax
	leaq	(%rax,%rax,4), %rcx
	movswl	(%r9,%rcx,2), %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movswl	2(%r9,%rcx,2), %eax
	cmpl	%r14d, %eax
	cmovlel	%eax, %r14d
	incq	%r12
	movswl	6(%r9,%rcx,2), %edx
	addl	%esi, %edx
	movb	(%r13), %al
	testb	%al, %al
	movq	%r13, %rbx
	jne	.LBB2_11
# BB#35:
	movb	$0, (%r12)
	movswl	8(%r9,%rcx,2), %eax
	addl	%edx, %eax
	movb	65(%rsp), %bl
	testb	%bl, %bl
	je	.LBB2_36
# BB#37:                                # %.lr.ph
	leaq	65(%rsp), %rcx
	leaq	64(%rsp), %rdx
	movq	finfo(%rip), %rsi
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	40(%r10), %edi
	andl	$4095, %edi             # imm = 0xFFF
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$5, %rdi
	movq	64(%rsi,%rdi), %r8
	leaq	72(%rsi,%rdi), %r9
	.p2align	4, 0x90
.LBB2_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_40 Depth 2
	movzbl	(%rdx), %esi
	movzbl	2945(%r11,%rsi), %esi
	movzwl	(%r8,%rsi,2), %esi
	xorl	%ebp, %ebp
	testq	%rsi, %rsi
	je	.LBB2_43
# BB#39:                                #   in Loop: Header=BB2_38 Depth=1
	movzbl	%bl, %edi
	movb	2945(%r11,%rdi), %bl
	movq	(%r9), %rdi
	.p2align	4, 0x90
.LBB2_40:                               #   Parent Loop BB2_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%bl, (%rdi,%rsi)
	leaq	1(%rsi), %rsi
	ja	.LBB2_40
# BB#41:                                #   in Loop: Header=BB2_38 Depth=1
	jne	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_38 Depth=1
	movq	16(%r9), %rdi
	movq	8(%r9), %rbp
	movzbl	-1(%rbp,%rsi), %esi
	movswl	(%rdi,%rsi,2), %ebp
.LBB2_43:                               #   in Loop: Header=BB2_38 Depth=1
	addl	%ebp, %eax
	incq	%rdx
	movb	1(%rcx), %bl
	incq	%rcx
	testb	%bl, %bl
	jne	.LBB2_38
	jmp	.LBB2_44
.LBB2_45:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbp)
	jmp	.LBB2_46
.LBB2_36:
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB2_44:                               # %._crit_edge
	movl	$0, 48(%r10)
	movl	%eax, 56(%r10)
	movl	%r15d, 52(%r10)
	negl	%r14d
	movl	%r14d, 60(%r10)
.LBB2_46:
	addq	$584, %rsp              # imm = 0x248
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	FontWordSize, .Lfunc_end2-FontWordSize
	.cfi_endproc

	.globl	FontFamilyAndFace
	.p2align	4, 0x90
	.type	FontFamilyAndFace,@function
FontFamilyAndFace:                      # @FontFamilyAndFace
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 48
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.50, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movq	24(%rax), %r14
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB3_3
# BB#4:
	movq	24(%r14), %rbx
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB3_5
# BB#6:
	addq	$64, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	addq	$64, %r14
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%r15,%rax), %rax
	cmpq	$81, %rax
	jb	.LBB3_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movq	%r14, (%rsp)
	movl	$37, %edi
	movl	$63, %esi
	movl	$.L.str.52, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	callq	Error
.LBB3_8:
	movl	$FontFamilyAndFace.buff, %edi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$FontFamilyAndFace.buff, %edi
	callq	strlen
	movw	$32, FontFamilyAndFace.buff(%rax)
	movl	$FontFamilyAndFace.buff, %edi
	movq	%r14, %rsi
	callq	strcat
	movl	$FontFamilyAndFace.buff, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	FontFamilyAndFace, .Lfunc_end3-FontFamilyAndFace
	.cfi_endproc

	.globl	FontSize
	.p2align	4, 0x90
	.type	FontSize,@function
FontSize:                               # @FontSize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 16
.Lcfi84:
	.cfi_offset %rbx, -16
	movq	%rsi, %r8
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB4_2
# BB#1:                                 # %.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.46, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB4_4
.LBB4_2:
	testl	%ebx, %ebx
	jne	.LBB4_4
# BB#3:
	addq	$32, %r8
	movl	$37, %edi
	movl	$61, %esi
	movl	$.L.str.47, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_4:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movl	48(%rax), %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	FontSize, .Lfunc_end4-FontSize
	.cfi_endproc

	.globl	FontHalfXHeight
	.p2align	4, 0x90
	.type	FontHalfXHeight,@function
FontHalfXHeight:                        # @FontHalfXHeight
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 16
.Lcfi86:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB5_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.48, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_2:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movl	52(%rax), %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	FontHalfXHeight, .Lfunc_end5-FontHalfXHeight
	.cfi_endproc

	.globl	FontMapping
	.p2align	4, 0x90
	.type	FontMapping,@function
FontMapping:                            # @FontMapping
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 16
.Lcfi88:
	.cfi_offset %rbx, -16
	movq	%rsi, %r8
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB6_2
# BB#1:                                 # %.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.49, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB6_4
.LBB6_2:
	testl	%ebx, %ebx
	jne	.LBB6_4
# BB#3:
	movl	$37, %edi
	movl	$62, %esi
	movl	$.L.str.47, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB6_4:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movzbl	60(%rax), %eax
	andl	$127, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	FontMapping, .Lfunc_end6-FontMapping
	.cfi_endproc

	.globl	FontName
	.p2align	4, 0x90
	.type	FontName,@function
FontName:                               # @FontName
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB7_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.50, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_2:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	addq	$64, %rax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	FontName, .Lfunc_end7-FontName
	.cfi_endproc

	.globl	FontFamily
	.p2align	4, 0x90
	.type	FontFamily,@function
FontFamily:                             # @FontFamily
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 16
.Lcfi92:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB8_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_2:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movq	24(%rax), %rax
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB8_3
# BB#4:
	movq	24(%rax), %rax
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB8_5
# BB#6:
	addq	$64, %rax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	FontFamily, .Lfunc_end8-FontFamily
	.cfi_endproc

	.globl	FontFace
	.p2align	4, 0x90
	.type	FontFace,@function
FontFace:                               # @FontFace
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 16
.Lcfi94:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpl	%ebx, font_count(%rip)
	jae	.LBB9_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB9_2:
	movq	finfo(%rip), %rax
	movl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rax
	movq	24(%rax), %rax
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB9_3
# BB#4:
	movq	24(%rax), %rcx
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB9_5
# BB#6:
	addq	$64, %rax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	FontFace, .Lfunc_end9-FontFace
	.cfi_endproc

	.globl	FontPrintAll
	.p2align	4, 0x90
	.type	FontPrintAll,@function
FontPrintAll:                           # @FontPrintAll
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 64
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	font_root(%rip), %rax
	testq	%rax, %rax
	je	.LBB10_2
# BB#1:
	cmpb	$17, 32(%rax)
	je	.LBB10_3
.LBB10_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	font_root(%rip), %rax
.LBB10_3:
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB10_29
	.p2align	4, 0x90
.LBB10_4:                               # %.preheader63
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #     Child Loop BB10_10 Depth 2
                                        #       Child Loop BB10_11 Depth 3
                                        #       Child Loop BB10_18 Depth 3
                                        #       Child Loop BB10_22 Depth 3
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB10_5:                               #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB10_5
# BB#6:                                 #   in Loop: Header=BB10_4 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB10_8:                               # %.preheader62
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	8(%r13), %r12
	cmpq	%r13, %r12
	jne	.LBB10_10
	jmp	.LBB10_28
	.p2align	4, 0x90
.LBB10_30:                              #   in Loop: Header=BB10_10 Depth=2
	addq	$64, %rbx
	addq	$64, %r15
	movl	$.L.str.61, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	fprintf
	movq	8(%r12), %r12
	cmpq	%r13, %r12
	je	.LBB10_28
.LBB10_10:                              # %.preheader
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_11 Depth 3
                                        #       Child Loop BB10_18 Depth 3
                                        #       Child Loop BB10_22 Depth 3
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB10_11:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB10_11
# BB#12:                                #   in Loop: Header=BB10_10 Depth=2
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB10_14
# BB#13:                                #   in Loop: Header=BB10_10 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.55, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB10_14:                              # %.loopexit61
                                        #   in Loop: Header=BB10_10 Depth=2
	leaq	8(%rbp), %rbx
	movq	8(%rbp), %r15
	cmpq	%rbp, %r15
	je	.LBB10_17
# BB#15:                                #   in Loop: Header=BB10_10 Depth=2
	movq	8(%r15), %rax
	cmpq	%rbp, %rax
	je	.LBB10_17
# BB#16:                                #   in Loop: Header=BB10_10 Depth=2
	cmpq	%rbp, 8(%rax)
	jne	.LBB10_18
	.p2align	4, 0x90
.LBB10_17:                              #   in Loop: Header=BB10_10 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %r15
	.p2align	4, 0x90
.LBB10_18:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB10_18
# BB#19:                                #   in Loop: Header=BB10_10 Depth=2
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB10_21
# BB#20:                                #   in Loop: Header=BB10_10 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.57, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB10_21:                              # %.loopexit60
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB10_22:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB10_22
# BB#23:                                #   in Loop: Header=BB10_10 Depth=2
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB10_25
# BB#24:                                #   in Loop: Header=BB10_10 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.58, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB10_25:                              # %.loopexit
                                        #   in Loop: Header=BB10_10 Depth=2
	movb	60(%rbp), %al
	testb	%al, %al
	jns	.LBB10_30
# BB#26:                                #   in Loop: Header=BB10_10 Depth=2
	addq	$64, %r15
	addq	$64, %rbx
	andb	$127, %al
	movzbl	%al, %edi
	callq	MapEncodingName
	movq	%rax, %r8
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r15, %r9
	callq	fprintf
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	fprintf
	movq	8(%r12), %r12
	cmpq	%r13, %r12
	jne	.LBB10_10
.LBB10_28:                              # %._crit_edge
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rcx), %rcx
	cmpq	font_root(%rip), %rcx
	jne	.LBB10_4
.LBB10_29:                              # %._crit_edge77
	movl	$10, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.Lfunc_end10:
	.size	FontPrintAll, .Lfunc_end10-FontPrintAll
	.cfi_endproc

	.globl	FontPrintPageSetup
	.p2align	4, 0x90
	.type	FontPrintPageSetup,@function
FontPrintPageSetup:                     # @FontPrintPageSetup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 48
.Lcfi113:
	.cfi_offset %rbx, -40
.Lcfi114:
	.cfi_offset %r12, -32
.Lcfi115:
	.cfi_offset %r14, -24
.Lcfi116:
	.cfi_offset %r15, -16
	movq	font_root(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_2
# BB#1:
	cmpb	$17, 32(%rax)
	je	.LBB11_3
.LBB11_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB11_3:
	movq	font_used(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_5
# BB#4:
	cmpb	$17, 32(%rax)
	je	.LBB11_6
.LBB11_5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.63, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	font_used(%rip), %rax
.LBB11_6:
	movq	8(%rax), %r12
	cmpq	%rax, %r12
	je	.LBB11_22
	.p2align	4, 0x90
.LBB11_7:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_8 Depth 2
                                        #     Child Loop BB11_14 Depth 2
                                        #     Child Loop BB11_18 Depth 2
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB11_8:                               #   Parent Loop BB11_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB11_8
# BB#9:                                 #   in Loop: Header=BB11_7 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.64, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB11_11:                              # %.loopexit33
                                        #   in Loop: Header=BB11_7 Depth=1
	leaq	8(%r14), %rbx
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB11_13
# BB#12:                                #   in Loop: Header=BB11_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %rax
.LBB11_13:                              #   in Loop: Header=BB11_7 Depth=1
	movq	8(%rax), %rax
	movq	8(%rax), %r15
	.p2align	4, 0x90
.LBB11_14:                              #   Parent Loop BB11_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB11_14
# BB#15:                                #   in Loop: Header=BB11_7 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.65, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB11_17:                              # %.loopexit32
                                        #   in Loop: Header=BB11_7 Depth=1
	movq	(%rbx), %rbx
	.p2align	4, 0x90
.LBB11_18:                              #   Parent Loop BB11_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB11_18
# BB#19:                                #   in Loop: Header=BB11_7 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB11_21
# BB#20:                                #   in Loop: Header=BB11_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.66, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB11_21:                              # %.loopexit
                                        #   in Loop: Header=BB11_7 Depth=1
	movq	BackEnd(%rip), %rax
	movl	font_curr_page(%rip), %esi
	addq	$64, %rbx
	addq	$64, %r15
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	*64(%rax)
	movq	8(%r12), %r12
	cmpq	font_used(%rip), %r12
	jne	.LBB11_7
.LBB11_22:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	FontPrintPageSetup, .Lfunc_end11-FontPrintPageSetup
	.cfi_endproc

	.globl	FontPrintPageResources
	.p2align	4, 0x90
	.type	FontPrintPageResources,@function
FontPrintPageResources:                 # @FontPrintPageResources
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 48
.Lcfi122:
	.cfi_offset %rbx, -48
.Lcfi123:
	.cfi_offset %r12, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movq	font_root(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_2
# BB#1:
	cmpb	$17, 32(%rax)
	je	.LBB12_3
.LBB12_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB12_3:
	movq	font_used(%rip), %rax
	testq	%rax, %rax
	je	.LBB12_5
# BB#4:
	cmpb	$17, 32(%rax)
	je	.LBB12_6
.LBB12_5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.63, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	font_used(%rip), %rax
.LBB12_6:
	movq	8(%rax), %r12
	cmpq	%rax, %r12
	je	.LBB12_27
# BB#7:                                 # %.preheader.preheader
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB12_8:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_9 Depth 2
                                        #     Child Loop BB12_14 Depth 2
                                        #     Child Loop BB12_20 Depth 2
                                        #       Child Loop BB12_21 Depth 3
                                        #       Child Loop BB12_23 Depth 3
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB12_9:                               #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB12_9
# BB#10:                                #   in Loop: Header=BB12_8 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB12_12
# BB#11:                                #   in Loop: Header=BB12_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.67, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB12_12:                              # %.loopexit53
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB12_14
# BB#13:                                #   in Loop: Header=BB12_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB12_14:                              # %.preheader78
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB12_14
# BB#15:                                # %.preheader78
                                        #   in Loop: Header=BB12_8 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB12_17
# BB#16:                                #   in Loop: Header=BB12_8 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.68, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB12_17:                              # %.loopexit
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	font_used(%rip), %r15
	movq	8(%r15), %rbp
	addq	$64, %rbx
	cmpq	%r12, %rbp
	jne	.LBB12_20
	jmp	.LBB12_25
	.p2align	4, 0x90
.LBB12_18:                              #   in Loop: Header=BB12_20 Depth=2
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	je	.LBB12_25
.LBB12_20:                              # %.lr.ph
                                        #   Parent Loop BB12_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_21 Depth 3
                                        #       Child Loop BB12_23 Depth 3
	leaq	16(%rbp), %rax
	.p2align	4, 0x90
.LBB12_21:                              #   Parent Loop BB12_8 Depth=1
                                        #     Parent Loop BB12_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB12_21
# BB#22:                                #   in Loop: Header=BB12_20 Depth=2
	movq	8(%rcx), %rdi
	.p2align	4, 0x90
.LBB12_23:                              #   Parent Loop BB12_8 Depth=1
                                        #     Parent Loop BB12_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB12_23
# BB#24:                                #   in Loop: Header=BB12_20 Depth=2
	addq	$64, %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB12_18
	jmp	.LBB12_26
	.p2align	4, 0x90
.LBB12_25:                              # %.thread
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	BackEnd(%rip), %rax
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	*72(%rax)
	xorl	%r14d, %r14d
	movq	font_used(%rip), %r15
.LBB12_26:                              # %.loopexit77
                                        #   in Loop: Header=BB12_8 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB12_8
.LBB12_27:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	FontPrintPageResources, .Lfunc_end12-FontPrintPageResources
	.cfi_endproc

	.globl	FontAdvanceCurrentPage
	.p2align	4, 0x90
	.type	FontAdvanceCurrentPage,@function
FontAdvanceCurrentPage:                 # @FontAdvanceCurrentPage
	.cfi_startproc
# BB#0:
	movq	font_used(%rip), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB13_7
	.p2align	4, 0x90
.LBB13_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB13_3
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB13_3:                               #   in Loop: Header=BB13_1 Depth=1
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB13_5:                               #   in Loop: Header=BB13_1 Depth=1
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB13_1
# BB#6:                                 # %._crit_edge
	movl	%edx, zz_size(%rip)
.LBB13_7:
	incl	font_curr_page(%rip)
	retq
.Lfunc_end13:
	.size	FontAdvanceCurrentPage, .Lfunc_end13-FontAdvanceCurrentPage
	.cfi_endproc

	.globl	FontPageUsed
	.p2align	4, 0x90
	.type	FontPageUsed,@function
FontPageUsed:                           # @FontPageUsed
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 16
.Lcfi128:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	42(%rbx), %eax
	cmpl	font_curr_page(%rip), %eax
	jl	.LBB14_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.69, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB14_2:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB14_3
# BB#4:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB14_5
.LBB14_3:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB14_5:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	font_used(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB14_8
# BB#6:
	testq	%rcx, %rcx
	je	.LBB14_8
# BB#7:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB14_8:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB14_10
# BB#9:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB14_10:
	movzwl	font_curr_page(%rip), %eax
	movw	%ax, 42(%rbx)
	popq	%rbx
	retq
.Lfunc_end14:
	.size	FontPageUsed, .Lfunc_end14-FontPageUsed
	.cfi_endproc

	.globl	FontNeeded
	.p2align	4, 0x90
	.type	FontNeeded,@function
FontNeeded:                             # @FontNeeded
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 64
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	font_root(%rip), %rax
	movq	8(%rax), %rcx
	movl	$1, %r13d
	cmpq	%rax, %rcx
	je	.LBB15_15
# BB#1:                                 # %.preheader35.preheader
	movl	$.L.str.71, %r12d
	.p2align	4, 0x90
.LBB15_2:                               # %.preheader35
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_3 Depth 2
                                        #     Child Loop BB15_6 Depth 2
                                        #       Child Loop BB15_7 Depth 3
                                        #       Child Loop BB15_9 Depth 3
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB15_3:                               #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB15_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	8(%rbp), %r15
	cmpq	%rbp, %r15
	je	.LBB15_14
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%rcx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_7 Depth 3
                                        #       Child Loop BB15_9 Depth 3
	leaq	16(%r15), %rax
	.p2align	4, 0x90
.LBB15_7:                               #   Parent Loop BB15_2 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB15_7
# BB#8:                                 #   in Loop: Header=BB15_6 Depth=2
	movq	8(%rcx), %rbx
	.p2align	4, 0x90
.LBB15_9:                               #   Parent Loop BB15_2 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB15_9
# BB#10:                                #   in Loop: Header=BB15_6 Depth=2
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB15_12
# BB#11:                                #   in Loop: Header=BB15_6 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.68, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB15_12:                              # %.loopexit
                                        #   in Loop: Header=BB15_6 Depth=2
	testl	%r13d, %r13d
	movl	$.L.str.72, %edx
	cmovneq	%r12, %rdx
	addq	$64, %rbx
	xorl	%r13d, %r13d
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	fprintf
	movq	8(%r15), %r15
	cmpq	%rbp, %r15
	jne	.LBB15_6
# BB#13:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB15_2 Depth=1
	xorl	%r13d, %r13d
	movq	font_root(%rip), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
.LBB15_14:                              # %._crit_edge
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.LBB15_2
.LBB15_15:                              # %._crit_edge45
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	FontNeeded, .Lfunc_end15-FontNeeded
	.cfi_endproc

	.p2align	4, 0x90
	.type	ReadCharMetrics,@function
ReadCharMetrics:                        # @ReadCharMetrics
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Lcfi148:
	.cfi_def_cfa_offset 1216
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movq	%r8, %rbx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movl	%esi, 84(%rsp)          # 4-byte Spill
	movq	1232(%rsp), %r13
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB16_1
# BB#2:                                 # %.preheader276
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	128(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB16_64
# BB#3:                                 # %.lr.ph300
	addq	$32, 32(%rsp)           # 8-byte Folded Spill
	leaq	128(%rsp), %rbp
	movabsq	$4294967296, %r14       # imm = 0x100000000
	leaq	68(%rsp), %r15
                                        # implicit-def: %EAX
	movl	%eax, 60(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 56(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 52(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 48(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB16_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_7 Depth 2
                                        #     Child Loop BB16_11 Depth 2
                                        #       Child Loop BB16_28 Depth 3
                                        #       Child Loop BB16_29 Depth 3
                                        #         Child Loop BB16_37 Depth 4
                                        #         Child Loop BB16_40 Depth 4
                                        #       Child Loop BB16_43 Depth 3
                                        #       Child Loop BB16_47 Depth 3
	movl	$.L.str.131, %esi
	movq	%rbp, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB16_64
# BB#5:                                 #   in Loop: Header=BB16_4 Depth=1
	movl	$.L.str.132, %esi
	movq	%rbp, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB16_64
# BB#6:                                 #   in Loop: Header=BB16_4 Depth=1
	movq	1224(%rsp), %rax
	incl	(%rax)
	movq	$-1, %r12
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB16_7:                               #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r14, %rax
	cmpb	$32, 129(%rsp,%r12)
	leaq	1(%r12), %r12
	je	.LBB16_7
# BB#8:                                 # %.preheader275
                                        #   in Loop: Header=BB16_4 Depth=1
	sarq	$32, %rax
	cmpb	$10, 128(%rsp,%rax)
	jne	.LBB16_10
# BB#9:                                 #   in Loop: Header=BB16_4 Depth=1
	leaq	128(%rsp), %rbp
	jmp	.LBB16_63
	.p2align	4, 0x90
.LBB16_10:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	leaq	128(%rsp,%rax), %rbp
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	xorl	%ebx, %ebx
	jmp	.LBB16_11
.LBB16_21:                              # %.thread325
                                        #   in Loop: Header=BB16_11 Depth=2
	cmpb	$76, %al
	jne	.LBB16_42
# BB#22:                                # %.thread325
                                        #   in Loop: Header=BB16_11 Depth=2
	movb	641(%rsp), %al
	testb	%al, %al
	jne	.LBB16_42
# BB#23:                                #   in Loop: Header=BB16_11 Depth=2
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	je	.LBB16_42
# BB#24:                                #   in Loop: Header=BB16_11 Depth=2
	movq	BackEnd(%rip), %rax
	movl	40(%rax), %eax
	testl	%eax, %eax
	je	.LBB16_42
# BB#25:                                #   in Loop: Header=BB16_11 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	movzbl	%dl, %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	cmpb	$1, (%rsi,%rax)
	jne	.LBB16_27
# BB#26:                                #   in Loop: Header=BB16_11 Depth=2
	movb	(%rbp), %al
	movq	112(%rsp), %rcx         # 8-byte Reload
	movb	%al, (%rcx)
.LBB16_27:                              #   in Loop: Header=BB16_11 Depth=2
	movl	%ebx, 92(%rsp)          # 4-byte Spill
	movslq	(%rbp), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbp)
	movb	%dl, (%rsi,%rax)
	movslq	%r12d, %r12
	.p2align	4, 0x90
.LBB16_28:                              #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$32, 129(%rsp,%r12)
	leaq	1(%r12), %r12
	je	.LBB16_28
	jmp	.LBB16_29
	.p2align	4, 0x90
.LBB16_31:                              #   in Loop: Header=BB16_29 Depth=3
	leaq	128(%rsp,%rbx), %r13
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	640(%rsp), %r15
	movq	%r15, %rdx
	callq	sscanf
	movq	120(%rsp), %rax         # 8-byte Reload
	movzbl	60(%rax), %esi
	andl	$127, %esi
	movq	%r15, %rdi
	callq	MapCharEncoding
	testb	%al, %al
	je	.LBB16_33
# BB#32:                                #   in Loop: Header=BB16_29 Depth=3
	movslq	(%rbp), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%rbp)
	addq	24(%rsp), %rcx          # 8-byte Folded Reload
	jmp	.LBB16_34
	.p2align	4, 0x90
.LBB16_33:                              #   in Loop: Header=BB16_29 Depth=3
	movzwl	20(%rsp), %edi          # 2-byte Folded Reload
	callq	FileName
	movq	1224(%rsp), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 8(%rsp)
	movq	%rax, (%rsp)
	movl	$37, %edi
	movl	$1, %esi
	movl	$.L.str.140, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%r15, %r9
	callq	Error
	movb	$1, %al
	movq	112(%rsp), %rcx         # 8-byte Reload
.LBB16_34:                              #   in Loop: Header=BB16_29 Depth=3
	movb	%al, (%rcx)
	cmpl	$508, (%rbp)            # imm = 0x1FC
	jl	.LBB16_36
# BB#35:                                #   in Loop: Header=BB16_29 Depth=3
	movzwl	20(%rsp), %edi          # 2-byte Folded Reload
	callq	FileName
	movq	%rax, %rbp
	movq	1224(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$2, %esi
	movl	$.L.str.141, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	movq	72(%rsp), %rbp          # 8-byte Reload
	callq	Error
.LBB16_36:                              # %.preheader.preheader
                                        #   in Loop: Header=BB16_29 Depth=3
	shlq	$32, %rbx
	jmp	.LBB16_37
	.p2align	4, 0x90
.LBB16_65:                              #   in Loop: Header=BB16_37 Depth=4
	addq	%r14, %rbx
	incq	%r13
	incl	%r12d
.LBB16_37:                              # %.preheader
                                        #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        #       Parent Loop BB16_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r13), %eax
	cmpb	$59, %al
	je	.LBB16_39
# BB#38:                                # %.preheader
                                        #   in Loop: Header=BB16_37 Depth=4
	cmpb	$32, %al
	jne	.LBB16_65
.LBB16_39:                              # %.critedge27.preheader
                                        #   in Loop: Header=BB16_29 Depth=3
	movslq	%r12d, %r12
	decq	%r12
	.p2align	4, 0x90
.LBB16_40:                              # %.critedge27
                                        #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        #       Parent Loop BB16_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$32, 129(%rsp,%r12)
	leaq	1(%r12), %r12
	je	.LBB16_40
.LBB16_29:                              # %.preheader274
                                        #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB16_37 Depth 4
                                        #         Child Loop BB16_40 Depth 4
	movslq	%r12d, %rbx
	movb	128(%rsp,%rbx), %al
	cmpb	$10, %al
	je	.LBB16_41
# BB#30:                                # %.preheader274
                                        #   in Loop: Header=BB16_29 Depth=3
	cmpb	$59, %al
	jne	.LBB16_31
.LBB16_41:                              # %.critedge25
                                        #   in Loop: Header=BB16_11 Depth=2
	movslq	(%rbp), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbp)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movb	$0, (%rcx,%rax)
	movq	1232(%rsp), %r13
	leaq	68(%rsp), %r15
	movl	92(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB16_42
	.p2align	4, 0x90
.LBB16_11:                              # %.lr.ph
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_28 Depth 3
                                        #       Child Loop BB16_29 Depth 3
                                        #         Child Loop BB16_37 Depth 4
                                        #         Child Loop BB16_40 Depth 4
                                        #       Child Loop BB16_43 Depth 3
                                        #       Child Loop BB16_47 Depth 3
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	640(%rsp), %rdx
	callq	sscanf
	movb	640(%rsp), %al
	cmpb	$78, %al
	jne	.LBB16_14
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB16_11 Depth=2
	movb	641(%rsp), %cl
	testb	%cl, %cl
	jne	.LBB16_14
# BB#13:                                #   in Loop: Header=BB16_11 Depth=2
	movl	$.L.str.134, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	640(%rsp), %rbp
	movq	%rbp, %rdx
	callq	sscanf
	movq	120(%rsp), %rax         # 8-byte Reload
	movzbl	60(%rax), %esi
	andl	$127, %esi
	movq	%rbp, %rdi
	callq	MapCharEncoding
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB16_42
	.p2align	4, 0x90
.LBB16_14:                              # %.thread
                                        #   in Loop: Header=BB16_11 Depth=2
	cmpb	$87, %al
	jne	.LBB16_18
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB16_11 Depth=2
	cmpb	$88, 641(%rsp)
	jne	.LBB16_18
# BB#16:                                # %.thread
                                        #   in Loop: Header=BB16_11 Depth=2
	movb	642(%rsp), %cl
	testb	%cl, %cl
	jne	.LBB16_18
# BB#17:                                #   in Loop: Header=BB16_11 Depth=2
	movl	$.L.str.136, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	sscanf
	movl	$1, 64(%rsp)            # 4-byte Folded Spill
	cvttss2si	68(%rsp), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	jmp	.LBB16_42
	.p2align	4, 0x90
.LBB16_18:                              # %.thread324
                                        #   in Loop: Header=BB16_11 Depth=2
	cmpb	$66, %al
	jne	.LBB16_21
# BB#19:                                # %.thread324
                                        #   in Loop: Header=BB16_11 Depth=2
	movb	641(%rsp), %cl
	testb	%cl, %cl
	jne	.LBB16_21
# BB#20:                                #   in Loop: Header=BB16_11 Depth=2
	movl	$.L.str.138, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	108(%rsp), %rdx
	leaq	104(%rsp), %rcx
	leaq	100(%rsp), %r8
	leaq	96(%rsp), %r9
	callq	sscanf
	cvttss2si	108(%rsp), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	cvttss2si	104(%rsp), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cvttss2si	100(%rsp), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$1, %ebx
	cvttss2si	96(%rsp), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB16_42:                              # %.preheader273
                                        #   in Loop: Header=BB16_11 Depth=2
	movq	%r12, %rax
	shlq	$32, %rax
	movslq	%r12d, %r12
	decq	%r12
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rax
	.p2align	4, 0x90
.LBB16_43:                              #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	129(%rsp,%r12), %ecx
	cmpb	$59, %cl
	setne	%dl
	incq	%r12
	addq	%r14, %rax
	cmpb	$10, %cl
	je	.LBB16_45
# BB#44:                                #   in Loop: Header=BB16_43 Depth=3
	testb	%dl, %dl
	jne	.LBB16_43
.LBB16_45:                              #   in Loop: Header=BB16_11 Depth=2
	cmpb	$59, %cl
	jne	.LBB16_48
# BB#46:                                # %.preheader271.preheader
                                        #   in Loop: Header=BB16_11 Depth=2
	sarq	$32, %rax
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB16_47:                              # %.preheader271
                                        #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$32, 129(%rsp,%r12)
	leaq	1(%r12), %r12
	je	.LBB16_47
.LBB16_48:                              # %.backedge
                                        #   in Loop: Header=BB16_11 Depth=2
	movslq	%r12d, %rax
	leaq	128(%rsp,%rax), %rbp
	cmpb	$10, 128(%rsp,%rax)
	jne	.LBB16_11
# BB#49:                                # %._crit_edge
                                        #   in Loop: Header=BB16_4 Depth=1
	movl	%ebx, %r12d
	movl	16(%rsp), %ebx          # 4-byte Reload
	testb	%bl, %bl
	leaq	128(%rsp), %rbp
	je	.LBB16_63
# BB#50:                                #   in Loop: Header=BB16_4 Depth=1
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_52
# BB#51:                                #   in Loop: Header=BB16_4 Depth=1
	movzwl	20(%rsp), %edi          # 2-byte Folded Reload
	callq	FileName
	movq	%rax, %r9
	movq	1224(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$3, %esi
	movl	$.L.str.142, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB16_52:                              #   in Loop: Header=BB16_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB16_54
# BB#53:                                #   in Loop: Header=BB16_4 Depth=1
	movzwl	20(%rsp), %edi          # 2-byte Folded Reload
	callq	FileName
	movq	%rax, %r9
	movq	1224(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, (%rsp)
	movl	$37, %edi
	movl	$4, %esi
	movl	$.L.str.143, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB16_54:                              #   in Loop: Header=BB16_4 Depth=1
	movzbl	%bl, %r9d
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	(%rax,%r9), %cl
	testb	%cl, %cl
	movq	72(%rsp), %rsi          # 8-byte Reload
	je	.LBB16_59
# BB#55:                                #   in Loop: Header=BB16_4 Depth=1
	cmpb	$1, %cl
	jne	.LBB16_57
# BB#56:                                #   in Loop: Header=BB16_4 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9), %rcx
	jmp	.LBB16_58
.LBB16_57:                              #   in Loop: Header=BB16_4 Depth=1
	movslq	(%rsi), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%rsi)
	addq	24(%rsp), %rcx          # 8-byte Folded Reload
.LBB16_58:                              # %.sink.split
                                        #   in Loop: Header=BB16_4 Depth=1
	movb	$0, (%rcx)
.LBB16_59:                              #   in Loop: Header=BB16_4 Depth=1
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 40(%rcx)
	je	.LBB16_61
# BB#60:                                #   in Loop: Header=BB16_4 Depth=1
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	88(%rsp), %esi          # 4-byte Reload
	subl	%esi, %edx
	movl	44(%rsp), %ecx          # 4-byte Reload
	subl	%esi, %ecx
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	movl	60(%rsp), %ebx          # 4-byte Reload
	subl	%ebx, %esi
	testl	%ebx, %ebx
	movl	$0, %edi
	cmovew	%di, %si
	testl	%eax, %eax
	cmovew	%di, %si
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	cmovnew	%di, %si
	movl	56(%rsp), %edi          # 4-byte Reload
                                        # kill: %DI<def> %DI<kill> %EDI<kill> %EDI<def>
	movl	%ebx, %r8d
	jmp	.LBB16_62
.LBB16_61:                              #   in Loop: Header=BB16_4 Depth=1
	movl	PlainCharHeight(%rip), %edx
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	movl	%ecx, %edx
	negl	%edx
	movl	PlainCharWidth(%rip), %r8d
	xorl	%edi, %edi
	xorl	%esi, %esi
.LBB16_62:                              #   in Loop: Header=BB16_4 Depth=1
	leaq	(%r9,%r9,4), %rax
	movq	1216(%rsp), %rbx
	movw	%di, 4(%rbx,%rax,2)
	movw	%dx, 2(%rbx,%rax,2)
	movw	%r8w, 6(%rbx,%rax,2)
	movw	%cx, (%rbx,%rax,2)
	movw	%si, 8(%rbx,%rax,2)
.LBB16_63:                              # %.backedge277
                                        #   in Loop: Header=BB16_4 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB16_4
.LBB16_64:                              # %.critedge
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	ReadCharMetrics, .Lfunc_end16-ReadCharMetrics
	.cfi_endproc

	.type	font_curr_page,@object  # @font_curr_page
	.comm	font_curr_page,4,4
	.type	font_count,@object      # @font_count
	.local	font_count
	.comm	font_count,4,4
	.type	font_root,@object       # @font_root
	.local	font_root
	.comm	font_root,8,8
	.type	font_used,@object       # @font_used
	.local	font_used
	.comm	font_used,8,8
	.type	font_seqnum,@object     # @font_seqnum
	.local	font_seqnum
	.comm	font_seqnum,4,4
	.type	finfo,@object           # @finfo
	.comm	finfo,8,8
	.type	finfo_size,@object      # @finfo_size
	.local	finfo_size
	.comm	finfo_size,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"@FontDef"
	.size	.L.str.1, 9

	.type	FontDefSym,@object      # @FontDefSym
	.local	FontDefSym
	.comm	FontDefSym,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"@Tag"
	.size	.L.str.2, 5

	.type	fd_tag,@object          # @fd_tag
	.local	fd_tag
	.comm	fd_tag,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"@Family"
	.size	.L.str.3, 8

	.type	fd_family,@object       # @fd_family
	.local	fd_family
	.comm	fd_family,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"@Face"
	.size	.L.str.4, 6

	.type	fd_face,@object         # @fd_face
	.local	fd_face
	.comm	fd_face,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"@Name"
	.size	.L.str.5, 6

	.type	fd_name,@object         # @fd_name
	.local	fd_name
	.comm	fd_name,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"@Metrics"
	.size	.L.str.6, 9

	.type	fd_metrics,@object      # @fd_metrics
	.local	fd_metrics
	.comm	fd_metrics,8,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"@ExtraMetrics"
	.size	.L.str.7, 14

	.type	fd_extra_metrics,@object # @fd_extra_metrics
	.local	fd_extra_metrics
	.comm	fd_extra_metrics,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"@Mapping"
	.size	.L.str.8, 9

	.type	fd_mapping,@object      # @fd_mapping
	.local	fd_mapping
	.comm	fd_mapping,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"@Recode"
	.size	.L.str.9, 8

	.type	fd_recode,@object       # @fd_recode
	.local	fd_recode
	.comm	fd_recode,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"assert failed in %s"
	.size	.L.str.10, 20

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FontChange: font_count!"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"smallcaps"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"nosmallcaps"
	.size	.L.str.13, 12

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.zero	1
	.size	.L.str.14, 1

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"error in left parameter of %s"
	.size	.L.str.15, 30

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"@Font"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"FontChange: num!"
	.size	.L.str.17, 17

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"initial font must have family, face and size"
	.size	.L.str.18, 45

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"FontChange: Up(finfo[font(*style)].font_table) !"
	.size	.L.str.19, 49

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"FontChange: type(tmpf)!"
	.size	.L.str.20, 24

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"FontChange: Up(tmpf)!"
	.size	.L.str.21, 22

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"FontChange: type(family)!"
	.size	.L.str.22, 26

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FontChange: type(face)!"
	.size	.L.str.23, 24

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"FontChange: Up(face)!"
	.size	.L.str.24, 22

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FontChange fr!"
	.size	.L.str.25, 15

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"FontChange requested_face!"
	.size	.L.str.26, 27

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"font family name %s must be followed by a face name"
	.size	.L.str.27, 52

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"there is no font with family name %s and face name %s"
	.size	.L.str.28, 54

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"FontChange: no children!"
	.size	.L.str.29, 25

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"FontChange: 1 child!"
	.size	.L.str.30, 21

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"FontChange: 2 children!"
	.size	.L.str.31, 24

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"syntax error in font size %s; ignoring it"
	.size	.L.str.32, 42

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"no current font on which to base size change %s"
	.size	.L.str.33, 48

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"FontChange: %d"
	.size	.L.str.34, 15

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%s %s ignored (result is not positive)"
	.size	.L.str.35, 39

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"too many different fonts and sizes (max is %d)"
	.size	.L.str.36, 47

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"run out of memory when increasing font table size"
	.size	.L.str.37, 50

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"FontChange: old!"
	.size	.L.str.38, 17

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"run out of memory when changing font or font size"
	.size	.L.str.39, 50

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"FontWordSize: !is_word(type(x))!"
	.size	.L.str.40, 33

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"no current font at word %s"
	.size	.L.str.41, 27

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"no current colour at word %s"
	.size	.L.str.42, 29

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"no current language at word %s"
	.size	.L.str.43, 31

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" "
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"character %s replaced by space (it has no glyph in font %s)"
	.size	.L.str.45, 60

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"FontSize!"
	.size	.L.str.46, 10

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"no current font at this point"
	.size	.L.str.47, 30

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"FontHalfXHeight!"
	.size	.L.str.48, 17

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"FontMapping!"
	.size	.L.str.49, 13

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"FontName!"
	.size	.L.str.50, 10

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"FontFamiliy!"
	.size	.L.str.51, 13

	.type	FontFamilyAndFace.buff,@object # @FontFamilyAndFace.buff
	.local	FontFamilyAndFace.buff
	.comm	FontFamilyAndFace.buff,80,16
	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"family and face names %s %s are too long"
	.size	.L.str.52, 41

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"FontDebug: font_root!"
	.size	.L.str.53, 22

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"FontPrintAll: family!"
	.size	.L.str.54, 22

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"FontPrintAll: face!"
	.size	.L.str.55, 20

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"FontDebug: Down(face)!"
	.size	.L.str.56, 23

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"FontPrintAll: ps_name!"
	.size	.L.str.57, 23

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"FontPrintAll: first_size!"
	.size	.L.str.58, 26

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"/%s%s %s /%s LoutRecode\n"
	.size	.L.str.59, 25

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"/%s { /%s%s LoutFont } def\n"
	.size	.L.str.60, 28

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"/%s { /%s LoutFont } def\n"
	.size	.L.str.61, 26

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"FontDebug: font_used!"
	.size	.L.str.63, 22

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"FontPrintPageSetup: face!"
	.size	.L.str.64, 26

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"FontPrintPageSetup: first_size!"
	.size	.L.str.65, 32

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"FontPrintPageSetup: ps_name!"
	.size	.L.str.66, 29

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"FontPrintPageResources: face!"
	.size	.L.str.67, 30

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"FontPrintPageResources: ps_name!"
	.size	.L.str.68, 33

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"FontPageUsed!"
	.size	.L.str.69, 14

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"%s font %s\n"
	.size	.L.str.70, 12

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"%%DocumentNeededResources:"
	.size	.L.str.71, 27

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"%%+"
	.size	.L.str.72, 4

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"unable to set font %s %s (no font databases loaded)"
	.size	.L.str.73, 52

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"%s-%s"
	.size	.L.str.74, 6

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"cannot read %s for %s"
	.size	.L.str.75, 22

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"FontRead: type(y) != PAR!"
	.size	.L.str.76, 26

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"font family name %s incompatible with %s value %s"
	.size	.L.str.77, 50

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"font face name %s incompatible with %s value %s"
	.size	.L.str.78, 48

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"illegal font name (quotes needed?)"
	.size	.L.str.79, 35

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"illegal font metrics file name (quotes needed?)"
	.size	.L.str.80, 48

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"illegal font extra metrics file name (quotes needed?)"
	.size	.L.str.81, 54

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"illegal mapping file name (quotes needed?)"
	.size	.L.str.82, 43

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"illegal value of %s"
	.size	.L.str.83, 20

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"FontRead: cannot identify component of FontDef"
	.size	.L.str.84, 47

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"font %s %s already defined, at%s"
	.size	.L.str.85, 33

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"No"
	.size	.L.str.86, 3

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"Yes"
	.size	.L.str.87, 4

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"expecting either Yes or No here"
	.size	.L.str.88, 32

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"too many different fonts and sizes (maximum is %d)"
	.size	.L.str.89, 51

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"fnt"
	.size	.L.str.90, 4

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"cannot open font file %s"
	.size	.L.str.91, 25

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"%s"
	.size	.L.str.92, 3

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"StartFontMetrics"
	.size	.L.str.93, 17

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"font file %s does not begin with StartFontMetrics"
	.size	.L.str.94, 50

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"run out of memory while reading font file %s"
	.size	.L.str.95, 45

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"EndFontMetrics\n"
	.size	.L.str.96, 16

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"UnderlinePosition"
	.size	.L.str.97, 18

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"UnderlinePosition found twice in font file (line %d)"
	.size	.L.str.98, 53

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"UnderlinePosition %f"
	.size	.L.str.99, 21

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"UnderlineThickness"
	.size	.L.str.100, 19

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"UnderlineThickness found twice in font file (line %d)"
	.size	.L.str.101, 54

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"UnderlineThickness %f"
	.size	.L.str.102, 22

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"XHeight"
	.size	.L.str.103, 8

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"XHeight found twice in font file (line %d)"
	.size	.L.str.104, 43

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"XHeight %f"
	.size	.L.str.105, 11

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"FontName"
	.size	.L.str.106, 9

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"FontName found twice in font file %s (line %d)"
	.size	.L.str.107, 47

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"FontName %s"
	.size	.L.str.108, 12

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"FontName empty in font file %s (line %d)"
	.size	.L.str.109, 41

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"FontName in font file (%s) and %s (%s) disagree"
	.size	.L.str.110, 48

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"IsFixedPitch"
	.size	.L.str.111, 13

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"IsFixedPitch %s"
	.size	.L.str.112, 16

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"true"
	.size	.L.str.113, 5

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"StartCharMetrics"
	.size	.L.str.114, 17

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"FontName missing in file %s"
	.size	.L.str.115, 28

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"StartKernPairs"
	.size	.L.str.116, 15

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"StartKernPairs %d"
	.size	.L.str.117, 18

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"syntax error on StartKernPairs line in font file %s (line %d)"
	.size	.L.str.118, 62

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"EndKernPairs"
	.size	.L.str.119, 13

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"KPX"
	.size	.L.str.120, 4

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"KPX %s %s %f"
	.size	.L.str.121, 13

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"syntax error in font file %s (line %d): %s"
	.size	.L.str.122, 43

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"non-contiguous kerning pair %s %s in font file %s (line %d)"
	.size	.L.str.123, 60

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"too many kerning pairs in font file %s (line %d)"
	.size	.L.str.124, 49

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"kerning pair %s %s appears twice in font file %s (line %d)"
	.size	.L.str.125, 59

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"EndFontMetrics missing from font file %s"
	.size	.L.str.126, 41

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"space"
	.size	.L.str.127, 6

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"cannot open extra font file %s"
	.size	.L.str.128, 31

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"StartExtraCharMetrics"
	.size	.L.str.129, 22

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"StartBuildComposites"
	.size	.L.str.130, 21

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"EndCharMetrics"
	.size	.L.str.131, 15

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"EndExtraCharMetrics"
	.size	.L.str.132, 20

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"N"
	.size	.L.str.133, 2

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"N %s"
	.size	.L.str.134, 5

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"WX"
	.size	.L.str.135, 3

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"WX %f"
	.size	.L.str.136, 6

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"B"
	.size	.L.str.137, 2

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"B %f %f %f %f"
	.size	.L.str.138, 14

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"L"
	.size	.L.str.139, 2

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"ignoring unencoded ligature character %s in font file %s (line %d)"
	.size	.L.str.140, 67

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"too many ligature characters in font file %s (line %d)"
	.size	.L.str.141, 55

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"WX missing in font file %s (line %d)"
	.size	.L.str.142, 37

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"B missing in font file %s (line %d)"
	.size	.L.str.143, 36

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"CC"
	.size	.L.str.144, 3

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"CC %s %d "
	.size	.L.str.145, 10

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"syntax error in extra font file %s (line %d)"
	.size	.L.str.146, 45

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"unknown character name %s in font file %s (line %d)"
	.size	.L.str.147, 52

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	" PCC %s %d %d"
	.size	.L.str.148, 14

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"too many composites in file %s (at line %d)"
	.size	.L.str.149, 44

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"EndBuildComposites"
	.size	.L.str.150, 19

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"missing EndBuildComposites in extra font file %s (line %d)"
	.size	.L.str.151, 59


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
