	.text
	.file	"7zDecode.bc"
	.globl	_ZN8NArchive3N7z8CDecoderC2Eb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CDecoderC2Eb,@function
_ZN8NArchive3N7z8CDecoderC2Eb:          # @_ZN8NArchive3N7z8CDecoderC2Eb
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	$8, 32(%rdi)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 8(%rdi)
	movups	%xmm0, 48(%rdi)
	movq	$8, 64(%rdi)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	movq	$4, 96(%rdi)
	movq	$_ZTV13CRecordVectorIjE+16, 72(%rdi)
	movups	%xmm0, 112(%rdi)
	movq	$4, 128(%rdi)
	movq	$_ZTV13CRecordVectorIjE+16, 104(%rdi)
	movups	%xmm0, 144(%rdi)
	movq	$8, 160(%rdi)
	movq	$_ZTV13CRecordVectorIyE+16, 136(%rdi)
	movq	$0, 192(%rdi)
	movups	%xmm0, 208(%rdi)
	movq	$8, 224(%rdi)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, 200(%rdi)
	movb	$1, 168(%rdi)
	movb	$0, (%rdi)
	retq
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CDecoderC2Eb, .Lfunc_end0-_ZN8NArchive3N7z8CDecoderC2Eb
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.text
	.globl	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj,@function
_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj: # @_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$488, %rsp              # imm = 0x1E8
.Lcfi6:
	.cfi_def_cfa_offset 544
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %r15
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
.Lcfi13:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZNK8NArchive3N7z7CFolder14CheckStructureEv
	movl	$-2147467263, %ecx      # imm = 0x80004001
	testb	%al, %al
	je	.LBB2_240
# BB#1:
	movq	560(%rsp), %rax
	movb	$0, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%rsp)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 112(%rsp)
	movq	$0, 440(%rsp)
	leaq	448(%rsp), %rdi
.Ltmp0:
.Lcfi14:
	.cfi_escape 0x2e, 0x00
	callq	CriticalSection_Init
.Ltmp1:
# BB#2:                                 # %_ZN15CLockedInStreamC2Ev.exit
	movq	%rbx, 432(%rsp)         # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB2_4
# BB#3:
	movq	(%rbp), %rax
.Ltmp6:
.Lcfi15:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp7:
.LBB2_4:                                # %.noexc
	movq	440(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp8:
.Lcfi16:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp9:
.LBB2_6:                                # %_ZN15CLockedInStream4InitEP9IInStream.exit
	movq	%rbp, 440(%rsp)
	cmpl	$0, 76(%r15)
	jle	.LBB2_21
# BB#7:                                 # %.lr.ph790
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
.Ltmp11:
.Lcfi17:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp12:
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	movl	$0, 8(%rbx)
	movq	$_ZTV28CLockedSequentialInStreamImp+16, (%rbx)
.Ltmp14:
.Lcfi18:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTV28CLockedSequentialInStreamImp+24(%rip)
.Ltmp15:
# BB#10:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB2_8 Depth=1
	leaq	440(%rsp), %rax
	movq	%rax, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	304(%rsp), %rax         # 8-byte Reload
	addq	(%rax,%r14,8), %r12
.Ltmp17:
.Lcfi19:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp18:
# BB#11:                                #   in Loop: Header=BB2_8 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rbp)
	movq	$0, 16(%rbp)
.Ltmp20:
.Lcfi20:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
.Ltmp21:
# BB#12:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit329
                                        #   in Loop: Header=BB2_8 Depth=1
	movq	(%rbx), %rax
.Ltmp23:
.Lcfi21:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp24:
# BB#13:                                # %.noexc330
                                        #   in Loop: Header=BB2_8 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_8 Depth=1
	movq	(%rdi), %rax
.Ltmp25:
.Lcfi22:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp26:
.LBB2_15:                               #   in Loop: Header=BB2_8 Depth=1
	movq	%rbx, 16(%rbp)
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	%rax, 24(%rbp)
	movq	$0, 32(%rbp)
	movb	$0, 40(%rbp)
.Ltmp27:
.Lcfi23:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp28:
# BB#16:                                #   in Loop: Header=BB2_8 Depth=1
	movq	%rbp, (%r13)
	movq	(%rbp), %rax
.Ltmp29:
.Lcfi24:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp30:
# BB#17:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB2_8 Depth=1
.Ltmp32:
.Lcfi25:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp33:
# BB#18:                                #   in Loop: Header=BB2_8 Depth=1
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
	movq	(%rbp), %rax
.Ltmp37:
.Lcfi26:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp38:
# BB#19:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB2_8 Depth=1
	movq	(%rbx), %rax
.Ltmp42:
.Lcfi27:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp43:
# BB#20:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit338
                                        #   in Loop: Header=BB2_8 Depth=1
	incq	%r14
	movslq	76(%r15), %rax
	cmpq	%rax, %r14
	jl	.LBB2_8
.LBB2_21:                               # %._crit_edge791
	movslq	12(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rsp)
	movq	$8, 168(%rsp)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 144(%rsp)
	movdqu	%xmm0, 184(%rsp)
	movq	$8, 200(%rsp)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 176(%rsp)
	movdqu	%xmm0, 216(%rsp)
	movq	$4, 232(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 208(%rsp)
	movdqu	%xmm0, 248(%rsp)
	movq	$4, 264(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 240(%rsp)
	movdqu	%xmm0, 280(%rsp)
	movq	$8, 296(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 272(%rsp)
.Ltmp45:
.Lcfi28:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp46:
# BB#22:                                # %.noexc343
	leaq	176(%rsp), %r12
.Ltmp47:
.Lcfi29:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp48:
# BB#23:                                # %.noexc344
	leaq	208(%rsp), %rdi
.Ltmp49:
.Lcfi30:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp50:
# BB#24:                                # %.noexc345
	leaq	240(%rsp), %rdi
.Ltmp51:
.Lcfi31:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp52:
# BB#25:                                # %.noexc346
	leaq	272(%rsp), %rdi
.Ltmp53:
.Lcfi32:
	.cfi_escape 0x2e, 0x00
	movq	%rdi, 352(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp54:
# BB#26:                                # %.noexc347
	cmpl	$0, 44(%r15)
	jle	.LBB2_30
# BB#27:                                # %.lr.ph64.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r15), %rax
	movl	(%rax,%rbx,8), %r14d
	movl	4(%rax,%rbx,8), %ebp
.Ltmp56:
.Lcfi33:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp57:
# BB#29:                                # %.noexc348
                                        #   in Loop: Header=BB2_28 Depth=1
	shlq	$32, %rbp
	orq	%r14, %rbp
	movq	192(%rsp), %rax
	movslq	188(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	188(%rsp)
	incq	%rbx
	movslq	44(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_28
.LBB2_30:                               # %.preheader50.i
	cmpl	$0, 12(%r15)
	jle	.LBB2_46
# BB#31:                                # %.lr.ph61.i
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_32:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_36 Depth 2
                                        #       Child Loop BB2_38 Depth 3
	movq	16(%r15), %rax
	movq	(%rax,%r12,8), %rbp
	movl	32(%rbp), %r14d
	movl	36(%rbp), %ebx
.Ltmp59:
.Lcfi34:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp60:
# BB#33:                                # %.noexc349
                                        #   in Loop: Header=BB2_32 Depth=1
	movq	%rbx, %rax
	shlq	$32, %rax
	orq	%r14, %rax
	movq	160(%rsp), %rcx
	movslq	156(%rsp), %rdx
	movq	%rax, (%rcx,%rdx,8)
	incl	156(%rsp)
	movq	(%rbp), %rbp
.Ltmp61:
.Lcfi35:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp62:
# BB#34:                                # %.noexc350
                                        #   in Loop: Header=BB2_32 Depth=1
	movq	288(%rsp), %rax
	movslq	284(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 284(%rsp)
	testl	%ebx, %ebx
	je	.LBB2_45
# BB#35:                                # %.lr.ph57.i.preheader
                                        #   in Loop: Header=BB2_32 Depth=1
	xorl	%r14d, %r14d
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	leaq	240(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_36:                               # %.lr.ph57.i
                                        #   Parent Loop BB2_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_38 Depth 3
	movslq	44(%r15), %rax
	testq	%rax, %rax
	jle	.LBB2_41
# BB#37:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_36 Depth=2
	movq	48(%r15), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_32 Depth=1
                                        #     Parent Loop BB2_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r13d, 4(%rdx,%rcx,8)
	je	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_38 Depth=3
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB2_38
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_40:                               # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i
                                        #   in Loop: Header=BB2_36 Depth=2
	testl	%ecx, %ecx
	jns	.LBB2_43
	.p2align	4, 0x90
.LBB2_41:                               # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB2_36 Depth=2
.Ltmp64:
.Lcfi36:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp65:
# BB#42:                                # %.noexc351
                                        #   in Loop: Header=BB2_36 Depth=2
	movq	256(%rsp), %rax
	movslq	252(%rsp), %rcx
	movl	%r13d, (%rax,%rcx,4)
	incl	252(%rsp)
.LBB2_43:                               #   in Loop: Header=BB2_36 Depth=2
	incl	%r14d
	incl	%r13d
	cmpl	%ebx, %r14d
	jne	.LBB2_36
# BB#44:                                # %._crit_edge58.loopexit.i
                                        #   in Loop: Header=BB2_32 Depth=1
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	%ebx, %r13d
.LBB2_45:                               # %._crit_edge58.i
                                        #   in Loop: Header=BB2_32 Depth=1
	incq	%r12
	movslq	12(%r15), %rax
	cmpq	%rax, %r12
	jl	.LBB2_32
.LBB2_46:                               # %.preheader.i
	cmpl	$0, 76(%r15)
	movq	16(%rsp), %r12          # 8-byte Reload
	leaq	208(%rsp), %r14
	jle	.LBB2_50
# BB#47:                                # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_48:                               # =>This Inner Loop Header: Depth=1
	movq	80(%r15), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp67:
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp68:
# BB#49:                                # %.noexc352
                                        #   in Loop: Header=BB2_48 Depth=1
	movq	224(%rsp), %rax
	movslq	220(%rsp), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	220(%rsp)
	incq	%rbx
	movslq	76(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_48
.LBB2_50:                               # %_ZN8NArchive3N7zL31ConvertFolderItemInfoToBindInfoERKNS0_7CFolderERNS0_11CBindInfoExE.exit
	cmpb	$0, (%r12)
	je	.LBB2_70
# BB#51:
	movslq	156(%rsp), %rax
	cmpl	20(%r12), %eax
	jne	.LBB2_70
# BB#52:                                # %.preheader40.i
	testl	%eax, %eax
	jle	.LBB2_57
# BB#53:                                # %.lr.ph46.i
	movq	160(%rsp), %rcx
	movq	24(%r12), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_54:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,8), %edi
	cmpl	(%rdx,%rsi,8), %edi
	jne	.LBB2_70
# BB#55:                                #   in Loop: Header=BB2_54 Depth=1
	movl	4(%rdx,%rsi,8), %edi
	cmpl	%edi, 4(%rcx,%rsi,8)
	jne	.LBB2_70
# BB#56:                                #   in Loop: Header=BB2_54 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB2_54
.LBB2_57:                               # %._crit_edge47.i
	movslq	188(%rsp), %rax
	cmpl	52(%r12), %eax
	jne	.LBB2_70
# BB#58:                                # %.preheader38.i
	testl	%eax, %eax
	jle	.LBB2_63
# BB#59:                                # %.lr.ph44.i
	movq	192(%rsp), %rcx
	movq	56(%r12), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_60:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,8), %edi
	cmpl	(%rdx,%rsi,8), %edi
	jne	.LBB2_70
# BB#61:                                #   in Loop: Header=BB2_60 Depth=1
	movl	4(%rdx,%rsi,8), %edi
	cmpl	%edi, 4(%rcx,%rsi,8)
	jne	.LBB2_70
# BB#62:                                #   in Loop: Header=BB2_60 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB2_60
.LBB2_63:                               # %.preheader.i353
	movslq	284(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB2_67
# BB#64:                                # %.lr.ph.i354
	movq	288(%rsp), %rcx
	movq	152(%r12), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	cmpq	(%rdx,%rsi,8), %rdi
	jne	.LBB2_70
# BB#66:                                #   in Loop: Header=BB2_65 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB2_65
.LBB2_67:                               # %._crit_edge.i
	movl	220(%rsp), %eax
	cmpl	84(%r12), %eax
	jne	.LBB2_70
# BB#68:
	movl	252(%rsp), %eax
	cmpl	116(%r12), %eax
	jne	.LBB2_70
# BB#69:                                # %._crit_edge854
	leaq	184(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
                                        # implicit-def: %R13D
	jmp	.LBB2_123
.LBB2_70:                               # %.thread
	leaq	200(%r12), %rbp
.Ltmp70:
.Lcfi38:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp71:
# BB#71:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB2_74
# BB#72:
	movq	(%rdi), %rax
.Ltmp72:
.Lcfi39:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp73:
# BB#73:                                # %.noexc357
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	$0, 192(%rax)
.LBB2_74:                               # %_ZN9CMyComPtrI15ICompressCoder2E7ReleaseEv.exit
	cmpb	$0, 168(%rax)
	je	.LBB2_80
# BB#75:
.Ltmp74:
.Lcfi40:
	.cfi_escape 0x2e, 0x00
	movl	$224, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp75:
# BB#76:
	movl	$0, 16(%rbx)
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbx)
	movq	$8, 48(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 24(%rbx)
	movdqu	%xmm0, 64(%rbx)
	movq	$8, 80(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 56(%rbx)
	movdqu	%xmm0, 96(%rbx)
	movq	$4, 112(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 88(%rbx)
	movdqu	%xmm0, 128(%rbx)
	movq	$4, 144(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 120(%rbx)
	movdqu	%xmm0, 160(%rbx)
	movq	$8, 176(%rbx)
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, 152(%rbx)
	movdqu	%xmm0, 200(%rbx)
	movq	$8, 216(%rbx)
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, 192(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rbx, 176(%rax)
.Ltmp76:
.Lcfi41:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTVN11NCoderMixer14CCoderMixer2MTE+24(%rip)
.Ltmp77:
# BB#77:                                # %.noexc358
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB2_79
# BB#78:
	movq	(%rdi), %rax
.Ltmp78:
.Lcfi42:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp79:
.LBB2_79:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, 192(%rcx)
	movq	176(%rcx), %rax
	leaq	8(%rax), %rdi
	testq	%rax, %rax
	cmoveq	%rax, %rdi
	leaq	184(%rcx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 184(%rcx)
	jmp	.LBB2_81
.LBB2_80:                               # %_ZN9CMyComPtrI15ICompressCoder2E7ReleaseEv.exit._crit_edge
	leaq	184(%rax), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	184(%rax), %rdi
.LBB2_81:
	movq	(%rdi), %rax
.Ltmp80:
.Lcfi43:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rsi
	callq	*(%rax)
.Ltmp81:
# BB#82:
	movq	%rbp, %rcx
	movl	%eax, %r12d
	testl	%eax, %eax
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	208(%rsp), %r14
	jne	.LBB2_231
# BB#83:                                # %.preheader563
	movq	%rax, %r12
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_114
# BB#84:                                # %.lr.ph785
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	jmp	.LBB2_105
	.p2align	4, 0x90
.LBB2_85:                               #   in Loop: Header=BB2_105 Depth=1
	incq	%rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB2_105
	jmp	.LBB2_115
	.p2align	4, 0x90
.LBB2_86:                               #   in Loop: Header=BB2_105 Depth=1
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	cmpl	$1, 32(%rbx)
	jne	.LBB2_92
# BB#87:                                # %_ZNK8NArchive3N7z10CCoderInfo13IsSimpleCoderEv.exit
                                        #   in Loop: Header=BB2_105 Depth=1
	cmpl	$1, 36(%rbx)
	jne	.LBB2_92
# BB#88:                                #   in Loop: Header=BB2_105 Depth=1
	movq	56(%rsp), %r13
	testq	%r13, %r13
	je	.LBB2_104
# BB#89:                                #   in Loop: Header=BB2_105 Depth=1
	movq	(%r13), %rax
.Ltmp89:
.Lcfi44:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp90:
# BB#90:                                # %_ZN9CMyComPtrI8IUnknownEaSEPS0_.exit
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	%r12, %r14
	cmpb	$0, 168(%r14)
	je	.LBB2_96
# BB#91:                                #   in Loop: Header=BB2_105 Depth=1
	movq	176(%r14), %rdi
	movq	56(%rsp), %rsi
.Ltmp92:
.Lcfi45:
	.cfi_escape 0x2e, 0x00
	callq	_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder
.Ltmp93:
	jmp	.LBB2_96
	.p2align	4, 0x90
.LBB2_92:                               # %_ZNK8NArchive3N7z10CCoderInfo13IsSimpleCoderEv.exit.thread
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	48(%rsp), %r13
	testq	%r13, %r13
	je	.LBB2_102
# BB#93:                                #   in Loop: Header=BB2_105 Depth=1
	movq	(%r13), %rax
.Ltmp85:
.Lcfi46:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp86:
# BB#94:                                # %_ZN9CMyComPtrI8IUnknownEaSEPS0_.exit365
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	%r12, %r14
	cmpb	$0, 168(%r14)
	je	.LBB2_96
# BB#95:                                #   in Loop: Header=BB2_105 Depth=1
	movq	176(%r14), %rdi
	movq	48(%rsp), %rsi
.Ltmp87:
.Lcfi47:
	.cfi_escape 0x2e, 0x00
	callq	_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2
.Ltmp88:
.LBB2_96:                               #   in Loop: Header=BB2_105 Depth=1
.Ltmp94:
.Lcfi48:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp95:
# BB#97:                                # %.noexc366
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.LBB2_99
# BB#98:                                #   in Loop: Header=BB2_105 Depth=1
	movq	(%r13), %rax
.Ltmp96:
.Lcfi49:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp97:
.LBB2_99:                               # %_ZN9CMyComPtrI8IUnknownEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB2_105 Depth=1
.Ltmp99:
.Lcfi50:
	.cfi_escape 0x2e, 0x00
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp100:
# BB#100:                               #   in Loop: Header=BB2_105 Depth=1
	movslq	212(%r14), %rax
	leal	1(%rax), %ecx
	movq	%r14, %rsi
	xorl	%r14d, %r14d
	testq	%r13, %r13
	movq	216(%rsi), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movl	%ecx, 212(%rsi)
	je	.LBB2_103
# BB#101:                               #   in Loop: Header=BB2_105 Depth=1
	movq	(%r13), %rax
.Ltmp104:
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp105:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB2_108
.LBB2_102:                              #   in Loop: Header=BB2_105 Depth=1
	movl	$1, %r14d
	movl	$-2147467263, %r13d     # imm = 0x80004001
	jmp	.LBB2_110
.LBB2_103:                              #   in Loop: Header=BB2_105 Depth=1
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB2_108
.LBB2_104:                              #   in Loop: Header=BB2_105 Depth=1
	movl	$-2147467263, %r13d     # imm = 0x80004001
	jmp	.LBB2_108
	.p2align	4, 0x90
.LBB2_105:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	$0, 56(%rsp)
	movq	$0, 48(%rsp)
	movq	(%rbx), %rdi
.Ltmp82:
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	xorl	%ecx, %ecx
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdx
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb
.Ltmp83:
# BB#106:                               #   in Loop: Header=BB2_105 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	movl	$1, %r14d
	je	.LBB2_86
# BB#107:                               #   in Loop: Header=BB2_105 Depth=1
	movl	%eax, %r13d
.LBB2_108:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit371
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_110
# BB#109:                               #   in Loop: Header=BB2_105 Depth=1
	movq	(%rdi), %rax
.Ltmp109:
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp110:
.LBB2_110:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
                                        #   in Loop: Header=BB2_105 Depth=1
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_112
# BB#111:                               #   in Loop: Header=BB2_105 Depth=1
	movq	(%rdi), %rax
.Ltmp114:
.Lcfi54:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp115:
.LBB2_112:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
                                        #   in Loop: Header=BB2_105 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_85
# BB#113:
	movl	%r13d, %r12d
	jmp	.LBB2_230
.LBB2_114:
	xorl	%r13d, %r13d
.LBB2_115:                              # %._crit_edge786
	leaq	8(%r12), %rdi
.Ltmp117:
.Lcfi55:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rsi
	callq	_ZN11NCoderMixer9CBindInfoaSERKS0_
.Ltmp118:
# BB#116:                               # %.noexc378
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	136(%rax), %r14
.Ltmp119:
.Lcfi56:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp120:
# BB#117:                               # %.noexc379
	movl	284(%rsp), %r12d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	148(%rax), %esi
	addl	%r12d, %esi
.Ltmp121:
.Lcfi57:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp122:
# BB#118:                               # %.noexc380
	testl	%r12d, %r12d
	jle	.LBB2_122
# BB#119:                               # %.lr.ph.i.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_120:                              # =>This Inner Loop Header: Depth=1
	movq	288(%rsp), %rax
	movq	(%rax,%rbx,8), %rbp
.Ltmp124:
.Lcfi58:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp125:
# BB#121:                               # %.noexc381
                                        #   in Loop: Header=BB2_120 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	152(%rdx), %rax
	movslq	148(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 148(%rdx)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB2_120
.LBB2_122:                              # %.thread510
	movq	16(%rsp), %r12          # 8-byte Reload
	movb	$1, (%r12)
	leaq	208(%rsp), %r14
.LBB2_123:
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp127:
.Lcfi59:
	.cfi_escape 0x2e, 0x00
	callq	*8(%rax)
.Ltmp128:
# BB#124:                               # %.preheader557
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_212
# BB#125:                               # %.lr.ph781
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_203:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_145 Depth 2
                                        #     Child Loop BB2_148 Depth 2
                                        #     Child Loop BB2_157 Depth 2
                                        #     Child Loop BB2_161 Depth 2
                                        #     Child Loop BB2_177 Depth 2
                                        #     Child Loop BB2_182 Depth 2
                                        #       Child Loop BB2_184 Depth 3
                                        #       Child Loop BB2_190 Depth 3
	movl	%r13d, 316(%rsp)        # 4-byte Spill
	movq	16(%r15), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	216(%r12), %rax
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rbp
	movq	$0, 104(%rsp)
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp130:
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICompressSetDecoderProperties2, %esi
	leaq	104(%rsp), %rdx
	callq	*(%rax)
.Ltmp131:
# BB#204:                               # %_ZNK9CMyComPtrI8IUnknownE14QueryInterfaceI30ICompressSetDecoderProperties2EEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	movq	80(%rsp), %r13          # 8-byte Reload
	je	.LBB2_208
# BB#205:                               #   in Loop: Header=BB2_203 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	movl	$-2147467263, %r12d     # imm = 0x80004001
	movl	$1, %ebx
	jne	.LBB2_209
# BB#206:                               #   in Loop: Header=BB2_203 Depth=1
	movq	(%rdi), %rax
	movq	24(%rcx), %rsi
.Ltmp133:
.Lcfi61:
	.cfi_escape 0x2e, 0x00
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*40(%rax)
.Ltmp134:
# BB#207:                               #   in Loop: Header=BB2_203 Depth=1
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.LBB2_209
.LBB2_208:                              # %.thread514
                                        #   in Loop: Header=BB2_203 Depth=1
	xorl	%ebx, %ebx
	movl	8(%rsp), %r12d          # 4-byte Reload
.LBB2_209:                              # %.thread516
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_211
# BB#210:                               #   in Loop: Header=BB2_203 Depth=1
	movq	(%rdi), %rax
.Ltmp138:
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp139:
.LBB2_211:                              # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%ebx, %ebx
	jne	.LBB2_231
# BB#126:                               #   in Loop: Header=BB2_203 Depth=1
	cmpb	$0, 568(%rsp)
	je	.LBB2_136
# BB#127:                               #   in Loop: Header=BB2_203 Depth=1
	movq	$0, 96(%rsp)
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp141:
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICompressSetCoderMt, %esi
	leaq	96(%rsp), %rdx
	callq	*(%rax)
.Ltmp142:
# BB#128:                               # %_ZNK9CMyComPtrI8IUnknownE14QueryInterfaceI19ICompressSetCoderMtEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_132
# BB#129:                               #   in Loop: Header=BB2_203 Depth=1
	movq	(%rdi), %rax
.Ltmp143:
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	movl	576(%rsp), %esi
	callq	*40(%rax)
.Ltmp144:
# BB#130:                               #   in Loop: Header=BB2_203 Depth=1
	testl	%eax, %eax
	je	.LBB2_132
# BB#131:                               #   in Loop: Header=BB2_203 Depth=1
	movl	$1, %ebx
	movl	%eax, %r12d
	jmp	.LBB2_133
.LBB2_132:                              #   in Loop: Header=BB2_203 Depth=1
	xorl	%ebx, %ebx
.LBB2_133:                              #   in Loop: Header=BB2_203 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_135
# BB#134:                               #   in Loop: Header=BB2_203 Depth=1
	movq	(%rdi), %rax
.Ltmp148:
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp149:
.LBB2_135:                              # %_ZN9CMyComPtrI19ICompressSetCoderMtED2Ev.exit392
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%ebx, %ebx
	jne	.LBB2_231
.LBB2_136:                              #   in Loop: Header=BB2_203 Depth=1
	movq	$0, 40(%rsp)
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp151:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICryptoSetPassword, %esi
	leaq	40(%rsp), %rdx
	callq	*(%rax)
.Ltmp152:
# BB#137:                               # %_ZNK9CMyComPtrI8IUnknownE14QueryInterfaceI18ICryptoSetPasswordEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	cmpq	$0, 40(%rsp)
	movq	552(%rsp), %rdi
	je	.LBB2_142
# BB#138:                               #   in Loop: Header=BB2_203 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_143
# BB#139:                               #   in Loop: Header=BB2_203 Depth=1
	movq	$0, 88(%rsp)
	movq	(%rdi), %rax
.Ltmp154:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rsi
	callq	*40(%rax)
.Ltmp155:
# BB#140:                               #   in Loop: Header=BB2_203 Depth=1
	testl	%eax, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmovnel	%eax, %r12d
	je	.LBB2_144
# BB#141:                               #   in Loop: Header=BB2_203 Depth=1
	movl	$1, %ebx
	jmp	.LBB2_167
	.p2align	4, 0x90
.LBB2_142:                              #   in Loop: Header=BB2_203 Depth=1
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB2_169
.LBB2_143:                              #   in Loop: Header=BB2_203 Depth=1
	movl	$-2147467259, 8(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x80004005
	movl	$1, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB2_170
.LBB2_144:                              #   in Loop: Header=BB2_203 Depth=1
	movq	560(%rsp), %rax
	movb	$1, (%rax)
	movq	$-1, %rbx
	movl	$-2, %ebp
	xorl	%ecx, %ecx
	movq	88(%rsp), %r13
	xorl	%eax, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB2_145:                              #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r14d
	addq	%rdx, %rax
	addl	$2, %ebp
	leal	1(%r14), %ecx
	cmpl	$0, 4(%r13,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB2_145
# BB#146:                               # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB2_203 Depth=1
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp157:
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp158:
# BB#147:                               # %.noexc402
                                        #   in Loop: Header=BB2_203 Depth=1
	movl	$0, (%rax)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_148:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rdx), %ecx
	movl	%ecx, (%rax,%rdx)
	addq	$4, %rdx
	testl	%ecx, %ecx
	jne	.LBB2_148
# BB#149:                               # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	je	.LBB2_154
# BB#150:                               #   in Loop: Header=BB2_203 Depth=1
	movl	%ebp, %edi
.Ltmp160:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp161:
	movq	80(%rsp), %r13          # 8-byte Reload
# BB#151:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%ebx, %ebx
	jle	.LBB2_162
# BB#152:                               # %.lr.ph761.preheader
                                        #   in Loop: Header=BB2_203 Depth=1
	movl	%ebx, %ecx
	cmpq	$3, %rcx
	ja	.LBB2_155
# BB#153:                               #   in Loop: Header=BB2_203 Depth=1
	xorl	%ecx, %ecx
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB2_160
.LBB2_154:                              #   in Loop: Header=BB2_203 Depth=1
	xorl	%ecx, %ecx
	movq	80(%rsp), %r13          # 8-byte Reload
	jmp	.LBB2_163
.LBB2_155:                              # %min.iters.checked
                                        #   in Loop: Header=BB2_203 Depth=1
	andl	$3, %ebx
	subq	%rbx, %rcx
	movdqa	.LCPI2_0(%rip), %xmm3   # xmm3 = [255,0,255,0,255,0,255,0,255,255,255,255,255,255,255,255]
	movq	64(%rsp), %rdi          # 8-byte Reload
	je	.LBB2_159
# BB#156:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_203 Depth=1
	movl	%r14d, %edx
	movl	%r14d, %esi
	andl	$3, %esi
	subq	%rsi, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_157:                              # %vector.body
                                        #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rsi,4), %xmm0
	pshuflw	$232, %xmm0, %xmm1      # xmm1 = xmm0[0,2,2,3,4,5,6,7]
	psrld	$8, %xmm0
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pand	%xmm3, %xmm1
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	movdqa	%xmm3, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movq	%xmm2, (%rax,%rsi,2)
	addq	$4, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_157
# BB#158:                               # %middle.block
                                        #   in Loop: Header=BB2_203 Depth=1
	testq	%rbx, %rbx
	jne	.LBB2_160
	jmp	.LBB2_162
.LBB2_159:                              #   in Loop: Header=BB2_203 Depth=1
	xorl	%ecx, %ecx
.LBB2_160:                              # %.lr.ph761.preheader968
                                        #   in Loop: Header=BB2_203 Depth=1
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB2_161:                              # %.lr.ph761
                                        #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx,4), %ebx
	movb	%bl, (%rax,%rcx,2)
	movb	%bh, 1(%rax,%rcx,2)  # NOREX
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB2_161
.LBB2_162:                              #   in Loop: Header=BB2_203 Depth=1
	movq	%rax, %rcx
.LBB2_163:                              # %._crit_edge762
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	40(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp162:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rsi
	movl	%ebp, %edx
	callq	*40(%rax)
.Ltmp163:
# BB#164:                               # %_ZN11CStringBaseIwED2Ev.exit413
                                        #   in Loop: Header=BB2_203 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	cmovnel	%eax, %r12d
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB2_166
# BB#165:                               #   in Loop: Header=BB2_203 Depth=1
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB2_166:                              # %_ZN7CBufferIhED2Ev.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	movl	%r12d, 8(%rsp)          # 4-byte Spill
.LBB2_167:                              # %_ZN7CBufferIhED2Ev.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	88(%rsp), %rdi
.Ltmp167:
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp168:
	movq	24(%rsp), %rbp          # 8-byte Reload
# BB#168:                               # %_ZN10CMyComBSTRD2Ev.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%ebx, %ebx
	jne	.LBB2_170
.LBB2_169:                              #   in Loop: Header=BB2_203 Depth=1
	xorl	%ebx, %ebx
.LBB2_170:                              #   in Loop: Header=BB2_203 Depth=1
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_172
# BB#171:                               #   in Loop: Header=BB2_203 Depth=1
	movq	(%rdi), %rax
.Ltmp172:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp173:
.LBB2_172:                              # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%ebx, %ebx
	jne	.LBB2_229
# BB#173:                               #   in Loop: Header=BB2_203 Depth=1
	movl	32(%rbp), %r14d
	movl	36(%rbp), %r12d
	leaq	328(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$8, 344(%rsp)
	movq	$_ZTV13CRecordVectorIPKyE+16, 320(%rsp)
	leaq	408(%rsp), %rax
	movdqu	%xmm0, (%rax)
	movq	$8, 424(%rsp)
	movq	$_ZTV13CRecordVectorIPKyE+16, 400(%rsp)
.Ltmp175:
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp176:
# BB#174:                               #   in Loop: Header=BB2_203 Depth=1
.Ltmp177:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	leaq	400(%rsp), %rdi
	movl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp178:
# BB#175:                               # %.preheader551
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%r12d, %r12d
	je	.LBB2_180
# BB#176:                               # %.lr.ph765.preheader
                                        #   in Loop: Header=BB2_203 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_177:                              # %.lr.ph765
                                        #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%r15), %rbx
.Ltmp180:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp181:
# BB#178:                               #   in Loop: Header=BB2_177 Depth=2
	leal	(%r13,%rbp), %eax
	cltq
	leaq	(%rbx,%rax,8), %rax
	movq	416(%rsp), %rcx
	movslq	412(%rsp), %rdx
	movq	%rax, (%rcx,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, 412(%rsp)
	incl	%ebp
	cmpl	%r12d, %ebp
	jb	.LBB2_177
# BB#179:                               # %.preheader549.loopexit
                                        #   in Loop: Header=BB2_203 Depth=1
	addl	%ebp, %r13d
	movq	%r13, 80(%rsp)          # 8-byte Spill
.LBB2_180:                              # %.preheader549
                                        #   in Loop: Header=BB2_203 Depth=1
	testl	%r14d, %r14d
	movl	316(%rsp), %r13d        # 4-byte Reload
	je	.LBB2_195
# BB#181:                               # %.lr.ph769.preheader
                                        #   in Loop: Header=BB2_203 Depth=1
	xorl	%r12d, %r12d
.LBB2_182:                              # %.lr.ph769
                                        #   Parent Loop BB2_203 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_184 Depth 3
                                        #       Child Loop BB2_190 Depth 3
	movslq	44(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB2_188
# BB#183:                               # %.lr.ph.i422
                                        #   in Loop: Header=BB2_182 Depth=2
	movq	48(%r15), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_184:                              #   Parent Loop BB2_203 Depth=1
                                        #     Parent Loop BB2_182 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r13d, (%rax,%rdx,8)
	je	.LBB2_186
# BB#185:                               #   in Loop: Header=BB2_184 Depth=3
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB2_184
	jmp	.LBB2_188
	.p2align	4, 0x90
.LBB2_186:                              # %_ZNK8NArchive3N7z7CFolder23FindBindPairForInStreamEj.exit
                                        #   in Loop: Header=BB2_182 Depth=2
	testl	%edx, %edx
	js	.LBB2_188
# BB#187:                               #   in Loop: Header=BB2_182 Depth=2
	movslq	4(%rax,%rdx,8), %rbx
	shlq	$3, %rbx
	addq	112(%r15), %rbx
.Ltmp186:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp187:
	jmp	.LBB2_194
	.p2align	4, 0x90
.LBB2_188:                              # %_ZNK8NArchive3N7z7CFolder23FindBindPairForInStreamEj.exit.thread
                                        #   in Loop: Header=BB2_182 Depth=2
	movslq	76(%r15), %rax
	testq	%rax, %rax
	movl	$1, %ebp
	jle	.LBB2_196
# BB#189:                               # %.lr.ph.i428
                                        #   in Loop: Header=BB2_182 Depth=2
	movq	80(%r15), %rcx
	movq	304(%rsp), %rbx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_190:                              #   Parent Loop BB2_203 Depth=1
                                        #     Parent Loop BB2_182 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r13d, (%rcx,%rdx,4)
	je	.LBB2_192
# BB#191:                               #   in Loop: Header=BB2_190 Depth=3
	incq	%rdx
	addq	$8, %rbx
	cmpq	%rax, %rdx
	jl	.LBB2_190
	jmp	.LBB2_196
	.p2align	4, 0x90
.LBB2_192:                              # %_ZNK8NArchive3N7z7CFolder24FindPackStreamArrayIndexEj.exit
                                        #   in Loop: Header=BB2_182 Depth=2
	testl	%edx, %edx
	js	.LBB2_196
# BB#193:                               #   in Loop: Header=BB2_182 Depth=2
.Ltmp183:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp184:
.LBB2_194:                              # %_ZN13CRecordVectorIPKyE3AddES1_.exit427
                                        #   in Loop: Header=BB2_182 Depth=2
	movq	336(%rsp), %rax
	movslq	332(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 332(%rsp)
	incl	%r12d
	incl	%r13d
	cmpl	%r14d, %r12d
	jb	.LBB2_182
	.p2align	4, 0x90
.LBB2_195:                              # %._crit_edge770
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	336(%rsp), %rdx
	movq	416(%rsp), %rcx
	xorl	%ebp, %ebp
.Ltmp189:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movq	360(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*16(%rax)
.Ltmp190:
	jmp	.LBB2_197
	.p2align	4, 0x90
.LBB2_196:                              #   in Loop: Header=BB2_203 Depth=1
	movl	$-2147467259, 8(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x80004005
.LBB2_197:                              # %_ZNK8NArchive3N7z7CFolder24FindPackStreamArrayIndexEj.exit.thread
                                        #   in Loop: Header=BB2_203 Depth=1
.Ltmp194:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp195:
	movq	16(%rsp), %r12          # 8-byte Reload
	leaq	208(%rsp), %r14
# BB#198:                               #   in Loop: Header=BB2_203 Depth=1
.Ltmp199:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp200:
# BB#199:                               #   in Loop: Header=BB2_203 Depth=1
	testl	%ebp, %ebp
	jne	.LBB2_241
# BB#200:                               #   in Loop: Header=BB2_203 Depth=1
	movq	360(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB2_203
.LBB2_212:                              # %._crit_edge782
	movl	156(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_216
# BB#213:                               # %.lr.ph.i441
	movq	544(%rsp), %r14
	movq	160(%rsp), %rdx
	movq	256(%rsp), %rcx
	movl	(%rcx), %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_214:                              # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rdi
	subl	4(%rdx,%rdi,8), %esi
	jb	.LBB2_218
# BB#215:                               #   in Loop: Header=BB2_214 Depth=1
	incl	%ecx
	cmpl	%eax, %ecx
	jb	.LBB2_214
.LBB2_216:                              # %._crit_edge.i442
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp266:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp267:
# BB#217:                               # %.noexc443
.LBB2_218:                              # %_ZNK11NCoderMixer9CBindInfo13FindOutStreamEjRjS1_.exit
	cmpb	$0, 168(%r12)
	je	.LBB2_220
# BB#219:
	movq	176(%r12), %rax
	movl	%ecx, 184(%rax)
.LBB2_220:
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB2_228
# BB#221:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 376(%rsp)
	movq	$8, 392(%rsp)
	movq	$_ZTV13CRecordVectorIP19ISequentialInStreamE+16, 368(%rsp)
	movl	124(%rsp), %esi
.Ltmp202:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp203:
# BB#222:                               # %.preheader
	movl	124(%rsp), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_226
# BB#223:                               # %.lr.ph
	xorl	%ebp, %ebp
	leaq	368(%rsp), %r15
	.p2align	4, 0x90
.LBB2_224:                              # =>This Inner Loop Header: Depth=1
	movq	128(%rsp), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %rbx
.Ltmp205:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp206:
# BB#225:                               #   in Loop: Header=BB2_224 Depth=1
	movq	384(%rsp), %rax
	movslq	380(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 380(%rsp)
	incq	%rbp
	movslq	124(%rsp), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB2_224
.LBB2_226:                              # %._crit_edge
	movq	432(%rsp), %rax         # 8-byte Reload
	movq	%rax, 320(%rsp)
	movq	192(%r12), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	384(%rsp), %rsi
.Ltmp208:
.Lcfi87:
	.cfi_escape 0x2e, 0x10
	leaq	320(%rsp), %r8
	movl	$0, %edx
	movl	$0, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%r14
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$16, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r12d
.Ltmp209:
# BB#227:
.Ltmp213:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp214:
	leaq	208(%rsp), %r14
	jmp	.LBB2_231
.LBB2_228:
	xorl	%r12d, %r12d
	jmp	.LBB2_230
.LBB2_229:
	movl	8(%rsp), %r12d          # 4-byte Reload
.LBB2_230:                              # %.thread512
	leaq	208(%rsp), %r14
.LBB2_231:                              # %.thread512
.Ltmp216:
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp217:
# BB#232:
.Ltmp238:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	leaq	176(%rsp), %rbx
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp239:
# BB#233:
.Ltmp243:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp244:
# BB#234:
.Ltmp248:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp249:
# BB#235:
.Ltmp254:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp255:
# BB#236:                               # %_ZN8NArchive3N7z11CBindInfoExD2Ev.exit440
	leaq	448(%rsp), %rdi
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_destroy
	movq	440(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_238
# BB#237:
	movq	(%rdi), %rax
.Ltmp257:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp258:
.LBB2_238:                              # %_ZN15CLockedInStreamD2Ev.exit436
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 112(%rsp)
.Ltmp260:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp261:
# BB#239:                               # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit387
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%r12d, %ecx
.LBB2_240:
	movl	%ecx, %eax
	addq	$488, %rsp              # imm = 0x1E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_241:
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB2_231
.LBB2_242:
.Ltmp268:
	jmp	.LBB2_336
.LBB2_243:
.Ltmp215:
	jmp	.LBB2_336
.LBB2_244:
.Ltmp210:
	jmp	.LBB2_302
.LBB2_245:                              # %.loopexit.split-lp
.Ltmp204:
	jmp	.LBB2_302
.LBB2_246:                              # %.thread518
.Ltmp159:
	jmp	.LBB2_256
.LBB2_247:
.Ltmp129:
	jmp	.LBB2_336
.LBB2_248:
.Ltmp164:
	movq	%rax, %r15
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB2_257
# BB#249:
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB2_257
.LBB2_250:
.Ltmp188:
	jmp	.LBB2_329
.LBB2_251:
.Ltmp150:
	jmp	.LBB2_336
.LBB2_252:
.Ltmp135:
	jmp	.LBB2_298
.LBB2_253:                              # %.loopexit.split-lp553
.Ltmp191:
	jmp	.LBB2_329
.LBB2_254:
.Ltmp169:
	jmp	.LBB2_294
.LBB2_255:
.Ltmp156:
.LBB2_256:                              # %_ZN7CBufferIhED2Ev.exit415
	movq	%rax, %r15
.LBB2_257:                              # %_ZN7CBufferIhED2Ev.exit415
	movq	88(%rsp), %rdi
.Ltmp165:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp166:
	jmp	.LBB2_295
.LBB2_258:
.Ltmp106:
	jmp	.LBB2_309
.LBB2_259:                              # %.body368.thread
.Ltmp98:
	movq	%rax, %r15
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB2_306
.LBB2_260:
.Ltmp259:
	movq	%rax, %r15
	jmp	.LBB2_344
.LBB2_261:
.Ltmp185:
	jmp	.LBB2_329
.LBB2_262:
.Ltmp174:
	jmp	.LBB2_336
.LBB2_263:
.Ltmp140:
	jmp	.LBB2_336
.LBB2_264:                              # %.body368.thread506
.Ltmp91:
	jmp	.LBB2_309
.LBB2_265:
.Ltmp145:
	movq	%rax, %r15
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_337
# BB#266:
	movq	(%rdi), %rax
.Ltmp146:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp147:
	jmp	.LBB2_337
.LBB2_267:
.Ltmp262:
	movq	%rax, %r15
.Ltmp263:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp264:
	jmp	.LBB2_346
.LBB2_268:
.Ltmp265:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_269:
.Ltmp256:
	jmp	.LBB2_326
.LBB2_270:
.Ltmp250:
	movq	%rax, %r15
	jmp	.LBB2_274
.LBB2_271:
.Ltmp245:
	movq	%rax, %r15
	jmp	.LBB2_273
.LBB2_272:
.Ltmp240:
	movq	%rax, %r15
.Ltmp241:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp242:
.LBB2_273:
.Ltmp246:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp247:
.LBB2_274:
.Ltmp251:
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp252:
	jmp	.LBB2_342
.LBB2_275:
.Ltmp253:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_276:
.Ltmp218:
	movq	%rax, %r15
.Ltmp219:
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp220:
# BB#277:
.Ltmp224:
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp225:
# BB#278:
.Ltmp229:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp230:
# BB#279:
.Ltmp235:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp236:
	jmp	.LBB2_342
.LBB2_280:
.Ltmp237:
	movq	%rax, %rbx
	jmp	.LBB2_286
.LBB2_281:
.Ltmp231:
	movq	%rax, %rbx
	jmp	.LBB2_285
.LBB2_282:
.Ltmp226:
	movq	%rax, %rbx
	jmp	.LBB2_284
.LBB2_283:
.Ltmp221:
	movq	%rax, %rbx
.Ltmp222:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp223:
.LBB2_284:
.Ltmp227:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp228:
.LBB2_285:
.Ltmp232:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp233:
.LBB2_286:                              # %.body449
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB2_287:
.Ltmp234:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_288:
.Ltmp2:
	movq	%rax, %r15
	movq	440(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_344
# BB#289:
	movq	(%rdi), %rax
.Ltmp3:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp4:
	jmp	.LBB2_344
.LBB2_290:
.Ltmp5:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_291:
.Ltmp201:
	jmp	.LBB2_336
.LBB2_292:
.Ltmp196:
	movq	%rax, %r15
	jmp	.LBB2_330
.LBB2_293:
.Ltmp153:
.LBB2_294:
	movq	%rax, %r15
.LBB2_295:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_337
# BB#296:
	movq	(%rdi), %rax
.Ltmp170:
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp171:
	jmp	.LBB2_337
.LBB2_297:
.Ltmp132:
.LBB2_298:
	movq	%rax, %r15
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_337
# BB#299:
	movq	(%rdi), %rax
.Ltmp136:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp137:
	jmp	.LBB2_337
.LBB2_300:
.Ltmp10:
	jmp	.LBB2_326
.LBB2_301:                              # %.loopexit
.Ltmp207:
.LBB2_302:
	movq	%rax, %r15
.Ltmp211:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp212:
	jmp	.LBB2_337
.LBB2_303:
.Ltmp111:
	movq	%rax, %r15
	jmp	.LBB2_312
.LBB2_304:
.Ltmp179:
	jmp	.LBB2_329
.LBB2_305:                              # %.body368
.Ltmp101:
	movq	%rax, %r15
	testq	%r13, %r13
	je	.LBB2_310
.LBB2_306:
	movq	(%r13), %rax
.Ltmp102:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp103:
	jmp	.LBB2_310
.LBB2_307:
.Ltmp116:
	jmp	.LBB2_336
.LBB2_308:
.Ltmp84:
.LBB2_309:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
	movq	%rax, %r15
.LBB2_310:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_312
# BB#311:
	movq	(%rdi), %rax
.Ltmp107:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp108:
.LBB2_312:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit375
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_337
# BB#313:
	movq	(%rdi), %rax
.Ltmp112:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp113:
	jmp	.LBB2_337
.LBB2_314:                              # %.loopexit.split-lp559
.Ltmp123:
	jmp	.LBB2_336
.LBB2_315:                              # %.loopexit.split-lp568.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp55:
	movq	%rax, %r15
	leaq	272(%rsp), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	jmp	.LBB2_337
.LBB2_316:                              # %.loopexit558
.Ltmp126:
	jmp	.LBB2_336
.LBB2_317:                              # %.loopexit567
.Ltmp69:
	jmp	.LBB2_336
.LBB2_318:
.Ltmp13:
	jmp	.LBB2_326
.LBB2_319:
.Ltmp16:
	jmp	.LBB2_326
.LBB2_320:
.Ltmp19:
	jmp	.LBB2_324
.LBB2_321:
.Ltmp22:
	jmp	.LBB2_324
.LBB2_322:
.Ltmp31:
	movq	%rax, %r15
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB2_333
.LBB2_323:
.Ltmp39:
.LBB2_324:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit340
	movq	%rax, %r15
	jmp	.LBB2_334
.LBB2_325:
.Ltmp44:
.LBB2_326:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit342
	movq	%rax, %r15
	jmp	.LBB2_342
.LBB2_327:                              # %.loopexit.split-lp568.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp58:
	jmp	.LBB2_336
.LBB2_328:                              # %.loopexit552
.Ltmp182:
.LBB2_329:
	movq	%rax, %r15
.Ltmp192:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp193:
.LBB2_330:
.Ltmp197:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp198:
	jmp	.LBB2_337
.LBB2_331:                              # %.loopexit.split-lp568.loopexit.split-lp.loopexit
.Ltmp63:
	jmp	.LBB2_336
.LBB2_332:
.Ltmp34:
	movq	%rax, %r15
.LBB2_333:
	movq	(%rbp), %rax
.Ltmp35:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp36:
.LBB2_334:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit340
	movq	(%rbx), %rax
.Ltmp40:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp41:
	jmp	.LBB2_342
.LBB2_335:                              # %.loopexit.split-lp568.loopexit
.Ltmp66:
.LBB2_336:
	movq	%rax, %r15
.LBB2_337:
.Ltmp269:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	movq	352(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp270:
# BB#338:
	leaq	240(%rsp), %rdi
.Ltmp291:
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp292:
# BB#339:
	leaq	208(%rsp), %rdi
.Ltmp296:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp297:
# BB#340:
	leaq	176(%rsp), %rdi
.Ltmp301:
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp302:
# BB#341:
.Ltmp307:
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp308:
.LBB2_342:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit342
	leaq	448(%rsp), %rdi
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_destroy
	movq	440(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_344
# BB#343:
	movq	(%rdi), %rax
.Ltmp309:
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp310:
.LBB2_344:
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 112(%rsp)
.Ltmp311:
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp312:
# BB#345:
.Ltmp317:
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp318:
.LBB2_346:                              # %unwind_resume
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB2_347:
.Ltmp303:
	movq	%rax, %rbx
	jmp	.LBB2_351
.LBB2_348:
.Ltmp298:
	movq	%rax, %rbx
	jmp	.LBB2_350
.LBB2_349:
.Ltmp293:
	movq	%rax, %rbx
	leaq	208(%rsp), %rdi
.Ltmp294:
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp295:
.LBB2_350:
	leaq	176(%rsp), %rdi
.Ltmp299:
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp300:
.LBB2_351:
.Ltmp304:
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp305:
	jmp	.LBB2_368
.LBB2_352:
.Ltmp306:
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_353:
.Ltmp271:
	movq	%rax, %rbx
	leaq	240(%rsp), %rdi
.Ltmp272:
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp273:
# BB#354:
	leaq	208(%rsp), %rdi
.Ltmp277:
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp278:
# BB#355:
	leaq	176(%rsp), %rdi
.Ltmp282:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp283:
# BB#356:
.Ltmp288:
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp289:
	jmp	.LBB2_368
.LBB2_357:
.Ltmp290:
	movq	%rax, %rbx
	jmp	.LBB2_363
.LBB2_358:
.Ltmp284:
	movq	%rax, %rbx
	jmp	.LBB2_362
.LBB2_359:
.Ltmp279:
	movq	%rax, %rbx
	jmp	.LBB2_361
.LBB2_360:
.Ltmp274:
	movq	%rax, %rbx
	leaq	208(%rsp), %rdi
.Ltmp275:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp276:
.LBB2_361:
	leaq	176(%rsp), %rdi
.Ltmp280:
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp281:
.LBB2_362:
.Ltmp285:
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp286:
.LBB2_363:                              # %.body398
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB2_364:
.Ltmp287:
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_365:
.Ltmp313:
	movq	%rax, %rbx
.Ltmp314:
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp315:
	jmp	.LBB2_368
.LBB2_366:
.Ltmp316:
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_367:
.Ltmp319:
	movq	%rax, %rbx
.LBB2_368:                              # %.body325
.Lcfi159:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj, .Lfunc_end2-_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\201\211\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\370\b"                # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp28-.Ltmp23         #   Call between .Ltmp23 and .Ltmp28
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin0   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp54-.Ltmp45         #   Call between .Ltmp45 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin0   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp62-.Ltmp59         #   Call between .Ltmp59 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp81-.Ltmp70         #   Call between .Ltmp70 and .Ltmp81
	.long	.Ltmp123-.Lfunc_begin0  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin0   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp101-.Lfunc_begin0  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp91-.Lfunc_begin0   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp95-.Ltmp87         #   Call between .Ltmp87 and .Ltmp95
	.long	.Ltmp101-.Lfunc_begin0  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin0   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin0  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 25 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin0  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin0   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin0  # >> Call Site 27 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin0  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin0  # >> Call Site 28 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin0  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin0  # >> Call Site 29 <<
	.long	.Ltmp122-.Ltmp117       #   Call between .Ltmp117 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin0  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin0  # >> Call Site 30 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin0  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin0  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin0  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin0  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin0  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp144-.Ltmp141       #   Call between .Ltmp141 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin0  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin0  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin0  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin0  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin0  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp163-.Ltmp160       #   Call between .Ltmp160 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin0  # >> Call Site 41 <<
	.long	.Ltmp168-.Ltmp167       #   Call between .Ltmp167 and .Ltmp168
	.long	.Ltmp169-.Lfunc_begin0  #     jumps to .Ltmp169
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin0  # >> Call Site 42 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin0  #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin0  # >> Call Site 43 <<
	.long	.Ltmp178-.Ltmp175       #   Call between .Ltmp175 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin0  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin0  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin0  # >> Call Site 45 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin0  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin0  # >> Call Site 46 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin0  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin0  # >> Call Site 47 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin0  #     jumps to .Ltmp191
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin0  # >> Call Site 48 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin0  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin0  # >> Call Site 49 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin0  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin0  # >> Call Site 50 <<
	.long	.Ltmp266-.Ltmp200       #   Call between .Ltmp200 and .Ltmp266
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin0  # >> Call Site 51 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin0  #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin0  # >> Call Site 52 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin0  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin0  # >> Call Site 53 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin0  # >> Call Site 54 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin0  #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin0  # >> Call Site 55 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin0  #     jumps to .Ltmp215
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin0  # >> Call Site 56 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin0  #     jumps to .Ltmp218
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin0  # >> Call Site 57 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	.Ltmp240-.Lfunc_begin0  #     jumps to .Ltmp240
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin0  # >> Call Site 58 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin0  #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin0  # >> Call Site 59 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin0  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin0  # >> Call Site 60 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin0  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin0  # >> Call Site 61 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin0  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin0  # >> Call Site 62 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin0  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp261-.Lfunc_begin0  # >> Call Site 63 <<
	.long	.Ltmp165-.Ltmp261       #   Call between .Ltmp261 and .Ltmp165
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin0  # >> Call Site 64 <<
	.long	.Ltmp147-.Ltmp165       #   Call between .Ltmp165 and .Ltmp147
	.long	.Ltmp319-.Lfunc_begin0  #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp263-.Lfunc_begin0  # >> Call Site 65 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin0  #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp241-.Lfunc_begin0  # >> Call Site 66 <<
	.long	.Ltmp252-.Ltmp241       #   Call between .Ltmp241 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin0  #     jumps to .Ltmp253
	.byte	1                       #   On action: 1
	.long	.Ltmp219-.Lfunc_begin0  # >> Call Site 67 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin0  #     jumps to .Ltmp221
	.byte	1                       #   On action: 1
	.long	.Ltmp224-.Lfunc_begin0  # >> Call Site 68 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin0  #     jumps to .Ltmp226
	.byte	1                       #   On action: 1
	.long	.Ltmp229-.Lfunc_begin0  # >> Call Site 69 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin0  #     jumps to .Ltmp231
	.byte	1                       #   On action: 1
	.long	.Ltmp235-.Lfunc_begin0  # >> Call Site 70 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin0  #     jumps to .Ltmp237
	.byte	1                       #   On action: 1
	.long	.Ltmp222-.Lfunc_begin0  # >> Call Site 71 <<
	.long	.Ltmp233-.Ltmp222       #   Call between .Ltmp222 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 72 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp170-.Lfunc_begin0  # >> Call Site 73 <<
	.long	.Ltmp41-.Ltmp170        #   Call between .Ltmp170 and .Ltmp41
	.long	.Ltmp319-.Lfunc_begin0  #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp269-.Lfunc_begin0  # >> Call Site 74 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin0  #     jumps to .Ltmp271
	.byte	1                       #   On action: 1
	.long	.Ltmp291-.Lfunc_begin0  # >> Call Site 75 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin0  #     jumps to .Ltmp293
	.byte	1                       #   On action: 1
	.long	.Ltmp296-.Lfunc_begin0  # >> Call Site 76 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin0  #     jumps to .Ltmp298
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin0  # >> Call Site 77 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	1                       #   On action: 1
	.long	.Ltmp307-.Lfunc_begin0  # >> Call Site 78 <<
	.long	.Ltmp310-.Ltmp307       #   Call between .Ltmp307 and .Ltmp310
	.long	.Ltmp319-.Lfunc_begin0  #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp311-.Lfunc_begin0  # >> Call Site 79 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin0  #     jumps to .Ltmp313
	.byte	1                       #   On action: 1
	.long	.Ltmp317-.Lfunc_begin0  # >> Call Site 80 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin0  #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp318-.Lfunc_begin0  # >> Call Site 81 <<
	.long	.Ltmp294-.Ltmp318       #   Call between .Ltmp318 and .Ltmp294
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin0  # >> Call Site 82 <<
	.long	.Ltmp305-.Ltmp294       #   Call between .Ltmp294 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin0  #     jumps to .Ltmp306
	.byte	1                       #   On action: 1
	.long	.Ltmp272-.Lfunc_begin0  # >> Call Site 83 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin0  #     jumps to .Ltmp274
	.byte	1                       #   On action: 1
	.long	.Ltmp277-.Lfunc_begin0  # >> Call Site 84 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin0  #     jumps to .Ltmp279
	.byte	1                       #   On action: 1
	.long	.Ltmp282-.Lfunc_begin0  # >> Call Site 85 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin0  #     jumps to .Ltmp284
	.byte	1                       #   On action: 1
	.long	.Ltmp288-.Lfunc_begin0  # >> Call Site 86 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin0  #     jumps to .Ltmp290
	.byte	1                       #   On action: 1
	.long	.Ltmp275-.Lfunc_begin0  # >> Call Site 87 <<
	.long	.Ltmp286-.Ltmp275       #   Call between .Ltmp275 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin0  #     jumps to .Ltmp287
	.byte	1                       #   On action: 1
	.long	.Ltmp314-.Lfunc_begin0  # >> Call Site 88 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin0  #     jumps to .Ltmp316
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp320:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp321:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB3_2:
.Ltmp322:
	movq	%rax, %r14
.Ltmp323:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp324:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp325:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev, .Lfunc_end3-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp320-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin1  #     jumps to .Ltmp322
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp323-.Ltmp321       #   Call between .Ltmp321 and .Ltmp323
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp323-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin1  #     jumps to .Ltmp325
	.byte	1                       #   On action: 1
	.long	.Ltmp324-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp324    #   Call between .Ltmp324 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 32
.Lcfi168:
	.cfi_offset %rbx, -24
.Lcfi169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp326:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp327:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp328:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev, .Lfunc_end4-_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp326-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin2  #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp327    #   Call between .Ltmp327 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi172:
	.cfi_def_cfa_offset 32
.Lcfi173:
	.cfi_offset %rbx, -24
.Lcfi174:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp329:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp330:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_2:
.Ltmp331:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev, .Lfunc_end5-_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp329-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin3  #     jumps to .Ltmp331
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp330    #   Call between .Ltmp330 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 32
.Lcfi178:
	.cfi_offset %rbx, -24
.Lcfi179:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp332:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp333:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp334:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end6-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp332-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin4  #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp333    #   Call between .Ltmp333 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi182:
	.cfi_def_cfa_offset 32
.Lcfi183:
	.cfi_offset %rbx, -24
.Lcfi184:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp335:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp336:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_2:
.Ltmp337:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end7-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp335-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin5  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp336-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp336    #   Call between .Ltmp336 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED2Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED2Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED2Ev,@function
_ZN13CObjectVectorI13CStreamBinderED2Ev: # @_ZN13CObjectVectorI13CStreamBinderED2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi187:
	.cfi_def_cfa_offset 32
.Lcfi188:
	.cfi_offset %rbx, -24
.Lcfi189:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp338:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp339:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB8_2:
.Ltmp340:
	movq	%rax, %r14
.Ltmp341:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp342:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp343:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorI13CStreamBinderED2Ev, .Lfunc_end8-_ZN13CObjectVectorI13CStreamBinderED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp338-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin6  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp339-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp341-.Ltmp339       #   Call between .Ltmp339 and .Ltmp341
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin6  #     jumps to .Ltmp343
	.byte	1                       #   On action: 1
	.long	.Ltmp342-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp342    #   Call between .Ltmp342 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED0Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED0Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED0Ev,@function
_ZN13CObjectVectorI13CStreamBinderED0Ev: # @_ZN13CObjectVectorI13CStreamBinderED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi192:
	.cfi_def_cfa_offset 32
.Lcfi193:
	.cfi_offset %rbx, -24
.Lcfi194:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp344:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp345:
# BB#1:
.Ltmp350:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp351:
# BB#2:                                 # %_ZN13CObjectVectorI13CStreamBinderED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_5:
.Ltmp352:
	movq	%rax, %r14
	jmp	.LBB9_6
.LBB9_3:
.Ltmp346:
	movq	%rax, %r14
.Ltmp347:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp348:
.LBB9_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp349:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN13CObjectVectorI13CStreamBinderED0Ev, .Lfunc_end9-_ZN13CObjectVectorI13CStreamBinderED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp344-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin7  #     jumps to .Ltmp346
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin7  #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin7  #     jumps to .Ltmp349
	.byte	1                       #   On action: 1
	.long	.Ltmp348-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp348    #   Call between .Ltmp348 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,@function
_ZN13CObjectVectorI13CStreamBinderE6DeleteEii: # @_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi199:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi201:
	.cfi_def_cfa_offset 80
.Lcfi202:
	.cfi_offset %rbx, -56
.Lcfi203:
	.cfi_offset %r12, -48
.Lcfi204:
	.cfi_offset %r13, -40
.Lcfi205:
	.cfi_offset %r14, -32
.Lcfi206:
	.cfi_offset %r15, -24
.Lcfi207:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB10_10
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB10_9
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB10_7
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	cmpb	$0, 88(%rbx)
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
	callq	pthread_cond_destroy
.LBB10_6:                               # %_ZN8NWindows16NSynchronization8CSynchroD2Ev.exit.i
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB10_7:                               #   in Loop: Header=BB10_2 Depth=1
	movq	$0, 152(%rbp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 128(%rbp)
	movq	$0, 136(%rbp)
	leaq	24(%rbp), %rdi
.Ltmp353:
	callq	Event_Close
.Ltmp354:
# BB#8:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB10_9:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB10_2
.LBB10_10:                              # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB10_11:                              # %.body
.Ltmp355:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii, .Lfunc_end10-_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp353-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin8  #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp354   #   Call between .Ltmp354 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB11_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB11_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB11_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB11_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end11:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end11-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi210:
	.cfi_def_cfa_offset 32
.Lcfi211:
	.cfi_offset %rbx, -24
.Lcfi212:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp356:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp357:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_2:
.Ltmp358:
	movq	%rax, %r14
.Ltmp359:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp360:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp361:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev, .Lfunc_end12-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp356-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin9  #     jumps to .Ltmp358
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp359-.Ltmp357       #   Call between .Ltmp357 and .Ltmp359
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin9  #     jumps to .Ltmp361
	.byte	1                       #   On action: 1
	.long	.Ltmp360-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp360   #   Call between .Ltmp360 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi215:
	.cfi_def_cfa_offset 32
.Lcfi216:
	.cfi_offset %rbx, -24
.Lcfi217:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp362:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp363:
# BB#1:
.Ltmp368:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp369:
# BB#2:                                 # %_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_5:
.Ltmp370:
	movq	%rax, %r14
	jmp	.LBB13_6
.LBB13_3:
.Ltmp364:
	movq	%rax, %r14
.Ltmp365:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp366:
.LBB13_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp367:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev, .Lfunc_end13-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp362-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin10 #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin10 #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin10 #     jumps to .Ltmp367
	.byte	1                       #   On action: 1
	.long	.Ltmp366-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp366   #   Call between .Ltmp366 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi218:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi219:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi220:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi221:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi222:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi224:
	.cfi_def_cfa_offset 64
.Lcfi225:
	.cfi_offset %rbx, -56
.Lcfi226:
	.cfi_offset %r12, -48
.Lcfi227:
	.cfi_offset %r13, -40
.Lcfi228:
	.cfi_offset %r14, -32
.Lcfi229:
	.cfi_offset %r15, -24
.Lcfi230:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
.Ltmp371:
	movq	%rbp, %rdi
	callq	_ZN11NCoderMixer7CCoder2D2Ev
.Ltmp372:
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB14_2
.LBB14_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB14_7:
.Ltmp373:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii, .Lfunc_end14-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp371-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin11 #     jumps to .Ltmp373
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp372   #   Call between .Ltmp372 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11NCoderMixer7CCoder2D2Ev,"axG",@progbits,_ZN11NCoderMixer7CCoder2D2Ev,comdat
	.weak	_ZN11NCoderMixer7CCoder2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder2D2Ev,@function
_ZN11NCoderMixer7CCoder2D2Ev:           # @_ZN11NCoderMixer7CCoder2D2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 32
.Lcfi234:
	.cfi_offset %rbx, -32
.Lcfi235:
	.cfi_offset %r14, -24
.Lcfi236:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTVN11NCoderMixer7CCoder2E+16, (%r15)
	leaq	496(%r15), %rdi
.Ltmp374:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp375:
# BB#1:
	leaq	464(%r15), %rdi
.Ltmp379:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp380:
# BB#2:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp390:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp391:
# BB#3:
.Ltmp396:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp397:
# BB#4:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp407:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp408:
# BB#5:
.Ltmp413:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp414:
# BB#6:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
.Ltmp418:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp419:
# BB#7:
	addq	$240, %r15
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN11NCoderMixer11CCoderInfo2D2Ev # TAILCALL
.LBB15_25:
.Ltmp420:
	movq	%rax, %r14
	jmp	.LBB15_26
.LBB15_22:
.Ltmp415:
	movq	%rax, %r14
	jmp	.LBB15_19
.LBB15_10:
.Ltmp409:
	movq	%rax, %r14
.Ltmp410:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp411:
	jmp	.LBB15_19
.LBB15_11:
.Ltmp412:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_13:
.Ltmp398:
	movq	%rax, %r14
	jmp	.LBB15_17
.LBB15_8:
.Ltmp392:
	movq	%rax, %r14
.Ltmp393:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp394:
	jmp	.LBB15_17
.LBB15_9:
.Ltmp395:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_14:
.Ltmp381:
	movq	%rax, %r14
	jmp	.LBB15_15
.LBB15_12:
.Ltmp376:
	movq	%rax, %r14
	leaq	464(%r15), %rdi
.Ltmp377:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp378:
.LBB15_15:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp382:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp383:
# BB#16:
.Ltmp388:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp389:
.LBB15_17:                              # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit10
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp399:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp400:
# BB#18:
.Ltmp405:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp406:
.LBB15_19:                              # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit13
.Ltmp416:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp417:
.LBB15_26:
	addq	$240, %r15
.Ltmp421:
	movq	%r15, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2D2Ev
.Ltmp422:
# BB#27:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_20:
.Ltmp384:
	movq	%rax, %r14
.Ltmp385:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp386:
	jmp	.LBB15_29
.LBB15_21:
.Ltmp387:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_23:
.Ltmp401:
	movq	%rax, %r14
.Ltmp402:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp403:
	jmp	.LBB15_29
.LBB15_24:
.Ltmp404:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_28:
.Ltmp423:
	movq	%rax, %r14
.LBB15_29:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN11NCoderMixer7CCoder2D2Ev, .Lfunc_end15-_ZN11NCoderMixer7CCoder2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp374-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin12 #     jumps to .Ltmp376
	.byte	0                       #   On action: cleanup
	.long	.Ltmp379-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp380-.Ltmp379       #   Call between .Ltmp379 and .Ltmp380
	.long	.Ltmp381-.Lfunc_begin12 #     jumps to .Ltmp381
	.byte	0                       #   On action: cleanup
	.long	.Ltmp390-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp391-.Ltmp390       #   Call between .Ltmp390 and .Ltmp391
	.long	.Ltmp392-.Lfunc_begin12 #     jumps to .Ltmp392
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin12 #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin12 #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin12 #     jumps to .Ltmp415
	.byte	0                       #   On action: cleanup
	.long	.Ltmp418-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp419-.Ltmp418       #   Call between .Ltmp418 and .Ltmp419
	.long	.Ltmp420-.Lfunc_begin12 #     jumps to .Ltmp420
	.byte	0                       #   On action: cleanup
	.long	.Ltmp419-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp410-.Ltmp419       #   Call between .Ltmp419 and .Ltmp410
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin12 #     jumps to .Ltmp412
	.byte	1                       #   On action: 1
	.long	.Ltmp393-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin12 #     jumps to .Ltmp395
	.byte	1                       #   On action: 1
	.long	.Ltmp377-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp423-.Lfunc_begin12 #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp382-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp383-.Ltmp382       #   Call between .Ltmp382 and .Ltmp383
	.long	.Ltmp384-.Lfunc_begin12 #     jumps to .Ltmp384
	.byte	1                       #   On action: 1
	.long	.Ltmp388-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp423-.Lfunc_begin12 #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp399-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp400-.Ltmp399       #   Call between .Ltmp399 and .Ltmp400
	.long	.Ltmp401-.Lfunc_begin12 #     jumps to .Ltmp401
	.byte	1                       #   On action: 1
	.long	.Ltmp405-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Ltmp422-.Ltmp405       #   Call between .Ltmp405 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin12 #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp422-.Lfunc_begin12 # >> Call Site 16 <<
	.long	.Ltmp385-.Ltmp422       #   Call between .Ltmp422 and .Ltmp385
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp385-.Lfunc_begin12 # >> Call Site 17 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin12 #     jumps to .Ltmp387
	.byte	1                       #   On action: 1
	.long	.Ltmp402-.Lfunc_begin12 # >> Call Site 18 <<
	.long	.Ltmp403-.Ltmp402       #   Call between .Ltmp402 and .Ltmp403
	.long	.Ltmp404-.Lfunc_begin12 #     jumps to .Ltmp404
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 32
.Lcfi240:
	.cfi_offset %rbx, -24
.Lcfi241:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp424:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp425:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB16_2:
.Ltmp426:
	movq	%rax, %r14
.Ltmp427:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp428:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp429:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev, .Lfunc_end16-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp424-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin13 #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp425-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp427-.Ltmp425       #   Call between .Ltmp425 and .Ltmp427
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin13 #     jumps to .Ltmp429
	.byte	1                       #   On action: 1
	.long	.Ltmp428-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp428   #   Call between .Ltmp428 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer11CCoderInfo2D2Ev,"axG",@progbits,_ZN11NCoderMixer11CCoderInfo2D2Ev,comdat
	.weak	_ZN11NCoderMixer11CCoderInfo2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo2D2Ev,@function
_ZN11NCoderMixer11CCoderInfo2D2Ev:      # @_ZN11NCoderMixer11CCoderInfo2D2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi243:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi244:
	.cfi_def_cfa_offset 32
.Lcfi245:
	.cfi_offset %rbx, -24
.Lcfi246:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	120(%rbx), %rdi
.Ltmp430:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp431:
# BB#1:
	leaq	88(%rbx), %rdi
.Ltmp435:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp436:
# BB#2:
	leaq	56(%rbx), %rdi
.Ltmp440:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp441:
# BB#3:
	leaq	24(%rbx), %rdi
.Ltmp445:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp446:
# BB#4:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp450:
	callq	*16(%rax)
.Ltmp451:
.LBB17_6:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_7
# BB#21:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB17_7:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB17_13:
.Ltmp452:
	movq	%rax, %r14
	jmp	.LBB17_17
.LBB17_14:
.Ltmp447:
	movq	%rax, %r14
	jmp	.LBB17_15
.LBB17_11:
.Ltmp442:
	movq	%rax, %r14
	jmp	.LBB17_12
.LBB17_9:
.Ltmp437:
	movq	%rax, %r14
	jmp	.LBB17_10
.LBB17_8:
.Ltmp432:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp433:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp434:
.LBB17_10:
	leaq	56(%rbx), %rdi
.Ltmp438:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp439:
.LBB17_12:
	leaq	24(%rbx), %rdi
.Ltmp443:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp444:
.LBB17_15:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp448:
	callq	*16(%rax)
.Ltmp449:
.LBB17_17:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit7
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp453:
	callq	*16(%rax)
.Ltmp454:
.LBB17_19:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_20:
.Ltmp455:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN11NCoderMixer11CCoderInfo2D2Ev, .Lfunc_end17-_ZN11NCoderMixer11CCoderInfo2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp430-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin14 #     jumps to .Ltmp432
	.byte	0                       #   On action: cleanup
	.long	.Ltmp435-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp436-.Ltmp435       #   Call between .Ltmp435 and .Ltmp436
	.long	.Ltmp437-.Lfunc_begin14 #     jumps to .Ltmp437
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin14 #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp445-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp446-.Ltmp445       #   Call between .Ltmp445 and .Ltmp446
	.long	.Ltmp447-.Lfunc_begin14 #     jumps to .Ltmp447
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp451-.Ltmp450       #   Call between .Ltmp450 and .Ltmp451
	.long	.Ltmp452-.Lfunc_begin14 #     jumps to .Ltmp452
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp433-.Ltmp451       #   Call between .Ltmp451 and .Ltmp433
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp433-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp454-.Ltmp433       #   Call between .Ltmp433 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin14 #     jumps to .Ltmp455
	.byte	1                       #   On action: 1
	.long	.Ltmp454-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Lfunc_end17-.Ltmp454   #   Call between .Ltmp454 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi249:
	.cfi_def_cfa_offset 32
.Lcfi250:
	.cfi_offset %rbx, -24
.Lcfi251:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp456:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp457:
# BB#1:
.Ltmp462:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp463:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp464:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp458:
	movq	%rax, %r14
.Ltmp459:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp460:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp461:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev, .Lfunc_end18-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp456-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp457-.Ltmp456       #   Call between .Ltmp456 and .Ltmp457
	.long	.Ltmp458-.Lfunc_begin15 #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp462-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp463-.Ltmp462       #   Call between .Ltmp462 and .Ltmp463
	.long	.Ltmp464-.Lfunc_begin15 #     jumps to .Ltmp464
	.byte	0                       #   On action: cleanup
	.long	.Ltmp459-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp460-.Ltmp459       #   Call between .Ltmp459 and .Ltmp460
	.long	.Ltmp461-.Lfunc_begin15 #     jumps to .Ltmp461
	.byte	1                       #   On action: 1
	.long	.Ltmp460-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp460   #   Call between .Ltmp460 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi256:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi258:
	.cfi_def_cfa_offset 64
.Lcfi259:
	.cfi_offset %rbx, -56
.Lcfi260:
	.cfi_offset %r12, -48
.Lcfi261:
	.cfi_offset %r13, -40
.Lcfi262:
	.cfi_offset %r14, -32
.Lcfi263:
	.cfi_offset %r15, -24
.Lcfi264:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB19_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_6
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	(%rdi), %rax
.Ltmp465:
	callq	*16(%rax)
.Ltmp466:
.LBB19_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB19_2
.LBB19_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB19_8:
.Ltmp467:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp465-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp466-.Ltmp465       #   Call between .Ltmp465 and .Ltmp466
	.long	.Ltmp467-.Lfunc_begin16 #     jumps to .Ltmp467
	.byte	0                       #   On action: cleanup
	.long	.Ltmp466-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end19-.Ltmp466   #   Call between .Ltmp466 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11NCoderMixer9CBindInfoaSERKS0_,"axG",@progbits,_ZN11NCoderMixer9CBindInfoaSERKS0_,comdat
	.weak	_ZN11NCoderMixer9CBindInfoaSERKS0_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer9CBindInfoaSERKS0_,@function
_ZN11NCoderMixer9CBindInfoaSERKS0_:     # @_ZN11NCoderMixer9CBindInfoaSERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi269:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi270:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi271:
	.cfi_def_cfa_offset 64
.Lcfi272:
	.cfi_offset %rbx, -56
.Lcfi273:
	.cfi_offset %r12, -48
.Lcfi274:
	.cfi_offset %r13, -40
.Lcfi275:
	.cfi_offset %r14, -32
.Lcfi276:
	.cfi_offset %r15, -24
.Lcfi277:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r14), %r15d
	movl	12(%r13), %esi
	addl	%r15d, %esi
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r15d, %r15d
	jle	.LBB20_3
# BB#1:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r12
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movq	%r12, (%rax,%rcx,8)
	incl	12(%r13)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB20_2
.LBB20_3:                               # %_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEaSERKS2_.exit
	leaq	32(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	44(%r14), %r12d
	movl	44(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB20_6
# BB#4:                                 # %.lr.ph.i.i6
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_5:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rax
	movq	(%rax,%rbx,8), %rbp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r13), %rax
	movslq	44(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	44(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB20_5
.LBB20_6:                               # %_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEaSERKS2_.exit
	leaq	64(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	76(%r14), %r12d
	movl	76(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB20_9
# BB#7:                                 # %.lr.ph.i.i14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_8:                               # =>This Inner Loop Header: Depth=1
	movq	80(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%r13), %rax
	movslq	76(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	76(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB20_8
.LBB20_9:                               # %_ZN13CRecordVectorIjEaSERKS0_.exit
	leaq	96(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	108(%r14), %r12d
	movl	108(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB20_12
# BB#10:                                # %.lr.ph.i.i19
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_11:                              # =>This Inner Loop Header: Depth=1
	movq	112(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%r13), %rax
	movslq	108(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	108(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB20_11
.LBB20_12:                              # %_ZN13CRecordVectorIjEaSERKS0_.exit23
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN11NCoderMixer9CBindInfoaSERKS0_, .Lfunc_end20-_ZN11NCoderMixer9CBindInfoaSERKS0_
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 32
.Lcfi281:
	.cfi_offset %rbx, -24
.Lcfi282:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp468:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp469:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB21_2:
.Ltmp470:
	movq	%rax, %r14
.Ltmp471:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp472:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_4:
.Ltmp473:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev, .Lfunc_end21-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp468-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp469-.Ltmp468       #   Call between .Ltmp468 and .Ltmp469
	.long	.Ltmp470-.Lfunc_begin17 #     jumps to .Ltmp470
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp471-.Ltmp469       #   Call between .Ltmp469 and .Ltmp471
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp471-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp472-.Ltmp471       #   Call between .Ltmp471 and .Ltmp472
	.long	.Ltmp473-.Lfunc_begin17 #     jumps to .Ltmp473
	.byte	1                       #   On action: 1
	.long	.Ltmp472-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp472   #   Call between .Ltmp472 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi283:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi284:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi285:
	.cfi_def_cfa_offset 32
.Lcfi286:
	.cfi_offset %rbx, -24
.Lcfi287:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp474:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp475:
# BB#1:
.Ltmp480:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp481:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_5:
.Ltmp482:
	movq	%rax, %r14
	jmp	.LBB22_6
.LBB22_3:
.Ltmp476:
	movq	%rax, %r14
.Ltmp477:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp478:
.LBB22_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp479:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev, .Lfunc_end22-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp474-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp475-.Ltmp474       #   Call between .Ltmp474 and .Ltmp475
	.long	.Ltmp476-.Lfunc_begin18 #     jumps to .Ltmp476
	.byte	0                       #   On action: cleanup
	.long	.Ltmp480-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp481-.Ltmp480       #   Call between .Ltmp480 and .Ltmp481
	.long	.Ltmp482-.Lfunc_begin18 #     jumps to .Ltmp482
	.byte	0                       #   On action: cleanup
	.long	.Ltmp477-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp478-.Ltmp477       #   Call between .Ltmp477 and .Ltmp478
	.long	.Ltmp479-.Lfunc_begin18 #     jumps to .Ltmp479
	.byte	1                       #   On action: 1
	.long	.Ltmp478-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp478   #   Call between .Ltmp478 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi288:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi289:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi291:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi292:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi293:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi294:
	.cfi_def_cfa_offset 64
.Lcfi295:
	.cfi_offset %rbx, -56
.Lcfi296:
	.cfi_offset %r12, -48
.Lcfi297:
	.cfi_offset %r13, -40
.Lcfi298:
	.cfi_offset %r14, -32
.Lcfi299:
	.cfi_offset %r15, -24
.Lcfi300:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB23_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB23_6
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	(%rdi), %rax
.Ltmp483:
	callq	*16(%rax)
.Ltmp484:
.LBB23_5:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB23_6:                               #   in Loop: Header=BB23_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB23_2
.LBB23_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB23_8:
.Ltmp485:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii, .Lfunc_end23-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp483-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp484-.Ltmp483       #   Call between .Ltmp483 and .Ltmp484
	.long	.Ltmp485-.Lfunc_begin19 #     jumps to .Ltmp485
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp484   #   Call between .Ltmp484 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi303:
	.cfi_def_cfa_offset 32
.Lcfi304:
	.cfi_offset %rbx, -24
.Lcfi305:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp486:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp487:
# BB#1:
.Ltmp492:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp493:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB24_5:
.Ltmp494:
	movq	%rax, %r14
	jmp	.LBB24_6
.LBB24_3:
.Ltmp488:
	movq	%rax, %r14
.Ltmp489:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp490:
.LBB24_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_4:
.Ltmp491:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev, .Lfunc_end24-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp486-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp487-.Ltmp486       #   Call between .Ltmp486 and .Ltmp487
	.long	.Ltmp488-.Lfunc_begin20 #     jumps to .Ltmp488
	.byte	0                       #   On action: cleanup
	.long	.Ltmp492-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp493-.Ltmp492       #   Call between .Ltmp492 and .Ltmp493
	.long	.Ltmp494-.Lfunc_begin20 #     jumps to .Ltmp494
	.byte	0                       #   On action: cleanup
	.long	.Ltmp489-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp490-.Ltmp489       #   Call between .Ltmp489 and .Ltmp490
	.long	.Ltmp491-.Lfunc_begin20 #     jumps to .Ltmp491
	.byte	1                       #   On action: 1
	.long	.Ltmp490-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Lfunc_end24-.Ltmp490   #   Call between .Ltmp490 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi306:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi309:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi310:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi311:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi312:
	.cfi_def_cfa_offset 64
.Lcfi313:
	.cfi_offset %rbx, -56
.Lcfi314:
	.cfi_offset %r12, -48
.Lcfi315:
	.cfi_offset %r13, -40
.Lcfi316:
	.cfi_offset %r14, -32
.Lcfi317:
	.cfi_offset %r15, -24
.Lcfi318:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB25_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB25_6
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_5
# BB#4:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	(%rdi), %rax
.Ltmp495:
	callq	*16(%rax)
.Ltmp496:
.LBB25_5:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB25_6:                               #   in Loop: Header=BB25_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB25_2
.LBB25_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB25_8:
.Ltmp497:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii, .Lfunc_end25-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp495-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp496-.Ltmp495       #   Call between .Ltmp495 and .Ltmp496
	.long	.Ltmp497-.Lfunc_begin21 #     jumps to .Ltmp497
	.byte	0                       #   On action: cleanup
	.long	.Ltmp496-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp496   #   Call between .Ltmp496 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKyED0Ev,"axG",@progbits,_ZN13CRecordVectorIPKyED0Ev,comdat
	.weak	_ZN13CRecordVectorIPKyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKyED0Ev,@function
_ZN13CRecordVectorIPKyED0Ev:            # @_ZN13CRecordVectorIPKyED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi319:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi320:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi321:
	.cfi_def_cfa_offset 32
.Lcfi322:
	.cfi_offset %rbx, -24
.Lcfi323:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp498:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp499:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp500:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN13CRecordVectorIPKyED0Ev, .Lfunc_end26-_ZN13CRecordVectorIPKyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp498-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin22 #     jumps to .Ltmp500
	.byte	0                       #   On action: cleanup
	.long	.Ltmp499-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp499   #   Call between .Ltmp499 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIP19ISequentialInStreamED0Ev,"axG",@progbits,_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,comdat
	.weak	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,@function
_ZN13CRecordVectorIP19ISequentialInStreamED0Ev: # @_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi326:
	.cfi_def_cfa_offset 32
.Lcfi327:
	.cfi_offset %rbx, -24
.Lcfi328:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp501:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp502:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB27_2:
.Ltmp503:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev, .Lfunc_end27-_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp501-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp503-.Lfunc_begin23 #     jumps to .Ltmp503
	.byte	0                       #   On action: cleanup
	.long	.Ltmp502-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Lfunc_end27-.Ltmp502   #   Call between .Ltmp502 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.asciz	"13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 52

	.type	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 24

	.type	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.asciz	"13CRecordVectorIN11NCoderMixer9CBindPairEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CObjectVectorI13CStreamBinderE,@object # @_ZTV13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTV13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTV13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTV13CObjectVectorI13CStreamBinderE
	.p2align	3
_ZTV13CObjectVectorI13CStreamBinderE:
	.quad	0
	.quad	_ZTI13CObjectVectorI13CStreamBinderE
	.quad	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.size	_ZTV13CObjectVectorI13CStreamBinderE, 40

	.type	_ZTS13CObjectVectorI13CStreamBinderE,@object # @_ZTS13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTS13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTS13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTS13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTS13CObjectVectorI13CStreamBinderE:
	.asciz	"13CObjectVectorI13CStreamBinderE"
	.size	_ZTS13CObjectVectorI13CStreamBinderE, 33

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI13CStreamBinderE,@object # @_ZTI13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTI13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTI13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTI13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTI13CObjectVectorI13CStreamBinderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI13CStreamBinderE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI13CStreamBinderE, 24

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	3
_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.size	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE, 40

	.type	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.asciz	"13CObjectVectorIN11NCoderMixer7CCoder2EE"
	.size	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE, 41

	.type	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 52

	.type	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.asciz	"13CObjectVectorI9CMyComPtrI8IUnknownEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE, 39

	.type	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 51

	.type	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 24

	.type	_ZTV13CRecordVectorIPKyE,@object # @_ZTV13CRecordVectorIPKyE
	.section	.rodata._ZTV13CRecordVectorIPKyE,"aG",@progbits,_ZTV13CRecordVectorIPKyE,comdat
	.weak	_ZTV13CRecordVectorIPKyE
	.p2align	3
_ZTV13CRecordVectorIPKyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPKyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPKyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPKyE, 40

	.type	_ZTS13CRecordVectorIPKyE,@object # @_ZTS13CRecordVectorIPKyE
	.section	.rodata._ZTS13CRecordVectorIPKyE,"aG",@progbits,_ZTS13CRecordVectorIPKyE,comdat
	.weak	_ZTS13CRecordVectorIPKyE
	.p2align	4
_ZTS13CRecordVectorIPKyE:
	.asciz	"13CRecordVectorIPKyE"
	.size	_ZTS13CRecordVectorIPKyE, 21

	.type	_ZTI13CRecordVectorIPKyE,@object # @_ZTI13CRecordVectorIPKyE
	.section	.rodata._ZTI13CRecordVectorIPKyE,"aG",@progbits,_ZTI13CRecordVectorIPKyE,comdat
	.weak	_ZTI13CRecordVectorIPKyE
	.p2align	4
_ZTI13CRecordVectorIPKyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPKyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPKyE, 24

	.type	_ZTV13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTV13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTV13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTV13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTV13CRecordVectorIP19ISequentialInStreamE
	.p2align	3
_ZTV13CRecordVectorIP19ISequentialInStreamE:
	.quad	0
	.quad	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTS13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTS13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTS13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTS13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTS13CRecordVectorIP19ISequentialInStreamE:
	.asciz	"13CRecordVectorIP19ISequentialInStreamE"
	.size	_ZTS13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTI13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTI13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTI13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTI13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTI13CRecordVectorIP19ISequentialInStreamE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIP19ISequentialInStreamE, 24


	.globl	_ZN8NArchive3N7z8CDecoderC1Eb
	.type	_ZN8NArchive3N7z8CDecoderC1Eb,@function
_ZN8NArchive3N7z8CDecoderC1Eb = _ZN8NArchive3N7z8CDecoderC2Eb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
