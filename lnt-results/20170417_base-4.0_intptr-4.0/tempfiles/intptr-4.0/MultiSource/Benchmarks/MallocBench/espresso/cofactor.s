	.text
	.file	"cofactor.bc"
	.globl	cofactor
	.p2align	4, 0x90
	.type	cofactor,@function
cofactor:                               # @cofactor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	cube+80(%rip), %rax
	movq	(%rax), %r13
	movq	8(%r14), %rax
	subq	%r14, %rax
	shlq	$29, %rax
	movabsq	$8589934592, %rdi       # imm = 0x200000000
	addq	%rax, %rdi
	sarq	$29, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB0_3
.LBB0_2:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rbp
	movq	(%r14), %r15
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	set_diff
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addq	$16, %r12
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_33
# BB#4:                                 # %.lr.ph100.preheader
	addq	$24, %r14
	movslq	cube+4(%rip), %r8
.LBB0_5:                                # %.lr.ph100
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_23 Depth 3
                                        #       Child Loop BB0_26 Depth 3
                                        #         Child Loop BB0_29 Depth 4
                                        #     Child Loop BB0_11 Depth 2
                                        #       Child Loop BB0_14 Depth 3
                                        #         Child Loop BB0_17 Depth 4
	movslq	cube+108(%rip), %rcx
	cmpq	$-1, %rcx
	movq	cube+72(%rip), %r9
	movq	cube+48(%rip), %r10
	movq	cube+40(%rip), %r11
	movslq	cube+8(%rip), %r15
	je	.LBB0_11
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movl	cube+104(%rip), %r13d
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph100.split
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_23 Depth 3
                                        #       Child Loop BB0_26 Depth 3
                                        #         Child Loop BB0_29 Depth 4
	cmpq	%rbx, %rax
	je	.LBB0_8
# BB#19:                                #   in Loop: Header=BB0_7 Depth=2
	movl	(%rbx,%rcx,4), %edx
	andl	(%rax,%rcx,4), %edx
	movl	%edx, %esi
	shrl	%esi
	orl	%edx, %esi
	notl	%esi
	testl	%esi, %r13d
	jne	.LBB0_8
# BB#20:                                # %.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	cmpl	$2, %ecx
	jl	.LBB0_24
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rsi,4), %edx
	andl	(%rax,%rsi,4), %edx
	movl	%edx, %edi
	shrl	%edi
	orl	%edx, %edi
	andl	$1431655765, %edi       # imm = 0x55555555
	cmpl	$1431655765, %edi       # imm = 0x55555555
	jne	.LBB0_8
# BB#22:                                #   in Loop: Header=BB0_23 Depth=3
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB0_23
.LBB0_24:                               # %.loopexit85
                                        #   in Loop: Header=BB0_7 Depth=2
	cmpl	%r8d, %r15d
	jge	.LBB0_31
# BB#25:                                # %.lr.ph95.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	%r15, %rbp
.LBB0_26:                               # %.lr.ph95
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_29 Depth 4
	movslq	(%r10,%rbp,4), %rdi
	movslq	(%r11,%rbp,4), %rsi
	cmpl	%edi, %esi
	jg	.LBB0_8
# BB#27:                                # %.lr.ph92.preheader
                                        #   in Loop: Header=BB0_26 Depth=3
	movq	(%r9,%rbp,8), %rdx
	decq	%rsi
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph92
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        #       Parent Loop BB0_26 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	4(%rbx,%rsi,4), %r12d
	andl	4(%rax,%rsi,4), %r12d
	testl	4(%rdx,%rsi,4), %r12d
	jne	.LBB0_30
# BB#28:                                #   in Loop: Header=BB0_29 Depth=4
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB0_29
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_26 Depth=3
	incq	%rbp
	cmpq	%r8, %rbp
	jl	.LBB0_26
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_8:                                # %.backedge
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	jne	.LBB0_7
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph100.split.us
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_14 Depth 3
                                        #         Child Loop BB0_17 Depth 4
	cmpq	%rbx, %rax
	je	.LBB0_10
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	cmpl	%r8d, %r15d
	jge	.LBB0_32
# BB#13:                                # %.lr.ph95.us.preheader
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	%r15, %rcx
.LBB0_14:                               # %.lr.ph95.us
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_17 Depth 4
	movslq	(%r10,%rcx,4), %rdx
	movslq	(%r11,%rcx,4), %rdi
	cmpl	%edx, %edi
	jg	.LBB0_10
# BB#15:                                # %.lr.ph92.us.preheader
                                        #   in Loop: Header=BB0_14 Depth=3
	movq	(%r9,%rcx,8), %rsi
	decq	%rdi
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph92.us
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        #       Parent Loop BB0_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	4(%rbx,%rdi,4), %ebp
	andl	4(%rax,%rdi,4), %ebp
	testl	4(%rsi,%rdi,4), %ebp
	jne	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_17 Depth=4
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB0_17
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_14 Depth=3
	incq	%rcx
	cmpq	%r8, %rcx
	jl	.LBB0_14
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_10:                               # %.backedge.us
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	jne	.LBB0_11
	jmp	.LBB0_33
.LBB0_31:                               #   in Loop: Header=BB0_5 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_32:                               # %.outer
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	%rax, (%r12)
	addq	$8, %r12
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	jne	.LBB0_5
	jmp	.LBB0_33
.LBB0_9:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB0_33:                               # %.outer._crit_edge
	movq	$0, (%r12)
	addq	$8, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r12, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cofactor, .Lfunc_end0-cofactor
	.cfi_endproc

	.globl	scofactor
	.p2align	4, 0x90
	.type	scofactor,@function
scofactor:                              # @scofactor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	cube+80(%rip), %rax
	movq	8(%rax), %rbx
	movq	cube+40(%rip), %rax
	movslq	%edx, %rcx
	movslq	(%rax,%rcx,4), %r12
	movq	cube+48(%rip), %rax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	(%rax,%rcx,4), %rbp
	movq	8(%r15), %rax
	subq	%r15, %rax
	shlq	$29, %rax
	movabsq	$8589934592, %rdi       # imm = 0x200000000
	addq	%rax, %rdi
	sarq	$29, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ecx
	cmpl	$33, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jge	.LBB1_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB1_3
.LBB1_2:
	leal	-1(%rcx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	set_clear
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	set_diff
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rcx, %rdx
	callq	set_or
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addq	$16, %r13
	movq	cube+72(%rip), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	set_and
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB1_7
# BB#4:                                 # %.lr.ph.preheader
	addq	$24, %r15
	decq	%r12
	.p2align	4, 0x90
.LBB1_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
	cmpq	%r14, %rax
	movq	%r12, %rcx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx,%rcx,4), %edx
	testl	4(%rax,%rcx,4), %edx
	jne	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_9 Depth=2
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB1_9
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_10:                               # %.outer
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rax, (%r13)
	addq	$8, %r13
.LBB1_6:                                # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	(%r15), %rax
	addq	$8, %r15
	testq	%rax, %rax
	jne	.LBB1_5
.LBB1_7:                                # %.outer._crit_edge
	movq	$0, (%r13)
	addq	$8, %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r13, 8(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	scofactor, .Lfunc_end1-scofactor
	.cfi_endproc

	.globl	massive_count
	.p2align	4, 0x90
	.type	massive_count,@function
massive_count:                          # @massive_count
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	cdata(%rip), %rbx
	movslq	cube(%rip), %rax
	testq	%rax, %rax
	jle	.LBB2_2
# BB#1:                                 # %.lr.ph213.preheader
	movq	%rax, %rcx
	decq	%rcx
	movl	%eax, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rax,%rsi), %eax
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_2:                                # %._crit_edge214
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB2_77
# BB#3:                                 # %.lr.ph208.preheader
	movq	(%r14), %r8
	movq	cube+88(%rip), %r9
	addq	$24, %r14
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph208
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	movl	(%rax), %esi
	testw	$1023, %si              # imm = 0x3FF
	je	.LBB2_4
# BB#6:                                 # %.lr.ph206.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %rdi
	shll	$5, %esi
	addl	$-32, %esi
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph206
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r8,%rdi,4), %ecx
	orl	-4(%rax,%rdi,4), %ecx
	notl	%ecx
	andl	-4(%r9,%rdi,4), %ecx
	je	.LBB2_76
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=2
	movslq	%esi, %rdx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB2_25
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=2
	testl	%ecx, %ecx
	jns	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_7 Depth=2
	incl	124(%rbx,%rdx,4)
.LBB2_11:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$1073741824, %ecx       # imm = 0x40000000
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_7 Depth=2
	incl	120(%rbx,%rdx,4)
.LBB2_13:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$536870912, %ecx        # imm = 0x20000000
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_7 Depth=2
	incl	116(%rbx,%rdx,4)
.LBB2_15:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$268435456, %ecx        # imm = 0x10000000
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_7 Depth=2
	incl	112(%rbx,%rdx,4)
.LBB2_17:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$134217728, %ecx        # imm = 0x8000000
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_7 Depth=2
	incl	108(%rbx,%rdx,4)
.LBB2_19:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$67108864, %ecx         # imm = 0x4000000
	je	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_7 Depth=2
	incl	104(%rbx,%rdx,4)
.LBB2_21:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$33554432, %ecx         # imm = 0x2000000
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_7 Depth=2
	incl	100(%rbx,%rdx,4)
.LBB2_23:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$16777216, %ecx         # imm = 0x1000000
	je	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_7 Depth=2
	incl	96(%rbx,%rdx,4)
.LBB2_25:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$16711680, %ecx         # imm = 0xFF0000
	je	.LBB2_42
# BB#26:                                #   in Loop: Header=BB2_7 Depth=2
	testl	$8388608, %ecx          # imm = 0x800000
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_7 Depth=2
	incl	92(%rbx,%rdx,4)
.LBB2_28:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$4194304, %ecx          # imm = 0x400000
	je	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_7 Depth=2
	incl	88(%rbx,%rdx,4)
.LBB2_30:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$2097152, %ecx          # imm = 0x200000
	je	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_7 Depth=2
	incl	84(%rbx,%rdx,4)
.LBB2_32:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$1048576, %ecx          # imm = 0x100000
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_7 Depth=2
	incl	80(%rbx,%rdx,4)
.LBB2_34:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$524288, %ecx           # imm = 0x80000
	je	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_7 Depth=2
	incl	76(%rbx,%rdx,4)
.LBB2_36:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$262144, %ecx           # imm = 0x40000
	je	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_7 Depth=2
	incl	72(%rbx,%rdx,4)
.LBB2_38:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$131072, %ecx           # imm = 0x20000
	je	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_7 Depth=2
	incl	68(%rbx,%rdx,4)
.LBB2_40:                               #   in Loop: Header=BB2_7 Depth=2
	testl	$65536, %ecx            # imm = 0x10000
	je	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_7 Depth=2
	incl	64(%rbx,%rdx,4)
.LBB2_42:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$-1, %ch
	je	.LBB2_59
# BB#43:                                #   in Loop: Header=BB2_7 Depth=2
	testw	%cx, %cx
	jns	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_7 Depth=2
	incl	60(%rbx,%rdx,4)
.LBB2_45:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$64, %ch
	je	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_7 Depth=2
	incl	56(%rbx,%rdx,4)
.LBB2_47:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$32, %ch
	je	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_7 Depth=2
	incl	52(%rbx,%rdx,4)
.LBB2_49:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$16, %ch
	je	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_7 Depth=2
	incl	48(%rbx,%rdx,4)
.LBB2_51:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$8, %ch
	je	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_7 Depth=2
	incl	44(%rbx,%rdx,4)
.LBB2_53:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$4, %ch
	je	.LBB2_55
# BB#54:                                #   in Loop: Header=BB2_7 Depth=2
	incl	40(%rbx,%rdx,4)
.LBB2_55:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$2, %ch
	je	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_7 Depth=2
	incl	36(%rbx,%rdx,4)
.LBB2_57:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$1, %ch
	je	.LBB2_59
# BB#58:                                #   in Loop: Header=BB2_7 Depth=2
	incl	32(%rbx,%rdx,4)
.LBB2_59:                               #   in Loop: Header=BB2_7 Depth=2
	testb	%cl, %cl
	je	.LBB2_76
# BB#60:                                #   in Loop: Header=BB2_7 Depth=2
	testb	%cl, %cl
	jns	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_7 Depth=2
	incl	28(%rbx,%rdx,4)
.LBB2_62:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$64, %cl
	je	.LBB2_64
# BB#63:                                #   in Loop: Header=BB2_7 Depth=2
	incl	24(%rbx,%rdx,4)
.LBB2_64:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$32, %cl
	je	.LBB2_66
# BB#65:                                #   in Loop: Header=BB2_7 Depth=2
	incl	20(%rbx,%rdx,4)
.LBB2_66:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$16, %cl
	je	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_7 Depth=2
	incl	16(%rbx,%rdx,4)
.LBB2_68:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$8, %cl
	je	.LBB2_70
# BB#69:                                #   in Loop: Header=BB2_7 Depth=2
	incl	12(%rbx,%rdx,4)
.LBB2_70:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$4, %cl
	je	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_7 Depth=2
	incl	8(%rbx,%rdx,4)
.LBB2_72:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$2, %cl
	je	.LBB2_74
# BB#73:                                #   in Loop: Header=BB2_7 Depth=2
	incl	4(%rbx,%rdx,4)
.LBB2_74:                               #   in Loop: Header=BB2_7 Depth=2
	testb	$1, %cl
	je	.LBB2_76
# BB#75:                                #   in Loop: Header=BB2_7 Depth=2
	incl	(%rbx,%rdx,4)
	.p2align	4, 0x90
.LBB2_76:                               #   in Loop: Header=BB2_7 Depth=2
	decq	%rdi
	addl	$-32, %esi
	cmpq	$1, %rdi
	jg	.LBB2_7
.LBB2_4:                                # %.loopexit192
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	jne	.LBB2_5
.LBB2_77:                               # %._crit_edge209
	movq	$0, cdata+32(%rip)
	cmpl	$0, cube+4(%rip)
	jle	.LBB2_78
# BB#79:                                # %.lr.ph202
	movq	cdata+8(%rip), %rax
	movq	cdata+16(%rip), %r14
	movq	cdata+24(%rip), %r15
	movq	cube+24(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$-1, %r10d
	xorl	%edi, %edi
	movl	$32000, %r13d           # imm = 0x7D00
	movq	cube+16(%rip), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_80:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_84 Depth 2
	movslq	cube+8(%rip), %rcx
	cmpq	%rcx, %rdi
	jge	.LBB2_82
# BB#81:                                #   in Loop: Header=BB2_80 Depth=1
	leal	(%rdi,%rdi), %ecx
	movslq	%ecx, %rcx
	movl	(%rbx,%rcx,4), %ebp
	leal	1(%rdi,%rdi), %ecx
	movslq	%ecx, %rcx
	movl	(%rbx,%rcx,4), %ecx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	setg	%sil
	xorl	%edx, %edx
	testl	%ecx, %ecx
	setg	%dl
	addl	%esi, %edx
	leal	(%rcx,%rbp), %esi
	movl	%esi, (%rax,%rdi,4)
	cmpl	%ecx, %ebp
	cmovgel	%ebp, %ecx
	cmpl	%r12d, %edx
	jg	.LBB2_86
	jmp	.LBB2_87
	.p2align	4, 0x90
.LBB2_82:                               #   in Loop: Header=BB2_80 Depth=1
	movl	$0, (%rax,%rdi,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rdi,4), %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	(%rcx,%rdi,4), %r8
	xorl	%esi, %esi
	cmpl	%ebp, %r8d
	movl	$0, %edx
	movl	$0, %ecx
	jg	.LBB2_85
# BB#83:                                # %.lr.ph
                                        #   in Loop: Header=BB2_80 Depth=1
	decq	%r8
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_84:                               #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	4(%rbx,%r8,4), %esi
	movl	%esi, (%rax,%rdi,4)
	xorl	%r9d, %r9d
	cmpl	$0, 4(%rbx,%r8,4)
	setg	%r9b
	addl	%r9d, %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	incq	%r8
	cmpq	%rbp, %r8
	jl	.LBB2_84
.LBB2_85:                               # %.loopexit
                                        #   in Loop: Header=BB2_80 Depth=1
	cmpl	%r12d, %edx
	jle	.LBB2_87
.LBB2_86:                               #   in Loop: Header=BB2_80 Depth=1
	movl	%edi, %r10d
	movl	%edx, %r12d
	movl	%esi, %r11d
	jmp	.LBB2_92
	.p2align	4, 0x90
.LBB2_87:                               #   in Loop: Header=BB2_80 Depth=1
	jne	.LBB2_91
# BB#88:                                #   in Loop: Header=BB2_80 Depth=1
	cmpl	%r11d, %esi
	jle	.LBB2_90
# BB#89:                                #   in Loop: Header=BB2_80 Depth=1
	movl	%edi, %r10d
	movl	%esi, %r11d
	jmp	.LBB2_92
.LBB2_90:                               #   in Loop: Header=BB2_80 Depth=1
	sete	%sil
	cmpl	%r13d, %ecx
	setl	%r8b
	andb	%sil, %r8b
	cmovnel	%ecx, %r13d
	testb	%r8b, %r8b
	cmovnel	%edi, %r10d
.LBB2_91:                               #   in Loop: Header=BB2_80 Depth=1
	movl	%r13d, %ecx
.LBB2_92:                               #   in Loop: Header=BB2_80 Depth=1
	movl	%edx, (%r14,%rdi,4)
	xorl	%esi, %esi
	cmpl	$1, %edx
	sete	%sil
	movl	%esi, (%r15,%rdi,4)
	xorl	%ebp, %ebp
	testl	%edx, %edx
	setg	%bpl
	addl	%ebp, cdata+32(%rip)
	addl	%esi, cdata+36(%rip)
	incq	%rdi
	movslq	cube+4(%rip), %rdx
	cmpq	%rdx, %rdi
	movl	%ecx, %r13d
	jl	.LBB2_80
	jmp	.LBB2_93
.LBB2_78:
	movl	$-1, %r10d
.LBB2_93:                               # %._crit_edge
	movl	%r10d, cdata+40(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	massive_count, .Lfunc_end2-massive_count
	.cfi_endproc

	.globl	binate_split_select
	.p2align	4, 0x90
	.type	binate_split_select,@function
binate_split_select:                    # @binate_split_select
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %rcx
	movslq	cdata+40(%rip), %r14
	movq	cube+24(%rip), %rax
	movl	(%rax,%r14,4), %ebp
	movq	(%rdi), %r15
	movq	cube+88(%rip), %rsi
	movq	cube+72(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	callq	set_diff
	movq	cube+88(%rip), %rsi
	movq	cube+72(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_diff
	movq	cube+16(%rip), %rax
	movl	(%rax,%r14,4), %ecx
	cmpl	%ebp, %ecx
	jg	.LBB3_8
# BB#1:                                 # %.lr.ph58.preheader
	movl	%ecx, %eax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph58
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movl	4(%r15,%rdx,4), %edx
	xorl	%esi, %esi
	btl	%eax, %edx
	setae	%sil
	addl	%esi, %r12d
	cmpl	%ebp, %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jl	.LBB3_2
# BB#3:                                 # %._crit_edge59
	cmpl	$2, %r12d
	movq	16(%rsp), %r8           # 8-byte Reload
	jl	.LBB3_8
# BB#4:                                 # %.lr.ph54.preheader
	shrl	%r12d
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %esi
	movl	$1, %edx
	shll	%cl, %edx
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %edi
	btl	%edi, %esi
	jb	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	incq	%rax
	decl	%r12d
	orl	%edx, (%r8,%rax,4)
.LBB3_7:                                #   in Loop: Header=BB3_5 Depth=1
	incl	%ecx
	testl	%r12d, %r12d
	jg	.LBB3_5
.LBB3_8:                                # %.preheader
	cmpl	%ebp, %ecx
	jg	.LBB3_12
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %esi
	movl	$1, %edx
	shll	%cl, %edx
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %edi
	btl	%edi, %esi
	jb	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	incq	%rax
	orl	%edx, (%r13,%rax,4)
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	cmpl	%ebp, %ecx
	leal	1(%rcx), %eax
	movl	%eax, %ecx
	jl	.LBB3_9
.LBB3_12:                               # %._crit_edge
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	debug(%rip), %eax
	je	.LBB3_15
# BB#13:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movq	16(%rsp), %rdi          # 8-byte Reload
	cmpl	$0, verbose_debug(%rip)
	je	.LBB3_15
# BB#14:
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	printf
.LBB3_15:
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	binate_split_select, .Lfunc_end3-binate_split_select
	.cfi_endproc

	.globl	cube1list
	.p2align	4, 0x90
	.type	cube1list,@function
cube1list:                              # @cube1list
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	12(%r15), %rax
	leaq	24(,%rax,8), %rdi
	callq	malloc
	movq	%rax, %r14
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB4_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB4_3
.LBB4_2:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB4_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, (%r14)
	leaq	16(%r14), %rax
	movslq	(%r15), %rcx
	movslq	12(%r15), %rsi
	imulq	%rcx, %rsi
	testl	%esi, %esi
	jle	.LBB4_4
# BB#5:                                 # %.lr.ph
	movq	24(%r15), %rdx
	leaq	(%rdx,%rsi,4), %rsi
	shlq	$2, %rcx
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rax)
	addq	$8, %rax
	addq	%rcx, %rdx
	cmpq	%rsi, %rdx
	jb	.LBB4_6
# BB#7:                                 # %._crit_edge.loopexit
	movq	%rax, %rcx
	addq	$8, %rcx
	jmp	.LBB4_8
.LBB4_4:
	movq	%r14, %rcx
	addq	$24, %rcx
.LBB4_8:                                # %._crit_edge
	movq	$0, (%rax)
	movq	%rcx, 8(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	cube1list, .Lfunc_end4-cube1list
	.cfi_endproc

	.globl	cube2list
	.p2align	4, 0x90
	.type	cube2list,@function
cube2list:                              # @cube2list
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r13, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	12(%r12), %eax
	movl	12(%r15), %ecx
	leal	3(%rax,%rcx), %eax
	movslq	%eax, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, %r14
	movl	cube(%rip), %r13d
	cmpl	$33, %r13d
	jge	.LBB5_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB5_3
.LBB5_2:
	leal	-1(%r13), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB5_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r13d, %esi
	callq	set_clear
	movq	%rax, (%r14)
	addq	$16, %rbx
	movslq	(%r12), %rax
	movslq	12(%r12), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB5_6
# BB#4:                                 # %.lr.ph44
	movq	24(%r12), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB5_5
.LBB5_6:                                # %._crit_edge45
	movslq	(%r15), %rax
	movslq	12(%r15), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB5_9
# BB#7:                                 # %.lr.ph
	movq	24(%r15), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB5_8
.LBB5_9:                                # %._crit_edge.loopexit
	leaq	8(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, 8(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	cube2list, .Lfunc_end5-cube2list
	.cfi_endproc

	.globl	cube3list
	.p2align	4, 0x90
	.type	cube3list,@function
cube3list:                              # @cube3list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 64
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	12(%r13), %eax
	movl	12(%r15), %ecx
	addl	12(%r12), %eax
	leal	3(%rcx,%rax), %eax
	movslq	%eax, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, %r14
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB6_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB6_3
.LBB6_2:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB6_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, (%r14)
	addq	$16, %rbx
	movslq	(%r13), %rax
	movslq	12(%r13), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB6_6
# BB#4:                                 # %.lr.ph63
	movq	24(%r13), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB6_5
.LBB6_6:                                # %._crit_edge64
	movslq	(%r12), %rax
	movslq	12(%r12), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB6_9
# BB#7:                                 # %.lr.ph57
	movq	24(%r12), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB6_8
.LBB6_9:                                # %._crit_edge58
	movslq	(%r15), %rax
	movslq	12(%r15), %rdx
	imulq	%rax, %rdx
	testl	%edx, %edx
	jle	.LBB6_12
# BB#10:                                # %.lr.ph
	movq	24(%r15), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB6_11
.LBB6_12:                               # %._crit_edge.loopexit
	leaq	8(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, 8(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	cube3list, .Lfunc_end6-cube3list
	.cfi_endproc

	.globl	cubeunlist
	.p2align	4, 0x90
	.type	cubeunlist,@function
cubeunlist:                             # @cubeunlist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 128
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$4294967293, %ebp       # imm = 0xFFFFFFFD
	movq	(%r14), %rbx
	movq	8(%r14), %rdi
	subq	%r14, %rdi
	sarq	$3, %rdi
	addq	$-3, %rdi
	movl	cube(%rip), %esi
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	callq	sf_new
	movq	%r14, %rdi
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB7_18
# BB#1:                                 # %.lr.ph
	movq	24(%rax), %r14
	leaq	-4(%r14), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	-4(%rbx), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	$2, %r15d
	movl	$-1024, %r11d           # imm = 0xFC00
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_12 Depth 2
                                        #     Child Loop BB7_16 Depth 2
	movl	(%rax), %r8d
	leal	1(%r15,%rbp), %edx
	imull	%r8d, %edx
	movslq	%edx, %r13
	movl	(%rcx), %r10d
	movl	(%r14,%r13,4), %edx
	andl	%r11d, %edx
	andl	$1023, %r10d            # imm = 0x3FF
	movq	%r10, %r9
	movl	$1, %esi
	cmoveq	%rsi, %r9
	movl	%r10d, %esi
	orl	%edx, %esi
	movl	%esi, (%r14,%r13,4)
	cmpq	$8, %r9
	jb	.LBB7_15
# BB#3:                                 # %min.iters.checked
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%r9, %rdx
	andq	$1016, %rdx             # imm = 0x3F8
	je	.LBB7_15
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r10, %rdx
	notq	%rdx
	testl	%r10d, %r10d
	movq	$-2, %rax
	cmovneq	%rax, %rdx
	movq	%r13, %rsi
	subq	%rdx, %rsi
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,4), %rsi
	leaq	(%r13,%r10), %rdi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rdi
	shlq	$2, %rdx
	movq	%rcx, %rbp
	subq	%rdx, %rbp
	addq	$-4, %rbp
	leaq	4(%rcx,%r10,4), %r11
	movq	48(%rsp), %rax          # 8-byte Reload
	subq	%rdx, %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r10,4), %rdx
	cmpq	%r11, %rsi
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rbp
	sbbb	%bpl, %bpl
	andb	%r11b, %bpl
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rdi, %rax
	sbbb	%sil, %sil
	testb	$1, %bpl
	jne	.LBB7_5
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB7_2 Depth=1
	andb	%sil, %dl
	andb	$1, %dl
	jne	.LBB7_5
# BB#7:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	-8(%rax), %rax
	movq	%rax, %rdx
	shrq	$3, %rdx
	btl	$3, %eax
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$-1024, %r11d           # imm = 0xFC00
	jb	.LBB7_8
# BB#9:                                 # %vector.body.prol
                                        #   in Loop: Header=BB7_2 Depth=1
	leaq	(%r14,%r13,4), %rsi
	movups	-12(%rcx,%r10,4), %xmm0
	movups	-28(%rcx,%r10,4), %xmm1
	movups	-12(%rbx,%r10,4), %xmm2
	movups	-28(%rbx,%r10,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%r10,4)
	movups	%xmm3, -28(%rsi,%r10,4)
	movl	$8, %ebp
	testq	%rdx, %rdx
	jne	.LBB7_11
	jmp	.LBB7_13
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$4294967293, %ebp       # imm = 0xFFFFFFFD
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$-1024, %r11d           # imm = 0xFC00
	jmp	.LBB7_15
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	je	.LBB7_13
.LBB7_11:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%r9, %r13
	andq	$-8, %r13
	negq	%r13
	negq	%rbp
	leaq	-12(%rcx,%r10,4), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r10,4), %rdi
	movl	%r8d, %esi
	imull	%r12d, %esi
	movslq	%esi, %rsi
	addq	%r10, %rsi
	leaq	(%r14,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB7_12:                               # %vector.body
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbp,4), %xmm0
	movups	-16(%rdx,%rbp,4), %xmm1
	movups	(%rdi,%rbp,4), %xmm2
	movups	-16(%rdi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%rbp,4)
	movups	%xmm3, -28(%rsi,%rbp,4)
	movups	-32(%rdx,%rbp,4), %xmm0
	movups	-48(%rdx,%rbp,4), %xmm1
	movups	-32(%rdi,%rbp,4), %xmm2
	movups	-48(%rdi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -44(%rsi,%rbp,4)
	movups	%xmm3, -60(%rsi,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %r13
	jne	.LBB7_12
.LBB7_13:                               # %middle.block
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %r9
	movl	$4294967293, %ebp       # imm = 0xFFFFFFFD
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB7_17
# BB#14:                                #   in Loop: Header=BB7_2 Depth=1
	subq	%rdx, %r10
	.p2align	4, 0x90
.LBB7_15:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%r10
	imull	%r12d, %r8d
	movslq	%r8d, %rdx
	leaq	(%r14,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB7_16:                               # %scalar.ph
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%r10,4), %esi
	orl	-4(%rcx,%r10,4), %esi
	movl	%esi, -4(%rdx,%r10,4)
	decq	%r10
	cmpq	$1, %r10
	jg	.LBB7_16
.LBB7_17:                               # %.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rdi,%r15,8), %rcx
	incq	%r15
	incl	%r12d
	testq	%rcx, %rcx
	jne	.LBB7_2
.LBB7_18:                               # %._crit_edge
	movq	8(%rdi), %rcx
	subq	%rdi, %rcx
	shrq	$3, %rcx
	addl	%ecx, %ebp
	movl	%ebp, 12(%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cubeunlist, .Lfunc_end7-cubeunlist
	.cfi_endproc

	.globl	simplify_cubelist
	.p2align	4, 0x90
	.type	simplify_cubelist,@function
simplify_cubelist:                      # @simplify_cubelist
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 48
.Lcfi99:
	.cfi_offset %rbx, -40
.Lcfi100:
	.cfi_offset %r12, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	movq	(%r12), %rsi
	xorl	%eax, %eax
	callq	set_copy
	movq	8(%r12), %rax
	subq	%r12, %rax
	shrq	$3, %rax
	movl	$4294967293, %ebx       # imm = 0xFFFFFFFD
	addq	%rax, %rbx
	leaq	16(%r12), %r14
	movslq	%ebx, %r15
	movl	$8, %edx
	movl	$d1_order, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	qsort
	cmpl	$4, %r15d
	jl	.LBB8_5
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebx, %ebx
	addq	$24, %r12
	addq	$-3, %rbx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%r12), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	d1_order
	testl	%eax, %eax
	je	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	(%r12), %rax
	movq	%rax, (%r14)
	addq	$8, %r14
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	addq	$8, %r12
	decq	%rbx
	jne	.LBB8_2
.LBB8_5:                                # %._crit_edge
	leaq	8(%r14), %rax
	movq	$0, (%r14)
	movq	%rax, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	simplify_cubelist, .Lfunc_end8-simplify_cubelist
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"BINATE_SPLIT_SELECT: split against %d\n"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cl=%s\ncr=%s\n"
	.size	.L.str.1, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
