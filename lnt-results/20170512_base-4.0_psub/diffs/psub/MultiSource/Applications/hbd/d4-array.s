	.text
	.file	"d4-array.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI0_1:
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI0_2:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	9                       # 0x9
	.long	18                      # 0x12
	.text
	.globl	_Z9anewarrayP9Classfile
	.p2align	4, 0x90
	.type	_Z9anewarrayP9Classfile,@function
_Z9anewarrayP9Classfile:                # @_Z9anewarrayP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r12d
	leal	2(%r12), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shlq	$8, %rcx
	movzbl	1(%rax), %eax
	orq	%rcx, %rax
	movq	40(%rdi), %rcx
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rbp
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#1:                                 # %.noexc
	decl	%r12d
	movq	%rbp, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%r12d, 16(%r14)
	movl	%r12d, 12(%r14)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
.Ltmp3:
# BB#2:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	stkptr(%rip), %r13
	movq	-8(%r13), %rbp
	movl	$1, 8(%r15)
	movl	%r12d, 16(%r15)
	movl	%r12d, 12(%r15)
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#3:
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,7,0,39]
	movups	%xmm0, (%rax)
	movq	%rax, (%r15)
	movq	%r14, 24(%r15)
	movq	%rbp, 32(%r15)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%rbp), %eax
	cmpl	%eax, %r12d
	cmovbl	%r12d, %eax
	movl	$1, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp8:
	movl	$24, %edi
	callq	_Znwm
.Ltmp9:
# BB#4:
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [0,2,9,18]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r15, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	%rbx, -8(%r13)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_7:
.Ltmp10:
	movq	%rax, %r12
	movq	%rbx, %rdi
	jmp	.LBB0_8
.LBB0_6:
.Ltmp7:
	movq	%rax, %r12
	movq	%r15, %rdi
	jmp	.LBB0_8
.LBB0_5:
.Ltmp4:
	movq	%rax, %r12
	movq	%r14, %rdi
.LBB0_8:
	callq	_ZdlPv
	movq	%r12, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z9anewarrayP9Classfile, .Lfunc_end0-_Z9anewarrayP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	9                       # 0x9
	.long	18                      # 0x12
	.text
	.globl	_Z14multianewarrayP9Classfile
	.p2align	4, 0x90
	.type	_Z14multianewarrayP9Classfile,@function
_Z14multianewarrayP9Classfile:          # @_Z14multianewarrayP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	currpc(%rip), %eax
	leal	-1(%rax), %ebp
	addl	$2, %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	addq	$2, inbuff(%rip)
	movq	stkptr(%rip), %r14
	movq	-8(%r14), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r15), %eax
	cmpl	%eax, %ebp
	cmovbl	%ebp, %eax
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp11:
	movl	$24, %edi
	callq	_Znwm
.Ltmp12:
# BB#1:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,2,9,18]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r15, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	%rbx, -8(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_2:
.Ltmp13:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z14multianewarrayP9Classfile, .Lfunc_end1-_Z14multianewarrayP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp11-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI2_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.text
	.globl	_Z13doarraylengthP9Classfile
	.p2align	4, 0x90
	.type	_Z13doarraylengthP9Classfile,@function
_Z13doarraylengthP9Classfile:           # @_Z13doarraylengthP9Classfile
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	movq	stkptr(%rip), %r15
	movq	-8(%r15), %r12
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp14:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp15:
# BB#1:                                 # %.noexc
	decl	%ebp
	movq	$.L.str.1, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r14)
	movl	%ebp, 16(%r14)
	movl	%ebp, 12(%r14)
.Ltmp16:
	movl	$24, %edi
	callq	_Znwm
.Ltmp17:
# BB#2:
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	cmpl	%eax, %ebp
	cmovbl	%ebp, %eax
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp19:
	movl	$24, %edi
	callq	_Znwm
.Ltmp20:
# BB#3:
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [0,4,4,5]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	%rbx, -8(%r15)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_5:
.Ltmp21:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB2_6
.LBB2_4:
.Ltmp18:
	movq	%rax, %r15
	movq	%r14, %rdi
.LBB2_6:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z13doarraylengthP9Classfile, .Lfunc_end2-_Z13doarraylengthP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp14-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp17         #   Call between .Ltmp17 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z10doarraygetP9Classfile
	.p2align	4, 0x90
	.type	_Z10doarraygetP9Classfile,@function
_Z10doarraygetP9Classfile:              # @_Z10doarraygetP9Classfile
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	decl	%ebp
	movq	stkptr(%rip), %r14
	leaq	-8(%r14), %rax
	movq	%rax, stkptr(%rip)
	movq	-16(%r14), %r12
	movq	-8(%r14), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	cmpl	%eax, %ebp
	cmovbl	%ebp, %eax
	movl	16(%r15), %ecx
	cmpl	%ecx, %eax
	cmovael	%ecx, %eax
	movl	ch(%rip), %r13d
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp22:
	movl	$24, %edi
	callq	_Znwm
.Ltmp23:
# BB#1:
	addl	$-42, %r13d
	movl	$0, (%rax)
	movl	$7, 4(%rax)
	movl	%r13d, 8(%rax)
	movl	$39, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%rbx, -16(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:
.Ltmp24:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_Z10doarraygetP9Classfile, .Lfunc_end3-_Z10doarraygetP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp22-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	39                      # 0x27
.LCPI4_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	6                       # 0x6
	.text
	.globl	_Z10doarrayputP9Classfile
	.p2align	4, 0x90
	.type	_Z10doarrayputP9Classfile,@function
_Z10doarrayputP9Classfile:              # @_Z10doarrayputP9Classfile
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r12d
	decl	%r12d
	movq	stkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %r15
	leaq	-16(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-16(%rax), %r13
	leaq	-24(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-24(%rax), %rbx
	movl	16(%rbx), %ebp
	cmpl	%ebp, %r12d
	cmovbl	%r12d, %ebp
	movl	16(%r13), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movl	$1, 8(%r14)
	movl	%r12d, 12(%r14)
	movl	%ebp, 16(%r14)
.Ltmp25:
	movl	$24, %edi
	callq	_Znwm
.Ltmp26:
# BB#1:
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [0,7,8,39]
	movups	%xmm0, (%rax)
	movq	%rax, (%r14)
	movq	%rbx, 24(%r14)
	movq	%r13, 32(%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$1, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movl	%ebp, 16(%rbx)
.Ltmp28:
	movl	$24, %edi
	callq	_Znwm
.Ltmp29:
# BB#2:
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [0,4,8,6]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r14, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%rbx, (%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_4:
.Ltmp30:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB4_5
.LBB4_3:
.Ltmp27:
	movq	%rax, %r15
	movq	%r14, %rdi
.LBB4_5:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_Z10doarrayputP9Classfile, .Lfunc_end4-_Z10doarrayputP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp25-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin4   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp26         #   Call between .Ltmp26 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin4   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Multi"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"length"
	.size	.L.str.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
