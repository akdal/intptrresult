	.text
	.file	"options.bc"
	.globl	opts_IdFirst
	.p2align	4, 0x90
	.type	opts_IdFirst,@function
opts_IdFirst:                           # @opts_IdFirst
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	opts_IdFirst, .Lfunc_end0-opts_IdFirst
	.cfi_endproc

	.globl	opts_IdIsNull
	.p2align	4, 0x90
	.type	opts_IdIsNull,@function
opts_IdIsNull:                          # @opts_IdIsNull
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$-1, %edi
	sete	%al
	retq
.Lfunc_end1:
	.size	opts_IdIsNull, .Lfunc_end1-opts_IdIsNull
	.cfi_endproc

	.globl	opts_Declare
	.p2align	4, 0x90
	.type	opts_Declare,@function
opts_Declare:                           # @opts_Declare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	opts_DECLARATIONS(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB2_5
# BB#1:                                 # %.outer.split.i.preheader
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.outer.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_4
# BB#3:                                 # %.us-lcssa.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbp), %rbp
	decl	%ebx
	testq	%rbp, %rbp
	jne	.LBB2_2
	jmp	.LBB2_5
.LBB2_4:                                # %opts_Id.exit
	testl	%ebx, %ebx
	jne	.LBB2_12
.LBB2_5:                                # %opts_Id.exit.thread
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	string_StringCopy
	movq	%rax, (%rbp)
	movl	%r14d, 8(%rbp)
	movq	opts_DECLARATIONS(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB2_6
# BB#7:
	testq	%rax, %rax
	je	.LBB2_11
# BB#8:                                 # %.preheader.i.preheader
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_9
# BB#10:
	movq	%rax, (%rcx)
	jmp	.LBB2_11
.LBB2_6:
	movq	%rax, %rbx
.LBB2_11:                               # %list_Nconc.exit
	movq	%rbx, opts_DECLARATIONS(%rip)
	movl	opts_IdNextAvailable(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, opts_IdNextAvailable(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_12:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$375, %ecx              # imm = 0x177
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end2:
	.size	opts_Declare, .Lfunc_end2-opts_Declare
	.cfi_endproc

	.globl	opts_Id
	.p2align	4, 0x90
	.type	opts_Id,@function
opts_Id:                                # @opts_Id
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	opts_DECLARATIONS(%rip), %rbp
	movl	$-1, %r14d
	testq	%rbp, %rbp
	je	.LBB3_5
# BB#1:                                 # %.outer.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_2:                                # %.outer.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_3
# BB#4:                                 # %.us-lcssa
                                        #   in Loop: Header=BB3_2 Depth=1
	incl	%ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_2
	jmp	.LBB3_5
.LBB3_3:
	movl	%ebx, %r14d
.LBB3_5:                                # %.critedge14
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	opts_Id, .Lfunc_end3-opts_Id
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end4:
	.size	misc_DumpCore, .Lfunc_end4-misc_DumpCore
	.cfi_endproc

	.globl	opts_DeclareVector
	.p2align	4, 0x90
	.type	opts_DeclareVector,@function
opts_DeclareVector:                     # @opts_DeclareVector
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	cmpb	$0, (%rdi)
	je	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rbx), %esi
	callq	opts_Declare
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpb	$0, (%rdi)
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movl	opts_IdNextAvailable(%rip), %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	opts_DeclareVector, .Lfunc_end5-opts_DeclareVector
	.cfi_endproc

	.globl	opts_ClName
	.p2align	4, 0x90
	.type	opts_ClName,@function
opts_ClName:                            # @opts_ClName
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	opts_DECLARATIONS(%rip), %rax
	testl	%edi, %edi
	je	.LBB6_6
# BB#1:                                 # %.lr.ph.i.preheader
	leal	-1(%rdi), %ecx
	movl	%edi, %esi
	xorl	%edx, %edx
	andl	$7, %esi
	je	.LBB6_3
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%edx
	movq	(%rax), %rax
	cmpl	%edx, %esi
	jne	.LBB6_2
.LBB6_3:                                # %.lr.ph.i.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB6_6
# BB#4:                                 # %.lr.ph.i.preheader.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	addl	$-8, %edi
	jne	.LBB6_5
.LBB6_6:                                # %opts_DeclGetById.exit
	movq	8(%rax), %rax
	movq	(%rax), %rax
	retq
.Lfunc_end6:
	.size	opts_ClName, .Lfunc_end6-opts_ClName
	.cfi_endproc

	.globl	opts_Init
	.p2align	4, 0x90
	.type	opts_Init,@function
opts_Init:                              # @opts_Init
	.cfi_startproc
# BB#0:
	movq	$0, opts_DECLARATIONS(%rip)
	movq	$0, opts_PARAMETERS(%rip)
	movl	$0, opts_IdNextAvailable(%rip)
	retq
.Lfunc_end7:
	.size	opts_Init, .Lfunc_end7-opts_Init
	.cfi_endproc

	.globl	opts_DeclareSPASSFlagsAsOptions
	.p2align	4, 0x90
	.type	opts_DeclareSPASSFlagsAsOptions,@function
opts_DeclareSPASSFlagsAsOptions:        # @opts_DeclareSPASSFlagsAsOptions
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	flag_Name
	movl	$2, %esi
	movq	%rax, %rdi
	callq	opts_Declare
	incl	%ebx
	cmpl	$96, %ebx
	jne	.LBB8_1
# BB#2:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	opts_DeclareSPASSFlagsAsOptions, .Lfunc_end8-opts_DeclareSPASSFlagsAsOptions
	.cfi_endproc

	.globl	opts_Free
	.p2align	4, 0x90
	.type	opts_Free,@function
opts_Free:                              # @opts_Free
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movq	opts_PARAMETERS(%rip), %rdi
	movl	$opts_FreeParameterPair, %esi
	callq	list_DeleteWithElement
	movq	opts_DECLARATIONS(%rip), %rdi
	movl	$opts_FreeDecl, %esi
	popq	%rax
	jmp	list_DeleteWithElement  # TAILCALL
.Lfunc_end9:
	.size	opts_Free, .Lfunc_end9-opts_Free
	.cfi_endproc

	.p2align	4, 0x90
	.type	opts_FreeParameterPair,@function
opts_FreeParameterPair:                 # @opts_FreeParameterPair
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	string_StringFree
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	opts_FreeParameterPair, .Lfunc_end10-opts_FreeParameterPair
	.cfi_endproc

	.p2align	4, 0x90
	.type	opts_FreeDecl,@function
opts_FreeDecl:                          # @opts_FreeDecl
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	string_StringFree
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end11:
	.size	opts_FreeDecl, .Lfunc_end11-opts_FreeDecl
	.cfi_endproc

	.globl	opts_PrintSPASSNames
	.p2align	4, 0x90
	.type	opts_PrintSPASSNames,@function
opts_PrintSPASSNames:                   # @opts_PrintSPASSNames
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	leal	1(%rbx), %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	leal	2(%rbx), %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	leal	3(%rbx), %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	addl	$4, %ebx
	cmpl	$96, %ebx
	jl	.LBB12_1
# BB#2:
	popq	%rbx
	retq
.Lfunc_end12:
	.size	opts_PrintSPASSNames, .Lfunc_end12-opts_PrintSPASSNames
	.cfi_endproc

	.globl	opts_DeclGetByClName
	.p2align	4, 0x90
	.type	opts_DeclGetByClName,@function
opts_DeclGetByClName:                   # @opts_DeclGetByClName
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -48
.Lcfi36:
	.cfi_offset %r12, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	opts_DECLARATIONS(%rip), %r12
	xorl	%r14d, %r14d
	testq	%r12, %r12
	je	.LBB13_10
# BB#1:                                 # %.outer.split.i.preheader
	movq	%r12, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # %.outer.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB13_4
# BB#3:                                 # %.us-lcssa.i
                                        #   in Loop: Header=BB13_2 Depth=1
	incl	%ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_2
	jmp	.LBB13_10
.LBB13_4:                               # %opts_Id.exit
	cmpl	$-1, %ebp
	je	.LBB13_5
# BB#6:                                 # %.lr.ph.i.preheader
	movl	$-1, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %ebp
	je	.LBB13_8
# BB#9:                                 #   in Loop: Header=BB13_7 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB13_7
	jmp	.LBB13_10
.LBB13_5:
	xorl	%r14d, %r14d
	jmp	.LBB13_10
.LBB13_8:
	movq	8(%r12), %r14
.LBB13_10:                              # %opts_DeclGetById.exit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	opts_DeclGetByClName, .Lfunc_end13-opts_DeclGetByClName
	.cfi_endproc

	.globl	opts_Read
	.p2align	4, 0x90
	.type	opts_Read,@function
opts_Read:                              # @opts_Read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 176
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r15d
	movl	$.L.str.14, %edi
	callq	string_StringCopy
	movq	%rax, %r13
	movq	opts_DECLARATIONS(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB14_2
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB14_6
.LBB14_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r14
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$1, %rax
	jne	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	%rbx, %rdi
	callq	string_StringCopy
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	string_Nconc
	movq	%rax, %r13
	movl	8(%r14), %eax
	decl	%eax
	cmpl	$1, %eax
	ja	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movl	$.L.str.15, %edi
	callq	string_StringCopy
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	string_Nconc
	movq	%rax, %r13
	jmp	.LBB14_5
.LBB14_6:                               # %._crit_edge.i
	cmpb	$0, (%r13)
	je	.LBB14_8
# BB#7:
	movl	$.L.str.15, %edi
	callq	string_StringCopy
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	string_Nconc
	movq	%rax, %r13
.LBB14_8:                               # %opts_TranslateShortOptDeclarations.exit
	movq	opts_DECLARATIONS(%rip), %rbp
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	jne	.LBB14_10
	jmp	.LBB14_13
	.p2align	4, 0x90
.LBB14_12:                              #   in Loop: Header=BB14_10 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB14_13
.LBB14_10:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	(%rbx), %rdi
	callq	strlen
	cmpq	$1, %rax
	je	.LBB14_12
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB14_12
.LBB14_13:                              # %opts_GetLongOptDeclarations.exit.i
	movq	%r14, %rdi
	callq	list_Length
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$5, %eax
	leal	32(%rax), %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	testq	%r14, %r14
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB14_18
# BB#14:                                # %.lr.ph.i47.preheader
	leaq	24(%rbx), %rcx
	xorl	%eax, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB14_15:                              # %.lr.ph.i47
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rdi
	movq	(%rdi), %rbp
	movq	%rbp, -24(%rcx)
	movl	8(%rdi), %edi
	xorl	%ebp, %ebp
	cmpl	$1, %edi
	sete	%bpl
	cmpl	$2, %edi
	cmovel	%edi, %ebp
	movl	%ebp, -16(%rcx)
	movq	$0, -8(%rcx)
	movl	$0, (%rcx)
	movq	(%rsi), %rsi
	addq	%rdx, %rax
	addq	$32, %rcx
	testq	%rsi, %rsi
	jne	.LBB14_15
# BB#16:                                # %.lr.ph.i44.i.preheader
	sarq	$27, %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	$0, (%rbx,%rax)
	movl	$0, 8(%rbx,%rax)
	movq	$0, 16(%rbx,%rax)
	movl	$0, 24(%rbx,%rax)
	.p2align	4, 0x90
.LBB14_17:                              # %.lr.ph.i44.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB14_17
	jmp	.LBB14_19
.LBB14_18:                              # %list_Delete.exit.critedge.i
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 24(%rbx)
.LBB14_19:                              # %opts_TranslateLongOptDeclarations.exit.preheader
	movq	%r13, 88(%rsp)          # 8-byte Spill
	leaq	1(%r13), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r15d, %edx
	movslq	%edx, %rbp
	movl	$1, %r14d
                                        # implicit-def: %EAX
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	jmp	.LBB14_20
.LBB14_164:                             #   in Loop: Header=BB14_20 Depth=1
	movq	8(%r15), %rax
	jmp	.LBB14_167
	.p2align	4, 0x90
.LBB14_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_44 Depth 2
                                        #     Child Loop BB14_70 Depth 2
                                        #     Child Loop BB14_74 Depth 2
                                        #     Child Loop BB14_115 Depth 2
                                        #     Child Loop BB14_119 Depth 2
                                        #     Child Loop BB14_158 Depth 2
                                        #     Child Loop BB14_163 Depth 2
                                        #     Child Loop BB14_176 Depth 2
                                        #     Child Loop BB14_185 Depth 2
                                        #     Child Loop BB14_193 Depth 2
	movq	$0, opts_Arg(%rip)
	movl	opts_Ind(%rip), %r13d
	testl	%r13d, %r13d
	movl	%r14d, 76(%rsp)         # 4-byte Spill
	je	.LBB14_22
# BB#21:                                #   in Loop: Header=BB14_20 Depth=1
	movb	opts_GetOptInitialized(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	jne	.LBB14_22
# BB#29:                                #   in Loop: Header=BB14_20 Depth=1
	movq	opts_NextChar(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB14_30
# BB#31:                                #   in Loop: Header=BB14_20 Depth=1
	cmpb	$0, (%rsi)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jne	.LBB14_65
	jmp	.LBB14_32
	.p2align	4, 0x90
.LBB14_22:                              #   in Loop: Header=BB14_20 Depth=1
	movq	%rbx, %r15
	movl	%edx, %ebx
	movl	$1, opts_Ind(%rip)
	movl	$1, opts_LastNonOpt(%rip)
	movl	$1, opts_FirstNonOpt(%rip)
	movq	$0, opts_NextChar(%rip)
	movl	$.L.str.31, %edi
	callq	getenv
	movq	%rax, opts_PosixlyCorrect(%rip)
	movq	88(%rsp), %rdx          # 8-byte Reload
	movb	(%rdx), %cl
	cmpb	$43, %cl
	je	.LBB14_23
# BB#25:                                #   in Loop: Header=BB14_20 Depth=1
	cmpb	$45, %cl
	jne	.LBB14_27
# BB#26:                                #   in Loop: Header=BB14_20 Depth=1
	movl	$2, %ecx
	jmp	.LBB14_24
.LBB14_30:                              #   in Loop: Header=BB14_20 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB14_32
.LBB14_23:                              #   in Loop: Header=BB14_20 Depth=1
	xorl	%ecx, %ecx
.LBB14_24:                              # %.thread49.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB14_28
.LBB14_27:                              #   in Loop: Header=BB14_20 Depth=1
	xorl	%ecx, %ecx
	testq	%rax, %rax
	sete	%cl
	movq	%rdx, 24(%rsp)          # 8-byte Spill
.LBB14_28:                              # %.thread49.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movl	%ebx, %edx
	movl	%ecx, opts_Ordering(%rip)
	movl	$1, opts_Ind(%rip)
	movb	$1, opts_GetOptInitialized(%rip)
	movl	$1, %r13d
	movq	%r15, %rbx
.LBB14_32:                              #   in Loop: Header=BB14_20 Depth=1
	movl	opts_LastNonOpt(%rip), %eax
	cmpl	%r13d, %eax
	jle	.LBB14_34
# BB#33:                                #   in Loop: Header=BB14_20 Depth=1
	movl	%r13d, opts_LastNonOpt(%rip)
	movl	%r13d, %eax
.LBB14_34:                              #   in Loop: Header=BB14_20 Depth=1
	movq	%rbx, %r14
	movl	opts_FirstNonOpt(%rip), %ecx
	cmpl	%r13d, %ecx
	jle	.LBB14_36
# BB#35:                                #   in Loop: Header=BB14_20 Depth=1
	movl	%r13d, opts_FirstNonOpt(%rip)
	movl	%r13d, %ecx
.LBB14_36:                              #   in Loop: Header=BB14_20 Depth=1
	movl	opts_Ordering(%rip), %ebx
	cmpl	$1, %ebx
	jne	.LBB14_48
# BB#37:                                #   in Loop: Header=BB14_20 Depth=1
	cmpl	%r13d, %eax
	je	.LBB14_40
# BB#38:                                #   in Loop: Header=BB14_20 Depth=1
	cmpl	%eax, %ecx
	je	.LBB14_40
# BB#39:                                #   in Loop: Header=BB14_20 Depth=1
	movl	%edx, %r15d
	movq	%r12, %rdi
	callq	opts_Exchange
	movl	%r15d, %edx
	cmpl	%edx, %r13d
	jl	.LBB14_43
	jmp	.LBB14_47
	.p2align	4, 0x90
.LBB14_40:                              #   in Loop: Header=BB14_20 Depth=1
	cmpl	%r13d, %eax
	je	.LBB14_42
# BB#41:                                #   in Loop: Header=BB14_20 Depth=1
	movl	%r13d, opts_FirstNonOpt(%rip)
.LBB14_42:                              # %.preheader.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	%edx, %r13d
	jge	.LBB14_47
.LBB14_43:                              # %.lr.ph42.i.preheader.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movslq	%r13d, %r13
	.p2align	4, 0x90
.LBB14_44:                              # %.lr.ph42.i.i
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r13,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB14_46
# BB#45:                                #   in Loop: Header=BB14_44 Depth=2
	cmpb	$0, 1(%rax)
	jne	.LBB14_47
.LBB14_46:                              # %.thread.i.i
                                        #   in Loop: Header=BB14_44 Depth=2
	incq	%r13
	movl	%r13d, opts_Ind(%rip)
	cmpq	%rbp, %r13
	jl	.LBB14_44
.LBB14_47:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movl	%r13d, opts_LastNonOpt(%rip)
	movl	%r13d, %eax
.LBB14_48:                              #   in Loop: Header=BB14_20 Depth=1
	cmpl	%edx, %r13d
	je	.LBB14_49
# BB#50:                                #   in Loop: Header=BB14_20 Depth=1
	movslq	%r13d, %rcx
	movq	(%r12,%rcx,8), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB14_62
# BB#51:                                #   in Loop: Header=BB14_20 Depth=1
	movb	1(%rbp), %cl
	cmpb	$45, %cl
	jne	.LBB14_61
# BB#52:                                #   in Loop: Header=BB14_20 Depth=1
	cmpb	$0, 2(%rbp)
	jne	.LBB14_64
	jmp	.LBB14_53
.LBB14_61:                              # %thread-pre-split
                                        #   in Loop: Header=BB14_20 Depth=1
	testb	%cl, %cl
	je	.LBB14_62
.LBB14_64:                              # %thread-pre-split.thread
                                        #   in Loop: Header=BB14_20 Depth=1
	incq	%rbp
	cmpb	$45, %cl
	sete	%al
	movq	%r14, %rbx
	testq	%rbx, %rbx
	setne	%cl
	andb	%al, %cl
	movzbl	%cl, %esi
	addq	%rbp, %rsi
	movq	%rsi, opts_NextChar(%rip)
.LBB14_65:                              #   in Loop: Header=BB14_20 Depth=1
	testq	%rbx, %rbx
	je	.LBB14_100
# BB#66:                                #   in Loop: Header=BB14_20 Depth=1
	movslq	%r13d, %rax
	movq	(%r12,%rax,8), %rax
	movsbl	1(%rax), %ebp
	cmpl	$45, %ebp
	je	.LBB14_69
# BB#67:                                #   in Loop: Header=BB14_20 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB14_68
.LBB14_69:                              # %.preheader56.i.i.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	movq	%rsi, %rcx
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_213:                             #   in Loop: Header=BB14_70 Depth=2
	incq	%rcx
.LBB14_70:                              # %.preheader56.i.i
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %r15d
	cmpb	$61, %r15b
	je	.LBB14_72
# BB#71:                                # %.preheader56.i.i
                                        #   in Loop: Header=BB14_70 Depth=2
	testb	%r15b, %r15b
	jne	.LBB14_213
.LBB14_72:                              # %.critedge.preheader.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%rbx, %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB14_97
# BB#73:                                # %.lr.ph38.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%rax, %r14
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, %r13
	subq	%rsi, %r13
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movl	$0, 56(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_74:                              #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%rsi, %rbp
	movq	%r13, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB14_82
# BB#75:                                #   in Loop: Header=BB14_74 Depth=2
	movq	%rbx, %rdi
	callq	strlen
	cmpl	%eax, %r13d
	je	.LBB14_76
# BB#81:                                #   in Loop: Header=BB14_74 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmoveq	%r14, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movl	56(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	4(%rsp), %eax           # 4-byte Reload
	cmovel	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB14_82:                              # %.critedge.i.i
                                        #   in Loop: Header=BB14_74 Depth=2
	movq	%rbp, %rsi
	incl	%r12d
	movq	32(%r14), %rbx
	addq	$32, %r14
	testq	%rbx, %rbx
	jne	.LBB14_74
# BB#83:                                # %.critedge._crit_edge.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	je	.LBB14_77
	jmp	.LBB14_84
	.p2align	4, 0x90
.LBB14_62:                              # %.thread52.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	testl	%ebx, %ebx
	je	.LBB14_203
# BB#63:                                #   in Loop: Header=BB14_20 Depth=1
	incl	%r13d
	movl	%r13d, opts_Ind(%rip)
	movq	%rbp, opts_Arg(%rip)
	movl	$1, %esi
	jmp	.LBB14_174
.LBB14_68:                              #   in Loop: Header=BB14_20 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rsi, %r15
	movl	%ebp, %esi
	callq	strchr
	movq	%r15, %rsi
	testq	%rax, %rax
	jne	.LBB14_100
	jmp	.LBB14_69
.LBB14_76:                              #   in Loop: Header=BB14_20 Depth=1
	movl	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r14, %rcx
	movq	%rbp, %rsi
.LBB14_77:                              # %.thread7.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	testq	%rcx, %rcx
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	je	.LBB14_97
# BB#78:                                #   in Loop: Header=BB14_20 Depth=1
	movq	%rax, %rbx
	leal	1(%r13), %eax
	movl	%eax, opts_Ind(%rip)
	testb	%r15b, %r15b
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	8(%rcx), %ecx
	je	.LBB14_90
# BB#79:                                #   in Loop: Header=BB14_20 Depth=1
	testl	%ecx, %ecx
	je	.LBB14_85
# BB#80:                                #   in Loop: Header=BB14_20 Depth=1
	incq	%rbp
	jmp	.LBB14_94
.LBB14_97:                              # %.thread7.thread.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%rax, %rbx
	cmpb	$45, 96(%rsp)           # 1-byte Folded Reload
	je	.LBB14_214
# BB#98:                                #   in Loop: Header=BB14_20 Depth=1
	movq	%rsi, %rbp
	movsbl	(%rbp), %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	strchr
	movq	%rbp, %rsi
	testq	%rax, %rax
	je	.LBB14_99
.LBB14_100:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%r12, %r14
	leaq	1(%rsi), %rbp
	movq	%rbp, opts_NextChar(%rip)
	movq	%rsi, %r12
	movsbl	(%r12), %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%esi, 4(%rsp)           # 4-byte Spill
	callq	strchr
	movb	1(%r12), %cl
	testb	%cl, %cl
	jne	.LBB14_102
# BB#101:                               #   in Loop: Header=BB14_20 Depth=1
	incl	%r13d
	movl	%r13d, opts_Ind(%rip)
.LBB14_102:                             #   in Loop: Header=BB14_20 Depth=1
	movl	4(%rsp), %esi           # 4-byte Reload
	cmpb	$58, %sil
	movq	%r14, %r12
	je	.LBB14_104
# BB#103:                               #   in Loop: Header=BB14_20 Depth=1
	testq	%rax, %rax
	je	.LBB14_104
# BB#108:                               #   in Loop: Header=BB14_20 Depth=1
	movb	1(%rax), %dl
	cmpb	$87, (%rax)
	jne	.LBB14_138
# BB#109:                               #   in Loop: Header=BB14_20 Depth=1
	cmpb	$59, %dl
	jne	.LBB14_138
# BB#110:                               #   in Loop: Header=BB14_20 Depth=1
	testb	%cl, %cl
	je	.LBB14_112
# BB#111:                               #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, opts_Arg(%rip)
	incl	%r13d
	movl	%r13d, opts_Ind(%rip)
	jmp	.LBB14_114
.LBB14_138:                             # %._crit_edge47.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpb	$58, %dl
	jne	.LBB14_139
# BB#140:                               #   in Loop: Header=BB14_20 Depth=1
	cmpb	$58, 2(%rax)
	jne	.LBB14_144
# BB#141:                               #   in Loop: Header=BB14_20 Depth=1
	testb	%cl, %cl
	jne	.LBB14_142
# BB#143:                               #   in Loop: Header=BB14_20 Depth=1
	movq	$0, opts_Arg(%rip)
	xorl	%ebp, %ebp
	jmp	.LBB14_147
.LBB14_139:                             #   in Loop: Header=BB14_20 Depth=1
	xorl	%ebp, %ebp
	cmpl	$57, %esi
	jle	.LBB14_149
	jmp	.LBB14_151
.LBB14_112:                             #   in Loop: Header=BB14_20 Depth=1
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	je	.LBB14_215
# BB#113:                               #   in Loop: Header=BB14_20 Depth=1
	movslq	%r13d, %rax
	leal	1(%r13), %ecx
	movl	%ecx, opts_Ind(%rip)
	movq	(%r12,%rax,8), %rbp
	movq	%rbp, opts_Arg(%rip)
	movl	%ecx, %r13d
.LBB14_114:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, opts_NextChar(%rip)
	movq	%rbp, %r14
	jmp	.LBB14_115
	.p2align	4, 0x90
.LBB14_216:                             #   in Loop: Header=BB14_115 Depth=2
	incq	%r14
.LBB14_115:                             #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14), %r15d
	cmpb	$61, %r15b
	je	.LBB14_117
# BB#116:                               #   in Loop: Header=BB14_115 Depth=2
	testb	%r15b, %r15b
	jne	.LBB14_216
.LBB14_117:                             # %.critedge10.preheader.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB14_173
# BB#118:                               # %.lr.ph.i.i50
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%r14, %rax
	subq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB14_119:                             #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB14_127
# BB#120:                               #   in Loop: Header=BB14_119 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%rax, 96(%rsp)          # 8-byte Folded Reload
	je	.LBB14_121
# BB#126:                               #   in Loop: Header=BB14_119 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmoveq	%rbx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movl	24(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	4(%rsp), %eax           # 4-byte Reload
	cmovel	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB14_127:                             # %.critedge10.i.i
                                        #   in Loop: Header=BB14_119 Depth=2
	incl	%r12d
	movq	32(%rbx), %rbp
	addq	$32, %rbx
	testq	%rbp, %rbp
	jne	.LBB14_119
# BB#128:                               # %.critedge10._crit_edge.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	je	.LBB14_122
	jmp	.LBB14_129
.LBB14_144:                             #   in Loop: Header=BB14_20 Depth=1
	testb	%cl, %cl
	je	.LBB14_145
.LBB14_142:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, opts_Arg(%rip)
	incl	%r13d
	movl	%r13d, opts_Ind(%rip)
	jmp	.LBB14_147
.LBB14_90:                              #   in Loop: Header=BB14_20 Depth=1
	cmpl	$1, %ecx
	jne	.LBB14_91
# BB#92:                                #   in Loop: Header=BB14_20 Depth=1
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB14_217
# BB#93:                                #   in Loop: Header=BB14_20 Depth=1
	addl	$2, %r13d
	movl	%r13d, opts_Ind(%rip)
	cltq
	movq	(%r12,%rax,8), %rbp
.LBB14_94:                              # %.sink.split.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	%rbp, opts_Arg(%rip)
.LBB14_95:                              #   in Loop: Header=BB14_20 Depth=1
	movq	%rsi, %rdi
	movq	%rsi, %r15
	callq	strlen
	addq	%r15, %rax
	movq	%rax, opts_NextChar(%rip)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	testq	%rax, %rax
	movl	24(%rcx), %esi
	je	.LBB14_96
# BB#154:                               #   in Loop: Header=BB14_20 Depth=1
	movl	%esi, (%rax)
	jmp	.LBB14_155
.LBB14_96:                              #   in Loop: Header=BB14_20 Depth=1
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	cmpl	$57, %esi
	jle	.LBB14_149
	jmp	.LBB14_151
.LBB14_121:                             #   in Loop: Header=BB14_20 Depth=1
	movl	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rbx, %rcx
.LBB14_122:                             # %.thread12.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	testq	%rcx, %rcx
	movq	40(%rsp), %r12          # 8-byte Reload
	je	.LBB14_173
# BB#123:                               #   in Loop: Header=BB14_20 Depth=1
	testb	%r15b, %r15b
	movl	8(%rcx), %eax
	movq	%rcx, %r15
	je	.LBB14_131
# BB#124:                               #   in Loop: Header=BB14_20 Depth=1
	testl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	je	.LBB14_130
# BB#125:                               #   in Loop: Header=BB14_20 Depth=1
	incq	%r14
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB14_134
.LBB14_173:                             # %.thread12.thread.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	$0, opts_NextChar(%rip)
	movl	$87, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB14_174
.LBB14_131:                             #   in Loop: Header=BB14_20 Depth=1
	cmpl	$1, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r14
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB14_135
# BB#132:                               #   in Loop: Header=BB14_20 Depth=1
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB14_217
# BB#133:                               #   in Loop: Header=BB14_20 Depth=1
	leal	1(%r13), %eax
	movl	%eax, opts_Ind(%rip)
	movslq	%r13d, %rax
	movq	(%r12,%rax,8), %r14
.LBB14_134:                             # %.sink.split13.i.i
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%r14, opts_Arg(%rip)
.LBB14_135:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	addq	%rbp, %rax
	movq	%rax, opts_NextChar(%rip)
	movq	16(%r15), %rax
	testq	%rax, %rax
	movl	24(%r15), %esi
	je	.LBB14_136
# BB#137:                               #   in Loop: Header=BB14_20 Depth=1
	movl	%esi, (%rax)
	movl	4(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB14_155
.LBB14_136:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%r14, %rbp
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpl	$57, %esi
	jle	.LBB14_149
	jmp	.LBB14_151
.LBB14_91:                              #   in Loop: Header=BB14_20 Depth=1
	xorl	%ebp, %ebp
	movl	4(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB14_95
.LBB14_145:                             #   in Loop: Header=BB14_20 Depth=1
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	je	.LBB14_215
# BB#146:                               #   in Loop: Header=BB14_20 Depth=1
	leal	1(%r13), %eax
	movl	%eax, opts_Ind(%rip)
	movslq	%r13d, %rax
	movq	(%r12,%rax,8), %rbp
	movq	%rbp, opts_Arg(%rip)
.LBB14_147:                             #   in Loop: Header=BB14_20 Depth=1
	movq	$0, opts_NextChar(%rip)
	cmpl	$57, %esi
	jg	.LBB14_151
.LBB14_149:                             # %opts_GetOptLongOnly.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	testl	%esi, %esi
	jne	.LBB14_202
# BB#150:                               #   in Loop: Header=BB14_20 Depth=1
	movl	8(%rsp), %r13d          # 4-byte Reload
.LBB14_155:                             # %opts_GetOptLongOnly.exit.thread77
                                        #   in Loop: Header=BB14_20 Depth=1
	movslq	%r13d, %rax
	shlq	$5, %rax
	movq	(%rbx,%rax), %r14
	movq	opts_DECLARATIONS(%rip), %r15
	testq	%r15, %r15
	je	.LBB14_156
# BB#157:                               # %.outer.split.i.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	xorl	%ebp, %ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB14_158:                             # %.outer.split.i
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB14_161
# BB#159:                               # %.us-lcssa.i
                                        #   in Loop: Header=BB14_158 Depth=2
	incl	%ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_158
# BB#160:                               #   in Loop: Header=BB14_20 Depth=1
	movl	$-1, %ebp
.LBB14_161:                             # %opts_Id.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	testq	%r15, %r15
	je	.LBB14_166
# BB#162:                               # %.lr.ph.i53.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB14_163:                             # %.lr.ph.i53
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	cmpl	%eax, %ebp
	je	.LBB14_164
# BB#165:                               #   in Loop: Header=BB14_163 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB14_163
	jmp	.LBB14_166
.LBB14_156:                             #   in Loop: Header=BB14_20 Depth=1
	movl	$-1, %ebp
.LBB14_166:                             #   in Loop: Header=BB14_20 Depth=1
	xorl	%eax, %eax
.LBB14_167:                             # %opts_DeclGetById.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	opts_Arg(%rip), %rsi
	testq	%rsi, %rsi
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %ebx
	je	.LBB14_168
# BB#170:                               #   in Loop: Header=BB14_20 Depth=1
	movl	%ebp, %edi
	callq	opts_AddParamCheck
	movl	%ebx, %edx
	movl	%eax, %r14d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB14_171
.LBB14_168:                             #   in Loop: Header=BB14_20 Depth=1
	cmpl	$1, 8(%rax)
	je	.LBB14_218
# BB#169:                               #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.6, %esi
	movl	%ebp, %edi
	callq	opts_AddParamCheck
	movl	%eax, %r14d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	%ebx, %edx
	movq	%rax, %rbx
	jmp	.LBB14_171
.LBB14_151:                             # %opts_GetOptLongOnly.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$58, %esi
	je	.LBB14_153
# BB#152:                               # %opts_GetOptLongOnly.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$63, %esi
	jne	.LBB14_174
	jmp	.LBB14_153
.LBB14_202:                             # %opts_GetOptLongOnly.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$-1, %esi
	je	.LBB14_203
	.p2align	4, 0x90
.LBB14_174:                             # %opts_GetOptLongOnly.exit.thread73
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	%rbp, %r15
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movb	%sil, 38(%rsp)
	movb	$0, 39(%rsp)
	movq	opts_DECLARATIONS(%rip), %r14
	testq	%r14, %r14
	leaq	38(%rsp), %r13
	je	.LBB14_178
# BB#175:                               # %.outer.split.i.i.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB14_176:                             # %.outer.split.i.i
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r13, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB14_179
# BB#177:                               # %.us-lcssa.i.i
                                        #   in Loop: Header=BB14_176 Depth=2
	incl	%r12d
	movq	(%rbx), %rbx
	incb	%bpl
	testq	%rbx, %rbx
	jne	.LBB14_176
	jmp	.LBB14_178
	.p2align	4, 0x90
.LBB14_179:                             # %opts_ShortOptId.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	testl	%r12d, %r12d
	je	.LBB14_180
# BB#181:                               # %opts_ShortOptId.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$-1, %r12d
	je	.LBB14_178
# BB#182:                               # %.lr.ph.i61.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	leal	-1(%r12), %ecx
	testb	$7, %r12b
	je	.LBB14_183
# BB#184:                               # %.lr.ph.i61.prol.preheader
                                        #   in Loop: Header=BB14_20 Depth=1
	andb	$7, %bpl
	movzbl	%bpl, %edx
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB14_185:                             # %.lr.ph.i61.prol
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	movq	(%r14), %r14
	cmpl	%eax, %edx
	jne	.LBB14_185
	jmp	.LBB14_186
	.p2align	4, 0x90
.LBB14_180:                             #   in Loop: Header=BB14_20 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rsi
	jmp	.LBB14_187
.LBB14_183:                             #   in Loop: Header=BB14_20 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rsi
.LBB14_186:                             # %.lr.ph.i61.prol.loopexit
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$7, %ecx
	jb	.LBB14_187
	.p2align	4, 0x90
.LBB14_193:                             # %.lr.ph.i61
                                        #   Parent Loop BB14_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$8, %eax
	movq	(%rcx), %r14
	cmpl	%eax, %r12d
	jne	.LBB14_193
.LBB14_187:                             # %opts_DeclGetById.exit67
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	8(%r14), %rax
	movl	8(%rax), %eax
	cmpl	$1, %eax
	je	.LBB14_194
# BB#188:                               # %opts_DeclGetById.exit67
                                        #   in Loop: Header=BB14_20 Depth=1
	cmpl	$2, %eax
	movq	80(%rsp), %rbp          # 8-byte Reload
	jne	.LBB14_201
# BB#189:                               #   in Loop: Header=BB14_20 Depth=1
	movq	%rbx, %r13
	movq	%r15, %rbx
	testq	%rbx, %rbx
	movl	12(%rsp), %r15d         # 4-byte Reload
	je	.LBB14_191
# BB#190:                               #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB14_191
# BB#200:                               #   in Loop: Header=BB14_20 Depth=1
	movl	%r12d, %edi
	movq	%rbx, %rsi
	jmp	.LBB14_192
	.p2align	4, 0x90
.LBB14_194:                             #   in Loop: Header=BB14_20 Depth=1
	movq	%rbx, %r15
	testq	%rsi, %rsi
	movq	%rsi, %rbx
	movq	80(%rsp), %rbp          # 8-byte Reload
	je	.LBB14_195
# BB#197:                               #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB14_198
# BB#199:                               #   in Loop: Header=BB14_20 Depth=1
	movl	%r12d, %edi
	movq	%rbx, %rsi
	callq	opts_AddParamCheck
	movl	%eax, %r14d
	movl	8(%rsp), %r13d          # 4-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	%r15, %rbx
	jmp	.LBB14_171
	.p2align	4, 0x90
.LBB14_201:                             #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.6, %esi
	movl	%r12d, %edi
	callq	opts_AddParamCheck
	movl	%eax, %r14d
	movl	8(%rsp), %r13d          # 4-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	jmp	.LBB14_171
.LBB14_191:                             #   in Loop: Header=BB14_20 Depth=1
	movl	$.L.str.6, %esi
	movl	%r12d, %edi
.LBB14_192:                             # %opts_TranslateLongOptDeclarations.exit.backedge
                                        #   in Loop: Header=BB14_20 Depth=1
	callq	opts_AddParamCheck
	movl	%eax, %r14d
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r13, %rbx
	movl	%eax, %r13d
	movl	%r15d, %edx
.LBB14_171:                             # %opts_TranslateLongOptDeclarations.exit.backedge
                                        #   in Loop: Header=BB14_20 Depth=1
	testl	%r14d, %r14d
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	jne	.LBB14_20
# BB#172:
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
	jmp	.LBB14_203
.LBB14_49:
	movl	%eax, %edx
.LBB14_59:                              # %.thread54.i.i
	movl	opts_FirstNonOpt(%rip), %eax
	cmpl	%edx, %eax
	je	.LBB14_203
# BB#60:
	movl	%eax, opts_Ind(%rip)
.LBB14_203:                             # %.critedge
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	string_StringFree
	xorl	%ecx, %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB14_204:                             # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rdi,%rcx)
	leaq	32(%rcx), %rcx
	jne	.LBB14_204
# BB#205:
	cmpl	$1024, %ecx             # imm = 0x400
	movl	76(%rsp), %r14d         # 4-byte Reload
	jae	.LBB14_206
# BB#211:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB14_212
.LBB14_206:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rdi
	movq	-8(%rdx), %rbp
	testq	%rdi, %rdi
	leaq	8(%rdi), %rdi
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdi, %rbx
	movq	%rbp, (%rbx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB14_208
# BB#207:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rdx)
.LBB14_208:
	addl	memory_MARKSIZE(%rip), %esi
	addl	%esi, %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB14_210
# BB#209:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB14_210:
	addq	$-16, %rdi
	callq	free
.LBB14_212:                             # %opts_FreeLongOptsArray.exit
	movl	%r14d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_53:
	incl	%r13d
	movl	%r13d, opts_Ind(%rip)
	movl	opts_FirstNonOpt(%rip), %ecx
	cmpl	%r13d, %eax
	je	.LBB14_56
# BB#54:
	cmpl	%eax, %ecx
	je	.LBB14_56
# BB#55:
	movl	%edx, %ebx
	movq	%r12, %rdi
	callq	opts_Exchange
	movl	%ebx, %edx
	jmp	.LBB14_58
.LBB14_153:                             # %opts_FreeLongOptsArray.exit.loopexit
	xorl	%r14d, %r14d
	jmp	.LBB14_212
.LBB14_56:
	cmpl	%eax, %ecx
	jne	.LBB14_58
# BB#57:
	movl	%r13d, opts_FirstNonOpt(%rip)
.LBB14_58:
	movl	%edx, opts_LastNonOpt(%rip)
	movl	%edx, opts_Ind(%rip)
	jmp	.LBB14_59
.LBB14_178:                             # %.loopexit
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$928, %ecx              # imm = 0x3A0
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.LBB14_104:
	movq	opts_PosixlyCorrect(%rip), %rbx
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpq	$0, %rbx
	movq	(%r12), %rsi
	jne	.LBB14_105
# BB#107:
	movl	$.L.str.27, %edi
	jmp	.LBB14_106
.LBB14_195:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.8, %edi
	jmp	.LBB14_196
.LBB14_198:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.10, %edi
.LBB14_196:
	xorl	%eax, %eax
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_105:
	movl	$.L.str.26, %edi
.LBB14_106:
	xorl	%eax, %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_214:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r12), %rsi
	movq	opts_NextChar(%rip), %rdx
	movl	$.L.str.23, %edi
	jmp	.LBB14_87
.LBB14_99:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r12), %rsi
	movslq	opts_Ind(%rip), %rax
	movq	(%r12,%rax,8), %rax
	movsbl	(%rax), %edx
	movq	opts_NextChar(%rip), %rcx
	movl	$.L.str.24, %edi
.LBB14_89:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_84:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rsi
	movslq	opts_Ind(%rip), %rax
	movq	(%rcx,%rax,8), %rdx
	movl	$.L.str.19, %edi
	jmp	.LBB14_87
.LBB14_129:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rsi
	movslq	opts_Ind(%rip), %rax
	movq	(%rcx,%rax,8), %rdx
	movl	$.L.str.29, %edi
	jmp	.LBB14_87
.LBB14_215:
	movq	stdout(%rip), %rdi
	movl	%esi, %ebx
	callq	fflush
	movq	(%r12), %rsi
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_218:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_85:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r12), %rsi
	cmpb	$45, 96(%rsp)           # 1-byte Folded Reload
	jne	.LBB14_88
# BB#86:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.20, %edi
	jmp	.LBB14_87
.LBB14_130:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r12), %rsi
	movq	(%r15), %rdx
	movl	$.L.str.30, %edi
	jmp	.LBB14_87
.LBB14_217:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%r12), %rsi
	movslq	opts_Ind(%rip), %rax
	movq	-8(%r12,%rax,8), %rdx
	movl	$.L.str.22, %edi
.LBB14_87:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_88:
	movslq	opts_Ind(%rip), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	-8(%rcx,%rax,8), %rax
	movsbl	(%rax), %edx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	movl	$.L.str.21, %edi
	jmp	.LBB14_89
.Lfunc_end14:
	.size	opts_Read, .Lfunc_end14-opts_Read
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end15:
	.size	misc_Error, .Lfunc_end15-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	opts_AddParamCheck,@function
opts_AddParamCheck:                     # @opts_AddParamCheck
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	movl	$opts_PARAMETERS, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB16_3
# BB#2:                                 # %select.unfold.i
                                        #   in Loop: Header=BB16_1 Depth=1
	movq	8(%rax), %rdx
	xorl	%ecx, %ecx
	cmpl	%ebx, 8(%rdx)
	sete	%cl
	jne	.LBB16_1
.LBB16_3:                               # %opts_GetValue.exit
	testl	%ecx, %ecx
	jne	.LBB16_5
# BB#4:
	movslq	%ebx, %r15
	movq	%rsi, %rdi
	callq	string_StringCopy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r15, 8(%rbx)
	movq	%r14, (%rbx)
	movq	opts_PARAMETERS(%rip), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, opts_PARAMETERS(%rip)
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB16_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	opts_ClName
	movq	%rax, %rcx
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end16:
	.size	opts_AddParamCheck, .Lfunc_end16-opts_AddParamCheck
	.cfi_endproc

	.globl	opts_ReadOptionsFromString
	.p2align	4, 0x90
	.type	opts_ReadOptionsFromString,@function
opts_ReadOptionsFromString:             # @opts_ReadOptionsFromString
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	callq	string_StringCopy
	movq	%rax, %r14
	leaq	4(%rsp), %rsi
	movq	%r14, %rdi
	callq	string_Tokens
	movq	%rax, %rbx
	movl	4(%rsp), %edi
	movq	%rbx, %rsi
	callq	opts_Read
	movl	4(%rsp), %ecx
	xorl	%r15d, %r15d
	cmpl	%ecx, opts_Ind(%rip)
	cmovgel	%eax, %r15d
	testl	%ecx, %ecx
	jle	.LBB17_4
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ecx, %rbp
	incq	%rbp
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rbx,%rbp,8), %rdi
	callq	string_StringFree
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB17_2
# BB#3:                                 # %._crit_edge.loopexit
	movl	4(%rsp), %ecx
.LBB17_4:                               # %._crit_edge
	incl	%ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB17_5
# BB#10:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	jmp	.LBB17_11
.LBB17_5:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB17_7
# BB#6:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rcx)
.LBB17_7:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB17_9
# BB#8:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB17_9:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB17_11:                              # %memory_Free.exit
	movq	%r14, %rdi
	callq	string_StringFree
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	opts_ReadOptionsFromString, .Lfunc_end17-opts_ReadOptionsFromString
	.cfi_endproc

	.globl	opts_Indicator
	.p2align	4, 0x90
	.type	opts_Indicator,@function
opts_Indicator:                         # @opts_Indicator
	.cfi_startproc
# BB#0:
	movl	opts_Ind(%rip), %eax
	retq
.Lfunc_end18:
	.size	opts_Indicator, .Lfunc_end18-opts_Indicator
	.cfi_endproc

	.globl	opts_GetValueByName
	.p2align	4, 0x90
	.type	opts_GetValueByName,@function
opts_GetValueByName:                    # @opts_GetValueByName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -40
.Lcfi75:
	.cfi_offset %r12, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$opts_PARAMETERS, %ebx
	.p2align	4, 0x90
.LBB19_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_5 Depth 2
                                        #     Child Loop BB19_8 Depth 2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB19_2
# BB#3:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	8(%rbx), %r12
	movl	8(%r12), %eax
	movq	opts_DECLARATIONS(%rip), %rcx
	testl	%eax, %eax
	je	.LBB19_9
# BB#4:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB19_1 Depth=1
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	andl	$7, %edi
	je	.LBB19_6
	.p2align	4, 0x90
.LBB19_5:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB19_5
.LBB19_6:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB19_1 Depth=1
	cmpl	$7, %edx
	jb	.LBB19_9
# BB#7:                                 # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB19_1 Depth=1
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB19_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB19_8
.LBB19_9:                               # %opts_ClName.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	movq	8(%rcx), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB19_1
# BB#10:                                # %.critedge16
	movq	(%r12), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.LBB19_11
.LBB19_2:
	xorl	%eax, %eax
.LBB19_11:                              # %.critedge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	opts_GetValueByName, .Lfunc_end19-opts_GetValueByName
	.cfi_endproc

	.globl	opts_GetValue
	.p2align	4, 0x90
	.type	opts_GetValue,@function
opts_GetValue:                          # @opts_GetValue
	.cfi_startproc
# BB#0:
	movl	$opts_PARAMETERS, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB20_4
# BB#2:                                 # %select.unfold
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rcx), %rdx
	xorl	%eax, %eax
	cmpl	%edi, 8(%rdx)
	sete	%al
	jne	.LBB20_1
# BB#3:                                 # %.critedge16
	movq	(%rdx), %rcx
	movq	%rcx, (%rsi)
.LBB20_4:                               # %.critedge
	retq
.Lfunc_end20:
	.size	opts_GetValue, .Lfunc_end20-opts_GetValue
	.cfi_endproc

	.globl	opts_GetIntValueByName
	.p2align	4, 0x90
	.type	opts_GetIntValueByName,@function
opts_GetIntValueByName:                 # @opts_GetIntValueByName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -40
.Lcfi84:
	.cfi_offset %r12, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$opts_PARAMETERS, %ebx
	.p2align	4, 0x90
.LBB21_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_4 Depth 2
                                        #     Child Loop BB21_7 Depth 2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB21_10
# BB#2:                                 #   in Loop: Header=BB21_1 Depth=1
	movq	8(%rbx), %r12
	movl	8(%r12), %eax
	movq	opts_DECLARATIONS(%rip), %rcx
	testl	%eax, %eax
	je	.LBB21_8
# BB#3:                                 # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB21_1 Depth=1
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	andl	$7, %edi
	je	.LBB21_5
	.p2align	4, 0x90
.LBB21_4:                               # %.lr.ph.i.i.i.prol
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB21_4
.LBB21_5:                               # %.lr.ph.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB21_1 Depth=1
	cmpl	$7, %edx
	jb	.LBB21_8
# BB#6:                                 # %.lr.ph.i.i.i.preheader.new
                                        #   in Loop: Header=BB21_1 Depth=1
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB21_7:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB21_7
.LBB21_8:                               # %opts_ClName.exit.i
                                        #   in Loop: Header=BB21_1 Depth=1
	movq	8(%rcx), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB21_1
# BB#9:
	movq	(%r12), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	string_StringToInt      # TAILCALL
.LBB21_10:                              # %opts_GetValueByName.exit.thread
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	opts_GetIntValueByName, .Lfunc_end21-opts_GetIntValueByName
	.cfi_endproc

	.globl	opts_GetIntValue
	.p2align	4, 0x90
	.type	opts_GetIntValue,@function
opts_GetIntValue:                       # @opts_GetIntValue
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -48
.Lcfi93:
	.cfi_offset %r12, -40
.Lcfi94:
	.cfi_offset %r13, -32
.Lcfi95:
	.cfi_offset %r14, -24
.Lcfi96:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	opts_DECLARATIONS(%rip), %r12
	testl	%edi, %edi
	movq	%r12, %rax
	je	.LBB22_6
# BB#1:                                 # %.lr.ph.i.i.preheader
	leal	-1(%rdi), %ecx
	movl	%edi, %esi
	xorl	%edx, %edx
	movq	%r12, %rax
	andl	$7, %esi
	je	.LBB22_3
	.p2align	4, 0x90
.LBB22_2:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%edx
	movq	(%rax), %rax
	cmpl	%edx, %esi
	jne	.LBB22_2
.LBB22_3:                               # %.lr.ph.i.i.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB22_6
# BB#4:                                 # %.lr.ph.i.i.preheader.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	addl	$-8, %edi
	jne	.LBB22_5
.LBB22_6:                               # %opts_ClName.exit
	movq	8(%rax), %rax
	movl	$opts_PARAMETERS, %ebx
	movq	(%rax), %r15
	.p2align	4, 0x90
.LBB22_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_10 Depth 2
                                        #     Child Loop BB22_13 Depth 2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB22_16
# BB#8:                                 #   in Loop: Header=BB22_7 Depth=1
	movq	8(%rbx), %r13
	movl	8(%r13), %eax
	testl	%eax, %eax
	movq	%r12, %rcx
	je	.LBB22_14
# BB#9:                                 # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB22_7 Depth=1
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	movq	%r12, %rcx
	andl	$7, %edi
	je	.LBB22_11
	.p2align	4, 0x90
.LBB22_10:                              # %.lr.ph.i.i.i.i.prol
                                        #   Parent Loop BB22_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB22_10
.LBB22_11:                              # %.lr.ph.i.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB22_7 Depth=1
	cmpl	$7, %edx
	jb	.LBB22_14
# BB#12:                                # %.lr.ph.i.i.i.i.preheader.new
                                        #   in Loop: Header=BB22_7 Depth=1
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB22_13:                              # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB22_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB22_13
.LBB22_14:                              # %opts_ClName.exit.i.i
                                        #   in Loop: Header=BB22_7 Depth=1
	movq	8(%rcx), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB22_7
# BB#15:
	movq	(%r13), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	string_StringToInt      # TAILCALL
.LBB22_16:                              # %opts_GetIntValueByName.exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	opts_GetIntValue, .Lfunc_end22-opts_GetIntValue
	.cfi_endproc

	.globl	opts_IsSet
	.p2align	4, 0x90
	.type	opts_IsSet,@function
opts_IsSet:                             # @opts_IsSet
	.cfi_startproc
# BB#0:
	movq	opts_PARAMETERS(%rip), %rax
	testq	%rax, %rax
	je	.LBB23_1
	.p2align	4, 0x90
.LBB23_2:                               # %select.unfold
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	8(%rcx), %ecx
	cmpl	%edi, %ecx
	je	.LBB23_4
# BB#3:                                 # %select.unfold
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB23_2
.LBB23_4:                               # %.critedge.loopexit
	xorl	%eax, %eax
	cmpl	%edi, %ecx
	sete	%al
	retq
.LBB23_1:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	opts_IsSet, .Lfunc_end23-opts_IsSet
	.cfi_endproc

	.globl	opts_IsSetByName
	.p2align	4, 0x90
	.type	opts_IsSetByName,@function
opts_IsSetByName:                       # @opts_IsSetByName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	opts_PARAMETERS(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB24_1
# BB#2:                                 # %.lr.ph.preheader
	movq	opts_DECLARATIONS(%rip), %r15
	.p2align	4, 0x90
.LBB24_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_5 Depth 2
                                        #     Child Loop BB24_8 Depth 2
	movq	8(%rbx), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	movq	%r15, %rcx
	je	.LBB24_9
# BB#4:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	movq	%r15, %rcx
	andl	$7, %edi
	je	.LBB24_6
	.p2align	4, 0x90
.LBB24_5:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB24_5
.LBB24_6:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB24_3 Depth=1
	cmpl	$7, %edx
	jb	.LBB24_9
# BB#7:                                 # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB24_3 Depth=1
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB24_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB24_8
.LBB24_9:                               # %opts_ClName.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rcx), %rax
	movq	(%rax), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB24_11
# BB#10:                                # %opts_ClName.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_3
.LBB24_11:                              # %.critedge.loopexit
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	jmp	.LBB24_12
.LBB24_1:
	xorl	%ecx, %ecx
.LBB24_12:                              # %.critedge
	movl	%ecx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	opts_IsSetByName, .Lfunc_end24-opts_IsSetByName
	.cfi_endproc

	.globl	opts_SetFlags
	.p2align	4, 0x90
	.type	opts_SetFlags,@function
opts_SetFlags:                          # @opts_SetFlags
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 80
.Lcfi110:
	.cfi_offset %rbx, -56
.Lcfi111:
	.cfi_offset %r12, -48
.Lcfi112:
	.cfi_offset %r13, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_3 Depth 2
                                        #     Child Loop BB25_9 Depth 2
                                        #     Child Loop BB25_14 Depth 2
                                        #     Child Loop BB25_17 Depth 2
                                        #     Child Loop BB25_20 Depth 2
                                        #       Child Loop BB25_22 Depth 3
                                        #       Child Loop BB25_25 Depth 3
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %r14
	movq	opts_DECLARATIONS(%rip), %r13
	movl	$-1, %r12d
	testq	%r13, %r13
	je	.LBB25_7
# BB#2:                                 # %.outer.split.i.preheader
                                        #   in Loop: Header=BB25_1 Depth=1
	movl	%ebx, %r15d
	xorl	%ebx, %ebx
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB25_3:                               # %.outer.split.i
                                        #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB25_4
# BB#5:                                 # %.us-lcssa.i
                                        #   in Loop: Header=BB25_3 Depth=2
	incl	%ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB25_3
	jmp	.LBB25_6
	.p2align	4, 0x90
.LBB25_4:                               #   in Loop: Header=BB25_1 Depth=1
	movl	%ebx, %r12d
.LBB25_6:                               #   in Loop: Header=BB25_1 Depth=1
	movl	%r15d, %ebx
.LBB25_7:                               # %opts_Id.exit
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	opts_PARAMETERS(%rip), %r15
	testq	%r15, %r15
	je	.LBB25_31
# BB#8:                                 # %select.unfold.i.preheader
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB25_9:                               # %select.unfold.i
                                        #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movl	8(%rcx), %ecx
	cmpl	%r12d, %ecx
	je	.LBB25_11
# BB#10:                                # %select.unfold.i
                                        #   in Loop: Header=BB25_9 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB25_9
.LBB25_11:                              # %opts_IsSet.exit
                                        #   in Loop: Header=BB25_1 Depth=1
	cmpl	%r12d, %ecx
	jne	.LBB25_31
# BB#12:                                #   in Loop: Header=BB25_1 Depth=1
	testl	%r12d, %r12d
	movq	%r13, %rax
	je	.LBB25_18
# BB#13:                                # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB25_1 Depth=1
	leal	-1(%r12), %ecx
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%r13, %rax
	andl	$7, %esi
	je	.LBB25_15
	.p2align	4, 0x90
.LBB25_14:                              # %.lr.ph.i.i.i.prol
                                        #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	movq	(%rax), %rax
	cmpl	%edx, %esi
	jne	.LBB25_14
.LBB25_15:                              # %.lr.ph.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB25_1 Depth=1
	cmpl	$7, %ecx
	jb	.LBB25_18
# BB#16:                                # %.lr.ph.i.i.i.preheader.new
                                        #   in Loop: Header=BB25_1 Depth=1
	movl	%r12d, %ecx
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB25_17:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	addl	$-8, %ecx
	jne	.LBB25_17
.LBB25_18:                              # %opts_ClName.exit.i
                                        #   in Loop: Header=BB25_1 Depth=1
	testq	%r15, %r15
	je	.LBB25_28
# BB#19:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	8(%rax), %rax
	movq	(%rax), %rbp
	.p2align	4, 0x90
.LBB25_20:                              # %.lr.ph
                                        #   Parent Loop BB25_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_22 Depth 3
                                        #       Child Loop BB25_25 Depth 3
	movq	8(%r15), %r14
	movl	8(%r14), %eax
	testl	%eax, %eax
	movq	%r13, %rcx
	je	.LBB25_26
# BB#21:                                # %.lr.ph.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB25_20 Depth=2
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	movq	%r13, %rcx
	andl	$7, %edi
	je	.LBB25_23
	.p2align	4, 0x90
.LBB25_22:                              # %.lr.ph.i.i.i.i.i.prol
                                        #   Parent Loop BB25_1 Depth=1
                                        #     Parent Loop BB25_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB25_22
.LBB25_23:                              # %.lr.ph.i.i.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB25_20 Depth=2
	cmpl	$7, %edx
	jb	.LBB25_26
# BB#24:                                # %.lr.ph.i.i.i.i.i.preheader.new
                                        #   in Loop: Header=BB25_20 Depth=2
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB25_25:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB25_1 Depth=1
                                        #     Parent Loop BB25_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB25_25
.LBB25_26:                              # %opts_ClName.exit.i.i.i
                                        #   in Loop: Header=BB25_20 Depth=2
	movq	8(%rcx), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB25_29
# BB#27:                                # %opts_ClName.exit.i.i.i._crit_edge
                                        #   in Loop: Header=BB25_20 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB25_20
	jmp	.LBB25_28
	.p2align	4, 0x90
.LBB25_29:                              # %opts_GetIntValue.exit
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	(%r14), %rdi
	xorl	%esi, %esi
	leaq	12(%rsp), %rdx
	callq	string_StringToInt
	testl	%eax, %eax
	je	.LBB25_28
# BB#30:                                #   in Loop: Header=BB25_1 Depth=1
	movl	12(%rsp), %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %esi
	callq	flag_SetFlagValue
.LBB25_31:                              # %opts_IsSet.exit.thread
                                        #   in Loop: Header=BB25_1 Depth=1
	incl	%ebx
	cmpl	$96, %ebx
	jb	.LBB25_1
# BB#32:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_28:                              # %opts_GetIntValue.exit.thread
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end25:
	.size	opts_SetFlags, .Lfunc_end25-opts_SetFlags
	.cfi_endproc

	.p2align	4, 0x90
	.type	flag_SetFlagValue,@function
flag_SetFlagValue:                      # @flag_SetFlagValue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 32
.Lcfi119:
	.cfi_offset %rbx, -32
.Lcfi120:
	.cfi_offset %r14, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB26_1
# BB#3:
	movl	%ebp, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB26_4
# BB#5:                                 # %flag_CheckFlagValueInRange.exit
	movl	%ebp, %eax
	movl	%ebx, (%r14,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB26_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.16, %edi
	jmp	.LBB26_2
.LBB26_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.17, %edi
.LBB26_2:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end26:
	.size	flag_SetFlagValue, .Lfunc_end26-flag_SetFlagValue
	.cfi_endproc

	.globl	opts_Transfer
	.p2align	4, 0x90
	.type	opts_Transfer,@function
opts_Transfer:                          # @opts_Transfer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 64
.Lcfi128:
	.cfi_offset %rbx, -48
.Lcfi129:
	.cfi_offset %r12, -40
.Lcfi130:
	.cfi_offset %r13, -32
.Lcfi131:
	.cfi_offset %r14, -24
.Lcfi132:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	opts_PARAMETERS(%rip), %r13
	testq	%r13, %r13
	je	.LBB27_10
# BB#1:                                 # %.lr.ph.preheader
	leaq	12(%rsp), %r14
	.p2align	4, 0x90
.LBB27_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_4 Depth 2
                                        #     Child Loop BB27_7 Depth 2
	movq	8(%r13), %rcx
	movl	8(%rcx), %eax
	movq	(%rcx), %r12
	movq	opts_DECLARATIONS(%rip), %rcx
	testl	%eax, %eax
	je	.LBB27_8
# BB#3:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB27_2 Depth=1
	leal	-1(%rax), %edx
	movl	%eax, %edi
	xorl	%esi, %esi
	andl	$7, %edi
	je	.LBB27_5
	.p2align	4, 0x90
.LBB27_4:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rcx), %rcx
	cmpl	%esi, %edi
	jne	.LBB27_4
.LBB27_5:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpl	$7, %edx
	jb	.LBB27_8
# BB#6:                                 # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB27_2 Depth=1
	subl	%esi, %eax
	.p2align	4, 0x90
.LBB27_7:                               # %.lr.ph.i.i
                                        #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	addl	$-8, %eax
	jne	.LBB27_7
.LBB27_8:                               # %opts_ClName.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	8(%rcx), %rax
	movq	(%rax), %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	string_StringToInt
	testl	%eax, %eax
	je	.LBB27_11
# BB#9:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	%rbx, %rdi
	callq	flag_Id
	movl	12(%rsp), %edx
	movq	%r15, %rdi
	movl	%eax, %esi
	callq	flag_SetFlagValue
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB27_2
.LBB27_10:                              # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB27_11:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end27:
	.size	opts_Transfer, .Lfunc_end27-opts_Transfer
	.cfi_endproc

	.p2align	4, 0x90
	.type	opts_Exchange,@function
opts_Exchange:                          # @opts_Exchange
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movl	opts_FirstNonOpt(%rip), %eax
	movl	%eax, -80(%rsp)         # 4-byte Spill
	movl	opts_LastNonOpt(%rip), %edx
	movl	opts_Ind(%rip), %eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	subl	%edx, %eax
	movl	%eax, -60(%rsp)         # 4-byte Spill
	jle	.LBB28_41
# BB#1:
	cmpl	-80(%rsp), %edx         # 4-byte Folded Reload
	jle	.LBB28_41
# BB#2:                                 # %.lr.ph76.preheader
	movslq	%edx, %r10
	leaq	(%rdi,%r10,8), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	8(%rdi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movl	-80(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebx
	movl	-76(%rsp), %eax         # 4-byte Reload
	movl	%eax, -84(%rsp)         # 4-byte Spill
	movl	%edx, -64(%rsp)         # 4-byte Spill
.LBB28_3:                               # %.lr.ph76.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_21 Depth 2
                                        #     Child Loop BB28_19 Depth 2
                                        #       Child Loop BB28_17 Depth 3
                                        #       Child Loop BB28_14 Depth 3
                                        #     Child Loop BB28_32 Depth 2
                                        #     Child Loop BB28_38 Depth 2
	movl	-84(%rsp), %esi         # 4-byte Reload
	movl	%esi, %r9d
	subl	%edx, %r9d
	jle	.LBB28_20
# BB#4:                                 # %.lr.ph76.split.us.split.us.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movslq	%ebx, %rbx
	movslq	%r9d, %r14
	movl	%r9d, %r13d
	leaq	(%rbx,%r13), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	-1(%r13), %rbp
	movl	%r9d, %eax
	andl	$3, %eax
	movq	%r13, %rcx
	movq	%rax, -32(%rsp)         # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	leaq	1(%rbx), %r15
	xorl	%r12d, %r12d
	movq	%rbx, -8(%rsp)          # 8-byte Spill
	jmp	.LBB28_19
	.p2align	4, 0x90
.LBB28_5:                               # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB28_19 Depth=2
	cmpl	$3, %r9d
	jbe	.LBB28_9
# BB#6:                                 # %min.iters.checked154
                                        #   in Loop: Header=BB28_19 Depth=2
	cmpq	$0, -72(%rsp)           # 8-byte Folded Reload
	je	.LBB28_9
# BB#7:                                 # %vector.memcheck172
                                        #   in Loop: Header=BB28_19 Depth=2
	movq	%r14, %rax
	imulq	%r12, %rax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpq	-16(%rsp), %rcx         # 8-byte Folded Reload
	jae	.LBB28_16
# BB#8:                                 # %vector.memcheck172
                                        #   in Loop: Header=BB28_19 Depth=2
	addq	-24(%rsp), %rax         # 8-byte Folded Reload
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, -48(%rsp)         # 8-byte Folded Reload
	jae	.LBB28_16
	.p2align	4, 0x90
.LBB28_9:                               #   in Loop: Header=BB28_19 Depth=2
	xorl	%eax, %eax
.LBB28_10:                              # %.lr.ph.us.us.preheader186
                                        #   in Loop: Header=BB28_19 Depth=2
	movl	%r13d, %ecx
	subl	%eax, %ecx
	testb	$1, %cl
	movq	%rax, %rcx
	je	.LBB28_12
# BB#11:                                # %.lr.ph.us.us.prol
                                        #   in Loop: Header=BB28_19 Depth=2
	leaq	(%rax,%rbx), %rcx
	movq	(%rdi,%rcx,8), %r8
	movq	%rbp, %rsi
	leaq	(%rax,%r10), %rbp
	movq	(%rdi,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rcx,8)
	movq	%r8, (%rdi,%rbp,8)
	movq	%rsi, %rbp
	leaq	1(%rax), %rcx
.LBB28_12:                              # %.lr.ph.us.us.prol.loopexit
                                        #   in Loop: Header=BB28_19 Depth=2
	cmpq	%rax, %rbp
	je	.LBB28_15
# BB#13:                                # %.lr.ph.us.us.preheader186.new
                                        #   in Loop: Header=BB28_19 Depth=2
	movq	%r13, %rax
	subq	%rcx, %rax
	leaq	(%r10,%rcx), %rdx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %r8
	addq	%r15, %rcx
	leaq	(%rdi,%rcx,8), %r11
	.p2align	4, 0x90
.LBB28_14:                              # %.lr.ph.us.us
                                        #   Parent Loop BB28_3 Depth=1
                                        #     Parent Loop BB28_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%r11), %rcx
	movq	-8(%r8), %rdx
	movq	%rdx, -8(%r11)
	movq	%rcx, -8(%r8)
	movq	(%r11), %rcx
	movq	(%r8), %rdx
	movq	%rdx, (%r11)
	movq	%rcx, (%r8)
	addq	$16, %r8
	addq	$16, %r11
	addq	$-2, %rax
	jne	.LBB28_14
.LBB28_15:                              # %._crit_edge.us.us
                                        #   in Loop: Header=BB28_19 Depth=2
	addq	%r14, %rbx
	incq	%r12
	addq	%r14, %r15
	cmpq	%rbx, %r10
	jg	.LBB28_19
	jmp	.LBB28_41
.LBB28_16:                              # %vector.body150.preheader
                                        #   in Loop: Header=BB28_19 Depth=2
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB28_17:                              # %vector.body150
                                        #   Parent Loop BB28_3 Depth=1
                                        #     Parent Loop BB28_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rcx,%rbx,8), %xmm0
	movups	(%rcx,%rbx,8), %xmm1
	movups	-16(%rcx,%r10,8), %xmm2
	movups	(%rcx,%r10,8), %xmm3
	movups	%xmm2, -16(%rcx,%rbx,8)
	movups	%xmm3, (%rcx,%rbx,8)
	movups	%xmm0, -16(%rcx,%r10,8)
	movups	%xmm1, (%rcx,%r10,8)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB28_17
# BB#18:                                # %middle.block151
                                        #   in Loop: Header=BB28_19 Depth=2
	cmpl	$0, -32(%rsp)           # 4-byte Folded Reload
	movq	-72(%rsp), %rax         # 8-byte Reload
	jne	.LBB28_10
	jmp	.LBB28_15
	.p2align	4, 0x90
.LBB28_19:                              # %.lr.ph76.split.us.split.us
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_17 Depth 3
                                        #       Child Loop BB28_14 Depth 3
	movq	%r10, %rax
	subq	%rbx, %rax
	cmpq	%rax, %r14
	jle	.LBB28_5
	jmp	.LBB28_23
	.p2align	4, 0x90
.LBB28_20:                              # %.lr.ph76.split.us.split.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	%edx, %eax
	subl	%ebx, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB28_21:                              # %.lr.ph76.split.us.split
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, %r9d
	jg	.LBB28_23
# BB#22:                                # %.preheader70.us
                                        #   in Loop: Header=BB28_21 Depth=2
	addl	%r9d, %ebx
	addl	%ecx, %eax
	cmpl	%ebx, %edx
	jg	.LBB28_21
	jmp	.LBB28_41
	.p2align	4, 0x90
.LBB28_23:                              # %.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	-84(%rsp), %r15d        # 4-byte Reload
	subl	%eax, %r15d
	testl	%eax, %eax
	jle	.LBB28_39
# BB#24:                                # %.lr.ph84
                                        #   in Loop: Header=BB28_3 Depth=1
	movslq	%ebx, %r8
	movslq	%r15d, %r9
	movl	%eax, %ecx
	cmpl	$4, %eax
	jae	.LBB28_26
# BB#25:                                #   in Loop: Header=BB28_3 Depth=1
	xorl	%r11d, %r11d
	jmp	.LBB28_34
.LBB28_26:                              # %min.iters.checked
                                        #   in Loop: Header=BB28_3 Depth=1
	andl	$3, %eax
	movq	%rcx, %r11
	subq	%rax, %r11
	je	.LBB28_30
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB28_3 Depth=1
	leaq	(%rdi,%r8,8), %rdx
	leaq	(%r9,%rcx), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB28_31
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB28_3 Depth=1
	leaq	(%r8,%rcx), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	leaq	(%rdi,%r9,8), %rsi
	cmpq	%rdx, %rsi
	jae	.LBB28_31
.LBB28_30:                              #   in Loop: Header=BB28_3 Depth=1
	xorl	%r11d, %r11d
	jmp	.LBB28_34
.LBB28_31:                              # %vector.body.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	-56(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r8,8), %rdx
	leaq	(%rsi,%r9,8), %rsi
	movq	%r11, %rbp
	.p2align	4, 0x90
.LBB28_32:                              # %vector.body
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	-16(%rsi), %xmm2
	movups	(%rsi), %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rbp
	jne	.LBB28_32
# BB#33:                                # %middle.block
                                        #   in Loop: Header=BB28_3 Depth=1
	testl	%eax, %eax
	je	.LBB28_39
	.p2align	4, 0x90
.LBB28_34:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	%ecx, %edx
	subl	%r11d, %edx
	leaq	-1(%rcx), %rax
	testb	$1, %dl
	movq	%r11, %rdx
	je	.LBB28_36
# BB#35:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB28_3 Depth=1
	leaq	(%r11,%r8), %rdx
	movq	(%rdi,%rdx,8), %r14
	leaq	(%r9,%r11), %rbp
	movq	(%rdi,%rbp,8), %rsi
	movq	%rsi, (%rdi,%rdx,8)
	movq	%r14, (%rdi,%rbp,8)
	leaq	1(%r11), %rdx
.LBB28_36:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB28_3 Depth=1
	cmpq	%r11, %rax
	je	.LBB28_39
# BB#37:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB28_3 Depth=1
	subq	%rdx, %rcx
	addq	%rdx, %r9
	movq	-40(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r9,8), %rax
	addq	%rdx, %r8
	leaq	(%rsi,%r8,8), %rdx
	.p2align	4, 0x90
.LBB28_38:                              # %scalar.ph
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %rsi
	movq	-8(%rax), %rbp
	movq	%rbp, -8(%rdx)
	movq	%rsi, -8(%rax)
	movq	(%rdx), %rsi
	movq	(%rax), %rbp
	movq	%rbp, (%rdx)
	movq	%rsi, (%rax)
	addq	$16, %rax
	addq	$16, %rdx
	addq	$-2, %rcx
	jne	.LBB28_38
.LBB28_39:                              # %.outer
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	-64(%rsp), %edx         # 4-byte Reload
	movl	%r15d, -84(%rsp)        # 4-byte Spill
	cmpl	%edx, %r15d
	jle	.LBB28_41
# BB#40:                                # %.outer
                                        #   in Loop: Header=BB28_3 Depth=1
	cmpl	%ebx, %edx
	jg	.LBB28_3
.LBB28_41:                              # %.critedge
	movl	-60(%rsp), %eax         # 4-byte Reload
	addl	-80(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, opts_FirstNonOpt(%rip)
	movl	-76(%rsp), %eax         # 4-byte Reload
	movl	%eax, opts_LastNonOpt(%rip)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	opts_Exchange, .Lfunc_end28-opts_Exchange
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/options.c"
	.size	.L.str.1, 79

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"internal error: option with command line name '%s' redeclared.\n"
	.size	.L.str.2, 64

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	opts_DECLARATIONS,@object # @opts_DECLARATIONS
	.local	opts_DECLARATIONS
	.comm	opts_DECLARATIONS,8,8
	.type	opts_PARAMETERS,@object # @opts_PARAMETERS
	.local	opts_PARAMETERS
	.comm	opts_PARAMETERS,8,8
	.type	opts_IdNextAvailable,@object # @opts_IdNextAvailable
	.local	opts_IdNextAvailable
	.comm	opts_IdNextAvailable,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%-18s "
	.size	.L.str.4, 7

	.type	opts_Arg,@object        # @opts_Arg
	.local	opts_Arg
	.comm	opts_Arg,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nerror, option %s requires argument.\n"
	.size	.L.str.5, 38

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"1"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\ninternal error: option %c not found.\n"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nerror: option %c requires argument.\n"
	.size	.L.str.8, 38

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"--"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nerror: option %c has delimiter -- as argument.\n"
	.size	.L.str.10, 49

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nerror: argument of option %s must be integer.\n"
	.size	.L.str.11, 48

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nerror: argument '%s' of option '%s' must be integer.\n"
	.size	.L.str.12, 55

	.type	opts_Ind,@object        # @opts_Ind
	.data
	.p2align	2
opts_Ind:
	.long	1                       # 0x1
	.size	opts_Ind, 4

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"\n\n"
	.size	.L.str.13, 3

	.type	.L.str.14,@object       # @.str.14
	.section	.rodata,"a",@progbits
.L.str.14:
	.zero	2
	.size	.L.str.14, 2

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	":"
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.16, 50

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.17, 50

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"error: option %s is multiply defined.\n"
	.size	.L.str.18, 39

	.type	opts_GetOptInitialized,@object # @opts_GetOptInitialized
	.local	opts_GetOptInitialized
	.comm	opts_GetOptInitialized,1,4
	.type	opts_NextChar,@object   # @opts_NextChar
	.local	opts_NextChar
	.comm	opts_NextChar,8,8
	.type	opts_LastNonOpt,@object # @opts_LastNonOpt
	.local	opts_LastNonOpt
	.comm	opts_LastNonOpt,4,4
	.type	opts_FirstNonOpt,@object # @opts_FirstNonOpt
	.local	opts_FirstNonOpt
	.comm	opts_FirstNonOpt,4,4
	.type	opts_Ordering,@object   # @opts_Ordering
	.local	opts_Ordering
	.comm	opts_Ordering,4,4
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s: option `%s' is ambiguous\n"
	.size	.L.str.19, 30

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%s: option `--%s' doesn't allow an argument\n"
	.size	.L.str.20, 45

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s: option `%c%s' doesn't allow an argument\n"
	.size	.L.str.21, 45

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s: option `%s' requires an argument\n"
	.size	.L.str.22, 38

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%s: unrecognized option `--%s'\n"
	.size	.L.str.23, 32

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%s: unrecognized option `%c%s'\n"
	.size	.L.str.24, 32

	.type	opts_PosixlyCorrect,@object # @opts_PosixlyCorrect
	.local	opts_PosixlyCorrect
	.comm	opts_PosixlyCorrect,8,8
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%s: illegal option -- %c\n"
	.size	.L.str.26, 26

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%s: invalid option -- %c\n"
	.size	.L.str.27, 26

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"%s: option requires an argument -- %c\n"
	.size	.L.str.28, 39

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%s: option `-W %s' is ambiguous\n"
	.size	.L.str.29, 33

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s: option `-W %s' doesn't allow an argument\n"
	.size	.L.str.30, 46

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"POSIXLY_CORRECT"
	.size	.L.str.31, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
