	.text
	.file	"adpcm.bc"
	.globl	adpcm_coder
	.p2align	4, 0x90
	.type	adpcm_coder,@function
adpcm_coder:                            # @adpcm_coder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movswl	(%rcx), %r8d
	movsbl	2(%rcx), %r12d
	testl	%edx, %edx
	jle	.LBB0_12
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movslq	%r12d, %rax
	movl	stepsizeTable(,%rax,4), %r14d
	incl	%edx
	movl	$1, %eax
	xorl	%esi, %esi
                                        # implicit-def: %ECX
	movl	%ecx, -20(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %r15d
	movswl	(%rdi), %r9d
	subl	%r8d, %r9d
	movl	%r9d, %ecx
	negl	%ecx
	cmovll	%r9d, %ecx
	movl	%r9d, %r13d
	shrl	$28, %r13d
	andl	$8, %r13d
	movl	%r14d, %eax
	sarl	$3, %eax
	xorl	%ebp, %ebp
	cmpl	%r14d, %ecx
	setge	%bpl
	movl	%r14d, %ebx
	cmovll	%esi, %ebx
	shll	$2, %ebp
	subl	%ebx, %ecx
	addl	%eax, %ebx
	movl	%r14d, %r10d
	sarl	%r10d
	movl	%ecx, %r11d
	subl	%r10d, %r11d
	jge	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	%ecx, %r11d
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	orl	$2, %ebp
	addl	%r10d, %ebx
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	sarl	$2, %r14d
	xorl	%eax, %eax
	cmpl	%r14d, %r11d
	setge	%al
	cmovll	%esi, %r14d
	addl	%ebx, %r14d
	movl	%r14d, %ecx
	negl	%ecx
	testl	%r9d, %r9d
	cmovnsl	%r14d, %ecx
	addl	%r8d, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	movl	$-32768, %r8d           # imm = 0x8000
	cmovgl	%ecx, %r8d
	cmpl	$32767, %ecx            # imm = 0x7FFF
	movl	$32767, %ecx            # imm = 0x7FFF
	cmovgl	%ecx, %r8d
	orl	%r13d, %ebp
	orl	%eax, %ebp
	movslq	%ebp, %rax
	addl	indexTable(,%rax,4), %r12d
	cmovsl	%esi, %r12d
	cmpl	$89, %r12d
	movl	$88, %eax
	cmovgel	%eax, %r12d
	andl	$15, %ebp
	testl	%r15d, %r15d
	je	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	shll	$4, %ebp
	movl	%ebp, -20(%rsp)         # 4-byte Spill
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	orl	-20(%rsp), %ebp         # 4-byte Folded Reload
	movq	-16(%rsp), %rax         # 8-byte Reload
	movb	%bpl, (%rax)
	incq	%rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %rdi
	movl	stepsizeTable(,%r12,4), %r14d
	xorl	%eax, %eax
	testl	%r15d, %r15d
	sete	%al
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB0_2
# BB#9:                                 # %._crit_edge
	testl	%r15d, %r15d
	je	.LBB0_11
# BB#10:
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	-20(%rsp), %ecx         # 4-byte Reload
	movb	%cl, (%rax)
.LBB0_11:                               # %._crit_edge.thread
	movq	-8(%rsp), %rcx          # 8-byte Reload
.LBB0_12:                               # %._crit_edge.thread
	movw	%r8w, (%rcx)
	movb	%r12b, 2(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	adpcm_coder, .Lfunc_end0-adpcm_coder
	.cfi_endproc

	.globl	adpcm_decoder
	.p2align	4, 0x90
	.type	adpcm_decoder,@function
adpcm_decoder:                          # @adpcm_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movswl	(%rcx), %r8d
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movsbl	2(%rcx), %r9d
	testl	%edx, %edx
	jle	.LBB1_6
# BB#1:                                 # %.lr.ph.preheader
	movslq	%r9d, %rbp
	movl	stepsizeTable(,%rbp,4), %r12d
	incl	%edx
	xorl	%ecx, %ecx
	movl	$88, %r10d
	movl	$32767, %r11d           # imm = 0x7FFF
	movl	$0, %eax
                                        # implicit-def: %R13D
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	je	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	%r13d, %r14d
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movsbl	(%rdi), %r14d
	incq	%rdi
	movl	%r14d, %r13d
	shrl	$4, %r13d
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%r13d, %ebp
	andl	$15, %ebp
	xorl	%r15d, %r15d
	testl	%eax, %eax
	sete	%r15b
	addl	indexTable(,%rbp,4), %r9d
	cmovsl	%ecx, %r9d
	cmpl	$89, %r9d
	cmovgel	%r10d, %r9d
	movl	%r12d, %eax
	sarl	$3, %eax
	movl	%r13d, %ebp
	shll	$29, %ebp
	sarl	$31, %ebp
	andl	%r12d, %ebp
	addl	%eax, %ebp
	movl	%r13d, %eax
	shll	$30, %eax
	sarl	$31, %eax
	movl	%r12d, %ebx
	sarl	%ebx
	andl	%eax, %ebx
	addl	%ebp, %ebx
	movl	%r13d, %eax
	andl	$1, %eax
	negl	%eax
	sarl	$2, %r12d
	andl	%eax, %r12d
	addl	%ebx, %r12d
	movl	%r12d, %eax
	negl	%eax
	testb	$8, %r13b
	cmovel	%r12d, %eax
	addl	%r8d, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	movl	$-32768, %r8d           # imm = 0x8000
	cmovgl	%eax, %r8d
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgl	%r11d, %r8d
	movl	stepsizeTable(,%r9,4), %r12d
	movw	%r8w, (%rsi)
	addq	$2, %rsi
	decl	%edx
	cmpl	$1, %edx
	movl	%r15d, %eax
	movl	%r14d, %r13d
	jg	.LBB1_2
.LBB1_6:                                # %._crit_edge
	movq	-8(%rsp), %rax          # 8-byte Reload
	movw	%r8w, (%rax)
	movb	%r9b, 2(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	adpcm_decoder, .Lfunc_end1-adpcm_decoder
	.cfi_endproc

	.type	stepsizeTable,@object   # @stepsizeTable
	.section	.rodata,"a",@progbits
	.p2align	4
stepsizeTable:
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	28                      # 0x1c
	.long	31                      # 0x1f
	.long	34                      # 0x22
	.long	37                      # 0x25
	.long	41                      # 0x29
	.long	45                      # 0x2d
	.long	50                      # 0x32
	.long	55                      # 0x37
	.long	60                      # 0x3c
	.long	66                      # 0x42
	.long	73                      # 0x49
	.long	80                      # 0x50
	.long	88                      # 0x58
	.long	97                      # 0x61
	.long	107                     # 0x6b
	.long	118                     # 0x76
	.long	130                     # 0x82
	.long	143                     # 0x8f
	.long	157                     # 0x9d
	.long	173                     # 0xad
	.long	190                     # 0xbe
	.long	209                     # 0xd1
	.long	230                     # 0xe6
	.long	253                     # 0xfd
	.long	279                     # 0x117
	.long	307                     # 0x133
	.long	337                     # 0x151
	.long	371                     # 0x173
	.long	408                     # 0x198
	.long	449                     # 0x1c1
	.long	494                     # 0x1ee
	.long	544                     # 0x220
	.long	598                     # 0x256
	.long	658                     # 0x292
	.long	724                     # 0x2d4
	.long	796                     # 0x31c
	.long	876                     # 0x36c
	.long	963                     # 0x3c3
	.long	1060                    # 0x424
	.long	1166                    # 0x48e
	.long	1282                    # 0x502
	.long	1411                    # 0x583
	.long	1552                    # 0x610
	.long	1707                    # 0x6ab
	.long	1878                    # 0x756
	.long	2066                    # 0x812
	.long	2272                    # 0x8e0
	.long	2499                    # 0x9c3
	.long	2749                    # 0xabd
	.long	3024                    # 0xbd0
	.long	3327                    # 0xcff
	.long	3660                    # 0xe4c
	.long	4026                    # 0xfba
	.long	4428                    # 0x114c
	.long	4871                    # 0x1307
	.long	5358                    # 0x14ee
	.long	5894                    # 0x1706
	.long	6484                    # 0x1954
	.long	7132                    # 0x1bdc
	.long	7845                    # 0x1ea5
	.long	8630                    # 0x21b6
	.long	9493                    # 0x2515
	.long	10442                   # 0x28ca
	.long	11487                   # 0x2cdf
	.long	12635                   # 0x315b
	.long	13899                   # 0x364b
	.long	15289                   # 0x3bb9
	.long	16818                   # 0x41b2
	.long	18500                   # 0x4844
	.long	20350                   # 0x4f7e
	.long	22385                   # 0x5771
	.long	24623                   # 0x602f
	.long	27086                   # 0x69ce
	.long	29794                   # 0x7462
	.long	32767                   # 0x7fff
	.size	stepsizeTable, 356

	.type	indexTable,@object      # @indexTable
	.p2align	4
indexTable:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.size	indexTable, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
