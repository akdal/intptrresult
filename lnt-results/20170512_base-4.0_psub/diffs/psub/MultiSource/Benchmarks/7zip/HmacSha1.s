	.text
	.file	"HmacSha1.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16,54
.LCPI0_1:
	.zero	16,106
	.text
	.globl	_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm,@function
_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm:    # @_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 112
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$64, %r14
	jbe	.LBB0_1
# BB#3:
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%r12)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	jmp	.LBB0_4
.LBB0_1:                                # %.preheader19
	testq	%r14, %r14
	je	.LBB0_4
# BB#2:                                 # %.lr.ph.preheader
	movq	%rsp, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	memcpy
.LBB0_4:                                # %vector.body27
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54]
	movaps	(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, (%rsp)
	movaps	16(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)
	movaps	32(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)
	xorps	48(%rsp), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%r12)
	movq	%rsp, %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [106,106,106,106,106,106,106,106,106,106,106,106,106,106,106,106]
	movaps	(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, (%rsp)
	movaps	16(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)
	movaps	32(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)
	xorps	48(%rsp), %xmm0
	movaps	%xmm0, 48(%rsp)
	leaq	104(%r12), %rbx
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 136(%r12)
	movl	$64, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm, .Lfunc_end0-_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha15CHmac5FinalEPhm,@function
_ZN7NCrypto5NSha15CHmac5FinalEPhm:      # @_ZN7NCrypto5NSha15CHmac5FinalEPhm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rsp, %r12
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	addq	$104, %rbx
	movl	$20, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsp, %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memcpy
.LBB1_2:                                # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN7NCrypto5NSha15CHmac5FinalEPhm, .Lfunc_end1-_ZN7NCrypto5NSha15CHmac5FinalEPhm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16,54
.LCPI2_1:
	.zero	16,106
	.text
	.globl	_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm,@function
_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm:  # @_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$208, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 256
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r13, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r15
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	cmpq	$64, %r14
	jbe	.LBB2_1
# BB#10:
	leaq	104(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 136(%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	leaq	32(%rsp), %r14
	movl	4(%rsp), %eax
	movl	12(%rsp), %ecx
	shll	$8, %ecx
	movzbl	8(%rsp), %edx
	orl	%ecx, %edx
	shll	$8, %eax
	movzbl	(%rsp), %ecx
	orl	%eax, %ecx
	pinsrw	$0, %ecx, %xmm1
	pinsrw	$1, %edx, %xmm1
	pxor	%xmm0, %xmm0
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pxor	%xmm3, %xmm3
	pslld	$24, %xmm1
	movzbl	13(%rsp), %eax
	shll	$8, %eax
	movzbl	9(%rsp), %ecx
	orl	%eax, %ecx
	movzbl	5(%rsp), %eax
	shll	$8, %eax
	movzbl	1(%rsp), %edx
	orl	%eax, %edx
	pinsrw	$0, %edx, %xmm0
	pinsrw	$1, %ecx, %xmm0
	punpcklbw	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3],xmm0[4],xmm3[4],xmm0[5],xmm3[5],xmm0[6],xmm3[6],xmm0[7],xmm3[7]
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	pslld	$16, %xmm0
	por	%xmm1, %xmm0
	movzbl	14(%rsp), %eax
	shll	$8, %eax
	movzbl	10(%rsp), %ecx
	orl	%eax, %ecx
	movzbl	6(%rsp), %eax
	shll	$8, %eax
	movzbl	2(%rsp), %edx
	orl	%eax, %edx
	pinsrw	$0, %edx, %xmm2
	pinsrw	$1, %ecx, %xmm2
	punpcklbw	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3],xmm2[4],xmm3[4],xmm2[5],xmm3[5],xmm2[6],xmm3[6],xmm2[7],xmm3[7]
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	pslld	$8, %xmm2
	movzbl	15(%rsp), %eax
	shll	$8, %eax
	movzbl	11(%rsp), %ecx
	orl	%eax, %ecx
	movzbl	7(%rsp), %eax
	shll	$8, %eax
	movzbl	3(%rsp), %edx
	orl	%eax, %edx
	pinsrw	$0, %edx, %xmm1
	pinsrw	$1, %ecx, %xmm1
	punpcklbw	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3],xmm1[4],xmm3[4],xmm1[5],xmm3[5],xmm1[6],xmm3[6],xmm1[7],xmm3[7]
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	por	%xmm2, %xmm1
	por	%xmm0, %xmm1
	movdqa	%xmm1, 32(%rsp)
	movl	16(%rsp), %eax
	shll	$24, %eax
	movzbl	17(%rsp), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	movzbl	18(%rsp), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	19(%rsp), %eax
	orl	%edx, %eax
	movl	%eax, 48(%rsp)
	movd	%xmm1, %edi
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movd	%xmm0, %esi
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %edx
	pshufd	$231, %xmm1, %xmm0      # xmm0 = xmm1[3,1,2,3]
	movd	%xmm0, %ecx
	jmp	.LBB2_11
.LBB2_1:                                # %.preheader33
	testq	%r14, %r14
	je	.LBB2_9
# BB#2:                                 # %.lr.ph.preheader
	testb	$1, %r14b
	jne	.LBB2_4
# BB#3:
	xorl	%eax, %eax
	cmpq	$1, %r14
	jne	.LBB2_6
	jmp	.LBB2_8
.LBB2_9:                                # %.preheader33..preheader.preheader_crit_edge
	xorl	%eax, %eax
	leaq	32(%rsp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	.LBB2_11
.LBB2_4:                                # %.lr.ph.prol
	movzbl	(%r13), %eax
	shll	$24, %eax
	orl	%eax, 32(%rsp)
	movl	$1, %eax
	cmpq	$1, %r14
	je	.LBB2_8
.LBB2_6:                                # %.lr.ph.preheader.new
	leal	8(,%rax,8), %edx
	leal	(,%rax,8), %esi
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13,%rax), %edi
	movl	%esi, %ecx
	notl	%ecx
	andb	$24, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movq	%rax, %rcx
	andq	$-4, %rcx
	orl	%edi, 32(%rsp,%rcx)
	leaq	1(%rax), %rdi
	movzbl	1(%r13,%rax), %ebx
	movl	%edx, %ecx
	notl	%ecx
	andb	$24, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	andq	$-4, %rdi
	orl	%ebx, 32(%rsp,%rdi)
	addq	$2, %rax
	addl	$16, %edx
	addl	$16, %esi
	cmpq	%rax, %r14
	jne	.LBB2_7
.LBB2_8:                                # %.preheader.preheader.loopexit
	leaq	32(%rsp), %r14
	movl	32(%rsp), %edi
	movl	36(%rsp), %esi
	movl	40(%rsp), %edx
	movl	44(%rsp), %ecx
	movl	48(%rsp), %eax
.LBB2_11:                               # %.preheader.preheader
	xorl	$909522486, %edi        # imm = 0x36363636
	movl	%edi, 32(%rsp)
	xorl	$909522486, %esi        # imm = 0x36363636
	movl	%esi, 36(%rsp)
	xorl	$909522486, %edx        # imm = 0x36363636
	movl	%edx, 40(%rsp)
	xorl	$909522486, %ecx        # imm = 0x36363636
	movl	%ecx, 44(%rsp)
	xorl	$909522486, %eax        # imm = 0x36363636
	movl	%eax, 48(%rsp)
	movups	52(%rsp), %xmm0
	movaps	.LCPI2_0(%rip), %xmm1   # xmm1 = [909522486,909522486,909522486,909522486]
	xorps	%xmm1, %xmm0
	movups	%xmm0, 52(%rsp)
	movups	68(%rsp), %xmm0
	xorps	%xmm1, %xmm0
	movups	%xmm0, 68(%rsp)
	xorl	$909522486, 84(%rsp)    # imm = 0x36363636
	xorl	$909522486, 88(%rsp)    # imm = 0x36363636
	xorl	$909522486, 92(%rsp)    # imm = 0x36363636
	movq	%r15, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%r15)
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	xorl	$1785358954, 32(%rsp)   # imm = 0x6A6A6A6A
	xorl	$1785358954, 36(%rsp)   # imm = 0x6A6A6A6A
	xorl	$1785358954, 40(%rsp)   # imm = 0x6A6A6A6A
	xorl	$1785358954, 44(%rsp)   # imm = 0x6A6A6A6A
	xorl	$1785358954, 48(%rsp)   # imm = 0x6A6A6A6A
	movups	52(%rsp), %xmm0
	movaps	.LCPI2_1(%rip), %xmm1   # xmm1 = [1785358954,1785358954,1785358954,1785358954]
	xorps	%xmm1, %xmm0
	movups	%xmm0, 52(%rsp)
	xorl	$1785358954, 68(%rsp)   # imm = 0x6A6A6A6A
	movups	72(%rsp), %xmm0
	xorps	%xmm1, %xmm0
	movups	%xmm0, 72(%rsp)
	xorl	$1785358954, 88(%rsp)   # imm = 0x6A6A6A6A
	xorl	$1785358954, 92(%rsp)   # imm = 0x6A6A6A6A
	leaq	104(%r15), %rbx
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 136(%r15)
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm, .Lfunc_end2-_ZN7NCrypto5NSha17CHmac326SetKeyEPKhm
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha17CHmac325FinalEPjm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha17CHmac325FinalEPjm,@function
_ZN7NCrypto5NSha17CHmac325FinalEPjm:    # @_ZN7NCrypto5NSha17CHmac325FinalEPjm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -40
.Lcfi35:
	.cfi_offset %r12, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rsp, %r12
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha110CContext325FinalEPj
	addq	$104, %rbx
	movl	$5, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha110CContext325FinalEPj
	testq	%r15, %r15
	je	.LBB3_2
# BB#1:                                 # %.lr.ph.preheader
	shlq	$2, %r15
	movq	%rsp, %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memcpy
.LBB3_2:                                # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN7NCrypto5NSha17CHmac325FinalEPjm, .Lfunc_end3-_ZN7NCrypto5NSha17CHmac325FinalEPjm
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj,@function
_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj: # @_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 192
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %rsi
	movl	$5, %edx
	callq	_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj
	leaq	104(%r15), %r12
	leaq	64(%rsp), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	callq	_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj
	movl	16(%r14), %eax
	movl	%eax, 16(%rsp)
	movups	(%r14), %xmm0
	movaps	%xmm0, (%rsp)
	testl	%ebx, %ebx
	je	.LBB4_3
# BB#1:
	movq	%rsp, %r13
	leaq	64(%rsp), %rbp
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	movups	(%r14), %xmm0
	xorps	(%rsp), %xmm0
	movups	%xmm0, (%r14)
	movl	16(%rsp), %eax
	xorl	%eax, 16(%r14)
	decl	%ebx
	jne	.LBB4_2
.LBB4_3:                                # %._crit_edge
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj, .Lfunc_end4-_ZN7NCrypto5NSha17CHmac3216GetLoopXorDigestEPjj
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
