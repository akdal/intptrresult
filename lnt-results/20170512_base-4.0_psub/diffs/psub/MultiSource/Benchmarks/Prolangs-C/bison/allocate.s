	.text
	.file	"allocate.bc"
	.globl	mallocate
	.p2align	4, 0x90
	.type	mallocate,@function
mallocate:                              # @mallocate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
.LBB0_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.LBB0_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	done
	jmp	.LBB0_2
.Lfunc_end0:
	.size	mallocate, .Lfunc_end0-mallocate
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"bison: memory exhausted\n"
	.size	.L.str, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
